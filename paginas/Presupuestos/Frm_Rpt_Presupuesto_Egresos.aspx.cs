﻿using System;
using System.Diagnostics;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Text;
using JAPAMI.DateDiff;
using System.Text.RegularExpressions;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Grupos_Dependencias.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;
using Excel = Microsoft.Office.Interop.Excel;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Catalogo_Compras_Partidas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;

using JAPAMI.Psp_Presupuesto.Negocio;

public partial class paginas_Paginas_Presupuestos_Frm_Rpt_Presupuesto_Egresos : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Inicializa_Controles " + Ex.Message.ToString());
            }

        }
    #endregion

    #region(Metodos)
        #region(Metodos Generales)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Inicializa_Controles
        /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
        ///               realizar diferentes operaciones
        /// PARAMETROS  : 
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 03/Diciembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Limpiar_Controles(); //Limpia los controles del formulario
                Cargar_Combo_Año();
                Llenar_Combo_Tipo_Reporte();
                Llenar_Combo_FF();
                Llenar_Combo_Programas();
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Inicializa_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Limpiar_Controles
        /// DESCRIPCION : Limpia los controles que se encuentran en la forma
        /// PARAMETROS  : 
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 03/Diciembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                DateTime Tiempo_Ahora = DateTime.Now;
                String Año ="" +Tiempo_Ahora.Year;
                //Cmb_Anio.SelectedIndex=Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(Año));
                Cmb_Anio.SelectedIndex = -1;
                Cmb_Tipo_Reporte.SelectedIndex = -1;
                Cmb_UR.SelectedIndex = -1;
                Cmb_PP.SelectedIndex = -1;
                Cmb_Partida.SelectedIndex = -1;
                Cmb_FF.SelectedIndex = -1;
                Chk_Todos.Checked = false;
                Chk_Dependencia.Checked = false;
                Chk_Unidad_Responsable.Checked = false;
                Chk_Programa.Checked = false;
                Chk_Fte_Financiamiento.Checked = false;
                Chk_Partida.Checked = false;
                Chk_Analitico.Checked = false; 
                Chk_Analitico_Detallado.Checked = false;
                Lbl_Mensaje_Error.Text = "";
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
        ///DESCRIPCIÓN          : metodo para obtener el grupo dependencia para el reporte
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 18/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private String Obtener_Gpo_Dependencia(String Tipo) 
        {
            Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio();
            String Dato = String.Empty;
            DataTable Dt_Datos = new DataTable();

            try
            {
                Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                Dt_Datos = Negocios.Consultar_Gpo_Dependencia();

                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        if (Tipo.Trim().Equals("UR"))
                        {
                            Dato = Dt_Datos.Rows[0]["UR"].ToString().Trim();
                        }
                        else
                        {
                            Dato = Dt_Datos.Rows[0]["GPO_DEP"].ToString().Trim();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener el grupo dependencia. Error[" + Ex.Message + "]");
            }
            return Dato;
        }
        #endregion

        #region(Control Acceso Pagina)
        /// ******************************************************************************
        /// NOMBRE: Configuracion_Acceso
        /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
        /// PARÁMETROS  :
        /// USUARIO CREÓ: Hugo Enrique Ramírez Aguilera
        /// FECHA CREÓ  : 03/Diciembre/2011 
        /// USUARIO MODIFICO  :
        /// FECHA MODIFICO    :
        /// CAUSA MODIFICACIÓN:
        /// ******************************************************************************
        protected void Configuracion_Acceso(String URL_Pagina)
        {
            List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
            DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

            try
            {
                //Agregamos los botones a la lista de botones de la página.

                Botones.Add(Btn_Salir);
                //Botones.Add(Btn_Reporte_Grupo_Dependencia);

                if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
                {
                    if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                    {
                        //Consultamos el menu de la página.
                        Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                        if (Dr_Menus.Length > 0)
                        {
                            //Validamos que el menu consultado corresponda a la página a validar.
                            if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                            {
                                Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                            }
                            else
                            {
                                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Es_Numero
        /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
        /// PARÁMETROS  : Cadena.- El dato a evaluar si es numerico.
        /// CREO        : Hugo Enrique Ramírez Aguilera
        /// FECHA_CREO  : 03/Diciembre/2011 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private Boolean Es_Numero(String Cadena)
        {
            Boolean Resultado = true;
            Char[] Array = Cadena.ToCharArray();
            try
            {
                for (int index = 0; index < Array.Length; index++)
                {
                    if (!Char.IsDigit(Array[index])) return false;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
            }
            return Resultado;
        }
        #endregion

        #region(Reporte)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 29/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Generar_Reporte_CAL(String UR, String FF, String PP, String P, String Tipo, String Dependencia, String Gpo_Dependencia)
        {
            Cls_Rpt_Presupuesto_Egresos_Negocio Negocio = new Cls_Rpt_Presupuesto_Egresos_Negocio();//instancia con capa de negocios
            DataTable Dt_Dependencia = new DataTable();
            DataTable Dt_UR = new DataTable();
            DataTable Dt_Programa = new DataTable();
            DataTable Dt_FF = new DataTable();
            DataTable Dt_Partida = new DataTable();
            DataTable Dt_Analitico = new DataTable();
            String Anio = Cmb_Anio.SelectedItem.Value.Trim();
            Double Cantidad = 0.00;
            String Clasificacion_Adm = String.Empty;

            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";
            try
            {
                //obtenemos la clasificacion administrativa
                Clasificacion_Adm = Negocio.Consultar_Clasificacion_Administrativa();

                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI_RH";

                #region Estilos
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1 = Libro.Styles.Add("HeaderStyle1");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1_2 = Libro.Styles.Add("Estilo_Cabecera1_2");
                //Creamos el estilo cabecera 2 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                //Creamos el estilo cabecera 3 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto = Libro.Styles.Add("Presupuesto");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Neg = Libro.Styles.Add("Presupuesto_Neg");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Presupuesto_Total");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total_Neg = Libro.Styles.Add("Presupuesto_Total_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto = Libro.Styles.Add("Concepto");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto_Total = Libro.Styles.Add("Concepto_Total");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Partida_Total = Libro.Styles.Add("Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Neg = Libro.Styles.Add("Total_Neg");

                //estilo para la cabecera
                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 12;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "blue";
                Estilo_Cabecera.Interior.Color = "white";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1.Font.FontName = "Tahoma";
                Estilo_Cabecera1.Font.Size = 12;
                Estilo_Cabecera1.Font.Bold = true;
                Estilo_Cabecera1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1.Font.Color = "#000000";
                Estilo_Cabecera1.Interior.Color = "white";
                Estilo_Cabecera1.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1_2.Font.FontName = "Tahoma";
                Estilo_Cabecera1_2.Font.Size = 12;
                Estilo_Cabecera1_2.Font.Bold = true;
                Estilo_Cabecera1_2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1_2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1_2.Font.Color = "#000000";
                Estilo_Cabecera1_2.Interior.Color = "white";
                Estilo_Cabecera1_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera2
                Estilo_Cabecera2.Font.FontName = "Tahoma";
                Estilo_Cabecera2.Font.Size = 10;
                Estilo_Cabecera2.Font.Bold = true;
                Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera2.Font.Color = "#FFFFFF";
                Estilo_Cabecera2.Interior.Color = "DarkGray";
                Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera3
                Estilo_Cabecera3.Font.FontName = "Tahoma";
                Estilo_Cabecera3.Font.Size = 10;
                Estilo_Cabecera3.Font.Bold = true;
                Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera3.Font.Color = "#000000";
                Estilo_Cabecera3.Interior.Color = "white";
                Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el contenido
                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto_Total.Font.FontName = "Tahoma";
                Estilo_Concepto_Total.Font.Size = 9;
                Estilo_Concepto_Total.Font.Bold = false;
                Estilo_Concepto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Concepto_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto_Total.Font.Color = "#000000";
                Estilo_Concepto_Total.Interior.Color = "#66FFCC";
                Estilo_Concepto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto.Font.FontName = "Tahoma";
                Estilo_Concepto.Font.Size = 9;
                Estilo_Concepto.Font.Bold = false;
                Estilo_Concepto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Concepto.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto.Font.Color = "#000000";
                Estilo_Concepto.Interior.Color = "White";
                Estilo_Concepto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto.Font.FontName = "Tahoma";
                Estilo_Presupuesto.Font.Size = 9;
                Estilo_Presupuesto.Font.Bold = false;
                Estilo_Presupuesto.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto.Font.Color = "#000000";
                Estilo_Presupuesto.Interior.Color = "White";
                Estilo_Presupuesto.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Neg.Font.Size = 9;
                Estilo_Presupuesto_Neg.Font.Bold = false;
                Estilo_Presupuesto_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto_Neg.Font.Color = "red";
                Estilo_Presupuesto_Neg.Interior.Color = "White";
                Estilo_Presupuesto_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total.Font.Size = 9;
                Estilo_Presupuesto_Total.Font.Bold = true;
                Estilo_Presupuesto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total.Font.Color = "#000000";
                Estilo_Presupuesto_Total.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total_Neg.Font.Size = 9;
                Estilo_Presupuesto_Total_Neg.Font.Bold = true;
                Estilo_Presupuesto_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total_Neg.Font.Color = "red";
                Estilo_Presupuesto_Total_Neg.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //para partida totales de todos los capitulos
                Estilo_Partida_Total.Font.FontName = "Tahoma";
                Estilo_Partida_Total.Font.Size = 9;
                Estilo_Partida_Total.Font.Bold = true;
                Estilo_Partida_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Partida_Total.Font.Color = "#000000";
                Estilo_Partida_Total.Interior.Color = "LightGreen";
                Estilo_Partida_Total.NumberFormat = "#,###,##0.00";
                Estilo_Partida_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Partida_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total.Font.FontName = "Tahoma";
                Estilo_Total.Font.Size = 9;
                Estilo_Total.Font.Bold = true;
                Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total.Font.Color = "#000000";
                Estilo_Total.Interior.Color = "#99CCCC";
                Estilo_Total.NumberFormat = "#,###,##0.00";
                Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Total_Neg.Font.Size = 9;
                Estilo_Total_Neg.Font.Bold = true;
                Estilo_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Neg.Font.Color = "red";
                Estilo_Total_Neg.Interior.Color = "#99CCCC";
                Estilo_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region Pestaña Dependencia
                if (Chk_Dependencia.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("RAMO ADMINISTRATIVO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon = Hoja.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("RAMO DEL CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G.", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RAMO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO  DE EGRESOS  " + Anio, "HeaderStyle2"));

                    /*pasos a realizar: se buscan los grupos de dependencia,
                    se continua buscando las dependencias a las que pertenecen al grupo de dependendcia,
                    las dependencias se les busca si tienen algun presupuesto y se le suma hasta tener 
                    el presupuesto de egresos del grupo de dependencias al que pertenecen las dependencias buscadas*/

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_Dependencia = Negocio.Consultar_Datos_Dependencia_CAL();

                    if (Dt_Dependencia != null && Dt_Dependencia.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Dependencia.Rows)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Dependencia.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm.Trim(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña UR
                if (Chk_Unidad_Responsable.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("GERENCIA");

                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2 = Hoja2.Table.Rows.Add();

                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon2 = Hoja2.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";

                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";

                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PP", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS  " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_UR = Negocio.Consultar_Datos_UR_CAL();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_UR.Rows)
                        {
                            Renglon2 = Hoja2.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_UR.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF")
                                            || Columna.ColumnName.Equals("AF") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF")
                                            || Columna.ColumnName.Equals("AF") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Programa
                if (Chk_Programa.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("PROGRAMA O PROYECTO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3 = Hoja3.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon3 = Hoja3.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("PROGRAMA O PROYECTO");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PROGRAMA O PROYECTO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_Programa = Negocio.Consultar_Datos_Programas_CAL();

                    if (Dt_Programa != null && Dt_Programa.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Programa.Rows)
                        {
                            Renglon3 = Hoja3.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Programa.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña FF
                if (Chk_Fte_Financiamiento.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja4 = Libro.Worksheets.Add("CLASIFICADOR POR FUENTE");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon4 = Hoja4.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon4 = Hoja4.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_FF = Negocio.Consultar_Datos_Fte_Financiamiento_CAL();

                    if (Dt_FF != null && Dt_FF.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_FF.Rows)
                        {
                            Renglon4 = Hoja4.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_FF.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Partida
                if (Chk_Partida.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja5 = Libro.Worksheets.Add("COG");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon5 = Hoja5.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon5 = Hoja5.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("CLASIFICADOR POR OBJETO DEL GASTO");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA ESPECIFICA", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_Partida = Negocio.Consultar_Datos_Partida_CAL();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon5 = Hoja5.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon5 = Hoja5.Table.Rows.Add();
                            }

                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico
                if (Chk_Analitico.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja6 = Libro.Worksheets.Add("ANALÍTICO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon6 = Hoja6.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 1
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//AREA FUNCIONAL 2.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//CA
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//Programa 3.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Unidad Responsable 4.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//Partida 5.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto 6.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto 7.

                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 7; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 7; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 7; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon6 = Hoja6.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 7; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CLASIFICACIÓN ANALÍTICA");
                    Celda.MergeAcross = 7; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.MergeAcross = 5; // Merge 4 cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon6.Cells.Add("CONCEPTO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("PRESUPUESTO  DE EGRESOS  " + Anio);
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";

                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("P.P", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA", "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_Partida = Negocio.Consultar_Datos_Analitico_CAL();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon6 = Hoja6.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon6 = Hoja6.Table.Rows.Add();
                            }

                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("TOTAL"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("TOTAL"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("TOTAL"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 7; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 7; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico Detallado
                if (Chk_Analitico_Detallado.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja7 = Libro.Worksheets.Add("ANALÍTICO DETALLADO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon7 = Hoja7.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(165));//CODIGO PROGRAMATICO
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Programa .
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Unidad Responsable.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//Partida.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//TOTAL.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ENERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//FEBRERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MARZO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABRIL.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MAYO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JUNIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JULIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AGOSTO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SEPTIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//OCTUBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//NOVIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DICIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//TOTAL.

                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 21; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 21; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 21; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon7 = Hoja7.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 21; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CLASIFICACIÓN ANALÍTICA DETALLADA");
                    Celda.MergeAcross = 21; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.StyleID = "HeaderStyle2";
                    //para la ff
                    Celda = Renglon7.Cells.Add("C.F.F");
                    Celda.StyleID = "HeaderStyle2";
                    //para la AREA FUNCIONAL
                    Celda = Renglon7.Cells.Add("C.F.G");
                    Celda.StyleID = "HeaderStyle2";
                    //para la 
                    Celda = Renglon7.Cells.Add("CA");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PROGRAMA
                    Celda = Renglon7.Cells.Add("P.P");
                    Celda.StyleID = "HeaderStyle2";
                    //para la UNIDAD RESPONSABLE
                    Celda = Renglon7.Cells.Add("GERENCIA");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PARTIDA
                    Celda = Renglon7.Cells.Add("PARTIDA");
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon7.Cells.Add("DESCRIPCIÓN");
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon7.Cells.Add("TOTAL " + Anio);
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ENERO
                    Celda = Renglon7.Cells.Add("ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE FEBRERO
                    Celda = Renglon7.Cells.Add("FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MARZO
                    Celda = Renglon7.Cells.Add("MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ABRIL
                    Celda = Renglon7.Cells.Add("ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MAYO
                    Celda = Renglon7.Cells.Add("MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JUNIO
                    Celda = Renglon7.Cells.Add("JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JULIO
                    Celda = Renglon7.Cells.Add("JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE AGOSTO
                    Celda = Renglon7.Cells.Add("AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE SEPTIEMBRE
                    Celda = Renglon7.Cells.Add("SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE OCTUBRE
                    Celda = Renglon7.Cells.Add("OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE NOVIEMBRE
                    Celda = Renglon7.Cells.Add("NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE DICIEMBRE
                    Celda = Renglon7.Cells.Add("DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon7.Cells.Add("TOTAL " + Anio);
                    Celda.StyleID = "HeaderStyle2";

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Negocio.P_Tipo_Reporte = Tipo;
                    Dt_Partida = Negocio.Consultar_Datos_Analitico_CAL();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon7 = Hoja7.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon7 = Hoja7.Table.Rows.Add();
                            }

                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 21; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 21; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();

            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + Ex.Message.ToString() + "]";
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_PSP
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 30/Mayo/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Generar_Reporte_PSP(String UR, String FF, String PP, String P, String Dependencia, String Gpo_Dependencia)
        {
            Cls_Rpt_Presupuesto_Egresos_Negocio Negocio = new Cls_Rpt_Presupuesto_Egresos_Negocio();//instancia con capa de negocios
            DataTable Dt_Dependencia = new DataTable();
            DataTable Dt_UR = new DataTable();
            DataTable Dt_Programa = new DataTable();
            DataTable Dt_FF = new DataTable();
            DataTable Dt_Partida = new DataTable();
            DataTable Dt_Analitico = new DataTable();
            String Anio = Cmb_Anio.SelectedItem.Value.Trim();
            Double Cantidad = 0.00;
            String Clasificacion_Adm = String.Empty;

            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";
            try
            {
                Clasificacion_Adm = Negocio.Consultar_Clasificacion_Administrativa();

                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI_RH";

                #region Estilos
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1 = Libro.Styles.Add("HeaderStyle1");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1_2 = Libro.Styles.Add("Estilo_Cabecera1_2");
                //Creamos el estilo cabecera 2 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                //Creamos el estilo cabecera 3 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto = Libro.Styles.Add("Presupuesto");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Neg = Libro.Styles.Add("Presupuesto_Neg");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Presupuesto_Total");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total_Neg = Libro.Styles.Add("Presupuesto_Total_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto = Libro.Styles.Add("Concepto");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto_Total = Libro.Styles.Add("Concepto_Total");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Partida_Total = Libro.Styles.Add("Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Neg = Libro.Styles.Add("Total_Neg");


                //estilo para la cabecera
                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 12;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "blue";
                Estilo_Cabecera.Interior.Color = "white";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1.Font.FontName = "Tahoma";
                Estilo_Cabecera1.Font.Size = 12;
                Estilo_Cabecera1.Font.Bold = true;
                Estilo_Cabecera1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1.Font.Color = "#000000";
                Estilo_Cabecera1.Interior.Color = "white";
                Estilo_Cabecera1.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1_2.Font.FontName = "Tahoma";
                Estilo_Cabecera1_2.Font.Size = 12;
                Estilo_Cabecera1_2.Font.Bold = true;
                Estilo_Cabecera1_2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1_2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1_2.Font.Color = "#000000";
                Estilo_Cabecera1_2.Interior.Color = "white";
                Estilo_Cabecera1_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera2
                Estilo_Cabecera2.Font.FontName = "Tahoma";
                Estilo_Cabecera2.Font.Size = 10;
                Estilo_Cabecera2.Font.Bold = true;
                Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera2.Font.Color = "#FFFFFF";
                Estilo_Cabecera2.Interior.Color = "DarkGray";
                Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera3
                Estilo_Cabecera3.Font.FontName = "Tahoma";
                Estilo_Cabecera3.Font.Size = 10;
                Estilo_Cabecera3.Font.Bold = true;
                Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera3.Font.Color = "#000000";
                Estilo_Cabecera3.Interior.Color = "white";
                Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el contenido
                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto_Total.Font.FontName = "Tahoma";
                Estilo_Concepto_Total.Font.Size = 9;
                Estilo_Concepto_Total.Font.Bold = false;
                Estilo_Concepto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Concepto_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto_Total.Font.Color = "#000000";
                Estilo_Concepto_Total.Interior.Color = "#66FFCC";
                Estilo_Concepto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto.Font.FontName = "Tahoma";
                Estilo_Concepto.Font.Size = 9;
                Estilo_Concepto.Font.Bold = false;
                Estilo_Concepto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Concepto.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto.Font.Color = "#000000";
                Estilo_Concepto.Interior.Color = "White";
                Estilo_Concepto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto.Font.FontName = "Tahoma";
                Estilo_Presupuesto.Font.Size = 9;
                Estilo_Presupuesto.Font.Bold = false;
                Estilo_Presupuesto.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto.Font.Color = "#000000";
                Estilo_Presupuesto.Interior.Color = "White";
                Estilo_Presupuesto.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Neg.Font.Size = 9;
                Estilo_Presupuesto_Neg.Font.Bold = false;
                Estilo_Presupuesto_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto_Neg.Font.Color = "red";
                Estilo_Presupuesto_Neg.Interior.Color = "White";
                Estilo_Presupuesto_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total.Font.Size = 9;
                Estilo_Presupuesto_Total.Font.Bold = true;
                Estilo_Presupuesto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total.Font.Color = "#000000";
                Estilo_Presupuesto_Total.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total_Neg.Font.Size = 9;
                Estilo_Presupuesto_Total_Neg.Font.Bold = true;
                Estilo_Presupuesto_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total_Neg.Font.Color = "red";
                Estilo_Presupuesto_Total_Neg.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //para partida totales de todos los capitulos
                Estilo_Partida_Total.Font.FontName = "Tahoma";
                Estilo_Partida_Total.Font.Size = 9;
                Estilo_Partida_Total.Font.Bold = true;
                Estilo_Partida_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Partida_Total.Font.Color = "#000000";
                Estilo_Partida_Total.Interior.Color = "LightGreen";
                Estilo_Partida_Total.NumberFormat = "#,###,##0.00";
                Estilo_Partida_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Partida_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total.Font.FontName = "Tahoma";
                Estilo_Total.Font.Size = 9;
                Estilo_Total.Font.Bold = true;
                Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total.Font.Color = "#000000";
                Estilo_Total.Interior.Color = "#99CCCC";
                Estilo_Total.NumberFormat = "#,###,##0.00";
                Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Total_Neg.Font.Size = 9;
                Estilo_Total_Neg.Font.Bold = true;
                Estilo_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Neg.Font.Color = "red";
                Estilo_Total_Neg.Interior.Color = "#99CCCC";
                Estilo_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region Pestaña Dependencia
                if (Chk_Dependencia.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("RAMO ADMINISTRATIVO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon = Hoja.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("RAMO DEL CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RAMO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO  DE EGRESOS  " + Anio, "HeaderStyle2"));

                    /*pasos a realizar: se buscan los grupos de dependencia,
                    se continua buscando las dependencias a las que pertenecen al grupo de dependendcia,
                    las dependencias se les busca si tienen algun presupuesto y se le suma hasta tener 
                    el presupuesto de egresos del grupo de dependencias al que pertenecen las dependencias buscadas*/

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_Dependencia = Negocio.Consultar_Datos_Dependencia_PSP();

                    if (Dt_Dependencia != null && Dt_Dependencia.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Dependencia.Rows)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Dependencia.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "PP")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "PP")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña UR
                if (Chk_Unidad_Responsable.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("GERENCIA");

                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2 = Hoja2.Table.Rows.Add();

                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon2 = Hoja2.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("P.P", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS  " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_UR = Negocio.Consultar_Datos_UR_PSP();

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_UR.Rows)
                        {
                            Renglon2 = Hoja2.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_UR.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Programa
                if (Chk_Programa.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("PROGRAMA O PROYECTO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3 = Hoja3.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon3 = Hoja3.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("PROGRAMA O PROYECTO");
                    Celda.MergeAcross =5; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PROGRAMA O PROYECTO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_Programa = Negocio.Consultar_Datos_Programas_PSP();

                    if (Dt_Programa != null && Dt_Programa.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Programa.Rows)
                        {
                            Renglon3 = Hoja3.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Programa.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))

                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña FF
                if (Chk_Fte_Financiamiento.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja4 = Libro.Worksheets.Add("CLASIFICADOR POR FUENTE");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon4 = Hoja4.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon4 = Hoja4.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO");
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_FF = Negocio.Consultar_Datos_Fte_Financiamiento_PSP();

                    if (Dt_FF != null && Dt_FF.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_FF.Rows)
                        {
                            Renglon4 = Hoja4.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_FF.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 2; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Partida
                if (Chk_Partida.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja5 = Libro.Worksheets.Add("COG");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon5 = Hoja5.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto.

                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon5 = Hoja5.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("CLASIFICADOR POR OBJETO DEL GASTO");
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA ESPECIFICA", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio + " AUTORIZADO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Anio, "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_Partida = Negocio.Consultar_Datos_Partida_PSP();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon5 = Hoja5.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon5 = Hoja5.Table.Rows.Add();
                            }

                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("PSP_EGRESO_AUTORIZADO")  || Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PSP_EGRESO_AUTORIZADO") || Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PSP_EGRESO_AUTORIZADO") || Columna.ColumnName.Equals("PRESUPUESTO_EGRESO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico
                if (Chk_Analitico.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja6 = Libro.Worksheets.Add("ANALÍTICO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon6 = Hoja6.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 1
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//AREA FUNCIONAL 2.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//AREA FUNCIONAL 2.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//Programa 3.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Unidad Responsable 4.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Partida 5.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto 6.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.

                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 8; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 8; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 8; // Merge 6 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon6 = Hoja6.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 8; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CLASIFICACIÓN ANALÍTICA");
                    Celda.MergeAcross = 8; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.MergeAcross = 5; // Merge 4 cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon6.Cells.Add("CONCEPTO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("PRESUPUESTO EGRESOS " + Anio + " AUTORIZADOO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS " + Anio);
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";

                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("P.P", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA", "HeaderStyle2"));

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_Partida = Negocio.Consultar_Datos_Analitico_PSP();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon6 = Hoja6.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon6 = Hoja6.Table.Rows.Add();
                            }
                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 8; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 8; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico Detallado
                if (Chk_Analitico_Detallado.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja7 = Libro.Worksheets.Add("ANALÍTICO DETALLADO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon7 = Hoja7.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(185));//CODIGO PROGRAMATICO
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Programa .
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Unidad Responsable.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//Partida.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACION.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCION.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DEVENGADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//EJERCIDO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//PAGADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//PRECOMPROMETIDO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//COMPROMETIDO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DISPONIBLE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ENERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//FEBRERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MARZO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABRIL.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MAYO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JUNIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JULIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AGOSTO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SEPTIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//OCTUBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//NOVIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DICIEMBRE.

                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 29; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 29; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 29; // Merge 6 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon7 = Hoja7.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 29; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CLASIFICACIÓN ANALÍTICA DETALLADA");
                    Celda.MergeAcross = 29; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.StyleID = "HeaderStyle2";
                    //para la ff
                    Celda = Renglon7.Cells.Add("C.F.F");
                    Celda.StyleID = "HeaderStyle2";
                    //para la AREA FUNCIONAL
                    Celda = Renglon7.Cells.Add("C.F.G");
                    Celda.StyleID = "HeaderStyle2";
                    //para la UNIDAD RESPONSABLE
                    Celda = Renglon7.Cells.Add("C.A");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PROGRAMA
                    Celda = Renglon7.Cells.Add("P.P");
                    Celda.StyleID = "HeaderStyle2";
                    //para la UNIDAD RESPONSABLE
                    Celda = Renglon7.Cells.Add("GERENCIA");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PARTIDA
                    Celda = Renglon7.Cells.Add("PARTIDA");
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon7.Cells.Add("DESCRIPCION");
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon7.Cells.Add("PRESUPUESTO APROBADO " + Anio);
                    Celda.StyleID = "HeaderStyle2";
                    //para el AMPLIACION
                    Celda = Renglon7.Cells.Add("AMPLIACION");
                    Celda.StyleID = "HeaderStyle2";
                    //para el REDUCCION
                    Celda = Renglon7.Cells.Add("REDUCCION");
                    Celda.StyleID = "HeaderStyle2";
                    //para el MODIFICADO
                    Celda = Renglon7.Cells.Add("MODIFICADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DEVENGADO
                    Celda = Renglon7.Cells.Add("DEVENGADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el EJERCIDO
                    Celda = Renglon7.Cells.Add("EJERCIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el PAGADO
                    Celda = Renglon7.Cells.Add("PAGADO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO
                    Celda = Renglon7.Cells.Add("PRE-COMPROMETIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el COMPROMETIDO
                    Celda = Renglon7.Cells.Add("COMPROMETIDO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el DISPONIBLE
                    Celda = Renglon7.Cells.Add("DISPONIBLE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ENERO
                    Celda = Renglon7.Cells.Add("ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE FEBRERO
                    Celda = Renglon7.Cells.Add("FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MARZO
                    Celda = Renglon7.Cells.Add("MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ABRIL
                    Celda = Renglon7.Cells.Add("ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MAYO
                    Celda = Renglon7.Cells.Add("MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JUNIO
                    Celda = Renglon7.Cells.Add("JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JULIO
                    Celda = Renglon7.Cells.Add("JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE AGOSTO
                    Celda = Renglon7.Cells.Add("AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE SEPTIEMBRE
                    Celda = Renglon7.Cells.Add("SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE OCTUBRE
                    Celda = Renglon7.Cells.Add("OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE NOVIEMBRE
                    Celda = Renglon7.Cells.Add("NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE DICIEMBRE
                    Celda = Renglon7.Cells.Add("DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;
                    Dt_Partida = Negocio.Consultar_Datos_Analitico_PSP();

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon7 = Hoja7.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon7 = Hoja7.Table.Rows.Add();
                            }
                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Clasificacion_Adm, DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 29; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 29; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();

            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + Ex.Message.ToString() + "]";
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_MOD
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 30/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Generar_Reporte_MOD(String UR, String FF, String PP, String P, String Tipo, String Dependencia, String Gpo_Dependencia)
        {
            Cls_Rpt_Presupuesto_Egresos_Negocio Negocio = new Cls_Rpt_Presupuesto_Egresos_Negocio();//instancia con capa de negocios
            DataTable Dt_Dependencia = new DataTable();
            DataTable Dt_UR = new DataTable();
            DataTable Dt_Programa = new DataTable();
            DataTable Dt_FF = new DataTable();
            DataTable Dt_Partida = new DataTable();
            DataTable Dt_Partida_Incremento = new DataTable();
            DataTable Dt_Partida_Traspaso = new DataTable();
            DataTable Dt_Importes = new DataTable();
            DataTable Dt_Analitico = new DataTable();
            DataTable Dt_Modificaciones = new DataTable();
            String Anio = Cmb_Anio.SelectedItem.Value.Trim();
            Double Cantidad = 0.00;
            Int32 No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Tipo.ToString()) ? "0" : Tipo.ToString());
            int i = 0;
            String Clasificacion_Adm = String.Empty;

            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";
            try
            {
                Clasificacion_Adm = Negocio.Consultar_Clasificacion_Administrativa();

                //OBENTEMOS LOS DATOS DEL PRESUPUESTO
                Negocio.P_Anio = Anio;
                Negocio.P_Dependencia_ID = UR;
                Negocio.P_Fte_Financiamiento_ID = FF;
                Negocio.P_Partida_ID = P;
                Negocio.P_Programa_ID = PP;
                Negocio.P_Tipo_Reporte = Tipo;
                Negocio.P_Tipo_Operacion = "'TRASPASO', 'REDUCCION','AMPLIACION'";
                //Negocio.P_Tipo_Operacion = "'TRASPASO', 'REDUCCION'";
                Dt_Partida_Traspaso = Negocio.Consultar_Datos_Analitico_MOD();
                Negocio.P_Tipo_Operacion = "'AMPLIACION'";
                Dt_Partida_Incremento = Negocio.Consultar_Datos_Analitico_MOD();
                Dt_Importes = Negocio.Consultar_Importes_Datos_Analitico_MOD();
                Dt_Modificaciones = Negocio.Consultar_Modificaciones_Analitico_MOD();

                Dt_Analitico = Unificar_Dt_Analitico(Dt_Partida_Traspaso, Dt_Partida_Incremento, Dt_Importes, Dt_Modificaciones);

                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                #region Estilos
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1 = Libro.Styles.Add("HeaderStyle1");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera1_2 = Libro.Styles.Add("Estilo_Cabecera1_2");
                //Creamos el estilo cabecera 2 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                //Creamos el estilo cabecera 3 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto = Libro.Styles.Add("Presupuesto");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Neg = Libro.Styles.Add("Presupuesto_Neg");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Presupuesto_Total");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total_Neg = Libro.Styles.Add("Presupuesto_Total_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto = Libro.Styles.Add("Concepto");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto_Total = Libro.Styles.Add("Concepto_Total");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Partida_Total = Libro.Styles.Add("Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Neg = Libro.Styles.Add("Total_Neg");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Pro = Libro.Styles.Add("Total_Pro");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Pro_Neg = Libro.Styles.Add("Total_Pro_Neg");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Gpo = Libro.Styles.Add("Total_Gpo");
                //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Gpo_Neg = Libro.Styles.Add("Total_Gpo_Neg");

                //estilo para la cabecera
                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 12;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "blue";
                Estilo_Cabecera.Interior.Color = "white";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1.Font.FontName = "Tahoma";
                Estilo_Cabecera1.Font.Size = 12;
                Estilo_Cabecera1.Font.Bold = true;
                Estilo_Cabecera1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1.Font.Color = "#000000";
                Estilo_Cabecera1.Interior.Color = "white";
                Estilo_Cabecera1.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera
                Estilo_Cabecera1_2.Font.FontName = "Tahoma";
                Estilo_Cabecera1_2.Font.Size = 12;
                Estilo_Cabecera1_2.Font.Bold = true;
                Estilo_Cabecera1_2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera1_2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera1_2.Font.Color = "#000000";
                Estilo_Cabecera1_2.Interior.Color = "white";
                Estilo_Cabecera1_2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera1_2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera2
                Estilo_Cabecera2.Font.FontName = "Tahoma";
                Estilo_Cabecera2.Font.Size = 10;
                Estilo_Cabecera2.Font.Bold = true;
                Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera2.Font.Color = "#FFFFFF";
                Estilo_Cabecera2.Interior.Color = "DarkGray";
                Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera3
                Estilo_Cabecera3.Font.FontName = "Tahoma";
                Estilo_Cabecera3.Font.Size = 10;
                Estilo_Cabecera3.Font.Bold = true;
                Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera3.Font.Color = "#000000";
                Estilo_Cabecera3.Interior.Color = "white";
                Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el contenido
                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto_Total.Font.FontName = "Tahoma";
                Estilo_Concepto_Total.Font.Size = 9;
                Estilo_Concepto_Total.Font.Bold = false;
                Estilo_Concepto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Concepto_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto_Total.Font.Color = "#000000";
                Estilo_Concepto_Total.Interior.Color = "#66FFCC";
                Estilo_Concepto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el Concepto
                Estilo_Concepto.Font.FontName = "Tahoma";
                Estilo_Concepto.Font.Size = 9;
                Estilo_Concepto.Font.Bold = false;
                Estilo_Concepto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Concepto.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Concepto.Font.Color = "#000000";
                Estilo_Concepto.Interior.Color = "White";
                Estilo_Concepto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Concepto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Concepto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto.Font.FontName = "Tahoma";
                Estilo_Presupuesto.Font.Size = 9;
                Estilo_Presupuesto.Font.Bold = false;
                Estilo_Presupuesto.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto.Font.Color = "#000000";
                Estilo_Presupuesto.Interior.Color = "White";
                Estilo_Presupuesto.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (importe)
                Estilo_Presupuesto_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Neg.Font.Size = 9;
                Estilo_Presupuesto_Neg.Font.Bold = false;
                Estilo_Presupuesto_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Presupuesto_Neg.Font.Color = "red";
                Estilo_Presupuesto_Neg.Interior.Color = "White";
                Estilo_Presupuesto_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total.Font.Size = 9;
                Estilo_Presupuesto_Total.Font.Bold = true;
                Estilo_Presupuesto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total.Font.Color = "#000000";
                Estilo_Presupuesto_Total.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Presupuesto_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Presupuesto_Total_Neg.Font.Size = 9;
                Estilo_Presupuesto_Total_Neg.Font.Bold = true;
                Estilo_Presupuesto_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Presupuesto_Total_Neg.Font.Color = "red";
                Estilo_Presupuesto_Total_Neg.Interior.Color = "#66FFCC";
                Estilo_Presupuesto_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Presupuesto_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //para partida totales de todos los capitulos
                Estilo_Partida_Total.Font.FontName = "Tahoma";
                Estilo_Partida_Total.Font.Size = 9;
                Estilo_Partida_Total.Font.Bold = true;
                Estilo_Partida_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Partida_Total.Font.Color = "#000000";
                Estilo_Partida_Total.Interior.Color = "LightGreen";
                Estilo_Partida_Total.NumberFormat = "#,###,##0.00";
                Estilo_Partida_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Partida_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Partida_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total.Font.FontName = "Tahoma";
                Estilo_Total.Font.Size = 9;
                Estilo_Total.Font.Bold = true;
                Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total.Font.Color = "#000000";
                Estilo_Total.Interior.Color = "#99CCCC";
                Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total.NumberFormat = "#,###,##0.00";
                Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el presupuesto (Total del importe)
                Estilo_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Total_Neg.Font.Size = 9;
                Estilo_Total_Neg.Font.Bold = true;
                Estilo_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Neg.Font.Color = "red";
                Estilo_Total_Neg.Interior.Color = "#99CCCC";
                Estilo_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el TOTAL DEL PROGRAMA
                Estilo_Total_Pro.Font.FontName = "Tahoma";
                Estilo_Total_Pro.Font.Size = 9;
                Estilo_Total_Pro.Font.Bold = true;
                Estilo_Total_Pro.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Pro.Font.Color = "#000000";
                Estilo_Total_Pro.Interior.Color = "#FFCC66";
                Estilo_Total_Pro.NumberFormat = "#,###,##0.00";
                Estilo_Total_Pro.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Pro.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para EL TOTAL DEL PROGRAMA
                Estilo_Total_Pro_Neg.Font.FontName = "Tahoma";
                Estilo_Total_Pro_Neg.Font.Size = 9;
                Estilo_Total_Pro_Neg.Font.Bold = true;
                Estilo_Total_Pro_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Pro_Neg.Font.Color = "red";
                Estilo_Total_Pro_Neg.Interior.Color = "#FFCC66";
                Estilo_Total_Pro_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Total_Pro_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Pro_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Pro_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el TOTAL DEL grupo dependencia
                Estilo_Total_Gpo.Font.FontName = "Tahoma";
                Estilo_Total_Gpo.Font.Size = 9;
                Estilo_Total_Gpo.Font.Bold = true;
                Estilo_Total_Gpo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Gpo.Font.Color = "#000000";
                Estilo_Total_Gpo.Interior.Color = "#006666";
                Estilo_Total_Gpo.NumberFormat = "#,###,##0.00";
                Estilo_Total_Gpo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Gpo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para EL TOTAL DEL grupo dependencia
                Estilo_Total_Gpo_Neg.Font.FontName = "Tahoma";
                Estilo_Total_Gpo_Neg.Font.Size = 9;
                Estilo_Total_Gpo_Neg.Font.Bold = true;
                Estilo_Total_Gpo_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Gpo_Neg.Font.Color = "red";
                Estilo_Total_Gpo_Neg.Interior.Color = "#006666";
                Estilo_Total_Gpo_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Total_Gpo_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Gpo_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Gpo_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region Pestaña Dependencia
                if (Chk_Dependencia.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("RAMO ADMINISTRATIVO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//aprobado.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificacion.
                        }
                    }
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//ampliacion.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//reduccion.
                    //Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//incremento.
                    Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificado.

                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 8 + No_Mod -1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon = Hoja.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add(Tipo + "a MODIFICACION AL PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("RAMO DEL CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A.", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RAMO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO APROBADO " + Anio.Trim() , "HeaderStyle2"));
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio, "HeaderStyle2"));
                        }
                    }
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AUMENTO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCION", "HeaderStyle2"));
                    //Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle2"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio, "HeaderStyle2"));

                    /*pasos a realizar: se buscan los grupos de dependencia,
                    se continua buscando las dependencias a las que pertenecen al grupo de dependendcia,
                    las dependencias se les busca si tienen algun presupuesto y se le suma hasta tener 
                    el presupuesto de egresos del grupo de dependencias al que pertenecen las dependencias buscadas*/

                    Dt_Dependencia = Agrupar_Ramo_Administrativo(Dt_Analitico, Clasificacion_Adm);

                    if (Dt_Dependencia != null && Dt_Dependencia.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Dependencia.Rows)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Dependencia.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("CA")
                                            || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA") 
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("CA")
                                            || Columna.ColumnName.Equals("AF"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña UR
                if (Chk_Unidad_Responsable.Checked == true)
                {
                    //Creamos una hoja que tendrá el libro.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("GERENCIA");

                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2 = Hoja2.Table.Rows.Add();

                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//aprobado.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificacion.
                        }
                    }
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//ampliacion.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//reduccion.
                    //Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//incremento.
                    Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificado.

                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon2 = Hoja2.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add(Tipo + "a MODIFICACION PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("CLASIFICADOR ADMINISTRATIVO");
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A.", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("P.Y", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO APROBADO " + Anio.Trim(), "HeaderStyle2"));
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio, "HeaderStyle2"));
                        }
                    }
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AUMENTO", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCION", "HeaderStyle2"));
                    //Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle2"));
                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio, "HeaderStyle2"));

                    Dt_UR = Agrupar_Clasificador_Administrativo(Dt_Analitico, Clasificacion_Adm);

                    if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_UR.Rows)
                        {
                            Renglon2 = Hoja2.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_UR.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF")
                                            || Columna.ColumnName.Equals("CA") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("GRUPO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF")
                                            || Columna.ColumnName.Equals("CA") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total_Gpo"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("DEPENDENCIA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF")
                                            || Columna.ColumnName.Equals("CA") || Columna.ColumnName.Equals("PP"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon2.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 9 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Programa
                if (Chk_Programa.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("PROGRAMA O PROYECTO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3 = Hoja3.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//aprobado.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificacion.
                        }
                    }
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//ampliacion.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//reduccion.
                    //Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//incremento.
                    Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificado.

                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon3 = Hoja3.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add(Tipo + "a MODIFICACION PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("PROGRAMA O PROYECTO");
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G.", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PROGRAMA O PROYECTO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO APROBADO " + Anio.Trim(), "HeaderStyle2"));
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio, "HeaderStyle2"));
                        }
                    }
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AUMENTO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCION", "HeaderStyle2"));
                    //Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle2"));
                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio, "HeaderStyle2"));

                    Dt_Programa = Agrupar_Programa(Dt_Analitico, Clasificacion_Adm);

                    if (Dt_Programa != null && Dt_Programa.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Programa.Rows)
                        {
                            Renglon3 = Hoja3.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Programa.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("PROGRAMA")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("AF")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon3.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 8 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña FF
                if (Chk_Fte_Financiamiento.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja4 = Libro.Worksheets.Add("CLASIFICADOR POR FUENTE");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon4 = Hoja4.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//aprobado.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificacion.
                        }
                    }
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//ampliacion.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//reduccion.
                    //Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//incremento.
                    Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificado.

                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon4 = Hoja4.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add(Tipo + "a MODIFICACION PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO");
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO APROBADO " + Anio.Trim(), "HeaderStyle2"));
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio, "HeaderStyle2"));
                        }
                    }
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AUMENTO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCION", "HeaderStyle2"));
                    //englon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle2"));
                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio, "HeaderStyle2"));

                    Dt_FF = Agrupar_Fuente_Financiamiento(Dt_Analitico);

                    if (Dt_FF != null && Dt_FF.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_FF.Rows)
                        {
                            Renglon4 = Hoja4.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_FF.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("FUENTE_FINANCIAMIENTO"))
                                        {
                                            Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon4.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon4 = Hoja4.Table.Rows.Add();
                    Celda = Renglon4.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Partida
                if (Chk_Partida.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja5 = Libro.Worksheets.Add("COG");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon5 = Hoja5.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//Dependencia.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//aprobado.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificacion.
                        }
                    }
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//ampliacion.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//reduccion.
                    //Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//incremento.
                    Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//modificado.

                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon5 = Hoja5.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add(Tipo + "a MODIFICACION PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("CLASIFICADOR POR OBJETO DEL GASTO");
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CODIGO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA ESPECIFICA", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO APROBADO " + Anio.Trim(), "HeaderStyle2"));
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio, "HeaderStyle2"));
                        }
                    }
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AUMENTO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DISMINUCION", "HeaderStyle2"));
                    //Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INCREMENTO", "HeaderStyle2"));
                    Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio, "HeaderStyle2"));

                    Dt_Partida = Agrupar_COG(Dt_Analitico);

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon5 = Hoja5.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon5 = Hoja5.Table.Rows.Add();
                            }

                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("CODIGO") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "INCREMENTO")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon5.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon5 = Hoja5.Table.Rows.Add();
                    Celda = Renglon5.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 5 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico
                if (Chk_Analitico.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja6 = Libro.Worksheets.Add("ANALÍTICO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon6 = Hoja6.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 1
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//AREA FUNCIONAL 2.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//Programa 3.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Unidad Responsable 4.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//Partida 5.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto 6.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//modificacion.
                        }
                    }
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.
                    //Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.
                    Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Presupuesto 7.

                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon6 = Hoja6.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CLASIFICACIÓN ANALÍTICA");
                    Celda.MergeAcross = 10+ No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.MergeAcross = 5; // Merge 4 cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon6.Cells.Add("CONCEPTO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("PRESUPUESTO APROBADO " + Anio.Trim());
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";

                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS " + i.ToString() + "a. MODIFICACION " + Anio);
                            Celda.MergeDown = 1; // Merge two cells together
                            Celda.StyleID = "HeaderStyle2";
                        }
                    }

                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("AUMENTO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("DISMINUCION");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";
                    ////para el presupuesto
                    //Celda = Renglon6.Cells.Add("INCREMENTO");
                    //Celda.MergeDown = 1; // Merge two cells together
                    //Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon6.Cells.Add("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio);
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle2";

                    //(uso del encabezado2) para los titulos del detalle o cuerpo principal 
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.F.", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.F.G", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("C.A", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("P.P", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("GERENCIA", "HeaderStyle2"));
                    Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA", "HeaderStyle2"));

                    Dt_Partida = Agrupar_Analitico_Detallado(Dt_Analitico, Clasificacion_Adm);

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon6 = Hoja6.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon6 = Hoja6.Table.Rows.Add();
                            }
                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION")
                                            || Columna.ColumnName.Equals("MODIFICADO") || Columna.ColumnName.Contains("MODIFICACION")) 
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO")
                                            || Columna.ColumnName.Contains("MODIFICACION"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Renglon6 = Hoja6.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL_PROGRAMA"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total_Pro"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO")
                                            || Columna.ColumnName.Contains("MODIFICACION"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Pro"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Pro_Neg"));
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Renglon6 = Hoja6.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL_GPO_DEP"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total_Gpo"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO")
                                            || Columna.ColumnName.Contains("MODIFICACION"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo_Neg"));
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO"))
                                        {
                                            Renglon6 = Hoja6.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("ENCABEZADO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "HeaderStyle3"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO")
                                            || Columna.ColumnName.Contains("MODIFICACION"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "HeaderStyle3"));
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else if (Columna.ColumnName.Equals("PRESUPUESTO_APROBADO") || Columna.ColumnName.Equals("AMPLIACION")
                                            || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO")
                                            || Columna.ColumnName.Contains("MODIFICACION"))
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Cantidad >= 0)
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                            }
                                            else
                                            {
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon6 = Hoja6.Table.Rows.Add();
                    Celda = Renglon6.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 10 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                #region Pestaña Analitico Detallado
                if (Chk_Analitico_Detallado.Checked == true)
                {
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja7 = Libro.Worksheets.Add("ANALÍTICO DETALLADO");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon7 = Hoja7.Table.Rows.Add();
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(165));//CODIGO PROGRAMATICO
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//fuente de financiamiento 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(65));
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Programa .
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//Unidad Responsable.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//Partida.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//modificacion.
                        }
                    }
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACION.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCION.
                    //Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//INCREMENTO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ENERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//FEBRERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MARZO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABRIL.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MAYO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JUNIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JULIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AGOSTO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SEPTIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//OCTUBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//NOVIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DICIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.

                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Gpo_Dependencia.Trim());
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle1";
                    //se llena el encabezado principal
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add(Dependencia.Trim());
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "Estilo_Cabecera1_2";
                    //encabezado3
                    Renglon7 = Hoja7.Table.Rows.Add();//espacio entre el encabezado y el contenido
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CLASIFICACIÓN ANALÍTICA DETALLADA");
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 6 cells together
                    Celda.StyleID = "HeaderStyle3";
                    //para el codigo programatico
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("CODIGO PROGRAMATICO");
                    Celda.StyleID = "HeaderStyle2";
                    //para la ff
                    Celda = Renglon7.Cells.Add("C.F.F");
                    Celda.StyleID = "HeaderStyle2";
                    //para la AREA FUNCIONAL
                    Celda = Renglon7.Cells.Add("C.F.G");
                    Celda.StyleID = "HeaderStyle2";
                    //para la UNIDAD RESPONSABLE
                    Celda = Renglon7.Cells.Add("C.A");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PROGRAMA
                    Celda = Renglon7.Cells.Add("P.P");
                    Celda.StyleID = "HeaderStyle2";
                    //para la UNIDAD RESPONSABLE
                    Celda = Renglon7.Cells.Add("GERENCIA");
                    Celda.StyleID = "HeaderStyle2";
                    //para la PARTIDA
                    Celda = Renglon7.Cells.Add("PARTIDA");
                    Celda.StyleID = "HeaderStyle2";
                    //para concepto
                    Celda = Renglon7.Cells.Add("DESCRIPCION");
                    Celda.StyleID = "HeaderStyle2";
                    //para el presupuesto
                    Celda = Renglon7.Cells.Add("PRESUPUESTO APROBADO " + Anio);
                    Celda.StyleID = "HeaderStyle2";

                    if (No_Mod > 1)
                    {
                        for (i = 1; i < No_Mod; i++)
                        {
                            //PARA LA MODIFICACION
                            Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS " + i.ToString().Trim() + "a. MODIFICACION " + Anio);
                            Celda.StyleID = "HeaderStyle2";
                        }
                    }

                    //para el AMPLIACION
                    Celda = Renglon7.Cells.Add("AMPLIACION");
                    Celda.StyleID = "HeaderStyle2";
                    //para el REDUCCION
                    Celda = Renglon7.Cells.Add("REDUCCION");
                    Celda.StyleID = "HeaderStyle2";
                    ////para el REDUCCION
                    //Celda = Renglon7.Cells.Add("INCREMENTO");
                    //Celda.StyleID = "HeaderStyle2";
                    //para el MODIFICADO
                    Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio);
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ENERO
                    Celda = Renglon7.Cells.Add("ENERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE FEBRERO
                    Celda = Renglon7.Cells.Add("FEBRERO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MARZO
                    Celda = Renglon7.Cells.Add("MARZO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE ABRIL
                    Celda = Renglon7.Cells.Add("ABRIL");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE MAYO
                    Celda = Renglon7.Cells.Add("MAYO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JUNIO
                    Celda = Renglon7.Cells.Add("JUNIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE JULIO
                    Celda = Renglon7.Cells.Add("JULIO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE AGOSTO
                    Celda = Renglon7.Cells.Add("AGOSTO");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE SEPTIEMBRE
                    Celda = Renglon7.Cells.Add("SEPTIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE OCTUBRE
                    Celda = Renglon7.Cells.Add("OCTUBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE NOVIEMBRE
                    Celda = Renglon7.Cells.Add("NOVIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el IMPORTE DE DICIEMBRE
                    Celda = Renglon7.Cells.Add("DICIEMBRE");
                    Celda.StyleID = "HeaderStyle2";
                    //para el MODIFICADO
                    Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS " + Tipo + "a. MODIFICACION " + Anio);
                    Celda.StyleID = "HeaderStyle2";

                    Dt_Partida = Agrupar_Analitico_Detallado(Dt_Analitico, Clasificacion_Adm);
                    //Dt_Partida = Obtener_Dt_Analitico(Dt_Partida_Traspaso, Dt_Partida_Incremento);

                    if (Dt_Partida != null && Dt_Partida.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partida.Rows)
                        {
                            Renglon7 = Hoja7.Table.Rows.Add();
                            if (Dr["TIPO"].ToString().Equals("TOTAL_PSP"))
                            {
                                Renglon7 = Hoja7.Table.Rows.Add();
                            }
                            foreach (DataColumn Columna in Dt_Partida.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION"
                                                && Columna.ColumnName != "Contador" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                }
                                            }
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto_Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION"
                                                && Columna.ColumnName != "Contador" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Total_Neg"));
                                                }
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO_TOTAL"))
                                        {
                                            Renglon7 = Hoja7.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL_PROGRAMA"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total_Pro"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION"
                                                && Columna.ColumnName != "Contador" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Pro"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Pro_Neg"));
                                                }
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO_TOTAL"))
                                        {
                                            Renglon7 = Hoja7.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("TOTAL_GPO_DEP"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total_Gpo"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION"
                                                && Columna.ColumnName != "Contador" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Gpo_Neg"));
                                                }
                                            }
                                        }
                                        if (Columna.ColumnName.Equals("MODIFICADO_TOTAL"))
                                        {
                                            Renglon7 = Hoja7.Table.Rows.Add();
                                        }
                                    }
                                    else if (Dr["TIPO"].ToString().Equals("ENCABEZADO"))
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "HeaderStyle3"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION"
                                                && Columna.ColumnName != "Contador" && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Empty, DataType.String, "HeaderStyle3"));
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("CONCEPTO")
                                            || Columna.ColumnName.Equals("FF") || Columna.ColumnName.Equals("SF")
                                            || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("UR")
                                            || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO") || Columna.ColumnName.Equals("CA"))
                                        {
                                            Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ESPECIFICACION" && Columna.ColumnName != "Contador"
                                                && Columna.ColumnName != "INCREMENTO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                                }
                                                else
                                                {
                                                    Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                    Renglon7 = Hoja7.Table.Rows.Add();
                    Celda = Renglon7.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                    Celda.MergeAcross = 24 + No_Mod - 1; // Merge 3 cells together
                    Celda.StyleID = "Concepto";
                }
                #endregion

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + Ex.Message.ToString() + "]";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Dt_Analitico
        ///DESCRIPCIÓN          : metodo para agrupar los datos de las dependencias
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 19/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Obtener_Dt_UR(DataTable Dt)
        {
            DataTable Dt_Datos = new DataTable();
            Double Mod = 0.00;
            Double Tot_Mod = 0.00;
            String Ur = String.Empty;
            String Nom_Ur = String.Empty;
            DataRow Fila;

            try
            {
                if (Dt != null && Dt.Rows.Count > 0)
                {
                    Dt_Datos.Columns.Add("CODIGO", System.Type.GetType("System.String"));
                    Dt_Datos.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                    Dt_Datos.Columns.Add("MODIFICADO", System.Type.GetType("System.Double"));
                    Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));

                    Ur = Dt.Rows[0]["UR"].ToString().Trim();
                    Nom_Ur = Dt.Rows[0]["NOMBRE_UR"].ToString().Trim();

                    foreach (DataRow Dr in Dt.Rows)
                    {
                        Tot_Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);

                        if (Ur.Trim().Equals(Dr["UR"].ToString().Trim()))
                        {
                            //SUMAMOS EL TOTAL DE LA UR
                            Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);
                        }
                        else
                        {
                            //INSERTAMOS LA FILA DEL TOTAL DEL UR
                            Fila = Dt_Datos.NewRow();
                            Fila["CODIGO"] = Ur.Trim();
                            Fila["DEPENDENCIA"] = Nom_Ur.Trim();
                            Fila["MODIFICADO"] = Mod;
                            Fila["TIPO"] = "CONCEPTO";
                            Dt_Datos.Rows.Add(Fila);
                            Mod = 0.00;

                            //SUMAMOS EL TOTAL DE LA UR
                            Mod += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"]);

                            //GUARDAMOS LOS NUEVOS DATOS DEL UR Y EL PROGRAMA
                            Ur = Dr["UR"].ToString().Trim();
                            Nom_Ur = Dr["NOMBRE_UR"].ToString().Trim();
                        }
                    }

                    Fila = Dt_Datos.NewRow();
                    Fila["CODIGO"] = Ur.Trim();
                    Fila["DEPENDENCIA"] = Nom_Ur.Trim();
                    Fila["MODIFICADO"] = Mod;
                    Fila["TIPO"] = "CONCEPTO";
                    Dt_Datos.Rows.Add(Fila);
                    Mod = 0.00;

                    Fila = Dt_Datos.NewRow();
                    Fila["CODIGO"] = String.Empty;
                    Fila["DEPENDENCIA"] = "TOTAL_PRESUPUESTO";
                    Fila["MODIFICADO"] = Tot_Mod;
                    Fila["TIPO"] = "TOTAL";
                    Dt_Datos.Rows.Add(Fila);
                    Mod = 0.00;
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
            }
            return Dt_Datos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Archivo
        ///DESCRIPCIÓN: Abre el archivo de excel
        ///PARAMETROS: String Ruta_Archivo.- Es la ruta donde se encuentra el archivo
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  06/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Abrir_Archivo(String Ruta_Archivo)
        {
            try
            {
                Response.Clear();
                Response.ClearContent();
                Response.ClearHeaders();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;

                Response.End();
            }
            catch (Exception Ex)
            {

                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                //throw new Exception("Error al generar el reporte de las Dependencias. Error: [" + Ex.Message + "]");
            }
        }
        #endregion

        #region(Metodos Excel)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Unificar_Dt_Analitico
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Unificar_Dt_Analitico(DataTable Dt, DataTable Dt_Inc, DataTable Dt_Importes, DataTable Dt_Mod)
            {
                DataTable Dt_Analitico = new DataTable();
                DataTable Dt_Incrementos = new DataTable();
                //Double Incremento = 0.00;
                Int32 i;
                Double Aprobado = 0.00;
                Double Ampliacion = 0.00;
                Double Reduccion = 0.00;
                Double Modificado = 0.00;
                Int32 No_Mod = 0;

                try
                {
                    Dt_Analitico = Dt;
                    Dt_Incrementos = Dt_Inc;

                    if (Dt_Analitico != null && Dt_Analitico.Rows.Count > 0 && Dt_Incrementos != null && Dt_Incrementos.Rows.Count > 0)
                    {
                        //unificamos los datos de las modificaciones
                        Dt_Analitico = Unificar_Dt_Analitico_Modificaciones(Dt_Analitico, Dt_Mod);

                        No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());
                        
                        //INTRODUCIMOS UN CONSECUTIVO AL DT ANALITICO PARA IDENTIFICARLO
                        Dt_Analitico.Columns.Add("Contador", System.Type.GetType("System.Int32"));
                        Dt_Analitico.Columns.Add("INCREMENTO", System.Type.GetType("System.Double"));
                        i = -1;

                        foreach (DataRow Dr in Dt_Analitico.Rows)
                        {
                            i++;
                            Dr["Contador"] = i;
                            Dr["INCREMENTO"] = 0.00;

                            if (No_Mod > 1)
                            {
                                Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr[(No_Mod-1).ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[(No_Mod-1).ToString().Trim() + "a. MODIFICACION"]);
                            }
                            else 
                            {
                                Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                            }

                            Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                            Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                            Modificado = Aprobado + Ampliacion - Reduccion;
                            
                            Dr["MODIFICADO_TOTAL"] = Modificado;
                        }

                        //Dt_Inc = new DataTable();
                        //Dt_Inc = (from Fila_Inc in Dt_Incrementos.AsEnumerable()
                        //          where Fila_Inc.Field<decimal>("AMPLIACION") > 0
                        //          select Fila_Inc).AsDataView().ToTable();

                        //foreach (DataRow Dr in Dt_Inc.Rows)
                        //{
                        //    Incremento = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);

                        //    Dt = new DataTable();
                        //    if (Dr["TIPO"].ToString().Trim().Equals("CONCEPTO"))
                        //    {
                        //        Dt = (from Fila_Analitico in Dt_Analitico.AsEnumerable()
                        //              where Fila_Analitico.Field<string>("CODIGO_PROGRAMATICO") == Dr["CODIGO_PROGRAMATICO"].ToString().Trim()
                        //              select Fila_Analitico).AsDataView().ToTable();
                        //    }

                        //    if (Dt != null)
                        //    {
                        //        if (Dt.Rows.Count > 0)
                        //        {
                        //            Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dt.Rows[0]["MODIFICADO_TOTAL"]);
                        //            Modificado = Modificado + Incremento;
                        //            i = Convert.ToInt32(Dt.Rows[0]["Contador"]);
                        //            Dt_Analitico.Rows[i]["INCREMENTO"] = Incremento;
                        //            Dt_Analitico.Rows[i]["MODIFICADO_TOTAL"] = Modificado;
                        //        }
                        //    }
                        //}

                        //unificamos los datos de los importes del presupuesto
                        Dt_Analitico = Unificar_Importes_Dt_Analitico(Dt_Analitico, Dt_Importes);
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Analitico;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Unificar_Importes_Dt_Analitico
            ///DESCRIPCIÓN          : metodo para unificar los datos de los importes de los meses
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Unificar_Importes_Dt_Analitico(DataTable Dt_Analitico, DataTable Dt_Importes)
            {
                DataTable Dt = new DataTable();
                Int32 i;
                Double Ene = 0.00;
                Double Feb = 0.00;
                Double Mar = 0.00;
                Double Abr = 0.00;
                Double May = 0.00;
                Double Jun = 0.00;
                Double Jul = 0.00;
                Double Ago = 0.00;
                Double Sep = 0.00;
                Double Oct = 0.00;
                Double Nov = 0.00;
                Double Dic = 0.00;
                Double Imp_Ene = 0.00;
                Double Imp_Feb = 0.00;
                Double Imp_Mar = 0.00;
                Double Imp_Abr = 0.00;
                Double Imp_May = 0.00;
                Double Imp_Jun = 0.00;
                Double Imp_Jul = 0.00;
                Double Imp_Ago = 0.00;
                Double Imp_Sep = 0.00;
                Double Imp_Oct = 0.00;
                Double Imp_Nov = 0.00;
                Double Imp_Dic = 0.00;
                Double Tot_Ene = 0.00;
                Double Tot_Feb = 0.00;
                Double Tot_Mar = 0.00;
                Double Tot_Abr = 0.00;
                Double Tot_May = 0.00;
                Double Tot_Jun = 0.00;
                Double Tot_Jul = 0.00;
                Double Tot_Ago = 0.00;
                Double Tot_Sep = 0.00;
                Double Tot_Oct = 0.00;
                Double Tot_Nov = 0.00;
                Double Tot_Dic = 0.00;
                try
                {
                     if (Dt_Analitico != null && Dt_Importes != null)
                     {
                          if (Dt_Analitico.Rows.Count > 0 && Dt_Importes.Rows.Count > 0)
                          {
                              i = -1;

                              foreach (DataRow Dr in Dt_Importes.Rows)
                              {
                                  Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString().Trim()) ? "0" : Dr["ENERO"]);
                                  Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString().Trim()) ? "0" : Dr["FEBRERO"]);
                                  Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString().Trim()) ? "0" : Dr["MARZO"]);
                                  Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString().Trim()) ? "0" : Dr["ABRIL"]);
                                  May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString().Trim()) ? "0" : Dr["MAYO"]);
                                  Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString().Trim()) ? "0" : Dr["JUNIO"]);
                                  Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString().Trim()) ? "0" : Dr["JULIO"]);
                                  Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString().Trim()) ? "0" : Dr["AGOSTO"]);
                                  Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["SEPTIEMBRE"]);
                                  Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString().Trim()) ? "0" : Dr["OCTUBRE"]);
                                  Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["NOVIEMBRE"]);
                                  Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString().Trim()) ? "0" : Dr["DICIEMBRE"]);

                                  if (Dr["TIPO"].ToString().Trim().Equals("REDUCCION"))
                                  {
                                      Ene = -Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString().Trim()) ? "0" : Dr["ENERO"]);
                                      Feb = -Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString().Trim()) ? "0" : Dr["FEBRERO"]);
                                      Mar = -Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString().Trim()) ? "0" : Dr["MARZO"]);
                                      Abr = -Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString().Trim()) ? "0" : Dr["ABRIL"]);
                                      May = -Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString().Trim()) ? "0" : Dr["MAYO"]);
                                      Jun = -Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString().Trim()) ? "0" : Dr["JUNIO"]);
                                      Jul = -Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString().Trim()) ? "0" : Dr["JULIO"]);
                                      Ago = -Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString().Trim()) ? "0" : Dr["AGOSTO"]);
                                      Sep = -Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["SEPTIEMBRE"]);
                                      Oct = -Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString().Trim()) ? "0" : Dr["OCTUBRE"]);
                                      Nov = -Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["NOVIEMBRE"]);
                                      Dic = -Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString().Trim()) ? "0" : Dr["DICIEMBRE"]);
                                  }

                                  Dt = new DataTable();
                                  Dt = (from Fila_Analitico in Dt_Analitico.AsEnumerable()
                                        where Fila_Analitico.Field<string>("CODIGO_PROGRAMATICO") == Dr["CODIGO_PROGRAMATICO"].ToString().Trim()
                                        select Fila_Analitico).AsDataView().ToTable();

                                  if (Dt != null)
                                  {
                                      if (Dt.Rows.Count > 0)
                                      {
                                          Imp_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["ENERO"].ToString().Trim()) ? "0" : Dt.Rows[0]["ENERO"]);
                                          Imp_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["FEBRERO"].ToString().Trim()) ? "0" : Dt.Rows[0]["FEBRERO"]);
                                          Imp_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["MARZO"].ToString().Trim()) ? "0" : Dt.Rows[0]["MARZO"]);
                                          Imp_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["ABRIL"].ToString().Trim()) ? "0" : Dt.Rows[0]["ABRIL"]);
                                          Imp_May = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["MAYO"].ToString().Trim()) ? "0" : Dt.Rows[0]["MAYO"]);
                                          Imp_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["JUNIO"].ToString().Trim()) ? "0" : Dt.Rows[0]["JUNIO"]);
                                          Imp_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["JULIO"].ToString().Trim()) ? "0" : Dt.Rows[0]["JULIO"]);
                                          Imp_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["AGOSTO"].ToString().Trim()) ? "0" : Dt.Rows[0]["AGOSTO"]);
                                          Imp_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dt.Rows[0]["SEPTIEMBRE"]);
                                          Imp_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["OCTUBRE"].ToString().Trim()) ? "0" : Dt.Rows[0]["OCTUBRE"]);
                                          Imp_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["NOVIEMBRE"].ToString().Trim()) ? "0" : Dt.Rows[0]["NOVIEMBRE"]);
                                          Imp_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dt.Rows[0]["DICIEMBRE"].ToString().Trim()) ? "0" : Dt.Rows[0]["DICIEMBRE"]);

                                          Tot_Ene = Imp_Ene + Ene;
                                          Tot_Feb = Imp_Feb + Feb;
                                          Tot_Mar = Imp_Mar + Mar;
                                          Tot_Abr = Imp_Abr + Abr;
                                          Tot_May = Imp_May + May;
                                          Tot_Jun = Imp_Jun + Jun;
                                          Tot_Jul = Imp_Jul + Jul;
                                          Tot_Ago = Imp_Ago + Ago;
                                          Tot_Sep = Imp_Sep + Sep;
                                          Tot_Oct = Imp_Oct + Oct;
                                          Tot_Nov = Imp_Nov + Nov;
                                          Tot_Dic = Imp_Dic + Dic;

                                          i = Convert.ToInt32(Dt.Rows[0]["Contador"]);
                                          Dt_Analitico.Rows[i]["ENERO"] = Tot_Ene;
                                          Dt_Analitico.Rows[i]["FEBRERO"] = Tot_Feb;
                                          Dt_Analitico.Rows[i]["MARZO"] = Tot_Mar;
                                          Dt_Analitico.Rows[i]["ABRIL"] = Tot_Abr;
                                          Dt_Analitico.Rows[i]["MAYO"] = Tot_May;
                                          Dt_Analitico.Rows[i]["JUNIO"] = Tot_Jun;
                                          Dt_Analitico.Rows[i]["JULIO"] = Tot_Jul;
                                          Dt_Analitico.Rows[i]["AGOSTO"] = Tot_Ago;
                                          Dt_Analitico.Rows[i]["SEPTIEMBRE"] = Tot_Sep;
                                          Dt_Analitico.Rows[i]["OCTUBRE"] = Tot_Oct;
                                          Dt_Analitico.Rows[i]["NOVIEMBRE"] = Tot_Nov;
                                          Dt_Analitico.Rows[i]["DICIEMBRE"] = Tot_Dic;
                                      }
                                  }
                              }
                          }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Analitico;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Ramo_Administrativo
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos por ramo
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Ramo_Administrativo(DataTable Dt_Psp, String Clasificacion_Adm)
            {
                DataTable Dt_Agrupado = new DataTable();
                DataTable Dt = new DataTable();
                DataRow Fila_Dt_Gpo = null;
                Double Tot_Aprobado = 0.00;
                Double Tot_Ampliacion = 0.00;
                Double Tot_Reduccion = 0.00;
                Double Tot_Incremento = 0.00;
                Double Tot_Modificado = 0.00;
                Int32 No_Mod = 0;
                int i = 0;
                int No_Columnas = 0;
                DataTable Dt_Mod = new DataTable();
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;
                int Contador = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            //verificamos si existen modificaciones
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            //declaramos el datatable donde almacenaremos los datos
                            Dt.Columns.Add("FF", typeof(String));
                            Dt.Columns.Add("AF", typeof(String));
                            Dt.Columns.Add("CA", typeof(String));
                            Dt.Columns.Add("CODIGO", typeof(String));
                            Dt.Columns.Add("DEPENDENCIA", typeof(String));
                            Dt.Columns.Add("TIPO", typeof(String));
                            Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            Dt.Columns.Add("AMPLIACION", typeof(Double));
                            Dt.Columns.Add("REDUCCION", typeof(Double));
                            Dt.Columns.Add("INCREMENTO", typeof(Double));
                            Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }

                            //obtenemos los grupos dependencia y aparte las dependencias de direccion general para desglosar por coordinacion
                            foreach (DataRow Dr_Gpo in Dt_Psp.Rows)
                            {
                                if (Dr_Gpo["NOMBRE_GPO_DEP"].ToString().Trim().Equals("DIRECCION GENERAL"))
                                {
                                    Fila_Dt_Gpo = Dt.NewRow();
                                    Fila_Dt_Gpo["FF"] = Dr_Gpo["FF"].ToString();
                                    Fila_Dt_Gpo["AF"] = Dr_Gpo["SF"].ToString();
                                    Fila_Dt_Gpo["CA"] = Clasificacion_Adm;
                                    Fila_Dt_Gpo["CODIGO"] = Dr_Gpo["UR"].ToString();
                                    Fila_Dt_Gpo["DEPENDENCIA"] = Dr_Gpo["NOMBRE_UR"].ToString();
                                    Fila_Dt_Gpo["TIPO"] = "CONCEPTO";
                                    Fila_Dt_Gpo["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_Gpo["PRESUPUESTO_APROBADO"]);
                                    Fila_Dt_Gpo["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["AMPLIACION"].ToString().Trim()) ? "0" : Dr_Gpo["AMPLIACION"]);
                                    Fila_Dt_Gpo["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["REDUCCION"].ToString().Trim()) ? "0" : Dr_Gpo["REDUCCION"]);
                                    Fila_Dt_Gpo["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["INCREMENTO"].ToString().Trim()) ? "0" : Dr_Gpo["INCREMENTO"]);
                                    Fila_Dt_Gpo["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_Gpo["MODIFICADO_TOTAL"]);
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila_Dt_Gpo[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"]);

                                        }
                                    }
                                    Dt.Rows.Add(Fila_Dt_Gpo);
                                }
                                else 
                                {
                                    Fila_Dt_Gpo = Dt.NewRow();
                                    Fila_Dt_Gpo["FF"] = Dr_Gpo["FF"].ToString();
                                    Fila_Dt_Gpo["AF"] = Dr_Gpo["SF"].ToString();
                                    Fila_Dt_Gpo["CA"] = Clasificacion_Adm;
                                    Fila_Dt_Gpo["CODIGO"] = Dr_Gpo["GPO_DEP"].ToString();
                                    Fila_Dt_Gpo["DEPENDENCIA"] = Dr_Gpo["NOMBRE_GPO_DEP"].ToString();
                                    Fila_Dt_Gpo["TIPO"] = "CONCEPTO";
                                    Fila_Dt_Gpo["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_Gpo["PRESUPUESTO_APROBADO"]);
                                    Fila_Dt_Gpo["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["AMPLIACION"].ToString().Trim()) ? "0" : Dr_Gpo["AMPLIACION"]);
                                    Fila_Dt_Gpo["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["REDUCCION"].ToString().Trim()) ? "0" : Dr_Gpo["REDUCCION"]);
                                    Fila_Dt_Gpo["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["INCREMENTO"].ToString().Trim()) ? "0" : Dr_Gpo["INCREMENTO"]);
                                    Fila_Dt_Gpo["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_Gpo["MODIFICADO_TOTAL"]);
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila_Dt_Gpo[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"]);

                                        }
                                    }
                                    Dt.Rows.Add(Fila_Dt_Gpo);
                                }
                            }

                            if (Dt != null)
                            { 
                                if(Dt.Rows.Count > 0)
                                {
                                    //sumamos los monstos para gruparlos por grupo dependencia
                                    var Agrupacion = (from Fila_Gpo_Dep in Dt.AsEnumerable()
                                                      group Fila_Gpo_Dep by new
                                                      {
                                                          FF = Fila_Gpo_Dep.Field<String>("FF"),
                                                          AF = Fila_Gpo_Dep.Field<String>("AF"),
                                                          CA = Fila_Gpo_Dep.Field<String>("CA"),
                                                          Gpo_Dep = Fila_Gpo_Dep.Field<String>("CODIGO"),
                                                          Nombre_Gpo_Dep = Fila_Gpo_Dep.Field<String>("DEPENDENCIA"),
                                                          Tipo = Fila_Gpo_Dep.Field<String>("TIPO")
                                                      }
                                                      into psp
                                                      orderby psp.Key.Gpo_Dep
                                                      select Dt.LoadDataRow(
                                                        new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.AF,
                                                            psp.Key.CA,
                                                            psp.Key.Gpo_Dep,
                                                            psp.Key.Nombre_Gpo_Dep,
                                                            psp.Key.Tipo,
                                                            psp.Sum(g => g.Field<Double>("PRESUPUESTO_APROBADO")),
                                                            psp.Sum(g => g.Field<Double>("AMPLIACION")),
                                                            psp.Sum(g => g.Field<Double>("REDUCCION")),
                                                            psp.Sum(g => g.Field<Double>("INCREMENTO")),
                                                            psp.Sum(g => g.Field<Double>("MODIFICADO_TOTAL")),
                                                        }, LoadOption.PreserveChanges));

                                    //obtenemos los datos agrupados en una tabla
                                    Dt_Agrupado = Agrupacion.CopyToDataTable();

                                    if (Dt_Agrupado != null)
                                    {
                                        if (Dt_Agrupado.Rows.Count > 0)
                                        {
                                            if (No_Mod > 1)
                                            {
                                                No_Columnas = Dt_Agrupado.Columns.Count;
                                                for (i = No_Mod - 1; i >= 1; i--)
                                                {
                                                    Contador++;
                                                    //sumamos los monstos para gruparlos por grupo dependencia de las modificaciones
                                                    var Agrupa_Mod = (from Fila_Gpo_Dep in Dt.AsEnumerable()
                                                                      group Fila_Gpo_Dep by new
                                                                      {
                                                                          FF = Fila_Gpo_Dep.Field<String>("FF"),
                                                                          AF = Fila_Gpo_Dep.Field<String>("AF"),
                                                                          CA = Fila_Gpo_Dep.Field<String>("CA"),
                                                                          Gpo_Dep = Fila_Gpo_Dep.Field<String>("CODIGO"),
                                                                          Nombre_Gpo_Dep = Fila_Gpo_Dep.Field<String>("DEPENDENCIA"),
                                                                          Tipo = Fila_Gpo_Dep.Field<String>("TIPO")
                                                                      }
                                                                          into psp
                                                                          orderby psp.Key.Gpo_Dep
                                                                          select Dt.LoadDataRow(
                                                                            new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.AF,
                                                            psp.Key.CA,
                                                            psp.Key.Gpo_Dep,
                                                            psp.Key.Nombre_Gpo_Dep,
                                                            psp.Key.Tipo,
                                                            psp.Sum(g => Convert.ToDouble(String.IsNullOrEmpty(g.ItemArray[No_Columnas - i].ToString().Trim()) ? "0" : g.ItemArray[No_Columnas - i].ToString().Trim())),
                                                        }, LoadOption.PreserveChanges));

                                                    Dt_Mod = Agrupa_Mod.CopyToDataTable();

                                                    if (Dt_Mod != null)
                                                    {
                                                        if (Dt_Mod.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                            {
                                                                Dt_Modif = new DataTable();
                                                                Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                                            where Fila.Field<String>("CODIGO").Trim() == Dr["CODIGO"].ToString().Trim()
                                                                            select Fila).AsDataView().ToTable();

                                                                if (Dt_Modif != null)
                                                                {
                                                                    if (Dt_Modif.Rows.Count > 0)
                                                                    {
                                                                        Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"]);
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //obtenemos la suma del total
                                            Tot_Aprobado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("PRESUPUESTO_APROBADO"));
                                            Tot_Ampliacion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("AMPLIACION"));
                                            Tot_Reduccion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("REDUCCION"));
                                            Tot_Incremento = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                                            Tot_Modificado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("MODIFICADO_TOTAL"));

                                            //agregamos el total al datatable
                                            Fila_Dt_Gpo = Dt_Agrupado.NewRow();
                                            Fila_Dt_Gpo["FF"] = String.Empty;
                                            Fila_Dt_Gpo["AF"] = String.Empty;
                                            Fila_Dt_Gpo["CA"] = String.Empty;
                                            Fila_Dt_Gpo["CODIGO"] = String.Empty;
                                            Fila_Dt_Gpo["DEPENDENCIA"] = "TOTAL PRESUPUESTO ";
                                            Fila_Dt_Gpo["TIPO"] = "TOTAL";
                                            Fila_Dt_Gpo["PRESUPUESTO_APROBADO"] = Tot_Aprobado;
                                            Fila_Dt_Gpo["AMPLIACION"] = Tot_Ampliacion;
                                            Fila_Dt_Gpo["REDUCCION"] = Tot_Reduccion;
                                            Fila_Dt_Gpo["INCREMENTO"] = Tot_Incremento;
                                            Fila_Dt_Gpo["MODIFICADO_TOTAL"] = Tot_Modificado;

                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_Gpo[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")); ;
                                                }
                                            }

                                            Dt_Agrupado.Rows.Add(Fila_Dt_Gpo);

                                            if (No_Mod > 1)
                                            {
                                                Dt = new DataTable();
                                                //acomodamos el datatable
                                                Dt.Columns.Add("FF", typeof(String));
                                                Dt.Columns.Add("AF", typeof(String));
                                                Dt.Columns.Add("CA", typeof(String));
                                                Dt.Columns.Add("CODIGO", typeof(String));
                                                Dt.Columns.Add("DEPENDENCIA", typeof(String));
                                                Dt.Columns.Add("TIPO", typeof(String));
                                                Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                                                if (No_Mod > 1)
                                                {
                                                    for (i = 1; i < No_Mod; i++)
                                                    {
                                                        Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                                    }
                                                }
                                                Dt.Columns.Add("AMPLIACION", typeof(Double));
                                                Dt.Columns.Add("REDUCCION", typeof(Double));
                                                Dt.Columns.Add("INCREMENTO", typeof(Double));
                                                Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));

                                                foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                {
                                                    Fila_Dt_Gpo = Dt.NewRow();
                                                    Fila_Dt_Gpo["FF"] = Dr["FF"].ToString();
                                                    Fila_Dt_Gpo["AF"] = Dr["AF"].ToString();
                                                    Fila_Dt_Gpo["CA"] = Clasificacion_Adm;
                                                    Fila_Dt_Gpo["CODIGO"] = Dr["CODIGO"].ToString();
                                                    Fila_Dt_Gpo["DEPENDENCIA"] = Dr["DEPENDENCIA"].ToString();
                                                    Fila_Dt_Gpo["TIPO"] = Dr["TIPO"].ToString();
                                                    Fila_Dt_Gpo["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Gpo[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);

                                                        }
                                                    }
                                                    Fila_Dt_Gpo["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                                    Fila_Dt_Gpo["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                                    Fila_Dt_Gpo["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                                    Fila_Dt_Gpo["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                                    
                                                    Dt.Rows.Add(Fila_Dt_Gpo);
                                                }

                                                Dt_Agrupado = Dt;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Agrupado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Clasificador_Administrativo
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos por UR
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Clasificador_Administrativo(DataTable Dt_Psp, String Clasificacion_Adm)
            {
                DataTable Dt_Agrupado = new DataTable();
                DataTable Dt = new DataTable();
                DataTable Dt_Dep_Agrupado = new DataTable();
                DataRow Fila_Dt_Dep = null;
                Double Tot_Aprobado = 0.00;
                Double Tot_Ampliacion = 0.00;
                Double Tot_Reduccion = 0.00;
                Double Tot_Incremento = 0.00;
                Double Tot_Modificado = 0.00;

                //para la agrupacion por gpo dep
                Double Tot_Aprobado_Gpo = 0.00;
                Double Tot_Ampliacion_Gpo = 0.00;
                Double Tot_Reduccion_Gpo = 0.00;
                Double Tot_Incremento_Gpo = 0.00;
                Double Tot_Modificado_Gpo = 0.00;
                String Gpo = String.Empty;
                String Nom_Gpo = String.Empty;
                
                //para las modificaciones
                Int32 No_Mod = 0;
                int i = 0;
                int No_Columnas = 0;
                DataTable Dt_Mod = new DataTable();
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;
                int Contador = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());
                            //declaramos el datatable donde almacenaremos los datos
                            Dt.Columns.Add("FF", typeof(String));
                            Dt.Columns.Add("AF", typeof(String));
                            Dt.Columns.Add("CA", typeof(String));
                            Dt.Columns.Add("PP", typeof(String));
                            Dt.Columns.Add("CODIGO", typeof(String));
                            Dt.Columns.Add("DEPENDENCIA", typeof(String));
                            Dt.Columns.Add("TIPO", typeof(String));
                            Dt.Columns.Add("GPO_DEP", typeof(String));
                            Dt.Columns.Add("NOMBRE_GPO_DEP", typeof(String));
                            Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            Dt.Columns.Add("AMPLIACION", typeof(Double));
                            Dt.Columns.Add("REDUCCION", typeof(Double));
                            Dt.Columns.Add("INCREMENTO", typeof(Double));
                            Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }

                            //declaramos el datatable donde almacenaremos los datos agrupados de las unidades responsables
                            Dt_Dep_Agrupado.Columns.Add("FF", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("AF", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("CA", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("PP", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("CODIGO", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("DEPENDENCIA", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("TIPO", typeof(String));
                            Dt_Dep_Agrupado.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt_Dep_Agrupado.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }
                            Dt_Dep_Agrupado.Columns.Add("AMPLIACION", typeof(Double));
                            Dt_Dep_Agrupado.Columns.Add("REDUCCION", typeof(Double));
                            Dt_Dep_Agrupado.Columns.Add("INCREMENTO", typeof(Double));
                            Dt_Dep_Agrupado.Columns.Add("MODIFICADO_TOTAL", typeof(Double));

                            //obtenemos los grupos dependencia y aparte las dependencias de direccion general para desglosar por coordinacion
                            foreach (DataRow Dr_Gpo in Dt_Psp.Rows)
                            {
                                Fila_Dt_Dep = Dt.NewRow();
                                Fila_Dt_Dep["FF"] = Dr_Gpo["FF"].ToString();
                                Fila_Dt_Dep["AF"] = Dr_Gpo["SF"].ToString();
                                Fila_Dt_Dep["CA"] = Clasificacion_Adm;
                                Fila_Dt_Dep["PP"] = Dr_Gpo["PROGRAMA"].ToString();
                                Fila_Dt_Dep["CODIGO"] = Dr_Gpo["UR"].ToString();
                                Fila_Dt_Dep["DEPENDENCIA"] = Dr_Gpo["NOMBRE_UR"].ToString();
                                Fila_Dt_Dep["TIPO"] = "CONCEPTO";
                                Fila_Dt_Dep["GPO_DEP"] = Dr_Gpo["GPO_DEP"].ToString();
                                Fila_Dt_Dep["NOMBRE_GPO_DEP"] = Dr_Gpo["NOMBRE_GPO_DEP"].ToString();
                                Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_Gpo["PRESUPUESTO_APROBADO"]);
                                Fila_Dt_Dep["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["AMPLIACION"].ToString().Trim()) ? "0" : Dr_Gpo["AMPLIACION"]);
                                Fila_Dt_Dep["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["REDUCCION"].ToString().Trim()) ? "0" : Dr_Gpo["REDUCCION"]);
                                Fila_Dt_Dep["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["INCREMENTO"].ToString().Trim()) ? "0" : Dr_Gpo["INCREMENTO"]);
                                Fila_Dt_Dep["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_Gpo["MODIFICADO_TOTAL"]);
                                if (No_Mod > 1)
                                {
                                    for (i = 1; i < No_Mod; i++)
                                    {
                                        Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"]);

                                    }
                                }
                                Dt.Rows.Add(Fila_Dt_Dep);
                            }

                            if (Dt != null)
                            {
                                if (Dt.Rows.Count > 0)
                                {
                                    //sumamos los montos para agruparlos por grupo dependencia
                                    var Agrupacion = (from Fila_Gpo_Dep in Dt.AsEnumerable()
                                                      group Fila_Gpo_Dep by new
                                                      {
                                                          FF = Fila_Gpo_Dep.Field<String>("FF"),
                                                          AF = Fila_Gpo_Dep.Field<String>("AF"),
                                                          CA = Fila_Gpo_Dep.Field<String>("CA"),
                                                          PP = Fila_Gpo_Dep.Field<String>("PP"),
                                                          Dep = Fila_Gpo_Dep.Field<String>("CODIGO"),
                                                          Nombre_Dep = Fila_Gpo_Dep.Field<String>("DEPENDENCIA"),
                                                          Tipo = Fila_Gpo_Dep.Field<String>("TIPO"),
                                                          Gpo_Dep = Fila_Gpo_Dep.Field<String>("GPO_DEP"),
                                                          Nombre_Gpo_Dep = Fila_Gpo_Dep.Field<String>("NOMBRE_GPO_DEP")
                                                      }
                                                      into psp
                                                      orderby psp.Key.Gpo_Dep, psp.Key.Dep
                                                      select Dt.LoadDataRow(
                                                        new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.AF,
                                                            psp.Key.CA,
                                                            psp.Key.PP,
                                                            psp.Key.Dep,
                                                            psp.Key.Nombre_Dep,
                                                            psp.Key.Tipo,
                                                            psp.Key.Gpo_Dep,
                                                            psp.Key.Nombre_Gpo_Dep,
                                                            psp.Sum(g => g.Field<Double>("PRESUPUESTO_APROBADO")),
                                                            psp.Sum(g => g.Field<Double>("AMPLIACION")),
                                                            psp.Sum(g => g.Field<Double>("REDUCCION")),
                                                            psp.Sum(g => g.Field<Double>("INCREMENTO")),
                                                            psp.Sum(g => g.Field<Double>("MODIFICADO_TOTAL")),
                                                        }, LoadOption.PreserveChanges));

                                    //obtenemos los datos agrupados en una tabla
                                    Dt_Agrupado = Agrupacion.CopyToDataTable();

                                    if (Dt_Agrupado != null)
                                    {
                                        if (Dt_Agrupado.Rows.Count > 0)
                                        {
                                            if (No_Mod > 1)
                                            {
                                                No_Columnas = Dt_Agrupado.Columns.Count;
                                                for (i = No_Mod - 1; i >= 1; i--)
                                                {
                                                    Contador++;
                                                    //sumamos los monstos para gruparlos por grupo dependencia de las modificaciones
                                                    var Agrupa_Mod = (from Fila_Gpo_Dep in Dt.AsEnumerable()
                                                                      group Fila_Gpo_Dep by new
                                                                      {
                                                                          FF = Fila_Gpo_Dep.Field<String>("FF"),
                                                                          AF = Fila_Gpo_Dep.Field<String>("AF"),
                                                                          CA = Fila_Gpo_Dep.Field<String>("CA"),
                                                                          PP = Fila_Gpo_Dep.Field<String>("PP"),
                                                                          Dep = Fila_Gpo_Dep.Field<String>("CODIGO"),
                                                                          Nombre_Dep = Fila_Gpo_Dep.Field<String>("DEPENDENCIA"),
                                                                          Tipo = Fila_Gpo_Dep.Field<String>("TIPO"),
                                                                          Gpo_Dep = Fila_Gpo_Dep.Field<String>("GPO_DEP"),
                                                                          Nombre_Gpo_Dep = Fila_Gpo_Dep.Field<String>("NOMBRE_GPO_DEP")
                                                                      }
                                                                          into psp
                                                                          orderby psp.Key.Gpo_Dep, psp.Key.Dep
                                                                          select Dt.LoadDataRow(
                                                                            new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.AF,
                                                            psp.Key.CA,
                                                            psp.Key.PP,
                                                            psp.Key.Dep,
                                                            psp.Key.Nombre_Dep,
                                                            psp.Key.Tipo,
                                                            psp.Key.Gpo_Dep,
                                                            psp.Key.Nombre_Gpo_Dep,
                                                            psp.Sum(g => Convert.ToDouble(String.IsNullOrEmpty(g.ItemArray[No_Columnas - i].ToString().Trim()) ? "0" : g.ItemArray[No_Columnas - i].ToString().Trim())),
                                                        }, LoadOption.PreserveChanges));

                                                    Dt_Mod = Agrupa_Mod.CopyToDataTable();

                                                    if (Dt_Mod != null)
                                                    {
                                                        if (Dt_Mod.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                            {
                                                                Dt_Modif = new DataTable();
                                                                Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                                            where Fila.Field<String>("CODIGO").Trim() == Dr["CODIGO"].ToString().Trim()
                                                                            select Fila).AsDataView().ToTable();

                                                                if (Dt_Modif != null)
                                                                {
                                                                    if (Dt_Modif.Rows.Count > 0)
                                                                    {
                                                                        Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"]);
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //obtenemos la suma del total
                                            Tot_Aprobado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("PRESUPUESTO_APROBADO"));
                                            Tot_Ampliacion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("AMPLIACION"));
                                            Tot_Reduccion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("REDUCCION"));
                                            Tot_Incremento = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                                            Tot_Modificado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("MODIFICADO_TOTAL"));

                                            //obtenemos los grupos dependencias para agruparlos tambien por este tipo
                                            Gpo = Dt_Agrupado.Rows[0]["GPO_DEP"].ToString().Trim();
                                            Nom_Gpo = Dt_Agrupado.Rows[0]["NOMBRE_GPO_DEP"].ToString().Trim();

                                            foreach (DataRow Dr_Dep in Dt_Agrupado.Rows)
                                            {
                                                if (Dr_Dep["GPO_DEP"].ToString().Trim().Equals(Gpo.Trim()))
                                                {
                                                    //agregamos la dependencia al  al datatable
                                                    Fila_Dt_Dep = Dt_Dep_Agrupado.NewRow();
                                                    Fila_Dt_Dep["FF"] = Dr_Dep["FF"].ToString().Trim();
                                                    Fila_Dt_Dep["AF"] = Dr_Dep["AF"].ToString().Trim();
                                                    Fila_Dt_Dep["CA"] = Clasificacion_Adm;
                                                    Fila_Dt_Dep["PP"] = Dr_Dep["PP"].ToString().Trim();
                                                    Fila_Dt_Dep["CODIGO"] = Dr_Dep["CODIGO"].ToString().Trim();
                                                    Fila_Dt_Dep["DEPENDENCIA"] = Dr_Dep["DEPENDENCIA"].ToString().Trim();
                                                    Fila_Dt_Dep["TIPO"] = Dr_Dep["TIPO"].ToString().Trim();
                                                    Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Dr_Dep["PRESUPUESTO_APROBADO"];
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = Dr_Dep[i.ToString().Trim() + "a. MODIFICACION"];
                                                        }
                                                    }
                                                    Fila_Dt_Dep["AMPLIACION"] = Dr_Dep["AMPLIACION"];
                                                    Fila_Dt_Dep["REDUCCION"] = Dr_Dep["REDUCCION"];
                                                    Fila_Dt_Dep["INCREMENTO"] = Dr_Dep["INCREMENTO"];
                                                    Fila_Dt_Dep["MODIFICADO_TOTAL"] = Dr_Dep["MODIFICADO_TOTAL"];
                                                    Dt_Dep_Agrupado.Rows.Add(Fila_Dt_Dep);
                                                }
                                                else 
                                                {
                                                    //obtenemos el total del gpo dep
                                                    Tot_Aprobado_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable() 
                                                                    where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                    select Fila_Sum_Gpo.Field<Double>("PRESUPUESTO_APROBADO")).Sum();

                                                    Tot_Ampliacion_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                        where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                        select Fila_Sum_Gpo.Field<Double>("AMPLIACION")).Sum();

                                                    Tot_Reduccion_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                        where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                        select Fila_Sum_Gpo.Field<Double>("REDUCCION")).Sum();

                                                    Tot_Incremento_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                        where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                        select Fila_Sum_Gpo.Field<Double>("INCREMENTO")).Sum();

                                                    Tot_Modificado_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                        where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                        select Fila_Sum_Gpo.Field<Double>("MODIFICADO_TOTAL")).Sum();

                                                    //agregamos el total al datatable
                                                    Fila_Dt_Dep = Dt_Dep_Agrupado.NewRow();
                                                    Fila_Dt_Dep["FF"] = String.Empty;
                                                    Fila_Dt_Dep["AF"] = String.Empty;
                                                    Fila_Dt_Dep["CA"] = String.Empty;
                                                    Fila_Dt_Dep["PP"] = String.Empty;
                                                    Fila_Dt_Dep["CODIGO"] = Gpo.Trim();
                                                    Fila_Dt_Dep["DEPENDENCIA"] = Nom_Gpo.Trim();
                                                    Fila_Dt_Dep["TIPO"] = "GRUPO";
                                                    Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Tot_Aprobado_Gpo;

                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                                                                    where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                                                                    select Fila_Sum_Gpo.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                        }
                                                    }
                                                    Fila_Dt_Dep["AMPLIACION"] = Tot_Ampliacion_Gpo;
                                                    Fila_Dt_Dep["REDUCCION"] = Tot_Reduccion_Gpo;
                                                    Fila_Dt_Dep["INCREMENTO"] = Tot_Incremento_Gpo;
                                                    Fila_Dt_Dep["MODIFICADO_TOTAL"] = Tot_Modificado_Gpo;
                                                    Dt_Dep_Agrupado.Rows.Add(Fila_Dt_Dep);

                                                    //cambiamos los parametros y limpiamos variables
                                                    Gpo = Dr_Dep["GPO_DEP"].ToString().Trim();
                                                    Nom_Gpo = Dr_Dep["NOMBRE_GPO_DEP"].ToString().Trim();
                                                    Tot_Aprobado_Gpo = 0.00;
                                                    Tot_Ampliacion_Gpo = 0.00;
                                                    Tot_Reduccion_Gpo = 0.00;
                                                    Tot_Incremento_Gpo = 0.00;
                                                    Tot_Modificado_Gpo = 0.00;

                                                    //agregamos la dependencia al  al datatable
                                                    Fila_Dt_Dep = Dt_Dep_Agrupado.NewRow();
                                                    Fila_Dt_Dep["FF"] = Dr_Dep["FF"].ToString().Trim();
                                                    Fila_Dt_Dep["AF"] = Dr_Dep["AF"].ToString().Trim();
                                                    Fila_Dt_Dep["CA"] = Clasificacion_Adm;
                                                    Fila_Dt_Dep["PP"] = Dr_Dep["PP"].ToString().Trim();
                                                    Fila_Dt_Dep["CODIGO"] = Dr_Dep["CODIGO"].ToString().Trim();
                                                    Fila_Dt_Dep["DEPENDENCIA"] = Dr_Dep["DEPENDENCIA"].ToString().Trim();
                                                    Fila_Dt_Dep["TIPO"] = Dr_Dep["TIPO"].ToString().Trim();
                                                    Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Dr_Dep["PRESUPUESTO_APROBADO"];
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = Dr_Dep[i.ToString().Trim() + "a. MODIFICACION"];
                                                        }
                                                    }
                                                    Fila_Dt_Dep["AMPLIACION"] = Dr_Dep["AMPLIACION"];
                                                    Fila_Dt_Dep["REDUCCION"] = Dr_Dep["REDUCCION"];
                                                    Fila_Dt_Dep["INCREMENTO"] = Dr_Dep["INCREMENTO"];
                                                    Fila_Dt_Dep["MODIFICADO_TOTAL"] = Dr_Dep["MODIFICADO_TOTAL"];
                                                    Dt_Dep_Agrupado.Rows.Add(Fila_Dt_Dep);
                                                }
                                            }

                                            //obtenemos el total del gpo dep final
                                            Tot_Aprobado_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                select Fila_Sum_Gpo.Field<Double>("PRESUPUESTO_APROBADO")).Sum();

                                            Tot_Ampliacion_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                  where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("AMPLIACION")).Sum();

                                            Tot_Reduccion_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                 where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                 select Fila_Sum_Gpo.Field<Double>("REDUCCION")).Sum();

                                            Tot_Incremento_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                  where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("INCREMENTO")).Sum();

                                            Tot_Modificado_Gpo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                  where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("MODIFICADO_TOTAL")).Sum();

                                            //agregamos el total al datatable
                                            Fila_Dt_Dep = Dt_Dep_Agrupado.NewRow();
                                            Fila_Dt_Dep["FF"] = String.Empty;
                                            Fila_Dt_Dep["AF"] = String.Empty;
                                            Fila_Dt_Dep["CA"] = String.Empty;
                                            Fila_Dt_Dep["PP"] = String.Empty;
                                            Fila_Dt_Dep["CODIGO"] = Gpo.Trim();
                                            Fila_Dt_Dep["DEPENDENCIA"] = Nom_Gpo.Trim();
                                            Fila_Dt_Dep["TIPO"] = "GRUPO";
                                            Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Tot_Aprobado_Gpo;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                                                            where Fila_Sum_Gpo.Field<String>("GPO_DEP").Trim() == Gpo.Trim()
                                                                                                            select Fila_Sum_Gpo.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                }
                                            }
                                            Fila_Dt_Dep["AMPLIACION"] = Tot_Ampliacion_Gpo;
                                            Fila_Dt_Dep["REDUCCION"] = Tot_Reduccion_Gpo;
                                            Fila_Dt_Dep["INCREMENTO"] = Tot_Incremento_Gpo;
                                            Fila_Dt_Dep["MODIFICADO_TOTAL"] = Tot_Modificado_Gpo;
                                            Dt_Dep_Agrupado.Rows.Add(Fila_Dt_Dep);

                                            //agregamos el total al datatable
                                            Fila_Dt_Dep = Dt_Dep_Agrupado.NewRow();
                                            Fila_Dt_Dep["FF"] = String.Empty;
                                            Fila_Dt_Dep["AF"] = String.Empty;
                                            Fila_Dt_Dep["CA"] = String.Empty;
                                            Fila_Dt_Dep["PP"] = String.Empty;
                                            Fila_Dt_Dep["CODIGO"] = String.Empty;
                                            Fila_Dt_Dep["DEPENDENCIA"] = "TOTAL PRESUPUESTO ";
                                            Fila_Dt_Dep["TIPO"] = "TOTAL";
                                            Fila_Dt_Dep["PRESUPUESTO_APROBADO"] = Tot_Aprobado;

                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_Dep[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION"));
                                                }
                                            }

                                            Fila_Dt_Dep["AMPLIACION"] = Tot_Ampliacion;
                                            Fila_Dt_Dep["REDUCCION"] = Tot_Reduccion;
                                            Fila_Dt_Dep["INCREMENTO"] = Tot_Incremento;
                                            Fila_Dt_Dep["MODIFICADO_TOTAL"] = Tot_Modificado;
                                            Dt_Dep_Agrupado.Rows.Add(Fila_Dt_Dep);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Dep_Agrupado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Programa
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos por programa
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Programa(DataTable Dt_Psp, String Clasificacion_Adm)
            {
                DataTable Dt_Agrupado = new DataTable();
                DataTable Dt = new DataTable();
                DataRow Fila_Dt_PP = null;
                Double Tot_Aprobado = 0.00;
                Double Tot_Ampliacion = 0.00;
                Double Tot_Reduccion = 0.00;
                Double Tot_Incremento = 0.00;
                Double Tot_Modificado = 0.00;
                Int32 No_Mod = 0;
                int i = 0;
                int No_Columnas = 0;
                DataTable Dt_Mod = new DataTable();
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;
                int Contador = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            //verificamos si existen modificaciones
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            //declaramos el datatable donde almacenaremos los datos
                            Dt.Columns.Add("FF", typeof(String));
                            Dt.Columns.Add("AF", typeof(String));
                            Dt.Columns.Add("CA", typeof(String));
                            Dt.Columns.Add("CODIGO", typeof(String));
                            Dt.Columns.Add("PROGRAMA", typeof(String));
                            Dt.Columns.Add("TIPO", typeof(String));
                            Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            Dt.Columns.Add("AMPLIACION", typeof(Double));
                            Dt.Columns.Add("REDUCCION", typeof(Double));
                            Dt.Columns.Add("INCREMENTO", typeof(Double));
                            Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }

                            //obtenemos los programas
                            foreach (DataRow Dr_Gpo in Dt_Psp.Rows)
                            {
                                Fila_Dt_PP = Dt.NewRow();
                                Fila_Dt_PP["FF"] = Dr_Gpo["FF"].ToString();
                                Fila_Dt_PP["AF"] = Dr_Gpo["SF"].ToString();
                                Fila_Dt_PP["CA"] = Clasificacion_Adm;
                                Fila_Dt_PP["CODIGO"] = Dr_Gpo["PROGRAMA"].ToString();
                                Fila_Dt_PP["PROGRAMA"] = Dr_Gpo["NOMBRE_PROGRAMA"].ToString();
                                Fila_Dt_PP["TIPO"] = "CONCEPTO";
                                Fila_Dt_PP["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_Gpo["PRESUPUESTO_APROBADO"]);
                                Fila_Dt_PP["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["AMPLIACION"].ToString().Trim()) ? "0" : Dr_Gpo["AMPLIACION"]);
                                Fila_Dt_PP["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["REDUCCION"].ToString().Trim()) ? "0" : Dr_Gpo["REDUCCION"]);
                                Fila_Dt_PP["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["INCREMENTO"].ToString().Trim()) ? "0" : Dr_Gpo["INCREMENTO"]);
                                Fila_Dt_PP["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_Gpo["MODIFICADO_TOTAL"]);
                                if (No_Mod > 1)
                                {
                                    for (i = 1; i < No_Mod; i++)
                                    {
                                        Fila_Dt_PP[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_Gpo[i.ToString().Trim() + "a. MODIFICACION"]);
                                    }
                                }
                                Dt.Rows.Add(Fila_Dt_PP);
                            }

                            if (Dt != null)
                            {
                                if (Dt.Rows.Count > 0)
                                {
                                    //sumamos los monstos para gruparlos por grupo dependencia
                                    var Agrupacion = (from Fila_PP in Dt.AsEnumerable()
                                                      group Fila_PP by new
                                                      {
                                                          FF = Fila_PP.Field<String>("FF"),
                                                          AF = Fila_PP.Field<String>("AF"),
                                                          CA = Fila_PP.Field<String>("CA"),
                                                          PP = Fila_PP.Field<String>("CODIGO"),
                                                          Nombre_PP= Fila_PP.Field<String>("PROGRAMA"),
                                                          Tipo = Fila_PP.Field<String>("TIPO")
                                                      }
                                                          into psp
                                                          orderby psp.Key.PP
                                                          select Dt.LoadDataRow(
                                                            new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.AF,
                                                            psp.Key.CA,
                                                            psp.Key.PP,
                                                            psp.Key.Nombre_PP,
                                                            psp.Key.Tipo,
                                                            psp.Sum(g => g.Field<Double>("PRESUPUESTO_APROBADO")),
                                                            psp.Sum(g => g.Field<Double>("AMPLIACION")),
                                                            psp.Sum(g => g.Field<Double>("REDUCCION")),
                                                            psp.Sum(g => g.Field<Double>("INCREMENTO")),
                                                            psp.Sum(g => g.Field<Double>("MODIFICADO_TOTAL")),
                                                        }, LoadOption.PreserveChanges));

                                    //obtenemos los datos agrupados en una tabla
                                    Dt_Agrupado = Agrupacion.CopyToDataTable();

                                    if (Dt_Agrupado != null)
                                    {
                                        if (Dt_Agrupado.Rows.Count > 0)
                                        {
                                            if (No_Mod > 1)
                                            {
                                                No_Columnas = Dt_Agrupado.Columns.Count;
                                                for (i = No_Mod - 1; i >= 1; i--)
                                                {
                                                    Contador++;
                                                    //sumamos los monstos para gruparlos por grupo dependencia de las modificaciones
                                                    var Agrupa_Mod = (from Fila_PP in Dt.AsEnumerable()
                                                                      group Fila_PP by new
                                                                      {
                                                                          FF = Fila_PP.Field<String>("FF"),
                                                                          AF = Fila_PP.Field<String>("AF"),
                                                                          CA = Fila_PP.Field<String>("CA"),
                                                                          PP = Fila_PP.Field<String>("CODIGO"),
                                                                          Nombre_PP = Fila_PP.Field<String>("PROGRAMA"),
                                                                          Tipo = Fila_PP.Field<String>("TIPO")
                                                                      }
                                                                          into psp
                                                                          orderby psp.Key.PP
                                                                          select Dt.LoadDataRow(
                                                                            new object[]
                                                                    {
                                                                        psp.Key.FF,
                                                                        psp.Key.AF,
                                                                        psp.Key.CA,
                                                                        psp.Key.PP,
                                                                        psp.Key.Nombre_PP,
                                                                        psp.Key.Tipo,
                                                                        psp.Sum(g => Convert.ToDouble(String.IsNullOrEmpty(g.ItemArray[No_Columnas - i].ToString().Trim()) ? "0" : g.ItemArray[No_Columnas - i].ToString().Trim())),
                                                                    }, LoadOption.PreserveChanges));

                                                    Dt_Mod = Agrupa_Mod.CopyToDataTable();

                                                    if (Dt_Mod != null)
                                                    {
                                                        if (Dt_Mod.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                            {
                                                                Dt_Modif = new DataTable();
                                                                Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                                            where Fila.Field<String>("CODIGO").Trim() == Dr["CODIGO"].ToString().Trim()
                                                                            select Fila).AsDataView().ToTable();

                                                                if (Dt_Modif != null)
                                                                {
                                                                    if (Dt_Modif.Rows.Count > 0)
                                                                    {
                                                                        Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"]);
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }


                                            //obtenemos la suma del total
                                            Tot_Aprobado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("PRESUPUESTO_APROBADO"));
                                            Tot_Ampliacion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("AMPLIACION"));
                                            Tot_Reduccion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("REDUCCION"));
                                            Tot_Incremento = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                                            Tot_Modificado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("MODIFICADO_TOTAL"));

                                            //agregamos el total al datatable
                                            Fila_Dt_PP = Dt_Agrupado.NewRow();
                                            Fila_Dt_PP["FF"] = String.Empty;
                                            Fila_Dt_PP["AF"] = String.Empty;
                                            Fila_Dt_PP["CA"] = String.Empty;
                                            Fila_Dt_PP["CODIGO"] = String.Empty;
                                            Fila_Dt_PP["PROGRAMA"] = "TOTAL PRESUPUESTO ";
                                            Fila_Dt_PP["TIPO"] = "TOTAL";
                                            Fila_Dt_PP["PRESUPUESTO_APROBADO"] = Tot_Aprobado;
                                            Fila_Dt_PP["AMPLIACION"] = Tot_Ampliacion;
                                            Fila_Dt_PP["REDUCCION"] = Tot_Reduccion;
                                            Fila_Dt_PP["INCREMENTO"] = Tot_Incremento;
                                            Fila_Dt_PP["MODIFICADO_TOTAL"] = Tot_Modificado;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_PP[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")); ;
                                                }
                                            }
                                            Dt_Agrupado.Rows.Add(Fila_Dt_PP);

                                            //acomodamos el datatable
                                            if (No_Mod > 1)
                                            {
                                                Dt = new DataTable();
                                                Dt.Columns.Add("FF", typeof(String));
                                                Dt.Columns.Add("AF", typeof(String));
                                                Dt.Columns.Add("CA", typeof(String));
                                                Dt.Columns.Add("CODIGO", typeof(String));
                                                Dt.Columns.Add("PROGRAMA", typeof(String));
                                                Dt.Columns.Add("TIPO", typeof(String));
                                                Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                                                if (No_Mod > 1)
                                                {
                                                    for (i = 1; i < No_Mod; i++)
                                                    {
                                                        Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                                    }
                                                }
                                                Dt.Columns.Add("AMPLIACION", typeof(Double));
                                                Dt.Columns.Add("REDUCCION", typeof(Double));
                                                Dt.Columns.Add("INCREMENTO", typeof(Double));
                                                Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));

                                                foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                {
                                                    Fila_Dt_PP = Dt.NewRow();
                                                    Fila_Dt_PP["FF"] = Dr["FF"].ToString();
                                                    Fila_Dt_PP["AF"] = Dr["AF"].ToString();
                                                    Fila_Dt_PP["CA"] = Dr["CA"].ToString();
                                                    Fila_Dt_PP["CODIGO"] = Dr["CODIGO"].ToString();
                                                    Fila_Dt_PP["PROGRAMA"] = Dr["PROGRAMA"].ToString();
                                                    Fila_Dt_PP["TIPO"] = Dr["TIPO"].ToString();
                                                    Fila_Dt_PP["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_PP[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);

                                                        }
                                                    }
                                                    Fila_Dt_PP["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                                    Fila_Dt_PP["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                                    Fila_Dt_PP["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                                    Fila_Dt_PP["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);

                                                    Dt.Rows.Add(Fila_Dt_PP);
                                                }

                                                Dt_Agrupado = Dt;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Agrupado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Fuente_Financiamiento
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos por fuente
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Fuente_Financiamiento(DataTable Dt_Psp)
            {
                DataTable Dt_Agrupado = new DataTable();
                DataTable Dt = new DataTable();
                DataRow Fila_Dt_FF = null;
                Double Tot_Aprobado = 0.00;
                Double Tot_Ampliacion = 0.00;
                Double Tot_Reduccion = 0.00;
                Double Tot_Incremento = 0.00;
                Double Tot_Modificado = 0.00;

                Int32 No_Mod = 0;
                int i = 0;
                int No_Columnas = 0;
                DataTable Dt_Mod = new DataTable();
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;
                int Contador = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            //verificamos si existen modificaciones
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            //declaramos el datatable donde almacenaremos los datos
                            Dt.Columns.Add("CODIGO", typeof(String));
                            Dt.Columns.Add("FUENTE_FINANCIAMIENTO", typeof(String));
                            Dt.Columns.Add("TIPO", typeof(String));
                            Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            Dt.Columns.Add("AMPLIACION", typeof(Double));
                            Dt.Columns.Add("REDUCCION", typeof(Double));
                            Dt.Columns.Add("INCREMENTO", typeof(Double));
                            Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }

                            //obtenemos las fuentes de financiamiento
                            foreach (DataRow Dr_FF in Dt_Psp.Rows)
                            {
                                Fila_Dt_FF = Dt.NewRow();
                                Fila_Dt_FF["CODIGO"] = Dr_FF["FF"].ToString();
                                Fila_Dt_FF["FUENTE_FINANCIAMIENTO"] = Dr_FF["NOMBRE_FF"].ToString();
                                Fila_Dt_FF["TIPO"] = "CONCEPTO";
                                Fila_Dt_FF["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_FF["PRESUPUESTO_APROBADO"]);
                                Fila_Dt_FF["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF["AMPLIACION"].ToString().Trim()) ? "0" : Dr_FF["AMPLIACION"]);
                                Fila_Dt_FF["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF["REDUCCION"].ToString().Trim()) ? "0" : Dr_FF["REDUCCION"]);
                                Fila_Dt_FF["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF["INCREMENTO"].ToString().Trim()) ? "0" : Dr_FF["INCREMENTO"]);
                                Fila_Dt_FF["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_FF["MODIFICADO_TOTAL"]);
                                if (No_Mod > 1)
                                {
                                    for (i = 1; i < No_Mod; i++)
                                    {
                                        Fila_Dt_FF[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_FF[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_FF[i.ToString().Trim() + "a. MODIFICACION"]);
                                    }
                                }
                                Dt.Rows.Add(Fila_Dt_FF);
                            }

                            if (Dt != null)
                            {
                                if (Dt.Rows.Count > 0)
                                {
                                    //sumamos los monstos para gruparlos por grupo dependencia
                                    var Agrupacion = (from Fila_FF in Dt.AsEnumerable()
                                                      group Fila_FF by new
                                                      {
                                                          FF = Fila_FF.Field<String>("CODIGO"),
                                                          Nombre_FF = Fila_FF.Field<String>("FUENTE_FINANCIAMIENTO"),
                                                          Tipo = Fila_FF.Field<String>("TIPO")
                                                      }
                                                          into psp
                                                          orderby psp.Key.FF
                                                          select Dt.LoadDataRow(
                                                            new object[]
                                                        {
                                                            psp.Key.FF,
                                                            psp.Key.Nombre_FF,
                                                            psp.Key.Tipo,
                                                            psp.Sum(g => g.Field<Double>("PRESUPUESTO_APROBADO")),
                                                            psp.Sum(g => g.Field<Double>("AMPLIACION")),
                                                            psp.Sum(g => g.Field<Double>("REDUCCION")),
                                                            psp.Sum(g => g.Field<Double>("INCREMENTO")),
                                                            psp.Sum(g => g.Field<Double>("MODIFICADO_TOTAL")),
                                                        }, LoadOption.PreserveChanges));

                                    //obtenemos los datos agrupados en una tabla
                                    Dt_Agrupado = Agrupacion.CopyToDataTable();

                                    if (Dt_Agrupado != null)
                                    {
                                        if (Dt_Agrupado.Rows.Count > 0)
                                        {
                                            if (No_Mod > 1)
                                            {
                                                No_Columnas = Dt_Agrupado.Columns.Count;
                                                for (i = No_Mod - 1; i >= 1; i--)
                                                {
                                                    Contador++;
                                                    //sumamos los monstos para gruparlos por grupo dependencia de las modificaciones
                                                    var Agrupa_Mod = (from Fila_FF in Dt.AsEnumerable()
                                                                      group Fila_FF by new
                                                                      {
                                                                          FF = Fila_FF.Field<String>("CODIGO"),
                                                                          Nombre_FF = Fila_FF.Field<String>("FUENTE_FINANCIAMIENTO"),
                                                                          Tipo = Fila_FF.Field<String>("TIPO")
                                                                      }
                                                                          into psp
                                                                          orderby psp.Key.FF
                                                                          select Dt.LoadDataRow(
                                                                            new object[]
                                                                        {
                                                                            psp.Key.FF,
                                                                            psp.Key.Nombre_FF,
                                                                            psp.Key.Tipo,
                                                                            psp.Sum(g => Convert.ToDouble(String.IsNullOrEmpty(g.ItemArray[No_Columnas - i].ToString().Trim()) ? "0" : g.ItemArray[No_Columnas - i].ToString().Trim())),
                                                                        }, LoadOption.PreserveChanges));

                                                    Dt_Mod = Agrupa_Mod.CopyToDataTable();

                                                    if (Dt_Mod != null)
                                                    {
                                                        if (Dt_Mod.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                            {
                                                                Dt_Modif = new DataTable();
                                                                Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                                            where Fila.Field<String>("CODIGO").Trim() == Dr["CODIGO"].ToString().Trim()
                                                                            select Fila).AsDataView().ToTable();

                                                                if (Dt_Modif != null)
                                                                {
                                                                    if (Dt_Modif.Rows.Count > 0)
                                                                    {
                                                                        Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"]);
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }

                                            //obtenemos la suma del total
                                            Tot_Aprobado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("PRESUPUESTO_APROBADO"));
                                            Tot_Ampliacion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("AMPLIACION"));
                                            Tot_Reduccion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("REDUCCION"));
                                            Tot_Incremento = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                                            Tot_Modificado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("MODIFICADO_TOTAL"));

                                            //agregamos el total al datatable
                                            Fila_Dt_FF = Dt_Agrupado.NewRow();
                                            Fila_Dt_FF["CODIGO"] = String.Empty;
                                            Fila_Dt_FF["FUENTE_FINANCIAMIENTO"] = "TOTAL PRESUPUESTO ";
                                            Fila_Dt_FF["TIPO"] = "TOTAL";
                                            Fila_Dt_FF["PRESUPUESTO_APROBADO"] = Tot_Aprobado;
                                            Fila_Dt_FF["AMPLIACION"] = Tot_Ampliacion;
                                            Fila_Dt_FF["REDUCCION"] = Tot_Reduccion;
                                            Fila_Dt_FF["INCREMENTO"] = Tot_Incremento;
                                            Fila_Dt_FF["MODIFICADO_TOTAL"] = Tot_Modificado;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_FF[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")); ;
                                                }
                                            }
                                            Dt_Agrupado.Rows.Add(Fila_Dt_FF);

                                            //acomodamos el datatable
                                            if (No_Mod > 1)
                                            {
                                                Dt = new DataTable();
                                                Dt.Columns.Add("CODIGO", typeof(String));
                                                Dt.Columns.Add("FUENTE_FINANCIAMIENTO", typeof(String));
                                                Dt.Columns.Add("TIPO", typeof(String));
                                                Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                                                if (No_Mod > 1)
                                                {
                                                    for (i = 1; i < No_Mod; i++)
                                                    {
                                                        Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                                    }
                                                }
                                                Dt.Columns.Add("AMPLIACION", typeof(Double));
                                                Dt.Columns.Add("REDUCCION", typeof(Double));
                                                Dt.Columns.Add("INCREMENTO", typeof(Double));
                                                Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));

                                                foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                {
                                                    Fila_Dt_FF = Dt.NewRow();
                                                    Fila_Dt_FF["CODIGO"] = Dr["CODIGO"].ToString();
                                                    Fila_Dt_FF["FUENTE_FINANCIAMIENTO"] = Dr["FUENTE_FINANCIAMIENTO"].ToString();
                                                    Fila_Dt_FF["TIPO"] = Dr["TIPO"].ToString();
                                                    Fila_Dt_FF["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_FF[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);

                                                        }
                                                    }
                                                    Fila_Dt_FF["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                                    Fila_Dt_FF["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                                    Fila_Dt_FF["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                                    Fila_Dt_FF["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);

                                                    Dt.Rows.Add(Fila_Dt_FF);
                                                }

                                                Dt_Agrupado = Dt;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Agrupado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_COG
            ///DESCRIPCIÓN          : metodo para unificar los datos de los incrementos y los traspasos por partida
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_COG(DataTable Dt_Psp)
            {
                DataTable Dt_Agrupado = new DataTable();
                DataTable Dt = new DataTable();
                DataTable Dt_Cog_Agrupado = new DataTable();
                DataRow Fila_Dt_Cog = null;
                Double Tot_Aprobado = 0.00;
                Double Tot_Ampliacion = 0.00;
                Double Tot_Reduccion = 0.00;
                Double Tot_Incremento = 0.00;
                Double Tot_Modificado = 0.00;

                //para la agrupacion por capitulo
                Double Tot_Aprobado_Capitulo = 0.00;
                Double Tot_Ampliacion_Capitulo = 0.00;
                Double Tot_Reduccion_Capitulo = 0.00;
                Double Tot_Incremento_Capitulo = 0.00;
                Double Tot_Modificado_Capitulo = 0.00;
                String Capitulo = String.Empty;
                String Nom_Capitulo = String.Empty;

                //para las modificaciones
                Int32 No_Mod = 0;
                int i = 0;
                int No_Columnas = 0;
                DataTable Dt_Mod = new DataTable();
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;
                int Contador = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            //declaramos el datatable donde almacenaremos los datos
                            Dt.Columns.Add("PARTIDA", typeof(String));
                            Dt.Columns.Add("CONCEPTO", typeof(String));
                            Dt.Columns.Add("TIPO", typeof(String));
                            Dt.Columns.Add("CAPITULO", typeof(String));
                            Dt.Columns.Add("NOMBRE_CAPITULO", typeof(String));
                            Dt.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            Dt.Columns.Add("AMPLIACION", typeof(Double));
                            Dt.Columns.Add("REDUCCION", typeof(Double));
                            Dt.Columns.Add("INCREMENTO", typeof(Double));
                            Dt.Columns.Add("MODIFICADO_TOTAL", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }

                            //declaramos el datatable donde almacenaremos los datos agrupados de las partida y capitulo
                            Dt_Cog_Agrupado.Columns.Add("CODIGO", typeof(String));
                            Dt_Cog_Agrupado.Columns.Add("CONCEPTO", typeof(String));
                            Dt_Cog_Agrupado.Columns.Add("TIPO", typeof(String));
                            Dt_Cog_Agrupado.Columns.Add("PRESUPUESTO_APROBADO", typeof(Double));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt_Cog_Agrupado.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }
                            Dt_Cog_Agrupado.Columns.Add("AMPLIACION", typeof(Double));
                            Dt_Cog_Agrupado.Columns.Add("REDUCCION", typeof(Double));
                            Dt_Cog_Agrupado.Columns.Add("INCREMENTO", typeof(Double));
                            Dt_Cog_Agrupado.Columns.Add("MODIFICADO_TOTAL", typeof(Double));

                            //obtenemos las partidas
                            foreach (DataRow Dr_Partida in Dt_Psp.Rows)
                            {
                                Fila_Dt_Cog = Dt.NewRow();
                                Fila_Dt_Cog["PARTIDA"] = Dr_Partida["PARTIDA"].ToString();
                                Fila_Dt_Cog["CONCEPTO"] = Dr_Partida["CONCEPTO"].ToString();
                                Fila_Dt_Cog["TIPO"] = "CONCEPTO";
                                Fila_Dt_Cog["CAPITULO"] = Dr_Partida["CAPITULO"].ToString();
                                Fila_Dt_Cog["NOMBRE_CAPITULO"] = Dr_Partida["NOMBRE_CAPITULO"].ToString();
                                Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr_Partida["PRESUPUESTO_APROBADO"]);
                                Fila_Dt_Cog["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida["AMPLIACION"].ToString().Trim()) ? "0" : Dr_Partida["AMPLIACION"]);
                                Fila_Dt_Cog["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida["REDUCCION"].ToString().Trim()) ? "0" : Dr_Partida["REDUCCION"]);
                                Fila_Dt_Cog["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida["INCREMENTO"].ToString().Trim()) ? "0" : Dr_Partida["INCREMENTO"]);
                                Fila_Dt_Cog["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr_Partida["MODIFICADO_TOTAL"]);
                                if (No_Mod > 1)
                                {
                                    for (i = 1; i < No_Mod; i++)
                                    {
                                        Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Partida[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr_Partida[i.ToString().Trim() + "a. MODIFICACION"]);

                                    }
                                }
                                Dt.Rows.Add(Fila_Dt_Cog);
                            }

                            if (Dt != null)
                            {
                                if (Dt.Rows.Count > 0)
                                {
                                    //sumamos los montos para gruparlos por partida
                                    var Agrupacion = (from Fila_Cog in Dt.AsEnumerable()
                                                      group Fila_Cog by new
                                                      {
                                                          Partida = Fila_Cog.Field<String>("PARTIDA"),
                                                          Nombre_Partida = Fila_Cog.Field<String>("CONCEPTO"),
                                                          Tipo = Fila_Cog.Field<String>("TIPO"),
                                                          Cap = Fila_Cog.Field<String>("CAPITULO"),
                                                          Nombre_Capitulo = Fila_Cog.Field<String>("NOMBRE_CAPITULO")
                                                      }
                                                          into psp
                                                          orderby psp.Key.Cap, psp.Key.Partida
                                                          select Dt.LoadDataRow(
                                                            new object[]
                                                        {
                                                            psp.Key.Partida,
                                                            psp.Key.Nombre_Partida,
                                                            psp.Key.Tipo,
                                                            psp.Key.Cap,
                                                            psp.Key.Nombre_Capitulo,
                                                            psp.Sum(g => g.Field<Double>("PRESUPUESTO_APROBADO")),
                                                            psp.Sum(g => g.Field<Double>("AMPLIACION")),
                                                            psp.Sum(g => g.Field<Double>("REDUCCION")),
                                                            psp.Sum(g => g.Field<Double>("INCREMENTO")),
                                                            psp.Sum(g => g.Field<Double>("MODIFICADO_TOTAL")),
                                                        }, LoadOption.PreserveChanges));

                                    //obtenemos los datos agrupados en una tabla
                                    Dt_Agrupado = Agrupacion.CopyToDataTable();

                                    if (Dt_Agrupado != null)
                                    {
                                        if (Dt_Agrupado.Rows.Count > 0)
                                        {
                                            if (No_Mod > 1)
                                            {
                                                No_Columnas = Dt_Agrupado.Columns.Count;
                                                for (i = No_Mod - 1; i >= 1; i--)
                                                {
                                                    Contador++;
                                                    //sumamos los monstos para gruparlos por grupo dependencia de las modificaciones
                                                    var Agrupa_Mod = (from Fila_Cog in Dt.AsEnumerable()
                                                                      group Fila_Cog by new
                                                                      {
                                                                          Partida = Fila_Cog.Field<String>("PARTIDA"),
                                                                          Nombre_Partida = Fila_Cog.Field<String>("CONCEPTO"),
                                                                          Tipo = Fila_Cog.Field<String>("TIPO"),
                                                                          Cap = Fila_Cog.Field<String>("CAPITULO"),
                                                                          Nombre_Capitulo = Fila_Cog.Field<String>("NOMBRE_CAPITULO")
                                                                      }
                                                                          into psp
                                                                          orderby psp.Key.Cap, psp.Key.Partida
                                                                          select Dt.LoadDataRow(
                                                                            new object[]
                                                                    {
                                                                        psp.Key.Partida,
                                                                        psp.Key.Nombre_Partida,
                                                                        psp.Key.Tipo,
                                                                        psp.Key.Cap,
                                                                        psp.Key.Nombre_Capitulo,
                                                                        psp.Sum(g => Convert.ToDouble(String.IsNullOrEmpty(g.ItemArray[No_Columnas - i].ToString().Trim()) ? "0" : g.ItemArray[No_Columnas - i].ToString().Trim())),
                                                                    }, LoadOption.PreserveChanges)); 
                                                        
                                                    Dt_Mod = Agrupa_Mod.CopyToDataTable();

                                                    if (Dt_Mod != null)
                                                    {
                                                        if (Dt_Mod.Rows.Count > 0)
                                                        {
                                                            foreach (DataRow Dr in Dt_Agrupado.Rows)
                                                            {
                                                                Dt_Modif = new DataTable();
                                                                Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                                            where Fila.Field<String>("PARTIDA").Trim() == Dr["PARTIDA"].ToString().Trim()
                                                                            select Fila).AsDataView().ToTable();

                                                                if (Dt_Modif != null)
                                                                {
                                                                    if (Dt_Modif.Rows.Count > 0)
                                                                    {
                                                                        Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[0]["PRESUPUESTO_APROBADO"]);
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                                                    }
                                                                    else
                                                                    {
                                                                        Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                    }
                                                                }
                                                                else
                                                                {
                                                                    Dr[Contador.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }


                                            //obtenemos los capitulos para agruparlos tambien por este tipo
                                            Capitulo = Dt_Agrupado.Rows[0]["CAPITULO"].ToString().Trim();
                                            Nom_Capitulo = Dt_Agrupado.Rows[0]["NOMBRE_CAPITULO"].ToString().Trim();

                                            foreach (DataRow Dr_Cog in Dt_Agrupado.Rows)
                                            {
                                                if (Dr_Cog["CAPITULO"].ToString().Trim().Equals(Capitulo.Trim()))
                                                {
                                                    //agregamos la PARTIDA al  al datatable
                                                    Fila_Dt_Cog = Dt_Cog_Agrupado.NewRow();
                                                    Fila_Dt_Cog["CODIGO"] = Dr_Cog["PARTIDA"].ToString().Trim();
                                                    Fila_Dt_Cog["CONCEPTO"] = Dr_Cog["CONCEPTO"].ToString().Trim();
                                                    Fila_Dt_Cog["TIPO"] = Dr_Cog["TIPO"].ToString().Trim();
                                                    Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Dr_Cog["PRESUPUESTO_APROBADO"];
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = Dr_Cog[i.ToString().Trim() + "a. MODIFICACION"];
                                                        }
                                                    }
                                                    Fila_Dt_Cog["AMPLIACION"] = Dr_Cog["AMPLIACION"];
                                                    Fila_Dt_Cog["REDUCCION"] = Dr_Cog["REDUCCION"];
                                                    Fila_Dt_Cog["INCREMENTO"] = Dr_Cog["INCREMENTO"];
                                                    Fila_Dt_Cog["MODIFICADO_TOTAL"] = Dr_Cog["MODIFICADO_TOTAL"];
                                                    Dt_Cog_Agrupado.Rows.Add(Fila_Dt_Cog);
                                                }
                                                else
                                                {
                                                    //obtenemos el total del gpo dep
                                                    Tot_Aprobado_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                            where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                                select Fila_Sum_Gpo.Field<Double>("PRESUPUESTO_APROBADO")).Sum();

                                                    Tot_Ampliacion_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                               where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                                select Fila_Sum_Gpo.Field<Double>("AMPLIACION")).Sum();

                                                    Tot_Reduccion_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                              where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                               select Fila_Sum_Gpo.Field<Double>("REDUCCION")).Sum();

                                                    Tot_Incremento_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                               where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                                select Fila_Sum_Gpo.Field<Double>("INCREMENTO")).Sum();

                                                    Tot_Modificado_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                               where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                               select Fila_Sum_Gpo.Field<Double>("MODIFICADO_TOTAL")).Sum();

                                                    //agregamos el total al datatable
                                                    Fila_Dt_Cog = Dt_Cog_Agrupado.NewRow();
                                                    Fila_Dt_Cog["CODIGO"] = Capitulo.Trim();
                                                    Fila_Dt_Cog["CONCEPTO"] = Nom_Capitulo.Trim();
                                                    Fila_Dt_Cog["TIPO"] = "CAPITULO";
                                                    Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Tot_Aprobado_Capitulo;
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                                                                    where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                                                                    select Fila_Sum_Gpo.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                        }
                                                    }
                                                    Fila_Dt_Cog["AMPLIACION"] = Tot_Ampliacion_Capitulo;
                                                    Fila_Dt_Cog["REDUCCION"] = Tot_Reduccion_Capitulo;
                                                    Fila_Dt_Cog["INCREMENTO"] = Tot_Incremento_Capitulo;
                                                    Fila_Dt_Cog["MODIFICADO_TOTAL"] = Tot_Modificado_Capitulo;
                                                    Dt_Cog_Agrupado.Rows.Add(Fila_Dt_Cog);

                                                    //cambiamos los parametros y limpiamos variables
                                                    Capitulo = Dr_Cog["CAPITULO"].ToString().Trim();
                                                    Nom_Capitulo = Dr_Cog["NOMBRE_CAPITULO"].ToString().Trim();
                                                    Tot_Aprobado_Capitulo = 0.00;
                                                    Tot_Ampliacion_Capitulo = 0.00;
                                                    Tot_Reduccion_Capitulo = 0.00;
                                                    Tot_Incremento_Capitulo = 0.00;
                                                    Tot_Modificado_Capitulo = 0.00;

                                                    //agregamos la dependencia al  al datatable
                                                    Fila_Dt_Cog = Dt_Cog_Agrupado.NewRow();
                                                    Fila_Dt_Cog["CODIGO"] = Dr_Cog["PARTIDA"].ToString().Trim();
                                                    Fila_Dt_Cog["CONCEPTO"] = Dr_Cog["CONCEPTO"].ToString().Trim();
                                                    Fila_Dt_Cog["TIPO"] = Dr_Cog["TIPO"].ToString().Trim();
                                                    Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Dr_Cog["PRESUPUESTO_APROBADO"];
                                                    if (No_Mod > 1)
                                                    {
                                                        for (i = 1; i < No_Mod; i++)
                                                        {
                                                            Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = Dr_Cog[i.ToString().Trim() + "a. MODIFICACION"];
                                                        }
                                                    }
                                                    Fila_Dt_Cog["AMPLIACION"] = Dr_Cog["AMPLIACION"];
                                                    Fila_Dt_Cog["REDUCCION"] = Dr_Cog["REDUCCION"];
                                                    Fila_Dt_Cog["INCREMENTO"] = Dr_Cog["INCREMENTO"];
                                                    Fila_Dt_Cog["MODIFICADO_TOTAL"] = Dr_Cog["MODIFICADO_TOTAL"];
                                                    Dt_Cog_Agrupado.Rows.Add(Fila_Dt_Cog);
                                                }
                                            }

                                            //obtenemos el total del gpo dep final
                                            Tot_Aprobado_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                     where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                select Fila_Sum_Gpo.Field<Double>("PRESUPUESTO_APROBADO")).Sum();

                                            Tot_Ampliacion_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                       where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("AMPLIACION")).Sum();

                                            Tot_Reduccion_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                      where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                 select Fila_Sum_Gpo.Field<Double>("REDUCCION")).Sum();

                                            Tot_Incremento_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                       where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("INCREMENTO")).Sum();

                                            Tot_Modificado_Capitulo = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                       where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                  select Fila_Sum_Gpo.Field<Double>("MODIFICADO_TOTAL")).Sum();

                                            //agregamos el total al datatable
                                            Fila_Dt_Cog = Dt_Cog_Agrupado.NewRow();
                                            Fila_Dt_Cog["CODIGO"] = Capitulo.Trim();
                                            Fila_Dt_Cog["CONCEPTO"] = Nom_Capitulo.Trim();
                                            Fila_Dt_Cog["TIPO"] = "CAPITULO";
                                            Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Tot_Aprobado_Capitulo;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum_Gpo in Dt_Agrupado.AsEnumerable()
                                                                                                            where Fila_Sum_Gpo.Field<String>("CAPITULO").Trim() == Capitulo.Trim()
                                                                                                            select Fila_Sum_Gpo.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                }
                                            }
                                            Fila_Dt_Cog["AMPLIACION"] = Tot_Ampliacion_Capitulo;
                                            Fila_Dt_Cog["REDUCCION"] = Tot_Reduccion_Capitulo;
                                            Fila_Dt_Cog["INCREMENTO"] = Tot_Incremento_Capitulo;
                                            Fila_Dt_Cog["MODIFICADO_TOTAL"] = Tot_Modificado_Capitulo;
                                            Dt_Cog_Agrupado.Rows.Add(Fila_Dt_Cog);

                                            //obtenemos la suma del total
                                            Tot_Aprobado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("PRESUPUESTO_APROBADO"));
                                            Tot_Ampliacion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("AMPLIACION"));
                                            Tot_Reduccion = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("REDUCCION"));
                                            Tot_Incremento = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                                            Tot_Modificado = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>("MODIFICADO_TOTAL"));

                                            //agregamos el total al datatable
                                            Fila_Dt_Cog = Dt_Cog_Agrupado.NewRow();
                                            Fila_Dt_Cog["CODIGO"] = String.Empty;
                                            Fila_Dt_Cog["CONCEPTO"] = "TOTAL PRESUPUESTO ";
                                            Fila_Dt_Cog["TIPO"] = "TOTAL";
                                            Fila_Dt_Cog["PRESUPUESTO_APROBADO"] = Tot_Aprobado;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila_Dt_Cog[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Agrupado.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION"));
                                                }
                                            }
                                            Fila_Dt_Cog["AMPLIACION"] = Tot_Ampliacion;
                                            Fila_Dt_Cog["REDUCCION"] = Tot_Reduccion;
                                            Fila_Dt_Cog["INCREMENTO"] = Tot_Incremento;
                                            Fila_Dt_Cog["MODIFICADO_TOTAL"] = Tot_Modificado;
                                            Dt_Cog_Agrupado.Rows.Add(Fila_Dt_Cog);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Cog_Agrupado;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Analitico_Detallado
            ///DESCRIPCIÓN          : metodo para agrupar los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Analitico_Detallado(DataTable Dt_Psp, String Clasificacion_Adm)
            {
                DataTable Dt_Analitico = new DataTable();
                Decimal Ene = 0;
                Decimal Feb = 0;
                Decimal Mar = 0;
                Decimal Abr = 0;
                Decimal May = 0;
                Decimal Jun = 0;
                Decimal Jul = 0;
                Decimal Ago = 0;
                Decimal Sep = 0;
                Decimal Oct = 0;
                Decimal Nov = 0;
                Decimal Dic = 0;
                Decimal Apr = 0;
                Decimal Amp = 0;
                Decimal Red = 0;
                Decimal Mod = 0;
                Double Inc = 0;

                String Ur = String.Empty;
                String Gpo_Dep = String.Empty;
                String Programa = String.Empty;
                String Nom_Programa = String.Empty;
                String Nom_Ur = String.Empty;
                String Nom_Gpo_Dep = String.Empty;

                DataRow Fila;
                Boolean New_Programa = false;

                //para las modificaciones
                Int32 No_Mod = 0;
                int i = 0;

                try
                {
                    if (Dt_Psp != null)
                    {
                        if (Dt_Psp.Rows.Count > 0)
                        {
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            //CREAMOS LAS COLUMNAS DE NUESTRA TABLA
                            Dt_Analitico.Columns.Add("CODIGO_PROGRAMATICO", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("FF", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("SF", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("CA", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("PROGRAMA", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("UR", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("PARTIDA", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                            Dt_Analitico.Columns.Add("PRESUPUESTO_APROBADO", System.Type.GetType("System.Double"));
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt_Analitico.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", typeof(Double));
                                }
                            }
                            Dt_Analitico.Columns.Add("AMPLIACION", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("REDUCCION", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("INCREMENTO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("MODIFICADO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("ENERO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("FEBRERO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("MARZO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("ABRIL", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("MAYO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("JUNIO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("JULIO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("AGOSTO", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("OCTUBRE", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("NOVIEMBRE", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("DICIEMBRE", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("MODIFICADO_TOTAL", System.Type.GetType("System.Double"));
                            Dt_Analitico.Columns.Add("TIPO", System.Type.GetType("System.String"));

                            //OBTENEMOS NUESTROS PRIMEROS DATOS A MOSTRAR
                            Ur = Dt_Psp.Rows[0]["UR"].ToString().Trim();
                            Programa = Dt_Psp.Rows[0]["PROGRAMA"].ToString().Trim();
                            Nom_Ur = Dt_Psp.Rows[0]["NOMBRE_UR"].ToString().Trim();
                            Nom_Programa = Dt_Psp.Rows[0]["NOMBRE_PROGRAMA"].ToString().Trim();
                            Gpo_Dep = Dt_Psp.Rows[0]["GPO_DEP"].ToString().Trim();
                            Nom_Gpo_Dep = Dt_Psp.Rows[0]["NOMBRE_GPO_DEP"].ToString().Trim();
                            New_Programa = false;

                            //insertamos los primeros datos del Area funcional o clasificador por funcion del gasto,
                            // el programa y el clasificador administrativo o unidad responsable
                            //INSERTAMOS LA FILA DEL AREA FUNCIONAL
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = Dt_Psp.Rows[0]["SF"].ToString().Trim();
                            Fila["PROGRAMA"] = String.Empty;
                            Fila["UR"] = String.Empty;
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = Dt_Psp.Rows[0]["NOMBRE_SF"].ToString().Trim();
                            Fila["PRESUPUESTO_APROBADO"] = 0;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                }
                            }
                            Fila["AMPLIACION"] = 0;
                            Fila["REDUCCION"] = 0;
                            Fila["INCREMENTO"] = 0;
                            Fila["MODIFICADO"] = 0;
                            Fila["ENERO"] = 0;
                            Fila["FEBRERO"] = 0;
                            Fila["MARZO"] = 0;
                            Fila["ABRIL"] = 0;
                            Fila["MAYO"] = 0;
                            Fila["JUNIO"] = 0;
                            Fila["JULIO"] = 0;
                            Fila["AGOSTO"] = 0;
                            Fila["SEPTIEMBRE"] = 0;
                            Fila["OCTUBRE"] = 0;
                            Fila["NOVIEMBRE"] = 0;
                            Fila["DICIEMBRE"] = 0;
                            Fila["MODIFICADO_TOTAL"] = 0;
                            Fila["TIPO"] = "ENCABEZADO";
                            Dt_Analitico.Rows.Add(Fila);

                            //INSERTAMOS LA FILA DEL PROGRAMA
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = Dt_Psp.Rows[0]["SF"].ToString().Trim();
                            Fila["PROGRAMA"] = Dt_Psp.Rows[0]["PROGRAMA"].ToString().Trim();
                            Fila["UR"] = String.Empty;
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = Dt_Psp.Rows[0]["NOMBRE_PROGRAMA"].ToString().Trim();
                            Fila["PRESUPUESTO_APROBADO"] = 0;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                }
                            }
                            Fila["AMPLIACION"] = 0;
                            Fila["REDUCCION"] = 0;
                            Fila["INCREMENTO"] = 0;
                            Fila["MODIFICADO"] = 0;
                            Fila["ENERO"] = 0;
                            Fila["FEBRERO"] = 0;
                            Fila["MARZO"] = 0;
                            Fila["ABRIL"] = 0;
                            Fila["MAYO"] = 0;
                            Fila["JUNIO"] = 0;
                            Fila["JULIO"] = 0;
                            Fila["AGOSTO"] = 0;
                            Fila["SEPTIEMBRE"] = 0;
                            Fila["OCTUBRE"] = 0;
                            Fila["NOVIEMBRE"] = 0;
                            Fila["DICIEMBRE"] = 0;
                            Fila["MODIFICADO_TOTAL"] = 0;
                            Fila["TIPO"] = "ENCABEZADO";
                            Dt_Analitico.Rows.Add(Fila);

                            //INSERTAMOS LA FILA DEL UR
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = Dt_Psp.Rows[0]["SF"].ToString().Trim();
                            Fila["PROGRAMA"] = Dt_Psp.Rows[0]["PROGRAMA"].ToString().Trim();
                            Fila["UR"] = Dt_Psp.Rows[0]["UR"].ToString().Trim();
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = Dt_Psp.Rows[0]["NOMBRE_UR"].ToString().Trim();
                            Fila["PRESUPUESTO_APROBADO"] = 0;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                }
                            }
                            Fila["AMPLIACION"] = 0;
                            Fila["REDUCCION"] = 0;
                            Fila["INCREMENTO"] = 0;
                            Fila["MODIFICADO"] = 0;
                            Fila["ENERO"] = 0;
                            Fila["FEBRERO"] = 0;
                            Fila["MARZO"] = 0;
                            Fila["ABRIL"] = 0;
                            Fila["MAYO"] = 0;
                            Fila["JUNIO"] = 0;
                            Fila["JULIO"] = 0;
                            Fila["AGOSTO"] = 0;
                            Fila["SEPTIEMBRE"] = 0;
                            Fila["OCTUBRE"] = 0;
                            Fila["NOVIEMBRE"] = 0;
                            Fila["DICIEMBRE"] = 0;
                            Fila["MODIFICADO_TOTAL"] = 0;
                            Fila["TIPO"] = "ENCABEZADO";
                            Dt_Analitico.Rows.Add(Fila);

                            foreach (DataRow Dr in Dt_Psp.Rows)
                            {
                                //validamos el Grupo dependencia o ramo administrativo
                                if (Dr["GPO_DEP"].ToString().Trim().Equals(Gpo_Dep.Trim()))
                                {
                                    //validamos la dependencia o clasificador administrativo
                                    if (Dr["UR"].ToString().Trim().Equals(Ur.Trim()))
                                    {
                                        //validamos el proyecto o programa
                                        if (Dr["PROGRAMA"].ToString().Trim() != Programa.Trim())
                                        {
                                            #region (programa)
                                            //insertamos el total de la dependencia
                                            Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                            Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                            Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                            Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                            May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                            Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                            Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                            Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                            Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                            Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                            Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                            Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                            Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                            Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                            Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                            Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                            Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                            //INSERTAMOS LA FILA DEL PROGRAMA
                                            Fila = Dt_Analitico.NewRow();
                                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                            Fila["FF"] = String.Empty;
                                            Fila["SF"] = String.Empty;
                                            Fila["PROGRAMA"] = String.Empty;
                                            Fila["UR"] = String.Empty;
                                            Fila["PARTIDA"] = String.Empty;
                                            Fila["CONCEPTO"] = "TOTAL " + Nom_Programa.ToString().Trim();
                                            Fila["PRESUPUESTO_APROBADO"] = Apr;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                                     where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                                     && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                                                                     select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                }
                                            }
                                            Fila["AMPLIACION"] = Amp;
                                            Fila["REDUCCION"] = Red;
                                            Fila["INCREMENTO"] = Inc;
                                            Fila["MODIFICADO"] = Mod;
                                            Fila["ENERO"] = Ene;
                                            Fila["FEBRERO"] = Feb;
                                            Fila["MARZO"] = Mar;
                                            Fila["ABRIL"] = Abr;
                                            Fila["MAYO"] = May;
                                            Fila["JUNIO"] = Jun;
                                            Fila["JULIO"] = Jul;
                                            Fila["AGOSTO"] = Ago;
                                            Fila["SEPTIEMBRE"] = Sep;
                                            Fila["OCTUBRE"] = Oct;
                                            Fila["NOVIEMBRE"] = Nov;
                                            Fila["DICIEMBRE"] = Dic;
                                            Fila["MODIFICADO_TOTAL"] = Mod;
                                            Fila["TIPO"] = "TOTAL_PROGRAMA";
                                            Dt_Analitico.Rows.Add(Fila);

                                            //INSERTAMOS LA FILA DEL PROGRAMA
                                            Fila = Dt_Analitico.NewRow();
                                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                            Fila["FF"] = String.Empty;
                                            Fila["SF"] = Dr["SF"].ToString().Trim();
                                            Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                            Fila["UR"] = String.Empty;
                                            Fila["PARTIDA"] = String.Empty;
                                            Fila["CONCEPTO"] = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                            Fila["PRESUPUESTO_APROBADO"] = 0;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                                }
                                            }
                                            Fila["AMPLIACION"] = 0;
                                            Fila["REDUCCION"] = 0;
                                            Fila["INCREMENTO"] = 0;
                                            Fila["MODIFICADO"] = 0;
                                            Fila["ENERO"] = 0;
                                            Fila["FEBRERO"] = 0;
                                            Fila["MARZO"] = 0;
                                            Fila["ABRIL"] = 0;
                                            Fila["MAYO"] = 0;
                                            Fila["JUNIO"] = 0;
                                            Fila["JULIO"] = 0;
                                            Fila["AGOSTO"] = 0;
                                            Fila["SEPTIEMBRE"] = 0;
                                            Fila["OCTUBRE"] = 0;
                                            Fila["NOVIEMBRE"] = 0;
                                            Fila["DICIEMBRE"] = 0;
                                            Fila["MODIFICADO_TOTAL"] = 0;
                                            Fila["TIPO"] = "ENCABEZADO";
                                            Dt_Analitico.Rows.Add(Fila);

                                            //ASIGNAMOS LOS NUEVOS PARAMETROS
                                            Programa = Dr["PROGRAMA"].ToString().Trim();
                                            Nom_Programa = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                            New_Programa = true;
                                            #endregion
                                        }

                                        //INSERTAMOS LOS DATOS DE LA PARTIDA
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = Dr["CODIGO_PROGRAMATICO"].ToString().Trim();
                                        Fila["FF"] = Dr["FF"].ToString().Trim();
                                        Fila["SF"] = Dr["SF"].ToString().Trim();
                                        Fila["CA"] = Clasificacion_Adm;
                                        Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                        Fila["UR"] = Dr["UR"].ToString().Trim();
                                        Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);
                                            }
                                        }
                                        Fila["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                        Fila["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                        Fila["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                        Fila["MODIFICADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString().Trim()) ? "0" : Dr["ENERO"]);
                                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString().Trim()) ? "0" : Dr["FEBRERO"]);
                                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString().Trim()) ? "0" : Dr["MARZO"]);
                                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString().Trim()) ? "0" : Dr["ABRIL"]);
                                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString().Trim()) ? "0" : Dr["MAYO"]);
                                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString().Trim()) ? "0" : Dr["JUNIO"]);
                                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString().Trim()) ? "0" : Dr["JULIO"]);
                                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString().Trim()) ? "0" : Dr["AGOSTO"]);
                                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["SEPTIEMBRE"]);
                                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString().Trim()) ? "0" : Dr["OCTUBRE"]);
                                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["NOVIEMBRE"]);
                                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString().Trim()) ? "0" : Dr["DICIEMBRE"]);
                                        Fila["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                        Fila["TIPO"] = "CONCEPTO";
                                        Dt_Analitico.Rows.Add(Fila);

                                    }// fin if dependencia o clasificador administrativo
                                    else //else dependencia o clasificador administrativo
                                    {
                                        #region (else Dependencia)

                                        if (New_Programa)
                                        {
                                            #region (programa)
                                            //insertamos el total de la dependencia
                                            Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                            Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                            Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                            Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                            May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                            Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                            Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                            Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                            Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                            Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                            Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                            Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                            Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                            Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                            Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                            Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                            Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                   && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                   select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                            //INSERTAMOS LA FILA DEL PROGRAMA
                                            Fila = Dt_Analitico.NewRow();
                                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                            Fila["FF"] = String.Empty;
                                            Fila["SF"] = String.Empty;
                                            Fila["PROGRAMA"] = String.Empty;
                                            Fila["UR"] = String.Empty;
                                            Fila["PARTIDA"] = String.Empty;
                                            Fila["CONCEPTO"] = "TOTAL " + Nom_Programa.ToString().Trim();
                                            Fila["PRESUPUESTO_APROBADO"] = Apr;
                                            if (No_Mod > 1)
                                            {
                                                for (i = 1; i < No_Mod; i++)
                                                {
                                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                                     where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                                     && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                                                                     select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                                }
                                            }
                                            Fila["AMPLIACION"] = Amp;
                                            Fila["REDUCCION"] = Red;
                                            Fila["INCREMENTO"] = Inc;
                                            Fila["MODIFICADO"] = Mod;
                                            Fila["ENERO"] = Ene;
                                            Fila["FEBRERO"] = Feb;
                                            Fila["MARZO"] = Mar;
                                            Fila["ABRIL"] = Abr;
                                            Fila["MAYO"] = May;
                                            Fila["JUNIO"] = Jun;
                                            Fila["JULIO"] = Jul;
                                            Fila["AGOSTO"] = Ago;
                                            Fila["SEPTIEMBRE"] = Sep;
                                            Fila["OCTUBRE"] = Oct;
                                            Fila["NOVIEMBRE"] = Nov;
                                            Fila["DICIEMBRE"] = Dic;
                                            Fila["MODIFICADO_TOTAL"] = Mod;
                                            Fila["TIPO"] = "TOTAL_PROGRAMA";
                                            Dt_Analitico.Rows.Add(Fila);
                                            #endregion
                                        }

                                        //insertamos el total de la dependencia
                                        Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                        Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                        Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                        Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                        May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                        Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                        Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                        Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                        Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                        Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                        Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                        Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                        Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                        Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                        Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                        Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                        Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                        //INSERTAMOS LA FILA DEL UR
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                        Fila["FF"] = String.Empty;
                                        Fila["SF"] = String.Empty;
                                        Fila["PROGRAMA"] = String.Empty;
                                        Fila["UR"] = String.Empty;
                                        Fila["PARTIDA"] = String.Empty;
                                        Fila["CONCEPTO"] = "TOTAL " + Nom_Ur.ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = Apr;
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                                where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                                select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                            }
                                        }
                                        Fila["AMPLIACION"] = Amp;
                                        Fila["REDUCCION"] = Red;
                                        Fila["INCREMENTO"] = Inc;
                                        Fila["MODIFICADO"] = Mod;
                                        Fila["ENERO"] = Ene;
                                        Fila["FEBRERO"] = Feb;
                                        Fila["MARZO"] = Mar;
                                        Fila["ABRIL"] = Abr;
                                        Fila["MAYO"] = May;
                                        Fila["JUNIO"] = Jun;
                                        Fila["JULIO"] = Jul;
                                        Fila["AGOSTO"] = Ago;
                                        Fila["SEPTIEMBRE"] = Sep;
                                        Fila["OCTUBRE"] = Oct;
                                        Fila["NOVIEMBRE"] = Nov;
                                        Fila["DICIEMBRE"] = Dic;
                                        Fila["MODIFICADO_TOTAL"] = Mod;
                                        Fila["TIPO"] = "TOTAL_UR";
                                        Dt_Analitico.Rows.Add(Fila);


                                        //INSERTAMOS LOS DATOS DE AREA FUNCIONAL, PROGRAMA, UNIDAD RESPONSABLE NUEVOS
                                        //INSERTAMOS LA FILA DEL AREA FUNCIONAL
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                        Fila["FF"] = String.Empty;
                                        Fila["SF"] = Dr["SF"].ToString().Trim();
                                        Fila["PROGRAMA"] = String.Empty;
                                        Fila["UR"] = String.Empty;
                                        Fila["PARTIDA"] = String.Empty;
                                        Fila["CONCEPTO"] = Dr["NOMBRE_SF"].ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = 0;
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                            }
                                        }
                                        Fila["AMPLIACION"] = 0;
                                        Fila["REDUCCION"] = 0;
                                        Fila["INCREMENTO"] = 0;
                                        Fila["MODIFICADO"] = 0;
                                        Fila["ENERO"] = 0;
                                        Fila["FEBRERO"] = 0;
                                        Fila["MARZO"] = 0;
                                        Fila["ABRIL"] = 0;
                                        Fila["MAYO"] = 0;
                                        Fila["JUNIO"] = 0;
                                        Fila["JULIO"] = 0;
                                        Fila["AGOSTO"] = 0;
                                        Fila["SEPTIEMBRE"] = 0;
                                        Fila["OCTUBRE"] = 0;
                                        Fila["NOVIEMBRE"] = 0;
                                        Fila["DICIEMBRE"] = 0;
                                        Fila["MODIFICADO_TOTAL"] = 0;
                                        Fila["TIPO"] = "ENCABEZADO";
                                        Dt_Analitico.Rows.Add(Fila);

                                        //INSERTAMOS LA FILA DEL PROGRAMA
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                        Fila["FF"] = String.Empty;
                                        Fila["SF"] = Dr["SF"].ToString().Trim();
                                        Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                        Fila["UR"] = String.Empty;
                                        Fila["PARTIDA"] = String.Empty;
                                        Fila["CONCEPTO"] = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = 0;
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                            }
                                        }
                                        Fila["AMPLIACION"] = 0;
                                        Fila["REDUCCION"] = 0;
                                        Fila["INCREMENTO"] = 0;
                                        Fila["MODIFICADO"] = 0;
                                        Fila["ENERO"] = 0;
                                        Fila["FEBRERO"] = 0;
                                        Fila["MARZO"] = 0;
                                        Fila["ABRIL"] = 0;
                                        Fila["MAYO"] = 0;
                                        Fila["JUNIO"] = 0;
                                        Fila["JULIO"] = 0;
                                        Fila["AGOSTO"] = 0;
                                        Fila["SEPTIEMBRE"] = 0;
                                        Fila["OCTUBRE"] = 0;
                                        Fila["NOVIEMBRE"] = 0;
                                        Fila["DICIEMBRE"] = 0;
                                        Fila["MODIFICADO_TOTAL"] = 0;
                                        Fila["TIPO"] = "ENCABEZADO";
                                        Dt_Analitico.Rows.Add(Fila);

                                        //INSERTAMOS LA FILA DEL UR
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                        Fila["FF"] = String.Empty;
                                        Fila["SF"] = Dr["SF"].ToString().Trim();
                                        Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                        Fila["UR"] = Dr["UR"].ToString().Trim();
                                        Fila["PARTIDA"] = String.Empty;
                                        Fila["CONCEPTO"] = Dr["NOMBRE_UR"].ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = 0;
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                            }
                                        }
                                        Fila["AMPLIACION"] = 0;
                                        Fila["REDUCCION"] = 0;
                                        Fila["INCREMENTO"] = 0;
                                        Fila["MODIFICADO"] = 0;
                                        Fila["ENERO"] = 0;
                                        Fila["FEBRERO"] = 0;
                                        Fila["MARZO"] = 0;
                                        Fila["ABRIL"] = 0;
                                        Fila["MAYO"] = 0;
                                        Fila["JUNIO"] = 0;
                                        Fila["JULIO"] = 0;
                                        Fila["AGOSTO"] = 0;
                                        Fila["SEPTIEMBRE"] = 0;
                                        Fila["OCTUBRE"] = 0;
                                        Fila["NOVIEMBRE"] = 0;
                                        Fila["DICIEMBRE"] = 0;
                                        Fila["MODIFICADO_TOTAL"] = 0;
                                        Fila["TIPO"] = "ENCABEZADO";
                                        Dt_Analitico.Rows.Add(Fila);

                                        //ASIGNAMOS LOS NUEVOS PARAMETROS
                                        Ur = Dr["UR"].ToString().Trim();
                                        Programa = Dr["PROGRAMA"].ToString().Trim();
                                        Nom_Ur = Dr["NOMBRE_UR"].ToString().Trim();
                                        Nom_Programa = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                        New_Programa = false;

                                        //INSERTAMOS LOS DATOS DE LA PARTIDA
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = Dr["CODIGO_PROGRAMATICO"].ToString().Trim();
                                        Fila["FF"] = Dr["FF"].ToString().Trim();
                                        Fila["SF"] = Dr["SF"].ToString().Trim();
                                        Fila["CA"] = Clasificacion_Adm;
                                        Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                        Fila["UR"] = Dr["UR"].ToString().Trim();
                                        Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                        Fila["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);
                                            }
                                        }
                                        Fila["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                        Fila["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                        Fila["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                        Fila["MODIFICADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString().Trim()) ? "0" : Dr["ENERO"]);
                                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString().Trim()) ? "0" : Dr["FEBRERO"]);
                                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString().Trim()) ? "0" : Dr["MARZO"]);
                                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString().Trim()) ? "0" : Dr["ABRIL"]);
                                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString().Trim()) ? "0" : Dr["MAYO"]);
                                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString().Trim()) ? "0" : Dr["JUNIO"]);
                                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString().Trim()) ? "0" : Dr["JULIO"]);
                                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString().Trim()) ? "0" : Dr["AGOSTO"]);
                                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["SEPTIEMBRE"]);
                                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString().Trim()) ? "0" : Dr["OCTUBRE"]);
                                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["NOVIEMBRE"]);
                                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString().Trim()) ? "0" : Dr["DICIEMBRE"]);
                                        Fila["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                        Fila["TIPO"] = "CONCEPTO";
                                        Dt_Analitico.Rows.Add(Fila);
                                        #endregion
                                    }
                                } // fin Grupo dependencia o ramo administrativo
                                else // else Grupo dependencia o ramo administrativo
                                {
                                    #region(Else Gpo dependencia)
                                    //si cambia de Gpo dependencia insertamos los datos.

                                    if (New_Programa) //VALIDAMOS SI HUBO CAMBIO DE PROGRAMA EN EL PRESUPUESTO DE LA DEPENDENCIA
                                    {
                                        #region (programa)
                                        //insertamos el total del programa
                                        Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                        Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                        Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                        Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                        May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                        Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                        Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                        Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                        Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                        Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                        Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                        Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                        Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                        Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                        Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                        Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                        Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                               where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                               && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                               select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                        //INSERTAMOS LA FILA DEL PROGRAMA
                                        Fila = Dt_Analitico.NewRow();
                                        Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                        Fila["FF"] = String.Empty;
                                        Fila["SF"] = String.Empty;
                                        Fila["PROGRAMA"] = String.Empty;
                                        Fila["UR"] = String.Empty;
                                        Fila["PARTIDA"] = String.Empty;
                                        Fila["CONCEPTO"] = "TOTAL " + Nom_Programa.ToString().Trim();
                                        if (No_Mod > 1)
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                                 where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                                 && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                                                                 select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                            }
                                        }
                                        Fila["PRESUPUESTO_APROBADO"] = Apr;
                                        Fila["AMPLIACION"] = Amp;
                                        Fila["REDUCCION"] = Red;
                                        Fila["INCREMENTO"] = Inc;
                                        Fila["MODIFICADO"] = Mod;
                                        Fila["ENERO"] = Ene;
                                        Fila["FEBRERO"] = Feb;
                                        Fila["MARZO"] = Mar;
                                        Fila["ABRIL"] = Abr;
                                        Fila["MAYO"] = May;
                                        Fila["JUNIO"] = Jun;
                                        Fila["JULIO"] = Jul;
                                        Fila["AGOSTO"] = Ago;
                                        Fila["SEPTIEMBRE"] = Sep;
                                        Fila["OCTUBRE"] = Oct;
                                        Fila["NOVIEMBRE"] = Nov;
                                        Fila["DICIEMBRE"] = Dic;
                                        Fila["MODIFICADO_TOTAL"] = Mod;
                                        Fila["TIPO"] = "TOTAL_PROGRAMA";
                                        Dt_Analitico.Rows.Add(Fila);
                                        #endregion
                                    }

                                    //insertamos el total de la dependencia
                                    Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                    Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                    Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                    Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                    May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                    Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                    Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                    Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                    Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                    Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                    Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                    Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                    Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                    Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                    Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                    Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                    Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                           select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                    //INSERTAMOS LA FILA DEL UR
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                    Fila["FF"] = String.Empty;
                                    Fila["SF"] = String.Empty;
                                    Fila["PROGRAMA"] = String.Empty;
                                    Fila["UR"] = String.Empty;
                                    Fila["PARTIDA"] = String.Empty;
                                    Fila["CONCEPTO"] = "TOTAL " + Nom_Ur.ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = Apr;
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                             where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                             select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                        }
                                    }
                                    Fila["AMPLIACION"] = Amp;
                                    Fila["REDUCCION"] = Red;
                                    Fila["INCREMENTO"] = Inc;
                                    Fila["MODIFICADO"] = Mod;
                                    Fila["ENERO"] = Ene;
                                    Fila["FEBRERO"] = Feb;
                                    Fila["MARZO"] = Mar;
                                    Fila["ABRIL"] = Abr;
                                    Fila["MAYO"] = May;
                                    Fila["JUNIO"] = Jun;
                                    Fila["JULIO"] = Jul;
                                    Fila["AGOSTO"] = Ago;
                                    Fila["SEPTIEMBRE"] = Sep;
                                    Fila["OCTUBRE"] = Oct;
                                    Fila["NOVIEMBRE"] = Nov;
                                    Fila["DICIEMBRE"] = Dic;
                                    Fila["MODIFICADO_TOTAL"] = Mod;
                                    Fila["TIPO"] = "TOTAL_UR";
                                    Dt_Analitico.Rows.Add(Fila);


                                    //insertamos el total del grupo dependencia
                                    Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                    Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                    Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                    Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                    May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                    Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                    Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                    Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                    Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                    Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                    Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                    Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                    Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                    Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                    Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                    Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                    Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                           where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                           select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                    //INSERTAMOS LA FILA DEL GRUPO DEPENDENCIA
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                    Fila["FF"] = String.Empty;
                                    Fila["SF"] = String.Empty;
                                    Fila["PROGRAMA"] = String.Empty;
                                    Fila["UR"] = String.Empty;
                                    Fila["PARTIDA"] = String.Empty;
                                    Fila["CONCEPTO"] = "TOTAL " + Nom_Gpo_Dep.ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = Apr;
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                             where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                                                                             select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                        }
                                    }
                                    Fila["AMPLIACION"] = Amp;
                                    Fila["REDUCCION"] = Red;
                                    Fila["INCREMENTO"] = Inc;
                                    Fila["MODIFICADO"] = Mod;
                                    Fila["ENERO"] = Ene;
                                    Fila["FEBRERO"] = Feb;
                                    Fila["MARZO"] = Mar;
                                    Fila["ABRIL"] = Abr;
                                    Fila["MAYO"] = May;
                                    Fila["JUNIO"] = Jun;
                                    Fila["JULIO"] = Jul;
                                    Fila["AGOSTO"] = Ago;
                                    Fila["SEPTIEMBRE"] = Sep;
                                    Fila["OCTUBRE"] = Oct;
                                    Fila["NOVIEMBRE"] = Nov;
                                    Fila["DICIEMBRE"] = Dic;
                                    Fila["MODIFICADO_TOTAL"] = Mod;
                                    Fila["TIPO"] = "TOTAL_GPO_DEP";
                                    Dt_Analitico.Rows.Add(Fila);

                                    //INSERTAMOS LOS DATOS DE AREA FUNCIONAL, PROGRAMA, UNIDAD RESPONSABLE NUEVOS
                                    //INSERTAMOS LA FILA DEL AREA FUNCIONAL
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                    Fila["FF"] = String.Empty;
                                    Fila["SF"] = Dr["SF"].ToString().Trim();
                                    Fila["PROGRAMA"] = String.Empty;
                                    Fila["UR"] = String.Empty;
                                    Fila["PARTIDA"] = String.Empty;
                                    Fila["CONCEPTO"] = Dr["NOMBRE_SF"].ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = 0;
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                        }
                                    }
                                    Fila["AMPLIACION"] = 0;
                                    Fila["REDUCCION"] = 0;
                                    Fila["INCREMENTO"] = 0;
                                    Fila["MODIFICADO"] = 0;
                                    Fila["ENERO"] = 0;
                                    Fila["FEBRERO"] = 0;
                                    Fila["MARZO"] = 0;
                                    Fila["ABRIL"] = 0;
                                    Fila["MAYO"] = 0;
                                    Fila["JUNIO"] = 0;
                                    Fila["JULIO"] = 0;
                                    Fila["AGOSTO"] = 0;
                                    Fila["SEPTIEMBRE"] = 0;
                                    Fila["OCTUBRE"] = 0;
                                    Fila["NOVIEMBRE"] = 0;
                                    Fila["DICIEMBRE"] = 0;
                                    Fila["MODIFICADO_TOTAL"] = 0;
                                    Fila["TIPO"] = "ENCABEZADO";
                                    Dt_Analitico.Rows.Add(Fila);

                                    //INSERTAMOS LA FILA DEL PROGRAMA
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                    Fila["FF"] = String.Empty;
                                    Fila["SF"] = Dr["SF"].ToString().Trim();
                                    Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                    Fila["UR"] = String.Empty;
                                    Fila["PARTIDA"] = String.Empty;
                                    Fila["CONCEPTO"] = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = 0;
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                        }
                                    }
                                    Fila["AMPLIACION"] = 0;
                                    Fila["REDUCCION"] = 0;
                                    Fila["INCREMENTO"] = 0;
                                    Fila["MODIFICADO"] = 0;
                                    Fila["ENERO"] = 0;
                                    Fila["FEBRERO"] = 0;
                                    Fila["MARZO"] = 0;
                                    Fila["ABRIL"] = 0;
                                    Fila["MAYO"] = 0;
                                    Fila["JUNIO"] = 0;
                                    Fila["JULIO"] = 0;
                                    Fila["AGOSTO"] = 0;
                                    Fila["SEPTIEMBRE"] = 0;
                                    Fila["OCTUBRE"] = 0;
                                    Fila["NOVIEMBRE"] = 0;
                                    Fila["DICIEMBRE"] = 0;
                                    Fila["MODIFICADO_TOTAL"] = 0;
                                    Fila["TIPO"] = "ENCABEZADO";
                                    Dt_Analitico.Rows.Add(Fila);

                                    //INSERTAMOS LA FILA DEL UR
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                    Fila["FF"] = String.Empty;
                                    Fila["SF"] = Dr["SF"].ToString().Trim();
                                    Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                    Fila["UR"] = Dr["UR"].ToString().Trim();
                                    Fila["PARTIDA"] = String.Empty;
                                    Fila["CONCEPTO"] = Dr["NOMBRE_UR"].ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = 0;
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = 0;
                                        }
                                    }
                                    Fila["AMPLIACION"] = 0;
                                    Fila["REDUCCION"] = 0;
                                    Fila["INCREMENTO"] = 0;
                                    Fila["MODIFICADO"] = 0;
                                    Fila["ENERO"] = 0;
                                    Fila["FEBRERO"] = 0;
                                    Fila["MARZO"] = 0;
                                    Fila["ABRIL"] = 0;
                                    Fila["MAYO"] = 0;
                                    Fila["JUNIO"] = 0;
                                    Fila["JULIO"] = 0;
                                    Fila["AGOSTO"] = 0;
                                    Fila["SEPTIEMBRE"] = 0;
                                    Fila["OCTUBRE"] = 0;
                                    Fila["NOVIEMBRE"] = 0;
                                    Fila["DICIEMBRE"] = 0;
                                    Fila["MODIFICADO_TOTAL"] = 0;
                                    Fila["TIPO"] = "ENCABEZADO";
                                    Dt_Analitico.Rows.Add(Fila);

                                    //ASIGNAMOS LOS NUEVOS PARAMETROS
                                    Ur = Dr["UR"].ToString().Trim();
                                    Programa = Dr["PROGRAMA"].ToString().Trim();
                                    Nom_Ur = Dr["NOMBRE_UR"].ToString().Trim();
                                    Nom_Programa = Dr["NOMBRE_PROGRAMA"].ToString().Trim();
                                    Gpo_Dep = Dr["GPO_DEP"].ToString().Trim();
                                    Nom_Gpo_Dep = Dr["NOMBRE_GPO_DEP"].ToString().Trim();
                                    New_Programa = false;

                                    //INSERTAMOS LOS DATOS DE LA PARTIDA
                                    Fila = Dt_Analitico.NewRow();
                                    Fila["CODIGO_PROGRAMATICO"] = Dr["CODIGO_PROGRAMATICO"].ToString().Trim();
                                    Fila["FF"] = Dr["FF"].ToString().Trim();
                                    Fila["SF"] = Dr["SF"].ToString().Trim();
                                    Fila["CA"] = Clasificacion_Adm;
                                    Fila["PROGRAMA"] = Dr["PROGRAMA"].ToString().Trim();
                                    Fila["UR"] = Dr["UR"].ToString().Trim();
                                    Fila["PARTIDA"] = Dr["PARTIDA"].ToString().Trim();
                                    Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                    Fila["PRESUPUESTO_APROBADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_APROBADO"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_APROBADO"]);
                                    if (No_Mod > 1)
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Fila[i.ToString().Trim() + "a. MODIFICACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr[i.ToString().Trim() + "a. MODIFICACION"].ToString().Trim()) ? "0" : Dr[i.ToString().Trim() + "a. MODIFICACION"]);
                                        }
                                    }
                                    Fila["AMPLIACION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"]);
                                    Fila["REDUCCION"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"]);
                                    Fila["INCREMENTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["INCREMENTO"].ToString().Trim()) ? "0" : Dr["INCREMENTO"]);
                                    Fila["MODIFICADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                    Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString().Trim()) ? "0" : Dr["ENERO"]);
                                    Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString().Trim()) ? "0" : Dr["FEBRERO"]);
                                    Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString().Trim()) ? "0" : Dr["MARZO"]);
                                    Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString().Trim()) ? "0" : Dr["ABRIL"]);
                                    Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString().Trim()) ? "0" : Dr["MAYO"]);
                                    Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString().Trim()) ? "0" : Dr["JUNIO"]);
                                    Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString().Trim()) ? "0" : Dr["JULIO"]);
                                    Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString().Trim()) ? "0" : Dr["AGOSTO"]);
                                    Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["SEPTIEMBRE"]);
                                    Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString().Trim()) ? "0" : Dr["OCTUBRE"]);
                                    Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["NOVIEMBRE"]);
                                    Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString().Trim()) ? "0" : Dr["DICIEMBRE"]);
                                    Fila["MODIFICADO_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO_TOTAL"].ToString().Trim()) ? "0" : Dr["MODIFICADO_TOTAL"]);
                                    Fila["TIPO"] = "CONCEPTO";
                                    Dt_Analitico.Rows.Add(Fila);
                                    #endregion
                                }
                            }

                            //INSERTAMOS LOS ULTIMOS TOTALES ALMACENADOS
                            if (New_Programa) //VALIDAMOS SI HUBO CAMBIO DE PROGRAMA EN EL PRESUPUESTO DE LA DEPENDENCIA
                            {
                                #region (programa)
                                //insertamos el total del programa
                                Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                                Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                                Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                                Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                                May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                                Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                                Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                                Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                                Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                                Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                                Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                                Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                                Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                                Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                                Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                                Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                                Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                       where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                       && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                       select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                                //INSERTAMOS LA FILA DEL PROGRAMA
                                Fila = Dt_Analitico.NewRow();
                                Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                                Fila["FF"] = String.Empty;
                                Fila["SF"] = String.Empty;
                                Fila["PROGRAMA"] = String.Empty;
                                Fila["UR"] = String.Empty;
                                Fila["PARTIDA"] = String.Empty;
                                Fila["CONCEPTO"] = "TOTAL " + Nom_Programa.ToString().Trim();
                                Fila["PRESUPUESTO_APROBADO"] = Apr;
                                if (No_Mod > 1)
                                {
                                    for (i = 1; i < No_Mod; i++)
                                    {
                                        Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                         where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                         && Fila_Sum.Field<String>("PROGRAMA").Trim() == Programa.Trim()
                                                                                         select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                    }
                                }
                                Fila["AMPLIACION"] = Amp;
                                Fila["REDUCCION"] = Red;
                                Fila["INCREMENTO"] = Inc;
                                Fila["MODIFICADO"] = Mod;
                                Fila["ENERO"] = Ene;
                                Fila["FEBRERO"] = Feb;
                                Fila["MARZO"] = Mar;
                                Fila["ABRIL"] = Abr;
                                Fila["MAYO"] = May;
                                Fila["JUNIO"] = Jun;
                                Fila["JULIO"] = Jul;
                                Fila["AGOSTO"] = Ago;
                                Fila["SEPTIEMBRE"] = Sep;
                                Fila["OCTUBRE"] = Oct;
                                Fila["NOVIEMBRE"] = Nov;
                                Fila["DICIEMBRE"] = Dic;
                                Fila["MODIFICADO_TOTAL"] = Mod;
                                Fila["TIPO"] = "TOTAL_PROGRAMA";
                                Dt_Analitico.Rows.Add(Fila);
                                #endregion
                            }

                            //insertamos el total de la dependencia
                            Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                            Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                            Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                            Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                            May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                            Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                            Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                            Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                            Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                            Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                            Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                            Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                            Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                            Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                            Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                            Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                            Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                   select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                            //INSERTAMOS LA FILA DEL UR
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = String.Empty;
                            Fila["PROGRAMA"] = String.Empty;
                            Fila["UR"] = String.Empty;
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = "TOTAL " + Nom_Ur.ToString().Trim();
                            Fila["PRESUPUESTO_APROBADO"] = Apr;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                     where Fila_Sum.Field<String>("UR").Trim() == Ur.Trim()
                                                                                     select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                }
                            }
                            Fila["AMPLIACION"] = Amp;
                            Fila["REDUCCION"] = Red;
                            Fila["INCREMENTO"] = Inc;
                            Fila["MODIFICADO"] = Mod;
                            Fila["ENERO"] = Ene;
                            Fila["FEBRERO"] = Feb;
                            Fila["MARZO"] = Mar;
                            Fila["ABRIL"] = Abr;
                            Fila["MAYO"] = May;
                            Fila["JUNIO"] = Jun;
                            Fila["JULIO"] = Jul;
                            Fila["AGOSTO"] = Ago;
                            Fila["SEPTIEMBRE"] = Sep;
                            Fila["OCTUBRE"] = Oct;
                            Fila["NOVIEMBRE"] = Nov;
                            Fila["DICIEMBRE"] = Dic;
                            Fila["MODIFICADO_TOTAL"] = Mod;
                            Fila["TIPO"] = "TOTAL_UR";
                            Dt_Analitico.Rows.Add(Fila);


                            //insertamos el total del grupo dependencia
                            Ene = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("ENERO")).Sum();
                            Feb = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("FEBRERO")).Sum();
                            Mar = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("MARZO")).Sum();
                            Abr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("ABRIL")).Sum();
                            May = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("MAYO")).Sum();
                            Jun = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("JUNIO")).Sum();
                            Jul = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("JULIO")).Sum();
                            Ago = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("AGOSTO")).Sum();
                            Sep = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("SEPTIEMBRE")).Sum();
                            Oct = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("OCTUBRE")).Sum();
                            Nov = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("NOVIEMBRE")).Sum();
                            Dic = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("DICIEMBRE")).Sum();
                            Apr = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("PRESUPUESTO_APROBADO")).Sum();
                            Amp = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("AMPLIACION")).Sum();
                            Red = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("REDUCCION")).Sum();
                            Inc = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Double>("INCREMENTO")).Sum();
                            Mod = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                   where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                   select Fila_Sum.Field<Decimal>("MODIFICADO_TOTAL")).Sum();

                            //INSERTAMOS LA FILA DEL GRUPO DEPENDENCIA
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = String.Empty;
                            Fila["PROGRAMA"] = String.Empty;
                            Fila["UR"] = String.Empty;
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = "TOTAL " + Nom_Gpo_Dep.ToString().Trim();
                            Fila["PRESUPUESTO_APROBADO"] = Apr;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = (from Fila_Sum in Dt_Psp.AsEnumerable()
                                                                                     where Fila_Sum.Field<String>("GPO_DEP").Trim() == Gpo_Dep.Trim()
                                                                                     select Fila_Sum.Field<Double>(i.ToString().Trim() + "a. MODIFICACION")).Sum();
                                }
                            }
                            Fila["AMPLIACION"] = Amp;
                            Fila["REDUCCION"] = Red;
                            Fila["INCREMENTO"] = Inc;
                            Fila["MODIFICADO"] = Mod;
                            Fila["ENERO"] = Ene;
                            Fila["FEBRERO"] = Feb;
                            Fila["MARZO"] = Mar;
                            Fila["ABRIL"] = Abr;
                            Fila["MAYO"] = May;
                            Fila["JUNIO"] = Jun;
                            Fila["JULIO"] = Jul;
                            Fila["AGOSTO"] = Ago;
                            Fila["SEPTIEMBRE"] = Sep;
                            Fila["OCTUBRE"] = Oct;
                            Fila["NOVIEMBRE"] = Nov;
                            Fila["DICIEMBRE"] = Dic;
                            Fila["MODIFICADO_TOTAL"] = Mod;
                            Fila["TIPO"] = "TOTAL_GPO_DEP";
                            Dt_Analitico.Rows.Add(Fila);

                            //INSERTAMOS EL TOTAL DEL PRESUPUESTO
                            //insertamos el total del grupo dependencia
                            Ene = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("ENERO"));
                            Feb = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("FEBRERO"));
                            Mar = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("MARZO"));
                            Abr = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("ABRIL"));
                            May = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("MAYO"));
                            Jun = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("JUNIO"));
                            Jul = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("JULIO"));
                            Ago = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("AGOSTO"));
                            Sep = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("SEPTIEMBRE"));
                            Oct = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("OCTUBRE"));
                            Nov = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("NOVIEMBRE"));
                            Dic = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("DICIEMBRE"));
                            Apr = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("PRESUPUESTO_APROBADO"));
                            Amp = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("AMPLIACION"));
                            Red = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("REDUCCION"));
                            Inc = Dt_Psp.AsEnumerable().Sum(x => x.Field<Double>("INCREMENTO"));
                            Mod = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("MODIFICADO_TOTAL"));

                            //INSERTAMOS LA FILA DEL TOTAL DEL PRESUPUESTO
                            Fila = Dt_Analitico.NewRow();
                            Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                            Fila["FF"] = String.Empty;
                            Fila["SF"] = String.Empty;
                            Fila["PROGRAMA"] = String.Empty;
                            Fila["UR"] = String.Empty;
                            Fila["PARTIDA"] = String.Empty;
                            Fila["CONCEPTO"] = "TOTAL PRESUPUESTO";
                            Fila["PRESUPUESTO_APROBADO"] = Apr;
                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Fila[i.ToString().Trim() + "a. MODIFICACION"] = Dt_Psp.AsEnumerable().Sum(x => x.Field<Double>(i.ToString().Trim() + "a. MODIFICACION"));
                                }
                            }
                            Fila["AMPLIACION"] = Amp;
                            Fila["REDUCCION"] = Red;
                            Fila["INCREMENTO"] = Inc;
                            Fila["MODIFICADO"] = Mod;
                            Fila["ENERO"] = Ene;
                            Fila["FEBRERO"] = Feb;
                            Fila["MARZO"] = Mar;
                            Fila["ABRIL"] = Abr;
                            Fila["MAYO"] = May;
                            Fila["JUNIO"] = Jun;
                            Fila["JULIO"] = Jul;
                            Fila["AGOSTO"] = Ago;
                            Fila["SEPTIEMBRE"] = Sep;
                            Fila["OCTUBRE"] = Oct;
                            Fila["NOVIEMBRE"] = Nov;
                            Fila["DICIEMBRE"] = Dic;
                            Fila["MODIFICADO_TOTAL"] = Mod;
                            Fila["TIPO"] = "TOTAL";
                            Dt_Analitico.Rows.Add(Fila);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Analitico;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Unificar_Dt_Analitico_Modificaciones
            ///DESCRIPCIÓN          : metodo para unificar los datos de las modificaciones y el presupuesto
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Unificar_Dt_Analitico_Modificaciones(DataTable Dt_Analitico,  DataTable Dt_Mod)
            {
                DataTable Dt_Analitico_Mod = new DataTable();
                Int32 No_Mod = 0;
                int i = 0;
                DataTable Dt_Modif = new DataTable();
                Double Modificado = 0.00;

                try
                {
                    if (Dt_Mod != null)
                    {
                        if (Dt_Mod.Rows.Count > 0)
                        { 
                           //verificamos cuantas columnas de modificado hay
                            No_Mod = Convert.ToInt32(String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()) ? "0" : Cmb_Tipo_Reporte.SelectedItem.Value.Trim());

                            if (No_Mod > 1)
                            {
                                for (i = 1; i < No_Mod; i++)
                                {
                                    Dt_Analitico.Columns.Add(i.ToString().Trim() + "a. MODIFICACION", System.Type.GetType("System.Double"));
                                }

                                //recorremos el dt analititico para agregrar los nuevos datos a las columnas
                                foreach (DataRow Dr in Dt_Analitico.Rows)
                                {
                                    //obtenemos el 
                                    Dt_Modif = new DataTable();
                                    Dt_Modif = (from Fila in Dt_Mod.AsEnumerable()
                                                where Fila.Field<String>("CODIGO_PROGRAMATICO").Trim() == Dr["CODIGO_PROGRAMATICO"].ToString().Trim()
                                                select Fila).AsDataView().ToTable();

                                    //agregamos los datos a la modificacion
                                    if (Dt_Modif != null)
                                    {
                                        if (Dt_Modif.Rows.Count > 0)
                                        {
                                            for (i = 0; i <= Dt_Modif.Rows.Count - 1; i++)
                                            {
                                                Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dt_Modif.Rows[i]["MODIFICADO"].ToString().Trim()) ? "0" : Dt_Modif.Rows[i]["MODIFICADO"]);
                                                Dr[(i+1).ToString().Trim() + "a. MODIFICACION"] = Modificado;
                                            }
                                        }
                                        else
                                        {
                                            for (i = 1; i < No_Mod; i++)
                                            {
                                                Dr[i.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        for (i = 1; i < No_Mod; i++)
                                        {
                                            Dr[i.ToString().Trim() + "a. MODIFICACION"] = 0.00;
                                        }
                                    }
                                }
                            }

                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Analitico;
            }
        #endregion

        #region (Metodos Combos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Año
            ///DESCRIPCIÓN: Cargara los años de los registros que se encuentran en el presupuesto
            ///PARAMETROS: 
            ///CREO:        Hugo Enrique Ramírez Aguilera
            ///FECHA_CREO:  07/Noviembre/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Cargar_Combo_Año()
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Rs_Año = new Cls_Rpt_Presupuesto_Egresos_Negocio();

                try
                {
                    System.Data.DataTable Dt_Año = Rs_Año.Consultar_Presupuesto_Año();
                    Cmb_Anio.Items.Clear();
                    Cmb_Anio.DataSource = Dt_Año;
                    Cmb_Anio.DataValueField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
                    Cmb_Anio.DataTextField = Ope_Psp_Presupuesto_Aprobado.Campo_Anio;
                    Cmb_Anio.DataBind();
                    Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Anio.SelectedIndex = 0;

                    Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(String.Format("{0:yyyy}",DateTime.Now)));
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    //throw new Exception("Error al generar el reporte de las Dependencias. Error: [" + Ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
            ///DESCRIPCIÓN          : Llena el combo de unidad responsable
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_UR()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();
                
                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim())){
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    }
                    else {
                        Negocios.P_Anio = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim())){
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else{
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Programa_ID = String.Empty;
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = String.Empty;
                    Negocios.P_Gpo_Dependencia_ID = String.Empty;
                    Negocios.P_Area_Funcional_ID = String.Empty;
                    Negocios.P_Dependencia_ID = String.Empty;
                    Dt_Datos = Negocios.Consultar_UR();

                    //para destino
                    Cmb_UR.Items.Clear();
                    Cmb_UR.DataSource = Dt_Datos;
                    Cmb_UR.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                    Cmb_UR.DataTextField = "CLAVE_NOMBRE";
                    Cmb_UR.DataBind();
                    Cmb_UR.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
            ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_FF()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim())){
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    }
                    else{
                        Negocios.P_Anio = String.Empty;
                    }

                    Negocios.P_Tipo_Usuario = String.Empty;
                    Negocios.P_Gpo_Dependencia_ID = String.Empty;
                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Presupuesto = "EGR";

                    Dt_Datos = Negocios.Consultar_FF();

                    //para destino
                    Cmb_FF.Items.Clear();
                    Cmb_FF.DataSource = Dt_Datos;
                    Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                    Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                    Cmb_FF.DataBind();
                    Cmb_FF.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
            ///DESCRIPCIÓN          : Llena el combo de proyectos programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Programas()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Anio = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    Negocios.P_Dependencia_ID = String.Empty;

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = String.Empty;
                    Negocios.P_Gpo_Dependencia_ID = String.Empty;
                    Negocios.P_Area_Funcional_ID = String.Empty;
                    Negocios.P_Tipo_Presupuesto = "EGR";
                    Dt_Datos = Negocios.Consultar_Programa();

                    //para destino
                    Cmb_PP.Items.Clear();
                    Cmb_PP.DataSource = Dt_Datos;
                    Cmb_PP.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                    Cmb_PP.DataTextField = "CLAVE_NOMBRE";
                    Cmb_PP.DataBind();
                    Cmb_PP.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
            ///DESCRIPCIÓN          : Llena el combo de partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Partidas()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Anio = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Dependencia_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Programa_ID = String.Empty;
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = String.Empty;
                    Negocios.P_Gpo_Dependencia_ID = String.Empty;
                    Negocios.P_Area_Funcional_ID = String.Empty;
                    Negocios.P_Capitulo_ID = String.Empty;
                    Negocios.P_Concepto_ID = String.Empty;
                    Negocios.P_Partida_Generica_ID = String.Empty;

                    Dt_Datos = Negocios.Consultar_Partidas();

                    //para destino
                    Cmb_Partida.Items.Clear();
                    Cmb_Partida.DataSource = Dt_Datos;
                    Cmb_Partida.DataValueField = Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                    Cmb_Partida.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Partida.DataBind();
                    Cmb_Partida.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo_Reporte
            ///DESCRIPCIÓN          : Llena el combo de tipo de reporte
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Tipo_Reporte()
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Anio = String.Empty;
                    }

                    Dt_Datos = Negocios.Consultar_Tipo_Reporte();

                    //para destino
                    Cmb_Tipo_Reporte.Items.Clear();
                    Cmb_Tipo_Reporte.DataSource = Dt_Datos;
                    Cmb_Tipo_Reporte.DataValueField = Ope_Psp_Presupuesto_Egr_Esp.Campo_No_Modificacion;
                    Cmb_Tipo_Reporte.DataTextField = "MODIFICACION";
                    Cmb_Tipo_Reporte.DataBind();
                    Cmb_Tipo_Reporte.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Tipo_Reporte.Items.Insert(1, new ListItem("CALENDARIZADO", "CALENDARIZADO"));
                    Cmb_Tipo_Reporte.Items.Insert(2, new ListItem("APROBADO", "APROBADO"));
                    Cmb_Tipo_Reporte.Items.Insert(3, new ListItem("ACTUAL", "ACTUAL"));

                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid
            ///DESCRIPCIÓN          : Llena el grid de los registros del reporte
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 07/Junio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Grid(String UR, String FF, String PP, String P)
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Negocio = new Cls_Rpt_Presupuesto_Egresos_Negocio();//instancia con capa de negocios
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Registros_Incremento = new DataTable();
                String Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();
                String Anio = Cmb_Anio.SelectedItem.Value.Trim();
                DataTable Dt_Importes = new DataTable();
                DataTable Dt_Modificaciones = new DataTable();
                String Clasificacion_Adm = String.Empty;

                try
                {
                    Clasificacion_Adm = Negocio.Consultar_Clasificacion_Administrativa();

                    Negocio.P_Anio = Anio;
                    Negocio.P_Dependencia_ID = UR;
                    Negocio.P_Fte_Financiamiento_ID = FF;
                    Negocio.P_Partida_ID = P;
                    Negocio.P_Programa_ID = PP;

                    if (Tipo_Reporte.Trim().Equals("CALENDARIZADO"))
                    {
                        Negocio.P_Tipo_Reporte = "'GENERADO'";
                        Dt_Registros = Negocio.Consultar_Datos_Analitico_CAL();
                        Dt_Registros.Columns.Add("APROBADO", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("AMPLIACION", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("REDUCCION", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("MODIFICADO", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            Dr["APROBADO"] = Dr["TOTAL_PSP"];
                            Dr["AMPLIACION"] = 0.00;
                            Dr["REDUCCION"] = 0.00;
                            Dr["MODIFICADO"] = Dr["TOTAL_PSP"];
                        }
                    }
                    else if (Tipo_Reporte.Trim().Equals("APROBADO"))
                    {
                        Negocio.P_Tipo_Reporte = "'AUTORIZADO', 'CARGADO'";
                        Dt_Registros = Negocio.Consultar_Datos_Analitico_CAL();
                        Dt_Registros.Columns.Add("APROBADO", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("AMPLIACION", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("REDUCCION", System.Type.GetType("System.Double"));
                        Dt_Registros.Columns.Add("MODIFICADO", System.Type.GetType("System.Double"));

                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            Dr["APROBADO"] = Dr["TOTAL_PSP"];
                            Dr["AMPLIACION"] = 0.00;
                            Dr["REDUCCION"] = 0.00;
                            Dr["MODIFICADO"] = Dr["TOTAL_PSP"];
                        }
                    }
                    else if (Tipo_Reporte.Trim().Equals("ACTUAL"))
                    {
                        Dt_Registros = Negocio.Consultar_Datos_Analitico_PSP();
                        Dt_Registros.Columns.Add("APROBADO", System.Type.GetType("System.Double"));
                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            Dr["APROBADO"] = Dr["PRESUPUESTO_APROBADO"];
                        }
                    }
                    else
                    {
                        Negocio.P_Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();
                        Negocio.P_Tipo_Operacion = "'TRASPASO', 'REDUCCION'";
                        Dt_Registros = Negocio.Consultar_Datos_Analitico_MOD();
                        Negocio.P_Tipo_Operacion = "'AMPLIACION'";
                        Dt_Registros_Incremento = Negocio.Consultar_Datos_Analitico_MOD();
                        Dt_Importes = Negocio.Consultar_Importes_Datos_Analitico_MOD();
                        Dt_Modificaciones = Negocio.Consultar_Modificaciones_Analitico_MOD();
                        Dt_Registros = Unificar_Dt_Analitico(Dt_Registros, Dt_Registros_Incremento, Dt_Importes, Dt_Modificaciones);
                        Dt_Registros = Agrupar_Analitico_Detallado(Dt_Registros, Clasificacion_Adm);

                        Dt_Registros.Columns.Add("APROBADO", System.Type.GetType("System.Double"));
                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            Dr["APROBADO"] = Dr["PRESUPUESTO_APROBADO"];
                        }
                    }

                    Grid_Registros.Columns[6].Visible = true;
                    Grid_Registros.DataSource = Dt_Registros;
                    Grid_Registros.DataBind();
                    Grid_Registros.Columns[6].Visible = false;
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion
    #endregion

    #region(Eventos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
        ///PARAMETROS: 
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  07/Noviembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
           
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Excel_Click
        ///DESCRIPCIÓN: Manda llamar los datos para cargarlos en el reporte de excel
        ///PARAMETROS:  
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  05/Diciembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e) 
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String Tipo_Reporte = String.Empty;
            String UR = String.Empty;
            String FF = String.Empty;
            String PP = String.Empty;
            String P = String.Empty;
            String Tipo = String.Empty;
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;

            try 
            {
                if (!String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()))
                {
                    if (Cmb_Anio.SelectedIndex > 0)
                    {
                        if ((Chk_Dependencia.Checked == true) || (Chk_Unidad_Responsable.Checked == true)
                            || (Chk_Programa.Checked == true) || (Chk_Partida.Checked == true)
                            || (Chk_Fte_Financiamiento.Checked == true)
                            || (Chk_Analitico.Checked == true) || (Chk_Analitico_Detallado.Checked == true))
                        {
                            Lbl_Mensaje_Error.Visible = false;
                            Lbl_Mensaje_Error.Text = "";
                            Tipo_Reporte = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                            if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                            {
                                UR = Cmb_UR.SelectedItem.Value.Trim();
                            }
                            if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                            {
                                PP = Cmb_PP.SelectedItem.Value.Trim();
                            }
                            if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                            {
                                FF = Cmb_FF.SelectedItem.Value.Trim();
                            }
                            if (!String.IsNullOrEmpty(Cmb_Partida.SelectedItem.Value.Trim()))
                            {
                                P = Cmb_Partida.SelectedItem.Value.Trim();
                            }

                            Llenar_Grid(UR, FF, PP, P);

                            //obtenemos los datos de la dependencia y del grupo dependencia de la persona que obtendra  el reporte
                            Dependencia = Obtener_Gpo_Dependencia("UR");
                            Gpo_Dependencia = Obtener_Gpo_Dependencia("GPO_DEP");

                            if (Tipo_Reporte.Trim().Equals("CALENDARIZADO"))
                            {
                                Tipo = "'GENERADO','AUTORIZADO','CARGADO'";
                                Generar_Reporte_CAL(UR, FF, PP, P, Tipo, Dependencia, Gpo_Dependencia);
                            }
                            else if (Tipo_Reporte.Trim().Equals("APROBADO"))
                            {
                                Tipo = "'AUTORIZADO', 'CARGADO'";
                                Generar_Reporte_CAL(UR, FF, PP, P, Tipo, Dependencia, Gpo_Dependencia);
                            }
                            else if (Tipo_Reporte.Trim().Equals("ACTUAL"))
                            {
                                Generar_Reporte_PSP(UR, FF, PP, P, Dependencia, Gpo_Dependencia);
                            }
                            else 
                            {
                                Tipo = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();
                                Generar_Reporte_MOD(UR, FF, PP, P, Tipo, Dependencia, Gpo_Dependencia);
                            }
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Seleccione alguno o varios de los reportes de la lista";
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione el Año";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione el tipo de reporte";
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de anios
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_Tipo_Reporte();
                Llenar_Combo_FF();
                Llenar_Combo_UR();
                Llenar_Combo_Programas();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_FF_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de fuente de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_FF_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_UR();
                Llenar_Combo_Programas();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_UR_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de dependencias
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_UR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Programas_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de proyectos programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Programas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
        ///PARAMETROS: 
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  07/Noviembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Consultar_Click(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String UR = String.Empty;
            String FF = String.Empty;
            String PP = String.Empty;
            String P = String.Empty;
            String Tipo = String.Empty;

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Tipo_Reporte.SelectedItem.Value.Trim()))
                {
                    if (Cmb_Anio.SelectedIndex > 0)
                    {
                        if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                        {
                            UR = Cmb_UR.SelectedItem.Value.Trim();
                        }
                        if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                        {
                            PP = Cmb_PP.SelectedItem.Value.Trim();
                        }
                        if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                        {
                            FF = Cmb_FF.SelectedItem.Value.Trim();
                        }
                        if (!String.IsNullOrEmpty(Cmb_Partida.SelectedItem.Value.Trim()))
                        {
                            P = Cmb_Partida.SelectedItem.Value.Trim();
                        }

                        Llenar_Grid(UR, FF, PP, P);
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione el Año";
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione el tipo de reporte";
                }
            }
            catch (Exception)
            {
                
                throw;
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double Tot = 0.00;
            Double Red = 0.00;
            Double Amp = 0.00;
            Double Mod = 0.00;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Tot = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[2].Text.Trim()) ? "0" : e.Row.Cells[2].Text.Trim());
                    Amp = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) ? "0" : e.Row.Cells[3].Text.Trim());
                    Red = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[4].Text.Trim()) ? "0" : e.Row.Cells[4].Text.Trim());
                    Mod = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[5].Text.Trim()) ? "0" : e.Row.Cells[5].Text.Trim());

                    if (e.Row.Cells[6].Text.Trim().Equals("TOTAL_PROGRAMA"))
                    {
                        e.Row.Style.Add("background-color", "#FFCC99");
                        e.Row.Style.Add("color", "black");
                    }
                    if (e.Row.Cells[6].Text.Trim().Equals("TOTAL"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                    }
                    if (e.Row.Cells[6].Text.Trim().Equals("TOTAL_UR"))
                    {
                        e.Row.Style.Add("background-color", "#FFCC66");
                        e.Row.Style.Add("color", "black");
                    }
                    if (e.Row.Cells[6].Text.Trim().Equals("TOTAL_GPO_DEP"))
                    {
                        e.Row.Style.Add("background-color", "#FFCC33");
                        e.Row.Style.Add("color", "black");
                    }
                    if (Tot < 0)
                        e.Row.Cells[2].Style.Add("color", "red");

                    if (Amp < 0)
                        e.Row.Cells[3].Style.Add("color", "red");

                    if (Red < 0)
                        e.Row.Cells[4].Style.Add("color", "red");

                    if (Mod < 0)
                        e.Row.Cells[5].Style.Add("color", "red");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }
    #endregion
}
