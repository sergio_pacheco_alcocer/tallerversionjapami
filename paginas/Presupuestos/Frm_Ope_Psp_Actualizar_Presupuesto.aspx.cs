﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Data.OleDb;
using System.Data.Common;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Actualizar_Presupuesto : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
       
    }
    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public static DataSet Leer_Excel(String DirPath, String SqlExcel)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
        String Rta = DirPath;
        string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";



        if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
        }
        else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
        {
            sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Rta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
            //sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
            //        "Data Source=" + Rta + ";" +
            //        "Extended Properties=Excel 8.0;";
        }

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(SqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        return DS;
    }

    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid


    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    protected void AFU_Archivo_Excel_UploadedComplete(object sender, EventArgs e)
    {

        Btn_Imagen_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Session["Dt_Productos_Excel"] = null;

        if (AFU_Archivo_Excel.HasFile)
        {
            bool xls = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xls");
            bool xlsx = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xlsx");
            string currentDateTime = String.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
            string fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo_Excel.FileName).Replace(" ", "_");



            if (xls || xlsx)
            {
                AFU_Archivo_Excel.SaveAs(@"C:/Ajustes/" + fileNameOnServer);
                String path_Excel = "C:/Ajustes/" + fileNameOnServer;
                String SQL = "Select * From [Presupuesto$]";

                DataSet Ds_Presupuesto = Leer_Excel(path_Excel, SQL);

                Session["Dt_Presupuestos"] = Ds_Presupuesto.Tables[0];
               


            }
            else
            {
                Lbl_Mensaje_Error.Text = "El formato del archivo no es valido";
                Btn_Imagen_Error.Visible = true;

            }
        }
        else
        {
            Lbl_Mensaje_Error.Text = "Es necesario seleccionar un archivo";
            Btn_Imagen_Error.Visible = true;

        }
        //Div_Listado_Ajustes.Visible = false;
    }

    protected void Btn_Cargar_Archivo_Click(object sender, ImageClickEventArgs e)
    {
        ScriptManager.RegisterStartupScript(this, this.GetType(), "W",
              "window.open('" + "../Presupuestos/Frm_Ope_Psp_Vista_Presupuesto.aspx" + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        
    }

    #endregion
   
}
