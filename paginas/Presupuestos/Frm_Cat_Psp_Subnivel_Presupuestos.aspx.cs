﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Subniveles_Presupuesto.Negocio;
using JAPAMI.SAP_Partidas_Especificas;
using JAPAMI.SAP_Partidas_Especificas.Datos;
using System.Data;


public partial class paginas_Presupuestos_Frm_Cat_Psp_Subnivel_Presupuestos : System.Web.UI.Page
{
    #region Page_Load
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Metodo de inicio de la pagina
    ///PARAMETROS           :
    ///CREO                 : Luis Daniel Guzmàn Malagòn
    ///FECHA_CREO           : 12/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if(Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))  Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        try
        {
            if (!IsPostBack)
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Sub_Psp_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }

        }catch(Exception Ex){
            
        }

    }
    #endregion 

    #region Grid
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_SubNIvel_Presupuestos
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los SubNivel_Presupuestos
    ///PARAMETROS           :
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Grid_SubNivel_Presupuestos()
    {
        //conexion con la capa de negocios
        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio(); 
        DataTable Dt_SubNivel_Presupuestos = new DataTable();

        try
        {
            Dt_SubNivel_Presupuestos = Negocio.Consultar_SubNivel_Presupuestos_Para_GridView();
            Grid_Subniveles_Presupuestos.Columns[10].Visible = true;
            if (Dt_SubNivel_Presupuestos != null)
            {
                //Hf_Partida_Id.Value = Dt_SubNivel_Presupuestos.Rows[0][1].ToString().Trim();
                
                Grid_Subniveles_Presupuestos.DataSource = Dt_SubNivel_Presupuestos;
                Grid_Subniveles_Presupuestos.DataBind();
            }
            else
            {
                Grid_Subniveles_Presupuestos.DataSource = new DataTable();
            }
            Grid_Subniveles_Presupuestos.Columns[10].Visible = false;
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla SubNivel_Presupuestos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Buscar_SubNivel_Presupuestos
    ///DESCRIPCIÓN          : Metodo para buscar 
    ///PARAMETROS           1 Texto_Buscar: texto que buscaremos en el grid
    ///                     2 Dt_Datos: tabla de donde buscaremos los datos
    ///                     3 Tbl_Datos: Grid donde realizaremos la busqueda
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public static void Buscar_SubNivel_Presupuestos(String Texto_Buscar, DataTable Dt_Datos, GridView Tbl_Datos)
    {
        //Variable que almacena una vista que obtendra a partir de la búsqueda.
        DataView Dv_Datos = null;
        //Variable que almacenara la expresion de búsqueda.
        String Expresion_Busqueda = String.Empty;

        try
        {
            //Creamos el objeto que almacenara una vista de la tabla de roles.
            Dv_Datos = new DataView(Dt_Datos);
            
            Expresion_Busqueda = String.Format("{0} '%{1}%'", Tbl_Datos.SortExpression, Texto_Buscar);

            Dv_Datos.RowFilter =  Cat_Psp_SubNivel_Presupuestos.Campo_Descripcion + " like " + Expresion_Busqueda;
            //Dv_Datos.RowFilter = Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " = " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal + " like " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + "UR" + " like " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + Cat_Psp_SubNivel_Presupuestos.Campo_Estatus + " like " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + "FTE_FINANCIAMIENTO" + " like " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + "PROGRAMA" + " like " + Expresion_Busqueda;
            Dv_Datos.RowFilter += " Or " + "PARTIDA" + " like " + Expresion_Busqueda;
            
            int x = 0;
            if(int.TryParse(Texto_Buscar, out x))
                Dv_Datos.RowFilter += " Or " + Cat_Psp_SubNivel_Presupuestos.Campo_Anio + " = " + Int32.Parse(Texto_Buscar);    
            
            Tbl_Datos.DataSource = Dv_Datos;
            Tbl_Datos.DataBind();
            
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al ejecutar la busqueda de subnivel presupuestos. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    #region Metodos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Sub_Psp_Inicio
            ///DESCRIPCIÓN          : Metodo para habilitar el estado de los botones
            ///PARAMETROS           : 
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Sub_Psp_Inicio() 
            {
                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Limpiar_Forma();
                    Habilitar_Forma(false);
                    Estado_Botones("Inicial");
                    Llenar_Combo_Estatus();
                    Llenar_Combo_UR();
                    Llenar_Combo_Capitulos();
                    Llenar_Grid_SubNivel_Presupuestos();
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de subnivel presupuestos. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Nombre_Partida 
            ///DESCRIPCIÓN          : Metodo para llenar El combo que mostrará el nombre de las partidas registradas
            ///PARAMETROS           :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 13/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            ///
            public void Llenar_Combo_Nombre_Partida()
            {
                DataTable Dt_Partida = new DataTable();

                Cmb_Nombre_Partida.Items.Clear();
                Dt_Partida = Cls_Cat_SAP_Partidas_Especificas_Datos.consulta_Nombre_Partida();
                int contador = 0;

                if (Dt_Partida != null)
                {
                    Cmb_Nombre_Partida.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

                    foreach (DataRow row in Dt_Partida.Rows)
                    {
                        Cmb_Nombre_Partida.Items.Insert((contador + 1),
                            new ListItem(Dt_Partida.Rows[contador][1].ToString().Trim(),
                                Dt_Partida.Rows[contador][0].ToString().Trim()));
                        
                        contador++;
                    }
                    //Cmb_Nombre_Partida.SelectedIndex = -1;
                }
                else
                    //Mostar_Limpiar_Error("aaaa","partida null",true);
                    Cmb_Nombre_Partida.SelectedIndex = -1;

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Errores 
            ///DESCRIPCIÓN          : Metodo para limpiar las etiquetas que muestran los errores
            ///PARAMETROS           :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
            {
                Lbl_Encabezado_Error.Text = Encabezado_Error;
                Lbl_Mensaje_Error.Text = Mensaje_Error;
                Lbl_Mensaje_Error.Visible = Mostrar;
                Td_Error.Visible = Mostrar;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
            ///DESCRIPCIÓN          : Metodo para limpiar los controles
            ///PARAMETROS           :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus 
            ///FECHA_MODIFICO       : 17/Agosto/2012
            ///CAUSA_MODIFICACIÓN   : Se agregaron campos al formulario
            ///*******************************************************************************
            private void Limpiar_Forma()
            {
               
                //Txt_Subnivel_Presupuestal.Text = String.Empty;
                Cmb_Programa.SelectedIndex = -1;
                Cmb_FTE_Financiamiento.SelectedIndex = -1;
                Cmb_Capitulo.SelectedIndex = -1;
                Cmb_Unidad_responsable.SelectedIndex = -1;
                Cmb_Nombre_Partida.SelectedIndex = -1;
                Txt_Descripcion_Subnivel.Text = String.Empty;
                Txt_Anio.Text = String.Empty;
                Cmb_Estatus.SelectedIndex = -1;
                Txt_Busqueda.Text = String.Empty;
                Txt_Clave_Subnivel_Presupuestal.Text = String.Empty;
                Txt_Codigo_Programatico.Text = String.Empty;
                
                //Hf_Clave.Value = String.Empty;
                //Hf_Partida_Id.Value = String.Empty;
                //Hf_Anio.Value = String.Empty;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
            ///PARAMETROS           1: Estatus: true o false para habilitar los controles
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 17/Agosto/2012
            ///CAUSA_MODIFICACIÓN   : Se agregaron campos al formulario
            ///*******************************************************************************
            private void Habilitar_Forma(Boolean Estatus)
            {
                //Txt_Partida_ID.Enabled = false;
                //Txt_Clave.Enabled = !Estatus;
                Txt_Codigo_Programatico.Enabled = false;
                Cmb_Nombre_Partida.Enabled = Estatus;
                Txt_Descripcion_Subnivel.Enabled = Estatus;
                Txt_Descripcion_Subnivel.ReadOnly = false;
                Txt_Anio.Enabled = Estatus;
                Cmb_Estatus.Enabled = Estatus;
                Grid_Subniveles_Presupuestos.Enabled = !Estatus;
                Txt_Busqueda.Enabled = !Estatus;
                Btn_Buscar.Enabled = !Estatus;
                Cmb_Programa.Enabled = Estatus;
                Cmb_Unidad_responsable.Enabled = Estatus;
                Cmb_FTE_Financiamiento.Enabled = Estatus;
                Cmb_Capitulo.Enabled = Estatus;
                Txt_Clave_Subnivel_Presupuestal.Enabled = Estatus;
                
            }
        
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estado_Botones
            ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
            ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Estado_Botones(String Estado)
            {
                switch (Estado)
                {
                    case "Inicial":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Enabled = true;
                        Btn_Eliminar.Visible = true;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        break;
                    case "Nuevo":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Guardar";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "Modificar":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                }//fin del switch
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Datos
            ///DESCRIPCIÓN          : metodo para validar los datos del formulario
            ///PARAMETROS           :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 13/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Boolean Validar_Datos()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_SubNivel_Presupuestos = new DataTable();
                String Mensaje_Encabezado = "Es necesario: ";
                String Mensaje_Error = String.Empty;
                Boolean Datos_Validos = true;

                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);

                    if (String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Introducir la clave del subnivel presupuestal. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Unidad_responsable.SelectedIndex <= 0) 
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar una unidad responsable. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_FTE_Financiamiento.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar una fuente de financiamiento. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Programa.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un programa. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Capitulo.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un Capitulo  <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Nombre_Partida.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar una partida  <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Estatus.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un estatus. <br />";
                        Datos_Validos = false;
                    }
                    if (String.IsNullOrEmpty(Txt_Descripcion_Subnivel.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir una descripción. <br />";
                        Datos_Validos = false;
                    }

                    if (!Datos_Validos)
                    {
                        Mostar_Limpiar_Error(Mensaje_Encabezado, Mensaje_Error, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al validar los datos. Error[" + Ex.Message + "]", true);
                }
                return Datos_Validos;
            }



            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
            ///DESCRIPCIÓN          : Metodo para llenar el combo de estatus
            ///PARAMETROS           :
            ///CREO                 : Luis Daniel Guzmán Malagón
            ///FECHA_CREO           : 12/Julio/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_Estatus()
            {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("ACTIVO", "ACTIVO"));
                Cmb_Estatus.Items.Insert(2, new ListItem("INACTIVO", "INACTIVO"));
                Cmb_Estatus.DataBind();
                Cmb_Estatus.SelectedIndex = -1;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
            ///DESCRIPCIÓN          : Metodo para llenar el combo de unidades responsables
            ///PARAMETROS           :
            ///CREO                 : Jennyer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_UR()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_UR = new DataTable();
                Cmb_Unidad_responsable.Items.Clear();
                Dt_UR = Sub_Pres_Negocio.Consultar_Unidades_Responsables();
                if (Dt_UR != null && Dt_UR.Rows.Count > 0) 
                {
                    Cargar_Combos(Cmb_Unidad_responsable, Dt_UR, "CLAVE_NOMBRE",
                    Cat_Dependencias.Campo_Dependencia_ID, 0);//Carga el combo de sindicatos.
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programa
            ///DESCRIPCIÓN          : Metodo para llenar el combo de Programas
            ///PARAMETROS           :
            ///CREO                 : Jennyer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_Programa()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_Programas = new DataTable();
                Cmb_Programa.Items.Clear();
                Sub_Pres_Negocio.P_Dependencia_ID = Cmb_Unidad_responsable.SelectedValue.ToString();
                Dt_Programas = Sub_Pres_Negocio.Consultar_Programas();
                if (Dt_Programas != null && Dt_Programas.Rows.Count > 0)
                {
                    Cargar_Combos(Cmb_Programa, Dt_Programas, "CLAVE_NOMBRE",
                    Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id, 0);//Carga el combo de sindicatos.
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos
            ///DESCRIPCIÓN          : Metodo para llenar el combo de Capitulos
            ///PARAMETROS           :
            ///CREO                 : Jennyer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_Capitulos()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_Capitulo = new DataTable();
                Cmb_Capitulo.Items.Clear();
                Dt_Capitulo = Sub_Pres_Negocio.Consultar_Capitulos();
                if (Dt_Capitulo != null && Dt_Capitulo.Rows.Count > 0)
                {
                    Cargar_Combos(Cmb_Capitulo, Dt_Capitulo, "NOMBRE",
                     Cat_SAP_Capitulos.Campo_Capitulo_ID, 0);//Carga el combo de sindicatos.
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
            ///DESCRIPCIÓN          : Metodo para llenar el combo de Partidas
            ///PARAMETROS           :
            ///CREO                 : Jennyer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_Partidas()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_Partidas = new DataTable();
                Cmb_Nombre_Partida.Items.Clear();
                Sub_Pres_Negocio.P_Capitulo_ID = Cmb_Capitulo.SelectedValue;
                Dt_Partidas = Sub_Pres_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null && Dt_Partidas.Rows.Count > 0)
                {
                    Cargar_Combos(Cmb_Nombre_Partida, Dt_Partidas, "NOMBRE",
                     Cat_Sap_Partidas_Especificas.Campo_Partida_ID, 0);//Carga el combo de sindicatos.
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FTE_Financiamiento
            ///DESCRIPCIÓN          : Metodo para llenar el combo de fuentes de dinanciamiento
            ///PARAMETROS           :
            ///CREO                 : Jennyer Ivonne Ceja Lemus
            ///FECHA_CREO           : 17/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Llenar_Combo_FTE_Financiamiento()
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                DataTable Dt_FTE_Financiamiento = new DataTable();
                //Sub_Pres_Negocio.P_Dependencia_ID = Cmb_Unidad_responsable.SelectedValue.ToString().Trim();
                Cmb_FTE_Financiamiento.Items.Clear();
                Dt_FTE_Financiamiento = Sub_Pres_Negocio.Consultar_FTE_Financiamiento();
                if (Dt_FTE_Financiamiento != null && Dt_FTE_Financiamiento.Rows.Count > 0)
                {
                    Cargar_Combos(Cmb_FTE_Financiamiento, Dt_FTE_Financiamiento, "CLAVE_NOMBRE",
                     Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID, 0);//Carga el combo de sindicatos.
                }
            }
            /// *************************************************************************************
            /// NOMBRE: Cargar_Combos
            /// DESCRIPCIÓN: Carga cualquier ctlr DropDownList que se le pase como parámetro.
            /// PARÁMETROS: Combo.- Ctlr que se va a cargar.
            ///             Dt_Datos.- Informacion que se cargara en el combo.
            ///             Text.- Texto que será la parte visible de la lista de tipos de nómina.
            ///             Value.- Valor que será el que almacenará el elemnto seleccionado.
            ///             Index.- Indice el cuál será el que se mostrara inicialmente. 
            /// USUARIO CREO: Juan Alberto Hernández Negrete.
            /// FECHA CREO: 3/Mayo/2011 11:12 a.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            private void Cargar_Combos(DropDownList Combo, DataTable Dt_Datos, String Text, String Value, Int32 Index)
            {
                try
                {
                    Combo.DataSource = Dt_Datos;
                    Combo.DataTextField = Text;
                    Combo.DataValueField = Value;
                    Combo.DataBind();
                    Combo.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                    Combo.SelectedIndex = Index;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al cargar el Ctlr de Tipo DropDownList. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Codigo_Programatico
            ///DESCRIPCIÓN          : Se encaragara de generar el codigo programatico
            ///PARAMETROS           : 
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 28/Agosto/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Generar_Codigo_Programatico() 
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                try
                {
                    DataTable Dt_Area_Funcional = new DataTable();
                    String Codigo_Prgramatico = String.Empty;
                    String aux = Cmb_FTE_Financiamiento.SelectedItem.Text;
                    String []Dato = aux.Split('-');

                    //Obtenemos la clave de la fuente de financiamiento
                    if(Dato.Length > 0)
                    Codigo_Prgramatico = Dato[0].ToString().Trim();

                    //Obtenemos la clave del area funcional 
                    Negocio.P_Dependencia_ID = Cmb_Unidad_responsable.SelectedValue.ToString();
                    Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
                    if (Dt_Area_Funcional != null && Dt_Area_Funcional.Rows.Count > 0) 
                    {
                      Codigo_Prgramatico = Codigo_Prgramatico + "-" + Dt_Area_Funcional.Rows[0][Cat_SAP_Area_Funcional.Campo_Clave].ToString();
                    }

                    //Obtenemos la clave del programa 
                    aux = Cmb_Programa.SelectedItem.Text;
                    Dato = aux.Split('-');
                    if (Dato.Length > 0)
                        Codigo_Prgramatico = Codigo_Prgramatico + "-" + Dato[0].ToString().Trim();

                    //Obtenemos la clave de la unidad responsable
                    aux = Cmb_Unidad_responsable.SelectedItem.Text;
                    Dato = aux.Split('-');
                    if (Dato.Length > 0)
                        Codigo_Prgramatico = Codigo_Prgramatico + "-" + Dato[0].ToString().Trim();

                    //Obtenemos la clave de la Partida Epecifica
                    aux = Cmb_Nombre_Partida.SelectedItem.Text;
                    Dato = aux.Split('-');
                    if (Dato.Length > 0)
                        Codigo_Prgramatico = Codigo_Prgramatico + "-" + Dato[0].ToString().Trim();

                    //Obtenemos la clave del subnivel
                    //if(!String.IsNullOrEmpty(Txt_Codigo_Programatico.Text))
                    Codigo_Prgramatico = Codigo_Prgramatico + "-" + Txt_Clave_Subnivel_Presupuestal.Text.Trim();

                    Hf_Codigo_Programatico.Value = Codigo_Prgramatico;
                    Txt_Codigo_Programatico.Text = Codigo_Prgramatico;

                    
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al Generar el codigo programatico. Error: [" + Ex.Message + "]");
                }
            }
    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Evento del boton Guardar
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty,String.Empty,false);
        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();

        try { 
            switch(Btn_Nuevo.ToolTip){
                case "Nuevo":
                    Estado_Botones("Nuevo");
                    Limpiar_Forma();
                    Habilitar_Forma(true);
                    Cmb_Estatus.Enabled = false;
                    Cmb_Estatus.SelectedIndex = 1;
                    Txt_Anio.Text = String.Format("{0:yyyy}",DateTime.Now);
                    break;
                case "Guardar":
                    if (Validar_Datos())
                    {
                        DataTable Dt_AreaFuncional = new DataTable();
                        Negocio.P_Dependencia_ID = Cmb_Unidad_responsable.SelectedValue.ToString();
                        Negocio.P_FTE_Financiamiento_ID = Cmb_FTE_Financiamiento.SelectedValue.ToString();
                        Negocio.P_Programa_ID = Cmb_Programa.SelectedValue.ToString();
                        Negocio.P_Partida_ID = Cmb_Nombre_Partida.SelectedValue.ToString();
                        Negocio.P_Subnivel_Presupuestal = Txt_Clave_Subnivel_Presupuestal.Text.Trim();
                        Negocio.P_Descripcion = Txt_Descripcion_Subnivel.Text.Trim();
                        Negocio.P_Anio = Txt_Anio.Text.Trim();
                        Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                        Negocio.P_Codigo_Programatico = Txt_Codigo_Programatico.Text.Trim();
                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();

                        Dt_AreaFuncional = Negocio.Obtener_Area_Funcional();
                        Negocio.P_Area_Funcional_ID = Dt_AreaFuncional.Rows[0]["AREA_FUNCIONAL_ID"].ToString();

                        if(Negocio.Insertar_SubNivel_Presupuestos())
                        {
                            Sub_Psp_Inicio();
                            Grid_Subniveles_Presupuestos.SelectedIndex = -1;

                            ScriptManager.RegisterClientScriptBlock(this,this.GetType(),"Alta","alert('Operacion Completa');",true);
                        }
                    }
                    break;
            }
        
        }catch(Exception Ex){
            Mostar_Limpiar_Error(string.Empty,"Error al guardar Subnivel Presupuesto. Error["+ Ex.Message+"]",true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Modificarar_Click
    ///DESCRIPCIÓN          : Evento del boton Eliminar 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 14/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        //Variable de conexion con la capa de negocios.
        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
        try
        {
            if (Grid_Subniveles_Presupuestos.SelectedIndex > (-1))
            {
                switch (Btn_Modificar.ToolTip)
                {
                    //Validacion para actualizar un registro y para habilitar los controles que se requieran
                    case "Modificar":
                        Estado_Botones("Modificar");
                        Habilitar_Forma(true);
                        break;
                    case "Actualizar":
                        if (Validar_Datos())
                        {
                            DataTable Dt_AreaFuncional = new DataTable();
                            Negocio.P_Dependencia_ID = Cmb_Unidad_responsable.SelectedValue.ToString();
                            Negocio.P_FTE_Financiamiento_ID = Cmb_FTE_Financiamiento.SelectedValue.ToString();
                            Negocio.P_Programa_ID = Cmb_Programa.SelectedValue.ToString();
                            Negocio.P_Partida_ID = Cmb_Nombre_Partida.SelectedValue.ToString();
                            Negocio.P_Anio = Txt_Anio.Text.Trim();
                            Negocio.P_Descripcion = Txt_Descripcion_Subnivel.Text.Trim();
                            Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                            Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                            Negocio.P_Subnivel_Presupuestal = Txt_Clave_Subnivel_Presupuestal.Text.Trim();
                            Negocio.P_Subnivel_Presupuestal_ID = Hf_Clave.Value.ToString().Trim();
                            Negocio.P_Codigo_Programatico = Txt_Codigo_Programatico.Text.Trim();
                            Dt_AreaFuncional = Negocio.Obtener_Area_Funcional();
                            Negocio.P_Area_Funcional_ID = Dt_AreaFuncional.Rows[0]["AREA_FUNCIONAL_ID"].ToString();

                            if (Negocio.Modificar_SubNivel_Presupuestos())
                            {
                                Sub_Psp_Inicio();
                                Grid_Subniveles_Presupuestos.SelectedIndex = -1;
                                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Actualizar", "alert('Operacion Completa');", true);
                            }
                        }
                        break;
                }//fin del switch
            }
            else
            {
                Mostar_Limpiar_Error("Favor de seleccionar un registro de la tabla", String.Empty, true);
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al modificar Subnivel Presupuesto. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
    ///DESCRIPCIÓN          : Evento del boton Eliminar 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        // objeto de la clase subnivel presupuesto negocio
        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
        //DataTable Dt_Subnivel_Presupuesto = new DataTable();
        try { 
            if(Grid_Subniveles_Presupuestos.SelectedIndex > (-1))
            {
                Negocio.P_Subnivel_Presupuestal_ID = Hf_Clave.Value.ToString().Trim();
                
                if(Negocio.Eliminar_SubNivel_Presupuestos())
                {
                    Sub_Psp_Inicio();
                    Grid_Subniveles_Presupuestos.SelectedIndex = -1;

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(),"Eliminar","alert('Operacion completa: El Estatus cammbio a INACTIVO');",true);
                }
            }
            else
                Mostar_Limpiar_Error("Favor de seleccionar un regisstro de la tabla",String.Empty,true);
        
        }catch(Exception Ex){
            Mostar_Limpiar_Error(String.Empty, "Error al eliminar SubNivel Presupuesto. Error[" + Ex.Message + "]", true);
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton Salir 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        switch(Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Sub_Psp_Inicio();
                Grid_Subniveles_Presupuestos.SelectedIndex = -1;
                break;
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                break;
        }//fin switch
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
    ///DESCRIPCIÓN          : Evento del boton Buscar 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        //Objeto de la clase Subnivel presupuesto negocio
        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
        DataTable Dt_Subnivel_Presupuesto = new DataTable();
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        try {
            if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                Dt_Subnivel_Presupuesto = Negocio.Consultar_SubNivel_Presupuestos_Para_GridView();
                Grid_Subniveles_Presupuestos.Columns[10].Visible = true;
                if (Dt_Subnivel_Presupuesto != null)
                {
                    if (Dt_Subnivel_Presupuesto.Rows.Count > 0)
                    {
                        Buscar_SubNivel_Presupuestos(Txt_Busqueda.Text.Trim(), Dt_Subnivel_Presupuesto, Grid_Subniveles_Presupuestos);
                    }
                }
            }
            else
            {
                Llenar_Grid_SubNivel_Presupuestos();
                Grid_Subniveles_Presupuestos.Columns[10].Visible = false;
            }
        }catch(Exception Ex){
            Mostar_Limpiar_Error(String.Empty, "Error en la busqueda de SubNivel Presupuestos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Subnivel_Presupuesto_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion del grid
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 14/Julio/2012
    ///MODIFICO             :Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       :20/Agosto/2012
    ///CAUSA_MODIFICACIÓN   :Se agregaron campos en la tabla
    ///*******************************************************************************
    protected void Grid_Subniveles_Presupuestos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        String Clave = String.Empty;
        try
        {
            if (Grid_Subniveles_Presupuestos.SelectedIndex > (-1))
            {
                Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Sub_Pres_Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
                Grid_Subniveles_Presupuestos.Columns[10].Visible = true;

                Sub_Pres_Negocio.P_Subnivel_Presupuestal_ID = Grid_Subniveles_Presupuestos.SelectedRow.Cells[10].Text;
                Hf_Clave.Value = Grid_Subniveles_Presupuestos.SelectedRow.Cells[10].Text;
                DataTable Dt_SubNivel = new DataTable();
                Dt_SubNivel = Sub_Pres_Negocio.Consultar_SubNivel_Presupuestos();
                if (Dt_SubNivel != null && Dt_SubNivel.Rows.Count > 0)
                {
                    Cmb_Unidad_responsable.SelectedIndex = Cmb_Unidad_responsable.Items.IndexOf(Cmb_Unidad_responsable.Items.FindByValue(Dt_SubNivel.Rows[0]["DEPENDENCIA_ID"].ToString()));
                    Llenar_Combo_Programa();
                    Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_SubNivel.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString()));
                    Llenar_Combo_FTE_Financiamiento();
                    Cmb_FTE_Financiamiento.SelectedIndex = Cmb_FTE_Financiamiento.Items.IndexOf(Cmb_FTE_Financiamiento.Items.FindByValue(Dt_SubNivel.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString()));
                    Cmb_Capitulo.SelectedIndex = Cmb_Capitulo.Items.IndexOf(Cmb_Capitulo.Items.FindByValue(Dt_SubNivel.Rows[0]["CAPITULO_ID"].ToString()));
                    Llenar_Combo_Partidas();
                    Cmb_Nombre_Partida.SelectedIndex = Cmb_Nombre_Partida.Items.IndexOf(Cmb_Nombre_Partida.Items.FindByValue(Dt_SubNivel.Rows[0]["PARTIDA_ID"].ToString()));
                    Txt_Anio.Text = Dt_SubNivel.Rows[0]["ANIO"].ToString();
                    Txt_Descripcion_Subnivel.Text = Dt_SubNivel.Rows[0]["DESCRIPCION"].ToString();
                    Txt_Clave_Subnivel_Presupuestal.Text = Dt_SubNivel.Rows[0]["SUBNIVEL_PRESUPUESTAL"].ToString();
                    Txt_Codigo_Programatico.Text = Dt_SubNivel.Rows[0]["CODIGO_PROGRAMATICO"].ToString();
                    //cocsulta para llenar los campos faltantes
                    //Limpiar_Forma();
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Dt_SubNivel.Rows[0]["ESTATUS"].ToString()));

                    //Hf_Anio.Value = HttpUtility.HtmlDecode(Grid_Subniveles_Presupuestos.SelectedRow.Cells[4].Text).ToString();

                    Estado_Botones("Inicial");
                    Btn_Eliminar.Enabled = true;
                    Btn_Modificar.Enabled = true;
                    Habilitar_Forma(false);
                }
                Grid_Subniveles_Presupuestos.Columns[10].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error("Error en la busqueda de Subnivel Presupuesto. Error[" + Ex.Message + "]", String.Empty, true);
        }  
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Subnivel_Presupuesto_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 14/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Subnivel_Presupuesto_Sorting(object sender, GridViewSortEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Subniveles = new DataTable();

        try
        {
            Llenar_Grid_SubNivel_Presupuestos();
            Grid_Subniveles_Presupuestos.Columns[10].Visible = true;
            Dt_Subniveles = (DataTable)Grid_Subniveles_Presupuestos.DataSource;
            if (Dt_Subniveles != null)
            {
                DataView Dv_Vista = new DataView(Dt_Subniveles);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }
                Grid_Subniveles_Presupuestos.DataSource = Dv_Vista;
                Grid_Subniveles_Presupuestos.DataBind();
                Grid_Subniveles_Presupuestos.Columns[10].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al ordenar la tabla de Subnivel Presupuesos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Busqueda_TextChanged
    ///DESCRIPCIÓN          : Evento que se realizará al dar enter en la caja de texto busqueda
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 17/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
    {
        Btn_Buscar_Click(sender, new ImageClickEventArgs(1000,100));

        Cls_Cat_Psp_Subnivel_Presupuesto_Negocio Negocio = new Cls_Cat_Psp_Subnivel_Presupuesto_Negocio();
        DataTable Dt_Subnivel_Presupuesto = new DataTable();
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        try
        {
            if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                Dt_Subnivel_Presupuesto = Negocio.Consultar_SubNivel_Presupuestos_Para_GridView();
                if (Dt_Subnivel_Presupuesto != null)
                {
                    if (Dt_Subnivel_Presupuesto.Rows.Count > 0)
                    {
                        Buscar_SubNivel_Presupuestos(Txt_Busqueda.Text.Trim(), Dt_Subnivel_Presupuesto, Grid_Subniveles_Presupuestos);
                    }
                }
            }
            else
                Llenar_Grid_SubNivel_Presupuestos();
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error en la busqueda de SubNivel Presupuestos. Error[" + Ex.Message + "]", true);
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_responsable_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento que se realizará al seleccionar una unidad responsable,
    ///                     : llenara losprogramas
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 17/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Unidad_responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_responsable.SelectedIndex > 0) 
        {
            Llenar_Combo_Programa();
            Llenar_Combo_FTE_Financiamiento();
            if (Cmb_FTE_Financiamiento.SelectedIndex > 0 && Cmb_Programa.SelectedIndex > 0 && Cmb_Nombre_Partida.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text)) 
            {
                Generar_Codigo_Programatico();
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento que se realizará al Seleccionar un capitulo
    ///PARAMETROS           : 
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 17/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Capitulo.SelectedIndex > 0) 
        {
            Llenar_Combo_Partidas();
           
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Txt_Clave_Subnivel_Presupuestal_TextChanged
    ///DESCRIPCIÓN          : Evento que se realizará al terminar de escribir en a caja de texto
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 28/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Txt_Clave_Subnivel_Presupuestal_TextChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_responsable.SelectedIndex > 0 && Cmb_FTE_Financiamiento.SelectedIndex > 0 && Cmb_Programa.SelectedIndex > 0 && Cmb_Nombre_Partida.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text))
        {
            Generar_Codigo_Programatico();
            
            Txt_Descripcion_Subnivel.Focus();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Nombre_Partida_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento que se realizará al Seleccionar una partida
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 28/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Nombre_Partida_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_FTE_Financiamiento.SelectedIndex > 0 && Cmb_Unidad_responsable.SelectedIndex > 0 && Cmb_Programa.SelectedIndex > 0 && Cmb_Nombre_Partida.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text))
        {
            Generar_Codigo_Programatico();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento que se realizará al Seleccionar un programa
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 28/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Programa.SelectedIndex > 0 && Cmb_FTE_Financiamiento.SelectedIndex > 0 && Cmb_Unidad_responsable.SelectedIndex > 0 && Cmb_Nombre_Partida.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text))
        {
            Generar_Codigo_Programatico();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento que se realizará al Seleccionar una fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 28/Agosto/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Cmb_FTE_Financiamiento_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_FTE_Financiamiento.SelectedIndex > 0 && Cmb_Unidad_responsable.SelectedIndex > 0 && Cmb_Programa.SelectedIndex > 0 && Cmb_Nombre_Partida.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Clave_Subnivel_Presupuestal.Text))
        {
            Generar_Codigo_Programatico();
        }
    }
    #endregion


 
}