﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Catalogo_Compras_Proyectos_Programas.Negocio;
using JAPAMI.Capitulos.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Reporte_Movimiento_Presupuestal.Negocio;
using CarlosAg.ExcelXmlWriter;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Movimiento_Presupuestal : System.Web.UI.Page
{
    #region (Variables Locales)
        private const String P_Dt_Movimientos_Presupuestales = "Dt_Movimientos_Presupuestales";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Page_Load) " + ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje de error a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error.
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              17/Abril/2012 17:39
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Informacion.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    private void Estado_Inicial()
    {
        try
        {
            Llena_Combo_Capitulos();
            Llena_Combos_Fuente_Financiamiento();
            Llena_Combos_Partidas();
            Llena_Combos_Programas();
            Llena_Combos_Unidad_Responsable();
            Limpiar_Controles();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combos_Unidad_Responsable
    ///DESCRIPCION:             Llenar los combos de las unidades responsables
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combos_Unidad_Responsable()
    {
        //Declaracion de variables
        Cls_Cat_Dependencias_Negocio Dependencias_Negocio = new Cls_Cat_Dependencias_Negocio(); //variable para la capa de negocio
        DataTable Dt_Dependencias = new DataTable(); //Tabla para las dependencias
       
        try
        {
            //Consulta para las dependencias
            Dt_Dependencias = Dependencias_Negocio.Consulta_Dependencias();

            //Llenar los combos
            Cmb_Unidad_Responsable_Origen.DataSource = Dt_Dependencias;
            Cmb_Unidad_Responsable_Origen.DataTextField = Cat_Dependencias.Campo_Nombre;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Unidad_Responsable_Origen.SelectedIndex = 0;

            Cmb_Unidad_Responsable_Destino.DataSource = Dt_Dependencias;
            Cmb_Unidad_Responsable_Destino.DataTextField = Cat_Dependencias.Campo_Nombre;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Unidad_Responsable_Destino.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combos_Fuente_Financiamiento
    ///DESCRIPCION:             Llenar los combos de las fuentes de financiamiento
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combos_Fuente_Financiamiento()
    {
        //Declaracion de variables
        Cls_Cat_SAP_Fuente_Financiamiento_Negocio Fuente_Financiamiento_Negocio = new Cls_Cat_SAP_Fuente_Financiamiento_Negocio(); //variable para la capa de negocios
        DataTable Dt_Fuente_Financiamiento = new DataTable(); //Tabla para las fuente de financiamiento

        try
        {
            //Consultar las fuentes de financiamiento
            Dt_Fuente_Financiamiento = Fuente_Financiamiento_Negocio.Consulta_Fuente_Financiamiento();

            //Llenar los combos
            Cmb_Fuente_Financiamiento_Origen.DataSource = Dt_Fuente_Financiamiento;
            Cmb_Fuente_Financiamiento_Origen.DataTextField = Cat_SAP_Fuente_Financiamiento.Campo_Descripcion;
            Cmb_Fuente_Financiamiento_Origen.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Cmb_Fuente_Financiamiento_Origen.DataBind();
            Cmb_Fuente_Financiamiento_Origen.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Fuente_Financiamiento_Origen.SelectedIndex = 0;

            Cmb_Fuente_Financiamiento_Destino.DataSource = Dt_Fuente_Financiamiento;
            Cmb_Fuente_Financiamiento_Destino.DataTextField = Cat_SAP_Fuente_Financiamiento.Campo_Descripcion;
            Cmb_Fuente_Financiamiento_Destino.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Cmb_Fuente_Financiamiento_Destino.DataBind();
            Cmb_Fuente_Financiamiento_Destino.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Fuente_Financiamiento_Destino.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combos_Programas
    ///DESCRIPCION:             Llenar los combos de los programas
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combos_Programas()
    {
        //Declaracion de variables
        Cls_Cat_Com_Proyectos_Programas_Negocio Programas_Negocio = new Cls_Cat_Com_Proyectos_Programas_Negocio(); //variable para la capa de negocio
        DataTable Dt_Programas = new DataTable(); //tabla para los programas

        try
        {
            //Ejecutar consulta
            Dt_Programas = Programas_Negocio.Consulta_Programas_Proyectos();

            //Llenar los combos
            Cmb_Programa_Origen.DataSource = Dt_Programas;
            Cmb_Programa_Origen.DataTextField = Cat_Sap_Proyectos_Programas.Campo_Nombre;
            Cmb_Programa_Origen.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa_Origen.DataBind();
            Cmb_Programa_Origen.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Programa_Origen.SelectedIndex = 0;

            Cmb_Programa_Destino.DataSource = Dt_Programas;
            Cmb_Programa_Destino.DataTextField = Cat_Sap_Proyectos_Programas.Campo_Nombre;
            Cmb_Programa_Destino.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
            Cmb_Programa_Destino.DataBind();
            Cmb_Programa_Destino.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Programa_Destino.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combos_Capitulos
    ///DESCRIPCION:             Llenar los combos de los capitulos
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combo_Capitulos()
    {
        //Declaracion de variables
        Cls_Cat_SAP_Capitulos_Negocio Capitulos_Negocio = new Cls_Cat_SAP_Capitulos_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Capitulos = new DataTable(); //Tabla para los capitulos

        try
        {
            //Ejecutar consulta
            Dt_Capitulos = Capitulos_Negocio.Consulta_Capitulos(); 

            //Llenar los combos
            Cmb_Capitulo_Origen.DataSource = Dt_Capitulos;
            Cmb_Capitulo_Origen.DataTextField = Cat_SAP_Capitulos.Campo_Descripcion;
            Cmb_Capitulo_Origen.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
            Cmb_Capitulo_Origen.DataBind();
            Cmb_Capitulo_Origen.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Capitulo_Origen.SelectedIndex = 0;

            Cmb_Capitulo_Destino.DataSource = Dt_Capitulos;
            Cmb_Capitulo_Destino.DataTextField = Cat_SAP_Capitulos.Campo_Descripcion;
            Cmb_Capitulo_Destino.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
            Cmb_Capitulo_Destino.DataBind();
            Cmb_Capitulo_Destino.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Capitulo_Destino.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Combos_Unidad_Partidas
    ///DESCRIPCION:             Llenar los combos de las partidas
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Combos_Partidas()
    {
        //Declaracion de variables
        Cls_Cat_SAP_Partidas_Especificas_Negocio Partidas_Especificas_Negocio = new Cls_Cat_SAP_Partidas_Especificas_Negocio(); //variable de la capa de negocios
        DataTable Dt_Partidas_Especificas = new DataTable(); //Tabla para las partidas especificas

        try
        {
            //Ejecutar consulta
            Dt_Partidas_Especificas = Partidas_Especificas_Negocio.Consulta_Partida_Especifica();
        
            //LLenar los combos
            Cmb_Partida_Origen.DataSource = Dt_Partidas_Especificas;
            Cmb_Partida_Origen.DataTextField = Cat_Sap_Partidas_Especificas.Campo_Nombre;
            Cmb_Partida_Origen.DataValueField = Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Cmb_Partida_Origen.DataBind();
            Cmb_Partida_Origen.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Partida_Origen.SelectedIndex = 0;

            Cmb_Partida_Destino.DataSource = Dt_Partidas_Especificas;
            Cmb_Partida_Destino.DataTextField = Cat_Sap_Partidas_Especificas.Campo_Nombre;
            Cmb_Partida_Destino.DataValueField = Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
            Cmb_Partida_Destino.DataBind();
            Cmb_Partida_Destino.Items.Insert(0, new ListItem("Seleccione", ""));
            Cmb_Partida_Destino.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Limpiar_COntroles
    ///DESCRIPCION:             Limpiar los controles del formulario
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              09/Junio/2012 13:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Cmb_Capitulo_Destino.SelectedIndex = 0;
            Cmb_Capitulo_Origen.SelectedIndex = 0;
            Cmb_Fuente_Financiamiento_Destino.SelectedIndex = 0;
            Cmb_Fuente_Financiamiento_Origen.SelectedIndex = 0;
            Cmb_Partida_Destino.SelectedIndex = 0;
            Cmb_Partida_Origen.SelectedIndex = 0;
            Cmb_Programa_Destino.SelectedIndex = 0;
            Cmb_Programa_Origen.SelectedIndex = 0;
            Cmb_Tipo_Operacion.SelectedIndex = 0;
            Cmb_Unidad_Responsable_Destino.SelectedIndex = 0;
            Cmb_Unidad_Responsable_Origen.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Eliminar_Sesiones
    ///DESCRIPCION:             Eliminar las sesiones que se utilizan en esta página
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              12/Junio/2012 17:30
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            HttpContext.Current.Session.Remove(P_Dt_Movimientos_Presupuestales);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Reporte_Excel
    ///DESCRIPCION:             Llenar un documento de Excel con el resultado de la consulta
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              13/Junio/2012 17:30
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Reporte_Excel()
    {
        //Declaracion de variables
        Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio Reporte_Movimiento_Presupuestal = new Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio(); //Variable para la capa de negocio
        DataTable Dt_Movimiento_Presupuestal = new DataTable();
        int Cont_Elementos = 0; //variable para el contador
        String Ruta_Archivo = String.Empty; //variable para la ruta del archivo
        String Nombre_Archivo = "Reporte_Movimientos_Presupuestales.xls"; //variable para el nombre del archivo
        Workbook book = new Workbook(); //Variable para el libro
        WorksheetStyle style; //Variable para el estilo
        Worksheet sheet; //variable para la hoja
        WorksheetRow row; //variable para el renglon
        WorksheetCell cell; //Variable para la celda
        String Script_js = String.Empty; //variable para el Script de javascript
        String Encabezado = String.Empty; //variable para el encabezado del reporte

        try
        {
            //Asignar porpiedades para la consulta
            if (Cmb_Capitulo_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Destino = Cmb_Capitulo_Destino.SelectedItem.Value;
            }

            if (Cmb_Capitulo_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Origen = Cmb_Capitulo_Origen.SelectedItem.Value;
            }

            if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Destino = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value;
            }

            if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Origen = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value;
            }

            if (Cmb_Partida_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Partida_ID_Destino = Cmb_Partida_Destino.SelectedItem.Value;
            }

            if (Cmb_Partida_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Partida_ID_Origen = Cmb_Partida_Origen.SelectedItem.Value;
            }

            if (Cmb_Programa_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Programa_ID_Destino = Cmb_Programa_Destino.SelectedItem.Value;
            }

            if (Cmb_Programa_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Programa_ID_Origen = Cmb_Programa_Origen.SelectedItem.Value;
            }

            if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Destino = Cmb_Unidad_Responsable_Destino.SelectedItem.Value;
            }

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Origen = Cmb_Unidad_Responsable_Origen.SelectedItem.Value;
            }

            if (Cmb_Tipo_Operacion.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Tipo_Operacion = Cmb_Tipo_Operacion.SelectedItem.Value;
            }

            if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
            {
                Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
                {
                    Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
                }
            }

            //Ejecutar consulta
            Dt_Movimiento_Presupuestal = Reporte_Movimiento_Presupuestal.Consulta_Reporte_Movimiento_Presupuestal();

            //Verificar si la consulta arrojo resultados
            if (Dt_Movimiento_Presupuestal.Rows.Count > 0)
            {
                //Asignar la ruta del archivo
                Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;

                // Especificar qué hoja debe ser abierto y el tamaño de la ventana por defecto
                book.ExcelWorkbook.ActiveSheetIndex = 0;
                book.ExcelWorkbook.WindowTopX = 100;
                book.ExcelWorkbook.WindowTopY = 200;
                book.ExcelWorkbook.WindowHeight = 7000;
                book.ExcelWorkbook.WindowWidth = 8000;

                // Propiedades del documento
                book.Properties.Author = "CONTEL";
                book.Properties.Title = "REPORTE";
                book.Properties.Created = DateTime.Now;

                // Se agrega estilo al libro
                style = book.Styles.Add("HeaderStyle");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 14;
                style.Font.Bold = true;
                style.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                style.Font.Color = "White";
                style.Interior.Color = "Blue";
                style.Interior.Pattern = StyleInteriorPattern.DiagCross;

                // Se Crea el estilo a usar
                style = book.Styles.Add("Default");
                style.Font.FontName = "Tahoma";
                style.Font.Size = 10;

                //Asignar el nombre a la hoja
                sheet = book.Worksheets.Add("Hoja1");

                //Agregar renglon para el encabezado
                row = sheet.Table.Rows.Add();
                row.Index = 0;//Para saltarse Filas

                // Se agrega el encabezado
                row.Cells.Add(new WorksheetCell(Encabezado, "HeaderStyle"));

                //Agregar renglones para separar
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();
                row = sheet.Table.Rows.Add();

                //Encabezado de la tabla
                row.Cells.Add(new WorksheetCell("No Solicitud", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Importe", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Justificacion", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Justificacion Solicitud", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Estatus", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Tipo Operacion", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Codigo Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Area Funcional Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Programa Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Partida Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Unidad Responsable Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Financiamiento Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Capitulo Origen", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Codigo Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Area Funcional Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Programa Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Partida Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Unidad Responsable Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Financiamiento Destino", "HeaderStyle"));
                row.Cells.Add(new WorksheetCell("Capitulo Destino", "HeaderStyle"));

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Movimiento_Presupuestal.Rows.Count; Cont_Elementos++)
                {
                    //Agregar renglon
                    row = sheet.Table.Rows.Add();

                    //Agregar las columnas
                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["No_Solicitud"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["No_Solicitud"].ToString().Trim(), DataType.Number));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Importe"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("0", DataType.Number));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Importe"].ToString().Trim(), DataType.Number));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Justificacion"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Justificacion"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Justificacion_Solicitud"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Justificacion_Solicitud"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Estatus"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Estatus"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Tipo_Operacion"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Tipo_Operacion"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Codigo1"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Codigo1"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Area_Funcional_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Area_Funcional_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Programa_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Programa_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Partida_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Partida_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Unidad_Responsable_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Unidad_Responsable_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Financiamiento_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Financiamiento_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Capitulo_Origen"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Capitulo_Origen"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Codigo2"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Codigo2"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Area_Funcional_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Area_Funcional_Destino"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Programa_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Programa_Destino"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Partida_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Partida_Destino"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Unidad_Responsable_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Unidad_Responsable_Destino"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Financiamiento_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Financiamiento_Destino"].ToString().Trim(), DataType.String));
                    }

                    if (Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Capitulo_Destino"] == DBNull.Value)
                    {
                        row.Cells.Add(new WorksheetCell("", DataType.String));
                    }
                    else
                    {
                        row.Cells.Add(new WorksheetCell(Dt_Movimiento_Presupuestal.Rows[Cont_Elementos]["Capitulo_Destino"].ToString().Trim(), DataType.String));
                    }
                }

                // Se Guarda el archivo                
                book.Save(Ruta_Archivo);
                string script = @"<script type='text/javascript'>alert('Registros Exportados a Excel');</script>";
                ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

                Mostrar_Reporte(Nombre_Archivo, "Excel");
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    private void Llena_Reporte_PDF()
    {
        //Declaracion de variables
        Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio Reporte_Movimiento_Presupuestal = new Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio(); //Variable para la capa de negocio
        DataTable Dt_Movimiento_Presupuestal = new DataTable();
        int Cont_Elementos = 0; //variable para el contador      
        DataRow Renglon; //Renglon para el llenado de las tablas
        DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles del reporte
        DataSet Ds_Reporte = new DataSet(); //Dataset para colocar los datos de la consulta
        Ds_Rpt_Psp_Movimientos_Presupuestales Ds_Rpt_Psp_Movimientos_Presupuestales_src = new Ds_Rpt_Psp_Movimientos_Presupuestales();
        try
        {
            //Asignar porpiedades para la consulta
            if (Cmb_Capitulo_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Destino = Cmb_Capitulo_Destino.SelectedItem.Value;
            }

            if (Cmb_Capitulo_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Origen = Cmb_Capitulo_Origen.SelectedItem.Value;
            }

            if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Destino = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value;
            }

            if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Origen = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value;
            }

            if (Cmb_Partida_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Partida_ID_Destino = Cmb_Partida_Destino.SelectedItem.Value;
            }

            if (Cmb_Partida_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Partida_ID_Origen = Cmb_Partida_Origen.SelectedItem.Value;
            }

            if (Cmb_Programa_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Programa_ID_Destino = Cmb_Programa_Destino.SelectedItem.Value;
            }

            if (Cmb_Programa_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Programa_ID_Origen = Cmb_Programa_Origen.SelectedItem.Value;
            }

            if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Destino = Cmb_Unidad_Responsable_Destino.SelectedItem.Value;
            }

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Origen = Cmb_Unidad_Responsable_Origen.SelectedItem.Value;
            }

            if (Cmb_Tipo_Operacion.SelectedIndex > 0)
            {
                Reporte_Movimiento_Presupuestal.P_Tipo_Operacion = Cmb_Tipo_Operacion.SelectedItem.Value;
            }

            if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
            {
                Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
                {
                    Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
                }
            }

            //Ejecutar consulta
            Dt_Movimiento_Presupuestal = Reporte_Movimiento_Presupuestal.Consulta_Reporte_Movimiento_Presupuestal();

            //verificar si la consulta arrojo resultados
            if (Dt_Movimiento_Presupuestal.Rows.Count > 0)
            {
                //Clonar la tabla
                Dt_Detalles = Dt_Movimiento_Presupuestal.Clone();

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Movimiento_Presupuestal.Rows.Count; Cont_Elementos++)
                {
                    //intanciar el renglon
                    Renglon = Dt_Movimiento_Presupuestal.Rows[Cont_Elementos];

                    //Importar el renglon
                    Dt_Detalles.ImportRow(Renglon);
                }

                //Colocar la tabla en el dataset
                Ds_Reporte.Tables.Add(Dt_Detalles);
                //Ds_Rpt_Psp_Movimientos_Presupuestales
                Generar_Reporte(Ds_Reporte, Ds_Rpt_Psp_Movimientos_Presupuestales_src, "Rpt_Psp_Movimiento_Presupuestal.rpt", "Reporte_Movimiento_Presupuestales.pdf");
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {

        ReportDocument Reporte = new ReportDocument();
        DataRow Renglon; //Renglon para el llenado de las tablas
        int Cont_Elementos; //Variable para el contador

        //Ciclo para el barrido de los detalles
        for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[0].Rows.Count; Cont_Elementos++)
        {
            //Instanciar el renglon
            Renglon = Data_Set_Consulta_DB.Tables[0].Rows[Cont_Elementos];

            //Importar el renglon
            Ds_Reporte.Tables[0].ImportRow(Renglon);
        }

        String File_Path = Server.MapPath("../Rpt/Presupuestos/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        //Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/" + Nombre_PDF;
        Mostrar_Reporte(Nombre_PDF, "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Grid_Movimientos_Presupuestales
    ///DESCRIPCION:             Llenar el grid con la consulta
    ///PARAMETROS:              Pagina: Entero que contiene la pagina a viksualizar
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              13/Junio/2012 12:30
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Grid_Movimientos_Presupuestales(int Pagina)
    {
        //Declaracion de variables
        Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio Reporte_Movimiento_Presupuestal = new Cls_Rpt_Psp_Movimiento_Presupuestal_Negocio(); //Variable para la capa de negocio
        DataTable Dt_Movimiento_Presupuestal = new DataTable();

        try
        {
            //verificar si existe la variable de sesion
            if (HttpContext.Current.Session[P_Dt_Movimientos_Presupuestales] == null)
            {
                //Asignar porpiedades para la consulta
                if (Cmb_Capitulo_Destino.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Destino = Cmb_Capitulo_Destino.SelectedItem.Value;
                }

                if (Cmb_Capitulo_Origen.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Capitulo_ID_Origen = Cmb_Capitulo_Origen.SelectedItem.Value;
                }

                if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Destino = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value;
                }

                if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Fuente_Financiamiento_ID_Origen = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value;
                }

                if (Cmb_Partida_Destino.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Partida_ID_Destino = Cmb_Partida_Destino.SelectedItem.Value;
                }

                if (Cmb_Partida_Origen.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Partida_ID_Origen = Cmb_Partida_Origen.SelectedItem.Value;
                }

                if (Cmb_Programa_Destino.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Programa_ID_Destino = Cmb_Programa_Destino.SelectedItem.Value;
                }

                if (Cmb_Programa_Origen.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Programa_ID_Origen = Cmb_Programa_Origen.SelectedItem.Value;
                }

                if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Destino = Cmb_Unidad_Responsable_Destino.SelectedItem.Value;
                }

                if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Unidad_Responsable_ID_Origen = Cmb_Unidad_Responsable_Origen.SelectedItem.Value;
                }

                if (Cmb_Tipo_Operacion.SelectedIndex > 0)
                {
                    Reporte_Movimiento_Presupuestal.P_Tipo_Operacion = Cmb_Tipo_Operacion.SelectedItem.Value;
                }

                if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
                {
                    Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);

                    if (Txt_Fecha_Inicial.Text.Trim() != String.Empty)
                    {
                        Reporte_Movimiento_Presupuestal.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
                    }
                }

                //Ejecutar consulta
                Dt_Movimiento_Presupuestal = Reporte_Movimiento_Presupuestal.Consulta_Reporte_Movimiento_Presupuestal();
            }
            else
            {
                //Colocar variable de sesion en la tabla
                Dt_Movimiento_Presupuestal = ((DataTable)HttpContext.Current.Session[P_Dt_Movimientos_Presupuestales]);
            }

            //Verificar si la consulta arrojo resultados
            if (Dt_Movimiento_Presupuestal.Rows.Count > 0)
            {
                //Llenar el grid
                Grid_Movimiento_Presupuestal.DataSource = Dt_Movimiento_Presupuestal;

                //verificar si hay pagina
                if (Pagina > -1)
                {
                    Grid_Movimiento_Presupuestal.PageIndex = Pagina;
                }

                Grid_Movimiento_Presupuestal.DataBind();
            }
            else
            {
                Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
                Grid_Movimiento_Presupuestal.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Eventos)
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Llena_Reporte_Excel();
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Llena_Reporte_PDF();
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Imprimir_Click) " + ex.Message, true);
        }        
    }

    protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Elimina_Sesiones();
            Llena_Grid_Movimientos_Presupuestales(-1);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.Message, true);
        }        
    }
    #endregion
}