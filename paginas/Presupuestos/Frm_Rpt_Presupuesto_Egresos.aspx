﻿<%@ Page Language="C#"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Rpt_Presupuesto_Egresos.aspx.cs" Inherits="paginas_Paginas_Presupuestos_Frm_Rpt_Presupuesto_Egresos" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
    <script type="text/javascript">
        function Seleccionar_Todo(Chk)
        {
            if($(Chk).attr('checked')){
                $("input:checkbox[id$=Chk_Dependencia]").attr("checked", true);
                $("input:checkbox[id$=Chk_Unidad_Responsable]").attr("checked", true);
                $("input:checkbox[id$=Chk_Programa]").attr("checked", true);
                $("input:checkbox[id$=Chk_Fte_Financiamiento]").attr("checked", true);
                $("input:checkbox[id$=Chk_Partida]").attr("checked", true);
                $("input:checkbox[id$=Chk_Analitico]").attr("checked", true);
                $("input:checkbox[id$=Chk_Analitico_Detallado]").attr("checked", true);
            }
            else{
                $("input:checkbox[id$=Chk_Dependencia]").attr("checked", false);
                $("input:checkbox[id$=Chk_Unidad_Responsable]").attr("checked", false);
                $("input:checkbox[id$=Chk_Programa]").attr("checked", false);
                $("input:checkbox[id$=Chk_Fte_Financiamiento]").attr("checked", false);
                $("input:checkbox[id$=Chk_Partida]").attr("checked", false);
                $("input:checkbox[id$=Chk_Analitico]").attr("checked", false);
                $("input:checkbox[id$=Chk_Analitico_Detallado]").attr("checked", false);
            }
        }
        
        //<--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server"  >
        <ContentTemplate>  
            
            <asp:UpdateProgress ID="Uprg_Reloj_Checador" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Reporte_Dependencia" style="background-color:#ffffff; width:100%; height:100%;">    
                 <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte Presupuesto</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                
               
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            &nbsp; 
                                            <asp:ImageButton ID="Btn_Reporte_Grupo_Dependencia" runat="server" 
                                                onclick="Btn_Generar_Reporte_Excel_Click"
                                                ToolTip="Generar Reporte (Excel)"   Width="25px" Height = "25px"
                                                 ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png"/> 
                                            &nbsp;
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                    
                                      <td align="right" style="width:41%;">
                                            <asp:Button id="Btn_Consultar" runat="server" Text = "Consultar" CssClass="button_agregar"
                                            OnClick="Btn_Consultar_Click" />
                                      </td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td style="width:25%;text-align:left; cursor:default;" class="button_autorizar2"> 
                            Reporte
                        </td>
                        <td style="width:35%;text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_Tipo_Reporte" runat="server" Width="98%" TabIndex="1"></asp:DropDownList>
                        </td>
                        <td style="width:15%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Año
                        </td>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_Anio" runat="server" Width="98%" TabIndex="2"
                            AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged" ></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Fuente Financiamiento
                        </td>
                        <td colspan ="3" style="text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_FF" runat="server" Width="99%" TabIndex="5"
                            AutoPostBack="true" OnSelectedIndexChanged="Cmb_FF_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                     <tr>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Programa o Proyecto
                        </td>
                        <td colspan ="3" style="text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_PP" runat="server" Width="99%" TabIndex="4"
                            AutoPostBack="true" OnSelectedIndexChanged="Cmb_Programas_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Clasificador Administrativo
                        </td>
                        <td colspan ="3" style="text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_UR" runat="server" Width="99%" TabIndex="3"
                            AutoPostBack="true" OnSelectedIndexChanged="Cmb_UR_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2"> 
                            Partida Especifica
                        </td>
                        <td colspan ="3" style="text-align:left;cursor:default;" class="button_autorizar2">
                            <asp:DropDownList ID="Cmb_Partida" runat="server" Width="99%" TabIndex="6"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td></td>
                    </tr>
                </table>
             
                    <table  width="98%" class="estilo_fuente">
                        <tr>
                            <td style="text-align:left;cursor:default;" class="button_autorizar2"  colspan="2">
                                 Reportes de presupuesto a realizar por clasificación 
                            </td>
                            <td style="text-align:left;cursor:default;" class="button_autorizar2" colspan="2">
                                <asp:CheckBox ID="Chk_Todos" runat="server" Text=" Todos" OnClick="javascript:Seleccionar_Todo(this);" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:23%;text-align:left;cursor:default;" class="button_autorizar2">
                                <asp:CheckBox ID="Chk_Dependencia" runat="server" Text=" Ramo Administrativo" />
                            </td>
                             <td style="width:27%;text-align:left;cursor:default;" class="button_autorizar2">
                                <asp:CheckBox ID="Chk_Unidad_Responsable" runat="server" Text=" Clasificador Administrativo" />
                            </td>
                            <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2">
                               <asp:CheckBox ID="Chk_Programa" runat="server" Text=" Programa o Proyecto" />
                            </td>
                            <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2">
                               <asp:CheckBox ID="Chk_Fte_Financiamiento" runat="server" Text=" Fuente Financiamiento" />
                            </td>
                         </tr>
                         <tr>
                            <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2" colspan="2">
                                 <asp:CheckBox ID="Chk_Partida" runat="server" Text=" Clasificador por Objeto del Gasto"/>
                            </td>
                             <td style="width:25%;text-align:left;cursor:default;" class="button_autorizar2">
                                 <asp:CheckBox ID="Chk_Analitico" runat="server" Text=" Analítico"  />
                            </td>
                             <td style="text-align:left;cursor:default;" class="button_autorizar2" >
                                 <asp:CheckBox ID="Chk_Analitico_Detallado" runat="server" Text=" Analítico Detallado" />
                            </td>
                        </tr>
                    </table>
                    <table width="98%" class="estilo_fuente" style="text-align:center;">
                        <tr>
                            <td>
                            <div style="overflow:auto;height:auto; max-height:320px;width:99%;vertical-align:top;" >
                                <asp:GridView ID="Grid_Registros" runat="server"  CssClass="GridView_1" Width="100%" 
                                    AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                    AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                    EmptyDataText="No se encuentra ningun registro" 
                                    OnRowDataBound="Grid_Registros_RowDataBound" >
                                    <Columns>
                                        <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Codigo Programatico"  SortExpression="CODIGO_PROGRAMATICO">
                                            <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto"  SortExpression="CONCEPTO">
                                            <HeaderStyle HorizontalAlign="Left" Width="33%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Left" Width="33%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="APROBADO" HeaderText="Aprobado"  SortExpression="APROBADO" DataFormatString="{0:n}">
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AMPLIACION" HeaderText="Aumento"  SortExpression="AMPLIACION" DataFormatString="{0:n}">
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="REDUCCION" HeaderText="Disminución"  SortExpression="REDUCCION" DataFormatString="{0:n}">
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado"  SortExpression="MODIFICADO" DataFormatString="{0:n}">
                                            <HeaderStyle HorizontalAlign="Center" Width="8%" Font-Size="7pt"/>
                                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="8pt"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO" />
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger  ControlID = "Btn_Reporte_Grupo_Dependencia"/>
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
