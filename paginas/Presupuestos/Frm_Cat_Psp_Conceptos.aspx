<%@ Page Title="SIAC Sistema Integral Administrativo y Comercial" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Psp_Conceptos.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Psp_Conceptos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script src="../../javascript/Js_Psp_Validaciones_Pronosticos_Ingresos.js" type="text/javascript"></script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
  <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_SAP_Conceptos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Conceptos Ingresos
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />
                            <br />
                            <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" style="width: 50%">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                CssClass="Img_Button" OnClick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                CssClass="Img_Button" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                CssClass="Img_Button" OnClick="Btn_Eliminar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CssClass="Img_Button" OnClick="Btn_Salir_Click" />
                        </td>
                        <td colspan="2" align="right" style="width: 50%">
                            B�squeda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180" ToolTip="Buscar" TabIndex="1"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Busqueda" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                TabIndex="2" OnClick="Btn_Busqueda_Click" />
                            <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Buscar>" TargetControlID="Txt_Busqueda" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="Lbl_ID" runat="server" Text="Concepto ID" Visible="false"></asp:Label>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_ID" runat="server" Width="90%" Visible="false"></asp:TextBox>
                        </td>
                        <td>
                            &nbsp;
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            <asp:Label ID="Lbl_Clave" runat="server" Text="*Clave"></asp:Label>
                            &nbsp;Concepto</td>
                        <td>
                            <asp:TextBox ID="Txt_Clave" runat="server" Width="40%" MaxLength="2" onBlur="javascript:Completar_Clave_Concepto();"></asp:TextBox>
                            <asp:TextBox ID="Txt_Clave_Con" runat="server" Width="40%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender" runat="server"
                                TargetControlID="Txt_Clave" FilterType="Numbers" ValidChars="0123456789">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 10%; text-align: left;">
                            &nbsp; *Estatus
                        </td>
                        <td style="width: 30%; text-align: left;">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Enabled="False" Width="93%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            *A�o
                        </td>
                        <td style="width: 30%; text-align: left;">
                            <asp:TextBox ID="Txt_Anio" runat="server" Enabled="False" Width="83%" MaxLength="4"></asp:TextBox>
                        </td>
                        <td style="width: 10%; text-align: left;">
                            &nbsp; Banco
                        </td>
                        <td style="width: 30%; text-align: left;">
                            <asp:DropDownList ID="Cmb_Banco" runat="server" Enabled="False" Width="93%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            <asp:Label ID="Lbl_Clase" runat="server" Text="*Clase"></asp:Label>
                        </td>
                        <td colspan='3'>
                            <asp:DropDownList ID="Cmb_Clase" runat="server" Width="97%" onChange="javascript:Completar_Clave_Concepto();">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%; text-align: left;">
                            *Cuenta Contable
                        </td>
                        <td style="text-align: left;" colspan="3">
                            <asp:DropDownList ID="Cmb_Cuenta_Contable" runat="server" Enabled="False" Width="97%" />
                        </td>
                    </tr>
                    <%--<tr>
                        <td style="width: 10%; text-align: left;">
                            *Unidad Responsable
                        </td>
                        <td style="text-align: left;" colspan="3">
                            <asp:DropDownList ID="Cmb_UR" runat="server" Enabled="False" Width="97%" /> 
                        </td>
                    </tr>--%>
                    <tr>
                        <td style="width: 18%">
                            <asp:Label ID="Lbl_Comentarios" runat="server" Text="*Descripci�n"></asp:Label>
                        </td>
                        <td colspan="3" style="width: 82%">
                            <asp:TextBox ID="Txt_Comentarios" runat="server" Width="96%" TextMode="MultiLine"
                                MaxLength="250" onKeyUp="javascript:Validar_Cantidad_Caracteres(this);"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Comentarios" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="L�mite de Caractes 250" TargetControlID="Txt_Comentarios">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Comentarios"
                                ValidChars="��.,:;()���������� ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            <asp:Label ID="Lbl_Fuente_Financiamiento" runat="server" 
                                Text="*Fuente Financiamiento"></asp:Label>
                        </td>
                        <td style="text-align: left;" colspan="3">
                            <asp:DropDownList ID="Cmb_Fuente_Financiamiento" runat="server" Enabled="False" Width="94%" > 
                <asp:ListItem Value= "-1" > <SELECCIONE></asp:ListItem>
                            </asp:DropDownList> 
                             
                            &nbsp;
                            <asp:ImageButton ID="Btn_Agregar_Documento" runat="server" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                CssClass="Img_Button" ToolTip="Agregar" Height="16px" Width="16px" AutoPostBack="true"
                                OnClick="Btn_Agregar_Fuente_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div style="overflow: auto; width: 98%; height: 80px; vertical-align: top;">
                                <asp:GridView ID="Grid_Fuente_Financiamiento" runat="server" Width="97%" OnRowDataBound="Grid_Fuentes_RowDataBound"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None">
                                    <Columns>
                                        <asp:BoundField DataField="Fuente_Financiamiento_ID" HeaderText="Fuente_ID">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripcion">
                                            <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                            <ItemStyle HorizontalAlign="Left" Width="80%" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <center>
                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" CausesValidation="false" OnClick="Btn_Eliminar_Partida"
                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" OnClientClick="return confirm('�Est� seguro de eliminar de la tabla la Fuente seleccionada?');" />
                                                </center>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="tblHead" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top;">
                                <asp:GridView ID="Grid_Psp_Conceptos" runat="server" CssClass="GridView_1" Style="white-space: normal"
                                    AutoGenerateColumns="False" AllowPaging="false" PageSize="5" Width="96%" GridLines="none"
                                    OnPageIndexChanging="Grid_Psp_Conceptos_PageIndexChanging" OnSelectedIndexChanged="Grid_Psp_Conceptos_SelectedIndexChanged"
                                    EmptyDataText="&quot;No se encontraron registros&quot;" HorizontalAlign="Left"
                                    OnSorting="Grid_Psp_Conceptos_Sorting" AllowSorting="true">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="3%" HorizontalAlign="Left" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="Concepto_Ing_ID" HeaderText="Concepto ID" SortExpression="Concepto_Ing_ID">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="15%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE">
                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="8pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci�n" SortExpression="DESCRIPCION">
                                            <HeaderStyle HorizontalAlign="Left" Width="41%" />
                                            <ItemStyle HorizontalAlign="Left" Width="41%" Font-Size="8pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CLASE_CLAVE" HeaderText="Clase" SortExpression="CLASE_CLAVE">
                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="8pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="8pt" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Clase_Ing_ID" HeaderText="Clase" SortExpression="Clase_Ing_ID">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="1%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_CONTABLE_ID" />
                                        <asp:BoundField DataField="DEPENDENCIA_ID" />
                                        <asp:BoundField DataField="ANIO" HeaderText="A�o" SortExpression="ANIO">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle Width="5%" HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="BANCO_ID" />
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
                &nbsp;&nbsp;
                &nbsp;
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
