﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Psp_Rol_Empleado.Datos;


public partial class paginas_Presupuestos_Frm_Psp_Ayudante_Rol_Empleado : System.Web.UI.Page
{
    #region PAGE LOAD
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Lbl_Mensaje_Error.Text = String.Empty;
                Inicio();
            }
        }
    #endregion

    #region METODOS

        #region (Metodos Generales)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Inicio
            ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Inicio()
            {
                try
                {
                    Img_Error.Visible = false;
                    Lbl_Mensaje_Error.Visible = false;
                    Lbl_Mensaje_Error.Text = String.Empty;
                    Llenar_Combo_Unidad_Responsable();
                    Llenar_Grid_Empleados();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Inicio Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region (Metodos Combos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Unidad_Responsable
            ///DESCRIPCIÓN          : Metodo para llenar el combo de unidades responsables
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Unidad_Responsable()
            {
                ClS_Psp_Ayudante_Rol_Empleado_Datos Datos = new ClS_Psp_Ayudante_Rol_Empleado_Datos();
                DataTable Dt_Dependencias = new DataTable();

                try
                {
                    Dt_Dependencias = ClS_Psp_Ayudante_Rol_Empleado_Datos.Consultar_Unidad_Responsable();
                    if (Dt_Dependencias != null)
                    {
                        if (Dt_Dependencias.Rows.Count > 0)
                        {
                            Cmb_Unidad_Responsable.Items.Clear();
                            Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                            Cmb_Unidad_Responsable.DataTextField = "NOMBRE";
                            Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                            Cmb_Unidad_Responsable.DataBind();
                        }
                    }

                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Unidad_Responsable ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Empleados
            ///DESCRIPCIÓN          : Metodo para llenar el grid de empleados
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Grid_Empleados()
            {
                Cls_Cat_Empleados_Negocios Rs_Consulta_Ca_Empleados = new Cls_Cat_Empleados_Negocios(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Empleados; //Variable que obtendra los datos de la consulta 

                try
                {
                    Rs_Consulta_Ca_Empleados.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.ToString().Trim();
                    Dt_Empleados = Rs_Consulta_Ca_Empleados.Consulta_Empleados_General();
                    if (Dt_Empleados != null)
                    {
                        if (Dt_Empleados.Rows.Count > 0)
                        {
                            Grid_Empleado.DataBind();
                            Grid_Empleado.Columns[5].Visible = true;
                            Grid_Empleado.DataSource = Dt_Empleados;
                            Grid_Empleado.DataBind();
                            Grid_Empleado.Columns[5].Visible = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar la tabla de empleados ERROR[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion

    #region Eventos
        #region Generales
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
            ///DESCRIPCIÓN          : Evento del boton de modificar
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 15/Marzo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Btn_Modificar_Click(object sender, EventArgs e)
            {
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Lbl_Mensaje_Error.Text = String.Empty;
                try
                {
                    if (!String.IsNullOrEmpty(Hf_Empleado_Id.Value.Trim()))
                    {
                        if (!String.IsNullOrEmpty(Txt_Rol_id.Text.Trim()))
                        {
                            if (ClS_Psp_Ayudante_Rol_Empleado_Datos.Modificar_Empleado(Txt_Rol_id.Text.Trim(), Hf_Empleado_Id.Value.Trim())) 
                            {
                                Hf_Empleado_Id.Value = String.Empty;
                                Grid_Empleado.SelectedIndex = -1;
                                Txt_Rol_id.Text = String.Empty;
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Rol", "alert('ACTUALIZADO');", true);
                            }
                        }
                        else 
                        {
                            Lbl_Mensaje_Error.Text = "Favor de ingresar un rol";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                    }
                    else 
                    {
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar un empleado";
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al modificar los datos. Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region Combos
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de unidad responsable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
            {
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Lbl_Mensaje_Error.Text = String.Empty;
                try
                {
                    Llenar_Grid_Empleados();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento del combo de unidad responsable Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region Grid
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Empleados_SelectedIndexChanged
            ///DESCRIPCIÓN          : Ejecuta la operacion de seleccion de un grid
            ///                       de parametros.
            ///PARAMETROS           : 
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 15/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Empleados_SelectedIndexChanged(object sender, EventArgs e)
            {
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Visible = false;
                Lbl_Mensaje_Error.Text = String.Empty;
                try
                {
                    if (Grid_Empleado.SelectedIndex > (-1))
                    {
                        Hf_Empleado_Id.Value = Grid_Empleado.SelectedRow.Cells[5].Text.Trim();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al querer seleccionar un registro del grid de empleados Error[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion
}
