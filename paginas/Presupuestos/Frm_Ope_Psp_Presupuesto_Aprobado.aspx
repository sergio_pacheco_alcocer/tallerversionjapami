﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Presupuesto_Aprobado.aspx.cs" 
    Inherits="paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Aprovado" Title="SIAC" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script src="../../easyui/jquery.tools.min.js" type="text/javascript"></script>
    
    <link href="../../easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script type="text/javascript">
    </script>

    <script type="text/javascript" language="javascript">
        <!--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">    
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" >
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
        </asp:UpdateProgress>

        <div style="background-color:White; width:98%;" >
            <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td style="width:100%;" colspan="2">
                        Cargar Cuentas Presupuestales
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false"  />
                        &nbsp;
                    </td>
                    <td>
                        <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                    </td>
                </tr>
            </table>                                                             
            
            <table width="98%"  border="0" cellspacing="0">
                     <tr align="center">
                         <td colspan="2">                
                             <div align="right" class="barra_busqueda">                        
                                  <table style="width:100%;height:28px;">
                                    <tr>
                                      <td align="left" style="width:59%;">                                                  
                                           <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" CausesValidation="false"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click" CausesValidation="false"/>                                            
                                            <asp:ImageButton ID="Btn_Descargar" runat="server" ToolTip="Descargar" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/mime/xlsx.ico" CausesValidation="false" OnClick="Btn_Descargar_Click"/>
                                                
                                      </td>
                                      <td align="right" style="width:41%;">
                                       </td>       
                                     </tr>         
                                  </table>                      
                                </div>
                         </td>
                     </tr>
            </table>
        
            <table style="width:98%;">
               <tr>
                    <td colspan="4" style="width:100%;">
                        <hr />
                    </td>
                </tr>
                <tr id="Tr_Periodos_Fiscales" runat="server">
                    <td style="text-align:left;width:20%;">
                        Año
                    </td>
                    <td  style="text-align:left;width:30%;" >
                        <asp:DropDownList ID="Cmb_Anio" runat="server" Width="100%"/>
                    </td>                     
                    <td  style="text-align:right;width:5%;" >
                        <asp:ImageButton ID="Btn_Subir" runat="server" ToolTip="Descargar" CssClass="Img_Button"
                            ImageUrl="~/paginas/imagenes/paginas/subir.png" onclick="Ver_Contenido" CausesValidation="false"/>
                    </td>      
                    <td  style="text-align:left;width:40%;" >
                        <cc1:AsyncFileUpload ID="AFU_Cargar_Archivo" runat="server" class="archivo" Width="200px" OnUploadedComplete="Guardar_Archivo"/>
                   </td>
                </tr>
                <tr>
                    <td style="width:100%;" colspan="4">
                        <div id="contenedor_empleados" style="overflow:auto;height:400px;width:100%;vertical-align:top;border-style:outset;border-color:Silver;" >
                            <asp:GridView ID="Grid_Presupuesto" runat="server" CssClass="GridView_1"
                                 AutoGenerateColumns="False"  GridLines="Both" ShowHeader="true" ShowFooter="true">
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <FooterStyle CssClass="GridHeader" />
                                    <Columns>     
                                        <asp:BoundField DataField="FUENTE_FINANCIAMIENTO" HeaderText="FUENTE_FINANCIAMIENTO">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="AREA_FUNCIONAL">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>    
                                        <asp:BoundField DataField="PROGRAMA" HeaderText="PROGRAMA">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="DEPENDENCIA">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>   
                                        <asp:BoundField DataField="PARTIDA" HeaderText="PARTIDA">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CUENTA_CONTABLE" HeaderText="CUENTA_CONTABLE">
                                            <HeaderStyle HorizontalAlign="Center" Width="100px" />
                                            <ItemStyle HorizontalAlign="Center" Width="100px" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Cuenta_Contable_ID"/>
                                    </Columns>
                                </asp:GridView>
                            </div>                                    
                        </td>
                    </tr>                                              
                </table> 
        </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Descargar" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>

