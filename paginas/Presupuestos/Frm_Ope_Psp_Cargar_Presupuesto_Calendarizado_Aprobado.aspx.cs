﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Cargar_Presupuesto_Calendarizado.Negocio;
using JAPAMI.Cargar_Presupuesto_Calendarizado.Datos;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Constantes;
using System.Net.Mail;
using System.Text;
using System.Data.SqlClient;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal.Negocio;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Cargar_Presupuesto_Calendarizado_Aprobado : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Cls_Sessiones.Mostrar_Menu = true;
            Lbl_Informacion.Text = "";
            Cargar_Anios_Presupuesto_Aprobado();
        }
    }

    #region MÉTODOS 

    public void Cargar_Anios_Presupuesto_Aprobado()
    {
        Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio = new Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio();
        DataTable Dt_Anios = Negocio.Consultar_Anios_Presupuestados();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Anio, Dt_Anios, "ANIO", "ANIO");
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Aprobada
    ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas aprobadas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 29/Noviembre/2011
    ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       : 23/Agosto/2012
    ///CAUSA_MODIFICACIÓN...: Debido a que se agrego un nuevo campo a la calendarizacion del presupuesto llamado 
    ///                       subnivel presupuestal
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Partida_Aprobada(DataTable Dt_Partidas)
    {
        DataTable Dt_Partida_Aprobada = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = Dt_Partidas;
        String Partida_Id = string.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        DataRow Fila;
        String Anio = String.Empty;
        Boolean Iguales;
        String Fuente_Financiamiento_ID = String.Empty;
        String Programa_ID = String.Empty;
        String Capitulo_ID = String.Empty;
        String Dependencia_ID = String.Empty;
        String Subnivel_Presupuestal_ID = String.Empty; //cambio
        String Nivel_Presupuesto = String.Empty;
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio = new Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio();
        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Anio , System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID, System.Type.GetType("System.String"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total, System.Type.GetType("System.Double"));
                    Dt_Partida_Aprobada.Columns.Add(Ope_Psp_Presupuesto_Aprobado.Campo_Nivel_Presupuesto, System.Type.GetType("System.String"));


                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        Partida_Id = Dr_Sessiones["PARTIDA_ID"].ToString().Trim();
                        Programa_ID = Dr_Sessiones["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                        Fuente_Financiamiento_ID = Dr_Sessiones["FTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Capitulo_ID = Dr_Sessiones["CAPITULO_ID"].ToString().Trim();
                        Dependencia_ID = Dr_Sessiones["DEPENDENCIA_ID"].ToString().Trim();
                        Anio  = Dr_Sessiones["ANIO"].ToString().Trim();
                        Negocio.P_Subnnivel_Presupuestal_ID = Subnivel_Presupuestal_ID;

                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Aprobada.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Aprobada.Rows)
                            {
                                if (Dr_Partidas["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id) && Dr_Partidas["FTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Fuente_Financiamiento_ID) && Dr_Partidas["PROYECTO_PROGRAMA_ID"].ToString().Trim().Equals(Programa_ID) && Dr_Partidas["CAPITULO_ID"].ToString().Trim().Equals(Capitulo_ID) && Dr_Partidas["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_ID))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        // tomamos los datos del datatable para agrupar las partidas asignadas
                        if (!Iguales)
                        {
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id) && Dr_Session["FTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Fuente_Financiamiento_ID) && Dr_Session["PROYECTO_PROGRAMA_ID"].ToString().Trim().Equals(Programa_ID) && Dr_Session["CAPITULO_ID"].ToString().Trim().Equals(Capitulo_ID) && Dr_Session["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_ID))
                                {
                                    Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero].ToString().Trim());
                                    Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero].ToString().Trim());
                                    Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo].ToString().Trim());
                                    Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril].ToString().Trim());
                                    May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo].ToString().Trim());
                                    Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio].ToString().Trim());
                                    Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio].ToString().Trim());
                                    Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto].ToString().Trim());
                                    Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre].ToString().Trim());
                                    Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre].ToString().Trim());
                                    Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre].ToString().Trim());
                                    Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre].ToString()) ? "0" : Dr_Session[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre].ToString().Trim());
                                }
                            }

                            Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                            Fila = Dt_Partida_Aprobada.NewRow();
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Dependencia_ID] = Dependencia_ID;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Fte_Financiamiento_ID] = Fuente_Financiamiento_ID;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Proyecto_Programa_ID] = Programa_ID;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Capitulo_ID] = Capitulo_ID;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Partida_ID] = Partida_Id;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Anio] = Anio;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID] = Dr_Sessiones[Ope_Psp_Calendarizacion_Presu.Campo_Area_Funcional_ID].ToString().Trim();
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Enero] = Ene;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Febrero] = Feb;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Marzo] = Mar;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Abril] = Abr;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Mayo] = May;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Junio] = Jun;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Julio] = Jul;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Agosto] = Ago;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Septiembre] = Sep;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Octubre] = Oct;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Noviembre] = Nov;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Diciembre] = Dic;
                            Fila[Ope_Psp_Calendarizacion_Presu.Campo_Importe_Total] = Total;
                            Fila[Ope_Psp_Presupuesto_Aprobado.Campo_Nivel_Presupuesto] = Nivel_Presupuesto;
                            Dt_Partida_Aprobada.Rows.Add(Fila);

                            Ene = 0.00;
                            Feb = 0.00;
                            Mar = 0.00;
                            Abr = 0.00;
                            May = 0.00;
                            Jun = 0.00;
                            Jul = 0.00;
                            Ago = 0.00;
                            Sep = 0.00;
                            Oct = 0.00;
                            Nov = 0.00;
                            Dic = 0.00;
                            Total = 0.00;
                        }
                    }
                }
            }
            return Dt_Partida_Aprobada;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas aprobadas Error[" + ex.Message + "]");
        }
    }
    //********************************** METODOS PARA MOSTRAR LOS DATOS EN LA GRID *****************************
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Agrupada_Dependencia
    ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       : 20/Septiembre/2012
    ///CAUSA_MODIFICACIÓN...: adaptar el metodo para que agrupe por depemdencia
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Agrupada_Dependencia()
    {
        DataTable Dt_Partida_Asignada = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Dependencia"];
        String Dependencia_Id = string.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        DataRow Fila;
        String Dependencia = String.Empty;
        Boolean Iguales;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Asignada.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("DEPENDENCIA", System.Type.GetType("System.String"));
                    Dt_Partida_Asignada.Columns.Add("MONTO_TOTAL", System.Type.GetType("System.String"));

                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        Dependencia_Id = Dr_Sessiones["DEPENDENCIA_ID"].ToString().Trim();
                        Dependencia = Dr_Sessiones["DEPENDENCIA"].ToString().Trim();
                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Asignada.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Asignada.Rows)
                            {
                                if (Dr_Partidas["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_Id))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        // tomamos los datos del datatable para agrupar las partidas asignadas
                        if (!Iguales)
                        {
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_Id))
                                {
                                    Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                    Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                    Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                    Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                    May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                    Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                    Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                    Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                    Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                    Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                    Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                }
                            }
                            Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                            Fila = Dt_Partida_Asignada.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dependencia_Id;
                            Fila["DEPENDENCIA"] = Dependencia;
                            Fila["MONTO_TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                            Dt_Partida_Asignada.Rows.Add(Fila);

                            Ene = 0.00;
                            Feb = 0.00;
                            Mar = 0.00;
                            Abr = 0.00;
                            May = 0.00;
                            Jun = 0.00;
                            Jul = 0.00;
                            Ago = 0.00;
                            Sep = 0.00;
                            Oct = 0.00;
                            Nov = 0.00;
                            Dic = 0.00;
                            Total = 0.00;
                        }
                    }
                }
            }
            return Dt_Partida_Asignada;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Detalles_Partida_Dependencia
    ///DESCRIPCIÓN          : Metodo para crear el datatable de los detalles de las partidas asignadas del grid anidado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Noviembre/2011
    ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO       : 20/Septiembre/2012
    ///CAUSA_MODIFICACIÓN...: adaptar el  metodo para que agrupe dependiendo de la dependencia, de la partida y del subnivel
    ///*********************************************************************************************************
    private DataTable Crear_Dt_Detalles_Partida_Dependencia(String Dependencia_ID)
    {
        DataTable Dt_Partida_Detalle = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Dependencia"];
        String Dependencia_Id = string.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Total = 0.00;
        DataRow Fila;
        String Partida_ID = String.Empty;
        String Subnivel = String.Empty;
        String Codigo_Programatico = String.Empty;
        Boolean Iguales;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Detalle.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("CODIGO_PROGRAMATICO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));

                    Boolean Dependencia_Existe = false;
                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        Dependencia_Id = Dependencia_ID;
                        Codigo_Programatico = Dr_Sessiones["CODIGO_PROGRAMATICO"].ToString().Trim();
                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Detalle.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Detalle.Rows)
                            {
                                if (Dr_Partidas["DEPENDENCIA_ID"].ToString().Trim().Equals(Dependencia_Id) && Dr_Partidas["CODIGO_PROGRAMATICO"].ToString().Equals(Codigo_Programatico))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        // tomamos los datos del datatable para agrupar las partidas asignadas
                        if (!Iguales)
                        {
                            Dependencia_Existe = false;
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["DEPENDENCIA_ID"].ToString().Equals(Dependencia_Id) && Dr_Session["CODIGO_PROGRAMATICO"].ToString().Trim().Equals(Codigo_Programatico))
                                {
                                    Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                    Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                    Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                    Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                    May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                    Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                    Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                    Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                    Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                    Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                    Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                    Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                    Dependencia_Existe = true;
                                }
                            }
                            Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                            Fila = Dt_Partida_Detalle.NewRow();
                            Fila["DEPENDENCIA_ID"] = Dependencia_Id;
                            Fila["CODIGO_PROGRAMATICO"] = Codigo_Programatico;
                            Fila["ENERO"] = String.Format("{0:##,###,##0.00}", Ene);
                            Fila["FEBRERO"] = String.Format("{0:##,###,##0.00}", Feb);
                            Fila["MARZO"] = String.Format("{0:##,###,##0.00}", Mar);
                            Fila["ABRIL"] = String.Format("{0:##,###,##0.00}", Abr);
                            Fila["MAYO"] = String.Format("{0:##,###,##0.00}", May);
                            Fila["JUNIO"] = String.Format("{0:##,###,##0.00}", Jun);
                            Fila["JULIO"] = String.Format("{0:##,###,##0.00}", Jul);
                            Fila["AGOSTO"] = String.Format("{0:##,###,##0.00}", Ago);
                            Fila["SEPTIEMBRE"] = String.Format("{0:##,###,##0.00}", Sep);
                            Fila["OCTUBRE"] = String.Format("{0:##,###,##0.00}", Oct);
                            Fila["NOVIEMBRE"] = String.Format("{0:##,###,##0.00}", Nov);
                            Fila["DICIEMBRE"] = String.Format("{0:##,###,##0.00}", Dic);
                            Fila["IMPORTE_TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                            if(Dependencia_Existe == true)
                            {
                                Dt_Partida_Detalle.Rows.Add(Fila);
                            }
                            Ene = 0.00;
                            Feb = 0.00;
                            Mar = 0.00;
                            Abr = 0.00;
                            May = 0.00;
                            Jun = 0.00;
                            Jul = 0.00;
                            Ago = 0.00;
                            Sep = 0.00;
                            Oct = 0.00;
                            Nov = 0.00;
                            Dic = 0.00;
                            Total = 0.00;
                        }
                    }
                }
            }
            return Dt_Partida_Detalle;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
    ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 20/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Partida_Asignada()
    {
        Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio = new Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio();
        DataTable Dt_Session = new DataTable();
        DataTable Dt_Partidas_Dependencia = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Dependencia"];
        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    Dt_Partidas_Dependencia = Crear_Dt_Agrupada_Dependencia();
                    Grid_Partida_Asignada.Columns[1].Visible = true;
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Dependencia;
                    Grid_Partida_Asignada.DataBind();
                    Grid_Partida_Asignada.Columns[1].Visible = false;
                }
                else
                {
                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Dependencia;
                    Grid_Partida_Asignada.DataBind();
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
        }
    }


    #endregion

    #region (Metodo Enviar Correo)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Correo
        ///DESCRIPCIÓN          : Envia un correo a un usuario
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Obtener_Datos_Correo()
        {
            DataTable Dt_Para = new DataTable();
            String Copia = String.Empty;
            String Texto_Correo = String.Empty;
            String Asunto_Correo = String.Empty;

            try
            {
                //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
                Dt_Para = Obtener_Correos();
                Copia = Obtener_Datos_Correo_Psp();

                Texto_Correo = "<html>" +
                         "<body> A quien corresponda:<br /><br />" +
                               Txt_Texto_Correo.Text.Trim() +
                           "</body>" +
                     "</html>";

                Asunto_Correo = "Carga de Presupuesto " + Cmb_Anio.SelectedValue.Trim();

                if (Dt_Para != null)
                {
                    if (Dt_Para.Rows.Count > 0)
                    {
                        Enviar_Correo(Dt_Para, Copia, Asunto_Correo, Texto_Correo);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Envio de Correo " + ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Enviar_Correo
        ///DESCRIPCIÓN          : Envia un correo a un usuario
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 14/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Enviar_Correo(DataTable Para, String Copia_Para, String Asunto, String Texto_Correo)
        {
            MailMessage Correo = new MailMessage(); //obtenemos el objeto del correo
            //variables para guardar los datos de los parametros del correo
            String Puerto = String.Empty;
            String Usuario_Correo = String.Empty;
            String De = String.Empty;
            String Servidor = String.Empty;
            String Contraseña = String.Empty;

            try
            {
                Puerto = ConfigurationManager.AppSettings["Puerto_Correo"];
                Usuario_Correo = ConfigurationManager.AppSettings["Usuario_Correo"];
                De = ConfigurationManager.AppSettings["Correo_Saliente"];
                Servidor = ConfigurationManager.AppSettings["Servidor_Correo"];
                Contraseña = ConfigurationManager.AppSettings["Password_Correo"];

                if (!String.IsNullOrEmpty(Puerto)
                       && !String.IsNullOrEmpty(Usuario_Correo) && !String.IsNullOrEmpty(De)
                       && !String.IsNullOrEmpty(Servidor) && !String.IsNullOrEmpty(Contraseña))
                {
                    Correo.To.Clear();
                    foreach (DataRow Dr in Para.Rows)
                    {
                        if (!String.IsNullOrEmpty(Dr[Cat_Empleados.Campo_Correo_Electronico].ToString().Trim()))
                        {
                            Correo.To.Add(Dr[Cat_Empleados.Campo_Correo_Electronico].ToString().Trim());
                        }
                    }
                    //validamos que no venga vacio el correo al que mandaremos copia
                    if (!String.IsNullOrEmpty(Copia_Para))
                    {
                        Correo.CC.Add(Copia_Para);
                    }

                    Correo.From = new MailAddress(De, Usuario_Correo.Trim());
                    Correo.Subject = Asunto.Trim();
                    Correo.SubjectEncoding = System.Text.Encoding.UTF8;

                    if ((!Correo.From.Equals("") || Correo.From != null) && (!Correo.To.Equals("") || Correo.To != null))
                    {
                        Correo.Body = Texto_Correo;
                        Correo.BodyEncoding = System.Text.Encoding.UTF8;
                        Correo.IsBodyHtml = true;

                        SmtpClient cliente_correo = new SmtpClient();
                        cliente_correo.Port = int.Parse(Puerto);
                        cliente_correo.UseDefaultCredentials = true;
                        cliente_correo.Credentials = new System.Net.NetworkCredential(De, Contraseña);
                        cliente_correo.Host = Servidor;
                        cliente_correo.Send(Correo);
                        Correo = null;
                    }
                    else
                    {
                        throw new Exception("No se tiene configurada una cuenta de correo, favor de notificar");
                    }
                }
            }
            catch (SmtpException ex)
            {
                throw new Exception("Envio de Correo " + ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Correos
        ///DESCRIPCIÓN          : Metodo para Obtener los datos de los correos de los 
        ///empleados que realizaron el presupuesto
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public DataTable Obtener_Correos() 
        {
            DataTable Dt_Correos = new DataTable();
            DataSet Ds_Correos = new DataSet();
            StringBuilder Query = new StringBuilder();

            try
            {
                Query.Append("SELECT DISTINCT " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Correo_Electronico);
                Query.Append(" FROM " + Cat_Empleados.Tabla_Cat_Empleados);
                Query.Append(" INNER JOIN " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu);
                Query.Append(" ON " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Empleado_ID_Creo);
                Query.Append(" = " + Cat_Empleados.Tabla_Cat_Empleados + "." + Cat_Empleados.Campo_Empleado_ID);
                Query.Append(" WHERE " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Estatus);
                Query.Append(" = 'CARGADO' ");
                Query.Append(" AND " + Ope_Psp_Calendarizacion_Presu.Tabla_Ope_Psp_Calendarizacion_Presu + "." + Ope_Psp_Calendarizacion_Presu.Campo_Anio);
                Query.Append(" = " + Cmb_Anio.SelectedValue.Trim());

                Ds_Correos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Query.ToString());

                if (Ds_Correos != null)
                {
                    Dt_Correos = Ds_Correos.Tables[0];
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener los correos. Error[" + Ex.Message + "]");
            }
            return Dt_Correos;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Correo_Psp
        ///DESCRIPCIÓN          : Metodo para Obtener los datos de los correos de los 
        ///empleados que realizaron el presupuesto
        ///PROPIEDADES          : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 31/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public String Obtener_Datos_Correo_Psp() 
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Coordinador = new DataTable();
            String Email_Psp = String.Empty;
            DataTable Dt_Datos = new DataTable();
            
            try
            {
                //OBTENEMOS EL NOMBRE DEL REPRESENTANTE DEL AREA
                Negocio.P_Dependencia_ID_Busqueda = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                Negocio.P_Busqueda = Cls_Sessiones.Empleado_ID.Trim();
                Dt_Coordinador = Negocio.Obtener_Coordinador_UR();

                if (Dt_Coordinador != null)
                {
                    if (Dt_Coordinador.Rows.Count > 0)
                    {
                        Dt_Datos = new DataTable();
                        Dt_Datos = (from Fila_Mov in Dt_Coordinador.AsEnumerable()
                                    where Fila_Mov.Field<string>("CLAVE") == "COORDINADOR PSP"
                                    select Fila_Mov).AsDataView().ToTable();

                        if (Dt_Datos != null)
                        {
                            if (Dt_Datos.Rows.Count > 0)
                            {
                                Email_Psp = Dt_Datos.Rows[0]["EMAIL"].ToString().Trim();
                            }
                        }
                    }
                }

            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener los datos del correo de presupuestos. Error[" + Ex.Message + "]");
            }
            return Email_Psp;
        }
    #endregion

    #region EVENTOS GRID
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Evento del boton de salir
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011 
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: devido a que se agrego al grid de detalles El subnivel_Presupuestal_ID
            ///*********************************************************************************************************
            protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Dependencia"];

                try
                {
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                            DataTable Dt_Detalles = new DataTable();
                            String Dependencia_ID = String.Empty;

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            {
                                Dependencia_ID = e.Row.Cells[1].Text.Trim();
                                Dt_Detalles = Crear_Dt_Detalles_Partida_Dependencia(Dependencia_ID);

                                Grid_Partidas_Detalle.Columns[0].Visible = true;
                                Grid_Partidas_Detalle.DataSource = Dt_Detalles;
                                Grid_Partidas_Detalle.DataBind();
                                Grid_Partidas_Detalle.Columns[0].Visible = false;
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
            ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
                }
            }
        #endregion

    protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio Negocio = new Cls_Ope_Psp_Cargar_Presup_Calendarizado_Negocio();
        Boolean Existente = false;
        double Importe = 0;

        Negocio.P_Anio = Cmb_Anio.SelectedValue.Trim();
        Importe = Negocio.Consultar_Importe_Presupuesto_Aprobado();
        Lbl_Aprobado.Text = " " + String.Format("{0:C}",Importe);

        Existente = Negocio.Consultar_Presupuesto_Generado();
        Session["Dt_Partidas_Dependencia"] = (DataTable)Negocio.Consultar_Partidas_Presupuesto();
        Llenar_Grid_Partida_Asignada();

        if (Existente)
        {
            Lbl_Informacion.Text = "NOTA: Aun existen Unidades Responsables con presupuestos sin autorizar!<br />";
        }
    }
    protected void Btn_Cargar_Presupuesto_Calendarizad_Click(object sender, EventArgs e)
    {
        //Consultar presupuesto calendarizado
        Lbl_Informacion.Text = "";
        DataTable Dt_Presupuesto_Calendarizado = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Presupuesto_Calendarizado(int.Parse(Cmb_Anio.SelectedValue.Trim()), "AUTORIZADO");

        Dt_Presupuesto_Calendarizado = Crear_Dt_Partida_Aprobada(Dt_Presupuesto_Calendarizado);

        int Registros_Guardados = Cls_Ope_Psp_Manejo_Presupuesto.Guardar_Presupuesto_Aprobado(Dt_Presupuesto_Calendarizado);
        ScriptManager.RegisterStartupScript(
            this, this.GetType(), "Presupuesto", "alert('Se guardaron " + Registros_Guardados + "  partidas presupuestales');", true);
        Obtener_Datos_Correo();
    }
}
