﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" Title="SIAC Sistema Integral Administrativo y Comercial"
CodeFile="Frm_Ope_Psp_Actualizar_Presupuesto.aspx.cs" Inherits="paginas_Presupuestos_Frm_Ope_Psp_Actualizar_Presupuesto" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
                </ProgressTemplate>
        </asp:UpdateProgress>
        <div id="Div_Contenido" style="width:97%;height:100%;">
            <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr >
                <td colspan="2" class="label_titulo">Actualizar Presupuestos</td>
            </tr>
            <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td colspan="2">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="Btn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px" Visible="false"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr> 
                    </table>                   
                   
                </td>
            </tr>
            <%--Fila de Busqueda y Botones Generales --%>
            <tr class="barra_busqueda">
                    <td colspan="2" >
                         <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                            />
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
            </tr>
               
                    <tr>
                        <td width="10%">
                            Archivo
                        </td>
                        <td >
                             <cc1:AsyncFileUpload ID="AFU_Archivo_Excel" runat="server" ErrorBackColor="Red" CompleteBackColor="Lime" UploadingBackColor="Silver"  OnUploadedComplete="AFU_Archivo_Excel_UploadedComplete"/>
                        </td>
                        
                    </tr>
                    
                    <tr> 
                        <td align="center" colspan="2">
                            <asp:ImageButton ID="Btn_Cargar_Archivo"  runat="server"   ToolTip="Validar Archivo" onclick="Btn_Cargar_Archivo_Click" ImageUrl="~/paginas/imagenes/paginas/icono_respuesta_peticion.png"/>
                             
                            Validar Archivo</td>
                    </tr>
            
                   
            
            </table>
        </div>
        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>