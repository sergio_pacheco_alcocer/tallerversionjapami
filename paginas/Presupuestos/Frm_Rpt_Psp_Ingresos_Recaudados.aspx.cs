﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Ingresos_Recaudados.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;
using System.Linq;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Ingresos_Recaudados : System.Web.UI.Page
{
    #region (Page Load)

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Reporte_Inicio();
                ViewState["SortDirection"] = "DESC";
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
        }
    }
    #endregion

    #region METODOS
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Reporte_Sueldos_Inicio
    ///DESCRIPCIÓN          : Metodo de inicio de la página
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Reporte_Inicio()
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Todo");
            Llenar_Combo_Anios();
            Llenar_Combo_FF();
            Cmb_Anio.SelectedIndex = -1;
            Cmb_FF.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
    ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
    ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Limpiar_Controles(String Accion)
    {
        try
        {
            switch (Accion)
            {
                case "Todo":
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Cmb_Anio.SelectedIndex = -1;
                    Cmb_FF.SelectedIndex = -1;
                    break;
                case "Error":
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    break;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Combo_Anios()
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio();
        DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

        try
        {
            Cmb_Anio.Items.Clear();

            Dt_Anios = Obj_Ingresos.Consultar_Anios();

            Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
            Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
            Cmb_Anio.DataSource = Dt_Anios;
            Cmb_Anio.DataBind();

            Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las fuentes de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Combo_FF()
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio();
        DataTable Dt_FF = new DataTable(); //Para almacenar los datos de los tipos de nominas

        try
        {
            Cmb_FF.Items.Clear();

            Dt_FF = Obj_Ingresos.Consultar_FF();

            Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Cmb_FF.DataTextField = "CLAVE_NOMBRE";
            Cmb_FF.DataSource = Dt_FF;
            Cmb_FF.DataBind();

            Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo de Fuentes de fianciamiento: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Pronostico
    ///DESCRIPCIÓN          : Metodo para generar el pronostico de ingresos
    ///PARAMETROS           1: Dt_Pronostico tabla que contiene los registros del pronostico de ingresos 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private DataTable Generar_Dt_Pronostico(DataTable Dt_Pronostico)
    {
        DataTable Dt_Datos = new DataTable();
        String Clave = String.Empty;
        String Rubro = String.Empty;
        Double Tot_Ene = 0.00;
        Double Tot_Feb = 0.00;
        Double Tot_Mar = 0.00;
        Double Tot_Abr = 0.00;
        Double Tot_May = 0.00;
        Double Tot_Jun = 0.00;
        Double Tot_Jul = 0.00;
        Double Tot_Ago = 0.00;
        Double Tot_Sep = 0.00;
        Double Tot_Oct = 0.00;
        Double Tot_Nov = 0.00;
        Double Tot_Dic = 0.00;
        Double Tot_Acumulado = 0.00;
        Double Tot_Estimado = 0.00;
        Double Tot_Proyectado = 0.00;
        Double Tot_Diferencia = 0.00;

        Double Tot_Imp_Ene = 0.00;
        Double Tot_Imp_Feb = 0.00;
        Double Tot_Imp_Mar = 0.00;
        Double Tot_Imp_Abr = 0.00;
        Double Tot_Imp_May = 0.00;
        Double Tot_Imp_Jun = 0.00;
        Double Tot_Imp_Jul = 0.00;
        Double Tot_Imp_Ago = 0.00;
        Double Tot_Imp_Sep = 0.00;
        Double Tot_Imp_Oct = 0.00;
        Double Tot_Imp_Nov = 0.00;
        Double Tot_Imp_Dic = 0.00;
        Double Tot_Imp_Acumulado = 0.00;
        Double Tot_Imp_Estimado = 0.00;
        Double Tot_Imp_Proyectado= 0.00;
        Double Tot_Imp_Diferencia = 0.00;
        
        DataRow Fila;

        try
        {
            Dt_Datos.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ENERO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("FEBRERO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("MARZO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("ABRIL", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("MAYO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("JUNIO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("JULIO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("AGOSTO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("OCTUBRE", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("NOVIEMBRE", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("DICIEMBRE", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("ACUMULADO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("ESTIMADO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("PROYECTADO", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("DIFERENCIA", System.Type.GetType("System.Double"));
            Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ELABORO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("RUBRO_ING", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TIPO_ING", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("CLASE_ING", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("CONCEPTO_ING", System.Type.GetType("System.String"));

            Clave = Dt_Pronostico.Rows[0]["CLAVE_RUBRO"].ToString().Trim();

            foreach (DataRow Dr in Dt_Pronostico.Rows)
            {
                if (Clave.Equals(Dr["CLAVE_RUBRO"].ToString().Trim()))
                {
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Acumulado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO"].ToString().Trim()) ? "0" : Dr["ACUMULADO"].ToString().Trim());
                    Tot_Estimado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                    Tot_Proyectado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PROYECTADO"].ToString().Trim()) ? "0" : Dr["PROYECTADO"].ToString().Trim());
                    Tot_Diferencia += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIFERENCIA"].ToString().Trim()) ? "0" : Dr["DIFERENCIA"].ToString().Trim());

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                        Fila["ACUMULADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO"].ToString().Trim()) ? "0" : Dr["ACUMULADO"].ToString().Trim());
                        Fila["ESTIMADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                        Fila["PROYECTADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PROYECTADO"].ToString().Trim()) ? "0" : Dr["PROYECTADO"].ToString().Trim());
                        Fila["DIFERENCIA"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIFERENCIA"].ToString().Trim()) ? "0" : Dr["DIFERENCIA"].ToString().Trim());
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                        Fila["RUBRO_ING"] = Dr["RUBRO_ING"].ToString().Trim();
                        Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                        Fila["TIPO_ING"] = Dr["TIPO_ING"].ToString().Trim();
                        Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                        Fila["CLASE_ING"] = Dr["CLASE_ING"].ToString().Trim();
                        Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                        Fila["CONCEPTO_ING"] = Dr["CONCEPTO_ING"].ToString().Trim();
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
                else
                {
                    //insertamos los totales del rubro anterior
                    Fila = Dt_Datos.NewRow();
                    Fila["CONCEPTO"] = "TOTAL " + Rubro;
                    Fila["ENERO"] = Tot_Ene;
                    Fila["FEBRERO"] = Tot_Feb;
                    Fila["MARZO"] = Tot_Mar;
                    Fila["ABRIL"] = Tot_Abr;
                    Fila["MAYO"] = Tot_May;
                    Fila["JUNIO"] = Tot_Jun;
                    Fila["JULIO"] = Tot_Jul;
                    Fila["AGOSTO"] = Tot_Ago;
                    Fila["SEPTIEMBRE"] = Tot_Sep;
                    Fila["OCTUBRE"] = Tot_Oct;
                    Fila["NOVIEMBRE"] = Tot_Nov;
                    Fila["DICIEMBRE"] = Tot_Dic;
                    Fila["ACUMULADO"] = Tot_Acumulado;
                    Fila["ESTIMADO"] = Tot_Estimado;
                    Fila["PROYECTADO"] = Tot_Proyectado;
                    Fila["DIFERENCIA"] = Tot_Diferencia;
                    Fila["TIPO"] = "RUBRO";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Datos.Rows.Add(Fila);

                    Tot_Imp_Ene += Tot_Ene;
                    Tot_Imp_Feb += Tot_Feb;
                    Tot_Imp_Mar += Tot_Mar;
                    Tot_Imp_Abr += Tot_Abr;
                    Tot_Imp_May += Tot_May;
                    Tot_Imp_Jun += Tot_Jun;
                    Tot_Imp_Jul += Tot_Jul;
                    Tot_Imp_Ago += Tot_Ago;
                    Tot_Imp_Sep += Tot_Sep;
                    Tot_Imp_Oct += Tot_Oct;
                    Tot_Imp_Nov += Tot_Nov;
                    Tot_Imp_Dic += Tot_Dic;
                    Tot_Imp_Acumulado += Tot_Acumulado;
                    Tot_Imp_Estimado += Tot_Estimado;
                    Tot_Imp_Proyectado += Tot_Proyectado;
                    Tot_Imp_Diferencia += Tot_Diferencia;

                    Tot_Ene = 0.00;
                    Tot_Feb = 0.00;
                    Tot_Mar = 0.00;
                    Tot_Abr = 0.00;
                    Tot_May = 0.00;
                    Tot_Jun = 0.00;
                    Tot_Jul = 0.00;
                    Tot_Ago = 0.00;
                    Tot_Sep = 0.00;
                    Tot_Oct = 0.00;
                    Tot_Nov = 0.00;
                    Tot_Dic = 0.00;
                    Tot_Acumulado = 0.00;
                    Tot_Estimado = 0.00;
                    Tot_Proyectado = 0.00;
                    Tot_Diferencia = 0.00;

                    //insertamos los datos del concepto que se esta comparando
                    Clave = Dr["CLAVE_RUBRO"].ToString().Trim();
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Acumulado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO"].ToString().Trim()) ? "0" : Dr["ACUMULADO"].ToString().Trim());
                    Tot_Estimado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                    Tot_Proyectado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PROYECTADO"].ToString().Trim()) ? "0" : Dr["PROYECTADO"].ToString().Trim());
                    Tot_Diferencia += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIFERENCIA"].ToString().Trim()) ? "0" : Dr["DIFERENCIA"].ToString().Trim());

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                        Fila["ACUMULADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO"].ToString().Trim()) ? "0" : Dr["ACUMULADO"].ToString().Trim());
                        Fila["ESTIMADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim());
                        Fila["PROYECTADO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["PROYECTADO"].ToString().Trim()) ? "0" : Dr["PROYECTADO"].ToString().Trim());
                        Fila["DIFERENCIA"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIFERENCIA"].ToString().Trim()) ? "0" : Dr["DIFERENCIA"].ToString().Trim());
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                        Fila["RUBRO_ING"] = Dr["RUBRO_ING"].ToString().Trim();
                        Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                        Fila["TIPO_ING"] = Dr["TIPO_ING"].ToString().Trim();
                        Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                        Fila["CLASE_ING"] = Dr["CLASE_ING"].ToString().Trim();
                        Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                        Fila["CONCEPTO_ING"] = Dr["CONCEPTO_ING"].ToString().Trim();
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
            }
            //insertamos los totales del rubro anterior
            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL " + Rubro;
            Fila["ENERO"] = Tot_Ene;
            Fila["FEBRERO"] = Tot_Feb;
            Fila["MARZO"] = Tot_Mar;
            Fila["ABRIL"] = Tot_Abr;
            Fila["MAYO"] = Tot_May;
            Fila["JUNIO"] = Tot_Jun;
            Fila["JULIO"] = Tot_Jul;
            Fila["AGOSTO"] = Tot_Ago;
            Fila["SEPTIEMBRE"] = Tot_Sep;
            Fila["OCTUBRE"] = Tot_Oct;
            Fila["NOVIEMBRE"] = Tot_Nov;
            Fila["DICIEMBRE"] = Tot_Dic;
            Fila["ACUMULADO"] = Tot_Acumulado;
            Fila["ESTIMADO"] = Tot_Estimado;
            Fila["PROYECTADO"] = Tot_Proyectado;
            Fila["DIFERENCIA"] = Tot_Diferencia;
            Fila["TIPO"] = "RUBRO";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Dt_Datos.Rows.Add(Fila);

            Tot_Imp_Ene += Tot_Ene;
            Tot_Imp_Feb += Tot_Feb;
            Tot_Imp_Mar += Tot_Mar;
            Tot_Imp_Abr += Tot_Abr;
            Tot_Imp_May += Tot_May;
            Tot_Imp_Jun += Tot_Jun;
            Tot_Imp_Jul += Tot_Jul;
            Tot_Imp_Ago += Tot_Ago;
            Tot_Imp_Sep += Tot_Sep;
            Tot_Imp_Oct += Tot_Oct;
            Tot_Imp_Nov += Tot_Nov;
            Tot_Imp_Dic += Tot_Dic;
            Tot_Imp_Acumulado += Tot_Acumulado;
            Tot_Imp_Estimado += Tot_Estimado;
            Tot_Imp_Proyectado += Tot_Proyectado;
            Tot_Imp_Diferencia += Tot_Diferencia;

            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL INGRESOS";
            Fila["ENERO"] = Tot_Imp_Ene;
            Fila["FEBRERO"] = Tot_Imp_Feb;
            Fila["MARZO"] = Tot_Imp_Mar;
            Fila["ABRIL"] = Tot_Imp_Abr;
            Fila["MAYO"] = Tot_Imp_May;
            Fila["JUNIO"] = Tot_Imp_Jun;
            Fila["JULIO"] = Tot_Imp_Jul;
            Fila["AGOSTO"] = Tot_Imp_Ago;
            Fila["SEPTIEMBRE"] = Tot_Imp_Sep;
            Fila["OCTUBRE"] = Tot_Imp_Oct;
            Fila["NOVIEMBRE"] = Tot_Imp_Nov;
            Fila["DICIEMBRE"] = Tot_Imp_Dic;
            Fila["ACUMULADO"] = Tot_Imp_Acumulado;
            Fila["ESTIMADO"] = Tot_Imp_Estimado;
            Fila["PROYECTADO"] = Tot_Imp_Proyectado;
            Fila["DIFERENCIA"] = Tot_Imp_Diferencia;
            Fila["TIPO"] = "TOTAL";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Dt_Datos.Rows.Add(Fila);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar los datos del pronostico de ingresos: Error[" + Ex.Message + "]");
        }
        return Dt_Datos;
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_Rpt_Ingresos
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Rpt_Ingresos(DataTable Dt_Datos, int Renglones, String Mes, String Gpo_Dep, String Dep)
    {
        String FF = String.Empty;
        Double Cantidad = 0.00;
        if (Cmb_FF.SelectedIndex > 0)
        {
            FF = Cmb_FF.SelectedItem.Text.Trim().Substring(0, Cmb_FF.SelectedItem.Text.Trim().IndexOf(" ")).ToUpper();
        }
        else
        {
            FF = "GLOBAL";
        }
        String Ruta_Archivo = "Reporte_Ingresos_Recaudado_" + FF + "_" + Cmb_Anio.SelectedItem.Value.Trim() + ".xls";
        WorksheetCell Celda = new WorksheetCell();
        try
        {
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "Reporte Ingresos Recaudado";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //******************************************** Hojas del libro ******************************************************//
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Reporte Ingresos Recaudado");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_1;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1;

            #region(Estilos)
            //******************************************** Estilos del libro ******************************************************//
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Negritas = Libro.Styles.Add("BodyStyleBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas = Libro.Styles.Add("BodyStyleCantBold");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("HeaderStyleTitulo");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo1 = Libro.Styles.Add("HeaderStyleTitulo1");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo2 = Libro.Styles.Add("HeaderStyleTitulo2");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo3 = Libro.Styles.Add("HeaderStyleTitulo3");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas = Libro.Styles.Add("BodyStyleTotBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("BodyTotalStyle");

            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Neg = Libro.Styles.Add("BodyStyleCant_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas_Neg = Libro.Styles.Add("BodyStyleCantBold_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas_Neg = Libro.Styles.Add("BodyStyleTotBold_Neg");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 9;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "LightGray";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Titulo.Font.FontName = "Tahoma";
            Estilo_Titulo.Font.Size = 12;
            Estilo_Titulo.Font.Bold = true;
            Estilo_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo.Font.Color = "blue";
            Estilo_Titulo.Interior.Color = "white";
            Estilo_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo1.Font.FontName = "Tahoma";
            Estilo_Titulo1.Font.Size = 12;
            Estilo_Titulo1.Font.Bold = true;
            Estilo_Titulo1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo1.Font.Color = "#000000";
            Estilo_Titulo1.Interior.Color = "white";
            Estilo_Titulo1.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo2.Font.FontName = "Tahoma";
            Estilo_Titulo2.Font.Size = 12;
            Estilo_Titulo2.Font.Bold = true;
            Estilo_Titulo2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo2.Font.Color = "#000000";
            Estilo_Titulo2.Interior.Color = "white";
            Estilo_Titulo2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo3.Font.FontName = "Tahoma";
            Estilo_Titulo3.Font.Size = 12;
            Estilo_Titulo3.Font.Bold = true;
            Estilo_Titulo3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo3.Font.Color = "#000000";
            Estilo_Titulo3.Interior.Color = "white";
            Estilo_Titulo3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Contenido_Negritas.Font.FontName = "Tahoma";
            Estilo_Contenido_Negritas.Font.Size = 9;
            Estilo_Contenido_Negritas.Font.Bold = true;
            Estilo_Contenido_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Negritas.Interior.Color = "White";
            Estilo_Contenido_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad.Font.FontName = "Tahoma";
            Estilo_Cantidad.Font.Size = 9;
            Estilo_Cantidad.Font.Bold = false;
            Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad.Font.Color = "#000000";
            Estilo_Cantidad.Interior.Color = "White";
            Estilo_Cantidad.NumberFormat = "#,###,##0.00";
            Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Neg.Font.FontName = "Tahoma";
            Estilo_Cantidad_Neg.Font.Size = 9;
            Estilo_Cantidad_Neg.Font.Bold = false;
            Estilo_Cantidad_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Neg.Font.Color = "red";
            Estilo_Cantidad_Neg.Interior.Color = "White";
            Estilo_Cantidad_Neg.NumberFormat = "#,###,##0.00";
            Estilo_Cantidad_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas.Font.Size = 9;
            Estilo_Cantidad_Negritas.Font.Bold = true;
            Estilo_Cantidad_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas.Font.Color = "#000000";
            Estilo_Cantidad_Negritas.Interior.Color = "White";
            Estilo_Cantidad_Negritas.NumberFormat = "#,###,##0.00";
            Estilo_Cantidad_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas_Neg.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas_Neg.Font.Size = 9;
            Estilo_Cantidad_Negritas_Neg.Font.Bold = true;
            Estilo_Cantidad_Negritas_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas_Neg.Font.Color = "red";
            Estilo_Cantidad_Negritas_Neg.Interior.Color = "White";
            Estilo_Cantidad_Negritas_Neg.NumberFormat = "#,###,##0.00";
            Estilo_Cantidad_Negritas_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas.Font.FontName = "Tahoma";
            Estilo_Total_Negritas.Font.Size = 9;
            Estilo_Total_Negritas.Font.Bold = true;
            Estilo_Total_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas.Font.Color = "#000000";
            Estilo_Total_Negritas.Interior.Color = "Yellow";
            Estilo_Total_Negritas.NumberFormat = "#,###,##0.00";
            Estilo_Total_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas_Neg.Font.FontName = "Tahoma";
            Estilo_Total_Negritas_Neg.Font.Size = 9;
            Estilo_Total_Negritas_Neg.Font.Bold = true;
            Estilo_Total_Negritas_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas_Neg.Font.Color = "red";
            Estilo_Total_Negritas_Neg.Interior.Color = "Yellow";
            Estilo_Total_Negritas_Neg.NumberFormat = "#,###,##0.00";
            Estilo_Total_Negritas_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total.Font.FontName = "Tahoma";
            Estilo_Total.Font.Size = 9;
            Estilo_Total.Font.Bold = true;
            Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Total.Font.Color = "#000000";
            Estilo_Total.Interior.Color = "Yellow";
            Estilo_Total.NumberFormat = "#,###,##0.00";
            Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            #endregion
            
            //******************************************** Columnas de las hojas ******************************************************//
            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Enero.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Febrero.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Marzo.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Abril.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Mayo.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Junio.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Julio.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Agosto.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Septiembre.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Octubre.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Noviembre.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Diciembre.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//acumulado.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//estimado.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//proyectado.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//diferencia.

            //se llena el encabezado principal de la hoja uno
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO ");
            Celda.MergeAcross = 4+Renglones; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Gpo_Dep.ToUpper());
            Celda.MergeAcross = 4 + Renglones; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo1";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Dep.ToUpper());
            Celda.MergeAcross = 4 + Renglones; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo2";
            Renglon1 = Hoja1.Table.Rows.Add();

            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add("PRONÓSTICO DE INGRESOS RECAUDADO EN EL MES DE " + Mes);
            Celda.MergeAcross = 4 + Renglones; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo3";
            Renglon1 = Hoja1.Table.Rows.Add();
            Renglon1 = Hoja1.Table.Rows.Add();

            //**************************************** Agregamos los datos al reporte ***********************************************//
            if (Dt_Datos is DataTable)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    foreach (DataColumn Columna in Dt_Datos.Columns)
                    {
                        if (Columna is DataColumn)
                        {
                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ELABORO")
                            {
                                if (Columna.ColumnName.Equals("CONCEPTO"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                                }
                                else if (Columna.ColumnName.Equals("ACUMULADO"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName + " HASTA " + Mes , "HeaderStyle"));
                                }
                                else if (Columna.ColumnName.Equals("DIFERENCIA"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName + " ENTRE PROYECTADO A DICIEMBRE Vs. PRONOSTICO", "HeaderStyle"));
                                }
                                else if (Columna.ColumnName.Equals("ESTIMADO"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO " + Cmb_Anio.SelectedItem.Value.Trim() + " META POR CUMPLIR", "HeaderStyle"));
                                }
                                else if (Columna.ColumnName.Equals("PROYECTADO"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO " + Columna.ColumnName + " A DICIEMBRE", "HeaderStyle"));
                                }
                                else
                                {
                                    switch (Mes)
                                    {
                                        case "ENERO":
                                            if (Columna.ColumnName.Equals("ENERO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "FEBRERO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "MARZO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "ABRIL":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "MAYO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "JUNIO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "JULIO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                || Columna.ColumnName.Equals("JULIO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "AGOSTO":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "SEPTIEMBRE":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "OCTUBRE":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "NOVIEMBRE":
                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE")
                                                | Columna.ColumnName.Equals("NOVIEMBRE"))
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            }
                                            break;
                                        case "DICIEMBRE":
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("RECAUDADO " + Columna.ColumnName, "HeaderStyle"));
                                            break;
                                    }
                                }
                            }
                        }
                    }


                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Renglon1 = Hoja1.Table.Rows.Add();

                        if (Dr["TIPO"].ToString().Trim().Equals("RUBRO") || Dr["TIPO"].ToString().Trim().Equals("TIPO_ING")
                            || Dr["TIPO"].ToString().Trim().Equals("CLASE") || Dr["TIPO"].ToString().Trim().Equals("CONCEPTO"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                    }
                                    else 
                                    {
                                        if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ELABORO")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString().Trim()) ? "0" : Dr[Columna.ColumnName]);
                                            if ( Columna.ColumnName.Equals("ACUMULADO") || Columna.ColumnName.Equals("DIFERENCIA")
                                                || Columna.ColumnName.Equals("ESTIMADO") || Columna.ColumnName.Equals("PROYECTADO"))
                                            {
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                }
                                                else
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                }
                                            }
                                            else
                                            {
                                                switch (Mes)
                                                {
                                                    case "ENERO":
                                                        if (Columna.ColumnName.Equals("ENERO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "FEBRERO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "MARZO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "ABRIL":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "MAYO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "JUNIO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "JULIO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "AGOSTO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "SEPTIEMBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "OCTUBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "NOVIEMBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE")
                                                            | Columna.ColumnName.Equals("NOVIEMBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "DICIEMBRE":
                                                        if (Cantidad >= 0)
                                                        {
                                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold"));
                                                        }
                                                        else
                                                        {
                                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCantBold_Neg"));
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                    }
                                    else
                                    {
                                        if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ELABORO")
                                        {
                                            Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString().Trim()) ? "0" : Dr[Columna.ColumnName]);
                                            if (Columna.ColumnName.Equals("ACUMULADO") || Columna.ColumnName.Equals("DIFERENCIA")
                                                || Columna.ColumnName.Equals("ESTIMADO") || Columna.ColumnName.Equals("PROYECTADO"))
                                            {
                                                if (Cantidad >= 0)
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                }
                                                else
                                                {
                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                }
                                            }
                                            else
                                            {
                                                switch (Mes)
                                                {
                                                    case "ENERO":
                                                        if (Columna.ColumnName.Equals("ENERO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "FEBRERO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "MARZO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "ABRIL":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "MAYO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "JUNIO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "JULIO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "AGOSTO":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "SEPTIEMBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "OCTUBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "NOVIEMBRE":
                                                        if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                            || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                            || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                            || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                            || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE")
                                                            | Columna.ColumnName.Equals("NOVIEMBRE"))
                                                        {
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                            }
                                                        }
                                                        break;
                                                    case "DICIEMBRE":
                                                        if (Cantidad >= 0)
                                                        {
                                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold"));
                                                        }
                                                        else
                                                        {
                                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleTotBold_Neg"));
                                                        }
                                                        break;
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO" && Columna.ColumnName != "ELABORO")
                                            {
                                                Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString().Trim()) ? "0" : Dr[Columna.ColumnName]);
                                                if (Columna.ColumnName.Equals("ACUMULADO") || Columna.ColumnName.Equals("DIFERENCIA")
                                                    || Columna.ColumnName.Equals("ESTIMADO") || Columna.ColumnName.Equals("PROYECTADO"))
                                                {
                                                    if (Cantidad >= 0)
                                                    {
                                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                    }
                                                    else
                                                    {
                                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                    }
                                                }
                                                else
                                                {
                                                    switch (Mes)
                                                    {
                                                        case "ENERO":
                                                            if (Columna.ColumnName.Equals("ENERO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "FEBRERO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "MARZO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "ABRIL":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "MAYO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "JUNIO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "JULIO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                                || Columna.ColumnName.Equals("JULIO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "AGOSTO":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "SEPTIEMBRE":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                                || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "OCTUBRE":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                                || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "NOVIEMBRE":
                                                            if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO")
                                                                || Columna.ColumnName.Equals("MARZO") || Columna.ColumnName.Equals("ABRIL")
                                                                || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO")
                                                                || Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO")
                                                                || Columna.ColumnName.Equals("SEPTIEMBRE") || Columna.ColumnName.Equals("OCTUBRE")
                                                                | Columna.ColumnName.Equals("NOVIEMBRE"))
                                                            {
                                                                if (Cantidad >= 0)
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                                }
                                                                else
                                                                {
                                                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                                }
                                                            }
                                                            break;
                                                        case "DICIEMBRE":
                                                            if (Cantidad >= 0)
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant"));
                                                            }
                                                            else
                                                            {
                                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Trim(), DataType.Number, "BodyStyleCant_Neg"));
                                                            }
                                                            break;
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }

                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon_1.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 4 + Renglones; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";
                }
            }

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Campos
    ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Datos_Validos = true;
        Limpiar_Controles("Error");
        Lbl_Ecabezado_Mensaje.Text = "Favor de:";
        try
        {
            if (Cmb_Anio.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                Datos_Validos = false;
            }

            return Datos_Validos;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Registros
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los registros
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Grid_Registros(DataTable Dt_Registros, String Mes)
    {
        try
        {
            if (Dt_Registros != null)
            {
                Grid_Registros.Columns[1].Visible = true;
                Grid_Registros.Columns[2].Visible = true;
                Grid_Registros.Columns[3].Visible = true;
                Grid_Registros.Columns[4].Visible = true;
                Grid_Registros.Columns[5].Visible = true;
                Grid_Registros.Columns[6].Visible = true;
                Grid_Registros.Columns[7].Visible = true;
                Grid_Registros.Columns[8].Visible = true;
                Grid_Registros.Columns[9].Visible = true;
                Grid_Registros.Columns[10].Visible = true;
                Grid_Registros.Columns[11].Visible = true;
                Grid_Registros.Columns[12].Visible = true;
                Grid_Registros.Columns[17].Visible = true;
                Grid_Registros.DataSource = Dt_Registros;
                Grid_Registros.DataBind();
                Grid_Registros.Columns[17].Visible = false;

                switch (Mes)
                { 
                    case "01":
                        Grid_Registros.Columns[2].Visible = false;
                        Grid_Registros.Columns[3].Visible = false;
                        Grid_Registros.Columns[4].Visible = false;
                        Grid_Registros.Columns[5].Visible = false;
                        Grid_Registros.Columns[6].Visible = false;
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "02":
                        Grid_Registros.Columns[3].Visible = false;
                        Grid_Registros.Columns[4].Visible = false;
                        Grid_Registros.Columns[5].Visible = false;
                        Grid_Registros.Columns[6].Visible = false;
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "03":
                        Grid_Registros.Columns[4].Visible = false;
                        Grid_Registros.Columns[5].Visible = false;
                        Grid_Registros.Columns[6].Visible = false;
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "04":
                        Grid_Registros.Columns[5].Visible = false;
                        Grid_Registros.Columns[6].Visible = false;
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "05":
                        Grid_Registros.Columns[6].Visible = false;
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "06":
                        Grid_Registros.Columns[7].Visible = false;
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "07":
                        Grid_Registros.Columns[8].Visible = false;
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "08":
                        Grid_Registros.Columns[9].Visible = false;
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "09":
                        Grid_Registros.Columns[10].Visible = false;
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "10":
                        Grid_Registros.Columns[11].Visible = false;
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                    case "11":
                        Grid_Registros.Columns[12].Visible = false;
                    break;
                } 

            }
            else
            {
                Grid_Registros.DataSource = new DataTable();
                Grid_Registros.DataBind();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo del estatus: Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
    ///DESCRIPCIÓN          : metodo para obtener el grupo dependencia para el reporte
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private String Obtener_Gpo_Dependencia(String Tipo)
    {
        Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio();
        String Dato = String.Empty;
        DataTable Dt_Datos = new DataTable();

        try
        {
            Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
            Dt_Datos = Negocios.Consultar_Gpo_Dependencia();

            if (Dt_Datos != null)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    if (Tipo.Trim().Equals("UR"))
                    {
                        Dato = Dt_Datos.Rows[0]["UR"].ToString().Trim();
                    }
                    else
                    {
                        Dato = Dt_Datos.Rows[0]["GPO_DEP"].ToString().Trim();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el grupo dependencia. Error[" + Ex.Message + "]");
        }
        return Dato;
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Agrupar_CRI
    ///DESCRIPCIÓN          : Metodo para agrupar los datos de las clases, conceptos y subconceptos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Junio/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private DataTable Agrupar_CRI(DataTable Dt_Detalles)
    {
        DataTable Dt_Agrupado = new DataTable();
        String Rubro_Ing_ID = String.Empty;
        String Tipo_Ing_ID = String.Empty;
        String Clase_Ing_ID = String.Empty;
        String Concepto_Ing_ID = String.Empty;
        Double Ene = 0.00;
        Double Feb = 0.00;
        Double Mar = 0.00;
        Double Abr = 0.00;
        Double May = 0.00;
        Double Jun = 0.00;
        Double Jul = 0.00;
        Double Ago = 0.00;
        Double Sep = 0.00;
        Double Oct = 0.00;
        Double Nov = 0.00;
        Double Dic = 0.00;
        Double Acumulado = 0.00;
        Double Estimado = 0.00;
        Double Proyectado = 0.00;
        Double Diferencia = 0.00;
        DataRow Fila;

        try
        {
            if (Dt_Detalles != null)
            {
                if (Dt_Detalles.Rows.Count > 0)
                {
                    // obtenemos solo los conceptos del datatable
                    Dt_Detalles = (from Fila_Det in Dt_Detalles.AsEnumerable()
                                   where Fila_Det.Field<String>("TIPO").Trim() == "CONCEPTO"
                                   select Fila_Det).AsDataView().ToTable();

                    //ordenamos el datatable de los detalles
                    var query = from Det in Dt_Detalles.AsEnumerable()
                                orderby Det.Field<String>("CONCEPTO")
                                select Det;

                    Dt_Detalles = query.CopyToDataTable();

                    Dt_Agrupado.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("ACUMULADO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("ESTIMADO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("PROYECTADO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("DIFERENCIA", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("TIPO", System.Type.GetType("System.String"));
                    Dt_Agrupado.Columns.Add("ELABORO", System.Type.GetType("System.String"));

                    //obtenemos la clase y el concepto
                    Rubro_Ing_ID = Dt_Detalles.Rows[0]["RUBRO_ID"].ToString().Trim();
                    Tipo_Ing_ID = Dt_Detalles.Rows[0]["TIPO_ID"].ToString().Trim();
                    Clase_Ing_ID = Dt_Detalles.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                    Concepto_Ing_ID = Dt_Detalles.Rows[0]["CONCEPTO_ING_ID"].ToString().Trim();

                    #region (Registro Rubro)
                    //CREAMOS EL REGISTRO DEL RUBRO
                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("ENERO")).Sum();

                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("FEBRERO")).Sum();

                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("MARZO")).Sum();

                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("ABRIL")).Sum();

                    May = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("MAYO")).Sum();

                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("JUNIO")).Sum();

                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("JULIO")).Sum();

                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("AGOSTO")).Sum();

                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("OCTUBRE")).Sum();

                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                select Sum.Field<Double>("ESTIMADO")).Sum();

                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                     where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                    Fila = Dt_Agrupado.NewRow();
                    Fila["CONCEPTO"] = "**** " + Dt_Detalles.Rows[0]["RUBRO_ING"].ToString().Trim();
                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                    Fila["MAYO"] = String.Format("{0:n}", May);
                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                    Fila["TIPO"] = "RUBRO";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Agrupado.Rows.Add(Fila);
                    #endregion

                    #region (Registro Tipo)
                    //CREAMOS EL REGISTRO DEL TIPO
                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("ENERO")).Sum();

                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("FEBRERO")).Sum();

                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("MARZO")).Sum();

                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("ABRIL")).Sum();

                    May = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("MAYO")).Sum();

                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("JUNIO")).Sum();

                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("JULIO")).Sum();

                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("AGOSTO")).Sum();

                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("OCTUBRE")).Sum();

                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                 && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                select Sum.Field<Double>("ESTIMADO")).Sum();

                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                    Fila = Dt_Agrupado.NewRow();
                    Fila["CONCEPTO"] = "*** " + Dt_Detalles.Rows[0]["TIPO_ING"].ToString().Trim();
                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                    Fila["MAYO"] = String.Format("{0:n}", May);
                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                    Fila["TIPO"] = "TIPO_ING";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Agrupado.Rows.Add(Fila);
                    #endregion

                    #region (Registro Clase de ingresos)
                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("ENERO")).Sum();

                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("FEBRERO")).Sum();

                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("MARZO")).Sum();

                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("ABRIL")).Sum();

                    May = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("MAYO")).Sum();

                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("JUNIO")).Sum();

                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("JULIO")).Sum();

                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("AGOSTO")).Sum();

                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("OCTUBRE")).Sum();

                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                 && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                 && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                select Sum.Field<Double>("ESTIMADO")).Sum();

                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                    Fila = Dt_Agrupado.NewRow();
                    Fila["CONCEPTO"] = "** " + Dt_Detalles.Rows[0]["CLASE_ING"].ToString().Trim();
                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                    Fila["MAYO"] = String.Format("{0:n}", May);
                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                    Fila["TIPO"] = "CLASE";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Agrupado.Rows.Add(Fila);
                    #endregion

                    #region (Registro Concepto Ingreso)
                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("ENERO")).Sum();

                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("FEBRERO")).Sum();

                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("MARZO")).Sum();

                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("ABRIL")).Sum();

                    May = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("MAYO")).Sum();

                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("JUNIO")).Sum();

                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("JULIO")).Sum();

                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("AGOSTO")).Sum();

                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("OCTUBRE")).Sum();

                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                select Sum.Field<Double>("ESTIMADO")).Sum();

                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                  && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                  && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                    Fila = Dt_Agrupado.NewRow();
                    Fila["CONCEPTO"] = "* " + Dt_Detalles.Rows[0]["CONCEPTO_ING"].ToString().Trim();
                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                    Fila["MAYO"] = String.Format("{0:n}", May);
                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                    Fila["TIPO"] = "CONCEPTO";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Agrupado.Rows.Add(Fila);
                    #endregion


                    foreach (DataRow Dr in Dt_Detalles.Rows)
                    {
                        if (Dr["RUBRO_ID"].ToString().Trim().Equals(Rubro_Ing_ID))
                        {
                            if (Dr["TIPO_ID"].ToString().Trim().Equals(Tipo_Ing_ID))
                            {
                                if (Dr["CLASE_ING_ID"].ToString().Trim().Equals(Clase_Ing_ID))
                                {
                                    if (Dr["CONCEPTO_ING_ID"].ToString().Trim().Equals(Concepto_Ing_ID))
                                    {
                                        //insertamos el subconcepto
                                        Fila = Dt_Agrupado.NewRow();
                                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                        Fila["ENERO"] = String.Format("{0:n}", Dr["ENERO"]);
                                        Fila["FEBRERO"] = String.Format("{0:n}", Dr["FEBRERO"]);
                                        Fila["MARZO"] = String.Format("{0:n}", Dr["MARZO"]);
                                        Fila["ABRIL"] = String.Format("{0:n}", Dr["ABRIL"]);
                                        Fila["MAYO"] = String.Format("{0:n}", Dr["MAYO"]);
                                        Fila["JUNIO"] = String.Format("{0:n}", Dr["JUNIO"]);
                                        Fila["JULIO"] = String.Format("{0:n}", Dr["JULIO"]);
                                        Fila["AGOSTO"] = String.Format("{0:n}", Dr["AGOSTO"]);
                                        Fila["SEPTIEMBRE"] = String.Format("{0:n}", Dr["SEPTIEMBRE"]);
                                        Fila["OCTUBRE"] = String.Format("{0:n}", Dr["OCTUBRE"]);
                                        Fila["NOVIEMBRE"] = String.Format("{0:n}", Dr["NOVIEMBRE"]);
                                        Fila["DICIEMBRE"] = String.Format("{0:n}", Dr["DICIEMBRE"]);
                                        Fila["ACUMULADO"] = String.Format("{0:n}", Dr["ACUMULADO"]);
                                        Fila["ESTIMADO"] = String.Format("{0:n}", Dr["ESTIMADO"]);
                                        Fila["PROYECTADO"] = String.Format("{0:n}", Dr["PROYECTADO"]);
                                        Fila["DIFERENCIA"] = String.Format("{0:n}", Dr["DIFERENCIA"]);
                                        Fila["TIPO"] = "SUBCONCEPTO";
                                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                        Dt_Agrupado.Rows.Add(Fila);
                                    }
                                    else //else concepto 
                                    {
                                        Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();

                                        #region (Registro Concepto Ingreso)
                                        Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("ENERO")).Sum();

                                        Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("FEBRERO")).Sum();

                                        Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("MARZO")).Sum();

                                        Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("ABRIL")).Sum();

                                        May = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("MAYO")).Sum();

                                        Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("JUNIO")).Sum();

                                        Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("JULIO")).Sum();

                                        Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("AGOSTO")).Sum();

                                        Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                        Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("OCTUBRE")).Sum();

                                        Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                        Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                               where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                               && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                               && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                               select Sum.Field<Double>("DICIEMBRE")).Sum();

                                        Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                                     where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                    && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                    && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                    && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                     select Sum.Field<Double>("ACUMULADO")).Sum();

                                        Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                                    where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                    && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                    && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                    && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                    select Sum.Field<Double>("ESTIMADO")).Sum();

                                        Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                                      where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                      && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                      && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                      && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                      select Sum.Field<Double>("PROYECTADO")).Sum();

                                        Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                                      where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                      && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                      && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                      && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                      select Sum.Field<Double>("DIFERENCIA")).Sum();

                                        Fila = Dt_Agrupado.NewRow();
                                        Fila["CONCEPTO"] = "* " + Dr["CONCEPTO_ING"].ToString().Trim();
                                        Fila["ENERO"] = String.Format("{0:n}", Ene);
                                        Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                        Fila["MARZO"] = String.Format("{0:n}", Mar);
                                        Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                        Fila["MAYO"] = String.Format("{0:n}", May);
                                        Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                        Fila["JULIO"] = String.Format("{0:n}", Jul);
                                        Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                        Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                        Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                        Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                        Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                        Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                        Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                        Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                        Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                        Fila["TIPO"] = "CONCEPTO";
                                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                        Dt_Agrupado.Rows.Add(Fila);
                                        #endregion

                                        //insertamos el subconcepto
                                        Fila = Dt_Agrupado.NewRow();
                                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                        Fila["ENERO"] = String.Format("{0:n}", Dr["ENERO"]);
                                        Fila["FEBRERO"] = String.Format("{0:n}", Dr["FEBRERO"]);
                                        Fila["MARZO"] = String.Format("{0:n}", Dr["MARZO"]);
                                        Fila["ABRIL"] = String.Format("{0:n}", Dr["ABRIL"]);
                                        Fila["MAYO"] = String.Format("{0:n}", Dr["MAYO"]);
                                        Fila["JUNIO"] = String.Format("{0:n}", Dr["JUNIO"]);
                                        Fila["JULIO"] = String.Format("{0:n}", Dr["JULIO"]);
                                        Fila["AGOSTO"] = String.Format("{0:n}", Dr["AGOSTO"]);
                                        Fila["SEPTIEMBRE"] = String.Format("{0:n}", Dr["SEPTIEMBRE"]);
                                        Fila["OCTUBRE"] = String.Format("{0:n}", Dr["OCTUBRE"]);
                                        Fila["NOVIEMBRE"] = String.Format("{0:n}", Dr["NOVIEMBRE"]);
                                        Fila["DICIEMBRE"] = String.Format("{0:n}", Dr["DICIEMBRE"]);
                                        Fila["ACUMULADO"] = String.Format("{0:n}", Dr["ACUMULADO"]);
                                        Fila["ESTIMADO"] = String.Format("{0:n}", Dr["ESTIMADO"]);
                                        Fila["PROYECTADO"] = String.Format("{0:n}", Dr["PROYECTADO"]);
                                        Fila["DIFERENCIA"] = String.Format("{0:n}", Dr["DIFERENCIA"]);
                                        Fila["TIPO"] = "SUBCONCEPTO";
                                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                        Dt_Agrupado.Rows.Add(Fila);
                                    }
                                }
                                else //else clase 
                                {
                                    Clase_Ing_ID = Dr["CLASE_ING_ID"].ToString().Trim();
                                    Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();

                                    #region (Registro Clase de ingresos)
                                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("ENERO")).Sum();

                                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("FEBRERO")).Sum();

                                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("MARZO")).Sum();

                                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("ABRIL")).Sum();

                                    May = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("MAYO")).Sum();

                                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("JUNIO")).Sum();

                                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("JULIO")).Sum();

                                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("AGOSTO")).Sum();

                                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("OCTUBRE")).Sum();

                                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                 && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                 && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                                where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                select Sum.Field<Double>("ESTIMADO")).Sum();

                                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["CONCEPTO"] = "** " + Dr["CLASE_ING"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                    Fila["MAYO"] = String.Format("{0:n}", May);
                                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                    Fila["TIPO"] = "CLASE";
                                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                    Dt_Agrupado.Rows.Add(Fila);
                                    #endregion

                                    #region (Registro Concepto Ingreso)
                                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("ENERO")).Sum();

                                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("FEBRERO")).Sum();

                                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("MARZO")).Sum();

                                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("ABRIL")).Sum();

                                    May = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("MAYO")).Sum();

                                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("JUNIO")).Sum();

                                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("JULIO")).Sum();

                                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("AGOSTO")).Sum();

                                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("OCTUBRE")).Sum();

                                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                           && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                           && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                                    Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                                 where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                 select Sum.Field<Double>("ACUMULADO")).Sum();

                                    Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                                where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                select Sum.Field<Double>("ESTIMADO")).Sum();

                                    Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                  && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                  select Sum.Field<Double>("PROYECTADO")).Sum();

                                    Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                                  where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                                  && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                                  && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                                  && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                                  select Sum.Field<Double>("DIFERENCIA")).Sum();

                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["CONCEPTO"] = "* " + Dr["CONCEPTO_ING"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                    Fila["MAYO"] = String.Format("{0:n}", May);
                                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                    Fila["TIPO"] = "CONCEPTO";
                                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                    Dt_Agrupado.Rows.Add(Fila);
                                    #endregion

                                    //insertamos el subconcepto
                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:n}", Dr["ENERO"]);
                                    Fila["FEBRERO"] = String.Format("{0:n}", Dr["FEBRERO"]);
                                    Fila["MARZO"] = String.Format("{0:n}", Dr["MARZO"]);
                                    Fila["ABRIL"] = String.Format("{0:n}", Dr["ABRIL"]);
                                    Fila["MAYO"] = String.Format("{0:n}", Dr["MAYO"]);
                                    Fila["JUNIO"] = String.Format("{0:n}", Dr["JUNIO"]);
                                    Fila["JULIO"] = String.Format("{0:n}", Dr["JULIO"]);
                                    Fila["AGOSTO"] = String.Format("{0:n}", Dr["AGOSTO"]);
                                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Dr["SEPTIEMBRE"]);
                                    Fila["OCTUBRE"] = String.Format("{0:n}", Dr["OCTUBRE"]);
                                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Dr["NOVIEMBRE"]);
                                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dr["DICIEMBRE"]);
                                    Fila["ACUMULADO"] = String.Format("{0:n}", Dr["ACUMULADO"]);
                                    Fila["ESTIMADO"] = String.Format("{0:n}", Dr["ESTIMADO"]);
                                    Fila["PROYECTADO"] = String.Format("{0:n}", Dr["PROYECTADO"]);
                                    Fila["DIFERENCIA"] = String.Format("{0:n}", Dr["DIFERENCIA"]);
                                    Fila["TIPO"] = "SUBCONCEPTO";
                                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                    Dt_Agrupado.Rows.Add(Fila);
                                }
                            } //else tipo
                            else
                            {
                                Tipo_Ing_ID = Dr["TIPO_ID"].ToString().Trim();
                                Clase_Ing_ID = Dr["CLASE_ING_ID"].ToString().Trim();
                                Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();

                                #region (Registro Tipo)
                                //CREAMOS EL REGISTRO DEL TIPO
                                Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("ENERO")).Sum();

                                Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("FEBRERO")).Sum();

                                Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("MARZO")).Sum();

                                Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("ABRIL")).Sum();

                                May = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("MAYO")).Sum();

                                Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("JUNIO")).Sum();

                                Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("JULIO")).Sum();

                                Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("AGOSTO")).Sum();

                                Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("OCTUBRE")).Sum();

                                Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       select Sum.Field<Double>("DICIEMBRE")).Sum();

                                Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                             where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                             && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                             select Sum.Field<Double>("ACUMULADO")).Sum();

                                Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                            where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                            && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                            select Sum.Field<Double>("ESTIMADO")).Sum();

                                Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              select Sum.Field<Double>("PROYECTADO")).Sum();

                                Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              select Sum.Field<Double>("DIFERENCIA")).Sum();

                                Fila = Dt_Agrupado.NewRow();
                                Fila["CONCEPTO"] = "*** " + Dr["TIPO_ING"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:n}", Ene);
                                Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                Fila["MARZO"] = String.Format("{0:n}", Mar);
                                Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                Fila["MAYO"] = String.Format("{0:n}", May);
                                Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                Fila["JULIO"] = String.Format("{0:n}", Jul);
                                Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                Fila["TIPO"] = "TIPO_ING";
                                Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Dt_Agrupado.Rows.Add(Fila);
                                #endregion

                                #region (Registro Clase de ingresos)
                                Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("ENERO")).Sum();

                                Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("FEBRERO")).Sum();

                                Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("MARZO")).Sum();

                                Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("ABRIL")).Sum();

                                May = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("MAYO")).Sum();

                                Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("JUNIO")).Sum();

                                Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("JULIO")).Sum();

                                Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("AGOSTO")).Sum();

                                Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("OCTUBRE")).Sum();

                                Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("DICIEMBRE")).Sum();

                                Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                             where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                             && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                             && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                             select Sum.Field<Double>("ACUMULADO")).Sum();

                                Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                            where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                            && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                            && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                            select Sum.Field<Double>("ESTIMADO")).Sum();

                                Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                              select Sum.Field<Double>("PROYECTADO")).Sum();

                                Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                              select Sum.Field<Double>("DIFERENCIA")).Sum();

                                Fila = Dt_Agrupado.NewRow();
                                Fila["CONCEPTO"] = "** " + Dr["CLASE_ING"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:n}", Ene);
                                Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                Fila["MARZO"] = String.Format("{0:n}", Mar);
                                Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                Fila["MAYO"] = String.Format("{0:n}", May);
                                Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                Fila["JULIO"] = String.Format("{0:n}", Jul);
                                Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                Fila["TIPO"] = "CLASE";
                                Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Dt_Agrupado.Rows.Add(Fila);
                                #endregion

                                #region (Registro Concepto Ingreso)
                                Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("ENERO")).Sum();

                                Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("FEBRERO")).Sum();

                                Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("MARZO")).Sum();

                                Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("ABRIL")).Sum();

                                May = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("MAYO")).Sum();

                                Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("JUNIO")).Sum();

                                Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("JULIO")).Sum();

                                Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("AGOSTO")).Sum();

                                Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("OCTUBRE")).Sum();

                                Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                       && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                       && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("DICIEMBRE")).Sum();

                                Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                             where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                            && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                            && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                            && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                             select Sum.Field<Double>("ACUMULADO")).Sum();

                                Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                            where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                            && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                            && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                            && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                            select Sum.Field<Double>("ESTIMADO")).Sum();

                                Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                              && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                              select Sum.Field<Double>("PROYECTADO")).Sum();

                                Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                              where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                              && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                              && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                              && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                              select Sum.Field<Double>("DIFERENCIA")).Sum();

                                Fila = Dt_Agrupado.NewRow();
                                Fila["CONCEPTO"] = "* " + Dr["CONCEPTO_ING"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:n}", Ene);
                                Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                                Fila["MARZO"] = String.Format("{0:n}", Mar);
                                Fila["ABRIL"] = String.Format("{0:n}", Abr);
                                Fila["MAYO"] = String.Format("{0:n}", May);
                                Fila["JUNIO"] = String.Format("{0:n}", Jun);
                                Fila["JULIO"] = String.Format("{0:n}", Jul);
                                Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                                Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                                Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                                Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                                Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                                Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                                Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                                Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                                Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                                Fila["TIPO"] = "CONCEPTO";
                                Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Dt_Agrupado.Rows.Add(Fila);
                                #endregion

                                //insertamos el subconcepto
                                Fila = Dt_Agrupado.NewRow();
                                Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:n}", Dr["ENERO"]);
                                Fila["FEBRERO"] = String.Format("{0:n}", Dr["FEBRERO"]);
                                Fila["MARZO"] = String.Format("{0:n}", Dr["MARZO"]);
                                Fila["ABRIL"] = String.Format("{0:n}", Dr["ABRIL"]);
                                Fila["MAYO"] = String.Format("{0:n}", Dr["MAYO"]);
                                Fila["JUNIO"] = String.Format("{0:n}", Dr["JUNIO"]);
                                Fila["JULIO"] = String.Format("{0:n}", Dr["JULIO"]);
                                Fila["AGOSTO"] = String.Format("{0:n}", Dr["AGOSTO"]);
                                Fila["SEPTIEMBRE"] = String.Format("{0:n}", Dr["SEPTIEMBRE"]);
                                Fila["OCTUBRE"] = String.Format("{0:n}", Dr["OCTUBRE"]);
                                Fila["NOVIEMBRE"] = String.Format("{0:n}", Dr["NOVIEMBRE"]);
                                Fila["DICIEMBRE"] = String.Format("{0:n}", Dr["DICIEMBRE"]);
                                Fila["ACUMULADO"] = String.Format("{0:n}", Dr["ACUMULADO"]);
                                Fila["ESTIMADO"] = String.Format("{0:n}", Dr["ESTIMADO"]);
                                Fila["PROYECTADO"] = String.Format("{0:n}", Dr["PROYECTADO"]);
                                Fila["DIFERENCIA"] = String.Format("{0:n}", Dr["DIFERENCIA"]);
                                Fila["TIPO"] = "SUBCONCEPTO";
                                Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                                Dt_Agrupado.Rows.Add(Fila);
                            }
                        }
                        else //else rubro 
                        {
                            Rubro_Ing_ID = Dr["RUBRO_ID"].ToString().Trim();
                            Tipo_Ing_ID = Dr["TIPO_ID"].ToString().Trim();
                            Clase_Ing_ID = Dr["CLASE_ING_ID"].ToString().Trim();
                            Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();

                            #region (Registro Rubro)
                            //CREAMOS EL REGISTRO DEL RUBRO
                            Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("ENERO")).Sum();

                            Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("FEBRERO")).Sum();

                            Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("MARZO")).Sum();

                            Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("ABRIL")).Sum();

                            May = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("MAYO")).Sum();

                            Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("JUNIO")).Sum();

                            Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("JULIO")).Sum();

                            Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("AGOSTO")).Sum();

                            Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                            Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("OCTUBRE")).Sum();

                            Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("NOVIEMBRE")).Sum();

                            Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   select Sum.Field<Double>("DICIEMBRE")).Sum();

                            Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                         where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                         select Sum.Field<Double>("ACUMULADO")).Sum();

                            Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                        where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                        select Sum.Field<Double>("ESTIMADO")).Sum();

                            Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          select Sum.Field<Double>("PROYECTADO")).Sum();

                            Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          select Sum.Field<Double>("DIFERENCIA")).Sum();

                            Fila = Dt_Agrupado.NewRow();
                            Fila["CONCEPTO"] = "**** " + Dr["RUBRO_ING"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Ene);
                            Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                            Fila["MARZO"] = String.Format("{0:n}", Mar);
                            Fila["ABRIL"] = String.Format("{0:n}", Abr);
                            Fila["MAYO"] = String.Format("{0:n}", May);
                            Fila["JUNIO"] = String.Format("{0:n}", Jun);
                            Fila["JULIO"] = String.Format("{0:n}", Jul);
                            Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                            Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                            Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                            Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                            Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                            Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                            Fila["TIPO"] = "RUBRO";
                            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                            Dt_Agrupado.Rows.Add(Fila);
                            #endregion

                            #region (Registro Tipo)
                            //CREAMOS EL REGISTRO DEL TIPO
                            Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("ENERO")).Sum();

                            Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("FEBRERO")).Sum();

                            Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("MARZO")).Sum();

                            Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("ABRIL")).Sum();

                            May = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("MAYO")).Sum();

                            Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("JUNIO")).Sum();

                            Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("JULIO")).Sum();

                            Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("AGOSTO")).Sum();

                            Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                            Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("OCTUBRE")).Sum();

                            Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("NOVIEMBRE")).Sum();

                            Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   select Sum.Field<Double>("DICIEMBRE")).Sum();

                            Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                         where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                         && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                         select Sum.Field<Double>("ACUMULADO")).Sum();

                            Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                        where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                        && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                        select Sum.Field<Double>("ESTIMADO")).Sum();

                            Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          select Sum.Field<Double>("PROYECTADO")).Sum();

                            Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          select Sum.Field<Double>("DIFERENCIA")).Sum();

                            Fila = Dt_Agrupado.NewRow();
                            Fila["CONCEPTO"] = "*** " + Dr["TIPO_ING"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Ene);
                            Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                            Fila["MARZO"] = String.Format("{0:n}", Mar);
                            Fila["ABRIL"] = String.Format("{0:n}", Abr);
                            Fila["MAYO"] = String.Format("{0:n}", May);
                            Fila["JUNIO"] = String.Format("{0:n}", Jun);
                            Fila["JULIO"] = String.Format("{0:n}", Jul);
                            Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                            Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                            Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                            Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                            Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                            Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                            Fila["TIPO"] = "TIPO_ING";
                            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                            Dt_Agrupado.Rows.Add(Fila);
                            #endregion

                            #region (Registro Clase de ingresos)
                            Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("ENERO")).Sum();

                            Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("FEBRERO")).Sum();

                            Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("MARZO")).Sum();

                            Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("ABRIL")).Sum();

                            May = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("MAYO")).Sum();

                            Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("JUNIO")).Sum();

                            Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("JULIO")).Sum();

                            Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("AGOSTO")).Sum();

                            Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                            Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("OCTUBRE")).Sum();

                            Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("NOVIEMBRE")).Sum();

                            Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   select Sum.Field<Double>("DICIEMBRE")).Sum();

                            Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                         where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                         && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                         && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                         select Sum.Field<Double>("ACUMULADO")).Sum();

                            Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                        where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                        && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                        && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                        select Sum.Field<Double>("ESTIMADO")).Sum();

                            Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                          select Sum.Field<Double>("PROYECTADO")).Sum();

                            Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                          select Sum.Field<Double>("DIFERENCIA")).Sum();

                            Fila = Dt_Agrupado.NewRow();
                            Fila["CONCEPTO"] = "** " + Dr["CLASE_ING"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Ene);
                            Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                            Fila["MARZO"] = String.Format("{0:n}", Mar);
                            Fila["ABRIL"] = String.Format("{0:n}", Abr);
                            Fila["MAYO"] = String.Format("{0:n}", May);
                            Fila["JUNIO"] = String.Format("{0:n}", Jun);
                            Fila["JULIO"] = String.Format("{0:n}", Jul);
                            Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                            Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                            Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                            Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                            Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                            Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                            Fila["TIPO"] = "CLASE";
                            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                            Dt_Agrupado.Rows.Add(Fila);
                            #endregion

                            #region (Registro Concepto Ingreso)
                            Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("ENERO")).Sum();

                            Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("FEBRERO")).Sum();

                            Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("MARZO")).Sum();

                            Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("ABRIL")).Sum();

                            May = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("MAYO")).Sum();

                            Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("JUNIO")).Sum();

                            Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("JULIO")).Sum();

                            Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("AGOSTO")).Sum();

                            Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                            Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("OCTUBRE")).Sum();

                            Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("NOVIEMBRE")).Sum();

                            Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                   where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                   && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                   && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                   && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                   select Sum.Field<Double>("DICIEMBRE")).Sum();

                            Acumulado = (from Sum in Dt_Detalles.AsEnumerable()
                                         where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                        && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                        && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                        && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                         select Sum.Field<Double>("ACUMULADO")).Sum();

                            Estimado = (from Sum in Dt_Detalles.AsEnumerable()
                                        where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                        && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                        && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                        && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                        select Sum.Field<Double>("ESTIMADO")).Sum();

                            Proyectado = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                          && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                          select Sum.Field<Double>("PROYECTADO")).Sum();

                            Diferencia = (from Sum in Dt_Detalles.AsEnumerable()
                                          where Sum.Field<String>("RUBRO_ID").Trim() == Rubro_Ing_ID.Trim()
                                          && Sum.Field<String>("TIPO_ID").Trim() == Tipo_Ing_ID.Trim()
                                          && Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                          && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                          select Sum.Field<Double>("DIFERENCIA")).Sum();

                            Fila = Dt_Agrupado.NewRow();
                            Fila["CONCEPTO"] = "* " + Dr["CONCEPTO_ING"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Ene);
                            Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                            Fila["MARZO"] = String.Format("{0:n}", Mar);
                            Fila["ABRIL"] = String.Format("{0:n}", Abr);
                            Fila["MAYO"] = String.Format("{0:n}", May);
                            Fila["JUNIO"] = String.Format("{0:n}", Jun);
                            Fila["JULIO"] = String.Format("{0:n}", Jul);
                            Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                            Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                            Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                            Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                            Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                            Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                            Fila["TIPO"] = "CONCEPTO";
                            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                            Dt_Agrupado.Rows.Add(Fila);
                            #endregion

                            //insertamos el subconcepto
                            Fila = Dt_Agrupado.NewRow();
                            Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Dr["ENERO"]);
                            Fila["FEBRERO"] = String.Format("{0:n}", Dr["FEBRERO"]);
                            Fila["MARZO"] = String.Format("{0:n}", Dr["MARZO"]);
                            Fila["ABRIL"] = String.Format("{0:n}", Dr["ABRIL"]);
                            Fila["MAYO"] = String.Format("{0:n}", Dr["MAYO"]);
                            Fila["JUNIO"] = String.Format("{0:n}", Dr["JUNIO"]);
                            Fila["JULIO"] = String.Format("{0:n}", Dr["JULIO"]);
                            Fila["AGOSTO"] = String.Format("{0:n}", Dr["AGOSTO"]);
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Dr["SEPTIEMBRE"]);
                            Fila["OCTUBRE"] = String.Format("{0:n}", Dr["OCTUBRE"]);
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Dr["NOVIEMBRE"]);
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Dr["DICIEMBRE"]);
                            Fila["ACUMULADO"] = String.Format("{0:n}", Dr["ACUMULADO"]);
                            Fila["ESTIMADO"] = String.Format("{0:n}", Dr["ESTIMADO"]);
                            Fila["PROYECTADO"] = String.Format("{0:n}", Dr["PROYECTADO"]);
                            Fila["DIFERENCIA"] = String.Format("{0:n}", Dr["DIFERENCIA"]);
                            Fila["TIPO"] = "SUBCONCEPTO";
                            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                            Dt_Agrupado.Rows.Add(Fila);
                        }
                    }

                    #region (Registro Total)
                    //CREAMOS EL REGISTRO DEL TIPO
                    Ene = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("ENERO"));
                    Feb = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("FEBRERO"));
                    Mar = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("MARZO"));
                    Abr = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("ABRIL"));
                    May = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("MAYO"));
                    Jun = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("JUNIO"));
                    Jul = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("JULIO"));
                    Ago = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("AGOSTO"));
                    Sep = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("SEPTIEMBRE"));
                    Oct = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("OCTUBRE"));
                    Nov = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("NOVIEMBRE"));
                    Dic = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("DICIEMBRE"));
                    Acumulado = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("ACUMULADO"));
                    Estimado = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("ESTIMADO"));
                    Proyectado = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("PROYECTADO"));
                    Diferencia = Dt_Detalles.AsEnumerable().Sum(x => x.Field<Double>("DIFERENCIA"));

                    Fila = Dt_Agrupado.NewRow();
                    Fila["CONCEPTO"] = " ";
                    Fila["ENERO"] = String.Format("{0:n}", Ene);
                    Fila["FEBRERO"] = String.Format("{0:n}", Feb);
                    Fila["MARZO"] = String.Format("{0:n}", Mar);
                    Fila["ABRIL"] = String.Format("{0:n}", Abr);
                    Fila["MAYO"] = String.Format("{0:n}", May);
                    Fila["JUNIO"] = String.Format("{0:n}", Jun);
                    Fila["JULIO"] = String.Format("{0:n}", Jul);
                    Fila["AGOSTO"] = String.Format("{0:n}", Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                    Fila["OCTUBRE"] = String.Format("{0:n}", Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:n}", Dic);
                    Fila["ACUMULADO"] = String.Format("{0:n}", Acumulado);
                    Fila["ESTIMADO"] = String.Format("{0:n}", Estimado);
                    Fila["PROYECTADO"] = String.Format("{0:n}", Proyectado);
                    Fila["DIFERENCIA"] = String.Format("{0:n}", Diferencia);
                    Fila["TIPO"] = "TOTAL";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Agrupado.Rows.Add(Fila);
                    #endregion
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al agrupar los datos del CRI. Error[" + Ex.Message + "]");
        }
        return Dt_Agrupado;
    }
    #endregion

    #region EVENTOS

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 15/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 28/Diciembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Generar_Click(object sender, EventArgs e)
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;
        String Mes = String.Format("{0:MM}", DateTime.Now);
        Int32 No_Mes = Convert.ToInt32(String.Format("{0:MM}", DateTime.Now));
        String Nombre_Mes = String.Empty;
        String Dependencia = String.Empty;
        String Gpo_Dependencia = String.Empty;

        try
        {
            if (Validar_Campos())
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();

                if (Dt_Pronostico != null)
                {
                    if (Dt_Pronostico.Rows.Count > 0)
                    {
                        Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                        Dt_Pronostico = Agrupar_CRI(Dt_Pronostico);
                        Llenar_Grid_Registros(Dt_Pronostico, Mes);

                        switch (Mes) {
                            case "01": Nombre_Mes = "ENERO"; break;
                            case "02": Nombre_Mes = "FEBRERO"; break;
                            case "03": Nombre_Mes = "MARZO"; break;
                            case "04": Nombre_Mes = "ABRIL"; break;
                            case "05": Nombre_Mes = "MAYO"; break;
                            case "06": Nombre_Mes = "JUNIO"; break;
                            case "07": Nombre_Mes = "JULIO"; break;
                            case "08": Nombre_Mes = "AGOSTO"; break;
                            case "09": Nombre_Mes = "SEPTIEMBRE"; break;
                            case "10": Nombre_Mes = "OCTUBRE"; break;
                            case "11": Nombre_Mes = "NOVIEMBRE"; break;
                            case "12": Nombre_Mes = "DICIEMBRE"; break;
                        }

                        //obtenemos los datos de la dependencia y del grupo dependencia de la persona que obtendra  el reporte
                        Dependencia = Obtener_Gpo_Dependencia("UR");
                        Gpo_Dependencia = Obtener_Gpo_Dependencia("GPO_DEP");

                        Generar_Rpt_Ingresos(Dt_Pronostico, No_Mes, Nombre_Mes, Gpo_Dependencia, Dependencia);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;
        DataSet Ds_Registros = null;
        String Mes = String.Format("{0:MM}", DateTime.Now);

        try
        {
            if (Validar_Campos())
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();

                if (Dt_Pronostico != null)
                {
                    if (Dt_Pronostico.Rows.Count > 0)
                    {
                        Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                        Llenar_Grid_Registros(Dt_Pronostico, Mes);

                        Ds_Registros = new DataSet();
                        Dt_Pronostico.TableName = "Dt_Registros";
                        Ds_Registros.Tables.Add(Dt_Pronostico.Copy());
                        Generar_Reporte(ref Ds_Registros, "Cr_Rpt_Psp_Analitico_Presupuesto_Ing.rpt",
                            "Rpt_Analitico_Presupuesto_Ingresos" + Session.SessionID + ".pdf");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Consultar_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Consultar_Click(object sender, EventArgs e)
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;
        String Mes = String.Format("{0:MM}", DateTime.Now);

        try
        {
            if (Validar_Campos())
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();

                if (Dt_Pronostico != null)
                {
                    if (Dt_Pronostico.Rows.Count > 0)
                    {
                        Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                        Dt_Pronostico = Agrupar_CRI(Dt_Pronostico);
                        Llenar_Grid_Registros(Dt_Pronostico, Mes);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Registros_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Registros_Sorting(object sender, GridViewSortEventArgs e)
    {
        Cs_Rpt_Psp_Ingresos_Recaudados_Negocio Obj_Ingresos = new Cs_Rpt_Psp_Ingresos_Recaudados_Negocio(); //Conexion con la capa de negocios
        DataTable Grid_Registros_Sorting = new DataTable();
        DataTable Dt_Pronostico = new DataTable();
        String Mes = String.Format("{0:MM}", DateTime.Now);
        try
        {
            Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
            Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();
            Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
            Llenar_Grid_Registros(Dt_Pronostico, Mes);

            Grid_Registros_Sorting = (DataTable)Grid_Registros.DataSource;
            if (Grid_Registros_Sorting != null)
            {
                DataView Dv_Vista = new DataView(Grid_Registros_Sorting);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }

                Grid_Registros.Columns[16].Visible = true;
                Grid_Registros.DataSource = Dv_Vista;
                Grid_Registros.DataBind();
                Grid_Registros.Columns[16].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Ecabezado_Mensaje.Text = "Error al ordenar la tabla de clases. Error[" + Ex.Message + "]";
            Lbl_Mensaje_Error.Text = String.Empty;
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Double Tot = 0.00;
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Tot = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[16].Text.Trim()) ? "0" : e.Row.Cells[16].Text.Trim());

                if (e.Row.Cells[17].Text.Trim().Equals("RUBRO") || e.Row.Cells[17].Text.Trim().Equals("TIPO_ING")
                    || e.Row.Cells[17].Text.Trim().Equals("CLASE") || e.Row.Cells[17].Text.Trim().Equals("CONCEPTO"))
                {
                    e.Row.Style.Add("background-color", "#FFFF99");
                    e.Row.Style.Add("color", "black");
                }
                if (e.Row.Cells[17].Text.Trim().Equals("TOTAL"))
                {
                    e.Row.Style.Add("background-color", "#FFCC66");
                    e.Row.Style.Add("color", "black");
                }
                if (Tot < 0)
                    e.Row.Cells[16].Style.Add("color", "red");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    #endregion

    #region (Reportes)
    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en pantalla.
    ///              
    /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
}
