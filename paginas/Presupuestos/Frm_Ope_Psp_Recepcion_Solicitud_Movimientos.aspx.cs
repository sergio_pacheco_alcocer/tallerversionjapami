﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using JAPAMI.Constantes;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Recepcion_Solicitud_Movimientos : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region(Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Inicializa_controles
        ///DESCRIPCIÓN          : metodo para inicializar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Habilitar_Controles("Inicial");//Inicializa todos los controles
                Obtener_Datos_Usuario();
                Llenar_Grid_Movimientos();
                Div_Grid_Movimientos.Visible = true;
                Chk_Aceptacion.Checked = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Inicializa_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Controles
        ///DESCRIPCIÓN          : metodo para habililitar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            try
            {
                switch (Operacion)
                {
                    case "Inicial":
                        Btn_Nuevo.ToolTip = "Actualizar";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        break;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de Movimientos
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Movimientos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimiento = new DataTable(); //Variable que obtendra los datos de la consulta 
            DataTable Dt_Modificacion = new DataTable();
            DataTable Dt_Mov = new DataTable();

            try
            {
                Consulta.P_Tipo_Egreso = "MUNICIPAL";
                Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Dt_Modificacion = Consulta.Consultar_Datos_Modificaciones();

                if(Dt_Modificacion != null && Dt_Modificacion.Rows.Count > 0)
                {
                    foreach(DataRow Dr in Dt_Modificacion.Rows)
                    {
                        if (Dr["ESTATUS"].ToString().Trim().Equals("GENERADO"))
                        {
                            Consulta.P_No_Solicitud = Dr["NO_MOVIMIENTO_EGR"].ToString().Trim();
                            Hf_No_Mov.Value = Dr["NO_MOVIMIENTO_EGR"].ToString().Trim();
                            break;
                        }
                    }
                }

                Consulta.P_Tipo_Egreso = "MUNICIPAL";
                Consulta.P_Estatus = "'GENERADO'";
                Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Consulta.P_No_Solicitud = String.Empty;
                Dt_Movimiento = Consulta.Consultar_Datos_Movimientos();

                if (Dt_Movimiento != null)
                {
                    if (Dt_Movimiento.Rows.Count > 0)
                    {
                        //mostramos solo los traspasos, ampliaciones y reducciones
                        Dt_Mov = (from Fila in Dt_Movimiento.AsEnumerable()
                                  where Fila.Field<string>("TIPO_OPERACION") != "ADECUACION"
                                  select Fila).AsDataView().ToTable();

                        Dt_Movimiento = Dt_Mov;
                    }
                }

                Grid_Movimientos.Columns[1].Visible = true;
                Grid_Movimientos.Columns[5].Visible = true;
                Grid_Movimientos.DataSource = Dt_Movimiento;
                Grid_Movimientos.DataBind();
                Grid_Movimientos.Columns[1].Visible = false;
                Grid_Movimientos.Columns[5].Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Usuario
        ///DESCRIPCIÓN          : Metodo para llenar los datos el usuario
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Obtener_Datos_Usuario()
        {
            Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Boolean Administrador;
            DataTable Dt_Ramo33 = new DataTable();

            Hf_Tipo_Usuario.Value = "";

            try
            {
                //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
                Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
                Administrador = Negocio.Consultar_Rol();

                if (Administrador)
                {
                    Hf_Tipo_Usuario.Value = "Administrador";
                }
                else
                {
                    //verificamos si el usuario logueado es el administrador de ramo33
                    Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                    if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                    {
                        if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                            || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                        {
                            Hf_Tipo_Usuario.Value = "Ramo33";
                        }
                    }
                    else
                    {
                        Hf_Tipo_Usuario.Value = "Usuario";
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Atualizar_Datos
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado o rechazado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Actualizar_Datos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Int32 Indice = 0;
            Boolean Recibido = false;
            Boolean Valido = false;
            String Autorizado = String.Empty;
            try
            {
                foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                {
                    Indice++;
                    Grid_Movimientos.SelectedIndex = Indice;
                    Recibido = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                    if (Recibido)
                    {
                        Valido = true;
                    }
                }

                if (Valido)
                {
                    Negocio.P_Estatus = "RECIBIDO";
                    Negocio.P_Dt_Mov = Crear_Dt_Autorizados();
                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                    Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                    Negocio.P_No_Solicitud = Hf_No_Mov.Value.Trim();
                    Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocio.P_Tipo_Egreso = "MUNICIPAL";

                    Autorizado = Negocio.Modificar_Autorizacion_Mov();

                    if (Autorizado.Trim().Equals("SI"))
                    {
                        Inicializa_Controles();
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Text = "Favor de marcar alguna solicitud como recibida";
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Autorizados
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los datos de la actualizacion
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Autorizados()
        {
            DataTable Dt_Autorizados = new DataTable();
            Int32 Indice = 0;
            Boolean Aceptado;
            DataRow Fila;
            DataTable Dt_Comentarios = new DataTable();

            try
            {
                Dt_Autorizados.Columns.Add("SOLICITUD_ID");
                Dt_Autorizados.Columns.Add("ESTATUS");

                foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                {
                    Indice++;
                    Grid_Movimientos.SelectedIndex = Indice;

                    Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                    if (Aceptado)
                    {
                        Fila = Dt_Autorizados.NewRow();
                        Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                        Fila["ESTATUS"] = "RECIBIDO";
                        Dt_Autorizados.Rows.Add(Fila);
                    }
                }

                return Dt_Autorizados;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region(Eventos)
      #region(Botones)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Evento del boton de Actualizar
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Mov = new DataTable();

            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";

                if (Btn_Nuevo.ToolTip == "Actualizar")
                {
                    Actualizar_Datos();
                }
            }

            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Evento del boton de salir
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Salir.ToolTip == "Cancelar")
                {
                    Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
                    Grid_Movimientos.SelectedIndex = -1;
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
        ///DESCRIPCIÓN          : Evento del boton Buscar 
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Febrero/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimiento = new DataTable(); //Variable que obtendra los datos de la consulta 
            DataTable Dt_Modificacion = new DataTable();
            DataTable Dt_Mov = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                {
                    Consulta.P_Tipo_Egreso = "MUNICIPAL";
                    Consulta.P_Estatus = "'GENERADO'";
                    Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Consulta.P_No_Solicitud = String.Empty;
                    Dt_Movimiento = Consulta.Consultar_Datos_Movimientos();

                    if (Dt_Movimiento != null)
                    {
                        if (Dt_Movimiento.Rows.Count > 0)
                        {
                            //mostramos solo los traspasos, ampliaciones y reducciones
                            Dt_Mov = (from Fila in Dt_Movimiento.AsEnumerable()
                                      where Fila.Field<String>("TIPO_OPERACION") != "ADECUACION"
                                      && (
                                        Convert.ToString(Fila.Field<String>("NO_SOLICITUD")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila.Field<Decimal>("IMPORTE")).Contains(Txt_Busqueda.Text.Trim())
                                        || Convert.ToString(Fila.Field<String>("FECHA_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila.Field<String>("TIPO_OPERACION")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila.Field<String>("USUARIO_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper()))
                                      select Fila).AsDataView().ToTable();

                            Dt_Movimiento = Dt_Mov;
                        }
                    }
                    Grid_Movimientos.Columns[1].Visible = true;
                    Grid_Movimientos.Columns[5].Visible = true;
                    Grid_Movimientos.DataSource = Dt_Movimiento;
                    Grid_Movimientos.DataBind();
                    Grid_Movimientos.Columns[1].Visible = false;
                    Grid_Movimientos.Columns[5].Visible = false;
                }
                else
                {
                    Llenar_Grid_Movimientos();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en la busqueda de clases. Error[" + Ex.Message + "]");
            }
        }
      #endregion

        #region (Grid))
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Movimientos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimientos = new DataTable();
            DataTable Dt_Movimientos_Detalles = new DataTable();

            try
            {
                GridView Grid_Movimientos_Detalle = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos_Detalle");
                String Solicitud_ID = String.Empty;

                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Solicitud_ID = e.Row.Cells[1].Text.Trim();
                    Consulta.P_No_Solicitud = Solicitud_ID;
                    Dt_Movimientos_Detalles = Consulta.Consultar_Datos_Movimientos_Det();

                    Grid_Movimientos_Detalle.Columns[0].Visible = true;
                    Grid_Movimientos_Detalle.Columns[1].Visible = true;
                    Grid_Movimientos_Detalle.Columns[2].Visible = true;
                    Grid_Movimientos_Detalle.Columns[12].Visible = true;
                    Grid_Movimientos_Detalle.Columns[13].Visible = true;
                    Grid_Movimientos_Detalle.Columns[14].Visible = true;
                    Grid_Movimientos_Detalle.Columns[15].Visible = true;
                    Grid_Movimientos_Detalle.Columns[16].Visible = true;
                    Grid_Movimientos_Detalle.Columns[17].Visible = true;
                    Grid_Movimientos_Detalle.Columns[18].Visible = true;
                    Grid_Movimientos_Detalle.Columns[19].Visible = true;
                    Grid_Movimientos_Detalle.Columns[20].Visible = true;
                    Grid_Movimientos_Detalle.Columns[21].Visible = true;
                    Grid_Movimientos_Detalle.Columns[22].Visible = true;
                    Grid_Movimientos_Detalle.Columns[23].Visible = true;
                    Grid_Movimientos_Detalle.Columns[24].Visible = true;
                    Grid_Movimientos_Detalle.Columns[25].Visible = true;
                    Grid_Movimientos_Detalle.DataSource = Dt_Movimientos_Detalles;
                    Grid_Movimientos_Detalle.DataBind();
                    Grid_Movimientos_Detalle.Columns[0].Visible = false;
                    Grid_Movimientos_Detalle.Columns[1].Visible = false;
                    Grid_Movimientos_Detalle.Columns[2].Visible = false;
                    Grid_Movimientos_Detalle.Columns[12].Visible = false;
                    Grid_Movimientos_Detalle.Columns[13].Visible = false;
                    Grid_Movimientos_Detalle.Columns[14].Visible = false;
                    Grid_Movimientos_Detalle.Columns[15].Visible = false;
                    Grid_Movimientos_Detalle.Columns[16].Visible = false;
                    Grid_Movimientos_Detalle.Columns[17].Visible = false;
                    Grid_Movimientos_Detalle.Columns[18].Visible = false;
                    Grid_Movimientos_Detalle.Columns[19].Visible = false;
                    Grid_Movimientos_Detalle.Columns[20].Visible = false;
                    Grid_Movimientos_Detalle.Columns[21].Visible = false;
                    Grid_Movimientos_Detalle.Columns[22].Visible = false;
                    Grid_Movimientos_Detalle.Columns[23].Visible = false;
                    Grid_Movimientos_Detalle.Columns[25].Visible = false;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowCreated
            ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
                }
            }
        #endregion
    #endregion
}
