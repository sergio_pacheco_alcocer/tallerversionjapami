﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Autorizar_Movimiento_Presupueso_Inversiones.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Autorizar_Movimiento_Presupueso_Inversiones" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

<asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
    </asp:UpdateProgress>
    <asp:ScriptManager ID="ScriptManager1" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />                           
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
        
        
            <div id="Div_Movimiento_Presupuestal" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Autorizar Movimiento Presupuestal</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                           <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">Busqueda
                            <asp:TextBox ID="Txt_Busqueda_Movimiento_Presupuestal" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar por Estatus o Solicitud"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Movimiento_Presupuestal" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese Solicitud>" TargetControlID="Txt_Busqueda_Movimiento_Presupuestal" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Movimiento_Presupuestal" runat="server" 
                                TargetControlID="Txt_Busqueda_Movimiento_Presupuestal" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="ÑñáéíóúÁÉÍÓÚ-. ">
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_Movimiento_Presupuestal" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Buscar_Movimiento_Presupuestal_Click"/>
                        </td> 
                    </tr>
                </table>
                
                
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente" style="text-align:left;">
                     <tr id="Tr_Grid_Movimientos" runat="server">
                        <td align="center">
                            <div id="Div_Grid" runat="server" style="overflow:auto;height:320px;width:99%;vertical-align:top;border-style:outset;border-color: Silver;" >
                                <asp:GridView ID="Grid_Movimiento_Presupuestal" runat="server"  CssClass="GridView_1" Width="100%" 
                                    AutoGenerateColumns="False"  GridLines="None" 
                                    AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                    onselectedindexchanged="Grid_Movimiento_Presupuestal_SelectedIndexChanged" 
                                    onsorting="Grid_Movimiento_Presupuestal_Sorting"  >
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="3%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="NO_SOLICITUD" HeaderText="Solicitud" Visible="True" SortExpression="NO_SOLICITUD">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CODIGO1" HeaderText="Partida Origen" Visible="True" SortExpression="CODIGO1">
                                            <HeaderStyle HorizontalAlign="Left" Width="26%" />
                                            <ItemStyle HorizontalAlign="Left" Width="26%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CODIGO2" HeaderText="Partida Destino" Visible="True" SortExpression="CODIGO2">
                                            <HeaderStyle HorizontalAlign="Left" Width="26%" />
                                            <ItemStyle HorizontalAlign="Left" Width="26%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TIPO_OPERACION" HeaderText="Operacion" Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="IMPORTE" HeaderText="Importe" Visible="True" SortExpression="IMPORTE" DataFormatString="{0:n}">
                                            <HeaderStyle HorizontalAlign="Right" Width="14%" />
                                            <ItemStyle HorizontalAlign="Right" Width="14%" />
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                         </div>
                        </td>
                    </tr>
                    <tr id="Tr_Datos_Movimientos" runat="server">
                        <td>
                            <div id="Div_Datos_Generales" runat="server" style="width:97%;vertical-align:top;" >
                                <asp:Panel ID="Panel1" GroupingText="Datos Generales" runat="server">
                                    <table id="Table_Datos_Genrerales" width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:17%; text-align:left;">
                                                <asp:Label ID="Lbl_Numero_solicitud" runat="server" Text="Número de solicitud" Width="98%"  ></asp:Label>
                                            </td >
                                            <td style="width:15%; text-align:left;">
                                                <asp:TextBox ID="Txt_No_Solicitud" runat="server" ReadOnly="True"  Width="90%"></asp:TextBox>
                                            </td>
                                            <td style="width:10%; text-align:left;"> 
                                                <asp:Label ID="Lbl_Operacion" runat="server" Text=" &nbsp;&nbsp;Operación" Width="98%"  ></asp:Label>
                                            </td>
                                            <td style="width:20%; text-align:left;">
                                                <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="98%" Enabled="false">
                                                    <asp:ListItem>TRASPASO</asp:ListItem>
                                                    <asp:ListItem>AMPLIAR</asp:ListItem>
                                                    <asp:ListItem>REDUCIR</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:15%; text-align:center;">
                                                 <asp:Label ID="Lbl_Importe" runat="server" Text="&nbsp;Importe " Width="98%"  ></asp:Label>
                                            </td>
                                            <td style="width:23%; text-align:left;">
                                                <asp:TextBox ID="Txt_Importe" runat="server" Width="93%" Enabled="false"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Lbl_Justificacion_Actual" runat="server" Width="100%" Text="Justificación"></asp:Label>
                                            </td>
                                            <td colspan="5">
                                                <asp:TextBox ID="Txt_Justidicacion_Actual" runat="server" Width="98%" ReadOnly="true"  TextMode="MultiLine" MaxLength="250"
                                                    TabIndex="10"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </div>
                            <br />
                            <div ID="Div_Partida_Origen" runat="server"  style="width:97%;vertical-align:top;" >
                                 <asp:Panel ID="Panel2" runat="server" GroupingText="Partida Origen">
                                    <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:150px; text-align:left;"> 
                                                <asp:Label ID="Lbl_Codigo_Origen" runat="server" Text="Codigo Programatico"   Width="100%"></asp:Label>
                                            </td>
                                            <td style="width:655px; text-align:left;">
                                                <asp:TextBox ID="Txt_Codigo1" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="lbl_Unidad_Responsable_Origen" runat="server" Text="Unidad Responsable" Width="100%"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Unidad_Responsable_Origen" runat="server" ReadOnly="true" Width="98%" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <asp:Label ID="Lbl_Fuente_Financiamiento_Origen" runat="server" Text="Fuente de Financiamiento"   Width="100%"></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Fuente_Financiamiento_Origen" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="Lbl_Programa_Origen" runat="server" Text="Programa" Width="100%"  
                                                ></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Programa_Origen" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="Lbl_Partida_Origen" runat="server" Text="Partida" Width="100%"  
                                                ></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Partida_Origen" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.5em;" colspan="2"></td></tr>
                                         <tr runat="server" id= "Tabla_Meses">
                                            <td colspan = "2"> 
                                               <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                                    <tr>
                                                        <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Enero </td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Febrero</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Marzo</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Abril</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Mayo</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Junio</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar">&nbsp;</td>
                                                        <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Enero" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Febrero" runat="server" Width="100px" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Marzo" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                           <asp:TextBox ID="Txt_Abril" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Mayo" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Junio" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="height:0.3em; cursor:default;" class="button_autorizar" colspan="7"></td></tr>
                                                    <tr>
                                                        <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Julio</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Agosto</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Septiembre </td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Octubre</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Noviembre</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Diciembre</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">&nbsp;</td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Julio" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                           <asp:TextBox ID="Txt_Agosto" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Septiembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Octubre" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Noviembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Diciembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                               </table>
                                            </td>
                                        </tr>
                                    </table>
                                 </asp:Panel>
                            </div>
                            <br />
                            <div id="Div_Partida_Destino" runat="server" style="width:97%;vertical-align:top;" >
                                <asp:Panel ID="Panel3" runat="server" GroupingText="Partida Destino">
                                    <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:150px; text-align:left;"> 
                                                <asp:Label ID="Lbl_Codigo_Pragramatico_Destino" runat="server" Text="Código Programatico"   Width="100%"></asp:Label>
                                            </td>
                                            <td style="width:655px; text-align:left;">
                                                <asp:TextBox ID="Txt_Codigo2" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="Lbl_Unidad_Responsable_Destino" runat="server" Text="Unidad Responsable" Width="98%"  ></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Unidad_Responsable_Destino" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td> 
                                                <asp:Label ID="Lbl_Fuente_Financiamiento_Destino" runat="server" Text="Fuente de Financiamiento"   Width="100%"></asp:Label>
                                            </td>
                                            <td>
                                                 <asp:TextBox ID="Txt_Fuente_Financiamiento_Destino" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="Lbl_Programa_Destino" runat="server" Text="Programa" Width="100%"  
                                                ></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Programa_Destino" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td > 
                                                <asp:Label ID="Lbl_Partida_Destino" runat="server" Text="Partida" Width="100%"  
                                                ></asp:Label>
                                            </td>
                                            <td >
                                                <asp:TextBox ID="Txt_Partida_Destino" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.5em;" colspan="2"></td></tr>
                                        <tr id="Tr_Tabla_Meses_Destino" runat="server">
                                            <td colspan="2">
                                                 <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                                    <tr>
                                                        <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Enero </td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Febrero</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Marzo</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Abril</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Mayo</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Junio</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar">&nbsp;</td>
                                                        <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Enero_Destino" runat="server" Width="100%"  style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Febrero_Destino" runat="server" Width="100px" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Marzo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                           <asp:TextBox ID="Txt_Abril_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Mayo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Junio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                                    <tr><td style="height:0.3em; cursor:default;" class="button_autorizar" colspan="7"></td></tr>
                                                    <tr>
                                                        <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Julio</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Agosto</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Septiembre </td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Octubre</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Noviembre</td>
                                                        <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Diciembre</td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">&nbsp;</td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Julio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                           <asp:TextBox ID="Txt_Agosto_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Septiembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Octubre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Noviembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                        <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                            <asp:TextBox ID="Txt_Diciembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;" ReadOnly="true"></asp:TextBox>
                                                        </td>
                                                    </tr>
                                               </table>
                                            </td>
                                        </tr>
                                </table> 
                                </asp:Panel>
                             </div>
                             <br />
                             <div id="Div1" runat="server" style="width:97%;vertical-align:top;" >
                                <asp:Panel ID="Panel4" runat="server" GroupingText="Observaciones">
                                    <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:12%;  text-align:left;">
                                                <asp:Label ID="Lbl_Estatus" runat="server" Width="100%" Text="*Estatus" ></asp:Label>
                                            </td>
                                            <td style="width:88%;  text-align:left;">
                                                <asp:DropDownList ID="Cmb_Tipo_Estatus" runat="server"  Width="99%" 
                                                    TabIndex="10" >
                                                    <asp:ListItem>&lt; SELECCIONE &gt;</asp:ListItem>
                                                    <asp:ListItem>AUTORIZADO</asp:ListItem>
                                                    <asp:ListItem>RECHAZADO</asp:ListItem>
                                                </asp:DropDownList>  
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:Label ID="Lbl_Justificacion_Solicitud" runat="server" Width="100%" 
                                                    Text="Comentario"></asp:Label>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="Txt_Justificacion_Solicitud" runat="server" TabIndex="11"
                                                    TextMode="MultiLine" Rows="3" Width="98%"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Justificacion_Solicitud" runat="server" WatermarkCssClass="watermarked"
                                                    TargetControlID ="Txt_Justificacion_Solicitud" WatermarkText="Límite de Caractes 250">
                                                </cc1:TextBoxWatermarkExtender>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Justificacion_Solciitud" runat="server" 
                                                    TargetControlID="Txt_Justificacion_Solicitud" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table> 
                                </asp:Panel>
                             </div>
                             <br />
                            <div id="Div_Grid_Comentarios" runat="server" style="width:97%;vertical-align:top;">
                               <asp:Panel ID="Panel5" runat="server" GroupingText="Comentarios">
                                <div id="Div2" runat="server" style="overflow:auto;height:100px;width:97%;vertical-align:top;">
                                     <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                                       <tr>
                                            <td>
                                                <asp:GridView ID="Grid_Comentarios" runat="server"  CssClass="GridView_1" Width="100%" 
                                                    AutoGenerateColumns="False"  GridLines="None"
                                                    AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                                    EmptyDataText="No se encuentra ningun comentario">
                                                    <Columns>
                                                        <asp:BoundField DataField="Comentario" HeaderText="Comentario" Visible="True" >
                                                            <HeaderStyle HorizontalAlign="Left" Width="50%" Font-Size="8pt" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50%" Font-Size="8pt"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario" Visible="True" >
                                                            <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" Visible="True" >
                                                            <HeaderStyle HorizontalAlign="Center" Width="25%" Font-Size="8pt"/>
                                                            <ItemStyle HorizontalAlign="Right" Width="25%" Font-Size="8pt"/>
                                                        </asp:BoundField>
                                                       
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                      </table>
                                   </div>
                               </asp:Panel>
                            </div>
                        </td>
                    </tr>
                 </table>
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

