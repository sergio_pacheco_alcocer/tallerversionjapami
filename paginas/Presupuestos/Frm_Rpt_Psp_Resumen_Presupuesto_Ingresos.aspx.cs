﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Estado_Analitico_Ingresos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Rpt_Psp_Movimeitnos_Presupuesto_Ingresos.Negocio;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Psp_Resumen_Presupuesto_Ingresos : System.Web.UI.Page
{
    #region (Page Load)

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Reporte_Inicio();
                ViewState["SortDirection"] = "DESC";
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
        }
    }
    #endregion

    #region METODOS
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Reporte_Sueldos_Inicio
    ///DESCRIPCIÓN          : Metodo de inicio de la página
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Reporte_Inicio()
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Todo");
            Cmb_Anio.SelectedIndex = -1;
            Cmb_Estatus.SelectedIndex = -1;
            Cmb_FF.SelectedIndex = -1;
            Llenar_Combo_Anios();
            Llenar_Combo_Estatus();
            Llenar_Combo_FF();

        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
    ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
    ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Limpiar_Controles(String Accion)
    {
        try
        {
            switch (Accion)
            {
                case "Todo":
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Cmb_Anio.SelectedIndex = -1;
                    Cmb_FF.SelectedIndex = -1;
                    break;
                case "Error":
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    break;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Combo_Anios()
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio();
        DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

        try
        {
            Cmb_Anio.Items.Clear();

            Dt_Anios = Obj_Ingresos.Consultar_Anios();

            Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
            Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
            Cmb_Anio.DataSource = Dt_Anios;
            Cmb_Anio.DataBind();

            Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(String.Format("{0:yyyy}", DateTime.Now)));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las fuentes de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 16/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Combo_FF()
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio();
        DataTable Dt_FF = new DataTable(); //Para almacenar los datos de los tipos de nominas

        try
        {
            Cmb_FF.Items.Clear();

            Dt_FF = Obj_Ingresos.Consultar_FF();

            Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
            Cmb_FF.DataTextField = "CLAVE_NOMBRE";
            Cmb_FF.DataSource = Dt_FF;
            Cmb_FF.DataBind();

            Cmb_FF.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo de Fuentes de fianciamiento: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
    ///DESCRIPCIÓN          : Metodo para llenar el combo del estatus
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Combo_Estatus()
    {
        try
        {
            Cmb_Estatus.Items.Clear();
            Cmb_Estatus.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            Cmb_Estatus.Items.Insert(1, new ListItem("PRONOSTICO", "PRONOSTICO"));
            Cmb_Estatus.Items.Insert(2, new ListItem("APROBADO", "APROBADO"));
            Cmb_Estatus.Items.Insert(3, new ListItem("ACTUAL", "ACTUAL"));
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo del estatus: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Pronostico
    ///DESCRIPCIÓN          : Metodo para generar el pronostico de ingresos
    ///PARAMETROS           1: Dt_Pronostico tabla que contiene los registros del pronostico de ingresos 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private DataTable Generar_Dt_Pronostico(DataTable Dt_Pronostico)
    {
        DataTable Dt_Datos = new DataTable();
        String Clave = String.Empty;
        String Rubro = String.Empty;
        Double Tot_Ene = 0.00;
        Double Tot_Feb = 0.00;
        Double Tot_Mar = 0.00;
        Double Tot_Abr = 0.00;
        Double Tot_May = 0.00;
        Double Tot_Jun = 0.00;
        Double Tot_Jul = 0.00;
        Double Tot_Ago = 0.00;
        Double Tot_Sep = 0.00;
        Double Tot_Oct = 0.00;
        Double Tot_Nov = 0.00;
        Double Tot_Dic = 0.00;
        Double Tot_Imp = 0.00;

        Double Tot_Imp_Ene = 0.00;
        Double Tot_Imp_Feb = 0.00;
        Double Tot_Imp_Mar = 0.00;
        Double Tot_Imp_Abr = 0.00;
        Double Tot_Imp_May = 0.00;
        Double Tot_Imp_Jun = 0.00;
        Double Tot_Imp_Jul = 0.00;
        Double Tot_Imp_Ago = 0.00;
        Double Tot_Imp_Sep = 0.00;
        Double Tot_Imp_Oct = 0.00;
        Double Tot_Imp_Nov = 0.00;
        Double Tot_Imp_Dic = 0.00;
        Double Tot_Imp_Imp = 0.00;
        Double Tot_Imp_Acumulado = 0.00;
        Double Tot_Imp_Por_Ejercer = 0.00;

        Double Tot_Acumulado = 0.00;
        Double Tot_Por_Ejercer = 0.00;
        Double Acumulado_Anio = 0.00;
        Double Importe = 0.00;
        Double Por_Ejercer = 0.00;
        DataRow Fila;

        try
        {
            Dt_Datos.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ENERO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("MARZO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ABRIL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("MAYO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("JUNIO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("JULIO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TOTAL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ACUMULADO EN EL AÑO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO ANUAL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO POR RECAUDAR", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRONOSTICO INGRESOS", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ELABORO", System.Type.GetType("System.String"));

            Clave = Dt_Pronostico.Rows[0]["CLAVE_RUBRO"].ToString().Trim();

            foreach (DataRow Dr in Dt_Pronostico.Rows)
            {
                if (Clave.Equals(Dr["CLAVE_RUBRO"].ToString().Trim()))
                {
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Imp += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());

                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                    Acumulado_Anio = Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim());
                    Por_Ejercer = Importe - Acumulado_Anio;

                    Tot_Acumulado += Acumulado_Anio;
                    Tot_Por_Ejercer += Por_Ejercer;

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim()));
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim()));
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim()));
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim()));
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim()));
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim()));
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim()));
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim()));
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim()));
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim()));
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim()));
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim()));
                        Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim()));
                        Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Por_Ejercer);
                        Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
                else
                {
                    //insertamos los totales del rubro anterior
                    Fila = Dt_Datos.NewRow();
                    Fila["CONCEPTO"] = "TOTAL " + Rubro;
                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Ene);
                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Feb);
                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Mar);
                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Abr);
                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_May);
                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Jun);
                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Jul);
                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Sep);
                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Dic);
                    Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
                    Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Acumulado);
                    Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
                    Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Por_Ejercer);
                    Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
                    Fila["TIPO"] = "RUBRO";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Dt_Datos.Rows.Add(Fila);

                    Tot_Imp_Ene += Tot_Ene;
                    Tot_Imp_Feb += Tot_Feb;
                    Tot_Imp_Mar += Tot_Mar;
                    Tot_Imp_Abr += Tot_Abr;
                    Tot_Imp_May += Tot_May;
                    Tot_Imp_Jun += Tot_Jun;
                    Tot_Imp_Jul += Tot_Jul;
                    Tot_Imp_Ago += Tot_Ago;
                    Tot_Imp_Sep += Tot_Sep;
                    Tot_Imp_Oct += Tot_Oct;
                    Tot_Imp_Nov += Tot_Nov;
                    Tot_Imp_Dic += Tot_Dic;
                    Tot_Imp_Imp += Tot_Imp;
                    Tot_Imp_Acumulado += Tot_Acumulado;
                    Tot_Imp_Por_Ejercer += Tot_Por_Ejercer;

                    Tot_Ene = 0.00;
                    Tot_Feb = 0.00;
                    Tot_Mar = 0.00;
                    Tot_Abr = 0.00;
                    Tot_May = 0.00;
                    Tot_Jun = 0.00;
                    Tot_Jul = 0.00;
                    Tot_Ago = 0.00;
                    Tot_Sep = 0.00;
                    Tot_Oct = 0.00;
                    Tot_Nov = 0.00;
                    Tot_Dic = 0.00;
                    Tot_Imp = 0.00;
                    Tot_Por_Ejercer = 0.00;
                    Tot_Acumulado = 0.00;

                    //insertamos los datos del concepto que se esta comparando
                    Clave = Dr["CLAVE_RUBRO"].ToString().Trim();
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Imp += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());

                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                    Acumulado_Anio = Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim());
                    Por_Ejercer = Importe - Acumulado_Anio;

                    Tot_Acumulado += Acumulado_Anio;
                    Tot_Por_Ejercer += Por_Ejercer;

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim()));
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim()));
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim()));
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim()));
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim()));
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim()));
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim()));
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim()));
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim()));
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim()));
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim()));
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim()));
                        Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim()));
                        Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Por_Ejercer);
                        Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
            }
            //insertamos los totales del rubro anterior
            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL " + Rubro;
            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Ene);
            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Feb);
            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Mar);
            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Abr);
            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_May);
            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Jun);
            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Jul);
            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Ago);
            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Sep);
            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Oct);
            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Nov);
            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Dic);
            Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
            Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Acumulado);
            Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
            Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Por_Ejercer);
            Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
            Fila["TIPO"] = "RUBRO";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Dt_Datos.Rows.Add(Fila);

            Tot_Imp_Ene += Tot_Ene;
            Tot_Imp_Feb += Tot_Feb;
            Tot_Imp_Mar += Tot_Mar;
            Tot_Imp_Abr += Tot_Abr;
            Tot_Imp_May += Tot_May;
            Tot_Imp_Jun += Tot_Jun;
            Tot_Imp_Jul += Tot_Jul;
            Tot_Imp_Ago += Tot_Ago;
            Tot_Imp_Sep += Tot_Sep;
            Tot_Imp_Oct += Tot_Oct;
            Tot_Imp_Nov += Tot_Nov;
            Tot_Imp_Dic += Tot_Dic;
            Tot_Imp_Imp += Tot_Imp;
            Tot_Imp_Acumulado += Tot_Acumulado;
            Tot_Imp_Por_Ejercer += Tot_Por_Ejercer;

            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL INGRESOS";
            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Ene);
            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Feb);
            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Mar);
            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Abr);
            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_May);
            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Jun);
            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Jul);
            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Ago);
            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Sep);
            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Oct);
            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Nov);
            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Dic);
            Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Imp);
            Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Acumulado);
            Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Imp);
            Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Por_Ejercer);
            Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Imp);
            Fila["TIPO"] = "TOTAL";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Dt_Datos.Rows.Add(Fila);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar los datos del pronostico de ingresos: Error[" + Ex.Message + "]");
        }
        return Dt_Datos;
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_Rpt_Ingresos
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Rpt_Ingresos(DataTable Dt_Datos, String Gpo_Dep, String Dep)
    {
        Double Cantidad = 0.00;
        String FF = String.Empty;
        if (Cmb_FF.SelectedIndex > 0)
        {
            FF = Cmb_FF.SelectedItem.Text.Trim().Substring(0, Cmb_FF.SelectedItem.Text.Trim().IndexOf(" ")).ToUpper();
        }
        else
        {
            FF = "GLOBAL";
        }
        String Ruta_Archivo = "Reporte_Analitico_Ingresos_" + FF + "_" + Cmb_Anio.SelectedItem.Value.Trim() + ".xls";
        WorksheetCell Celda = new WorksheetCell();
        try
        {
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "Reporte Analítico Ingresos";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //******************************************** Hojas del libro ******************************************************//
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Reporte Analítico de Ingresos");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_1;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1;
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("Concentrado");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_2;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2;
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("Anual");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_3;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3;

            //******************************************** Estilos del libro ******************************************************//
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Negritas = Libro.Styles.Add("BodyStyleBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas = Libro.Styles.Add("BodyStyleCantBold");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("HeaderStyleTitulo");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo1 = Libro.Styles.Add("HeaderStyleTitulo1");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo2 = Libro.Styles.Add("HeaderStyleTitulo2");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo3 = Libro.Styles.Add("HeaderStyleTitulo3");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas = Libro.Styles.Add("BodyStyleTotBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("BodyTotalStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negativo = Libro.Styles.Add("BodyStyleCant_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas__Negativo = Libro.Styles.Add("BodyStyleCantBold_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas_Negativo = Libro.Styles.Add("BodyStyleTotBold_Neg");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "LightGray";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Titulo.Font.FontName = "Tahoma";
            Estilo_Titulo.Font.Size = 12;
            Estilo_Titulo.Font.Bold = true;
            Estilo_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo.Font.Color = "#FFFFFF";
            Estilo_Titulo.Interior.Color = "Gray";
            Estilo_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo1.Font.FontName = "Tahoma";
            Estilo_Titulo1.Font.Size = 12;
            Estilo_Titulo1.Font.Bold = true;
            Estilo_Titulo1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo1.Font.Color = "#FFFFFF";
            Estilo_Titulo1.Interior.Color = "Gray";
            Estilo_Titulo1.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo1.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo1.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo2.Font.FontName = "Tahoma";
            Estilo_Titulo2.Font.Size = 12;
            Estilo_Titulo2.Font.Bold = true;
            Estilo_Titulo2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo2.Font.Color = "#FFFFFF";
            Estilo_Titulo2.Interior.Color = "Gray";
            Estilo_Titulo2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo3.Font.FontName = "Tahoma";
            Estilo_Titulo3.Font.Size = 12;
            Estilo_Titulo3.Font.Bold = true;
            Estilo_Titulo3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo3.Font.Color = "#FFFFFF";
            Estilo_Titulo3.Interior.Color = "Gray";
            Estilo_Titulo3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.NumberFormat = "##,###,###,##0.00";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Contenido_Negritas.Font.FontName = "Tahoma";
            Estilo_Contenido_Negritas.Font.Size = 9;
            Estilo_Contenido_Negritas.Font.Bold = true;
            Estilo_Contenido_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Negritas.Interior.Color = "White";
            Estilo_Contenido_Negritas.NumberFormat = "##,###,###,##0.00";
            Estilo_Contenido_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad.Font.FontName = "Tahoma";
            Estilo_Cantidad.Font.Size = 9;
            Estilo_Cantidad.Font.Bold = false;
            Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad.Font.Color = "#000000";
            Estilo_Cantidad.Interior.Color = "White";
            Estilo_Cantidad.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas.Font.Size = 9;
            Estilo_Cantidad_Negritas.Font.Bold = true;
            Estilo_Cantidad_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas.Font.Color = "#000000";
            Estilo_Cantidad_Negritas.Interior.Color = "White";
            Estilo_Cantidad_Negritas.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas.Font.FontName = "Tahoma";
            Estilo_Total_Negritas.Font.Size = 9;
            Estilo_Total_Negritas.Font.Bold = true;
            Estilo_Total_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas.Font.Color = "#000000";
            Estilo_Total_Negritas.Interior.Color = "Yellow";
            Estilo_Total_Negritas.NumberFormat = "##,###,###,##0.00";
            Estilo_Total_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total.Font.FontName = "Tahoma";
            Estilo_Total.Font.Size = 9;
            Estilo_Total.Font.Bold = true;
            Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Total.Font.Color = "#000000";
            Estilo_Total.Interior.Color = "Yellow";
            Estilo_Total.NumberFormat = "##,###,###,##0.00";
            Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negativo.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negativo.Font.Size = 9;
            Estilo_Cantidad_Negativo.Font.Bold = false;
            Estilo_Cantidad_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negativo.Font.Color = "Red";
            Estilo_Cantidad_Negativo.Interior.Color = "White";
            Estilo_Cantidad_Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas__Negativo.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas__Negativo.Font.Size = 9;
            Estilo_Cantidad_Negritas__Negativo.Font.Bold = true;
            Estilo_Cantidad_Negritas__Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas__Negativo.Font.Color = "Red";
            Estilo_Cantidad_Negritas__Negativo.Interior.Color = "White";
            Estilo_Cantidad_Negritas__Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negritas__Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas_Negativo.Font.FontName = "Tahoma";
            Estilo_Total_Negritas_Negativo.Font.Size = 9;
            Estilo_Total_Negritas_Negativo.Font.Bold = true;
            Estilo_Total_Negritas_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas_Negativo.Font.Color = "Red";
            Estilo_Total_Negritas_Negativo.Interior.Color = "Yellow";
            Estilo_Total_Negritas_Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Total_Negritas_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //******************************************** Columnas de las hojas ******************************************************//
            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Pronostico de ingresos.

            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//Concepto.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Enero.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Febrero.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Marzo.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Abril.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Mayo.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Junio.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Julio.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Agosto.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Septiembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Octubre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Noviembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Diciembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Total.

            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//Concepto.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Acumulado Año.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto anual.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Presupuesto por ejercer.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Acumulado en el mes.

            //se llena el encabezado principal de la hoja uno
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Gpo_Dep.ToUpper());
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo1";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Dep.ToUpper());
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo2";
            Renglon1 = Hoja1.Table.Rows.Add();
            Renglon1 = Hoja1.Table.Rows.Add();

            //se llena el encabezado principal de la hoja dos
            Renglon_2 = Hoja2.Table.Rows.Add();
            Celda = Renglon_2.Cells.Add("PRONÓSTICO DE INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim());
            Celda.MergeAcross = 13; // Merge 3 cells together
            Celda.MergeDown = 1;
            Celda.StyleID = "HeaderStyleTitulo3";
            Renglon_2 = Hoja2.Table.Rows.Add();
            Renglon2 = Hoja2.Table.Rows.Add();
            Renglon2 = Hoja2.Table.Rows.Add();

            //se llena el encabezado principal de la hoja tres
            Renglon_3 = Hoja3.Table.Rows.Add();
            Celda = Renglon_3.Cells.Add("RESUMEN DE INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim());
            Celda.MergeAcross = 3; // Merge 3 cells together
            Celda.MergeDown = 1;
            Celda.StyleID = "HeaderStyleTitulo3";
            Renglon_3 = Hoja3.Table.Rows.Add();
            Renglon3 = Hoja3.Table.Rows.Add();
            Renglon3 = Hoja3.Table.Rows.Add();

            //**************************************** Agregamos los datos al reporte ***********************************************//
            if (Dt_Datos is DataTable)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    foreach (DataColumn Columna in Dt_Datos.Columns)
                    {
                        if (Columna is DataColumn)
                        {
                            if (Columna.ColumnName.Equals("CONCEPTO"))
                            {
                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                            {
                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }

                            else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INGRESO POR RECAUDAR", "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONÓSTICO ANUAL", "HeaderStyle"));
                            }

                            else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                        }
                    }


                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Renglon1 = Hoja1.Table.Rows.Add();
                        Renglon2 = Hoja2.Table.Rows.Add();
                        Renglon3 = Hoja3.Table.Rows.Add();

                        if (Dr["TIPO"].ToString().Trim().Equals("RUBRO"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                }
                            }
                        }
                        else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                }
                            }
                        }
                    }
                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon_1.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 1; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";
                    //se llena el encabezado principal de la hoja dos
                    Renglon_2 = Hoja2.Table.Rows.Add();
                    Renglon_2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon_2.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 13; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";

                    //se llena el encabezado principal de la hoja tres
                    Renglon_3 = Hoja3.Table.Rows.Add();
                    Renglon_3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon_3.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 3; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";
                }
            }

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Campos
    ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private Boolean Validar_Campos()
    {
        Boolean Datos_Validos = true;
        Limpiar_Controles("Error");
        Lbl_Ecabezado_Mensaje.Text = "Favor de:";
        try
        {
            if (Cmb_Anio.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                Datos_Validos = false;
            }
            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un tipo de reporte <br />";
                Datos_Validos = false;
            }

            return Datos_Validos;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Registros
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los registros
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private void Llenar_Grid_Registros(DataTable Dt_Registros)
    {
        try
        {
            if (Dt_Registros != null)
            {
                Grid_Registros.Columns[16].Visible = true;
                Grid_Registros.DataSource = Dt_Registros;
                Grid_Registros.DataBind();
                Grid_Registros.Columns[16].Visible = false;
            }
            else
            {
                Grid_Registros.DataSource = new DataTable();
                Grid_Registros.DataBind();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el combo del estatus: Error[" + Ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Presupuesto
    ///DESCRIPCIÓN          : Metodo para generar el pronostico de ingresos
    ///PARAMETROS           1: Dt_Pronostico tabla que contiene los registros del pronostico de ingresos 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    private DataTable Generar_Dt_Presupuesto(DataTable Dt_Pronostico)
    {
        DataTable Dt_Datos = new DataTable();
        String Clave = String.Empty;
        String Rubro = String.Empty;
        DateTime Anio = DateTime.Now;
        String Acumulado_en_el_Anio = "ACUMULADO EN EL AÑO " + Anio.Year.ToString();
        String Comparativo_del_Acumulado = "COMPARATIVO DE ACUMULADO AL AÑO " + (Anio.Year - 1).ToString() + "-" + Anio.Year.ToString();
        Double Tot_Ene = 0.00;
        Double Tot_Feb = 0.00;
        Double Tot_Mar = 0.00;
        Double Tot_Abr = 0.00;
        Double Tot_May = 0.00;
        Double Tot_Jun = 0.00;
        Double Tot_Jul = 0.00;
        Double Tot_Ago = 0.00;
        Double Tot_Sep = 0.00;
        Double Tot_Oct = 0.00;
        Double Tot_Nov = 0.00;
        Double Tot_Dic = 0.00;
        Double Tot_Imp = 0.00;

        Double Tot_Imp_Ene = 0.00;
        Double Tot_Imp_Feb = 0.00;
        Double Tot_Imp_Mar = 0.00;
        Double Tot_Imp_Abr = 0.00;
        Double Tot_Imp_May = 0.00;
        Double Tot_Imp_Jun = 0.00;
        Double Tot_Imp_Jul = 0.00;
        Double Tot_Imp_Ago = 0.00;
        Double Tot_Imp_Sep = 0.00;
        Double Tot_Imp_Oct = 0.00;
        Double Tot_Imp_Nov = 0.00;
        Double Tot_Imp_Dic = 0.00;
        Double Tot_Imp_Imp = 0.00;
        Double Tot_Imp_Acumulado = 0.00;
        Double Tot_Imp_Por_Ejercer = 0.00;
        //Double Tot_Imp_Modificado = 0.00;
        //Double Tot_Imp_Ampliacion = 0.00;
        //Double Tot_Imp_Reduccion = 0.00;
        Double Tot_Imp_Amprobado = 0.00;
        //Double Sum_Tot_En_Mes = 0.00;
        //Double Sum_Tot_Pre_Men = 0.00;

        Double Tot_Acumulado = 0.00;
        Double Tot_Por_Ejercer = 0.00;

        //Double Tot_Modificado = 0.00;
        //Double Tot_Ampliacion = 0.00;
        //Double Tot_Reduccion = 0.00;
        Double Tot_Amprobado = 0.00;
        Double Tot_En_Mes = 0.00;
        Double Tot_Pre_Men = 0.00;
        Double Tot_Pre_Por_Rec_Men = 0.00;
        Double Tot_Por_Rec_Men = 0.00;
        Double Tot_Por_Rec_Anio = 0.00; 
        Double Tot_Acu_Anio_Anterior = 0.00;
        Double Tot_Comp_Anio_Anterior = 0.00;
        Double Tot_Por_Comparativo = 0.00;


        Double Sum_En_Mes = 0.00;
        Double Sum_Pre_Men = 0.00;
        Double Sum_Pre_Por_Rec_Men = 0.00;
        Double Sum_Por_Rec_Men = 0.00;
        Double Sum_Por_Rec_Anio = 0.00;
        Double Sum_Acu_Anio_Anterior = 0.00;
        Double Sum_Comp_Anio_Anterior = 0.00;
        Double Sum_Por_Comparativo = 0.00;
        DataRow Fila;

        try
        {
            Dt_Datos.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ENERO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("MARZO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ABRIL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("MAYO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("JUNIO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("JULIO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TOTAL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO ANUAL", System.Type.GetType("System.String"));
            //Dt_Datos.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
            //Dt_Datos.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
            //Dt_Datos.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ACUMULADO EN EL AÑO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO POR RECAUDAR", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRONOSTICO INGRESOS", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ELABORO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("ACUMULADO EN EL MES", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO MENSUAL", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("PRESUPUESTO POR RECAUDAR EN EL MES", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("% POR RECAUDAR EN EL MES", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("% POR RECAUDAR EN EL AÑO", System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add(Acumulado_en_el_Anio, System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add(Comparativo_del_Acumulado, System.Type.GetType("System.String"));
            Dt_Datos.Columns.Add("% DEL COMPARATIVO", System.Type.GetType("System.String"));


            Clave = Dt_Pronostico.Rows[0]["CLAVE_RUBRO"].ToString().Trim();

            foreach (DataRow Dr in Dt_Pronostico.Rows)
            {
                if (Clave.Equals(Dr["CLAVE_RUBRO"].ToString().Trim()))
                {
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Imp += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                    Tot_Acumulado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim());
                    //Tot_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                    //Tot_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                    //Tot_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                    Tot_Por_Ejercer += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                    Tot_Amprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                    Tot_En_Mes += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()) ? "0" : Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim());
                    Tot_Pre_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_MENSUAL"].ToString().Trim());
                    Tot_Pre_Por_Rec_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim());
                    Tot_Por_Rec_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim());
                    Tot_Por_Rec_Anio += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim());
                    Tot_Acu_Anio_Anterior += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()) ? "0" : Dr["ACU_ANIO_ANTERIOR"].ToString().Trim());
                    Tot_Comp_Anio_Anterior += Convert.ToDouble(String.IsNullOrEmpty(Dr["COM_ACU_ANIOS"].ToString().Trim()) ? "0" : Dr["COM_ACU_ANIOS"].ToString().Trim());
                    Tot_Por_Comparativo += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_DEL_COMPARATIVO"].ToString().Trim()) ? "0" : Dr["POR_DEL_COMPARATIVO"].ToString().Trim());

                     

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim()));
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim()));
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim()));
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim()));
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim()));
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim()));
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim()));
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim()));
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim()));
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim()));
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim()));
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim()));
                        Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                        //Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                        //Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                        //Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                        Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                        Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Fila["ACUMULADO EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()) ? "0" : Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()));
                        Fila["PRESUPUESTO MENSUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()));
                        Fila["% POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()));
                        Fila["% POR RECAUDAR EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()));
                        Fila[Acumulado_en_el_Anio] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()) ? "0" : Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()));
                        Fila[Comparativo_del_Acumulado] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COM_ACU_ANIOS"].ToString().Trim()) ? "0" : Dr["COM_ACU_ANIOS"].ToString().Trim()));
                        Fila["% DEL COMPARATIVO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_DEL_COMPARATIVO"].ToString().Trim()) ? "0" : Dr["POR_DEL_COMPARATIVO"].ToString().Trim()));
                        
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
                else
                {
                    //insertamos los totales del rubro anterior
                    Fila = Dt_Datos.NewRow();
                    Fila["CONCEPTO"] = "TOTAL " + Rubro;
                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Ene);
                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Feb);
                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Mar);
                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Abr);
                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_May);
                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Jun);
                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Jul);
                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Ago);
                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Sep);
                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Oct);
                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Nov);
                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Dic);
                    Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
                    Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Amprobado);
                    //Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Tot_Ampliacion);
                    //Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Tot_Reduccion);
                    //Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Tot_Modificado);
                    Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Acumulado);
                    Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Por_Ejercer);
                    Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
                    Fila["TIPO"] = "RUBRO";
                    Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                    Fila["ACUMULADO EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_En_Mes);
                    Fila["PRESUPUESTO MENSUAL"] = String.Format("{0:#,###,##0.00}", Tot_Pre_Men);
                    Fila["PRESUPUESTO POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_Pre_Por_Rec_Men);
                    Fila["% POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_Por_Rec_Men);
                    Fila["% POR RECAUDAR EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Por_Rec_Anio);
                    Fila[Acumulado_en_el_Anio] = String.Format("{0:#,###,##0.00}", Tot_Acu_Anio_Anterior);
                    Fila[Comparativo_del_Acumulado] = String.Format("{0:#,###,##0.00}", Tot_Comp_Anio_Anterior);
                    Fila["% DEL COMPARATIVO"] = String.Format("{0:#,###,##0.00}", Tot_Por_Comparativo);
                    Dt_Datos.Rows.Add(Fila);

                    Tot_Imp_Ene += Tot_Ene;
                    Tot_Imp_Feb += Tot_Feb;
                    Tot_Imp_Mar += Tot_Mar;
                    Tot_Imp_Abr += Tot_Abr;
                    Tot_Imp_May += Tot_May;
                    Tot_Imp_Jun += Tot_Jun;
                    Tot_Imp_Jul += Tot_Jul;
                    Tot_Imp_Ago += Tot_Ago;
                    Tot_Imp_Sep += Tot_Sep;
                    Tot_Imp_Oct += Tot_Oct;
                    Tot_Imp_Nov += Tot_Nov;
                    Tot_Imp_Dic += Tot_Dic;
                    Tot_Imp_Imp += Tot_Imp;
                    Tot_Imp_Acumulado += Tot_Acumulado;
                    Tot_Imp_Por_Ejercer += Tot_Por_Ejercer;
                    //Tot_Imp_Ampliacion += Tot_Ampliacion;
                    //Tot_Imp_Modificado += Tot_Modificado;
                    Tot_Imp_Amprobado += Tot_Amprobado;
                    //Tot_Imp_Reduccion += Tot_Reduccion;

                    Sum_En_Mes += Tot_En_Mes;
                    Sum_Pre_Men += Tot_Pre_Men;
                    Sum_Pre_Por_Rec_Men += Tot_Pre_Por_Rec_Men;
                    Sum_Por_Rec_Men += Tot_Por_Rec_Men;
                    Sum_Por_Rec_Anio += Tot_Por_Rec_Anio;
                    Sum_Acu_Anio_Anterior += Tot_Acu_Anio_Anterior;
                    Sum_Comp_Anio_Anterior += Tot_Comp_Anio_Anterior;
                    Sum_Por_Comparativo += Tot_Por_Comparativo;

                    Tot_Ene = 0.00;
                    Tot_Feb = 0.00;
                    Tot_Mar = 0.00;
                    Tot_Abr = 0.00;
                    Tot_May = 0.00;
                    Tot_Jun = 0.00;
                    Tot_Jul = 0.00;
                    Tot_Ago = 0.00;
                    Tot_Sep = 0.00;
                    Tot_Oct = 0.00;
                    Tot_Nov = 0.00;
                    Tot_Dic = 0.00;
                    Tot_Imp = 0.00;
                    Tot_Por_Ejercer = 0.00;
                    Tot_Acumulado = 0.00;
                    //Tot_Ampliacion = 0.00;
                    //Tot_Modificado = 0.00;
                    Tot_Amprobado = 0.00;
                    //Tot_Reduccion = 0.00;
                    Tot_En_Mes = 0.00;
                    Tot_Pre_Men = 0.00;
                    Tot_Pre_Por_Rec_Men = 0.00;
                    Tot_Por_Rec_Men = 0.00;
                    Tot_Por_Rec_Anio = 0.00;
                    Tot_Acu_Anio_Anterior = 0.00;
                    Tot_Comp_Anio_Anterior = 0.00;
                    Tot_Por_Comparativo = 0.00;

                    //insertamos los datos del concepto que se esta comparando
                    Clave = Dr["CLAVE_RUBRO"].ToString().Trim();
                    Rubro = Dr["RUBRO"].ToString().Trim();
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim());
                    Tot_Imp += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim());
                    Tot_Acumulado += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim());
                    //Tot_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                    //Tot_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                    //Tot_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                    Tot_Por_Ejercer += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim());
                    Tot_Amprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                    Tot_En_Mes += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()) ? "0" : Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim());
                    Tot_Pre_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_MENSUAL"].ToString().Trim());
                    Tot_Pre_Por_Rec_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim());
                    Tot_Por_Rec_Men += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim());
                    Tot_Por_Rec_Anio += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim());
                    Tot_Acu_Anio_Anterior += Convert.ToDouble(String.IsNullOrEmpty(Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()) ? "0" : Dr["ACU_ANIO_ANTERIOR"].ToString().Trim());
                    Tot_Comp_Anio_Anterior += Convert.ToDouble(String.IsNullOrEmpty(Dr["COM_ACU_ANIOS"].ToString().Trim()) ? "0" : Dr["COM_ACU_ANIOS"].ToString().Trim());
                    Tot_Por_Comparativo += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_DEL_COMPARATIVO"].ToString().Trim()) ? "0" : Dr["POR_DEL_COMPARATIVO"].ToString().Trim());

                    if (!String.IsNullOrEmpty(Dr["CONCEPTO"].ToString().Trim()))
                    {
                        Fila = Dt_Datos.NewRow();
                        Fila["CONCEPTO"] = Dr["CONCEPTO"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ENERO"].ToString().Trim()) ? "0" : Dr["IMP_ENERO"].ToString().Trim()));
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_FEBRERO"].ToString().Trim()) ? "0" : Dr["IMP_FEBRERO"].ToString().Trim()));
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MARZO"].ToString().Trim()) ? "0" : Dr["IMP_MARZO"].ToString().Trim()));
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_ABRIL"].ToString().Trim()) ? "0" : Dr["IMP_ABRIL"].ToString().Trim()));
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_MAYO"].ToString().Trim()) ? "0" : Dr["IMP_MAYO"].ToString().Trim()));
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JUNIO"].ToString().Trim()) ? "0" : Dr["IMP_JUNIO"].ToString().Trim()));
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_JULIO"].ToString().Trim()) ? "0" : Dr["IMP_JULIO"].ToString().Trim()));
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_AGOSTO"].ToString().Trim()) ? "0" : Dr["IMP_AGOSTO"].ToString().Trim()));
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_SEPTIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_SEPTIEMBRE"].ToString().Trim()));
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_OCTUBRE"].ToString().Trim()) ? "0" : Dr["IMP_OCTUBRE"].ToString().Trim()));
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_NOVIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_NOVIEMBRE"].ToString().Trim()));
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_DICIEMBRE"].ToString().Trim()) ? "0" : Dr["IMP_DICIEMBRE"].ToString().Trim()));
                        Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                        //Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                        //Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                        //Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                        Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_ANIO"].ToString().Trim()) ? "0" : Dr["ACUMULADO_ANIO"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                        Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["IMP_TOTAL"].ToString().Trim()) ? "0" : Dr["IMP_TOTAL"].ToString().Trim()));
                        Fila["TIPO"] = "CONCEPTO";
                        Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
                        Fila["ACUMULADO EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()) ? "0" : Dr["ACUMULADO_MES_ACTUAL"].ToString().Trim()));
                        Fila["PRESUPUESTO MENSUAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_MENSUAL"].ToString().Trim()));
                        Fila["PRESUPUESTO POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()) ? "0" : Dr["PRESUPUESTO_RECAUDAR_MES"].ToString().Trim()));
                        Fila["% POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_MES"].ToString().Trim()));
                        Fila["% POR RECAUDAR EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_EN_EL_ANIO"].ToString().Trim()));
                        Fila[Acumulado_en_el_Anio] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()) ? "0" : Dr["ACU_ANIO_ANTERIOR"].ToString().Trim()));
                        Fila[Comparativo_del_Acumulado] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COM_ACU_ANIOS"].ToString().Trim()) ? "0" : Dr["COM_ACU_ANIOS"].ToString().Trim()));
                        Fila["% DEL COMPARATIVO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_DEL_COMPARATIVO"].ToString().Trim()) ? "0" : Dr["POR_DEL_COMPARATIVO"].ToString().Trim()));
                        Dt_Datos.Rows.Add(Fila);
                    }
                }
            }
            //insertamos los totales del rubro anterior
            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL " + Rubro;
            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Ene);
            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Feb);
            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Mar);
            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Abr);
            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_May);
            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Jun);
            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Jul);
            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Ago);
            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Sep);
            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Oct);
            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Nov);
            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Dic);
            Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
            Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Amprobado);
            //Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Tot_Ampliacion);
            //Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Tot_Reduccion);
            //Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Tot_Modificado);
            Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Acumulado);
            Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Por_Ejercer);
            Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp);
            Fila["TIPO"] = "RUBRO";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Fila["ACUMULADO EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_En_Mes);
            Fila["PRESUPUESTO MENSUAL"] = String.Format("{0:#,###,##0.00}", Tot_Pre_Men);
            Fila["PRESUPUESTO POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_Pre_Por_Rec_Men);
            Fila["% POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Tot_Por_Rec_Men);
            Fila["% POR RECAUDAR EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Por_Rec_Anio);
            Fila[Acumulado_en_el_Anio] = String.Format("{0:#,###,##0.00}", Tot_Acu_Anio_Anterior);
            Fila[Comparativo_del_Acumulado] = String.Format("{0:#,###,##0.00}", Tot_Comp_Anio_Anterior);
            Fila["% DEL COMPARATIVO"] = String.Format("{0:#,###,##0.00}", Tot_Por_Comparativo);
            Dt_Datos.Rows.Add(Fila);

            Tot_Imp_Ene += Tot_Ene;
            Tot_Imp_Feb += Tot_Feb;
            Tot_Imp_Mar += Tot_Mar;
            Tot_Imp_Abr += Tot_Abr;
            Tot_Imp_May += Tot_May;
            Tot_Imp_Jun += Tot_Jun;
            Tot_Imp_Jul += Tot_Jul;
            Tot_Imp_Ago += Tot_Ago;
            Tot_Imp_Sep += Tot_Sep;
            Tot_Imp_Oct += Tot_Oct;
            Tot_Imp_Nov += Tot_Nov;
            Tot_Imp_Dic += Tot_Dic;
            Tot_Imp_Imp += Tot_Imp;
            Tot_Imp_Acumulado += Tot_Acumulado;
            Tot_Imp_Por_Ejercer += Tot_Por_Ejercer;
            //Tot_Imp_Ampliacion += Tot_Ampliacion;
            //Tot_Imp_Modificado += Tot_Modificado;
            //Tot_Imp_Amprobado += Tot_Amprobado;
            //Tot_Imp_Reduccion += Tot_Reduccion;


            Sum_En_Mes += Tot_En_Mes;
            Sum_Pre_Men += Tot_Pre_Men;
            Sum_Pre_Por_Rec_Men += Tot_Pre_Por_Rec_Men;
            Sum_Por_Rec_Men += Tot_Por_Rec_Men;
            Sum_Por_Rec_Anio += Tot_Por_Rec_Anio;
            Sum_Acu_Anio_Anterior += Tot_Acu_Anio_Anterior;
            Sum_Comp_Anio_Anterior += Tot_Comp_Anio_Anterior;
            Sum_Por_Comparativo += Tot_Por_Comparativo;

            Fila = Dt_Datos.NewRow();
            Fila["CONCEPTO"] = "TOTAL INGRESOS";
            Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Ene);
            Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Feb);
            Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Mar);
            Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Abr);
            Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_May);
            Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Jun);
            Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Jul);
            Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Ago);
            Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Sep);
            Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Oct);
            Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Nov);
            Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Dic);
            Fila["TOTAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Imp);
            Fila["PRESUPUESTO ANUAL"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Amprobado);
            //Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Ampliacion);
            //Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Reduccion);
            //Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Modificado);
            Fila["ACUMULADO EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Acumulado);
            Fila["PRESUPUESTO POR RECAUDAR"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Por_Ejercer);
            Fila["PRONOSTICO INGRESOS"] = String.Format("{0:#,###,##0.00}", Tot_Imp_Imp);
            Fila["TIPO"] = "TOTAL";
            Fila["ELABORO"] = Cls_Sessiones.Nombre_Empleado.Trim();
            Fila["ACUMULADO EN EL MES"] = String.Format("{0:#,###,##0.00}", Sum_En_Mes);
            Fila["PRESUPUESTO MENSUAL"] = String.Format("{0:#,###,##0.00}", Sum_Pre_Men);
            Fila["PRESUPUESTO POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Sum_Pre_Por_Rec_Men);
            Fila["% POR RECAUDAR EN EL MES"] = String.Format("{0:#,###,##0.00}", Sum_Por_Rec_Men);
            Fila["% POR RECAUDAR EN EL AÑO"] = String.Format("{0:#,###,##0.00}", Sum_Por_Rec_Anio);
            Fila[Acumulado_en_el_Anio] = String.Format("{0:#,###,##0.00}", Sum_Acu_Anio_Anterior);
            Fila[Comparativo_del_Acumulado] = String.Format("{0:#,###,##0.00}", Sum_Comp_Anio_Anterior);
            Fila["% DEL COMPARATIVO"] = String.Format("{0:#,###,##0.00}", Sum_Por_Comparativo);
            Dt_Datos.Rows.Add(Fila);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar los datos del pronostico de ingresos: Error[" + Ex.Message + "]");
        }
        return Dt_Datos;
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar_RptPresupuesto__Ingresos
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Rpt_Presupuesto_Ingresos(DataTable Dt_Datos, String Gpo_Dep, String Dep)
    {
        DateTime Anio = DateTime.Now;
        String Acumulado_en_el_Anio = "ACUMULADO EN EL AÑO " + Anio.Year.ToString();
        String Comparativo_del_Acumulado = "COMPARATIVO DE ACUMULADO AL AÑO " + (Anio.Year - 1).ToString() + "-" + Anio.Year.ToString();
        Double Cantidad = 0.00;
        String FF = String.Empty;
        if (Cmb_FF.SelectedIndex > 0)
        {
            FF = Cmb_FF.SelectedItem.Text.Trim().Substring(0, Cmb_FF.SelectedItem.Text.Trim().IndexOf(" ")).ToUpper();
        }
        else
        {
            FF = "GLOBAL";
        }
        String Ruta_Archivo = "REPORTE_PRONOSTICO_INGRESOS_" + FF + "_" + Cmb_Anio.SelectedItem.Value.Trim() + ".xls";
        WorksheetCell Celda = new WorksheetCell();
        try
        {
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            Libro.Properties.Title = "Reporte Pronóstico Ingresos";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //******************************************** Hojas del libro ******************************************************//
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Reporte Pronóstico de Ingresos");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_1;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1;
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("Concentrado");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_2;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2;
            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("Anual");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_3;
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3;

            //******************************************** Estilos del libro ******************************************************//
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_SIAG = Libro.Styles.Add("HeaderStyleSIAG");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Negritas = Libro.Styles.Add("BodyStyleBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas = Libro.Styles.Add("BodyStyleCantBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negativo = Libro.Styles.Add("BodyStyleCant_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas__Negativo = Libro.Styles.Add("BodyStyleCantBold_Neg");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo = Libro.Styles.Add("HeaderStyleTitulo");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo1 = Libro.Styles.Add("HeaderStyleTitulo1");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo2 = Libro.Styles.Add("HeaderStyleTitulo2");
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Titulo3 = Libro.Styles.Add("HeaderStyleTitulo3");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas = Libro.Styles.Add("BodyStyleTotBold");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas_Negativo = Libro.Styles.Add("BodyStyleTotBold_Neg");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("BodyTotalStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "LightGray";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_SIAG.Font.FontName = "Tahoma";
            Estilo_SIAG.Font.Size = 11;
            Estilo_SIAG.Font.Bold = true;
            Estilo_SIAG.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_SIAG.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_SIAG.Font.Color = "#FF9900";
            Estilo_SIAG.Interior.Color = "#FFFFFF";
            Estilo_SIAG.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_SIAG.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "LightGray");
            Estilo_SIAG.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "LightGray");
            Estilo_SIAG.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "LightGray");
            Estilo_SIAG.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "LightGray");

            Estilo_Titulo.Font.FontName = "Tahoma";
            Estilo_Titulo.Font.Size = 12;
            Estilo_Titulo.Font.Bold = true;
            Estilo_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo.Font.Color = "#FFFFFF";
            Estilo_Titulo.Interior.Color = "Gray";
            Estilo_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo1.Font.FontName = "Tahoma";
            Estilo_Titulo1.Font.Size = 12;
            Estilo_Titulo1.Font.Bold = true;
            Estilo_Titulo1.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo1.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo1.Font.Color = "#FFFFFF";
            Estilo_Titulo1.Interior.Color = "Gray";
            Estilo_Titulo1.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo1.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo1.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo1.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo1.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo2.Font.FontName = "Tahoma";
            Estilo_Titulo2.Font.Size = 12;
            Estilo_Titulo2.Font.Bold = true;
            Estilo_Titulo2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo2.Font.Color = "#FFFFFF";
            Estilo_Titulo2.Interior.Color = "Gray";
            Estilo_Titulo2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Gray");
            Estilo_Titulo2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Titulo3.Font.FontName = "Tahoma";
            Estilo_Titulo3.Font.Size = 12;
            Estilo_Titulo3.Font.Bold = true;
            Estilo_Titulo3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Titulo3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Titulo3.Font.Color = "#FFFFFF";
            Estilo_Titulo3.Interior.Color = "Gray";
            Estilo_Titulo3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Titulo3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 2, "Black");
            Estilo_Titulo3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 2, "Black");

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Contenido_Negritas.Font.FontName = "Tahoma";
            Estilo_Contenido_Negritas.Font.Size = 9;
            Estilo_Contenido_Negritas.Font.Bold = true;
            Estilo_Contenido_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Negritas.Interior.Color = "White";
            Estilo_Contenido_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad.Font.FontName = "Tahoma";
            Estilo_Cantidad.Font.Size = 9;
            Estilo_Cantidad.Font.Bold = false;
            Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad.Font.Color = "#000000";
            Estilo_Cantidad.Interior.Color = "White";
            Estilo_Cantidad.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas.Font.Size = 9;
            Estilo_Cantidad_Negritas.Font.Bold = true;
            Estilo_Cantidad_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas.Font.Color = "#000000";
            Estilo_Cantidad_Negritas.Interior.Color = "White";
            Estilo_Cantidad_Negritas.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas.Font.FontName = "Tahoma";
            Estilo_Total_Negritas.Font.Size = 9;
            Estilo_Total_Negritas.Font.Bold = true;
            Estilo_Total_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas.Font.Color = "#000000";
            Estilo_Total_Negritas.Interior.Color = "Yellow";
            Estilo_Total_Negritas.NumberFormat = "##,###,###,##0.00";
            Estilo_Total_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negativo.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negativo.Font.Size = 9;
            Estilo_Cantidad_Negativo.Font.Bold = false;
            Estilo_Cantidad_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negativo.Font.Color = "Red";
            Estilo_Cantidad_Negativo.Interior.Color = "White";
            Estilo_Cantidad_Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Cantidad_Negritas__Negativo.Font.FontName = "Tahoma";
            Estilo_Cantidad_Negritas__Negativo.Font.Size = 9;
            Estilo_Cantidad_Negritas__Negativo.Font.Bold = true;
            Estilo_Cantidad_Negritas__Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Cantidad_Negritas__Negativo.Font.Color = "Red";
            Estilo_Cantidad_Negritas__Negativo.Interior.Color = "White";
            Estilo_Cantidad_Negritas__Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Cantidad_Negritas__Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cantidad_Negritas__Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total_Negritas_Negativo.Font.FontName = "Tahoma";
            Estilo_Total_Negritas_Negativo.Font.Size = 9;
            Estilo_Total_Negritas_Negativo.Font.Bold = true;
            Estilo_Total_Negritas_Negativo.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Total_Negritas_Negativo.Font.Color = "Red";
            Estilo_Total_Negritas_Negativo.Interior.Color = "Yellow";
            Estilo_Total_Negritas_Negativo.NumberFormat = "##,###,###,##0.00";
            Estilo_Total_Negritas_Negativo.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total_Negritas_Negativo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            Estilo_Total.Font.FontName = "Tahoma";
            Estilo_Total.Font.Size = 9;
            Estilo_Total.Font.Bold = true;
            Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Total.Font.Color = "#000000";
            Estilo_Total.Interior.Color = "Yellow";
            Estilo_Total.NumberFormat = "##,###,###,##0.00";
            Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //******************************************** Columnas de las hojas ******************************************************//
            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
            Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Pronostico de ingresos.

            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//Concepto.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Enero.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Febrero.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Marzo.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Abril.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Mayo.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Junio.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Julio.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Agosto.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Septiembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Octubre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Noviembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Diciembre.
            Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Total.

            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//Concepto.

            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Presupuesto anual.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Acumulado Año.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Presupuesto por ejercer.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Acumulado en el mes.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Presupuesto mensual.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Presupuesto por recaudar en el mes.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//% Por recaudar en el mes.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//% Por recaudar en el año.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Acumulado en el anio.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//Comparativo de acumulado al anio.
            Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//% Del compartivo.

            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Gpo_Dep.ToUpper());
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo1";
            Renglon_1 = Hoja1.Table.Rows.Add();
            Celda = Renglon_1.Cells.Add(Dep.ToUpper());
            Celda.MergeAcross = 1; // Merge 3 cells together
            Celda.StyleID = "HeaderStyleTitulo2";
            Renglon1 = Hoja1.Table.Rows.Add();
            Renglon1 = Hoja1.Table.Rows.Add();

            //se llena el encabezado principal de la hoja dos
            Renglon_2 = Hoja2.Table.Rows.Add();
            Celda = Renglon_2.Cells.Add("PRONÓSTICO DE INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim());
            Celda.MergeAcross = 13; // Merge 3 cells together
            Celda.MergeDown = 1;
            Celda.StyleID = "HeaderStyleTitulo3";
            Renglon_2 = Hoja2.Table.Rows.Add();
            Renglon2 = Hoja2.Table.Rows.Add();
            Renglon2 = Hoja2.Table.Rows.Add();

            //se llena el encabezado principal de la hoja tres
            Renglon_3 = Hoja3.Table.Rows.Add();
            Celda = Renglon_3.Cells.Add("RESUMEN DE PRONÓSTICO DE INGRESOS " + Cmb_Anio.SelectedItem.Value.Trim());
            Celda.MergeAcross = 11; // Merge 3 cells together
            Celda.MergeDown = 1;
            Celda.StyleID = "HeaderStyleTitulo3";
            Renglon_3 = Hoja3.Table.Rows.Add();
            Renglon3 = Hoja3.Table.Rows.Add();
            Renglon3 = Hoja3.Table.Rows.Add();

            //**************************************** Agregamos los datos al reporte ***********************************************//
            if (Dt_Datos is DataTable)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    foreach (DataColumn Columna in Dt_Datos.Columns)
                    {
                        if (Columna is DataColumn)
                        {
                            if (Columna.ColumnName.Equals("CONCEPTO"))
                            {
                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName, "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                            {
                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INGRESO POR RECAUDAR", "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONÓSTICO ANUAL", "HeaderStyle"));
                            }

                            else if (Columna.ColumnName.Equals("ACUMULADO EN EL MES"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO MENSUAL"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRONOSTICO MENSUAL", "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR EN EL MES"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("INGRESO POR RECAUDAR EN EL MES", "HeaderStyle"));
                            }

                            else if (Columna.ColumnName.Equals("% POR RECAUDAR EN EL MES") || Columna.ColumnName.Equals("% POR RECAUDAR EN EL AÑO"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals(Acumulado_en_el_Anio) || Columna.ColumnName.Equals(Comparativo_del_Acumulado) || Columna.ColumnName.Equals("% DEL COMPARATIVO"))
                            {
                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            
                            else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                            {
                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }

                        }
                    }


                    foreach (DataRow Dr in Dt_Datos.Rows)
                    {
                        Renglon1 = Hoja1.Table.Rows.Add();
                        Renglon2 = Hoja2.Table.Rows.Add();
                        Renglon3 = Hoja3.Table.Rows.Add();

                        if (Dr["TIPO"].ToString().Trim().Equals("RUBRO"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals(Acumulado_en_el_Anio) || Columna.ColumnName.Equals(Comparativo_del_Acumulado) || Columna.ColumnName.Equals("% DEL COMPARATIVO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    //else if (Columna.ColumnName.Equals("AMPLIACION") || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO") || Columna.ColumnName.Equals("APROBADO"))
                                    //{
                                    //    Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                    //    if (Cantidad >= 0)
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                    //    else
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    //}
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO MENSUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("% POR RECAUDAR EN EL MES") || Columna.ColumnName.Equals("% POR RECAUDAR EN EL AÑO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                }
                            }
                        }
                        else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));

                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    //else if (Columna.ColumnName.Equals("AMPLIACION") || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO") || Columna.ColumnName.Equals("APROBADO"))
                                    //{
                                    //    Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                    //    if (Cantidad >= 0)
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                    //    else
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold_Neg"));
                                    //}
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO MENSUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("% POR RECAUDAR EN EL MES") || Columna.ColumnName.Equals("% POR RECAUDAR EN EL AÑO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals(Acumulado_en_el_Anio) || Columna.ColumnName.Equals(Comparativo_del_Acumulado) || Columna.ColumnName.Equals("% DEL COMPARATIVO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyTotalStyle"));
                                    }
                                }
                            }
                        }
                        else
                        {
                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Equals("PRONOSTICO INGRESOS"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("CONCEPTO"))
                                    {
                                        Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                    else if (Columna.ColumnName.Equals("ENERO") || Columna.ColumnName.Equals("FEBRERO") || Columna.ColumnName.Equals("MARZO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ABRIL") || Columna.ColumnName.Equals("MAYO") || Columna.ColumnName.Equals("JUNIO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("JULIO") || Columna.ColumnName.Equals("AGOSTO") || Columna.ColumnName.Equals("SEPTIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("OCTUBRE") || Columna.ColumnName.Equals("NOVIEMBRE"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("DICIEMBRE") || Columna.ColumnName.Equals("TOTAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL AÑO") || Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR") || Columna.ColumnName.Equals("PRESUPUESTO ANUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    }
                                    //else if (Columna.ColumnName.Equals("AMPLIACION") || Columna.ColumnName.Equals("REDUCCION") || Columna.ColumnName.Equals("MODIFICADO") || Columna.ColumnName.Equals("APROBADO"))
                                    //{
                                    //    Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                    //    if (Cantidad >= 0)
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                    //    else
                                    //        Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant_Neg"));
                                    //}
                                    else if (Columna.ColumnName.Equals("ACUMULADO EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO MENSUAL"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("PRESUPUESTO POR RECAUDAR EN EL MES"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals("% POR RECAUDAR EN EL MES") || Columna.ColumnName.Equals("% POR RECAUDAR EN EL AÑO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                    else if (Columna.ColumnName.Equals(Acumulado_en_el_Anio) || Columna.ColumnName.Equals(Comparativo_del_Acumulado) || Columna.ColumnName.Equals("% DEL COMPARATIVO"))
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                        else
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold_Neg"));
                                    }
                                }
                            }
                        }
                    }
                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Renglon_1 = Hoja1.Table.Rows.Add();
                    Celda = Renglon_1.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 1; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";
                    //se llena el encabezado principal de la hoja dos
                    Renglon_2 = Hoja2.Table.Rows.Add();
                    Renglon_2 = Hoja2.Table.Rows.Add();
                    Celda = Renglon_2.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 13; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";

                    //se llena el encabezado principal de la hoja tres
                    Renglon_3 = Hoja3.Table.Rows.Add();
                    Renglon_3 = Hoja3.Table.Rows.Add();
                    Celda = Renglon_3.Cells.Add("ELABORO: " + Dt_Datos.Rows[0]["ELABORO"].ToString().Trim());
                    Celda.MergeAcross = 6; // Merge 3 cells together
                    Celda.StyleID = "BodyStyle";
                }
            }

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
    ///DESCRIPCIÓN          : metodo para obtener el grupo dependencia para el reporte
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private String Obtener_Gpo_Dependencia(String Tipo)
    {
        Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio();
        String Dato = String.Empty;
        DataTable Dt_Datos = new DataTable();

        try
        {
            Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
            Dt_Datos = Negocios.Consultar_Gpo_Dependencia();

            if (Dt_Datos != null)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    if (Tipo.Trim().Equals("UR"))
                    {
                        Dato = Dt_Datos.Rows[0]["UR"].ToString().Trim();
                    }
                    else
                    {
                        Dato = Dt_Datos.Rows[0]["GPO_DEP"].ToString().Trim();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al obtener el grupo dependencia. Error[" + Ex.Message + "]");
        }
        return Dato;
    }
    #endregion

    #region EVENTOS

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 02/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 28/Diciembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Generar_Click(object sender, EventArgs e)
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;
        DateTime Mes_Acutual=DateTime.Now;
        String Dependencia = String.Empty;
        String Gpo_Dependencia = String.Empty;

        try
        {
            if (Validar_Campos())
            {
                //obtenemos los datos de la dependencia y del grupo dependencia de la persona que obtendra  el reporte
                Dependencia = Obtener_Gpo_Dependencia("UR");
                Gpo_Dependencia = Obtener_Gpo_Dependencia("GPO_DEP");

                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();


                if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("PRONOSTICO"))
                {
                    Obj_Ingresos.P_Estatus = "'GENERADO', 'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();

                    if (Dt_Pronostico != null)
                    {
                        if (Dt_Pronostico.Rows.Count > 0)
                        {
                            Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                            Llenar_Grid_Registros(Dt_Pronostico);
                            Generar_Rpt_Ingresos(Dt_Pronostico, Gpo_Dependencia, Dependencia);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("APROBADO"))
                {
                    Obj_Ingresos.P_Estatus = Obj_Ingresos.P_Estatus = "'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();

                    if (Dt_Pronostico != null)
                    {
                        if (Dt_Pronostico.Rows.Count > 0)
                        {
                            Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                            Llenar_Grid_Registros(Dt_Pronostico);
                            Generar_Rpt_Ingresos(Dt_Pronostico, Gpo_Dependencia, Dependencia);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("ACTUAL"))
                {
                    Obj_Ingresos.P_Importe_Mes = "IMPORTE_" + Mes_Acutual.ToString("MMMM").ToUpper();
                    Obj_Ingresos.P_Acumulado_Mes = "ACUMULADO_" + Mes_Acutual.ToString("MMMM").ToUpper();
                    Dt_Pronostico = Obj_Ingresos.Consultar_Resumen_Presupuesto_Ingresos();

                    if (Dt_Pronostico != null)
                    {
                        if (Dt_Pronostico.Rows.Count > 0)
                        {
                            Dt_Pronostico = Generar_Dt_Presupuesto(Dt_Pronostico);
                            Llenar_Grid_Registros(Dt_Pronostico);
                            Generar_Rpt_Presupuesto_Ingresos(Dt_Pronostico, Gpo_Dependencia, Dependencia);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;
        DataSet Ds_Registros = null;

        try
        {
            if (Validar_Campos())
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }

                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("PRONOSTICO"))
                {
                    Obj_Ingresos.P_Estatus = "'GENERADO', 'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("APROBADO"))
                {
                    Obj_Ingresos.P_Estatus = Obj_Ingresos.P_Estatus = "'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("ACTUAL"))
                {
                    Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();
                }

                if (Dt_Pronostico != null)
                {
                    if (Dt_Pronostico.Rows.Count > 0)
                    {
                        Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                        Llenar_Grid_Registros(Dt_Pronostico);

                        Ds_Registros = new DataSet();
                        Dt_Pronostico.TableName = "Dt_Registros";
                        Ds_Registros.Tables.Add(Dt_Pronostico.Copy());
                        Generar_Reporte(ref Ds_Registros, "Cr_Rpt_Psp_Analitico_Presupuesto_Ing.rpt",
                            "Rpt_Analitico_Presupuesto_Ingresos" + Session.SessionID + ".pdf");
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Consultar_Click
    ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    protected void Btn_Consultar_Click(object sender, EventArgs e)
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio(); //Conexion con la capa de negocios
        DataTable Dt_Pronostico = new DataTable(); //Para almacenar los datos de las ordenes de compras
        Div_Contenedor_Msj_Error.Visible = false;
        Limpiar_Controles("Error");
        String Complemento = String.Empty;

        try
        {
            if (Validar_Campos())
            {
                if (Cmb_FF.SelectedIndex > 0)
                {
                    Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("PRONOSTICO"))
                {
                    Obj_Ingresos.P_Estatus = "'GENERADO', 'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("APROBADO"))
                {
                    Obj_Ingresos.P_Estatus = Obj_Ingresos.P_Estatus = "'AUTORIZADO'";
                    Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();
                }
                else if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("ACTUAL"))
                {
                    Dt_Pronostico = Obj_Ingresos.Consultar_Presupuesto_Ingresos();
                }

                if (Dt_Pronostico != null)
                {
                    if (Dt_Pronostico.Rows.Count > 0)
                    {
                        Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
                        Llenar_Grid_Registros(Dt_Pronostico);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                }
            }
            else
            {
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Registros_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Registros_Sorting(object sender, GridViewSortEventArgs e)
    {
        Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Estado_Analitico_Ingresos_Negocio(); //Conexion con la capa de negocios
        DataTable Grid_Registros_Sorting = new DataTable();
        DataTable Dt_Pronostico = new DataTable();

        try
        {
            Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
            Dt_Pronostico = Obj_Ingresos.Consultar_Pronostico_Ingresos();
            Dt_Pronostico = Generar_Dt_Pronostico(Dt_Pronostico);
            Llenar_Grid_Registros(Dt_Pronostico);

            Llenar_Grid_Registros(Dt_Pronostico
                );
            Grid_Registros_Sorting = (DataTable)Grid_Registros.DataSource;
            if (Grid_Registros_Sorting != null)
            {
                DataView Dv_Vista = new DataView(Grid_Registros_Sorting);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }

                Grid_Registros.Columns[16].Visible = true;
                Grid_Registros.DataSource = Dv_Vista;
                Grid_Registros.DataBind();
                Grid_Registros.Columns[16].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Ecabezado_Mensaje.Text = "Error al ordenar la tabla de clases. Error[" + Ex.Message + "]";
            Lbl_Mensaje_Error.Text = String.Empty;
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Double Tot = 0.00;
        Double Rec = 0.00;
        Double xRec = 0.00;


        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Tot = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[1].Text.Trim()) ? "0" : e.Row.Cells[1].Text.Trim());
                Rec = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[2].Text.Trim()) ? "0" : e.Row.Cells[2].Text.Trim());
                xRec = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) ? "0" : e.Row.Cells[3].Text.Trim());

                if (e.Row.Cells[16].Text.Trim().Equals("RUBRO"))
                {
                    e.Row.Style.Add("background-color", "#FFFF99");
                    e.Row.Style.Add("color", "black");
                }
                if (e.Row.Cells[16].Text.Trim().Equals("TOTAL"))
                {
                    e.Row.Style.Add("background-color", "#FFCC66");
                    e.Row.Style.Add("color", "black");
                }
                if (Tot < 0)
                    e.Row.Cells[1].Style.Add("color", "red");

                if (Rec < 0)
                    e.Row.Cells[2].Style.Add("color", "red");

                if (xRec < 0)
                    e.Row.Cells[3].Style.Add("color", "red");

            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de años
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Combo_Estatus();
            Llenar_Combo_FF();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de años Error[" + ex.Message + "]");
        }
    }
    #endregion

    #region (Reportes)
    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en pantalla.
    ///              
    /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        //   String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
}

