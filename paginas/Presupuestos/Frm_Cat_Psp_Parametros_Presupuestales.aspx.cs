﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Parametros_Presupuestales.Negocio;

public partial class paginas_Presupuestos_Frm_Cat_Psp_Parametros_Presupuestales : System.Web.UI.Page
{
    #region (Page_Load)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo de inicio de la pagina
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            try
            {
                if (!IsPostBack)
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Configuracion_Inicial();
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de clases. Error[" + Ex.Message + "]", true);
            }
        }
    #endregion

    #region (Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Inicial
        ///DESCRIPCIÓN          : Metodo para inicializar los controles de la pagina
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Inicial()
        {
            try
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Limpiar_Forma();
                Habilitar_Forma(false);
                Estado_Botones("Inicial");
                Llenar_Combos_Fuentes();
                Llenar_Combos_Programas();
                Llenar_Combos_Cuentas();
                Llenar_Combos_Tipo_Poliza();
                Llenar_Combos_Clasificacion_Administrativa();
                Obtener_Parametros();
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de parametros. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
        ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
        ///PARAMETROS           1: Estatus: true o false para habilitar los controles
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Habilitar_Forma(Boolean Estatus)
        {
            Cmb_Aplicar_Acumulado.Enabled = Estatus;
            Cmb_Fte_Ingresos.Enabled = Estatus;
            Cmb_Fte_Inversiones.Enabled = Estatus;
            Cmb_Programa_Ingresos.Enabled = Estatus;
            Cmb_Programa_Inversiones.Enabled = Estatus;
            Cmb_Cta_Ing_Estimado.Enabled = Estatus;
            Cmb_Cta_Ing_Ampliacion.Enabled = Estatus;
            Cmb_Cta_Ing_Reduccion.Enabled = Estatus;
            Cmb_Cta_Ing_Modificado.Enabled = Estatus;
            Cmb_Cta_Ing_Por_Recaudar.Enabled = Estatus;
            Cmb_Cta_Ing_Devengado.Enabled = Estatus;
            Cmb_Cta_Ing_Recaudado.Enabled = Estatus;
            Cmb_Cta_Egr_Aprobado.Enabled = Estatus;
            Cmb_Cta_Egr_Ampliacion.Enabled = Estatus;
            Cmb_Cta_Egr_Ampliacion_Interna.Enabled = Estatus;
            Cmb_Cta_Egr_Reduccion.Enabled = Estatus;
            Cmb_Cta_Egr_Reduccion_Interna.Enabled = Estatus;
            Cmb_Cta_Egr_Modificado.Enabled = Estatus;
            Cmb_Cta_Egr_Por_Ejercer.Enabled = Estatus;
            Cmb_Cta_Egr_Comprometido.Enabled = Estatus;
            Cmb_Cta_Egr_Devengado.Enabled = Estatus;
            Cmb_Cta_Egr_Ejercido.Enabled = Estatus;
            Cmb_Cta_Egr_Pagado.Enabled = Estatus;
            Cmb_Tipo_Poliza_Egr.Enabled = Estatus;
            Cmb_Tipo_Poliza_Ing.Enabled = Estatus;
            Cmb_Clasificacion_Adm.Enabled = Estatus;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
        ///DESCRIPCIÓN          : Metodo para limpiar los controles
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Limpiar_Forma()
        {
            Cmb_Aplicar_Acumulado.SelectedIndex = -1;
            Cmb_Aplicar_Acumulado.SelectedIndex = -1;
            Cmb_Fte_Ingresos.SelectedIndex = -1;
            Cmb_Fte_Inversiones.SelectedIndex = -1;
            Cmb_Programa_Ingresos.SelectedIndex = -1;
            Cmb_Programa_Inversiones.SelectedIndex = -1;
            Cmb_Cta_Ing_Estimado.SelectedIndex = -1;
            Cmb_Cta_Ing_Ampliacion.SelectedIndex = -1;
            Cmb_Cta_Ing_Reduccion.SelectedIndex = -1;
            Cmb_Cta_Ing_Modificado.SelectedIndex = -1;
            Cmb_Cta_Ing_Por_Recaudar.SelectedIndex = -1;
            Cmb_Cta_Ing_Devengado.SelectedIndex = -1;
            Cmb_Cta_Ing_Recaudado.SelectedIndex = -1;
            Cmb_Cta_Egr_Aprobado.SelectedIndex = -1;
            Cmb_Cta_Egr_Ampliacion.SelectedIndex = -1;
            Cmb_Cta_Egr_Ampliacion_Interna.SelectedIndex = -1;
            Cmb_Cta_Egr_Reduccion.SelectedIndex = -1;
            Cmb_Cta_Egr_Reduccion_Interna.SelectedIndex = -1;
            Cmb_Cta_Egr_Modificado.SelectedIndex = -1;
            Cmb_Cta_Egr_Por_Ejercer.SelectedIndex = -1;
            Cmb_Cta_Egr_Comprometido.SelectedIndex = -1;
            Cmb_Cta_Egr_Devengado.SelectedIndex = -1;
            Cmb_Cta_Egr_Ejercido.SelectedIndex = -1;
            Cmb_Cta_Egr_Pagado.SelectedIndex = -1;
            Cmb_Tipo_Poliza_Egr.SelectedIndex = -1;
            Cmb_Tipo_Poliza_Ing.SelectedIndex = -1;
            Cmb_Clasificacion_Adm.SelectedIndex = -1;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
        ///DESCRIPCIÓN          : Metodo para limpiar los controles
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
        {
            Lbl_Encabezado_Error.Text = Encabezado_Error;
            Lbl_Mensaje_Error.Text = Mensaje_Error;
            Lbl_Mensaje_Error.Visible = Mostrar;
            Td_Error.Visible = Mostrar;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Estado_Botones
        ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
        ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Estado_Botones(String Estado)
        {
            switch (Estado)
            {
                case "Inicial":
                    //Boton Modificar
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.Enabled = true;
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    break;
                case "Modificar":
                    //Boton Modificar
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.Enabled = true;
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    break;
            }//fin del switch
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Fuentes
        ///DESCRIPCIÓN          : Metodo para llenar los datos de las fuentes de financiamiento
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:27 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Llenar_Combos_Fuentes()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt= new DataTable();

            try
            {
                Dt = Negocio.Consultar_Fuente_Financiamiento();

                Cmb_Fte_Ingresos.Items.Clear();
                Cmb_Fte_Ingresos.DataTextField = "NOMBRE";
                Cmb_Fte_Ingresos.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Cmb_Fte_Ingresos.DataSource = Dt;
                Cmb_Fte_Ingresos.DataBind();
                Cmb_Fte_Ingresos.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Fte_Ingresos.SelectedIndex = -1;


                Cmb_Fte_Inversiones.Items.Clear();
                Cmb_Fte_Inversiones.DataTextField = "NOMBRE";
                Cmb_Fte_Inversiones.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Cmb_Fte_Inversiones.DataSource = Dt;
                Cmb_Fte_Inversiones.DataBind();
                Cmb_Fte_Inversiones.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Fte_Inversiones.SelectedIndex = -1;

            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al llenar el combo de fuentes de financiamiento. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Programas
        ///DESCRIPCIÓN          : Metodo para llenar los datos de los programas
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:23 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Llenar_Combos_Programas()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt = new DataTable();

            try
            {
                Dt = Negocio.Consultar_Programas();

                Cmb_Programa_Ingresos.Items.Clear();
                Cmb_Programa_Ingresos.DataTextField = "NOMBRE";
                Cmb_Programa_Ingresos.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                Cmb_Programa_Ingresos.DataSource = Dt;
                Cmb_Programa_Ingresos.DataBind();
                Cmb_Programa_Ingresos.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Programa_Ingresos.SelectedIndex = -1;

                Cmb_Programa_Inversiones.Items.Clear();
                Cmb_Programa_Inversiones.DataTextField = "NOMBRE";
                Cmb_Programa_Inversiones.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                Cmb_Programa_Inversiones.DataSource = Dt;
                Cmb_Programa_Inversiones.DataBind();
                Cmb_Programa_Inversiones.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Programa_Inversiones.SelectedIndex = -1;

            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al llenar el combo de los programas. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Cuentas
        ///DESCRIPCIÓN          : Metodo para llenar los datos de las cuentas contables
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:25 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Llenar_Combos_Cuentas()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt = new DataTable();

            try
            {
                Dt = Negocio.Consultar_Cuentas_Contables();

                Cmb_Cta_Ing_Estimado.Items.Clear();
                Cmb_Cta_Ing_Estimado.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Estimado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Estimado.DataSource = Dt;
                Cmb_Cta_Ing_Estimado.DataBind();
                Cmb_Cta_Ing_Estimado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Estimado.SelectedIndex = -1;

                Cmb_Cta_Ing_Ampliacion.Items.Clear();
                Cmb_Cta_Ing_Ampliacion.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Ampliacion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Ampliacion.DataSource = Dt;
                Cmb_Cta_Ing_Ampliacion.DataBind();
                Cmb_Cta_Ing_Ampliacion.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Ampliacion.SelectedIndex = -1;

                Cmb_Cta_Ing_Reduccion.Items.Clear();
                Cmb_Cta_Ing_Reduccion.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Reduccion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Reduccion.DataSource = Dt;
                Cmb_Cta_Ing_Reduccion.DataBind();
                Cmb_Cta_Ing_Reduccion.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Reduccion.SelectedIndex = -1;

                Cmb_Cta_Ing_Modificado.Items.Clear();
                Cmb_Cta_Ing_Modificado.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Modificado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Modificado.DataSource = Dt;
                Cmb_Cta_Ing_Modificado.DataBind();
                Cmb_Cta_Ing_Modificado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Modificado.SelectedIndex = -1;

                Cmb_Cta_Ing_Por_Recaudar.Items.Clear();
                Cmb_Cta_Ing_Por_Recaudar.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Por_Recaudar.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Por_Recaudar.DataSource = Dt;
                Cmb_Cta_Ing_Por_Recaudar.DataBind();
                Cmb_Cta_Ing_Por_Recaudar.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Por_Recaudar.SelectedIndex = -1;

                Cmb_Cta_Ing_Devengado.Items.Clear();
                Cmb_Cta_Ing_Devengado.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Devengado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Devengado.DataSource = Dt;
                Cmb_Cta_Ing_Devengado.DataBind();
                Cmb_Cta_Ing_Devengado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Devengado.SelectedIndex = -1;

                Cmb_Cta_Ing_Recaudado.Items.Clear();
                Cmb_Cta_Ing_Recaudado.DataTextField = "CUENTA";
                Cmb_Cta_Ing_Recaudado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Ing_Recaudado.DataSource = Dt;
                Cmb_Cta_Ing_Recaudado.DataBind();
                Cmb_Cta_Ing_Recaudado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Ing_Recaudado.SelectedIndex = -1;

                Cmb_Cta_Egr_Aprobado.Items.Clear();
                Cmb_Cta_Egr_Aprobado.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Aprobado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Aprobado.DataSource = Dt;
                Cmb_Cta_Egr_Aprobado.DataBind();
                Cmb_Cta_Egr_Aprobado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Aprobado.SelectedIndex = -1;

                Cmb_Cta_Egr_Ampliacion.Items.Clear();
                Cmb_Cta_Egr_Ampliacion.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Ampliacion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Ampliacion.DataSource = Dt;
                Cmb_Cta_Egr_Ampliacion.DataBind();
                Cmb_Cta_Egr_Ampliacion.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Ampliacion.SelectedIndex = -1;

                Cmb_Cta_Egr_Reduccion.Items.Clear();
                Cmb_Cta_Egr_Reduccion.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Reduccion.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Reduccion.DataSource = Dt;
                Cmb_Cta_Egr_Reduccion.DataBind();
                Cmb_Cta_Egr_Reduccion.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Reduccion.SelectedIndex = -1;

                Cmb_Cta_Egr_Reduccion_Interna.Items.Clear();
                Cmb_Cta_Egr_Reduccion_Interna.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Reduccion_Interna.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Reduccion_Interna.DataSource = Dt;
                Cmb_Cta_Egr_Reduccion_Interna.DataBind();
                Cmb_Cta_Egr_Reduccion_Interna.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Reduccion_Interna.SelectedIndex = -1;

                Cmb_Cta_Egr_Ampliacion_Interna.Items.Clear();
                Cmb_Cta_Egr_Ampliacion_Interna.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Ampliacion_Interna.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Ampliacion_Interna.DataSource = Dt;
                Cmb_Cta_Egr_Ampliacion_Interna.DataBind();
                Cmb_Cta_Egr_Ampliacion_Interna.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Ampliacion_Interna.SelectedIndex = -1;

                Cmb_Cta_Egr_Modificado.Items.Clear();
                Cmb_Cta_Egr_Modificado.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Modificado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Modificado.DataSource = Dt;
                Cmb_Cta_Egr_Modificado.DataBind();
                Cmb_Cta_Egr_Modificado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Modificado.SelectedIndex = -1;

                Cmb_Cta_Egr_Por_Ejercer.Items.Clear();
                Cmb_Cta_Egr_Por_Ejercer.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Por_Ejercer.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Por_Ejercer.DataSource = Dt;
                Cmb_Cta_Egr_Por_Ejercer.DataBind();
                Cmb_Cta_Egr_Por_Ejercer.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Por_Ejercer.SelectedIndex = -1;

                Cmb_Cta_Egr_Comprometido.Items.Clear();
                Cmb_Cta_Egr_Comprometido.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Comprometido.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Comprometido.DataSource = Dt;
                Cmb_Cta_Egr_Comprometido.DataBind();
                Cmb_Cta_Egr_Comprometido.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Comprometido.SelectedIndex = -1;

                Cmb_Cta_Egr_Devengado.Items.Clear();
                Cmb_Cta_Egr_Devengado.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Devengado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Devengado.DataSource = Dt;
                Cmb_Cta_Egr_Devengado.DataBind();
                Cmb_Cta_Egr_Devengado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Devengado.SelectedIndex = -1;

                Cmb_Cta_Egr_Ejercido.Items.Clear();
                Cmb_Cta_Egr_Ejercido.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Ejercido.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Ejercido.DataSource = Dt;
                Cmb_Cta_Egr_Ejercido.DataBind();
                Cmb_Cta_Egr_Ejercido.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Ejercido.SelectedIndex = -1;

                Cmb_Cta_Egr_Pagado.Items.Clear();
                Cmb_Cta_Egr_Pagado.DataTextField = "CUENTA";
                Cmb_Cta_Egr_Pagado.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                Cmb_Cta_Egr_Pagado.DataSource = Dt;
                Cmb_Cta_Egr_Pagado.DataBind();
                Cmb_Cta_Egr_Pagado.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Cta_Egr_Pagado.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al llenar el combo de las cuentas contables. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Parametros
        ///DESCRIPCIÓN          : Metodo para obtener los datos de los parametros
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Obtener_Parametros()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt_Parametros = new DataTable();

            try
            {
                Dt_Parametros = Negocio.Consultar_Parametros_Presupuestales();
                if(Dt_Parametros != null)
                {
                    if (Dt_Parametros.Rows.Count > 0)
                    {
                        Cmb_Aplicar_Acumulado.SelectedIndex = Cmb_Aplicar_Acumulado.Items.IndexOf(Cmb_Aplicar_Acumulado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Aplicar_Disponible_Acumulado].ToString().Trim()));
                        Cmb_Fte_Ingresos.SelectedIndex = Cmb_Fte_Ingresos.Items.IndexOf(Cmb_Fte_Ingresos.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Ingresos].ToString().Trim()));
                        Cmb_Fte_Inversiones.SelectedIndex = Cmb_Fte_Inversiones.Items.IndexOf(Cmb_Fte_Inversiones.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Fte_Financiamiento_ID_Inversiones].ToString().Trim()));
                        Cmb_Programa_Ingresos.SelectedIndex = Cmb_Programa_Ingresos.Items.IndexOf(Cmb_Programa_Ingresos.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Ingresos].ToString().Trim()));
                        Cmb_Programa_Inversiones.SelectedIndex = Cmb_Programa_Inversiones.Items.IndexOf(Cmb_Programa_Inversiones.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Programa_ID_Inversiones].ToString().Trim()));

                        Cmb_Cta_Ing_Estimado.SelectedIndex = Cmb_Cta_Ing_Estimado.Items.IndexOf(Cmb_Cta_Ing_Estimado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Estimado].ToString().Trim()));
                        Cmb_Cta_Ing_Ampliacion.SelectedIndex = Cmb_Cta_Ing_Ampliacion.Items.IndexOf(Cmb_Cta_Ing_Ampliacion.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Ampliacion].ToString().Trim()));
                        Cmb_Cta_Ing_Reduccion.SelectedIndex = Cmb_Cta_Ing_Reduccion.Items.IndexOf(Cmb_Cta_Ing_Reduccion.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Reduccion].ToString().Trim()));
                        Cmb_Cta_Ing_Modificado.SelectedIndex = Cmb_Cta_Ing_Modificado.Items.IndexOf(Cmb_Cta_Ing_Modificado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Modificado].ToString().Trim()));
                        Cmb_Cta_Ing_Por_Recaudar.SelectedIndex = Cmb_Cta_Ing_Por_Recaudar.Items.IndexOf(Cmb_Cta_Ing_Por_Recaudar.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Por_Recaudar].ToString().Trim()));
                        Cmb_Cta_Ing_Devengado.SelectedIndex = Cmb_Cta_Ing_Devengado.Items.IndexOf(Cmb_Cta_Ing_Devengado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Devengado].ToString().Trim()));
                        Cmb_Cta_Ing_Recaudado.SelectedIndex = Cmb_Cta_Ing_Recaudado.Items.IndexOf(Cmb_Cta_Ing_Recaudado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Ingresos_Recaudado].ToString().Trim()));

                        Cmb_Cta_Egr_Aprobado.SelectedIndex = Cmb_Cta_Egr_Aprobado.Items.IndexOf(Cmb_Cta_Egr_Aprobado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Aprobado].ToString().Trim()));
                        Cmb_Cta_Egr_Ampliacion.SelectedIndex = Cmb_Cta_Egr_Ampliacion.Items.IndexOf(Cmb_Cta_Egr_Ampliacion.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion].ToString().Trim()));
                        Cmb_Cta_Egr_Ampliacion_Interna.SelectedIndex = Cmb_Cta_Egr_Ampliacion_Interna.Items.IndexOf(Cmb_Cta_Egr_Ampliacion_Interna.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ampliacion_Interna].ToString().Trim()));
                        Cmb_Cta_Egr_Reduccion.SelectedIndex = Cmb_Cta_Egr_Reduccion.Items.IndexOf(Cmb_Cta_Egr_Reduccion.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion].ToString().Trim()));
                        Cmb_Cta_Egr_Reduccion_Interna.SelectedIndex = Cmb_Cta_Egr_Reduccion_Interna.Items.IndexOf(Cmb_Cta_Egr_Reduccion_Interna.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Reduccion_Interna].ToString().Trim()));
                        Cmb_Cta_Egr_Modificado.SelectedIndex = Cmb_Cta_Egr_Modificado.Items.IndexOf(Cmb_Cta_Egr_Modificado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Modificado].ToString().Trim()));
                        Cmb_Cta_Egr_Por_Ejercer.SelectedIndex = Cmb_Cta_Egr_Por_Ejercer.Items.IndexOf(Cmb_Cta_Egr_Por_Ejercer.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Por_Ejercer].ToString().Trim()));
                        Cmb_Cta_Egr_Comprometido.SelectedIndex = Cmb_Cta_Egr_Comprometido.Items.IndexOf(Cmb_Cta_Egr_Comprometido.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Comprometido].ToString().Trim()));
                        Cmb_Cta_Egr_Devengado.SelectedIndex = Cmb_Cta_Egr_Devengado.Items.IndexOf(Cmb_Cta_Egr_Devengado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Devengado].ToString().Trim()));
                        Cmb_Cta_Egr_Ejercido.SelectedIndex = Cmb_Cta_Egr_Ejercido.Items.IndexOf(Cmb_Cta_Egr_Ejercido.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Ejercido].ToString().Trim()));
                        Cmb_Cta_Egr_Pagado.SelectedIndex = Cmb_Cta_Egr_Pagado.Items.IndexOf(Cmb_Cta_Egr_Pagado.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Cuenta_Orden_Egresos_Pagado].ToString().Trim()));

                        Cmb_Tipo_Poliza_Egr.SelectedIndex = Cmb_Tipo_Poliza_Egr.Items.IndexOf(Cmb_Tipo_Poliza_Egr.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Egr].ToString().Trim()));
                        Cmb_Tipo_Poliza_Ing.SelectedIndex = Cmb_Tipo_Poliza_Ing.Items.IndexOf(Cmb_Tipo_Poliza_Ing.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Tipo_Poliza_ID_Psp_Ing].ToString().Trim()));

                        Cmb_Clasificacion_Adm.SelectedIndex = Cmb_Clasificacion_Adm.Items.IndexOf(Cmb_Clasificacion_Adm.Items.FindByValue(Dt_Parametros.Rows[0][Cat_Psp_Parametros_Presupuestales.Campo_Clasificador_Adm_ID].ToString().Trim()));
                    }
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al obtener los datos de los parametros. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Tipo_Poliza
        ///DESCRIPCIÓN          : Metodo para llenar los datos de los tipos de poliza
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Abril/2013 10:12 AM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Llenar_Combos_Tipo_Poliza()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt = new DataTable();

            try
            {
                Dt = Negocio.Consultar_Tipo_Poliza();

                Cmb_Tipo_Poliza_Ing.Items.Clear();
                Cmb_Tipo_Poliza_Ing.DataTextField = "NOMBRE";
                Cmb_Tipo_Poliza_Ing.DataValueField = Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
                Cmb_Tipo_Poliza_Ing.DataSource = Dt;
                Cmb_Tipo_Poliza_Ing.DataBind();
                Cmb_Tipo_Poliza_Ing.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Tipo_Poliza_Ing.SelectedIndex = -1;

                Cmb_Tipo_Poliza_Egr.Items.Clear();
                Cmb_Tipo_Poliza_Egr.DataTextField = "NOMBRE";
                Cmb_Tipo_Poliza_Egr.DataValueField = Cat_Con_Tipo_Polizas.Campo_Tipo_Poliza_ID;
                Cmb_Tipo_Poliza_Egr.DataSource = Dt;
                Cmb_Tipo_Poliza_Egr.DataBind();
                Cmb_Tipo_Poliza_Egr.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Tipo_Poliza_Egr.SelectedIndex = -1;

            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al llenar el combo de los tipos de poliza. Error[" + Ex.Message + "]", true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Tipo_Poliza
        ///DESCRIPCIÓN          : Metodo para llenar los datos de los tipos de poliza
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Abril/2013 10:12 AM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Llenar_Combos_Clasificacion_Administrativa()
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = 
                new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            DataTable Dt = new DataTable();

            try
            {
                Dt = Negocio.Consultar_Clasificacion_Administrativa();

                Cmb_Clasificacion_Adm.Items.Clear();
                Cmb_Clasificacion_Adm.DataTextField = "NOMBRE";
                Cmb_Clasificacion_Adm.DataValueField = Cat_Psp_Clasificador_Administrativo.Campo_CADM_ID;
                Cmb_Clasificacion_Adm.DataSource = Dt;
                Cmb_Clasificacion_Adm.DataBind();
                Cmb_Clasificacion_Adm.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Clasificacion_Adm.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al llenar el combo de los tipos de poliza. Error[" + Ex.Message + "]", true);
            }
        }
    #endregion

    #region (Eventos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Evento del boton Salir 
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            switch (Btn_Salir.ToolTip)
            {
                case "Cancelar":
                    Configuracion_Inicial();
                    break;

                case "Inicio":
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    break;
            }//fin del switch
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Evento del boton Modificar
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2013 05:13 PM
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Cls_Cat_Psp_Parametros_Presupuestales_Negocio Negocio = new Cls_Cat_Psp_Parametros_Presupuestales_Negocio();//Variable de conexion con la capa de negocios.
            try
            {
                switch (Btn_Modificar.ToolTip)
                {
                    case "Modificar":
                        Estado_Botones("Modificar");
                        Habilitar_Forma(true);
                        break;
                    case "Actualizar":
                        Negocio.P_Aplicar_Disponible_Acumulado = Cmb_Aplicar_Acumulado.SelectedItem.Value.Trim();
                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();

                        Negocio.P_Fte_Financiamiento_ID_Ingresos = Cmb_Fte_Ingresos.SelectedItem.Value.Trim();
                        Negocio.P_Fte_Financiamiento_ID_Inversiones = Cmb_Fte_Inversiones.SelectedItem.Value.Trim();
                        Negocio.P_Programa_ID_Ingresos = Cmb_Programa_Ingresos.SelectedItem.Value.Trim();
                        Negocio.P_Programa_ID_Inversiones = Cmb_Programa_Inversiones.SelectedItem.Value.Trim();

                        Negocio.P_Cta_Orden_Ing_Estimado = Cmb_Cta_Ing_Estimado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Ampliacion = Cmb_Cta_Ing_Ampliacion.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Reduccion = Cmb_Cta_Ing_Reduccion.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Modificado = Cmb_Cta_Ing_Modificado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Por_Recaudar = Cmb_Cta_Ing_Por_Recaudar.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Devengado = Cmb_Cta_Ing_Devengado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Ing_Recaudado = Cmb_Cta_Ing_Recaudado.SelectedItem.Value.Trim();

                        Negocio.P_Cta_Orden_Egr_Aprobado = Cmb_Cta_Egr_Aprobado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Ampliacion = Cmb_Cta_Egr_Ampliacion.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Reduccion = Cmb_Cta_Egr_Reduccion.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Reduccion_Interna = Cmb_Cta_Egr_Reduccion_Interna.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Ampliacion_Interna = Cmb_Cta_Egr_Ampliacion_Interna.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Modificado = Cmb_Cta_Egr_Modificado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Por_Ejercer = Cmb_Cta_Egr_Por_Ejercer.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Comprometido = Cmb_Cta_Egr_Comprometido.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Devengado = Cmb_Cta_Egr_Devengado.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Ejercido = Cmb_Cta_Egr_Ejercido.SelectedItem.Value.Trim();
                        Negocio.P_Cta_Orden_Egr_Pagado = Cmb_Cta_Egr_Pagado.SelectedItem.Value.Trim();

                        Negocio.P_Tipo_Poliza_ID_Psp_Egr = Cmb_Tipo_Poliza_Egr.SelectedItem.Value.Trim();
                        Negocio.P_Tipo_Poliza_ID_Psp_Ing = Cmb_Tipo_Poliza_Ing.SelectedItem.Value.Trim();
                        Negocio.P_Clasificacion_Administrativa = Cmb_Clasificacion_Adm.SelectedItem.Value.Trim();

                        if (Negocio.Modificar_Parametros_Presupuestales())
                        {
                            Configuracion_Inicial();
                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Actualizar", "alert('Operacion Completa');", true);
                        }
                    break;
                }//fin del switch
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al modificar los parametros. Error[" + Ex.Message + "]", true);
            }
        }//fin de Modificar
    #endregion
}
