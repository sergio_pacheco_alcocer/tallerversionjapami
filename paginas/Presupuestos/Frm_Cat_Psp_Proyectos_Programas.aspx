<%@ Page  Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" 
AutoEventWireup="true" CodeFile="Frm_Cat_Psp_Proyectos_Programas.aspx.cs" 
Inherits="paginas_Compras_Frm_Cat_Psp_Proyectos_Programas" Title="SIAC Sistema Integral Administrativo y Comercial"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style2
        {
            width: 706px;
        }
        .style5
        {
            width: 307px;
        }
        </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>            
                <%--<div id="Div_Cat_Com_Cotizadores_Botones" style="background-color:#ffffff; width:100%; height:100px;">--%>                
                        <table id="Tbl_Comandos" border="0" cellspacing="0" class="estilo_fuente" width="98%">                        
                            <tr>
                                <td colspan="2" class="label_titulo">
                                    &nbsp;Proyectos y Programas</td>
                            </tr>                            
                            <tr>
                                <div id="Div_Contenedor_error" runat="server">
                                <td colspan="2">
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                                    <br />
                                    <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                                </td>
                                </div>
                            </tr>
                            
                            <tr class="barra_busqueda">
                                <td class="style2">
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    CssClass="Img_Button" onclick="Btn_Nuevo_Click"/>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" onclick="Btn_Modificar_Click"/>
                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    CssClass="Img_Button" onclick="Btn_Eliminar_Click"/>                                    
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    CssClass="Img_Button" onclick="Btn_Salir_Click"/>
                                </td>
                                <td align="right" style="width:50%">
                                    B�squeda
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180" 
                                    ToolTip="Buscar" TabIndex="1" ></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                                             runat="server" FilterType="Custom, Numbers" 
                                                            TargetControlID="Txt_Busqueda" ValidChars="1234567890">
                                     </cc1:FilteredTextBoxExtender>
                                    <asp:ImageButton ID="Btn_Busqueda" runat="server" ToolTip="Consultar"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                        TabIndex="2" onclick="Btn_Busqueda_Click"/>
                                        <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Buscar>" TargetControlID="Txt_Busqueda"/>
                                </td>                         
                            </tr>                            
                        </table>
                    <%--</div>--%>
            <div id="Div_Datos_Cotizador" runat="server" style=" height:150px">                 
                <div id="Div_Cat_Com_Cotizadores_Controles" runat="server" style="background-color:#ffffff; width:100%; height:100%;">        
                       <table id="Datos Generales_Inner" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                            
                            <tr>
                                <td></td>
                            </tr>
                           <tr>
                                <td>                                    
                                    * Clave</td>
                                <td class="style18">
                                    <asp:TextBox ID="Txt_Clave" runat="server" Width="50%" 
                                        Enabled="false"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Clave_FilteredTextBoxExtender1" 
                                                             runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Clave" ValidChars="������������">
                                     </cc1:FilteredTextBoxExtender>
                                </td>   
                                <td></td>                             
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    * Nombre</td>
                                
                                 <td class="style5">
                                    <asp:TextBox ID="Txt_Nombre" runat="server" Width="95%"></asp:TextBox>
                                     <cc1:FilteredTextBoxExtender ID="Txt_Nombre_FilteredTextBoxExtender" 
                                                             runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Nombre" ValidChars="��.,:;()����������-%/ ">
                                     </cc1:FilteredTextBoxExtender>  
                                </td>               
                                <td></td>                    
                            </tr>
                            <tr>
                                <td>
                                    * Estatus
                                </td>
                                <td class="style18">
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="95%">
                                        <asp:ListItem Value="ACTIVO">ACTIVO</asp:ListItem>
                                        <asp:ListItem Value="INACTIVO">INACTIVO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    * Area Funcional
                                </td>
                                <td class="style5">
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="Btn_Buscar_Area">
                                        <asp:TextBox ID="Txt_Area" runat="server" MaxLength="10" 
                                            style="width:95%;" />
                                        <cc1:FilteredTextBoxExtender ID="Txt_Area_FilteredTextBoxExtender" 
                                            runat="server" TargetControlID="Txt_Area" FilterType="Custom, Numbers" ValidChars="." />
                                            <asp:Button ID="Btn_Buscar_Area" runat="server" style="display:none;" onclick="Btn_Buscar_Area_Click" />
                                   </asp:Panel>
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Area" runat="server" Width="98%" AutoPostBack="true"
                                        onselectedindexchanged="Cmb_Area_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                      Descripci�n</td>
                                <td colspan="2">                                    
                                    <asp:TextBox ID="Txt_Descripcion" runat="server" Width="80%" 
                                        TextMode="MultiLine"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Descripcion_FilteredTextBoxExtender" 
                                        runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                        TargetControlID="Txt_Descripcion" ValidChars="��.,:;()����������-%/ ">
                                    </cc1:FilteredTextBoxExtender>                                    
                                </td>                                                              
                            </tr>                                                      
                          </table>                            
                    </div>
                </div>
            <br>            
            <div ID="Div_Listado_Cotizadores" runat="server" 
                style="background-color:#ffffff; width:100%; height:100%;">
                <table ID="Tbl_Grid_Cotizadores" border="0" cellspacing="0" 
                    class="estilo_fuente" style="width:98%;">
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Grid_Clasificador" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" CssClass="GridView_1" 
                                EmptyDataText="&quot;No se encontraron registros&quot;" GridLines="None" 
                                onpageindexchanging="Grid_Clasificador_PageIndexChanging" 
                                onselectedindexchanged="Grid_Clasificador_SelectedIndexChanged" 
                                Style="white-space:normal" Width="96%">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="5%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" HeaderText="ID">
                                        <HeaderStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                        <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" 
                                        HeaderText="Nombre">
                                        <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci�n" />
                                    <asp:BoundField DataField="AREA" HeaderText="Area Funcional" />
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" />
                                    <asp:BoundField DataField="AREA_FUNCIONAL_ID" HeaderText="AREA ID">
                                        <HeaderStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                        <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>                    
         </ContentTemplate>                  
    </asp:UpdatePanel>
</asp:Content>