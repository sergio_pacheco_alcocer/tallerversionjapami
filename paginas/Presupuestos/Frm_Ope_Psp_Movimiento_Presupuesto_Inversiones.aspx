﻿<%@ Page Title="SIAC Sistema Integral Administrativo y Comercial" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Movimiento_Presupuesto_Inversiones.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Movimiento_Presupuesto_Inversiones" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script src="../../javascript/Js_Ope_Psp_Movimiento_Presupuestal.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        function pageLoad() { $('[id*=Txt_Comen').keyup(function () { var Caracteres = $(this).val().length; if (Caracteres > 250) { this.value = this.value.substring(0, 250); $(this).css("background-color", "Yellow"); $(this).css("color", "Red"); } else { $(this).css("background-color", "White"); $(this).css("color", "Black"); } $('#Contador_Caracteres_Comentarios').text('Carácteres Ingresados [ ' + Caracteres + ' ]/[ 250 ]'); }); }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <%--<div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>--%>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           Movimiento de Presupuesto
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" Visible ="false"  />
                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Inactivar" CssClass="Img_Button" TabIndex="3"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" onclick="Btn_Eliminar_Click" Visible ="false" 
                                            OnClientClick="return confirm('Desea eliminar este Elemento. ¿Confirma que desea proceder?');"/>
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width:100%;height:28px;">
                                        
                                    </table>
                                   </td>
                                 </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
            <center>
             <div id="Div_Grid_Movimientos" runat="server" style="overflow:auto;height:300px;width:98%;vertical-align:top;border-style:solid;border-color: Silver;">
                    <table width="100%">
                       <tr>
                            <td style="width: 15%; text-align:left;"> 
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable" ></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="95%"  >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align:left; width:15%">
                                <asp:Label ID="Lbl_Fecha" runat="server" Text="Fecha"   Width="100%"></asp:Label>
                            </td>
                             <td style="width:35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="100px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td style="vertical-align:middle;text-align:right;width:20%;">B&uacute;squeda:</td>
                            <td style="width:35%;">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre" Width="180px"/>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar"
                                onclick="Btn_Buscar_Click" />
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese Folio>" TargetControlID="Txt_Busqueda" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                    runat="server" TargetControlID="Txt_Busqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="Grid_Movimiento" runat="server"  CssClass="GridView_1" Width="100%" 
                                        AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                        AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="No se encuentra ningun Movimiento"
                                        OnSelectedIndexChanged="Grid_Movimiento_SelectedIndexChanged"
                                        OnSorting="Grid_Movimiento_Sorting">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="Solicitud" Visible="True" SortExpression="NO_SOLICITUD">
                                                <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CODIGO1" HeaderText="Partida Origen" Visible="True" SortExpression="CODIGO1">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CODIGO2" HeaderText="Partida Destino" Visible="True" SortExpression="CODIGO2">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_OPERACION" HeaderText="Operacion" Visible="True" >
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMPORTE" HeaderText="Importe" Visible="True" SortExpression="IMPORTE" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="14%" />
                                                <ItemStyle HorizontalAlign="Right" Width="14%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" Visible="True" SortExpression="ESTATUS" >
                                                <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" Width="12%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                        </tr>
                    </table >
                </div>
             <div id="Div_Datos" runat="server">
                <br />
                <div id="Div_Datos_Generales" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel1" GroupingText="Datos Generales" runat="server">
                        <table id="Table_Datos_Genrerales" width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Numero_solicitud" runat="server" Text="Nùmero de solicitud" Width="98%"  ></asp:Label>
                                </td >
                                <td style="width:20%; text-align:left;">
                                    <asp:TextBox ID="Txt_No_Solicitud" runat="server" ReadOnly="True"  Width="90%"></asp:TextBox>
                                </td>
                                <td style="width:12%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="Operación" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:50%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="98%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Operacion_SelectedIndexChanged" >
                                        <asp:ListItem>TRASPASO</asp:ListItem>
                                        <asp:ListItem>AMPLIAR</asp:ListItem>
                                        <asp:ListItem>REDUCIR</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <asp:Label ID="Lbl_Importe" runat="server" Text="Importe" Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Importe" runat="server" Width="90%"
                                        onClick="$('input[id$=Txt_Importe]').select();" MaxLength="10"
                                        onBlur="$('input[id$=Txt_Importe]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(0);" >
                                    </asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Importe_FilteredTextBoxExtender1" 
                                        runat="server" FilterType="Custom ,Numbers" 
                                        ValidChars=",."
                                        TargetControlID="Txt_Importe" Enabled="True" >
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                 <td>
                                     <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus " Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" >
                                        <asp:ListItem>&lt; SELECCIONE ESTATUS&gt;</asp:ListItem>
                                        <asp:ListItem>GENERADO</asp:ListItem>
                                        <asp:ListItem>RECHAZADO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Tipo_Partida"> 
                                <td>
                                     <asp:Label ID="Lbl_Tipo_Paritda" runat="server" Text="Tipo Partida" Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:RadioButtonList ID="Rbl_Tipos_Paritda" runat="server" AutoPostBack= "true" OnSelectedIndexChanged="Rbl_Tipos_Paritda_CheckedChanged">
                                        <asp:ListItem Text="Existente" Value="0" Selected></asp:ListItem>
                                        <asp:ListItem Text="Nueva" Value="1"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </td>
                                 <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <br />
                <div ID="Div_Partida_Origen" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Panel2" runat="server" GroupingText="Partida Origen">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:20%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Codigo_Origen" runat="server" Text="Codigo Programatico"   Width="98%"></asp:Label>
                                </td>
                                <td style="width:80%; text-align:left;">
                                    <asp:TextBox ID="Txt_Codigo1" runat="server" ReadOnly="true" Width="98%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td > 
                                    <asp:Label ID="lbl_Unidad_Responsable_Origen" runat="server" Text="Unidad Responsable" Width="100%"  
                                    ></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Origen" runat="server" Width="99%" AutoPostBack="true" 
                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_Origen_SelectedIndexChanged"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Fuente_Financiamiento_Origen" runat="server" Text="Fuente de Financiamiento"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Fuente_Financiamiento_Origen" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Programa_Origen">
                                <td> 
                                    <asp:Label ID="Lbl_Programa" runat="server" Text="Programa"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Programa_Origen" runat="server" Width="99%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Capitulo_Origen">
                                <td> 
                                    <asp:Label ID="Lbl_Capitulo" runat="server" Text="Capitulo"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Capitulo_Origen" runat="server" Width="99%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Capitulo_Origen_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan = "2"> 
                                    <asp:HiddenField id="Hf_Area_Funcional_Origen_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Area_Funcional_Origen" runat="server" />
                                    <asp:HiddenField id="Hf_Tipo_Usuario" runat="server"/>
                                    <asp:HiddenField id="Hf_Programa_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Capitulo_Origen" runat="server"/>
                                    <asp:HiddenField id="Hf_Disponible" runat="server"/>
                                    <asp:HiddenField id="Hf_Mes_Actual" runat="server"/>
                                    <asp:HiddenField id="Hf_Tipo_Presupuesto" runat="server"/>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                             <tr runat="server" id= "Tabla_Meses">
                                <td colspan = "2"> 
                                   <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Junio</td>
                                        </tr>
                                        <tr>
                                             <td style="text-align:left; font-size:8pt; cursor:default; width:10% " class="button_autorizar">Disponible </td>
                                            <td style="text-align:left; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Ene" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Feb" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Mar" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Abr" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_May" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Jun" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Enero" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Enero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(1);"
                                                onClick="$('input[id$=Txt_Enero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Enero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Febrero" runat="server" Width="100px" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Febrero]').formatCurrency({colorize:true, region: 'es-MX'});  Validar(2);"
                                                onClick="$('input[id$=Txt_Febrero]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Febrero" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Marzo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Marzo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(3);"
                                                onClick="$('input[id$=Txt_Marzo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Marzo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Abril" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Abril]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(4);"
                                                onClick="$('input[id$=Txt_Abril]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Abril" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Mayo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Mayo]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(5);"
                                                onClick="$('input[id$=Txt_Mayo]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender5" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Mayo" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Junio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Junio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(6);"
                                                onClick="$('input[id$=Txt_Junio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender6" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Junio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.3em; cursor:default;" class="button_autorizar" colspan="7"></td></tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Diciembre</td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">Disponible</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Jul" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Ago" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Sep" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Oct" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Nov" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Dic" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Julio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Julio]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(7);"
                                                onClick="$('input[id$=Txt_Julio]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender7" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Julio" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Agosto" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Agosto]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(8);"
                                                onClick="$('input[id$=Txt_Agosto]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender8" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Agosto" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Septiembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Septiembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(9);"
                                                onClick="$('input[id$=Txt_Septiembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender9" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Septiembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Octubre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Octubre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(10);"
                                                onClick="$('input[id$=Txt_Octubre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender10" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Octubre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Noviembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Noviembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(11);"
                                                onClick="$('input[id$=Txt_Noviembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender11" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Noviembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Diciembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Diciembre]').formatCurrency({colorize:true, region: 'es-MX'}); Validar(12);"
                                                onClick="$('input[id$=Txt_Diciembre]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Diciembre" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:1em; cursor:default;" class="button_autorizar" colspan="5">
                                                <asp:Label ID="Lbl_Validacion" runat="server" Width="100%" style="text-align:left; font-size:8pt; color:Red;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Total" runat="server" Width="100%" Text="Total" style="text-align:center; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar" >
                                                <asp:TextBox ID="Txt_Total" runat="server" Width="100%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div_Partidas" runat="server" style="width:99%; height:120px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Partidas" runat="server"  CssClass="GridView_1" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" 
                                            EmptyDataText="No se encuentraron partidas"
                                            OnSelectedIndexChanged = "Grid_Partidas_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="nombre" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="32%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="32%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE" HeaderText="Disponible" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="Partida_id" />
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="CLAVE_PARTIDA" />
                                                <asp:BoundField DataField="CLAVE_PROGRAMA" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div>   
                <br />
                <div id="Div_Partida_Destino" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel3" runat="server" GroupingText="Partida Destino">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:20%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Codigo_Pragramatico_Destino" runat="server" Text="Código Programatico"   Width="100%"></asp:Label>
                                </td>
                                <td style="width:80%; text-align:left;">
                                    <asp:TextBox ID="Txt_Codigo2" runat="server" ReadOnly="true" Width="96%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td > 
                                    <asp:Label ID="Lbl_Unidad_Responsable_Destino" runat="server" Text="Unidad Responsable" Width="98%"  ></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable_Destino" runat="server" Width="97%" AutoPostBack ="true" 
                                    OnSelectedIndexChanged="Cmb_Unidad_Responsable_Destino_SelectedIndexChanged" >
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Fuente_Financiamiento_Destino" runat="server" Text="Fuente de Financiamiento"   Width="100%"></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Fuente_Financiamiento_Destino" runat="server" Width="97%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Programa_Destino">
                                <td> 
                                    <asp:Label ID="Lbl_Programa_Destino" runat="server" Text="Programa"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Programa_Destino" runat="server" Width="97%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr runat="server" id="Tr_Capitulo_Destino">
                                <td> 
                                    <asp:Label ID="Lbl_Capitulo_Destino" runat="server" Text="Capitulo"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Capitulo_Destino" runat="server" Width="97%" AutoPostBack="True" 
                                     OnSelectedIndexChanged="Cmb_Capitulo_Destino_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <asp:HiddenField id="Hf_Area_Funcional_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Area_Funcional_Destino_ID" runat="server" />
                                    <asp:HiddenField id="Hf_Programa_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Capitulo_Destino" runat="server"/>
                                    <asp:HiddenField id="Hf_Partida_Destino" runat="server"/>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr id="Tr_Tabla_Meses_Destino" runat="server">
                                <td colspan="2">
                                     <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Junio</td>
                                        </tr>
                                        <tr>
                                             <td style="text-align:left; font-size:8pt; cursor:default; width:10% " class="button_autorizar">Disponible </td>
                                            <td style="text-align:left; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Ene_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Feb_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Mar_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Abr_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_May_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Jun_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Enero_Destino" runat="server" Width="100%"  style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Enero_Destino]').formatCurrency({colorize:true, region: 'es-MX'});  Validacion();"
                                                onClick="$('input[id$=Txt_Enero_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender13" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Enero_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Febrero_Destino" runat="server" Width="100px" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Febrero_Destino]').formatCurrency({colorize:true, region: 'es-MX'});  Validacion();"
                                                onClick="$('input[id$=Txt_Febrero_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender14" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Febrero_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Marzo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Marzo_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Marzo_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender15" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Marzo_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Abril_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Abril_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Abril_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender16" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Abril_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Mayo_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Mayo_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Mayo_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender17" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Mayo_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Junio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Junio_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Junio_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender18" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Junio_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.3em; cursor:default;" class="button_autorizar" colspan="7"></td></tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Diciembre</td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">Disponible</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Jul_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Ago_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Sep_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Oct_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Nov_Destino" runat="server"  Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Disp_Dic_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Julio_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Julio_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Julio_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender19" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Julio_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Agosto_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Agosto_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Agosto_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender20" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Agosto_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Septiembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Septiembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Septiembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender21" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Septiembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Octubre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                               onBlur="$('input[id$=Txt_Octubre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Octubre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender22" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Octubre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Noviembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Noviembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Noviembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender23" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Noviembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Diciembre_Destino" runat="server" Width="100%" style="text-align:right; font-size:8pt;"
                                                onBlur="$('input[id$=Txt_Diciembre_Destino]').formatCurrency({colorize:true, region: 'es-MX'}); Validacion();"
                                                onClick="$('input[id$=Txt_Diciembre_Destino]').select();" MaxLength="10"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender24" 
                                                    runat="server" FilterType="Custom ,Numbers" 
                                                    ValidChars=",."
                                                    TargetControlID="Txt_Diciembre_Destino" Enabled="True" >
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="height:1em; cursor:default;" class="button_autorizar" colspan="5">
                                                <asp:Label ID="Lbl_Mensaje_Validacion" runat="server" Width="100%" style="text-align:left; font-size:8pt; color:Red;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar">
                                                <asp:Label ID="Lbl_Total_Des" runat="server" Width="100%" Text="Total" style="text-align:center; font-size:8pt;"></asp:Label>
                                            </td>
                                            <td style="height:1em; width:15%; cursor:default;" class="button_autorizar" >
                                                <asp:TextBox ID="Txt_Total_Des" runat="server" Width="100%" style="text-align:right; font-size:8pt; border-color:Navy" ReadOnly="true"></asp:TextBox>
                                            </td>
                                        </tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div2" runat="server" style="width:99%; height:120px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Partidas_Destino" runat="server"  CssClass="GridView_Nested" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" 
                                            EmptyDataText="No se encuentraron partidas"
                                            OnSelectedIndexChanged = "Grid_Partidas_Destino_SelectedIndexChanged"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="nombre" HeaderText="Partida" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PROGRAMA" HeaderText="Programa" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="32%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="32%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DISPONIBLE" HeaderText="Disponible" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="IMPORTE_ENERO"  DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_FEBRERO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MARZO" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_ABRIL" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_MAYO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JUNIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_JULIO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_AGOSTO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_OCTUBRE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="Partida_id" />
                                                <asp:BoundField DataField="CAPITULO_ID" />
                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                <asp:BoundField DataField="CLAVE_PARTIDA" />
                                                <asp:BoundField DataField="CLAVE_PROGRAMA" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected_Nested" />
                                            <PagerStyle CssClass="GridHeader_Nested" />
                                            <HeaderStyle CssClass="GridHeader_Nested" />
                                            <AlternatingRowStyle CssClass="GridAltItem_Nested" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                    </table> 
                    </asp:Panel>
            </div> 
            <div ID="Div3" runat="server"  style="width:97%;vertical-align:top;">
                <asp:Panel ID="Pnl_Justificacion" runat="server" GroupingText="Justificación">
                    <table width="100%">
                         <tr>
                            <td>
                                 <asp:TextBox ID="Txt_Justificacion" runat="server" 
                                    TextMode="MultiLine" Width="98%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Justificacion" 
                                     runat="server" WatermarkCssClass="watermarked"
                                    TargetControlID ="Txt_Justificacion" 
                                     WatermarkText="Límite de Caractes 250" Enabled="True">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Justificacion" runat="server" 
                                    TargetControlID="Txt_Justificacion" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" 
                                    ValidChars="Ññ.:;()áéíóúÁÉÍÓÚ-/[]{} " Enabled="True">
                                </cc1:FilteredTextBoxExtender> 
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
            </div>
            
                <br />
                <div id="Div_Grid_Comentarios" runat="server" style="width:97%;vertical-align:top;">
                   <asp:Panel ID="Panel4" runat="server" GroupingText="Comentarios">
                    <div id="Div1" runat="server" style="overflow:auto;height:100px;width:97%;vertical-align:top;">
                         <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                           <tr>
                                <td>
                                    <asp:GridView ID="Grid_Comentarios" runat="server"  CssClass="GridView_1" Width="100%" 
                                        AutoGenerateColumns="False"  GridLines="None"
                                        AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="No se encuentra ningun comentario">
                                        <Columns>
                                            <asp:BoundField DataField="Comentario" HeaderText="Comentario" Visible="True" >
                                                <HeaderStyle HorizontalAlign="Left" Width="50%" Font-Size="8pt" />
                                                <ItemStyle HorizontalAlign="Left" Width="50%" Font-Size="8pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario" Visible="True" >
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                                <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="8pt"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Fecha" HeaderText="Fecha" Visible="True" >
                                                <HeaderStyle HorizontalAlign="Center" Width="25%" Font-Size="8pt"/>
                                                <ItemStyle HorizontalAlign="Right" Width="25%" Font-Size="8pt"/>
                                            </asp:BoundField>
                                           
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                          </table>
                       </div>
                   </asp:Panel>
                </div>
             </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

