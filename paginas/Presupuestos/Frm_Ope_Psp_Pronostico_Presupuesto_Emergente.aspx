﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Ope_Psp_Pronostico_Presupuesto_Emergente.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Pronostico_Presupuesto_Emergente" ValidateRequest="false" Title="SIAC Sistema Integral Administrativo y Comercial"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Anteproyecto de Pronostico de Ingresos</title>
    <meta http-equiv="X-UA-Compatible" content="IE=8.0" />
    <link href="../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency-1.4.0.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.formatCurrency.all.js" type="text/javascript"></script>
    <script type="text/javascript" >
        (function ($) {
            $.formatCurrency.regions['es-MX'] = {
                symbol: '',
                positiveFormat: '%s%n',
                negativeFormat: '-%s%n',
                decimalSymbol: '.',
                digitGroupSymbol: ',',
                groupDigits: true
            };
        })(jQuery);

        function Sumar(){
            var Total=parseFloat("0.00");
            if($('input[id$=Txt_Enero]').val() != "" && $('input[id$=Txt_Enero]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Enero]').val().replace(/,/gi,''));
            }
            if($('input[id$=Txt_Febrero]').val() != "" && $('input[id$=Txt_Febrero]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Febrero]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Marzo]').val() != "" && $('input[id$=Txt_Marzo]').val() != "NaN"){
                 Total = Total + parseFloat($('input[id$=Txt_Marzo]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Abril]').val() != "" && $('input[id$=Txt_Abril]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Abril]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Mayo]').val() != "" && $('input[id$=Txt_Mayo]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Mayo]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Junio]').val() != "" && $('input[id$=Txt_Junio]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Junio]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Julio]').val() != "" && $('input[id$=Txt_Julio]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Julio]').val().replace(/,/gi,''));
            }   
             if($('input[id$=Txt_Agosto]').val() != "" && $('input[id$=Txt_Agosto]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Agosto]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Septiembre]').val() != "" && $('input[id$=Txt_Septiembre]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Septiembre]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Octubre]').val() != "" && $('input[id$=Txt_Octubre]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Octubre]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Noviembre]').val() != "" && $('input[id$=Txt_Noviembre]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Noviembre]').val().replace(/,/gi,''));
            }
             if($('input[id$=Txt_Diciembre]').val() != "" && $('input[id$=Txt_Diciembre]').val() != "NaN"){
                Total = Total + parseFloat($('input[id$=Txt_Diciembre]').val().replace(/,/gi,''));
            }
            
            $('input[id$=Txt_Total]').val(Total);
            $('input[id$=Txt_Total]').formatCurrency({colorize:true, region: 'es-MX'});
        }
        
        function Calcular(Control){
            Sumar();
        }
        
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
    </script>
</head>
<body>
    <form id="Form" runat="server">
        <div style="min-height:580px; height:auto; width:99%;vertical-align:top;border-style:outset;border-color: Silver; background-color:White;">
            <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
                EnableScriptLocalization="true" />
            <div>
                <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
                    <ContentTemplate>
                         <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                </div>
                                <div class="processMessage" id="div_progress">
                                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                </div>
                            </ProgressTemplate>                    
                        </asp:UpdateProgress>
                        <center>
                          <div id="Div_Encabezado" runat="server">
                            <table style="width:99%;" border="0" cellspacing="0">
                                <tr align="center">
                                    <td colspan="4" class="label_titulo">
                                        Anteproyecto de Pronóstico de Ingresos
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td colspan="4">
                                        <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                            Visible="false" />
                                        <asp:Label ID="Lbl_Encanezado_Error" runat="server" Text="Favor de:" ForeColor="#990000" Visible="false"></asp:Label><br />
                                        <asp:Label ID="Lbl_Error" runat="server" ForeColor="#990000" Visible="false" ></asp:Label>
                                    </td>
                                </tr>
                                <tr class="barra_busqueda" align="right">
                                    <td align="left" valign="middle" colspan="2">
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                            CssClass="Img_Button" ToolTip="Nuevo" OnClick="Btn_Nuevo_Click"/>
                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                            CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" OnClick="Btn_Modificar_Click" />
                                        <asp:ImageButton ID="Btn_Cargar_Excel" runat="server" ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png"
                                            CssClass="Img_Button" AlternateText="Cargar Excel" ToolTip="Cargar Excel" OnClick="Btn_Cargar_Excel_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                            ToolTip="Salir" OnClick="Btn_Salir_Click" />
                                    </td>
                                    <td colspan="2"> &nbsp; </td>
                                </tr>
                            </table>
                        </div>
                        <div>
                        <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos Generales" Width="99%">
                            <table style="width: 100%; text-align: center;">
                                <tr>
                                    <td colspan="13" style="text-align: left;">
                                       <asp:HiddenField id="Hf_Rubro_ID" runat="server" />
                                       <asp:HiddenField id="Hf_Tipo_ID" runat="server" />
                                       <asp:HiddenField id="Hf_Clase_ID" runat="server" />
                                       <asp:HiddenField id="Hf_Concepto_ID" runat="server" />
                                       <asp:HiddenField id="Hf_SubConcepto_ID" runat="server" />
                                       <asp:HiddenField id="Hf_Programa" runat="server" />
                                       <asp:HiddenField id="Hf_Importe_Programa" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">* Fuente Financiamiento</td>
                                    <td colspan="6" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Fuente_Financiamientos" runat="server" Style="width: 99%;" TabIndex="1"
                                         /> <%--AutoPostBack="true" OnSelectedIndexChanged="Cmb_Fuente_Financiamientos_SelectedIndexChanged"--%>
                                    </td>
                                    <td style="text-align: left;">* Estatus</td>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" Style="width: 90%; font-size:x-small;"></asp:DropDownList>
                                    </td>
                                    <td colspan="2" style="text-align: left;">
                                        <asp:Label ID="Lbl_Anio" runat="server" Text="*  Año" Width="44%"></asp:Label>
                                        <asp:DropDownList ID="Cmb_Anio" runat="server" Style="width: 48%; font-size:x-small;"
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged"></asp:DropDownList>
                                    </td>
                                </tr>
                                <%--<tr>
                                    <td colspan="2" style="text-align: left;">Programa</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Programa" runat="server" Style="width: 99%;" TabIndex="2" 
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Programa_SelectedIndexChanged"/>
                                    </td>
                                    
                                </tr>--%>
                                
                                <tr>
                                    <td colspan="2" style="text-align: left;">Rubro</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Rubro" runat="server" Style="width: 99%;" TabIndex="4"
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Rubro_SelectedIndexChanged"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">Tipo</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Tipo" runat="server" Style="width: 99%;" TabIndex="4"
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_SelectedIndexChanged"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">Clase</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Clase" runat="server" Style="width: 99%;" TabIndex="4"
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Clase_SelectedIndexChanged"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">* Concepto</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Concepto" runat="server" Style="width: 83%;" TabIndex="4"
                                        AutoPostBack="true" OnSelectedIndexChanged="Cmb_Concepto_SelectedIndexChanged"/>
                                        <asp:TextBox ID="Txt_Busqueda_Concepto" runat="server" Style="width: 13%; text-align: left;
                                            font-size: x-small;" MaxLength="50" TabIndex="7" 
                                            onClick="$('input[id$=Txt_Busqueda_Concepto]').select();"
                                            ontextchanged="Txt_Busueda_Concepto_TextChanged" AutoPostBack ="true"/>
                                        <asp:ImageButton ID="Btn_Busqueda_Concepto" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar Concepto"  OnClick="Btn_Busqueda_Conceptos_Click" TabIndex="19" style="width:20px; height:18px;"/>
                                    </td>
                                </tr>
                                <tr  id="Tr_SubConceptos" runat="server">
                                     <td colspan="2" style="text-align: left;">* SubConcepto</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_SubConcepto" runat="server" Style="width: 83%;" TabIndex="4"
                                        AutoPostBack="true" OnSelectedIndexChanged = "Cmb_SubConcepto_SelectedIndexChanged"/>
                                        <asp:TextBox ID="Txt_Busueda_SubConcepto" runat="server" Style="width: 13%; text-align: left;
                                            font-size: x-small;" MaxLength="50" TabIndex="7" 
                                            onClick="$('input[id$=Txt_Busueda_SubConcepto]').select();"
                                            ontextchanged="Txt_Busueda_SubConcepto_TextChanged" AutoPostBack ="true" />
                                        <asp:ImageButton ID="Btn_Busqueda_SubConcepto" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                            ToolTip="Buscar SubConcepto"  OnClick="Btn_Busqueda_SubConceptos_Click" TabIndex="19" style="width:20px; height:18px;"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="2" style="text-align: left;">Justificación</td>
                                    <td colspan="11" style="text-align: left;">
                                        <asp:TextBox ID="Txt_Justificacion" runat="server" Style="width: 99%; font-size: x-small;" TabIndex="6" MaxLength="250"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Enero</td>
                                    <td>Febrero</td>
                                    <td>Marzo</td>
                                    <td>Abril</td>
                                    <td>Mayo</td>
                                    <td>Junio</td>
                                    <td>Julio</td>
                                    <td>Agosto</td>
                                    <td>Septiembre</td>
                                    <td>Octubre</td>
                                    <td>Noviembre</td>
                                    <td>Diciembre</td>
                                    <td>Total</td>
                                </tr>
                                <tr>
                                    <td style="text-align:right;">
                                        <asp:TextBox ID="Txt_Enero" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="7" class="Cantidad"
                                            onClick="$('input[id$=Txt_Enero]').select();"
                                            onBlur="Calcular(this); $('input[id$=Txt_Enero]').formatCurrency({colorize:true, region: 'es-MX'});"/>
                                         <cc1:FilteredTextBoxExtender ID="FTE_Txt_Enero" runat="server" TargetControlID="Txt_Enero" FilterType="Custom,Numbers"   
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Febrero" runat="server" Style="width:75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="8"
                                            onClick="$('input[id$=Txt_Febrero]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Febrero]').formatCurrency({colorize:true, region: 'es-MX'});" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Febrero" runat="server" TargetControlID="Txt_Febrero" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Marzo" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="9"
                                            onClick="$('input[id$=Txt_Marzo]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Marzo]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Marzo" runat="server" TargetControlID="Txt_Marzo" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Abril" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="10"
                                            onClick="$('input[id$=Txt_Abril]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Abril]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Abril" runat="server" TargetControlID="Txt_Abril" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Mayo" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="11"
                                            onClick="$('input[id$=Txt_Mayo]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Mayo]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Mayo" runat="server" TargetControlID="Txt_Mayo" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Junio" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="12"
                                            onClick="$('input[id$=Txt_Junio]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Junio]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Junio" runat="server" TargetControlID="Txt_Junio" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Julio" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="13"
                                            onClick="$('input[id$=Txt_Julio]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Julio]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Julio" runat="server" TargetControlID="Txt_Julio" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Agosto" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="14"
                                            onClick="$('input[id$=Txt_Agosto]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Agosto]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Agosto" runat="server" TargetControlID="Txt_Agosto" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Septiembre" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="15"
                                            onClick="$('input[id$=Txt_Septiembre]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Septiembre]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Septiembre" runat="server" TargetControlID="Txt_Septiembre" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Octubre" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="16"
                                            onClick="$('input[id$=Txt_Octubre]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Octubre]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Octubre" runat="server" TargetControlID="Txt_Octubre" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Noviembre" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="17"
                                            onClick="$('input[id$=Txt_Noviembre]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Noviembre]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Noviembre" runat="server" TargetControlID="Txt_Noviembre" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Diciembre" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="18"
                                            onClick="$('input[id$=Txt_Diciembre]').select();"
                                            onblur="Calcular(this); $('input[id$=Txt_Diciembre]').formatCurrency({colorize:true, region: 'es-MX'});"></asp:TextBox>
                                         <cc1:FilteredTextBoxExtender ID="FTE_Txt_Diciembre" runat="server" TargetControlID="Txt_Diciembre" FilterType="Custom,Numbers" 
                                          ValidChars=",."></cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Total" runat="server" Style="width: 75px; text-align: right;
                                            font-size: x-small;" MaxLength="13" TabIndex="19" Enabled="false" ></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="12">
                                       &nbsp;
                                    </td>
                                    <td style="text-align: right;">
                                        <asp:ImageButton ID="Btn_Agregar" runat="server" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                                ToolTip="Agregar" OnClick="Btn_Agregar_Click" TabIndex="19"/>&nbsp;&nbsp;
                                        <asp:ImageButton ID="Btn_Borrar" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_clear.png"
                                                ToolTip="Limpiar" OnClick="Btn_Limpiar_Click" TabIndex="19"/>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </div>
                        <div>
                        <asp:Panel ID="Pnl_Cargar_Excel" runat="server" GroupingText="Cargar Excel" Width="99%">
                            <table style="width: 100%; text-align: center;">
                                <tr>
                                    <td  style="text-align: left; width: 60%;" rowspan="2">
                                        <asp:Label ID="Throbber" Text="wait" runat="server"  Width="80%">
                                            <div id="Div5" class="progressBackgroundFilter"></div>
                                            <div  class="processMessage" id="div6">
                                                 <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                            </div>
                                        </asp:Label>
                                        <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" CompleteBackColor="LightBlue" ErrorBackColor="Red" 
                                             ThrobberID="Throbber"  UploadingBackColor="LightGray" Width="650px" onuploadedcomplete="AFU_Archivo_Excel_UploadedComplete"
                                             Enabled="true"  />
                                    </td>
                                     <td style="text-align: left; width:40%">
                                        <asp:Label ID="Lbl_Estatus_P" runat="server" Text="* Estatus" Width="45%"></asp:Label>
                                        <asp:DropDownList ID="Cmb_Estatus_P" runat="server" Style="width: 50%; font-size:x-small;"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                     <td style="text-align: left;">
                                        <asp:Label ID="Lbl_Anio_P" runat="server" Text="* Año" Width="45%"></asp:Label>
                                        <asp:DropDownList ID="Cmb_Anio_P" runat="server" Style="width: 50%; font-size:x-small;"></asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        </div>
                        <table style="width:100%;">
                            <tr>
                                <td class="barra_busqueda" style="height:1px;">
                                </td>
                            </tr>
                        </table>
                        <table style="width:100%;">
                            <tr>
                                <td>
                                    <asp:Panel ID="Pnl_Registros" runat="server" GroupingText="Ingresos" Width="99%">
                                        <div style="width:100%; ">
                                            <table style="width:100%;">
                                                <tr>
                                                    <td style=" text-align:left;">
                                                        Nomenclatura Tabla: 
                                                        &nbsp;
                                                        <asp:Label ID = "Lbl_Clase" Text = "Clase" runat = "server" Width="75px" 
                                                            style="background-color:#66CCCC; color:Black; border-color:Gray; border-style:solid;border-width:thin;"></asp:Label>
                                                        &nbsp;
                                                        <asp:Label ID = "Lbl_Concepto" Text = "Concepto" Width="75px" runat = "server" 
                                                            style="background-color:#CCFF99; color:Black;border-color:Gray; border-style:solid; border-width:thin;"></asp:Label>
                                                        &nbsp;
                                                        <asp:Label ID = "Lbl_SubConcepto" Width="77px" Text = "SubConcepto" runat = "server" 
                                                            style="background-color:#FFFFFF; color:Black;border-color:Gray; border-style:solid;border-width:thin;"></asp:Label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width:100%;">
                                                      <asp:GridView ID="Grid_Registros" runat="server" style="white-space:normal;"
                                                        AutoGenerateColumns="False" GridLines="None" 
                                                        Width="100%"  EmptyDataText="No se encontraron registros" 
                                                        CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                                        DataKeyNames="FUENTE_FINANCIAMIENTO_ID"   OnRowDataBound="Grid_Registros_RowDataBound"
                                                        OnRowCreated="Grid_Registros_Detalle_RowCreated">
                                                        <Columns>
                                                            <asp:TemplateField> 
                                                              <ItemTemplate> 
                                                                    <a href="javascript:Grid_Anidado('div<%# Eval("FUENTE_FINANCIAMIENTO_ID") %>', 'one');"> 
                                                                         <img id="imgdiv<%# Eval("FUENTE_FINANCIAMIENTO_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                    </a> 
                                                              </ItemTemplate> 
                                                              <AlternatingItemTemplate> 
                                                                   <a href="javascript:Grid_Anidado('div<%# Eval("FUENTE_FINANCIAMIENTO_ID") %>', 'alt');"> 
                                                                        <img id="imgdiv<%# Eval("FUENTE_FINANCIAMIENTO_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                                   </a> 
                                                              </AlternatingItemTemplate> 
                                                              <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="2%" />
                                                            </asp:TemplateField>      
                                                            <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                                            <asp:BoundField DataField="CLAVE_FTE_FINANCIAMIENTO" HeaderText="Fte. Financiamiento" >
                                                                <HeaderStyle HorizontalAlign="Left" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Left" Width="12%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_ENE" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_FEB" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_MAR" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_ABR" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_MAY" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_JUN" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                               <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_JUL" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_AGO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_SEP" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="TOTAL_OCT" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                 
                                                            <asp:BoundField DataField="TOTAL_NOV" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                   
                                                            <asp:BoundField DataField="TOTAL_DIC" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="7%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>                
                                                            <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                                <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small"/>
                                                                <ItemStyle HorizontalAlign="Right"  Width="8%" Font-Size="XX-Small"/>
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                               <ItemTemplate>
                                                                 </td>
                                                                 </tr>
                                                                 <tr>
                                                                  <td colspan="100%">
                                                                   <div id="div<%# Eval("FUENTE_FINANCIAMIENTO_ID") %>" style="display:inline;position:relative;left:20px;" >
                                                                   
                                                                       <asp:GridView ID="Grid_Registros_Detalle" runat="server" style="white-space:normal;"
                                                                           CssClass="GridView_Nested" HeaderStyle-CssClass="tblHead"
                                                                           AutoGenerateColumns="false" GridLines="Vertical, Horizontal" Width="98%"
                                                                           OnSelectedIndexChanged="Grid_Registros_Detalle_SelectedIndexChanged"
                                                                           OnRowCreated="Grid_Registros_Detalle_RowCreated"
                                                                           OnRowDataBound="Grid_Registros_Detalle_RowDataBound">
                                                                           <Columns>
                                                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                                    ImageUrl="~/paginas/imagenes/paginas/Select_Grid_Inner.png">
                                                                                    <ItemStyle Width="1%" />
                                                                                </asp:ButtonField> 
                                                                                <asp:BoundField DataField="RUBRO_ID"  />
                                                                                <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID"  />
                                                                                <asp:BoundField DataField="TIPO_ID"  />
                                                                                <asp:BoundField DataField="CLASE_ING_ID"  />
                                                                                <asp:BoundField DataField="CONCEPTO_ING_ID"  />
                                                                                <asp:BoundField DataField="SUBCONCEPTO_ING_ID"  />
                                                                                <asp:BoundField DataField="JUSTIFICACION"  />
                                                                                <asp:BoundField DataField="CLAVE_NOMBRE_CONCEPTOS" HeaderText="Concepto" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Left" Width="24%" Font-Size="6pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="CLAVE_NOMBRE_PROGRAMAS" HeaderText="Programa" >
                                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Left" Width="12%" Font-Size="6pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ENERO" HeaderText="Enero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt" />
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MARZO" HeaderText="Marzo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="ABRIL" HeaderText="Abril" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="MAYO" HeaderText="Mayo" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JUNIO" HeaderText="Junio" DataFormatString="{0:#,###,##0.00}">
                                                                                   <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="JULIO" HeaderText="Julio" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>                 
                                                                                <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="6pt"/>
                                                                                </asp:BoundField>                   
                                                                                <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>                
                                                                                <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Total" DataFormatString="{0:#,###,##0.00}">
                                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7pt"/>
                                                                                    <ItemStyle HorizontalAlign="Right"  Width="6%" Font-Size="7pt"/>
                                                                                </asp:BoundField>
                                                                                <asp:TemplateField>
                                                                                    <ItemTemplate>
                                                                                        <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                                            ImageUrl="~/paginas/imagenes/gridview/tecnico.png" 
                                                                                            onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("ID")%>'
                                                                                            />
                                                                                    </ItemTemplate>
                                                                                    <ItemStyle HorizontalAlign ="Center" Font-Size="7pt" Width="1%" />
                                                                                </asp:TemplateField>
                                                                                <asp:BoundField DataField="ID" />
                                                                                <asp:BoundField DataField="PROGRAMA_ID" />
                                                                                <asp:BoundField DataField="TIPO_CRI" />
                                                                           </Columns>
                                                                           <SelectedRowStyle CssClass="GridSelected_Nested" />
                                                                           <PagerStyle CssClass="GridHeader_Nested" />
                                                                           <HeaderStyle CssClass="GridHeader_Nested" />
                                                                           <AlternatingRowStyle CssClass="GridAltItem_Nested" /> 
                                                                       </asp:GridView>
                                                                   </div>
                                                                  </td>
                                                                 </tr>
                                                               </ItemTemplate>
                                                            </asp:TemplateField>
                                                         </Columns>
                                                         <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                      </asp:GridView>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </asp:Panel>
                                </td>
                            </tr>
                        </table>
                        <table style="width:100%;">
                            <tr>
                                <td class="barra_busqueda" style="height:1px;">                            
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align:right;">
                                    <asp:Label ID="Lbl_Total_Ajuste" runat="server" Text="Total" style="font-size:small; font-weight:bold;"></asp:Label>
                                    &nbsp;<asp:TextBox ID="Txt_Total_Ajuste" runat="server" style="text-align:right; border-color:Navy;" ReadOnly="true"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                       </center>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
        </div>
    </form>
</body>
</html>
