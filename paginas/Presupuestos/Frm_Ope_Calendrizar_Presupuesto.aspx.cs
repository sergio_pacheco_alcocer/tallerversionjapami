﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using AjaxControlToolkit;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using System.Text;
using CarlosAg.ExcelXmlWriter;

public partial class paginas_presupuestos_Frm_Ope_Calendrizar_Presupuesto : System.Web.UI.Page
{
    #region PAGE LOAD

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Calendarizar_Presupuesto_Inicio();
            }
        }

    #endregion

    #region METODOS 
        
        #region (Metodos Generales)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Calendarizar_Presupuesto_Inicio
            ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Calendarizar_Presupuesto_Inicio()
            {
                try
                {
                    Mostrar_Ocultar_Error(false);
                    Limpiar_Formulario("Todo");
                    Llenar_Combo_Estatus();
                    LLenar_Presupuesto_Dependencia();
                    Llenar_Combo_Capitulos();
                    Estado_Botones("inicial");
                    Llenar_Grid_Partida_Asignada();
                    Calcular_Total_Presupuestado();
                    Habilitar_Forma(false);
                    Tr_Productos.Visible = false;
                    Estado_Botones("inicial");
                    Pnl_Cargar_Excel.Style.Add("display", "none");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Calendarizar_Presupuesto_Inicio ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
            ///PROPIEDADES          1 Estatus true o false para habilitar los controles
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Habilitar_Forma(Boolean Estatus)
            {
                try
                {
                    if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
                    {
                        Cmb_Unidad_Responsable.Enabled = false;
                    }
                    else 
                    {
                        Cmb_Unidad_Responsable.Enabled = Estatus;
                    }

                    Txt_Limite_Presupuestal.Enabled = false;
                    Cmb_Programa.Enabled = Estatus;
                    Cmb_Capitulos.Enabled = Estatus;
                    Cmb_Partida_Especifica.Enabled = false;
                    Txt_Justificacion.Enabled = Estatus;
                    Txt_Enero.Enabled = Estatus;
                    Txt_Febrero.Enabled = Estatus;
                    Txt_Marzo.Enabled = Estatus;
                    Txt_Abril.Enabled = Estatus;
                    Txt_Mayo.Enabled = Estatus;
                    Txt_Junio.Enabled = Estatus;
                    Txt_Julio.Enabled = Estatus;
                    Txt_Agosto.Enabled = Estatus;
                    Txt_Septiembre.Enabled = Estatus;
                    Txt_Octubre.Enabled = Estatus;
                    Txt_Noviembre.Enabled = Estatus;
                    Txt_Diciembre.Enabled = Estatus;
                    Btn_Agregar.Enabled = Estatus;
                    Btn_Borrar.Enabled = Estatus;
                    Cmb_Producto.Enabled = Estatus;
                    Btn_Buscar_Producto.Enabled = Estatus;
                    Cmb_Estatus.Enabled = Estatus;
                    Cmb_Producto.Enabled = false;
                    //Txt_Porcentaje.Enabled = Estatus;
                    //Btn_Porcentaje.Enabled = Estatus;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Habilitar_Forma ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Formulario
            ///DESCRIPCIÓN          : Metodo para limpiar los controles de la pagina
            ///PROPIEDADES          1 Accion para especificar que parte se limpiara si todo o las partidas asignadas
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Limpiar_Formulario(String Accion)
            {
                try
                {
                    switch (Accion)
                    {
                        case "Todo":
                            Cmb_Capitulos.SelectedIndex = -1;
                            Cmb_Partida_Especifica.SelectedIndex = -1;
                            Cmb_Producto.SelectedIndex = -1;
                            Txt_Justificacion.Text = "";
                            Txt_Enero.Text = "";
                            Txt_Febrero.Text = "";
                            Txt_Marzo.Text = "";
                            Txt_Abril.Text = "";
                            Txt_Mayo.Text = "";
                            Txt_Junio.Text = "";
                            Txt_Julio.Text = "";
                            Txt_Agosto.Text = "";
                            Txt_Septiembre.Text = "";
                            Txt_Octubre.Text = "";
                            Txt_Noviembre.Text = "";
                            Txt_Diciembre.Text = "";
                            Txt_Total.Text = "";
                            Hf_Producto_ID.Value = "";
                            Hf_Precio.Value = "";
                            Lbl_Cantidad.Text = "";
                            Lbl_Txt_Enero.Text = "";
                            Lbl_Txt_Febrero.Text = "";
                            Lbl_Txt_Marzo.Text = "";
                            Lbl_Txt_Abril.Text = "";
                            Lbl_Txt_Mayo.Text = "";
                            Lbl_Txt_Junio.Text = "";
                            Lbl_Txt_Julio.Text = "";
                            Lbl_Txt_Agosto.Text = "";
                            Lbl_Txt_Septiembre.Text = "";
                            Lbl_Txt_Octubre.Text = "";
                            Lbl_Txt_Noviembre.Text = "";
                            Lbl_Txt_Diciembre.Text = "";
                            //Txt_Porcentaje.Text = "";
                            break;
                        case "Datos_Partida":
                            Cmb_Producto.SelectedIndex = -1;
                            Grid_Partida_Asignada.SelectedIndex = -1;
                            Txt_Justificacion.Text = "";
                            Txt_Enero.Text = "";
                            Txt_Febrero.Text = "";
                            Txt_Marzo.Text = "";
                            Txt_Abril.Text = "";
                            Txt_Mayo.Text = "";
                            Txt_Junio.Text = "";
                            Txt_Julio.Text = "";
                            Txt_Agosto.Text = "";
                            Txt_Septiembre.Text = "";
                            Txt_Octubre.Text = "";
                            Txt_Noviembre.Text = "";
                            Txt_Diciembre.Text = "";
                            Txt_Total.Text = "";
                            Hf_Producto_ID.Value = "";
                            Hf_Precio.Value = "";
                            Lbl_Cantidad.Text = "";
                            Lbl_Txt_Enero.Text = "";
                            Lbl_Txt_Febrero.Text = "";
                            Lbl_Txt_Marzo.Text = "";
                            Lbl_Txt_Abril.Text = "";
                            Lbl_Txt_Mayo.Text = "";
                            Lbl_Txt_Junio.Text = "";
                            Lbl_Txt_Julio.Text = "";
                            Lbl_Txt_Agosto.Text = "";
                            Lbl_Txt_Septiembre.Text = "";
                            Lbl_Txt_Octubre.Text = "";
                            Lbl_Txt_Noviembre.Text = "";
                            Lbl_Txt_Diciembre.Text = "";
                            break;
                        case "Datos_Producto":
                            Txt_Enero.Text = "";
                            Txt_Febrero.Text = "";
                            Txt_Marzo.Text = "";
                            Txt_Abril.Text = "";
                            Txt_Mayo.Text = "";
                            Txt_Junio.Text = "";
                            Txt_Julio.Text = "";
                            Txt_Agosto.Text = "";
                            Txt_Septiembre.Text = "";
                            Txt_Octubre.Text = "";
                            Txt_Noviembre.Text = "";
                            Txt_Diciembre.Text = "";
                            Txt_Total.Text = "";
                            Hf_Producto_ID.Value = "";
                            Hf_Precio.Value = "";
                            Lbl_Cantidad.Text = "";
                            Lbl_Txt_Enero.Text = "";
                            Lbl_Txt_Febrero.Text = "";
                            Lbl_Txt_Marzo.Text = "";
                            Lbl_Txt_Abril.Text = "";
                            Lbl_Txt_Mayo.Text = "";
                            Lbl_Txt_Junio.Text = "";
                            Lbl_Txt_Julio.Text = "";
                            Lbl_Txt_Agosto.Text = "";
                            Lbl_Txt_Septiembre.Text = "";
                            Lbl_Txt_Octubre.Text = "";
                            Lbl_Txt_Noviembre.Text = "";
                            Lbl_Txt_Diciembre.Text = "";
                            break;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Limpiar_Formulario ERROR[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estado_Botones
            ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
            ///PARAMETROS:          1.- String Estado: El estado de los botones solo puede tomar 
            ///                        + inicial
            ///                        + nuevo
            ///                        + modificar
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Estado_Botones(String Estado)
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                
                try
                {
                    switch (Estado)
                    {
                        case "inicial":
                            Limpiar_Sessiones();
                            Llenar_Grid_Partida_Asignada();
                            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

                            //Boton Modificar
                            Btn_Modificar.ToolTip = "Modificar";
                            Btn_Modificar.Enabled = true;
                            Btn_Modificar.Visible = true;
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                            //Boton Cargar
                            Btn_Cargar_Excel.ToolTip = "Cargar Excel";
                            Btn_Cargar_Excel.Visible = true;
                            Btn_Cargar_Excel.Enabled = true;
                            Btn_Cargar_Excel.ImageUrl = "~/paginas/imagenes/paginas/microsoft_office2003_excel.png";
                            //Boton Nuevo
                            Btn_Nuevo.ToolTip = "Nuevo";
                            Btn_Nuevo.Visible = true;
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                            if (Dt_Partidas_Asignadas != null && Dt_Partidas_Asignadas.Rows.Count > 0)
                            {
                                Btn_Nuevo.Enabled = false;
                                if (Cmb_Estatus.SelectedValue == "GENERADO" || Cmb_Estatus.SelectedValue == "AUTORIZADO")
                                {
                                    Btn_Modificar.Enabled = false;
                                    Btn_Cargar_Excel.Enabled = false;
                                    Mostrar_Ocultar_Error(true);
                                    Lbl_Encanezado_Error.Text = "No se puede Modificar la calendarización porque se encuentra en validación por el personal del presupuesto.";
                                }
                                else 
                                {
                                    Btn_Modificar.Enabled = true;
                                    Btn_Cargar_Excel.Enabled = true;
                                }
                            }
                            else 
                            {
                                Btn_Nuevo.Enabled = true;
                                Btn_Modificar.Enabled = true;
                                Btn_Cargar_Excel.Enabled = true;
                            }

                            //Boton reporte
                            Btn_Generar_Reporte.ToolTip = "Generar Reporte";
                            Btn_Generar_Reporte.Visible = true;
                            Btn_Generar_Reporte.Enabled = true;
                            Btn_Generar_Reporte.ImageUrl = "~/paginas/imagenes/paginas/microsoft_office2003_excel.png";

                            //Boton Salir
                            Btn_Salir.ToolTip = "Salir";
                            Btn_Salir.Enabled = true;
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                           
                            //Boton Carga Masiva
                            Btn_Copiar.Visible = false;
                            break;
                        case "nuevo":
                            //Boton Nuevo
                            Btn_Nuevo.ToolTip = "Guardar";
                            Btn_Nuevo.Enabled = true;
                            Btn_Nuevo.Visible = true;
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                            //Boton Modificar
                            Btn_Modificar.Visible = false;
                            //Boton Cargar
                            Btn_Cargar_Excel.Visible = false;
                            //Boton reporte
                            Btn_Generar_Reporte.Visible = false;
                            //Boton Salir
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.Enabled = true;
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            //Boton Carga Masiva
                            Btn_Copiar.Visible = true;
                            Btn_Copiar.Enabled = true;
                            break;
                        case "modificar":
                            //Boton Nuevo
                            Btn_Nuevo.Visible = false;
                            //Boton Cargar
                            Btn_Cargar_Excel.Visible = false;
                            //Boton reporte
                            Btn_Generar_Reporte.Visible = false;
                            //Boton Modificar
                            Btn_Modificar.ToolTip = "Actualizar";
                            Btn_Modificar.Enabled = true;
                            Btn_Modificar.Visible = true;
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                            //Boton Salir
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.Enabled = true;
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            //Boton Carga Masiva
                            Btn_Copiar.Visible = true;
                            Btn_Copiar.Enabled = true;
                            break;

                        case "excel":
                            //Boton Nuevo
                            Btn_Nuevo.Visible = false;
                            //Boton Modificar
                            Btn_Modificar.Visible = false;
                            //Boton reporte
                            Btn_Generar_Reporte.Visible = false;
                            //Boton Cargar
                            Btn_Cargar_Excel.Visible = true;
                            Btn_Cargar_Excel.ToolTip = "Cargar";
                            Btn_Cargar_Excel.Enabled = true;
                            Btn_Cargar_Excel.ImageUrl = "~/paginas/imagenes/paginas/subir.png";
                            //Boton Salir
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.Enabled = true;
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
 
                            break;
                        case "carga":
                            //Boton Carga Masiva
                            Btn_Copiar.Enabled = false;
                            //Boton reporte
                            Btn_Generar_Reporte.Visible = false;
                            if (Btn_Nuevo.Visible == true)
                            {
                                Btn_Nuevo.ToolTip = "Guardar";
                                Btn_Nuevo.Enabled = true;
                                Btn_Nuevo.Visible = true;
                                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                            }
                            else 
                            {
                                //Boton Modificar
                                Btn_Modificar.ToolTip = "Actualizar";
                                Btn_Modificar.Enabled = true;
                                Btn_Modificar.Visible = true;
                                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                                Btn_Nuevo.Visible = false;
                            }
                            //Boton Salir
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Salir.Enabled = true;
                            Btn_Salir.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                            break;
                    }//fin del switch
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al habilitar el  Estado_Botones ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partidas_Asignadas
            ///DESCRIPCIÓN          : Metodo para crear las csolumnas del datatable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Crear_Dt_Partidas_Asignadas() {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                try 
                {
                    Dt_Partidas_Asignadas.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
                    Dt_Partidas_Asignadas.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("UR", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                    Dt_Partidas_Asignadas.Columns.Add("ID", System.Type.GetType("System.String"));

                    Session["Dt_Partidas_Asignadas"] = (DataTable)Dt_Partidas_Asignadas;
                }
                catch (Exception ex) {
                    throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Partidas_Asignadas
            ///DESCRIPCIÓN          : Metodo para agregar los datos de las partidas asignadas al datatable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Noviembre/2011
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego el campo Subnivel Presupuestas a la calendarizacion
            ///*********************************************************************************************************
            private void Agregar_Fila_Dt_Partidas_Asignadas()
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                DataRow Fila;
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                try
                {
                    Fila = Dt_Partidas_Asignadas.NewRow();
                    Fila["DEPENDENCIA_ID"] = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
                    Fila["PROYECTO_ID"] = Cmb_Programa.SelectedItem.Value.Trim();
                    Fila["CAPITULO_ID"] = Cmb_Capitulos.SelectedItem.Value.Trim();
                    Fila["PARTIDA_ID"] = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
                    if (!string.IsNullOrEmpty(Hf_Producto_ID.Value.Trim())) //verificamos si se selecciono un producto
                    {
                        Fila["PRODUCTO_ID"] = Cmb_Producto.SelectedItem.Value.Trim();
                        Fila["PRECIO"] = Convert.ToDouble(String.IsNullOrEmpty(Hf_Precio.Value.Trim()) ? "0" : Hf_Precio.Value.Trim());
                    }
                    else {
                        Fila["PRODUCTO_ID"] = String.Empty;
                        Fila["PRECIO"] = 0;
                    }
                    
                    Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();

                    if (!string.IsNullOrEmpty(Hf_Producto_ID.Value.Trim())) //si existe un producto ponermos su clave y si no la clave de la partida
                    {
                        //Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.IndexOf(" ")).Trim();
                        //Fila["CLAVE_PRODUCTO"] = Cmb_Producto.SelectedItem.Text.Substring(0, Cmb_Producto.SelectedItem.Text.IndexOf(" ")).Trim();

                        Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Trim();
                        Fila["CLAVE_PRODUCTO"] = Cmb_Producto.SelectedItem.Text.Substring(0, Cmb_Producto.SelectedItem.Text.IndexOf("-")).Trim();
                    }
                    else
                    {
                        //Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.IndexOf(" ")).Trim();
                        Fila["CLAVE_PARTIDA"] = Cmb_Partida_Especifica.SelectedItem.Text.Trim();
                        Fila["CLAVE_PRODUCTO"] = String.Empty;
                    }
                    Fila["UR"] = Cmb_Unidad_Responsable.SelectedItem.Text.Substring(Cmb_Unidad_Responsable.SelectedItem.Text.IndexOf(" ")).Trim();
                    String x = Cmb_Unidad_Responsable.SelectedItem.Text.Substring(Cmb_Unidad_Responsable.SelectedItem.Text.IndexOf(" ")).Trim();
                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim()));
                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim()));
                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim()));
                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim()));
                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim()));
                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim()));
                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim()));
                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim()));
                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim()));
                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim()));
                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim()));
                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim()));
                    String Total =  Txt_Total.Text.Replace("$", "");
                    Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Total.Trim()) ? "0" : Total.Trim()));
                    Fila["ID"] = "";

                    Dt_Partidas_Asignadas.Rows.Add(Fila);

                    Session["Dt_Partidas_Asignadas"] = (DataTable)Dt_Partidas_Asignadas;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
                }
            }
            
            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
            ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Limpiar_Sessiones()
            {
                Session["Dt_Partidas_Asignadas"] = null;
                Session.Remove("Dt_Partidas_Asignadas");
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
            ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Noviembre/2011
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 21/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego una validacion, devido a que se agrego un nivel presupuestal 
            ///                       llamado Subnivel Pressuestal
            ///*********************************************************************************************************
            private Boolean Validar_Datos()
            {
                Boolean Datos_Validos = true;
                Lbl_Error.Text  = String.Empty;

                try {
                    if(Cmb_Unidad_Responsable.SelectedIndex <= 0){
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una unidad responsable. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Programa.SelectedIndex <= 0)
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un programa. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Capitulos.SelectedIndex <= 0)
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un capítulo. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Partida_Especifica.SelectedIndex <= 0)
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una partida. <br />";
                        Datos_Validos = false;
                    }
                    //if (Tr_Productos.Visible)
                    //{
                    //    if (String.IsNullOrEmpty(Hf_Producto_ID.Value.Trim()))
                    //    {
                    //        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un producto. <br />";
                    //        Datos_Validos = false;
                    //    }
                    //}
                    if (String.IsNullOrEmpty(Txt_Justificacion.Text.Trim()))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir una justificación. <br />";
                        Datos_Validos = false;
                    }
                    if (String.IsNullOrEmpty(Txt_Total.Text.Trim()) || Txt_Total.Text.Trim() == "0.00")
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir la cantidad presupuestal para los meses. <br />";
                        Datos_Validos = false;
                    }
                    else 
                    {
                        
                    }
                    
                    return Datos_Validos;
                }
                catch(Exception ex)
                {
                    throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
                }
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Justificacion
            ///DESCRIPCIÓN          : Metodo para validar que la justificacion sea diferente
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 07/Septiembre/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN...: 
            ///*********************************************************************************************************
            private Boolean Validar_Justificacion()
            {
                Boolean Datos_Validos = true;
                Lbl_Error.Text = String.Empty;
                DataTable Dt_Partidas_Asignadas = new DataTable();
                //DataRow Fila;
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                try
                {
                
                    foreach (DataRow Registro in Dt_Partidas_Asignadas.Rows)
                    {
                        if (Registro["DEPENDENCIA_ID"].ToString().ToUpper().Equals(Cmb_Unidad_Responsable.SelectedValue.ToUpper().Trim()))
                        {
                            if (Registro["PROYECTO_ID"].ToString().ToUpper().Equals(Cmb_Programa.SelectedValue.ToUpper().Trim())) 
                            {
                                if (Registro["PARTIDA_ID"].ToString().Equals(Cmb_Partida_Especifica.SelectedValue.Trim()))
                                {
                                    if (Tr_Productos.Visible)
                                    {
                                        if (Registro["PRODUCTO_ID"].ToString().Equals(Hf_Producto_ID.Value.Trim()))
                                        {
                                            if (Registro["JUSTIFICACION"].ToString().ToUpper().Equals(Txt_Justificacion.Text.ToUpper())) 
                                            {
                                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ya existe un registro con estos datos y con la misma justificación, cambie la justificación. <br />";
                                                Datos_Validos = false;
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        if (Registro["JUSTIFICACION"].ToString().ToUpper().Equals(Txt_Justificacion.Text.ToUpper()))
                                        {
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ya existe un registro con estos datos y con la misma justificación, cambie la justificación. <br />";
                                            Datos_Validos = false;
                                        }
                                    }
                                }
                            }
                        }
                    }
                   
                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de validar que la partida nosea igual Error[" + ex.Message + "]");
                }
            }
            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Mostrar_Ocultar_Error
            ///DESCRIPCIÓN          : Metodo para mostrar u ocultar los errores
            ///PROPIEDADES          1 Estatus ocultar o mostrar
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Mostrar_Ocultar_Error(Boolean Estatus)
            {
                Lbl_Error.Visible = Estatus;
                Lbl_Encanezado_Error.Visible = Estatus;
                Img_Error.Visible = Estatus;
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : LLenar_Presupuesto_Dependencia
            ///DESCRIPCIÓN          : Metodo para mostrar los datos del presupuesto dela dependencia
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void LLenar_Presupuesto_Dependencia() 
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Datos = new DataTable();
                DataTable Dt_Comentarios = new DataTable();
                String Estatus = "EN CONSTRUCCION";

                try 
                {
                    Cmb_Programa.SelectedIndex = -1;
                    Txt_Limite_Presupuestal.Text = "";
                    Hf_Anio.Value = "";
                    Hf_Fte_Financiamiento.Value = "";
                    Hf_Programa.Value = "";
                    Cmb_Estatus.SelectedIndex = -1;
                    Txt_Comentarios.Text = "";

                    Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    Hf_Tipo_Calendario.Value = "Empleado";
                    Dt_Datos = Negocio.Consultar_Presupuesto_Dependencia();
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count <= 0)
                        {
                            Negocio.P_Empleado_ID = String.Empty;
                            Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                            Dt_Datos = Negocio.Consultar_Presupuesto_Dependencia();
                            Hf_Tipo_Calendario.Value = "Dependencia";
                            
                        }
                    }
                   

                    if (Dt_Datos != null) 
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Llenar_Combo_Unidad_Responsable();

                            if (!String.IsNullOrEmpty(Dt_Datos.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Datos.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim()));
                            }
                            else
                            {
                                Cmb_Programa.Enabled = true;
                            }

                            
                            Txt_Limite_Presupuestal.Text = String.Format("{0:c}", Dt_Datos.Rows[0]["LIMITE_PRESUPUESTAL"]);
                            Hf_Anio.Value = Dt_Datos.Rows[0]["ANIO_PRESUPUESTAR"].ToString().Trim();
                            Hf_Fte_Financiamiento.Value = Dt_Datos.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString().Trim();
                            Hf_Programa.Value = Dt_Datos.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                            
                            if (Dt_Datos.Rows[0]["ESTATUS"].ToString().Trim().Equals("RECHAZADO"))
                            {
                                Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                                Dt_Comentarios = Negocio.Consultar_Comentarios();
                                if (Dt_Comentarios != null) {
                                    if (Dt_Comentarios.Rows.Count > 0) { 
                                        Tr_Comentarios.Visible = true;
                                        Txt_Comentarios.Text = "";
                                        Txt_Comentarios.Text = Dt_Comentarios.Rows[0]["COMENTARIO"].ToString().Trim();
                                        Cmb_Estatus.Items.Clear();
                                        Cmb_Estatus.Items.Insert(0, new ListItem("RECHAZADO", "RECHAZADO"));
                                        Cmb_Estatus.Items.Insert(1, new ListItem("EN CONSTRUCCION", "EN CONSTRUCCION"));
                                        Cmb_Estatus.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));
                                        Estatus = "RECHAZADO";
                                    }
                                }
                            }
                            else 
                            {
                                if (Dt_Datos.Rows[0]["ESTATUS"].ToString().Trim().Equals("AUTORIZADO")
                                    || Dt_Datos.Rows[0]["ESTATUS"].ToString().Trim().Equals("CARGADO"))
                                {
                                    Cmb_Estatus.Items.Clear();
                                    Cmb_Estatus.Items.Insert(0, new ListItem("AUTORIZADO", "AUTORIZADO"));
                                    Estatus = "AUTORIZADO";
                                }
                                else 
                                {
                                    Estatus = Dt_Datos.Rows[0]["ESTATUS"].ToString().Trim();
                                }
                                Tr_Comentarios.Visible = false;
                                Txt_Comentarios.Text = "";
                            }
                            Cmb_Estatus.SelectedValue = Estatus;
                        }
                    }

                }
                catch(Exception ex)
                {
                    throw new Exception("Error al tratar de llenar los datos del presupuesto de la dependencia Error[" + ex.Message + "]");
                }
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Presupuestado
            ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva presupuestodo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Calcular_Total_Presupuestado() 
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                Double Total = 0.00;
                Double Presupuesto = 0.00;
                
                try 
                {
                    if (Dt_Partidas_Asignadas != null) 
                    {
                       if(Dt_Partidas_Asignadas.Rows.Count > 0)
                       {
                           foreach(DataRow Dr in Dt_Partidas_Asignadas.Rows)
                           {
                               Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                           }
                       }
                    }
                    Txt_Total_Ajuste.Text = "";
                    Txt_Total_Ajuste.Text = String.Format("{0:c}", Total);
                    
                    //VALIDAMOS SI EL PRESUPUESTO NO ES MAYOR AL QUE SE OTORGO
                    Presupuesto = Convert.ToDouble(string.IsNullOrEmpty(Txt_Limite_Presupuestal.Text.Trim()) ? "0" : Txt_Limite_Presupuestal.Text.Trim().Replace("$",""));
                    Txt_Presupuesto_Restante.Text = "";
                    Txt_Presupuesto_Restante.Text = String.Format("{0:c}", (Presupuesto - Total));
                    if (Total > Presupuesto)
                    {
                        Lbl_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Verificar, se ha excedido el límite presupuestal otorgado";
                        Mostrar_Ocultar_Error(true);
                        Btn_Agregar.Enabled = false;
                        Btn_Modificar.Enabled = false;
                        Btn_Nuevo.Enabled = false;
                    }
                    else {
                        Btn_Agregar.Enabled = true;
                        Btn_Nuevo.Enabled = true;
                        Btn_Modificar.Enabled = true;
                        Mostrar_Ocultar_Error(false);
                        Lbl_Error.Text = "";
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al calcular el total del presupuesto. Error[" + ex.Message + "]");
                }

            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Consultar_Combo_Stock
            ///DESCRIPCIÓN          : Metodo para consultar si una partida es de stock o no
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Consultar_Combo_Stock() 
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Partida = new DataTable();
                try 
                {
                    if (Cmb_Partida_Especifica.SelectedIndex > 0) {
                        Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                        Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.Trim();
                        Dt_Partida = Negocio.Consultar_Partida_Stock();
                        if(Dt_Partida != null){
                            if (Dt_Partida.Rows.Count > 0)
                            {
                                Cmb_Stock.SelectedValue = "SI";
                            }
                            else {
                                Cmb_Stock.SelectedValue = "NO";
                            }
                        }
                        else
                        {
                            Cmb_Stock.SelectedValue = "NO";
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al calcular el total del presupuesto. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Partida_Asignada
            ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Grid_Partida_Asignada()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Session = new DataTable();
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas"];
                try
                {
                    if (Dt_Session != null)
                    {
                        if (Dt_Session.Rows.Count > 0)
                        {
                            Dt_Partidas_Asignadas = Dt_Session; ;
                        }
                        else
                        {
                            if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                            {
                                if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                                {
                                    Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                                    Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();

                                    //if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                                    //{
                                    //    if (Dt_Partidas_Asignadas.Rows.Count <= 0)
                                    //    {
                                    //        Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                                    //        Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                                    //    }
                                    //}

                                    Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                                }
                            }
                            else
                            {
                                Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                                Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();

                                if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                                {
                                    if (Dt_Partidas_Asignadas.Rows.Count < 0)
                                    {
                                        Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                                        Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                                    }
                                }

                                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                            }
                        }
                    }
                    else
                    {
                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                        {
                            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                            {
                                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                                Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();

                                if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                                {
                                    if (Dt_Partidas_Asignadas.Rows.Count < 0)
                                    {
                                        Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                                        Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                                    }
                                }

                                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                            }
                        }
                        else 
                        {
                            Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                            Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas();

                            if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                            {
                                if (Dt_Partidas_Asignadas.Rows.Count < 0)
                                {
                                    Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                                    Dt_Partidas_Asignadas = Negocio.Consultar_Partidas_Asignadas_Anteriores();
                                }
                            }

                            Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                        }
                    }
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            Obtener_Consecutivo_ID();
                            Dt_Partidas_Asignadas = Crear_Dt_Partida_Asignada();
                            Grid_Partida_Asignada.Columns[1].Visible = true;
                            Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                            Grid_Partida_Asignada.DataBind();
                            Grid_Partida_Asignada.Columns[1].Visible = false;
                        }
                        else
                        {
                            Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                            Grid_Partida_Asignada.DataBind();
                        }
                    }
                    else 
                    {
                        Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Partida_Asignada
            ///DESCRIPCIÓN          : Metodo para crear el datatable de las partidas asignadas del grid anidado
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Crear_Dt_Partida_Asignada()
            {
                DataTable Dt_Partida_Asignada = new DataTable();
                DataTable Dt_Session = new DataTable();
                Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas"];
                String Partida_Id = string.Empty;
                Double Ene = 0.00;
                Double Feb = 0.00;
                Double Mar = 0.00;
                Double Abr = 0.00;
                Double May = 0.00;
                Double Jun = 0.00;
                Double Jul = 0.00;
                Double Ago = 0.00;
                Double Sep = 0.00;
                Double Oct = 0.00;
                Double Nov = 0.00;
                Double Dic = 0.00;
                Double Total = 0.00;
                DataRow Fila;
                String Clave = String.Empty;
                Boolean Iguales;

                try
                {
                    if (Dt_Session != null)
                    {
                        if (Dt_Session.Rows.Count > 0)
                        {
                            //creamos las columnas del datatable donde se guardaran los datos
                            Dt_Partida_Asignada.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("CLAVE", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_ENE", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_FEB", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_MAR", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_ABR", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_MAY", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_JUN", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_JUL", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_AGO", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_SEP", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_OCT", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_NOV", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL_DIC", System.Type.GetType("System.String"));
                            Dt_Partida_Asignada.Columns.Add("TOTAL", System.Type.GetType("System.String"));

                            
                            foreach(DataRow Dr_Sessiones in Dt_Session.Rows)
                            {
                                //Obtenemos la partida id y la clave
                                 Partida_Id = Dr_Sessiones["PARTIDA_ID"].ToString().Trim();
                                 Clave = Dr_Sessiones["CLAVE_PARTIDA"].ToString().Trim();
                                 Iguales = false;
                                //verificamos si la partida no a sido ya agrupada
                                 if (Dt_Partida_Asignada.Rows.Count > 0)
                                 { 
                                    foreach(DataRow Dr_Partidas in Dt_Partida_Asignada.Rows)
                                    {
                                        if (Dr_Partidas["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                        {
                                            Iguales = true;
                                        }
                                    }
                                 }
                                
                                // tomamos los datos del datatable para agrupar las partidas asignadas
                                 if(!Iguales)
                                 {
                                    foreach(DataRow Dr_Session in Dt_Session.Rows)
                                    {
                                        if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_Id))
                                        {
                                            Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                            Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                            Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                            Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                            May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                            Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                            Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                            Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                            Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                            Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                            Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                            Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                        }
                                    }
                                    Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                                    Fila = Dt_Partida_Asignada.NewRow();
                                    Fila["PARTIDA_ID"] = Partida_Id;
                                    Fila["CLAVE"] = Clave;
                                    Fila["TOTAL_ENE"] = String.Format("{0:##,###,##0.00}", Ene);
                                    Fila["TOTAL_FEB"] = String.Format("{0:##,###,##0.00}", Feb);
                                    Fila["TOTAL_MAR"] = String.Format("{0:##,###,##0.00}", Mar);
                                    Fila["TOTAL_ABR"] = String.Format("{0:##,###,##0.00}", Abr);
                                    Fila["TOTAL_MAY"] = String.Format("{0:##,###,##0.00}", May);
                                    Fila["TOTAL_JUN"] = String.Format("{0:##,###,##0.00}", Jun);
                                    Fila["TOTAL_JUL"] = String.Format("{0:##,###,##0.00}", Jul);
                                    Fila["TOTAL_AGO"] = String.Format("{0:##,###,##0.00}", Ago);
                                    Fila["TOTAL_SEP"] = String.Format("{0:##,###,##0.00}", Sep);
                                    Fila["TOTAL_OCT"] = String.Format("{0:##,###,##0.00}", Oct);
                                    Fila["TOTAL_NOV"] = String.Format("{0:##,###,##0.00}", Nov);
                                    Fila["TOTAL_DIC"] = String.Format("{0:##,###,##0.00}", Dic);
                                    Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                                    Dt_Partida_Asignada.Rows.Add(Fila);

                                    Ene = 0.00;
                                    Feb = 0.00;
                                    Mar = 0.00;
                                    Abr = 0.00;
                                    May = 0.00;
                                    Jun = 0.00;
                                    Jul = 0.00;
                                    Ago = 0.00;
                                    Sep = 0.00;
                                    Oct = 0.00;
                                    Nov = 0.00;
                                    Dic = 0.00;
                                    Total = 0.00;
                                }
                            }
                        }
                    }
                    return Dt_Partida_Asignada;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Detalles
            ///DESCRIPCIÓN          : Metodo para crear el datatable de los detalles de las partidas asignadas del grid 
            ///PROPIEDADES          1 Partida_ID del cual obtendremos los detalles
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/ Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego el subnivel presupuestal
            ///*********************************************************************************************************
            private DataTable Crear_Dt_Detalles(String Partida_ID)
            {
                DataTable Dt_Detalle = new DataTable();
                DataTable Dt_Session = new DataTable();
                Dt_Session = (DataTable)Session["Dt_Partidas_Asignadas"];
                DataRow Fila;
                String Partida = String.Empty;

                try
                {
                    if (Dt_Session != null)
                    {
                        if (Dt_Session.Rows.Count > 0)
                        {
                            Dt_Detalle.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
                            Dt_Detalle.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("UR", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("ENERO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("MARZO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("MAYO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("JULIO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                            Dt_Detalle.Columns.Add("ID", System.Type.GetType("System.String"));

                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["PARTIDA_ID"].ToString().Trim().Equals(Partida_ID.Trim()))
                                {
                                    Fila = Dt_Detalle.NewRow();
                                    Fila["DEPENDENCIA_ID"] = Dr_Session["DEPENDENCIA_ID"].ToString().Trim();
                                    Fila["PROYECTO_ID"] = Dr_Session["PROYECTO_ID"].ToString().Trim();
                                    Fila["CAPITULO_ID"] = Dr_Session["CAPITULO_ID"].ToString().Trim();
                                    Fila["PARTIDA_ID"] = Dr_Session["PARTIDA_ID"].ToString().Trim();
                                    Fila["PRODUCTO_ID"] = Dr_Session["PRODUCTO_ID"].ToString().Trim();
                                    Fila["PRECIO"] = Dr_Session["PRECIO"];
                                    Fila["JUSTIFICACION"] = Dr_Session["JUSTIFICACION"].ToString().Trim();
                                    Fila["CLAVE_PARTIDA"] = Dr_Session["CLAVE_PARTIDA"].ToString().Trim();

                                    //obtenemos la clave de la partida
                                    Partida = Dr_Session["CLAVE_PARTIDA"].ToString().Trim().Substring(0, Dr_Session["CLAVE_PARTIDA"].ToString().Trim().IndexOf(" "));

                                    if (Partida.Trim().Equals("3551"))
                                    {
                                        if (String.IsNullOrEmpty(Dr_Session["PRODUCTO_ID"].ToString().Trim()))
                                        {
                                            Fila["CLAVE_PRODUCTO"] = "SERVICIO Y MANTENIMIENTO";
                                        }
                                        else 
                                        {
                                            Fila["CLAVE_PRODUCTO"] = Dr_Session["CLAVE_PRODUCTO"].ToString().Trim();
                                        }

                                        
                                    }
                                    else
                                    {
                                        Fila["CLAVE_PRODUCTO"] = Dr_Session["CLAVE_PRODUCTO"].ToString().Trim();
                                    }

                                    Fila["UR"] = Dr_Session["UR"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["ENERO"]);
                                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Dr_Session["FEBRERO"]);
                                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MARZO"]);
                                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Dr_Session["ABRIL"]);
                                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Dr_Session["MAYO"]);
                                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JUNIO"]);
                                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Dr_Session["JULIO"]);
                                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Dr_Session["AGOSTO"]);
                                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["SEPTIEMBRE"]);
                                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["OCTUBRE"]);
                                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["NOVIEMBRE"]);
                                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dr_Session["DICIEMBRE"]);
                                    Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Dr_Session["IMPORTE_TOTAL"]);
                                    Fila["ID"] = Dr_Session["ID"].ToString().Trim();
                                    Dt_Detalle.Rows.Add(Fila);
                                }
                            }
                        }
                    }
                    return Dt_Detalle;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de crear la tabla de partidas asignadas Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
            ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Obtener_Consecutivo_ID() 
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                int Contador;
                try 
                {
                    if(Dt_Partidas_Asignadas != null)
                    {
                        if(Dt_Partidas_Asignadas.Columns.Count > 0)
                        {
                            if(Dt_Partidas_Asignadas.Rows.Count > 0){
                                Contador = -1;
                                foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                                {
                                    Contador++;
                                    Dr["ID"] = Contador.ToString().Trim();
                                }
                            }
                        }
                    }
                    Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                }
                catch(Exception ex)
                {
                    throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cargar_Excel
            ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Cargar_Excel(String Ruta, String Extension)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Mostrar_Ocultar_Error(false);
                DataTable Dt_Registros = new DataTable();


                try
                {
                    Dt_Registros = Leer_Excel(Ruta, Extension);
                    Dt_Registros = Crear_Dt_Registros_Carga_Excel(Dt_Registros);

                    Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                    Negocio.P_Dt_Datos = Dt_Registros;
                    Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                    Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                    Negocio.P_Estatus = Cmb_Estatus_P.SelectedItem.Value.Trim();
                    Negocio.P_Empleado_ID = String.Empty;

                    if (Negocio.Guardar_Partidas_Asignadas())
                    {
                        Limpiar_Sessiones();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Carga Exitosa');", true);
                        Calendarizar_Presupuesto_Inicio();
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Leer_Excel
            ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Leer_Excel(String Ruta, String Extension)
            {
                DataTable Dt_Informacion = new DataTable();
                DataSet Ds_Datos = new DataSet();
                OleDbDataAdapter Adaptador = new OleDbDataAdapter();
                OleDbConnection Conexion = new OleDbConnection();
                OleDbCommand Comando = new OleDbCommand();

                try
                {
                    if (Extension.Equals("xls"))
                    {
                        Conexion.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                        "Data Source=" + Ruta + ";" +
                        "Extended Properties= Excel 8.0;";
                    }
                    else if (Extension.Equals("xlsx"))
                    {
                        Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Ruta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                    }

                    if (!String.IsNullOrEmpty(Conexion.ConnectionString))
                    {
                        Conexion.Open();

                        Comando.CommandText = "SELECT * FROM [Presupuesto$]";
                        Comando.Connection = Conexion;
                        
                        Adaptador.SelectCommand = Comando;
                        Adaptador.Fill(Ds_Datos);
                        
                        Conexion.Close();

                        System.IO.File.Delete(Ruta);
        
                    }
                    Dt_Informacion = Ds_Datos.Tables[0];
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
                }
                return Dt_Informacion;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Registros_Carga_Excel
            ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private DataTable Crear_Dt_Registros_Carga_Excel(DataTable Dt_Datos)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Cls_Ope_Con_Polizas_Negocio Rs_Consulta_ID = new Cls_Ope_Con_Polizas_Negocio();
                Cls_Cat_SAP_Partidas_Especificas_Negocio Rs_Consultar_Partida = new Cls_Cat_SAP_Partidas_Especificas_Negocio();
                DataTable Dt_Registros = new DataTable();
                DataTable Dt_Fte_Financiamiento = new DataTable();
                DataTable Dt_Conceptos = new DataTable();
                String Concepto = String.Empty;
                String FF = String.Empty;
                String Concepto_ID = String.Empty;
                String Rubro_ID = String.Empty;
                String Rubro = String.Empty;
                String Clase_ID = String.Empty;
                String Tipo_ID = String.Empty;
                String FF_ID = String.Empty;
                DataRow Fila;
                Int32 Contador = -1;
                DataTable Dt_Pronostico_Ing = new DataTable();
                Boolean Repetido = false;
                String Fuente_Financiamiento_ID = "";
                String Programa_ID = "";
                String Unidad_Responsable_ID = "";
                String Partida_ID = "";
                DataTable Dt_Consulta_ID = new DataTable();

                try
                {
                    Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("DEPENDENCIA_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PROYECTO_ID", System.Type.GetType("System.String"));//programa
                    Dt_Registros.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PRODUCTO_ID", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("PRECIO", System.Type.GetType("System.Double"));
                    Dt_Registros.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("CLAVE_PARTIDA", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("CLAVE_PRODUCTO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("UR", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("ENERO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("MARZO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("MAYO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("JULIO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                    Dt_Registros.Columns.Add("ID", System.Type.GetType("System.String"));

                    Session["Dt_Partidas_Asignadas"] = (DataTable)Dt_Registros;

                    //Ingresos_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    //Dt_Pronostico_Ing = Ingresos_Negocio.Consulta_Pronostico();
                    //Dt_Conceptos = Negocio_Ing.Consultar_Datos_Ing();
                    //Dt_Fte_Financiamiento = Ingresos_Negocio.Consulta_Fte_Financiamiento_Todas();



                    if (Dt_Datos is DataTable)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Registro in Dt_Datos.Rows)
                            {
                                if (Dr_Registro is DataRow)
                                {

                                    //  para el id de fuente de financiamiento
                                    Rs_Consulta_ID.P_Clave_Fte_Financiamiento = Dr_Registro["Fuente"].ToString();
                                    Dt_Consulta_ID = Rs_Consulta_ID.Consulta_ID_Fte_Financiamiento();

                                    if (Dt_Consulta_ID.Rows.Count > 0)
                                    {
                                        Fuente_Financiamiento_ID = Dt_Consulta_ID.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                    }

                                    //  para el id de la unidad responsable
                                    Rs_Consulta_ID.P_Clave_Dependencia = Dr_Registro["UR"].ToString();
                                    Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Dependencia();

                                    if (Dt_Consulta_ID.Rows.Count > 0)
                                    {
                                        Unidad_Responsable_ID = Dt_Consulta_ID.Rows[0]["DEPENDENCIA"].ToString().Trim();
                                    } 
                                    
                                    //  para la partida id
                                    Rs_Consultar_Partida.P_Clave = Dr_Registro["Partida"].ToString();
                                    Dt_Consulta_ID = Rs_Consultar_Partida.Consulta_Partida_Especifica();
                                    if (Dt_Consulta_ID.Rows.Count > 0)
                                    {
                                        Partida_ID = Dt_Consulta_ID.Rows[0]["PARTIDA_ID"].ToString().Trim();
                                    }

                                    //  para los programas
                                    Rs_Consulta_ID.P_Partida_ID = Partida_ID;
                                    Rs_Consulta_ID.P_Clave_Programa = Dr_Registro["Programa"].ToString();
                                    Rs_Consulta_ID.P_Dependencia_ID = Unidad_Responsable_ID;
                                    Dt_Consulta_ID = Rs_Consulta_ID.Consulta_Programas();
                                    if (Dt_Consulta_ID.Rows.Count > 0)
                                    {
                                        Programa_ID = Dt_Consulta_ID.Rows[0]["PROGRAMA_ID"].ToString().Trim();
                                    }
                                    
                                   
                                    //  para buscar los registros y evitar que se ingrese alguno repetido
                                    if (Dt_Registros.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Comparacion in Dt_Registros.Rows)
                                        {
                                            if (Fuente_Financiamiento_ID == Dr_Comparacion["FUENTE_FINANCIAMIENTO_ID"].ToString() &&
                                                    Programa_ID == Dr_Comparacion["PROYECTO_ID"].ToString() &&
                                                    Partida_ID == Dr_Comparacion["PARTIDA_ID"].ToString() &&
                                                    Unidad_Responsable_ID == Dr_Comparacion["DEPENDENCIA_ID"].ToString())
                                            {
                                                Repetido = true;
                                                break;
                                            }

                                            else
                                            {
                                                Repetido = false;
                                            }
                                        }// fin del foreach

                                    }// fin de Dt_Registros > 0

                                    if (!Repetido)
                                    {
                                        Contador++;

                                        Fila = Dt_Registros.NewRow();
                                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Fuente_Financiamiento_ID;
                                        Fila["DEPENDENCIA_ID"] = Unidad_Responsable_ID;
                                        Fila["PROYECTO_ID"] = Programa_ID;
                                        //Fila["CAPITULO_ID"] = Rubro_ID.Trim();
                                        Fila["PARTIDA_ID"] = Partida_ID;

                                        Fila["PRODUCTO_ID"] = String.Empty; //Cmb_UR.SelectedItem.Value.Trim();
                                        //Fila["PRECIO"] = 0.0;
                                        Fila["JUSTIFICACION"] = Clase_ID.Trim();
                                        Fila["CLAVE_PARTIDA"] = Concepto_ID.Trim();
                                        Fila["CLAVE_PRODUCTO"] = String.Empty;
                                        Fila["UR"] = String.Empty; //Cmb_UR.SelectedItem.Text.Substring(Cmb_UR.SelectedItem.Text.IndexOf(" ")).Trim();

                                        Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Enero"].ToString().Trim()) ? "0" : Dr_Registro["Enero"].ToString().Trim().Replace(",", ""));
                                        Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Febrero"].ToString().Trim()) ? "0" : Dr_Registro["Febrero"].ToString().Trim().Replace(",", ""));
                                        Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Marzo"].ToString().Trim()) ? "0" : Dr_Registro["Marzo"].ToString().Trim().Replace(",", ""));
                                        Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Abril"].ToString().Trim()) ? "0" : Dr_Registro["Abril"].ToString().Trim().Replace(",", ""));
                                        Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Mayo"].ToString().Trim()) ? "0" : Dr_Registro["Mayo"].ToString().Trim().Replace(",", ""));
                                        Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Junio"].ToString().Trim()) ? "0" : Dr_Registro["Junio"].ToString().Trim().Replace(",", ""));
                                        Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Julio"].ToString().Trim()) ? "0" : Dr_Registro["Julio"].ToString().Trim().Replace(",", ""));
                                        Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Agosto"].ToString().Trim()) ? "0" : Dr_Registro["Agosto"].ToString().Trim().Replace(",", ""));
                                        Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Septiembre"].ToString().Trim()) ? "0" : Dr_Registro["Septiembre"].ToString().Trim().Replace(",", ""));
                                        Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Octubre"].ToString().Trim()) ? "0" : Dr_Registro["Octubre"].ToString().Trim().Replace(",", ""));
                                        Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Noviembre"].ToString().Trim()) ? "0" : Dr_Registro["Noviembre"].ToString().Trim().Replace(",", ""));
                                        Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Diciembre"].ToString().Trim()) ? "0" : Dr_Registro["Diciembre"].ToString().Trim().Replace(",", ""));
                                        Fila["IMPORTE_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Registro["Total"].ToString().Trim()) ? "0" : Dr_Registro["Total"].ToString().Trim().Replace(",", ""));
                                        Fila["ID"] = Contador.ToString().Trim();

                                        Dt_Registros.Rows.Add(Fila);

                                    }// fin de repetir

                                }// fin de Dr_Registro is datarow

                            }// fin del foreach

                        }// fin de Dt_Datos > 0

                    }// fin de Dt_Datos is datable

                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
                }
                return Dt_Registros;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Calcular_Restante
            ///DESCRIPCIÓN          : Metodo para calcular el restante del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 01/Marzo/2013 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Calcular_Restante() 
            {
                Double Total = 0.00;
                Double Limite_Presupuestal = 0.00;
                Double Restante = 0.00;
                Double Total_Presupuestado = 0.00;

                Total = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim().Replace(",", "").Replace("$", ""));
                Limite_Presupuestal = Convert.ToDouble(String.IsNullOrEmpty(Txt_Limite_Presupuestal.Text.Trim()) ? "0" : Txt_Limite_Presupuestal.Text.Trim().Replace(",", "").Replace("$", ""));
                Total_Presupuestado = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Ajuste.Text.Trim()) ? "0" : Txt_Total_Ajuste.Text.Trim().Replace(",", "").Replace("$", ""));

                Restante = Limite_Presupuestal - Total_Presupuestado - Total;

                Txt_Presupuesto_Restante.Text = String.Format("{0:n}", Restante); 
            }
        #endregion

        #region (Metodos Combos)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Unidad_Responsable
            ///DESCRIPCIÓN          : Metodo para llenar el combo de la unidad responsable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Unidad_Responsable()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Cls_Ope_Psp_Limite_Presupuestal_Negocio Limite_Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();
                DataTable Dt_Dependencias = new DataTable();
                String Dependencia_Empleado = string.Empty;
                try
                {
                    if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
                    {
                        Dt_Dependencias = Calendarizar_Negocio.Consultar_Unidad_Responsable();
                    }
                    else 
                    {
                        Limite_Negocio.P_Accion = String.Empty;
                        Dt_Dependencias = Limite_Negocio.Consultar_Unidades_Responsables_Sin_Asignar();
                    }

                   
                    if(Dt_Dependencias != null){
                        if(Dt_Dependencias.Rows.Count > 0){
                            Cmb_Unidad_Responsable.Items.Clear();
                            Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                            Cmb_Unidad_Responsable.DataTextField = "NOMBRE";
                            Cmb_Unidad_Responsable.DataSource = Dt_Dependencias;
                            Cmb_Unidad_Responsable.DataBind();
                            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        }
                    }

                    if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
                    {

                        Dependencia_Empleado = Cls_Sessiones.Dependencia_ID_Empleado; //obtenemos la dependencia del usuario logueado
                        if (!string.IsNullOrEmpty(Dependencia_Empleado))
                        {
                            Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dependencia_Empleado));
                        }
                        Cmb_Unidad_Responsable.Enabled = true;
                        Llenar_Combo_Programas();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Unidad_Responsable ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos
            ///DESCRIPCIÓN          : Metodo para llenar el combo de los capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Capitulos()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Capitulos = new DataTable();
                try
                {
                    if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                    {
                        if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                        {
                            Calendarizar_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
                        }
                    }
                    else 
                    {
                        Calendarizar_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    }
                    //Calendarizar_Negocio.P_Capitulos = "'2000','5000'";
                    Dt_Capitulos = Calendarizar_Negocio.Consultar_Capitulos();
                    if (Dt_Capitulos != null)
                    {
                        if (Dt_Capitulos.Rows.Count > 0)
                        {
                            Cmb_Capitulos.Items.Clear();
                            Cmb_Capitulos.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
                            Cmb_Capitulos.DataTextField = "NOMBRE";
                            Cmb_Capitulos.DataSource = Dt_Capitulos;
                            Cmb_Capitulos.DataBind();
                        }
                    }
                    Cmb_Capitulos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Capitulos ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
            ///DESCRIPCIÓN          : Metodo para llenar el combo de las partidas de un capitulo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Partidas()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Partidas = new DataTable();
                try
                {
                    if(Cmb_Capitulos.SelectedIndex > 0){
                        Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulos.SelectedItem.Value.Trim();
                        Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                        if (Dt_Partidas != null)
                        {
                            if (Dt_Partidas.Rows.Count > 0)
                            {
                                Cmb_Partida_Especifica.Items.Clear();
                                Cmb_Partida_Especifica.DataValueField = Cat_Com_Partidas.Campo_Partida_ID;
                                Cmb_Partida_Especifica.DataTextField = "NOMBRE";
                                Cmb_Partida_Especifica.DataSource = Dt_Partidas;
                                Cmb_Partida_Especifica.DataBind();
                                Cmb_Partida_Especifica.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Partidas ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
            ///DESCRIPCIÓN          : Metodo para llenar el combo de las partidas de un capitulo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Productos()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Productos = new DataTable();
                String Partida = String.Empty;

                try
                {
                    if (Cmb_Partida_Especifica.SelectedIndex > 0)
                    {
                        Calendarizar_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
                        Dt_Productos = Calendarizar_Negocio.Consultar_Productos();
                        if (Dt_Productos != null)
                        {
                            if (Dt_Productos.Rows.Count > 0)
                            {
                                Cmb_Producto.Items.Clear();
                                Cmb_Producto.DataValueField = Cat_Com_Productos.Campo_Producto_ID;
                                Cmb_Producto.DataTextField = "CLAVE_PRODUCTO";
                                Cmb_Producto.DataSource = Dt_Productos;
                                Cmb_Producto.DataBind();
                                
                                //obtenemos la clave de la partida
                                Partida = Cmb_Partida_Especifica.SelectedItem.Text.Trim().Substring(0, Cmb_Partida_Especifica.SelectedItem.Text.Trim().IndexOf(" "));

                                if (Partida.Trim().Equals("3551"))
                                {
                                    Cmb_Producto.Items.Insert(0, new ListItem("SERVICIO Y MANTENIMIENTO", ""));
                                }
                                else 
                                {
                                    Cmb_Producto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                                }
                                
                                Tr_Productos.Visible = true;
                            }
                            else
                            {
                                Tr_Productos.Visible = false;
                            }
                        }
                        else {
                            Tr_Productos.Visible = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Productos ERROR[" + ex.Message + "]");
                }
            }
    
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
            ///DESCRIPCIÓN          : Metodo para llenar el combo del estatus del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Estatus()
            {
                try
                {
                    Cmb_Estatus.Items.Clear();
                    Cmb_Estatus.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Estatus.Items.Insert(1, new ListItem("EN CONSTRUCCION", "EN CONSTRUCCION"));
                    Cmb_Estatus.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));

                    Cmb_Estatus_P.Items.Clear();
                    Cmb_Estatus_P.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Estatus_P.Items.Insert(1, new ListItem("EN CONSTRUCCION", "EN CONSTRUCCION"));
                    Cmb_Estatus_P.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Partidas ERROR[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
            ///DESCRIPCIÓN          : Llena el combo de programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Programas()
            {
                Cls_Ope_Psp_Limite_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();//Instancia con la clase de negocios
                DataTable Dt_Programas = new DataTable();


                Cmb_Programa.Items.Clear(); //limpiamos el combo
                if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                {
                    Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
                    Dt_Programas = Negocio.Consultar_Programa_Unidades_Responsables();

                    Cmb_Programa.DataSource = Dt_Programas;
                    Cmb_Programa.DataTextField = "NOMBRE";
                    Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                    Cmb_Programa.DataBind();
                    Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

                    if (Dt_Programas != null)
                    {
                        if (Dt_Programas.Rows.Count > 0)
                        {
                            Cmb_Programa.SelectedIndex = 1;
                        }
                    }
                }
            }
        #endregion
       
    #endregion

    #region EVENTOS

        #region EVENTOS GENERALES
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Salir_Click(object sender, EventArgs e)
            {
                switch (Btn_Salir.ToolTip)
                {
                    case "Cancelar":
                        Limpiar_Sessiones();
                        Calendarizar_Presupuesto_Inicio();
                        Pnl_Busqueda_Contenedor.Style.Add("display", "none");
                        Pnl_Datos_Generales.Visible = true;
                        Div_Carga_Masiva.Style.Add("display", "none");
                        break;

                    case "Salir":
                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "window.close();", true);
                        break;
                }//fin del switch
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton nuevo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Nuevo_Click(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Mostrar_Ocultar_Error(false);
                DataTable Dt_Partidas_Asignadas = new DataTable();
                try
                {
                    switch (Btn_Nuevo.ToolTip)
                    {
                        case "Nuevo":
                            Estado_Botones("nuevo");
                            Limpiar_Formulario("Todo");
                            Limpiar_Sessiones();
                            Habilitar_Forma(true);
                            if (Cmb_Unidad_Responsable.SelectedIndex <= 0)
                            {
                                Cmb_Capitulos.Enabled = false;
                            }
                            Llenar_Grid_Partida_Asignada();
                            Calcular_Total_Presupuestado();
                            break;
                        case "Guardar":
                            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                            if (Dt_Partidas_Asignadas != null)
                            {
                                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                                {

                                    if (Cmb_Estatus.SelectedIndex <= 0)
                                    {
                                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un estatus. <br />";
                                        Mostrar_Ocultar_Error(true);
                                    }
                                    else 
                                    {
                                        Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                                        Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value.Trim();
                                        Negocio.P_Dt_Datos = Dt_Partidas_Asignadas;
                                        Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                                        Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                                        Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();

                                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                                        {
                                            Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                                        }
                                        else {
                                            Negocio.P_Empleado_ID = string.Empty;
                                        }

                                        if (Negocio.Guardar_Partidas_Asignadas())
                                        {
                                            Limpiar_Sessiones();
                                            Pnl_Busqueda_Contenedor.Style.Add("display", "none");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Alta Exitosa');", true);
                                            Calendarizar_Presupuesto_Inicio();
                                        }
                                    }
                                }
                                else
                                {
                                    Lbl_Error.Text = " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Agregar partidas asignadas ";
                                    Mostrar_Ocultar_Error(true);
                                }
                                
                            }
                            else 
                            {
                                Lbl_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Agregar partidas asignadas ";
                                Mostrar_Ocultar_Error(true);
                            }
                            break;
                    }//fin del swirch
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de dar de alta los datos ERROR[" + ex.Message + "]");
                }
            }//fin del boton Nuevo

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
            ///DESCRIPCIÓN          : Evento del boton modificar
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Modificar_Click(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Mostrar_Ocultar_Error(false);
                DataTable Dt_Partidas_Asignadas = new DataTable();
                try
                {
                    switch (Btn_Modificar.ToolTip)
                    {
                        //Validacion para actualizar un registro y para habilitar los controles que se requieran
                        case "Modificar":
                            Estado_Botones("modificar");
                            Habilitar_Forma(true);
                            Grid_Partida_Asignada.SelectedIndex = -1;
                            Limpiar_Formulario("Todo");
                            Limpiar_Sessiones();
                            Llenar_Grid_Partida_Asignada();
                            Llenar_Combo_Estatus();
                            LLenar_Presupuesto_Dependencia();
                            break;
                        case "Actualizar":
                            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                            if (Dt_Partidas_Asignadas != null)
                            {
                                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                                {
                                    if (Cmb_Estatus.SelectedIndex <= 0)
                                    {
                                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un estatus. <br />";
                                        Mostrar_Ocultar_Error(true);
                                    }
                                    else
                                    {
                                        Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.Trim();
                                        Negocio.P_Anio_Presupuesto = Hf_Anio.Value.Trim();
                                        Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value.Trim();
                                        Negocio.P_Dt_Datos = Dt_Partidas_Asignadas;
                                        Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                                        Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;
                                        Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();
                                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                                        {
                                            Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                                            Negocio.P_Dependencia_ID = string.Empty;
                                        }
                                        else 
                                        {
                                            Negocio.P_Empleado_ID = string.Empty;
                                        }
                                        if (Negocio.Modificar_Partidas_Asignadas())
                                        {
                                            Limpiar_Sessiones();
                                            Pnl_Busqueda_Contenedor.Style.Add("display", "none");
                                            Div_Carga_Masiva.Style.Add("display", "none");
                                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Modificación Exitosa');", true);
                                            Calendarizar_Presupuesto_Inicio();
                                        }
                                    }
                                }
                                else
                                {
                                    Lbl_Error.Text = " Agregar partidas asignadas ";
                                    Mostrar_Ocultar_Error(true);
                                }
                            }
                            else
                            {
                                Lbl_Error.Text = " Agregar partidas asignadas ";
                                Mostrar_Ocultar_Error(true);
                            }
                            break;
                    }//fin del switch
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de modificar los datos ERROR[" + ex.Message + "]");
                }
            }//fin de Modificar

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Click
            ///DESCRIPCIÓN          : Evento del boton de agregar un nuevo presupuesto de un producto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Agregar_Click(object sender, EventArgs e)
            {   
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                Mostrar_Ocultar_Error(false);
                try
                {
                    if (Validar_Datos())
                    {
                        if (Validar_Justificacion())
                        {
                            if (Dt_Partidas_Asignadas == null)
                            {
                                Crear_Dt_Partidas_Asignadas();
                                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                            }

                            Agregar_Fila_Dt_Partidas_Asignadas();
                            Limpiar_Formulario("Datos_Partida");
                            Llenar_Grid_Partida_Asignada();
                            Calcular_Total_Presupuestado();
                        }
                        else 
                        {
                            Mostrar_Ocultar_Error(true);
                        }
                    }
                    else 
                    {
                        Mostrar_Ocultar_Error(true);
                    }
                    Calcular_Restante();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
            ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 14/Noviembre/2011 
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego el campo  subnivel presupuestal a la calendarizacion
            ///*********************************************************************************************************
            protected void Btn_Eliminar_Click(object sender, EventArgs e) 
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                ImageButton Btn_Eliminar = (ImageButton)sender;
                Int32 No_Fila = -1;
                String Id = String.Empty;
                Double Ene = 0.00;
                Double Feb = 0.00;
                Double Mar = 0.00;
                Double Abr = 0.00;
                Double May = 0.00;
                Double Jun = 0.00;
                Double Jul = 0.00;
                Double Ago = 0.00;
                Double Sep = 0.00;
                Double Oct = 0.00;
                Double Nov = 0.00;
                Double Dic = 0.00;
                Double Precio = 0.00;
                try 
                {
                    Id = Btn_Eliminar.CommandArgument.ToString().Trim();
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            foreach(DataRow Dr in Dt_Partidas_Asignadas.Rows)
                            {
                                No_Fila++;
                                if(Dr["ID"].ToString().Trim().Equals(Id)){
                                    if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                                    {
                                        Llenar_Combo_Unidad_Responsable();
                                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                        Llenar_Combo_Programas();
                                        Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                                    }
                                    Llenar_Combo_Capitulos();
                                    LLenar_Presupuesto_Dependencia();
                                    Cmb_Capitulos.SelectedIndex = Cmb_Capitulos.Items.IndexOf(Cmb_Capitulos.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["CAPITULO_ID"].ToString().Trim()));
                                    Llenar_Combo_Partidas();
                                    Cmb_Partida_Especifica.SelectedIndex = Cmb_Partida_Especifica.Items.IndexOf(Cmb_Partida_Especifica.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PARTIDA_ID"].ToString().Trim()));
                                    Llenar_Combo_Productos();
                                    Consultar_Combo_Stock();
                                    Txt_Justificacion.Text = Dt_Partidas_Asignadas.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();
                                    if (!string.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()))
                                    {
                                        Cmb_Partida_Especifica.Enabled = true;
                                        Cmb_Producto.Enabled = true;
                                        Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()));
                                        Precio = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim());
                                        Hf_Precio.Value = Precio.ToString();
                                        Hf_Producto_ID.Value = Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim();

                                        if (Precio > 0)
                                        {
                                            Ene = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim());
                                            Feb = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim());
                                            Mar = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim());
                                            Abr = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim());
                                            May = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim());
                                            Jun = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim());
                                            Jul = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim());
                                            Ago = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim());
                                            Sep = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim());
                                            Oct = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim());
                                            Nov = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim());
                                            Dic = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim());

                                            Lbl_Cantidad.Text = "Cantidad";
                                            Lbl_Txt_Enero.Text = Convert.ToString(Ene / Precio);
                                            Lbl_Txt_Febrero.Text = Convert.ToString(Feb / Precio);
                                            Lbl_Txt_Marzo.Text = Convert.ToString(Mar / Precio);
                                            Lbl_Txt_Abril.Text = Convert.ToString(Abr / Precio);
                                            Lbl_Txt_Mayo.Text = Convert.ToString(May / Precio);
                                            Lbl_Txt_Junio.Text = Convert.ToString(Jun / Precio);
                                            Lbl_Txt_Julio.Text = Convert.ToString(Jul / Precio);
                                            Lbl_Txt_Agosto.Text = Convert.ToString(Ago / Precio);
                                            Lbl_Txt_Septiembre.Text = Convert.ToString(Sep / Precio);
                                            Lbl_Txt_Octubre.Text = Convert.ToString(Oct / Precio);
                                            Lbl_Txt_Noviembre.Text = Convert.ToString(Nov / Precio);
                                            Lbl_Txt_Diciembre.Text = Convert.ToString(Dic / Precio);
                                        }
                                    }
                                    else
                                    {
                                        if (Tr_Productos.Visible)
                                        {
                                            Cmb_Producto.Enabled = true;
                                        }
                                    }
                                    Cmb_Partida_Especifica.Enabled = true;

                                    Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"]);
                                    Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"]);
                                    Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"]);
                                    Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"]);
                                    Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"]);
                                    Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"]);
                                    Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"]);
                                    Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"]);
                                    Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"]);
                                    Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"]);
                                    Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"]);
                                    Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"]);
                                    Txt_Total.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["IMPORTE_TOTAL"]);

                                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                    Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                                   
                                    Dt_Partidas_Asignadas.Rows.RemoveAt(No_Fila);
                                    Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                                    
                                    //Llenar_Grid_Partida_Asignada();
                                    Obtener_Consecutivo_ID();
                                    Dt_Partidas_Asignadas = Crear_Dt_Partida_Asignada();
                                    Grid_Partida_Asignada.Columns[1].Visible = true;
                                    Grid_Partida_Asignada.DataSource = Dt_Partidas_Asignadas;
                                    Grid_Partida_Asignada.DataBind();
                                    Grid_Partida_Asignada.Columns[1].Visible = false;

                                    Calcular_Total_Presupuestado();
                                    Llenar_Combo_Estatus();
                                    break;
                                }
                            }
                            
                        }
                    }
                    Calcular_Restante();
                }
                catch(Exception ex)
                {
                    throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Cargar_Excel_Click
            ///DESCRIPCIÓN          : Evento del boton de cargar excel
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Cargar_Excel_Click(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Mostrar_Ocultar_Error(false);
                DataTable Dt_Registros = new DataTable();
                String Ruta_Destino = String.Empty;
                String Extension = String.Empty;
                String Archivo = String.Empty;

                try
                {
                    switch (Btn_Cargar_Excel.ToolTip)
                    {
                        //Validacion para actualizar un registro y para habilitar los controles que se requieran
                        case "Cargar Excel":
                            Estado_Botones("excel");
                            Habilitar_Forma(false);
                            Grid_Partida_Asignada.SelectedIndex = -1;
                            Limpiar_Formulario("Todo");
                            Llenar_Grid_Partida_Asignada();
                            Grid_Partida_Asignada.Enabled = false;
                            Pnl_Datos_Generales.Visible = false;
                            Pnl_Cargar_Excel.Visible = true;
                            Pnl_Cargar_Excel.Style.Add("display", "block");
                            break;
                        case "Cargar":
                            Ruta_Destino = Server.MapPath("../../Archivos");
                            if (AFU_Archivo.HasFile)
                            {
                                if (!System.IO.Directory.Exists(Ruta_Destino))
                                {
                                    System.IO.Directory.CreateDirectory(Ruta_Destino);
                                }

                                if (Cmb_Estatus.SelectedIndex > 0)
                                {
                                    Archivo = AFU_Archivo.FileName;
                                    int Posicion = Archivo.IndexOf('.') + 1;
                                    Extension = Archivo.Substring(Posicion);
                                    AFU_Archivo.SaveAs(Ruta_Destino + Archivo);
                                    Ruta_Destino += "\\" + Archivo;
                                    Cargar_Excel(Ruta_Destino , Extension);
                                }
                                else
                                {
                                    Mostrar_Ocultar_Error(true);
                                    Lbl_Encanezado_Error.Text = "Favor de seleccionar un estatus";
                                    Lbl_Error.Text = String.Empty;
                                }
                            }
                            break;
                    }//fin del switch
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de cargar los datos del presupuesto ERROR[" + ex.Message + "]");
                }
            }//fin de Modificar

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : AFU_Archivo_UploadedComplete
            ///DESCRIPCIÓN          : Evento del boton de cargar excel
            ///PROPIEDADES          :
            ///CREO                 : Hugo Enrique Ramírez Aguilera
            ///FECHA_CREO           : 23/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void AFU_Archivo_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
            {
                //****************************************
                //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
                //GUARDA EN LA UBICACION PRESTABLECIDA
                //****************************************
                try
                {
                    if (AFU_Archivo.HasFile)
                    {
                        string currentDateTime = String.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
                        string fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo.FileName).Replace(" ", "_");

                        bool xls = Path.GetExtension(AFU_Archivo.PostedFile.FileName).Contains(".xls");
                        bool xlsx = Path.GetExtension(AFU_Archivo.PostedFile.FileName).Contains(".xlsx");

                        if (xls || xlsx)
                        {
                            String Ruta = Server.MapPath("../../Archivos");
                            AFU_Archivo.SaveAs(Ruta +"\\" + fileNameOnServer);
                        }
                        else
                            return;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_ALimpiar_Click
            ///DESCRIPCIÓN          : Evento del boton de limpiar los campos del presupuesto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 28/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Limpiar_Click(object sender, EventArgs e)
            {
                Mostrar_Ocultar_Error(false);
                try
                {
                    Limpiar_Formulario("Datos_Partida");
                    Calcular_Total_Presupuestado();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de a limpiar los datos del formulario. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Porcentaje_Click
            ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Porcentaje_Click(object sender, EventArgs e)
            {
                //DataTable Dt_Partidas_Asignadas = new DataTable();
                //Double Ene = 0.00;
                //Double Feb = 0.00;
                //Double Mar = 0.00;
                //Double Abr = 0.00;
                //Double May = 0.00;
                //Double Jun = 0.00;
                //Double Jul = 0.00;
                //Double Ago = 0.00;
                //Double Sep = 0.00;
                //Double Oct = 0.00;
                //Double Nov = 0.00;
                //Double Dic = 0.00;
                //Double Total = 0.00;
                //Double Porcentaje;
                //try
                //{
                //    //if (!String.IsNullOrEmpty(Txt_Porcentaje.Text.Trim()))
                //    //{
                //        //Porcentaje = Convert.ToDouble(String.IsNullOrEmpty(Txt_Porcentaje.Text.Trim()) ? "0" : Txt_Porcentaje.Text.Trim()) / 100;
                //        if (Porcentaje > 0 || Porcentaje < 0) 
                //        {
                //            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                //            if (Dt_Partidas_Asignadas != null)
                //            {
                //                if (Dt_Partidas_Asignadas.Rows.Count > 0)
                //                {
                //                    foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                //                    {
                //                        Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString()) ? "0" : Dr["ENERO"].ToString().Trim());
                //                        Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString()) ? "0" : Dr["FEBRERO"].ToString().Trim());
                //                        Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString()) ? "0" : Dr["MARZO"].ToString().Trim());
                //                        Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString()) ? "0" : Dr["ABRIL"].ToString().Trim());
                //                        May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString()) ? "0" : Dr["MAYO"].ToString().Trim());
                //                        Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString()) ? "0" : Dr["JUNIO"].ToString().Trim());
                //                        Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString()) ? "0" : Dr["JULIO"].ToString().Trim());
                //                        Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString()) ? "0" : Dr["AGOSTO"].ToString().Trim());
                //                        Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString()) ? "0" : Dr["SEPTIEMBRE"].ToString().Trim());
                //                        Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString()) ? "0" : Dr["OCTUBRE"].ToString().Trim());
                //                        Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString()) ? "0" : Dr["NOVIEMBRE"].ToString().Trim());
                //                        Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString()) ? "0" : Dr["DICIEMBRE"].ToString().Trim());
                //                        Total = Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());

                //                        Ene = Ene + (Ene * Porcentaje);
                //                        Feb = Feb + (Feb * Porcentaje);
                //                        Mar = Mar + (Mar * Porcentaje);
                //                        Abr = Abr + (Abr * Porcentaje);
                //                        May = May + (May * Porcentaje);
                //                        Jun = Jun + (Jun * Porcentaje);
                //                        Jul = Jul + (Jul * Porcentaje);
                //                        Ago = Ago + (Ago * Porcentaje);
                //                        Sep = Sep + (Sep * Porcentaje);
                //                        Oct = Oct + (Oct * Porcentaje);
                //                        Nov = Nov + (Nov * Porcentaje);
                //                        Dic = Dic + (Dic * Porcentaje);
                //                        Total = Total + (Total * Porcentaje);

                //                        Dr["ENERO"] = String.Format("{0:n}", Ene);
                //                        Dr["FEBRERO"] = String.Format("{0:n}", Feb);
                //                        Dr["MARZO"] = String.Format("{0:n}", Mar);
                //                        Dr["ABRIL"] = String.Format("{0:n}", Abr);
                //                        Dr["MAYO"] = String.Format("{0:n}", May);
                //                        Dr["JUNIO"] = String.Format("{0:n}", Jun);
                //                        Dr["JULIO"] = String.Format("{0:n}", Jul);
                //                        Dr["AGOSTO"] = String.Format("{0:n}", Ago);
                //                        Dr["SEPTIEMBRE"] = String.Format("{0:n}", Sep);
                //                        Dr["OCTUBRE"] = String.Format("{0:n}", Oct);
                //                        Dr["NOVIEMBRE"] = String.Format("{0:n}", Nov);
                //                        Dr["DICIEMBRE"] = String.Format("{0:n}", Dic);
                //                        Dr["IMPORTE_TOTAL"] = String.Format("{0:n}", Total);
                //                    }

                //                    Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Asignadas;
                //                    Llenar_Grid_Partida_Asignada();
                //                    Calcular_Total_Presupuestado();
                //                    Llenar_Combo_Estatus();
                //                    //Txt_Porcentaje.Text = "";
                //                }
                //            }
                //        }
                //    //}
                //    //else 
                //    //{
                //    //    Lbl_Encanezado_Error.Text = "Favor de introducir el porcentaje para aplicar al presupuesto.";
                //    //    Lbl_Error.Text = "";
                //    //    Mostrar_Ocultar_Error(true);
                //    //}
                //}
                //catch (Exception ex)
                //{
                //    throw new Exception("Error al aumentar el porcentaje del presupuesto. Error[" + ex.Message + "]");
                //}
            }
        #endregion
       
        #region EVENTOS GRID
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Noviembre/2011 
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: devido a que se agrego al grid de detalles El subnivel_Presupuestal_ID
            ///*********************************************************************************************************
            protected void Grid_Partida_Asignada_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

                try
                {
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            GridView Grid_Partidas_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Partidas_Asignadas_Detalle");
                            DataTable Dt_Detalles = new DataTable();
                            String Partida_ID = String.Empty;

                            if (e.Row.RowType == DataControlRowType.DataRow)
                            {
                                Partida_ID = e.Row.Cells[1].Text.Trim();
                                Dt_Detalles = Crear_Dt_Detalles(Partida_ID);

                                Grid_Partidas_Detalle.Columns[0].Visible = true;
                                Grid_Partidas_Detalle.Columns[1].Visible = true;
                                Grid_Partidas_Detalle.Columns[2].Visible = true;
                                Grid_Partidas_Detalle.Columns[3].Visible = true;
                                Grid_Partidas_Detalle.Columns[4].Visible = true;
                                Grid_Partidas_Detalle.Columns[5].Visible = true;
                                Grid_Partidas_Detalle.Columns[6].Visible = true;
                                Grid_Partidas_Detalle.Columns[7].Visible = true;
                                Grid_Partidas_Detalle.Columns[8].Visible = true;
                                Grid_Partidas_Detalle.Columns[9].Visible = true;
                                Grid_Partidas_Detalle.Columns[10].Visible = true;
                                Grid_Partidas_Detalle.Columns[25].Visible = true;
                                Grid_Partidas_Detalle.Columns[26].Visible = true;
                                Grid_Partidas_Detalle.DataSource = Dt_Detalles;
                                Grid_Partidas_Detalle.DataBind();
                                Grid_Partidas_Detalle.Columns[1].Visible = false;
                                Grid_Partidas_Detalle.Columns[2].Visible = false;
                                Grid_Partidas_Detalle.Columns[3].Visible = false;
                                Grid_Partidas_Detalle.Columns[4].Visible = false;
                                Grid_Partidas_Detalle.Columns[5].Visible = false;
                                Grid_Partidas_Detalle.Columns[6].Visible = false;
                                Grid_Partidas_Detalle.Columns[7].Visible = false;
                                Grid_Partidas_Detalle.Columns[8].Visible = false;
                                Grid_Partidas_Detalle.Columns[26].Visible = false;
                                if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                                {
                                    Grid_Partidas_Detalle.Columns[9].Visible = false;
                                }
                                else 
                                {
                                    Grid_Partidas_Detalle.Columns[10].Visible = false;
                                }

                                if (Btn_Nuevo.ToolTip == "Guardar" || Btn_Modificar.ToolTip == "Actualizar")
                                {
                                    Grid_Partidas_Detalle.Columns[0].Visible = false;
                                }
                                else
                                {
                                    Grid_Partidas_Detalle.Columns[25].Visible = false;
                                }
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Noviembre/2011 
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 22/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego un Subnivel Presupuestal
            ///*********************************************************************************************************
            protected void Grid_Partidas_Asignadas_Detalle_SelectedIndexChanged(object sender, EventArgs e)
            {
                Double Ene = 0.00;
                Double Feb = 0.00;
                Double Mar = 0.00;
                Double Abr = 0.00;
                Double May = 0.00;
                Double Jun = 0.00;
                Double Jul = 0.00;
                Double Ago = 0.00;
                Double Sep = 0.00;
                Double Oct = 0.00;
                Double Nov = 0.00;
                Double Dic = 0.00;
                Double Precio = 0.00;
                DataTable Dt_Partidas_Asignadas = new DataTable();
                Int32 No_Fila = -1;
                String Id = String.Empty;
                try
                {
                    Limpiar_Formulario("Datos_Producto");

                    Id = ((GridView)sender).SelectedRow.Cells[26].Text;
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    if (Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Partidas_Asignadas.Rows)
                            {
                                No_Fila++;
                                if (Dr["ID"].ToString().Trim().Equals(Id))
                                {
                                    if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                                    {
                                        Llenar_Combo_Unidad_Responsable();
                                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                        Llenar_Combo_Programas();
                                        Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                                    }
                                    Llenar_Combo_Capitulos();
                                    LLenar_Presupuesto_Dependencia();
                                    Cmb_Capitulos.SelectedIndex = Cmb_Capitulos.Items.IndexOf(Cmb_Capitulos.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["CAPITULO_ID"].ToString().Trim()));
                                    Llenar_Combo_Partidas();
                                    Cmb_Partida_Especifica.SelectedIndex = Cmb_Partida_Especifica.Items.IndexOf(Cmb_Partida_Especifica.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PARTIDA_ID"].ToString().Trim()));
                                    Llenar_Combo_Productos();
                                    Consultar_Combo_Stock();
                                    Txt_Justificacion.Text = Dt_Partidas_Asignadas.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();
                                    if (!string.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()))
                                    {
                                        Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PRODUCTO_ID"].ToString().Trim()));
                                        Precio = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["PRECIO"].ToString().Trim());

                                        if (Precio > 0)
                                        {
                                            Ene = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"].ToString().Trim());
                                            Feb = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"].ToString().Trim());
                                            Mar = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"].ToString().Trim());
                                            Abr = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"].ToString().Trim());
                                            May = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"].ToString().Trim());
                                            Jun = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"].ToString().Trim());
                                            Jul = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"].ToString().Trim());
                                            Ago = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"].ToString().Trim());
                                            Sep = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"].ToString().Trim());
                                            Oct = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"].ToString().Trim());
                                            Nov = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"].ToString().Trim());
                                            Dic = Convert.ToDouble(String.IsNullOrEmpty(Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim()) ? "0" : Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"].ToString().Trim());

                                            Lbl_Cantidad.Text = "Cantidad";
                                            Lbl_Txt_Enero.Text = Convert.ToString(Ene / Precio);
                                            Lbl_Txt_Febrero.Text = Convert.ToString(Feb / Precio);
                                            Lbl_Txt_Marzo.Text = Convert.ToString(Mar / Precio);
                                            Lbl_Txt_Abril.Text = Convert.ToString(Abr / Precio);
                                            Lbl_Txt_Mayo.Text = Convert.ToString(May / Precio);
                                            Lbl_Txt_Junio.Text = Convert.ToString(Jun / Precio);
                                            Lbl_Txt_Julio.Text = Convert.ToString(Jul / Precio);
                                            Lbl_Txt_Agosto.Text = Convert.ToString(Ago / Precio);
                                            Lbl_Txt_Septiembre.Text = Convert.ToString(Sep / Precio);
                                            Lbl_Txt_Octubre.Text = Convert.ToString(Oct / Precio);
                                            Lbl_Txt_Noviembre.Text = Convert.ToString(Nov / Precio);
                                            Lbl_Txt_Diciembre.Text = Convert.ToString(Dic / Precio);
                                        }
                                    }
                                   
                                    Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["DEPENDENCIA_ID"].ToString().Trim()));
                                    Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Partidas_Asignadas.Rows[No_Fila]["PROYECTO_ID"].ToString().Trim()));
                                    
                                    Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ENERO"]);
                                    Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["FEBRERO"]);
                                    Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MARZO"]);
                                    Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["ABRIL"]);
                                    Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["MAYO"]);
                                    Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JUNIO"]);
                                    Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["JULIO"]);
                                    Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["AGOSTO"]);
                                    Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["SEPTIEMBRE"]);
                                    Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["OCTUBRE"]);
                                    Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["NOVIEMBRE"]);
                                    Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["DICIEMBRE"]);
                                    Txt_Total.Text = String.Format("{0:#,###,##0.00}", Dt_Partidas_Asignadas.Rows[No_Fila]["IMPORTE_TOTAL"]);
                                    break;
                                }
                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al seleccionar el registro el grid. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
            ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Partidas_Asignadas_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
                }
            }
        #endregion

        #region EVENTOS COMBOS
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Unidad_Responsable_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de unidad responsable
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                    Mostrar_Ocultar_Error(false);
                    if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                    {
                        Limpiar_Sessiones();
                        Llenar_Combo_Capitulos();
                        Cmb_Capitulos.Enabled = true;
                        Cmb_Partida_Especifica.SelectedIndex = -1;
                        Cmb_Partida_Especifica.Enabled = false;
                        Cmb_Producto.SelectedIndex = -1;
                        Tr_Productos.Visible = false;
                        Limpiar_Formulario("Datos_Partida");
                        Llenar_Combo_Programas();
                        Cmb_Programa.Enabled = true;
                    }
                    else 
                    {
                        Llenar_Combo_Programas();
                        Cmb_Programa.Enabled = true;
                        Cmb_Capitulos.Enabled = true;
                        Cmb_Partida_Especifica.Enabled = true;
                        Cmb_Producto.SelectedIndex = -1;
                        Tr_Productos.Visible = false;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento del combo de unidad responsable Error[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de Capitulos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 10/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Cmb_Capitulo_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                     Mostrar_Ocultar_Error(false);
                     Llenar_Combo_Partidas();
                     Cmb_Partida_Especifica.Enabled = true;
                     Cmb_Producto.SelectedIndex = -1;
                     Tr_Productos.Visible = false;
                     Limpiar_Formulario("Datos_Partida");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Partidas_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de partidas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 11/Noviembre/2011 
            ///MODIFICO             : Jennyfer Ivonne Ceja Lemus
            ///FECHA_MODIFICO       : 21/Agosto/2012
            ///CAUSA_MODIFICACIÓN...: Se agrego un Subnivel Presupuestal
            ///******************************************************************************* 
            protected void Cmb_Partidas_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                    Mostrar_Ocultar_Error(false);
                    Llenar_Combo_Productos();
                    Consultar_Combo_Stock();
                    Cmb_Producto.Enabled = true;
                    Limpiar_Formulario("Datos_Partida");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento del combo de partidas Error[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Productos_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de productos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///******************************************************************************* 
            protected void Cmb_Productos_SelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Productos = new DataTable();
                try
                {
                    Mostrar_Ocultar_Error(false);
                    Limpiar_Formulario("Datos_Producto");
                    Grid_Partida_Asignada.SelectedIndex = -1;
                    Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedItem.Value.Trim();
                    if (Cmb_Producto.SelectedIndex > 0)
                    {
                        Negocio.P_Producto_ID = Cmb_Producto.SelectedItem.Value.Trim();
                        Dt_Productos = Negocio.Consultar_Productos();

                        if (Dt_Productos != null)
                        {
                            if (Dt_Productos.Rows.Count > 0)
                            {
                                Hf_Precio.Value = Dt_Productos.Rows[0]["COSTO"].ToString().Trim();
                                Hf_Producto_ID.Value = Dt_Productos.Rows[0]["PRODUCTO_ID"].ToString().Trim();
                            }
                        }
                    }
                    else 
                    {
                        Hf_Producto_ID.Value = String.Empty;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento del combo de productos Error[" + ex.Message + "]");
                }
            }
        #endregion

    #endregion

    #region MODAL POPUP
            //*******************************************************************************
            //NOMBRE DE LA FUNCIÓN : Grid_Productos_SelectedIndexChanged
            //DESCRIPCIÓN          : Evento de seleccion de un registro del del grid
            //PARAMETROS           :   
            //CREO                 : Leslie González Vázquez
            //FECHA_CREO           : 15/Noviembre/2011 
            //MODIFICO             :
            //FECHA_MODIFICO       :
            //CAUSA_MODIFICACIÓN   :
            //*******************************************************************************
            protected void Grid_Productos_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                    Mostrar_Ocultar_Error(false);
                    Hf_Producto_ID.Value = HttpUtility.HtmlDecode(Grid_Productos.SelectedRow.Cells[1].Text).Trim();
                    Hf_Precio.Value = HttpUtility.HtmlDecode(Grid_Productos.SelectedRow.Cells[4].Text).Trim();
                    Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(Hf_Producto_ID.Value.Trim()));
                    Mpe_Busqueda_Productos.Hide();
                    Lbl_Error_Busqueda.Text = "";
                    Lbl_Error_Busqueda.Style.Add("display", "none");
                    Img_Error_Busqueda.Style.Add("display", "none");
                    Txt_Busqueda_Nombre_Producto.Text = "";
                    Txt_Busqueda_Clave.Text = "";
                    Lbl_Numero_Registros.Text = "";
                    Grid_Productos.DataBind();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error en el evento de seleccionar un registro de la tabla de productos Error[" + ex.Message + "]");
                }
            }

            //*******************************************************************************
            //NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Productos_Click
            //DESCRIPCIÓN          : Evento del boton de busquedas de productos
            //PARAMETROS           :   
            //CREO                 : Leslie González Vázquez
            //FECHA_CREO           : 15/Noviembre/2011 
            //MODIFICO             :
            //FECHA_MODIFICO       :
            //CAUSA_MODIFICACIÓN   :
            //*******************************************************************************
            protected void Btn_Busqueda_Productos_Click(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Productos; //Variable que obtendra los datos de la consulta 
                try
                {
                    
                    if (Cmb_Partida_Especifica.SelectedIndex > 0)
                    {
                        if (!String.IsNullOrEmpty(Txt_Busqueda_Clave.Text.Trim()))
                        {
                            Negocio.P_Clave_Producto = Txt_Busqueda_Clave.Text.Trim();
                        }
                        if (!String.IsNullOrEmpty(Txt_Busqueda_Nombre_Producto.Text.Trim()))
                        {
                            Negocio.P_Nombre_Producto = Txt_Busqueda_Nombre_Producto.Text.Trim();
                        }

                        Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.Trim();
                        Dt_Productos = Negocio.Consultar_Productos();
                        Llenar_Grid_Productos(Dt_Productos);
                        Mpe_Busqueda_Productos.Show();
                        Lbl_Error_Busqueda.Style.Add("display", "none");
                        Img_Error_Busqueda.Style.Add("display", "none");

                        if (Dt_Productos is DataTable)
                            Lbl_Numero_Registros.Text = "Registros Encontrados: [" + Dt_Productos.Rows.Count + "]";
                        else
                            Lbl_Numero_Registros.Text = "Registros Encontrados: [0]";
                    }
                    else 
                    {
                        Lbl_Error_Busqueda.Text = "Favor de seleccionar una partida";
                        Lbl_Error_Busqueda.Style.Add("display", "block");
                        Img_Error_Busqueda.Style.Add("display", "block");
                    }
                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Productos " + ex.Message.ToString(), ex);
                }
            }

            //*******************************************************************************
            //NOMBRE DE LA FUNCIÓN : Llenar_Grid_Productos
            //DESCRIPCIÓN          : Metodo para llenar el grid del modal popup de productos
            //PARAMETROS           1 Dt_Productos datatable que contendra los productos que mostraremos
            //CREO                 : Leslie González Vázquez
            //FECHA_CREO           : 15/Noviembre/2011 
            //MODIFICO             :
            //FECHA_MODIFICO       :
            //CAUSA_MODIFICACIÓN   :
            //*******************************************************************************
            protected void Llenar_Grid_Productos(DataTable Dt_Productos)
            {
                try
                {
                    Grid_Productos.DataBind();
                    if(Dt_Productos != null)
                    {
                        if (Dt_Productos.Rows.Count > 0) 
                        {
                            Grid_Productos.Columns[1].Visible = true;
                            Grid_Productos.Columns[4].Visible = true;
                            Grid_Productos.DataSource = Dt_Productos;
                            Grid_Productos.DataBind();
                            Grid_Productos.Columns[1].Visible = false;
                            Grid_Productos.Columns[4].Visible = false;
                        }
                    }

                }
                catch(Exception ex) 
                {
                    throw new Exception("Error al llenar el grid de productos Error[" + ex.Message + "]");
                }
            }

    #endregion

    #region CARGA MASIVA
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Carga_Click
            /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 22/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Cerrar_Ventana_Carga_Click(object sender, ImageClickEventArgs e)
            {
                //Mpe_Carga_Masiva.Hide();
                Div_Carga_Masiva.Style.Add("display", "none");
                Btn_Copiar.Enabled = true;
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Carga_Masiva_OnClick
            /// DESCRIPCION : Cierra la ventana de busqueda de Polizas.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 22/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Carga_Masiva_OnClick(object sender, ImageClickEventArgs e)
            {
                Div_Carga_Masiva.Style.Add("display", "block");
                Estado_Botones("carga");
            }

            protected void Btn_Carga_Masiva_Popup_Click(object sender, EventArgs e)
            {
                Lbl_Error.Text = String.Empty;
                Lbl_Encanezado_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(false);

                try
                {

                    if (!String.IsNullOrEmpty(HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim()))
                    {
                        Interpretar_Excel();    //Interpreta el archivo de Excel cargado.
                    }
                    else
                    {

                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ingrese el nombre del objeto que se cargara. <br />";
                        Mostrar_Ocultar_Error(true);
                    }
                }
                catch (Exception Ex)
                {
                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - " + Ex.Message.ToString() +" <br />";
                    Mostrar_Ocultar_Error(true);
                }
            }

            protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
            {
                //****************************************
                //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
                //GUARDA EN LA UBICACION PRESTABLECIDA
                //****************************************
                try
                {
                    AsyncFileUpload Archivo;
                    String Nombre_Archivo = String.Empty;
                    String Extension = String.Empty;
                    String[] Archivos;
                    Int32 No = 0;
                    String Ruta = String.Empty;
                    String Ruta_Destino = String.Empty;
                    HttpContext.Current.Session["Nombre_Archivo_Excel"] = String.Empty;

                    if (AFU_Archivo_Excel.HasFile)
                    {
                        //obtenemos los datos del archivo
                        Archivo = (AsyncFileUpload)sender;
                        Nombre_Archivo = Archivo.FileName;

                        Archivos = Nombre_Archivo.Split('\\');
                        No = Archivos.Length;
                        Ruta = Archivos[No - 1];
                        Extension = Ruta.Substring(Ruta.LastIndexOf(".") + 1);

                        if (Extension == "xls" || Extension == "xlsx")
                        {
                            if (Archivo.FileContent.Length > 2621440)
                            {
                                Lbl_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Error.Text = "El tamaño del archivo sobrepasa";
                                return;
                            }

                            Ruta_Destino = Server.MapPath("../../Archivos/Presupuestos/Presupuesto_" + String.Format("{0:yyyy}", DateTime.Now) + "/");

                            //validamos q exista la carpeta si no la creamos
                            if (!System.IO.Directory.Exists(Ruta_Destino))
                            {
                                System.IO.Directory.CreateDirectory(Ruta_Destino);
                            }

                            if (System.IO.File.Exists(Ruta_Destino + Nombre_Archivo))
                            {
                                System.IO.File.Delete(Ruta_Destino + Nombre_Archivo);
                            }
                            
                            AFU_Archivo_Excel.SaveAs(Ruta_Destino + Nombre_Archivo);
                            HttpContext.Current.Session["Nombre_Archivo_Excel"] = Ruta_Destino + Nombre_Archivo;
                        }
                        else
                        {
                            Lbl_Error.Text = "Solo se pueden cargar archivos de Excel con extensión xls o xslx";
                            Mostrar_Ocultar_Error(true);
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - " + ex.Message.ToString() + " <br />";
                    Mostrar_Ocultar_Error(true);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Convertir_Datos_Poliza
            /// DESCRIPCION : Busca los IDs correspondientes para formar el codigo programatico
            /// PARAMETROS  : Dt_Poliza_Detalles: Almacena los datos capturados desde el Excel.
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 10/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Convertir_Datos_Calendarizacion(DataTable Dt_Calendarizacion_Detalles)
            {
                Lbl_Error.Text = String.Empty;
                Lbl_Encanezado_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(false);
                DataTable Dt_Partidas_Calendarizacion = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
                DataTable Dt_Partida_ID = new DataTable();
                DataTable Dt_Consulta_ID = new DataTable();
 
                try
                {
                    DataTable Dt_Partidas_Asignadas = new DataTable();
                    Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                    Mostrar_Ocultar_Error(false);
                    //try
                    //{
                        if(Dt_Calendarizacion_Detalles.Rows.Count > 0)
                        {
                            if (Dt_Partidas_Asignadas == null)
                            {
                                Crear_Dt_Partidas_Asignadas();
                                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                            }
                            if (Agregar_Filas_Dt_Partidas_Asignadas_Carga_Masiva(Dt_Calendarizacion_Detalles))
                            {
                                Limpiar_Formulario("Datos_Partida");
                                Agrupar_Partidas_Capitulos();
                                Llenar_Grid_Partida_Asignada();
                                Calcular_Total_Presupuestado();
                            }
                            else
                            {
                                Mostrar_Ocultar_Error(true); 
                            }
                            
                        }
                        else{
                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El arcivo de excel no contiene informacion <br />";
                            Mostrar_Ocultar_Error(true); 
                        }
                    //}
                    //catch (Exception ex)
                    //{
                    //    throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
                    //}

                    
                }
                catch (Exception ex)
                {
                    throw new Exception("Convertir_Datos_Poliza " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Interpretar_Excel
            /// DESCRIPCION : Interpreta los datos contenidos por el archivo de Excel.
            /// PARAMETROS  : 
            /// CREO        : Salvador L. Rea Ayala
            /// FECHA_CREO  : 13/Octubre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Interpretar_Excel()
            {
                OleDbConnection Conexion = new OleDbConnection();
                OleDbCommand Comando = new OleDbCommand();
                OleDbDataAdapter Adaptador = new OleDbDataAdapter();
                DataSet Ds_Informacion = new DataSet();
                String Query = String.Empty;
                DataSet Ds_Consulta = new DataSet();//Definimos el DataSet donde insertaremos los datos que leemos del excel
                String Tabla = "";
                String Rta = "";
                DataTable Dt_Calendario_Detalles = null;    //Almacena los detalles de la poliza
                String Nombre_Archivo = String.Empty; //variable para el nombre del archivo
                String Extension = String.Empty; //variable para la extension del archivo
                string Conexion_Excel = "";

                try
                {
                    //Colocar el nombre del archivo
                    Rta = HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim();
                    Extension = Rta.Substring(Rta.LastIndexOf(".") + 1);

                    if (Extension == "xlsx")       // Formar la cadena de conexion si el archivo es Exceml xml
                    {
                        Conexion_Excel = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                                "Data Source=" + Rta + ";" +
                                "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";//Almacena si el archivo es 2007
                    }
                    else if (Extension == "xls")   // Formar la cadena de conexion si el archivo es Exceml binario
                    {
                        Conexion_Excel = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                                    "Data Source=" + Rta + ";" +
                                    "Extended Properties=\"Excel 8.0;HDR=YES\""; //Almacena si el archivo es 2003
                    }

                        // Get the name of the first worksheet:
                    Conexion = new OleDbConnection(Conexion_Excel);
                    Conexion.Open();

                    DataTable dbSchema = Conexion.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
                        if (dbSchema == null || dbSchema.Rows.Count < 1)
                        {
                            throw new Exception("Error: Could not determine the name of the first worksheet.");
                        }
                        String FirstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString(); //Obtiene el nombre de la primera Hoja

                    Tabla = "SELECT * FROM [Presupuesto$]";
                    OleDbCommand cmd;   // Almacenara el comando a ejecutar.
                    cmd = new OleDbCommand(Tabla, Conexion);    // Crea el nuevo adaptador Ole
                    Adaptador.SelectCommand = cmd;
                    Adaptador.Fill(Ds_Informacion); //Obtiene la informacion en un dataset
                    Conexion.Close();

                    Dt_Calendario_Detalles = Ds_Informacion.Tables[0];    // Liga los datos con el DataTable

                    if (Validar_Columnas_Excel(Dt_Calendario_Detalles))
                   {
                       Convertir_Datos_Calendarizacion(Dt_Calendario_Detalles); //Convierte los datos extraidos para que sean visualizados de manera correcta.
                   }
                    else
                    {
                        Mostrar_Ocultar_Error(true);
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error  Btn_Cargar_Click: " + ex.Message.ToString(), ex);
                }
                finally
                {
                   // Eliminar_Doc_Excel();
                }
            }

    ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Partidas_Asignadas_Carga
            ///DESCRIPCIÓN          : Metodo para agregar los datos de las partidas asignadas al datatable
            ///                       de la carga masiva
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja lemus
            ///FECHA_CREO           : 05/Septiembre/2012 12:34p pm
            ///MODIFICO             : 
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...: 
            ///*********************************************************************************************************
            private Boolean Agregar_Filas_Dt_Partidas_Asignadas_Carga_Masiva(DataTable Dt_Calendarizacion_Detalles)
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                Cls_Ope_Psp_Limite_Presupuestal_Negocio Limite_Negocio = new Cls_Ope_Psp_Limite_Presupuestal_Negocio();
                DataTable Dt_Partidas_Asignadas = new DataTable();
                DataTable Dt_Usos_Multiples = new DataTable();
                DataTable Dt_UR = new DataTable();
                

                DataRow Fila;
                Lbl_Error.Text = String.Empty;
                Boolean Validacion = true;

                Session.Remove("Dt_Partidas_Asignadas");
                Crear_Dt_Partidas_Asignadas();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];

                try
                {
                    if (Dt_Calendarizacion_Detalles is DataTable)
                    {
                        if (Dt_Calendarizacion_Detalles.Rows.Count > 0)
                        {
                            Int32 Contador = 0; 
                            foreach (DataRow Registro in Dt_Calendarizacion_Detalles.Rows)
                            {
                                if (Validacion == true)
                                {
                                    Contador++;
                                    Fila = Dt_Partidas_Asignadas.NewRow(); //Crea un nuevo registro a la tabla
                                    //Validar la unidad responsable
                                    if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                                    {
                                        if (!String.IsNullOrEmpty(Registro["UNIDAD_RESPONSABLE"].ToString()))
                                        {
                                            if (Hf_Tipo_Calendario.Value.Equals("Dependencia"))
                                            {
                                                 Negocio.P_Dependencia_Clave = Registro["UNIDAD_RESPONSABLE"].ToString();
                                                Dt_Usos_Multiples = Negocio.Consultar_Unidad_Responsable_Carga_Masiva();
                                            }
                                            else
                                            {
                                                Limite_Negocio.P_Accion = String.Empty;
                                                Limite_Negocio.P_Dependencia_Clave = Registro["UNIDAD_RESPONSABLE"].ToString();
                                                Dt_Usos_Multiples = Limite_Negocio.Consultar_Unidades_Responsables_Sin_Asignar();
                                            }

                                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                            {
                                                Fila["DEPENDENCIA_ID"] = Dt_Usos_Multiples.Rows[0]["DEPENDENCIA_ID"];
                                            }
                                            else
                                            {
                                                Validacion = false;
                                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Verificar el arhivo, en el registro " + Contador + "  la unidad responsable no es la que se va a presupuestar<br>";
                                            }
                                        }
                                        else 
                                        {
                                            Validacion = false;
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Verificar el arhivo, el registro " + Contador + " no contiene unidad resposnasble <br>";
                                        }
                                    }
                                    //Validar fuente de financiamiento
                                    if (!String.IsNullOrEmpty(Hf_Fte_Financiamiento.Value))
                                    {
                                        if (!String.IsNullOrEmpty(Registro["FUENTE_FINANCIAMIENTO"].ToString()))
                                        {
                                            Negocio.P_Fuente_Financiamiento_Clave = Registro["FUENTE_FINANCIAMIENTO"].ToString();
                                            Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value;
                                            Dt_Usos_Multiples = Negocio.Consultar_Fuente_Financiamiento_Carga_Masiva();
                                            if (Dt_Usos_Multiples == null || Dt_Usos_Multiples.Rows.Count <= 0)
                                            {
                                                Validacion = false;
                                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, la fuente de fianciamiento del registro " + Contador + " no corresponde a la fuente de financiamiento de la unidad responsable <br>";
                                            }
                                        }
                                        else
                                        {
                                            Validacion = false;
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el registro" + Contador + " no contiene  Fuente de financiamiento <br>"; 
                                        }
                                    }
                                    //validar el  programa
                                    if (!String.IsNullOrEmpty(Registro["PROGRAMA"].ToString()))
                                    {
                                        if (Hf_Tipo_Calendario.Value.Trim().Equals("Empleado"))
                                        {
                                            Limite_Negocio.P_Dependencia_Clave = Registro["UNIDAD_RESPONSABLE"].ToString();
                                            Limite_Negocio.P_Accion = String.Empty;
                                            Dt_UR = Limite_Negocio.Consultar_Unidades_Responsables_Sin_Asignar();
                                            if (Dt_UR != null)
                                            {
                                                if (Dt_UR.Rows.Count > 0)
                                                {
                                                    Limite_Negocio.P_Dependencia_ID = Dt_UR.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim();
                                                }
                                            }

                                        }
                                        else 
                                        {
                                            Limite_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue;
                                        }

                                        Limite_Negocio.P_Programa_Clave = Registro["PROGRAMA"].ToString();
                                        Dt_Usos_Multiples = Limite_Negocio.Consultar_Programa_Unidades_Responsables();
                                        if (Dt_Usos_Multiples.Rows.Count > 0)
                                        {
                                            Fila["PROYECTO_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id];
                                        }
                                        else
                                        {
                                            Validacion = false;
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el  programa del registro " + Contador + " no esta asociado a esa unidad responsable <br>";
                                        }
                                    }
                                    else
                                    {
                                        Validacion = false;
                                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el registro " + Contador + " no contiene  programa <br>";
                                    }

                                    //validar el Capitulo y  la partida
                                    if (!String.IsNullOrEmpty(Registro["PARTIDA"].ToString()))
                                    {
                                        Negocio.P_Partida_Clave = Registro["PARTIDA"].ToString();
                                        Dt_Usos_Multiples = Negocio.Consultar_Partidas_Especificas_Capitulo_Carga_Masiva();
                                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                        {
                                            Fila["CAPITULO_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Capitulos.Campo_Capitulo_ID].ToString();
                                            Fila["PARTIDA_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString();
                                        }
                                        else
                                        {
                                            Validacion = false;
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, la partida del registro " + Contador + " puede no estar registrada <br>";
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el capitulo al que pertence la partida del registro " + Contador + " puede no estar asignado para este presupuesto <br>";
                                        }
                                    }
                                    else
                                    {

                                        Validacion = false;
                                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el registro " + Contador + " no contiene partida <br>";
                                    }
                                    //Validar el subnivel
                                    //if (!String.IsNullOrEmpty(Registro["SUBNIVEL"].ToString())) // CAMBIO- verificamos si existen subniveles y  si selecciono  un subnivel
                                    //{
                                    //    Negocio.P_Dependencia_ID = Fila["DEPENDENCIA_ID"].ToString();
                                    //    Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value;
                                    //    Negocio.P_Partida_ID = Fila["PARTIDA_ID"].ToString();
                                    //    Negocio.P_Programa_ID = Fila["PROYECTO_ID"].ToString();
                                    //    Negocio.P_Subnivel_Clave = Registro["SUBNIVEL"].ToString();
                                    //    Dt_Usos_Multiples = Negocio.Consultar_Subniveles_Presupuestales();
                                    //    if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                    //        Fila["SUBNIVEL_ID"] = Dt_Usos_Multiples.Rows[0][Cat_Psp_SubNivel_Presupuestos.Campo_Subnivel_Presupuestal_ID].ToString();
                                    //    else
                                    //    {
                                    //        Validacion = false;
                                    //        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el subnivel presupuestal del registro " + Contador + " es incorrecto <br>";
                                    //    }
                                    //}
                                    //else
                                    //{
                                    //    Negocio.P_Dependencia_ID = Fila["DEPENDENCIA_ID"].ToString();
                                    //    Negocio.P_Fuente_Financiamiento_ID = Hf_Fte_Financiamiento.Value;
                                    //    Negocio.P_Partida_ID = Fila["PARTIDA_ID"].ToString();
                                    //    Negocio.P_Programa_ID = Fila["PROYECTO_ID"].ToString();
                                    //    Dt_Usos_Multiples = Negocio.Consultar_Subniveles_Presupuestales();
                                    //    if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                    //    {
                                    //        Validacion = false;
                                    //        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el subnivel presupuestal del registro " + Contador + " no puede estar vacio <br>";
                                    //    }
                                    //    else
                                    //    {
                                    //        Fila["SUBNIVEL_ID"] = 0;
                                    //    }
                                    //}
                                    //Validar el producto
                                    if (!String.IsNullOrEmpty(Registro["PRODUCTO_CLAVE"].ToString())) //verificamos si se selecciono un producto
                                    {

                                        Negocio.P_Producto_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["PRODUCTO_CLAVE"].ToString()));
                                        Negocio.P_Partida_ID = Fila["PARTIDA_ID"].ToString();
                                        Dt_Usos_Multiples = Negocio.Consultar_Productos();
                                        if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                        {
                                            Fila["PRODUCTO_ID"] = String.Format("{0:0000000000}", Convert.ToInt32(Registro["PRODUCTO_CLAVE"].ToString()));
                                            Fila["PRECIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dt_Usos_Multiples.Rows[0][Cat_Com_Productos.Campo_Costo].ToString()) ? "0" : Dt_Usos_Multiples.Rows[0][Cat_Com_Productos.Campo_Costo].ToString());
                                        }
                                        else 
                                        {
                                            Validacion = false;
                                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo, el producto del registro " + Contador + " puede pertenecer a otra partida o puede estar inactivo <br>";
                                        }
                                    }
                                    else
                                    {
                                        Fila["PRODUCTO_ID"] = String.Empty;
                                        Fila["PRECIO"] = 0;
                                    }
                                    //Validar Justificacion
                                    if (!String.IsNullOrEmpty(Registro["JUSTIFICACION"].ToString())) //verificamos si se selecciono un producto
                                    {
                                        Fila["JUSTIFICACION"] = Registro["JUSTIFICACION"].ToString();
                                    }
                                    else
                                    {
                                        Validacion = false;
                                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo el registro " + Contador + " No contiene una justificación <br>";
                                    }

                                    if (!String.IsNullOrEmpty(Registro["PRODUCTO_CLAVE"].ToString())) //si existe un producto ponermos su clave y si no la clave de la partida
                                    {
                                        Fila["CLAVE_PARTIDA"] = Registro["PARTIDA"].ToString() + " " + Registro["DESCRIPCION_PARTIDA"].ToString();
                                        Fila["CLAVE_PRODUCTO"] = Registro["PRODUCTO_CLAVE"].ToString() + " " + Registro["PRODUCTO_DESCRIPCION"].ToString();
                                    }
                                    else
                                    {
                                        Fila["CLAVE_PARTIDA"] = Registro["PARTIDA"].ToString() + " " + Registro["DESCRIPCION_PARTIDA"].ToString();
                                        Fila["CLAVE_PRODUCTO"] = String.Empty;
                                    }
                                    if (Validacion == true)
                                    {
                                        Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Fila["DEPENDENCIA_ID"].ToString()));
                                        Fila["UR"] = Cmb_Unidad_Responsable.SelectedItem.Text.Substring(Cmb_Unidad_Responsable.SelectedItem.Text.IndexOf(" ")).Trim();

                                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["ENERO"].ToString()) ? "0" : Registro["ENERO"].ToString()));
                                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["FEBRERO"].ToString()) ? "0" : Registro["FEBRERO"].ToString()));
                                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["MARZO"].ToString()) ? "0" : Registro["MARZO"].ToString()));
                                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["ABRIL"].ToString()) ? "0" : Registro["ABRIL"].ToString()));
                                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["MAYO"].ToString()) ? "0" : Registro["MAYO"].ToString()));
                                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["JUNIO"].ToString()) ? "0" : Registro["JUNIO"].ToString()));
                                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["JULIO"].ToString()) ? "0" : Registro["JULIO"].ToString()));
                                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["AGOSTO"].ToString()) ? "0" : Registro["AGOSTO"].ToString()));
                                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["SEPTIEMBRE"].ToString()) ? "0" : Registro["SEPTIEMBRE"].ToString()));
                                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["OCTUBRE"].ToString()) ? "0" : Registro["OCTUBRE"].ToString()));
                                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["NOVIEMBRE"].ToString()) ? "0" : Registro["NOVIEMBRE"].ToString()));
                                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Registro["DICIEMBRE"].ToString()) ? "0" : Registro["DICIEMBRE"].ToString()));
                                        String Total = Txt_Total.Text.Replace("$", "");

                                        //obtenemos el total
                                        Double Imp_Total = 0.00;
                                        Imp_Total = Convert.ToDouble(String.IsNullOrEmpty(Registro["ENERO"].ToString()) ? "0" : Registro["ENERO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["FEBRERO"].ToString()) ? "0" : Registro["FEBRERO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["MARZO"].ToString()) ? "0" : Registro["MARZO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["ABRIL"].ToString()) ? "0" : Registro["ABRIL"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["MAYO"].ToString()) ? "0" : Registro["MAYO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["JUNIO"].ToString()) ? "0" : Registro["JUNIO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["JULIO"].ToString()) ? "0" : Registro["JULIO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["AGOSTO"].ToString()) ? "0" : Registro["AGOSTO"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["SEPTIEMBRE"].ToString()) ? "0" : Registro["SEPTIEMBRE"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["OCTUBRE"].ToString()) ? "0" : Registro["OCTUBRE"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["NOVIEMBRE"].ToString()) ? "0" : Registro["NOVIEMBRE"].ToString());
                                        Imp_Total += Convert.ToDouble(String.IsNullOrEmpty(Registro["DICIEMBRE"].ToString()) ? "0" : Registro["DICIEMBRE"].ToString());

                                        Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Imp_Total);
                                        Fila["ID"] = "";

                                        if (Validar_Justificacion_Carga_Masiva(Contador, Fila))//valida que el registro nuevo no se haya registrado antes
                                            Dt_Partidas_Asignadas.Rows.Add(Fila);
                                        else
                                            Validacion = false;
                                    }
                                }//Fin del if Validacion ==  true
                            }//Fin del for each
                                if (Validacion == true)
                                    Session["Dt_Partidas_Asignadas"] = (DataTable)Dt_Partidas_Asignadas;
                        }//Fin del if Detalles.rows.count >0
                        else
                        {
                            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Revisar el archivo de Excel, No contiene informacion <br>";
                            Mostrar_Ocultar_Error(true);
                            
                        } //    else de count
                    }
                   return Validacion;

                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
                }
            }
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Columnas_Excel
            ///DESCRIPCIÓN          : Metodo que 
            ///PROPIEDADES          : Objeto DataTable, Del cual se va a nalizar las columnas que debe de conter el excel
            ///                       Para poder hacer la carga masiva
            ///CREO                 : Jennyfer Ivonne Ceja lemus
            ///FECHA_CREO           : 06/Septiembre/2012 12:34p pm
            ///MODIFICO             : 
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...: 
            ///*********************************************************************************************************
            public Boolean Validar_Columnas_Excel(DataTable Dt_Tabla) 
            {
                Boolean Validacion = true;
                try
                {
                    
                    if (!Dt_Tabla.Columns.Contains("FUENTE_FINANCIAMIENTO") )
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna FUENTE_FINANCIAMIENTO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("AREA_FUNCIONAL"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna AREA_FUNCIONAL. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("UNIDAD_RESPONSABLE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna UNIDAD_RESPONSABLE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("PROGRAMA"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna PROGRAMA. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("PARTIDA"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna PARTIDA. <br />";
                        Validacion = false;
                    }
                    //if (!Dt_Tabla.Columns.Contains("SUBNIVEL"))
                    //{
                    //    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna SUBNIVEL. <br />";
                    //    Validacion = false;
                    //}
                    if (!Dt_Tabla.Columns.Contains("PRODUCTO_CLAVE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna PRODUCTO_CLAVE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("JUSTIFICACION"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna JUSTIFICACION. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("ENERO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna ENERO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("FEBRERO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna FEBRERO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("MARZO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna MARZO . <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("ABRIL"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna ABRIL. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("MAYO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna MAYO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("JUNIO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna JUNIO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("JULIO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna JULIO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("AGOSTO"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna AGOSTO. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("SEPTIEMBRE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna SEPTIEMBRE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("OCTUBRE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna OCTUBRE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("NOVIEMBRE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna NOVIEMBRE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("DICIEMBRE"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna DICIEMBRE. <br />";
                        Validacion = false;
                    }
                    if (!Dt_Tabla.Columns.Contains("TOTAL"))
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna TOTAL. <br />";
                        Validacion = false;
                    }
                    return Validacion;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
                }
            }
            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Justificacion
            ///DESCRIPCIÓN          : Metodo para validar que la justificacion sea diferente
            ///PROPIEDADES          :
            ///CREO                 : Jennyfer Ivonne Ceja Lemus
            ///FECHA_CREO           : 07/Septiembre/2012
            ///MODIFICO             : 
            ///FECHA_MODIFICO       : 
            ///CAUSA_MODIFICACIÓN...: 
            ///*********************************************************************************************************
            private Boolean Validar_Justificacion_Carga_Masiva(Int32 No_Fila, DataRow Fila)
            {
                Boolean Datos_Validos = true;
                Lbl_Error.Text = String.Empty;
                DataTable Dt_Partidas_Asignadas = new DataTable();
                //DataRow Fila;
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                try
                {

                    foreach (DataRow Registro in Dt_Partidas_Asignadas.Rows)
                    {
                        if (Registro["DEPENDENCIA_ID"].ToString().ToUpper().Equals(Fila["DEPENDENCIA_ID"].ToString().ToUpper().Trim()))
                        {
                            if (Registro["PROYECTO_ID"].ToString().ToUpper().Equals(Fila["PROYECTO_ID"].ToString().ToUpper().Trim()))
                            {
                                if (Registro["PARTIDA_ID"].ToString().Equals(Fila["PARTIDA_ID"].ToString().ToUpper().Trim()))
                                {
                                    if (!String.IsNullOrEmpty(Fila["PRODUCTO_ID"].ToString().Trim()))
                                    {
                                        if (Registro["PRODUCTO_ID"].ToString().Equals(Fila["PRODUCTO_ID"].ToString().Trim()))
                                        {
                                            if (Registro["JUSTIFICACION"].ToString().ToUpper().Equals(Fila["JUSTIFICACION"].ToString().ToUpper().Trim()))
                                            {
                                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ya existe un registro con estos datos y con la misma justificación del registro "+ No_Fila+", cambie la justificación. <br />";
                                                Datos_Validos = false;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        //if (!String.IsNullOrEmpty(Fila["SUBNIVEL_ID"].ToString().Trim()))
                                        //{
                                        //    if (Registro["SUBNIVEL_ID"].ToString().Equals(Fila["SUBNIVEL_ID"].ToString().Trim()))
                                        //    {
                                        //        if (Registro["JUSTIFICACION"].ToString().ToUpper().Equals(Fila["JUSTIFICACION"].ToString().ToUpper().Trim()))
                                        //        {
                                        //            Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ya existe un registro con estos datos y con la misma justificación del registro " + No_Fila + ", cambie la justificación. <br />";
                                        //            Datos_Validos = false;

                                        //        }
                                        //    }

                                        //}//fin de if subnivel presupuestal
                                        //else
                                        //{
                                            if (Registro["JUSTIFICACION"].ToString().ToUpper().Equals(Fila["JUSTIFICACION"].ToString().ToUpper().Trim()))
                                            {
                                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Ya existe un registro con estos datos y con la misma justificación del registro " + No_Fila + ", cambie la justificación. <br />";
                                                Datos_Validos = false;
                                            }
                                        //}
                                    }
                                }
                            }
                        }
                    }

                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de validar que la partida nosea igual Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Partidas_Capitulos
            ///DESCRIPCIÓN          : Metodo para agrupar solo las partidas de los capitulos permitidos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Junio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            public void Agrupar_Partidas_Capitulos()
            {
                Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
                DataTable Dt_Capitulos = new DataTable();
                DataTable Dt_Partidas_Asignadas = new DataTable();
                DataTable Dt_Partidas_Temp = new DataTable();
                DataTable Dt_Partidas_Adignadas_Temp = new DataTable();
                Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
                
                Lbl_Error.Text = String.Empty;
                Lbl_Encanezado_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(false); 


                try
                {
                    //obtenemos  los capitulos que se pueden calendarizar
                    if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                    {
                        if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                        {
                            Calendarizar_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
                        }
                    }
                    else
                    {
                        Calendarizar_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    }

                    Dt_Capitulos = Calendarizar_Negocio.Consultar_Capitulos();

                    if (Dt_Capitulos != null && Dt_Partidas_Asignadas != null)
                    {
                        if (Dt_Capitulos.Rows.Count > 0 && Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Capitulos.Rows)
                            {
                                Dt_Partidas_Adignadas_Temp = (from Fila_Psp in Dt_Partidas_Asignadas.AsEnumerable()
                                                              where Fila_Psp.Field<String>("CAPITULO_ID") == Dr["CAPITULO_ID"].ToString().Trim()
                                                         select Fila_Psp).AsDataView().ToTable();

                                if (Dt_Partidas_Adignadas_Temp != null)
                                {
                                    if (Dt_Partidas_Temp != null)
                                    {
                                        if (Dt_Partidas_Temp.Rows.Count <= 0)
                                        {
                                            Dt_Partidas_Temp = Dt_Partidas_Adignadas_Temp.Copy();
                                        }
                                        else
                                        {
                                            foreach (DataRow Dr_Int in Dt_Partidas_Adignadas_Temp.Rows)
                                            {
                                                Dt_Partidas_Temp.ImportRow(Dr_Int);
                                            }
                                        }
                                    }
                                    else {
                                        Dt_Partidas_Temp = Dt_Partidas_Adignadas_Temp.Copy();
                                    }
                   
                                }
                            }

                            if (Dt_Partidas_Temp != null)
                            {
                                Session["Dt_Partidas_Asignadas"] = Dt_Partidas_Temp;
                            }
                        }
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Eliminar_Archivo
            ///DESCRIPCIÓN          : Metodo para  eliminar el documento de excel
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Junio/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            public void Eliminar_Doc_Excel() 
            {
                String Ruta = HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim(); 

                try
                {
                    if (System.IO.File.Exists(Ruta))
                    {
                        System.IO.File.Delete(Ruta);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al eliminar el documento de excel. Error[" + Ex.Message + "]");
                }
            }
    #endregion

    #region (Generar Excel)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Evento del boton de generar el calendarizado en reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
            DataTable Dt_Partidas = new DataTable();
            Mostrar_Ocultar_Error(false);

            try
            {
                if (Hf_Tipo_Calendario.Value.Trim().Equals("Dependencia"))
                {
                    Negocio.P_Empleado_ID = String.Empty;
                    Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                }
                else 
                {
                    Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                    Negocio.P_Dependencia_ID = String.Empty;
                }

               // Negocio.P_Estatus = "'EN CONSTRUCCION','GENERADO','AUTORIZADO', 'CARGADO', 'RECHAZADO'";
                Dt_Partidas = Negocio.Consultar_Partida_Asignadas_Reporte();

                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        Dt_Partidas = Dt_Partidas_Con_Servicio_Mantenimiento(Dt_Partidas);

                        Generar_Rpt_Calendarizado(Dt_Partidas);
                    }
                    else 
                    {
                        Lbl_Encanezado_Error.Text = "No hay registros para exportar a Excel. ";
                        Mostrar_Ocultar_Error(true);
                    }
                }
                else {
                    Lbl_Encanezado_Error.Text = "No hay registros para exportar a Excel. ";
                    Mostrar_Ocultar_Error(true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error[" + Ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Rpt_Calendarizado
        ///DESCRIPCIÓN          : Reporte para generar el calendarizado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 20/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public void Generar_Rpt_Calendarizado(DataTable Dt_Datos)
        {
            String Ruta_Archivo = "Calendarizacion_Egresos_" + Hf_Anio.Value.Trim() + "_" + Cmb_Unidad_Responsable.SelectedItem.Text.Trim().Replace(" ","_") + ".xls";
            try
            {
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                Libro.Properties.Title = "Calendarizacion_Egresos_" + Hf_Anio.Value.Trim();
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";
                Double Importe = 0.00;

                #region(Estilos)
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Presupuesto");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("Estilo_Cantidad");

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "#7DCC7D";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido2.Font.Size = 9;
                Estilo_Contenido2.Font.Bold = false;
                Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido2.Font.Color = "#000000";
                Estilo_Contenido2.Interior.Color = "#FFFFFF";
                Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad.Font.Size = 9;
                Estilo_Cantidad.Font.Bold = false;
                Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad.Font.Color = "#000000";
                Estilo_Cantidad.Interior.Color = "#FFFFFF";
                Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//FF
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES FF
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//AF
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES AF
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PP
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES PP
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//UR
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES UR
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//P
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES P
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PR
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DES PR
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//JUST
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//enero.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//febrero.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//marzo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//abril.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//may.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//jun.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//jul.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ago.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//sep.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//oct.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//nov.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//dic.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//tot.

                //Renglon = Hoja.Table.Rows.Add();
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FUENTE_FINANCIAMIENTO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION_FUENTE_FINANCIAMIENTO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AREA_FUNCIONAL", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION_AREA_FUNCIONAL", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PROGRAMA", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION_PROGRAMA", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("UNIDAD_RESPONSABLE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION_UNIDAD_RESPONSABLE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PARTIDA", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DESCRIPCION_PARTIDA", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRODUCTO_CLAVE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("PRODUCTO_DESCRIPCION", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("JUSTIFICACION", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ENERO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("FEBRERO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MARZO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABRIL", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("MAYO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("JUNIO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("JULIO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("AGOSTO", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SEPTIEMBRE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("OCTUBRE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("NOVIEMBRE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("DICIEMBRE", "BodyStyle"));
                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("TOTAL", "BodyStyle"));


                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                         foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Columna.ColumnName.Trim().Equals("FUENTE_FINANCIAMIENTO") || Columna.ColumnName.Trim().Equals("DESCRIPCION_FUENTE_FINANCIAMIENTO")
                                        || Columna.ColumnName.Trim().Equals("AREA_FUNCIONAL") || Columna.ColumnName.Trim().Equals("DESCRIPCION_UNIDAD_RESPONSABLE")
                                        || Columna.ColumnName.Trim().Equals("DESCRIPCION_AREA_FUNCIONAL") || Columna.ColumnName.Trim().Equals("PROGRAMA")
                                        || Columna.ColumnName.Trim().Equals("DESCRIPCION_PROGRAMA") || Columna.ColumnName.Trim().Equals("PARTIDA")
                                        || Columna.ColumnName.Trim().Equals("UNIDAD_RESPONSABLE") || Columna.ColumnName.Trim().Equals("DESCRIPCION_PARTIDA")
                                        || Columna.ColumnName.Trim().Equals("PRODUCTO_CLAVE") || Columna.ColumnName.Trim().Equals("PRODUCTO_DESCRIPCION")
                                        || Columna.ColumnName.Trim().Equals("JUSTIFICACION"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle2"));
                                    }
                                    else 
                                    {
                                        Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0.00" : Dr[Columna.ColumnName].ToString());

                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Importe), DataType.Number, "Estilo_Cantidad"));
                                    }
                                }
                            }
                        }
                    }
                }

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Dt_Partidas_Con_Servicio_Mantenimiento
        ///DESCRIPCIÓN          : Reporte para generar el calendarizado con la partida de mantenimiento y servicio
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        public DataTable Dt_Partidas_Con_Servicio_Mantenimiento(DataTable Dt_Datos) 
        {
            String Partida = String.Empty;

            try
            {
                if (Dt_Datos != null)
                {
                    foreach (DataRow Dr in Dt_Datos.Rows) 
                    {
                        Partida = Dr["PARTIDA"].ToString().Trim();

                        if (Partida.Trim().Equals("3551"))
                        {
                            if (String.IsNullOrEmpty(Dr["PRODUCTO_CLAVE"].ToString().Trim()))
                            {
                                Dr["PRODUCTO_DESCRIPCION"] = "SERVICIO Y MANTENIMIENTO";
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al obtener las partidas de mantenimiento y servicio. Error[" + Ex.Message + "]");
            }
            return Dt_Datos;
        }
    #endregion
}
