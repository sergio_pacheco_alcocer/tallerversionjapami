﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Presupuesto_Egresos_Ingresos_Mensual_Usuario.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Egresos_Ingresos_Mensual_Usuario" Title="SIAC Sistema Integral Administrativo y Comercial"
 EnableEventValidation="false" ValidateRequest="false" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <meta http-equiv="X-UA-Compatible" content="IE=8.0" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <link href="../../jquery-easyui/themes/gray/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
    <script type="text/javascript" src="../../jquery/jquery-1.7.1.min.js"></script>
    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/validacion/jquery.timers.js" type="text/javascript"></script>
    <script src="../../javascript/jquery.media.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //<--
            //El nombre del controlador que mantiene la sesión
            var CONTROLADOR = "../../Mantenedor_Session.ashx";

            //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
            function MantenSesion() {
                var head = document.getElementsByTagName('head').item(0);
                script = document.createElement('script');
                script.src = CONTROLADOR;
                script.setAttribute('type', 'text/javascript');
                script.defer = true;
                head.appendChild(script);
            }

            //Temporizador para matener la sesión activa
            setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        //-->
        
       
    </script>
    <style type="text/css">
         .button_autorizar2{
            margin:0 7px 0 0;
            background-color:#f5f5f5;
            border:1px solid #dedede;
            border-top:1px solid #eee;
            border-left:1px solid #eee;

            font-family:"Lucida Grande", Tahoma, Arial, Verdana, sans-serif;
            font-size:100%;    
            line-height:130%;
            text-decoration:none;
            font-weight:bold;
            color:#565656;
            cursor:pointer;
            padding:5px 10px 6px 7px; /* Links */
            width:99%;
        }
    </style>
    <script src="../../javascript/Js_Ope_Psp_Presupuesto_Egresos_Ingresos_Mensual_Usuario.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        //Sys.WebForms.PageRequestManager.getInstance().add_endRequest(Refrescar_Pagina);
//        Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(Configuracion_Inicial);
//        Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(Configuracion_Inicial);
        
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"
    AsyncPostBackTimeout = "36000">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <div style="width:100%;" id="Div_Contenido">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Presupuesto Egresos</td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%; display:none" runat="server">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px" Enabled="false"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%">
                            &nbsp;
                            <asp:ImageButton ID="Btn_Generar_Reporte" runat="server" ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png" 
                                OnClick="Btn_Generar_Reporte_Click" ToolTip="Generar Reporte"   CssClass="Img_Button"
                                style="cursor:hand;" /> &nbsp;
                            <asp:ImageButton ID="Btn_Salir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" ToolTip="Salir" />
                        </td>
                        <td style="width:50%">&nbsp;</td>
                    </tr>
                </table>   
                <br />
                <center>
                  <asp:Panel ID="Panel1" runat="server" GroupingText="Filtros" Width="96%" style="text-align:left;">
                    <center>
                        <table width="98%" class="estilo_fuente">
                            <tr>
                                <td colspan="4" style="height:0.3em;">
                                    <asp:HiddenField id="Hf_Tipo_Usuario" runat="server"/>
                                    <asp:HiddenField id="Hf_Dep_ID" runat="server"/>
                                    <asp:HiddenField ID = "Hf_Parametros_Rpt" runat = "server" />
                                </td>
                            </tr>
                            <tr>
                                <td style="cursor:default; text-align:left; width:20%;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Tipo_Psp" runat="server" Text="Tipo Presupuesto" style="font-size:9pt; width:120px;"></asp:Label>
                                </td>
                                <td style="cursor:default; text-align:left; width:30%;" class="button_autorizar2">
                                     <select id="Cmb_Tipo_Psp" class="easyui-combobox" name="Cmb_Tipo_Psp" style="width:220px;"
                                     onClick="$('#Cmb_Tipo_Psp').select();">
                                        <option value="" selected>SELECCIONE</option>
                                        <option value="ING">INGRESOS</option>
                                        <option value="EGR">EGRESOS</option>
                                     </select>
                                </td>
                                <td colspan="2" style=" cursor:default; text-align:center; width:50%;" class="button_autorizar2">
                                    <asp:Button ID="Btn_Generar" runat="server" CssClass="button" Text="Generar" ToolTip="Generar" style="font-size:9pt; cursor:pointer;" />
                                    &nbsp;
                                </td>
                            </tr>
                             <tr>
                                <td style=" cursor:default; width:20%;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Anio" runat="server" Text="* Año" style="font-size:9pt; width:120px;"></asp:Label>
                                </td>
                                <td style="cursor:default; width:30%;font-family:Arial;" class="button_autorizar2">
                                     <select id="Cmb_Anio" class="easyui-combobox" name="Cmb_Anio" style="width:220px;"></select>
                                </td>
                                <td style=" cursor:default; width:20%;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Meses" runat="server" Text="Periodo" style="font-size:9pt; width:120px;"></asp:Label>
                                </td>
                                <td style=" cursor:default; width:50%;" class="button_autorizar2">
                                     <select id="Cmb_Meses" class="easyui-combobox" name="Cmb_Meses" style="width:220px;" >
                                        <option value="" selected>SELECCIONE</option>
                                        <option value="01">ENERO</option>
                                        <option value="02">FEBRERO</option>
                                        <option value="03">MARZO</option>
                                        <option value="04">ABRIL</option>
                                        <option value="05">MAYO</option>
                                        <option value="06">JUNIO</option>
                                        <option value="07">JULIO</option>
                                        <option value="08">AGOSTO</option>
                                        <option value="09">SEPTIEMBRE</option>
                                        <option value="10">OCTUBRE</option>
                                        <option value="11">NOVIEMBRE</option>
                                        <option value="12">DICIEMBRE</option>
                                     </select>
                                </td>
                            </tr>
                            <tr>
                                <td style=" cursor:default; width:20%;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Fecha_Ini" runat="server" Text="Fecha Inicial" style="font-size:9pt; width:120px;"></asp:Label>
                                </td>
                                <td style="  cursor:default; width:30%;" class="button_autorizar2">
                                     <input id="Txt_Fecha_Ini" class="easyui-datebox" style="width:220px;" ></input>
                                </td>
                                <td style="cursor:default; width:20%;" class="button_autorizar2">
                                    <asp:Label ID="Lbl_Fecha_Fin" runat="server" Text="Fecha Final" style="font-size:9pt; width:120px;"></asp:Label>
                                </td>
                                <td style=" cursor:default; width:30%;" class="button_autorizar2">
                                     <input id="Txt_Fecha_Fin" class="easyui-datebox" style="width:220px;"></input>
                                </td>
                            </tr>
                        </table>
                    </center> 
                  </asp:Panel>
                   <%--<div style="width:99%;" id="Div_Instrucciones">
                        Al seleccionar algun campo son para indicar los niveles de las tablas
                   </div>--%>
                  <div style="width:100%; " id="Div_Filtros_Egresos" >
                      <asp:Panel ID="Pnl_Filtros_Egresos" runat="server" GroupingText="Filtros Egresos" Width="96%" style="text-align:left;">
                        <center>
                            <table width="98%">
                                <tr><td colspan="4" style="height:0.3em;"></td></tr>
                                <tr>
                                    <td style=" text-align:left;  cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_FF" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_FF" runat="server" Text="Fte Financiamiento" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td  style=" text-align:left; cursor:default;" class="button_autorizar2"  colspan="3">
                                        <select id="Cmb_FF" class="easyui-combobox" name="Cmb_FF" style="width:580px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_AF" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_AF" runat="server" Text="Area Funcional" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default;" class="button_autorizar2"  colspan="3">
                                        <select id="Cmb_AF" class="easyui-combobox" name="Cmb_AF" style="width:580px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default; width:40%;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_PP" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_PP" runat="server" Text="Programa" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; width:35%;" class="button_autorizar2">
                                        <select id="Cmb_PP" class="easyui-combobox" name="Cmb_PP" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; width:3%; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_PP_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; width:24%;display:none;" class="button_autorizar2">
                                        <select id="Cmb_PP_F" class="easyui-combobox" name="Cmb_PP" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_Ur" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_Ur" runat="server" Text="U. Responsable" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default;" class="button_autorizar2">
                                        <select id="Cmb_UR" class="easyui-combobox" name="Cmb_UR" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_Ur_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_UR_F" class="easyui-combobox" name="Cmb_UR" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_Cap" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_Cap" runat="server" Text="Capitulo" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default;" class="button_autorizar2">
                                        <select id="Cmb_Cap" class="easyui-combobox" name="Cmb_Cap" style="width:580px;"></select>
                                    </td>
                                     <td style="text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_Cap_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_Cap_F" class="easyui-combobox" name="Cmb_Cap" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default; " class="button_autorizar2">
                                        <asp:CheckBox id="Chk_Con" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_Con" class="easyui-combobox" name="Cmb_Con" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_Con_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_Con_F" class="easyui-combobox" name="Cmb_Con" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_PG" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_PG" runat="server" Text="Partida Generica" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_PG" class="easyui-combobox" name="Cmb_PG" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_PG_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_PG_F" class="easyui-combobox" name="Cmb_PG" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_PE" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_P" runat="server" Text="Partida" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_P" class="easyui-combobox" name="Cmb_P" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_P_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2" >
                                        <select id="Cmb_P_F" class="easyui-combobox" name="Cmb_P" style="width:280px;"></select>
                                    </td>
                                </tr>
                            </table>
                        </center>
                      </asp:Panel>
                  </div>
                  <div style="width:100%;" id="Div_Filtros_Ingresos">
                      <asp:Panel ID="Pnl_Filtros_Ingresos" runat="server" GroupingText="Filtros Ingresos" Width="96%" style="text-align:left;">
                        <center>
                            <table width="98%">
                                <tr><td colspan="4" style="height:0.3em;"></td></tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2" >
                                        <asp:CheckBox id="Chk_FF_Ing" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_FF_Ing" runat="server" Text="Fte Financiamiento" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2"  colspan="3">
                                        <select id="Cmb_FF_Ing" class="easyui-combobox" name="Cmb_FF_Ing" style="width:580px;"></select>
                                    </td>
                                </tr>
                                <tr id = "Tr_Ing_Programa" style ="display:none;">
                                    <td style="text-align:left; cursor:default; width:40%;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_PP_Ing" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_PP_Ing" runat="server" Text="Programa" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; width:39%;" class="button_autorizar2">
                                        <select id="Cmb_PP_Ing" class="easyui-combobox" name="Cmb_PP_Ing" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default; width:3%; display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_PP_Ing_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; width:24; display:none;" class="button_autorizar2">
                                        <select id="Cmb_PP_Ing_F" class="easyui-combobox" name="Cmb_PP_Ing" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_R" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_Rubro" runat="server" Text="Rubro" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_R" class="easyui-combobox" name="Cmb_R" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default;display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_R_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_R_F" class="easyui-combobox" name="Cmb_R" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_T" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_T" runat="server" Text="Tipo" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_T" class="easyui-combobox" name="Cmb_T" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default;display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_T_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_T_F" class="easyui-combobox" name="Cmb_T" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_Cl" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_Cl" runat="server" Text="Clase" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_Cl" class="easyui-combobox" name="Cmb_Cl" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default;display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_Cl_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_Cl_F" class="easyui-combobox" name="Cmb_Cl" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_Con_Ing" runat = "server" Checked ="true"/>
                                        <asp:Label ID="Lbl_Con_Ing" runat="server" Text="Concepto" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; " class="button_autorizar2">
                                        <select id="Cmb_Con_Ing" class="easyui-combobox" name="Cmb_Con_Ing" style="width:580px;"></select>
                                    </td>
                                     <td style="text-align:left; cursor:default;display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_Con_Ing_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_Con_Ing_F" class="easyui-combobox" name="Cmb_Con_Ing" style="width:280px;"></select>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align:left; cursor:default;" class="button_autorizar2">
                                        <asp:CheckBox id="Chk_SubCon" runat = "server" Checked ="false"/>
                                        <asp:Label ID="Lbl_SubCon" runat="server" Text="SubConcepto" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default;" class="button_autorizar2">
                                        <select id="Cmb_SubCon" class="easyui-combobox" name="Cmb_SubCon" style="width:580px;"></select>
                                    </td>
                                    <td style="text-align:left; cursor:default;display:none;" class="button_autorizar2">
                                        <asp:Label ID="Lbl_SubCon_F" runat="server" Text="al" style="font-size:8pt;"></asp:Label>
                                    </td>
                                    <td style=" text-align:left; cursor:default; display:none;" class="button_autorizar2">
                                        <select id="Cmb_SubCon_F" class="easyui-combobox" name="Cmb_SubCon" style="width:280px;"></select>
                                    </td>
                                </tr>
                            </table>
                        </center>
                      </asp:Panel>
                    </div>
                    <div style="width:99%; ">
                        <center>
                            <div id="Div_Psp" class="easyui-window">
                                <center>
                                    <table>
                                        <tr>
                                            <td style="text-align:left; width:100%; border-color:Blue;" id="Td_Iconos">
                                                <table>
                                                    <tr>
                                                        <td>
                                                            <asp:ImageButton ID="Btn_Expandir" runat="server" ToolTip="Expandir Todo" CssClass="Img_Button"
                                                            ImageUrl="~/paginas/imagenes/paginas/accordion_expand.png" style="cursor:pointer;" /> &nbsp;
                                                            <asp:ImageButton ID="Btn_Contraer" runat="server" ToolTip="Contraer Todo" CssClass="Img_Button"
                                                            ImageUrl="~/paginas/imagenes/paginas/accordion_collapse.png" style="cursor:pointer;" />&nbsp;
                                                        </td>
                                                        <td id="Td_Botones_Reportes">
                                                            <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" ToolTip="Exportar Excel" CssClass="Img_Button"
                                                            ImageUrl="~/paginas/imagenes/paginas/microsoft_office2003_excel.png" style="cursor:pointer;" />&nbsp;
                                                        </td>
                                                        <td>
                                                            <asp:ImageButton ID="Btn_Actualizar" runat="server" ToolTip="Actualizar" CssClass="Img_Button"
                                                            ImageUrl="~/paginas/imagenes/paginas/actualizar_detalle.png" style="cursor:pointer;width:19.5px; height:19.5px;" />&nbsp;
                                                        </td>
                                                        
                                                    </tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Psp_Ing">
                                                 <table id="Grid_Psp_Ing"></table>
                                                 
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Psp_Egr">
                                                <table id="Grid_Psp_Egr"></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Det_Mov_Ing">
                                                <table id="Grid_Det_Mov_Ing"></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Det_Rec_Ing">
                                                <table id="Grid_Det_Rec_Ing"></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Det_Mov_Egr">
                                                <table id="Grid_Det_Mov_Egr"></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Grid_Det_Egr">
                                                <table id="Grid_Det_Egr"></table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="text-align:center; width:100%;" id="Td_Mostrar_PFD">
                                                <div id="Div_Mostrar_PDF" class="easyui-window" style="text-align:center; width:100%;">
                                                    
                                                </div>
                                            </td>
                                        </tr>
                                    </table>
                                </center>
                                <br /><br />
                                <br /><br />
                            </div>
                        </center>
                    </div>
                </center>
            </div>
            <br />
            <br />
            <br />
            <br />
          </ContentTemplate>
          <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Generar_Reporte"/>
          </Triggers>
    </asp:UpdatePanel>
</asp:Content>

