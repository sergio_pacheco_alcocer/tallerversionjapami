﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cat_Psp_Rubros.Negocio;
using JAPAMI.Cat_Psp_Tipos.Negocio;
using JAPAMI.Cat_Psp_Clases_Ing.Negocio;
using JAPAMI.Cls_Cat_Psp_SubConcepto.Negocio;
using JAPAMI.Catalogo_Psp_Conceptos.Negocio;
using JAPAMI.Ope_Psp_Pronosticos_Ingresos.Negocio;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;
using System.Data.OleDb;
using AjaxControlToolkit;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Pronostico_Presupuesto_Emergente : System.Web.UI.Page
{
    #region PAGE LOAD

        protected void Page_Load(object sender, EventArgs e)
        {
            String Busqueda = String.Empty;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Pronostico_Ingresos_Inicio();
            }
        }

    #endregion

    #region METODOS

    #region (Metodos Generales)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Pronostico_Ingresos_Inicio
        ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Pronostico_Ingresos_Inicio()
        {
            try
            {
                Mostrar_Ocultar_Error(false);
                Limpiar_Formulario("Todo");
                Llenar_Combo_Estatus();
                Estado_Botones("inicial");
                Llenar_Combo_Anio();
                Llenar_Combo_Fte_Financiamiento();
                Llenar_Combo_Rubro();
                Llenar_Combo_Tipo();
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos();
                Llenar_Combo_SubConceptos();
                Llenar_Grid_Registros();
                Habilitar_Forma(false);
                Estado_Botones("inicial");
                Pnl_Datos_Generales.Visible = true;
                Pnl_Cargar_Excel.Style.Add("display","none");
                Grid_Registros.Enabled = true;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Pronostico_Ingresos_Inicio ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
        ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
        ///PROPIEDADES          1 Estatus true o false para habilitar los controles
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Habilitar_Forma(Boolean Estatus)
        {
            try
            {
                Cmb_Fuente_Financiamientos.Enabled = Estatus;
                Cmb_Concepto.Enabled = Estatus;
                Cmb_Rubro.Enabled = Estatus;
                Cmb_Tipo.Enabled = Estatus;
                Cmb_Clase.Enabled = Estatus;
                Txt_Justificacion.Enabled = Estatus;
                Txt_Enero.Enabled = Estatus;
                Txt_Febrero.Enabled = Estatus;
                Txt_Marzo.Enabled = Estatus;
                Txt_Abril.Enabled = Estatus;
                Txt_Mayo.Enabled = Estatus;
                Txt_Junio.Enabled = Estatus;
                Txt_Julio.Enabled = Estatus;
                Txt_Agosto.Enabled = Estatus;
                Txt_Septiembre.Enabled = Estatus;
                Txt_Octubre.Enabled = Estatus;
                Txt_Noviembre.Enabled = Estatus;
                Txt_Diciembre.Enabled = Estatus;
                Btn_Agregar.Enabled = Estatus;
                Cmb_Estatus.Enabled = Estatus;
                Tr_SubConceptos.Visible = true;
                Cmb_SubConcepto.Enabled = Estatus;
                Cmb_Anio.Enabled = !Estatus;
                Txt_Busqueda_Concepto.Enabled = Estatus;
                Btn_Busqueda_Concepto.Enabled = Estatus;
                Txt_Busueda_SubConcepto.Enabled = Estatus;
                Btn_Busqueda_SubConcepto.Enabled = Estatus;
                //Cmb_Programa.Enabled = Estatus;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Habilitar_Forma ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Formulario
        ///DESCRIPCIÓN          : Metodo para limpiar los controles de la pagina
        ///PROPIEDADES          1 Accion para especificar que parte se limpiara si todo o las partidas asignadas
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Formulario(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Cmb_Fuente_Financiamientos.SelectedIndex = -1;
                        //Cmb_Programa.SelectedIndex = -1;
                        Cmb_SubConcepto.SelectedIndex = -1;
                        Cmb_Rubro.SelectedIndex = -1;
                        Cmb_Tipo.SelectedIndex = -1;
                        Cmb_Clase.SelectedIndex = -1;
                        Cmb_Concepto.SelectedIndex = -1;
                        Txt_Justificacion.Text = "";
                        Txt_Enero.Text = "";
                        Txt_Febrero.Text = "";
                        Txt_Marzo.Text = "";
                        Txt_Abril.Text = "";
                        Txt_Mayo.Text = "";
                        Txt_Junio.Text = "";
                        Txt_Julio.Text = "";
                        Txt_Agosto.Text = "";
                        Txt_Septiembre.Text = "";
                        Txt_Octubre.Text = "";
                        Txt_Noviembre.Text = "";
                        Txt_Diciembre.Text = "";
                        Txt_Total.Text = "";
                        Hf_Rubro_ID.Value = "";
                        Hf_Tipo_ID.Value = "";
                        Hf_Clase_ID.Value = "";
                        Hf_Concepto_ID.Value = "";
                        Hf_SubConcepto_ID.Value = "";
                        Hf_Programa.Value = "";
                        Txt_Busqueda_Concepto.Text = "";
                        Txt_Busueda_SubConcepto.Text = "";
                        break;
                    case "Datos_Concepto":
                        Txt_Enero.Text = "";
                        Txt_Febrero.Text = "";
                        Txt_Marzo.Text = "";
                        Txt_Abril.Text = "";
                        Txt_Mayo.Text = "";
                        Txt_Junio.Text = "";
                        Txt_Julio.Text = "";
                        Txt_Agosto.Text = "";
                        Txt_Septiembre.Text = "";
                        Txt_Octubre.Text = "";
                        Txt_Noviembre.Text = "";
                        Txt_Diciembre.Text = "";
                        Txt_Total.Text = "";
                        Hf_Concepto_ID.Value = "";
                        Hf_SubConcepto_ID.Value = "";
                        Cmb_SubConcepto.SelectedIndex = -1;
                        Txt_Busueda_SubConcepto.Text = "";
                        Txt_Justificacion.Text = "";
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Limpiar_Formulario ERROR[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Estado_Botones
        ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
        ///PARAMETROS:          1.- String Estado: El estado de los botones solo puede tomar 
        ///                        + inicial
        ///                        + nuevo
        ///                        + modificar
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Estado_Botones(String Estado)
        {
            DataTable Dt_Partidas_Asignadas = new DataTable();
            Dt_Partidas_Asignadas = (DataTable)Session["Dt_Partidas_Asignadas"];
            try
            {
                switch (Estado)
                {
                    case "inicial":
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        //Boton Cargar
                        Btn_Cargar_Excel.ToolTip = "Cargar Excel";
                        Btn_Cargar_Excel.Visible = true;
                        Btn_Cargar_Excel.Enabled = true;
                        Btn_Cargar_Excel.ImageUrl = "~/paginas/imagenes/paginas/microsoft_office2003_excel.png";
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        if (Dt_Partidas_Asignadas != null && Dt_Partidas_Asignadas.Rows.Count > 0)
                        {
                            Btn_Nuevo.Enabled = false;
                            if (Cmb_Estatus.SelectedValue == "GENERADO" || Cmb_Estatus.SelectedValue == "AUTORIZADO")
                            {
                                Btn_Modificar.Enabled = false;
                                Btn_Cargar_Excel.Enabled = false;
                            }
                            else
                            {
                                Btn_Modificar.Enabled = true;
                                Btn_Cargar_Excel.Enabled = true;
                            }
                        }
                        else
                        {
                            Btn_Nuevo.Enabled = true;
                            Btn_Modificar.Enabled = true;
                            Btn_Cargar_Excel.Enabled = true;
                        }
                        

                        //Boton Salir
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                       
                        break;
                    case "nuevo":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Guardar";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Cargar
                        Btn_Cargar_Excel.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "modificar":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Cargar
                        Btn_Cargar_Excel.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "excel":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Cargar
                        Btn_Cargar_Excel.Visible = true;
                        Btn_Cargar_Excel.ToolTip = "Cargar";
                        Btn_Cargar_Excel.Enabled = true;
                        Btn_Cargar_Excel.ImageUrl = "~/paginas/imagenes/paginas/subir.png";
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                }//fin del switch
            }
            catch (Exception ex)
            {
                throw new Exception("Error al habilitar el  Estado_Botones ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Inicio_Dt_Registros
        ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Crear_Inicio_Dt_Registros()
        {
            DataTable Dt_Registros = new DataTable();
            try
            {
                Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_FTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_CONCEPTOS", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_PROGRAMAS", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("SUBCONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ENERO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("MARZO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("MAYO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JULIO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_CONCEPTO_ING", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_CLASE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_TIPO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_RUBRO", System.Type.GetType("System.String"));

                Session["Dt_Registros"] = (DataTable)Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Registros
        ///DESCRIPCIÓN          : Metodo para agregar los datos de los registros al datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Agregar_Fila_Dt_Registros()
        {
            DataTable Dt_Registros = new DataTable();
            DataRow Fila;
            Dt_Registros = (DataTable)Session["Dt_Registros"];
            try
            {
                Fila = Dt_Registros.NewRow();
                Fila["FUENTE_FINANCIAMIENTO_ID"] = Cmb_Fuente_Financiamientos.SelectedItem.Value.Trim();
                Fila["CLAVE_FTE_FINANCIAMIENTO"] = Cmb_Fuente_Financiamientos.SelectedItem.Text.Trim();
                Fila["RUBRO_ID"] = Hf_Rubro_ID.Value.Trim();

                if (!String.IsNullOrEmpty(Cmb_SubConcepto.SelectedItem.Value.Trim()))
                {
                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Cmb_SubConcepto.SelectedItem.Text.Trim();
                }
                else
                {
                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Cmb_Concepto.SelectedItem.Text.Trim();
                }
                //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                //{
                //    Fila["CLAVE_NOMBRE_PROGRAMAS"] = Cmb_Programa.SelectedItem.Text.Trim();
                //}
                //else
                //{
                    Fila["CLAVE_NOMBRE_PROGRAMAS"] = String.Empty;
                //}

                Fila["TIPO_ID"] = Hf_Tipo_ID.Value.Trim();
                Fila["CLASE_ING_ID"] = Hf_Clase_ID.Value.Trim();
                Fila["CONCEPTO_ING_ID"] = Cmb_Concepto.SelectedItem.Value.Trim();
                if (Tr_SubConceptos.Visible)
                {
                    if (!String.IsNullOrEmpty(Cmb_SubConcepto.SelectedItem.Value.Trim()))
                    {
                        Fila["SUBCONCEPTO_ING_ID"] = Cmb_SubConcepto.SelectedItem.Value.Trim();
                    }
                    else 
                    {
                        Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                    }
                }
                else
                {
                    Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                }

                Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();
               
                
                Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim()));
                Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim()));
                Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim()));
                Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim()));
                Fila["MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim()));
                Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim()));
                Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim()));
                Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim()));
                Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim()));
                Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim()));
                Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim()));
                Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim()));
                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim()));
                Fila["ID"] = "";
                //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                //{
                //    Fila["PROGRAMA_ID"] = Cmb_Programa.SelectedItem.Value.Trim();
                //}
                //else
                //{
                    Fila["PROGRAMA_ID"] = String.Empty;
                //}
                Fila["CLAVE_NOMBRE_CONCEPTO_ING"] = Cmb_Concepto.SelectedItem.Text.Trim();
                Fila["CLAVE_NOMBRE_CLASE"] = Cmb_Clase.SelectedItem.Text.Trim();
                Fila["CLAVE_NOMBRE_TIPO"] = Cmb_Tipo.SelectedItem.Text.Trim();
                Fila["CLAVE_NOMBRE_RUBRO"] = Cmb_Rubro.SelectedItem.Text.Trim();
                Dt_Registros.Rows.Add(Fila);

                Session["Dt_Registros"] = (DataTable)Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
        ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Sessiones()
        {
            Session["Dt_Registros"] = new DataTable();
            Grid_Registros.DataSource = new DataTable();
            Grid_Registros.DataBind();
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Datos
        ///DESCRIPCIÓN          : Metodo para validar los datos del formulario
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private Boolean Validar_Datos()
        {
            Boolean Datos_Validos = true;
            Lbl_Encanezado_Error.Text = String.Empty;
            Lbl_Encanezado_Error.Text = "Favor de:";
            Lbl_Error.Text = String.Empty;
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = (DataTable)Session["Dt_Registros"];
            String Rubro = String.Empty;
            String Tipo = String.Empty;
            String Clase = String.Empty;
            String Concepto = String.Empty;
            String SubConcepto = String.Empty;
            String Fte_Financiamiento = String.Empty;
            String Programa = String.Empty;

            String Rubro_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String Clase_ID = String.Empty;
            String Concepto_ID = String.Empty;
            String SubConcepto_ID = String.Empty;
            String Fte_Financiamiento_ID = String.Empty;
            String Programa_ID = String.Empty;
            Double Importe_Pro = 0.00;
            Double Total_Con = 0.00;

            try
            {
                if (String.IsNullOrEmpty(Cmb_Fuente_Financiamientos.SelectedItem.Value.Trim()))
                {
                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una Fuente de Financiamiento. <br />";
                    Datos_Validos = false;
                }
                if (String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
                {
                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un Concepto. <br />";
                    Datos_Validos = false;
                }
                if (Tr_SubConceptos.Visible)
                {
                    if (Cmb_SubConcepto.SelectedIndex <= 0)
                    {
                        Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un SubConcepto. <br />";
                        Datos_Validos = false;
                    }
                }
                      
                if (String.IsNullOrEmpty(Txt_Total.Text.Trim()) || Txt_Total.Text.Trim() == "0.00")
                {
                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir la cantidad presupuestal para los meses. <br />";
                    Datos_Validos = false;
                }
                
                //if(!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                //{
                //    Importe_Pro = Convert.ToDouble(String.IsNullOrEmpty(Hf_Importe_Programa.Value) ? "0" : Hf_Importe_Programa.Value.Trim());
                //    Total_Con = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text) ? "0" : Txt_Total.Text.Trim());

                //    if (String.IsNullOrEmpty(Cmb_SubConcepto.SelectedItem.Value.Trim()))
                //    {
                //        if (Importe_Pro > 0)
                //        {
                //            if (Total_Con > Importe_Pro || Total_Con < Importe_Pro)
                //            {
                //                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir la cantidad presupuestal correcta para el programa que es: Importe Programa[" + String.Format("{0:n}", Importe_Pro) + "]. <br />";
                //                Datos_Validos = false;
                //            }
                //        }
                //    }
                //}

                if(Dt_Registros != null)
                {
                    if(Dt_Registros.Rows.Count > 0)
                    {
                        foreach(DataRow Dr in Dt_Registros.Rows)
                        {
                            Fte_Financiamiento = Cmb_Fuente_Financiamientos.SelectedItem.Value.Trim();
                            //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                            //    Programa = Cmb_Programa.SelectedItem.Value.Trim();
                            Rubro = Hf_Rubro_ID.Value.Trim();
                            Tipo = Hf_Tipo_ID.Value.Trim();
                            Clase = Hf_Clase_ID.Value.Trim();
                            Concepto = Cmb_Concepto.SelectedItem.Value.Trim();
                            if (Tr_SubConceptos.Visible)
                                SubConcepto = Cmb_SubConcepto.SelectedItem.Value.Trim();


                            Rubro_ID = Dr["RUBRO_ID"].ToString().Trim();
                            Tipo_ID = Dr["TIPO_ID"].ToString().Trim();
                            Clase_ID= Dr["CLASE_ING_ID"].ToString().Trim();
                            Concepto_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                            SubConcepto_ID = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                            Fte_Financiamiento_ID = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                            Programa_ID = Dr["PROGRAMA_ID"].ToString().Trim();

                            if (Fte_Financiamiento_ID.Trim().Equals(Fte_Financiamiento.Trim()) && Rubro_ID.Trim().Equals(Rubro.Trim()) 
                                    && Tipo_ID.Trim().Equals(Tipo.Trim()) && Clase_ID.Trim().Equals(Clase.Trim()) 
                                    && Concepto_ID.Trim().Equals(Concepto.Trim()) && SubConcepto_ID.Trim().Equals(SubConcepto.Trim())
                                    && Programa_ID.Trim().Equals(Programa.Trim()))
                            {
                                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar otro registro pues este ya fue seleccionado. <br />";
                                Datos_Validos = false;
                            }
                            
                        }
                    }
                }

                return Datos_Validos;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Mostrar_Ocultar_Error
        ///DESCRIPCIÓN          : Metodo para mostrar u ocultar los errores
        ///PROPIEDADES          1 Estatus ocultar o mostrar
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Mostrar_Ocultar_Error(Boolean Estatus)
        {
            Lbl_Error.Visible = Estatus;
            Lbl_Encanezado_Error.Visible = Estatus;
            Img_Error.Visible = Estatus;
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Presupuestado
        ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva presupuestodo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Calcular_Total_Presupuestado()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = (DataTable)Session["Dt_Registros"];
            Double Total = 0.00;

            try
            {
                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                        }
                    }
                }
                Txt_Total_Ajuste.Text = "";
                Txt_Total_Ajuste.Text = String.Format("{0:c}", Total);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al calcular el total de los registros. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Registros
        ///DESCRIPCIÓN          : Metodo para llenar el grid anidado de las partidas asignadas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Grid_Registros()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Session = new DataTable();
            DataTable Dt_Registros = new DataTable();
            Dt_Session = (DataTable)Session["Dt_Registros"];
            Cmb_Estatus.SelectedIndex = -1;
            try
            {
                if (Dt_Session != null)
                {
                    if (Dt_Session.Rows.Count > 0)
                    {
                        Dt_Registros = Dt_Session; ;
                    }
                    else
                    {
                            Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                            Dt_Registros = Negocio.Consulta_Pronostico();
                            if(Dt_Registros != null)
                            {
                                if (Dt_Registros.Rows.Count > 0)
                                {
                                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Dt_Registros.Rows[0]["ESTATUS"].ToString().Trim()));
                                    Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(Dt_Registros.Rows[0]["Anio"].ToString().Trim()));
                                }
                            }

                            Session["Dt_Registros"] = Dt_Registros;
                    }
                }
                else
                {
                    Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Dt_Registros = Negocio.Consulta_Pronostico();

                    if (Dt_Registros != null)
                    {
                        if (Dt_Registros.Rows.Count > 0)
                        {
                            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Dt_Registros.Rows[0]["ESTATUS"].ToString().Trim()));
                            Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(Dt_Registros.Rows[0]["Anio"].ToString().Trim()));
                        }
                    }

                    Session["Dt_Registros"] = Dt_Registros;
                }

                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Rows.Count > 0)
                    {
                        if (Dt_Registros.Columns.Contains("ESTATUS") && !String.IsNullOrEmpty(Dt_Registros.Rows[0]["ESTATUS"].ToString().Trim()))
                        {
                            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Dt_Registros.Rows[0]["ESTATUS"].ToString().Trim()));
                            Cmb_Anio.SelectedIndex = Cmb_Anio.Items.IndexOf(Cmb_Anio.Items.FindByValue(Dt_Registros.Rows[0]["Anio"].ToString().Trim()));
                        }

                        Obtener_Consecutivo_ID();
                        Dt_Registros = Crear_Dt_Registros();
                        Grid_Registros.Columns[1].Visible = true;
                        Grid_Registros.DataSource = Dt_Registros;
                        Grid_Registros.DataBind();
                        Grid_Registros.Columns[1].Visible = false;
                    }
                    else
                    {
                        Grid_Registros.DataSource = new DataTable();
                        Grid_Registros.DataBind();
                    }
                }
                else
                {
                    Grid_Registros.DataSource = new DataTable();
                    Grid_Registros.DataBind();
                }
                Calcular_Total_Presupuestado();
                if (Cmb_Estatus.SelectedItem.Text.Trim().Equals("AUTORIZADO"))
                {
                    Btn_Agregar.Visible = false;
                    Btn_Borrar.Visible = false;
                }
                else {
                    Btn_Agregar.Visible = true;
                    Btn_Borrar.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de llenar la tabla de los registros Error[" + ex.Message + "]");
            }
    }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Registros
        ///DESCRIPCIÓN          : Metodo para crear el datatable de los registros del grid anidado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Registros()
        {
            DataTable Dt_Registros = new DataTable();
            DataTable Dt_Session = new DataTable();
            Dt_Session = (DataTable)Session["Dt_Registros"];
            String Fte_Financiamiento_Id = String.Empty;
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Total = 0.00;
            DataRow Fila;
            String Clave = String.Empty;
            Boolean Iguales;

            try
            {
                if (Dt_Session != null)
                {
                    if (Dt_Session.Rows.Count > 0)
                    {
                        //creamos las columnas del datatable donde se guardaran los datos
                        Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("CLAVE_FTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_ENE", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_FEB", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_MAR", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_ABR", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_MAY", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_JUN", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_JUL", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_AGO", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_SEP", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_OCT", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_NOV", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL_DIC", System.Type.GetType("System.String"));
                        Dt_Registros.Columns.Add("TOTAL", System.Type.GetType("System.String"));


                        foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                        {
                            //Obtenemos la partida id y la clave
                            Fte_Financiamiento_Id = Dr_Sessiones["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                            Clave = Dr_Sessiones["CLAVE_FTE_FINANCIAMIENTO"].ToString().Trim();
                            Iguales = false;
                            //verificamos si la partida no a sido ya agrupada
                            if (Dt_Registros.Rows.Count > 0)
                            {
                                foreach (DataRow Dr_Partidas in Dt_Registros.Rows)
                                {
                                    if (Dr_Partidas["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Fte_Financiamiento_Id))
                                    {
                                        Iguales = true;
                                    }
                                }
                            }

                            // tomamos los datos del datatable para agrupar las partidas asignadas
                            if (!Iguales)
                            {
                                foreach (DataRow Dr_Session in Dt_Session.Rows)
                                {
                                    if (Dr_Session["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Fte_Financiamiento_Id))
                                    {
                                        Ene = Ene + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"].ToString().Trim());
                                        Feb = Feb + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"].ToString().Trim());
                                        Mar = Mar + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"].ToString().Trim());
                                        Abr = Abr + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"].ToString().Trim());
                                        May = May + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"].ToString().Trim());
                                        Jun = Jun + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"].ToString().Trim());
                                        Jul = Jul + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"].ToString().Trim());
                                        Ago = Ago + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"].ToString().Trim());
                                        Sep = Sep + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"].ToString().Trim());
                                        Oct = Oct + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"].ToString().Trim());
                                        Nov = Nov + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"].ToString().Trim());
                                        Dic = Dic + Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"].ToString().Trim());
                                    }
                                }
                                Total = Ene + Feb + Mar + Abr + May + Jun + Jul + Ago + Sep + Oct + Nov + Dic;

                                Fila = Dt_Registros.NewRow();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Fte_Financiamiento_Id;
                                Fila["CLAVE_FTE_FINANCIAMIENTO"] = Clave;
                                Fila["TOTAL_ENE"] = String.Format("{0:##,###,##0.00}", Ene);
                                Fila["TOTAL_FEB"] = String.Format("{0:##,###,##0.00}", Feb);
                                Fila["TOTAL_MAR"] = String.Format("{0:##,###,##0.00}", Mar);
                                Fila["TOTAL_ABR"] = String.Format("{0:##,###,##0.00}", Abr);
                                Fila["TOTAL_MAY"] = String.Format("{0:##,###,##0.00}", May);
                                Fila["TOTAL_JUN"] = String.Format("{0:##,###,##0.00}", Jun);
                                Fila["TOTAL_JUL"] = String.Format("{0:##,###,##0.00}", Jul);
                                Fila["TOTAL_AGO"] = String.Format("{0:##,###,##0.00}", Ago);
                                Fila["TOTAL_SEP"] = String.Format("{0:##,###,##0.00}", Sep);
                                Fila["TOTAL_OCT"] = String.Format("{0:##,###,##0.00}", Oct);
                                Fila["TOTAL_NOV"] = String.Format("{0:##,###,##0.00}", Nov);
                                Fila["TOTAL_DIC"] = String.Format("{0:##,###,##0.00}", Dic);
                                Fila["TOTAL"] = String.Format("{0:##,###,##0.00}", Total);
                                Dt_Registros.Rows.Add(Fila);

                                Ene = 0.00;
                                Feb = 0.00;
                                Mar = 0.00;
                                Abr = 0.00;
                                May = 0.00;
                                Jun = 0.00;
                                Jul = 0.00;
                                Ago = 0.00;
                                Sep = 0.00;
                                Oct = 0.00;
                                Nov = 0.00;
                                Dic = 0.00;
                                Total = 0.00;
                            }
                        }
                    }
                }
                return Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear la tabla de registros Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Detalles
        ///DESCRIPCIÓN          : Metodo para crear el datatable de los detalles de las partidas asignadas del grid 
        ///PROPIEDADES          1 Rubro_ID del cual obtendremos los detalles
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Detalles(String Fte_Financiamiento_ID)
        {
            DataTable Dt_Detalle = new DataTable();
            DataTable Dt_Session = new DataTable();
            Dt_Session = (DataTable)Session["Dt_Registros"];
            DataRow Fila;

            try
            {
                if (Dt_Session != null)
                {
                    if (Dt_Session.Rows.Count > 0)
                    {
                        Dt_Detalle.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("SUBCONCEPTO_ING_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_CONCEPTOS", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_PROGRAMAS", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("ENERO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("FEBRERO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("MARZO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("ABRIL", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("MAYO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("JUNIO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("JULIO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("AGOSTO", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("OCTUBRE", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("NOVIEMBRE", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("DICIEMBRE", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.Double"));
                        Dt_Detalle.Columns.Add("ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_CONCEPTO_ING", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_CLASE", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_TIPO", System.Type.GetType("System.String"));
                        Dt_Detalle.Columns.Add("CLAVE_NOMBRE_RUBRO", System.Type.GetType("System.String"));

                        foreach (DataRow Dr_Session in Dt_Session.Rows)
                        {
                            if (Dr_Session["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(Fte_Financiamiento_ID.Trim()))
                            {
                                Fila = Dt_Detalle.NewRow();
                                Fila["RUBRO_ID"] = Dr_Session["RUBRO_ID"].ToString().Trim();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr_Session["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Fila["TIPO_ID"] = Dr_Session["TIPO_ID"].ToString().Trim();
                                Fila["CLASE_ING_ID"] = Dr_Session["CLASE_ING_ID"].ToString().Trim();
                                Fila["CONCEPTO_ING_ID"] = Dr_Session["CONCEPTO_ING_ID"].ToString().Trim();
                                Fila["SUBCONCEPTO_ING_ID"] = Dr_Session["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                Fila["JUSTIFICACION"] = Dr_Session["JUSTIFICACION"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr_Session["CLAVE_NOMBRE_CONCEPTOS"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr_Session["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ENERO"].ToString()) ? "0" : Dr_Session["ENERO"]);
                                Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["FEBRERO"].ToString()) ? "0" : Dr_Session["FEBRERO"]);
                                Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MARZO"].ToString()) ? "0" : Dr_Session["MARZO"]);
                                Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["ABRIL"].ToString()) ? "0" : Dr_Session["ABRIL"]);
                                Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["MAYO"].ToString()) ? "0" : Dr_Session["MAYO"]);
                                Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JUNIO"].ToString()) ? "0" : Dr_Session["JUNIO"]);
                                Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["JULIO"].ToString()) ? "0" : Dr_Session["JULIO"]);
                                Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["AGOSTO"].ToString()) ? "0" : Dr_Session["AGOSTO"]);
                                Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["SEPTIEMBRE"].ToString()) ? "0" : Dr_Session["SEPTIEMBRE"]);
                                Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["OCTUBRE"].ToString()) ? "0" : Dr_Session["OCTUBRE"]);
                                Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["NOVIEMBRE"].ToString()) ? "0" : Dr_Session["NOVIEMBRE"]);
                                Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["DICIEMBRE"].ToString()) ? "0" : Dr_Session["DICIEMBRE"]);
                                Fila["IMPORTE_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["IMPORTE_TOTAL"].ToString()) ? "0" : Dr_Session["IMPORTE_TOTAL"]);
                                Fila["ID"] = Dr_Session["ID"].ToString().Trim();
                                Fila["PROGRAMA_ID"] = Dr_Session["PROGRAMA_ID"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_CONCEPTO_ING"] = Dr_Session["CLAVE_NOMBRE_CONCEPTO_ING"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_CLASE"] = Dr_Session["CLAVE_NOMBRE_CLASE"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_TIPO"] = Dr_Session["CLAVE_NOMBRE_TIPO"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_RUBRO"] = Dr_Session["CLAVE_NOMBRE_RUBRO"].ToString().Trim();
                                Dt_Detalle.Rows.Add(Fila);
                            }
                        }
                    }
                }
                return Dt_Detalle;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear la tabla de registros Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
        ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Obtener_Consecutivo_ID()
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = (DataTable)Session["Dt_Registros"];
            int Contador;
            try
            {
                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Columns.Count > 0)
                    {
                        if (Dt_Registros.Rows.Count > 0)
                        {
                            Contador = -1;
                            foreach (DataRow Dr in Dt_Registros.Rows)
                            {
                                Contador++;
                                Dr["ID"] = Contador.ToString().Trim();
                            }
                        }
                    }
                }
                Session["Dt_Registros"] = Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cargar_Excel
        ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Cargar_Excel(String Ruta, String Extension)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            Mostrar_Ocultar_Error(false);
            DataTable Dt_Registros = new DataTable();

            
            try
            {
                Dt_Registros = Leer_Excel(Ruta, Extension);
                Dt_Registros = Crear_Dt_Registros_Carga_Excel(Dt_Registros);
                
                Negocio.P_Anio = Cmb_Anio_P.SelectedItem.Value.Trim();
                Negocio.P_Dt_Datos = Dt_Registros;
                Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Negocio.P_Estatus = Cmb_Estatus_P.SelectedItem.Value.Trim();

                if (Negocio.Guardar_Registros())
                {
                    Limpiar_Sessiones();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Carga Exitosa');", true);
                    Pronostico_Ingresos_Inicio();
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Leer_Excel
        ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Leer_Excel(String Ruta, String Extension)
        {
            DataTable Dt_Informacion = new DataTable();
            DataSet Ds_Datos = new DataSet();
            OleDbDataAdapter Adaptador = new OleDbDataAdapter();
            OleDbConnection Conexion = new OleDbConnection();
            OleDbCommand Comando = new OleDbCommand();

            try
            {
                if (Extension.Equals("xls"))
                {
                    Conexion.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                                    "Data Source=" + Ruta + ";" +
                                    "Extended Properties=\"Excel 8.0;HDR=YES\"";
                }
                else if (Extension.Equals("xlsx")) 
                {
                    Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                    "Data Source=" + Ruta + ";" +
                    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                }

                if (!String.IsNullOrEmpty(Conexion.ConnectionString))
                {
                    Conexion.Open();
                    Comando.CommandText = "SELECT * FROM [PronosticoIng$]";
                    Comando.Connection = Conexion;
                    Adaptador.SelectCommand = Comando;

                    if (Adaptador != null)
                    {
                        Adaptador.Fill(Ds_Datos);
                    }

                    if (Ds_Datos != null)
                    {
                        Dt_Informacion = Ds_Datos.Tables[0];
                    }

                    Conexion.Close();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
            return Dt_Informacion;
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Registros_Carga_Excel
        ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Registros_Carga_Excel(DataTable Dt_Datos)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();

            Cls_Cat_Psp_SubConceptos_Negocio Negocio_Ing = new Cls_Cat_Psp_SubConceptos_Negocio();
            DataTable Dt_Registros = new DataTable();
            DataTable Dt_Fte_Financiamiento = new DataTable();
            DataTable Dt_Programa = new DataTable();
            DataTable Dt_Conceptos = new DataTable();
            DataTable Dt = new DataTable();
            String Concepto = String.Empty;
            String FF = String.Empty;
            String PP = String.Empty;
            String Concepto_ID = String.Empty;
            String Rubro_ID = String.Empty;
            String SubConcepto_ID = String.Empty;
            String Clase_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String FF_ID = String.Empty;
            String PP_ID = String.Empty;
            DataRow Fila;
            Int32 Contador = -1;
            DataTable Dt_Pronostico_Ing = new DataTable();
            Boolean Repetido = false;
            String Mensaje = String.Empty;

            try
            {
                Dt_Registros.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_FTE_FINANCIAMIENTO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_CONCEPTOS", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLAVE_NOMBRE_PROGRAMAS", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("SUBCONCEPTO_ING_ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ENERO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("MARZO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("MAYO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("JULIO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("ID", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));

                Ingresos_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                Dt_Pronostico_Ing = Ingresos_Negocio.Consulta_Pronostico();
                Dt_Conceptos = Negocio_Ing.Consultar_Datos_Completos_Ing();
                Dt_Fte_Financiamiento = Ingresos_Negocio.Consulta_Fte_Financiamiento_Todas();

                if (Dt_Conceptos != null && Dt_Fte_Financiamiento != null && Dt_Datos != null)
                {
                    if (Dt_Conceptos.Rows.Count > 0 && Dt_Fte_Financiamiento.Rows.Count > 0 && Dt_Datos.Rows.Count > 0)
                    {
                        foreach (DataRow Dr_Ing in Dt_Datos.Rows)
                        {
                            PP_ID = String.Empty;
                            FF_ID = String.Empty;
                            Concepto_ID = String.Empty;
                            SubConcepto_ID = String.Empty;
                            Rubro_ID = String.Empty;
                            Tipo_ID = String.Empty;
                            Clase_ID = String.Empty;

                            Contador++;
                            Concepto = Dr_Ing["Clave Nombre Concepto"].ToString().Trim().ToUpper();
                            FF = Dr_Ing["Fuente Financiamiento"].ToString().Trim().ToUpper();
                            PP = Dr_Ing["Programa"].ToString().Trim().ToUpper();

                            Concepto = Concepto.Substring(0, Concepto.IndexOf(" "));

                            if(FF.Contains(" "))
                            {
                                FF = FF.Substring(0, FF.IndexOf(" "));
                            }

                            if (PP.Contains(" "))
                            {
                                PP = PP.Substring(0, PP.IndexOf(" "));
                            }

                            foreach (DataRow Dr_Con in Dt_Conceptos.Rows)
                            {
                                if (Dr_Con["CLAVE_CONCEPTO"].ToString().Trim().Equals(Concepto))
                                {
                                    Concepto_ID = Dr_Con["CONCEPTO_ING_ID"].ToString().Trim();
                                    SubConcepto_ID = String.Empty;
                                    Rubro_ID = Dr_Con["RUBRO_ID"].ToString().Trim();
                                    Tipo_ID = Dr_Con["TIPO_ID"].ToString().Trim();
                                    Clase_ID = Dr_Con["CLASE_ING_ID"].ToString().Trim();
                                    break;
                                }
                                else if (Dr_Con["CLAVE_SUBCONCEPTO"].ToString().Trim().Equals(Concepto))
                                {
                                    Concepto_ID = Dr_Con["CONCEPTO_ING_ID"].ToString().Trim();
                                    SubConcepto_ID = Dr_Con["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                    Rubro_ID = Dr_Con["RUBRO_ID"].ToString().Trim();
                                    Tipo_ID = Dr_Con["TIPO_ID"].ToString().Trim();
                                    Clase_ID = Dr_Con["CLASE_ING_ID"].ToString().Trim();
                                    break;
                                }
                            }

                            foreach (DataRow Dr_FF in Dt_Fte_Financiamiento.Rows)
                            {
                                if (Dr_FF["CLAVE"].ToString().Trim().Equals(FF))
                                {
                                    FF_ID = Dr_FF["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                    break;
                                }
                            }

                            if (!String.IsNullOrEmpty(PP))
                            {
                                Negocio.P_Busqueda = PP.ToUpper().Trim();
                                Negocio.P_Concepto_ID = String.Empty;
                                Negocio.P_Programa_ID = String.Empty;
                                Negocio.P_Fte_Financiamiento = String.Empty;
                                Dt_Programa = Negocio.Consultar_Programas();

                                if (Dt_Programa != null && Dt_Programa.Rows.Count > 0)
                                {
                                    PP_ID = Dt_Programa.Rows[0][Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString().Trim();
                                    PP = Dt_Programa.Rows[0]["CLAVE_NOMBRE"].ToString().Trim();
                                }
                            }

                            if (!String.IsNullOrEmpty(FF_ID.Trim()) && !String.IsNullOrEmpty(Concepto_ID.Trim()))
                            {
                                if (Dt_Pronostico_Ing != null)
                                {
                                    if (Dt_Pronostico_Ing.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Ingreso in Dt_Pronostico_Ing.Rows)
                                        {
                                            if (Dr_Ingreso["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(FF_ID) &&
                                                Dr_Ingreso["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) &&
                                                Dr_Ingreso["RUBRO_ID"].ToString().Trim().Equals(Rubro_ID) &&
                                                Dr_Ingreso["TIPO_ID"].ToString().Trim().Equals(Tipo_ID) &&
                                                Dr_Ingreso["CLASE_ING_ID"].ToString().Trim().Equals(Clase_ID) &&
                                                Dr_Ingreso["CONCEPTO_ING_ID"].ToString().Trim().Equals(Concepto_ID) &&
                                                Dr_Ingreso["SUBCONCEPTO_ING_ID"].ToString().Trim().Equals(SubConcepto_ID) &&
                                                Dr_Ingreso["ANIO"].ToString().Trim().Equals(Cmb_Anio.SelectedItem.Value.Trim()))
                                            {
                                                Repetido = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (Dt_Registros != null)
                                {
                                    if (Dt_Registros.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Ingresos in Dt_Registros.Rows)
                                        {
                                            if (Dr_Ingresos["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim().Equals(FF_ID) &&
                                                Dr_Ingresos["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) &&
                                                Dr_Ingresos["RUBRO_ID"].ToString().Trim().Equals(Rubro_ID) &&
                                                Dr_Ingresos["TIPO_ID"].ToString().Trim().Equals(Tipo_ID) &&
                                                Dr_Ingresos["CLASE_ING_ID"].ToString().Trim().Equals(Clase_ID) &&
                                                Dr_Ingresos["CONCEPTO_ING_ID"].ToString().Trim().Equals(Concepto_ID) &&
                                                Dr_Ingresos["SUBCONCEPTO_ING_ID"].ToString().Trim().Equals(SubConcepto_ID) &&
                                                Dr_Ingresos["ANIO"].ToString().Trim().Equals(Cmb_Anio.SelectedItem.Value.Trim()))
                                            {
                                                Repetido = true;
                                                break;
                                            }
                                        }
                                    }
                                }

                                if (!Repetido)
                                {
                                    Fila = Dt_Registros.NewRow();
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = FF_ID.Trim();
                                    Fila["CLAVE_FTE_FINANCIAMIENTO"] = FF.Trim();
                                    Fila["RUBRO_ID"] = Rubro_ID.Trim();
                                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Concepto.Trim();
                                    Fila["CLAVE_NOMBRE_PROGRAMAS"] = PP.Trim();
                                    Fila["TIPO_ID"] = Tipo_ID.Trim();
                                    Fila["CLASE_ING_ID"] = Clase_ID.Trim();
                                    Fila["CONCEPTO_ING_ID"] = Concepto_ID.Trim();
                                    Fila["SUBCONCEPTO_ING_ID"] = SubConcepto_ID.Trim();
                                    Fila["JUSTIFICACION"] = String.Empty;
                                    Fila["ENERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Enero"].ToString().Trim()) ? "0" : Dr_Ing["Enero"].ToString().Trim().Replace(",", ""));
                                    Fila["FEBRERO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Febrero"].ToString().Trim()) ? "0" : Dr_Ing["Febrero"].ToString().Trim().Replace(",", ""));
                                    Fila["MARZO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Marzo"].ToString().Trim()) ? "0" : Dr_Ing["Marzo"].ToString().Trim().Replace(",", ""));
                                    Fila["ABRIL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Abril"].ToString().Trim()) ? "0" : Dr_Ing["Abril"].ToString().Trim().Replace(",", ""));
                                    Fila["MAYO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Mayo"].ToString().Trim()) ? "0" : Dr_Ing["Mayo"].ToString().Trim().Replace(",", ""));
                                    Fila["JUNIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Junio"].ToString().Trim()) ? "0" : Dr_Ing["Junio"].ToString().Trim().Replace(",", ""));
                                    Fila["JULIO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Julio"].ToString().Trim()) ? "0" : Dr_Ing["Julio"].ToString().Trim().Replace(",", ""));
                                    Fila["AGOSTO"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Agosto"].ToString().Trim()) ? "0" : Dr_Ing["Agosto"].ToString().Trim().Replace(",", ""));
                                    Fila["SEPTIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Septiembre"].ToString().Trim()) ? "0" : Dr_Ing["Septiembre"].ToString().Trim().Replace(",", ""));
                                    Fila["OCTUBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Octubre"].ToString().Trim()) ? "0" : Dr_Ing["Octubre"].ToString().Trim().Replace(",", ""));
                                    Fila["NOVIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Noviembre"].ToString().Trim()) ? "0" : Dr_Ing["Noviembre"].ToString().Trim().Replace(",", ""));
                                    Fila["DICIEMBRE"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Diciembre"].ToString().Trim()) ? "0" : Dr_Ing["Diciembre"].ToString().Trim().Replace(",", ""));
                                    Fila["IMPORTE_TOTAL"] = Convert.ToDouble(String.IsNullOrEmpty(Dr_Ing["Total"].ToString().Trim()) ? "0" : Dr_Ing["Total"].ToString().Trim().Replace(",", ""));
                                    Fila["ID"] = Contador.ToString().Trim();
                                    Fila["PROGRAMA_ID"] = PP_ID.ToString().Trim();

                                    Dt_Registros.Rows.Add(Fila);
                                }
                            }
                            else 
                            {
                                //guardamos el numero de registro que no se pudo agregar
                                Mensaje += Contador + ", ";
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
            return Dt_Registros;
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Busqueda_Conceptos
        ///DESCRIPCIÓN          : Metodo para obtener la busqueda de los conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Busqueda_Conceptos(String Texto_Busqueda)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Datos = new DataTable();

            try
            {
                Negocio.P_Anio = String.Empty;
                if (!String.IsNullOrEmpty(Cmb_Concepto.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Concepto_Ing_ID = String.Empty;
                }

                //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                //{
                //    Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                //}
                //else
                //{
                    Negocio.P_Programa_ID = String.Empty;
                //}

                if (String.IsNullOrEmpty(Texto_Busqueda.Trim()))
                {
                    Negocio.P_Descripcion = String.Empty;
                    Dt_Datos = Negocio.Consultar_Concepto_Ing();
                }
                else 
                {
                    Negocio.P_Descripcion = Texto_Busqueda.Trim();
                    Dt_Datos = Negocio.Consultar_Concepto_Ing();
                }

                if (!String.IsNullOrEmpty(Cmb_Concepto.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Concepto_Ing_ID = String.Empty;
                }

                Cmb_Concepto.Items.Clear();
                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    Cmb_Concepto.DataValueField = Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID;
                    Cmb_Concepto.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Concepto.DataSource = Dt_Datos;
                    Cmb_Concepto.DataBind();
                    Cmb_Concepto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Concepto.SelectedIndex = -1;
                    Cmb_SubConcepto.SelectedIndex = -1;
                    Llenar_Combo_SubConceptos();
                }
                else 
                {
                    Cmb_Concepto.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la busqueda de los conceptos Erro[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Busqueda_SubConceptos
        ///DESCRIPCIÓN          : Metodo para obtener la busqueda de los subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Busqueda_SubConceptos(String Texto_Busqueda)
        {
            Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();
            DataTable Dt_Datos = new DataTable();

            try
            {
                Negocio.P_Anio = String.Empty;
                if (String.IsNullOrEmpty(Texto_Busqueda.Trim()))
                {
                    Negocio.P_Descripcion = String.Empty;
                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                    Dt_Datos = Negocio.Consultar_SubConcepto_Concepto_Ing();
                }
                else
                {
                    Negocio.P_Descripcion = Texto_Busqueda.Trim();
                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                    Dt_Datos = Negocio.Consultar_SubConcepto_Concepto_Ing();
                }

                Cmb_SubConcepto.Items.Clear();
                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    Cmb_SubConcepto.DataValueField = Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID;
                    Cmb_SubConcepto.DataTextField = "CLAVE_NOMBRE_SUBCONCEPTO";
                    Cmb_SubConcepto.DataSource = Dt_Datos;
                    Cmb_SubConcepto.DataBind();
                    Cmb_SubConcepto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_SubConcepto.SelectedIndex = -1;
                }
                else
                {
                    Cmb_SubConcepto.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en la busqueda de los conceptos Erro[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : AFU_Archivo_Excel_UploadedComplete
        ///DESCRIPCIÓN          : Metodo para obtener el nombre del archivo que se cargara y guardarlo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Abril/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
        {
            try
            {
                AsyncFileUpload Archivo;
                String Nombre_Archivo = String.Empty;
                String Extension = String.Empty;
                String[] Archivos;
                Int32 No = 0;
                String Ruta = String.Empty;
                String Ruta_Destino = String.Empty;
                HttpContext.Current.Session["Nombre_Archivo_Excel"] = null;
                DataTable Dt_Archivos = new DataTable();
                DataRow Fila = null;

                if (AFU_Archivo.HasFile)
                {
                    //obtenemos los datos del archivo
                    Archivo = (AsyncFileUpload)sender;
                    Nombre_Archivo = Archivo.FileName;

                    Archivos = Nombre_Archivo.Split('\\');
                    No = Archivos.Length;
                    Ruta = Archivos[No - 1];
                    Extension = Ruta.Substring(Ruta.LastIndexOf(".") + 1);

                    if (Extension == "xls" || Extension == "xlsx")
                    {
                        if (Archivo.FileContent.Length > 2621440)
                        {
                            Lbl_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Error.Text = "El tamaño del archivo sobrepasa";
                            return;
                        }

                        Ruta_Destino = Server.MapPath("../../Archivos/Presupuestos/Pronostico_" + Cmb_Anio_P.SelectedItem.Value.Trim() + "/");

                        //validamos q exista la carpeta si no la creamos
                        if (!System.IO.Directory.Exists(Ruta_Destino))
                        {
                            System.IO.Directory.CreateDirectory(Ruta_Destino);
                        }

                        if (System.IO.File.Exists(Ruta_Destino + Nombre_Archivo))
                        {
                            Lbl_Error.Text = "Ya exixte un documento con este nombre, favor de cambiar el nombre";
                            Mostrar_Ocultar_Error(true);
                        }
                        else
                        {
                            Dt_Archivos.Columns.Add("Archivo", typeof(System.String));
                            Dt_Archivos.Columns.Add("Extension", typeof(System.String));
                            Dt_Archivos.Columns.Add("Ruta", typeof(System.String));

                            Fila = Dt_Archivos.NewRow();
                            Fila["Archivo"] = Nombre_Archivo.Trim();
                            Fila["Extension"] = Extension.Trim();
                            Fila["Ruta"] = Ruta_Destino;
                            Dt_Archivos.Rows.Add(Fila);


                            AFU_Archivo.SaveAs(Ruta_Destino + Nombre_Archivo);
                            HttpContext.Current.Session["Nombre_Archivo_Excel"] = Dt_Archivos;
                        }
                    }
                    else
                    {
                        Lbl_Error.Text = "Solo se pueden cargar archivos de Excel con extensión xls o xslx";
                        Mostrar_Ocultar_Error(true);
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - " + ex.Message.ToString() + " <br />";
                Mostrar_Ocultar_Error(true);
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : AFU_Archivo_Excel_UploadedComplete
        ///DESCRIPCIÓN          : Metodo para agrupar los datos de las clases, conceptos y subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Agrupar_CRI(DataTable Dt_Detalles) 
        {
            DataTable Dt_Agrupado = new DataTable();
            String Clase_Ing_ID = String.Empty;
            String Concepto_Ing_ID = String.Empty;
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Tot = 0.00;
            DataRow Fila;

            try
            {
                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)
                    {
                        //ordenamos el datatable de los detalles
                        var query = from Det in Dt_Detalles.AsEnumerable()
                                    orderby Det.Field<String>("CLAVE_NOMBRE_CONCEPTOS")
                                    select Det;

                        Dt_Detalles = query.CopyToDataTable();

                        Dt_Agrupado.Columns.Add("RUBRO_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("FUENTE_FINANCIAMIENTO_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("TIPO_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("CLASE_ING_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("CONCEPTO_ING_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("SUBCONCEPTO_ING_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("CLAVE_NOMBRE_CONCEPTOS", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("CLAVE_NOMBRE_PROGRAMAS", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("ENERO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("MARZO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("MAYO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("JULIO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
                        Dt_Agrupado.Columns.Add("TIPO_CRI", System.Type.GetType("System.String"));


                        //obtenemos la clase y el concepto
                        Clase_Ing_ID = Dt_Detalles.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                        Concepto_Ing_ID = Dt_Detalles.Rows[0]["CONCEPTO_ING_ID"].ToString().Trim();

                        #region (Registro Clase)
                        //CREAMOS EL REGISTRO DE LA CLASE
                        Ene = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("ENERO")).Sum();

                        Feb = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("FEBRERO")).Sum();

                        Mar = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("MARZO")).Sum();

                        Abr = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("ABRIL")).Sum();

                        May = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("MAYO")).Sum();

                        Jun = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("JUNIO")).Sum();

                        Jul = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("JULIO")).Sum();

                        Ago = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("AGOSTO")).Sum();

                        Sep = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                        Oct = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("OCTUBRE")).Sum();

                        Nov = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("NOVIEMBRE")).Sum();

                        Dic = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("DICIEMBRE")).Sum();

                        Tot = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               select Sum.Field<Double>("IMPORTE_TOTAL")).Sum();

                        Fila = Dt_Agrupado.NewRow();
                        Fila["RUBRO_ID"] = Dt_Detalles.Rows[0]["RUBRO_ID"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Dt_Detalles.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Fila["TIPO_ID"] = Dt_Detalles.Rows[0]["TIPO_ID"].ToString().Trim();
                        Fila["CLASE_ING_ID"] = Dt_Detalles.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                        Fila["CONCEPTO_ING_ID"] = String.Empty;
                        Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                        Fila["JUSTIFICACION"] = string.Empty;
                        Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dt_Detalles.Rows[0]["CLAVE_NOMBRE_CLASE"].ToString().Trim();
                        Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dt_Detalles.Rows[0]["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Ene);
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Feb);
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Mar);
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Abr);
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", May);
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Jun);
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Jul);
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Ago);
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Sep);
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Oct);
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Nov);
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dic);
                        Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Tot);
                        Fila["ID"] = String.Empty;
                        Fila["PROGRAMA_ID"] = Dt_Detalles.Rows[0]["PROGRAMA_ID"].ToString().Trim();
                        Fila["TIPO_CRI"] = "CLASE";
                        Dt_Agrupado.Rows.Add(Fila);
                        #endregion

                        #region (Registro Concepto)
                        //CREAMOS EL REGISTRO DEL CONCEPTO
                        Ene = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("ENERO")).Sum();

                        Feb = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("FEBRERO")).Sum();

                        Mar = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("MARZO")).Sum();

                        Abr = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("ABRIL")).Sum();

                        May = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("MAYO")).Sum();

                        Jun = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("JUNIO")).Sum();

                        Jul = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("JULIO")).Sum();

                        Ago = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("AGOSTO")).Sum();

                        Sep = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                        Oct = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("OCTUBRE")).Sum();

                        Nov = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("NOVIEMBRE")).Sum();

                        Dic = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("DICIEMBRE")).Sum();

                        Tot = (from Sum in Dt_Detalles.AsEnumerable()
                               where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                               && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                               select Sum.Field<Double>("IMPORTE_TOTAL")).Sum();

                        Fila = Dt_Agrupado.NewRow();
                        Fila["RUBRO_ID"] = Dt_Detalles.Rows[0]["RUBRO_ID"].ToString().Trim();
                        Fila["FUENTE_FINANCIAMIENTO_ID"] = Dt_Detalles.Rows[0]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                        Fila["TIPO_ID"] = Dt_Detalles.Rows[0]["TIPO_ID"].ToString().Trim();
                        Fila["CLASE_ING_ID"] = Dt_Detalles.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                        Fila["CONCEPTO_ING_ID"] = Dt_Detalles.Rows[0]["CONCEPTO_ING_ID"].ToString().Trim();
                        Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                        Fila["JUSTIFICACION"] = string.Empty;
                        Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dt_Detalles.Rows[0]["CLAVE_NOMBRE_CONCEPTO_ING"].ToString().Trim();
                        Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dt_Detalles.Rows[0]["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Ene);
                        Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Feb);
                        Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Mar);
                        Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Abr);
                        Fila["MAYO"] = String.Format("{0:#,###,##0.00}", May);
                        Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Jun);
                        Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Jul);
                        Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Ago);
                        Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Sep);
                        Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Oct);
                        Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Nov);
                        Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dic);
                        Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Tot);
                        Fila["ID"] = String.Empty;
                        Fila["PROGRAMA_ID"] = Dt_Detalles.Rows[0]["PROGRAMA_ID"].ToString().Trim();
                        Fila["TIPO_CRI"] = "CONCEPTO";
                        Dt_Agrupado.Rows.Add(Fila);
                        #endregion

                        foreach (DataRow Dr in Dt_Detalles.Rows)
                        {
                            if (Dr["CLASE_ING_ID"].ToString().Trim().Equals(Clase_Ing_ID))
                            {
                                if (Dr["CONCEPTO_ING_ID"].ToString().Trim().Equals(Concepto_Ing_ID))
                                {
                                    //insertamos el subconcepto
                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                    Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                    Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                    Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                    Fila["SUBCONCEPTO_ING_ID"] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                    Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CONCEPTOS"].ToString().Trim();
                                    Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString()) ? "0" : Dr["ENERO"]));
                                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString()) ? "0" : Dr["FEBRERO"]));
                                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString()) ? "0" : Dr["MARZO"]));
                                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString()) ? "0" : Dr["ABRIL"]));
                                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString()) ? "0" : Dr["MAYO"]));
                                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString()) ? "0" : Dr["JUNIO"]));
                                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString()) ? "0" : Dr["JULIO"]));
                                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString()) ? "0" : Dr["AGOSTO"]));
                                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString()) ? "0" : Dr["SEPTIEMBRE"]));
                                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString()) ? "0" : Dr["OCTUBRE"]));
                                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString()) ? "0" : Dr["NOVIEMBRE"]));
                                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString()) ? "0" : Dr["DICIEMBRE"]));
                                    Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"]));
                                    Fila["ID"] = Dr["ID"].ToString().Trim();
                                    Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                    Fila["TIPO_CRI"] = "SUBCONCEPTO";
                                    Dt_Agrupado.Rows.Add(Fila);
                                }
                                else //else concepto 
                                {
                                    Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();

                                    #region (Registro Concepto)
                                    //CREAMOS EL REGISTRO DEL CONCEPTO
                                    Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("ENERO")).Sum();

                                    Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("FEBRERO")).Sum();

                                    Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("MARZO")).Sum();

                                    Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("ABRIL")).Sum();

                                    May = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("MAYO")).Sum();

                                    Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("JUNIO")).Sum();

                                    Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("JULIO")).Sum();

                                    Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("AGOSTO")).Sum();

                                    Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                    Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("OCTUBRE")).Sum();

                                    Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                    Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("DICIEMBRE")).Sum();

                                    Tot = (from Sum in Dt_Detalles.AsEnumerable()
                                           where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                           && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                           select Sum.Field<Double>("IMPORTE_TOTAL")).Sum();

                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                    Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                    Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                    Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                    Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                                    Fila["JUSTIFICACION"] = string.Empty;
                                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CONCEPTO_ING"].ToString().Trim();
                                    Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Ene);
                                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Feb);
                                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Mar);
                                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Abr);
                                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}", May);
                                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Jun);
                                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Jul);
                                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Ago);
                                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Sep);
                                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Oct);
                                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Nov);
                                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dic);
                                    Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Tot);
                                    Fila["ID"] = String.Empty;
                                    Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                    Fila["TIPO_CRI"] = "CONCEPTO";
                                    Dt_Agrupado.Rows.Add(Fila);
                                    #endregion

                                    //insertamos el subconcepto
                                    Fila = Dt_Agrupado.NewRow();
                                    Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                    Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                    Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                    Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                    Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                    Fila["SUBCONCEPTO_ING_ID"] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                    Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                    Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CONCEPTOS"].ToString().Trim();
                                    Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString()) ? "0" : Dr["ENERO"]));
                                    Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString()) ? "0" : Dr["FEBRERO"]));
                                    Fila["MARZO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString()) ? "0" : Dr["MARZO"]));
                                    Fila["ABRIL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString()) ? "0" : Dr["ABRIL"]));
                                    Fila["MAYO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString()) ? "0" : Dr["MAYO"]));
                                    Fila["JUNIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString()) ? "0" : Dr["JUNIO"]));
                                    Fila["JULIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString()) ? "0" : Dr["JULIO"]));
                                    Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString()) ? "0" : Dr["AGOSTO"]));
                                    Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString()) ? "0" : Dr["SEPTIEMBRE"]));
                                    Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString()) ? "0" : Dr["OCTUBRE"]));
                                    Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString()) ? "0" : Dr["NOVIEMBRE"]));
                                    Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString()) ? "0" : Dr["DICIEMBRE"]));
                                    Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"]));
                                    Fila["ID"] = Dr["ID"].ToString().Trim();
                                    Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                    Fila["TIPO_CRI"] = "SUBCONCEPTO";
                                    Dt_Agrupado.Rows.Add(Fila);
                                }
                            }
                            else //else clase 
                            {
                                Concepto_Ing_ID = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Clase_Ing_ID = Dr["CLASE_ING_ID"].ToString().Trim();

                                #region (Registro Clase)
                                //CREAMOS EL REGISTRO DE LA CLASE
                                Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("ENERO")).Sum();

                                Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("FEBRERO")).Sum();

                                Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("MARZO")).Sum();

                                Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("ABRIL")).Sum();

                                May = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("MAYO")).Sum();

                                Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("JUNIO")).Sum();

                                Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("JULIO")).Sum();

                                Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("AGOSTO")).Sum();

                                Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("OCTUBRE")).Sum();

                                Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("DICIEMBRE")).Sum();

                                Tot = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       select Sum.Field<Double>("IMPORTE_TOTAL")).Sum();

                                Fila = Dt_Agrupado.NewRow();
                                Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                Fila["CONCEPTO_ING_ID"] = String.Empty;
                                Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                                Fila["JUSTIFICACION"] = string.Empty;
                                Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CLASE"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Ene);
                                Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Feb);
                                Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Mar);
                                Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Abr);
                                Fila["MAYO"] = String.Format("{0:#,###,##0.00}", May);
                                Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Jun);
                                Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Jul);
                                Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Ago);
                                Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Sep);
                                Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Oct);
                                Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Nov);
                                Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dic);
                                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Tot);
                                Fila["ID"] = String.Empty;
                                Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                Fila["TIPO_CRI"] = "CLASE";
                                Dt_Agrupado.Rows.Add(Fila);
                                #endregion

                                #region (Registro Concepto)
                                //CREAMOS EL REGISTRO DEL CONCEPTO
                                Ene = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("ENERO")).Sum();

                                Feb = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("FEBRERO")).Sum();

                                Mar = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("MARZO")).Sum();

                                Abr = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("ABRIL")).Sum();

                                May = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("MAYO")).Sum();

                                Jun = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("JUNIO")).Sum();

                                Jul = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("JULIO")).Sum();

                                Ago = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("AGOSTO")).Sum();

                                Sep = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("SEPTIEMBRE")).Sum();

                                Oct = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("OCTUBRE")).Sum();

                                Nov = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("NOVIEMBRE")).Sum();

                                Dic = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("DICIEMBRE")).Sum();

                                Tot = (from Sum in Dt_Detalles.AsEnumerable()
                                       where Sum.Field<String>("CLASE_ING_ID").Trim() == Clase_Ing_ID.Trim()
                                       && Sum.Field<String>("CONCEPTO_ING_ID").Trim() == Concepto_Ing_ID.Trim()
                                       select Sum.Field<Double>("IMPORTE_TOTAL")).Sum();

                                Fila = Dt_Agrupado.NewRow();
                                Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Fila["SUBCONCEPTO_ING_ID"] = String.Empty;
                                Fila["JUSTIFICACION"] = string.Empty;
                                Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CONCEPTO_ING"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:#,###,##0.00}", Ene);
                                Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}", Feb);
                                Fila["MARZO"] = String.Format("{0:#,###,##0.00}", Mar);
                                Fila["ABRIL"] = String.Format("{0:#,###,##0.00}", Abr);
                                Fila["MAYO"] = String.Format("{0:#,###,##0.00}", May);
                                Fila["JUNIO"] = String.Format("{0:#,###,##0.00}", Jun);
                                Fila["JULIO"] = String.Format("{0:#,###,##0.00}", Jul);
                                Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}", Ago);
                                Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Sep);
                                Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}", Oct);
                                Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Nov);
                                Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Dic);
                                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Tot);
                                Fila["ID"] = String.Empty;
                                Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                Fila["TIPO_CRI"] = "CONCEPTO";
                                Dt_Agrupado.Rows.Add(Fila);
                                #endregion

                                //insertamos el subconcepto
                                Fila = Dt_Agrupado.NewRow();
                                Fila["RUBRO_ID"] = Dr["RUBRO_ID"].ToString().Trim();
                                Fila["FUENTE_FINANCIAMIENTO_ID"] = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Fila["TIPO_ID"] = Dr["TIPO_ID"].ToString().Trim();
                                Fila["CLASE_ING_ID"] = Dr["CLASE_ING_ID"].ToString().Trim();
                                Fila["CONCEPTO_ING_ID"] = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Fila["SUBCONCEPTO_ING_ID"] = Dr["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                Fila["JUSTIFICACION"] = Dr["JUSTIFICACION"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_CONCEPTOS"] = Dr["CLAVE_NOMBRE_CONCEPTOS"].ToString().Trim();
                                Fila["CLAVE_NOMBRE_PROGRAMAS"] = Dr["CLAVE_NOMBRE_PROGRAMAS"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ENERO"].ToString()) ? "0" : Dr["ENERO"]));
                                Fila["FEBRERO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["FEBRERO"].ToString()) ? "0" : Dr["FEBRERO"]));
                                Fila["MARZO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MARZO"].ToString()) ? "0" : Dr["MARZO"]));
                                Fila["ABRIL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["ABRIL"].ToString()) ? "0" : Dr["ABRIL"]));
                                Fila["MAYO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["MAYO"].ToString()) ? "0" : Dr["MAYO"]));
                                Fila["JUNIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JUNIO"].ToString()) ? "0" : Dr["JUNIO"]));
                                Fila["JULIO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["JULIO"].ToString()) ? "0" : Dr["JULIO"]));
                                Fila["AGOSTO"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["AGOSTO"].ToString()) ? "0" : Dr["AGOSTO"]));
                                Fila["SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["SEPTIEMBRE"].ToString()) ? "0" : Dr["SEPTIEMBRE"]));
                                Fila["OCTUBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["OCTUBRE"].ToString()) ? "0" : Dr["OCTUBRE"]));
                                Fila["NOVIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["NOVIEMBRE"].ToString()) ? "0" : Dr["NOVIEMBRE"]));
                                Fila["DICIEMBRE"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["DICIEMBRE"].ToString()) ? "0" : Dr["DICIEMBRE"]));
                                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}",Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString()) ? "0" : Dr["IMPORTE_TOTAL"]));
                                Fila["ID"] = Dr["ID"].ToString().Trim();
                                Fila["PROGRAMA_ID"] = Dr["PROGRAMA_ID"].ToString().Trim();
                                Fila["TIPO_CRI"] = "SUBCONCEPTO";
                                Dt_Agrupado.Rows.Add(Fila);
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al agrupar los datos del CRI. Error[" + Ex.Message + "]");
            }
            return Dt_Agrupado;
        }
    #endregion

    #region (Metodos Combos)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Conceptos
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Conceptos()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Conceptos = new DataTable();

            try
            {
                Negocio.P_Descripcion = String.Empty;
                Negocio.P_Anio = String.Empty;
                //if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                //{
                //    Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                //}
                //else 
                //{
                //    Negocio.P_Anio = String.Empty;
                //}

                //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                //{
                //    Negocio.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                //}
                //else 
                //{
                    Negocio.P_Programa_ID = String.Empty;
                //}


                if (!String.IsNullOrEmpty(Cmb_Rubro.SelectedItem.Value))
                {
                    Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Rubro_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Tipo.SelectedItem.Value))
                {
                    Negocio.P_Tipo_ID = Cmb_Tipo.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Tipo_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Clase.SelectedItem.Value))
                {
                    Negocio.P_Clase_Ing_ID = Cmb_Clase.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Clase_Ing_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Txt_Busqueda_Concepto.Text.Trim()))
                {
                    Negocio.P_Busqueda = Txt_Busqueda_Concepto.Text.Trim();
                }
                else 
                {
                    Negocio.P_Busqueda = String.Empty;
                }

                Negocio.P_Concepto_Ing_ID = String.Empty;

                Dt_Conceptos = Negocio.Consultar_Concepto_Ing();
                Cmb_Concepto.Items.Clear();
                
                if (Dt_Conceptos != null)
                {
                    if (Dt_Conceptos.Rows.Count > 0)
                    {
                        Cmb_Concepto.DataValueField = Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID;
                        Cmb_Concepto.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Concepto.DataSource = Dt_Conceptos;
                        Cmb_Concepto.DataBind();
                    }
                    else
                    {
                        Cmb_Concepto.Enabled = false;
                    }
                }
                else 
                {
                    Cmb_Concepto.Enabled = false;
                }
                Cmb_Concepto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Conceptos ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_SubConceptos
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los Subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 8/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_SubConceptos()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Conceptos = new DataTable();

            try
            {
                Negocio.P_Descripcion = String.Empty;
                //if (!String.IsNullOrEmpty(Cmb_Anio.SelectedItem.Value.Trim()))
                //{
                //    Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                //}
                //else
                //{
                //    Negocio.P_Anio = String.Empty;
                //}


                if (!String.IsNullOrEmpty(Cmb_Rubro.SelectedItem.Value))
                {
                    Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Rubro_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Tipo.SelectedItem.Value))
                {
                    Negocio.P_Tipo_ID = Cmb_Tipo.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Tipo_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Clase.SelectedItem.Value))
                {
                    Negocio.P_Clase_Ing_ID = Cmb_Clase.SelectedItem.Value;
                }
                else
                {
                    Negocio.P_Clase_Ing_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Concepto.SelectedItem.Value))
                {
                    Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Concepto_Ing_ID = String.Empty;
                }

                Negocio.P_Anio = String.Empty;
                Dt_Conceptos = Negocio.Consultar_SubConcepto_Ing();

                Cmb_SubConcepto.Items.Clear();

                if (Dt_Conceptos != null)
                {
                    if (Dt_Conceptos.Rows.Count > 0)
                    {
                        Cmb_SubConcepto.DataValueField = Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID;
                        Cmb_SubConcepto.DataTextField = "CLAVE_NOMBRE";
                        Cmb_SubConcepto.DataSource = Dt_Conceptos;
                        Cmb_SubConcepto.DataBind();
                        Tr_SubConceptos.Visible = true;
                        Cmb_SubConcepto.Enabled = true;
                    }
                    else
                    {
                        Tr_SubConceptos.Visible = false;
                        Cmb_SubConcepto.Enabled = false;
                    }
                }
                else
                {
                    Tr_SubConceptos.Visible = false;
                    Cmb_SubConcepto.Enabled = false;
                }
                Cmb_SubConcepto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Conceptos ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
        ///DESCRIPCIÓN          : Metodo para llenar el combo del estatus del presupuesto
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Estatus()
        {
            try
            {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("EN CONSTRUCCION", "EN CONSTRUCCION"));
                Cmb_Estatus.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));
                Cmb_Estatus.Items.Insert(3, new ListItem("AUTORIZADO", "AUTORIZADO"));

                Cmb_Estatus_P.Items.Clear();
                Cmb_Estatus_P.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                Cmb_Estatus_P.Items.Insert(1, new ListItem("EN CONSTRUCCION", "EN CONSTRUCCION"));
                Cmb_Estatus_P.Items.Insert(2, new ListItem("GENERADO", "GENERADO"));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Partidas ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anio
        ///DESCRIPCIÓN          : Metodo para llenar el combo de año
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Anio()
        {
            Int32 Anio = Convert.ToInt32(String.Format("{0:yyyy}", DateTime.Now));
            Int32 Anio_Dos = Anio + 1;

            try
            {
                Cmb_Anio.Items.Clear();
                Cmb_Anio.Items.Insert(0, new ListItem(String.Format("{0:yyyy}", DateTime.Now), String.Format("{0:yyyy}", DateTime.Now)));
                Cmb_Anio.Items.Insert(1, new ListItem(Anio_Dos.ToString().Trim(), Anio_Dos.ToString().Trim()));

                Cmb_Anio_P.Items.Clear();
                Cmb_Anio_P.Items.Insert(0, new ListItem(String.Format("{0:yyyy}", DateTime.Now), String.Format("{0:yyyy}", DateTime.Now)));
                Cmb_Anio_P.Items.Insert(1, new ListItem(Anio_Dos.ToString().Trim(), Anio_Dos.ToString().Trim()));
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Anio ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fte_Financiamiento
        ///DESCRIPCIÓN          : Metodo para llenar el combo de fuente de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Abril/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Fte_Financiamiento()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Fte_Financiamiento = new DataTable();

            try
            {
                Dt_Fte_Financiamiento = Ingresos_Negocio.Consulta_Fte_Financiamiento();

                Cmb_Fuente_Financiamientos.Items.Clear();

                if (Dt_Fte_Financiamiento != null)
                {
                    if (Dt_Fte_Financiamiento.Rows.Count > 0)
                    {
                        Cmb_Fuente_Financiamientos.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                        Cmb_Fuente_Financiamientos.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Fuente_Financiamientos.DataSource = Dt_Fte_Financiamiento;
                        Cmb_Fuente_Financiamientos.DataBind();
                        Cmb_Fuente_Financiamientos.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        ////Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Fte_Financiamiento ERROR[" + ex.Message + "]");
            }
        }

       
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Rubro
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los rubros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Rubro()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt = new DataTable();

            try
            {
                Dt = Ingresos_Negocio.Consultar_Rubros();
                Cmb_Rubro.Items.Clear();

                if (Dt != null)
                {
                    if (Dt.Rows.Count > 0)
                    {
                        Cmb_Rubro.DataValueField = Cat_Psp_Rubro.Campo_Rubro_ID;
                        Cmb_Rubro.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Rubro.DataSource = Dt;
                        Cmb_Rubro.DataBind();
                        Cmb_Rubro.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Rubro ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los tipos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Tipo()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Rubro.SelectedItem.Value))
                {
                    Ingresos_Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value;
                }
                else 
                {
                    Ingresos_Negocio.P_Rubro_ID = String.Empty;
                }

                Dt = Ingresos_Negocio.Consultar_Tipos();
                Cmb_Tipo.Items.Clear();

                if (Dt != null)
                {
                    if (Dt.Rows.Count > 0)
                    {
                        Cmb_Tipo.DataValueField = Cat_Psp_Tipo.Campo_Tipo_ID ;
                        Cmb_Tipo.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Tipo.DataSource = Dt;
                        Cmb_Tipo.DataBind();
                        Cmb_Tipo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Tipo ERROR[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Clase
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las clases de ingresos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Llenar_Combo_Clase()
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Cmb_Rubro.SelectedItem.Value))
                {
                    Ingresos_Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value;
                }
                else
                {
                    Ingresos_Negocio.P_Rubro_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Tipo.SelectedItem.Value))
                {
                    Ingresos_Negocio.P_Tipo_ID = Cmb_Tipo.SelectedItem.Value;
                }
                else
                {
                    Ingresos_Negocio.P_Tipo_ID = String.Empty;
                }

                Dt = Ingresos_Negocio.Consultar_Clase_Ing();
                Cmb_Clase.Items.Clear();

                if (Dt != null)
                {
                    if (Dt.Rows.Count > 0)
                    {
                        Cmb_Clase.DataValueField = Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID;
                        Cmb_Clase.DataTextField = "CLAVE_NOMBRE";
                        Cmb_Clase.DataSource = Dt;
                        Cmb_Clase.DataBind();
                        Cmb_Clase.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Llenar_Combo_Clase ERROR[" + ex.Message + "]");
            }
        }
        

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programa
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Julio/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        //private void Llenar_Combo_Programa()
        //{
        //    Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
        //    DataTable Dt = new DataTable();

        //    try
        //    {
        //        Cmb_Programa.Items.Clear();

        //        if (Cmb_Fuente_Financiamientos.SelectedIndex > 0)
        //        {
        //            Negocio.P_Fte_Financiamiento = Cmb_Fuente_Financiamientos.SelectedItem.Value.Trim();
        //        }
        //        else
        //        {
        //            Negocio.P_Fte_Financiamiento = String.Empty;
        //        }


        //        Negocio.P_Busqueda = String.Empty;
        //        Negocio.P_Concepto_ID = String.Empty;
        //        Negocio.P_Programa_ID = String.Empty;
        //        Dt = Negocio.Consultar_Programas();
        //        if (Dt != null && Dt.Rows.Count > 0)
        //        {
        //            Cmb_Programa.DataTextField = "CLAVE_NOMBRE";
        //            Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
        //            Cmb_Programa.DataSource = Dt;
        //            Cmb_Programa.DataBind();
        //            Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        //            Cmb_Programa.SelectedIndex = -1;

        //            Cmb_Programa.Enabled = true;
        //        }
        //        else
        //        {
        //            Cmb_Programa.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        //        }
        //    }
        //    catch (Exception Ex)
        //    {
        //        Lbl_Encanezado_Error.Text = "Error al cargar el combo de Programas. Error[" + Ex.Message + "]";
        //        Lbl_Error.Text = String.Empty;
        //        Mostrar_Ocultar_Error(true);
        //    }
        //}
    #endregion

    #endregion

    #region EVENTOS

    #region EVENTOS GENERALES
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Evento del boton de salir
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            switch (Btn_Salir.ToolTip)
            {
                case "Cancelar":
                    Limpiar_Sessiones();
                    Pronostico_Ingresos_Inicio();
                    break;

                case "Salir":
                    Limpiar_Sessiones();
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "window.close();", true);
                    break;
            }//fin del switch
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Evento del boton nuevo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Nuevo_Click(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            Mostrar_Ocultar_Error(false);
            DataTable Dt_Registros = new DataTable();
            DataTable Dt_Registos = new DataTable();
            try
            {
                switch (Btn_Nuevo.ToolTip)
                {
                    case "Nuevo":
                         Session["Dt_Registros"] = null;

                        Llenar_Grid_Registros();
                        Dt_Registos = (DataTable)Grid_Registros.DataSource;

                        if (Dt_Registos != null)
                        {
                            if (Dt_Registos.Rows.Count > 0)
                            {
                                Lbl_Encanezado_Error.Text = "No se puede hacer de nuevo el pronostico de este año, pase a la opción de modificar o seleccione otro año";
                                Mostrar_Ocultar_Error(true);
                            }
                            else
                            {
                                Estado_Botones("nuevo");
                                Limpiar_Formulario("Todo");
                                Habilitar_Forma(true);
                            }
                        }
                        else 
                        {
                            Estado_Botones("nuevo");
                            Limpiar_Formulario("Todo");
                            Habilitar_Forma(true);
                        }

                        break;
                    case "Guardar":
                        Dt_Registros = (DataTable)Session["Dt_Registros"];
                        if (Dt_Registros != null)
                        {
                            if (Dt_Registros.Rows.Count > 0)
                            {

                                if (Cmb_Estatus.SelectedIndex <= 0)
                                {
                                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un estatus. <br />";
                                    Mostrar_Ocultar_Error(true);
                                }
                                else
                                {
                                    Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                                    Negocio.P_Dt_Datos = Dt_Registros;
                                    Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                                    Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                                    Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();

                                    if (Negocio.Guardar_Registros())
                                    {
                                        Limpiar_Sessiones();
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Alta Exitosa');", true);
                                        Pronostico_Ingresos_Inicio();
                                    }
                                }
                            }
                            else
                            {
                                Lbl_Error.Text = " &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Agregar registros ";
                                Mostrar_Ocultar_Error(true);
                            }

                        }
                        else
                        {
                            Lbl_Error.Text = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Agregar registros ";
                            Mostrar_Ocultar_Error(true);
                        }
                        break;
                }//fin del swirch
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de dar de alta los datos ERROR[" + ex.Message + "]");
            }
        }//fin del boton Nuevo

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Evento del boton modificar
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            Mostrar_Ocultar_Error(false);
            DataTable Dt_Registros = new DataTable();
            try
            {
                switch (Btn_Modificar.ToolTip)
                {
                    //Validacion para actualizar un registro y para habilitar los controles que se requieran
                    case "Modificar":

                        if (Cmb_Estatus.SelectedItem.Text.Trim().Equals("AUTORIZADO"))
                        {
                            Estado_Botones("inicial");
                            Lbl_Encanezado_Error.Text = "No se puede modificar el pronostico por que ya fue autorizado";
                            Mostrar_Ocultar_Error(true);
                        }
                        else 
                        {
                            Estado_Botones("modificar");
                            Habilitar_Forma(true);
                            Grid_Registros.SelectedIndex = -1;
                            Limpiar_Formulario("Todo");
                            Llenar_Grid_Registros();
                        }
                        break;
                    case "Actualizar":
                        Dt_Registros = (DataTable)Session["Dt_Registros"];
                        if (Dt_Registros != null)
                        {
                            if (Dt_Registros.Rows.Count > 0)
                            {
                                if (Cmb_Estatus.SelectedIndex <= 0)
                                {
                                    Lbl_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un estatus. <br />";
                                    Mostrar_Ocultar_Error(true);
                                }
                                else
                                {
                                    Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                                    Negocio.P_Dt_Datos = Dt_Registros;
                                    Negocio.P_Total = Txt_Total_Ajuste.Text.Trim().Replace("$", "");
                                    Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();

                                    if (Negocio.Modificar_Registros())
                                    {
                                        Limpiar_Sessiones();
                                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Modificación Exitosa');", true);
                                        Pronostico_Ingresos_Inicio();
                                    }
                                }
                            }
                            else
                            {
                                Lbl_Error.Text = " Agregar registros para el pronostico de ingresos.";
                                Mostrar_Ocultar_Error(true);
                            }
                        }
                        else
                        {
                            Lbl_Error.Text = " Agregar registros para el pronostico de ingresos ";
                            Mostrar_Ocultar_Error(true);
                        }
                        break;
                }//fin del switch
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de modificar los datos ERROR[" + ex.Message + "]");
            }
        }//fin de Modificar

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Click
        ///DESCRIPCIÓN          : Evento del boton de agregar un nuevo presupuesto de un producto
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Agregar_Click(object sender, EventArgs e)
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = (DataTable)Session["Dt_Registros"];
            Mostrar_Ocultar_Error(false);
            try
            {
                if (Validar_Datos())
                {
                    if (Dt_Registros == null)
                    {
                        Crear_Inicio_Dt_Registros();
                        Dt_Registros = (DataTable)Session["Dt_Registros"];
                    }

                    Agregar_Fila_Dt_Registros();
                    Limpiar_Formulario("Datos_Concepto");
                    Llenar_Grid_Registros();
                    Calcular_Total_Presupuestado();
                }
                else
                {
                    Mostrar_Ocultar_Error(true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de agregar un registro a la tabla. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
        ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Eliminar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio_Pro = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt_Registros = new DataTable();
            ImageButton Btn_Eliminar = (ImageButton)sender;
            Int32 No_Fila = -1;
            String Id = String.Empty;
            DataTable Dt = new DataTable();

            try
            {
                Id = Btn_Eliminar.CommandArgument.ToString().Trim();
                Dt_Registros = (DataTable)Session["Dt_Registros"];
                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            No_Fila++;
                            if (Dr["ID"].ToString().Trim().Equals(Id))
                            {
                                Llenar_Combo_Fte_Financiamiento();
                                Cmb_Fuente_Financiamientos.SelectedIndex = Cmb_Fuente_Financiamientos.Items.IndexOf(Cmb_Fuente_Financiamientos.Items.FindByValue(Dt_Registros.Rows[No_Fila]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim()));
                                //Llenar_Combo_Programa();
                                //Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Registros.Rows[No_Fila]["PROGRAMA_ID"].ToString().Trim()));
                                Hf_Rubro_ID.Value = Dt_Registros.Rows[No_Fila]["RUBRO_ID"].ToString().Trim();
                                Hf_Tipo_ID.Value = Dt_Registros.Rows[No_Fila]["TIPO_ID"].ToString().Trim();
                                Hf_Clase_ID.Value = Dt_Registros.Rows[No_Fila]["CLASE_ING_ID"].ToString().Trim();
                                Hf_Concepto_ID.Value = Dt_Registros.Rows[No_Fila]["CONCEPTO_ING_ID"].ToString().Trim();
                                Hf_SubConcepto_ID.Value = Dt_Registros.Rows[No_Fila]["SUBCONCEPTO_ING_ID"].ToString().Trim();
                                Hf_Programa.Value = Dt_Registros.Rows[No_Fila]["PROGRAMA_ID"].ToString().Trim();

                                Llenar_Combo_Rubro();
                                Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(Dt_Registros.Rows[No_Fila]["RUBRO_ID"].ToString().Trim()));
                                Llenar_Combo_Tipo();
                                Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Dt_Registros.Rows[No_Fila]["TIPO_ID"].ToString().Trim()));
                                Llenar_Combo_Clase();
                                Cmb_Clase.SelectedIndex = Cmb_Clase.Items.IndexOf(Cmb_Clase.Items.FindByValue(Dt_Registros.Rows[No_Fila]["CLASE_ING_ID"].ToString().Trim()));
                                Llenar_Combo_Conceptos();
                                Cmb_Concepto.SelectedIndex = Cmb_Concepto.Items.IndexOf(Cmb_Concepto.Items.FindByValue(Dt_Registros.Rows[No_Fila]["CONCEPTO_ING_ID"].ToString().Trim()));

                                if (Cmb_Concepto.SelectedIndex > -1)
                                {
                                    Cmb_SubConcepto.Enabled = true;
                                    Llenar_Combo_SubConceptos();
                                    Cmb_SubConcepto.SelectedIndex = Cmb_SubConcepto.Items.IndexOf(Cmb_SubConcepto.Items.FindByValue(Dt_Registros.Rows[No_Fila]["SUBCONCEPTO_ING_ID"].ToString().Trim()));
                                    Cmb_SubConcepto.Enabled = true;
                                    Tr_SubConceptos.Visible = true;
                                }
                                else
                                {
                                    Tr_SubConceptos.Visible = false;
                                }

                                Txt_Justificacion.Text = Dt_Registros.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();

                                Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["ENERO"]);
                                Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["FEBRERO"]);
                                Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["MARZO"]);
                                Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["ABRIL"]);
                                Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["MAYO"]);
                                Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["JUNIO"]);
                                Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["JULIO"]);
                                Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["AGOSTO"]);
                                Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["SEPTIEMBRE"]);
                                Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["OCTUBRE"]);
                                Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["NOVIEMBRE"]);
                                Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["DICIEMBRE"]);
                                Txt_Total.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["IMPORTE_TOTAL"]);

                                //obtenemos el importe si es que se eligio un programa
                                //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim()))
                                //{
                                //    if (Cmb_Concepto.SelectedIndex > 0)
                                //    {
                                //        Negocio_Pro.P_Descripcion = String.Empty;
                                //        Negocio_Pro.P_Anio = String.Empty;
                                //        Negocio_Pro.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                                //        Negocio_Pro.P_Busqueda = String.Empty;
                                //        Negocio_Pro.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                                //        Dt = Negocio_Pro.Consultar_Concepto_Ing();

                                //        if (Dt != null && Dt.Rows.Count > 0)
                                //        {
                                //            Hf_Importe_Programa.Value = Dt.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe].ToString().Trim();
                                //        }
                                //    }
                                //}


                                Dt_Registros.Rows.RemoveAt(No_Fila);
                                Session["Dt_Registros"] = Dt_Registros;

                                if (Dt_Registros.Rows.Count > 0)
                                {

                                    Obtener_Consecutivo_ID();
                                    Dt_Registros = Crear_Dt_Registros();
                                    Grid_Registros.Columns[1].Visible = true;
                                    Grid_Registros.DataSource = Dt_Registros;
                                    Grid_Registros.DataBind();
                                    Grid_Registros.Columns[1].Visible = false;
                                }
                                else
                                {
                                    Grid_Registros.DataSource = new DataTable();
                                    Grid_Registros.DataBind();
                                }

                                //Llenar_Grid_Registros();
                                Calcular_Total_Presupuestado();
                                break;
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Cargar_Excel_Click
        ///DESCRIPCIÓN          : Evento del boton de cargar excel
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Cargar_Excel_Click(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            Mostrar_Ocultar_Error(false);
            DataTable Dt_Registros = new DataTable();
            String Ruta_Destino = String.Empty;
            String Extension = String.Empty;
            String Archivo = String.Empty;
            DataTable Dt_Registos = new DataTable();
            DataTable Dt_Archivo = new DataTable();

            try
            {
                switch (Btn_Cargar_Excel.ToolTip)
                {
                    //Validacion para actualizar un registro y para habilitar los controles que se requieran
                    case "Cargar Excel":

                        Session["Dt_Registros"] = null;

                        Llenar_Grid_Registros();
                        Dt_Registos = (DataTable)Grid_Registros.DataSource;

                        if (Dt_Registos != null)
                        {
                            if (Dt_Registos.Rows.Count > 0)
                            {
                                Lbl_Encanezado_Error.Text = "No se puede cargar de nuevo el pronostico de este año, pase a la opción de modificar o seleccione otro año";
                                Mostrar_Ocultar_Error(true);
                            }
                            else
                            {
                                Estado_Botones("excel");
                                Habilitar_Forma(false);
                                Grid_Registros.SelectedIndex = -1;
                                Limpiar_Formulario("Todo");
                                Llenar_Grid_Registros();
                                Cmb_Estatus_P.SelectedIndex = Cmb_Estatus_P.Items.IndexOf(Cmb_Estatus_P.Items.FindByValue(Cmb_Estatus.SelectedItem.Value.Trim()));
                                Cmb_Anio_P.SelectedIndex = Cmb_Anio_P.Items.IndexOf(Cmb_Anio_P.Items.FindByValue(Cmb_Anio.SelectedItem.Value.Trim()));
                                Grid_Registros.Enabled = false;
                                Pnl_Datos_Generales.Visible = false;
                                Pnl_Cargar_Excel.Style.Add("display", "block");
                            }
                        }
                        else
                        {
                            Estado_Botones("excel");
                            Habilitar_Forma(false);
                            Grid_Registros.SelectedIndex = -1;
                            Limpiar_Formulario("Todo");
                            Llenar_Grid_Registros();
                            Grid_Registros.Enabled = false;
                            Pnl_Datos_Generales.Visible = false;
                            Pnl_Cargar_Excel.Style.Add("display", "block");
                        }

                        break;
                    case "Cargar":
                        Dt_Archivo = (DataTable)HttpContext.Current.Session["Nombre_Archivo_Excel"];

                        if (Dt_Archivo != null)
                        {
                            if (Dt_Archivo.Rows.Count > 0)
                            {
                                if (Cmb_Estatus_P.SelectedIndex > 0)
                                {
                                    Archivo = Dt_Archivo.Rows[0]["Archivo"].ToString().Trim();
                                    Extension = Dt_Archivo.Rows[0]["Extension"].ToString().Trim();
                                    Ruta_Destino = Dt_Archivo.Rows[0]["Ruta"].ToString().Trim();
                                    Cargar_Excel(Ruta_Destino + Archivo, Extension);
                                }
                                else
                                {
                                    Mostrar_Ocultar_Error(true);
                                    Lbl_Encanezado_Error.Text = "Favor de seleccionar un estatus";
                                    Lbl_Error.Text = String.Empty;
                                }
                            }
                        }
                        break;
                }//fin del switch
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de cargar los datos del pronostico ERROR[" + ex.Message + "]");
            }
        }//fin de Modificar

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_ALimpiar_Click
        ///DESCRIPCIÓN          : Evento del boton de limpiar los campos del presupuesto
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Limpiar_Click(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);
            try
            {
                Limpiar_Formulario("Datos_Concepto");
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de a limpiar los datos del formulario. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Conceptos_Click
        ///DESCRIPCIÓN          : Evento del boton de busqueda de conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Busqueda_Conceptos_Click(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);
            try
            {
                Busqueda_Conceptos(Txt_Busqueda_Concepto.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de conceptos. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_SubConceptos_Click
        ///DESCRIPCIÓN          : Evento del boton de busqueda de subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Busqueda_SubConceptos_Click(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);
            try
            {
                Busqueda_SubConceptos(Txt_Busueda_SubConcepto.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de subconceptos. Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region EVENTOS GRID
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            DataTable Dt_Registros = new DataTable();
            Dt_Registros = (DataTable)Session["Dt_Registros"];

            try
            {
                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Rows.Count > 0)
                    {
                        GridView Grid_Registros_Detalle = (GridView)e.Row.Cells[4].FindControl("Grid_Registros_Detalle");
                        DataTable Dt_Detalles = new DataTable();
                        String Rubro_ID = String.Empty;

                        if (e.Row.RowType == DataControlRowType.DataRow)
                        {
                            Rubro_ID = e.Row.Cells[1].Text.Trim();
                            Dt_Detalles = Crear_Dt_Detalles(Rubro_ID);

                            Dt_Detalles = Agrupar_CRI(Dt_Detalles);

                            Grid_Registros_Detalle.Columns[0].Visible = true;
                            Grid_Registros_Detalle.Columns[1].Visible = true;
                            Grid_Registros_Detalle.Columns[2].Visible = true;
                            Grid_Registros_Detalle.Columns[3].Visible = true;
                            Grid_Registros_Detalle.Columns[4].Visible = true;
                            Grid_Registros_Detalle.Columns[5].Visible = true;
                            Grid_Registros_Detalle.Columns[6].Visible = true;
                            Grid_Registros_Detalle.Columns[7].Visible = true;
                            Grid_Registros_Detalle.Columns[9].Visible = true;
                            Grid_Registros_Detalle.Columns[23].Visible = true;
                            Grid_Registros_Detalle.Columns[24].Visible = true;
                            Grid_Registros_Detalle.Columns[25].Visible = true;
                            Grid_Registros_Detalle.Columns[26].Visible = true;
                            Grid_Registros_Detalle.DataSource = Dt_Detalles;
                            Grid_Registros_Detalle.DataBind();
                            Grid_Registros_Detalle.Columns[1].Visible = false;
                            Grid_Registros_Detalle.Columns[2].Visible = false;
                            Grid_Registros_Detalle.Columns[3].Visible = false;
                            Grid_Registros_Detalle.Columns[4].Visible = false;
                            Grid_Registros_Detalle.Columns[5].Visible = false;
                            Grid_Registros_Detalle.Columns[6].Visible = false;
                            Grid_Registros_Detalle.Columns[7].Visible = false;
                            Grid_Registros_Detalle.Columns[9].Visible = false;/*columna del programa*/
                            Grid_Registros_Detalle.Columns[24].Visible = false;
                            Grid_Registros_Detalle.Columns[25].Visible = false;
                            Grid_Registros_Detalle.Columns[26].Visible = false;
                        
                            if (Btn_Nuevo.ToolTip == "Guardar" || Btn_Modificar.ToolTip == "Actualizar")
                            {
                                Grid_Registros_Detalle.Columns[0].Visible = false;
                            }
                            else
                            {
                                Grid_Registros_Detalle.Columns[23].Visible = false;
                            }
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_Detalle_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_Detalle_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable Dt_Registros = new DataTable();
            Int32 No_Fila = -1;
            String Id = String.Empty;
            try
            {
                Limpiar_Formulario("Todo");

                Id = ((GridView)sender).SelectedRow.Cells[24].Text;
                Dt_Registros = (DataTable)Session["Dt_Registros"];
                if (Dt_Registros != null)
                {
                    if (Dt_Registros.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Registros.Rows)
                        {
                            No_Fila++;
                            if (Dr["ID"].ToString().Trim().Equals(Id))
                            {
                                Llenar_Combo_Fte_Financiamiento();
                                Cmb_Fuente_Financiamientos.SelectedIndex = Cmb_Fuente_Financiamientos.Items.IndexOf(Cmb_Fuente_Financiamientos.Items.FindByValue(Dt_Registros.Rows[No_Fila]["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim()));
                                //Llenar_Combo_Programa();
                                //Cmb_Programa.SelectedIndex = Cmb_Programa.Items.IndexOf(Cmb_Programa.Items.FindByValue(Dt_Registros.Rows[No_Fila]["PROGRAMA_ID"].ToString().Trim()));

                                Llenar_Combo_Rubro();
                                Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(Dt_Registros.Rows[No_Fila]["RUBRO_ID"].ToString().Trim()));
                                Llenar_Combo_Tipo();
                                Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Dt_Registros.Rows[No_Fila]["TIPO_ID"].ToString().Trim()));
                                Llenar_Combo_Clase();
                                Cmb_Clase.SelectedIndex = Cmb_Clase.Items.IndexOf(Cmb_Clase.Items.FindByValue(Dt_Registros.Rows[No_Fila]["CLASE_ING_ID"].ToString().Trim()));
                                Llenar_Combo_Conceptos();
                                Cmb_Concepto.SelectedIndex = Cmb_Concepto.Items.IndexOf(Cmb_Concepto.Items.FindByValue(Dt_Registros.Rows[No_Fila]["CONCEPTO_ING_ID"].ToString().Trim()));
                                Llenar_Combo_SubConceptos();
                                Cmb_SubConcepto.SelectedIndex = Cmb_SubConcepto.Items.IndexOf(Cmb_SubConcepto.Items.FindByValue(Dt_Registros.Rows[No_Fila]["SUBCONCEPTO_ING_ID"].ToString().Trim()));

                                if (Cmb_SubConcepto.SelectedIndex > -1)
                                {
                                    Cmb_Concepto.Enabled = false;
                                    Cmb_SubConcepto.Enabled = false;
                                    Tr_SubConceptos.Visible = true;
                                }
                                else 
                                {
                                    Tr_SubConceptos.Visible = false;
                                }
                                
                                Txt_Justificacion.Text = Dt_Registros.Rows[No_Fila]["JUSTIFICACION"].ToString().Trim();

                                Txt_Enero.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["ENERO"]);
                                Txt_Febrero.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["FEBRERO"]);
                                Txt_Marzo.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["MARZO"]);
                                Txt_Abril.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["ABRIL"]);
                                Txt_Mayo.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["MAYO"]);
                                Txt_Junio.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["JUNIO"]);
                                Txt_Julio.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["JULIO"]);
                                Txt_Agosto.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["AGOSTO"]);
                                Txt_Septiembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["SEPTIEMBRE"]);
                                Txt_Octubre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["OCTUBRE"]);
                                Txt_Noviembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["NOVIEMBRE"]);
                                Txt_Diciembre.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["DICIEMBRE"]);
                                Txt_Total.Text = String.Format("{0:#,###,##0.00}", Dt_Registros.Rows[No_Fila]["IMPORTE_TOTAL"]);
                                //Cmb_Programa.Enabled = false;
                                break;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al seleccionar el registro el grid. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Partidas_Asignadas_Detalle_RowCreated
        ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 22/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_Detalle_RowCreated(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';");
                e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_Detalle_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 10/Junio/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_Detalle_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (e.Row.Cells[26].Text.Trim().Equals("CLASE"))
                    {
                        e.Row.Style.Add("background-color", "#66CCCC");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "";
                        e.Row.Cells[23].Text = "";
                        e.Row.Cells[23].Enabled = false;
                    }

                    if (e.Row.Cells[26].Text.Trim().Equals("CONCEPTO"))
                    {
                        e.Row.Style.Add("background-color", "#CCFF99");
                        e.Row.Style.Add("color", "black");
                        e.Row.Cells[0].Text = "";
                        e.Row.Cells[23].Text = "";
                        e.Row.Cells[23].Enabled = false;
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }
    #endregion

    #region EVENTOS COMBOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Fuente_Financiamientos_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de fuente de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        //protected void Cmb_Fuente_Financiamientos_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    Mostrar_Ocultar_Error(false);
        //    try
        //    {
        //        Llenar_Combo_Programa();
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
        //    }
        //}
        
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Concepto_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Concepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Ope_Psp_Pronostico_Ingresos_Negocio Negocio_Pro = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            DataTable Dt = new DataTable();
            Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();
            DataTable Dt_Datos = new DataTable();
            Mostrar_Ocultar_Error(false);
            try
            {
                Negocio.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                Dt_Datos = Negocio.Consultar_Datos_Completos_Ing();

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    Hf_Rubro_ID.Value = Dt_Datos.Rows[0]["RUBRO_ID"].ToString().Trim();
                    Hf_Tipo_ID.Value = Dt_Datos.Rows[0]["TIPO_ID"].ToString().Trim();
                    Hf_Clase_ID.Value = Dt_Datos.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                    Hf_Concepto_ID.Value = Dt_Datos.Rows[0]["CONCEPTO_ING_ID"].ToString().Trim();

                    Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(Hf_Rubro_ID.Value.Trim()));
                    Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Hf_Tipo_ID.Value.Trim()));
                    Cmb_Clase.SelectedIndex = Cmb_Clase.Items.IndexOf(Cmb_Clase.Items.FindByValue(Hf_Clase_ID.Value.Trim()));

                    //obtenemos el importe si es que se eligio un programa
                    //if (!String.IsNullOrEmpty(Cmb_Programa.SelectedItem.Value.Trim())) 
                    //{
                    //    if (Cmb_Concepto.SelectedIndex > 0)
                    //    {
                    //        Negocio_Pro.P_Descripcion = String.Empty;
                    //        Negocio_Pro.P_Anio = String.Empty;
                    //        Negocio_Pro.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim();
                    //        Negocio_Pro.P_Busqueda = String.Empty;
                    //        Negocio_Pro.P_Concepto_Ing_ID = Cmb_Concepto.SelectedItem.Value.Trim();
                    //        Dt = Negocio_Pro.Consultar_Concepto_Ing();

                    //        if (Dt != null && Dt.Rows.Count > 0)
                    //        {
                    //            Hf_Importe_Programa.Value = Dt.Rows[0][Cat_Sap_Det_Fte_Programa.Campo_Importe].ToString().Trim();
                    //        }
                    //    }
                    //}
                }

                Llenar_Combo_SubConceptos();

                //if (Cmb_Concepto.SelectedIndex > 0)
                //{
                //    Llenar_Combo_SubConceptos();
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de conceptos Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Anio_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de año
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
        {
            DataTable Dt_Registos = new DataTable();
            Mostrar_Ocultar_Error(false);
            try
            {
                Estado_Botones("inicial");
                Session["Dt_Registros"] = null;
                Llenar_Combo_Conceptos();
                Llenar_Combo_SubConceptos();
                Cmb_SubConcepto.Enabled = false;
                Llenar_Grid_Registros();
                Dt_Registos = (DataTable)Grid_Registros.DataSource;

                if(Dt_Registos  != null)
                {
                    if (Dt_Registos.Rows.Count > 0)
                    {
                        if(Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                        {
                            Lbl_Encanezado_Error.Text = "No se puede hacer de nuevo el pronostico de este año, pase a la opción de modificar"; 
                            Mostrar_Ocultar_Error(true);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de año Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Busueda_Concepto_TextChanged
        ///DESCRIPCIÓN          : Evento de la caja de texto de busqueda de conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Txt_Busueda_Concepto_TextChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);
            try
            {
                Busqueda_Conceptos(Txt_Busqueda_Concepto.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de conceptos. Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Busueda_SubConcepto_TextChanged
        ///DESCRIPCIÓN          : Evento de la caja de texto de busqueda de subconceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Txt_Busueda_SubConcepto_TextChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);
            try
            {
                Busqueda_SubConceptos(Txt_Busueda_SubConcepto.Text.Trim());
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de realizar la busqueda de subconceptos. Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_SubConcepto_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de conceptos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 22/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_SubConcepto_SelectedIndexChanged(object sender, EventArgs e)
        {
            Cls_Cat_Psp_SubConceptos_Negocio Negocio = new Cls_Cat_Psp_SubConceptos_Negocio();
            DataTable Dt_Datos = new DataTable();
            Mostrar_Ocultar_Error(false);
            try
            {
                Negocio.P_SubConcepto_Ing_ID = Cmb_SubConcepto.SelectedItem.Value.Trim();
                Dt_Datos = Negocio.Consultar_Datos_Completos_Ing();

                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                {
                    Hf_Rubro_ID.Value = Dt_Datos.Rows[0]["RUBRO_ID"].ToString().Trim();
                    Hf_Tipo_ID.Value = Dt_Datos.Rows[0]["TIPO_ID"].ToString().Trim();
                    Hf_Clase_ID.Value = Dt_Datos.Rows[0]["CLASE_ING_ID"].ToString().Trim();
                    Hf_Concepto_ID.Value = Dt_Datos.Rows[0]["CONCEPTO_ING_ID"].ToString().Trim();
                    Hf_SubConcepto_ID.Value = Dt_Datos.Rows[0]["SUBCONCEPTO_ING_ID"].ToString().Trim();

                    Cmb_Concepto.SelectedIndex = Cmb_Concepto.Items.IndexOf(Cmb_Concepto.Items.FindByValue(Hf_Concepto_ID.Value.Trim()));
                    Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(Hf_Rubro_ID.Value.Trim()));
                    Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Hf_Tipo_ID.Value.Trim()));
                    Cmb_Clase.SelectedIndex = Cmb_Clase.Items.IndexOf(Cmb_Clase.Items.FindByValue(Hf_Clase_ID.Value.Trim()));
                }

            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de conceptos Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de Programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 16/Julio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);

            try
            {
                Llenar_Combo_Conceptos();
            }
            catch (Exception ex)
            {
                Lbl_Encanezado_Error.Text = "Error en el evento del combo de programas. Error[" + ex.Message + "]";
                Lbl_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Rubro_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de rubros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Rubro_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);

            try
            {
                Llenar_Combo_Tipo();
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos();
                Llenar_Combo_SubConceptos();
            }
            catch (Exception ex)
            {
                Lbl_Encanezado_Error.Text = "Error en el evento del combo de rubros. Error[" + ex.Message + "]";
                Lbl_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Tipo_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de tipo
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Tipo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);

            try
            {
                Llenar_Combo_Clase();
                Llenar_Combo_Conceptos();
                Llenar_Combo_SubConceptos();
            }
            catch (Exception ex)
            {
                Lbl_Encanezado_Error.Text = "Error en el evento del combo de tipos. Error[" + ex.Message + "]";
                Lbl_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Clase_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de clases de ingresos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 08/Junio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Clase_SelectedIndexChanged(object sender, EventArgs e)
        {
            Mostrar_Ocultar_Error(false);

            try
            {
                Llenar_Combo_Conceptos();
                Llenar_Combo_SubConceptos();
            }
            catch (Exception ex)
            {
                Lbl_Encanezado_Error.Text = "Error en el evento del combo de clases de ingresos. Error[" + ex.Message + "]";
                Lbl_Error.Text = String.Empty;
                Mostrar_Ocultar_Error(true);
            }
        }
    #endregion

    #endregion
}
