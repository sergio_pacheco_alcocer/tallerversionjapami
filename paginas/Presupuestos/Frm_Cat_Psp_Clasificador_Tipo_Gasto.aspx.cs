﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Clasificador_Tipo_Gasto.Negocio;
using System.Text.RegularExpressions;

public partial class paginas_Compras_Frm_Cat_Psp_Clasificador_Tipo_Gasto : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Grid_Cotizador = 2;
        private const int Const_Estado_Modificar = 3;

        private static DataTable Dt_Clasificador = new DataTable();    
    #endregion

    #region Page Load / Init

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Controles();
                Cargar_Grid();
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
        
    }

    #endregion
    
    #region Metodos

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Mensaje_Error
        ///DESCRIPCION : Muestra el error
        ///PARAMETROS  : P_Texto: texto de un TextBox
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Mensaje_Error(String P_Mensaje)
        {
            Img_Error.Visible = true;
            Lbl_Error.Text += P_Mensaje + "</br>";
            Div_Contenedor_error.Visible = true;
        }
        private void Mensaje_Error()
        {
            Img_Error.Visible = false;
            Lbl_Error.Text = "";
        }

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Limpiar_Controles
        ///DESCRIPCION : Limpia los valores de los controles
        ///PARAMETROS  : 
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Limpiar_Controles()
        {
            //Limpia los controles
            Txt_Clave.Text = "";
            Txt_Nombre.Text = "";
            Txt_Descripcion.Text = "";            
            Txt_Busqueda.Text = "";

            Session.Remove("ID");
            Session.Remove("Clave");
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Estado_Botones
        ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
        ///PARAMETROS: P_Estado: El estado de la pagina
        ///CREO: David herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Estado_Botones(int P_Estado)
        {
            switch (P_Estado)
            {
                case 0: //Estado inicial                    

                    Div_Listado_Cotizadores.Visible = true;

                    Grid_Clasificador.Enabled = true;
                    Grid_Clasificador.SelectedIndex = (-1);
                    
                    Txt_Clave.Enabled =false;
                    Txt_Nombre.Enabled = false;
                    Txt_Descripcion.Enabled = false;
                    
                    Txt_Busqueda.Enabled = true;
                    Btn_Busqueda.Enabled = true;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Eliminar.AlternateText = "Eliminar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Inicio";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";

                    Btn_Modificar.Enabled = false;
                    Btn_Eliminar.Enabled = false;
                    Btn_Nuevo.Enabled = true;
                    Btn_Salir.Enabled = true;

                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;

                    Configuracion_Acceso("Frm_Cat_Psp_Clasificador_Tipo_Gasto.aspx");
                    break;

                case 1: //Nuevo                    

                    Div_Listado_Cotizadores.Visible = true;

                    Grid_Clasificador.Enabled = false;
                    Grid_Clasificador.SelectedIndex = (-1);

                    Txt_Clave.Enabled = true;
                    Txt_Nombre.Enabled = true;
                    Txt_Descripcion.Enabled = true;

                    Txt_Busqueda.Enabled = false;
                    Btn_Busqueda.Enabled = false;

                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false; 
                                       
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Eliminar.AlternateText = "Eliminar";
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";

                    break;

                case 2: //Grid Cotizador

                    Div_Listado_Cotizadores.Visible = true;
                    //Div_Datos_Cotizador.Visible = true;
                    Grid_Clasificador.Enabled = true;
                    
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.Enabled = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Eliminar.Enabled = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Eliminar.AlternateText = "Eliminar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Listado";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Listado";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";

                    break;

                case 3: //Modificar

                    Div_Listado_Cotizadores.Visible = true;
                    Grid_Clasificador.Enabled = false;
                    //Div_Datos_Cotizador.Visible = true;

                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;
                    Btn_Eliminar.Visible = false;

                    Txt_Clave.Enabled = true;
                    Txt_Nombre.Enabled = true;
                    Txt_Descripcion.Enabled = true;

                    Txt_Busqueda.Enabled = false;
                    Btn_Busqueda.Enabled = false;

                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Eliminar.AlternateText = "Eliminar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_Nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";

                    break;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar
        ///DESCRIPCIÓN: se actualizan los datos 
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************

        private void Modificar(Boolean Bln_Nuevo)
        {
            Div_Contenedor_error.Visible = false;
            Lbl_Error.Text = "";
            try
            {
                Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio Negocio = new Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio();
                DataTable Dt_Temp = new DataTable();
                Boolean Validar_Clave;

                Validar_Cajas();                               
                if (Div_Contenedor_error.Visible == false)
                {
                    if (Txt_Clave.Text.Trim() != (String)Session["Clave"])
                        Validar_Clave = true;
                    else
                        Validar_Clave = false;
                    if (Validar_Clave == true)
                    {
                        Negocio.P_Clave = Txt_Clave.Text.Trim();
                        Dt_Temp = Negocio.Validar_Clave();
                    }
                    if ((Dt_Temp != null) && Dt_Temp.Rows.Count > 0)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Clasificador Economico", "alert('Ya existe la clave.');", true);
                    }
                    else
                    {
                        Negocio.P_CTG_ID = (String)Session["ID"];
                        Negocio.P_Clave = Txt_Clave.Text.Trim();
                        Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                        Negocio.P_Descripcion = Txt_Descripcion.Text.Trim();

                        Negocio.Actualizar();
                        if (!Bln_Nuevo)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Clasificador Economico", "alert('La Modificación fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid();
                            Limpiar_Controles();
                        }
                    }
                }//Fin del if Div_Contenedor_error.Visible == false
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }//fin de Modificar  

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
        ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
        ///PARAMETROS: Page_Index: Numero de pagina del grid
        ///CREO: David Herrera rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Cargar_Grid()
        {
            try
            {
                Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio Negocio = new Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio();
                
                //Por si busca por la clave
                if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
                    Negocio.P_Clave = Txt_Busqueda.Text;

                //Realizamos la consulta
                Dt_Clasificador = Negocio.Consultar_Fechas_Pago();
                Session["Dt_Clasificador"] = Dt_Clasificador;
                //Asignamos los datos
                Grid_Clasificador.DataSource = Dt_Clasificador;
                Grid_Clasificador.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Cajas
        ///DESCRIPCIÓN: Metodo que valida que las cajas tengan contenido. 
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Validar_Cajas()
        {
            if (String.IsNullOrEmpty(Txt_Clave.Text.Trim()))
            {
                Lbl_Error.Text += "Falta la clave.<br />";
                Div_Contenedor_error.Visible = true;
            }
            else {
                if (Txt_Clave.Text.Trim().Length > 5)
                {
                    Lbl_Error.Text += "La clave debe tener menos de 5 caracteres.<br />";
                    Div_Contenedor_error.Visible = true;
                }
            }
            if (String.IsNullOrEmpty(Txt_Nombre.Text.Trim()))
            {
                Lbl_Error.Text += "Falta el nombre.<br />";
                Div_Contenedor_error.Visible = true;
            }            
        }     
    
    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio Negocio = new Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio();
                DataTable Dt_Temp = new DataTable();

                if (Btn_Nuevo.AlternateText == "Nuevo")
                {                    
                    Estado_Botones(Const_Estado_Nuevo);
                }
                else if (Btn_Nuevo.AlternateText == "Dar de Alta")
                {
                    Div_Contenedor_error.Visible = false;
                    Validar_Cajas();
                    //Si pasa todas las Validaciones damos de alta las fechas
                    if (Div_Contenedor_error.Visible == false)
                    {
                        try
                        {
                            Negocio.P_Clave = Txt_Clave.Text.Trim();
                            Dt_Temp = Negocio.Validar_Clave();
                            if (Dt_Temp.Rows.Count > 0)
                            { 
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Clasificador Economico", "alert('Ya existe la clave.');", true); 
                            }
                            else
                            {
                                //Asignamos los valores
                                Negocio.P_Clave = Txt_Clave.Text.Trim();
                                Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                                Negocio.P_Descripcion = Txt_Descripcion.Text.Trim();
                                //Agregamos el registro
                                Negocio.Agregar();

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Clasificador Economico", "alert('El alta fue Exitosa');", true);
                                Estado_Botones(Const_Estado_Inicial);
                                Cargar_Grid();
                                Limpiar_Controles();
                            }
                        }
                        catch
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Clasificador Economico", "alert('El alta no fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Evento del Boton Modificar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {                
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {   
                    Modificar(false);     
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
        ///DESCRIPCIÓN: Evento del Boton Eliminar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 12/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Declaracion de Variables
                Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio Negocio = new Cls_Cat_Psp_Clasificador_Tipo_Gasto_Negocio();
                String Respuesta = "";
                //Asignamos los valores
                Negocio.P_CTG_ID = (String)Session["ID"];
                Respuesta = Negocio.Eliminar();
                //Mostramos el mensaje
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Fechas Pago", "alert('" + Respuesta + "');", true);
                //Regresamos al estado normal la pagina
                Estado_Botones(Const_Estado_Inicial);
                Cargar_Grid();
                Limpiar_Controles();                
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Salir_Click
        ///DESCRIPCION:             Boton para SALIR
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Salir.AlternateText == "Inicio")
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
                else
                {
                    Estado_Botones(Const_Estado_Inicial);
                    Limpiar_Controles();
                    Cargar_Grid();
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }
            
        protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
        {
            try{
                Cargar_Grid();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }

    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Correo_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Clasificador_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Estado_Botones(Const_Grid_Cotizador);
                Session["ID"] = Grid_Clasificador.SelectedRow.Cells[1].Text.Trim();
                Session["Clave"] = Grid_Clasificador.SelectedRow.Cells[2].Text.Trim();
                Txt_Clave.Text = Grid_Clasificador.SelectedRow.Cells[2].Text.Trim();
                Txt_Nombre.Text = Grid_Clasificador.SelectedRow.Cells[3].Text.Trim();
                Txt_Descripcion.Text = Grid_Clasificador.SelectedRow.Cells[4].Text.Trim();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Fechas_Pago_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para cargar los datos de la pagina seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 11/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Clasificador_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Clasificador.DataSource = ((DataTable)Session["Dt_Clasificador"]);
                Grid_Clasificador.PageIndex = e.NewPageIndex;
                Grid_Clasificador.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        

    #endregion   

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:       Configuracion_Acceso
    /// DESCRIPCIÓN:  Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:   URL_Pagina: Nombre de la pagina
    /// USUARIO CREÓ: David Herrera Rincon
    /// FECHA CREÓ:   15/Enero/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);            

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 15/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion    
}
