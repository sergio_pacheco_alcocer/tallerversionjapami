<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
 CodeFile="Frm_Rpt_Psp_Calendarizacion.aspx.cs" Inherits="paginas_Presupuestos_Rpt_Psp_Calendarizacion" 
 Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script language="javascript" type="text/javascript">
// <!CDATA[
function Hr3_onclick() {
}
// ]]>85180811
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="36000" EnableScriptLocalization="true" EnableScriptGlobalization="true"/> 
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    <ContentTemplate>
                <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                            <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>                     
                </asp:UpdateProgress>
                <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte de Calendarizaci�n</td>
                    </tr>
                    <tr>
                        <td>
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top" >
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text=""  CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                          
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="left">
                        <td>
                            <asp:ImageButton ID="ImageButton1" runat="server" ImageUrl="~/paginas/imagenes/paginas/Listado.png" CausesValidation="False" 
                                OnClick="Btn_Generar_Listado_Click" AlternateText="Generar Listado" ToolTip="Generar Listado" Width="24px"/>
                                &nbsp;
                            <asp:ImageButton ID="ImageButton2" runat="server" ToolTip="Generar Reporte (PDF)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Generar Reporte (PDF)" onclick="Btn_Generar_Reporte_PDF_Click" /> 
                            <asp:ImageButton ID="ImageButton3" runat="server" ToolTip="Generar Reporte (Excel)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Generar Reporte (Excel)" onclick="Btn_Generar_Reporte_Excel_Click" /> 
                            <asp:ImageButton ID="ImageButton4" runat="server" ToolTip="Limpiar Filtros" CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                AlternateText="Limpiar Filtros" onclick="Btn_Limpiar_Filtros_Click" Width="25px"/>       
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            &nbsp;&nbsp;
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                        ToolTip="Salir"     AlternateText="Salir" OnClick="Btn_Salir_Click"/>
                        </td>                        
                    </tr>
                </table>
                <br />
                <center>
                    <div style="border-width:medium; width:95%; border-bottom-style:solid;" >
                    <table width="100%">                                                        
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Anio" runat="server" Text=" * A�o " CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td colspan="3" style="text-align:left;">
                                <asp:DropDownList ID="Cmb_Anio" runat="server" Width="150" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_SelectedIndexChanged">
                                    <%--<asp:ListItem Text="&lt;-- TODAS --&gt;" Value="TODAS"></asp:ListItem>   --%>
                                </asp:DropDownList>                                   
                            </td>
                        </tr>                          
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width:80%; text-align:left;" colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="90%" 
                                    onselectedindexchanged="Cmb_Unidad_Responsable_SelectedIndexChanged" AutoPostBack="true" >
                                    <%--<asp:ListItem Text="&lt;-- SELECCIONA UNIDAD RESPONSABLE --&gt;" Value="SELECCIONE"></asp:ListItem>--%>
                                </asp:DropDownList> 
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%; text-align:left; ">
                                <asp:Label ID="Lbl_Partida" runat="server" Text="Partida" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width:80%; text-align:left;" colspan="3">
                                <asp:DropDownList ID="Cmb_Partida" runat="server" Width="90%" >
                                    <%--<asp:ListItem Text="&lt;-- SELECCIONA PARTIDA --&gt;" Value="SELECCIONE"></asp:ListItem>--%>
                                </asp:DropDownList> 
                            </td>
                        </tr>                                      
                        <tr>
                            <td style="text-align:left; " colspan="4">
                                <hr />
                            </td>
                        </tr>                                                                                                                   
                        <%--<tr>
                            <td style="text-align:left; " colspan="3">
                                <asp:Label ID="Lbl_Con_Licencia" runat="server" Text="Elegir" CssClass="estilo_fuente"></asp:Label>
                                &nbsp;
                                <asp:DropDownList ID="Cmb_Con_Licencia" runat="server" Width="200px" >
                                    <asp:ListItem Text="&lt;-- TODOS --&gt;" Value="TODOS"></asp:ListItem>
                                    <asp:ListItem Text="CON LICENCIA" Value="CON_LICENCIA"></asp:ListItem>
                                    <asp:ListItem Text="SIN LICENCIA" Value="SIN_LICENCIA"></asp:ListItem>
                                </asp:DropDownList>     
                            </td>
                            <td style="text-align:right;">
                                <asp:ImageButton ID="Btn_Generar_Listado" runat="server" ImageUrl="~/paginas/imagenes/paginas/Listado.png" CausesValidation="False" 
                                    OnClick="Btn_Generar_Listado_Click" AlternateText="Generar Listado" ToolTip="Generar Listado" />
                                <asp:ImageButton ID="Btn_Generar_Reporte_PDF" runat="server" ToolTip="Generar Reporte (PDF)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                    AlternateText="Generar Reporte (PDF)" onclick="Btn_Generar_Reporte_PDF_Click" /> 
                                <asp:ImageButton ID="Btn_Generar_Reporte_Excel" runat="server" ToolTip="Generar Reporte (Excel)" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                    AlternateText="Generar Reporte (Excel)" onclick="Btn_Generar_Reporte_Excel_Click" /> 
                                <asp:ImageButton ID="Btn_Limpiar_Filtros" runat="server" ToolTip="Limpiar Filtros" CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                    AlternateText="Limpiar Filtros" onclick="Btn_Limpiar_Filtros_Click" Width="25px"/>       
                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                               
                            </td>--%>
                        </tr>                                                             
                    </table>
                </div>
                </center>
                <div style="overflow:auto;height:210px;width:99%;vertical-align:top;border-style:outset;border-color: Silver;" >
                    <center>
                            <br />
                            <asp:GridView ID="Grid_Calendarizar" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                PageSize="15" OnPageIndexChanging="Grid_Calendarizar_PageIndexChanging"
                                Width="98%">
                                <RowStyle CssClass="GridItem" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                <asp:BoundField DataField="ANIO" HeaderText="A�o" SortExpression="ANIO" >
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" HeaderText="Fte. Financiamiento" SortExpression="FTE_FINANCIAMIENTO_ID" >
                                    <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="AREA_FUNCIONAL_ID" HeaderText="Area Funcional" SortExpression="AREA_FUNCIONAL_ID" NullDisplayText="-">
                                    <ItemStyle Width="130px" HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" HeaderText="Programa" SortExpression="PROYECTO_PROGRAMA_ID" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="DEPENDENCIA_ID" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA_ID" NullDisplayText="-">
                                    <ItemStyle Width="90px" HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="PARTIDA_ID" HeaderText="Partida" SortExpression="PARTIDA_ID" NullDisplayText="-">
                                    <ItemStyle Width="90px" HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="SUBNIVEL_PRESUPUESTAL_ID" HeaderText="Subnivel" SortExpression="SUBNIVEL_PRESUPUESTAL_ID"  NullDisplayText="-">
                                    <ItemStyle Width="90px" HorizontalAlign="Center"/>
                                </asp:BoundField>
                                <asp:BoundField DataField="PRODUCTO_ID" HeaderText="Producto" SortExpression="PRODUCTO_ID" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_ENERO" HeaderText="Importe Enero" SortExpression="IMPORTE_ENERO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_FEBRERO" HeaderText="Importe Febrero" SortExpression="IMPORTE_FEBRERO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_MARZO" HeaderText="Importe Marzo" SortExpression="IMPORTE_MARZO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_ABRIL" HeaderText="Importe AbriL" SortExpression="IMPORTE_ABRIL" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_MAYO" HeaderText="Importe Mayo" SortExpression="IMPORTE_MAYO" NullDisplayText="-"> 
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_JUNIO" HeaderText="Importe Junio" SortExpression="IMPORTE_JUNIO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_JULIO" HeaderText="Importe Julio" SortExpression="IMPORTE_JULIO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_AGOSTO" HeaderText="Importe Agosto" SortExpression="IMPORTE_AGOSTO" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_SEPTIEMBRE" HeaderText="Importe Septiembre" SortExpression="IMPORTE_SEPTIEMBRE" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_OCTUBRE" HeaderText="Importe Octubre" SortExpression="IMPORTE_OCTUBRE" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_NOVIEMBRE" HeaderText="Importe Noviembre" SortExpression="IMPORTE_NOVIEMBRE" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_DICIEMBRE" HeaderText="Importe Diciembre" SortExpression="IMPORTE_DICIEMBRE" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Importe Total" SortExpression="IMPORTE_TOTAL" NullDisplayText="-">
                                     <ItemStyle Width="90px" HorizontalAlign="Center" />
                                </asp:BoundField>
                                
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                    </center>   
                </div>                                             
            </div>
            <br />
            <br />
            <br />
            <br />
    </ContentTemplate>
    </asp:UpdatePanel>


</asp:Content>

