﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Cat_Psp_Parametros_Presupuestales.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Psp_Parametros_Presupuestales" 
Title="SIAG Sistema Integral de Ardministración Gubernamental" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScriptManager_Tipos" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 99%; height: 100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Parametros
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="Td_Error">
                            &nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />&nbsp;
                            <asp:Label ID="Lbl_Encabezado_Error" runat="server" CssClass="estilo_fuente_mensaje_error" />
                            <br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" CssClass="estilo_fuente_mensaje_error" />
                        </td>
                    </tr>
                </table>
                <table width="100%" border="0" cellspacing="0">
                    <tr align="center">
                        <td>
                            <div style="width: 99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold;
                                font-style: normal; font-variant: normal; font-family: fantasy; height: 32px">
                                <table style="width: 100%; height: 28px;">
                                    <tr>
                                        <td align="left" style="width: 59%;">
                                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" OnClick="Btn_Modificar_Click" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" />
                                        </td>
                                        <td align="right" style="width: 41%;">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <center>
                    <br />
                    <asp:Panel ID="Panel1" runat = "server" GroupingText=" Datos Generales " Width="97%" style="text-align:left; color:Navy; font-weight:bold;">
                        <table width="99%">
                            <tr>
                                <td style="text-align: left; color:Black; font-weight:normal; width: 38%;">&nbsp;Aplicar Presupuesto Disponible Acumulado</td>
                                <td style="text-align: left;width: 62%;">
                                    <asp:DropDownList runat = "server" ID="Cmb_Aplicar_Acumulado" Width="99%">
                                        <asp:ListItem Value="NO" Selected="True">NO</asp:ListItem>
                                        <asp:ListItem Value="SI">SI</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; color:Black; font-weight:normal; width: 38%;">&nbsp;Clasificación Administrativa</td>
                                <td style="text-align: left;width: 62%;">
                                    <asp:DropDownList runat = "server" ID="Cmb_Clasificacion_Adm" Width="99%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; color:Black; font-weight:normal; width: 38%;">&nbsp;Póliza Presupuestal Egresos</td>
                                <td style="text-align: left;width: 62%;">
                                    <asp:DropDownList runat = "server" ID="Cmb_Tipo_Poliza_Egr" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left; color:Black; font-weight:normal; width: 38%;">&nbsp;Póliza Presupuestal Ingresos</td>
                                <td style="text-align: left;width: 62%;">
                                    <asp:DropDownList runat = "server" ID="Cmb_Tipo_Poliza_Ing" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    
                    <asp:Panel ID="Pnl_Datos_Ingresos" runat = "server" GroupingText=" Datos Ingresos " Width="97%" style="text-align:left;color:Navy; font-weight:bold;">
                        <table width="99%">
                            <tr>
                                <td style="width: 20%; text-align: left; color:Black; font-weight:normal;">&nbsp;Fuente</td>
                                <td style="width: 80%; text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Fte_Ingresos" Width="99%"></asp:DropDownList>
                                </td>
                                
                            </tr>
                            <tr>
                               <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Programa</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Programa_Ingresos" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    
                    <asp:Panel ID="Pnl_Datos_Inversiones" runat = "server" GroupingText=" Datos Inversiones " Width="97%" style="text-align:left;color:Navy; font-weight:bold;">
                        <table width="99%">
                            <tr>
                                <td style="width: 20%; text-align: left;color:Black; font-weight:normal;">&nbsp;Fuente</td>
                                <td style="width: 80%; text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Fte_Inversiones" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Programa</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Programa_Inversiones" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    
                    <asp:Panel ID="Pnl_Datos_Ctas_Ingresos" runat = "server" GroupingText=" Cuentas de Orden: Ley de Ingresos " Width="97%" style="text-align:left;color:Navy; font-weight:bold;">
                        <table width="99%">
                            <tr>
                                <td style="width: 20%; text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Estimado</td>
                                <td style="width: 80%; text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Estimado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Ampliación</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Ampliacion" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Reducción</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Reduccion" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Modificado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Modificado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Por Recaudar</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Por_Recaudar" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Devengado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Devengado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Recaudado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Ing_Recaudado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                    
                    <asp:Panel ID="Pnl_Datos_Ctas_Egresos" runat = "server" GroupingText=" Cuentas de Orden: Ley de Egresos " Width="97%" style="text-align:left;color:Navy; font-weight:bold;">
                        <table width="99%">
                            <tr>
                                <td style="width: 23%; text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Aprobado</td>
                                <td style="width: 77%; text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Aprobado" Width="99%"></asp:DropDownList>
                                </td>
                                
                            </tr>
                            <tr>
                                <td style=" text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Ampliación</td>
                                <td style=" text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Ampliacion" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Reducción</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Reduccion" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Ampliación Interna</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Ampliacion_Interna" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Reducción Interna</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Reduccion_Interna" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Modificado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Modificado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Por Ejercer</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Por_Ejercer" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Comprometido</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Comprometido" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Devengado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Devengado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Ejercido</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Ejercido" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;color:Black; font-weight:normal;">&nbsp;Cuenta Pagado</td>
                                <td style="text-align: left;">
                                    <asp:DropDownList runat="server" ID="Cmb_Cta_Egr_Pagado" Width="99%"></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <br />
                </center>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

