﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
 CodeFile="Frm_Ope_Psp_Movimientos_Ingresos_Autorizacion.aspx.cs" 
 Inherits="paginas_Presupuestos_Frm_Ope_Psp_Movimientos_Ingresos_Autorizacion" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Validar_Caracteres() {
            $('textarea[id$=Txt_Comentario]').keyup(function() {
                var Caracteres =  $(this).val().length;
                if (Caracteres > 250) {
                    this.value = this.value.substring(0, 250);
                }
            });
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                           Autorización Movimiento de Presupuesto Ingresos
                        </td>
                    </tr>
                    <tr>
                        <td runat="server" id="Td_Error" colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />&nbsp;
                            <asp:Label ID="Lbl_Encabezado_Error" runat="server"   CssClass="estilo_fuente_mensaje_error"/>
                            <br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server"  CssClass="estilo_fuente_mensaje_error"/>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width:100%;height:28px;">
                                        
                                    </table>
                                   </td>
                                 </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
            <center>
             <div id="Div_Grid_Movimientos" runat="server" style="overflow:auto;height:300px;width:98%;vertical-align:top;border-style:solid;border-color: Silver;">
                    <table width="100%">
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="Grid_Movimiento" runat="server"  CssClass="GridView_1" Width="100%" 
                                        AutoGenerateColumns="False"  GridLines="None" AllowPaging="false" 
                                        AllowSorting="True" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="No se encuentra ningun Movimiento"
                                        OnSelectedIndexChanged="Grid_Movimiento_SelectedIndexChanged"
                                        OnSorting="Grid_Movimiento_Sorting">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="MOVIMIENTO_ING_ID" HeaderText="Solicitud"  SortExpression="MOVIMIENTO_ING_ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE_NOM_CONCEPTO" HeaderText="Concepto" SortExpression="CLAVE_NOM_CONCEPTO">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="Operacion" SortExpression="TIPO_MOVIMIENTO">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMP_TOTAL" HeaderText="Importe" SortExpression="IMP_TOTAL" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="14%" />
                                                <ItemStyle HorizontalAlign="Right" Width="14%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus"  SortExpression="ESTATUS" >
                                                <HeaderStyle HorizontalAlign="Center" Width="12%" />
                                                <ItemStyle HorizontalAlign="Center" Width="12%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RUBRO_ID" />
                                            <asp:BoundField DataField="TIPO_ID" />
                                            <asp:BoundField DataField="CLASE_ING_ID" />
                                            <asp:BoundField DataField="CONCEPTO_ING_ID" />
                                            <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                            <asp:BoundField DataField="IMP_ENE" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_FEB" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_MAR" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_ABR" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_MAY" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_JUN" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_JUL" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_AGO" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_SEP" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_OCT" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_NOV" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="IMP_DIC" DataFormatString="{0:#,###,##0.00}"/>
                                            <asp:BoundField DataField="JUSTIFICACION" />
                                            <asp:BoundField DataField="COMENTARIO" />
                                            <asp:BoundField DataField="FECHA_MODIFICO" />
                                            <asp:BoundField DataField="TIPO_CONCEPTO" />
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                        </tr>
                    </table >
                </div>
             <div id="Div_Datos" runat="server">
                <br />
                <div id="Div_Datos_Generales" runat="server" style="width:97%;vertical-align:top;" >
                    <asp:Panel ID="Panel1" GroupingText="Datos Generales" runat="server">
                        <table id="Table_Datos_Genrerales" width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Numero_solicitud" runat="server" Text="Número de solicitud" Width="98%"  ></asp:Label>
                                </td >
                                <td style="width:20%; text-align:left;">
                                    <asp:TextBox ID="Txt_No_Solicitud" runat="server" ReadOnly="True"  Width="90%"></asp:TextBox>
                                </td>
                                <td style="width:12%; text-align:left;"> 
                                    <asp:Label ID="Lbl_Operacion" runat="server" Text="Operación" Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:50%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="98%" >
                                        <asp:ListItem>AMPLIACION</asp:ListItem>
                                        <asp:ListItem>REDUCCION</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <asp:Label ID="Lbl_Importe" runat="server" Text="Importe" Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Importe" runat="server" Width="90%" ></asp:TextBox>
                                </td>
                                 <td>
                                     <asp:Label ID="Lbl_Estatus" runat="server" Text="* Estatus " Width="98%"  ></asp:Label>
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <asp:Label ID="Label2" runat="server" Text="Justificación " Width="98%"  ></asp:Label>
                                </td>
                                <td colspan="3">
                                     <asp:TextBox ID="Txt_Justificacion" runat="server" 
                                        TextMode="MultiLine" Width="98%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                     <asp:Label ID="Label1" runat="server" Text="* Comentario " Width="98%"  ></asp:Label>
                                </td>
                                <td style="width:100%; text-align:left;" colspan="3">
                                    <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" Width="98%"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                </div>
                <br />
                <div ID="Div_CRI" runat="server"  style="width:97%;vertical-align:top;" >
                     <asp:Panel ID="Panel2" runat="server" GroupingText="Conceptos">
                        <table width="100%"   border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width:18%; text-align:left;" > 
                                    <asp:Label ID="Lbl_FF" runat="server" Text="Fuente Financiamiento" Width="100%"></asp:Label>
                                </td>
                                <td style="width:82%; text-align:left;">
                                    <asp:DropDownList ID="Cmb_FF" runat="server" Width="99%" ></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td> 
                                    <asp:Label ID="Lbl_Rubro" runat="server" Text="Rubro"   Width="100%"></asp:Label>
                                </td>
                                <td >
                                    <asp:DropDownList ID="Cmb_Rubro" runat="server" Width="99%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td colspan = "2"> 
                                    <asp:HiddenField id="Hf_Clase_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Concepto_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Tipo_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Rubro_ID" runat="server"/>
                                    <asp:HiddenField id="Hf_Modificado" runat="server"/>
                                    <asp:HiddenField id="Hf_tipo_Concepto" runat="server"/>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                             <tr runat="server" id= "Tabla_Meses">
                                <td colspan = "2"> 
                                   <table width="99%" style=" " border="0" cellspacing="0" class="estilo_fuente">
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Enero </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Febrero</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Marzo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Abril</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Mayo</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Junio</td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; cursor:default;"  class="button_autorizar">&nbsp;</td>
                                            <td  style="text-align:right; width:15%; cursor:default; font-size:8pt;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Enero" runat="server" Width="100%"  style="text-align:right; font-size:8pt;" ></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Febrero" runat="server" Width="100px" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Marzo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Abril" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Mayo" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Junio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr><td style="height:0.3em; cursor:default;" class="button_autorizar" colspan="7"></td></tr>
                                        <tr>
                                            <td style="width:10%; text-align:left; font-size:8pt; cursor:default;"  class="button_autorizar">Mes</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Julio</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Agosto</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Septiembre </td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Octubre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Noviembre</td>
                                            <td style="width:15%; text-align:center; font-size:8pt; cursor:default;" class="button_autorizar">Diciembre</td>
                                        </tr>
                                        <tr>
                                            <td style="width:10%;  text-align:left; font-size:8pt; cursor:default;" class="button_autorizar">&nbsp;</td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:100px;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Julio" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                               <asp:TextBox ID="Txt_Agosto" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Septiembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Octubre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Noviembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                            <td style="text-align:right; font-size:8pt; cursor:default; width:15%;" class="button_autorizar">
                                                <asp:TextBox ID="Txt_Diciembre" runat="server" Width="100%" style="text-align:right; font-size:8pt;"></asp:TextBox>
                                            </td>
                                        </tr>
                                   </table>
                                </td>
                            </tr>
                            <tr><td style="height:0.5em;" colspan="2"></td></tr>
                            <tr>
                                <td colspan="2">
                                    <div ID="Div_Conceptos" runat="server" style="width:99%; height:45px; overflow:auto; vertical-align:top;">
                                        <asp:GridView ID="Grid_Conceptos" runat="server"  CssClass="GridView_1" Width="97%" 
                                            AutoGenerateColumns="False"  GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" 
                                            EmptyDataText="No se encuentraron conceptos"
                                            >
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="CLAVE_NOM_CONCEPTO" HeaderText="Concepto" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="39%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="39%" Font-Size="7pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESTIMADO" HeaderText="Estimado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RECAUDADO" HeaderText="Recaudado" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="POR_RECAUDAR" HeaderText="Por Recaudar" DataFormatString="{0:#,###,##0.00}" >
                                                    <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="RUBRO_ID" />
                                                <asp:BoundField DataField="TIPO_ID" />
                                                <asp:BoundField DataField="CLASE_ING_ID" />
                                                <asp:BoundField DataField="CONCEPTO_ING_ID" />
                                                <asp:BoundField DataField="FUENTE_FINANCIAMIENTO_ID" />
                                                <asp:BoundField DataField="IMP_ENE" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_FEB" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_MAR" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_ABR" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_MAY" DataFormatString="{0:#,###,##0.00}" />
                                                <asp:BoundField DataField="IMP_JUN" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_JUL" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_AGO" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_SEP" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_OCT" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_NOV" DataFormatString="{0:#,###,##0.00}"/>
                                                <asp:BoundField DataField="IMP_DIC" DataFormatString="{0:#,###,##0.00}"/>
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        </table>
                     </asp:Panel>
                </div>   
        <%--        <div ID="Div3" runat="server"  style="width:97%;vertical-align:top;">
                    <asp:Panel ID="Pnl_Justificacion" runat="server" GroupingText="Justificación">
                        <table width="100%">
                             
                        </table>
                    </asp:Panel>
                </div>
                <br />--%>
                <%--<div id="Div_Grid_Comentarios" runat="server" style="width:97%;vertical-align:top;">
                   <asp:Panel ID="Panel4" runat="server" GroupingText="Comentarios">
                    <div id="Div1" runat="server" style="overflow:auto;height:50px;width:97%;vertical-align:top;">
                         <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                           
                          </table>
                       </div>
                   </asp:Panel>
                </div>--%>
             </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

