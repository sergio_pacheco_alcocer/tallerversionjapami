﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Presupuestal.Negocio;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Presupuetal : System.Web.UI.Page
{
    #region (Page Load)
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Reporte_Inicio();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte presupuestal. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Sueldos_Inicio
        ///DESCRIPCIÓN          : Metodo de inicio de la página
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Reporte_Inicio()
        {
            try
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Limpiar_Controles("Todo");
                Llenar_Combo_Anios();
                Cmb_Anio.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte analitico de ingresos. Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
        ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Limpiar_Controles(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        Cmb_Anio.SelectedIndex = -1;
                        break;
                    case "Error":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_Anios()
        {
            Cls_Rpt_Psp_Presupuestal_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Presupuestal_Negocio();
            DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Anio.Items.Clear();

                Dt_Anios = Obj_Ingresos.Consultar_Anios();

                Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataSource = Dt_Anios;
                Cmb_Anio.DataBind();

                Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar
        ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
        ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        public void Generar_Rpt(DataTable Dt_EAIP, DataTable Dt_EAEPE, DataTable Dt_EAEPEFP)
        {
            String Valor = String.Empty;
            Double Modificado = 0.00;
            Double Recaudado = 0.00;
            String Ruta_Archivo = "04_PRE_" + String.Format("{0:dd_MMM_yyyy}", DateTime.Now) + ".xls";
            WorksheetCell Celda = new WorksheetCell();
            try
            {
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                Libro.Properties.Title = "Reportes de Ley Presupuestos";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                #region (Estilos)
                //******************************************** Estilos del libro ******************************************************//
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera_Titulo = Libro.Styles.Add("HeaderStyleTit");
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Negritas = Libro.Styles.Add("BodyStyleBold");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad_Negritas = Libro.Styles.Add("BodyStyleCantBold");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Negritas = Libro.Styles.Add("BodyStyleTotBold");

                Estilo_Cabecera_Titulo.Font.FontName = "Tahoma";
                Estilo_Cabecera_Titulo.Font.Size = 12;
                Estilo_Cabecera_Titulo.Font.Bold = true;
                Estilo_Cabecera_Titulo.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera_Titulo.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera_Titulo.Font.Color = "#000000";
                Estilo_Cabecera_Titulo.Interior.Color = "#FF9900";
                Estilo_Cabecera_Titulo.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera_Titulo.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera_Titulo.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera_Titulo.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera_Titulo.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 9;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "#000000";
                Estilo_Cabecera.Interior.Color = "#FF9900";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = false;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido_Negritas.Font.FontName = "Tahoma";
                Estilo_Contenido_Negritas.Font.Size = 9;
                Estilo_Contenido_Negritas.Font.Bold = true;
                Estilo_Contenido_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido_Negritas.Font.Color = "#000000";
                Estilo_Contenido_Negritas.Interior.Color = "White";
                Estilo_Contenido_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad.Font.FontName = "Tahoma";
                Estilo_Cantidad.Font.Size = 9;
                Estilo_Cantidad.Font.Bold = false;
                Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad.Font.Color = "#000000";
                Estilo_Cantidad.Interior.Color = "White";
                Estilo_Cantidad.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad_Negritas.Font.FontName = "Tahoma";
                Estilo_Cantidad_Negritas.Font.Size = 9;
                Estilo_Cantidad_Negritas.Font.Bold = true;
                Estilo_Cantidad_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad_Negritas.Font.Color = "#000000";
                Estilo_Cantidad_Negritas.Interior.Color = "White";
                Estilo_Cantidad_Negritas.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Total_Negritas.Font.FontName = "Tahoma";
                Estilo_Total_Negritas.Font.Size = 9;
                Estilo_Total_Negritas.Font.Bold = true;
                Estilo_Total_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Total_Negritas.Font.Color = "#000000";
                Estilo_Total_Negritas.Interior.Color = "#FFCC66";
                Estilo_Total_Negritas.NumberFormat = "#,###,##0.00";
                Estilo_Total_Negritas.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Total_Negritas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Total_Negritas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region (Hojas y Columnas)
                //******************************************** Hojas del libro ******************************************************//
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("EAIP");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_1;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1;
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja2 = Libro.Worksheets.Add("EAEPE");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_2;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon2;
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja3 = Libro.Worksheets.Add("EAEPECFP");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_3;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon3;

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja6 = Libro.Worksheets.Add("EAEPECPP");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_6;
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon6;

                /*//Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja4 = Libro.Worksheets.Add("EGPP");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_4;
                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja5 = Libro.Worksheets.Add("AFFOP");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_5;*/

                //******************************************** Columnas de las hojas ******************************************************//
                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//INDICE CRI.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//Concepto.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//LEY DE INGRESOS ESTIMADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARGO_D.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_D.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_D.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//RECAUDADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ACANCE RECAUDADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO.

                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//INDICE COG.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//CONCEPTO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACIONES.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCIONES.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARCO_C.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_C.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_C.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARGO_D.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_D.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_D.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//EJERCIDO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//PAGADO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//DISPONIBLE.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//COMPROMETIDO.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SIN DEVENGAR.
                Hoja2.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//POR PAGAR.

                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CODIGO PROGRAMATICO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//CONCEPTO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACIONES.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCIONES.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARCO_C.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_C.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_C.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARGO_D.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_D.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_D.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//EJERCIDO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//PAGADO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DISPONIBLE.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//COMPROMETIDO.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SIN DEVENGAR.
                Hoja3.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//POR PAGAR.

                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CODIGO PROGRAMATICO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//CONCEPTO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACIONES.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCIONES.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARCO_C.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_C.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_C.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CARGO_D.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABONO_D.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SALDO_D.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//EJERCIDO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//PAGADO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DISPONIBLE.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//COMPROMETIDO.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SIN DEVENGAR.
                Hoja6.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//POR PAGAR.

                /*Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CODIGO PROGRAMATICO PROGRAMA.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CODIGO PROGRAMATICO UR.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(280));//CONCEPTO.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//METAS INDICADOR.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//CANTIDAD.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ESTIMADA.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AP CANTIDAD.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AP%.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AF PAGADO.
                Hoja4.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AF %.

                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//1.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//2.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//3.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//4.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//5.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//6.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//7.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//8.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//9.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//10.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//11.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//12.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//13.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//14.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//15.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//16.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//17.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//18.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//19.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//20.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//21.
                Hoja5.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//22.*/
                #endregion

                #region(Estructura Columnas)
                //se llena el encabezado principal de la hoja uno
                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add("ESTADO ANALÍTICO DE INGRESOS PRESUPUESTARIOS ");
                Celda.MergeAcross = 11; // COLUMNAS A EXPANDIR
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_1 = Hoja1.Table.Rows.Add();

                Renglon_1 = Hoja1.Table.Rows.Add();
                Celda = Renglon_1.Cells.Add("INDICE CRI");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("CONCEPTO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("LEY DE INGRESOS ESTIMADO(A)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("AMPLIACIÓN");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("REDUCCIÓN");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("MODIFICADO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add(" DEVENGADO CARGO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add(" DEVENGADO ABONO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add(" DEVENGADO SALDO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("RECAUDADO(D)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("AVANCE DE RECAUDACIÓN E=(D/B)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_1.Cells.Add("SALDO B-D-E");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_1 = Hoja1.Table.Rows.Add();
                Renglon_1 = Hoja1.Table.Rows.Add();
                //Renglon_1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARGO(C)", DataType.String, "HeaderStyle"));
                //Renglon_1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONO", "HeaderStyle"));
                //Renglon_1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("SALDO", "HeaderStyle"));

                //se llena el encabezado principal de la hoja dos
                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add("ESTADO ANALÍTICO DEL EJERCICIO DEL PRESUPUESTO DE EGRESOS");
                Celda.MergeAcross = 17; // Merge 3 cells together
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_2 = Hoja2.Table.Rows.Add();

                Renglon_2 = Hoja2.Table.Rows.Add();
                Celda = Renglon_2.Cells.Add("INDICE COG");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("CONCEPTO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("APROBADO(A)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("AMPLIACIÓN(B)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("REDUCCIÓN(C)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("MODIFICADO(D)=A+B+C");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" COMPROMETIDO CARGO(E)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" COMPROMETIDO ABONO(F)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" COMPROMETIDO SALDO(G)=E-F");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" DEVENGADO CARGO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" DEVENGADO ABONO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add(" DEVENGADO SALDO(H)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("EJERCIDO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("PAGADO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("PRESUPUESTO DISPONIBLE PARA COMPROMETER");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("PRESUPUESTO COMPROMETIDO SIN DEVENGAR");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("PRESUPUESTO SIN DEVENGAR (J)=I+G");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_2.Cells.Add("CUENTAS POR PAGAR(DEUDA)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_2 = Hoja2.Table.Rows.Add();
                Renglon_2 = Hoja2.Table.Rows.Add();

                //se llena el encabezado principal de la hoja tres
                Renglon_3 = Hoja3.Table.Rows.Add();
                Celda = Renglon_3.Cells.Add("ESTADO ANALÍTICO DEL EJERCICIO DEL PRESUPUESTO DE EGRESOS EN SU CLASIFICACIÓN FUNCIONAL PROGRAMÁTICA");
                Celda.MergeAcross = 17; // Merge 3 cells together
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_3 = Hoja3.Table.Rows.Add();

                Renglon_3 = Hoja3.Table.Rows.Add();
                Celda = Renglon_3.Cells.Add("INDICE COG");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("CONCEPTO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("APROBADO(A)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("AMPLIACIÓN(B)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("REDUCCIÓN(C)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("MODIFICADO(D)=A+B+C");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" COMPROMETIDO CARGO(E)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" COMPROMETIDO ABONO(F)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" COMPROMETIDO SALDO(G)=E-F");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" DEVENGADO CARGO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" DEVENGADO ABONO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add(" DEVENGADO SALDO(H)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("EJERCIDO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("PAGADO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("PRESUPUESTO DISPONIBLE PARA COMPROMETER");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("PRESUPUESTO COMPROMETIDO SIN DEVENGAR");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("PRESUPUESTO SIN DEVENGAR (J)=I+G");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_3.Cells.Add("CUENTAS POR PAGAR(DEUDA)(H)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_3 = Hoja3.Table.Rows.Add();
                Renglon_3 = Hoja3.Table.Rows.Add();


                //se llena el encabezado principal de la hoja tres
                Renglon_6 = Hoja6.Table.Rows.Add();
                Celda = Renglon_6.Cells.Add("ESTADO ANALÍTICO DEL EJERCICIO DEL PRESUPUESTO DE EGRESOS EN SU CLASIFICACIÓN POR PROYECTO O PROGRAMA");
                Celda.MergeAcross = 17; // Merge 3 cells together
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_6 = Hoja6.Table.Rows.Add();

                Renglon_6 = Hoja6.Table.Rows.Add();
                Celda = Renglon_6.Cells.Add("INDICE PROGRAMA");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("CONCEPTO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("APROBADO(A)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("AMPLIACIÓN(B)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("REDUCCIÓN(C)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("MODIFICADO(D)=A+B+C");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" COMPROMETIDO CARGO(E)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" COMPROMETIDO ABONO(F)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" COMPROMETIDO SALDO(G)=E-F");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" DEVENGADO CARGO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" DEVENGADO ABONO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add(" DEVENGADO SALDO(H)");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("EJERCIDO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("PAGADO");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("PRESUPUESTO DISPONIBLE PARA COMPROMETER");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("PRESUPUESTO COMPROMETIDO SIN DEVENGAR");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("PRESUPUESTO SIN DEVENGAR (J)=I+G");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_6.Cells.Add("CUENTAS POR PAGAR(DEUDA)(H)");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_6 = Hoja6.Table.Rows.Add();
                Renglon_6 = Hoja6.Table.Rows.Add();

                /*//se llena el encabezado principal de la hoja uno
                Renglon_4 = Hoja4.Table.Rows.Add();
                Celda = Renglon_4.Cells.Add("ESTADO DEL GASTO PRESUPUESTAL PROGRAMATICO AL " + String.Format("{0:dd_MMM_yyyy}", DateTime.Now));
                Celda.MergeAcross = 9; // COLUMNAS A EXPANDIR
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_4 = Hoja4.Table.Rows.Add();

                Renglon_4 = Hoja4.Table.Rows.Add();
                Celda = Renglon_4.Cells.Add("CLAVE PROGRAMATICA PROGRAMA");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("CLAVE PROGRAMATICA UNIDAD RESPONSABLE");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("PROCESO Y OBJETIVOS ESPECIFICOS");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("METAS INDICADOR");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("METAS CANTIDAD");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("VALUACIÓN ESTIMADA");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("AVANCE PROGRAMÁTICO CANTIDAD");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("AVANCE PROGRAMÁTICO %");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("AVANCE FINANCIERO PAGADO");
                Celda.MergeDown = 2; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_4.Cells.Add("AVANCE FINANCIERO %");
                Celda.MergeDown = 2; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_4 = Hoja4.Table.Rows.Add();

                //se llena el encabezado principal de la hoja uno
                Renglon_5 = Hoja5.Table.Rows.Add();
                Celda = Renglon_5.Cells.Add("AVANCE FÍSICO FINANCIERO DE LA OBRA");
                Celda.MergeAcross = 21; // COLUMNAS A EXPANDIR
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyleTit";
                Renglon_5 = Hoja5.Table.Rows.Add();

                Renglon_5 = Hoja5.Table.Rows.Add();
                Celda = Renglon_5.Cells.Add("OBRA/OBJETO DEL CONTRATO Y/O CONVENIO");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("PROGRAMA APROBADO EN");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("MONTO APROBADO");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("ORIGEN DE LOS RECURSOS");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("MONTO DE LOS RECURSOS");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("RUBRO/LEY DE LA COORDINACIÓN FISCAL");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("MODALIDAD");
                Celda.MergeDown = 3; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("CONTRATO Y/O CONVENIO");
                Celda.MergeDown = 3; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("FECHA DE CELEBRACIÓN DEL CONTRATO Y/O CONVENIO");
                Celda.MergeDown = 3; // COLUMNAS A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("IMPORTE DEL CONTRATO Y/O CONVENIO");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("VIGENCIA DEL CONTRATO Y7O CONVENIO (INICIO)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("VIGENCIA DEL CONTRATO Y7O CONVENIO (TERMINO)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("PROVEEDOR O CONTRATISTA (TOMADO DE LA CÉDULA FISCAL)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("DOMICILIO");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("DEPENDENCIA EJECUTADORA DE LA OBRA O LOS TRABAJOS");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("IMPORTE EJERCIDO (RECURSOS FEDERALES)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("IMPORTE EJERCIDO (RECURSOS ESTATALES)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("IMPORTE EJERCIDO (RECURSOS MUNICIPALES)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("CUENTA CONTABLE DE LA OBRA EN LA BALANZA DE COMPROBACIÓN");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("AVANCE FÍSICO(%)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("AVANCE FINANCIERO(%)");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Celda = Renglon_5.Cells.Add("OBSERVACIONES");
                Celda.MergeDown = 3; // filas A EXPANDIR
                Celda.StyleID = "HeaderStyle";
                Renglon_5 = Hoja5.Table.Rows.Add();
                Renglon_5 = Hoja5.Table.Rows.Add();*/
                #endregion

                /***************prueba*********************/
                //    CarlosAg.ExcelXmlWriter.Worksheet sheet = Libro.Worksheets.Add("prueba");//hoja
     
                //    sheet.Table.Columns.Add(new WorksheetColumn(150));//columna
                //    sheet.Table.Columns.Add(new WorksheetColumn(100));//columna

                //    WorksheetRow row = sheet.Table.Rows.Add();
                //    WorksheetCell cellrr = row.Cells.Add("Header 1");
                //    cellrr.MergeDown = 1;            // Merge two cells together
                //    cellrr.StyleID = "HeaderStyle";

                //    row.Cells.Add(new WorksheetCell("Header 2", "HeaderStyle"));

                //    WorksheetCell cell = row.Cells.Add("Header 3");
                //    cell.MergeAcross = 1;            // Merge two cells together
                //    cell.StyleID = "HeaderStyle";

                //    WorksheetCell cell2 = row.Cells.Add("Header 4");
                //    cell2.MergeAcross = 1;            // Merge two cells together
                //    cell2.MergeDown = 1;
                //    cell2.StyleID = "HeaderStyle";
                //    row = sheet.Table.Rows.Add();
                //    row.Index = 2;
                //    row.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("CARGO(C)", DataType.String, "HeaderStyle"));
                //    row.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONO", "HeaderStyle"));
                //    row.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("ABONO", "HeaderStyle"));
                    

                //    // Skip one row, and add some text
                //    row.Index = 5;
                    
                //    row.Cells.Add("Data");
                //    row.Cells.Add("Data 1");
                //    row.Cells.Add("Data 2");
                //    row.Cells.Add("Data 3");

                //    // Generate 30 rows
                //for (int i=0; i<30; i++) {
                //    row = sheet.Table.Rows.Add();
                //    row.Cells.Add("Row " + i.ToString());
                //    row.Cells.Add(new WorksheetCell(i.ToString(), DataType.Number));
                //}

                //// Add a Hyperlink
                //row = sheet.Table.Rows.Add();
                //cell = row.Cells.Add();
                //cell.Data.Text = "Carlos Aguilar Mares";
                //cell.HRef = "http://www.carlosag.net";
                //// Add a Formula for the above 30 rows
                //cell = row.Cells.Add();
                    //cell.Formula = "=SUM(R[-30]C:R[-1]C)";

                //**************************************** Agregamos los datos al reporte DE LA HOJA 1***********************************************//
                #region(Hoja 1 EAIP)
                if (Dt_EAIP is DataTable)
                {
                    if (Dt_EAIP.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_EAIP.Rows)
                        {
                            Renglon1 = Hoja1.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Trim().Equals("RUBRO"))
                            {
                                foreach (DataColumn Columna in Dt_EAIP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_CRI") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        }
                                        else if (Columna.ColumnName.Equals("AVANCE_DE_RECAUDACION"))
                                        {
                                            if (Recaudado > 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("0.00", DataType.Number, "BodyStyleCantBold"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCantBold"));
                                            }
                                            Modificado = 0.00; Recaudado = 0.00;
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCantBold"));
                                                if (Columna.ColumnName.Equals("MODIFICADO"))
                                                {
                                                    Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                                else if (Columna.ColumnName.Equals("RECAUDADO"))
                                                {
                                                    Recaudado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                            {
                                foreach (DataColumn Columna in Dt_EAIP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_CRI") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleTotBold"));
                                        }
                                        else if (Columna.ColumnName.Equals("AVANCE_DE_RECAUDACION"))
                                        {
                                            if (Recaudado > 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("0.00", DataType.Number, "BodyStyleTotBold"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleTotBold"));
                                            }
                                            Modificado = 0.00; Recaudado = 0.00;
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleTotBold"));
                                                if (Columna.ColumnName.Equals("MODIFICADO"))
                                                {
                                                    Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                                else if (Columna.ColumnName.Equals("RECAUDADO"))
                                                {
                                                    Recaudado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataColumn Columna in Dt_EAIP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_CRI") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else if (Columna.ColumnName.Equals("AVANCE_DE_RECAUDACION"))
                                        {
                                            if (Recaudado > 0)
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("0.00", DataType.Number, "BodyStyleCant"));
                                            }
                                            else
                                            {
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                            }
                                            Modificado = 0.00; Recaudado = 0.00;
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCant"));
                                                if (Columna.ColumnName.Equals("MODIFICADO"))
                                                {
                                                    Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                                else if (Columna.ColumnName.Equals("RECAUDADO"))
                                                {
                                                    Recaudado = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString());
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                //**************************************** Agregamos los datos al reporte DE LA HOJA 2***********************************************//
                #region(Hoja 2 EAEPE)
                if (Dt_EAEPE is DataTable)
                {
                    if (Dt_EAEPE.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_EAEPE.Rows)
                        {
                            Renglon2 = Hoja2.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Trim().Equals("CAPITULO"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPE.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCantBold"));
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPE.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleTotBold"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleTotBold"));
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataColumn Columna in Dt_EAEPE.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon2.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCant"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                //**************************************** Agregamos los datos al reporte DE LA HOJA 3***********************************************//
                #region(Hoja 3 EAEPECFP)
                if (Dt_EAEPEFP is DataTable)
                {
                    if (Dt_EAEPEFP.Rows.Count > 0)
                    {
                        Dt_EAEPEFP = Generar_Dt_COG(Dt_EAEPEFP);

                        foreach (DataRow Dr in Dt_EAEPEFP.Rows)
                        {
                            Renglon3 = Hoja3.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Trim().Equals("COGT") || Dr["TIPO"].ToString().Trim().Equals("COGTP"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleBold"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCantBold"));
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleTotBold"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleTotBold"));
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon3.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCant"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region(Hoja 4 EAEPECPP)
                if (Dt_EAEPEFP is DataTable)
                {
                    if (Dt_EAEPEFP.Rows.Count > 0)
                    {
                        //Dt_EAEPEFP = Generar_Dt_COG(Dt_EAEPEFP);

                        //obtenemos solo los programas
                        Dt_EAEPEFP = (from Fila_Inc in Dt_EAEPEFP.AsEnumerable()
                                      where Fila_Inc.Field<String>("TIPO") == "COGTP"
                                      || Fila_Inc.Field<String>("TIPO") == "TOTAL"
                                      select Fila_Inc).AsDataView().ToTable();

                        foreach (DataRow Dr in Dt_EAEPEFP.Rows)
                        {
                            Renglon6 = Hoja6.Table.Rows.Add();

                            if (Dr["TIPO"].ToString().Trim().Equals("COGT") || Dr["TIPO"].ToString().Trim().Equals("COGTP"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString().Replace("*", ""),DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCant"));
                                            }
                                        }
                                    }
                                }
                            }
                            else if (Dr["TIPO"].ToString().Trim().Equals("TOTAL"))
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyleTotBold"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleTotBold"));
                                            }
                                        }
                                    }
                                }
                            }
                            else
                            {
                                foreach (DataColumn Columna in Dt_EAEPEFP.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Columna.ColumnName.Equals("INDICE_COG") || Columna.ColumnName.Equals("CONCEPTO"))
                                        {
                                            Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (!Columna.ColumnName.Equals("TIPO"))
                                            {
                                                Valor = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName].ToString()));
                                                Renglon6.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Valor, DataType.Number, "BodyStyleCant"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                #endregion

                #region(Hoja 5 EGPP)
                #endregion

                #region(Hoja 6 AFFOP)
                #endregion
                

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Campos
        ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private Boolean Validar_Campos()
        {
            Boolean Datos_Validos = true;
            Limpiar_Controles("Error");
            Lbl_Ecabezado_Mensaje.Text = "Favor de:";
            try
            {
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                    Datos_Validos = false;
                }

                return Datos_Validos;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Dt_COG
        ///DESCRIPCIÓN          : Metodo para generar la tabla de cog
        ///PARAMETROS           1: Dt tabla que contiene los registros del cog
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private DataTable Generar_Dt_COG(DataTable Dt)
        {
            DataTable Dt_Datos = new DataTable();
            String FF_ID = String.Empty;
            String AF_ID = String.Empty;
            String P_ID = String.Empty;
            String DES_PP = String.Empty;
            String UR_ID = String.Empty;
            DataRow Fila;

            Double Tot_Aprobado = 0.00;
            Double Tot_Ampliacion = 0.00;
            Double Tot_Reduccion = 0.00;
            Double Tot_Modificado = 0.00;
            Double Tot_Cargo_C = 0.00;
            Double Tot_Abono_C = 0.00;
            Double Tot_Saldo_C = 0.00;
            Double Tot_Cargo_D = 0.00;
            Double Tot_Abono_D = 0.00;
            Double Tot_Saldo_D = 0.00;
            Double Tot_Ejercido = 0.00;
            Double Tot_Pagado = 0.00;
            Double Tot_Disponible = 0.00;
            Double Tot_Comprometido = 0.00;
            Double Tot_Sin_Devengar = 0.00;
            Double Tot_Por_Pagar = 0.00;

            Double Tot_FF_Aprobado = 0.00;
            Double Tot_FF_Ampliacion = 0.00;
            Double Tot_FF_Reduccion = 0.00;
            Double Tot_FF_Modificado = 0.00;
            Double Tot_FF_Cargo_C = 0.00;
            Double Tot_FF_Abono_C = 0.00;
            Double Tot_FF_Saldo_C = 0.00;
            Double Tot_FF_Cargo_D = 0.00;
            Double Tot_FF_Abono_D = 0.00;
            Double Tot_FF_Saldo_D = 0.00;
            Double Tot_FF_Ejercido = 0.00;
            Double Tot_FF_Pagado = 0.00;
            Double Tot_FF_Disponible = 0.00;
            Double Tot_FF_Comprometido = 0.00;
            Double Tot_FF_Sin_Devengar = 0.00;
            Double Tot_FF_Por_Pagar = 0.00;

            Double Tot_AF_Aprobado = 0.00;
            Double Tot_AF_Ampliacion = 0.00;
            Double Tot_AF_Reduccion = 0.00;
            Double Tot_AF_Modificado = 0.00;
            Double Tot_AF_Cargo_C = 0.00;
            Double Tot_AF_Abono_C = 0.00;
            Double Tot_AF_Saldo_C = 0.00;
            Double Tot_AF_Cargo_D = 0.00;
            Double Tot_AF_Abono_D = 0.00;
            Double Tot_AF_Saldo_D = 0.00;
            Double Tot_AF_Ejercido = 0.00;
            Double Tot_AF_Pagado = 0.00;
            Double Tot_AF_Disponible = 0.00;
            Double Tot_AF_Comprometido = 0.00;
            Double Tot_AF_Sin_Devengar = 0.00;
            Double Tot_AF_Por_Pagar = 0.00;

            Double Tot_P_Aprobado = 0.00;
            Double Tot_P_Ampliacion = 0.00;
            Double Tot_P_Reduccion = 0.00;
            Double Tot_P_Modificado = 0.00;
            Double Tot_P_Cargo_C = 0.00;
            Double Tot_P_Abono_C = 0.00;
            Double Tot_P_Saldo_C = 0.00;
            Double Tot_P_Cargo_D = 0.00;
            Double Tot_P_Abono_D = 0.00;
            Double Tot_P_Saldo_D = 0.00;
            Double Tot_P_Ejercido = 0.00;
            Double Tot_P_Pagado = 0.00;
            Double Tot_P_Disponible = 0.00;
            Double Tot_P_Comprometido = 0.00;
            Double Tot_P_Sin_Devengar = 0.00;
            Double Tot_P_Por_Pagar = 0.00;

            Double Tot_UR_Aprobado = 0.00;
            Double Tot_UR_Ampliacion = 0.00;
            Double Tot_UR_Reduccion = 0.00;
            Double Tot_UR_Modificado = 0.00;
            Double Tot_UR_Cargo_C = 0.00;
            Double Tot_UR_Abono_C = 0.00;
            Double Tot_UR_Saldo_C = 0.00;
            Double Tot_UR_Cargo_D = 0.00;
            Double Tot_UR_Abono_D = 0.00;
            Double Tot_UR_Saldo_D = 0.00;
            Double Tot_UR_Ejercido = 0.00;
            Double Tot_UR_Pagado = 0.00;
            Double Tot_UR_Disponible = 0.00;
            Double Tot_UR_Comprometido = 0.00;
            Double Tot_UR_Sin_Devengar = 0.00;
            Double Tot_UR_Por_Pagar = 0.00;

            try
            {
                Dt_Datos.Columns.Add("INDICE_COG", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("CONCEPTO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("APROBADO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("CARGO_C", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("ABONO_C", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("SALDO_C", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("CARGO_D", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("ABONO_D", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("SALDO_D", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("EJERCIDO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("PAGADO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("DISPONIBLE", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("COMPROMETIDO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("SIN_DEVENGAR", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("POR_PAGAR", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));

                FF_ID = Dt.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString().Trim();
                AF_ID = Dt.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                P_ID = Dt.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                DES_PP = HttpUtility.HtmlDecode(Dt.Rows[0]["DES_PP"].ToString().Trim());
                UR_ID = Dt.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();

                Fila = Dt_Datos.NewRow();
                Fila["INDICE_COG"] = "**** " + Dt.Rows[0]["AF"].ToString().Trim();
                Fila["CONCEPTO"] = Dt.Rows[0]["DES_AF"].ToString().Trim();
                Fila["APROBADO"] = "0";
                Fila["AMPLIACION"] = "0";
                Fila["REDUCCION"] = "0";
                Fila["MODIFICADO"] = "0";
                Fila["CARGO_C"] = "0";
                Fila["ABONO_C"] = "0";
                Fila["SALDO_C"] = "0";
                Fila["CARGO_D"] = "0";
                Fila["ABONO_D"] = "0";
                Fila["SALDO_D"] = "0";
                Fila["EJERCIDO"] = "0";
                Fila["PAGADO"] = "0";
                Fila["DISPONIBLE"] = "0";
                Fila["COMPROMETIDO"] = "0";
                Fila["SIN_DEVENGAR"] = "0";
                Fila["POR_PAGAR"] = "0";
                Fila["TIPO"] = "AF";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["INDICE_COG"] = "*** " + Dt.Rows[0]["PP"].ToString().Trim();
                Fila["CONCEPTO"] = Dt.Rows[0]["DES_PP"].ToString().Trim();
                Fila["APROBADO"] = "0";
                Fila["AMPLIACION"] = "0";
                Fila["REDUCCION"] = "0";
                Fila["MODIFICADO"] = "0";
                Fila["CARGO_C"] = "0";
                Fila["ABONO_C"] = "0";
                Fila["SALDO_C"] = "0";
                Fila["CARGO_D"] = "0";
                Fila["ABONO_D"] = "0";
                Fila["SALDO_D"] = "0";
                Fila["EJERCIDO"] = "0";
                Fila["PAGADO"] = "0";
                Fila["DISPONIBLE"] = "0";
                Fila["COMPROMETIDO"] = "0";
                Fila["SIN_DEVENGAR"] = "0";
                Fila["POR_PAGAR"] = "0";
                Fila["TIPO"] = "PP";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["INDICE_COG"] = "** " + Dt.Rows[0]["UR"].ToString().Trim();
                Fila["CONCEPTO"] = Dt.Rows[0]["DES_UR"].ToString().Trim();
                Fila["APROBADO"] = "0";
                Fila["AMPLIACION"] = "0";
                Fila["REDUCCION"] = "0";
                Fila["MODIFICADO"] = "0";
                Fila["CARGO_C"] = "0";
                Fila["ABONO_C"] = "0";
                Fila["SALDO_C"] = "0";
                Fila["CARGO_D"] = "0";
                Fila["ABONO_D"] = "0";
                Fila["SALDO_D"] = "0";
                Fila["EJERCIDO"] = "0";
                Fila["PAGADO"] = "0";
                Fila["DISPONIBLE"] = "0";
                Fila["COMPROMETIDO"] = "0";
                Fila["SIN_DEVENGAR"] = "0";
                Fila["POR_PAGAR"] = "0";
                Fila["TIPO"] = "UR";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["INDICE_COG"] = "* " +  Dt.Rows[0]["FF"].ToString().Trim();
                Fila["CONCEPTO"] = Dt.Rows[0]["DES_FF"].ToString().Trim();
                Fila["APROBADO"] = "0";
                Fila["AMPLIACION"] = "0";
                Fila["REDUCCION"] = "0";
                Fila["MODIFICADO"] = "0";
                Fila["CARGO_C"] = "0";
                Fila["ABONO_C"] = "0";
                Fila["SALDO_C"] = "0";
                Fila["CARGO_D"] = "0";
                Fila["ABONO_D"] = "0";
                Fila["SALDO_D"] = "0";
                Fila["EJERCIDO"] = "0";
                Fila["PAGADO"] = "0";
                Fila["DISPONIBLE"] = "0";
                Fila["COMPROMETIDO"] = "0";
                Fila["SIN_DEVENGAR"] = "0";
                Fila["POR_PAGAR"] = "0";
                Fila["TIPO"] = "FF";
                Dt_Datos.Rows.Add(Fila);

                foreach (DataRow Dr in Dt.Rows)
                {
                    Tot_Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                    Tot_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                    Tot_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                    Tot_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                    Tot_Cargo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                    Tot_Abono_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                    Tot_Saldo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                    Tot_Cargo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                    Tot_Abono_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                    Tot_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                    Tot_Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                    Tot_Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                    Tot_Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                    Tot_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                    Tot_Sin_Devengar += Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                    Tot_Por_Pagar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());



                    if (AF_ID.Equals(Dr["AREA_FUNCIONAL_ID"].ToString().Trim()))
                    {
                        Tot_AF_Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_AF_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_AF_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_AF_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_AF_Cargo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                        Tot_AF_Abono_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                        Tot_AF_Saldo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                        Tot_AF_Cargo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                        Tot_AF_Abono_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                        Tot_AF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                        Tot_AF_Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                        Tot_AF_Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_AF_Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_AF_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_AF_Sin_Devengar += Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                        Tot_AF_Por_Pagar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                        if (P_ID.Equals(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                        {
                            Tot_P_Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Tot_P_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Tot_P_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Tot_P_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Tot_P_Cargo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                            Tot_P_Abono_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                            Tot_P_Saldo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                            Tot_P_Cargo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                            Tot_P_Abono_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                            Tot_P_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                            Tot_P_Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                            Tot_P_Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Tot_P_Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Tot_P_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Tot_P_Sin_Devengar += Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                            Tot_P_Por_Pagar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                            if (UR_ID.Equals(Dr["DEPENDENCIA_ID"].ToString().Trim()))
                            {
                                Tot_UR_Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Tot_UR_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Tot_UR_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Tot_UR_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Tot_UR_Cargo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                                Tot_UR_Abono_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                                Tot_UR_Saldo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                                Tot_UR_Cargo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                                Tot_UR_Abono_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                                Tot_UR_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                                Tot_UR_Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                Tot_Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Tot_UR_Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Tot_UR_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Tot_UR_Sin_Devengar += Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                                Tot_UR_Por_Pagar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                                if (FF_ID.Equals(Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim()))
                                {
                                    Tot_FF_Aprobado += Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Tot_FF_Ampliacion += Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Tot_FF_Reduccion += Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Tot_FF_Modificado += Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Tot_FF_Cargo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                                    Tot_FF_Abono_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                                    Tot_FF_Saldo_C += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                                    Tot_FF_Cargo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                                    Tot_FF_Abono_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                                    Tot_FF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                                    Tot_FF_Ejercido += Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                    Tot_FF_Pagado += Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Tot_FF_Disponible += Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    Tot_FF_Comprometido += Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Tot_FF_Sin_Devengar += Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                                    Tot_FF_Por_Pagar += Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());


                                    Fila = Dt_Datos.NewRow();
                                    Fila["INDICE_COG"] = Dr["PARTIDA"].ToString().Trim();
                                    Fila["CONCEPTO"] = Dr["DES_PARTIDA"].ToString().Trim();
                                    Fila["APROBADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                    Fila["AMPLIACION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["REDUCCION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["MODIFICADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["CARGO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim()));
                                    Fila["ABONO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim()));
                                    Fila["SALDO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim()));
                                    Fila["CARGO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim()));
                                    Fila["ABONO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim()));
                                    Fila["SALDO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim()));
                                    Fila["EJERCIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                    Fila["PAGADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                    Fila["DISPONIBLE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                    Fila["COMPROMETIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                    Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim()));
                                    Fila["POR_PAGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim()));
                                    Fila["TIPO"] = "PARTIDA";
                                    Dt_Datos.Rows.Add(Fila);
                                }//FIN FF
                                else
                                {
                                    foreach (DataRow Dr_FF in Dt_Datos.Rows)
                                    {
                                        if (Dr_FF["TIPO"].ToString().Trim().Equals("FF"))
                                        {
                                            Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_FF_Aprobado);
                                            Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_FF_Ampliacion);
                                            Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_FF_Reduccion);
                                            Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_FF_Modificado);
                                            Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_FF_Cargo_C);
                                            Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_FF_Abono_C);
                                            Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_FF_Saldo_C);
                                            Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_FF_Cargo_D);
                                            Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_FF_Abono_D);
                                            Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_FF_Saldo_D);
                                            Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_FF_Ejercido);
                                            Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                            Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_FF_Disponible);
                                            Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_FF_Sin_Devengar);
                                            Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_FF_Por_Pagar);
                                            Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                            Dr_FF["TIPO"] = "COGT";
                                            break;
                                        }
                                    }

                                    Tot_FF_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                    Tot_FF_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                    Tot_FF_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                    Tot_FF_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                    Tot_FF_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                                    Tot_FF_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                                    Tot_FF_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                                    Tot_FF_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                                    Tot_FF_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                                    Tot_FF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                                    Tot_FF_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                    Tot_FF_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                    Tot_FF_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                    Tot_FF_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                    Tot_FF_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                                    Tot_FF_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                                    Fila = Dt_Datos.NewRow();
                                    Fila["INDICE_COG"] = "* " +  Dr["FF"].ToString().Trim();
                                    Fila["CONCEPTO"] = Dr["DES_FF"].ToString().Trim();
                                    Fila["APROBADO"] = "0";
                                    Fila["AMPLIACION"] = "0";
                                    Fila["REDUCCION"] = "0";
                                    Fila["MODIFICADO"] = "0";
                                    Fila["CARGO_C"] = "0";
                                    Fila["ABONO_C"] = "0";
                                    Fila["SALDO_C"] = "0";
                                    Fila["CARGO_D"] = "0";
                                    Fila["ABONO_D"] = "0";
                                    Fila["SALDO_D"] = "0";
                                    Fila["EJERCIDO"] = "0";
                                    Fila["PAGADO"] = "0";
                                    Fila["DISPONIBLE"] = "0";
                                    Fila["COMPROMETIDO"] = "0";
                                    Fila["SIN_DEVENGAR"] = "0";
                                    Fila["POR_PAGAR"] = "0";
                                    Fila["TIPO"] = "FF";
                                    Dt_Datos.Rows.Add(Fila);

                                    FF_ID = Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim();

                                    Fila = Dt_Datos.NewRow();
                                    Fila["INDICE_COG"] = Dr["PARTIDA"].ToString().Trim();
                                    Fila["CONCEPTO"] = Dr["DES_PARTIDA"].ToString().Trim();
                                    Fila["APROBADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                    Fila["AMPLIACION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["REDUCCION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["MODIFICADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["CARGO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim()));
                                    Fila["ABONO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim()));
                                    Fila["SALDO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim()));
                                    Fila["CARGO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim()));
                                    Fila["ABONO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim()));
                                    Fila["SALDO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim()));
                                    Fila["EJERCIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                    Fila["PAGADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                    Fila["DISPONIBLE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                    Fila["COMPROMETIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                    Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim()));
                                    Fila["POR_PAGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim()));
                                    Fila["TIPO"] = "PARTIDA";
                                    Dt_Datos.Rows.Add(Fila);
                                }//fin else FF
                            }//fin if ur
                            else
                            {
                                foreach (DataRow Dr_FF in Dt_Datos.Rows)
                                {
                                    if (Dr_FF["TIPO"].ToString().Trim().Equals("UR"))
                                    {
                                        Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_UR_Aprobado);
                                        Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_UR_Ampliacion);
                                        Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_UR_Reduccion);
                                        Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_UR_Modificado);
                                        Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_UR_Cargo_C);
                                        Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_UR_Abono_C);
                                        Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_UR_Saldo_C);
                                        Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_UR_Cargo_D);
                                        Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_UR_Abono_D);
                                        Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_UR_Saldo_D);
                                        Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_UR_Ejercido);
                                        Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                                        Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_UR_Disponible);
                                        Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_UR_Sin_Devengar);
                                        Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_UR_Por_Pagar);
                                        Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                                        if (DES_PP.Trim().Equals(HttpUtility.HtmlDecode("DIRECCIÓN GENERAL")))
                                        {
                                            Dr_FF["TIPO"] = "COGTP";
                                        }
                                        else
                                        {
                                            Dr_FF["TIPO"] = "COGT";
                                        }
                                    }
                                    else if (Dr_FF["TIPO"].ToString().Trim().Equals("FF"))
                                    {
                                        Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_FF_Aprobado);
                                        Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_FF_Ampliacion);
                                        Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_FF_Reduccion);
                                        Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_FF_Modificado);
                                        Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_FF_Cargo_C);
                                        Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_FF_Abono_C);
                                        Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_FF_Saldo_C);
                                        Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_FF_Cargo_D);
                                        Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_FF_Abono_D);
                                        Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_FF_Saldo_D);
                                        Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_FF_Ejercido);
                                        Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                        Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_FF_Disponible);
                                        Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_FF_Sin_Devengar);
                                        Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_FF_Por_Pagar);
                                        Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                        Dr_FF["TIPO"] = "COGT";
                                        break;
                                    }
                                }

                                Tot_FF_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Tot_FF_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Tot_FF_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Tot_FF_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Tot_FF_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                                Tot_FF_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                                Tot_FF_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                                Tot_FF_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                                Tot_FF_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                                Tot_FF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                                Tot_FF_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                Tot_FF_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Tot_FF_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Tot_FF_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Tot_FF_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                                Tot_FF_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                                Tot_UR_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                                Tot_UR_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                                Tot_UR_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                                Tot_UR_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                                Tot_UR_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                                Tot_UR_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                                Tot_UR_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                                Tot_UR_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                                Tot_UR_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                                Tot_UR_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                                Tot_UR_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                                Tot_UR_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                                Tot_UR_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                                Tot_UR_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                                Tot_UR_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                                Tot_UR_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                                Fila = Dt_Datos.NewRow();
                                Fila["INDICE_COG"] = "** " +  Dr["UR"].ToString().Trim();
                                Fila["CONCEPTO"] = Dr["DES_UR"].ToString().Trim();
                                Fila["APROBADO"] = "0";
                                Fila["AMPLIACION"] = "0";
                                Fila["REDUCCION"] = "0";
                                Fila["MODIFICADO"] = "0";
                                Fila["CARGO_C"] = "0";
                                Fila["ABONO_C"] = "0";
                                Fila["SALDO_C"] = "0";
                                Fila["CARGO_D"] = "0";
                                Fila["ABONO_D"] = "0";
                                Fila["SALDO_D"] = "0";
                                Fila["EJERCIDO"] = "0";
                                Fila["PAGADO"] = "0";
                                Fila["DISPONIBLE"] = "0";
                                Fila["COMPROMETIDO"] = "0";
                                Fila["SIN_DEVENGAR"] = "0";
                                Fila["POR_PAGAR"] = "0";
                                Fila["TIPO"] = "UR";
                                Dt_Datos.Rows.Add(Fila);

                                UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                                Fila = Dt_Datos.NewRow();
                                Fila["INDICE_COG"] = "* " + Dr["FF"].ToString().Trim();
                                Fila["CONCEPTO"] = Dr["DES_FF"].ToString().Trim();
                                Fila["APROBADO"] = "0";
                                Fila["AMPLIACION"] = "0";
                                Fila["REDUCCION"] = "0";
                                Fila["MODIFICADO"] = "0";
                                Fila["CARGO_C"] = "0";
                                Fila["ABONO_C"] = "0";
                                Fila["SALDO_C"] = "0";
                                Fila["CARGO_D"] = "0";
                                Fila["ABONO_D"] = "0";
                                Fila["SALDO_D"] = "0";
                                Fila["EJERCIDO"] = "0";
                                Fila["PAGADO"] = "0";
                                Fila["DISPONIBLE"] = "0";
                                Fila["COMPROMETIDO"] = "0";
                                Fila["SIN_DEVENGAR"] = "0";
                                Fila["POR_PAGAR"] = "0";
                                Fila["TIPO"] = "FF";
                                Dt_Datos.Rows.Add(Fila);

                                FF_ID = Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim();

                                Fila = Dt_Datos.NewRow();
                                Fila["INDICE_COG"] = Dr["PARTIDA"].ToString().Trim();
                                Fila["CONCEPTO"] = Dr["DES_PARTIDA"].ToString().Trim();
                                Fila["APROBADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                Fila["AMPLIACION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["REDUCCION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["MODIFICADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["CARGO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim()));
                                Fila["ABONO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim()));
                                Fila["SALDO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim()));
                                Fila["CARGO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim()));
                                Fila["ABONO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim()));
                                Fila["SALDO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim()));
                                Fila["EJERCIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                                Fila["PAGADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                                Fila["DISPONIBLE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                Fila["COMPROMETIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim()));
                                Fila["POR_PAGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim()));
                                Fila["TIPO"] = "PARTIDA";
                                Dt_Datos.Rows.Add(Fila);
                            } //FIN ELSE UR
                        }//FIN IF PP
                        else
                        {
                            foreach (DataRow Dr_FF in Dt_Datos.Rows)
                            {
                                if (Dr_FF["TIPO"].ToString().Trim().Equals("PP"))
                                {
                                    Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_P_Aprobado);
                                    Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_P_Ampliacion);
                                    Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_P_Reduccion);
                                    Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_P_Modificado);
                                    Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_P_Cargo_C);
                                    Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_P_Abono_C);
                                    Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_P_Saldo_C);
                                    Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_P_Cargo_D);
                                    Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_P_Abono_D);
                                    Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_P_Saldo_D);
                                    Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_P_Ejercido);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                                    Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_P_Disponible);
                                    Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_P_Sin_Devengar);
                                    Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_P_Por_Pagar);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                                    if (DES_PP.Trim().Equals(HttpUtility.HtmlDecode("DIRECCIÓN GENERAL")))
                                    {
                                        Dr_FF["TIPO"] = "COGT";
                                    }
                                    else
                                    {
                                        Dr_FF["TIPO"] = "COGTP";
                                    }
                                }
                                else if (Dr_FF["TIPO"].ToString().Trim().Equals("UR"))
                                {
                                    Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_UR_Aprobado);
                                    Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_UR_Ampliacion);
                                    Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_UR_Reduccion);
                                    Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_UR_Modificado);
                                    Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_UR_Cargo_C);
                                    Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_UR_Abono_C);
                                    Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_UR_Saldo_C);
                                    Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_UR_Cargo_D);
                                    Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_UR_Abono_D);
                                    Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_UR_Saldo_D);
                                    Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_UR_Ejercido);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                                    Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_UR_Disponible);
                                    Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_UR_Sin_Devengar);
                                    Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_UR_Por_Pagar);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);

                                    if (DES_PP.Trim().Equals(HttpUtility.HtmlDecode("DIRECCIÓN GENERAL")))
                                    {
                                        Dr_FF["TIPO"] = "COGTP";
                                    }
                                    else 
                                    {
                                        Dr_FF["TIPO"] = "COGT";
                                    }
                                }
                                else if (Dr_FF["TIPO"].ToString().Trim().Equals("FF"))
                                {
                                    Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_FF_Aprobado);
                                    Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_FF_Ampliacion);
                                    Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_FF_Reduccion);
                                    Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_FF_Modificado);
                                    Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_FF_Cargo_C);
                                    Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_FF_Abono_C);
                                    Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_FF_Saldo_C);
                                    Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_FF_Cargo_D);
                                    Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_FF_Abono_D);
                                    Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_FF_Saldo_D);
                                    Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_FF_Ejercido);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                    Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_FF_Disponible);
                                    Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_FF_Sin_Devengar);
                                    Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_FF_Por_Pagar);
                                    Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                    Dr_FF["TIPO"] = "COGT";
                                    break;
                                }
                            }

                            Tot_FF_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Tot_FF_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Tot_FF_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Tot_FF_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Tot_FF_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                            Tot_FF_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                            Tot_FF_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                            Tot_FF_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                            Tot_FF_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                            Tot_FF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                            Tot_FF_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                            Tot_FF_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Tot_FF_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Tot_FF_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Tot_FF_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                            Tot_FF_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                            Tot_UR_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Tot_UR_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Tot_UR_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Tot_UR_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Tot_UR_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                            Tot_UR_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                            Tot_UR_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                            Tot_UR_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                            Tot_UR_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                            Tot_UR_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                            Tot_UR_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                            Tot_UR_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Tot_UR_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Tot_UR_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Tot_UR_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                            Tot_UR_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                            Tot_P_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                            Tot_P_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                            Tot_P_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                            Tot_P_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                            Tot_P_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                            Tot_P_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                            Tot_P_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                            Tot_P_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                            Tot_P_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                            Tot_P_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                            Tot_P_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                            Tot_P_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                            Tot_P_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                            Tot_P_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                            Tot_P_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                            Tot_P_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                            Fila = Dt_Datos.NewRow();
                            Fila["INDICE_COG"] = "*** " +  Dr["PP"].ToString().Trim();
                            Fila["CONCEPTO"] = Dr["DES_PP"].ToString().Trim();
                            Fila["APROBADO"] = "0";
                            Fila["AMPLIACION"] = "0";
                            Fila["REDUCCION"] = "0";
                            Fila["MODIFICADO"] = "0";
                            Fila["CARGO_C"] = "0";
                            Fila["ABONO_C"] = "0";
                            Fila["SALDO_C"] = "0";
                            Fila["CARGO_D"] = "0";
                            Fila["ABONO_D"] = "0";
                            Fila["SALDO_D"] = "0";
                            Fila["EJERCIDO"] = "0";
                            Fila["PAGADO"] = "0";
                            Fila["DISPONIBLE"] = "0";
                            Fila["COMPROMETIDO"] = "0";
                            Fila["SIN_DEVENGAR"] = "0";
                            Fila["POR_PAGAR"] = "0";
                            Fila["TIPO"] = "PP";
                            Dt_Datos.Rows.Add(Fila);

                            P_ID = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                            DES_PP = HttpUtility.HtmlDecode(Dr["DES_PP"].ToString().Trim());

                            Fila = Dt_Datos.NewRow();
                            Fila["INDICE_COG"] = "** " +  Dr["UR"].ToString().Trim();
                            Fila["CONCEPTO"] = Dr["DES_UR"].ToString().Trim();
                            Fila["APROBADO"] = "0";
                            Fila["AMPLIACION"] = "0";
                            Fila["REDUCCION"] = "0";
                            Fila["MODIFICADO"] = "0";
                            Fila["CARGO_C"] = "0";
                            Fila["ABONO_C"] = "0";
                            Fila["SALDO_C"] = "0";
                            Fila["CARGO_D"] = "0";
                            Fila["ABONO_D"] = "0";
                            Fila["SALDO_D"] = "0";
                            Fila["EJERCIDO"] = "0";
                            Fila["PAGADO"] = "0";
                            Fila["DISPONIBLE"] = "0";
                            Fila["COMPROMETIDO"] = "0";
                            Fila["SIN_DEVENGAR"] = "0";
                            Fila["POR_PAGAR"] = "0";
                            Fila["TIPO"] = "UR";
                            Dt_Datos.Rows.Add(Fila);

                            UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                            Fila = Dt_Datos.NewRow();
                            Fila["INDICE_COG"] = "* " + Dr["FF"].ToString().Trim();
                            Fila["CONCEPTO"] = Dr["DES_FF"].ToString().Trim();
                            Fila["APROBADO"] = "0";
                            Fila["AMPLIACION"] = "0";
                            Fila["REDUCCION"] = "0";
                            Fila["MODIFICADO"] = "0";
                            Fila["CARGO_C"] = "0";
                            Fila["ABONO_C"] = "0";
                            Fila["SALDO_C"] = "0";
                            Fila["CARGO_D"] = "0";
                            Fila["ABONO_D"] = "0";
                            Fila["SALDO_D"] = "0";
                            Fila["EJERCIDO"] = "0";
                            Fila["PAGADO"] = "0";
                            Fila["DISPONIBLE"] = "0";
                            Fila["COMPROMETIDO"] = "0";
                            Fila["SIN_DEVENGAR"] = "0";
                            Fila["POR_PAGAR"] = "0";
                            Fila["TIPO"] = "FF";
                            Dt_Datos.Rows.Add(Fila);

                            FF_ID = Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim();

                            Fila = Dt_Datos.NewRow();
                            Fila["INDICE_COG"] = Dr["PARTIDA"].ToString().Trim();
                            Fila["CONCEPTO"] = Dr["DES_PARTIDA"].ToString().Trim();
                            Fila["APROBADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                            Fila["AMPLIACION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                            Fila["REDUCCION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                            Fila["MODIFICADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                            Fila["CARGO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim()));
                            Fila["ABONO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim()));
                            Fila["SALDO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim()));
                            Fila["CARGO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim()));
                            Fila["ABONO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim()));
                            Fila["SALDO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim()));
                            Fila["EJERCIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                            Fila["PAGADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                            Fila["DISPONIBLE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                            Fila["COMPROMETIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                            Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim()));
                            Fila["POR_PAGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim()));
                            Fila["TIPO"] = "PARTIDA";
                            Dt_Datos.Rows.Add(Fila);
                        }//fin else PP
                    }//FIN IF AF
                    else 
                    {
                        foreach (DataRow Dr_FF in Dt_Datos.Rows)
                        {
                            if (Dr_FF["TIPO"].ToString().Trim().Equals("AF"))
                            {
                                Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_AF_Aprobado);
                                Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_AF_Ampliacion);
                                Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_AF_Reduccion);
                                Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_AF_Modificado);
                                Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_AF_Cargo_C);
                                Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_AF_Abono_C);
                                Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_AF_Saldo_C);
                                Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_AF_Cargo_D);
                                Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_AF_Abono_D);
                                Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_AF_Saldo_D);
                                Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_AF_Ejercido);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_AF_Pagado);
                                Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_AF_Disponible);
                                Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_AF_Sin_Devengar);
                                Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_AF_Por_Pagar);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_AF_Pagado);
                                Dr_FF["TIPO"] = "COGT";
                            }
                            else if (Dr_FF["TIPO"].ToString().Trim().Equals("PP"))
                            {
                                Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_P_Aprobado);
                                Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_P_Ampliacion);
                                Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_P_Reduccion);
                                Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_P_Modificado);
                                Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_P_Cargo_C);
                                Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_P_Abono_C);
                                Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_P_Saldo_C);
                                Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_P_Cargo_D);
                                Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_P_Abono_D);
                                Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_P_Saldo_D);
                                Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_P_Ejercido);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                                Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_P_Disponible);
                                Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_P_Sin_Devengar);
                                Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_P_Por_Pagar);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                                Dr_FF["TIPO"] = "COGTP";
                            }
                            else if (Dr_FF["TIPO"].ToString().Trim().Equals("UR"))
                            {
                                Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_UR_Aprobado);
                                Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_UR_Ampliacion);
                                Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_UR_Reduccion);
                                Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_UR_Modificado);
                                Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_UR_Cargo_C);
                                Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_UR_Abono_C);
                                Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_UR_Saldo_C);
                                Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_UR_Cargo_D);
                                Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_UR_Abono_D);
                                Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_UR_Saldo_D);
                                Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_UR_Ejercido);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                                Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_UR_Disponible);
                                Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_UR_Sin_Devengar);
                                Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_UR_Por_Pagar);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                                if (DES_PP.Trim().Equals(HttpUtility.HtmlDecode("DIRECCIÓN GENERAL")))
                                {
                                    Dr_FF["TIPO"] = "COGTP";
                                }
                                else
                                {
                                    Dr_FF["TIPO"] = "COGT";
                                }
                            }
                            else if (Dr_FF["TIPO"].ToString().Trim().Equals("FF"))
                            {
                                Dr_FF["APROBADO"] = String.Format("{0:n}", Tot_FF_Aprobado);
                                Dr_FF["AMPLIACION"] = String.Format("{0:n}", Tot_FF_Ampliacion);
                                Dr_FF["REDUCCION"] = String.Format("{0:n}", Tot_FF_Reduccion);
                                Dr_FF["MODIFICADO"] = String.Format("{0:n}", Tot_FF_Modificado);
                                Dr_FF["CARGO_C"] = String.Format("{0:n}", Tot_FF_Cargo_C);
                                Dr_FF["ABONO_C"] = String.Format("{0:n}", Tot_FF_Abono_C);
                                Dr_FF["SALDO_C"] = String.Format("{0:n}", Tot_FF_Saldo_C);
                                Dr_FF["CARGO_D"] = String.Format("{0:n}", Tot_FF_Cargo_D);
                                Dr_FF["ABONO_D"] = String.Format("{0:n}", Tot_FF_Abono_D);
                                Dr_FF["SALDO_D"] = String.Format("{0:n}", Tot_FF_Saldo_D);
                                Dr_FF["EJERCIDO"] = String.Format("{0:n}", Tot_FF_Ejercido);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                Dr_FF["DISPONIBLE"] = String.Format("{0:n}", Tot_FF_Disponible);
                                Dr_FF["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_FF_Sin_Devengar);
                                Dr_FF["POR_PAGAR"] = String.Format("{0:n}", Tot_FF_Por_Pagar);
                                Dr_FF["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                                Dr_FF["TIPO"] = "COGT";
                                break;
                            }
                        }

                        Tot_FF_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_FF_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_FF_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_FF_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_FF_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                        Tot_FF_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                        Tot_FF_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                        Tot_FF_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                        Tot_FF_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                        Tot_FF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                        Tot_FF_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                        Tot_FF_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_FF_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_FF_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_FF_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                        Tot_FF_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                        Tot_UR_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_UR_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_UR_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_UR_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_UR_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                        Tot_UR_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                        Tot_UR_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                        Tot_UR_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                        Tot_UR_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                        Tot_UR_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                        Tot_UR_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                        Tot_UR_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_UR_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_UR_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_UR_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                        Tot_UR_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                        Tot_P_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_P_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_P_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_P_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_P_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                        Tot_P_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                        Tot_P_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                        Tot_P_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                        Tot_P_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                        Tot_P_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                        Tot_P_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                        Tot_P_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_P_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_P_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_P_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                        Tot_P_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                        Tot_AF_Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim());
                        Tot_AF_Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim());
                        Tot_AF_Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim());
                        Tot_AF_Modificado = Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim());
                        Tot_AF_Cargo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim());
                        Tot_AF_Abono_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim());
                        Tot_AF_Saldo_C = Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim());
                        Tot_AF_Cargo_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim());
                        Tot_AF_Abono_D = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim());
                        Tot_AF_Saldo_D += Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim());
                        Tot_AF_Ejercido = Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim());
                        Tot_AF_Pagado = Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim());
                        Tot_AF_Disponible = Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim());
                        Tot_AF_Comprometido = Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim());
                        Tot_AF_Sin_Devengar = Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim());
                        Tot_AF_Por_Pagar = Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["INDICE_COG"] = "**** " + Dr["AF"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["DES_AF"].ToString().Trim();
                        Fila["APROBADO"] = "0";
                        Fila["AMPLIACION"] = "0";
                        Fila["REDUCCION"] = "0";
                        Fila["MODIFICADO"] = "0";
                        Fila["CARGO_C"] = "0";
                        Fila["ABONO_C"] = "0";
                        Fila["SALDO_C"] = "0";
                        Fila["CARGO_D"] = "0";
                        Fila["ABONO_D"] = "0";
                        Fila["SALDO_D"] = "0";
                        Fila["EJERCIDO"] = "0";
                        Fila["PAGADO"] = "0";
                        Fila["DISPONIBLE"] = "0";
                        Fila["COMPROMETIDO"] = "0";
                        Fila["SIN_DEVENGAR"] = "0";
                        Fila["POR_PAGAR"] = "0";
                        Fila["TIPO"] = "AF";
                        Dt_Datos.Rows.Add(Fila);

                        AF_ID = Dr["AREA_FUNCIONAL_ID"].ToString().Trim();

                        Fila = Dt_Datos.NewRow();
                        Fila["INDICE_COG"] = "*** " + Dr["PP"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["DES_PP"].ToString().Trim();
                        Fila["APROBADO"] = "0";
                        Fila["AMPLIACION"] = "0";
                        Fila["REDUCCION"] = "0";
                        Fila["MODIFICADO"] = "0";
                        Fila["CARGO_C"] = "0";
                        Fila["ABONO_C"] = "0";
                        Fila["SALDO_C"] = "0";
                        Fila["CARGO_D"] = "0";
                        Fila["ABONO_D"] = "0";
                        Fila["SALDO_D"] = "0";
                        Fila["EJERCIDO"] = "0";
                        Fila["PAGADO"] = "0";
                        Fila["DISPONIBLE"] = "0";
                        Fila["COMPROMETIDO"] = "0";
                        Fila["SIN_DEVENGAR"] = "0";
                        Fila["POR_PAGAR"] = "0";
                        Fila["TIPO"] = "PP";
                        Dt_Datos.Rows.Add(Fila);

                        P_ID = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                        DES_PP = HttpUtility.HtmlDecode(Dr["DES_PP"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["INDICE_COG"] = "** " + Dr["UR"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["DES_UR"].ToString().Trim();
                        Fila["APROBADO"] = "0";
                        Fila["AMPLIACION"] = "0";
                        Fila["REDUCCION"] = "0";
                        Fila["MODIFICADO"] = "0";
                        Fila["CARGO_C"] = "0";
                        Fila["ABONO_C"] = "0";
                        Fila["SALDO_C"] = "0";
                        Fila["CARGO_D"] = "0";
                        Fila["ABONO_D"] = "0";
                        Fila["SALDO_D"] = "0";
                        Fila["EJERCIDO"] = "0";
                        Fila["PAGADO"] = "0";
                        Fila["DISPONIBLE"] = "0";
                        Fila["COMPROMETIDO"] = "0";
                        Fila["SIN_DEVENGAR"] = "0";
                        Fila["POR_PAGAR"] = "0";
                        Fila["TIPO"] = "UR";
                        Dt_Datos.Rows.Add(Fila);

                        UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                        Fila = Dt_Datos.NewRow();
                        Fila["INDICE_COG"] = "* " +  Dr["FF"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["DES_FF"].ToString().Trim();
                        Fila["APROBADO"] = "0";
                        Fila["AMPLIACION"] = "0";
                        Fila["REDUCCION"] = "0";
                        Fila["MODIFICADO"] = "0";
                        Fila["CARGO_C"] = "0";
                        Fila["ABONO_C"] = "0";
                        Fila["SALDO_C"] = "0";
                        Fila["CARGO_D"] = "0";
                        Fila["ABONO_D"] = "0";
                        Fila["SALDO_D"] = "0";
                        Fila["EJERCIDO"] = "0";
                        Fila["PAGADO"] = "0";
                        Fila["DISPONIBLE"] = "0";
                        Fila["COMPROMETIDO"] = "0";
                        Fila["SIN_DEVENGAR"] = "0";
                        Fila["POR_PAGAR"] = "0";
                        Fila["TIPO"] = "FF";
                        Dt_Datos.Rows.Add(Fila);

                        FF_ID = Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim();

                        Fila = Dt_Datos.NewRow();
                        Fila["INDICE_COG"] = Dr["PARTIDA"].ToString().Trim();
                        Fila["CONCEPTO"] = Dr["DES_PARTIDA"].ToString().Trim();
                        Fila["APROBADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                        Fila["AMPLIACION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                        Fila["REDUCCION"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                        Fila["MODIFICADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                        Fila["CARGO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_C"].ToString().Trim()) ? "0" : Dr["CARGO_C"].ToString().Trim()));
                        Fila["ABONO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_C"].ToString().Trim()) ? "0" : Dr["ABONO_C"].ToString().Trim()));
                        Fila["SALDO_C"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_C"].ToString().Trim()) ? "0" : Dr["SALDO_C"].ToString().Trim()));
                        Fila["CARGO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["CARGO_D"].ToString().Trim()) ? "0" : Dr["CARGO_D"].ToString().Trim()));
                        Fila["ABONO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABONO_D"].ToString().Trim()) ? "0" : Dr["ABONO_D"].ToString().Trim()));
                        Fila["SALDO_D"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SALDO_D"].ToString().Trim()) ? "0" : Dr["SALDO_D"].ToString().Trim()));
                        Fila["EJERCIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["EJERCIDO"].ToString().Trim()) ? "0" : Dr["EJERCIDO"].ToString().Trim()));
                        Fila["PAGADO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PAGADO"].ToString().Trim()) ? "0" : Dr["PAGADO"].ToString().Trim()));
                        Fila["DISPONIBLE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                        Fila["COMPROMETIDO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                        Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SIN_DEVENGAR"].ToString().Trim()) ? "0" : Dr["SIN_DEVENGAR"].ToString().Trim()));
                        Fila["POR_PAGAR"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_PAGAR"].ToString().Trim()) ? "0" : Dr["POR_PAGAR"].ToString().Trim()));
                        Fila["TIPO"] = "PARTIDA";
                        Dt_Datos.Rows.Add(Fila);
                    }//FIN ELSE AF
                }//fin foreach

                Fila = Dt_Datos.NewRow();
                Fila["INDICE_COG"] = "";
                Fila["CONCEPTO"] = " Total Conceptos ";
                Fila["APROBADO"] = String.Format("{0:n}", Tot_Aprobado);
                Fila["AMPLIACION"] = String.Format("{0:n}", Tot_Ampliacion);
                Fila["REDUCCION"] = String.Format("{0:n}", Tot_Reduccion);
                Fila["MODIFICADO"] = String.Format("{0:n}", Tot_Modificado);
                Fila["CARGO_C"] = String.Format("{0:n}", Tot_Cargo_C);
                Fila["ABONO_C"] = String.Format("{0:n}", Tot_Abono_C);
                Fila["SALDO_C"] = String.Format("{0:n}", Tot_Saldo_C);
                Fila["CARGO_D"] = String.Format("{0:n}", Tot_Cargo_D);
                Fila["ABONO_D"] = String.Format("{0:n}", Tot_Abono_D);
                Fila["SALDO_D"] = String.Format("{0:n}", Tot_Saldo_D);
                Fila["EJERCIDO"] = String.Format("{0:n}", Tot_Ejercido);
                Fila["PAGADO"] = String.Format("{0:n}", Tot_Pagado);
                Fila["DISPONIBLE"] = String.Format("{0:n}", Tot_Disponible);
                Fila["COMPROMETIDO"] = String.Format("{0:n}", Tot_Comprometido);
                Fila["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_Sin_Devengar);
                Fila["POR_PAGAR"] = String.Format("{0:n}", Tot_Por_Pagar);
                Fila["TIPO"] = "TOTAL";
                Dt_Datos.Rows.Add(Fila);

                foreach (DataRow Dr_P in Dt_Datos.Rows)
                {
                    if (Dr_P["TIPO"].ToString().Trim().Equals("AF"))
                    {
                        Dr_P["APROBADO"] = String.Format("{0:n}", Tot_AF_Aprobado);
                        Dr_P["AMPLIACION"] = String.Format("{0:n}", Tot_AF_Ampliacion);
                        Dr_P["REDUCCION"] = String.Format("{0:n}", Tot_AF_Reduccion);
                        Dr_P["MODIFICADO"] = String.Format("{0:n}", Tot_AF_Modificado);
                        Dr_P["CARGO_C"] = String.Format("{0:n}", Tot_AF_Cargo_C);
                        Dr_P["ABONO_C"] = String.Format("{0:n}", Tot_AF_Abono_C);
                        Dr_P["SALDO_C"] = String.Format("{0:n}", Tot_AF_Saldo_C);
                        Dr_P["CARGO_D"] = String.Format("{0:n}", Tot_AF_Cargo_D);
                        Dr_P["ABONO_D"] = String.Format("{0:n}", Tot_AF_Abono_D);
                        Dr_P["SALDO_D"] = String.Format("{0:n}", Tot_AF_Saldo_D);
                        Dr_P["EJERCIDO"] = String.Format("{0:n}", Tot_AF_Ejercido);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_AF_Pagado);
                        Dr_P["DISPONIBLE"] = String.Format("{0:n}", Tot_AF_Disponible);
                        Dr_P["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_AF_Sin_Devengar);
                        Dr_P["POR_PAGAR"] = String.Format("{0:n}", Tot_AF_Por_Pagar);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_AF_Pagado);
                        Dr_P["TIPO"] = "COGT";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("PP"))
                    {
                        Dr_P["APROBADO"] = String.Format("{0:n}", Tot_P_Aprobado);
                        Dr_P["AMPLIACION"] = String.Format("{0:n}", Tot_P_Ampliacion);
                        Dr_P["REDUCCION"] = String.Format("{0:n}", Tot_P_Reduccion);
                        Dr_P["MODIFICADO"] = String.Format("{0:n}", Tot_P_Modificado);
                        Dr_P["CARGO_C"] = String.Format("{0:n}", Tot_P_Cargo_C);
                        Dr_P["ABONO_C"] = String.Format("{0:n}", Tot_P_Abono_C);
                        Dr_P["SALDO_C"] = String.Format("{0:n}", Tot_P_Saldo_C);
                        Dr_P["CARGO_D"] = String.Format("{0:n}", Tot_P_Cargo_D);
                        Dr_P["ABONO_D"] = String.Format("{0:n}", Tot_P_Abono_D);
                        Dr_P["SALDO_D"] = String.Format("{0:n}", Tot_P_Saldo_D);
                        Dr_P["EJERCIDO"] = String.Format("{0:n}", Tot_P_Ejercido);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                        Dr_P["DISPONIBLE"] = String.Format("{0:n}", Tot_P_Disponible);
                        Dr_P["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_P_Sin_Devengar);
                        Dr_P["POR_PAGAR"] = String.Format("{0:n}", Tot_P_Por_Pagar);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_P_Pagado);
                        Dr_P["TIPO"] = "COGTP";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                    {
                        Dr_P["APROBADO"] = String.Format("{0:n}", Tot_UR_Aprobado);
                        Dr_P["AMPLIACION"] = String.Format("{0:n}", Tot_UR_Ampliacion);
                        Dr_P["REDUCCION"] = String.Format("{0:n}", Tot_UR_Reduccion);
                        Dr_P["MODIFICADO"] = String.Format("{0:n}", Tot_UR_Modificado);
                        Dr_P["CARGO_C"] = String.Format("{0:n}", Tot_UR_Cargo_C);
                        Dr_P["ABONO_C"] = String.Format("{0:n}", Tot_UR_Abono_C);
                        Dr_P["SALDO_C"] = String.Format("{0:n}", Tot_UR_Saldo_C);
                        Dr_P["CARGO_D"] = String.Format("{0:n}", Tot_UR_Cargo_D);
                        Dr_P["ABONO_D"] = String.Format("{0:n}", Tot_UR_Abono_D);
                        Dr_P["SALDO_D"] = String.Format("{0:n}", Tot_UR_Saldo_D);
                        Dr_P["EJERCIDO"] = String.Format("{0:n}", Tot_UR_Ejercido);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                        Dr_P["DISPONIBLE"] = String.Format("{0:n}", Tot_UR_Disponible);
                        Dr_P["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_UR_Sin_Devengar);
                        Dr_P["POR_PAGAR"] = String.Format("{0:n}", Tot_UR_Por_Pagar);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_UR_Pagado);
                        if (DES_PP.Trim().Equals(HttpUtility.HtmlDecode("DIRECCIÓN GENERAL")))
                        {
                            Dr_P["TIPO"] = "COGTP";
                        }
                        else
                        {
                            Dr_P["TIPO"] = "COGT";
                        }
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("FF"))
                    {
                        Dr_P["APROBADO"] = String.Format("{0:n}", Tot_FF_Aprobado);
                        Dr_P["AMPLIACION"] = String.Format("{0:n}", Tot_FF_Ampliacion);
                        Dr_P["REDUCCION"] = String.Format("{0:n}", Tot_FF_Reduccion);
                        Dr_P["MODIFICADO"] = String.Format("{0:n}", Tot_FF_Modificado);
                        Dr_P["CARGO_C"] = String.Format("{0:n}", Tot_FF_Cargo_C);
                        Dr_P["ABONO_C"] = String.Format("{0:n}", Tot_FF_Abono_C);
                        Dr_P["SALDO_C"] = String.Format("{0:n}", Tot_FF_Saldo_C);
                        Dr_P["CARGO_D"] = String.Format("{0:n}", Tot_FF_Cargo_D);
                        Dr_P["ABONO_D"] = String.Format("{0:n}", Tot_FF_Abono_D);
                        Dr_P["SALDO_D"] = String.Format("{0:n}", Tot_FF_Saldo_D);
                        Dr_P["EJERCIDO"] = String.Format("{0:n}", Tot_FF_Ejercido);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                        Dr_P["DISPONIBLE"] = String.Format("{0:n}", Tot_FF_Disponible);
                        Dr_P["SIN_DEVENGAR"] = String.Format("{0:n}", Tot_FF_Sin_Devengar);
                        Dr_P["POR_PAGAR"] = String.Format("{0:n}", Tot_FF_Por_Pagar);
                        Dr_P["PAGADO"] = String.Format("{0:n}", Tot_FF_Pagado);
                        Dr_P["TIPO"] = "COGT";
                        break;
                    }
                    
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar los datos de economias: Error[" + Ex.Message + "]");
            }
            return Dt_Datos;
        }

    #endregion

    #region EVENTOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 24/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Generar_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Presupuestal_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Presupuestal_Negocio(); //Conexion con la capa de negocios
            DataTable Dt_EAIP = new DataTable(); //Para almacenar los datos de las ordenes de compras
            DataTable Dt_EAEPE = new DataTable(); //Para almacenar los datos de las ordenes de compras
            DataTable Dt_EAEPEFP = new DataTable();
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;

            try
            {
                if (Validar_Campos())
                {
                    Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Dt_EAIP = Obj_Ingresos.Consultar_Analitico_Ingresos();
                    Dt_EAEPE = Obj_Ingresos.Consultar_Analitico_Ejercido_Psp_Egresos();
                    Dt_EAEPEFP = Obj_Ingresos.Consultar_Analitico_Ejercido_Psp_Egresos_COG();

                    if (Dt_EAIP != null || Dt_EAEPE != null || Dt_EAEPEFP != null)
                    {
                        if (Dt_EAIP.Rows.Count > 0 || Dt_EAEPE.Rows.Count > 0 || Dt_EAEPEFP.Rows.Count > 0)
                        {
                            Generar_Rpt(Dt_EAIP, Dt_EAEPE, Dt_EAEPEFP);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }
    #endregion
}
