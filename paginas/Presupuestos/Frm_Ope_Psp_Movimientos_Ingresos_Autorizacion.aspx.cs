﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
//using JAPAMI.Cat_Psp_Rubros.Negocio;
//using JAPAMI.Cat_Psp_Tipos.Negocio;
//using JAPAMI.Cat_Psp_Clases_Ing.Negocio;
//using JAPAMI.Ope_Psp_Pronosticos_Ingresos.Negocio;
using JAPAMI.Ope_Psp_Movimientos_Ingresos.Negocio;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Movimientos_Ingresos_Autorizacion : System.Web.UI.Page
{
    #region (Page_Load)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Metodo de inicio de la pagina
    ///PARAMETROS           :
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        try
        {
            if (!IsPostBack)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Configuracion_Inicial();
                ViewState["SortDirection"] = "DESC";
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de movimientos de ingresos. Error[" + Ex.Message + "]", true);
        }
    }
    #endregion

    #region (Metodos)
    #region (Generales)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Configuracion_Inicial
    ///DESCRIPCIÓN          : Metodo para cargar la configuracion inicial de la pagina
    ///PARAMETROS           1: Estatus: true o false para habilitar los controles
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Configuracion_Inicial()
    {
        try
        {
            Mostar_Limpiar_Error(String.Empty, String.Empty, false);
            Limpiar_Forma();
            Habilitar_Forma(false);
            Estado_Botones("Inicial");
            Llenar_Combo_Estatus();
            Llenar_Grid_Movimientos();
            Div_Grid_Movimientos.Visible = true;
            Div_Datos.Visible = false;
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de Movimiento de ingresos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
    ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
    ///PARAMETROS           1: Estatus: true o false para habilitar los controles
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Habilitar_Forma(Boolean Estatus)
    {
        Txt_Importe.Enabled = false;
        Txt_Enero.Enabled = false;
        Txt_Febrero.Enabled = false;
        Txt_Marzo.Enabled = false;
        Txt_Abril.Enabled = false;
        Txt_Mayo.Enabled = false;
        Txt_Junio.Enabled = false;
        Txt_Julio.Enabled = false;
        Txt_Agosto.Enabled = false;
        Txt_Septiembre.Enabled = false;
        Txt_Octubre.Enabled = false;
        Txt_Noviembre.Enabled = false;
        Txt_Diciembre.Enabled = false;
        Cmb_Rubro.Enabled = false;
        Cmb_FF.Enabled = false;
        Cmb_Operacion.Enabled = false;
        Cmb_Estatus.Enabled = false;
        Txt_Justificacion.Enabled = false;
        Txt_No_Solicitud.Enabled = false;
        Txt_Comentario.Enabled = Estatus;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
    ///DESCRIPCIÓN          : Metodo para limpiar los controles
    ///PARAMETROS           :
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Limpiar_Forma()
    {
        Txt_Importe.Text = String.Empty;
        Txt_Enero.Text = String.Empty;
        Txt_Febrero.Text = String.Empty;
        Txt_Marzo.Text = String.Empty;
        Txt_Abril.Text = String.Empty;
        Txt_Mayo.Text = String.Empty;
        Txt_Junio.Text = String.Empty;
        Txt_Julio.Text = String.Empty;
        Txt_Agosto.Text = String.Empty;
        Txt_Septiembre.Text = String.Empty;
        Txt_Octubre.Text = String.Empty;
        Txt_Noviembre.Text = String.Empty;
        Txt_Diciembre.Text = String.Empty;
        Cmb_Rubro.SelectedIndex = -1;
        Cmb_FF.SelectedIndex = -1;
        Cmb_Operacion.SelectedIndex = -1;
        Cmb_Estatus.SelectedIndex = -1;
        Txt_Justificacion.Text = String.Empty;
        Txt_No_Solicitud.Text = String.Empty;
        Hf_Rubro_ID.Value = String.Empty;
        Hf_Tipo_ID.Value = String.Empty;
        Hf_Clase_ID.Value = String.Empty;
        Hf_Concepto_ID.Value = String.Empty;
        Hf_Modificado.Value = String.Empty;
        Txt_Comentario.Text = String.Empty;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
    ///DESCRIPCIÓN          : Metodo para limpiar los controles
    ///PARAMETROS           :
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
    {
        Lbl_Encabezado_Error.Text = Encabezado_Error;
        Lbl_Mensaje_Error.Text = Mensaje_Error;
        Lbl_Mensaje_Error.Visible = Mostrar;
        Td_Error.Visible = Mostrar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Estado_Botones
    ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
    ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Estado_Botones(String Estado)
    {
        switch (Estado)
        {
            case "Inicial":
                //Boton Nuevo
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.Enabled = true;
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Salir
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.Enabled = true;
                Btn_Salir.Visible = true;
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                break;
            case "Nuevo":
                //Boton Nuevo
                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Nuevo.Enabled = true;
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                //Boton Salir
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.Enabled = true;
                Btn_Salir.Visible = true;
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                break;
        }//fin del switch
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Datos
    ///DESCRIPCIÓN          : metodo para validar los datos del formulario
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public Boolean Validar_Datos()
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Rubros = new DataTable();
        String Mensaje_Encabezado = "Es necesario: ";
        String Mensaje_Error = String.Empty;
        Boolean Datos_Validos = true;
        Double Total = 0.00;
        Double Importe = 0.00;

        try
        {
            if (String.IsNullOrEmpty(Txt_Importe.Text.Trim()))
            {
                Mensaje_Error += "&nbsp;&nbsp;* Instroducir un importe. <br />";
                Datos_Validos = false;
            }
            else
            {
                if (!String.IsNullOrEmpty(Hf_Modificado.Value.Trim()))
                {
                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim().Replace(",", ""));
                    Total = Convert.ToDouble(String.IsNullOrEmpty(Hf_Modificado.Value.Trim()) ? "0" : Hf_Modificado.Value.Trim().Replace(",", ""));


                    if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("REDUCCION") && Cmb_Estatus.SelectedItem.Text.Trim().Equals("AUTORIZADA")) 
                    {
                        if (Importe > Total)
                        {
                            Mensaje_Error += "&nbsp;&nbsp;* No se puede realizar el proceso, pues ya no se cuenta con el monto suficiente para realizar la reducción. <br />";
                            Datos_Validos = false;
                            Btn_Nuevo.Visible = false;
                        }
                    }
                }
            }


            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Mensaje_Error += "&nbsp;&nbsp;* Seleccionar una fuente de financiamiento. <br />";
                Datos_Validos = false;
            }
            else 
            {
                if (Cmb_Estatus.SelectedItem.Text.Trim().Equals("RECHAZADA"))
                {
                    if (String.IsNullOrEmpty(Txt_Comentario.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir un comentario. <br />";
                        Datos_Validos = false;
                    }
                }
            }

            if (!Datos_Validos)
            {
                Mostar_Limpiar_Error(Mensaje_Encabezado, Mensaje_Error, true);
            }

        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al validar los datos. Error[" + Ex.Message + "]", true);
        }
        return Datos_Validos;
    }
    #endregion

    #region (Combos / Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
    ///DESCRIPCIÓN          : Metodo para llenar el combo de estatus
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Estatus()
    {
        Cmb_Estatus.Items.Clear();
        Cmb_Estatus.Items.Insert(0, new ListItem("<-SELECCIONE->", ""));
        Cmb_Estatus.Items.Insert(1, new ListItem("RECHAZADA", "RECHAZADA"));
        Cmb_Estatus.Items.Insert(2, new ListItem("AUTORIZADA", "AUTORIZADA"));
        Cmb_Estatus.DataBind();
        Cmb_Estatus.SelectedIndex = -1;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los movimientos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Grid_Movimientos()
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Movimienos = new DataTable();

        try
        {
            Negocio.P_Estatus = "GENERADA";
            Dt_Movimienos = Negocio.Consultar_Movimientos();
            if (Dt_Movimienos != null)
            {
                Grid_Movimiento.Columns[6].Visible = true;
                Grid_Movimiento.Columns[7].Visible = true;
                Grid_Movimiento.Columns[8].Visible = true;
                Grid_Movimiento.Columns[9].Visible = true;
                Grid_Movimiento.Columns[10].Visible = true;
                Grid_Movimiento.Columns[11].Visible = true;
                Grid_Movimiento.Columns[12].Visible = true;
                Grid_Movimiento.Columns[13].Visible = true;
                Grid_Movimiento.Columns[14].Visible = true;
                Grid_Movimiento.Columns[15].Visible = true;
                Grid_Movimiento.Columns[16].Visible = true;
                Grid_Movimiento.Columns[17].Visible = true;
                Grid_Movimiento.Columns[18].Visible = true;
                Grid_Movimiento.Columns[19].Visible = true;
                Grid_Movimiento.Columns[20].Visible = true;
                Grid_Movimiento.Columns[21].Visible = true;
                Grid_Movimiento.Columns[22].Visible = true;
                Grid_Movimiento.Columns[23].Visible = true;
                Grid_Movimiento.Columns[24].Visible = true;
                Grid_Movimiento.Columns[25].Visible = true;
                Grid_Movimiento.Columns[26].Visible = true;
                Grid_Movimiento.DataSource = Dt_Movimienos;
                Grid_Movimiento.DataBind();
                Grid_Movimiento.Columns[6].Visible = false;
                Grid_Movimiento.Columns[7].Visible = false;
                Grid_Movimiento.Columns[8].Visible = false;
                Grid_Movimiento.Columns[9].Visible = false;
                Grid_Movimiento.Columns[10].Visible = false;
                Grid_Movimiento.Columns[11].Visible = false;
                Grid_Movimiento.Columns[12].Visible = false;
                Grid_Movimiento.Columns[13].Visible = false;
                Grid_Movimiento.Columns[14].Visible = false;
                Grid_Movimiento.Columns[15].Visible = false;
                Grid_Movimiento.Columns[16].Visible = false;
                Grid_Movimiento.Columns[17].Visible = false;
                Grid_Movimiento.Columns[18].Visible = false;
                Grid_Movimiento.Columns[19].Visible = false;
                Grid_Movimiento.Columns[20].Visible = false;
                Grid_Movimiento.Columns[21].Visible = false;
                Grid_Movimiento.Columns[22].Visible = false;
                Grid_Movimiento.Columns[23].Visible = false;
                Grid_Movimiento.Columns[24].Visible = false;
                Grid_Movimiento.Columns[25].Visible = false;
                Grid_Movimiento.Columns[26].Visible = false;
            }
            else
            {
                Grid_Movimiento.DataSource = new DataTable();
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de Movimientos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Rubros
    ///DESCRIPCIÓN          : Metodo para llenar el combo de los rubros del presupuesto de ingresos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Rubros_Ingresos()
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Rubros = new DataTable();

        try
        {
            Cmb_Rubro.Items.Clear();

            if (Cmb_FF.SelectedIndex > 0)
            {
                Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fte_Financiamiento = String.Empty;
            }

            Dt_Rubros = Negocio.Consultar_Rubros();
            if (Dt_Rubros != null)
            {
                Cmb_Rubro.DataSource = Dt_Rubros;
                Cmb_Rubro.DataTextField = "CLAVE_NOMBRE";
                Cmb_Rubro.DataValueField = Cat_Psp_Rubro.Campo_Rubro_ID;
                Cmb_Rubro.DataBind();
                Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_Rubro.SelectedIndex = 1;
            }
            else
            {
                Cmb_Rubro.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de Rubros. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento_Ingresos
    ///DESCRIPCIÓN          : Metodo para llenar el combo de las fuentes de financiamiento del presupuesto de ingresos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Fuente_Financiamiento_Ingresos()
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
        DataTable Dt_FF = new DataTable();

        try
        {
            Cmb_FF.Items.Clear();
            Dt_FF = Negocio.Consultar_FF();
            if (Dt_FF != null)
            {
                Cmb_FF.DataSource = Dt_FF;
                Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Cmb_FF.DataBind();
                Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                Cmb_FF.SelectedIndex = 1;
            }
            else
            {
                Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar el combo de fuentes de financiamiento. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Conceptos_Ingresos
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los conceptos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Grid_Conceptos_Ingresos()
    {
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Conceptos = new DataTable();

        try
        {
            if (Cmb_FF.SelectedIndex > 0)
            {
                Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fte_Financiamiento = String.Empty;
            }

            if (Cmb_Rubro.SelectedIndex > 0)
            {
                Negocio.P_Rubro_ID = Cmb_Rubro.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Rubro_ID = String.Empty;
            }

            if (!String.IsNullOrEmpty(Hf_Concepto_ID.Value.Trim()))
            {
                Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
            }
            else
            {
                Negocio.P_Concepto_ID = String.Empty;
            }

            
            Dt_Conceptos = Negocio.Consultar_Conceptos();

            if (Dt_Conceptos != null)
            {
                Grid_Conceptos.Columns[0].Visible = true;
                Grid_Conceptos.Columns[6].Visible = true;
                Grid_Conceptos.Columns[7].Visible = true;
                Grid_Conceptos.Columns[8].Visible = true;
                Grid_Conceptos.Columns[9].Visible = true;
                Grid_Conceptos.Columns[10].Visible = true;
                Grid_Conceptos.Columns[11].Visible = true;
                Grid_Conceptos.Columns[12].Visible = true;
                Grid_Conceptos.Columns[13].Visible = true;
                Grid_Conceptos.Columns[14].Visible = true;
                Grid_Conceptos.Columns[15].Visible = true;
                Grid_Conceptos.Columns[16].Visible = true;
                Grid_Conceptos.Columns[17].Visible = true;
                Grid_Conceptos.Columns[18].Visible = true;
                Grid_Conceptos.Columns[19].Visible = true;
                Grid_Conceptos.Columns[20].Visible = true;
                Grid_Conceptos.Columns[21].Visible = true;
                Grid_Conceptos.Columns[22].Visible = true;
                Grid_Conceptos.DataSource = Dt_Conceptos;
                Grid_Conceptos.DataBind();
                Grid_Conceptos.Columns[6].Visible = false;
                Grid_Conceptos.Columns[7].Visible = false;
                Grid_Conceptos.Columns[8].Visible = false;
                Grid_Conceptos.Columns[9].Visible = false;
                Grid_Conceptos.Columns[10].Visible = false;
                Grid_Conceptos.Columns[11].Visible = false;
                Grid_Conceptos.Columns[12].Visible = false;
                Grid_Conceptos.Columns[13].Visible = false;
                Grid_Conceptos.Columns[14].Visible = false;
                Grid_Conceptos.Columns[15].Visible = false;
                Grid_Conceptos.Columns[16].Visible = false;
                Grid_Conceptos.Columns[17].Visible = false;
                Grid_Conceptos.Columns[18].Visible = false;
                Grid_Conceptos.Columns[19].Visible = false;
                Grid_Conceptos.Columns[20].Visible = false;
                Grid_Conceptos.Columns[21].Visible = false;
                Grid_Conceptos.Columns[22].Visible = false;

                Hf_Modificado.Value = Dt_Conceptos.Rows[0]["MODIFICADO"].ToString().Trim();
            }
            else
            {
                Grid_Conceptos.DataSource = new DataTable();
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de conceptos. Error[" + Ex.Message + "]", true);
        }
    }

    #endregion
    #endregion

    #region (Eventos)
    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton Salir 
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        switch (Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Configuracion_Inicial();
                Grid_Movimiento.SelectedIndex = -1;
                break;

            case "Regresar":
                Configuracion_Inicial();
                Grid_Movimiento.SelectedIndex = -1;
                break;

            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                break;
        }//fin del switch
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Evento del boton Guardar
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        Cls_Ope_Psp_Movimientos_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Movimientos_Ingresos_Negocio();//Variable de conexion con la capa de negocios.
        try
        {
            switch (Btn_Nuevo.ToolTip)
            {
                case "Nuevo":
                    if (Grid_Movimiento.SelectedIndex < 0)
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar un movimiento de la tabla.", String.Empty, true);
                    }
                    break;
                case "Guardar":
                    if (Validar_Datos())
                    {
                        //Negocio.P_Importe = Txt_Importe.Text.Trim().Replace(",", "");
                        //Negocio.P_Justificacion = Txt_Justificacion.Text.Trim().ToUpper();
                        //Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                        //Negocio.P_Operacion = Cmb_Operacion.SelectedItem.Value.Trim();
                        //Negocio.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                        //Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                        //Negocio.P_Imp_Ene = Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Feb = Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Mar = Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Abr = Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_May = Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Jun = Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Jul = Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Ago = Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Sep = Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Oct = Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Nov = Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Imp_Dic = Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim().Replace(",", "")).ToString();
                        //Negocio.P_Rubro_ID = Hf_Rubro_ID.Value.Trim();
                        //Negocio.P_Clase_ID = Hf_Clase_ID.Value.Trim();
                        //Negocio.P_Concepto_ID = Hf_Concepto_ID.Value.Trim();
                        //Negocio.P_Tipo_ID = Hf_Tipo_ID.Value.Trim();
                        //Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        //Negocio.P_No_Movimiento = Txt_No_Solicitud.Text.Trim();
                        //Negocio.P_Tipo_Movimiento = Hf_tipo_Concepto.Value.Trim();
                        //Negocio.P_Comentario = Txt_Comentario.Text.Trim();
                        
                        //if (Negocio.Modificar_Movimientos())
                        //{
                        //    Configuracion_Inicial();
                        //    Grid_Movimiento.SelectedIndex = -1;

                        //    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                        //}
                    }
                    break;
            }//fin del swirch
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al guardar el movimiento. Error[" + Ex.Message + "]", true);
        }
    }//fin del boton Nuevo

    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion del grid
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Movimiento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        try
        {
            if (Grid_Movimiento.SelectedIndex > (-1))
            {
                Limpiar_Forma();

                Txt_No_Solicitud.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[1].Text).ToString();
                Txt_Importe.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[4].Text).ToString(); ;
                Txt_Justificacion.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[23].Text).ToString(); ;
                Txt_Importe.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[4].Text).ToString(); ;
                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[5].Text).ToString()));
                Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[3].Text).ToString()));
                Llenar_Combo_Fuente_Financiamiento_Ingresos();
                Cmb_FF.SelectedIndex = Cmb_FF.Items.IndexOf(Cmb_FF.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[10].Text).ToString()));
                Llenar_Combo_Rubros_Ingresos();
                Cmb_Rubro.SelectedIndex = Cmb_Rubro.Items.IndexOf(Cmb_Rubro.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[6].Text).ToString()));

                Hf_Rubro_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[6].Text).ToString();
                Hf_Tipo_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[7].Text).ToString();
                Hf_Clase_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[8].Text).ToString();
                Hf_Concepto_ID.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[9].Text).ToString();
                Hf_tipo_Concepto.Value = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[26].Text).ToString();
                
                Llenar_Grid_Conceptos_Ingresos();

                Txt_Enero.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[11].Text).ToString();
                Txt_Febrero.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[12].Text).ToString();
                Txt_Marzo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[13].Text).ToString();
                Txt_Abril.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[14].Text).ToString();
                Txt_Mayo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[15].Text).ToString();
                Txt_Junio.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[16].Text).ToString();
                Txt_Julio.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[17].Text).ToString();
                Txt_Agosto.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[18].Text).ToString();
                Txt_Septiembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[19].Text).ToString();
                Txt_Octubre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[20].Text).ToString();
                Txt_Noviembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[21].Text).ToString();
                Txt_Diciembre.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[22].Text).ToString();

                Estado_Botones("Inicial");
                Habilitar_Forma(false);
                Tabla_Meses.Visible = true;
                Div_Datos.Visible = true;
                Div_Grid_Movimientos.Visible = false;
                Grid_Conceptos.Columns[0].Visible = false;

                Estado_Botones("Nuevo");
                Habilitar_Forma(true);
                Txt_Comentario.Text = String.Empty;
                Cmb_Estatus.Enabled = true;
                Cmb_Estatus.SelectedIndex = -1;
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al cargar los datos del movimientos. Error[" + Ex.Message + "]", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_Sorting
    ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Movimiento_Sorting(object sender, GridViewSortEventArgs e)
    {
        Mostar_Limpiar_Error(String.Empty, String.Empty, false);
        DataTable Dt_Movimientos = new DataTable();

        try
        {
            Llenar_Grid_Movimientos();
            Dt_Movimientos = (DataTable)Grid_Movimiento.DataSource;
            if (Dt_Movimientos != null)
            {
                DataView Dv_Vista = new DataView(Dt_Movimientos);
                String Orden = ViewState["SortDirection"].ToString();
                if (Orden.Equals("ASC"))
                {
                    Dv_Vista.Sort = e.SortExpression + " DESC";
                    ViewState["SortDirection"] = "DESC";
                }
                else
                {
                    Dv_Vista.Sort = e.SortExpression + " ASC";
                    ViewState["SortDirection"] = "ASC";
                }
                Grid_Movimiento.DataSource = Dv_Vista;
                Grid_Movimiento.DataBind();
            }
        }
        catch (Exception Ex)
        {
            Mostar_Limpiar_Error(String.Empty, "Error al ordenar la tabla de Movimientos. Error[" + Ex.Message + "]", true);
        }
    }
    #endregion
    #endregion
}
