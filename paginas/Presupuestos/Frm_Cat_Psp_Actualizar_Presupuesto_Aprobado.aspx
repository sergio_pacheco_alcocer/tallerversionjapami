﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"  AutoEventWireup="true" 
CodeFile="Frm_Cat_Psp_Actualizar_Presupuesto_Aprobado.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Cat_Psp_Actualizar_Presupuesto_Aprobado" Title="SIAC Sistema Integral Administrativo y Comercial"  %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
       <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
     <asp:UpdatePanel ID="Upd_Panel" runat="server">
            <ContentTemplate>
                <div id="Div_Actuializar_Presupuesto_Aprbado" >
                    <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">Actualizar Presupuesto Aprobado</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>
                        </tr>               
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                    TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                            </td>
                            <td style="width:50%"></td> 
                        </tr>
                    </table>
                     <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:25%">
                                       Ruta del Archivo
                            </td>
                            <td colspan="2" align="left"  >
                                <cc1:AsyncFileUpload ID="AFU_Archivo_Excel" runat="server" Width="45%"  UploadingBackColor="LightBlue"
                                            ThrobberID="Throbber" onuploadedcomplete="AFU_Archivo_Excel_UploadedComplete" />
                            </td> 
                        </tr> 
                         <tr>
                            <td colspan="3">

                            </td>
                        </tr> 
                        <tr>
                            <td style="width:25%">
                                 <asp:Button ID="Btn_Cargar_Presupuesto_Grid" runat="server"  
                                     Text="Cargar Presupuesto" CssClass="button"  
                                              Width="200px" onclick="Btn_Cargar_Presupuesto_Grid_Click" /> 
                            </td>
                             <td style="width:25%">
                                 <asp:Button ID="Btn_Actualizar_Presupuesto" runat="server"  
                                     Text="Actualizar Presupuesto" CssClass="button"  
                                   Width="200px" onclick="Btn_Actualizar_Presupuesto_Click"/> 
                            </td>
                             <td style="width:25%">

                            </td>
                        </tr> 
                     </table>
                      <div style="height:auto; max-height:300px; overflow:auto; width:100%; vertical-align:top;">
                          <table style="width:100%;">
                                <tr>
                            <td>
                                <asp:GridView ID="Grid_Presupuesto" runat="server" AutoGenerateColumns="False" 
                                CssClass="GridView_1" AllowPaging="True" PageSize="10" 
                                GridLines="None" Width = "98%" OnPageIndexChanging ="Grid_Presupuesto_PageIndexChanging"
                                > 
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="FUENTE_FINANCIAMIENTO" HeaderText="Fuente de financiamiento">
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="XX-Small" />                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="Area funcional">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="UNIDAD_RESPONSABLE" HeaderText="Unidad Responsable">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%"  Font-Size="XX-Small" />                                                                            
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PROGRAMA" HeaderText="Programa">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CAPITULO" HeaderText="Capitulo">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PARTIDA" HeaderText="Partida">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="SUBNIVEL" HeaderText="Subnivel">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>  
                                     <asp:BoundField DataField="ANIO" HeaderText="Año">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="APROBADO" HeaderText="$Aprobado">
                                        <HeaderStyle HorizontalAlign="Right" Width="12%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="AMPLIACION" HeaderText="$Ampliacion">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="REDUCCION" HeaderText="$Reduccion">
                                        <HeaderStyle HorizontalAlign="Right" Width="35%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="35%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="MODIFICADO" HeaderText="$Modificado">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="DISPONIBLE" HeaderText="$Disponible">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PRE_COMPROMETIDO" HeaderText="$Pre_Comprometido">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMPROMETIDO" HeaderText="$Comprometido">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DEVENGADO" HeaderText="$Devengado">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                     <asp:BoundField DataField="EJERCIDO" HeaderText="$Ejercido">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="PAGADO" HeaderText="$Pagado">
                                        <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="SALDO" HeaderText="$Saldo">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                       <asp:BoundField DataField="ENERO" HeaderText="Enero">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FEBRERO" HeaderText="Febrero">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="MARZO" HeaderText="Marzo">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="ABRIL" HeaderText="Abril">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MAYO" HeaderText="Mayo">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>  
                                      <asp:BoundField DataField="JUNIO" HeaderText="Junio">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="JULIO" HeaderText="Julio">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                     <asp:BoundField DataField="AGOSTO" HeaderText="Agosto">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                    <asp:BoundField DataField="JUNIO" HeaderText="Junio">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="JULIO" HeaderText="Julio">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                     <asp:BoundField DataField="AGOSTO" HeaderText="Agosto">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>     
                                    <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                     <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre">
                                        <HeaderStyle HorizontalAlign="Right" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Right" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                     <asp:BoundField DataField="TOTAL" HeaderText="Total">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>                                  
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />                                
                                <AlternatingRowStyle CssClass="GridAltItem" />                                
                            </asp:GridView>
                            </td>
                        </tr>
                          </table>
                     </div>
                </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>