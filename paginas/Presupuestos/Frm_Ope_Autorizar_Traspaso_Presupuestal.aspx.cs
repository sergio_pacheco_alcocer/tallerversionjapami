﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Paramentros_Presupuestos.Negocio;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using JAPAMI.Constantes;

public partial class paginas_presupuestos_Frm_Ope_Autorizar_Traspaso_Presupuestal : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region(Metodos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Inicializa_controles
        ///DESCRIPCIÓN          : metodo para inicializar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Habilitar_Controles("Inicial");//Inicializa todos los controles
                Obtener_Datos_Usuario();
                Llenar_Grid_Modificaciones();
                Div_Grid_Movimientos.Visible = false;
                Div_Grid_Modificaciones.Visible = true;
                Tabla_Busqueda.Visible = false;
                Chk_Autorizar.Checked = false;
                Hf_No_Movimiento_Egr.Value = String.Empty;
                Hf_No_Rechazado.Value = String.Empty;
                Hf_Estatus.Value = String.Empty;
                Txt_Busqueda.Text = String.Empty;
                Chk_Aceptacion.Checked = false;
                Chk_Autorizar.Checked = false;
                Chk_Reabrir.Checked = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Inicializa_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Controles
        ///DESCRIPCIÓN          : metodo para habililitar los controles
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            try
            {
                switch (Operacion)
                {
                    case "Inicial":
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Nuevo.Visible = false;
                        Btn_Nuevo.CausesValidation = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";

                        break;

                    case "Nuevo":
                        Btn_Nuevo.ToolTip = "Actualizar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        break;

                }
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de Movimientos
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Movimientos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimiento; //Variable que obtendra los datos de la consulta 
            DataTable Dt_Tipo_Mod = new DataTable();
            DataRow Fila;
            Boolean Ampliacion = false;
            Boolean Reduccion = false;
            Boolean Traspaso = false;
            Double Total = 0.00;
            DataTable Dt_Mov = new DataTable();

            try
            {
                Grid_Tipo_Modificacion.DataBind();
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Consulta.P_Tipo_Egreso = "RAMO33";
                    if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                    {
                        Consulta.P_Estatus = "'GENERADO', 'ACEPTADO'";
                    }
                    else
                    {
                        Consulta.P_Estatus = "'" + Hf_Estatus.Value.Trim() + "'";
                    }
                }
                else
                {
                    Consulta.P_Tipo_Egreso = "MUNICIPAL";
                    if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                    {
                        Consulta.P_Estatus = "'RECIBIDO', 'ACEPTADO'";
                    }
                    else
                    {
                        Consulta.P_Estatus = "'" + Hf_Estatus.Value.Trim() + "'";
                    }
                }

                Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Consulta.P_No_Solicitud = Hf_No_Movimiento_Egr.Value.Trim();
                Dt_Movimiento = Consulta.Consultar_Datos_Movimientos();

                if (Dt_Movimiento != null)
                {
                    if (Dt_Movimiento.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                        {
                            Dt_Mov = (from Fila_Tot in Dt_Movimiento.AsEnumerable()
                                      where Fila_Tot.Field<string>("TIPO_OPERACION") != "ADECUACION"
                                      && (Convert.ToString(Fila_Tot.Field<String>("NO_SOLICITUD")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<Decimal>("IMPORTE")).Contains(Txt_Busqueda.Text.Trim())
                                        || Convert.ToString(Fila_Tot.Field<String>("TIPO_OPERACION")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<String>("FECHA_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<String>("ESTATUS")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                        || Convert.ToString(Fila_Tot.Field<String>("USUARIO_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper()))
                                      select Fila_Tot).AsDataView().ToTable();

                            Dt_Movimiento = Dt_Mov;
                        }
                        else 
                        {
                            //mostramos solo las ampliaciones, reducciones y traspasos
                            Dt_Mov = (from Fila_Mov in Dt_Movimiento.AsEnumerable()
                                      where Fila_Mov.Field<string>("TIPO_OPERACION") != "ADECUACION"
                                      select Fila_Mov).AsDataView().ToTable();

                            Dt_Movimiento = Dt_Mov;
                        }

                        Dt_Tipo_Mod.Columns.Add("TIPO_OPERACION", System.Type.GetType("System.String"));

                        foreach (DataRow Dr in Dt_Movimiento.Rows)
                        {
                            Total = Total + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE"].ToString().Trim()) ? "0" : Dr["IMPORTE"].ToString().Trim());
                            if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("TRASPASO"))
                            {
                                Traspaso = true;
                            }
                            else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("AMPLIACION"))
                            {
                                Ampliacion = true;
                            }
                            else if (Dr["TIPO_OPERACION"].ToString().Trim().Equals("REDUCCION"))
                            {
                                Reduccion = true;
                            }
                        }

                        if (Ampliacion)
                        {
                            Fila = Dt_Tipo_Mod.NewRow();
                            Fila["TIPO_OPERACION"] = "AMPLIACION";
                            Dt_Tipo_Mod.Rows.Add(Fila);
                        }

                        if (Reduccion)
                        {
                            Fila = Dt_Tipo_Mod.NewRow();
                            Fila["TIPO_OPERACION"] = "REDUCCION";
                            Dt_Tipo_Mod.Rows.Add(Fila);
                        }

                        if (Traspaso)
                        {
                            Fila = Dt_Tipo_Mod.NewRow();
                            Fila["TIPO_OPERACION"] = "TRASPASO";
                            Dt_Tipo_Mod.Rows.Add(Fila);
                        }

                        Grid_Tipo_Modificacion.DataSource = Dt_Tipo_Mod;
                        Grid_Tipo_Modificacion.DataBind();

                        if (Dt_Tipo_Mod.Rows.Count <= 0)
                        {
                            Chk_Autorizar.Visible = false;
                            Lbl_Autorizacion.Visible = false;
                            Chk_Aceptacion.Visible = false;
                            Lbl_Aceptar.Visible = false;
                            Chk_Autorizar.Checked = false;
                            Chk_Aceptacion.Checked = false;
                            Lbl_Reabrir.Visible = false;
                            Chk_Reabrir.Visible = false;
                            Chk_Reabrir.Checked = false;
                        }
                    }
                    else
                    {
                        Chk_Autorizar.Visible = false;
                        Lbl_Autorizacion.Visible = false;
                        Chk_Aceptacion.Visible = false;
                        Lbl_Aceptar.Visible = false;
                        Chk_Autorizar.Checked = false;
                        Chk_Aceptacion.Checked = false;
                        Lbl_Reabrir.Visible = false;
                        Chk_Reabrir.Visible = false;
                        Chk_Reabrir.Checked = false;
                    }
                }
                else 
                {
                    Chk_Autorizar.Visible = false;
                    Lbl_Autorizacion.Visible = false;
                    Chk_Aceptacion.Visible = false;
                    Lbl_Aceptar.Visible = false;
                    Chk_Autorizar.Checked = false;
                    Chk_Aceptacion.Checked = false;
                    Lbl_Reabrir.Visible = false;
                    Chk_Reabrir.Visible = false;
                    Chk_Reabrir.Checked = false;
                }
                Txt_Total_Modificado.Text = "";
                Txt_Total_Modificado.Text = String.Format("{0:c}", Total);
            }
            catch (Exception ex)
            {
                throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Movimientos
        ///DESCRIPCIÓN          : Metodo para llenar el grid de Movimientos
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Modificaciones()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            DataTable Dt_Movimiento; //Variable que obtendra los datos de la consulta 

            try
            {
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Consulta.P_Tipo_Egreso = "RAMO33";
                }
                else
                {
                    Consulta.P_Tipo_Egreso = "MUNICIPAL";
                }
                Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Dt_Movimiento = Consulta.Consultar_Datos_Modificaciones();

                Grid_Modificaciones.DataSource = Dt_Movimiento;
                Grid_Modificaciones.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Usuario
        ///DESCRIPCIÓN          : Metodo para llenar los datos el usuario
        ///PARAMETROS           :    
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :Jennyfer Ivonne Ceja Lemus 
        ///FECHA_MODIFICO       :13/Agosto/2012
        ///CAUSA_MODIFICACIÓN   :Porque cuando la consulta que nos dice  si el usuario es del ramo33 
        ///                      trae una fila con campos nulls no entraba a ninguna condicion
        ///*******************************************************************************
        protected void Obtener_Datos_Usuario()
        {
            Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Boolean Administrador;
            DataTable Dt_Ramo33 = new DataTable();

            Hf_Tipo_Usuario.Value = "";

            try
            {
                //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
                Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
                Administrador = Negocio.Consultar_Rol();

                if (Administrador)
                {
                    Hf_Tipo_Usuario.Value = "Administrador";
                }
                else
                {
                    //verificamos si el usuario logueado es el administrador de ramo33
                    Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                    if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                    {
                        if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                            || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                        {
                            Hf_Tipo_Usuario.Value = "Ramo33";
                        }
                        else //SE AGREGO ESTE ELSE
                        {
                            Hf_Tipo_Usuario.Value = "Usuario";
                        }
                    }
                    else
                    {
                        Hf_Tipo_Usuario.Value = "Usuario";
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Atualizar_Datos
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado o rechazado
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Actualizar_Datos()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Int32 Indice = 0;
            Boolean Autorizado;
            Boolean Rechazado;
            String Autorizados = String.Empty;
            Boolean Cancelado;
            Boolean Aceptado;

            try
            {
                foreach (GridViewRow Dr in Grid_Tipo_Modificacion.Rows)
                {
                    GridView Grid_Movimientos = (GridView)Dr.FindControl("Grid_Movimientos");

                    foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                    {
                        Indice++;
                        Grid_Movimientos.SelectedIndex = Indice;

                        if (Hf_Estatus.Value.Trim().Equals("PREAUTORIZADO"))
                        {
                            if (Chk_Reabrir.Checked)
                            {
                                Negocio.P_Estatus = "REABRIR";
                            }
                            else
                            {
                                Negocio.P_Estatus = "AUTORIZADO";
                                Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                                if (!Autorizado)
                                {
                                    Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                                    if (!Rechazado)
                                    {
                                        Lbl_Mensaje_Error.Text = "Favor de Autorizar o Rechazar todos los movimientos, ya que existen movimientos sin seleccionar";
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                        return;
                                    }
                                }
                                else
                                {
                                    Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                                    if (Rechazado)
                                    {
                                        Lbl_Mensaje_Error.Text = "Favor de solo Autorizar o Rechazar los movimientos, ya que existen movimientos que se seleccionaron ambas opciones";
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                        return;
                                    }
                                }
                            }
                        }
                        else
                        {
                            Negocio.P_Estatus = "GENERADO";
                            Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                            if (Aceptado)
                            {
                                Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                                if (Cancelado)
                                {
                                    Lbl_Mensaje_Error.Text = "Favor de solo Aceptar o Cancelar los movimientos, ya que existen movimientos que se seleccionaron ambas opciones";
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                    return;
                                }
                            }

                            if (Chk_Autorizar.Checked)
                            {
                                Negocio.P_Estatus = "PREAUTORIZADO";
                                Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                                if (!Aceptado)
                                {
                                    Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                                    if (!Cancelado)
                                    {
                                        Lbl_Mensaje_Error.Text = "Favor de Aceptar o Cancelar todos los movimientos, ya que existen movimientos sin seleccionar";
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                        return;
                                    }
                                }
                            }
                        }
                    }
                }

                Negocio.P_Dt_Mov = Crear_Dt_Autorizados();
                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                Negocio.P_No_Solicitud = Hf_No_Movimiento_Egr.Value.Trim();
                Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Negocio.P_Tipo_Egreso = "RAMO33";
                }
                else
                {
                    Negocio.P_Tipo_Egreso = "MUNICIPAL";
                }

                Autorizados = Negocio.Modificar_Autorizacion_Mov();
                if (Autorizados.Trim().Equals("SI"))
                {
                    Inicializa_Controles();
                    Grid_Modificaciones.SelectedIndex = -1;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                }
                else
                {
                    Inicializa_Controles();
                    Grid_Modificaciones.SelectedIndex = -1;
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('"+Autorizados+"');", true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Autorizados
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los datos de la actualizacion
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Autorizados()
        {
            DataTable Dt_Autorizados = new DataTable();
            Int32 Indice = 0;
            Boolean Autorizado;
            Boolean Rechazado;
            Boolean Cancelado;
            Boolean Aceptado;
            DataRow Fila;
            DataTable Dt_Comentarios = new DataTable();

            try
            {
                Dt_Comentarios = Obtener_Comentarios();
                Dt_Autorizados.Columns.Add("SOLICITUD_ID");
                Dt_Autorizados.Columns.Add("ESTATUS");
                Dt_Autorizados.Columns.Add("COMENTARIO");

                foreach (GridViewRow Dr in Grid_Tipo_Modificacion.Rows)
                {
                    GridView Grid_Movimientos = (GridView)Dr.FindControl("Grid_Movimientos");
                    foreach (GridViewRow Renglon_Grid in Grid_Movimientos.Rows)
                    {
                        Indice++;
                        Grid_Movimientos.SelectedIndex = Indice;

                        if (Hf_Estatus.Value.Trim().Equals("PREAUTORIZADO"))
                        {
                            Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                            if (Autorizado)
                            {
                                Fila = Dt_Autorizados.NewRow();
                                Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                Fila["ESTATUS"] = "AUTORIZADO";
                                Fila["COMENTARIO"] = String.Empty;
                                Dt_Autorizados.Rows.Add(Fila);
                            }
                            else
                            {
                                Rechazado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Rechazado")).Checked;
                                if (Rechazado)
                                {
                                    Fila = Dt_Autorizados.NewRow();
                                    Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                    Fila["ESTATUS"] = "RECHAZADO";
                                    if (Dt_Comentarios != null && Dt_Comentarios.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Com in Dt_Comentarios.Rows)
                                        {
                                            if (Dr_Com["SOLICITUD_ID"].ToString().Trim().Equals(Renglon_Grid.Cells[1].Text.Trim()))
                                            {
                                                Fila["COMENTARIO"] = Dr_Com["COMENTARIO"].ToString().Trim();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Fila["COMENTARIO"] = String.Empty;
                                    }
                                    Dt_Autorizados.Rows.Add(Fila);
                                }
                            }
                        }
                        else
                        {
                            Aceptado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Aceptar")).Checked;
                            if (Aceptado)
                            {
                                Fila = Dt_Autorizados.NewRow();
                                Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                Fila["ESTATUS"] = "ACEPTADO";
                                Fila["COMENTARIO"] = String.Empty;
                                Dt_Autorizados.Rows.Add(Fila);
                            }
                            else
                            {
                                Cancelado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Cancelado")).Checked;
                                if (Cancelado)
                                {
                                    Fila = Dt_Autorizados.NewRow();
                                    Fila["SOLICITUD_ID"] = Renglon_Grid.Cells[1].Text.Trim();
                                    Fila["ESTATUS"] = "CANCELADO";
                                    if (Dt_Comentarios != null && Dt_Comentarios.Rows.Count > 0)
                                    {
                                        foreach (DataRow Dr_Com in Dt_Comentarios.Rows)
                                        {
                                            if (Dr_Com["SOLICITUD_ID"].ToString().Trim().Equals(Renglon_Grid.Cells[1].Text.Trim()))
                                            {
                                                Fila["COMENTARIO"] = Dr_Com["COMENTARIO"].ToString().Trim();
                                                break;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        Fila["COMENTARIO"] = String.Empty;
                                    }
                                    Dt_Autorizados.Rows.Add(Fila);
                                }
                            }
                        }
                    }
                }
                return Dt_Autorizados;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        //********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Comentarios
        ///DESCRIPCIÓN          : Metodo para crear el datatable con los comentarios
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 26/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Obtener_Comentarios()
        {
            String Comentarios = String.Empty;
            DataTable Dt_Comentarios = new DataTable();
            DataRow Fila;
            String[] Coment;
            String[] Comenta;

            try
            {
                Comentarios = Hf_No_Rechazado.Value.Trim();
                Dt_Comentarios.Columns.Add("SOLICITUD_ID");
                Dt_Comentarios.Columns.Add("COMENTARIO");

                if (!String.IsNullOrEmpty(Comentarios))
                {
                    Coment = Comentarios.Split('-');

                    for (int i = 0; i <= Coment.Length - 2; i++)
                    {
                        Comenta = Coment[i].Split(';');
                        Fila = Dt_Comentarios.NewRow();
                        Fila["SOLICITUD_ID"] = Comenta[0].ToString().Trim();
                        Fila["COMENTARIO"] = Comenta[1].ToString().Trim();
                        Dt_Comentarios.Rows.Add(Fila);
                    }
                }

                return Dt_Comentarios;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }
    #endregion

    #region(Eventos)
        #region(Botones)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton de Actualizar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();

                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Lbl_Mensaje_Error.Text = "";

                    if (Btn_Nuevo.ToolTip == "Actualizar")
                    {
                        Actualizar_Datos();
                    }
                }

                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }

            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Cancelar")
                    {
                        Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
                        Grid_Modificaciones.SelectedIndex = -1;
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
            ///DESCRIPCIÓN          : Evento del boton Buscar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Llenar_Grid_Movimientos();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error en la busqueda de movimientos. Error[" + Ex.Message + "]");
                }
            }
        #endregion

        #region (Grid)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Modificaciones_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion del grid
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Modificaciones_SelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Rs_Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//Variable de conexión hacia la capa de Negocios
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Habilitar_Controles("Nuevo");
                    if (Grid_Modificaciones.SelectedRow.Cells[4].Text.Equals("AUTORIZADO"))
                    {
                        Btn_Nuevo.Visible = false;
                    }
                    else 
                    {
                        Btn_Nuevo.Visible = true;
                    }
                  
                    Hf_Estatus.Value = Grid_Modificaciones.SelectedRow.Cells[4].Text;
                    Hf_No_Movimiento_Egr.Value = Grid_Modificaciones.SelectedRow.Cells[1].Text;
                    Div_Grid_Movimientos.Visible = true;
                    Div_Grid_Modificaciones.Visible = false;
                    Tabla_Busqueda.Visible = false;
                    Llenar_Grid_Movimientos();
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Modificacion_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion del grid
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Modificaciones_Sorting(object sender, GridViewSortEventArgs e)
            {
                //Se consultan los movimientos que actualmente se encuentran registradas en el sistema.
                Llenar_Grid_Modificaciones();
                DataTable Dt_Modificaciones = (Grid_Modificaciones.DataSource as DataTable);

                if (Dt_Modificaciones != null)
                {
                    DataView Dv_Modificaciones = new DataView(Dt_Modificaciones);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC"))
                    {
                        Dv_Modificaciones.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    }
                    else
                    {
                        Dv_Modificaciones.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Modificaciones.DataSource = Dv_Modificaciones;
                    Grid_Modificaciones.DataBind();
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Movimientos = new DataTable();
                DataTable Dt_Movimientos_Detalles = new DataTable();

                try
                {
                    GridView Grid_Movimientos_Detalle = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos_Detalle");
                    String Solicitud_ID = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        Solicitud_ID = e.Row.Cells[1].Text.Trim();
                        Consulta.P_No_Solicitud = Solicitud_ID;
                        Dt_Movimientos_Detalles = Consulta.Consultar_Datos_Movimientos_Det();

                        Grid_Movimientos_Detalle.Columns[0].Visible = true;
                        Grid_Movimientos_Detalle.Columns[1].Visible = true;
                        Grid_Movimientos_Detalle.Columns[2].Visible = true;
                        Grid_Movimientos_Detalle.Columns[12].Visible = true;
                        Grid_Movimientos_Detalle.Columns[13].Visible = true;
                        Grid_Movimientos_Detalle.Columns[14].Visible = true;
                        Grid_Movimientos_Detalle.Columns[15].Visible = true;
                        Grid_Movimientos_Detalle.Columns[16].Visible = true;
                        Grid_Movimientos_Detalle.Columns[17].Visible = true;
                        Grid_Movimientos_Detalle.Columns[18].Visible = true;
                        Grid_Movimientos_Detalle.Columns[19].Visible = true;
                        Grid_Movimientos_Detalle.Columns[20].Visible = true;
                        Grid_Movimientos_Detalle.Columns[21].Visible = true;
                        Grid_Movimientos_Detalle.Columns[22].Visible = true;
                        Grid_Movimientos_Detalle.Columns[23].Visible = true;
                        Grid_Movimientos_Detalle.Columns[24].Visible = true;
                        Grid_Movimientos_Detalle.Columns[25].Visible = true;
                        Grid_Movimientos_Detalle.DataSource = Dt_Movimientos_Detalles;
                        Grid_Movimientos_Detalle.DataBind();
                        Grid_Movimientos_Detalle.Columns[0].Visible = false;
                        Grid_Movimientos_Detalle.Columns[1].Visible = false;
                        Grid_Movimientos_Detalle.Columns[2].Visible = false;
                        Grid_Movimientos_Detalle.Columns[12].Visible = false;
                        Grid_Movimientos_Detalle.Columns[13].Visible = false;
                        Grid_Movimientos_Detalle.Columns[14].Visible = false;
                        Grid_Movimientos_Detalle.Columns[15].Visible = false;
                        Grid_Movimientos_Detalle.Columns[16].Visible = false;
                        Grid_Movimientos_Detalle.Columns[17].Visible = false;
                        Grid_Movimientos_Detalle.Columns[18].Visible = false;
                        Grid_Movimientos_Detalle.Columns[19].Visible = false;
                        Grid_Movimientos_Detalle.Columns[20].Visible = false;
                        Grid_Movimientos_Detalle.Columns[21].Visible = false;
                        Grid_Movimientos_Detalle.Columns[22].Visible = false;
                        Grid_Movimientos_Detalle.Columns[23].Visible = false;
                        Grid_Movimientos_Detalle.Columns[25].Visible = false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Movimientos_RowCreated
            ///DESCRIPCIÓN          : Evento del grid del movimiento del cursor sobre los registros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Movimientos_RowCreated(object sender, GridViewRowEventArgs e)
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
                    e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Tipo_Modificacion_RowDataBound
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Mayo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Tipo_Modificacion_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                DataTable Dt_Mov = new DataTable();
                DataTable Dt_Movimiento = new DataTable();

                try
                {
                    GridView Grid_Movimientos = (GridView)e.Row.Cells[0].FindControl("Grid_Movimientos");
                    String Tipo_Operacion = String.Empty;

                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {

                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            Consulta.P_Tipo_Egreso = "RAMO33";
                            if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                            {
                                Consulta.P_Estatus = "'GENERADO', 'ACEPTADO'";
                            }
                            else
                            {
                                Consulta.P_Estatus = "'" + Hf_Estatus.Value.Trim() + "'";
                            }
                        }
                        else
                        {
                            Consulta.P_Tipo_Egreso = "MUNICIPAL";
                            if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                            {
                                Consulta.P_Estatus = "'RECIBIDO', 'ACEPTADO'";
                            }
                            else
                            {
                                Consulta.P_Estatus = "'" + Hf_Estatus.Value.Trim() + "'";
                            }
                        }
                        Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                        Consulta.P_No_Solicitud = Hf_No_Movimiento_Egr.Value.Trim();
                        Tipo_Operacion = e.Row.Cells[0].Text.Trim();
                        Consulta.P_Tipo_Solicitud = Tipo_Operacion;
                        Dt_Movimiento = Consulta.Consultar_Datos_Movimientos_Tipo();

                        if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                        {
                            if (Dt_Movimiento != null)
                            {
                                if (Dt_Movimiento.Rows.Count > 0)
                                {
                                    Dt_Mov = (from Fila in Dt_Movimiento.AsEnumerable()
                                              where (
                                                Convert.ToString(Fila.Field<String>("NO_SOLICITUD")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<Decimal>("IMPORTE")).Contains(Txt_Busqueda.Text.Trim())
                                                || Convert.ToString(Fila.Field<String>("TIPO_OPERACION")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("FECHA_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("ESTATUS")).Contains(Txt_Busqueda.Text.Trim().ToUpper())
                                                || Convert.ToString(Fila.Field<String>("USUARIO_CREO")).Contains(Txt_Busqueda.Text.Trim().ToUpper()))
                                              select Fila).AsDataView().ToTable();

                                    Dt_Movimiento = Dt_Mov;
                                }
                            }
                        }

                        Grid_Movimientos.Columns[1].Visible = true;
                        Grid_Movimientos.Columns[3].Visible = true;
                        Grid_Movimientos.Columns[4].Visible = true;
                        Grid_Movimientos.Columns[5].Visible = true;
                        Grid_Movimientos.Columns[8].Visible = true;
                        Grid_Movimientos.Columns[11].Visible = true;
                        Grid_Movimientos.Columns[12].Visible = true;
                        Grid_Movimientos.Columns[13].Visible = true;
                        Grid_Movimientos.Columns[14].Visible = true;
                        Grid_Movimientos.Columns[15].Visible = true;
                        Grid_Movimientos.DataSource = Dt_Movimiento;
                        Grid_Movimientos.DataBind();
                        Grid_Movimientos.Columns[1].Visible = false;
                        Grid_Movimientos.Columns[3].Visible = false;
                        Grid_Movimientos.Columns[4].Visible = false;
                        Grid_Movimientos.Columns[5].Visible = false;
                        Grid_Movimientos.Columns[8].Visible = false;
                        Grid_Movimientos.Columns[11].Visible = false;

                        if (Hf_Estatus.Value.Trim().Equals("PREAUTORIZADO"))
                        {
                            Grid_Movimientos.Columns[12].Visible = false;
                            Grid_Movimientos.Columns[13].Visible = false;

                            //consultamos si se puede reabrir la modificacion
                            Consulta.P_No_Modificacion= Hf_No_Movimiento_Egr.Value.Trim();
                            Consulta.P_Busqueda = "ANTES";

                            if (Consulta.Consultar_Estatus_Mod_Sig())
                            {
                                Chk_Autorizar.Visible = true;
                                Lbl_Autorizacion.Visible = true;
                                Chk_Autorizar.Checked = false;
                            }
                            else 
                            {
                                Chk_Autorizar.Visible = false;
                                Lbl_Autorizacion.Visible = false;
                                Chk_Autorizar.Checked = false;
                                Grid_Movimientos.Columns[14].Visible = false;
                                Grid_Movimientos.Columns[15].Visible = false;
                            }

                            //consultamos si se puede reabrir la modificacion
                            Consulta.P_No_Modificacion = Hf_No_Movimiento_Egr.Value.Trim();
                            Consulta.P_Busqueda = "DESPUES";
                            if (Consulta.Consultar_Estatus_Mod_Sig())
                            {
                                Lbl_Reabrir.Visible = true;
                                Chk_Reabrir.Visible = true;
                                Chk_Reabrir.Checked = false;
                            }
                            else 
                            {
                                Lbl_Reabrir.Visible = false;
                                Chk_Reabrir.Visible = false;
                                Chk_Reabrir.Checked = false;
                            }
                            Chk_Aceptacion.Checked = false;
                            Lbl_Autorizacion.Text = "Autorizar Todo";
                            Chk_Aceptacion.Visible = false;
                            Lbl_Aceptar.Visible = false;
                            
                        }
                        else if (Hf_Estatus.Value.Trim().Equals("GENERADO"))
                        {
                            Grid_Movimientos.Columns[14].Visible = false;
                            Grid_Movimientos.Columns[15].Visible = false;
                            Chk_Autorizar.Visible = true;
                            Lbl_Autorizacion.Visible = true;
                            Chk_Aceptacion.Visible = true;
                            Lbl_Aceptar.Visible = true;
                            Chk_Autorizar.Checked = false;
                            Chk_Aceptacion.Checked = false;
                            Lbl_Autorizacion.Text = "Cerrar Modificación";
                            Lbl_Reabrir.Visible = false;
                            Chk_Reabrir.Visible = false;
                            Chk_Reabrir.Checked = false;

                            if (Dt_Movimiento is DataTable)
                            {
                                if (Dt_Movimiento.Rows.Count > 0)
                                {
                                    foreach (DataRow Dr in Dt_Movimiento.Rows)
                                    {
                                        if (Dr is DataRow)
                                        {
                                            if (!String.IsNullOrEmpty(Dr["ESTATUS"].ToString()))
                                            {
                                                if (Grid_Movimientos is GridView)
                                                {
                                                    if (Grid_Movimientos.Rows.Count > 0)
                                                    {
                                                        foreach (GridViewRow Gv_Mov in Grid_Movimientos.Rows)
                                                        {
                                                            if (Gv_Mov is GridViewRow)
                                                            {
                                                                if (!String.IsNullOrEmpty(Gv_Mov.Cells[8].Text))
                                                                {
                                                                    if (Gv_Mov.Cells[8].Text.Trim().Equals("ACEPTADO"))
                                                                    {
                                                                        ((CheckBox)Gv_Mov.Cells[12].FindControl("Chk_Aceptar")).Checked = true;
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        else
                        {
                            Grid_Movimientos.Columns[12].Visible = false;
                            Grid_Movimientos.Columns[13].Visible = false;
                            Grid_Movimientos.Columns[14].Visible = false;
                            Grid_Movimientos.Columns[15].Visible = false;
                            Chk_Autorizar.Visible = false;
                            Lbl_Autorizacion.Visible = false;
                            Chk_Aceptacion.Visible = false;
                            Lbl_Aceptar.Visible = false;
                            Chk_Autorizar.Checked = false;
                            Chk_Aceptacion.Checked = false;
                            Lbl_Reabrir.Visible = false;
                            Chk_Reabrir.Visible = false;
                            Chk_Reabrir.Checked = false;
                        }
                    }
                    
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error:[" + Ex.Message + "]");
                }
            }
        #endregion
    #endregion

        //private void test() {
        //    List<Cls_Cat_Empleados_Negocios> Empleados = new List<Cls_Cat_Empleados_Negocios>();

        //    Cls_Cat_Empleados_Negocios E1 = new Cls_Cat_Empleados_Negocios();
        //    Cls_Cat_Empleados_Negocios E2 = new Cls_Cat_Empleados_Negocios();

        //    E1.P_No_Empleado = "250";
        //    E2.P_No_Empleado = "251";

        //    Empleados.Add(E1);
        //    Empleados.Add(E2);


        //    var Empleado = from empleado in Empleados
        //                   where empleado.P_No_Empleado == "250" || empleado.P_No_Empleado == "251"
        //                   select empleado;

        //    foreach (Cls_Cat_Empleados_Negocios item in Empleado)
        //    {
        //        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('" + item.P_No_Empleado + "');", true);
        //    }
    //}
}
