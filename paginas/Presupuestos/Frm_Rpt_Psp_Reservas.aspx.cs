﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Sessiones;
using JAPAMI.Reporte_Reservas.Negocio;
using System.Data;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Reservas : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.No_Empleado == null || Cls_Sessiones.No_Empleado.Trim().Length == 0)
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            }
            if (!IsPostBack)
            {
                Llenar_Combo_Anio();
                Llenar_Combo_Unidad_Responsable();
                Llenar_Combo_Partida();
            }
        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al cargar la pagina ", Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Anio
    ///DESCRIPCIÓN:          Llena el Combo de Año.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Anio()
    {
        try
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();
            DataTable Dt_Anio = Reporte_Negocio.Consultar_Anio();
            Cmb_Anio.DataSource = Reporte_Negocio.Consultar_Anio();
            Cmb_Anio.DataTextField = "ANNO";
            Cmb_Anio.DataValueField = "ANIO";
            Cmb_Anio.DataBind();
            Cmb_Anio.Items.Insert(0, new ListItem("<-- TODAS -->", ""));

        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al llenar Combo Año " , Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Unidad_Responsable
    ///DESCRIPCIÓN:          Llena el Combo de Unidad_responsable.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Unidad_Responsable()
    {
        try
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();
            Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            Cmb_Unidad_Responsable.DataSource = Reporte_Negocio.Consultar_Unidad_Reponsable();
            Cmb_Unidad_Responsable.DataTextField = "DEPENDENCIA";
            Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- TODAS -->", ""));
        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al llenar Combo Unidad Responsable " , Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Partida
    ///DESCRIPCIÓN:          Llena el Combo de Partida.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Partida()
    {
        try
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();
            Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            Cmb_Partida.DataSource = Reporte_Negocio.Consultar_Partida();
            Cmb_Partida.DataTextField = "PARTIDA";
            Cmb_Partida.DataValueField = "PARTIDA_ID";
            Cmb_Partida.DataBind();
            Cmb_Partida.Items.Insert(0, new ListItem("<-- TODAS -->", ""));
        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al llenar Combo Partida ",Ex.Message,true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Partida
    ///DESCRIPCIÓN:          Llena el Combo de Partida.
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Mostrar_Error(String encabezado,String mensaje, Boolean mostrar)
    {
        Lbl_Mensaje_Error.Text = mensaje;
        Lbl_Ecabezado_Mensaje.Text = encabezado;
        Div_Contenedor_Msj_Error.Visible = mostrar;
    }

    #region Eventos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Filtros_Click
    ///DESCRIPCIÓN:          Maneja el Evento del Boton para realizar la Limpieza de los filtros
    ///                      para la busqueda.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Limpiar_Filtros_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Limpiar_Filtros_Busqueda(false);
            Mostrar_Error("", "", false);
        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al limpiar filtros " , Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Listado_Click
    ///DESCRIPCIÓN:          Maneja el Evento del Boton para realizar la Busqueda con los filtros
    ///                      para la busqueda.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Generar_Listado_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Valida_Filtros())
            {
                Grid_Reservas.PageIndex = 0;
                Llenar_Grid_Reservas();
            }
            else
            {
                Mostrar_Error("Favor de seleccionar el año", "", true);
            }

        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al generar listado " , Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Cmb_Anio_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Anio.SelectedIndex > 0)
        {
            Llenar_Combo_Unidad_Responsable();
        }
       
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_Responsable.SelectedIndex > 0)
        {
            Llenar_Combo_Partida();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:          Sale del Formulario.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_PDF_Click
    ///DESCRIPCIÓN:          Genera el Reporte en PDF.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
    {
        if (Valida_Filtros())
            Generar_Reporte("PDF");
        else
        {

            Mostrar_Error("Favor de seleccionar el año", "", true);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Excel_Click
    ///DESCRIPCIÓN:      Genera el Reporte en Excel.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
    {
        if (Valida_Filtros())
            Generar_Reporte("EXCEL");
        else
        {

            Mostrar_Error("Favor de seleccionar el año", "", true);
        }
    }

    protected void Grid_Reservas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();
            DataTable Dt_Reservas_Detalles = new DataTable();
           
            int no = Convert.ToInt32(e.Row.Cells[1].Text);
        
            GridView Grid_Reservas_Detalles = (GridView)e.Row.Cells[4].FindControl("Grid_Reservas_Detalles");
            DataTable Dt_Detalles = new DataTable();
            String No_Requisicion = String.Empty;

            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                No_Requisicion = e.Row.Cells[1].Text.Trim();
                Dt_Detalles = Reporte_Negocio.Consultar_Movimiento_Reserva(Convert.ToInt32(no));

                Grid_Reservas_Detalles.Columns[0].Visible = true;
                Grid_Reservas_Detalles.DataSource = Dt_Detalles;
                Grid_Reservas_Detalles.DataBind();
                Grid_Reservas_Detalles.Columns[0].Visible = false;
            }

        }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error:[" + Ex.Message + "]");
        }
    }

    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: llenar_Grid_Reservas()
    ///DESCRIPCIÓN:          Llena el grid de acuerdo a los filtros seleccionados
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Reservas()
    {
        try
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();

            if (Cmb_Anio.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            }
            if (Cmb_Partida.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Partida = Cmb_Partida.SelectedItem.Value;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text))
            {
                Reporte_Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Reporte_Negocio.P_Fecha_Final = Txt_Fecha_Final.Text;
            }

            if (Reporte_Negocio.Consulta_Reservas().Rows.Count != 0)
            {
                Grid_Reservas.DataSource = Reporte_Negocio.Consulta_Reservas();
                Grid_Reservas.DataBind();
                Session["Dt_Reservas"] = Reporte_Negocio.Consulta_Reservas();
                //Llenar_Grid_Reservas_Detalles(Convert.ToInt32(Grid_Reservas.SelectedRow.Cells[1]));

                Btn_Reporte_PDF.Visible = true;
                Btn_Reporte_Excel.Visible = true;
            }
            else
            {
                Grid_Reservas.DataSource = null;
                Grid_Reservas.DataBind();
                Btn_Reporte_PDF.Visible = false;
                Btn_Reporte_Excel.Visible = false;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);
            }
        }
        catch (Exception Ex)
        {
            Mostrar_Error("Error al llenar el grid " , Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Crear_Dt_Reservas_Detalles
    ///DESCRIPCIÓN:          Llena el grid de acuerdo a los filtros seleccionados
    ///PROPIEDADES:          
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           03/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private DataTable Crear_Dt_Reservas_Detalles(String Dependencia_ID)
    {
        DataTable Dt_Partida_Detalle = new DataTable();
        DataTable Dt_Session = new DataTable();
        Dt_Session = (DataTable)Session["Dt_Partidas_Dependencia"];
        String No_Reserva = string.Empty;
        String Fecha = string.Empty;
        String Hora = string.Empty;
        String Modifico = string.Empty;
        String Cargo = string.Empty;
        String Abono = string.Empty;
        Double Importe = 0.00;
        Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();

        DataRow Fila;

        String Poliza = String.Empty;
        Boolean Iguales;

        try
        {
            if (Dt_Session != null)
            {
                if (Dt_Session.Rows.Count > 0)
                {
                    //creamos las columnas del datatable donde se guardaran los datos
                    Dt_Partida_Detalle.Columns.Add("No_Reserva", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("FECHA", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("HORA", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("MODIFICO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("CARGO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("ABONO", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
                    Dt_Partida_Detalle.Columns.Add("POLIZA", System.Type.GetType("System.String"));
                    
                    Boolean Dependencia_Existe = false;
                    foreach (DataRow Dr_Sessiones in Dt_Session.Rows)
                    {
                        //Obtenemos la partida id y la clave
                        No_Reserva = Dependencia_ID;
                        
                        Iguales = false;
                        //verificamos si la partida no a sido ya agrupada
                        if (Dt_Partida_Detalle.Rows.Count > 0)
                        {
                            foreach (DataRow Dr_Partidas in Dt_Partida_Detalle.Rows)
                            {
                                if (Dr_Partidas["No_Reserva"].ToString().Trim().Equals(No_Reserva))
                                {
                                    Iguales = true;
                                }
                            }
                        }

                        if (!Iguales)
                        {
                            Dependencia_Existe = false;
                            foreach (DataRow Dr_Session in Dt_Session.Rows)
                            {
                                if (Dr_Session["No_Reserva"].ToString().Equals(No_Reserva))
                                {
                                    No_Reserva = (String.IsNullOrEmpty(Dr_Session["No_Reserva"].ToString()) ? "0" : Dr_Session["No_Reserva"].ToString().Trim());
                                    Fecha = (String.IsNullOrEmpty(Dr_Session["FECHA"].ToString()) ? "0" : Dr_Session["FECHA"].ToString().Trim());
                                    Hora = (String.IsNullOrEmpty(Dr_Session["HORA"].ToString()) ? "0" : Dr_Session["HORA"].ToString().Trim());
                                    Modifico = (String.IsNullOrEmpty(Dr_Session["MODIFICO"].ToString()) ? "0" : Dr_Session["MODIFICO"].ToString().Trim());
                                    Cargo = (String.IsNullOrEmpty(Dr_Session["CARGO"].ToString()) ? "0" : Dr_Session["CARGO"].ToString().Trim());
                                    Abono = (String.IsNullOrEmpty(Dr_Session["ABONO"].ToString()) ? "0" : Dr_Session["ABONO"].ToString().Trim());
                                    Importe = Convert.ToDouble(String.IsNullOrEmpty(Dr_Session["IMPORTE"].ToString()) ? "0" : Dr_Session["IMPORTE"].ToString().Trim());
                                    Poliza = (String.IsNullOrEmpty(Dr_Session["POLIZA"].ToString()) ? "0" : Dr_Session["POLIZA"].ToString().Trim());
                                    Dependencia_Existe = true;
                                    Reporte_Negocio.Consultar_Movimiento_Reserva(Convert.ToInt32(Dr_Session["No_Reserva"]));
                                }
                            }

                            Fila = Dt_Partida_Detalle.NewRow();
                            Fila["No_Reserva"] = No_Reserva;
                            Fila["FECHA"] = String.Format("{0:##,###,##0.00}", Fecha);
                            Fila["HORA"] = String.Format("{0:##,###,##0.00}", Hora);
                            Fila["MODIFICO"] = String.Format("{0:##,###,##0.00}", Modifico);
                            Fila["CARGO"] = String.Format("{0:##,###,##0.00}", Cargo);
                            Fila["ABONO"] = String.Format("{0:##,###,##0.00}", Abono);
                            Fila["IMPORTE"] = String.Format("{0:##,###,##0.00}", Importe);
                            Fila["POLIZA"] = String.Format("{0:##,###,##0.00}", Poliza);

                            if (Dependencia_Existe == true)
                            {
                                Dt_Partida_Detalle.Rows.Add(Fila);
                            }
                           
                        }
                    }
                }
            }
            return Dt_Partida_Detalle;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear la tabla de reservas detalles Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           03/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Reservas_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            e.Row.Attributes.Add("onmouseover", "this.originalstyle=this.style.backgroundColor;this.style.backgroundColor='#507CD1';this.style.color='#FFFFFF'");
            e.Row.Attributes.Add("onmouseout", "this.style.backgroundColor=this.originalstyle;this.style.color=this.originalstyle;");
        }
    }



    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Maneja el Evento del Cambio de Seleccion para el los Combos de
    ///                      Dependencias.
    ///PARAMETROS:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Valida_Filtros()
    {

        if (Cmb_Anio.SelectedIndex > 0)
        {
            return true;
        }

        return false;

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Filtros_Busqueda
    ///DESCRIPCIÓN:          Limpia los combos de los filtros.
    ///PROPIEDADES:     
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Filtros_Busqueda(Boolean Limpiar_Grid)
    {
        Cmb_Anio.SelectedIndex = 0;
        Cmb_Unidad_Responsable.SelectedIndex = 0;
        Cmb_Partida.SelectedIndex = 0;
        Txt_Fecha_Inicial.Text = "";
        Txt_Fecha_Final.Text = "";

        if (Limpiar_Grid)
        {
            Grid_Reservas.DataSource = new DataTable();
            Grid_Reservas.DataBind();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Genera los Datos para el Reporte y dependiendo de la Opción hace
    ///                      la exportación de los datos.
    ///PROPIEDADES:          1. Tipo.    Tipo de Reporte ya se PDF y Excel.
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Generar_Reporte(String Tipo)
    {
        try
        {
            Cls_Rpt_Psp_Reservas_Negocio Reporte_Negocio = new Cls_Rpt_Psp_Reservas_Negocio();

            if (Cmb_Anio.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value;
            }
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Unidad_Responsable = Cmb_Unidad_Responsable.SelectedItem.Value;
            }
            if (Cmb_Partida.SelectedIndex > 0)
            {
                Reporte_Negocio.P_Partida = Cmb_Partida.SelectedItem.Value;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text))
            {
                Reporte_Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Reporte_Negocio.P_Fecha_Final = Txt_Fecha_Final.Text;
            }
            //Consultar la union/////////////////////////////////////

            DataTable Dt_Datos = Reporte_Negocio.Consulta_Reservas_Para_Reporte();

            Ds_Rpt_Psp_Calendarizacion Ds_Reporte = new Ds_Rpt_Psp_Calendarizacion();
            DataSet Ds_Consulta = new DataSet();
            Dt_Datos.TableName = "Dt_Reservas";
            Ds_Consulta.Tables.Add(Dt_Datos.Copy());
            if (Tipo.Trim().Equals("PDF"))
            {
                Generar_Reporte_PDF(Ds_Consulta, Ds_Reporte, "Cr_Rpt_Psp_Reservas.rpt");
            }
            else if (Tipo.Trim().Equals("EXCEL"))
            {
                Generar_Reporte_Excel(Ds_Consulta, Ds_Reporte, "Cr_Rpt_Psp_Reservas.rpt");
            }
        }
        catch (Exception Ex)
        {

            Mostrar_Error("Error al generar Reporte ", Ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Excel
    ///DESCRIPCIÓN:          carga el data set fisico con el cual se genera el Reporte especificado
    ///PROPIEDADES:          1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Generar_Reporte_Excel(DataSet Data_Set, DataSet Ds_Reporte, String Nombre_Reporte)
    {
        if (Data_Set.Tables[0].Rows.Count != 0)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Presupuestos/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/Rpt_Psp_Reservas.xls");
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.Excel;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/Rpt_Psp_Reservas.xls";

            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_PDF
    ///DESCRIPCIÓN:          carga el data set fisico con el cual se genera el Reporte especificado
    ///PROPIEDADES:          1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Luis Daniel Guzmán Malagón
    ///FECHA_CREO:           20/Septiembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Generar_Reporte_PDF(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        if (Data_Set_Consulta_DB.Tables[0].Rows.Count != 0)
        {
            ReportDocument Reporte = new ReportDocument();
            String File_Path = Server.MapPath("../Rpt/Presupuestos/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/Rpt_Psp_Reservas.pdf");
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/Rpt_Psp_Reservas.pdf";
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No se encontraron Registros')", true);
    }

}
