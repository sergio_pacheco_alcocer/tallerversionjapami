﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Common;
using System.Windows.Forms;
using System.Data.OleDb;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data.SqlClient;
using System.Text;
using JAPAMI.Cat_Psp_Rubros.Datos;

public partial class paginas_Presupuestos_Frm_Cat_Psp_Cargar_Excel : System.Web.UI.Page
{
    #region (PAGE LOAD)
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";
                Cls_Sessiones.Mostrar_Menu = true;
                Cls_Sessiones.Nombre_Empleado = "CARGA INICIAL";
            }
        }
    #endregion

    #region (METODOS)
        ///**********************************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Leer_Excel
        ///**********************************************************************************************************************************
        public DataSet Leer_Excel(String sqlExcel, String Path)
        {
            //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
            //String Rta = @MapPath("../../Archivos/PRESUPUESTO_IRAPUATO.xls");
            String Rta = @MapPath(Path);
            string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";

            if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
            {
                sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
            }
            else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Excel binario
            {
                sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=Excel 8.0;";
            }

            //Definimos el DataSet donde insertaremos los datos que leemos del excel
            DataSet DS = new DataSet();

            //Definimos la conexión OleDb al fichero Excel y la abrimos
            OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
            oledbConn.Open();

            //Creamos un comand para ejecutar la sentencia SELECT.
            OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

            //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
            OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
            da.Fill(DS);
            return DS;
        }


        ///**********************************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Leer_Excel
        ///**********************************************************************************************************************************
        private String Consultar_Tablas(String Campo, String Tabla, String Where, String Condicion)
        {
            String Mi_Sql = "";
            String Dato = "";
            Mi_Sql = "SELECT " + Campo + " FROM " + Tabla + " WHERE " + Where + " = '" + Condicion + "'";
            DataSet _DataSet = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql);
            if (_DataSet != null && _DataSet.Tables[0].Rows.Count > 0)
            {
                Dato = _DataSet.Tables[0].Rows[0][Campo].ToString().Trim();
            }
            else
            {
                Dato = "";
            }
            return Dato;
        }


        ///**********************************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Capitulo
        ///**********************************************************************************************************************************
        private String Obtener_Capitulo(String Partida_Id)
        {
            StringBuilder Mi_SQL = new StringBuilder();
            String Capitulo_Id = String.Empty;

            try
            {
                Mi_SQL.Append("SELECT " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto);
                Mi_SQL.Append(" ON " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Capitulo_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos + "." + Cat_SAP_Capitulos.Campo_Capitulo_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica);
                Mi_SQL.Append(" ON " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Concepto_ID);
                Mi_SQL.Append(" = " + Cat_Sap_Concepto.Tabla_Cat_SAP_Concepto + "." + Cat_Sap_Concepto.Campo_Concepto_ID);
                Mi_SQL.Append(" INNER JOIN " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
                Mi_SQL.Append(" ON " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" = " + Cat_SAP_Partida_Generica.Tabla_Cat_SAP_Partida_Generica + "." + Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID);
                Mi_SQL.Append(" AND " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas + "." + Cat_Sap_Partidas_Especificas.Campo_Partida_ID);
                Mi_SQL.Append(" = '" + Partida_Id + "'");

                Capitulo_Id = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
            }
            catch (Exception Ex)
            {

                throw new Exception("Error al consultar el capitulo Error[" + Ex.Message + "]");
            }
            return Capitulo_Id;
        }

    #endregion

    #region (Btn_Psp_2012_UR)
        protected void Btn_Psp_2012_UR_Click(object sender, EventArgs e)
        {
            String Fuente_ID = String.Empty;
            String Programa_ID = String.Empty;
            String UR_ID = String.Empty;
            String Partida_ID = String.Empty;
            String Area_Funcional_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Capitulo_ID = String.Empty;
            String Mensaje = String.Empty;

            String SqlExcel = "Select * From [PRESUPUESTOS$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/PSP2012.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Fuente_ID = Consultar_Tablas(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID,
                    Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento,
                    "CLAVE", Renglon["FUENTE"].ToString().Trim());
                Programa_ID = Consultar_Tablas(Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID,
                    Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas,
                    "CLAVE", Renglon["PROGRAMA"].ToString().Trim());
                UR_ID = Consultar_Tablas("DEPENDENCIA_ID",
                    Cat_Dependencias.Tabla_Cat_Dependencias,
                    "CLAVE", Renglon["UR"].ToString().Trim());
                Partida_ID = Consultar_Tablas("PARTIDA_ID",
                    Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas,
                    "CLAVE", Renglon["PARTIDA"].ToString().Trim());
                Area_Funcional_ID = Consultar_Tablas("AREA_FUNCIONAL_ID",
                    "CAT_SAP_AREA_FUNCIONAL", "CLAVE",
                    Renglon["AFUNCIONAL"].ToString());
                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Capitulo_ID = Obtener_Capitulo(Partida_ID);
                }

                if (!String.IsNullOrEmpty(Fuente_ID) && !String.IsNullOrEmpty(UR_ID) && !String.IsNullOrEmpty(Programa_ID) && !String.IsNullOrEmpty(Partida_ID) && !String.IsNullOrEmpty(Capitulo_ID) && !String.IsNullOrEmpty(Area_Funcional_ID))
                {
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                    Mi_SQL += "(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ";
                    Mi_SQL += " ACTUALIZADO) VALUES( ";
                    Mi_SQL += "'" + UR_ID + "', ";
                    Mi_SQL += "'" + Fuente_ID + "', ";
                    Mi_SQL += "'" + Programa_ID + "', ";
                    Mi_SQL += "'" + Capitulo_ID + "', ";
                    Mi_SQL += "'" + Partida_ID + "', ";
                    Mi_SQL += "2012, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString()) ? "0" : Renglon["TOTAL"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString()) ? "0" : Renglon["TOTAL"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENE"].ToString()) ? "0" : Renglon["ENE"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEB"].ToString()) ? "0" : Renglon["FEB"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAR"].ToString()) ? "0" : Renglon["MAR"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABR"].ToString()) ? "0" : Renglon["ABR"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAY"].ToString()) ? "0" : Renglon["MAY"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUN"].ToString()) ? "0" : Renglon["JUN"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUL"].ToString()) ? "0" : Renglon["JUL"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGO"].ToString()) ? "0" : Renglon["AGO"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEP"].ToString()) ? "0" : Renglon["SEP"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCT"].ToString()) ? "0" : Renglon["OCT"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOV"].ToString()) ? "0" : Renglon["NOV"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["DIC"].ToString()) ? "0" : Renglon["DIC"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString()) ? "0" : Renglon["TOTAL"].ToString().Replace(",", "")) + ", ";
                    Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += "GETDATE(), ";
                    Mi_SQL += "'NO') ";

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                else
                {
                    Mensaje += Renglon["FUENTE"].ToString() + "-";
                    Mensaje += Renglon["AFUNCIONAL"].ToString() + "-" + Renglon["PROGRAMA"].ToString() + "-";
                    Mensaje += Renglon["UR"].ToString() + "-" + Renglon["PARTIDA"].ToString() + "<br />";
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
            Lbl_Mensaje_Error.Text = Mensaje;
            Lbl_Mensaje_Error.Visible = true;
            Div_Contenedor_Msj_Error.Visible = true;
        }
   #endregion

    #region (Btn_Psp_Modificado_2012)
        protected void Btn_Psp_Modificado_2012_Click(object sender, EventArgs e)
        {
            String Fuente_ID = String.Empty;
            String Programa_ID = String.Empty;
            String UR_ID = String.Empty;
            String Partida_ID = String.Empty;
            String Area_Funcional_ID = String.Empty;
            String Capitulo_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            String Clave = String.Empty;
            String Asteriscos = String.Empty;
            String Clasificacion = String.Empty;
            int Num_Asteriscos;
            int contador = 1;
            String FF = String.Empty;
            String UR = String.Empty;
            String AF = String.Empty;
            String PP = String.Empty;
            String P = String.Empty;

            String SqlExcel = "Select * From [CP12C$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/CP12C.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Clasificacion = Renglon["Conceptos"].ToString().Trim();
                if (Clasificacion.Contains("*"))
                {
                    Asteriscos = Clasificacion.Substring(0, Clasificacion.IndexOf(" "));
                    Num_Asteriscos = Asteriscos.Trim().Length;

                    switch (Num_Asteriscos)
                    {
                        case 12:
                            AF = String.Empty;
                            Area_Funcional_ID = String.Empty;
                            PP = String.Empty;
                            Programa_ID = String.Empty;
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 11:
                            AF = String.Empty;
                            Area_Funcional_ID = String.Empty;
                            PP = String.Empty;
                            Programa_ID = String.Empty;
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 10:
                            AF = String.Empty;
                            Area_Funcional_ID = String.Empty;
                            PP = String.Empty;
                            Programa_ID = String.Empty;
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 9:
                            Clasificacion = Clasificacion.Replace("*", "").Trim();
                            Clave = Clasificacion.Substring(0, Clasificacion.IndexOf(" ")).Trim();

                            AF = String.Empty;
                            Area_Funcional_ID = String.Empty;

                            AF = Clave;
                            Area_Funcional_ID = Consultar_Tablas("AREA_FUNCIONAL_ID", "CAT_SAP_AREA_FUNCIONAL", "CLAVE", Clave.Trim());
                            Programa_ID = String.Empty;
                            UR_ID = String.Empty;
                            Partida_ID = String.Empty;
                            Fuente_ID = String.Empty;
                            PP = String.Empty;
                            P = String.Empty;
                            UR = String.Empty;
                            FF = String.Empty;
                            break;
                        case 8:
                            Clasificacion = Clasificacion.Replace("*", "").Trim();
                            Clave = Clasificacion.Substring(0, Clasificacion.IndexOf(" ")).Trim();
                            PP = String.Empty;
                            Programa_ID = String.Empty;
                            PP = Clave;
                            Programa_ID = Consultar_Tablas(Cat_Com_Proyectos_Programas.Campo_Proyecto_Programa_ID, Cat_Com_Proyectos_Programas.Tabla_Cat_Com_Proyectos_Programas, "CLAVE", Clave.Trim());
                            UR_ID = String.Empty;
                            Partida_ID = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            UR = String.Empty;
                            FF = String.Empty;
                            break;
                        case 7:
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 6:
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 5:
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 4:
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 3:
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            Partida_ID = String.Empty;
                            break;
                        case 2:
                            Clasificacion = Clasificacion.Replace("*", "").Trim();
                            Clave = Clasificacion.Substring(0, Clasificacion.IndexOf(" ")).Trim().Replace("-", "").Trim();
                            UR = String.Empty;
                            UR_ID = String.Empty;
                            UR = Clave;
                            UR_ID = Consultar_Tablas("DEPENDENCIA_ID", Cat_Dependencias.Tabla_Cat_Dependencias, "CLAVE", Clave.Trim());
                            Partida_ID = String.Empty;
                            Fuente_ID = String.Empty;
                            P = String.Empty;
                            FF = String.Empty;
                            break;
                        case 1:
                            Clasificacion = Clasificacion.Replace("*", "").Trim();
                            Clave = Clasificacion.Substring(0, Clasificacion.IndexOf(" ")).Trim();
                            FF = String.Empty;
                            Fuente_ID = String.Empty;
                            FF = Clave;
                            Fuente_ID = Consultar_Tablas(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID, Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento, "CLAVE", Clave.Trim());
                            Partida_ID = String.Empty;
                            P = String.Empty;
                            break;
                    }
                }
                else
                {
                    Clave = Clasificacion.Substring(0, Clasificacion.IndexOf(" "));
                    P = String.Empty;
                    Partida_ID = String.Empty;
                    P = Clave;
                    Partida_ID = Consultar_Tablas("PARTIDA_ID", Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas, "CLAVE", Clave.Trim());
                    if (!String.IsNullOrEmpty(Partida_ID))
                    {
                        Capitulo_ID = Obtener_Capitulo(Partida_ID);
                    }
                }



                if (!String.IsNullOrEmpty(Fuente_ID) && !String.IsNullOrEmpty(UR_ID) && !String.IsNullOrEmpty(Programa_ID) && !String.IsNullOrEmpty(Partida_ID) && !String.IsNullOrEmpty(Capitulo_ID))
                {
                    contador++;
                    if (!String.IsNullOrEmpty(Renglon["Aprobado"].ToString().Trim()))
                    {
                        Mi_SQL = "UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                        Mi_SQL += " SET " + Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Modificado"].ToString().Trim()) ? "0" : Renglon["Modificado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Ampliacion"].ToString().Trim()) ? "0" : Renglon["Ampliacion"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Reduccion"].ToString().Trim()) ? "0" : Renglon["Reduccion"].ToString().Trim().Replace(",", "").Replace("-", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Devengado"].ToString().Trim()) ? "0" : Renglon["Devengado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Pagado"].ToString().Trim()) ? "0" : Renglon["Pagado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Compromiso"].ToString().Trim()) ? "0" : Renglon["Compromiso"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Por Ejercer"].ToString().Trim()) ? "0" : Renglon["Por Ejercer"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Modifico + " = GETDATE() ";
                        Mi_SQL += " WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + UR_ID + "'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fuente_ID + "'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Programa_ID + "'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida_ID + "'";
                        Mi_SQL += " AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = 2012";

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    else
                    {
                        Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado;
                        Mi_SQL += "(" + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Saldo + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Ope_Psp_Presupuesto_Aprobado.Campo_Fecha_Creo + ", ";
                        Mi_SQL += " ACTUALIZADO) VALUES( ";
                        Mi_SQL += "'" + UR_ID + "', ";
                        Mi_SQL += "'" + Fuente_ID + "', ";
                        Mi_SQL += "'" + Programa_ID + "', ";
                        Mi_SQL += "'" + Capitulo_ID + "', ";
                        Mi_SQL += "'" + Partida_ID + "', ";
                        Mi_SQL += "2012, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Ampliacion"].ToString().Trim()) ? "0" : Renglon["Ampliacion"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Reduccion"].ToString().Trim()) ? "0" : Renglon["Reduccion"].ToString().Trim().Replace(",", "").Replace("-", "")) + ", ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Modificado"].ToString().Trim()) ? "0" : Renglon["Modificado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Devengado"].ToString().Trim()) ? "0" : Renglon["Devengado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += "0, ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Pagado"].ToString().Trim()) ? "0" : Renglon["Pagado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += "0, ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Compromiso"].ToString().Trim()) ? "0" : Renglon["Compromiso"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Por Ejercer"].ToString().Trim()) ? "0" : Renglon["Por Ejercer"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += "GETDATE(), ";
                        Mi_SQL += "'NO') ";

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
                else
                {
                    contador++;
                    Mensaje += contador + ": " + FF + "-" + AF + "-" + PP + "-" + UR + "-" + P + "<br />";
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
            Lbl_Mensaje_Error.Text = Mensaje;
            Lbl_Mensaje_Error.Visible = true;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    #endregion

    #region (Btn_Cargar_Tipos)
        protected void Btn_Cargar_Tipos_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Rubro_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            object Id;

            String SqlExcel = "Select * From [TIPOS$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Rubro_ID = Consultar_Tablas(Cat_Psp_Rubro.Campo_Rubro_ID,
                    Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro,
                    Cat_Psp_Rubro.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + "000000000");
                Tipo_ID = Consultar_Tablas(Cat_Psp_Tipo.Campo_Tipo_ID,
                     Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo,
                     Cat_Psp_Tipo.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + "00000000");

                if (!String.IsNullOrEmpty(Rubro_ID))
                {
                    if (!String.IsNullOrEmpty(Tipo_ID))
                    {
                        Mi_SQL = "UPDATE " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo;
                        Mi_SQL += " SET " + Cat_Psp_Tipo.Campo_Estatus + " = 'ACTIVO', ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Clave + " = '" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + "00000000', ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Anio + " = " + Renglon["ANIO"].ToString().Trim();
                        Mi_SQL += " WHERE " + Cat_Psp_Tipo.Campo_Rubro_ID + " = '" + Rubro_ID + "' ";
                        Mi_SQL += " AND " + Cat_Psp_Tipo.Campo_Tipo_ID + " = '" + Tipo_ID + "' ";
                    }
                    else
                    {
                        Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Psp_Tipo.Campo_Tipo_ID + "), '00000')";
                        Mi_SQL += " FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo;
                        Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                        if (Convert.IsDBNull(Id))
                        {
                            Tipo_ID = "00001";
                        }
                        else
                        {
                            Tipo_ID = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo;
                        Mi_SQL += "(" + Cat_Psp_Tipo.Campo_Tipo_ID + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Rubro_ID + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Clave + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Descripcion + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Estatus + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Anio + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Cat_Psp_Tipo.Campo_Fecha_Creo + ") VALUES( ";
                        Mi_SQL += "'" + Tipo_ID + "', ";
                        Mi_SQL += "'" + Rubro_ID + "', ";
                        Mi_SQL += "'" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + "00000000', ";
                        Mi_SQL += "'" + Renglon["DESCRIPCION"].ToString().ToUpper() + "', ";
                        Mi_SQL += "'ACTIVO', ";
                        Mi_SQL += "" + Renglon["ANIO"].ToString().Trim() + ", ";
                        Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += "GETDATE()) ";
                    }
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                else
                {
                    Mensaje += Renglon["RUBRO"].ToString() + "<br />";
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO <br /> No se encontraron: <br /> " + Mensaje + "');", true);
        }
    #endregion

    #region (Btn_Cargar_Rubros)
        protected void Btn_Cargar_Rubros_Click(object sender, EventArgs e)
        {
            String Rubro_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            object Id;

            String SqlExcel = "Select * From [RUBROS$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Psp_Rubro.Campo_Rubro_ID + "), '00000')";
                Mi_SQL += " FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro;
                Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (Convert.IsDBNull(Id))
                {
                    Rubro_ID = "00001";
                }
                else
                {
                    Rubro_ID = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                }

                Mi_SQL = "INSERT INTO " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro;
                Mi_SQL += "(" + Cat_Psp_Rubro.Campo_Rubro_ID + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Clave + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Descripcion + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Estatus + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Anio + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Usuario_Creo + ", ";
                Mi_SQL += Cat_Psp_Rubro.Campo_Fecha_Creo + ") VALUES( ";
                Mi_SQL += "'" + Rubro_ID + "', ";
                Mi_SQL += "'" + Renglon["RUBRO"].ToString().Trim() + "000000000', ";
                Mi_SQL += "'" + Renglon["DESCRIPCION"].ToString().ToUpper().Trim() + "', ";
                Mi_SQL += "'ACTIVO', ";
                Mi_SQL += Renglon["ANIO"].ToString().Trim() + ", ";
                Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                Mi_SQL += "GETDATE()) ";

                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Clases)
        protected void Btn_Cargar_Clases_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Clase_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            object Id;

            String SqlExcel = "Select * From [CLASES$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                if (!String.IsNullOrEmpty(Renglon["TIPO"].ToString().Trim()))
                {
                    Tipo_ID = Consultar_Tablas(Cat_Psp_Tipo.Campo_Tipo_ID,
                     Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo,
                     Cat_Psp_Tipo.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + "00000000");

                    Clase_ID = Consultar_Tablas(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID,
                     Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing,
                     Cat_Psp_Clase_Ing.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + Renglon["CLASE"].ToString().Trim() + "000000");

                    if (!String.IsNullOrEmpty(Tipo_ID))
                    {
                        if (!String.IsNullOrEmpty(Clase_ID))
                        {
                            Mi_SQL = "UPDATE " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;
                            Mi_SQL += " SET " + Cat_Psp_Clase_Ing.Campo_Estatus + " = 'ACTIVO', ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                                + Renglon["CLASE"].ToString().Trim() + "000000', ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Anio + " = " + Renglon["ANIO"].ToString().Trim();
                            Mi_SQL += " WHERE " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + " = '" + Clase_ID + "' ";
                            Mi_SQL += " AND " + Cat_Psp_Clase_Ing.Campo_Tipo_ID + " = '" + Tipo_ID + "' ";
                        }
                        else 
                        {
                            Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + "), '0000000000')";
                            Mi_SQL += " FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;
                            Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                            if (Convert.IsDBNull(Id))
                            {
                                Clase_ID = "0000000001";
                            }
                            else
                            {
                                Clase_ID = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                            }

                            Mi_SQL = "INSERT INTO " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing;
                            Mi_SQL += "(" + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Tipo_ID + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Clave + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Descripcion + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Estatus + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Usuario_Creo + ", ";
                            Mi_SQL += Cat_Psp_Clase_Ing.Campo_Fecha_Creo + ") VALUES( ";
                            Mi_SQL += "'" + Clase_ID + "', ";
                            Mi_SQL += "'" + Tipo_ID + "', ";
                            Mi_SQL += "'" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                                + Renglon["CLASE"].ToString().Trim() + "000000', ";
                            if (String.IsNullOrEmpty(Renglon["DESCRIPCION"].ToString()))
                            {
                                Mi_SQL += "' ', ";
                            }
                            else
                            {
                                Mi_SQL += "'" + Renglon["DESCRIPCION"].ToString().ToUpper() + "', ";
                            }
                            Mi_SQL += "'ACTIVO', ";
                            Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                            Mi_SQL += "GETDATE()) ";
                        }
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Conceptos)
        protected void Btn_Cargar_Conceptos_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Clase_ID = String.Empty;
            String Concepto_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            String CC_ID = String.Empty;
            object Id;

            String SqlExcel = "Select * From [CONCEPTOS$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Clase_ID = Consultar_Tablas(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID,
                    Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing,
                    Cat_Psp_Clase_Ing.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                    + Renglon["CLASE"].ToString().Trim() + "000000");
                Concepto_ID = Consultar_Tablas(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID,
                   Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing,
                   Cat_Psp_Concepto_Ing.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() +
                   Renglon["CLASE"].ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + "0000");

                CC_ID = Consultar_Tablas(Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID,
                   Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables,
                   Cat_Con_Cuentas_Contables.Campo_Cuenta, Renglon["CUENTA_CONTABLE"].ToString().Trim());

                if (!String.IsNullOrEmpty(Clase_ID))
                {
                    if (String.IsNullOrEmpty(Concepto_ID))
                    {
                        Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + "), '0000000000')";
                        Mi_SQL += " FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;
                        Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                        if (Convert.IsDBNull(Id))
                        {
                            Concepto_ID = "0000000001";
                        }
                        else
                        {
                            Concepto_ID = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;
                        Mi_SQL += "(" + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Clave + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Descripcion + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Estatus + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Anio + ", ";

                        if (!String.IsNullOrEmpty(CC_ID.Trim()))
                        {
                            Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + ", ";
                        }

                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Fecha_Creo + ") VALUES( ";
                        Mi_SQL += "'" + Concepto_ID + "', ";
                        Mi_SQL += "'" + Clase_ID + "', ";
                        Mi_SQL += "'" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                            + Renglon["CLASE"].ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + "0000', ";
                        Mi_SQL += "'" + Renglon["DESCRIPCION"].ToString().ToUpper() + "', ";
                        Mi_SQL += "'ACTIVO', ";
                        Mi_SQL += Renglon["ANIO"].ToString().Trim() + ", ";

                        if (!String.IsNullOrEmpty(CC_ID.Trim()))
                        {
                            Mi_SQL += "'" + CC_ID.Trim() + "', ";
                        }

                        Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += "GETDATE()) ";
                    }
                    else
                    {
                        Mi_SQL = "UPDATE " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing;
                        Mi_SQL += " SET " + Cat_Psp_Concepto_Ing.Campo_Estatus + " = 'ACTIVO', ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                            + Renglon["CLASE"].ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + "0000', ";
                        Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Anio + " = " + Renglon["ANIO"].ToString().Trim() + ", ";

                        if (!String.IsNullOrEmpty(CC_ID.Trim()))
                        {
                            Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + " = '" + CC_ID.Trim() + "'";
                        }
                        else 
                        {
                            Mi_SQL += Cat_Psp_Concepto_Ing.Campo_Cuenta_Contable_ID + " = NULL";
                        }

                        Mi_SQL += " WHERE " + Cat_Psp_Concepto_Ing.Campo_Clase_Ing_ID + " = '" + Clase_ID + "' ";
                        Mi_SQL += " AND " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID + " = '" + Concepto_ID + "' ";
                    }
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_SubConceptos)
        protected void Btn_Cargar_SubConceptos_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Concepto_ID = String.Empty;
            String SubConcepto_ID = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            String Concepto = String.Empty;
            String Clase = String.Empty;
            object Id;

            String SqlExcel = "Select * From [SUBCONCEPTO$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];

            try
            {
                foreach (DataRow Renglon in Dt_Psp.Rows)
                {
                    Concepto = Renglon["CONCEPTO"].ToString().Trim();
                    if (Concepto.Length < 2)
                    {
                        Concepto = "0" + Concepto;
                    }

                    Clase = Renglon["CLASE"].ToString().Trim();
                    if (Clase.Length < 2)
                    {
                        Clase = "0" + Clase;
                    }

                    Concepto_ID = Consultar_Tablas(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID,
                       Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing,
                       Cat_Psp_Concepto_Ing.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() +
                       Clase.ToString().Trim() + Concepto + "0000");

                    Mi_SQL = "SELECT " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID;
                    Mi_SQL += " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing;
                    Mi_SQL += " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID + " = '" + Concepto_ID.Trim() + "'";
                    Mi_SQL += " AND " + Cat_Psp_SubConcepto_Ing.Campo_Clave + " = '" + Renglon["SUBCONCEPTO"].ToString().Trim() + "'";

                    SubConcepto_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    if (!String.IsNullOrEmpty(SubConcepto_ID) && !String.IsNullOrEmpty(Concepto_ID))
                    {
                        Mi_SQL = "UPDATE " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing;
                        Mi_SQL += " SET " + Cat_Psp_SubConcepto_Ing.Campo_Estatus + " = 'ACTIVO', ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Clave + " = '" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim()
                            + Clase.ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + Renglon["SUBCONCEPTO"].ToString().Trim() + "', ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Anio + " = " + Renglon["ANIO"].ToString().Trim() + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Descripcion + " = '" + HttpUtility.HtmlDecode(Renglon["DESCRIPCION"].ToString().ToUpper().Replace("\"", "").Replace("'", "")) + "'";
                        Mi_SQL += " WHERE " + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + " = '" + SubConcepto_ID.Trim() + "' ";
                        Mi_SQL += " AND " + Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID + " = '" + Concepto_ID + "' ";

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                    else
                    {
                        Mi_SQL = "SELECT ISNULL(MAX (" + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + "), '0000000000')";
                        Mi_SQL += " FROM " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing;
                        Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                        if (Convert.IsDBNull(Id))
                        {
                            SubConcepto_ID = "0000000001";
                        }
                        else
                        {
                            SubConcepto_ID = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                        }

                        Mi_SQL = "INSERT INTO " + Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing;
                        Mi_SQL += "(" + Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Concepto_Ing_ID + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Clave + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Descripcion + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Estatus + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Anio + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Cat_Psp_SubConcepto_Ing.Campo_Fecha_Creo + ") VALUES( ";
                        Mi_SQL += "'" + SubConcepto_ID.Trim() + "', ";
                        Mi_SQL += "'" + Concepto_ID.Trim() + "', ";
                        Mi_SQL += "'" + Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + Clase.ToString().Trim() +
                             Concepto.Trim() + Renglon["SUBCONCEPTO"].ToString().Trim() + "', ";
                        Mi_SQL += "'" + HttpUtility.HtmlDecode(Renglon["DESCRIPCION"].ToString().ToUpper().Replace("\"", "").Replace("'", "")) + "', ";
                        Mi_SQL += "'ACTIVO', ";
                        Mi_SQL += "" + Renglon["ANIO"].ToString().Trim() + ", ";
                        Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += "GETDATE()) ";

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
                ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
            }
            catch (Exception ex)
            {

            }
        }
    #endregion

    #region (Btn_Cargar_Pronostico_Ingresos)
        protected void Btn_Cargar_Pronostico_Ingresos_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Clase_ID = String.Empty;
            String Fte_Financiamiento = String.Empty;
            String Concepto_ID = String.Empty;
            String SubConcepto_ID = String.Empty;
            String Rubro_ID = String.Empty;
            String Tipo_ID = String.Empty;            
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            String Pronostico_Id = String.Empty;
            Int32 Presupuesto_Id;
            object Id;

            String SqlExcel = "Select * From [PRONOSTICO_INGRESOS_2013$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Clase_ID = String.Empty;
                Fte_Financiamiento = String.Empty;
                Concepto_ID = String.Empty;
                Rubro_ID = String.Empty;
                Tipo_ID = String.Empty;

                if (!Renglon["FUENTE"].ToString().Trim().Contains("*"))
                {
                    Fte_Financiamiento = Consultar_Tablas(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID,
                    Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento,
                    Cat_SAP_Fuente_Financiamiento.Campo_Clave, Renglon["FUENTE"].ToString().Trim());

                    Rubro_ID = Consultar_Tablas(Cat_Psp_Rubro.Campo_Rubro_ID,
                        Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro,
                        Cat_Psp_Rubro.Campo_Clave, Renglon["RUBRO"].ToString().Trim() + "000000000");

                    Tipo_ID = Consultar_Tablas(Cat_Psp_Tipo.Campo_Tipo_ID,
                        Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo,
                        Cat_Psp_Tipo.Campo_Clave, 
                        Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() + "00000000");

                    Clase_ID = Consultar_Tablas(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID,
                        Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing,
                        Cat_Psp_Clase_Ing.Campo_Clave, 
                        Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() +
                        Renglon["CLASE"].ToString().Trim() + "000000");

                    Concepto_ID = Consultar_Tablas(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID,
                        Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing,
                        Cat_Psp_Concepto_Ing.Campo_Clave, 
                        Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() +
                        Renglon["CLASE"].ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + "0000");

                    SubConcepto_ID = Consultar_Tablas(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID,
                        Cat_Psp_SubConcepto_Ing.Tabla_Cat_Psp_SubConcepto_Ing,
                        Cat_Psp_SubConcepto_Ing.Campo_Clave,
                        Renglon["RUBRO"].ToString().Trim() + Renglon["TIPO"].ToString().Trim() +
                        Renglon["CLASE"].ToString().Trim() + Renglon["CONCEPTO"].ToString().Trim() + 
                        Renglon["SUBCONCEPTO"].ToString().Trim());
                }

                if (!String.IsNullOrEmpty(Fte_Financiamiento) && !String.IsNullOrEmpty(Clase_ID) && !String.IsNullOrEmpty(Concepto_ID)
                    && !String.IsNullOrEmpty(Tipo_ID) && !String.IsNullOrEmpty(Rubro_ID) && !String.IsNullOrEmpty(SubConcepto_ID))
                {
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + "), '0000000000')";
                    Mi_SQL += " FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos;
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Pronostico_Id = "0000000001";
                    }
                    else
                    {
                        Pronostico_Id = string.Format("{0:0000000000}", (Convert.ToInt32(Id.ToString().Trim()) + 1));
                    }


                    //INSERTAMOS EL PRONOSTICO DE INGRESOS
                    Mi_SQL = "INSERT INTO " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos;
                    Mi_SQL += "(" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_SubConcepto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Fecha_Creo + ") VALUES( ";
                    Mi_SQL += "'" + Pronostico_Id + "', ";
                    Mi_SQL += "'" + Fte_Financiamiento + "', ";
                    Mi_SQL += "'" + Rubro_ID + "', ";
                    Mi_SQL += "'" + Tipo_ID + "', ";
                    Mi_SQL += "'" + Clase_ID + "', ";
                    Mi_SQL += "'" + Concepto_ID + "', ";
                    Mi_SQL += "'" + SubConcepto_ID + "', ";
                    Mi_SQL += Renglon["ANIO"].ToString().Trim() + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString().Trim()) ? "0" : Renglon["TOTAL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "'EN CONSTRUCCION', ";
                    Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += "GETDATE()) ";

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    //OBTENEMOS EL ID DEL PRESUPUESTO DE INGRESOS
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + "), '0000000000')";
                    Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos;
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Presupuesto_Id = 1;
                    }
                    else
                    {
                        Presupuesto_Id = Convert.ToInt32(Id) + 1;
                    }

                    Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos;
                    Mi_SQL += "(" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_SubConcepto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Actualizado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES( ";
                    Mi_SQL += Presupuesto_Id + ", ";
                    Mi_SQL += "'" + Fte_Financiamiento + "', ";
                    Mi_SQL += "'" + Rubro_ID + "', ";
                    Mi_SQL += "'" + Tipo_ID + "', ";
                    Mi_SQL += "'" + Clase_ID + "', ";
                    Mi_SQL += "'" + Concepto_ID + "', ";
                    Mi_SQL += "'" + SubConcepto_ID + "', ";
                    Mi_SQL += Renglon["ANIO"].ToString().Trim() + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString().Trim()) ? "0" : Renglon["TOTAL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString().Trim()) ? "0" : Renglon["TOTAL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString().Trim()) ? "0" : Renglon["TOTAL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["TOTAL"].ToString().Trim()) ? "0" : Renglon["TOTAL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ENERO"].ToString().Trim()) ? "0" : Renglon["ENERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["FEBRERO"].ToString().Trim()) ? "0" : Renglon["FEBRERO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MARZO"].ToString().Trim()) ? "0" : Renglon["MARZO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["ABRIL"].ToString().Trim()) ? "0" : Renglon["ABRIL"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["MAYO"].ToString().Trim()) ? "0" : Renglon["MAYO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JUNIO"].ToString().Trim()) ? "0" : Renglon["JUNIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["JULIO"].ToString().Trim()) ? "0" : Renglon["JULIO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["AGOSTO"].ToString().Trim()) ? "0" : Renglon["AGOSTO"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["SEPTIEMBRE"].ToString().Trim()) ? "0" : Renglon["SEPTIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["OCTUBRE"].ToString().Trim()) ? "0" : Renglon["OCTUBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["NOVIEMBRE"].ToString().Trim()) ? "0" : Renglon["NOVIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["DICIEMBRE"].ToString().Trim()) ? "0" : Renglon["DICIEMBRE"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "'NO', ";
                    Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += "GETDATE()) ";

                    //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Psp_Ingresos)
        protected void Btn_Cargar_Psp_Ingresos_Click(object sender, EventArgs e)
        {
            Cls_Cat_Psp_Rubros_Datos Datos = new Cls_Cat_Psp_Rubros_Datos();
            String Clase_ID = String.Empty;
            String Fte_Financiamiento = String.Empty;
            String Concepto_ID = String.Empty;
            String Rubro_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String CLASE = String.Empty;
            String CONCEPTO = String.Empty;
            String RUBRO = String.Empty;
            String TIPO = String.Empty;
            String Mi_SQL = String.Empty;
            String Mensaje = String.Empty;
            String Nombre = String.Empty;
            String Pronostico_Id = String.Empty;
            Int32 Presupuesto_Id;
            object Id;

            String SqlExcel = "Select * From [GLOBAL$]";
            DataSet Ds_Psp = Leer_Excel(SqlExcel, "../../Archivos/Rubros_tipos.xlsx");
            DataTable Dt_Psp = Ds_Psp.Tables[0];
            foreach (DataRow Renglon in Dt_Psp.Rows)
            {
                Clase_ID = String.Empty;
                Fte_Financiamiento = String.Empty;
                Concepto_ID = String.Empty;
                Rubro_ID = String.Empty;
                Tipo_ID = String.Empty;

                if (!Renglon["Clas por Rubros"].ToString().Trim().Contains("*"))
                {

                    Nombre = Renglon["Clas por Rubros"].ToString().Trim();

                    CONCEPTO = Nombre.Substring(0, Nombre.IndexOf(" "));
                    CLASE = CONCEPTO.Substring(0, 4);
                    TIPO = CLASE.Substring(0, 2);
                    RUBRO = TIPO.Substring(0, 1);

                    Concepto_ID = Consultar_Tablas(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID,
                    Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing,
                    Cat_Psp_Concepto_Ing.Campo_Clave, CONCEPTO.Trim() + "0000");

                    Clase_ID = Consultar_Tablas(Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID,
                        Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing,
                        Cat_Psp_Clase_Ing.Campo_Clave, CLASE.Trim() + "000000");

                    Tipo_ID = Consultar_Tablas(Cat_Psp_Tipo.Campo_Tipo_ID,
                        Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo,
                        Cat_Psp_Tipo.Campo_Clave, TIPO.Trim() + "00000000");

                    Rubro_ID = Consultar_Tablas(Cat_Psp_Rubro.Campo_Rubro_ID,
                        Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro,
                        Cat_Psp_Rubro.Campo_Clave, RUBRO.Trim() + "000000000");

                    Fte_Financiamiento = Consultar_Tablas(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID,
                    Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento,
                    Cat_SAP_Fuente_Financiamiento.Campo_Clave, Renglon["Fuente Financiamiento"].ToString().Trim());
                }

                if (!String.IsNullOrEmpty(Fte_Financiamiento) && !String.IsNullOrEmpty(Clase_ID) && !String.IsNullOrEmpty(Concepto_ID) && !String.IsNullOrEmpty(Tipo_ID) && !String.IsNullOrEmpty(Rubro_ID))
                {
                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + "), '0000000000')";
                    Mi_SQL += " FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos;
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Pronostico_Id = "0000000001";
                    }
                    else
                    {
                        Pronostico_Id = string.Format("{0:0000000000}", (Convert.ToInt32(Id.ToString().Trim()) + 1));
                    }

                    Mi_SQL = "SELECT ISNULL(MAX (" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + "), '0000000000')";
                    Mi_SQL += " FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos;
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Presupuesto_Id = 1;
                    }
                    else
                    {
                        Presupuesto_Id = Convert.ToInt32(Id) + 1;
                    }

                    Mi_SQL = "INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos;
                    Mi_SQL += "(" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Devengado_Recaudado + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ";
                    Mi_SQL += Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES( ";
                    Mi_SQL += Presupuesto_Id + ", ";
                    Mi_SQL += "'" + Fte_Financiamiento + "', ";
                    Mi_SQL += "'" + Rubro_ID + "', ";
                    Mi_SQL += "'" + Tipo_ID + "', ";
                    Mi_SQL += "'" + Clase_ID + "', ";
                    Mi_SQL += "'" + Concepto_ID + "', ";
                    Mi_SQL += String.Format("{0:yyyy}", DateTime.Now) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += "0, ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Ampliacion"].ToString().Trim()) ? "0" : Renglon["Ampliacion"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Reduccion"].ToString().Trim()) ? "0" : Renglon["Reduccion"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Modificado"].ToString().Trim()) ? "0" : Renglon["Modificado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Devengado"].ToString().Trim()) ? "0" : Renglon["Devengado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Recaudado"].ToString().Trim()) ? "0" : Renglon["Recaudado"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Dev_Rec"].ToString().Trim()) ? "0" : Renglon["Dev_Rec"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Por Ejecutar"].ToString().Trim()) ? "0" : Renglon["Por Ejecutar"].ToString().Trim().Replace(",", "")) + ", ";
                    Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                    Mi_SQL += "GETDATE()) ";

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                    if (Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) > 0)
                    {

                        Mi_SQL = "INSERT INTO " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos;
                        Mi_SQL += "(" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Usuario_Creo + ", ";
                        Mi_SQL += Ope_Psp_Pronostico_Ingresos.Campo_Fecha_Creo + ") VALUES( ";
                        Mi_SQL += "'" + Pronostico_Id + "', ";
                        Mi_SQL += "'" + Fte_Financiamiento + "', ";
                        Mi_SQL += "'" + Rubro_ID + "', ";
                        Mi_SQL += "'" + Tipo_ID + "', ";
                        Mi_SQL += "'" + Clase_ID + "', ";
                        Mi_SQL += "'" + Concepto_ID + "', ";
                        Mi_SQL += String.Format("{0:yyyy}", DateTime.Now) + ", ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += "0, ";
                        Mi_SQL += Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ";
                        Mi_SQL += "'AUTORIZADO', ";
                        Mi_SQL += "'" + Cls_Sessiones.Nombre_Empleado + "', ";
                        Mi_SQL += "GETDATE()) ";
                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Fuentes)
        protected void Btn_Cargar_Fuentes_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_SQL = new StringBuilder(); ;
            String Mensaje = String.Empty;
            String FF_ID = String.Empty;
            object Id;

            String SqlExcel = "Select * From [FUENTES FINANCIAMIENTO$]";
            DataSet Ds= Leer_Excel(SqlExcel, "../../Archivos/Programas_FF_Validado.xlsx");
            DataTable Dt = Ds.Tables[0];
            foreach (DataRow Renglon in Dt.Rows)
            {
                //concultamos si existe la fuente de financiamiento
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Renglon["CLAVE"].ToString().Trim() + "'");

                FF_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (String.IsNullOrEmpty(FF_ID))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(MAX (" + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + "), '00000')");
                    Mi_SQL.Append( " FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        FF_ID = "00001";
                    }
                    else
                    {
                        FF_ID = string.Format("{0:00000}", Convert.ToInt32(Id) + 1);
                    }

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento + "(");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Clave + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Estatus + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Anio + ", ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + ") VALUES( ");
                    Mi_SQL.Append("'" + FF_ID + "', ");
                    Mi_SQL.Append("'" + Renglon["CLAVE"].ToString().Trim() + "', ");
                    Mi_SQL.Append("'" + HttpUtility.HtmlDecode(Renglon["DESCRIPCION"].ToString().Trim()) + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado.Trim() + "', ");
                    Mi_SQL.Append("GETDATE(), ");
                    Mi_SQL.Append("'ACTIVO', ");
                    Mi_SQL.Append("'" + Renglon["ANIO"].ToString().Trim() + "', ");
                    Mi_SQL.Append("'" + Renglon["RAMO33"].ToString().Trim() + "')");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                else 
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("UPDATE " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                    Mi_SQL.Append(" SET " + Cat_SAP_Fuente_Financiamiento.Campo_Descripcion + " = '" + HttpUtility.HtmlDecode(Renglon["DESCRIPCION"].ToString().Trim()) + "', ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.Trim() + "', ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Fecha_Modifico + " = GETDATE(), ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Anio + " = '" + Renglon["ANIO"].ToString().Trim() + "', ");
                    Mi_SQL.Append(Cat_SAP_Fuente_Financiamiento.Campo_Especiales_Ramo_33 + " = '" + Renglon["RAMO33"].ToString().Trim() + "'");
                    Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID + " = '" + FF_ID.Trim() + "'");
                    
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Programas)
        protected void Btn_Cargar_Programas_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_SQL = new StringBuilder(); ;
            String Mensaje = String.Empty;
            String PP_ID = String.Empty;
            object Id;

            String SqlExcel = "Select * From [PROGRAMAS_TODO$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/Programas_FF_Validado.xlsx");
            DataTable Dt = Ds.Tables[0];
            foreach (DataRow Renglon in Dt.Rows)
            {
                //concultamos si existe la fuente de financiamiento
                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Renglon["Clave"].ToString().Trim() + "'");

                PP_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (String.IsNullOrEmpty(PP_ID))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(MAX (" + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + "), '0000000000')");
                    Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        PP_ID = "0000000001";
                    }
                    else
                    {
                        PP_ID = string.Format("{0:0000000000}", Convert.ToInt32(Id) + 1);
                    }

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas + "(");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id + ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Clave+ ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Descripcion + ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Fecha_Creo + ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Estatus + ", ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Nombre + ") VALUES( ");
                    Mi_SQL.Append("'" + PP_ID + "', ");
                    Mi_SQL.Append("'" + Renglon["Clave"].ToString().Trim() + "', ");
                    Mi_SQL.Append("'" + HttpUtility.HtmlDecode(Renglon["Descripcion"].ToString().Trim()) + "', ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado.Trim() + "', ");
                    Mi_SQL.Append("GETDATE(), ");
                    Mi_SQL.Append("'ACTIVO', ");
                    Mi_SQL.Append("'" + HttpUtility.HtmlDecode(Renglon["Descripcion"].ToString().Trim()) + "')");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                else
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("UPDATE " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                    Mi_SQL.Append(" SET " + Cat_Sap_Proyectos_Programas.Campo_Descripcion + " = '" + HttpUtility.HtmlDecode(Renglon["Descripcion"].ToString().Trim()) + "', ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Usuario_Modifico + " = '" + Cls_Sessiones.Nombre_Empleado.Trim() + "', ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Fecha_Modifico + " = GETDATE(), ");
                    Mi_SQL.Append(Cat_Sap_Proyectos_Programas.Campo_Nombre + " = '" + HttpUtility.HtmlDecode(Renglon["Descripcion"].ToString().Trim()) + "' ");
                    Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id+ " = '" + PP_ID.Trim() + "'");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Concepto_FF)
        protected void Btn_Concepto_FF_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_SQL = new StringBuilder(); ;
            String Mensaje = String.Empty;
            String C_ID = String.Empty;
            String FF_ID = String.Empty;
            object Id;

            String SqlExcel = "Select * From [FF_CONCEPTO$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/Programas_FF_Validado.xlsx");
            DataTable Dt = Ds.Tables[0];
            foreach (DataRow Renglon in Dt.Rows)
            {
                //concultamos si existe la fuente de financiamiento

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Renglon["FF"].ToString().Trim() + "'");

                FF_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Renglon["CONCEPTO"].ToString().Trim() + "'");

                C_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (!String.IsNullOrEmpty(FF_ID) && !String.IsNullOrEmpty(C_ID))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Sap_Det_Fte_Concepto.Tabla_Cat_Sap_Det_Fte_Concepto + "(");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Concepto.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Concepto.Campo_Fuente_Financiamiento_ID + ") VALUES( ");
                    Mi_SQL.Append("'" + C_ID.Trim() + "', ");
                    Mi_SQL.Append("'" + FF_ID.Trim() + "')");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
                
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_PP_FF_Concepto)
        protected void Btn_PP_FF_Concepto_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_SQL = new StringBuilder(); ;
            String Mensaje = String.Empty;
            String C_ID = String.Empty;
            String FF_ID = String.Empty;
            String PP_ID = String.Empty;
            object Id;

            String SqlExcel = "Select * From [FF_PP_CONCEPTO$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/Programas_FF_Validado.xlsx");
            DataTable Dt = Ds.Tables[0];
            foreach (DataRow Renglon in Dt.Rows)
            {
                //concultamos si existe la fuente de financiamiento

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Renglon["FF"].ToString().Trim() + "'");

                FF_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Renglon["CONCEPTO"].ToString().Trim() + "'");

                C_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Renglon["PP"].ToString().Trim() + "'");

                PP_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (!String.IsNullOrEmpty(FF_ID) && !String.IsNullOrEmpty(C_ID) && !String.IsNullOrEmpty(PP_ID))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Cat_Sap_Det_Fte_Programa.Tabla_Cat_Sap_Det_Fte_Programa + "(");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe+ ", ");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Importe_Calendarizado + ", ");
                    Mi_SQL.Append(Cat_Sap_Det_Fte_Programa.Campo_Fuente_Financiamiento_ID + ") VALUES( ");
                    Mi_SQL.Append("'" + C_ID.Trim() + "', ");
                    Mi_SQL.Append("'" + PP_ID.Trim() + "', ");
                    Mi_SQL.Append(Renglon["IMPORTE"].ToString().Trim().Replace(",","") + ", ");
                    Mi_SQL.Append(Renglon["IMPORTE"].ToString().Trim().Replace(",", "") + ", ");
                    Mi_SQL.Append("'" + FF_ID.Trim() + "')");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
    #endregion

    #region (Btn_Cargar_Psp_Ingresos2)
        protected void Btn_Cargar_Psp_Ingresos2_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_SQL = new StringBuilder(); ;
            String Mensaje = String.Empty;
            String R_ID = String.Empty;
            String T_ID = String.Empty;
            String CL_ID = String.Empty;
            String C_ID = String.Empty;
            String FF_ID = String.Empty;
            String PP_ID = String.Empty;
            object Id;
            String Pronostico_Id = String.Empty;
            Int32 Presupuesto_Id;

            String SqlExcel = "Select * From [PRONOSTICO ING$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/Programas_FF_Validado.xlsx");
            DataTable Dt = Ds.Tables[0];
            foreach (DataRow Renglon in Dt.Rows)
            {
                //concultamos si existe la fuente de financiamiento

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID);
                Mi_SQL.Append(" FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
                Mi_SQL.Append(" WHERE " + Cat_SAP_Fuente_Financiamiento.Campo_Clave + " = '" + Renglon["FF"].ToString().Trim() + "'");

                FF_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Concepto_Ing.Tabla_Cat_Psp_Concepto_Ing);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Concepto_Ing.Campo_Clave + " = '" + Renglon["Concepto"].ToString().Trim() + "'");

                C_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id);
                Mi_SQL.Append(" FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
                Mi_SQL.Append(" WHERE " + Cat_Sap_Proyectos_Programas.Campo_Clave + " = '" + Renglon["PP"].ToString().Trim() + "'");

                PP_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Rubro.Campo_Rubro_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Rubro.Tabla_Cat_Psp_Rubro);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Rubro.Campo_Clave + " = '" + Renglon["RUBRO"].ToString().Trim() + "'");

                R_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Tipo.Campo_Tipo_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Tipo.Tabla_Cat_Psp_Tipo);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Tipo.Campo_Clave + " = '" + Renglon["TIPO"].ToString().Trim() + "'");

                T_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                Mi_SQL = new StringBuilder();
                Mi_SQL.Append("SELECT " + Cat_Psp_Clase_Ing.Campo_Clase_Ing_ID);
                Mi_SQL.Append(" FROM " + Cat_Psp_Clase_Ing.Tabla_Cat_Psp_Clase_Ing);
                Mi_SQL.Append(" WHERE " + Cat_Psp_Clase_Ing.Campo_Clave + " = '" + Renglon["CLASE"].ToString().Trim() + "'");

                CL_ID = (String)SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                if (!String.IsNullOrEmpty(FF_ID) && !String.IsNullOrEmpty(C_ID) && !String.IsNullOrEmpty(PP_ID)
                    && !String.IsNullOrEmpty(R_ID) && !String.IsNullOrEmpty(T_ID) && !String.IsNullOrEmpty(CL_ID))
                {
                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(MAX (" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + "), '0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Pronostico_Id = "0000000001";
                    }
                    else
                    {
                        Pronostico_Id = string.Format("{0:0000000000}", (Convert.ToInt32(Id.ToString().Trim()) + 1));
                    }

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("SELECT ISNULL(MAX (" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + "), '0000000000')");
                    Mi_SQL.Append(" FROM " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Id = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    if (Convert.IsDBNull(Id))
                    {
                        Presupuesto_Id = 1;
                    }
                    else
                    {
                        Presupuesto_Id = Convert.ToInt32(Id) + 1;
                    }

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Presupuesto_Ingresos.Tabla_Ope_Psp_Presupuesto_Ingresos);
                    Mi_SQL.Append( "(" + Ope_Psp_Presupuesto_Ingresos.Campo_Presupuesto_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Proyecto_Programa_ID+ ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Enero + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Febrero + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Marzo + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Abril + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Mayo + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Junio + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Julio + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Agosto + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Septiembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Octubre + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Noviembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Diciembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Importe_Total + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Aprobado + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Ampliacion + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Reduccion + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Modificado + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append( Ope_Psp_Presupuesto_Ingresos.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_SQL.Append( Presupuesto_Id + ", ");
                    Mi_SQL.Append( "'" + FF_ID + "', ");
                    Mi_SQL.Append( "'" + PP_ID + "', ");
                    Mi_SQL.Append( "'" + R_ID + "', ");
                    Mi_SQL.Append( "'" + T_ID+ "', ");
                    Mi_SQL.Append( "'" + CL_ID + "', ");
                    Mi_SQL.Append( "'" + C_ID + "', ");
                    Mi_SQL.Append( String.Format("{0:yyyy}", DateTime.Now) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Enero"].ToString().Trim()) ? "0" : Renglon["Enero"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Febrero"].ToString().Trim()) ? "0" : Renglon["Febrero"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Marzo"].ToString().Trim()) ? "0" : Renglon["Marzo"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Abril"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Mayo"].ToString().Trim()) ? "0" : Renglon["Mayo"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Junio"].ToString().Trim()) ? "0" : Renglon["Junio"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Julio"].ToString().Trim()) ? "0" : Renglon["Julio"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Agosto"].ToString().Trim()) ? "0" : Renglon["Agosto"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Septiembre"].ToString().Trim()) ? "0" : Renglon["Septiembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Octubre"].ToString().Trim()) ? "0" : Renglon["Octubre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Noviembre"].ToString().Trim()) ? "0" : Renglon["Noviembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Diciembre"].ToString().Trim()) ? "0" : Renglon["Diciembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Total"].ToString().Trim()) ? "0" : Renglon["Total"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append("0, ");
                    Mi_SQL.Append("0, ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append("0, ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Estimado"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append("'" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_SQL.Append("GETDATE()) ");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());

                    Mi_SQL = new StringBuilder();
                    Mi_SQL.Append("INSERT INTO " + Ope_Psp_Pronostico_Ingresos.Tabla_Ope_Psp_Pronostico_Ingresos);
                    Mi_SQL.Append( "(" + Ope_Psp_Pronostico_Ingresos.Campo_Pronostico_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Fuente_Financiamiento_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Proyecto_Programa_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Rubro_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Tipo_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Clase_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Concepto_Ing_ID + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Anio + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Enero + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Febrero + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Marzo + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Abril + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Mayo + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Junio + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Julio + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Agosto + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Septiembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Octubre + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Noviembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Diciembre + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Importe_Total + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Estatus + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Usuario_Creo + ", ");
                    Mi_SQL.Append( Ope_Psp_Pronostico_Ingresos.Campo_Fecha_Creo + ") VALUES( ");
                    Mi_SQL.Append( "'" + Pronostico_Id + "', ");
                    Mi_SQL.Append( "'" + FF_ID + "', ");
                    Mi_SQL.Append( "'" + PP_ID + "', ");
                    Mi_SQL.Append( "'" + R_ID + "', ");
                    Mi_SQL.Append( "'" + T_ID + "', ");
                    Mi_SQL.Append( "'" + CL_ID + "', ");
                    Mi_SQL.Append( "'" + C_ID + "', ");
                    Mi_SQL.Append( String.Format("{0:yyyy}", DateTime.Now) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Enero"].ToString().Trim()) ? "0" : Renglon["Enero"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Febrero"].ToString().Trim()) ? "0" : Renglon["Febrero"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Marzo"].ToString().Trim()) ? "0" : Renglon["Marzo"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Abril"].ToString().Trim()) ? "0" : Renglon["Estimado"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Mayo"].ToString().Trim()) ? "0" : Renglon["Mayo"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Junio"].ToString().Trim()) ? "0" : Renglon["Junio"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Julio"].ToString().Trim()) ? "0" : Renglon["Julio"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Agosto"].ToString().Trim()) ? "0" : Renglon["Agosto"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Septiembre"].ToString().Trim()) ? "0" : Renglon["Septiembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Octubre"].ToString().Trim()) ? "0" : Renglon["Octubre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Noviembre"].ToString().Trim()) ? "0" : Renglon["Noviembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Diciembre"].ToString().Trim()) ? "0" : Renglon["Diciembre"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append(Convert.ToDouble(String.IsNullOrEmpty(Renglon["Total"].ToString().Trim()) ? "0" : Renglon["Total"].ToString().Trim().Replace(",", "")) + ", ");
                    Mi_SQL.Append( "'AUTORIZADO', ");
                    Mi_SQL.Append( "'" + Cls_Sessiones.Nombre_Empleado + "', ");
                    Mi_SQL.Append( "GETDATE()) ");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL.ToString());
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2012", "alert('ACTUALIZADO');", true);
        }
        #endregion

    #region (Btn_2a_Modificacion_2013)
        protected void Btn_2a_Modificacion_2013_Click(object sender, EventArgs e)
        {
            String FF_ID = String.Empty;
            String AF_ID = String.Empty;
            String PP_ID = String.Empty;
            String UR_ID = String.Empty;
            String P_ID = String.Empty;
            String CAP_ID = String.Empty;
            String CC_ID = String.Empty;
            String SUELDO = String.Empty;
            DataTable Dt_FF = new DataTable();
            DataTable Dt_AF = new DataTable();
            DataTable Dt_PP = new DataTable();
            DataTable Dt_UR = new DataTable();
            DataTable Dt_P = new DataTable();
            DataTable Dt_CAP = new DataTable();
            DataTable Dt_CC = new DataTable();
            StringBuilder Mi_Sql = new StringBuilder();
            DataTable Dt_ID = new DataTable();

            String SqlExcel = "Select * From [2a-Mod$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/PSP_2A_MOD.xlsx");
            DataTable Dt = Ds.Tables[0];

            #region (Obtener tablas codigo programatico)
            //obtenemos las fuentes de financiamiento
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
            Dt_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las AREAS FUNCIONALES
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional );
            Dt_AF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los programas
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
            Dt_PP = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las dependencias
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
            Dt_UR = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las partidas
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
            Dt_P = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los capitulos
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
            Dt_CAP = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los cuentas contables
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
            Dt_CC = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            #endregion

            Dt = Agrupar_PSP(Dt);

            foreach (DataRow Dr in Dt.Rows)
            {
                FF_ID = String.Empty;
                AF_ID = String.Empty;
                PP_ID = String.Empty;
                UR_ID = String.Empty;
                P_ID = String.Empty;
                CAP_ID = String.Empty;
                SUELDO = String.Empty;
                CC_ID = String.Empty;

                #region (Obtener Id Codigo Programatico)
                //OBTENEMOS EL ID DEL CODIGO PROGRAMATICO
                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_FF.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Fuente_Financiamiento.Campo_Clave).Trim() == Dr["FF"].ToString().Trim()
                          select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        FF_ID = Dt_ID.Rows[0][Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_AF.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Area_Funcional.Campo_Clave).Trim() == Dr["AF"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        AF_ID = Dt_ID.Rows[0][Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_PP.AsEnumerable()
                         where Fila.Field<String>(Cat_Sap_Proyectos_Programas.Campo_Clave).Trim() == Dr["PP"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        PP_ID = Dt_ID.Rows[0][Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_UR.AsEnumerable()
                         where Fila.Field<String>(Cat_Dependencias.Campo_Clave).Trim() == Dr["UR"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        UR_ID = Dt_ID.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_P.AsEnumerable()
                         where Fila.Field<String>(Cat_Sap_Partidas_Especificas.Campo_Clave).Trim() == Dr["Partida"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        P_ID = Dt_ID.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_CAP.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Capitulos.Campo_Clave).Trim() == Dr["Cap"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        CAP_ID = Dt_ID.Rows[0][Cat_SAP_Capitulos.Campo_Capitulo_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_CC.AsEnumerable()
                         where Fila.Field<String>(Cat_Con_Cuentas_Contables.Campo_Cuenta).Trim() == Dr["CC"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if(Dt_ID.Rows.Count > 0)
                        CC_ID = Dt_ID.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();

                #endregion

                if (!String.IsNullOrEmpty(FF_ID) && !String.IsNullOrEmpty(AF_ID) && !String.IsNullOrEmpty(PP_ID)
                    && !String.IsNullOrEmpty(UR_ID) && !String.IsNullOrEmpty(P_ID) && !String.IsNullOrEmpty(CAP_ID))
                {
                    //consultamos si esta repetido el registro en el presupuesto
                    if (Repetido_Psp(FF_ID, AF_ID, PP_ID, UR_ID, P_ID, CAP_ID))
                    {
                        #region (Modificacion)
                        Mi_Sql = new StringBuilder();
                        Mi_Sql.Append(" UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                        Mi_Sql.Append(" SET ");
                        if (!String.IsNullOrEmpty(CC_ID))
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " = '" + CC_ID.Trim() + "', ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Enero"].ToString()) ? "0" : Dr["Imp_Enero"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Febrero"].ToString()) ? "0" : Dr["Imp_Febrero"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Marzo"].ToString()) ? "0" : Dr["Imp_Marzo"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Abril"].ToString()) ? "0" : Dr["Imp_Abril"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Mayo"].ToString()) ? "0" : Dr["Imp_Mayo"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Junio"].ToString()) ? "0" : Dr["Imp_Junio"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Julio"].ToString()) ? "0" : Dr["Imp_Julio"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Agosto"].ToString()) ? "0" : Dr["Imp_Agosto"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Septiembre"].ToString()) ? "0" : Dr["Imp_Septiembre"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Octubre"].ToString()) ? "0" : Dr["Imp_Octubre"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Noviembre"].ToString()) ? "0" : Dr["Imp_Noviembre"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Diciembre"].ToString()) ? "0" : Dr["Imp_Diciembre"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = 0,");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comprometido"].ToString()) ? "0" : Dr["Comprometido"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " = 0,");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + " = 0,");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = 0");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_May"].ToString()) ? "0" : Dr["Disp_May"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Disp_Jun"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Disp_Jul"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Disp_Ago"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Disp_Sep"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Disp_Oct"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Disp_Nov"].ToString()) + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Disp_Dic"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Ene"].ToString()) ? "0" : Dr["Comp_Ene"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Feb"].ToString()) ? "0" : Dr["Comp_Feb"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Mar"].ToString()) ? "0" : Dr["Comp_Mar"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + " = ");
                        //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Abr"].ToString()) ? "0" : Dr["Comp_Abr"].ToString()) + ", ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + " = 0, ");
                        //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + " = 0, ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'NO'");
                        Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " = '" + AF_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + PP_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + UR_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + P_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + CAP_ID.Trim() + "' ");
                        Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = 2013 ");

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                        #endregion
                    }
                    else 
                    {
                        #region (insersion)
                        
                        Mi_Sql = new StringBuilder();
                        Mi_Sql.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "(");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ") VALUES(");
                        Mi_Sql.Append("'" + FF_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + AF_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + PP_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + UR_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + P_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + CAP_ID.Trim() + "', ");
                        Mi_Sql.Append("2013, ");
                        
                        if (!String.IsNullOrEmpty(CC_ID))
                            Mi_Sql.Append("'" + CC_ID.Trim() + "', ");
                        else
                            Mi_Sql.Append("NULL, ");

                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Enero"].ToString()) ? "0" : Dr["Imp_Enero"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Febrero"].ToString()) ? "0" : Dr["Imp_Febrero"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Marzo"].ToString()) ? "0" : Dr["Imp_Marzo"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Abril"].ToString()) ? "0" : Dr["Imp_Abril"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Mayo"].ToString()) ? "0" : Dr["Imp_Mayo"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Imp_Junio"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Imp_Julio"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Imp_Agosto"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Imp_Septiembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Imp_Octubre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Imp_Noviembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Imp_Diciembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //ampliacion
                        Mi_Sql.Append("0, "); //reduccion
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //precomprometido
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comprometido"].ToString()) ? "0" : Dr["Comprometido"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //devengado
                        Mi_Sql.Append("0, "); //ejercido
                        Mi_Sql.Append("0, "); //pagado
                        Mi_Sql.Append("0, "); //disp  ene
                        Mi_Sql.Append("0, "); //disp feb
                        Mi_Sql.Append("0, "); //disp mar
                        Mi_Sql.Append("0, "); //disp abr
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Disp_Jun"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Disp_Jul"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Disp_Ago"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Disp_Sep"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Disp_Oct"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Disp_Nov"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Disp_Dic"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //precom ene
                        Mi_Sql.Append("0, "); //precom f
                        Mi_Sql.Append("0, "); //precom m
                        Mi_Sql.Append("0, "); //precom a 
                        Mi_Sql.Append("0, "); //precom m
                        Mi_Sql.Append("0, "); //precom j
                        Mi_Sql.Append("0, "); //precom j
                        Mi_Sql.Append("0, "); //precom a
                        Mi_Sql.Append("0, "); //precom s
                        Mi_Sql.Append("0, "); //precom o
                        Mi_Sql.Append("0, "); //precom n
                        Mi_Sql.Append("0, "); //precom d
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, "); // comp mayo
                        Mi_Sql.Append("0, "); // comp j
                        Mi_Sql.Append("0, "); // comp j 
                        Mi_Sql.Append("0, "); // comp a
                        Mi_Sql.Append("0, "); // comp s
                        Mi_Sql.Append("0, "); // comp o
                        Mi_Sql.Append("0, "); // comp n
                        Mi_Sql.Append("0, "); // comp d
                        Mi_Sql.Append("0, "); // dev ene
                        Mi_Sql.Append("0, "); // dev f
                        Mi_Sql.Append("0, "); // dev m
                        Mi_Sql.Append("0, "); // dev a
                        Mi_Sql.Append("0, "); // dev m
                        Mi_Sql.Append("0, "); // dev j
                        Mi_Sql.Append("0, "); // dev j
                        Mi_Sql.Append("0, "); // dev a
                        Mi_Sql.Append("0, "); // dev s
                        Mi_Sql.Append("0, "); // dev o
                        Mi_Sql.Append("0, "); // dev n
                        Mi_Sql.Append("0, "); // dev dic
                        Mi_Sql.Append("0, "); // ej ene
                        Mi_Sql.Append("0, "); // ej f
                        Mi_Sql.Append("0, "); // ej m
                        Mi_Sql.Append("0, "); // ej a
                        Mi_Sql.Append("0, "); // ej m
                        Mi_Sql.Append("0, "); // ej j
                        Mi_Sql.Append("0, "); // ej j
                        Mi_Sql.Append("0, "); // ej a
                        Mi_Sql.Append("0, "); // ej s
                        Mi_Sql.Append("0, "); // ej o
                        Mi_Sql.Append("0, "); // ej n
                        Mi_Sql.Append("0, "); // ej dic
                        Mi_Sql.Append("0, "); // pag ene
                        Mi_Sql.Append("0, "); // pag f
                        Mi_Sql.Append("0, "); // pag m
                        Mi_Sql.Append("0, "); // pag a
                        Mi_Sql.Append("0, "); // pag m
                        Mi_Sql.Append("0, "); // pag j
                        Mi_Sql.Append("0, "); // pag j
                        Mi_Sql.Append("0, "); // pag a
                        Mi_Sql.Append("0, "); // pag s
                        Mi_Sql.Append("0, "); // pag o
                        Mi_Sql.Append("0, "); // pag n
                        Mi_Sql.Append("0, "); // pag dic
                        Mi_Sql.Append(" 'NO')");

                        SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                        #endregion
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2013", "alert('ACTUALIZADO');", true);
        }

        public Boolean Repetido_Psp(String Fuente, String Area, String Progr, String Dep, String Partida, String Cap) 
        {
            StringBuilder Mi_Sql = new StringBuilder();
            DataTable Dt_Datos = new DataTable();
            Boolean Rep = false;

            Mi_Sql.Append("SELECT * FROM " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = 2013 ");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + Fuente.Trim() + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " = '" + Area.Trim() + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + Progr.Trim() + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + Dep.Trim() + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + Partida.Trim() + "'");
            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + Cap.Trim() + "'");

            Dt_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            if (Dt_Datos != null)
            {
                if (Dt_Datos.Rows.Count > 0)
                {
                    Rep = true;
                }
            }

            return Rep;
        }

        public DataTable Agrupar_PSP(DataTable Dt_PSP) 
        {
            DataTable Dt_Agrupado = new DataTable();
            DataTable Dt_Grup = new DataTable();

            if (Dt_PSP != null)
            {
                if (Dt_PSP.Rows.Count > 0)
                {
                    Dt_Agrupado.Columns.Add("CP", typeof(String));
                    Dt_Agrupado.Columns.Add("FF", typeof(String));
                    Dt_Agrupado.Columns.Add("AF", typeof(String));
                    Dt_Agrupado.Columns.Add("PP", typeof(String));
                    Dt_Agrupado.Columns.Add("UR", typeof(String));
                    Dt_Agrupado.Columns.Add("Partida", typeof(String));
                    Dt_Agrupado.Columns.Add("Cap", typeof(String));
                    Dt_Agrupado.Columns.Add("CC", typeof(String));
                    Dt_Agrupado.Columns.Add("Imp_Enero", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Febrero", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Marzo", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Abril", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Mayo", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Junio", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Julio", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Agosto", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Septiembre", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Octubre", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Noviembre", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Diciembre", typeof(Double));
                    Dt_Agrupado.Columns.Add("Imp_Total", typeof(Double));
                    Dt_Agrupado.Columns.Add("Comp_Ene", typeof(Double));
                    Dt_Agrupado.Columns.Add("Comp_Feb", typeof(Double));
                    Dt_Agrupado.Columns.Add("Comp_Mar", typeof(Double));
                    Dt_Agrupado.Columns.Add("Comp_Abr", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_May", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Jun", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Jul", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Ago", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Sep", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Oct", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Nov", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disp_Dic", typeof(Double));
                    Dt_Agrupado.Columns.Add("Disponible", typeof(Double));
                    Dt_Agrupado.Columns.Add("Comprometido", typeof(Double));
                    DataRow Fil = null;

                    foreach (DataRow Dr in Dt_PSP.Rows)
                    {
                        Fil = Dt_Agrupado.NewRow();
                        Fil["CP"] = Dr["CP"].ToString().Trim();
                        Fil["FF"] = Dr["FF"].ToString().Trim();
                        Fil["AF"] = Dr["AF"].ToString().Trim();
                        Fil["PP"] = Dr["PP"].ToString().Trim();
                        Fil["UR"] = Dr["UR"].ToString().Trim();
                        Fil["Partida"] = Dr["Partida"].ToString().Trim();
                        Fil["Cap"] = Dr["Capitulo"].ToString().Trim();
                        Fil["CC"] = Dr["CC"].ToString().Trim();
                        Fil["Imp_Enero"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Enero"].ToString()) ? "0" : Dr["Imp_Enero"]);
                        Fil["Imp_Febrero"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Febrero"].ToString()) ? "0" : Dr["Imp_Febrero"]);
                        Fil["Imp_Marzo"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Marzo"].ToString()) ? "0" : Dr["Imp_Marzo"]);
                        Fil["Imp_Abril"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Abril"].ToString()) ? "0" : Dr["Imp_Abril"]);
                        Fil["Imp_Mayo"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Mayo"].ToString()) ? "0" : Dr["Imp_Mayo"]); 
                        Fil["Imp_Junio"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Junio"].ToString()) ? "0" : Dr["Imp_Junio"]);
                        Fil["Imp_Julio"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Julio"].ToString()) ? "0" : Dr["Imp_Julio"]);
                        Fil["Imp_Agosto"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Agosto"].ToString()) ? "0" : Dr["Imp_Agosto"]);
                        Fil["Imp_Septiembre"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Septiembre"].ToString()) ? "0" : Dr["Imp_Septiembre"]);
                        Fil["Imp_Octubre"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Octubre"].ToString()) ? "0" : Dr["Imp_Octubre"]);
                        Fil["Imp_Noviembre"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Noviembre"].ToString()) ? "0" : Dr["Imp_Noviembre"]);
                        Fil["Imp_Diciembre"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Diciembre"].ToString()) ? "0" : Dr["Imp_Diciembre"]);
                        Fil["Imp_Total"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"]);
                        Fil["Comp_Ene"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Ene"].ToString()) ? "0" : Dr["Comp_Ene"]);
                        Fil["Comp_Feb"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Feb"].ToString()) ? "0" : Dr["Comp_Feb"]);
                        Fil["Comp_Mar"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Mar"].ToString()) ? "0" : Dr["Comp_Mar"]);
                        Fil["Comp_Abr"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Abr"].ToString()) ? "0" : Dr["Comp_Abr"]);
                        Fil["Disp_May"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_May"].ToString()) ? "0" : Dr["Disp_May"]);
                        Fil["Disp_Jun"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Disp_Jun"]);
                        Fil["Disp_Jul"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Disp_Jul"]);
                        Fil["Disp_Ago"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Disp_Ago"]);
                        Fil["Disp_Sep"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Disp_Sep"]);
                        Fil["Disp_Oct"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Disp_Oct"]);
                        Fil["Disp_Nov"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Disp_Nov"]);
                        Fil["Disp_Dic"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Disp_Dic"]);
                        Fil["Disponible"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"]);
                        Fil["Comprometido"] = Convert.ToDouble(String.IsNullOrEmpty(Dr["Comprometido"].ToString()) ? "0" : Dr["Comprometido"]);
                        Dt_Agrupado.Rows.Add(Fil);
                    }

                    var Agrupacion = (from Fila in Dt_Agrupado.AsEnumerable()
                                      group Fila by new
                                      {
                                          CP = Fila.Field<String>("CP"),
                                          FF = Fila.Field<String>("FF"),
                                          AF = Fila.Field<String>("AF"),
                                          PP = Fila.Field<String>("PP"),
                                          UR = Fila.Field<String>("UR"),
                                          PAR = Fila.Field<String>("Partida"),
                                          CAP = Fila.Field<String>("Cap"),
                                          CC = Fila.Field<String>("CC"),
                                      }
                                          into psp
                                          orderby psp.Key.CP
                                          select Dt_Agrupado.LoadDataRow(
                                            new object[]
                                            {
                                                psp.Key.CP,
                                                psp.Key.FF,
                                                psp.Key.AF,
                                                psp.Key.PP,
                                                psp.Key.UR,
                                                psp.Key.PAR,
                                                psp.Key.CAP,
                                                psp.Key.CC,
                                                psp.Sum(g => g.Field<Double>("Imp_Enero")),
                                                psp.Sum(g => g.Field<Double>("Imp_Febrero")),
                                                psp.Sum(g => g.Field<Double>("Imp_Marzo")),
                                                psp.Sum(g => g.Field<Double>("Imp_Abril")),
                                                psp.Sum(g => g.Field<Double>("Imp_Mayo")),
                                                psp.Sum(g => g.Field<Double>("Imp_Junio")),
                                                psp.Sum(g => g.Field<Double>("Imp_Julio")),
                                                psp.Sum(g => g.Field<Double>("Imp_Agosto")),
                                                psp.Sum(g => g.Field<Double>("Imp_Septiembre")),
                                                psp.Sum(g => g.Field<Double>("Imp_Octubre")),
                                                psp.Sum(g => g.Field<Double>("Imp_Noviembre")),
                                                psp.Sum(g => g.Field<Double>("Imp_Diciembre")),
                                                psp.Sum(g => g.Field<Double>("Imp_Total")),
                                                psp.Sum(g => g.Field<Double>("Comp_Ene")),
                                                psp.Sum(g => g.Field<Double>("Comp_Feb")),
                                                psp.Sum(g => g.Field<Double>("Comp_Mar")),
                                                psp.Sum(g => g.Field<Double>("Comp_Abr")),
                                                psp.Sum(g => g.Field<Double>("Disp_May")),
                                                psp.Sum(g => g.Field<Double>("Disp_Jun")),
                                                psp.Sum(g => g.Field<Double>("Disp_Jul")),
                                                psp.Sum(g => g.Field<Double>("Disp_Ago")),
                                                psp.Sum(g => g.Field<Double>("Disp_Sep")),
                                                psp.Sum(g => g.Field<Double>("Disp_Oct")),
                                                psp.Sum(g => g.Field<Double>("Disp_Nov")),
                                                psp.Sum(g => g.Field<Double>("Disp_Dic")),
                                                psp.Sum(g => g.Field<Double>("Disponible")),
                                                psp.Sum(g => g.Field<Double>("Comprometido")),
                                            }, LoadOption.PreserveChanges));

                    //obtenemos los datos agrupados en una tabla
                    Dt_Grup = Agrupacion.CopyToDataTable();
                }
            }

            return Dt_Grup;
        }

        protected void Btn_2a_Modificacion_2013_Economias_Click(object sender, EventArgs e)
        {
            String FF_ID = String.Empty;
            String AF_ID = String.Empty;
            String PP_ID = String.Empty;
            String UR_ID = String.Empty;
            String P_ID = String.Empty;
            String CAP_ID = String.Empty;
            String CC_ID = String.Empty;
            String SUELDO = String.Empty;
            DataTable Dt_FF = new DataTable();
            DataTable Dt_AF = new DataTable();
            DataTable Dt_PP = new DataTable();
            DataTable Dt_UR = new DataTable();
            DataTable Dt_P = new DataTable();
            DataTable Dt_CAP = new DataTable();
            DataTable Dt_CC = new DataTable();
            StringBuilder Mi_Sql = new StringBuilder();
            DataTable Dt_ID = new DataTable();

            String SqlExcel = "Select * From [2a-Mod$]";
            DataSet Ds = Leer_Excel(SqlExcel, "../../Archivos/PSP_2A_MOD.xlsx");
            DataTable Dt = Ds.Tables[0];

            #region (Obtener tablas codigo programatico)
            //obtenemos las fuentes de financiamiento
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Fuente_Financiamiento.Tabla_Cat_SAP_Fuente_Financiamiento);
            Dt_FF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las AREAS FUNCIONALES
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Area_Funcional.Tabla_Cat_SAP_Area_Funcional);
            Dt_AF = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los programas
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Sap_Proyectos_Programas.Tabla_Cat_Sap_Proyectos_Programas);
            Dt_PP = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las dependencias
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Dependencias.Tabla_Cat_Dependencias);
            Dt_UR = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos las partidas
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas);
            Dt_P = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los capitulos
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_SAP_Capitulos.Tabla_Cat_SAP_Capitulos);
            Dt_CAP = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];

            //obtenemos los cuentas contables
            Mi_Sql = new StringBuilder();
            Mi_Sql.Append("SELECT * FROM " + Cat_Con_Cuentas_Contables.Tabla_Cat_Con_Cuentas_Contables);
            Dt_CC = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString()).Tables[0];
            #endregion

            Dt = Agrupar_PSP(Dt);

            foreach (DataRow Dr in Dt.Rows)
            {
                FF_ID = String.Empty;
                AF_ID = String.Empty;
                PP_ID = String.Empty;
                UR_ID = String.Empty;
                P_ID = String.Empty;
                CAP_ID = String.Empty;
                SUELDO = String.Empty;
                CC_ID = String.Empty;

                #region (Obtener Id Codigo Programatico)
                //OBTENEMOS EL ID DEL CODIGO PROGRAMATICO
                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_FF.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Fuente_Financiamiento.Campo_Clave).Trim() == Dr["FF"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        FF_ID = Dt_ID.Rows[0][Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_AF.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Area_Funcional.Campo_Clave).Trim() == Dr["AF"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        AF_ID = Dt_ID.Rows[0][Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_PP.AsEnumerable()
                         where Fila.Field<String>(Cat_Sap_Proyectos_Programas.Campo_Clave).Trim() == Dr["PP"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        PP_ID = Dt_ID.Rows[0][Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_UR.AsEnumerable()
                         where Fila.Field<String>(Cat_Dependencias.Campo_Clave).Trim() == Dr["UR"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        UR_ID = Dt_ID.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_P.AsEnumerable()
                         where Fila.Field<String>(Cat_Sap_Partidas_Especificas.Campo_Clave).Trim() == Dr["Partida"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        P_ID = Dt_ID.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_CAP.AsEnumerable()
                         where Fila.Field<String>(Cat_SAP_Capitulos.Campo_Clave).Trim() == Dr["Cap"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        CAP_ID = Dt_ID.Rows[0][Cat_SAP_Capitulos.Campo_Capitulo_ID].ToString().Trim();

                Dt_ID = new DataTable();
                Dt_ID = (from Fila in Dt_CC.AsEnumerable()
                         where Fila.Field<String>(Cat_Con_Cuentas_Contables.Campo_Cuenta).Trim() == Dr["CC"].ToString().Trim()
                         select Fila).AsDataView().ToTable();
                if (Dt_ID != null)
                    if (Dt_ID.Rows.Count > 0)
                        CC_ID = Dt_ID.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString().Trim();

                #endregion

                if (!String.IsNullOrEmpty(FF_ID) && !String.IsNullOrEmpty(AF_ID) && !String.IsNullOrEmpty(PP_ID)
                    && !String.IsNullOrEmpty(UR_ID) && !String.IsNullOrEmpty(P_ID) && !String.IsNullOrEmpty(CAP_ID))
                {
                    //consultamos si esta repetido el registro en el presupuesto
                    if (Repetido_Psp(FF_ID, AF_ID, PP_ID, UR_ID, P_ID, CAP_ID))
                    {
                        if (Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_May"].ToString()) ? "0" : Dr["Disp_May"].ToString()) > 0)
                        {
                            #region (Modificacion)
                            Mi_Sql = new StringBuilder();
                            Mi_Sql.Append(" UPDATE " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado);
                            Mi_Sql.Append(" SET ");
                            if (!String.IsNullOrEmpty(CC_ID))
                                Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + " = '" + CC_ID.Trim() + "', ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Enero"].ToString()) ? "0" : Dr["Imp_Enero"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Febrero"].ToString()) ? "0" : Dr["Imp_Febrero"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Marzo"].ToString()) ? "0" : Dr["Imp_Marzo"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Abril"].ToString()) ? "0" : Dr["Imp_Abril"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Mayo"].ToString()) ? "0" : Dr["Imp_Mayo"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Junio"].ToString()) ? "0" : Dr["Imp_Junio"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Julio"].ToString()) ? "0" : Dr["Imp_Julio"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Agosto"].ToString()) ? "0" : Dr["Imp_Agosto"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Septiembre"].ToString()) ? "0" : Dr["Imp_Septiembre"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Octubre"].ToString()) ? "0" : Dr["Imp_Octubre"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Noviembre"].ToString()) ? "0" : Dr["Imp_Noviembre"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Diciembre"].ToString()) ? "0" : Dr["Imp_Diciembre"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + " = 0,");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comprometido"].ToString()) ? "0" : Dr["Comprometido"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + " = 0,");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + " = 0,");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + " = 0, ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + " = ");
                            Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_May"].ToString()) ? "0" : Dr["Disp_May"].ToString()) + ", ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " = ");
                            Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + " + ");
                            Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_May"].ToString()) ? "0" : Dr["Disp_May"].ToString()) + " ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Disp_Jul"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Disp_Ago"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Disp_Sep"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Disp_Oct"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Disp_Nov"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Disp_Dic"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Ene"].ToString()) ? "0" : Dr["Comp_Ene"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Feb"].ToString()) ? "0" : Dr["Comp_Feb"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Mar"].ToString()) ? "0" : Dr["Comp_Mar"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + " = ");
                            //Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comp_Abr"].ToString()) ? "0" : Dr["Comp_Abr"].ToString()) + ", ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + " = 0, ");
                            //Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + " = 'NO'");
                            Mi_Sql.Append(" WHERE " + Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + " = '" + FF_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + " = '" + AF_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + " = '" + PP_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + " = '" + UR_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + " = '" + P_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + " = '" + CAP_ID.Trim() + "' ");
                            Mi_Sql.Append(" AND " + Ope_Psp_Presupuesto_Aprobado.Campo_Anio + " = 2013 ");

                            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                            #endregion
                        }
                    }
                    else
                    {
                        #region (insersion)

                        Mi_Sql = new StringBuilder();
                        Mi_Sql.Append("INSERT INTO " + Ope_Psp_Presupuesto_Aprobado.Tabla_Ope_Psp_Presupuesto_Aprobado + "(");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Fte_Financiamiento_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Area_Funcional_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Proyecto_Programa_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Dependencia_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Partida_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Capitulo_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Anio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Cuenta_Contable_ID + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Importe_Total + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Aprobado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ampliacion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Reduccion + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Modificado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Disponible_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pre_Comprometido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Comprometido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Devengado_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Ejercido_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Enero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Febrero + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Marzo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Abril + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Mayo + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Junio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Julio + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Agosto + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Septiembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Octubre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Noviembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Pagado_Diciembre + ", ");
                        Mi_Sql.Append(Ope_Psp_Presupuesto_Aprobado.Campo_Actualizado + ") VALUES(");
                        Mi_Sql.Append("'" + FF_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + AF_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + PP_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + UR_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + P_ID.Trim() + "', ");
                        Mi_Sql.Append("'" + CAP_ID.Trim() + "', ");
                        Mi_Sql.Append("2013, ");

                        if (!String.IsNullOrEmpty(CC_ID))
                            Mi_Sql.Append("'" + CC_ID.Trim() + "', ");
                        else
                            Mi_Sql.Append("NULL, ");

                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Enero"].ToString()) ? "0" : Dr["Imp_Enero"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Febrero"].ToString()) ? "0" : Dr["Imp_Febrero"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Marzo"].ToString()) ? "0" : Dr["Imp_Marzo"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Abril"].ToString()) ? "0" : Dr["Imp_Abril"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Mayo"].ToString()) ? "0" : Dr["Imp_Mayo"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Imp_Junio"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Imp_Julio"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Imp_Agosto"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Imp_Septiembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Imp_Octubre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Imp_Noviembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Imp_Diciembre"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Imp_Total"].ToString()) ? "0" : Dr["Imp_Total"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //ampliacion
                        Mi_Sql.Append("0, "); //reduccion
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disponible"].ToString()) ? "0" : Dr["Disponible"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //precomprometido
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Comprometido"].ToString()) ? "0" : Dr["Comprometido"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //devengado
                        Mi_Sql.Append("0, "); //ejercido
                        Mi_Sql.Append("0, "); //pagado
                        Mi_Sql.Append("0, "); //disp  ene
                        Mi_Sql.Append("0, "); //disp feb
                        Mi_Sql.Append("0, "); //disp mar
                        Mi_Sql.Append("0, "); //disp abr
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jun"].ToString()) ? "0" : Dr["Disp_Jun"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Jul"].ToString()) ? "0" : Dr["Disp_Jul"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Ago"].ToString()) ? "0" : Dr["Disp_Ago"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Sep"].ToString()) ? "0" : Dr["Disp_Sep"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Oct"].ToString()) ? "0" : Dr["Disp_Oct"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Nov"].ToString()) ? "0" : Dr["Disp_Nov"].ToString()) + ", ");
                        Mi_Sql.Append(Convert.ToDouble(String.IsNullOrEmpty(Dr["Disp_Dic"].ToString()) ? "0" : Dr["Disp_Dic"].ToString()) + ", ");
                        Mi_Sql.Append("0, "); //precom ene
                        Mi_Sql.Append("0, "); //precom f
                        Mi_Sql.Append("0, "); //precom m
                        Mi_Sql.Append("0, "); //precom a 
                        Mi_Sql.Append("0, "); //precom m
                        Mi_Sql.Append("0, "); //precom j
                        Mi_Sql.Append("0, "); //precom j
                        Mi_Sql.Append("0, "); //precom a
                        Mi_Sql.Append("0, "); //precom s
                        Mi_Sql.Append("0, "); //precom o
                        Mi_Sql.Append("0, "); //precom n
                        Mi_Sql.Append("0, "); //precom d
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, ");
                        Mi_Sql.Append("0, "); // comp mayo
                        Mi_Sql.Append("0, "); // comp j
                        Mi_Sql.Append("0, "); // comp j 
                        Mi_Sql.Append("0, "); // comp a
                        Mi_Sql.Append("0, "); // comp s
                        Mi_Sql.Append("0, "); // comp o
                        Mi_Sql.Append("0, "); // comp n
                        Mi_Sql.Append("0, "); // comp d
                        Mi_Sql.Append("0, "); // dev ene
                        Mi_Sql.Append("0, "); // dev f
                        Mi_Sql.Append("0, "); // dev m
                        Mi_Sql.Append("0, "); // dev a
                        Mi_Sql.Append("0, "); // dev m
                        Mi_Sql.Append("0, "); // dev j
                        Mi_Sql.Append("0, "); // dev j
                        Mi_Sql.Append("0, "); // dev a
                        Mi_Sql.Append("0, "); // dev s
                        Mi_Sql.Append("0, "); // dev o
                        Mi_Sql.Append("0, "); // dev n
                        Mi_Sql.Append("0, "); // dev dic
                        Mi_Sql.Append("0, "); // ej ene
                        Mi_Sql.Append("0, "); // ej f
                        Mi_Sql.Append("0, "); // ej m
                        Mi_Sql.Append("0, "); // ej a
                        Mi_Sql.Append("0, "); // ej m
                        Mi_Sql.Append("0, "); // ej j
                        Mi_Sql.Append("0, "); // ej j
                        Mi_Sql.Append("0, "); // ej a
                        Mi_Sql.Append("0, "); // ej s
                        Mi_Sql.Append("0, "); // ej o
                        Mi_Sql.Append("0, "); // ej n
                        Mi_Sql.Append("0, "); // ej dic
                        Mi_Sql.Append("0, "); // pag ene
                        Mi_Sql.Append("0, "); // pag f
                        Mi_Sql.Append("0, "); // pag m
                        Mi_Sql.Append("0, "); // pag a
                        Mi_Sql.Append("0, "); // pag m
                        Mi_Sql.Append("0, "); // pag j
                        Mi_Sql.Append("0, "); // pag j
                        Mi_Sql.Append("0, "); // pag a
                        Mi_Sql.Append("0, "); // pag s
                        Mi_Sql.Append("0, "); // pag o
                        Mi_Sql.Append("0, "); // pag n
                        Mi_Sql.Append("0, "); // pag dic
                        Mi_Sql.Append(" 'NO')");

                        //SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());

                        #endregion
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2013", "alert('ACTUALIZADO');", true);
        }

        
    #endregion

    #region(Btn_Reg_Mov_Click)
        protected void Btn_Reg_Mov_Click(object sender, EventArgs e)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            String SqlExcel = String.Empty;
            DataSet Ds = new DataSet();
            DataTable Dt = new DataTable();
            SqlConnection Conexion = new SqlConnection();
            SqlCommand Comando = new SqlCommand();
            SqlTransaction Transaccion = null;


            Conexion.ConnectionString = Cls_Constantes.Str_Conexion;
            Conexion.Open();
            Transaccion = Conexion.BeginTransaction();
            Comando.Connection = Transaccion.Connection;
            Comando.Transaction = Transaccion;

            try
            {
                SqlExcel = "Select * From [MM1$]";
                Ds = Leer_Excel(SqlExcel, "../../Archivos/MOMENTOS_PRESUPUESTALES.xlsx");
                Dt = Ds.Tables[0];

                foreach (DataRow Dr in Dt.Rows)
                {
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                    Mi_Sql.Append(" SET " + Ope_Psp_Registro_Movimientos.Campo_Estatus_Movimiento + " = '" + Dr["ESTATUS_MOVIMIENTO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = '" + Dr["CARGO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = '" + Dr["ABONO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = '" + Dr["NO_RESERVA"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = " + Dr["IMPORTE"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Fecha + " = CONVERT(DATETIME, '" + Dr["FECHA"].ToString().Trim() + "', 121)");

                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_Sql.ToString());
                }


                SqlExcel = String.Empty;
                Ds = new DataSet();
                Dt = new DataTable();

                SqlExcel = "Select * From [MM2$]";
                Ds = Leer_Excel(SqlExcel, "../../Archivos/MOMENTOS_PRESUPUESTALES.xlsx");
                Dt = Ds.Tables[0];

                foreach (DataRow Dr in Dt.Rows)
                {
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                    Mi_Sql.Append(" SET " + Ope_Psp_Registro_Movimientos.Campo_Estatus_Movimiento + " = '" + Dr["ESTATUS_MOVIMIENTO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = '" + Dr["CARGO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = '" + Dr["ABONO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = " + Dr["NO_RESERVA"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = " + Dr["IMPORTE"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Fecha + " = CONVERT(DATETIME, '" + Dr["FECHA"].ToString().Trim() + "', 121)");

                    Comando.CommandText = Mi_Sql.ToString();
                    Comando.ExecuteNonQuery();
                }

                SqlExcel = String.Empty;
                Ds = new DataSet();
                Dt = new DataTable();

                SqlExcel = "Select * From [MM3$]";
                Ds = Leer_Excel(SqlExcel, "../../Archivos/MOMENTOS_PRESUPUESTALES.xlsx");
                Dt = Ds.Tables[0];

                foreach (DataRow Dr in Dt.Rows)
                {
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                    Mi_Sql.Append(" SET " + Ope_Psp_Registro_Movimientos.Campo_Estatus_Movimiento + " = '" + Dr["ESTATUS_MOVIMIENTO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = '" + Dr["CARGO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = '" + Dr["ABONO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = '" + Dr["NO_RESERVA"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = " + Dr["IMPORTE"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Fecha + " = CONVERT(DATETIME, '" + Dr["FECHA"].ToString().Trim() + "', 121)");

                    Comando.CommandText = Mi_Sql.ToString();
                    Comando.ExecuteNonQuery();
                }


                SqlExcel = String.Empty;
                Ds = new DataSet();
                Dt = new DataTable();

                SqlExcel = "Select * From [MM4$]";
                Ds = Leer_Excel(SqlExcel, "../../Archivos/MOMENTOS_PRESUPUESTALES.xlsx");
                Dt = Ds.Tables[0];

                foreach (DataRow Dr in Dt.Rows)
                {
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                    Mi_Sql.Append(" SET " + Ope_Psp_Registro_Movimientos.Campo_Estatus_Movimiento + " = '" + Dr["ESTATUS_MOVIMIENTO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = '" + Dr["CARGO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = '" + Dr["ABONO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = '" + Dr["NO_RESERVA"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = " + Dr["IMPORTE"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Fecha + " = CONVERT(DATETIME, '" + Dr["FECHA"].ToString().Trim() + "', 121)");

                    Comando.CommandText = Mi_Sql.ToString();
                    Comando.ExecuteNonQuery();
                }

                SqlExcel = String.Empty;
                Ds = new DataSet();
                Dt = new DataTable();

                SqlExcel = "Select * From [MM5$]";
                Ds = Leer_Excel(SqlExcel, "../../Archivos/MOMENTOS_PRESUPUESTALES.xlsx");
                Dt = Ds.Tables[0];

                foreach (DataRow Dr in Dt.Rows)
                {
                    Mi_Sql.Append("UPDATE " + Ope_Psp_Registro_Movimientos.Tabla_Ope_Psp_Registro_Movimientos);
                    Mi_Sql.Append(" SET " + Ope_Psp_Registro_Movimientos.Campo_Estatus_Movimiento + " = '" + Dr["ESTATUS_MOVIMIENTO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" WHERE " + Ope_Psp_Registro_Movimientos.Campo_Cargo + " = '" + Dr["CARGO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Abono + " = '" + Dr["ABONO"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_No_Reserva + " = '" + Dr["NO_RESERVA"].ToString().Trim() + "'");
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Importe + " = " + Dr["IMPORTE"].ToString().Trim());
                    Mi_Sql.Append(" AND " + Ope_Psp_Registro_Movimientos.Campo_Fecha + " = CONVERT(DATETIME, '" + Dr["FECHA"].ToString().Trim() + "', 121)");

                    Comando.CommandText = Mi_Sql.ToString();
                    Comando.ExecuteNonQuery();
                }

                Transaccion.Commit();
                Conexion.Close();
            }
            catch (Exception)
            {
                Transaccion.Commit();
                Conexion.Close();
                throw;
            }
            finally 
            {
                Transaccion.Commit();
                Conexion.Close();
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "PSP_2013", "alert('ACTUALIZADO');", true);
        }
    #endregion
}
