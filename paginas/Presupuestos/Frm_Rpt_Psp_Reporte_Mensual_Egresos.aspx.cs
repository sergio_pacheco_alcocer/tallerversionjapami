﻿using System;
using System.Configuration;
using System.Data;
using System.Web;
using System.Text;
using JAPAMI.DateDiff;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Rpt_Psp_Reporte_Mensual_Egresos.Negocios;
using JAPAMI.Reporte_Presupuesto_Egresos.Negocio;
using System.Linq;
using System.Web.UI.WebControls;
using JAPAMI.Psp_Presupuesto.Negocio;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Psp_Presupuesto_Egresos_Ingresos.Negocio;
using System.Web.UI;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Reporte_Mensual_Egresos : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Inicializa_Controles " + Ex.Message.ToString());
            }

        }
    #endregion

    #region(Metodos)
        #region(Metodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION         : Prepara los controles en la forma para que el usuario pueda
            ///                realizar diferentes operaciones
            /// PARAMETROS          : 
            /// CREO                : LESLIE GONZÁLEZ VÁZQUEZ
            /// FECHA_CREO          : 25/FEBRERO/2013
            /// MODIFICO            :
            /// FECHA_MODIFICO      :
            /// CAUSA_MODIFICACION  :
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Limpiar_Controles(); //Limpia los controles del formulario
                    Obtener_Tipo_Usuario();
                    Llenar_Combo_FF();
                    Llenar_Combo_Area_Funcional();
                    Llenar_Combo_Programas();
                    Llenar_Combo_UR();
                    Llenar_Combo_Partidas();
                    Llenar_Grid();
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString());
                }
            }

            //*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
            ///DESCRIPCIÓN          : metodo para ñimpiar los controles de la pagina
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Limpiar_Controles()
            {
                try
                {
                    Cmb_Area_Funcional.SelectedIndex = -1;
                    Cmb_UR.SelectedIndex = -1;
                    Cmb_PP.SelectedIndex = -1;
                    Cmb_Partida.SelectedIndex = -1;
                    Cmb_FF.SelectedIndex = -1;
                    Lbl_Mensaje_Error.Text = "";
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Hf_Tipo_Usuario.Value = String.Empty;
                    Hf_Gpo_Dependencia.Value = String.Empty;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString());
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Gpo_Dependencia
            ///DESCRIPCIÓN          : metodo para obtener el grupo dependencia para el reporte
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private String Obtener_Gpo_Dependencia(String Tipo)
            {
                Cls_Rpt_Presupuesto_Egresos_Negocio Negocios = new Cls_Rpt_Presupuesto_Egresos_Negocio();
                String Dato = String.Empty;
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    Dt_Datos = Negocios.Consultar_Gpo_Dependencia();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            if (Tipo.Trim().Equals("UR"))
                            {
                                Dato = Dt_Datos.Rows[0]["UR"].ToString().Trim();
                            }
                            else
                            {
                                Dato = Dt_Datos.Rows[0]["GPO_DEP"].ToString().Trim();
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al obtener el grupo dependencia. Error[" + Ex.Message + "]");
                }
                return Dato;
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Tipo_Usuario
            ///DESCRIPCIÓN          : Metodo para obtener el tipo de usuario
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 17/Agostp/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Obtener_Tipo_Usuario()
            {
                Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
                Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio_PSP = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
                String Administrador = String.Empty;
                DataTable Dt_Ramo33 = new DataTable();
                String Tipo_Usuario = String.Empty;
                String Gpo_Dep = String.Empty;
                DataTable Dt_Gpo_Dep = new DataTable();

                try
                {
                    //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
                    Negocio_PSP.P_Rol_ID = Cls_Sessiones.Rol_ID.Trim();
                    Administrador = Negocio_PSP.Consultar_Rol();

                    if (!String.IsNullOrEmpty(Administrador.Trim()))
                    {
                        if (Administrador.Trim().Equals("Presupuestos"))
                        {
                            Tipo_Usuario = "Administrador";
                        }
                        else if (Administrador.Trim().Equals("Nomina"))
                        {
                            Tipo_Usuario = "Nomina";
                            //Tipo_Usuario = "Administrador";
                        }
                    }
                    else
                    {
                        //verificamos si el usuario logueado es el administrador de ramo33
                        Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                        if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                        {
                            if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                                || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                            {
                                Tipo_Usuario = "Inversiones";
                            }
                            else
                            {
                                Tipo_Usuario = "Usuario";
                            }
                        }
                        else
                        {
                            Tipo_Usuario = "Usuario";
                        }

                        if (Tipo_Usuario.Trim().Equals("Usuario"))
                        {
                            //verificamos si no es algun coordinador administrativo de direccion
                            Negocio_PSP.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                            Dt_Gpo_Dep = Negocio_PSP.Obtener_Gpo_Dependencia();

                            if (Dt_Gpo_Dep != null && Dt_Gpo_Dep.Rows.Count > 0)
                            {
                                Gpo_Dep = Dt_Gpo_Dep.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                                Tipo_Usuario = "Coordinador";
                            }
                        }
                    }

                    Hf_Tipo_Usuario.Value =  Tipo_Usuario.Trim();
                    Hf_Gpo_Dependencia.Value = Gpo_Dep.Trim();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
                }
            }
        #endregion

        #region(Reporte)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_PSP
            ///DESCRIPCIÓN          : metodo para generar el reporte en excel
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Generar_Reporte_PSP(DataTable Dt_Registros, String Dependencia, String Gpo_Dependencia)
            {
                String Anio = DateTime.Now.Year.ToString().Trim();
                Double Cantidad = 0.00;

                WorksheetCell Celda = new WorksheetCell();
                String Ruta_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";
                try
                {
                    //Creamos el libro de Excel.
                    CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                    //propiedades del libro
                    Libro.Properties.Title = "Reporte_Presupuesto";
                    Libro.Properties.Created = DateTime.Now;
                    Libro.Properties.Author = "JAPAMI";

                    #region Estilos
                    //Creamos el estilo cabecera para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                    //Creamos el estilo cabecera 2 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                    //Creamos el estilo cabecera 3 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                    //Creamos el estilo cabecera 3 para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera4 = Libro.Styles.Add("HeaderStyle4");
                    //Creamos el estilo contenido para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto = Libro.Styles.Add("Presupuesto");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Neg = Libro.Styles.Add("Presupuesto_Neg");
                    //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Presupuesto_Total");
                    //Creamos el estilo contenido del presupuesto total para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total_Neg = Libro.Styles.Add("Presupuesto_Total_Neg");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto = Libro.Styles.Add("Concepto");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto_Total = Libro.Styles.Add("Concepto_Total");
                    //Creamos el estilo contenido del concepto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Partida_Total = Libro.Styles.Add("Partida");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("Total");
                    //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                    CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total_Neg = Libro.Styles.Add("Total_Neg");


                    //estilo para la cabecera
                    Estilo_Cabecera.Font.FontName = "Tahoma";
                    Estilo_Cabecera.Font.Size = 12;
                    Estilo_Cabecera.Font.Bold = true;
                    Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera.Font.Color = "blue";
                    Estilo_Cabecera.Interior.Color = "white";
                    Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera2
                    Estilo_Cabecera2.Font.FontName = "Tahoma";
                    Estilo_Cabecera2.Font.Size = 10;
                    Estilo_Cabecera2.Font.Bold = true;
                    Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera2.Font.Color = "#000000";
                    Estilo_Cabecera2.Interior.Color = "white";
                    Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera3
                    Estilo_Cabecera3.Font.FontName = "Tahoma";
                    Estilo_Cabecera3.Font.Size = 10;
                    Estilo_Cabecera3.Font.Bold = true;
                    Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera3.Font.Color = "#000000";
                    Estilo_Cabecera3.Interior.Color = "white";
                    Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para la cabecera4
                    Estilo_Cabecera4.Font.FontName = "Tahoma";
                    Estilo_Cabecera4.Font.Size = 10;
                    Estilo_Cabecera4.Font.Bold = true;
                    Estilo_Cabecera4.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Cabecera4.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Cabecera4.Font.Color = "#000000";
                    Estilo_Cabecera4.Interior.Color = "lightgray";
                    Estilo_Cabecera4.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera4.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Cabecera4.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el contenido
                    Estilo_Contenido.Font.FontName = "Tahoma";
                    Estilo_Contenido.Font.Size = 9;
                    Estilo_Contenido.Font.Bold = false;
                    Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Contenido.Font.Color = "#000000";
                    Estilo_Contenido.Interior.Color = "White";
                    Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el Concepto
                    Estilo_Concepto_Total.Font.FontName = "Tahoma";
                    Estilo_Concepto_Total.Font.Size = 9;
                    Estilo_Concepto_Total.Font.Bold = false;
                    Estilo_Concepto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    Estilo_Concepto_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Concepto_Total.Font.Color = "#000000";
                    Estilo_Concepto_Total.Interior.Color = "#66FFCC";
                    Estilo_Concepto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Concepto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el Concepto
                    Estilo_Concepto.Font.FontName = "Tahoma";
                    Estilo_Concepto.Font.Size = 9;
                    Estilo_Concepto.Font.Bold = false;
                    Estilo_Concepto.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                    Estilo_Concepto.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                    Estilo_Concepto.Font.Color = "#000000";
                    Estilo_Concepto.Interior.Color = "White";
                    Estilo_Concepto.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Concepto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Concepto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (importe)
                    Estilo_Presupuesto.Font.FontName = "Tahoma";
                    Estilo_Presupuesto.Font.Size = 9;
                    Estilo_Presupuesto.Font.Bold = false;
                    Estilo_Presupuesto.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Presupuesto.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Presupuesto.Font.Color = "#000000";
                    Estilo_Presupuesto.Interior.Color = "White";
                    Estilo_Presupuesto.NumberFormat = "#,###,##0.00";
                    Estilo_Presupuesto.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Presupuesto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (importe)
                    Estilo_Presupuesto_Neg.Font.FontName = "Tahoma";
                    Estilo_Presupuesto_Neg.Font.Size = 9;
                    Estilo_Presupuesto_Neg.Font.Bold = false;
                    Estilo_Presupuesto_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Presupuesto_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                    Estilo_Presupuesto_Neg.Font.Color = "red";
                    Estilo_Presupuesto_Neg.Interior.Color = "White";
                    Estilo_Presupuesto_Neg.NumberFormat = "#,###,##0.00";
                    Estilo_Presupuesto_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (Total del importe)
                    Estilo_Presupuesto_Total.Font.FontName = "Tahoma";
                    Estilo_Presupuesto_Total.Font.Size = 9;
                    Estilo_Presupuesto_Total.Font.Bold = true;
                    Estilo_Presupuesto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Presupuesto_Total.Font.Color = "#000000";
                    Estilo_Presupuesto_Total.Interior.Color = "#66FFCC";
                    Estilo_Presupuesto_Total.NumberFormat = "#,###,##0.00";
                    Estilo_Presupuesto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Presupuesto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (Total del importe)
                    Estilo_Presupuesto_Total_Neg.Font.FontName = "Tahoma";
                    Estilo_Presupuesto_Total_Neg.Font.Size = 9;
                    Estilo_Presupuesto_Total_Neg.Font.Bold = true;
                    Estilo_Presupuesto_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Presupuesto_Total_Neg.Font.Color = "red";
                    Estilo_Presupuesto_Total_Neg.Interior.Color = "#66FFCC";
                    Estilo_Presupuesto_Total_Neg.NumberFormat = "#,###,##0.00";
                    Estilo_Presupuesto_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Presupuesto_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //para partida totales de todos los capitulos
                    Estilo_Partida_Total.Font.FontName = "Tahoma";
                    Estilo_Partida_Total.Font.Size = 9;
                    Estilo_Partida_Total.Font.Bold = true;
                    Estilo_Partida_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Partida_Total.Font.Color = "#000000";
                    Estilo_Partida_Total.Interior.Color = "LightGreen";
                    Estilo_Partida_Total.NumberFormat = "#,###,##0.00";
                    Estilo_Partida_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Partida_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Partida_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Partida_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Partida_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (Total del importe)
                    Estilo_Total.Font.FontName = "Tahoma";
                    Estilo_Total.Font.Size = 9;
                    Estilo_Total.Font.Bold = true;
                    Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Total.Font.Color = "#000000";
                    Estilo_Total.Interior.Color = "#99CCCC";
                    Estilo_Total.NumberFormat = "#,###,##0.00";
                    Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                    //estilo para el presupuesto (Total del importe)
                    Estilo_Total_Neg.Font.FontName = "Tahoma";
                    Estilo_Total_Neg.Font.Size = 9;
                    Estilo_Total_Neg.Font.Bold = true;
                    Estilo_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                    Estilo_Total_Neg.Font.Color = "red";
                    Estilo_Total_Neg.Interior.Color = "#99CCCC";
                    Estilo_Total_Neg.NumberFormat = "#,###,##0.00";
                    Estilo_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                    Estilo_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                    Estilo_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                    #endregion

                    #region Pestaña Partida
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.Worksheet Hoja7 = Libro.Worksheets.Add("PRESUPUESTO MENSUAL");
                    //Agregamos un renglón a la hoja de excel.
                    CarlosAg.ExcelXmlWriter.WorksheetRow Renglon7;
                    //Agregamos las columnas que tendrá la hoja de excel.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(185));//CODIGO PROGRAMATICO
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//fuente de financiamiento 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//AREA FUNCIONAL 
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//Programa .
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//Unidad Responsable.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Partida.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//APROBADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AMPLIACION.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//REDUCCION.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MODIFICADO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ENERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//FEBRERO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MARZO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ABRIL.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//MAYO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JUNIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//JULIO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//AGOSTO.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//SEPTIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//OCTUBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//NOVIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//DICIEMBRE.
                    Hoja7.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//TOTAL.

                        //se llena el encabezado principal
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                        Celda.MergeAcross = 22; // Merge 6 cells together
                        Celda.StyleID = "HeaderStyle";
                        //se llena el encabezado principal
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add(Gpo_Dependencia.Trim());
                        Celda.MergeAcross = 22; // Merge 6 cells together
                        Celda.StyleID = "HeaderStyle2";
                        //se llena el encabezado principal
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add(Dependencia.Trim());
                        Celda.MergeAcross = 22; // Merge 6 cells together
                        Celda.StyleID = "HeaderStyle2";
                        //encabezado3
                        Renglon7 = Hoja7.Table.Rows.Add();//espacio entre el encabezado y el contenido
                        Celda = Renglon7.Cells.Add("PRESUPUESTO DE EGRESOS EJERCICIO FISCAL " + Anio);
                        Celda.MergeAcross = 22; // Merge 6 cells together
                        Celda.StyleID = "HeaderStyle3";
                        
                        //para el codigo programatico
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add("CODIGO PROGRAMATICO");
                        Celda.StyleID = "HeaderStyle4";
                        //para la ff
                        Celda = Renglon7.Cells.Add("CLASIFICADOR POR FUENTE DE FINANCIAMIENTO");
                        Celda.StyleID = "HeaderStyle4";
                        //para la AREA FUNCIONAL
                        Celda = Renglon7.Cells.Add("CLASIFICADOR FUNCIONAL DEL GASTO");
                        Celda.StyleID = "HeaderStyle4";
                        //para la PROGRAMA
                        Celda = Renglon7.Cells.Add("PROYECTO O PROGRAMA");
                        Celda.StyleID = "HeaderStyle4";
                        //para la UNIDAD RESPONSABLE
                        Celda = Renglon7.Cells.Add("CLASIFICADOR ADMINISTRATIVO");
                        Celda.StyleID = "HeaderStyle4";
                        //para la PARTIDA
                        Celda = Renglon7.Cells.Add("PARTIDA ESPECIFICA");
                        Celda.StyleID = "HeaderStyle4";
                        //para el presupuesto
                        Celda = Renglon7.Cells.Add("APROBADO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el AMPLIACION
                        Celda = Renglon7.Cells.Add("AMPLIACION");
                        Celda.StyleID = "HeaderStyle4";
                        //para el REDUCCION
                        Celda = Renglon7.Cells.Add("REDUCCION");
                        Celda.StyleID = "HeaderStyle4";
                        //para el MODIFICADO
                        Celda = Renglon7.Cells.Add("MODIFICADO");
                        Celda.StyleID = "HeaderStyle4";
                        Celda = Renglon7.Cells.Add("ENERO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE FEBRERO
                        Celda = Renglon7.Cells.Add("FEBRERO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE MARZO
                        Celda = Renglon7.Cells.Add("MARZO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE ABRIL
                        Celda = Renglon7.Cells.Add("ABRIL");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE MAYO
                        Celda = Renglon7.Cells.Add("MAYO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE JUNIO
                        Celda = Renglon7.Cells.Add("JUNIO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE JULIO
                        Celda = Renglon7.Cells.Add("JULIO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE AGOSTO
                        Celda = Renglon7.Cells.Add("AGOSTO");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE SEPTIEMBRE
                        Celda = Renglon7.Cells.Add("SEPTIEMBRE");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE OCTUBRE
                        Celda = Renglon7.Cells.Add("OCTUBRE");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE NOVIEMBRE
                        Celda = Renglon7.Cells.Add("NOVIEMBRE");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE DE DICIEMBRE
                        Celda = Renglon7.Cells.Add("DICIEMBRE");
                        Celda.StyleID = "HeaderStyle4";
                        //para el IMPORTE TOTAL
                        Celda = Renglon7.Cells.Add("TOTAL");
                        Celda.StyleID = "HeaderStyle4";

                        if (Dt_Registros != null)
                        {
                            foreach (DataRow Dr in Dt_Registros.Rows)
                            {
                                Renglon7 = Hoja7.Table.Rows.Add();
                                foreach (DataColumn Columna in Dt_Registros.Columns)
                                {
                                    if (Columna is DataColumn)
                                    {
                                        if (Dr["TIPO"].ToString().Equals("CONCEPTO"))
                                        {
                                            if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("FUENTE")
                                                || Columna.ColumnName.Equals("AREA") || Columna.ColumnName.Equals("DEPENDENCIA")
                                                || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                            {
                                                Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Concepto"));
                                            }
                                            else
                                            {
                                                if (Columna.ColumnName != "TIPO")
                                                {
                                                    Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                    if (Cantidad >= 0)
                                                    {
                                                        Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto"));
                                                    }
                                                    else
                                                    {
                                                        Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Presupuesto_Neg"));
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName.Equals("PARTIDA") || Columna.ColumnName.Equals("FUENTE")
                                                || Columna.ColumnName.Equals("AREA") || Columna.ColumnName.Equals("DEPENDENCIA")
                                                || Columna.ColumnName.Equals("PROGRAMA") || Columna.ColumnName.Equals("CODIGO_PROGRAMATICO"))
                                            {
                                                Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Total"));
                                            }
                                            else
                                            {
                                                if (Columna.ColumnName != "TIPO")
                                                {
                                                    Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                                    if (Cantidad >= 0)
                                                    {
                                                        Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total"));
                                                    }
                                                    else
                                                    {
                                                        Renglon7.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Total_Neg"));
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                        //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                        Celda.MergeAcross = 22; // Merge 3 cells together
                        Celda.StyleID = "Concepto";
                        Renglon7 = Hoja7.Table.Rows.Add();
                        Celda = Renglon7.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                        Celda.MergeAcross = 22; // Merge 3 cells together
                        Celda.StyleID = "Concepto";
                    #endregion

                    //Abre el archivo de excel
                    Response.Clear();
                    Response.Buffer = true;
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                    Response.Charset = "UTF-8";
                    Response.ContentEncoding = Encoding.Default;
                    Libro.Save(Response.OutputStream);
                    Response.End();

                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + Ex.Message.ToString() + "]";
                }

            }
        #endregion

        #region(Metodos Excel)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Agrupar_Analitico_Detallado
            ///DESCRIPCIÓN          : metodo para agrupar los datos de las partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public DataTable Agrupar_Analitico_Detallado(DataTable Dt_Psp)
            {
                DataRow Fila = null;
                Decimal Ene = 0;
                Decimal Feb = 0;
                Decimal Mar = 0;
                Decimal Abr = 0;
                Decimal May = 0;
                Decimal Jun = 0;
                Decimal Jul = 0;
                Decimal Ago = 0;
                Decimal Sep = 0;
                Decimal Oct = 0;
                Decimal Nov = 0;
                Decimal Dic = 0;
                Decimal Tot = 0;
                Decimal Apr = 0;
                Decimal Amp = 0;
                Decimal Red = 0;
                Decimal Mod = 0;

                try
                {
                    //INSERTAMOS EL TOTAL DEL PRESUPUESTO
                    //insertamos el total del grupo dependencia
                    Ene = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_ENE"));
                    Feb = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_FEB"));
                    Mar = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_MAR"));
                    Abr = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_ABR"));
                    May = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_MAY"));
                    Jun = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_JUN"));
                    Jul = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_JUL"));
                    Ago = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_AGO"));
                    Sep = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_SEP"));
                    Oct = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_OCT"));
                    Nov = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_NOV"));
                    Dic = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_DIC"));
                    Tot = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("IMPORTE_TOTAL"));
                    Apr = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("APROBADO"));
                    Amp = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("AMPLIACION"));
                    Red = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("REDUCCION"));
                    Mod = Dt_Psp.AsEnumerable().Sum(x => x.Field<Decimal>("MODIFICADO"));

                    //INSERTAMOS LA FILA DEL TOTAL DEL PRESUPUESTO
                    Fila = Dt_Psp.NewRow();
                    Fila["FUENTE"] = String.Empty;
                    Fila["PROGRAMA"] = String.Empty;
                    Fila["AREA"] = String.Empty;
                    Fila["DEPENDENCIA"] = String.Empty;
                    Fila["PARTIDA"] = "TOTAL PRESUPUESTO";
                    Fila["CODIGO_PROGRAMATICO"] = String.Empty;
                    Fila["IMPORTE_ENE"] = Ene;
                    Fila["IMPORTE_FEB"] = Feb;
                    Fila["IMPORTE_MAR"] = Mar;
                    Fila["IMPORTE_ABR"] = Abr;
                    Fila["IMPORTE_MAY"] = May;
                    Fila["IMPORTE_JUN"] = Jun;
                    Fila["IMPORTE_JUL"] = Jul;
                    Fila["IMPORTE_AGO"] = Ago;
                    Fila["IMPORTE_SEP"] = Sep;
                    Fila["IMPORTE_OCT"] = Oct;
                    Fila["IMPORTE_NOV"] = Nov;
                    Fila["IMPORTE_DIC"] = Dic;
                    Fila["IMPORTE_TOTAL"] = Tot;
                    Fila["APROBADO"] = Apr;
                    Fila["AMPLIACION"] = Amp;
                    Fila["REDUCCION"] = Red;
                    Fila["MODIFICADO"] = Mod;
                    Fila["TIPO"] = "TOTAL";
                    Dt_Psp.Rows.Add(Fila);
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Error al generar el reporte. Error[" + ex.Message.ToString() + "]";
                }
                return Dt_Psp;
            }
        #endregion

        #region (Metodos Combos)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
            ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_FF()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Anio = DateTime.Now.Year.ToString().Trim();
                    Negocios.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Gpo_Dependencia_ID = Hf_Gpo_Dependencia.Value.Trim();
                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Fte_Financiamiento = String.Empty;
                    Negocios.P_Tipo_Presupuesto = "EGR";

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    Dt_Datos = Negocios.Consultar_FF();

                    //para destino
                    Cmb_FF.Items.Clear();
                    Cmb_FF.DataSource = Dt_Datos;
                    Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                    Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                    Cmb_FF.DataBind();
                    Cmb_FF.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Area_Funcional
            ///DESCRIPCIÓN          : Llena el combo de areas funcionales
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 26/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Area_Funcional()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Anio = DateTime.Now.Year.ToString().Trim();

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Gpo_Dependencia_ID = Hf_Gpo_Dependencia.Value.Trim();
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    Dt_Datos = Negocios.Consultar_AF();

                    //para destino
                    Cmb_Area_Funcional.Items.Clear();
                    Cmb_Area_Funcional.DataSource = Dt_Datos;
                    Cmb_Area_Funcional.DataValueField = Cat_SAP_Area_Funcional.Campo_Area_Funcional_ID;
                    Cmb_Area_Funcional.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Area_Funcional.DataBind();
                    Cmb_Area_Funcional.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas
            ///DESCRIPCIÓN          : Llena el combo de proyectos programas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Programas()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Anio = DateTime.Now.Year.ToString().Trim();

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_Area_Funcional.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Area_Funcional_ID = String.Empty;
                    }

                    Negocios.P_Dependencia_ID = String.Empty;

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Gpo_Dependencia_ID = Hf_Gpo_Dependencia.Value.Trim();
                    Negocios.P_Tipo_Presupuesto = "EGR";
                    Dt_Datos = Negocios.Consultar_Programa();

                    //para destino
                    Cmb_PP.Items.Clear();
                    Cmb_PP.DataSource = Dt_Datos;
                    Cmb_PP.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                    Cmb_PP.DataTextField = "CLAVE_NOMBRE";
                    Cmb_PP.DataBind();
                    Cmb_PP.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
            ///DESCRIPCIÓN          : Llena el combo de unidad responsable
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_UR()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Anio = DateTime.Now.Year.ToString().Trim();
                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_Area_Funcional.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Area_Funcional_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Programa_ID = String.Empty;
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Gpo_Dependencia_ID = Hf_Gpo_Dependencia.Value.Trim();
                    Negocios.P_Dependencia_ID = String.Empty;

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    Dt_Datos = Negocios.Consultar_UR();

                    //para destino
                    Cmb_UR.Items.Clear();
                    Cmb_UR.DataSource = Dt_Datos;
                    Cmb_UR.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                    Cmb_UR.DataTextField = "CLAVE_NOMBRE";
                    Cmb_UR.DataBind();
                    Cmb_UR.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
            ///DESCRIPCIÓN          : Llena el combo de partidas
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Combo_Partidas()
            {
                Cls_Ope_Psp_Presupuesto_Negocio Negocios = new Cls_Ope_Psp_Presupuesto_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Datos = new DataTable();

                try
                {
                    Negocios.P_Anio = DateTime.Now.Year.ToString().Trim();

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Fte_Financiamiento = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Fte_Financiamiento = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Dependencia_ID = String.Empty;
                    }

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocios.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Programa_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_Area_Funcional.SelectedItem.Value.Trim()))
                    {
                        Negocios.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocios.P_Area_Funcional_ID = String.Empty;
                    }

                    Negocios.P_Busqueda = String.Empty;
                    Negocios.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Gpo_Dependencia_ID = Hf_Tipo_Usuario.Value.Trim();
                    Negocios.P_Capitulo_ID = String.Empty;
                    Negocios.P_Concepto_ID = String.Empty;
                    Negocios.P_Partida_Generica_ID = String.Empty;

                    Dt_Datos = Negocios.Consultar_Partidas();

                    //para destino
                    Cmb_Partida.Items.Clear();
                    Cmb_Partida.DataSource = Dt_Datos;
                    Cmb_Partida.DataValueField = Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                    Cmb_Partida.DataTextField = "CLAVE_NOMBRE";
                    Cmb_Partida.DataBind();
                    Cmb_Partida.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid
            ///DESCRIPCIÓN          : Llena el grid de los registros del reporte
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Febrero/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Llenar_Grid()
            {
                Cls_Rpt_Psp_Reporte_Mensual_Egresos_Negocios Negocio = new Cls_Rpt_Psp_Reporte_Mensual_Egresos_Negocios();
                DataTable Dt_Registros = new DataTable();

                try
                {
                    if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                    {
                        Negocio.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Dependencia_ID = String.Empty;
                    }

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
                    }

                    if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                    {
                        Negocio.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Programa_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                    {
                        Negocio.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Fte_Financiamiento_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_Partida.SelectedItem.Value.Trim()))
                    {
                        Negocio.P_Partida_ID = Cmb_Partida.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Partida_ID = String.Empty;
                    }

                    if (!String.IsNullOrEmpty(Cmb_Area_Funcional.SelectedItem.Value.Trim()))
                    {
                        Negocio.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();
                    }
                    else
                    {
                        Negocio.P_Area_Funcional_ID = String.Empty;
                    }

                    Dt_Registros = Negocio.Consultar_Presupuesto_Mensual();
                    Dt_Registros = Agrupar_Analitico_Detallado(Dt_Registros);

                    Grid_Registros.Columns[0].Visible = true;
                    Grid_Registros.DataSource = Dt_Registros;
                    Grid_Registros.DataBind();
                    Grid_Registros.Columns[0].Visible = false;
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion
    #endregion

    #region(Eventos)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
        ///PARAMETROS: 
        ///CREO:        Hugo Enrique Ramírez Aguilera
            ///FECHA_CREO:  25/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Excel_Click
        ///DESCRIPCIÓN: Manda llamar los datos para cargarlos en el reporte de excel
        ///PARAMETROS:  
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  25/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Rpt_Psp_Reporte_Mensual_Egresos_Negocios Negocio = new Cls_Rpt_Psp_Reporte_Mensual_Egresos_Negocios();
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            String Dependencia = String.Empty;
            String Gpo_Dependencia = String.Empty;
            DataTable Dt = new DataTable();

            try
            {
                if (!String.IsNullOrEmpty(Cmb_UR.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim();
                }
                else 
                {
                    Negocio.P_Dependencia_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_PP.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Programa_ID = Cmb_PP.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Programa_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_FF.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Fte_Financiamiento_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Partida.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Partida_ID = Cmb_Partida.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Partida_ID = String.Empty;
                }

                if (!String.IsNullOrEmpty(Cmb_Area_Funcional.SelectedItem.Value.Trim()))
                {
                    Negocio.P_Area_Funcional_ID = Cmb_Area_Funcional.SelectedItem.Value.Trim();
                }
                else
                {
                    Negocio.P_Area_Funcional_ID = String.Empty;
                }

                Dt = Negocio.Consultar_Presupuesto_Mensual();
                Dt = Agrupar_Analitico_Detallado(Dt);

                Llenar_Grid();

                //obtenemos los datos de la dependencia y del grupo dependencia de la persona que obtendra  el reporte
                Dependencia = Obtener_Gpo_Dependencia("UR");
                Gpo_Dependencia = Obtener_Gpo_Dependencia("GPO_DEP");
                Generar_Reporte_PSP(Dt, Dependencia, Gpo_Dependencia);
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_FF_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de fuente de financiamiento
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_FF_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_Area_Funcional();
                Llenar_Combo_UR();
                Llenar_Combo_Programas();
                Llenar_Combo_Partidas();
                Llenar_Grid();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Area_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de area funcional
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Area_Funcional_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_Programas();
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
                Llenar_Grid();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de area funcional Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_UR_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de dependencias
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_UR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_Partidas();
                Llenar_Grid();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Programas_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo de proyectos programas
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Programas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;

            try
            {
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
                Llenar_Grid();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo de fuente de financiamiento Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
        ///PARAMETROS: 
        ///CREO:        Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO:  07/Noviembre/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Consultar_Click(object sender, EventArgs e)
        {
            Lbl_Mensaje_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";

            try
            {
                Llenar_Grid();
            }
            catch (Exception)
            {
                throw;
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 05/Junio/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            Double Tot = 0.00;
            Double Apr = 0.00;
            Double Amp = 0.00;
            Double Red = 0.00;
            Double Mod = 0.00;

            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Apr = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) ? "0" : e.Row.Cells[3].Text.Trim());
                    Amp = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[4].Text.Trim()) ? "0" : e.Row.Cells[4].Text.Trim());
                    Red = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[5].Text.Trim()) ? "0" : e.Row.Cells[5].Text.Trim());
                    Mod = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[6].Text.Trim()) ? "0" : e.Row.Cells[6].Text.Trim());
                    Ene = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[7].Text.Trim()) ? "0" : e.Row.Cells[7].Text.Trim());
                    Feb = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[8].Text.Trim()) ? "0" : e.Row.Cells[8].Text.Trim());
                    Mar = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[9].Text.Trim()) ? "0" : e.Row.Cells[9].Text.Trim());
                    Abr = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[10].Text.Trim()) ? "0" : e.Row.Cells[10].Text.Trim());
                    May = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[11].Text.Trim()) ? "0" : e.Row.Cells[11].Text.Trim());
                    Jun = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[12].Text.Trim()) ? "0" : e.Row.Cells[12].Text.Trim());
                    Jul = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[13].Text.Trim()) ? "0" : e.Row.Cells[13].Text.Trim());
                    Ago = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[14].Text.Trim()) ? "0" : e.Row.Cells[14].Text.Trim());
                    Sep = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[15].Text.Trim()) ? "0" : e.Row.Cells[15].Text.Trim());
                    Oct = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[16].Text.Trim()) ? "0" : e.Row.Cells[16].Text.Trim());
                    Nov = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[17].Text.Trim()) ? "0" : e.Row.Cells[17].Text.Trim());
                    Dic = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[18].Text.Trim()) ? "0" : e.Row.Cells[18].Text.Trim());
                    Tot = Convert.ToDouble(!String.IsNullOrEmpty(e.Row.Cells[19].Text.Trim()) ? "0" : e.Row.Cells[19].Text.Trim());

                   
                    if (e.Row.Cells[0].Text.Trim().Equals("TOTAL"))
                    {
                        e.Row.Style.Add("background-color", "#FFFF99");
                        e.Row.Style.Add("color", "black");
                    }

                    if (Amp < 0)
                        e.Row.Cells[3].Style.Add("color", "red");
                    if (Apr < 0)
                        e.Row.Cells[4].Style.Add("color", "red");
                    if (Red < 0)
                        e.Row.Cells[5].Style.Add("color", "red");
                    if (Mod < 0)
                        e.Row.Cells[6].Style.Add("color", "red");

                    if (Ene < 0)
                        e.Row.Cells[7].Style.Add("color", "red");
                    if (Feb < 0)
                        e.Row.Cells[8].Style.Add("color", "red");
                    if (Mar < 0)
                        e.Row.Cells[9].Style.Add("color", "red");
                    if (Abr < 0)
                        e.Row.Cells[10].Style.Add("color", "red");
                    if (May < 0)
                        e.Row.Cells[11].Style.Add("color", "red");
                    if (Jun < 0)
                        e.Row.Cells[12].Style.Add("color", "red");
                    if (Jul < 0)
                        e.Row.Cells[13].Style.Add("color", "red");
                    if (Ago < 0)
                        e.Row.Cells[14].Style.Add("color", "red");
                    if (Sep < 0)
                        e.Row.Cells[15].Style.Add("color", "red");
                    if (Oct < 0)
                        e.Row.Cells[16].Style.Add("color", "red");
                    if (Nov < 0)
                        e.Row.Cells[17].Style.Add("color", "red");
                    if (Dic < 0)
                        e.Row.Cells[18].Style.Add("color", "red");
                    if (Tot < 0)
                        e.Row.Cells[19].Style.Add("color", "red");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }
    #endregion
}
