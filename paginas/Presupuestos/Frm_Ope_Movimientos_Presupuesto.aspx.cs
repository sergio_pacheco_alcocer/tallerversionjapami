﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.SqlClient;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Autorizar_Traspaso_Presupuestal.Negocio;
using JAPAMI.Clasificacion_Gasto.Negocio;
using JAPAMI.Limite_Presupuestal.Negocio;
using JAPAMI.Calendarizar_Presupuesto.Negocio;
using JAPAMI.Paramentros_Presupuestos.Negocio;
using JAPAMI.Movimiento_Presupuestal_Ingresos.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using AjaxControlToolkit;
using System.IO;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Ope_Con_Poliza_Ingresos.Datos;
using JAPAMI.Manejo_Presupuesto.Datos;
using System.Net.Mail;

public partial class paginas_Presupuestos_Frm_Ope_Movimientos_Presupuesto : System.Web.UI.Page
{
    #region(Load)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Div_Anexos.Visible = false;
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    Mostrar_Datos_Solicitante();
                    ViewState["SortDirection"] = "ASC";
                    //prueba();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }

        public void prueba()
        {
            DataTable Dt_Psp_Ing = new DataTable();
            DataRow Fila;

            Dt_Psp_Ing.Columns.Add("Fte_Financiamiento_ID", typeof(System.String));
            Dt_Psp_Ing.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
            Dt_Psp_Ing.Columns.Add(Cat_Psp_Concepto_Ing.Campo_Concepto_Ing_ID, typeof(System.String));
            Dt_Psp_Ing.Columns.Add(Cat_Psp_SubConcepto_Ing.Campo_SubConcepto_Ing_ID, typeof(System.String));
            Dt_Psp_Ing.Columns.Add("Anio", typeof(System.String));
            Dt_Psp_Ing.Columns.Add("Importe", typeof(System.String));

            Fila = Dt_Psp_Ing.NewRow();
            Fila[0] = "00001";
            Fila[1] = "";
            Fila[2] = "0000000001";
            Fila[3] = "0000000001";
            Fila[4] = "2013";
            Fila[5] = "1000";
            Dt_Psp_Ing.Rows.Add(Fila);

            //Fila = Dt_Psp_Ing.NewRow();
            //Fila[0] = "00001";
            //Fila[1] = "";
            //Fila[2] = "0000000001";
            //Fila[3] = "0000000002";
            //Fila[4] = "2013";
            //Fila[5] = "20";
            //Dt_Psp_Ing.Rows.Add(Fila);

            Cls_Ope_Con_Poliza_Ingresos_Datos.Alta_Movimientos_Presupuestales(Dt_Psp_Ing, null, "prueba", "0000000001", "00001", "0112",
                Ope_Psp_Presupuesto_Ingresos.Campo_Por_Recaudar , Ope_Psp_Presupuesto_Ingresos.Campo_Recaudado);
        }
    #endregion

    #region(Metodos)
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles("Todo"); //Limpia los controles del forma
            Habilitar_Controles("Inicial");//Inicializa todos los controles
            Cargar_Combo_Responsable();
            Consulta_Movimiento();
            Div_Grid_Movimientos.Visible = true;
            Div_Datos.Style.Add("display", "none");
            Div_Anexos.Style.Add("display", "none");
            Llenar_Combo_Mes();
            Llenar_Combo_Operacion();
            Cmb_Operacion.SelectedIndex = 0;
            Div2.Visible = true;
            Div_Partidas.Visible = true;
            Tabla_Encabezado_Destino.Visible = true;
            Tabla_Encabezado_Origen.Visible = true;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = String.Empty;
        }
        catch (Exception ex)
        {
            throw new Exception("Inicializa_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          : Leslie González Vázquez
    /// FECHA_MODIFICO    : 12/mayo/2012 
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles(String Accion)
    {
        try
        {
            switch (Accion)
            {
                case "Todo":
                    Txt_Busqueda.Text = "";
                    Txt_Fecha_Inicial.Text = String.Empty;
                    Txt_Fecha_Final.Text = String.Empty;
                    Txt_Importe.Text = "";
                    Cmb_Estatus.SelectedIndex = 0;
                    Txt_Justificacion.Text = "";
                    Cmb_Fuente_Financiamiento_Origen.Items.Clear();
                    Cmb_Capitulo_Origen.SelectedIndex = -1;
                    Cmb_Programa_Origen.SelectedIndex = -1;
                    Txt_Total_Mov_Origen.Text = String.Empty;
                    Cmb_Fuente_Financiamiento_Destino.Items.Clear();
                    Cmb_Capitulo_Destino.SelectedIndex = -1;
                    Cmb_Programa_Destino.SelectedIndex = -1;
                    Txt_Total_Mov_Origen.Text = String.Empty;
                    Txt_Comentario.Text = String.Empty;
                    Txt_Autorizo.Text = String.Empty;
                    Txt_Fecha_Autorizo.Text = String.Empty;
                    Lbl_Mensaje_Error.Text = String.Empty;
                    Hf_Ampliacion_Origen.Value = String.Empty;
                    Hf_Aprobado_Origen.Value = String.Empty;
                    Hf_Capitulo_Origen_ID.Value = String.Empty;
                    Hf_Disponible.Value = "0.00";
                    Hf_Modificado_Origen.Value = String.Empty;
                    Hf_Partida_Origen_ID.Value = String.Empty;
                    Hf_Programa_Origen_ID.Value = String.Empty;
                    Hf_Partida_Origen.Value = String.Empty;
                    Hf_Programa_Origen.Value = String.Empty;
                    Hf_Reduccion_Origen.Value = String.Empty;
                    Lbl_Disp_Ene.Text = "0.00";
                    Lbl_Disp_Feb.Text = "0.00";
                    Lbl_Disp_Mar.Text = "0.00";
                    Lbl_Disp_Abr.Text = "0.00";
                    Lbl_Disp_May.Text = "0.00";
                    Lbl_Disp_Jun.Text = "0.00";
                    Lbl_Disp_Jul.Text = "0.00";
                    Lbl_Disp_Ago.Text = "0.00";
                    Lbl_Disp_Sep.Text = "0.00";
                    Lbl_Disp_Oct.Text = "0.00";
                    Lbl_Disp_Nov.Text = "0.00";
                    Lbl_Disp_Dic.Text = "0.00";
                    Txt_Enero.Text = "0.00";
                    Txt_Febrero.Text = "0.00";
                    Txt_Marzo.Text = "0.00";
                    Txt_Abril.Text = "0.00";
                    Txt_Mayo.Text = "0.00";
                    Txt_Junio.Text = "0.00";
                    Txt_Julio.Text = "0.00";
                    Txt_Agosto.Text = "0.00";
                    Txt_Septiembre.Text = "0.00";
                    Txt_Octubre.Text = "0.00";
                    Txt_Noviembre.Text = "0.00";
                    Txt_Diciembre.Text = "0.00";
                    Hf_Ampliacion_Destino.Value = String.Empty;
                    Hf_Aprobado_Destino.Value = String.Empty;
                    Hf_Capitulo_Destino_ID.Value = String.Empty;
                    Hf_Disponible.Value = "0.00";
                    Hf_Modificado_Destino.Value = String.Empty;
                    Hf_Partida_Destino_ID.Value = String.Empty;
                    Hf_Programa_Destino_ID.Value = String.Empty;
                    Hf_Partida_Destino.Value = String.Empty;
                    Hf_Programa_Destino.Value = String.Empty;
                    Hf_Reduccion_Destino.Value = String.Empty;
                    Lbl_Disp_Ene_Destino.Text = "0.00";
                    Lbl_Disp_Feb_Destino.Text = "0.00";
                    Lbl_Disp_Mar_Destino.Text = "0.00";
                    Lbl_Disp_Abr_Destino.Text = "0.00";
                    Lbl_Disp_May_Destino.Text = "0.00";
                    Lbl_Disp_Jun_Destino.Text = "0.00";
                    Lbl_Disp_Jul_Destino.Text = "0.00";
                    Lbl_Disp_Ago_Destino.Text = "0.00";
                    Lbl_Disp_Sep_Destino.Text = "0.00";
                    Lbl_Disp_Oct_Destino.Text = "0.00";
                    Lbl_Disp_Nov_Destino.Text = "0.00";
                    Lbl_Disp_Dic_Destino.Text = "0.00";
                    Txt_Enero_Destino.Text = "0.00";
                    Txt_Febrero_Destino.Text = "0.00";
                    Txt_Marzo_Destino.Text = "0.00";
                    Txt_Abril_Destino.Text = "0.00";
                    Txt_Mayo_Destino.Text = "0.00";
                    Txt_Junio_Destino.Text = "0.00";
                    Txt_Julio_Destino.Text = "0.00";
                    Txt_Agosto_Destino.Text = "0.00";
                    Txt_Septiembre_Destino.Text = "0.00";
                    Txt_Octubre_Destino.Text = "0.00";
                    Txt_Noviembre_Destino.Text = "0.00";
                    Txt_Diciembre_Destino.Text = "0.00";
                    Hf_Capitulo_Origen_ID.Value = String.Empty;
                    Hf_Capitulo_Destino_ID.Value = String.Empty;
                    Txt_Busqueda_UR.Text = String.Empty;
                    Txt_Busqueda_URO.Text = String.Empty;
                    Txt_Busqueda_URD.Text = String.Empty;
                    Txt_Total_Mov_Destino.Text = String.Empty;
                    Txt_Total_Des.Text = String.Empty;
                    Lbl_Nombre_Partida_Origen.Text = String.Empty;
                    Lbl_Nombre_Partida_Destino.Text = String.Empty;
                    Txt_Restante_Destino.Text = String.Empty;
                    break;
                case "Error":
                    Lbl_Mensaje_Error.Text = String.Empty;
                    break;
                case "Mov_Origen":
                    Lbl_Nombre_Partida_Origen.Text = String.Empty;
                    Hf_Ampliacion_Origen.Value = String.Empty;
                    Hf_Aprobado_Origen.Value = String.Empty;
                    Hf_Capitulo_Origen_ID.Value = String.Empty;
                    Hf_Disponible.Value = "0.00";
                    Hf_Modificado_Origen.Value = String.Empty;
                    Hf_Partida_Origen_ID.Value = String.Empty;
                    Hf_Programa_Origen_ID.Value = String.Empty;
                    Hf_Partida_Origen.Value = String.Empty;
                    Hf_Programa_Origen.Value = String.Empty;
                    Hf_Reduccion_Origen.Value = String.Empty;
                    Hf_Capitulo_Origen_ID.Value = String.Empty;
                    Lbl_Disp_Ene.Text = "0.00";
                    Lbl_Disp_Feb.Text = "0.00";
                    Lbl_Disp_Mar.Text = "0.00";
                    Lbl_Disp_Abr.Text = "0.00";
                    Lbl_Disp_May.Text = "0.00";
                    Lbl_Disp_Jun.Text = "0.00";
                    Lbl_Disp_Jul.Text = "0.00";
                    Lbl_Disp_Ago.Text = "0.00";
                    Lbl_Disp_Sep.Text = "0.00";
                    Lbl_Disp_Oct.Text = "0.00";
                    Lbl_Disp_Nov.Text = "0.00";
                    Lbl_Disp_Dic.Text = "0.00";
                    Txt_Enero.Text = "0.00";
                    Txt_Febrero.Text = "0.00";
                    Txt_Marzo.Text = "0.00";
                    Txt_Abril.Text = "0.00";
                    Txt_Mayo.Text = "0.00";
                    Txt_Junio.Text = "0.00";
                    Txt_Julio.Text = "0.00";
                    Txt_Agosto.Text = "0.00";
                    Txt_Septiembre.Text = "0.00";
                    Txt_Octubre.Text = "0.00";
                    Txt_Noviembre.Text = "0.00";
                    Txt_Diciembre.Text = "0.00";
                    Txt_Busqueda_URO.Text = String.Empty;
                    break;
                case "Mov_Destino":
                    Hf_Ampliacion_Destino.Value = String.Empty;
                    Hf_Aprobado_Destino.Value = String.Empty;
                    Hf_Capitulo_Destino_ID.Value = String.Empty;
                    Hf_Disponible.Value = "0.00";
                    Hf_Modificado_Destino.Value = String.Empty;
                    Hf_Partida_Destino_ID.Value = String.Empty;
                    Hf_Programa_Destino_ID.Value = String.Empty;
                    Hf_Partida_Destino.Value = String.Empty;
                    Hf_Programa_Destino.Value = String.Empty;
                    Hf_Reduccion_Destino.Value = String.Empty;
                    Hf_Capitulo_Destino_ID.Value = String.Empty;
                    Lbl_Disp_Ene_Destino.Text = "0.00";
                    Lbl_Disp_Feb_Destino.Text = "0.00";
                    Lbl_Disp_Mar_Destino.Text = "0.00";
                    Lbl_Disp_Abr_Destino.Text = "0.00";
                    Lbl_Disp_May_Destino.Text = "0.00";
                    Lbl_Disp_Jun_Destino.Text = "0.00";
                    Lbl_Disp_Jul_Destino.Text = "0.00";
                    Lbl_Disp_Ago_Destino.Text = "0.00";
                    Lbl_Disp_Sep_Destino.Text = "0.00";
                    Lbl_Disp_Oct_Destino.Text = "0.00";
                    Lbl_Disp_Nov_Destino.Text = "0.00";
                    Lbl_Disp_Dic_Destino.Text = "0.00";
                    Txt_Enero_Destino.Text = "0.00";
                    Txt_Febrero_Destino.Text = "0.00";
                    Txt_Marzo_Destino.Text = "0.00";
                    Txt_Abril_Destino.Text = "0.00";
                    Txt_Mayo_Destino.Text = "0.00";
                    Txt_Junio_Destino.Text = "0.00";
                    Txt_Julio_Destino.Text = "0.00";
                    Txt_Agosto_Destino.Text = "0.00";
                    Txt_Septiembre_Destino.Text = "0.00";
                    Txt_Octubre_Destino.Text = "0.00";
                    Txt_Noviembre_Destino.Text = "0.00";
                    Txt_Diciembre_Destino.Text = "0.00";
                    Txt_Busqueda_URD.Text = String.Empty;
                    Lbl_Nombre_Partida_Destino.Text = String.Empty;
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : 1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///               si es una alta, modificacion
    ///                           
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";

                    //Configuracion_Acceso("Frm_Ope_Movimientos_Presupuesto.aspx");
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    break;

                case "Modificar":
                    //Boton Nuevo
                    Btn_Nuevo.Visible = false;
                    //Boton Modificar
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.Enabled = true;
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    break;

            }
            Grid_Partidas.Visible = true;
            //para datos generales
            Cmb_Operacion.Enabled = Habilitado;
            Txt_Importe.Enabled = false;
            Cmb_Estatus.Enabled = Habilitado;
            Txt_Justificacion.Enabled = Habilitado;
            //partida Origen
            Cmb_Fuente_Financiamiento_Origen.Enabled = Habilitado;
            Txt_Total_Mov_Origen.Enabled = false;
            //partida Destino
            Cmb_Fuente_Financiamiento_Destino.Enabled = Habilitado;
            Txt_Total_Mov_Destino.Enabled = false;
            //mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Consultar_Partida_Especifica
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 30/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Mes()
    {
        int Mes = DateTime.Today.Month;
        Hf_Mes_Actual.Value = "";
        try
        {
            Txt_Enero.Enabled = true;
            Txt_Febrero.Enabled = true;
            Txt_Marzo.Enabled = true;
            Txt_Abril.Enabled = true;
            Txt_Mayo.Enabled = true;
            Txt_Junio.Enabled = true;
            Txt_Julio.Enabled = true;
            Txt_Agosto.Enabled = true;
            Txt_Septiembre.Enabled = true;
            Txt_Octubre.Enabled = true;
            Txt_Noviembre.Enabled = true;
            Txt_Diciembre.Enabled = true;
            Txt_Enero_Destino.Enabled = true;
            Txt_Febrero_Destino.Enabled = true;
            Txt_Marzo_Destino.Enabled = true;
            Txt_Abril_Destino.Enabled = true;
            Txt_Mayo_Destino.Enabled = true;
            Txt_Junio_Destino.Enabled = true;
            Txt_Julio_Destino.Enabled = true;
            Txt_Agosto_Destino.Enabled = true;
            Txt_Septiembre_Destino.Enabled = true;
            Txt_Octubre_Destino.Enabled = true;
            Txt_Noviembre_Destino.Enabled = true;
            Txt_Diciembre_Destino.Enabled = true;
            switch (Mes)
            {
                case 1:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = true;
                        Txt_Febrero.Enabled = true;
                        Txt_Marzo.Enabled = true;
                        Txt_Abril.Enabled = true;
                        Txt_Mayo.Enabled = true;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = true;
                        Txt_Febrero_Destino.Enabled = true;
                        Txt_Marzo_Destino.Enabled = true;
                        Txt_Abril_Destino.Enabled = true;
                        Txt_Mayo_Destino.Enabled = true;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "ENERO";
                    break;
                case 2:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = true;
                        Txt_Marzo.Enabled = true;
                        Txt_Abril.Enabled = true;
                        Txt_Mayo.Enabled = true;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = true;
                        Txt_Marzo_Destino.Enabled = true;
                        Txt_Abril_Destino.Enabled = true;
                        Txt_Mayo_Destino.Enabled = true;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "FEBRERO";
                    break;
                case 3:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = true;
                        Txt_Abril.Enabled = true;
                        Txt_Mayo.Enabled = true;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = true;
                        Txt_Abril_Destino.Enabled = true;
                        Txt_Mayo_Destino.Enabled = true;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "MARZO";
                    break;
                case 4:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = true;
                        Txt_Mayo.Enabled = true;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = true;
                        Txt_Mayo_Destino.Enabled = true;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "ABRIL";
                    break;
                case 5:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = true;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = true;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "MAYO";
                    break;
                case 6:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = true;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = true;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "JUNIO";
                    break;
                case 7:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = true;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = true;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "JULIO";
                    break;
                case 8:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = false;
                        Txt_Agosto.Enabled = true;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = false;
                        Txt_Agosto_Destino.Enabled = true;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "AGOSTO";
                    break;
                case 9:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = false;
                        Txt_Agosto.Enabled = false;
                        Txt_Septiembre.Enabled = true;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = false;
                        Txt_Agosto_Destino.Enabled = false;
                        Txt_Septiembre_Destino.Enabled = true;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "SEPTIEMBRE";
                    break;
                case 10:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = false;
                        Txt_Agosto.Enabled = false;
                        Txt_Septiembre.Enabled = false;
                        Txt_Octubre.Enabled = true;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = false;
                        Txt_Agosto_Destino.Enabled = false;
                        Txt_Septiembre_Destino.Enabled = false;
                        Txt_Octubre_Destino.Enabled = true;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "OCTUBRE";
                    break;
                case 11:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = false;
                        Txt_Agosto.Enabled = false;
                        Txt_Septiembre.Enabled = false;
                        Txt_Octubre.Enabled = false;
                        Txt_Noviembre.Enabled = true;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = false;
                        Txt_Agosto_Destino.Enabled = false;
                        Txt_Septiembre_Destino.Enabled = false;
                        Txt_Octubre_Destino.Enabled = false;
                        Txt_Noviembre_Destino.Enabled = true;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "NOVIEMBRE";
                    break;
                case 12:
                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Usuario"))
                    {
                        Txt_Enero.Enabled = false;
                        Txt_Febrero.Enabled = false;
                        Txt_Marzo.Enabled = false;
                        Txt_Abril.Enabled = false;
                        Txt_Mayo.Enabled = false;
                        Txt_Junio.Enabled = false;
                        Txt_Julio.Enabled = false;
                        Txt_Agosto.Enabled = false;
                        Txt_Septiembre.Enabled = false;
                        Txt_Octubre.Enabled = false;
                        Txt_Noviembre.Enabled = false;
                        Txt_Diciembre.Enabled = true;

                        Txt_Enero_Destino.Enabled = false;
                        Txt_Febrero_Destino.Enabled = false;
                        Txt_Marzo_Destino.Enabled = false;
                        Txt_Abril_Destino.Enabled = false;
                        Txt_Mayo_Destino.Enabled = false;
                        Txt_Junio_Destino.Enabled = false;
                        Txt_Julio_Destino.Enabled = false;
                        Txt_Agosto_Destino.Enabled = false;
                        Txt_Septiembre_Destino.Enabled = false;
                        Txt_Octubre_Destino.Enabled = false;
                        Txt_Noviembre_Destino.Enabled = false;
                        Txt_Diciembre_Destino.Enabled = true;
                    }
                    Hf_Mes_Actual.Value = "DICIEMBRE";
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el combo de meses. Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
    ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Sessiones()
    {
        Session["Dt_Mov"] = null;
        Session["Dt_Mov"] = new DataTable();
        Grid_Mov_Origen.DataSource = new DataTable();
        Grid_Mov_Origen.DataBind();
        Grid_Mov_Destino.DataSource = new DataTable();
        Grid_Mov_Destino.DataBind();

        Session["Dt_Anexos"] = null;
        Session["Dt_Anexos"] = new DataTable();
        Grid_Anexos.DataSource = new DataTable();
        Grid_Anexos.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_grid_Partidas
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 05/marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_grid_Partidas()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio_Inv.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Negocio_Inv.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();
            Negocio_Inv.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();

            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();

            if (!String.IsNullOrEmpty(Hf_Partida_Origen.Value.Trim()))
            {
                Negocio.P_Partida_Especifica_ID = Hf_Partida_Origen.Value.Trim().Replace("-", "");
                Negocio_Inv.P_Partida_Especifica_ID = Hf_Partida_Origen.Value.Trim().Replace("-", "");
            }
            else
            {
                Negocio.P_Partida_Especifica_ID = String.Empty;
                Negocio_Inv.P_Partida_Especifica_ID = String.Empty;
            }

            if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex > 0)
            {
                Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                Negocio_Inv.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fuente_Financiamiento_ID = String.Empty;
                Negocio_Inv.P_Fuente_Financiamiento_ID = String.Empty;
            }

            if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Dt_Partidas = Negocio_Inv.Consultar_Partidas_Especificas();
            }
            else
            {
                Dt_Partidas = Negocio.Consultar_Partidas_Especificas();
            }

            if (Dt_Partidas is DataTable)
            {
                Grid_Partidas.DataBind();

                Grid_Partidas.Columns[0].Visible = true;
                Grid_Partidas.Columns[5].Visible = true;
                Grid_Partidas.Columns[6].Visible = true;
                Grid_Partidas.Columns[7].Visible = true;
                Grid_Partidas.Columns[8].Visible = true;
                Grid_Partidas.Columns[9].Visible = true;
                Grid_Partidas.Columns[10].Visible = true;
                Grid_Partidas.Columns[11].Visible = true;
                Grid_Partidas.Columns[12].Visible = true;
                Grid_Partidas.Columns[13].Visible = true;
                Grid_Partidas.Columns[14].Visible = true;
                Grid_Partidas.Columns[15].Visible = true;
                Grid_Partidas.Columns[16].Visible = true;
                Grid_Partidas.Columns[17].Visible = true;
                Grid_Partidas.Columns[18].Visible = true;
                Grid_Partidas.Columns[19].Visible = true;
                Grid_Partidas.Columns[20].Visible = true;
                Grid_Partidas.Columns[21].Visible = true;
                Grid_Partidas.Columns[22].Visible = true;
                Grid_Partidas.Columns[23].Visible = true;
                Grid_Partidas.Columns[24].Visible = true;
                Grid_Partidas.Columns[25].Visible = true;
                Grid_Partidas.DataSource = Dt_Partidas;
                Grid_Partidas.DataBind();
                Grid_Partidas.Columns[5].Visible = false;
                Grid_Partidas.Columns[6].Visible = false;
                Grid_Partidas.Columns[7].Visible = false;
                Grid_Partidas.Columns[8].Visible = false;
                Grid_Partidas.Columns[9].Visible = false;
                Grid_Partidas.Columns[10].Visible = false;
                Grid_Partidas.Columns[11].Visible = false;
                Grid_Partidas.Columns[12].Visible = false;
                Grid_Partidas.Columns[13].Visible = false;
                Grid_Partidas.Columns[14].Visible = false;
                Grid_Partidas.Columns[15].Visible = false;
                Grid_Partidas.Columns[16].Visible = false;
                Grid_Partidas.Columns[17].Visible = false;
                Grid_Partidas.Columns[18].Visible = false;
                Grid_Partidas.Columns[19].Visible = false;
                Grid_Partidas.Columns[20].Visible = false;
                Grid_Partidas.Columns[21].Visible = false;
                Grid_Partidas.Columns[22].Visible = false;
                Grid_Partidas.Columns[23].Visible = false;
                Grid_Partidas.Columns[24].Visible = false;
                Grid_Partidas.Columns[25].Visible = false;
                Div_Partidas.Visible = true;
            }
            Grid_Partidas.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el grid de las partidas. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Operacion
    ///DESCRIPCIÓN          : Metodo para llenar el combo de la operacion
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Operacion()
    {
        try
        {
            Cmb_Operacion.Items.Clear();
            Cmb_Operacion.Items.Insert(0, new ListItem("TRASPASO", "TRASPASO"));
            if (Hf_Tipo_Usuario.Value.Trim()!= "Usuario")
            {
                Cmb_Operacion.Items.Insert(1, new ListItem("AMPLIACION", "AMPLIACION"));
                Cmb_Operacion.Items.Insert(2, new ListItem("REDUCCION", "REDUCCION"));
                Cmb_Operacion.Items.Insert(3, new ListItem("CREACION", "SUPLEMENTO"));
            }
            Cmb_Operacion.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el combo de la operación. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_grid_Partidas_Destino
    ///DESCRIPCIÓN          : consulta para obtener los datos de las partidas especificas
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 08/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_grid_Partidas_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio_Inv.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Negocio_Inv.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Destino_ID.Value.Trim();
            Negocio_Inv.P_Area_Funcional_ID = Hf_Area_Funcional_Destino_ID.Value.Trim();

            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();

            if (!String.IsNullOrEmpty(Hf_Partida_Destino.Value.Trim()))
            {
                Negocio.P_Partida_Especifica_ID = Hf_Partida_Destino.Value.Trim().Replace("-", "");
                Negocio_Inv.P_Partida_Especifica_ID = Hf_Partida_Destino.Value.Trim().Replace("-", "");
            }
            else
            {
                Negocio.P_Partida_Especifica_ID = String.Empty;
                Negocio_Inv.P_Partida_Especifica_ID = String.Empty;
            }

            if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex > 0)
            {
                Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
                Negocio_Inv.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
            }
            else
            {
                Negocio.P_Fuente_Financiamiento_ID = String.Empty;
                Negocio_Inv.P_Fuente_Financiamiento_ID = String.Empty;
            }

            if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Dt_Partidas = Negocio_Inv.Consultar_Partidas_Especificas();
            }
            else
            {
                Dt_Partidas = Negocio.Consultar_Partidas_Especificas();
            }


            if (Dt_Partidas is DataTable)
            {
                Grid_Partidas_Destino.DataBind();

                Grid_Partidas_Destino.Columns[0].Visible = true;
                Grid_Partidas_Destino.Columns[5].Visible = true;
                Grid_Partidas_Destino.Columns[6].Visible = true;
                Grid_Partidas_Destino.Columns[7].Visible = true;
                Grid_Partidas_Destino.Columns[8].Visible = true;
                Grid_Partidas_Destino.Columns[9].Visible = true;
                Grid_Partidas_Destino.Columns[10].Visible = true;
                Grid_Partidas_Destino.Columns[11].Visible = true;
                Grid_Partidas_Destino.Columns[12].Visible = true;
                Grid_Partidas_Destino.Columns[13].Visible = true;
                Grid_Partidas_Destino.Columns[14].Visible = true;
                Grid_Partidas_Destino.Columns[15].Visible = true;
                Grid_Partidas_Destino.Columns[16].Visible = true;
                Grid_Partidas_Destino.Columns[17].Visible = true;
                Grid_Partidas_Destino.Columns[18].Visible = true;
                Grid_Partidas_Destino.Columns[19].Visible = true;
                Grid_Partidas_Destino.Columns[20].Visible = true;
                Grid_Partidas_Destino.Columns[21].Visible = true;
                Grid_Partidas_Destino.Columns[22].Visible = true;
                Grid_Partidas_Destino.Columns[23].Visible = true;
                Grid_Partidas_Destino.Columns[24].Visible = true;
                Grid_Partidas_Destino.Columns[25].Visible = true;
                Grid_Partidas_Destino.DataSource = Dt_Partidas;
                Grid_Partidas_Destino.DataBind();
                Grid_Partidas_Destino.Columns[5].Visible = false;
                Grid_Partidas_Destino.Columns[6].Visible = false;
                Grid_Partidas_Destino.Columns[7].Visible = false;
                Grid_Partidas_Destino.Columns[8].Visible = false;
                Grid_Partidas_Destino.Columns[9].Visible = false;
                Grid_Partidas_Destino.Columns[10].Visible = false;
                Grid_Partidas_Destino.Columns[11].Visible = false;
                Grid_Partidas_Destino.Columns[12].Visible = false;
                Grid_Partidas_Destino.Columns[13].Visible = false;
                Grid_Partidas_Destino.Columns[14].Visible = false;
                Grid_Partidas_Destino.Columns[15].Visible = false;
                Grid_Partidas_Destino.Columns[16].Visible = false;
                Grid_Partidas_Destino.Columns[17].Visible = false;
                Grid_Partidas_Destino.Columns[18].Visible = false;
                Grid_Partidas_Destino.Columns[19].Visible = false;
                Grid_Partidas_Destino.Columns[20].Visible = false;
                Grid_Partidas_Destino.Columns[21].Visible = false;
                Grid_Partidas_Destino.Columns[22].Visible = false;
                Grid_Partidas_Destino.Columns[23].Visible = false;
                Grid_Partidas_Destino.Columns[24].Visible = false;
                Grid_Partidas_Destino.Columns[25].Visible = false;
                Div_Partidas.Visible = true;
            }
            Grid_Partidas_Destino.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el grid de las partidas. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Sumar_Total_Origen
    ///DESCRIPCIÓN          : Metodo para sumar el total de la partida origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 09/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Sumar_Total_Origen()
    {
        Double Total = 0.00;

        try
        {
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim());

            Txt_Total.Text = String.Format("{0:#,###,##0.00}", Total);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al sumar el total de la partida origen. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Sumar_Total_Destino
    ///DESCRIPCIÓN          : Metodo para sumar el total de la partida destino
    ///PARAMETROS           : 
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 09/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Sumar_Total_Destino()
    {
        Double Total = 0.00;

        try
        {
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero_Destino.Text.Trim()) ? "0" : Txt_Enero_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero_Destino.Text.Trim()) ? "0" : Txt_Febrero_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo_Destino.Text.Trim()) ? "0" : Txt_Marzo_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril_Destino.Text.Trim()) ? "0" : Txt_Abril_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo_Destino.Text.Trim()) ? "0" : Txt_Mayo_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio_Destino.Text.Trim()) ? "0" : Txt_Junio_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio_Destino.Text.Trim()) ? "0" : Txt_Julio_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto_Destino.Text.Trim()) ? "0" : Txt_Agosto_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre_Destino.Text.Trim()) ? "0" : Txt_Septiembre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre_Destino.Text.Trim()) ? "0" : Txt_Octubre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre_Destino.Text.Trim()) ? "0" : Txt_Noviembre_Destino.Text.Trim());
            Total += Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre_Destino.Text.Trim()) ? "0" : Txt_Diciembre_Destino.Text.Trim());

            Txt_Total_Des.Text = String.Format("{0:#,###,##0.00}", Total);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al sumar el total de la partida  destino. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov
    ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Mov()
    {
        DataTable Dt_Mov = new DataTable();
        try
        {
            Dt_Mov.Columns.Add("CLAVE_NOM_PARTIDA", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("CLAVE_NOM_PROGRAMA", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("APROBADO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("AMPLIACION", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("REDUCCION", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("MODIFICADO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("DISPONIBLE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_ENERO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_FEBRERO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_MARZO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_ABRIL", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_MAYO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_JUNIO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_JULIO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_AGOSTO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_SEPTIEMBRE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_OCTUBRE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_NOVIEMBRE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_DICIEMBRE", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("IMPORTE_TOTAL", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("TIPO_OPERACION", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("TIPO_PARTIDA", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("TIPO_EGRESO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("TIPO_MOVIMIENTO", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("PARTIDA_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("PROGRAMA_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("FF_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("AF_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("UR_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("MOVIMIENTO_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("CAPITULO_ID", System.Type.GetType("System.String"));
            Dt_Mov.Columns.Add("JUSTIFICACION", System.Type.GetType("System.String"));

            Session["Dt_Mov"] = (DataTable)Dt_Mov;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Mov
    ///DESCRIPCIÓN          : Metodo para agregar los datos de los movimientos al datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Agregar_Fila_Dt_Mov(String Tipo_Partida)
    {
        DataTable Dt_Mov = new DataTable();
        DataRow Fila;
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        Double Importe = 0.00;
        Double Ampliacion = 0.00;
        Double Reduccion = 0.00;
        Double Modificado = 0.00;
        Double Aprobado = 0.00;

        try
        {
            if (Tipo_Partida.Trim().Equals("Origen"))
            {
                Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Hf_Aprobado_Origen.Value.Trim()) ? "0" : Hf_Aprobado_Origen.Value.Trim());
                Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Ampliacion_Origen.Value.Trim()) ? "0" : Hf_Ampliacion_Origen.Value.Trim());
                Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Reduccion_Origen.Value.Trim()) ? "0" : Hf_Reduccion_Origen.Value.Trim());
                Modificado = Convert.ToDouble(String.IsNullOrEmpty(Hf_Modificado_Origen.Value.Trim()) ? "0" : Hf_Modificado_Origen.Value.Trim());
                Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim());

                Aprobado = Aprobado + Ampliacion - Reduccion;
                Ampliacion = 0.00;
                Reduccion = 0.00;
            }
            else
            {
                Aprobado = Convert.ToDouble(String.IsNullOrEmpty(Hf_Aprobado_Destino.Value.Trim()) ? "0" : Hf_Aprobado_Destino.Value.Trim());
                Ampliacion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Ampliacion_Destino.Value.Trim()) ? "0" : Hf_Ampliacion_Destino.Value.Trim());
                Reduccion = Convert.ToDouble(String.IsNullOrEmpty(Hf_Reduccion_Destino.Value.Trim()) ? "0" : Hf_Reduccion_Destino.Value.Trim());
                Modificado = Convert.ToDouble(String.IsNullOrEmpty(Hf_Modificado_Destino.Value.Trim()) ? "0" : Hf_Modificado_Destino.Value.Trim());
                Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Des.Text.Trim()) ? "0" : Txt_Total_Des.Text.Trim());

                Aprobado = Aprobado + Ampliacion - Reduccion;
                Ampliacion = 0.00;
                Reduccion = 0.00;
            }

            if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("AMPLIACION"))
            {
                Ampliacion = Importe;
            }
            else if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("REDUCCION"))
            {
                Reduccion = Importe;
            }
            else if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("TRASPASO"))
            {
                if (Tipo_Partida.Trim().Equals("Origen"))
                {
                    Reduccion = Importe;
                }
                else
                {
                    Ampliacion = Importe;
                }
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
            {
                Ampliacion = Importe;
                Hf_Programa_Origen_ID.Value = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                Hf_Programa_Origen.Value = Cmb_Programa_Origen.SelectedItem.Text.Trim();
            }

            Modificado = Aprobado + Ampliacion - Reduccion;

            if (Tipo_Partida.Trim().Equals("Origen"))
            {
                Fila = Dt_Mov.NewRow();
                Fila["CLAVE_NOM_PARTIDA"] = Hf_Partida_Origen.Value.Trim();
                Fila["CLAVE_NOM_PROGRAMA"] = Hf_Programa_Origen.Value.Trim();
                Fila["IMPORTE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim()));
                Fila["APROBADO"] = String.Format("{0:#,###,##0.00}", Aprobado); ;
                Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Ampliacion);
                Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Reduccion);
                Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Modificado);
                Fila["DISPONIBLE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Hf_Disponible.Value.Trim()) ? "0" : Hf_Disponible.Value.Trim()));
                Fila["IMPORTE_ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero.Text.Trim()) ? "0" : Txt_Enero.Text.Trim()));
                Fila["IMPORTE_FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero.Text.Trim()) ? "0" : Txt_Febrero.Text.Trim()));
                Fila["IMPORTE_MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo.Text.Trim()) ? "0" : Txt_Marzo.Text.Trim()));
                Fila["IMPORTE_ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril.Text.Trim()) ? "0" : Txt_Abril.Text.Trim()));
                Fila["IMPORTE_MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo.Text.Trim()) ? "0" : Txt_Mayo.Text.Trim()));
                Fila["IMPORTE_JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio.Text.Trim()) ? "0" : Txt_Junio.Text.Trim()));
                Fila["IMPORTE_JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio.Text.Trim()) ? "0" : Txt_Julio.Text.Trim()));
                Fila["IMPORTE_AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto.Text.Trim()) ? "0" : Txt_Agosto.Text.Trim()));
                Fila["IMPORTE_SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre.Text.Trim()) ? "0" : Txt_Septiembre.Text.Trim()));
                Fila["IMPORTE_OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre.Text.Trim()) ? "0" : Txt_Octubre.Text.Trim()));
                Fila["IMPORTE_NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre.Text.Trim()) ? "0" : Txt_Noviembre.Text.Trim()));
                Fila["IMPORTE_DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre.Text.Trim()) ? "0" : Txt_Diciembre.Text.Trim()));
                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim()));
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Fila["ESTATUS"] = "AUTORIZADO";
                }
                else
                {
                    Fila["ESTATUS"] = Cmb_Estatus.SelectedItem.Text.Trim();
                }

                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    Fila["TIPO_OPERACION"] = "AMPLIACION";
                }
                else 
                {
                    Fila["TIPO_OPERACION"] = Cmb_Operacion.SelectedItem.Text.Trim();
                }

                Fila["TIPO_PARTIDA"] = Tipo_Partida.Trim();
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Fila["TIPO_EGRESO"] = "RAMO33";
                }
                else
                {
                    Fila["TIPO_EGRESO"] = "MUNICIPAL";
                }

                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    Fila["TIPO_MOVIMIENTO"] = "Nueva";
                }
                else
                {
                    Fila["TIPO_MOVIMIENTO"] = "Existente";
                }

                Fila["PARTIDA_ID"] = Hf_Partida_Origen_ID.Value.Trim();
                Fila["PROGRAMA_ID"] = Hf_Programa_Origen_ID.Value.Trim();
                Fila["FF_ID"] = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                Fila["AF_ID"] = Hf_Area_Funcional_Origen_ID.Value.Trim();
                Fila["UR_ID"] = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                if (Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                {
                    Fila["MOVIMIENTO_ID"] = 0;
                }
                else if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                {
                    Fila["MOVIMIENTO_ID"] = "";
                }
                Fila["CAPITULO_ID"] = Hf_Capitulo_Origen_ID.Value.Trim();
                Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();
                Dt_Mov.Rows.Add(Fila);
            }
            else
            {
                Fila = Dt_Mov.NewRow();
                Fila["CLAVE_NOM_PARTIDA"] = Hf_Partida_Destino.Value.Trim();
                Fila["CLAVE_NOM_PROGRAMA"] = Hf_Programa_Destino.Value.Trim();
                Fila["IMPORTE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Des.Text.Trim()) ? "0" : Txt_Total_Des.Text.Trim()));
                Fila["APROBADO"] = String.Format("{0:#,###,##0.00}", Aprobado); ;
                Fila["AMPLIACION"] = String.Format("{0:#,###,##0.00}", Ampliacion);
                Fila["REDUCCION"] = String.Format("{0:#,###,##0.00}", Reduccion);
                Fila["MODIFICADO"] = String.Format("{0:#,###,##0.00}", Modificado);
                Fila["DISPONIBLE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Hf_Disponible.Value.Trim()) ? "0" : Hf_Disponible.Value.Trim()));
                Fila["IMPORTE_ENERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Enero_Destino.Text.Trim()) ? "0" : Txt_Enero_Destino.Text.Trim()));
                Fila["IMPORTE_FEBRERO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Febrero_Destino.Text.Trim()) ? "0" : Txt_Febrero_Destino.Text.Trim()));
                Fila["IMPORTE_MARZO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Marzo_Destino.Text.Trim()) ? "0" : Txt_Marzo_Destino.Text.Trim()));
                Fila["IMPORTE_ABRIL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Abril_Destino.Text.Trim()) ? "0" : Txt_Abril_Destino.Text.Trim()));
                Fila["IMPORTE_MAYO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Mayo_Destino.Text.Trim()) ? "0" : Txt_Mayo_Destino.Text.Trim()));
                Fila["IMPORTE_JUNIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Junio_Destino.Text.Trim()) ? "0" : Txt_Junio_Destino.Text.Trim()));
                Fila["IMPORTE_JULIO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Julio_Destino.Text.Trim()) ? "0" : Txt_Julio_Destino.Text.Trim()));
                Fila["IMPORTE_AGOSTO"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Agosto_Destino.Text.Trim()) ? "0" : Txt_Agosto_Destino.Text.Trim()));
                Fila["IMPORTE_SEPTIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Septiembre_Destino.Text.Trim()) ? "0" : Txt_Septiembre_Destino.Text.Trim()));
                Fila["IMPORTE_OCTUBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Octubre_Destino.Text.Trim()) ? "0" : Txt_Octubre_Destino.Text.Trim()));
                Fila["IMPORTE_NOVIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Noviembre_Destino.Text.Trim()) ? "0" : Txt_Noviembre_Destino.Text.Trim()));
                Fila["IMPORTE_DICIEMBRE"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Diciembre_Destino.Text.Trim()) ? "0" : Txt_Diciembre_Destino.Text.Trim()));
                Fila["IMPORTE_TOTAL"] = String.Format("{0:#,###,##0.00}", Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Des.Text.Trim()) ? "0" : Txt_Total_Des.Text.Trim()));
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Fila["ESTATUS"] = "AUTORIZADO";
                }
                else
                {
                    Fila["ESTATUS"] = Cmb_Estatus.SelectedItem.Text.Trim();
                }
                
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    Fila["TIPO_OPERACION"] = "AMPLIACION";
                }
                else
                {
                    Fila["TIPO_OPERACION"] = Cmb_Operacion.SelectedItem.Text.Trim();
                }

                Fila["TIPO_PARTIDA"] = Tipo_Partida.Trim();
                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Fila["TIPO_EGRESO"] = "RAMO33";
                }
                else
                {
                    Fila["TIPO_EGRESO"] = "MUNICIPAL";
                }
                Fila["TIPO_MOVIMIENTO"] = "Existente";
                Fila["PARTIDA_ID"] = Hf_Partida_Destino_ID.Value.Trim();
                Fila["PROGRAMA_ID"] = Hf_Programa_Destino_ID.Value.Trim();
                Fila["FF_ID"] = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
                Fila["AF_ID"] = Hf_Area_Funcional_Destino_ID.Value.Trim();
                Fila["UR_ID"] = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                if (Btn_Modificar.ToolTip.Trim().Equals("Actualizar")) 
                {
                    Fila["MOVIMIENTO_ID"] = 0;
                }
                else if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar")) 
                {
                    Fila["MOVIMIENTO_ID"] = "";
                }
                Fila["CAPITULO_ID"] = Hf_Capitulo_Destino_ID.Value.Trim();
                Fila["JUSTIFICACION"] = Txt_Justificacion.Text.Trim();
                Dt_Mov.Rows.Add(Fila);
            }

            Session["Dt_Mov"] = (DataTable)Dt_Mov;
            Obtener_Consecutivo_ID();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_ID
    ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable 
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Consecutivo_ID()
    {
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        String Tipo_Partida = String.Empty;
        int Contador;
        try
        {
            if (Dt_Mov != null)
            {
                if (Dt_Mov.Columns.Count > 0)
                {
                    if (Dt_Mov.Rows.Count > 0)
                    {
                        Contador = 0;
                        foreach (DataRow Dr in Dt_Mov.Rows)
                        {
                            Contador++;
                            
                            if (Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                            {
                                Dr["MOVIMIENTO_ID"] = Contador;
                            }
                            else if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                            {
                                Dr["MOVIMIENTO_ID"] = Contador.ToString().Trim();
                            }
                        }
                    }
                }
            }
            Session["Dt_Mov"] = Dt_Mov;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Calcular_Total_Modificado
    ///DESCRIPCIÓN          : Metodo para calcular el total que se lleva modificado
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Calcular_Total_Modificado()
    {
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        Double Tot_Ori = 0.00;
        Double Tot_Des = 0.00;
        Double Restante = 0.00;

        try
        {
            if (Dt_Mov != null)
            {
                if (Dt_Mov.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Mov.Rows)
                    {
                        if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                        {
                            Tot_Ori += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                        }
                        else
                        {
                            Tot_Des += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                        }
                    }
                }
            }

            Restante = Tot_Ori - Tot_Des;

            Txt_Total_Mov_Destino.Text = String.Empty;
            Txt_Total_Mov_Origen.Text = String.Empty;
            Txt_Restante_Destino.Text = String.Empty;
            Txt_Importe.Text = String.Empty;
            Txt_Total_Mov_Destino.Text = String.Format("{0:c}", Tot_Des);
            Txt_Total_Mov_Origen.Text = String.Format("{0:c}", Tot_Ori);
            Txt_Restante_Destino.Text = String.Format("{0:c}", Restante);
            Txt_Importe.Text = String.Format("{0:#,###,##0.00}", Tot_Ori);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al calcular el total de modificado. Error[" + ex.Message + "]");
        }

    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Mov
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los movimientos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Mov()
    {
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        DataView Dv_Mov = null;

        try
        {
            Dv_Mov = new DataView(Dt_Mov);//Creamos el objeto que almacenara una vista de la tabla de roles.
            Dv_Mov.RowFilter = "TIPO_PARTIDA LIKE '%Origen%'";

            Grid_Mov_Origen.Columns[0].Visible = true;
            Grid_Mov_Origen.Columns[8].Visible = true;
            Grid_Mov_Origen.Columns[9].Visible = true;
            Grid_Mov_Origen.Columns[10].Visible = true;
            Grid_Mov_Origen.Columns[11].Visible = true;
            Grid_Mov_Origen.Columns[12].Visible = true;
            Grid_Mov_Origen.Columns[13].Visible = true;
            Grid_Mov_Origen.Columns[14].Visible = true;
            Grid_Mov_Origen.Columns[15].Visible = true;
            Grid_Mov_Origen.Columns[16].Visible = true;
            Grid_Mov_Origen.Columns[17].Visible = true;
            Grid_Mov_Origen.Columns[18].Visible = true;
            Grid_Mov_Origen.Columns[19].Visible = true;
            Grid_Mov_Origen.Columns[20].Visible = true;
            Grid_Mov_Origen.Columns[21].Visible = true;
            Grid_Mov_Origen.Columns[22].Visible = true;
            Grid_Mov_Origen.Columns[23].Visible = true;
            Grid_Mov_Origen.Columns[24].Visible = true;
            Grid_Mov_Origen.Columns[25].Visible = true;
            Grid_Mov_Origen.Columns[26].Visible = true;
            Grid_Mov_Origen.Columns[27].Visible = true;
            Grid_Mov_Origen.Columns[28].Visible = true;
            Grid_Mov_Origen.Columns[29].Visible = true;
            Grid_Mov_Origen.Columns[30].Visible = true;
            Grid_Mov_Origen.Columns[31].Visible = true;
            Grid_Mov_Origen.Columns[32].Visible = true;
            Grid_Mov_Origen.Columns[33].Visible = true;
            Grid_Mov_Origen.Columns[34].Visible = true;
            Grid_Mov_Origen.Columns[35].Visible = true;
            Grid_Mov_Origen.DataSource = Dv_Mov;
            Grid_Mov_Origen.DataBind();
            Grid_Mov_Origen.Columns[8].Visible = false;
            Grid_Mov_Origen.Columns[9].Visible = false;
            Grid_Mov_Origen.Columns[10].Visible = false;
            Grid_Mov_Origen.Columns[11].Visible = false;
            Grid_Mov_Origen.Columns[12].Visible = false;
            Grid_Mov_Origen.Columns[13].Visible = false;
            Grid_Mov_Origen.Columns[14].Visible = false;
            Grid_Mov_Origen.Columns[15].Visible = false;
            Grid_Mov_Origen.Columns[16].Visible = false;
            Grid_Mov_Origen.Columns[17].Visible = false;
            Grid_Mov_Origen.Columns[18].Visible = false;
            Grid_Mov_Origen.Columns[19].Visible = false;
            Grid_Mov_Origen.Columns[20].Visible = false;
            Grid_Mov_Origen.Columns[21].Visible = false;
            Grid_Mov_Origen.Columns[22].Visible = false;
            Grid_Mov_Origen.Columns[23].Visible = false;
            Grid_Mov_Origen.Columns[24].Visible = false;
            Grid_Mov_Origen.Columns[25].Visible = false;
            Grid_Mov_Origen.Columns[26].Visible = false;
            Grid_Mov_Origen.Columns[27].Visible = false;
            Grid_Mov_Origen.Columns[28].Visible = false;
            Grid_Mov_Origen.Columns[29].Visible = false;
            Grid_Mov_Origen.Columns[30].Visible = false;
            Grid_Mov_Origen.Columns[31].Visible = false;
            Grid_Mov_Origen.Columns[32].Visible = false;
            Grid_Mov_Origen.Columns[34].Visible = false;
            Grid_Mov_Origen.Columns[35].Visible = false;

            if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar") || Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
            {
                Grid_Mov_Origen.Columns[0].Visible = false;
            }
            else
            {
                Grid_Mov_Origen.Columns[33].Visible = false;
            }

            Dv_Mov = new DataView(Dt_Mov);//Creamos el objeto que almacenara una vista de la tabla de roles.
            Dv_Mov.RowFilter = "TIPO_PARTIDA LIKE '%Destino%'";

            Grid_Mov_Destino.Columns[0].Visible = true;
            Grid_Mov_Destino.Columns[8].Visible = true;
            Grid_Mov_Destino.Columns[9].Visible = true;
            Grid_Mov_Destino.Columns[10].Visible = true;
            Grid_Mov_Destino.Columns[11].Visible = true;
            Grid_Mov_Destino.Columns[12].Visible = true;
            Grid_Mov_Destino.Columns[13].Visible = true;
            Grid_Mov_Destino.Columns[14].Visible = true;
            Grid_Mov_Destino.Columns[15].Visible = true;
            Grid_Mov_Destino.Columns[16].Visible = true;
            Grid_Mov_Destino.Columns[17].Visible = true;
            Grid_Mov_Destino.Columns[18].Visible = true;
            Grid_Mov_Destino.Columns[19].Visible = true;
            Grid_Mov_Destino.Columns[20].Visible = true;
            Grid_Mov_Destino.Columns[21].Visible = true;
            Grid_Mov_Destino.Columns[22].Visible = true;
            Grid_Mov_Destino.Columns[23].Visible = true;
            Grid_Mov_Destino.Columns[24].Visible = true;
            Grid_Mov_Destino.Columns[25].Visible = true;
            Grid_Mov_Destino.Columns[26].Visible = true;
            Grid_Mov_Destino.Columns[27].Visible = true;
            Grid_Mov_Destino.Columns[28].Visible = true;
            Grid_Mov_Destino.Columns[29].Visible = true;
            Grid_Mov_Destino.Columns[30].Visible = true;
            Grid_Mov_Destino.Columns[31].Visible = true;
            Grid_Mov_Destino.Columns[32].Visible = true;
            Grid_Mov_Destino.Columns[33].Visible = true;
            Grid_Mov_Destino.Columns[34].Visible = true;
            Grid_Mov_Destino.Columns[35].Visible = true;
            Grid_Mov_Destino.DataSource = Dv_Mov;
            Grid_Mov_Destino.DataBind();
            Grid_Mov_Destino.Columns[8].Visible = false;
            Grid_Mov_Destino.Columns[9].Visible = false;
            Grid_Mov_Destino.Columns[10].Visible = false;
            Grid_Mov_Destino.Columns[11].Visible = false;
            Grid_Mov_Destino.Columns[12].Visible = false;
            Grid_Mov_Destino.Columns[13].Visible = false;
            Grid_Mov_Destino.Columns[14].Visible = false;
            Grid_Mov_Destino.Columns[15].Visible = false;
            Grid_Mov_Destino.Columns[16].Visible = false;
            Grid_Mov_Destino.Columns[17].Visible = false;
            Grid_Mov_Destino.Columns[18].Visible = false;
            Grid_Mov_Destino.Columns[19].Visible = false;
            Grid_Mov_Destino.Columns[20].Visible = false;
            Grid_Mov_Destino.Columns[21].Visible = false;
            Grid_Mov_Destino.Columns[22].Visible = false;
            Grid_Mov_Destino.Columns[23].Visible = false;
            Grid_Mov_Destino.Columns[24].Visible = false;
            Grid_Mov_Destino.Columns[25].Visible = false;
            Grid_Mov_Destino.Columns[26].Visible = false;
            Grid_Mov_Destino.Columns[27].Visible = false;
            Grid_Mov_Destino.Columns[28].Visible = false;
            Grid_Mov_Destino.Columns[29].Visible = false;
            Grid_Mov_Destino.Columns[30].Visible = false;
            Grid_Mov_Destino.Columns[31].Visible = false;
            Grid_Mov_Destino.Columns[32].Visible = false;
            Grid_Mov_Destino.Columns[34].Visible = false;
            Grid_Mov_Destino.Columns[35].Visible = false;
            if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar") || Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
            {
                Grid_Mov_Destino.Columns[0].Visible = false;
            }
            else
            {
                Grid_Mov_Destino.Columns[33].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al calcular el total de modificado. Error[" + ex.Message + "]");
        }

    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Busqueda_UR
    ///DESCRIPCIÓN          : Metodo para obtener la busqueda de las UR
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 17/julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Busqueda_UR(String Texto_Busqueda, String Tipo)
    {
        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Mov_Negocio_Ing = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Responsable = new DataTable();
        DataView Dv = null;
        String Expresion_Busqueda = String.Empty;//Variable que almacenara la expresion de búsqueda.

        try
        {
            if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Mov_Negocio_Ing.P_Tipo_Usuario = "Administrador";
                Dt_Responsable = Mov_Negocio_Ing.Consultar_URs();
            }
            else
            {
                Negocio.P_Empleado_Id = Cls_Sessiones.Empleado_ID.Trim();
                Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
                Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();

                Dt_Responsable = Negocio.Consultar_UR();
            }

            Dv = new DataView(Dt_Responsable);
            //Expresion_Busqueda = String.Format("{0} '%{1}%'", Texto_Busqueda);
            Expresion_Busqueda = String.Format("{0} '%{1}%'", Grid_Mov_Destino.SortExpression, Texto_Busqueda); 
            Dv.RowFilter = "Nombre like " + Expresion_Busqueda;
            Dv.RowFilter += " Or Nombre like " + Expresion_Busqueda.ToUpper();
            Dv.RowFilter += " Or Nombre like " + Expresion_Busqueda.ToLower();

            if (Tipo.Trim().Equals("ORIGEN"))
            {
                Cmb_Unidad_Responsable_Origen.Items.Clear();
                Cmb_Unidad_Responsable_Origen.DataSource = Dv;
                Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                Cmb_Unidad_Responsable_Origen.DataTextField = "Nombre";
                Cmb_Unidad_Responsable_Origen.DataBind();
                Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            else if (Tipo.Trim().Equals("DESTINO"))
            {
                Cmb_Unidad_Responsable_Destino.Items.Clear();
                Cmb_Unidad_Responsable_Destino.DataSource = Dv;
                Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                Cmb_Unidad_Responsable_Destino.DataTextField = "Nombre";
                Cmb_Unidad_Responsable_Destino.DataBind();
                Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
            else if (Tipo.Trim().Equals("INICIO"))
            {
                Cmb_Unidad_Responsable.Items.Clear();
                Cmb_Unidad_Responsable.DataSource = Dv;
                Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                Cmb_Unidad_Responsable.DataTextField = "Nombre";
                Cmb_Unidad_Responsable.DataBind();
                Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en la busqueda de las unidades responsables Erro[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mostrar_Datos_Solicitante
    ///DESCRIPCIÓN          : METODO PARA CREAR Dt del solicitante
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 13/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Mostrar_Datos_Solicitante()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Coordinador = new DataTable();
        String UR = String.Empty;
        String Tipo = "GERENTE";
        String Coordinador = String.Empty;
        DataTable Dt_Datos = new DataTable();

        Txt_Nombre_Solicitante.Text = String.Empty;
        Txt_Nombre_Dr.Text = String.Empty;
        Txt_Nombre_Psp.Text = String.Empty;
        Txt_Puesto_Solicitante.Text = String.Empty;
        Txt_Puesto_Dr.Text = String.Empty;
        Txt_Puesto_Psp.Text = String.Empty;
        Txt_Email_Dr.Text = String.Empty;
        Txt_Email_Psp.Text = String.Empty;

        try
        {
            //OBTENEMOS EL NOMBRE DEL REPRESENTANTE DEL AREA
            Negocio.P_Dependencia_ID_Busqueda = Cls_Sessiones.Dependencia_ID_Empleado.Trim();
            Negocio.P_Busqueda = Cls_Sessiones.Empleado_ID.Trim();
            Dt_Coordinador = Negocio.Obtener_Coordinador_UR();

            if (Dt_Coordinador != null)
            {
                if (Dt_Coordinador.Rows.Count > 0)
                {
                    Dt_Datos = (from Fila_Mov in Dt_Coordinador.AsEnumerable()
                                where Fila_Mov.Field<string>("CLAVE") == "USUARIO"
                                select Fila_Mov).AsDataView().ToTable();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Txt_Nombre_Solicitante.Text = Dt_Datos.Rows[0]["NOMBRE"].ToString().Trim().ToUpper();
                            Txt_Puesto_Solicitante.Text = Dt_Datos.Rows[0]["TIPO"].ToString().Trim().ToUpper();
                        }
                    }

                    Dt_Datos = new DataTable();
                    Dt_Datos = (from Fila_Mov in Dt_Coordinador.AsEnumerable()
                                where Fila_Mov.Field<string>("CLAVE") == "COORDINADOR PSP"
                                select Fila_Mov).AsDataView().ToTable();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Txt_Nombre_Psp.Text = Dt_Datos.Rows[0]["NOMBRE"].ToString().Trim().ToUpper();
                            Txt_Puesto_Psp.Text = Dt_Datos.Rows[0]["TIPO"].ToString().Trim().ToUpper();
                            Txt_Email_Psp.Text = Dt_Datos.Rows[0]["EMAIL"].ToString().Trim();
                        }
                    }

                    Dt_Datos = new DataTable();
                    Dt_Datos = (from Fila_Mov in Dt_Coordinador.AsEnumerable()
                                where Fila_Mov.Field<string>("CLAVE") == "OTROS"
                                select Fila_Mov).AsDataView().ToTable();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                if (Dr["TIPO"].ToString().Trim().Equals(Tipo) || Dr["TIPO"].ToString().Trim().Equals(Tipo.ToUpper()))
                                {
                                    Txt_Nombre_Dr.Text = Dr["NOMBRE"].ToString().Trim().ToUpper();
                                    Txt_Puesto_Dr.Text = "GERENTE " + Dr["UR"].ToString().Trim().ToUpper();
                                    Txt_Email_Dr.Text = Dr["EMAIL"].ToString().Trim();
                                }
                            }
                            if (String.IsNullOrEmpty(Coordinador.Trim()))
                            {
                                Tipo = "COORDINADOR";
                                foreach (DataRow Dr in Dt_Datos.Rows)
                                {
                                    if (Dr["TIPO"].ToString().Trim().Equals(Tipo) || Dr["TIPO"].ToString().Trim().Equals(Tipo.ToUpper()))
                                    {
                                        Txt_Nombre_Dr.Text = Dr["NOMBRE"].ToString().Trim().ToUpper();
                                        Txt_Puesto_Dr.Text = "COORDINADOR " + Dr["UR"].ToString().Trim().ToUpper();
                                        Txt_Email_Dr.Text = Dr["EMAIL"].ToString().Trim();
                                    }
                                }
                            }
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar la tabla de solicitud. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Solicitante
    ///DESCRIPCIÓN          : METODO PARA CREAR Dt del solicitante
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public DataTable Crear_Dt_Solicitante()
    {
        DataTable Dt_Encabezado = new DataTable();
        DataRow Fila;

        try
        {
            Dt_Encabezado.Columns.Add("Coordinador_PSP");
            Dt_Encabezado.Columns.Add("Puesto_Coordinador_PSP");
            Dt_Encabezado.Columns.Add("Email_Coordinador_PSP");
            Dt_Encabezado.Columns.Add("Solicitante");
            Dt_Encabezado.Columns.Add("Puesto_Solicitante");
            Dt_Encabezado.Columns.Add("Ur_Solicitante");
            Dt_Encabezado.Columns.Add("Dr");
            Dt_Encabezado.Columns.Add("Puesto_Dr");
            Dt_Encabezado.Columns.Add("Email_Dr");

            Fila = Dt_Encabezado.NewRow();

            Fila["Coordinador_PSP"] = Txt_Nombre_Psp.Text.ToUpper().Trim();
            Fila["Puesto_Coordinador_PSP"] = Txt_Puesto_Psp.Text.ToUpper().Trim();
            Fila["Email_Coordinador_PSP"] = Txt_Email_Psp.Text.Trim();
            Fila["Solicitante"] = Txt_Nombre_Solicitante.Text.ToUpper().Trim();
            Fila["Puesto_Solicitante"] = Txt_Puesto_Solicitante.Text.ToUpper().Trim();
            Fila["Ur_Solicitante"] = Cmb_Unidad_Responsable_Origen.SelectedItem.Text.Trim();
            Fila["Dr"] = Txt_Nombre_Dr.Text.ToUpper().Trim();
            Fila["Puesto_Dr"] = Txt_Puesto_Dr.Text.ToUpper().Trim();
            Fila["Email_Dr"] = Txt_Email_Dr.Text.Trim();
            Dt_Encabezado.Rows.Add(Fila);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar la tabla de solicitud. Error[" + ex.Message + "]");
        }
        return Dt_Encabezado;
    }
    #endregion
    
    #region(Control Acceso Pagina)
    /// ******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Hugo Enrique Ramírez Aguilera
    /// FECHA CREÓ  : 07/Noviembre/2011 
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(this.Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS  : Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011 
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 07/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        String Espacios_Blanco;
        DataTable Dt_Partidas = new DataTable();
        String Mes = String.Empty;
        Double Importe = 0.00;
        Double Tot_Mov_Origen = 0.00;
        Double Tot_Mov_Destino = 0.00;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Favor de hacer lo siguiente: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());

        Tot_Mov_Origen = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Mov_Origen.Text.Trim()) ? "0" : Txt_Total_Mov_Origen.Text.Trim().Replace("$", ""));
        Tot_Mov_Destino = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Mov_Destino.Text.Trim()) ? "0" : Txt_Total_Mov_Destino.Text.Trim().Replace("$", ""));

        //para la seccion de datos generales
        if (Cmb_Operacion.SelectedIndex < 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione la operacion deseada. <br>";
            Datos_Validos = false;
        }
        else
        {
            if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("TRASPASO"))
            {
                if (Importe < Tot_Mov_Destino || Tot_Mov_Destino < Importe)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Favor de verificar los datos de la operacion porque el importe no coincide con el total de la partida destino.<br>";
                    Datos_Validos = false;
                }
            }
        }
        if (Txt_Importe.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el Importe deseado.<br>";
            Datos_Validos = false;
        }
        if (Txt_Justificacion.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese la justificación.<br>";
            Datos_Validos = false;
        }

        if (Importe < Tot_Mov_Origen || Tot_Mov_Origen < Importe)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Favor de verificar los datos de la operacion porque el importe no coincide con el total de la partida origen.<br>";
            Datos_Validos = false;
        }

        return Datos_Validos;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Mov_Org
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos
    /// CREO        : Leslie Gonzalez Vazquez
    /// FECHA_CREO  : 14/mayo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Mov_Org()
    {
        String Espacios_Blanco;
        DataTable Dt_Mov = new DataTable();
        String Mes = String.Empty;
        String P_ID = String.Empty;
        String PP_ID = String.Empty;
        String UR_ID = String.Empty;
        String FF_ID = String.Empty;
        Double Importe = 0.00;
        Double Importe_Programa = 0.00;
        Double Total_Origen = 0.00;
        Double Tot_Mov_Origen = 0.00;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Favor de hacer lo siguiente: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Sumar_Total_Destino();
        Sumar_Total_Origen();
        Dt_Mov = (DataTable)Session["Dt_Mov"];

        Total_Origen = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total.Text.Trim()) ? "0" : Txt_Total.Text.Trim());
        Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());
        Tot_Mov_Origen = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Mov_Origen.Text.Trim()) ? "0" : Txt_Total_Mov_Origen.Text.Trim().Replace("$", "").Replace(",", "")) + Total_Origen;

        //para la seccion de datos generales
        if (Cmb_Operacion.SelectedIndex < 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione la operacion deseada. <br>";
            Datos_Validos = false;
        }
        if (Cmb_Unidad_Responsable_Origen.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Unidad responsable de Origen.<br>";
            Datos_Validos = false;
        }
        if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Fuente de financiamiento de Origen.<br>";
            Datos_Validos = false;
        }

        if (Txt_Total.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida origen.<br>";
            Datos_Validos = false;
        }

        if (Txt_Justificacion.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese una justificación.<br>";
            Datos_Validos = false;
        }

        else
        {
            if (Total_Origen <= 0)
            {
                if (Cmb_Operacion.SelectedItem.Value.Trim() != "SUPLEMENTO")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida origen.<br>";
                    Datos_Validos = false;
                }
            }
        }

        
        if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
        {
            if (Cmb_Programa_Origen.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione un programa.<br>";
                Datos_Validos = false;
            }
            else 
            {
                Hf_Programa_Origen_ID.Value = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                Importe_Programa = Convert.ToDouble(String.IsNullOrEmpty(Hf_Importe_Programa.Value.Trim()) ? "0" : Hf_Importe_Programa.Value.Trim());
                if (Importe_Programa > 0)
                {
                    if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Mov.Rows)
                        {
                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                            {
                                Importe += Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim()) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim());
                            }
                        }
                        Importe = Importe + Total_Origen;
                    }
                    else 
                    {
                        Importe = Importe + Total_Origen;
                    }

                    if (Importe > Importe_Programa) 
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "* El importe ampliar es mayor al importe del programa. Importe programa[" +String.Format("{0:n}", Importe_Programa)+ "].<br>";
                        Datos_Validos = false;
                    }
                }
            }
        }
        
        if (Datos_Validos)
        {
            if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
            {
                P_ID = Hf_Partida_Origen_ID.Value.Trim();
                PP_ID = Hf_Programa_Origen_ID.Value.Trim();
                FF_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                UR_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();

                foreach (DataRow Dr in Dt_Mov.Rows)
                {
                    if (Dr["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                    {
                        if (Dr["PARTIDA_ID"].ToString().Trim().Equals(P_ID) && Dr["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) && Dr["UR_ID"].ToString().Trim().Equals(UR_ID) && Dr["FF_ID"].ToString().Trim().Equals(FF_ID) && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione otra partida porque esta ya existe en la modificación.<br>";
                            Datos_Validos = false;
                            break;
                        }
                    }
                    else if (Dr["PROGRAMA_ID"].ToString().Trim() != PP_ID && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Solo se pueden elegir movimientos del mismo programa.<br>";
                        Datos_Validos = false;
                        break;
                    }
                }
            }
        }

        return Datos_Validos;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Mov_Des
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos
    /// CREO        : Leslie Gonzalez Vazquez
    /// FECHA_CREO  : 14/mayo/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Mov_Des()
    {
        String Espacios_Blanco;
        DataTable Dt_Mov = new DataTable();
        String Mes = String.Empty;
        String P_ID = String.Empty;
        String PP_ID = String.Empty;
        String UR_ID = String.Empty;
        String FF_ID = String.Empty;
        Double Importe = 0.00;
        Double Total_Destino = 0.00;
        Double Tot_Mov_Destino = 0.00;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Favor de hacer lo siguiente: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Sumar_Total_Destino();
        Dt_Mov = (DataTable)Session["Dt_Mov"];

        Total_Destino = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Des.Text.Trim()) ? "0" : Txt_Total_Des.Text.Trim());
        Importe = Convert.ToDouble(String.IsNullOrEmpty(Txt_Importe.Text.Trim()) ? "0" : Txt_Importe.Text.Trim());
        Tot_Mov_Destino = Convert.ToDouble(String.IsNullOrEmpty(Txt_Total_Mov_Destino.Text.Trim()) ? "0" : Txt_Total_Mov_Destino.Text.Trim().Replace("$", "").Replace(",", "")) + Total_Destino;

        //para la seccion de datos generales
        if (Cmb_Operacion.SelectedIndex < 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione la operacion deseada. <br>";
            Datos_Validos = false;
        }
        else
        {
            if (Cmb_Operacion.SelectedItem.Text.Trim().Equals("TRASPASO"))
            {
                if (Cmb_Unidad_Responsable_Destino.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Unidad responsable de Destino.<br>";
                    Datos_Validos = false;
                }
                if (Cmb_Fuente_Financiamiento_Destino.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun Fuente de financiamiento de Destino.<br>";
                    Datos_Validos = false;
                }
                if (Txt_Total_Des.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida destino.<br>";
                    Datos_Validos = false;
                }
                else
                {
                    if (Total_Destino <= 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el importe de los meses de la partida destino.<br>";
                        Datos_Validos = false;
                    }
                }
                
                if (Tot_Mov_Destino > Importe)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "* Verifique el movimiento puesto que ya sobrepaso el importe a traspasar.<br>";
                    Datos_Validos = false;
                }

                if (Txt_Justificacion.Text == "")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese una justificación.<br>";
                    Datos_Validos = false;
                }
            }
        }
        if (Txt_Importe.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el Importe deseado.<br>";
            Datos_Validos = false;
        }

        if (Datos_Validos)
        {
            if (Dt_Mov != null && Dt_Mov.Rows.Count > 0)
            {
                P_ID = Hf_Partida_Destino_ID.Value.Trim();
                PP_ID = Hf_Programa_Destino_ID.Value.Trim();
                FF_ID = Cmb_Fuente_Financiamiento_Destino.SelectedItem.Value.Trim();
                UR_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();

                foreach (DataRow Dr in Dt_Mov.Rows)
                {
                    if (Dr["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino"))
                    {
                        if (Dr["PARTIDA_ID"].ToString().Trim().Equals(P_ID) && Dr["PROGRAMA_ID"].ToString().Trim().Equals(PP_ID) && Dr["UR_ID"].ToString().Trim().Equals(UR_ID) && Dr["FF_ID"].ToString().Trim().Equals(FF_ID) && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino"))
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione otra partida porque esta ya existe en la modificación.<br>";
                            Datos_Validos = false;
                            break;
                        }
                    }
                    else if (Dr["PROGRAMA_ID"].ToString().Trim() != PP_ID && Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Destino"))
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Solo se pueden elegir movimientos del mismo programa.<br>";
                        Datos_Validos = false;
                        break;
                    }
                }
            }
        }

        return Datos_Validos;
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Datos_Solicitante
    ///DESCRIPCIÓN          : METODO validar los datos del solicitante
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 13/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public Boolean Validar_Datos_Solicitante()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Error_Busqueda.Text = "Favor de llenar todo los datos, para generar la catatula de solicitud de movimiento y poder enviar el correo de aviso <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        try
        {
            if (String.IsNullOrEmpty(Txt_Nombre_Solicitante.Text))
            {
                Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el nombre del Remitente. <br>";
                Datos_Validos = false;
            }

            if (String.IsNullOrEmpty(Txt_Nombre_Dr.Text))
            {
                Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el nombre del Destinatario. <br>";
                Datos_Validos = false;
            }

            if (String.IsNullOrEmpty(Txt_Puesto_Solicitante.Text))
            {
                Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el puesto del Remitente. <br>";
                Datos_Validos = false;
            }

            if (String.IsNullOrEmpty(Txt_Puesto_Dr.Text))
            {
                Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el puesto del Destinatario. <br>";
                Datos_Validos = false;
            }

            if (Chk_Enviar_correo.Checked)
            {
                if (String.IsNullOrEmpty(Txt_Nombre_Psp.Text))
                {
                    Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el nombre del revisor del Presupuesto. <br>";
                    Datos_Validos = false;
                }

                if (String.IsNullOrEmpty(Txt_Puesto_Psp.Text))
                {
                    Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el puesto del revisor del Presupuesto. <br>";
                    Datos_Validos = false;
                }

                if (String.IsNullOrEmpty(Txt_Email_Dr.Text))
                {
                    Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el correo electronico del Destinatario. <br>";
                    Datos_Validos = false;
                }

                if (String.IsNullOrEmpty(Txt_Email_Psp.Text))
                {
                    Lbl_Error_Busqueda.Text += Espacios_Blanco + "*Introducir el correo electronico del revisor del Presupuesto. <br>";
                    Datos_Validos = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al validar los datos de la solicitud. Error[" + ex.Message + "]");
        }

        return Datos_Validos;
    }
    #endregion

    #region(metodos De consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Movimiento
    /// DESCRIPCION : Consulta los movimientos que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 17-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Movimiento()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Movimiento; //Variable que obtendra los datos de la consulta 
        DateTime Fecha_Ini;
        DateTime Fecha_Fin;
        DataTable Dt_Mov = new DataTable();

        try
        {
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Consulta.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable.SelectedValue.Trim();
            }
            else
            {
                Consulta.P_Dependencia_ID_Busqueda = String.Empty;
            }

            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text.Trim()) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text.Trim()))
            {
                Fecha_Fin = Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", Txt_Fecha_Final.Text.Trim()));
                Fecha_Ini = Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", Txt_Fecha_Inicial.Text.Trim()));
                if (Fecha_Ini.CompareTo(Fecha_Fin) <= 0)
                {
                    Consulta.P_Fecha_Inicio = String.Format("{0:dd/MM/yyyy}", Fecha_Ini);
                    Consulta.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Fecha_Fin);
                }
            }
            else
            {
                Consulta.P_Fecha_Inicio = String.Empty;
                Consulta.P_Fecha_Final = String.Empty;
            }

            if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                Consulta.P_No_Solicitud = Txt_Busqueda.Text.Trim();
            }
            else
            {
                Consulta.P_No_Solicitud = String.Empty;
            }

            if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Consulta.P_Tipo_Egreso = "RAMO33";
            }
            else
            {
                Consulta.P_Tipo_Egreso = "MUNICIPAL";
            }
            Consulta.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
            Dt_Movimiento = Consulta.Consultar_Like_Movimiento();

            if (Dt_Movimiento != null)
            {
                if (Dt_Movimiento.Rows.Count > 0)
                {
                    Dt_Mov = (from Fila in Dt_Movimiento.AsEnumerable()
                              where Fila.Field<string>("TIPO_OPERACION") != "ADECUACION"
                              select Fila).AsDataView().ToTable();
                    Dt_Movimiento = Dt_Mov;
                }
            }

            Grid_Movimiento.Columns[7].Visible = true;
            Grid_Movimiento.Columns[8].Visible = true;
            Grid_Movimiento.Columns[9].Visible = true;
            Grid_Movimiento.Columns[10].Visible = true;
            Grid_Movimiento.Columns[11].Visible = true;
            Grid_Movimiento.DataSource = Dt_Movimiento;
            Grid_Movimiento.DataBind();
            Grid_Movimiento.Columns[7].Visible = false;
            Grid_Movimiento.Columns[8].Visible = false;
            Grid_Movimiento.Columns[9].Visible = false;
            Grid_Movimiento.Columns[10].Visible = false;
            Grid_Movimiento.Columns[11].Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Movimiento Presupuestal" + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Buscar_Movimientos
    ///DESCRIPCIÓN          : Metodo para buscar 
    ///PARAMETROS           1 Texto_Buscar: texto que buscaremos en el grid
    ///                     2 Dt_Datos: tabla de donde buscaremos los datos
    ///                     3 Tbl_Datos: Grid donde realizaremos la busqueda
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 25/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    public void Buscar_Movimientos(String Texto_Buscar, DataTable Dt_Datos)
    {
        DataTable Dt = new DataTable();
        try
        {
            Dt = (from Fila in Dt_Datos.AsEnumerable()
                  where (Convert.ToString(Fila.Field<Decimal>("SOLICITUD_ID")) == Texto_Buscar
                  || Convert.ToString(Fila.Field<String>("TIPO_OPERACION")).Contains(Texto_Buscar.ToUpper())
                  || Convert.ToString(Fila.Field<Decimal>("IMPORTE")) == Texto_Buscar
                  || Convert.ToString(Fila.Field<String>("ESTATUS")).Contains(Texto_Buscar.ToUpper())
                  || Convert.ToString(Fila.Field<String>("FECHA_CREO")).Contains(Texto_Buscar.ToUpper())
                  || Convert.ToString(Fila.Field<String>("USUARIO_CREO")).Contains(Texto_Buscar.ToUpper()))
                  select Fila).AsDataView().ToTable();

            Grid_Movimiento.Columns[7].Visible = true;
            Grid_Movimiento.Columns[8].Visible = true;
            Grid_Movimiento.Columns[9].Visible = true;
            Grid_Movimiento.Columns[10].Visible = true;
            Grid_Movimiento.Columns[11].Visible = true;
            Grid_Movimiento.DataSource = Dt;
            Grid_Movimiento.DataBind();
            Grid_Movimiento.Columns[7].Visible = false;
            Grid_Movimiento.Columns[8].Visible = false;
            Grid_Movimiento.Columns[9].Visible = false;
            Grid_Movimiento.Columns[10].Visible = false;
            Grid_Movimiento.Columns[11].Visible = false;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al ejecutar la busqueda de movimientos. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #region (Reporte)
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Reporte
    ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 01/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Crear_Reporte(DataTable Dt_Datos)
    {
        DataSet Ds_Registros = null;
        DataTable Dt_Encabezado = new DataTable();
        DataTable Dt_Mov_Justificacion = new DataTable();
        DataTable Dt_Mov_Origen = new DataTable();
        DataTable Dt_Mov_Destino = new DataTable();
        DataTable Dt_Mov_Origen_Det = new DataTable();
        DataTable Dt_Mov_Destino_Det = new DataTable();
        String No_Movimiento = String.Empty;
        String No_Solicitud = String.Empty;

        try
        {
            No_Movimiento = Dt_Datos.Rows[0]["No_Movimiento"].ToString().Trim();
            No_Solicitud = Dt_Datos.Rows[0]["No_Solicitud"].ToString().Trim();

            Ds_Registros = new DataSet();
            Dt_Encabezado = Crear_Dt_Encabezado(No_Movimiento, No_Solicitud);
            Dt_Mov_Origen = Crear_Dt_Movimientos("Origen", "Normal", No_Solicitud);
            Dt_Mov_Destino = Crear_Dt_Movimientos("Destino", "Normal", No_Solicitud);
            Dt_Mov_Origen_Det = Crear_Dt_Movimientos("Origen", "Det", No_Solicitud);
            Dt_Mov_Destino_Det = Crear_Dt_Movimientos("Destino", "Det", No_Solicitud);
            Dt_Mov_Justificacion = Crear_Dt_Movimientos("", "Jus", No_Solicitud);
            Dt_Encabezado.TableName = "Dt_Encabezado";
            Dt_Mov_Origen.TableName = "Dt_Mov_Origen";
            Dt_Mov_Destino.TableName = "Dt_Mov_Destino";
            Dt_Mov_Origen_Det.TableName = "Dt_Mov_Origen_Det";
            Dt_Mov_Destino_Det.TableName = "Dt_Mov_Destino_Det";
            Dt_Mov_Justificacion.TableName = "Dt_Mov_Justificacion";

            Ds_Registros.Tables.Add(Dt_Encabezado.Copy());
            Ds_Registros.Tables.Add(Dt_Mov_Origen.Copy());
            Ds_Registros.Tables.Add(Dt_Mov_Destino.Copy());
            Ds_Registros.Tables.Add(Dt_Mov_Origen_Det.Copy());
            Ds_Registros.Tables.Add(Dt_Mov_Destino_Det.Copy());
            Ds_Registros.Tables.Add(Dt_Mov_Justificacion.Copy());

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
            {
                Generar_Reporte(ref Ds_Registros, "Cr_Ope_Psp_Movimiento_Presupuestal.rpt",
                    "Solicitud_Movimientos" + Session.SessionID + ".pdf");
            }
            else
            {
                Generar_Reporte(ref Ds_Registros, "Cr_Rpt_Psp_Movimiento_Presupuestal.rpt",
                    "Solicitud_Movimientos" + Session.SessionID + ".pdf");
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
        }
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Encabezado
    ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 01/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public DataTable Crear_Dt_Encabezado(String No_Mov, String No_Solicitud)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Encabezado = new DataTable();
        DataRow Fila;
        String Texto = String.Empty;
        String UR = String.Empty;
        String Coordinador = String.Empty;
        String Puesto_Coordinador = String.Empty;
        String Puesto_Solicito = String.Empty;
        String Solicito = String.Empty;
        DataTable Dt = new DataTable();

        try
        {
            Negocio.P_No_Solicitud = No_Solicitud;
            Dt = Negocio.Consultar_Datos_Movimientos_Det();

            if (Dt != null)
            {
                if (Dt.Rows.Count > 0)
                {
                    Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Solicitante].ToString().Trim().ToUpper();
                    Puesto_Solicito = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Solicitante].ToString().Trim().ToUpper();
                    Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Nombre_Director].ToString().Trim().ToUpper();
                    Puesto_Coordinador = Dt.Rows[0][Ope_Psp_Movimiento_Egr_Det.Campo_Puesto_Director].ToString().Trim().ToUpper();
                    UR = Dt.Rows[0]["UR"].ToString().Trim().ToUpper();
                }
            }

            Dt_Encabezado.Columns.Add("Nombre_UR");
            Dt_Encabezado.Columns.Add("No_Modificacion");
            Dt_Encabezado.Columns.Add("No_Solicitud");
            Dt_Encabezado.Columns.Add("Anio");
            Dt_Encabezado.Columns.Add("Fecha");
            Dt_Encabezado.Columns.Add("Solicitante");
            Dt_Encabezado.Columns.Add("Texto");
            Dt_Encabezado.Columns.Add("Puesto_Solicitante");
            Dt_Encabezado.Columns.Add("Dirigido");
            Dt_Encabezado.Columns.Add("Puesto_Dirigido");

            Texto = "Por medio del presente reciba un cordial saludo, a la vez que solicito su valioso apoyo";
            Texto += " para que sean consideradas las modificaciones al presupuesto de la " + UR;
            Texto += " de acuerdo al archivo anexo.";

            Fila = Dt_Encabezado.NewRow();
            Fila["Nombre_UR"] = UR.Trim();
            Fila["No_Modificacion"] = No_Mov.Trim() + "a. Modificación - No. Solicitud " + No_Solicitud.Trim() 
                + " - " + String.Format("{0:yyyy}", DateTime.Now);
            Fila["No_Solicitud"] = No_Solicitud.Trim();
            Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
            Fila["Fecha"] = Crear_Fecha(String.Format("{0:MM/dd/yyyy}", DateTime.Now));
            Fila["Solicitante"] = Solicito.Trim();
            Fila["Texto"] = Texto;
            Fila["Puesto_Solicitante"] = Puesto_Solicito.Trim();
            Fila["Dirigido"] = Coordinador.Trim();
            Fila["Puesto_Dirigido"] = Puesto_Coordinador.Trim();
            Dt_Encabezado.Rows.Add(Fila);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
        }
        return Dt_Encabezado;
    }

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Mov_Origen
    ///DESCRIPCIÓN          : METODO PARA CREAR EL REPORTE DE MOVIMIENTOS
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 01/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public DataTable Crear_Dt_Movimientos(String Tipo_Partida, String Tipo_Dt, String No_Solicitud)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Mov = new DataTable();
        DataTable Dt_Agrupado = new DataTable();
        DataRow Fila;
        DataTable Dt = new DataTable();
        Negocio.P_No_Solicitud = No_Solicitud;
        Dt = Negocio.Consultar_Datos_Movimientos_Det();
        String Codigo_Programatico = String.Empty;

        try
        {
            if (Tipo_Dt.Trim().Equals("Det"))
            {
                Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("Imp_Ene", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Feb", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Mar", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Abr", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_May", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Jun", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Jul", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Ago", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Sep", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Oct", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Nov", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Dic", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Imp_Tot", System.Type.GetType("System.Double"));

                foreach (DataRow Dr in Dt.Rows)
                {
                    if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                    {
                        Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                             "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                             "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                        Fila = Dt_Mov.NewRow();
                        Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                        Fila["FF"] = Codigo_Programatico.Trim();
                        Fila["AF"] = Dr["AF"].ToString().Trim();
                        Fila["PP"] = Dr["PP"].ToString().Trim();
                        Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                        Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                        Fila["Imp_Ene"] = Dr["ENERO"];
                        Fila["Imp_Feb"] = Dr["FEBRERO"];
                        Fila["Imp_Mar"] = Dr["MARZO"];
                        Fila["Imp_Abr"] = Dr["ABRIL"];
                        Fila["Imp_May"] = Dr["MAYO"];
                        Fila["Imp_Jun"] = Dr["JUNIO"];
                        Fila["Imp_Jul"] = Dr["JULIO"];
                        Fila["Imp_Ago"] = Dr["AGOSTO"];
                        Fila["Imp_Sep"] = Dr["SEPTIEMBRE"];
                        Fila["Imp_Oct"] = Dr["OCTUBRE"];
                        Fila["Imp_Nov"] = Dr["NOVIEMBRE"];
                        Fila["Imp_Dic"] = Dr["DICIEMBRE"];
                        Fila["Imp_Tot"] = Dr["IMPORTE"];
                        Dt_Mov.Rows.Add(Fila);
                    }
                }
            }
            else if (Tipo_Dt.Trim().Equals("Jus"))
            {
                Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("Justificacion", System.Type.GetType("System.String"));

                foreach (DataRow Dr in Dt.Rows)
                {
                    Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                             "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                             "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                    Fila = Dt_Mov.NewRow();
                    Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                    Fila["FF"] = Codigo_Programatico.Trim();
                    Fila["AF"] = Dr["AF"].ToString().Trim();
                    Fila["PP"] = Dr["PP"].ToString().Trim();
                    Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                    Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                    Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                    Fila["Justificacion"] = Dr["JUSTIFICACION"].ToString().Trim();
                    Dt_Mov.Rows.Add(Fila);
                }

                //FILTRAMOS QUE SEA UNA JUSTIFICACION POR CODIGO PROGRAMATICO
                var Agrupacion = (from Fila_Just in Dt_Mov.AsEnumerable()
                                  group Fila_Just by new
                                  {
                                      Anio = Fila_Just.Field<String>("Anio"),
                                      FF = Fila_Just.Field<String>("FF"),
                                      AF = Fila_Just.Field<String>("AF"),
                                      PP = Fila_Just.Field<String>("PP"),
                                      UR = Fila_Just.Field<String>("UR"),
                                      PARTIDA = Fila_Just.Field<String>("Partida"),
                                      P = Fila_Just.Field<String>("P"),
                                      Just = Fila_Just.Field<String>("Justificacion")
                                  }
                                      into mov
                                      orderby mov.Key.FF
                                      select Dt_Mov.LoadDataRow(
                                        new object[]
                                        {
                                            mov.Key.Anio,
                                            mov.Key.FF,
                                            mov.Key.AF,
                                            mov.Key.PP,
                                            mov.Key.UR,
                                            mov.Key.PARTIDA,
                                            mov.Key.P,
                                            mov.Key.Just,
                                            
                                        }, LoadOption.PreserveChanges));

                //obtenemos los datos agrupados en una tabla
                Dt_Agrupado = Agrupacion.CopyToDataTable();
                Dt_Mov = Dt_Agrupado;
            }
            else
            {
                Dt_Mov.Columns.Add("Anio", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("FF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("AF", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("PP", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("UR", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("P", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("Partida", System.Type.GetType("System.String"));
                Dt_Mov.Columns.Add("Presupuesto_Aprobado", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Importe", System.Type.GetType("System.Double"));
                Dt_Mov.Columns.Add("Presupuesto_Modificado", System.Type.GetType("System.Double"));

                foreach (DataRow Dr in Dt.Rows)
                {
                    if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals(Tipo_Partida.Trim()))
                    {
                        Codigo_Programatico = Dr["FF"].ToString().Trim() + "-" + Dr["AF"].ToString().Trim() + "-" + Dr["PP"].ToString().Trim() +
                             "-" + Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" ")) +
                             "-" + Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));

                        Fila = Dt_Mov.NewRow();
                        Fila["Anio"] = String.Format("{0:yyyy}", DateTime.Now);
                        Fila["FF"] = Codigo_Programatico.Trim();
                        Fila["AF"] = Dr["AF"].ToString().Trim();
                        Fila["PP"] = Dr["PP"].ToString().Trim();
                        Fila["UR"] = Dr["UR"].ToString().Trim().Substring(0, Dr["UR"].ToString().Trim().IndexOf(" "));
                        Fila["P"] = Dr["PARTIDA"].ToString().Trim().Substring(0, Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                        Fila["Partida"] = Dr["PARTIDA"].ToString().Trim().Substring(Dr["PARTIDA"].ToString().Trim().IndexOf(" "));
                        Fila["Presupuesto_Aprobado"] = Dr["APROBADO"];
                        Fila["Importe"] = Dr["IMPORTE"];
                        Fila["Presupuesto_Modificado"] = Dr["MODIFICADO"];
                        Dt_Mov.Rows.Add(Fila);
                    }
                }

            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al generar el reporte. Error[" + ex.Message + "]");
        }
        return Dt_Mov;
    }

    //****************************************************************************************************
    //NOMBRE DE LA FUNCIÓN : Crear_Fecha
    //DESCRIPCIÓN          : Metodo para obtener la fecha en letras
    //PARAMETROS           1 Fecha: fecha a la cual le daremos formato
    //CREO                 : Leslie González Vázquez
    //FECHA_CREO           : 13/Diciembre/2011 
    //MODIFICO             :
    //FECHA_MODIFICO       :
    //CAUSA_MODIFICACIÓN   :
    //****************************************************************************************************
    internal static String Crear_Fecha(String Fecha)
    {
        String Fecha_Formateada = String.Empty;
        String Mes = String.Empty;
        String[] Fechas;

        try
        {
            Fechas = Fecha.Split('/');
            Mes = Fechas[0].ToString();
            switch (Mes)
            {
                case "01":
                    Mes = "Enero";
                    break;
                case "02":
                    Mes = "Febrero";
                    break;
                case "03":
                    Mes = "Marzo";
                    break;
                case "04":
                    Mes = "Abril";
                    break;
                case "05":
                    Mes = "Mayo";
                    break;
                case "06":
                    Mes = "Junio";
                    break;
                case "07":
                    Mes = "Julio";
                    break;
                case "08":
                    Mes = "Agosto";
                    break;
                case "09":
                    Mes = "Septiembre";
                    break;
                case "10":
                    Mes = "Octubre";
                    break;
                case "11":
                    Mes = "Noviembre";
                    break;
                default:
                    Mes = "Diciembre";
                    break;
            }
            Fecha_Formateada = "Irapuato, Guanajuato a " + Fechas[1].ToString() + " de " + Mes + " de " + Fechas[2].ToString();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al crear el formato de fecha. Error: [" + Ex.Message + "]");
        }
        return Fecha_Formateada;
    }

    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en pantalla.
    ///              
    /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        //String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
              "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #region (Subir Archivos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Asy_Cargar_Archivo_Complete
    ///DESCRIPCIÓN          : Metodo para cargar los archivos anexos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        AsyncFileUpload Archivo;
        String Nombre_Archivo = String.Empty;
        String[] Archivos;
        Int32 No = 0;
        String Extension = String.Empty;
        String Ruta = String.Empty;

        try
        {
            Archivo = (AsyncFileUpload)sender;
            Nombre_Archivo = Archivo.FileName;

            if (!String.IsNullOrEmpty(Nombre_Archivo))
            {
                Archivos = Nombre_Archivo.Split('\\');
                No = Archivos.Length;
                Ruta = Archivos[No - 1];
                Extension = Ruta.Substring(Ruta.LastIndexOf(".") + 1);

                if (Extension == "txt" || Extension == "doc" || Extension == "pdf" || Extension == "xls" || Extension == "zip" || Extension == "rar"
                    || Extension == "docx" || Extension == "jpg" || Extension == "JPG" || Extension == "jpeg" || Extension == "JPEG"
                    || Extension == "png" || Extension == "PNG" || Extension == "gif" || Extension == "GIF" || Extension == "xlsx")
                {
                    if (Archivo.FileContent.Length > 2621440)
                    {
                        return;
                    }
                }
                else
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('La extension no es valida');", true);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al validar los anexos. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : ClearSession_AsyncFileUpload
    ///DESCRIPCIÓN          : Metodo para limpiar el control de AsyncFileUpload
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void ClearSession_AsyncFileUpload(String ClientID)
    {
        HttpContext currentContext;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            currentContext = HttpContext.Current;
        }
        else
        {
            currentContext = null;
        }
        if (currentContext != null)
        {
            foreach (String Key in currentContext.Session.Keys)
            {
                if (Key.Contains(ClientID))
                {
                    currentContext.Session.Remove(Key);
                    break;
                }
            }
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Anexos
    ///DESCRIPCIÓN          : Metodo para crear las csolumnas del datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Anexos()
    {
        DataTable Dt_Anexos = new DataTable();
        try
        {
            Dt_Anexos.Columns.Add("NOMBRE", System.Type.GetType("System.String"));
            Dt_Anexos.Columns.Add("ANEXO_ID", System.Type.GetType("System.Int32"));
            Dt_Anexos.Columns.Add("RUTA_DOCUMENTO", System.Type.GetType("System.String"));
            Dt_Anexos.Columns.Add("EXTENSION", System.Type.GetType("System.String"));

            Session["Dt_Anexos"] = (DataTable)Dt_Anexos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Agregar_Fila_Dt_Anexos
    ///DESCRIPCIÓN          : Metodo para agregar los datos de los documentos al datatable
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Agregar_Fila_Dt_Anexos()
    {
        DataTable Dt_Anexos = new DataTable();
        DataRow Fila;
        Dt_Anexos = (DataTable)Session["Dt_Anexos"];

        try
        {
            Fila = Dt_Anexos.NewRow();
            Fila["NOMBRE"] = Hf_Nombre_Doc.Value.Trim();
            Fila["ANEXO_ID"] = 0;
            Fila["RUTA_DOCUMENTO"] = Hf_Ruta_Doc.Value.Trim();
            Fila["EXTENSION"] = Hf_Ruta_Doc.Value.Substring(Hf_Ruta_Doc.Value.LastIndexOf(".") + 1);
            Dt_Anexos.Rows.Add(Fila);

            Session["Dt_Anexos"] = (DataTable)Dt_Anexos;
            Obtener_Consecutivo_Doc_ID();
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Consecutivo_Doc_ID
    ///DESCRIPCIÓN          : Metodo para obtener el consecutivo del datatable de documentos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Obtener_Consecutivo_Doc_ID()
    {
        DataTable Dt_Anexos = new DataTable();
        Dt_Anexos = (DataTable)Session["Dt_Anexos"];
        int Contador;
        try
        {
            if (Dt_Anexos != null)
            {
                if (Dt_Anexos.Columns.Count > 0)
                {
                    if (Dt_Anexos.Rows.Count > 0)
                    {
                        Contador = 0;
                        foreach (DataRow Dr in Dt_Anexos.Rows)
                        {
                            Contador++;
                            Dr["ANEXO_ID"] = Contador;
                        }
                    }
                }
            }
            Session["Dt_Anexos"] = Dt_Anexos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Anexos
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los anexos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Grid_Anexos()
    {
        DataTable Dt_Anexos = new DataTable();

        try
        {
            Dt_Anexos = (DataTable)Session["Dt_Anexos"];
            if (Dt_Anexos != null)
            {
                Grid_Anexos.Columns[2].Visible = true;
                Grid_Anexos.Columns[3].Visible = true;
                Grid_Anexos.Columns[4].Visible = true;
                Grid_Anexos.Columns[5].Visible = true;
                Grid_Anexos.DataSource = Dt_Anexos;
                Grid_Anexos.DataBind();
                Grid_Anexos.Columns[2].Visible = false;
                Grid_Anexos.Columns[3].Visible = false;
                Grid_Anexos.Columns[4].Visible = false;
                if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar") || Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                {
                    Grid_Anexos.Columns[5].Visible = true;
                }
                else {
                    Grid_Anexos.Columns[5].Visible = false;
                }
            }
            else
            {
                Grid_Anexos.DataSource = new DataTable();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Carpeta_Anexos
    ///DESCRIPCIÓN          : Metodo para limpiar la carpeta de anexos que ya no existen en la base de datos
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 06/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Limpiar_Carpeta_Anexos() 
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt = new DataTable();
        String Archivo = String.Empty;
        String Ruta_Destino = String.Empty;
        String Ruta_Temporal = String.Empty;
        String[] Archivos;

        try
        {
            Negocio.P_No_Solicitud = Hf_No_Solicitud.Value.Trim();
            Dt = Negocio.Consultar_Anexos();

            if(Dt != null && Dt.Rows.Count > 0)
            {
                Ruta_Destino = Server.MapPath("Archivos/Anexos_Egresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/" + Hf_No_Solicitud.Value.Trim() + "/");
                Ruta_Temporal = Server.MapPath("Archivos/Anexos_Egresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/" + Hf_No_Solicitud.Value.Trim() + "/Otro/");

                if (!System.IO.Directory.Exists(Ruta_Temporal))
                {
                    System.IO.Directory.CreateDirectory(Ruta_Temporal);
                }

                if (System.IO.Directory.Exists(Ruta_Destino))
                {
                    //copiamos los archivos de la carpeta a una temporal
                    Archivos = System.IO.Directory.GetFiles(Ruta_Destino);
                    foreach (String Arc in Archivos)
                    {
                        Archivo = System.IO.Path.GetFileName(Arc);
                        System.IO.File.Copy(Arc, Ruta_Temporal+ Archivo, true);
                        System.IO.File.Delete(Arc);
                    }
                    
                    //copiamos a la carpeta los archivos pertenecientes al movimiento solamente, por si se elimino algun registro
                    //que no existan archivos basura en las carpetas
                    foreach (DataRow Dr in Dt.Rows)
                    {
                        Archivo = Dr["NOMBRE"].ToString().Trim() + "." + Dr["EXTENSION"].ToString().Trim();
                        if (System.IO.File.Exists(Ruta_Temporal + Archivo))
                        {
                            System.IO.File.Copy(Ruta_Temporal + Archivo, Ruta_Destino + Archivo, true);
                            System.IO.File.Delete(Ruta_Temporal + Archivo);
                        }
                    }

                    //vaciamos el directorio temporal y lo eliminamos
                    Archivos = System.IO.Directory.GetFiles(Ruta_Temporal);
                    foreach (String Arc in Archivos)
                    {
                        System.IO.File.Delete(Arc);
                    }
                    System.IO.Directory.Delete(Ruta_Temporal);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    #endregion

    #region (Validar_Programas_Ramo33)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Programas_Ramo33
    ///DESCRIPCIÓN          : consulta para validar los programas de ramo 33 y su 
    ///                       afectacion contable y bancaria
    ///PARAMETROS           :
    ///CREO                 : Leslie Gonzalez Vázquez
    ///FECHA_CREO           : 18/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected String Validar_Programas_Ramo33(DataTable Dt)
    {
        Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        String No_Poliza = String.Empty;
        String Programa_Origen = String.Empty;
        String Programa_Destino = String.Empty;
        String Cuenta_Origen = String.Empty;
        String Cuenta_Destino = String.Empty;
        String Cuenta_Origen_ID = String.Empty;
        String Cuenta_Destino_ID = String.Empty;
        Double Importe_Origen = 0.00;
        Double Importe_Destino = 0.00;
        DataTable Dt_Programas = new DataTable();
        DataTable Dt_Detalle = new DataTable();
        DataRow Fila;
        String Nom_Programa_Destino = String.Empty;
        String Nom_Programa_Origen = String.Empty;

        try
        {
            foreach (DataRow Dr in Dt.Rows)
            {
                if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                {
                    Programa_Origen = Dr["PROGRAMA_ID"].ToString().Trim();
                    Nom_Programa_Origen = Dr["CLAVE_NOM_PROGRAMA"].ToString().Trim();
                    Importe_Origen = Importe_Origen + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "")) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", ""));
                }
                else
                {
                    Programa_Destino = Dr["PROGRAMA_ID"].ToString().Trim();
                    Nom_Programa_Destino = Dr["CLAVE_NOM_PROGRAMA"].ToString().Trim();
                    Importe_Destino = Importe_Destino + Convert.ToDouble(String.IsNullOrEmpty(Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", "")) ? "0" : Dr["IMPORTE_TOTAL"].ToString().Trim().Replace(",", ""));
                }
            }
            //validamos si son programas diferentes
            if (Programa_Origen.Trim() != Programa_Destino.Trim())
            {
                //obtenermos la cuenta contable del programa origen
                Negocio.P_Programa_ID = Programa_Origen.Trim();
                Dt_Programas = Negocio.Consultar_Convenio_Programa();

                if (Dt_Programas != null)
                {
                    if (Dt_Programas.Rows.Count > 0)
                    {
                        Cuenta_Origen = Dt_Programas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
                        Cuenta_Origen_ID = Dt_Programas.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString().Trim();
                    }
                }

                //obtenemos la cuenta contable del programa destino
                Negocio.P_Programa_ID = Programa_Destino.Trim();
                Dt_Programas = Negocio.Consultar_Convenio_Programa();

                if (Dt_Programas != null)
                {
                    if (Dt_Programas.Rows.Count > 0)
                    {
                        Cuenta_Destino = Dt_Programas.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString().Trim();
                        Cuenta_Destino_ID = Dt_Programas.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString().Trim();
                    }
                }

                //generamos los datos de la poliza
                if (!String.IsNullOrEmpty(Cuenta_Origen_ID.Trim()) && !String.IsNullOrEmpty(Cuenta_Destino_ID.Trim()))
                {
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
                    Dt_Detalle.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
                    Dt_Detalle.Columns.Add("MOMENTO_FINAL", typeof(System.String));

                    

                    Fila = Dt_Detalle.NewRow();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Origen_ID.Trim();
                    Fila["CUENTA"] = Cuenta_Origen.Trim();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Concepto] = Nom_Programa_Origen;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Origen;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = String.Empty;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = String.Empty;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = String.Empty;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = String.Empty;
                    Fila["MOMENTO_INICIAL"] = String.Empty;
                    Fila["MOMENTO_FINAL"] = String.Empty;
                    Dt_Detalle.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla


                    Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = "00003";
                    Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now);
                    Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = DateTime.Now;
                    Rs_Alta_Ope_Con_Polizas.P_Concepto = "Movimiento presupuestal entre programas";
                    Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Importe_Destino;
                    Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Importe_Origen;
                    Rs_Alta_Ope_Con_Polizas.P_No_Partida = 2;
                    Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.Trim();
                    Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Detalle;
                    Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID.Trim();
                    //Rs_Alta_Ope_Con_Polizas.P_Cmmd = null;
                    string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD

                    No_Poliza = Datos_Poliza[0];
                }
            }
        }
        catch (Exception Ex)
        {
            String Mensaje = "Error al intentar consultar los programas de ramo 33. Error: [" + Ex.Message + "]"; //"Error general en la base de datos"
            throw new Exception(Mensaje);
        }
        return No_Poliza;
    }
    #endregion

    #region (Metodo Enviar Correo)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Obtener_Datos_Correo
    ///DESCRIPCIÓN          : Envia un correo a un usuario
    ///PROPIEDADES          : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    internal static void Obtener_Datos_Correo(DataTable Dt_Solicitante, DataTable Dt_Solicitud)
    {
        String Solicitande = String.Empty;
        String Coordinador_PSP = String.Empty;
        String Director = String.Empty;
        String Puesto_Solicitande = String.Empty;
        String Puesto_Coordinador_PSP = String.Empty;
        String Puesto_Director = String.Empty;
        String UR_Solicitande = String.Empty;
        String Email_Coordinador_PSP = String.Empty;
        String Email_Director = String.Empty;
        String Numero_Solicitud = String.Empty;
        String Texto_Correo = String.Empty;
        String Asunto_Correo = String.Empty;

        try
        {
            //OBTENEMOS LOS DATOS DEL SOLICITANTE Y DEL DIRECTOR DEL AREA
            if (Dt_Solicitante != null)
            {
                if (Dt_Solicitante.Rows.Count > 0)
                {
                    Solicitande = Dt_Solicitante.Rows[0]["Solicitante"].ToString().Trim();
                    Coordinador_PSP = Dt_Solicitante.Rows[0]["Coordinador_PSP"].ToString().Trim();
                    Director = Dt_Solicitante.Rows[0]["Dr"].ToString().Trim();
                    Puesto_Solicitande = Dt_Solicitante.Rows[0]["Puesto_Solicitante"].ToString().Trim();
                    Puesto_Coordinador_PSP = Dt_Solicitante.Rows[0]["Puesto_Coordinador_PSP"].ToString().Trim();
                    Puesto_Director = Dt_Solicitante.Rows[0]["Puesto_Dr"].ToString().Trim();
                    UR_Solicitande = Dt_Solicitante.Rows[0]["Ur_Solicitante"].ToString().Trim();
                    Email_Coordinador_PSP = Dt_Solicitante.Rows[0]["Email_Coordinador_PSP"].ToString().Trim();
                    Email_Director = Dt_Solicitante.Rows[0]["Email_Dr"].ToString().Trim();

                    //ENVIAMOS EL CORREO CORRESPONDIENTE AVISANDO DE LA SOLICITUD
                    Numero_Solicitud = "T" +
                        String.Format("{0:00}", Convert.ToInt32(String.IsNullOrEmpty(Dt_Solicitud.Rows[0]["No_Movimiento"].ToString().Trim()) ? "0" : Dt_Solicitud.Rows[0]["No_Movimiento"])) +
                        "-" + Dt_Solicitud.Rows[0]["No_Solicitud"].ToString().Trim() + "-" + DateTime.Now.Year;

                    Texto_Correo = "<html>" +
                             "<body> ESTIMADO(A): <b>" + Director + "</b><br /><br />" +
                                   "POR MEDIO DEL PRESENTE RECIBA UN CORDIAL SALUDO, A LA VEZ QUE LE INFORMO QUE <br />" +
                                   "HAY UNA SOLICITUD DE MOVIMIENTO DE PRESUPUESTO PENDIENTE POR REVISAR: <br />" +
                                   "LA SOLICITUD CON NO. : <b>" + Numero_Solicitud + "</b> FUE GENERADA POR EL USUARIO: <b>"
                                   + Solicitande + "</b> DE LA DEPENDENCIA DE: <b>" + UR_Solicitande + "</b> <br /><br /><br /><br />" +
                                   "SIN MÁS POR EL MOMENTO, ME DESPIDO AGRADECIENDO SU ATENCION <br />" +
                               "</body>" +
                         "</html>";

                    Asunto_Correo = "Solicitud Movimiento Presupuesto " + Numero_Solicitud.Trim();

                    Enviar_Correo(Email_Director, Email_Coordinador_PSP, Asunto_Correo, Texto_Correo);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Envio de Correo " + ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Enviar_Correo
    ///DESCRIPCIÓN          : Envia un correo a un usuario
    ///PROPIEDADES          : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    internal static void Enviar_Correo(String Para, String Copia_Para, String Asunto, String Texto_Correo)
    {
        MailMessage Correo = new MailMessage(); //obtenemos el objeto del correo
        //variables para guardar los datos de los parametros del correo
        String Puerto = String.Empty;
        String Usuario_Correo = String.Empty;
        String De = String.Empty;
        String Servidor = String.Empty;
        String Contraseña = String.Empty;

        try
        {
            Puerto = ConfigurationManager.AppSettings["Puerto_Correo"];
            Usuario_Correo = ConfigurationManager.AppSettings["Usuario_Correo"];
            De = ConfigurationManager.AppSettings["Correo_Saliente"];
            Servidor = ConfigurationManager.AppSettings["Servidor_Correo"];
            Contraseña = ConfigurationManager.AppSettings["Password_Correo"];

            if (!String.IsNullOrEmpty(Para) && !String.IsNullOrEmpty(Puerto)
                   && !String.IsNullOrEmpty(Usuario_Correo) && !String.IsNullOrEmpty(De)
                   && !String.IsNullOrEmpty(Servidor) && !String.IsNullOrEmpty(Contraseña))
            {
                Correo.To.Clear();
                Correo.To.Add(Para);
                //validamos que no venga vacio el correo al que mandaremos copia
                if (!String.IsNullOrEmpty(Copia_Para))
                {
                    Correo.CC.Add(Copia_Para);
                }

                Correo.From = new MailAddress(De, Usuario_Correo.Trim());
                Correo.Subject = Asunto.Trim();
                Correo.SubjectEncoding = System.Text.Encoding.UTF8;

                if ((!Correo.From.Equals("") || Correo.From != null) && (!Correo.To.Equals("") || Correo.To != null))
                {
                    Correo.Body = Texto_Correo;
                    Correo.BodyEncoding = System.Text.Encoding.UTF8;
                    Correo.IsBodyHtml = true;

                    SmtpClient cliente_correo = new SmtpClient();
                    cliente_correo.Port = int.Parse(Puerto);
                    cliente_correo.UseDefaultCredentials = true;
                    cliente_correo.Credentials = new System.Net.NetworkCredential(De, Contraseña);
                    cliente_correo.Host = Servidor;
                    cliente_correo.Send(Correo);
                    Correo = null;
                }
                else
                {
                    throw new Exception("No se tiene configurada una cuenta de correo, favor de notificar");
                }
            }
        }
        catch (SmtpException ex)
        {
            throw new Exception("Envio de Correo " + ex.Message, ex);
        }
    }
    #endregion
    #endregion

    #region(Eventos)
   #region(Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Habilita las cajas de texto necesarias para crear un Nuevo Movimiento
    ///             se convierte en dar alta cuando oprimimos Nuevo y dar alta  Crea un registro  
    ///                de un movimiento presupuestal en la base de datos
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Rs_Alta_Movimiento = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        DataTable Dt_Datos = new DataTable();
        DataTable Dt_Anexos = new DataTable();
        Int32 No_Solicitud;

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Mpe_Datos_Solicitud.Hide();
                Hf_No_Solicitud.Value = String.Empty;
                Limpiar_Controles("Todo");
                Limpiar_Sessiones();
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Hf_Tipo_Presupuesto.Value = "Existente";
                Llenar_Combo_Mes();
                Llenar_Combo_Programas_Origen();
                Cargar_Combo_Responsable();
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas();
                Llenar_grid_Partidas_Destino();
                Div_Grid_Movimientos.Visible = false;
                Div_Datos.Style.Add("display", "block");
                Div_Anexos.Style.Add("display", "block");
                Tabla_Meses.Visible = false;
                Tr_Tabla_Meses_Destino.Visible = false;
                Cmb_Estatus.Enabled = false;
                Div_Grid_Comentarios.Visible = false;
                Div_Partida_Destino.Visible = true;
                Tr_Capitulo_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = false;
                Div2.Visible = true;
                Div_Partidas.Visible = true;
                Tabla_Encabezado_Destino.Visible = true;
                Tabla_Encabezado_Origen.Visible = true;
                Tr_Disponible_Origen.Visible = true;
                Llenar_Combo_Mes();
                Btn_Agregar_Origen.Visible = true;
                Btn_Agregar_Destino.Visible = true;
                Cmb_Operacion.SelectedIndex = -1;
                Llenar_Combo_Operacion();
                Btn_Agregar_Doc.Enabled = true;
                AFU_Archivo.Enabled = true;
                Btn_Busqueda_URO.Enabled = true;
                Txt_Busqueda_URO.Enabled = true;
                Btn_Busqueda_URD.Enabled = true;
                Txt_Busqueda_URD.Enabled = true;

                No_Solicitud = Rs_Alta_Movimiento.Consultar_No_Solicitud();
                Hf_No_Solicitud.Value = No_Solicitud.ToString().Trim();
            }
            else
            {
                //se validaran los datos para saber si las sumas son iguales
                if (Validar_Datos())
                {
                    Hf_Estatus_Btn_Aceptar.Value = "NUEVO";
                    Lbl_Error_Busqueda.Style.Add("display", "none");
                    Img_Error_Busqueda.Style.Add("display", "none");
                    Mpe_Datos_Solicitud.Show();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }//else de validar_Datos
            }
        }

        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Movimiento_Presupuestal_Click
    ///DESCRIPCIÓN: Busca el movimiento y lo carga en el grid
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  15/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Movimientos = new DataTable();
        DataTable Dt_Mov = new DataTable();
        DateTime Fecha_Ini;
        DateTime Fecha_Fin;

        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        try
        {
            if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
            {
                if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                {
                    Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable.SelectedValue.Trim();
                }
                else
                {
                    Negocio.P_Dependencia_ID_Busqueda = String.Empty;
                }

                if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text.Trim()) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text.Trim()))
                {
                    Fecha_Fin = Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", Txt_Fecha_Final.Text.Trim()));
                    Fecha_Ini = Convert.ToDateTime(String.Format("{0:dd/MM/yyyy}", Txt_Fecha_Inicial.Text.Trim()));
                    if (Fecha_Ini.CompareTo(Fecha_Fin) <= 0)
                    {
                        Negocio.P_Fecha_Inicio = String.Format("{0:dd/MM/yyyy}", Fecha_Ini);
                        Negocio.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Fecha_Fin);
                    }
                }
                else
                {
                    Negocio.P_Fecha_Inicio = String.Empty;
                    Negocio.P_Fecha_Final = String.Empty;
                }

                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Negocio.P_Tipo_Egreso = "RAMO33";
                }
                else
                {
                    Negocio.P_Tipo_Egreso = "MUNICIPAL";
                }
                Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Negocio.P_No_Solicitud = String.Empty;
                Dt_Movimientos = Negocio.Consultar_Like_Movimiento();

                Grid_Movimiento.DataSource = new DataTable();
                Grid_Movimiento.DataBind();

                if (Dt_Movimientos != null)
                {
                    if (Dt_Movimientos.Rows.Count > 0)
                    {
                        Dt_Mov = (from Fila in Dt_Movimientos.AsEnumerable()
                                  where Fila.Field<string>("TIPO_OPERACION") != "ADECUACION"
                                  select Fila).AsDataView().ToTable();
                        Dt_Movimientos = Dt_Mov;
                        Buscar_Movimientos(Txt_Busqueda.Text.Trim(), Dt_Movimientos);
                    }
                }
            }
            else
            {
                Consulta_Movimiento();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual qye se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if ((Btn_Salir.ToolTip == "Salir") && (Div_Grid_Movimientos.Visible == true))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else if (Btn_Salir.ToolTip == "Cancelar")
            {
                Grid_Movimiento.SelectedIndex = -1;
                Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
            }
            else
            {
                Inicializa_Controles(); //Habilita los controles para la siguiente operación del usuario en el catálogo
                Grid_Movimiento.SelectedIndex = -1;
            }
            Limpiar_Controles("Todo");
            Limpiar_Sessiones();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Origen_Click
    ///DESCRIPCIÓN          : Evento del boton de agregar
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Agregar_Origen_Click(object sender, EventArgs e)
    {
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        try
        {
            Sumar_Total_Origen();
            if (Validar_Datos_Mov_Org())
            {
                if (Dt_Mov == null || Dt_Mov.Columns.Count <= 0)
                {
                    Crear_Dt_Mov();
                    Dt_Mov = (DataTable)Session["Dt_Mov"];
                }

                Agregar_Fila_Dt_Mov("Origen");
                Limpiar_Controles("Mov_Origen");
                Tabla_Meses.Visible = false;
                Llenar_Grid_Mov();
                Calcular_Total_Modificado();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al agregar un movimiento. Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Destino_Click
    ///DESCRIPCIÓN          : Evento del boton de agregar
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 14/Mayo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Agregar_Destino_Click(object sender, EventArgs e)
    {
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        try
        {
            Sumar_Total_Destino();
            if (Validar_Datos_Mov_Des())
            {
                if (Dt_Mov == null || Dt_Mov.Columns.Count <= 0)
                {
                    Crear_Dt_Mov();
                    Dt_Mov = (DataTable)Session["Dt_Mov"];
                }

                Agregar_Fila_Dt_Mov("Destino");
                Limpiar_Controles("Mov_Destino");
                Tr_Tabla_Meses_Destino.Visible = false;
                Llenar_Grid_Mov();
                Calcular_Total_Modificado();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al agregar un movimiento. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de un presupuesto de un producto
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 12/Mayo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//conexion con la capa de negocios
        DataTable Dt_Registros = new DataTable();
        ImageButton Btn_Eliminar = (ImageButton)sender;
        Int32 No_Fila = -1;
        String Id = String.Empty;
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Id = Btn_Eliminar.CommandArgument.ToString().Trim();
            Dt_Registros = (DataTable)Session["Dt_Mov"];
            if (Dt_Registros != null)
            {
                if (Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        No_Fila++;
                        if (Dr["MOVIMIENTO_ID"].ToString().Trim().Equals(Id))
                        {
                            Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByText(Dr["TIPO_OPERACION"].ToString().Trim()));
                            
                            if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                            {
                                Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue("SUPLEMENTO"));
                            }
                            

                            if (Dr["TIPO_PARTIDA"].ToString().Trim().Equals("Origen"))
                            {
                                Cmb_Unidad_Responsable_Origen.SelectedIndex = Cmb_Unidad_Responsable_Origen.Items.IndexOf(Cmb_Unidad_Responsable_Origen.Items.FindByValue(Dr["UR_ID"].ToString().Trim()));

                                if (Dr["TIPO_MOVIMIENTO"].ToString().Trim().Equals("Nueva"))
                                {
                                    Llenar_Combo_Fuente_Financiamiento_Origen();
                                }
                                else 
                                {
                                    Cargar_Combo_Financiamiento(1);
                                }

                                Cmb_Fuente_Financiamiento_Origen.SelectedIndex = Cmb_Fuente_Financiamiento_Origen.Items.IndexOf(Cmb_Fuente_Financiamiento_Origen.Items.FindByValue(Dr["FF_ID"].ToString().Trim()));
                                Txt_Enero.Text = Dr["IMPORTE_ENERO"].ToString().Trim();
                                Txt_Febrero.Text = Dr["IMPORTE_FEBRERO"].ToString().Trim();
                                Txt_Marzo.Text = Dr["IMPORTE_MARZO"].ToString().Trim();
                                Txt_Abril.Text = Dr["IMPORTE_ABRIL"].ToString().Trim();
                                Txt_Mayo.Text = Dr["IMPORTE_MAYO"].ToString().Trim();
                                Txt_Junio.Text = Dr["IMPORTE_JUNIO"].ToString().Trim();
                                Txt_Julio.Text = Dr["IMPORTE_JULIO"].ToString().Trim();
                                Txt_Agosto.Text = Dr["IMPORTE_AGOSTO"].ToString().Trim();
                                Txt_Septiembre.Text = Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim();
                                Txt_Octubre.Text = Dr["IMPORTE_OCTUBRE"].ToString().Trim();
                                Txt_Noviembre.Text = Dr["IMPORTE_NOVIEMBRE"].ToString().Trim();
                                Txt_Diciembre.Text = Dr["IMPORTE_DICIEMBRE"].ToString().Trim();

                                Tabla_Meses.Visible = true;
                                Tr_Disponible_Origen.Visible = true;

                                Negocio.P_Area_Funcional_ID = Dr["AF_ID"].ToString().Trim();
                                Negocio.P_Dependencia_ID_Busqueda = Dr["UR_ID"].ToString().Trim();
                                Negocio.P_Fuente_Financiamiento_ID = Dr["FF_ID"].ToString().Trim();
                                Negocio.P_Programa_ID = Dr["PROGRAMA_ID"].ToString().Trim();
                                Negocio.P_Partida_Especifica_ID = Dr["PARTIDA_ID"].ToString().Trim();
                                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();

                                Dt_Partidas = Negocio.Consultar_Partidas_Especificas();

                                Hf_Partida_Origen.Value = HttpUtility.HtmlDecode(Dr["CLAVE_NOM_PARTIDA"].ToString().Trim());
                                Lbl_Nombre_Partida_Destino.Text = HttpUtility.HtmlDecode(Hf_Partida_Origen.Value.Trim());
                                Hf_Programa_Origen.Value = HttpUtility.HtmlDecode(Dr["CLAVE_NOM_PROGRAMA"].ToString().Trim());
                                Hf_Partida_Origen_ID.Value = HttpUtility.HtmlDecode(Dr["PARTIDA_ID"].ToString().Trim());
                                Hf_Programa_Origen_ID.Value = HttpUtility.HtmlDecode(Dr["PROGRAMA_ID"].ToString().Trim());
                                Hf_Capitulo_Origen_ID.Value = HttpUtility.HtmlDecode(Dr["CAPITULO_ID"].ToString().Trim());

                                Cmb_Programa_Origen.SelectedIndex = Cmb_Programa_Origen.Items.IndexOf(Cmb_Programa_Origen.Items.FindByValue(Hf_Programa_Origen_ID.Value.Trim()));

                                if (Dt_Partidas != null || Dt_Partidas.Rows.Count > 0)
                                {
                                    Lbl_Disp_Ene.Text = Dt_Partidas.Rows[0]["IMPORTE_ENERO"].ToString().Trim();
                                    Lbl_Disp_Feb.Text = Dt_Partidas.Rows[0]["IMPORTE_FEBRERO"].ToString().Trim();
                                    Lbl_Disp_Mar.Text = Dt_Partidas.Rows[0]["IMPORTE_MARZO"].ToString().Trim();
                                    Lbl_Disp_Abr.Text = Dt_Partidas.Rows[0]["IMPORTE_ABRIL"].ToString().Trim();
                                    Lbl_Disp_May.Text = Dt_Partidas.Rows[0]["IMPORTE_MAYO"].ToString().Trim();
                                    Lbl_Disp_Jun.Text = Dt_Partidas.Rows[0]["IMPORTE_JUNIO"].ToString().Trim();
                                    Lbl_Disp_Jul.Text = Dt_Partidas.Rows[0]["IMPORTE_JULIO"].ToString().Trim();
                                    Lbl_Disp_Ago.Text = Dt_Partidas.Rows[0]["IMPORTE_AGOSTO"].ToString().Trim();
                                    Lbl_Disp_Sep.Text = Dt_Partidas.Rows[0]["IMPORTE_SEPTIEMBRE"].ToString().Trim();
                                    Lbl_Disp_Oct.Text = Dt_Partidas.Rows[0]["IMPORTE_OCTUBRE"].ToString().Trim();
                                    Lbl_Disp_Nov.Text = Dt_Partidas.Rows[0]["IMPORTE_NOVIEMBRE"].ToString().Trim();
                                    Lbl_Disp_Dic.Text = Dt_Partidas.Rows[0]["IMPORTE_DICIEMBRE"].ToString().Trim();

                                    Hf_Disponible.Value = Dt_Partidas.Rows[0]["DISPONIBLE"].ToString().Trim();
                                    Hf_Aprobado_Origen.Value = Dt_Partidas.Rows[0]["APROBADO"].ToString().Trim();
                                    Hf_Ampliacion_Origen.Value = Dt_Partidas.Rows[0]["AMPLIACION"].ToString().Trim();
                                    Hf_Reduccion_Origen.Value = Dt_Partidas.Rows[0]["REDUCCION"].ToString().Trim();
                                    Hf_Modificado_Origen.Value = Dt_Partidas.Rows[0]["MODIFICADO"].ToString().Trim();
                                }
                            }
                            else 
                            {
                                Cmb_Unidad_Responsable_Destino.SelectedIndex = Cmb_Unidad_Responsable_Destino.Items.IndexOf(Cmb_Unidad_Responsable_Destino.Items.FindByValue(Dr["UR_ID"].ToString().Trim()));
                                Cargar_Combo_Financiamiento(2);
                                Cmb_Fuente_Financiamiento_Destino.SelectedIndex = Cmb_Fuente_Financiamiento_Destino.Items.IndexOf(Cmb_Fuente_Financiamiento_Destino.Items.FindByValue(Dr["FF_ID"].ToString().Trim()));
                                Txt_Enero_Destino.Text = Dr["IMPORTE_ENERO"].ToString().Trim();
                                Txt_Febrero_Destino.Text = Dr["IMPORTE_FEBRERO"].ToString().Trim();
                                Txt_Marzo_Destino.Text = Dr["IMPORTE_MARZO"].ToString().Trim();
                                Txt_Abril_Destino.Text = Dr["IMPORTE_ABRIL"].ToString().Trim();
                                Txt_Mayo_Destino.Text = Dr["IMPORTE_MAYO"].ToString().Trim();
                                Txt_Junio_Destino.Text = Dr["IMPORTE_JUNIO"].ToString().Trim();
                                Txt_Julio_Destino.Text = Dr["IMPORTE_JULIO"].ToString().Trim();
                                Txt_Agosto_Destino.Text = Dr["IMPORTE_AGOSTO"].ToString().Trim();
                                Txt_Septiembre_Destino.Text = Dr["IMPORTE_SEPTIEMBRE"].ToString().Trim();
                                Txt_Octubre_Destino.Text = Dr["IMPORTE_OCTUBRE"].ToString().Trim();
                                Txt_Noviembre_Destino.Text = Dr["IMPORTE_NOVIEMBRE"].ToString().Trim();
                                Txt_Diciembre_Destino.Text = Dr["IMPORTE_DICIEMBRE"].ToString().Trim();

                                Tr_Tabla_Meses_Destino.Visible = true;
                                Tr_Disponible_Destino.Visible = true;

                                Hf_Area_Funcional_Destino_ID.Value = Dr["AF_ID"].ToString().Trim();
                                Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Destino_ID.Value.Trim();
                                Negocio.P_Dependencia_ID_Busqueda = Dr["UR_ID"].ToString().Trim();
                                Negocio.P_Fuente_Financiamiento_ID = Dr["FF_ID"].ToString().Trim();
                                Negocio.P_Programa_ID = Dr["PROGRAMA_ID"].ToString().Trim();
                                Negocio.P_Partida_Especifica_ID = Dr["PARTIDA_ID"].ToString().Trim();
                                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();

                                Dt_Partidas = Negocio.Consultar_Partidas_Especificas();

                                Hf_Partida_Destino_ID.Value = HttpUtility.HtmlDecode(Dr["PARTIDA_ID"].ToString().Trim());
                                Hf_Programa_Destino_ID.Value = HttpUtility.HtmlDecode(Dr["PROGRAMA_ID"].ToString().Trim());
                                Hf_Partida_Destino.Value = HttpUtility.HtmlDecode(Dr["CLAVE_NOM_PARTIDA"].ToString().Trim());
                                Hf_Programa_Destino.Value = HttpUtility.HtmlDecode(Dr["CLAVE_NOM_PROGRAMA"].ToString().Trim());
                                Hf_Capitulo_Destino_ID.Value = HttpUtility.HtmlDecode(Dr["CAPITULO_ID"].ToString().Trim());
                                Lbl_Nombre_Partida_Destino.Text = HttpUtility.HtmlDecode(Hf_Partida_Destino.Value.Trim());

                                Lbl_Disp_Ene_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_ENERO"].ToString().Trim();
                                Lbl_Disp_Feb_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_FEBRERO"].ToString().Trim();
                                Lbl_Disp_Mar_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_MARZO"].ToString().Trim();
                                Lbl_Disp_Abr_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_ABRIL"].ToString().Trim();
                                Lbl_Disp_May_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_MAYO"].ToString().Trim();
                                Lbl_Disp_Jun_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_JUNIO"].ToString().Trim();
                                Lbl_Disp_Jul_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_JULIO"].ToString().Trim();
                                Lbl_Disp_Ago_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_AGOSTO"].ToString().Trim();
                                Lbl_Disp_Sep_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_SEPTIEMBRE"].ToString().Trim();
                                Lbl_Disp_Oct_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_OCTUBRE"].ToString().Trim();
                                Lbl_Disp_Nov_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_NOVIEMBRE"].ToString().Trim();
                                Lbl_Disp_Dic_Destino.Text = Dt_Partidas.Rows[0]["IMPORTE_DICIEMBRE"].ToString().Trim();

                                Hf_Aprobado_Destino.Value = Dt_Partidas.Rows[0]["APROBADO"].ToString().Trim();
                                Hf_Ampliacion_Destino.Value = Dt_Partidas.Rows[0]["AMPLIACION"].ToString().Trim();
                                Hf_Reduccion_Destino.Value = Dt_Partidas.Rows[0]["REDUCCION"].ToString().Trim();
                                Hf_Modificado_Destino.Value = Dt_Partidas.Rows[0]["MODIFICADO"].ToString().Trim();
                            }

                            Sumar_Total_Destino();
                            Sumar_Total_Origen();

                            Dt_Registros.Rows.RemoveAt(No_Fila);
                            Session["Dt_Mov"] = Dt_Registros;
                            Llenar_Grid_Mov();
                            Calcular_Total_Modificado();
                            break;
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
    ///DESCRIPCIÓN          : Evento del boton modificar
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Rs_Alta_Movimiento = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        DataTable Dt_Datos = new DataTable();
        DataTable Dt_Anexos = new DataTable();
        try
        {
            switch (Btn_Modificar.ToolTip)
            {
                //Validacion para actualizar un registro y para habilitar los controles que se requieran
                case "Modificar":
                    Mpe_Datos_Solicitud.Hide();
                    Habilitar_Controles("Modificar");
                    Txt_Justificacion.Enabled = true;
                    Hf_Tipo_Presupuesto.Value = "Existente";
                    Llenar_Combo_Mes();
                    Llenar_Combo_Programas_Origen();
                    Cargar_Combo_Responsable();
                    Cargar_Combo_Financiamiento(1);
                    Llenar_grid_Partidas();
                    Div_Grid_Movimientos.Visible = false;
                    Div_Datos.Style.Add("display", "block");
                    Div_Anexos.Style.Add("display", "block");
                    Tabla_Meses.Visible = false;
                    Div_Grid_Comentarios.Visible = false;
                    Tr_Capitulo_Origen.Visible = false;
                    Div2.Visible = true;
                     Div_Partidas.Visible = true;
                    Tabla_Encabezado_Origen.Visible = true;
                    Btn_Agregar_Doc.Enabled = true;
                    AFU_Archivo.Enabled = true;
                    Btn_Busqueda_URO.Enabled = true;
                    Txt_Busqueda_URO.Enabled = true;
                    Btn_Busqueda_URD.Enabled = true;
                    Txt_Busqueda_URD.Enabled = true;
                    Btn_Agregar_Origen.Visible = true;
                    Grid_Mov_Origen.SelectedIndex = -1;
                    Llenar_Grid_Anexos();

                    if(Cmb_Operacion.SelectedItem.Text.Equals("TRASPASO"))
                    {
                        Grid_Mov_Destino.SelectedIndex = -1;
                        Btn_Agregar_Destino.Visible = true; Btn_Agregar_Destino.Visible = true;
                        Tabla_Encabezado_Destino.Visible = true;
                        Div_Partida_Destino.Visible = true;
                        Tr_Capitulo_Destino.Visible = false;
                        Tr_Tabla_Meses_Destino.Visible = false;
                        Llenar_grid_Partidas_Destino();
                        Cargar_Combo_Financiamiento(2);
                    }
                    Llenar_Grid_Mov();
                    Tr_Disponible_Origen.Visible = true;
                    break;
                case "Actualizar":
                    if (Validar_Datos())
                    {
                        Hf_Estatus_Btn_Aceptar.Value = "MODIFICAR";
                        Lbl_Error_Busqueda.Style.Add("display", "none");
                        Img_Error_Busqueda.Style.Add("display", "none");
                        Mpe_Datos_Solicitud.Show();
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }//else de validar_Datos
                    break;
            }//fin del switch
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de modificar los datos ERROR[" + ex.Message + "]");
        }
    }//fin de Modificar

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_UR_Click
    ///DESCRIPCIÓN          : Evento del boton de busqueda de UR
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Busqueda_UR_Click(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = String.Empty;

        try
        {
            Busqueda_UR(Txt_Busqueda_UR.Text.Trim(), "INICIO");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al tratar de realizar la busqueda de las unidades responsables. Error[" + ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_UR_Click
    ///DESCRIPCIÓN          : Evento del boton de busqueda de UR
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Busqueda_URO_Click(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = String.Empty;

        try
        {
            Busqueda_UR(Txt_Busqueda_URO.Text.Trim(), "ORIGEN");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al tratar de realizar la busqueda de las unidades responsables. Error[" + ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_UR_Click
    ///DESCRIPCIÓN          : Evento del boton de busqueda de UR
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 20/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Busqueda_URD_Click(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = String.Empty;

        try
        {
            Busqueda_UR(Txt_Busqueda_URD.Text.Trim(), "DESTINO");
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al tratar de realizar la busqueda de las unidades responsables. Error[" + ex.Message + "]";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Doc_Click
    ///DESCRIPCIÓN          : Evento del boton agregar los anexos al grid
    ///PARAMETROS           :    
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Agregar_Doc_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();

        DataTable Dt_Anexos = new DataTable();
        Dt_Anexos = (DataTable)Session["Dt_Anexos"];
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = String.Empty;
       
        String Ruta = String.Empty;
        String Ruta_Destino = String.Empty;
        String Extension = String.Empty;
        String Archivo = String.Empty;

        try
        {
            if (AFU_Archivo.HasFile)
            {
                Ruta_Destino = Server.MapPath("Archivos/Anexos_Egresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/"+ Hf_No_Solicitud.Value.Trim() + "/");
                Archivo = AFU_Archivo.FileName;
                Extension = Archivo.Substring(Archivo.IndexOf(".") + 1);

                if (Extension == "txt" || Extension == "doc" || Extension == "pdf" || Extension == "docx" || Extension == "jpg"
                    || Extension == "JPG" || Extension == "jpeg" || Extension == "JPEG" || Extension == "GIF" || Extension == "xls"
                    || Extension == "png" || Extension == "PNG" || Extension == "gif" || Extension == "xlsx" || Extension == "rar"
                    || Extension == "zip")
                {
                    if (!System.IO.Directory.Exists(Ruta_Destino))
                    {
                        System.IO.Directory.CreateDirectory(Ruta_Destino);
                    }

                    if (System.IO.File.Exists(Ruta_Destino + Archivo))
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Ya exixte un documento con este nombre, favor de cambiar el nombre";
                    }
                    else
                    {
                        AFU_Archivo.SaveAs(Ruta_Destino + Archivo);
                        Hf_Ruta_Doc.Value = "../Presupuestos/Archivos/Anexos_Egresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/" + Hf_No_Solicitud.Value.Trim() + "/" + Archivo.Trim();
                        Hf_Nombre_Doc.Value = Archivo.Trim().Substring(0, Archivo.IndexOf("."));

                        if (Dt_Anexos == null || Dt_Anexos.Columns.Count <= 0)
                        {
                            Crear_Dt_Anexos();
                            Dt_Anexos = (DataTable)Session["Dt_Anexos"];
                        }

                        Agregar_Fila_Dt_Anexos();
                        Llenar_Grid_Anexos();
                        //ClearSession_AsyncFileUpload(AFU_Archivo.ClientID);
                        Hf_Nombre_Doc.Value = String.Empty;
                        Hf_Ruta_Doc.Value = String.Empty;
                    }
                    
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Solo se pueden cargar archivos con terminacion: <br /> "
                        + "pdf, txt, doc, docx, xls, png, gif, jpg";
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al agregar un documento. Error[" + ex.Message + "]";
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Doc_Click
    ///DESCRIPCIÓN          : Evento del boton de eliminar un registro de los documentos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Eliminar_Doc_Click(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = String.Empty;
        DataTable Dt_Registros = new DataTable();
        ImageButton Btn_Eliminar_Doc = (ImageButton)sender;
        Int32 No_Fila = -1;
        String Id = String.Empty;
        String Archivo = String.Empty;
        String Ruta_Destino = String.Empty;

        try
        {
            Id = Btn_Eliminar_Doc.CommandArgument.ToString().Trim();
            Dt_Registros = (DataTable)Session["Dt_Anexos"];
            if (Dt_Registros != null)
            {
                if (Dt_Registros.Rows.Count > 0)
                {
                    foreach (DataRow Dr in Dt_Registros.Rows)
                    {
                        No_Fila++;
                        if (Dr["ANEXO_ID"].ToString().Trim().Equals(Id))
                        {
                            if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                            {
                                //obtenmenos los datos del archivo para eliminarlo de la carpeta
                                Ruta_Destino = Server.MapPath("Archivos/Anexos_Egresos/" + String.Format("{0:yyyy}", DateTime.Now) + "/" + Hf_No_Solicitud.Value.Trim() + "/");
                                Archivo = Dr["NOMBRE"].ToString().Trim() + "." + Dr["EXTENSION"].ToString().Trim();
                                if (System.IO.Directory.Exists(Ruta_Destino))
                                {
                                    if (System.IO.File.Exists(Ruta_Destino + Archivo))
                                    {
                                        System.IO.File.Delete(Ruta_Destino + Archivo);
                                    }
                                }
                            }

                            Dt_Registros.Rows.RemoveAt(No_Fila);
                            Session["Dt_Anexos"] = Dt_Registros;
                            Llenar_Grid_Anexos();
                            break;
                        }
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Pdf_Click
    ///DESCRIPCIÓN          : Evento del boton de pdf para generar la solicitud
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 18/Diciembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Pdf_Click(object sender, EventArgs e)
    {
        DataTable Dt_Datos = new DataTable();
        ImageButton Btn_Pdf = (ImageButton)sender;
        String Solicitud_Id = String.Empty;
        String No_Movimiento_Egr = String.Empty;
        DataRow Fila;
        String Tipo_Operacion = String.Empty;

        try
        {
            No_Movimiento_Egr = Btn_Pdf.CommandArgument.ToString().Trim();
            Solicitud_Id = Btn_Pdf.ToolTip.ToString().Trim();
            Tipo_Operacion = Btn_Pdf.CommandName.ToString().Trim();

            Llenar_Combo_Operacion();
            Cmb_Operacion.SelectedIndex = Cmb_Operacion.Items.IndexOf(Cmb_Operacion.Items.FindByValue(Tipo_Operacion.Trim()));

            Dt_Datos.Columns.Add("No_Movimiento");
            Dt_Datos.Columns.Add("No_Solicitud");

            Fila = Dt_Datos.NewRow();
            Fila["No_Movimiento"] = No_Movimiento_Egr.Trim();
            Fila["No_Solicitud"] = Solicitud_Id.Trim();
            Dt_Datos.Rows.Add(Fila);

            Crear_Reporte(Dt_Datos);
        }
        catch (Exception ex)
        {
            throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Aceptar_Click
    ///DESCRIPCIÓN          : Evento del boton de aceptar los datos de la solicitud de movimientos
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 13/Febrero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Aceptar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Rs_Alta_Movimiento = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Mov = new DataTable();
        Dt_Mov = (DataTable)Session["Dt_Mov"];
        DataTable Dt_Datos = new DataTable();
        DataTable Dt_Anexos = new DataTable();
        DataTable Dt_Solicitante = new DataTable();

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";

            if (Validar_Datos_Solicitante())
            {
                Mpe_Datos_Solicitud.Hide();

                Rs_Alta_Movimiento.P_Importe = Txt_Importe.Text.Replace(",", "").Replace("$", "");
                Rs_Alta_Movimiento.P_Total_Modificado = Txt_Importe.Text.Trim().Replace(",", "");
                Rs_Alta_Movimiento.P_Justificacion = Txt_Justificacion.Text.Trim();
                Rs_Alta_Movimiento.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                Rs_Alta_Movimiento.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Alta_Movimiento.P_Dt_Mov = Dt_Mov;
                Rs_Alta_Movimiento.P_Mes_Actual = Hf_Mes_Actual.Value.Trim();
                Rs_Alta_Movimiento.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                Rs_Alta_Movimiento.P_No_Solicitud = Hf_No_Solicitud.Value.Trim();
                //DATOS PARA CONSULTAR LOS DATOS DEL SOLICITANTE Y DIRECTOR
                Dt_Solicitante = Crear_Dt_Solicitante();
                Rs_Alta_Movimiento.P_Dt_Solicitante = Dt_Solicitante;

                Dt_Anexos = (DataTable)Session["Dt_Anexos"];
                if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0)
                {
                    Rs_Alta_Movimiento.P_Dt_Anexos = Dt_Anexos;
                }
                else
                {
                    Rs_Alta_Movimiento.P_Dt_Anexos = new DataTable();
                }

                //vemos si vamos a crear una nueva solicitud o se modificara una
                if (Hf_Estatus_Btn_Aceptar.Value.Trim().Equals("NUEVO"))
                {
                    Dt_Datos = Rs_Alta_Movimiento.Alta_Movimiento();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            String No_Poliza = String.Empty;
                            Limpiar_Carpeta_Anexos();
                            Cargar_Combo_Responsable();
                            Crear_Reporte(Dt_Datos);
                            Limpiar_Sessiones();
                            Hf_No_Solicitud.Value = String.Empty;
                            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                            if (!String.IsNullOrEmpty(No_Poliza.Trim()))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('El Alta del Movimiento Presupuestal fue Exitosa, y se genero en número de poliza: " + No_Poliza.Trim() + "');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('El Alta del Movimiento Presupuestal fue Exitosa');", true);
                            }
                            Lbl_Mensaje_Error.Visible = false;
                            Img_Error.Visible = false;
                            Lbl_Mensaje_Error.Text = String.Empty;
                        }
                        else
                        {
                            //enviamos el correo de aviso de creacion de solicitud de movimientos
                            
                            Limpiar_Carpeta_Anexos();
                            Cargar_Combo_Responsable();
                            Crear_Reporte(Dt_Datos);
                            Limpiar_Sessiones();
                            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones

                            if (Chk_Enviar_correo.Checked)
                            {
                                Obtener_Datos_Correo(Dt_Solicitante, Dt_Datos);
                            }
                        }
                    }
                    Hf_Estatus_Btn_Aceptar.Value = String.Empty;
                }
                else if (Hf_Estatus_Btn_Aceptar.Value.Trim().Equals("MODIFICAR"))
                {
                    Rs_Alta_Movimiento.P_No_Modificacion = Hf_No_Modificacion.Value.Trim();
                    Rs_Alta_Movimiento.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();

                    Dt_Datos = Rs_Alta_Movimiento.Consultar_Modif_Solicitud_Mov();

                    if (Dt_Datos != null && Dt_Datos.Rows.Count > 0)
                    {
                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            Limpiar_Carpeta_Anexos();
                            Limpiar_Sessiones();
                            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Movimiento Presupuestal", "alert('El Alta del Movimiento Presupuestal fue Exitosa');", true);
                        }
                        else
                        {
                            Limpiar_Carpeta_Anexos();
                            Cargar_Combo_Responsable();
                            Crear_Reporte(Dt_Datos);
                            Limpiar_Sessiones();
                            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                            if (Chk_Enviar_correo.Checked)
                            {
                                Obtener_Datos_Correo(Dt_Solicitante, Dt_Datos);
                            }
                        }
                    }
                    Hf_No_Solicitud.Value = String.Empty;
                    Hf_Estatus_Btn_Aceptar.Value = String.Empty;
                }
            }
            else
            {
                Lbl_Error_Busqueda.Style.Add("display", "block");
                Img_Error_Busqueda.Style.Add("display", "block");
                Mpe_Datos_Solicitud.Show();
            }//else de validar_Datos
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }

    }

    #endregion

    #region(Combos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Responsable
    ///DESCRIPCIÓN: Cargara todos los responsables dentro del combo 
    ///             (Proviene del metodo Inicializa_Controles())
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  14/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para adecuarlo al presupuesto aprobado
    ///*******************************************************************************
    protected void Cargar_Combo_Responsable()
    {
        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Mov_Negocio_Ing = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();
        Boolean Administrador;
        DataTable Dt_Ramo33 = new DataTable();

        Hf_Tipo_Usuario.Value = "";
        Limpiar_Controles("Mov_Origen");
        Limpiar_Controles("Mov_Destino");
        Hf_Area_Funcional_Origen_ID.Value = String.Empty;
        Hf_Area_Funcional_Destino_ID.Value = String.Empty;

        try
        {
            //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
            Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
            Administrador = Negocio.Consultar_Rol();

            if (Administrador)
            {
                Cmb_Unidad_Responsable_Origen.Enabled = true;
                Cmb_Unidad_Responsable_Destino.Enabled = true;
                Cmb_Unidad_Responsable.Enabled = true;

                Txt_Busqueda_UR.Visible = true;
                Btn_Busqueda_UR.Visible = true;
                Txt_Busqueda_URO.Visible = true;
                Btn_Busqueda_URO.Visible = true;
                Txt_Busqueda_URD.Visible = true;
                Btn_Busqueda_URD.Visible = true;

                Hf_Tipo_Usuario.Value = "Administrador";
                Cmb_Unidad_Responsable.SelectedIndex = -1;
                Tr_Programa_Origen.Visible = true;
                Tr_Programa_Destino.Visible = true;
            }
            else
            {
                //verificamos si el usuario logueado es el administrador de ramo33
                Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                {
                    if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                        || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                    {
                        Cmb_Unidad_Responsable_Origen.Enabled = true;
                        Cmb_Unidad_Responsable_Destino.Enabled = true;
                        Cmb_Unidad_Responsable.Enabled = true;
                        Txt_Busqueda_UR.Visible = true;
                        Btn_Busqueda_UR.Visible = true;
                        Txt_Busqueda_URO.Visible = true;
                        Btn_Busqueda_URO.Visible = true;
                        Txt_Busqueda_URD.Visible = true;
                        Btn_Busqueda_URD.Visible = true;
                        Hf_Tipo_Usuario.Value = "Ramo33";
                        Cmb_Unidad_Responsable.SelectedIndex = -1;
                        Tr_Programa_Origen.Visible = true;
                        Tr_Programa_Destino.Visible = true;
                    }
                    else
                    {
                        Cmb_Unidad_Responsable_Origen.Enabled = false;
                        Cmb_Unidad_Responsable_Destino.Enabled = false;
                        Cmb_Unidad_Responsable.Enabled = false;
                        Txt_Busqueda_UR.Visible = false;
                        Btn_Busqueda_UR.Visible = false;
                        Txt_Busqueda_URO.Visible = false;
                        Btn_Busqueda_URO.Visible = false;
                        Txt_Busqueda_URD.Visible = false;
                        Btn_Busqueda_URD.Visible = false;
                        Hf_Tipo_Usuario.Value = "Usuario";
                        Tr_Programa_Origen.Visible = false;
                        Tr_Programa_Destino.Visible = false;
                    }
                }
                else
                {
                    Cmb_Unidad_Responsable_Origen.Enabled = false;
                    Cmb_Unidad_Responsable_Destino.Enabled = false;
                    Cmb_Unidad_Responsable.Enabled = false;
                    Txt_Busqueda_UR.Visible = false;
                    Btn_Busqueda_UR.Visible = false;
                    Txt_Busqueda_URO.Visible = false;
                    Btn_Busqueda_URO.Visible = false;
                    Txt_Busqueda_URD.Visible = false;
                    Btn_Busqueda_URD.Visible = false;
                    Hf_Tipo_Usuario.Value = "Usuario";
                    Tr_Programa_Origen.Visible = false;
                    Tr_Programa_Destino.Visible = false;
                }
            }

            if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Mov_Negocio_Ing.P_Tipo_Usuario = "Administrador";
                Dt_Responsable = Mov_Negocio_Ing.Consultar_URs();
            }
            else
            {
                Negocio.P_Empleado_Id = Cls_Sessiones.Empleado_ID.Trim();
                Negocio.P_Rol_Id = Cls_Sessiones.Rol_ID.Trim();
                Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();

                Dt_Responsable = Negocio.Consultar_UR();
            }

            Cmb_Unidad_Responsable_Origen.Items.Clear();
            Cmb_Unidad_Responsable_Origen.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            //para destino
            Cmb_Unidad_Responsable_Destino.Items.Clear();
            Cmb_Unidad_Responsable_Destino.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            //para la inicial
            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Unidad_Responsable.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable.DataTextField = "Nombre";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            if (!Hf_Tipo_Usuario.Value.Trim().Equals("Administrador") && !Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
            {
                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));
            }
            Cmb_Unidad_Responsable_Origen.SelectedIndex = Cmb_Unidad_Responsable_Origen.Items.IndexOf(Cmb_Unidad_Responsable_Origen.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));
            Cmb_Unidad_Responsable_Destino.SelectedIndex = Cmb_Unidad_Responsable_Destino.Items.IndexOf(Cmb_Unidad_Responsable_Destino.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado.Trim()));

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
                if (Dt_Area_Funcional != null)
                {
                    if (Dt_Area_Funcional.Rows.Count > 0)
                    {
                        Hf_Area_Funcional_Origen_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    }
                }

                Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
                if (Dt_Area_Funcional != null)
                {
                    if (Dt_Area_Funcional.Rows.Count > 0)
                    {
                        Hf_Area_Funcional_Destino_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Combo_Financiamiento
    ///DESCRIPCIÓN: Cargara todos las fuentes de financiamiento dentro del combo 
    ///PARAMETROS: int Parametros.- Sirve para saber cual es el combo que va a cargar
    ///             si el de origen o destino
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  07/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO:  02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: Para filtar las fuente de financiamiento de acuerdo a la dependencia
    ///*******************************************************************************
    protected void Cargar_Combo_Financiamiento(int Operacion)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Financinamiento = new DataTable();
        try
        {
            switch (Operacion)
            {
                case 1:
                    Cmb_Fuente_Financiamiento_Origen.Items.Clear();
                    if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
                    {
                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            Negocio_Inv.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Negocio_Inv.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                            Dt_Financinamiento = Negocio_Inv.Consultar_Fuente_Financiamiento();
                        }
                        else
                        {
                            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                            Dt_Financinamiento = Negocio.Consultar_Fuente_Financiamiento();
                        }

                        Cmb_Fuente_Financiamiento_Origen.DataSource = Dt_Financinamiento;
                        Cmb_Fuente_Financiamiento_Origen.DataValueField = "id";
                        Cmb_Fuente_Financiamiento_Origen.DataTextField = "nombre";
                        Cmb_Fuente_Financiamiento_Origen.DataBind();
                        Cmb_Fuente_Financiamiento_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                    if (Dt_Financinamiento.Rows.Count > 0)
                    {
                        Cmb_Fuente_Financiamiento_Origen.SelectedIndex = 1;
                        Cmb_Fuente_Financiamiento_Origen.Enabled = true;
                        Llenar_grid_Partidas();
                    }

                    break;

                case 2:
                    Cmb_Fuente_Financiamiento_Destino.Items.Clear();
                    if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
                    {

                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                        {
                            Negocio_Inv.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Negocio_Inv.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                            Dt_Financinamiento = Negocio_Inv.Consultar_Fuente_Financiamiento();
                        }
                        else
                        {
                            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                            Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
                            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
                            Dt_Financinamiento = Negocio.Consultar_Fuente_Financiamiento();
                        }

                        Cmb_Fuente_Financiamiento_Destino.DataSource = Dt_Financinamiento;
                        Cmb_Fuente_Financiamiento_Destino.DataValueField = "id";
                        Cmb_Fuente_Financiamiento_Destino.DataTextField = "nombre";
                        Cmb_Fuente_Financiamiento_Destino.DataBind();
                        Cmb_Fuente_Financiamiento_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    }
                    if (Dt_Financinamiento.Rows.Count > 0)
                    {
                        Cmb_Fuente_Financiamiento_Destino.SelectedIndex = 1;
                        Cmb_Fuente_Financiamiento_Destino.Enabled = true;
                        Llenar_grid_Partidas_Destino();
                    }

                    break;
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  08/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para hacer la funcionalidad correcta del combo
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            Hf_Programa_Origen.Value = "";
            Hf_Partida_Origen.Value = "";
            Hf_Disponible.Value = "0.00";

            if (Cmb_Unidad_Responsable_Origen.SelectedIndex > 0)
            {
                Tabla_Meses.Visible = false;
                Hf_Disponible.Value = "0.00";
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
                {
                    Sumar_Total_Destino();
                    Llenar_grid_Partidas();
                }
                else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    //Llenar_Combo_Programas_Origen();
                }
                else 
                {
                    Llenar_grid_Partidas();
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Selecciona una Unidad Responsable";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  08/Noviembre/2011
    ///MODIFICO:    Leslie Gonzalez Vazquez
    ///FECHA_MODIFICO: 02/Marzo/2012
    ///CAUSA_MODIFICACIÓN: para hacer la funcionalidad correcta del combo
    ///*******************************************************************************
    protected void Cmb_Fuente_Financiamiento_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            Hf_Programa_Destino.Value = "";
            Hf_Partida_Destino.Value = "";

            if (Cmb_Unidad_Responsable_Destino.SelectedIndex > 0)
            {
                Llenar_grid_Partidas_Destino();
                Tr_Tabla_Meses_Destino.Visible = false;
                Sumar_Total_Origen();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Selecciona una Unidad Responsable Destino";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Unidad_Responsable_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: 
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Area_Funcional = new DataTable();
        try
        {
            Limpiar_Controles("Mov_Origen");
            Hf_Area_Funcional_Origen_ID.Value = String.Empty;

            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
            Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
            if (Dt_Area_Funcional != null)
            {
                if (Dt_Area_Funcional.Rows.Count > 0)
                {
                    Hf_Area_Funcional_Origen_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    Cmb_Programa_Origen.SelectedIndex = Cmb_Programa_Origen.Items.IndexOf(Cmb_Programa_Origen.Items.FindByValue(Dt_Area_Funcional.Rows[0][Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID].ToString().Trim()));
                }
            }

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
            {
                Llenar_Combo_Fuente_Financiamiento_Origen();
            }
            else
            {
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
                {
                    Sumar_Total_Destino();
                }

                Cargar_Combo_Financiamiento(1);
                Llenar_grid_Partidas();
                Tabla_Meses.Visible = false;
                Hf_Disponible.Value = "0.00";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Unidad_Responsable_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: 
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  05/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Clasificacion_Gasto_Negocio Negocio = new Cls_Ope_Psp_Clasificacion_Gasto_Negocio(); //conexion con la capa de negocios
        DataTable Dt_Area_Funcional = new DataTable();
        Limpiar_Controles("Mov_Destino");
        Hf_Area_Funcional_Destino_ID.Value = String.Empty;

        try
        {
            Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable_Destino.SelectedItem.Value.Trim();
            Dt_Area_Funcional = Negocio.Obtener_Area_Funcional();
            if (Dt_Area_Funcional != null)
            {
                if (Dt_Area_Funcional.Rows.Count > 0)
                {
                    Hf_Area_Funcional_Destino_ID.Value = Dt_Area_Funcional.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                    Cmb_Programa_Destino.SelectedIndex = Cmb_Programa_Destino.Items.IndexOf(Cmb_Programa_Destino.Items.FindByValue(Dt_Area_Funcional.Rows[0][Cat_SAP_Det_Prog_Dependencia.Campo_Proyecto_Programa_ID].ToString().Trim()));
                }
            }

            Sumar_Total_Origen();

            Cargar_Combo_Financiamiento(2);
            Llenar_grid_Partidas_Destino();
            Tr_Tabla_Meses_Destino.Visible = false;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Operacion_SelectedIndexChanged
    ///DESCRIPCIÓN: evento para mostrar u ocultar los datos de las partidas destino
    ///PARAMETROS: 
    ///CREO:        Leslie Gonzalez Vazquez
    ///FECHA_CREO:  08/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Operacion_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        try
        {
            Limpiar_Controles("Todo");
            Limpiar_Sessiones();

            Div_Partida_Destino.Visible = false;
            Tr_Capitulo_Destino.Visible = false;
            Tr_Capitulo_Origen.Visible = false;
            Tr_Tabla_Meses_Destino.Visible = false;
            Tabla_Meses.Visible = false;

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("TRASPASO"))
            {
                Div_Partida_Destino.Visible = true;
                Sumar_Total_Origen();
                Sumar_Total_Destino();
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas_Destino();
                Llenar_grid_Partidas();
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("REDUCCION") || Cmb_Operacion.SelectedItem.Value.Trim().Equals("AMPLIACION"))
            {
                Sumar_Total_Origen();

                Hf_Tipo_Presupuesto.Value = "Existente";

                Cargar_Combo_Responsable();
                
                Cargar_Combo_Financiamiento(1);
                Cargar_Combo_Financiamiento(2);
                Llenar_grid_Partidas_Destino();
                Llenar_grid_Partidas();
            }
            else if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
            {
                Tr_Capitulo_Origen.Visible = true;

                if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                {
                    Llenar_Combo_UR_Origen_Inv();
                }
                else
                {
                    Cargar_Combo_Responsable();
                }

                Llenar_Combo_Fuente_Financiamiento_Origen();
                Llenar_Combo_Programas_Origen();
                Llenar_Combo_Capitulos_Origen();
                Grid_Partidas.DataSource = new DataTable();
                Grid_Partidas.DataBind();

                Tr_Capitulo_Destino.Visible = false;
                Tr_Capitulo_Origen.Visible = true;
                Hf_Tipo_Presupuesto.Value = "Nuevo";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programas_Origen
    ///DESCRIPCIÓN          : Llena el combo de programas de origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Programas_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt_Programas = new DataTable();

        Cmb_Programa_Origen.Items.Clear(); //limpiamos el combo

        Negocio.P_Programa_ID = String.Empty;
        Dt_Programas = Negocio.Consultar_Programa_Unidades_Responsables();

       Cmb_Programa_Origen.DataSource = Dt_Programas;
        Cmb_Programa_Origen.DataTextField = "NOMBRE";
        Cmb_Programa_Origen.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
        Cmb_Programa_Origen.DataBind();
        Cmb_Programa_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

        Cmb_Programa_Destino.DataSource = Dt_Programas;
        Cmb_Programa_Destino.DataTextField = "NOMBRE";
        Cmb_Programa_Destino.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
        Cmb_Programa_Destino.DataBind();
        Cmb_Programa_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Unificar_Programas
    ///DESCRIPCIÓN          : Metodo para unificar los programas
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 28/Noviembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private DataTable Unificar_Programas(DataTable Dt_Programas)
    {
        DataTable Dt = new DataTable();
        DataTable Dt_Unificado = new DataTable();
        DataTable Dt_Psp = new DataTable();
        DataTable Dt_PP = new DataTable();
        DataRow Fila;
        Boolean Repetido = false;

        Dt = Dt_Programas;

        try
        {
            if (Dt_Programas != null)
            {
                if (Dt_Programas.Rows.Count > 0)
                {
                    Dt_PP = (from Fila_PP in Dt_Programas.AsEnumerable()
                             where Fila_PP.Field<string>("TIPO") == "PROGRAMA"
                             select Fila_PP).AsDataView().ToTable();

                    Dt_Psp = (from Fila_Psp in Dt_Programas.AsEnumerable()
                              where Fila_Psp.Field<string>("TIPO") == "PSP"
                              select Fila_Psp).AsDataView().ToTable();

                    if (Dt_PP != null && Dt_Psp != null)
                    {
                        if (Dt_PP.Rows.Count > 0 && Dt_Psp.Rows.Count > 0)
                        {
                            Dt_Unificado = Dt_PP;

                            foreach (DataRow Dr_Psp in Dt_Psp.Rows)
                            {
                                Repetido = false;
                                foreach (DataRow Dr in Dt_Unificado.Rows) 
                                {
                                    if (Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim().Equals(Dr_Psp["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                                    {
                                        Repetido = true;
                                        break;
                                    }
                                }

                                if (!Repetido)
                                {
                                    Fila = Dt_Unificado.NewRow();
                                    Fila[0] = Dr_Psp["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                                    Fila[1] = Dr_Psp["NOMBRE"].ToString().Trim();
                                    Fila[2] = 0;
                                    Fila[3] = "PROGRAMA";
                                    Dt_Unificado.Rows.Add(Fila);
                                }
                            }

                            Dt = Dt_Unificado;
                        }
                    }
                }
            }
        }
        catch (Exception)
        {
            
            throw;
        }
        return Dt;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos_Origen
    ///DESCRIPCIÓN          : Llena el combo de capitulos de origen
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Capitulos_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();

        Cmb_Capitulo_Origen.Items.Clear(); //limpiamos el combo
        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
        {
            Cmb_Capitulo_Origen.DataSource = Negocio_Inv.Obtener_Capitulos();
        }
        else
        {
            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
            Cmb_Capitulo_Origen.DataSource = Negocio.Obtener_Capitulos();
        }
        Cmb_Capitulo_Origen.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo_Origen.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
        Cmb_Capitulo_Origen.DataBind();
        Cmb_Capitulo_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Capitulos_Destino
    ///DESCRIPCIÓN          : Llena el combo de capitulos de Destino
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Capitulos_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();

        Cmb_Capitulo_Destino.Items.Clear(); //limpiamos el combo
        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
        {
            Cmb_Capitulo_Destino.DataSource = Negocio_Inv.Obtener_Capitulos();
        }
        else
        {
            Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();
            Cmb_Capitulo_Destino.DataSource = Negocio.Obtener_Capitulos();
        }
        Cmb_Capitulo_Destino.DataTextField = "CLAVE_NOMBRE";
        Cmb_Capitulo_Destino.DataValueField = Cat_SAP_Capitulos.Campo_Capitulo_ID;
        Cmb_Capitulo_Destino.DataBind();
        Cmb_Capitulo_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Capitulos origen
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Capitulo_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Origen = new DataTable();
        DataRow Fila;
        try
        {
            Dt_Partidas_Origen.Columns.Add("nombre");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("APROBADO");
            Dt_Partidas_Origen.Columns.Add("DISPONIBLE");
            Dt_Partidas_Origen.Columns.Add("AMPLIACION");
            Dt_Partidas_Origen.Columns.Add("REDUCCION");
            Dt_Partidas_Origen.Columns.Add("MODIFICADO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ENERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_FEBRERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MARZO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ABRIL");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MAYO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JUNIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JULIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_AGOSTO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_SEPTIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_OCTUBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_NOVIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_DICIEMBRE");
            Dt_Partidas_Origen.Columns.Add("Partida_id");
            Dt_Partidas_Origen.Columns.Add("CAPITULO_ID");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA_ID");
            Dt_Partidas_Origen.Columns.Add("FF_ID");
            Dt_Partidas_Origen.Columns.Add("AF_ID");
            Dt_Partidas_Origen.Columns.Add("UR_ID");

            Grid_Partidas.DataSource = new DataTable();
            Hf_Capitulo_Origen_ID.Value = "";

            if (Cmb_Capitulo_Origen.SelectedIndex > 0)
            {
                Hf_Capitulo_Origen_ID.Value = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                //Calendarizar_Negocio.P_Busqueda = String.Empty;
                Calendarizar_Negocio.P_Partida_ID = String.Empty;
                Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partidas.Rows)
                        {
                            Fila = Dt_Partidas_Origen.NewRow();
                            Fila["nombre"] = Dr["NOMBRE"].ToString().Trim();
                            Fila["PROGRAMA"] = "";
                            Fila["APROBADO"] = "0.00";
                            Fila["DISPONIBLE"] = "0.00";
                            Fila["AMPLIACION"] = "0.00";
                            Fila["REDUCCION"] = "0.00";
                            Fila["MODIFICADO"] = "0.00";
                            Fila["IMPORTE_ENERO"] = "0.00";
                            Fila["IMPORTE_FEBRERO"] = "0.00";
                            Fila["IMPORTE_MARZO"] = "0.00";
                            Fila["IMPORTE_ABRIL"] = "0.00";
                            Fila["IMPORTE_MAYO"] = "0.00";
                            Fila["IMPORTE_JUNIO"] = "0.00";
                            Fila["IMPORTE_JULIO"] = "0.00";
                            Fila["IMPORTE_AGOSTO"] = "0.00";
                            Fila["IMPORTE_SEPTIEMBRE"] = "0.00";
                            Fila["IMPORTE_OCTUBRE"] = "0.00";
                            Fila["IMPORTE_NOVIEMBRE"] = "0.00";
                            Fila["IMPORTE_DICIEMBRE"] = "0.00";
                            Fila["Partida_id"] = Dr["PARTIDA_ID"].ToString().Trim();
                            Fila["CAPITULO_ID"] = Cmb_Capitulo_Origen.SelectedItem.Value.Trim();
                            Fila["PROGRAMA_ID"] = "";
                            Fila["FF_ID"] = "";
                            Fila["AF_ID"] = "";
                            Fila["UR_ID"] = "";
                            Dt_Partidas_Origen.Rows.Add(Fila);
                        }

                        Grid_Partidas.DataBind();
                        Grid_Partidas.Columns[0].Visible = true;
                        Grid_Partidas.Columns[5].Visible = true;
                        Grid_Partidas.Columns[6].Visible = true;
                        Grid_Partidas.Columns[7].Visible = true;
                        Grid_Partidas.Columns[8].Visible = true;
                        Grid_Partidas.Columns[9].Visible = true;
                        Grid_Partidas.Columns[10].Visible = true;
                        Grid_Partidas.Columns[11].Visible = true;
                        Grid_Partidas.Columns[12].Visible = true;
                        Grid_Partidas.Columns[13].Visible = true;
                        Grid_Partidas.Columns[14].Visible = true;
                        Grid_Partidas.Columns[15].Visible = true;
                        Grid_Partidas.Columns[16].Visible = true;
                        Grid_Partidas.Columns[17].Visible = true;
                        Grid_Partidas.Columns[18].Visible = true;
                        Grid_Partidas.Columns[19].Visible = true;
                        Grid_Partidas.Columns[20].Visible = true;
                        Grid_Partidas.Columns[21].Visible = true;
                        Grid_Partidas.Columns[23].Visible = true;
                        Grid_Partidas.Columns[22].Visible = true;
                        Grid_Partidas.Columns[24].Visible = true;
                        Grid_Partidas.Columns[25].Visible = true;
                        Grid_Partidas.DataSource = Dt_Partidas_Origen;
                        Grid_Partidas.DataBind();
                        Grid_Partidas.Columns[5].Visible = false;
                        Grid_Partidas.Columns[6].Visible = false;
                        Grid_Partidas.Columns[7].Visible = false;
                        Grid_Partidas.Columns[8].Visible = false;
                        Grid_Partidas.Columns[9].Visible = false;
                        Grid_Partidas.Columns[10].Visible = false;
                        Grid_Partidas.Columns[11].Visible = false;
                        Grid_Partidas.Columns[12].Visible = false;
                        Grid_Partidas.Columns[13].Visible = false;
                        Grid_Partidas.Columns[14].Visible = false;
                        Grid_Partidas.Columns[15].Visible = false;
                        Grid_Partidas.Columns[16].Visible = false;
                        Grid_Partidas.Columns[17].Visible = false;
                        Grid_Partidas.Columns[18].Visible = false;
                        Grid_Partidas.Columns[19].Visible = false;
                        Grid_Partidas.Columns[20].Visible = false;
                        Grid_Partidas.Columns[21].Visible = false;
                        Grid_Partidas.Columns[22].Visible = false;
                        Grid_Partidas.Columns[23].Visible = false;
                        Grid_Partidas.Columns[24].Visible = false;
                        Grid_Partidas.Columns[25].Visible = false;
                        Div_Partidas.Visible = true;
                        Grid_Partidas.SelectedIndex = -1;

                    }
                }
            }
            if (Cmb_Capitulo_Origen.SelectedItem.Text.Trim().Substring(0, Cmb_Capitulo_Origen.SelectedItem.Text.Trim().IndexOf(" ")).Equals("1000"))
            {
                if (!Hf_Tipo_Usuario.Value.Trim().Equals("Administrador"))
                {
                    Grid_Partidas.DataSource = new DataTable();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Capitulo_Destinon_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Capitulos destino
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Capitulo_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio Calendarizar_Negocio = new Cls_Ope_Psp_Calendarizar_Presupuesto_Negocio();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Origen = new DataTable();
        DataRow Fila;
        Hf_Capitulo_Destino_ID.Value = "";

        try
        {
            Dt_Partidas_Origen.Columns.Add("nombre");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA");
            Dt_Partidas_Origen.Columns.Add("APROBADO");
            Dt_Partidas_Origen.Columns.Add("DISPONIBLE");
            Dt_Partidas_Origen.Columns.Add("AMPLIACION");
            Dt_Partidas_Origen.Columns.Add("REDUCCION");
            Dt_Partidas_Origen.Columns.Add("MODIFICADO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ENERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_FEBRERO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MARZO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_ABRIL");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_MAYO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JUNIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_JULIO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_AGOSTO");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_SEPTIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_OCTUBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_NOVIEMBRE");
            Dt_Partidas_Origen.Columns.Add("IMPORTE_DICIEMBRE");
            Dt_Partidas_Origen.Columns.Add("Partida_id");
            Dt_Partidas_Origen.Columns.Add("CAPITULO_ID");
            Dt_Partidas_Origen.Columns.Add("PROGRAMA_ID");
            Dt_Partidas_Origen.Columns.Add("FF_ID");
            Dt_Partidas_Origen.Columns.Add("AF_ID");
            Dt_Partidas_Origen.Columns.Add("UR_ID");

            Grid_Partidas_Destino.DataSource = new DataTable();

            if (Cmb_Capitulo_Destino.SelectedIndex > 0)
            {
                Hf_Capitulo_Destino_ID.Value = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                Calendarizar_Negocio.P_Capitulo_ID = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                //Calendarizar_Negocio.P_Busqueda = String.Empty;
                Calendarizar_Negocio.P_Partida_ID = String.Empty;
                Dt_Partidas = Calendarizar_Negocio.Consultar_Partidas();
                if (Dt_Partidas != null)
                {
                    if (Dt_Partidas.Rows.Count > 0)
                    {
                        foreach (DataRow Dr in Dt_Partidas.Rows)
                        {
                            Fila = Dt_Partidas_Origen.NewRow();
                            Fila["nombre"] = Dr["NOMBRE"].ToString().Trim();
                            Fila["PROGRAMA"] = "";
                            Fila["APROBADO"] = "0.00";
                            Fila["DISPONIBLE"] = "0.00";
                            Fila["AMPLIACION"] = "0.00";
                            Fila["REDUCCION"] = "0.00";
                            Fila["MODIFICADO"] = "0.00";
                            Fila["IMPORTE_ENERO"] = "0.00";
                            Fila["IMPORTE_FEBRERO"] = "0.00";
                            Fila["IMPORTE_MARZO"] = "0.00";
                            Fila["IMPORTE_ABRIL"] = "0.00";
                            Fila["IMPORTE_MAYO"] = "0.00";
                            Fila["IMPORTE_JUNIO"] = "0.00";
                            Fila["IMPORTE_JULIO"] = "0.00";
                            Fila["IMPORTE_AGOSTO"] = "0.00";
                            Fila["IMPORTE_SEPTIEMBRE"] = "0.00";
                            Fila["IMPORTE_OCTUBRE"] = "0.00";
                            Fila["IMPORTE_NOVIEMBRE"] = "0.00";
                            Fila["IMPORTE_DICIEMBRE"] = "0.00";
                            Fila["Partida_id"] = Dr["PARTIDA_ID"].ToString().Trim();
                            Fila["CAPITULO_ID"] = Cmb_Capitulo_Destino.SelectedItem.Value.Trim();
                            Fila["PROGRAMA_ID"] = "";
                            Fila["FF_ID"] = "";
                            Fila["AF_ID"] = "";
                            Fila["UR_ID"] = "";
                            Dt_Partidas_Origen.Rows.Add(Fila);
                        }

                        Grid_Partidas_Destino.DataBind();
                        Grid_Partidas_Destino.Columns[0].Visible = true;
                        Grid_Partidas_Destino.Columns[5].Visible = true;
                        Grid_Partidas_Destino.Columns[6].Visible = true;
                        Grid_Partidas_Destino.Columns[7].Visible = true;
                        Grid_Partidas_Destino.Columns[8].Visible = true;
                        Grid_Partidas_Destino.Columns[9].Visible = true;
                        Grid_Partidas_Destino.Columns[10].Visible = true;
                        Grid_Partidas_Destino.Columns[11].Visible = true;
                        Grid_Partidas_Destino.Columns[12].Visible = true;
                        Grid_Partidas_Destino.Columns[13].Visible = true;
                        Grid_Partidas_Destino.Columns[14].Visible = true;
                        Grid_Partidas_Destino.Columns[15].Visible = true;
                        Grid_Partidas_Destino.Columns[16].Visible = true;
                        Grid_Partidas_Destino.Columns[17].Visible = true;
                        Grid_Partidas_Destino.Columns[18].Visible = true;
                        Grid_Partidas_Destino.Columns[19].Visible = true;
                        Grid_Partidas_Destino.Columns[20].Visible = true;
                        Grid_Partidas_Destino.Columns[21].Visible = true;
                        Grid_Partidas_Destino.Columns[22].Visible = true;
                        Grid_Partidas_Destino.Columns[23].Visible = true;
                        Grid_Partidas_Destino.Columns[24].Visible = true;
                        Grid_Partidas_Destino.Columns[25].Visible = true;
                        Grid_Partidas_Destino.DataSource = Dt_Partidas_Origen;
                        Grid_Partidas_Destino.DataBind();
                        Grid_Partidas_Destino.Columns[5].Visible = false;
                        Grid_Partidas_Destino.Columns[6].Visible = false;
                        Grid_Partidas_Destino.Columns[7].Visible = false;
                        Grid_Partidas_Destino.Columns[8].Visible = false;
                        Grid_Partidas_Destino.Columns[9].Visible = false;
                        Grid_Partidas_Destino.Columns[10].Visible = false;
                        Grid_Partidas_Destino.Columns[11].Visible = false;
                        Grid_Partidas_Destino.Columns[12].Visible = false;
                        Grid_Partidas_Destino.Columns[13].Visible = false;
                        Grid_Partidas_Destino.Columns[14].Visible = false;
                        Grid_Partidas_Destino.Columns[15].Visible = false;
                        Grid_Partidas_Destino.Columns[16].Visible = false;
                        Grid_Partidas_Destino.Columns[17].Visible = false;
                        Grid_Partidas_Destino.Columns[18].Visible = false;
                        Grid_Partidas_Destino.Columns[19].Visible = false;
                        Grid_Partidas_Destino.Columns[20].Visible = false;
                        Grid_Partidas_Destino.Columns[21].Visible = false;
                        Grid_Partidas_Destino.Columns[22].Visible = false;
                        Grid_Partidas_Destino.Columns[23].Visible = false;
                        Grid_Partidas_Destino.Columns[24].Visible = false;
                        Grid_Partidas_Destino.Columns[25].Visible = false;
                        Div_Partidas.Visible = true;
                        Grid_Partidas_Destino.SelectedIndex = -1;

                    }
                }
                if (Cmb_Capitulo_Destino.SelectedItem.Text.Trim().Substring(0, Cmb_Capitulo_Destino.SelectedItem.Text.Trim().IndexOf(" ")).Equals("1000"))
                {
                    if (!Hf_Tipo_Usuario.Value.Trim().Equals("Administrador"))
                    {
                        Grid_Partidas_Destino.DataSource = new DataTable();
                    }
                }
            }
            Sumar_Total_Origen();
            Sumar_Total_Destino();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de capitulos Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Fuente_Financiamiento_Origen()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Parametros_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//Instancia con la clase de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Fuentes = new DataTable();


        Cmb_Fuente_Financiamiento_Origen.Items.Clear(); //limpiamos el combo
        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
        {
            Dt_Fuentes = Negocio_Inv.Consultar_Fuente_Financiamiento_Ramo33();
            
        }
        else
        {
            Dt_Fuentes = Parametros_Negocio.Consultar_Fuente_Financiamiento_Ramo33();
        }

        Cmb_Fuente_Financiamiento_Origen.DataSource = Dt_Fuentes;
        Cmb_Fuente_Financiamiento_Origen.DataTextField = "NOMBRE";
        Cmb_Fuente_Financiamiento_Origen.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
        Cmb_Fuente_Financiamiento_Origen.DataBind();
        Cmb_Fuente_Financiamiento_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

        if (Dt_Fuentes != null)
        {
            if (Dt_Fuentes.Rows.Count > 0)
            {
                Cmb_Fuente_Financiamiento_Origen.SelectedIndex = 1;
            }
            else 
            {
                Cmb_Fuente_Financiamiento_Origen.SelectedIndex = -1;
            }
        }
        else 
        {
            Cmb_Fuente_Financiamiento_Origen.SelectedIndex = -1;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Llenar_Combo_Fuente_Financiamiento_Destino()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Parametros_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//Instancia con la clase de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        DataTable Dt_Fuentes = new DataTable();

        Cmb_Fuente_Financiamiento_Destino.Items.Clear(); //limpiamos el combo
        if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
        {
            Dt_Fuentes = Negocio_Inv.Consultar_Fuente_Financiamiento_Ramo33();
        }
        else
        {
            Dt_Fuentes = Parametros_Negocio.Consultar_Fuente_Financiamiento_Ramo33();
        }

        Cmb_Fuente_Financiamiento_Destino.DataSource = Dt_Fuentes;
        Cmb_Fuente_Financiamiento_Destino.DataTextField = "NOMBRE";
        Cmb_Fuente_Financiamiento_Destino.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
        Cmb_Fuente_Financiamiento_Destino.DataBind();
        Cmb_Fuente_Financiamiento_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

        if (Dt_Fuentes != null)
        {
            if (Dt_Fuentes.Rows.Count > 0)
            {
                Cmb_Fuente_Financiamiento_Destino.SelectedIndex = 1;
            }
            else
            {
                Cmb_Fuente_Financiamiento_Destino.SelectedIndex = -1;
            }
        }
        else
        {
            Cmb_Fuente_Financiamiento_Destino.SelectedIndex = -1;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Llenar_Combo_UR_Destino_Inv()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocios = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos(); //conexion con la capa de negocios
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();
        Hf_Area_Funcional_Destino_ID.Value = "";
        try
        {

            Negocios.P_Tipo_Usuario = "Todos";
            Dt_Responsable = Negocios.Consultar_URs();

            //para destino
            Cmb_Unidad_Responsable_Destino.Items.Clear();
            Cmb_Unidad_Responsable_Destino.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Fuente_Financiamiento
    ///DESCRIPCIÓN          : Llena el combo de fuente de financiamiento
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 11/Noviembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Llenar_Combo_UR_Origen_Inv()
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocios = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos(); //conexion con la capa de negocios
        DataTable Dt_Responsable = new DataTable();
        DataTable Dt_Area_Funcional = new DataTable();

        Hf_Area_Funcional_Origen_ID.Value = "";
        try
        {

            Negocios.P_Tipo_Usuario = "Todos";
            Dt_Responsable = Negocios.Consultar_URs();

            //para destino
            Cmb_Unidad_Responsable_Origen.Items.Clear();
            Cmb_Unidad_Responsable_Origen.DataSource = Dt_Responsable;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataTextField = "Nombre";
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Programas origen
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Programa_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Hf_Importe_Programa.Value = "";

        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt = new DataTable();
        try
        {
            Hf_Importe_Programa.Value = "0";
            Negocio.P_Programa_ID = Cmb_Programa_Origen.SelectedItem.Value.Trim();
            Negocio.P_Dependencia_ID_Busqueda = String.Empty;
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
            {
                Dt = Negocio.Consultar_Ur_Programas();
            }
            else 
            {
                Dt = Negocio.Consultar_Ur_Programas_Psp();
            }

            Cmb_Unidad_Responsable_Origen.Items.Clear();
            Cmb_Unidad_Responsable_Origen.DataSource = Dt;
            Cmb_Unidad_Responsable_Origen.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Origen.DataTextField = "UR";
            Cmb_Unidad_Responsable_Origen.DataBind();
            Cmb_Unidad_Responsable_Origen.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            
            Llenar_grid_Partidas();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de programas Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de Programas
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Hf_Importe_Programa.Value = "";

        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt = new DataTable();
        try
        {
            Hf_Importe_Programa.Value = "0";
            Negocio.P_Programa_ID = Cmb_Programa_Origen.SelectedItem.Value.Trim();
            Negocio.P_Dependencia_ID_Busqueda = String.Empty;
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

            Dt = Negocio.Consultar_Ur_Programas_Psp();

            Cmb_Unidad_Responsable.Items.Clear();
            Cmb_Unidad_Responsable.DataSource = Dt;
            Cmb_Unidad_Responsable.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable.DataTextField = "UR";
            Cmb_Unidad_Responsable.DataBind();
            Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            Llenar_grid_Partidas();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de programas Error[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_Destino_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del combo de programas destino
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Marzo/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///******************************************************************************* 
    protected void Cmb_Programa_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Hf_Importe_Programa.Value = "";

        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
        DataTable Dt = new DataTable();
        try
        {
            Hf_Importe_Programa.Value = "0";
            Negocio.P_Programa_ID = Cmb_Programa_Destino.SelectedItem.Value.Trim();
            Negocio.P_Dependencia_ID_Busqueda = String.Empty;
            Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

            if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
            {
                Dt = Negocio.Consultar_Ur_Programas();
            }
            else
            {
                Dt = Negocio.Consultar_Ur_Programas_Psp();
            }

            Cmb_Unidad_Responsable_Destino.Items.Clear();
            Cmb_Unidad_Responsable_Destino.DataSource = Dt;
            Cmb_Unidad_Responsable_Destino.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
            Cmb_Unidad_Responsable_Destino.DataTextField = "UR";
            Cmb_Unidad_Responsable_Destino.DataBind();
            Cmb_Unidad_Responsable_Destino.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));

            Llenar_grid_Partidas_Destino();
        }
        catch (Exception ex)
        {
            throw new Exception("Error en el evento del combo de programas Error[" + ex.Message + "]");
        }
    }
    #endregion

    #region (Grid)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Movimiento_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los movimientos seleccionada por el usuario
    /// CREO        : Hugo Enrique Ramirez Aguilera
    /// FECHA_CREO  : 21-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Movimiento_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Rs_Consulta = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//Variable de conexión hacia la capa de Negocios
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Partidas_Detalles = new DataTable();
        DataTable Dt_Movimiento;//Variable que obtendra los datos de la consulta 
        Hf_No_Solicitud.Value = String.Empty;
        Hf_No_Modificacion.Value = String.Empty;
        DataTable Dt_Estatus_Mov = new DataTable();
        DataTable Dt_Anexos = new DataTable();

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Limpiar_Controles("Todo"); //Limpia los controles del la forma para poder agregar los valores del registro seleccionado

            Llenar_Combo_Operacion();
            Cmb_Operacion.SelectedValue = Grid_Movimiento.SelectedRow.Cells[2].Text;
            Txt_Importe.Text = Grid_Movimiento.SelectedRow.Cells[3].Text;
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Grid_Movimiento.SelectedRow.Cells[4].Text));

            Rs_Consulta.P_No_Solicitud = Grid_Movimiento.SelectedRow.Cells[1].Text;
            Dt_Estatus_Mov = Rs_Consulta.Consultar_Estatus_Modificacion();

            if (Grid_Movimiento.SelectedRow.Cells[4].Text.Trim().Equals("GENERADO") || Grid_Movimiento.SelectedRow.Cells[4].Text.Trim().Equals("CANCELADO"))
            {
                if (Dt_Estatus_Mov != null && Dt_Estatus_Mov.Rows.Count > 0) 
                {
                    if (Dt_Estatus_Mov.Rows[0]["ESTATUS"].ToString().Trim().Equals("GENERADO")) 
                    {
                        Btn_Modificar.Visible = true;
                        Hf_No_Modificacion.Value = Dt_Estatus_Mov.Rows[0]["NO_MODIFICACION"].ToString().Trim();
                    }
                }
            }
            else if (Grid_Movimiento.SelectedRow.Cells[4].Text.Trim().Equals("PREAUTORIZADO"))
            {
                
                if (Dt_Estatus_Mov != null && Dt_Estatus_Mov.Rows.Count > 0)
                {
                    if (Dt_Estatus_Mov.Rows[0]["ESTATUS"].ToString().Trim().Equals("PREAUTORIZADO"))
                    {
                        if (Hf_Tipo_Usuario.Value.Trim().Equals("Administrador"))
                        {
                            Btn_Modificar.Visible = true;
                            Hf_No_Modificacion.Value = Dt_Estatus_Mov.Rows[0]["NO_MODIFICACION"].ToString().Trim();
                        }
                        else
                        {
                            Btn_Modificar.Visible = false;
                            Hf_No_Modificacion.Value = Dt_Estatus_Mov.Rows[0]["NO_MODIFICACION"].ToString().Trim();
                        }
                    }
                }
            }
            else 
            {
                Btn_Modificar.Visible = false;
            }

            Txt_Justificacion.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[7].Text);

            Rs_Consulta.P_No_Solicitud = Grid_Movimiento.SelectedRow.Cells[1].Text;
            Hf_No_Solicitud.Value = Grid_Movimiento.SelectedRow.Cells[1].Text;
            Dt_Movimiento = Rs_Consulta.Consulta_Movimiento();//Consulta todos los datos de los movimientos que fue seleccionada por el usuario
            Dt_Anexos = Rs_Consulta.Consultar_Anexos();

            Div_Grid_Movimientos.Visible = false;
            Div_Datos.Style.Add("display", "block");
            Div_Anexos.Style.Add("display", "block");

            if (!String.IsNullOrEmpty(HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[7].Text)))
            {
                Txt_Comentario.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[8].Text);
                Txt_Fecha_Autorizo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[9].Text);
                Txt_Autorizo.Text = HttpUtility.HtmlDecode(Grid_Movimiento.SelectedRow.Cells[10].Text);
            }

            if (Dt_Movimiento.Rows.Count > 0)
            {
                Session["Dt_Mov"] = Dt_Movimiento;
                Llenar_Grid_Mov();
                Sumar_Total_Destino();
                Sumar_Total_Origen();

                Cmb_Unidad_Responsable_Origen.Enabled = false;
                Cmb_Fuente_Financiamiento_Origen.Enabled = false;
                Tr_Capitulo_Origen.Visible = false;
                Tabla_Meses.Visible = false;
                Div_Movimientos_Origen.Visible = true;
                Div_Partidas.Visible = false;
                Btn_Agregar_Origen.Visible = false;
                Tabla_Encabezado_Origen.Visible = false;
                Calcular_Total_Modificado();
                Grid_Mov_Origen.SelectedIndex = -1;
                Grid_Mov_Destino.SelectedIndex = -1;

                if (Cmb_Operacion.SelectedItem.Text.Equals("TRASPASO"))
                {
                    Div_Partida_Destino.Visible = true;
                    Cmb_Unidad_Responsable_Destino.Enabled = false;
                    Tr_Capitulo_Destino.Visible = false;
                    Tr_Tabla_Meses_Destino.Visible = false;
                    Div2.Visible = false;
                    Div_Mov_Destino.Visible = true;
                    Btn_Agregar_Destino.Visible = false;
                    Tabla_Encabezado_Destino.Visible = false;
                    Calcular_Total_Modificado();
                }
                else
                {
                    Div_Partida_Destino.Visible = false;
                }
            }

            Btn_Agregar_Doc.Enabled = false;
            AFU_Archivo.Enabled = false;
            Btn_Busqueda_URO.Enabled = false;
            Txt_Busqueda_URO.Enabled = false;
            Btn_Busqueda_URD.Enabled = false;
            Txt_Busqueda_URD.Enabled = false;
            if (Dt_Anexos != null && Dt_Anexos.Rows.Count > 0) 
            {
                Session["Dt_Anexos"] = Dt_Anexos;
                Llenar_Grid_Anexos();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    /// ********************************************************************************
    /// NOMBRE: Grid_Movimiento_Sorting
    /// 
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// 
    /// CREÓ:   Hugo Enrique Ramirez Aguilera
    /// FECHA CREÓ: 21-Octubre-2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// **********************************************************************************
    protected void Grid_Movimiento_Sorting(object sender, GridViewSortEventArgs e)
    {
        //Se consultan los movimientos que actualmente se encuentran registradas en el sistema.
        Consulta_Movimiento();
        DataTable Dt_Movimiento_Presupuestal = (Grid_Movimiento.DataSource as DataTable);

        if (Dt_Movimiento_Presupuestal != null)
        {
            DataView Dv_Movimiento_Presupuestal = new DataView(Dt_Movimiento_Presupuestal);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Movimiento_Presupuestal.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }
            Grid_Movimiento.Columns[7].Visible = true;
            Grid_Movimiento.Columns[8].Visible = true;
            Grid_Movimiento.Columns[9].Visible = true;
            Grid_Movimiento.Columns[10].Visible = true;
            Grid_Movimiento.Columns[11].Visible = true;
            Grid_Movimiento.DataSource = Dv_Movimiento_Presupuestal;
            Grid_Movimiento.DataBind();
            Grid_Movimiento.Columns[7].Visible = false;
            Grid_Movimiento.Columns[8].Visible = false;
            Grid_Movimiento.Columns[9].Visible = false;
            Grid_Movimiento.Columns[10].Visible = false;
            Grid_Movimiento.Columns[11].Visible = false;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Mov_Origen_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 24/mayo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Mov_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Limpiar_Controles("Mov_Origen");

        try
        {
            if (Grid_Mov_Origen.SelectedIndex > -1)
            {
                Cmb_Unidad_Responsable_Origen.SelectedIndex = Cmb_Unidad_Responsable_Origen.Items.IndexOf(Cmb_Unidad_Responsable_Origen.Items.FindByValue(Grid_Mov_Origen.SelectedRow.Cells[31].Text.Trim()));
                Llenar_Combo_Fuente_Financiamiento_Origen();
                Cmb_Fuente_Financiamiento_Origen.SelectedIndex = Cmb_Fuente_Financiamiento_Origen.Items.IndexOf(Cmb_Fuente_Financiamiento_Origen.Items.FindByValue(Grid_Mov_Origen.SelectedRow.Cells[29].Text.Trim()));
                Lbl_Nombre_Partida_Origen.Text = HttpUtility.HtmlDecode(Grid_Mov_Origen.SelectedRow.Cells[1].Text);

                Txt_Enero.Text = Grid_Mov_Origen.SelectedRow.Cells[9].Text.Trim();
                Txt_Febrero.Text = Grid_Mov_Origen.SelectedRow.Cells[10].Text.Trim();
                Txt_Marzo.Text = Grid_Mov_Origen.SelectedRow.Cells[11].Text.Trim();
                Txt_Abril.Text = Grid_Mov_Origen.SelectedRow.Cells[12].Text.Trim();
                Txt_Mayo.Text = Grid_Mov_Origen.SelectedRow.Cells[13].Text.Trim();
                Txt_Junio.Text = Grid_Mov_Origen.SelectedRow.Cells[14].Text.Trim();
                Txt_Julio.Text = Grid_Mov_Origen.SelectedRow.Cells[15].Text.Trim();
                Txt_Agosto.Text = Grid_Mov_Origen.SelectedRow.Cells[16].Text.Trim();
                Txt_Septiembre.Text = Grid_Mov_Origen.SelectedRow.Cells[17].Text.Trim();
                Txt_Octubre.Text = Grid_Mov_Origen.SelectedRow.Cells[18].Text.Trim();
                Txt_Noviembre.Text = Grid_Mov_Origen.SelectedRow.Cells[19].Text.Trim();
                Txt_Diciembre.Text = Grid_Mov_Origen.SelectedRow.Cells[20].Text.Trim();

                Tabla_Meses.Visible = true;
                Tr_Disponible_Origen.Visible = false;

                Txt_Enero.Enabled = false;
                Txt_Febrero.Enabled = false;
                Txt_Marzo.Enabled = false;
                Txt_Abril.Enabled = false;
                Txt_Mayo.Enabled = false;
                Txt_Junio.Enabled = false;
                Txt_Julio.Enabled = false;
                Txt_Agosto.Enabled = false;
                Txt_Septiembre.Enabled = false;
                Txt_Octubre.Enabled = false;
                Txt_Noviembre.Enabled = false;
                Txt_Diciembre.Enabled = false;
                Sumar_Total_Destino();
                Sumar_Total_Origen();

                Txt_Justificacion.Text = HttpUtility.HtmlDecode(Grid_Mov_Origen.SelectedRow.Cells[35].Text);
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Mov_Origen_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 24/mayo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Mov_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Limpiar_Controles("Mov_Destino");

        try
        {
            if (Grid_Mov_Destino.SelectedIndex > -1)
            {
                Cmb_Unidad_Responsable_Destino.SelectedIndex = Cmb_Unidad_Responsable_Destino.Items.IndexOf(Cmb_Unidad_Responsable_Destino.Items.FindByValue(Grid_Mov_Destino.SelectedRow.Cells[31].Text.Trim()));
                Llenar_Combo_Fuente_Financiamiento_Destino();
                Cmb_Fuente_Financiamiento_Destino.SelectedIndex = Cmb_Fuente_Financiamiento_Destino.Items.IndexOf(Cmb_Fuente_Financiamiento_Destino.Items.FindByValue(Grid_Mov_Destino.SelectedRow.Cells[29].Text.Trim()));
                Lbl_Nombre_Partida_Destino.Text = HttpUtility.HtmlDecode(Grid_Mov_Destino.SelectedRow.Cells[1].Text);

                Txt_Enero_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[9].Text.Trim();
                Txt_Febrero_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[10].Text.Trim();
                Txt_Marzo_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[11].Text.Trim();
                Txt_Abril_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[12].Text.Trim();
                Txt_Mayo_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[13].Text.Trim();
                Txt_Junio_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[14].Text.Trim();
                Txt_Julio_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[15].Text.Trim();
                Txt_Agosto_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[16].Text.Trim();
                Txt_Septiembre_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[17].Text.Trim();
                Txt_Octubre_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[18].Text.Trim();
                Txt_Noviembre_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[19].Text.Trim();
                Txt_Diciembre_Destino.Text = Grid_Mov_Destino.SelectedRow.Cells[20].Text.Trim();

                Tr_Tabla_Meses_Destino.Visible = true;
                Tr_Disponible_Destino.Visible = false;

                Txt_Enero_Destino.Enabled = false;
                Txt_Febrero_Destino.Enabled = false;
                Txt_Marzo_Destino.Enabled = false;
                Txt_Abril_Destino.Enabled = false;
                Txt_Mayo_Destino.Enabled = false;
                Txt_Junio_Destino.Enabled = false;
                Txt_Julio_Destino.Enabled = false;
                Txt_Agosto_Destino.Enabled = false;
                Txt_Septiembre_Destino.Enabled = false;
                Txt_Octubre_Destino.Enabled = false;
                Txt_Noviembre_Destino.Enabled = false;
                Txt_Diciembre_Destino.Enabled = false;

                Sumar_Total_Destino();
                Sumar_Total_Origen();

                Txt_Justificacion.Text = HttpUtility.HtmlDecode(Grid_Mov_Destino.SelectedRow.Cells[35].Text);
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Partidas_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 05/marzo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Partidas_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//conexion con la capa de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        String Codigo_Presupuesto = String.Empty;
        String Mes = String.Empty;
        DataTable Dt_Partidas = new DataTable();
        Boolean Repetido = false;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Limpiar_Controles("Mov_Origen");

        try
        {
            if (Grid_Partidas.SelectedIndex > -1)
            {
                if (Cmb_Operacion.SelectedItem.Value.Trim().Equals("SUPLEMENTO"))
                {
                    if (Cmb_Programa_Origen.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar un programa";
                        Grid_Partidas.SelectedIndex = -1;
                        return;
                    }
                    if (Cmb_Fuente_Financiamiento_Origen.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar una fuente de financiamiento";
                        Grid_Partidas.SelectedIndex = -1;
                        return;
                    }
                    if (Cmb_Unidad_Responsable_Origen.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Favor de seleccionar una unidad responsable";
                        Grid_Partidas.SelectedIndex = -1;
                        return;
                    }

                    if (Hf_Tipo_Usuario.Value.Trim().Equals("Ramo33"))
                    {
                        Negocio_Inv.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();
                        Negocio_Inv.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                        Negocio_Inv.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                        Negocio_Inv.P_Programa_ID = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                        Negocio_Inv.P_Partida_Especifica_ID = Grid_Partidas.SelectedRow.Cells[20].Text.Trim();
                        Negocio_Inv.P_Anio = String.Format("{0:yyyy}", DateTime.Now);

                        Dt_Partidas = Negocio_Inv.Consultar_Partidas_Especificas();
                    }
                    else
                    {
                        Negocio.P_Area_Funcional_ID = Hf_Area_Funcional_Origen_ID.Value.Trim();
                        Negocio.P_Dependencia_ID_Busqueda = Cmb_Unidad_Responsable_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Fuente_Financiamiento_ID = Cmb_Fuente_Financiamiento_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Programa_ID = Cmb_Programa_Origen.SelectedItem.Value.Trim();
                        Negocio.P_Partida_Especifica_ID = Grid_Partidas.SelectedRow.Cells[20].Text.Trim();
                        Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                        Negocio.P_Tipo_Usuario = Hf_Tipo_Usuario.Value.Trim();

                        Dt_Partidas = Negocio.Consultar_Partidas_Especificas();
                    }

                    if (Dt_Partidas != null)
                    {
                        if (Dt_Partidas.Rows.Count > 0)
                        {
                            Repetido = true;
                        }
                    }
                }

                if (!Repetido)
                {
                    Tabla_Meses.Visible = true;

                    Llenar_Combo_Mes();

                    Hf_Partida_Origen.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[1].Text.Trim());
                    Lbl_Nombre_Partida_Origen.Text = HttpUtility.HtmlDecode(Hf_Partida_Origen.Value.Trim());
                    Hf_Programa_Origen.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[2].Text.Trim());
                    Hf_Partida_Origen_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[20].Text.Trim());
                    Hf_Programa_Origen_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[22].Text.Trim());
                    Hf_Capitulo_Origen_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas.SelectedRow.Cells[21].Text.Trim());

                    Lbl_Disp_Ene.Text = Grid_Partidas.SelectedRow.Cells[8].Text.Trim();
                    Lbl_Disp_Feb.Text = Grid_Partidas.SelectedRow.Cells[9].Text.Trim();
                    Lbl_Disp_Mar.Text = Grid_Partidas.SelectedRow.Cells[10].Text.Trim();
                    Lbl_Disp_Abr.Text = Grid_Partidas.SelectedRow.Cells[11].Text.Trim();
                    Lbl_Disp_May.Text = Grid_Partidas.SelectedRow.Cells[12].Text.Trim();
                    Lbl_Disp_Jun.Text = Grid_Partidas.SelectedRow.Cells[13].Text.Trim();
                    Lbl_Disp_Jul.Text = Grid_Partidas.SelectedRow.Cells[14].Text.Trim();
                    Lbl_Disp_Ago.Text = Grid_Partidas.SelectedRow.Cells[15].Text.Trim();
                    Lbl_Disp_Sep.Text = Grid_Partidas.SelectedRow.Cells[16].Text.Trim();
                    Lbl_Disp_Oct.Text = Grid_Partidas.SelectedRow.Cells[17].Text.Trim();
                    Lbl_Disp_Nov.Text = Grid_Partidas.SelectedRow.Cells[18].Text.Trim();
                    Lbl_Disp_Dic.Text = Grid_Partidas.SelectedRow.Cells[19].Text.Trim();

                    Hf_Disponible.Value = Grid_Partidas.SelectedRow.Cells[4].Text.Trim();
                    Hf_Aprobado_Origen.Value = Grid_Partidas.SelectedRow.Cells[7].Text.Trim();
                    Hf_Ampliacion_Origen.Value = Grid_Partidas.SelectedRow.Cells[5].Text.Trim();
                    Hf_Reduccion_Origen.Value = Grid_Partidas.SelectedRow.Cells[6].Text.Trim();
                    Hf_Modificado_Origen.Value = Grid_Partidas.SelectedRow.Cells[3].Text.Trim();

                    Sumar_Total_Destino();
                    Sumar_Total_Origen();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Favor de seleccionar otra partida pues esta si existe en el presupuesto";
                    Grid_Partidas.SelectedIndex = -1;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION : Grid_Partidas_Destino_SelectedIndexChanged
    /// DESCRIPCION          : 
    /// CREO                 : Leslie Gonzalez
    /// FECHA_CREO           : 08/marzo/2012
    /// MODIFICO             :
    /// FECHA_MODIFICO       : 
    /// CAUSA_MODIFICACION   :
    ///*******************************************************************************
    protected void Grid_Partidas_Destino_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();//conexion con la capa de negocios
        Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos Negocio_Inv = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio_Ingresos();
        String Mes = String.Empty;
        DataTable Dt_Partidas = new DataTable();
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        Limpiar_Controles("Mov_Destino");

        try
        {
            if (Grid_Partidas_Destino.SelectedIndex > -1)
            {
                Tr_Tabla_Meses_Destino.Visible = true;
                Tr_Disponible_Destino.Visible = true;
                Llenar_Combo_Mes();

                Hf_Partida_Destino_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[20].Text.Trim());
                Hf_Programa_Destino_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[22].Text.Trim());
                Hf_Partida_Destino.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[1].Text.Trim());
                Lbl_Nombre_Partida_Destino.Text = HttpUtility.HtmlDecode(Hf_Partida_Destino.Value.Trim());
                Hf_Programa_Destino.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[2].Text.Trim());
                Hf_Capitulo_Destino_ID.Value = HttpUtility.HtmlDecode(Grid_Partidas_Destino.SelectedRow.Cells[21].Text.Trim());

                Lbl_Disp_Ene_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[8].Text.Trim();
                Lbl_Disp_Feb_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[9].Text.Trim();
                Lbl_Disp_Mar_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[10].Text.Trim();
                Lbl_Disp_Abr_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[11].Text.Trim();
                Lbl_Disp_May_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[12].Text.Trim();
                Lbl_Disp_Jun_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[13].Text.Trim();
                Lbl_Disp_Jul_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[14].Text.Trim();
                Lbl_Disp_Ago_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[15].Text.Trim();
                Lbl_Disp_Sep_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[16].Text.Trim();
                Lbl_Disp_Oct_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[17].Text.Trim();
                Lbl_Disp_Nov_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[18].Text.Trim();
                Lbl_Disp_Dic_Destino.Text = Grid_Partidas_Destino.SelectedRow.Cells[19].Text.Trim();

                Hf_Aprobado_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[7].Text.Trim();
                Hf_Ampliacion_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[5].Text.Trim();
                Hf_Reduccion_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[6].Text.Trim();
                Hf_Modificado_Destino.Value = Grid_Partidas_Destino.SelectedRow.Cells[3].Text.Trim();

                Sumar_Total_Destino();
                Sumar_Total_Origen();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Anexos_RowDataBound
    ///DESCRIPCIÓN          : Evento del grid de los anexos
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 05/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Anexos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            HyperLink Hyp_Lnk_Ruta;

            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "../Contabilidad/Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion
    #endregion
}

