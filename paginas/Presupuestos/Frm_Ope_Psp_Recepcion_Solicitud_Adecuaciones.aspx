﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Recepcion_Solicitud_Adecuaciones.aspx.cs" 
Inherits="paginas_Presupuestos_Frm_Ope_Psp_Recepcion_Solicitud_Adecuaciones" Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" >
        function Aceptacion_Masiva(Chk)
        {
            var $chkBox = $("input:checkbox[id$=Chk_Aceptar]");
            if($(Chk).attr('checked')){
                $chkBox.attr("checked", true);
            }
            else{
                $chkBox.attr("checked", false);
            }
        }
        
        function Grid_Anidado(Control, Fila)
        {
            var div = document.getElementById(Control); 
            var img = document.getElementById('img' + Control);
            
            if (div.style.display == "none") 
            {
                div.style.display = "inline";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                else {
                    img.src = "../imagenes/paginas/stocks_indicator_down.png";
                }
                img.alt = "Contraer Registros";
            }
            else 
            {
                div.style.display = "none";
                if (Fila == 'alt') {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                else {
                    img.src = "../imagenes/paginas/add_up.png";
                }
                img.alt = "Expandir Registros";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<asp:ScriptManager ID="ScriptManager1" runat="server"  EnableScriptGlobalization="true" EnableScriptLocalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div id="div_progress" class="processMessage" >
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
             
                
             <div id="Div_Fuentes_Financiamiento" style="background-color:#ffffff; width:100%; height:100%;">
                    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="4" class="label_titulo">
                          Recepción de Solicitud de Adecuaciones de Presupuesto
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
               </table>             
            
               <table width="100%"  border="0" cellspacing="0">
                 <tr align="center">
                     <td colspan="2">                
                         <div  align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                              <table style="width:100%;height:28px;">
                                <tr>
                                  <td align="left" style="width:59%;">  
                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="1"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                  </td>
                                  <td align="right" style="width:41%;">
                                    <table style="width: 100%; height: 28px;">
                                        <tr>
                                            <td style="vertical-align: middle; text-align: right; width: 20%;">
                                                B&uacute;squeda:
                                            </td>
                                            <td style="width: 55%;">
                                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px" MaxLength="100"></asp:TextBox>
                                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                    WatermarkText="<Ingrese Filtro>" TargetControlID="Txt_Busqueda" />
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. " />
                                            </td>
                                            <td style="vertical-align: middle; width: 5%;">
                                                <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                    OnClick="Btn_Buscar_Click" />
                                            </td>
                                        </tr>
                                    </table>
                                   </td>
                                 </tr>
                              </table>
                        </div>
                     </td>
                 </tr>
             </table> 
             <br />
            <center>
             <div id="Div_Grid_Movimientos" runat="server" style="overflow:auto;height:500px;width:98%;vertical-align:top;">
                    <asp:HiddenField id="Hf_Tipo_Usuario" runat="server"/>
                    <asp:HiddenField id="Hf_No_Mov" runat="server"/>
                    <table width="100%">
                        <tr id="Tr_Chk_Autorizar" runat="server">
                            <td style="text-align:right;">
                                <asp:CheckBox ID="Chk_Aceptacion" runat="server" OnClick="javascript:Aceptacion_Masiva(this);"/>
                                <asp:Label id="Lbl_Aceptar" runat="server" Text="Seleccionar Todo"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:GridView ID="Grid_Movimientos" runat="server" style="white-space:normal;"
                                    AutoGenerateColumns="False" GridLines="None" 
                                    Width="100%"  EmptyDataText="No se encontraron registros" 
                                    CssClass="GridView_1" HeaderStyle-CssClass="tblHead"
                                    DataKeyNames="SOLICITUD_ID" OnRowDataBound="Grid_Movimientos_RowDataBound">
                                        <Columns>
                                            <asp:TemplateField> 
                                                  <ItemTemplate> 
                                                        <a href="javascript:Grid_Anidado('div<%# Eval("SOLICITUD_ID") %>', 'one');"> 
                                                             <img id="imgdiv<%# Eval("SOLICITUD_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                        </a> 
                                                  </ItemTemplate> 
                                                  <AlternatingItemTemplate> 
                                                       <a href="javascript:Grid_Anidado('div<%# Eval("SOLICITUD_ID") %>', 'alt');"> 
                                                            <img id="imgdiv<%# Eval("SOLICITUD_ID") %>" alt="Click expander/contraer registros" border="0" src="../imagenes/paginas/stocks_indicator_down.png" /> 
                                                       </a> 
                                                  </AlternatingItemTemplate> 
                                                  <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="3%" BackColor="CadetBlue" ForeColor="BlanchedAlmond" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="SOLICITUD_ID" />
                                            <asp:BoundField DataField="NO_SOLICITUD" HeaderText="No. Solicitud" SortExpression="NO_SOLICITUD" >
                                                <HeaderStyle HorizontalAlign="Left" Width="14%" Font-Size="7pt" />
                                                <ItemStyle HorizontalAlign="Left" Width="14%" Font-Size="7.5pt" BackColor="CadetBlue" ForeColor="BlanchedAlmond" Font-Bold="true" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_OPERACION" />
                                            <asp:BoundField DataField="IMPORTE" HeaderText="Importe"  SortExpression="IMPORTE" DataFormatString="{0:n}">
                                                <HeaderStyle HorizontalAlign="Right" Width="17%" Font-Size="7pt"  />
                                                <ItemStyle HorizontalAlign="Right" Width="17%" Font-Size="7.5pt" BackColor="CadetBlue" ForeColor="BlanchedAlmond" Font-Bold="true" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" />
                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" SortExpression="FECHA_CREO" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss}">
                                                <HeaderStyle HorizontalAlign="Center" Width="16%" Font-Size="7pt" />
                                                <ItemStyle HorizontalAlign="Center" Width="16%" Font-Size="7.5pt" BackColor="CadetBlue" ForeColor="BlanchedAlmond" Font-Bold="true" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario" SortExpression="USUARIO_CREO">
                                                <HeaderStyle HorizontalAlign="Center" Width="42%" Font-Size="7pt" />
                                                <ItemStyle HorizontalAlign="Center" Width="42%" Font-Size="7.5pt" BackColor="CadetBlue" ForeColor="BlanchedAlmond" Font-Bold="true"/>
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText ="Recibido"> 
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Aceptar" runat="server" CssClass ='<%# Eval("SOLICITUD_ID") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="8pt"/>
                                                <ItemStyle HorizontalAlign ="Center" Width="12%" Font-Size="7.5pt" BackColor="CadetBlue" ForeColor="BlanchedAlmond" Font-Bold="true" />
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                               <ItemTemplate>
                                                 </td>
                                                 </tr> 
                                                 <tr>
                                                  <td colspan="100%">
                                                   <div id="div<%# Eval("SOLICITUD_ID") %>" style="display:inline;position:relative;left:20px;" >
                                                       <asp:GridView ID="Grid_Movimientos_Datos" runat="server" GridLines="None" Width="97%"
                                                           CssClass="GridView_1" HeaderStyle-CssClass="tblHead" 
                                                           AutoGenerateColumns="false" OnRowCreated="Grid_Movimientos_RowCreated"
                                                           style="white-space:normal;" >
                                                           <Columns> 
                                                                <asp:BoundField DataField="SOLICITUD_ID"  />
                                                                <asp:BoundField DataField="FF" HeaderText="Fuente" >
                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="8pt"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="PP" HeaderText="Programa">
                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Left"  Width="8%" Font-Size="8pt" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="UR" HeaderText="U. Responsable">
                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Left"  Width="23%" Font-Size="8pt" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="PARTIDA" HeaderText="Partida">
                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Left"  Width="28%" Font-Size="8pt" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="IMPORTE" HeaderText="Importe" DataFormatString="{0:#,###,##0.00}">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Right"  Width="11%" Font-Size="8pt"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="APROBADO" HeaderText="Aprobado" DataFormatString="{0:#,###,##0.00}">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Right"  Width="11%" Font-Size="8pt"/>
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MODIFICADO" HeaderText="Modificado" DataFormatString="{0:#,###,##0.00}">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="7.5pt"/>
                                                                    <ItemStyle HorizontalAlign="Right"  Width="11%" Font-Size="8pt" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="JUSTIFICACION" />
                                                           </Columns>
                                                           <SelectedRowStyle CssClass="GridSelected" />
                                                           <PagerStyle CssClass="GridHeader" />
                                                           <HeaderStyle  Font-Bold="true" BackColor="Beige" ForeColor="DarkSlateGray" BorderStyle="Ridge" BorderColor="Beige" />
                                                           <AlternatingRowStyle CssClass="GridAltItem" /> 
                                                       </asp:GridView>
                                                   </div>
                                                  </td>
                                                 </tr>
                                               </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="GridHeader" />
                                  </asp:GridView>
                            </td>
                        </tr>
                    </table >
                </div>
            </center>
         </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

