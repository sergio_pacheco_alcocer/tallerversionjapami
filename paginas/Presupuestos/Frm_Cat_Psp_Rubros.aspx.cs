﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cat_Psp_Rubros.Negocio;


public partial class paginas_Presupuestos_Frm_Cat_Psp_Rubros : System.Web.UI.Page
{
    #region (Page_Load)
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo de inicio de la pagina
        ///PARAMETROS           :
        ///CREO                 : Leslie Gonzalez Vázquez
        ///FECHA_CREO           : 16/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            try
            {
                if (!IsPostBack)
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Rubros_Inicio();
                    ViewState["SortDirection"] = "DESC";
                }
            }
            catch (Exception Ex)
            {
                Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de Rubros. Error[" + Ex.Message + "]", true);
            }
        }
    #endregion

    #region (Metodos)
        #region (Generales)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
            ///PARAMETROS           1: Estatus: true o false para habilitar los controles
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Rubros_Inicio() 
            {
                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                    Limpiar_Forma();
                    Habilitar_Forma(false);
                    Estado_Botones("Inicial");
                    Llenar_Combo_Estatus();
                    Llenar_Grid_Rubros();
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar la pagina de Rubros. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles
            ///PARAMETROS           1: Estatus: true o false para habilitar los controles
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Habilitar_Forma(Boolean Estatus)
            {
                Txt_Clave.Enabled = Estatus;
                Txt_Descripcion.Enabled = Estatus;
                Cmb_Estatus.Enabled = Estatus;
                Grid_Rubros.Enabled = !Estatus;
                Txt_Busqueda.Enabled = !Estatus;
                Btn_Buscar.Enabled = !Estatus;
                Txt_Anio.Enabled = Estatus;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
            ///DESCRIPCIÓN          : Metodo para limpiar los controles
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Limpiar_Forma()
            {
                Txt_Clave.Text = String.Empty;
                Txt_Descripcion.Text = String.Empty;
                Cmb_Estatus.SelectedIndex = -1;
                Txt_Busqueda.Text = String.Empty;
                Hf_Clave.Value = String.Empty;
                Hf_Rubro_Id.Value = String.Empty;
                Txt_Clave_Rubro.Text = String.Empty;
                Txt_Anio.Text = String.Empty;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Forma
            ///DESCRIPCIÓN          : Metodo para limpiar los controles
            ///PARAMETROS           :
            ///CREO                 : Leslie Gonzalez Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Mostar_Limpiar_Error(String Encabezado_Error, String Mensaje_Error, Boolean Mostrar)
            {
                Lbl_Encabezado_Error.Text = Encabezado_Error;
                Lbl_Mensaje_Error.Text = Mensaje_Error;
                Lbl_Mensaje_Error.Visible = Mostrar;
                Td_Error.Visible = Mostrar;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estado_Botones
            ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
            ///PARAMETROS           1: String Estado: El estado de los botones solo puede tomar 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Estado_Botones(String Estado)
            {
                switch (Estado)
                {
                    case "Inicial":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Enabled = true;
                        Btn_Eliminar.Visible = true;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        break;
                    case "Nuevo":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Guardar";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "Modificar":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        //Boton Eliminar
                        Btn_Eliminar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                }//fin del switch
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Datos
            ///DESCRIPCIÓN          : metodo para validar los datos del formulario
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Boolean Validar_Datos()
            {
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio();
                DataTable Dt_Rubros = new DataTable();
                String Mensaje_Encabezado = "Es necesario: ";
                String Mensaje_Error = String.Empty;
                Boolean Datos_Validos = true;

                try
                {
                    Mostar_Limpiar_Error(String.Empty, String.Empty, false);

                    if (String.IsNullOrEmpty(Txt_Clave.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Introducir una clave. <br />";
                        Datos_Validos = false;
                    }
                    else 
                    {
                        if (Btn_Modificar.ToolTip.Trim().Equals("Actualizar"))
                        {
                            if (!Txt_Clave_Rubro.Text.Trim().Equals(Hf_Clave.Value.Trim()) || !Txt_Anio.Text.Trim().Equals(Hf_Anio.Value.Trim()))
                            {
                                Negocio.P_Clave = Txt_Clave_Rubro.Text.Trim();
                                if (!String.IsNullOrEmpty(Txt_Anio.Text.Trim()))
                                {
                                    Negocio.P_Anio = Txt_Anio.Text.Trim();
                                }
                                else 
                                {
                                    Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                    Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                                }
                                Dt_Rubros = Negocio.Consultar_Rubros();
                                if (Dt_Rubros != null)
                                { 
                                    if(Dt_Rubros.Rows.Count > 0)
                                    {
                                        Mensaje_Error += "&nbsp;&nbsp;* Introducir una clave diferente. <br />";
                                        Datos_Validos = false;
                                    }
                                }
                            }
                        }
                        else if (Btn_Nuevo.ToolTip.Trim().Equals("Guardar"))
                        {
                            Negocio.P_Clave = Txt_Clave_Rubro.Text.Trim();
                            if (!String.IsNullOrEmpty(Txt_Anio.Text.Trim()))
                            {
                                Negocio.P_Anio = Txt_Anio.Text.Trim();
                            }
                            else
                            {
                                Negocio.P_Anio = String.Format("{0:yyyy}", DateTime.Now);
                                Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                            }
                            Dt_Rubros = Negocio.Consultar_Rubros();
                            if (Dt_Rubros != null)
                            {
                                if (Dt_Rubros.Rows.Count > 0)
                                {
                                    Mensaje_Error += "&nbsp;&nbsp;* Introducir una clave diferente. <br />";
                                    Datos_Validos = false;
                                }
                            }
                        }
                    }
                    if (Cmb_Estatus.SelectedIndex <= 0)
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Seleccionar un estatus. <br />";
                        Datos_Validos = false;
                    }
                    if (String.IsNullOrEmpty(Txt_Descripcion.Text.Trim()))
                    {
                        Mensaje_Error += "&nbsp;&nbsp;* Instroducir una descripción. <br />";
                        Datos_Validos = false;
                    }

                    if (!Datos_Validos)
                    {
                        Mostar_Limpiar_Error(Mensaje_Encabezado, Mensaje_Error, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al validar los datos. Error[" + Ex.Message + "]", true);
                }
                return Datos_Validos;
            }
        #endregion

        #region (Combos / Grid)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Estatus
            ///DESCRIPCIÓN          : Metodo para llenar el combo de estatus
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Estatus()
            {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("ACTIVO", "ACTIVO"));
                Cmb_Estatus.Items.Insert(2, new ListItem("INACTIVO", "INACTIVO"));
                Cmb_Estatus.DataBind();
                Cmb_Estatus.SelectedIndex = -1;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Rubros
            ///DESCRIPCIÓN          : Metodo para llenar el grid de los rubros
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Grid_Rubros()
            {
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio(); //conexion con la capa de negocios
                DataTable Dt_Rubros = new DataTable();

                try
                {
                    Dt_Rubros = Negocio.Consultar_Rubros();
                    if (Dt_Rubros != null)
                    {
                        Grid_Rubros.Columns[4].Visible = true;
                        Grid_Rubros.DataSource = Dt_Rubros;
                        Grid_Rubros.DataBind();
                        Grid_Rubros.Columns[4].Visible = false;
                    }
                    else 
                    {
                        Grid_Rubros.DataSource = new DataTable();
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al cargar la tabla de Rubros. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Buscar_Rubros
            ///DESCRIPCIÓN          : Metodo para buscar 
            ///PARAMETROS           1 Texto_Buscar: texto que buscaremos en el grid
            ///                     2 Dt_Datos: tabla de donde buscaremos los datos
            ///                     3 Tbl_Datos: Grid donde realizaremos la busqueda
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public static void Buscar_Rubros(String Texto_Buscar, DataTable Dt_Datos, GridView Tbl_Datos)
            {
                DataView Dv_Datos = null;//Variable que almacena una vista que obtendra a partir de la búsqueda.
                String Expresion_Busqueda = String.Empty;//Variable que almacenara la expresion de búsqueda.

                try
                {
                    Dv_Datos = new DataView(Dt_Datos);//Creamos el objeto que almacenara una vista de la tabla de roles.

                    Expresion_Busqueda = String.Format("{0} '%{1}%'", Tbl_Datos.SortExpression, Texto_Buscar);
                    Dv_Datos.RowFilter = Cat_Psp_Rubro.Campo_Clave + " like " + Expresion_Busqueda;
                    Dv_Datos.RowFilter += " Or " + Cat_Psp_Rubro.Campo_Descripcion + " like " + Expresion_Busqueda;
                    Dv_Datos.RowFilter += " Or " + Cat_Psp_Rubro.Campo_Estatus + " like " + Expresion_Busqueda;

                    Tbl_Datos.Columns[4].Visible = true;
                    Tbl_Datos.DataSource = Dv_Datos;
                    Tbl_Datos.DataBind();
                    Tbl_Datos.Columns[4].Visible = false;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al ejecutar la busqueda de rubros. Error: [" + Ex.Message + "]");
                }
            }

        #endregion
    #endregion

    #region (Eventos)
        #region (Botones)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
            ///DESCRIPCIÓN          : Evento del boton Buscar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio(); //Conexion a la capa de negocios
                DataTable Dt_Rubros = new DataTable();
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);

                try
                {
                    if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                    {
                        Dt_Rubros = Negocio.Consultar_Rubros();
                        if (Dt_Rubros != null)
                        {
                            if (Dt_Rubros.Rows.Count > 0)
                            {
                                Buscar_Rubros(Txt_Busqueda.Text.Trim(), Dt_Rubros, Grid_Rubros);
                            }
                        }
                    }
                    else 
                    {
                        Llenar_Grid_Rubros();
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error en la busqueda de Rubros. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton Salir 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Salir_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                switch (Btn_Salir.ToolTip)
                {
                    case "Cancelar":
                        Rubros_Inicio();
                        Grid_Rubros.SelectedIndex = -1;
                        break;

                    case "Inicio":
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        break;
                }//fin del switch
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
            ///DESCRIPCIÓN          : Evento del boton Eliminar 
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Eliminar_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    if (Grid_Rubros.SelectedIndex > (-1))
                    {
                        Negocio.P_Rubro_ID = Hf_Rubro_Id.Value.Trim();

                        if (Negocio.Eliminar_Rubros())
                        {
                            Rubros_Inicio();
                            Grid_Rubros.SelectedIndex = -1;

                            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Eliminar", "alert('Operacion Completa: El estatus cambio a INACTIVO');", true);
                        }
                    }
                    else
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar un registro de la tabla", String.Empty, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al eliminar el Rubro. Error[" + Ex.Message + "]", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton Guardar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Nuevo_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    switch (Btn_Nuevo.ToolTip)
                    {
                        case "Nuevo":
                            Estado_Botones("Nuevo");
                            Limpiar_Forma();
                            Habilitar_Forma(true);
                            Cmb_Estatus.Enabled = false;
                            Cmb_Estatus.SelectedIndex = 1;
                            Txt_Anio.Text = String.Format("{0:yyyy}", DateTime.Now);
                            break;
                        case "Guardar":
                            if (Validar_Datos())
                            {
                                Negocio.P_Clave = Txt_Clave_Rubro.Text.Trim();
                                Negocio.P_Anio = Txt_Anio.Text.Trim();
                                Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                                Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                                Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();

                                if (Negocio.Guardar_Rubros())
                                {
                                    Rubros_Inicio();
                                    Grid_Rubros.SelectedIndex = -1;

                                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('Operacion Completa');", true);
                                }
                            }
                            break;
                    }//fin del swirch
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al guardar el Rubro. Error[" + Ex.Message + "]", true);
                }
            }//fin del boton Nuevo

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
            ///DESCRIPCIÓN          : Evento del boton Modificar
            ///PARAMETROS           :    
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Btn_Modificar_Click(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                Cls_Cat_Psp_Rubros_Negocio Negocio = new Cls_Cat_Psp_Rubros_Negocio();//Variable de conexion con la capa de negocios.
                try
                {
                    if (Grid_Rubros.SelectedIndex > (-1))
                    {
                        switch (Btn_Modificar.ToolTip)
                        {
                            //Validacion para actualizar un registro y para habilitar los controles que se requieran
                            case "Modificar":
                                Estado_Botones("Modificar");
                                Habilitar_Forma(true);
                                break;
                            case "Actualizar":
                                if (Validar_Datos())
                                {
                                    Negocio.P_Clave = Txt_Clave_Rubro.Text.Trim();
                                    Negocio.P_Anio = Txt_Anio.Text.Trim();
                                    Negocio.P_Descripcion = Txt_Descripcion.Text.Trim().ToUpper();
                                    Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                                    Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.Trim();
                                    Negocio.P_Rubro_ID = Hf_Rubro_Id.Value.Trim();
                                    if (Negocio.Modificar_Rubros())
                                    {
                                        Rubros_Inicio();
                                        Grid_Rubros.SelectedIndex = -1;
                                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Actualizar", "alert('Operacion Completa');", true);
                                    }
                                }
                                break;
                        }//fin del switch
                    }
                    else
                    {
                        Mostar_Limpiar_Error("Favor de seleccionar un registro de la tabla", String.Empty, true);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al modificar el Rubro. Error[" + Ex.Message + "]", true);
                }
            }//fin de Modificar
        #endregion

        #region (Grid)
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Rubros_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion del grid
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Rubros_SelectedIndexChanged(object sender, EventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                String Clave = String.Empty;
                try
                {
                    if (Grid_Rubros.SelectedIndex > (-1))
                    {
                        Limpiar_Forma();
                        Clave = Grid_Rubros.SelectedRow.Cells[1].Text.Trim();
                        Txt_Clave.Text = Clave.Substring(0,1);
                        Txt_Clave_Rubro.Text = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[1].Text).ToString();
                        Txt_Descripcion.Text = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[2].Text).ToString(); ;
                        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[3].Text).ToString()));
                        Txt_Anio.Text = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[5].Text).ToString();
                        Hf_Anio.Value = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[5].Text).ToString();

                        Hf_Clave.Value = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[1].Text).ToString();
                        Hf_Rubro_Id.Value = HttpUtility.HtmlDecode(Grid_Rubros.SelectedRow.Cells[4].Text).ToString();

                        Estado_Botones("Inicial");
                        Btn_Eliminar.Enabled = true;
                        Btn_Modificar.Enabled = true;
                        Habilitar_Forma(false);
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error("Error en la busqueda de Rubros. Error[" + Ex.Message + "]", String.Empty, true);
                }  
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Rubros_Sorting
            ///DESCRIPCIÓN          : Evento de ordenar las columnas de los grids
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 16/Marzo/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Rubros_Sorting(object sender, GridViewSortEventArgs e)
            {
                Mostar_Limpiar_Error(String.Empty, String.Empty, false);
                DataTable Dt_Rubros = new DataTable();

                try
                {
                    Llenar_Grid_Rubros();
                    Dt_Rubros = (DataTable)Grid_Rubros.DataSource;
                    if (Dt_Rubros != null)
                    {
                        DataView Dv_Vista = new DataView(Dt_Rubros);
                        String Orden = ViewState["SortDirection"].ToString();
                        if (Orden.Equals("ASC"))
                        {
                            Dv_Vista.Sort = e.SortExpression + " DESC";
                            ViewState["SortDirection"] = "DESC";
                        }
                        else
                        {
                            Dv_Vista.Sort = e.SortExpression + " ASC";
                            ViewState["SortDirection"] = "ASC";
                        }
                        Grid_Rubros.DataSource = Dv_Vista;
                        Grid_Rubros.DataBind();
                    }
                }
                catch (Exception Ex)
                {
                    Mostar_Limpiar_Error(String.Empty, "Error al ordenar la tabla de Rubros. Error[" + Ex.Message + "]", true);
                }
            }
        #endregion
    #endregion
}
