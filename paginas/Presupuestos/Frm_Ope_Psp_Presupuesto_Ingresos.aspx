﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
 AutoEventWireup="true" CodeFile="Frm_Ope_Psp_Presupuesto_Ingresos.aspx.cs" 
 Inherits="paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Ingresos" Title="SIAC Sistema Integral Administrativo y Comercial" EnableEventValidation="false"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <link href="../../easyui/themes/jquery-ui-1.8.1.custom.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/jquery-ui.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../easyui/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../easyui/jquery-1.4.2.min.js" type="text/javascript"></script>
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../easyui/jquery.treegrid.js" type="text/javascript"></script>
    <script src="../../easyui/jquery-ui-1.8.11.custom.min.js" type="text/javascript"></script>
    <script src="../../javascript/Js_Ope_Psp_Presupuesto_Ingresos.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </asp:ScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div style="width:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Presupuesto de Ingresos</td>
                    </tr>
                    <tr>
                        <td colspan ="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%; display:none" runat="server">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px" Enabled="false"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr><td colspan="2" style="height:0.5em;"></td></tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%">
                            <asp:ImageButton ID="Btn_Salir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" 
                                CssClass="Img_Button" ToolTip="Salir" />
                        </td>
                        <td style="width:50%">&nbsp;</td>
                    </tr>
                </table>   
                <br />
                <center>
                  <asp:Panel ID="Panel1" runat="server" GroupingText="Filtros" Width="96%">
                    <center>
                        <table width="98%">
                            <tr><td colspan="6" style="height:0.3em;"></td></tr>
                            <tr>
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_Anio" runat="server" Text="* Año"></asp:Label>
                                </td>
                                <td style="width:27%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:DropDownList id= "Cmb_Anio" runat = "server" Width="100%"></asp:DropDownList>
                                </td>
                                 <td colspan="2" style="width:50%; text-align:center; cursor:default;" class="button_autorizar">
                                    <asp:Button ID="Btn_Generar" runat="server" CssClass="button" Text="Generar" ToolTip="Generar" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_FF" runat="server" Text="Fuente Financiamiento"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_FF" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                             <tr style="display:none;">
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_R" runat="server" Text="Rubro"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_R" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                             <tr style="display:none;">
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_T" runat="server" Text="Tipo"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_T" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                             <tr style="display:none;">
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_Cl" runat="server" Text="Clase"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_Cl" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                             <tr>
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_Co" runat="server" Text="Concepto"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_Co" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                            <tr id="Tr_Subconceptos" style="display:block;">
                                <td style="width:23%; text-align:left; cursor:default;" class="button_autorizar">
                                    <asp:Label ID="Lbl_SubCo" runat="server" Text="SubConcepto"></asp:Label>
                                </td>
                                <td style=" text-align:left; cursor:default; width:85%;"  colspan="3" class="button_autorizar">
                                    <asp:DropDownList ID="Cmb_SubCo" runat="server" Width="100%" ></asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </center> 
                  </asp:Panel>
                </center>
                <center>
                    <div style="text-align:center">
                        <table id="Tbl_Opciones">
                            <tr>
                                <td>
                                    <a href="#" onclick="javascript:expandAll()"  style="color:Navy; font-size:8pt;">Abrir Todo</a>
                                    <a href="#" onclick="javascript:collapseAll()" style="color:Navy; font-size:8pt;">Cerrar Todo</a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <table width="97%">
                        <tr>
                            <td> <table id="Grid_CRI"></table></td>
                        </tr>
                    </table>
                </center>
            </div>
          </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

