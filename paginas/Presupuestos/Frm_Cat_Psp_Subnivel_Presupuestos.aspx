<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true"
 CodeFile="Frm_Cat_Psp_Subnivel_Presupuestos.aspx.cs" Inherits="paginas_Presupuestos_Frm_Cat_Psp_Subnivel_Presupuestos" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 10%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"></cc1:ToolkitScriptManager>
     <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
              <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress">
                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                </div>
            </ProgressTemplate>
           </asp:UpdateProgress>    

        <div id="Div_Requisitos" style="background-color:#ffffff; width:99%; height:100%;">      
            <table width="100%" class="estilo_fuente">
                <tr align="center">
                    <td class="label_titulo">
                        SubNiveles Presupuestos
                    </td>
                </tr>
                <tr>
                    <td runat="server" id="Td_Error">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" />&nbsp;
                            <asp:Label ID="Lbl_Encabezado_Error" runat="server"   CssClass="estilo_fuente_mensaje_error"/>
                            <br />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server"  CssClass="estilo_fuente_mensaje_error"/>
                    </td>
                </tr>
           </table>          
           
            <table width="100%"  border="0" cellspacing="0">
                     <tr align="center">
                         <td>                
                             <div style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px">                        
                                  <table style="width:100%;height:28px;">
                                    <tr>
                                      <td align="left" style="width:59%;">  
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                        CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" 
                                         />
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                              CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click"
                                         />
                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" 
                                              CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                         
                                              OnClientClick="return confirm('El estatus del Subnivel Presupuesto cambiara a Inactivo. �Desea continuar?');" 
                                              onclick="Btn_Eliminar_Click"/>
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                              ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"
                                         />
                                      </td>
                                      <td align="right" style="width:41%;">
                                        <table style="width:100%;height:28px;">
                                            <tr>
                                                <td style="vertical-align:middle;text-align:right;width:20%;">B&uacute;squeda:</td>
                                                <td style="width:55%;">
                                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="200px" MaxLength ="100" 
                                                        ontextchanged="Txt_Busqueda_TextChanged"></asp:TextBox>
                                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Rol_ID" runat="server" WatermarkCssClass="watermarked"
                                                          WatermarkText="<Ingrese Busqueda>" TargetControlID="Txt_Busqueda" />
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                                        runat="server" TargetControlID="Txt_Busqueda" 
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                        ValidChars="������������. "/>
                                                </td>
                                                <td style="vertical-align:middle;width:5%;" >
                                                    <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar"
                                                          ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Click" 
                                                         />                                       
                                                </td>
                                            </tr>                                                                          
                                        </table>                                    
                                       </td>       
                                     </tr>         
                                  </table>                      
                                </div>
                         </td>
                     </tr>
            </table>         
            <center>
            <table width="97%">
                <tr>
                    <td colspan="4">    
                        <hr />
                    </td>
                </tr>   
                <tr>
                    <td style="width:10%;text-align:left;">
                        * Unidad Responsable
                    </td>
                    <td style="width:10%;text-align:left;">
                        <asp:DropDownList ID="Cmb_Unidad_responsable" Width="100%" runat="server" 
                            onselectedindexchanged="Cmb_Unidad_responsable_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList> </td>
                </tr>
                 <tr>
                    <td style="width:10%;text-align:left;">
                        * Fuente de financiamiento
                    </td>
                    <td style="width:10%;text-align:left;">
                        <asp:DropDownList ID="Cmb_FTE_Financiamiento" Width="100%" runat="server" 
                            onselectedindexchanged="Cmb_FTE_Financiamiento_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList> </td>
                </tr>
                <tr>
                    <td style="width:10%;text-align:left;">
                       * Programa 
                    </td>
                    <td style="width:30%;text-align:left;">
                        <asp:DropDownList ID="Cmb_Programa" runat="server" Width="100%" 
                            onselectedindexchanged="Cmb_Programa_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td style="text-align:left;" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                 <tr>
                    <td style="width:10%;text-align:left;">
                        * Capitulo
                    </td>
                    <td style="width:30%;text-align:left;">
                        <asp:DropDownList ID="Cmb_Capitulo" runat="server" Width="100%" AutoPostBack="true"
                            onselectedindexchanged="Cmb_Capitulo_SelectedIndexChanged">
                        </asp:DropDownList>
                    <td style="text-align:left;" colspan="2">
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td style="width:10%;text-align:left;">
                        * Partida
                    </td>
                
                    <td style="width:30%;text-align:left;">
                        <asp:DropDownList ID="Cmb_Nombre_Partida" runat="server" Enabled="true"  
                            Width="100%" 
                            onselectedindexchanged="Cmb_Nombre_Partida_SelectedIndexChanged" AutoPostBack="true" />
                    </td>
                
                </tr>
             
                
                <tr>
                    <td style="width:10%;text-align:left;">
                         * A�o Subnivel Presupuestal
                    </td>
                    <td style="width:30%;text-align:left;">
                        <asp:TextBox ID="Txt_Anio" runat="server" Width="50%" MaxLength="4"></asp:TextBox></td>
                         <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anio" runat="server" TargetControlID="Txt_Anio"
                                                    FilterType="Custom, Numbers"  Enabled="True"/>
                        <td style="width:10%; text-align:left;">
                        &nbsp;&nbsp;&nbsp;* Estatus
                    </td>
                     <td style="width:30%;text-align:left;">
                       <asp:DropDownList ID="Cmb_Estatus" runat="server" Enabled="False" Width="100%" /> 
                    </td>
                </tr>
                    <tr>
                    <td style="width:10%;text-align:left;">
                        * Clave Subnivel Presupuestal
                    </td>
                    <td style="width:30%;text-align:left;">
                        <asp:TextBox ID="Txt_Clave_Subnivel_Presupuestal" MaxLength="10" runat="server" 
                            Width="50%" ontextchanged="Txt_Clave_Subnivel_Presupuestal_TextChanged" AutoPostBack="true" ></asp:TextBox>
                        <cc1:FilteredTextBoxExtender ID="FTE_Clave_Txt_Subnivel_Presupuestal" runat="server" TargetControlID="Txt_Clave_Subnivel_Presupuestal"
                                                    FilterType="Custom, Numbers"  Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                     <td  style="text-align:left;" >
                       C�digo Program�tico
                    </td>
                     <td  style="text-align:left;" >
                       <asp:TextBox ID="Txt_Codigo_Programatico" MaxLength="50" runat="server" Width="99%"  ></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width:10%;text-align:left;">
                        Descripci&oacute;n Sunbivel Presupuestal
                    </td>
                    <td style="width:90%;text-align:left;" colspan="3">
                        <asp:TextBox ID="Txt_Descripcion_Subnivel" runat="server" Width="99%" MaxLength="250" Height="55"></asp:TextBox>
                        <cc1:TextBoxWatermarkExtender ID="Txt_Descripcion_TextBoxWatermarkExtender" runat="server" 
                                TargetControlID="Txt_Descripcion_Subnivel" WatermarkCssClass="watermarked" 
                                WatermarkText="L�mite de Caracteres 250">
                        </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="Txt_Descripcion_FilteredTextBoxExtender" 
                                runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                TargetControlID="Txt_Descripcion_Subnivel" ValidChars=" ��.,:;/()����������">
                            </cc1:FilteredTextBoxExtender>      
                    </td>
                </tr>
                <tr>
                    <td colspan="4">    
                        <asp:HiddenField id="Hf_Clave" runat="server"/>
                        <asp:HiddenField id="Hf_Codigo_Programatico" runat="server"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">    
                        <hr />
                    </td>
                </tr>
            </table>
            
            <table width="97%">
                <tr>
                    <td align = "center" style="width:100%;">
                        <div style="overflow:auto;height:320px;width:99%;vertical-align:top; width:100%;" >
                            <asp:GridView ID="Grid_Subniveles_Presupuestos" runat="server" AutoGenerateColumns="false" Width="98%" 
                                 onselectedindexchanged="Grid_Subniveles_Presupuestos_SelectedIndexChanged"
                                      style="white-space:normal"
                                     CssClass="GridView_1" GridLines="None" OnSorting="Grid_Subnivel_Presupuesto_Sorting"  
                                     EmptyDataText="No se encontr� ningun registro" AllowSorting="true" >
                                <Columns>
                                
                                     <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                         ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                         <ItemStyle Width="3%" />
                                     </asp:ButtonField>
                                    <asp:BoundField DataField="CODIGO_PROGRAMATICO" HeaderText="Cod. Program�tico" SortExpression="CODIGO_PROGRAMATICO">
                                        <HeaderStyle HorizontalAlign="Left" Width="16%" />
                                        <ItemStyle HorizontalAlign="Left" Width="16%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    
                                     <asp:BoundField DataField="AREA_FUNCIONAL" HeaderText="Area Funcional" SortExpression="AREA_FUNCIONAL">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                     
                                    <asp:BoundField DataField="UR" HeaderText="U. Responsable"  SortExpression="UR">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                     
                                      <asp:BoundField DataField="FTE_FINANCIAMIENTO" HeaderText="Fte. Financiamiento"  SortExpression="FTE_FINANCIAMIENTO">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                     
                                    <asp:BoundField DataField="PROGRAMA" HeaderText="Programa"  SortExpression="PROGRAMA">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="PARTIDA" HeaderText="Partida" SortExpression="PARTIDA">
                                         <HeaderStyle HorizontalAlign="Left" Width="15%"/>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="SUBNIVEL_PRESUPUESTAL" HeaderText="Subnivel"  SortExpression="SUBNIVEL_PRESUPUESTAL">
                                         <HeaderStyle HorizontalAlign="Left" Width="5%"/>
                                        <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    
                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion"  SortExpression="DESCRIPCION">
                                         <HeaderStyle HorizontalAlign="Left" Width="10%"/>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ANIO" HeaderText="A�o"  SortExpression="ANIO">
                                         <HeaderStyle HorizontalAlign="Left" Width="5%"/>
                                        <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                    </asp:BoundField>
  
                                     <asp:BoundField DataField="CLAVE" HeaderText="">
                                        <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                        <ItemStyle HorizontalAlign="Left" Width="0%" Font-Size="X-Small"/>
                                    </asp:BoundField>
                                    
                                </Columns>
                                <RowStyle CssClass="GridItem" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" /> 
                                <AlternatingRowStyle CssClass="GridAltItem" />    
                            </asp:GridView>
                        </div>
                   </td>
            </tr>
        </table>
        </center>
    </div>
 </ContentTemplate>
</asp:UpdatePanel>
</asp:Content>