﻿using System;
using JAPAMI.Sessiones;
using System.Web.Services;
using CarlosAg.ExcelXmlWriter;
using System.Data;
using JAPAMI.Psp_Presupuesto_Egresos_Ingresos.Negocio;
using JAPAMI.Movimiento_Presupuestal.Negocio;
using JAPAMI.Constantes;
using System.Text;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Linq;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Egresos_Ingresos_Mensual_Usuario : System.Web.UI.Page
{
    const String FF_Nomina = "'RP13'";

    #region (Page Load)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 03/Julio/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Obtener_Parametros();
            }
        }
    #endregion

    #region (Metodos Generales)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Controlador_Inicio
        ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Controlador_Inicio(String Anio, String Tipo_Psp, String FF_ID, String FF_Ing_ID, String AF_ID, String UR_ID, 
            String PP_ID, String PP_Ing_ID, String Capitulo_ID, String Concepto_ID, String Partida_Gen_ID, 
            String Partida_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_Ing_ID, String SubConcepto_ID,
            String Niv_FF, String Niv_PP, String Niv_AF, String Niv_UR, String Niv_Cap, String Niv_Con, String Niv_Par_Gen, String Niv_Par,
            String Niv_FF_Ing, String Niv_PP_Ing, String Niv_Rub, String Niv_Tip, String Niv_Cla, String Niv_Con_Ing, String Niv_SubCon)
        {
            if (!String.IsNullOrEmpty(Tipo_Psp))
            {
                    switch (Tipo_Psp)
                    {
                         #region(Reporte)
                        case "EGR":
                            Obtener_Reporte_Mensual_Egresos(Anio, FF_ID, PP_ID, UR_ID,
                                AF_ID, Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID, Niv_FF, Niv_PP,
                                Niv_AF, Niv_UR, Niv_Cap, Niv_Con, Niv_Par_Gen, Niv_Par);
                            break;
                        case "ING":
                            Obtener_Reporte_Mensual_Ingresos(Anio, FF_Ing_ID, PP_Ing_ID, 
                                Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID,SubConcepto_ID, Niv_FF_Ing, Niv_PP_Ing,
                                Niv_Rub, Niv_Tip, Niv_Cla, Niv_Con_Ing, Niv_SubCon);
                            break;
                        #endregion
                    }
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Parametros
        ///DESCRIPCIÓN          : Metodo para obtener los parametros
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 25/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Obtener_Parametros()
        {
            //Parametros de los combos
            String Anio = String.Empty;
            String FF_ID = String.Empty;
            String FF_Ing_ID = String.Empty;
            String AF_ID = String.Empty;
            String UR_ID = String.Empty;
            String PP_ID = String.Empty;
            String PP_Ing_ID = String.Empty;
            String Capitulo_ID = String.Empty;
            String Concepto_ID = String.Empty;
            String Partida_Gen_ID = String.Empty;
            String Partida_ID = String.Empty;
            String Tipo_Psp = String.Empty;
            String Rubro_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String Clase_ID = String.Empty;
            String Concepto_Ing_ID = String.Empty;
            String SubConcepto_ID = String.Empty;

            String Niv_FF = String.Empty;
            String Niv_PP = String.Empty;
            String Niv_AF = String.Empty;
            String Niv_UR = String.Empty;
            String Niv_Cap = String.Empty;
            String Niv_Con = String.Empty;
            String Niv_Par_Gen = String.Empty;
            String Niv_Par = String.Empty;


            //Parametros de los combos
            String Niv_FF_Ing = String.Empty;
            String Niv_PP_Ing = String.Empty;
            String Niv_Rub = String.Empty;
            String Niv_Tip = String.Empty;
            String Niv_Cla = String.Empty;
            String Niv_Con_Ing = String.Empty;
            String Niv_SubCon = String.Empty;

            //Obtenemos los campos que seran mostrados en el reporte.
            String[] array = Hf_Parametros_Rpt.Value.Trim().Split(new Char[] { '&' });
            String[] Valor = null;

            if (array.Length > 0)
            {
                foreach (String Parametro in array)
                {
                    Valor = null;

                    if (Parametro is String)
                    {
                        if (Parametro.Trim().StartsWith("Anio="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Anio = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Tipo_Psp="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Tipo_Psp = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("FF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                FF_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("AF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                AF_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("FF_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                FF_Ing_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("UR="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                UR_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("PP="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                PP_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("PP_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                PP_Ing_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Cap="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Capitulo_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Con="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Concepto_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("PG="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Partida_Gen_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("P="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Partida_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("R="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Rubro_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("T="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Tipo_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Cl="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Clase_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Con_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Concepto_Ing_ID = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("SubCon="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                SubConcepto_ID = Valor[1];
                            }
                        }
                        //parameos de los niveles
                        else if (Parametro.Trim().StartsWith("Niv_FF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_FF = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_PP="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_PP = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_AF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_AF = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_UR="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_UR = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Cap="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Cap = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Con="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Con = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Par_Gen="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Par_Gen = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Par="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Par = Valor[1];
                            }
                        }
                        //parametros niveles de ingresos
                        else if (Parametro.Trim().StartsWith("Niv_FF_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_FF_Ing = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_PP_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_PP_Ing = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Rub="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Rub = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Tip="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Tip = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Cla="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Cla = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Con_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Con_Ing = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_SubCon="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_SubCon = Valor[1];
                            }
                        }
                    }
                }
            }

            Controlador_Inicio(Anio, Tipo_Psp, FF_ID, FF_Ing_ID, AF_ID, UR_ID,PP_ID, PP_Ing_ID,
                Capitulo_ID, Concepto_ID, Partida_Gen_ID, Partida_ID,
                Rubro_ID, Tipo_ID, Clase_ID, Concepto_Ing_ID, SubConcepto_ID,
                Niv_FF, Niv_PP, Niv_AF, Niv_UR, Niv_Cap, Niv_Con, Niv_Par_Gen, Niv_Par,
                Niv_FF_Ing, Niv_PP_Ing, Niv_Rub, Niv_Tip, Niv_Cla, Niv_Con_Ing, Niv_SubCon);
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Dependencias
        ///DESCRIPCIÓN          : Metodo para obtener los datos de las dependencias del usuario
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 14/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Dependencias()
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            DataTable Dt_Dependencias = new DataTable();
            String Dependencias = String.Empty;

            //try
            //{
                Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                Dt_Dependencias = Negocio.Consultar_Dependencias_Usuarios();

                if (Dt_Dependencias != null)
                {
                    if (Dt_Dependencias.Rows.Count > 0)
                    {
                        //obtenemos las dependencias
                        foreach (DataRow Dr_Ur in Dt_Dependencias.Rows)
                        {
                            Dependencias += "'" + Dr_Ur[Cat_Det_Empleado_UR.Campo_Dependencia_ID].ToString().Trim() + "',";
                        }
                        //quitamos la ultima coma
                        Dependencias = Dependencias.Substring(0, Dependencias.Length - 1);
                    }
                    else
                    {
                        Dependencias = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";
                    }
                }
                else
                {
                    Dependencias = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";
                }
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("Error al obtener los datos de la dependencia del usuario. Error[" + Ex.Message + "]");
            //}
            return Dependencias;
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Tipo_Usuario
        ///DESCRIPCIÓN          : Metodo para obtener el tipo de usuario
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Agostp/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Tipo_Usuario()
        {
            Cls_Ope_Psp_Movimiento_Presupuestal_Negocio Mov_Negocio = new Cls_Ope_Psp_Movimiento_Presupuestal_Negocio();
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio_PSP = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Administrador = String.Empty;
            DataTable Dt_Ramo33 = new DataTable();
            String Tipo_Usuario = String.Empty;
            String Gpo_Dep = "00000";
            DataTable Dt_Gpo_Dep = new DataTable();

            //try
            //{
                //verificar si el empleado que esta logueado tiene el rol de administrador del presupuesto municipal y estatal
                Negocio_PSP.P_Rol_ID = Cls_Sessiones.Rol_ID.Trim();
                Administrador = Negocio_PSP.Consultar_Rol();

                if (!String.IsNullOrEmpty(Administrador.Trim()))
                {
                    if (Administrador.Trim().Equals("Presupuestos"))
                    {
                        Tipo_Usuario = "Administrador";
                    }
                    else if (Administrador.Trim().Equals("Nomina"))
                    {
                        Tipo_Usuario = "Nomina";
                        //Tipo_Usuario = "Administrador";
                    }
                }
                else
                {
                    //verificamos si el usuario logueado es el administrador de ramo33
                    Dt_Ramo33 = Mov_Negocio.Consultar_Usuario_Ramo33();
                    if (Dt_Ramo33 != null && Dt_Ramo33.Rows.Count > 0)
                    {
                        if (Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim())
                            || Dt_Ramo33.Rows[0][Cat_Con_Parametros.Campo_Usuario_Autoriza_Inv_2].ToString().Trim().Equals(Cls_Sessiones.Empleado_ID.Trim()))
                        {
                            Tipo_Usuario = "Inversiones";
                        }
                        else
                        {
                            Tipo_Usuario = "Usuario";
                        }
                    }
                    else
                    {
                        Tipo_Usuario = "Usuario";
                    }

                    if (Tipo_Usuario.Trim().Equals("Usuario"))
                    {
                        //verificamos si no es algun coordinador administrativo de direccion
                        Negocio_PSP.P_Empleado_ID = Cls_Sessiones.Empleado_ID.Trim();
                        Dt_Gpo_Dep = Negocio_PSP.Obtener_Gpo_Dependencia();

                        if (Dt_Gpo_Dep != null && Dt_Gpo_Dep.Rows.Count > 0)
                        {
                            Gpo_Dep = Dt_Gpo_Dep.Rows[0][Cat_Dependencias.Campo_Grupo_Dependencia_ID].ToString().Trim();
                            Tipo_Usuario = "Coordinador";
                        }
                    }
                }

                Tipo_Usuario = Tipo_Usuario.Trim() + "/" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "/" + Gpo_Dep.Trim();
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("Error al obtener el tipo de usuario. Error[" + Ex.Message + "]");
            //}
            return Tipo_Usuario;
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Datos
        ///DESCRIPCIÓN          : Metodo para validar los datos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 04/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Validar_Datos() 
        {
            Boolean Datos_Validos = true;
            int Seleccionado = 0;
            String Anio = String.Empty;
            String Tipo_Psp = String.Empty;
            String Mensaje = String.Empty;

            String Niv_FF = String.Empty;
            String Niv_PP = String.Empty;
            String Niv_AF = String.Empty;
            String Niv_UR = String.Empty;
            String Niv_Cap = String.Empty;
            String Niv_Con = String.Empty;
            String Niv_Par_Gen = String.Empty;
            String Niv_Par = String.Empty;

            String Niv_FF_Ing = String.Empty;
            String Niv_PP_Ing = String.Empty;
            String Niv_Rub = String.Empty;
            String Niv_Tip = String.Empty;
            String Niv_Cla = String.Empty;
            String Niv_Con_Ing = String.Empty;
            String Niv_SubCon = String.Empty;
            

            //Obtenemos los campos que seran mostrados en el reporte.
            String[] array = Hf_Parametros_Rpt.Value.Trim().Split(new Char[] { '&' });
            String[] Valor = null;

            if (array.Length > 0)
            {
                foreach (String Parametro in array)
                {
                    Valor = null;

                    if (Parametro is String)
                    {
                        if (Parametro.Trim().StartsWith("Anio="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Anio = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Tipo_Psp="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Tipo_Psp = Valor[1];
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_FF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_FF = Valor[1];
                                if (Niv_FF.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_PP="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_PP = Valor[1];
                                if (Niv_PP.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_AF="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_AF = Valor[1];
                                if (Niv_AF.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_UR="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_UR = Valor[1];
                                if (Niv_UR.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Cap="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Cap = Valor[1];
                                if (Niv_Cap.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Con="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Con = Valor[1];
                                if (Niv_Con.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Par_Gen="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Par_Gen = Valor[1];
                                if (Niv_Par_Gen.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Par="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Par = Valor[1];
                                if (Niv_Par.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        //parametros niveles de ingresos
                        else if (Parametro.Trim().StartsWith("Niv_FF_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_FF_Ing = Valor[1];
                                if (Niv_FF_Ing.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_PP_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_PP_Ing = Valor[1];
                                if (Niv_PP_Ing.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Rub="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Rub = Valor[1];
                                if (Niv_Rub.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Tip="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Tip = Valor[1];
                                if (Niv_Tip.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Cla="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Cla = Valor[1];
                                if (Niv_Cla.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_Con_Ing="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_Con_Ing = Valor[1];
                                if (Niv_Con_Ing.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                        else if (Parametro.Trim().StartsWith("Niv_SubCon="))
                        {
                            Valor = Parametro.Split('=');
                            if (Valor.Length > 0)
                            {
                                Niv_SubCon = Valor[1];
                                if (Niv_SubCon.Trim().Equals("S"))
                                {
                                    Seleccionado++;
                                }
                            }
                        }
                    }
                }
            }

            if (String.IsNullOrEmpty(Anio))
            {
                Mensaje += "* Seleccionar un Año.  ";
                Datos_Validos = false;
            }

            if (String.IsNullOrEmpty(Tipo_Psp))
            {
                Mensaje += "* Seleccionar un tipo de presupuesto.   ";
                Datos_Validos = false;
            }

            if (Datos_Validos)
            {
                if (Seleccionado <= 0)
                {
                    Mensaje += "* Palomear algun nivel de los filtros.   ";
                    Datos_Validos = false;
                }
            }

            return Mensaje;
        }
    #endregion

    #region (Eventos)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Evento del boton de generar reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 04/Julio/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            String Mensaje = String.Empty;

            Mensaje = Validar_Datos();

            if (String.IsNullOrEmpty(Mensaje.Trim()))
            {
                Obtener_Parametros();
            }
            else 
            {
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Alta", "alert('" + Mensaje.Trim() +"');", true);
            }
        }
    #endregion

    #region (Reportes Psp Egresos Mensual)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte_Mensual
        ///DESCRIPCIÓN          : Metodo para obtener los datos para los reportes mensual
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 12/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Obtener_Reporte_Mensual_Egresos(String Anio, String FF, String PP, String UR, 
            String AF, String Cap, String Con, String PG, String P, String Niv_FF, String Niv_PP, String Niv_AF, 
            String Niv_UR, String Niv_Cap, String Niv_Con, String Niv_Par_Gen, String Niv_Par)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            DataTable Dt_Egr = new DataTable();
            DataTable Dt_Egr_Completo = new DataTable();

            //try
            //{
                //parametros egresos
                Negocio.P_Anio = Anio;
                Negocio.P_Fte_Financiamiento = FF;
                Negocio.P_Area_Funcional_ID = AF;
                Negocio.P_Programa_ID = PP;
                Negocio.P_ProgramaF_ID = String.Empty;
                Negocio.P_Dependencia_ID = UR;
                Negocio.P_DependenciaF_ID = String.Empty;
                Negocio.P_Capitulo_ID = Cap;
                Negocio.P_CapituloF_ID = String.Empty;
                Negocio.P_ConceptoF_ID = Con;
                Negocio.P_Concepto_ID = String.Empty;
                Negocio.P_Partida_Generica_ID = PG;
                Negocio.P_Partida_GenericaF_ID = String.Empty;
                Negocio.P_Partida_ID = P;
                Negocio.P_PartidaF_ID = String.Empty;

                //obtenemos los datos del tipo de usuario
                String Tipo_Usuario = Obtener_Tipo_Usuario();
                String[] Usuario = Tipo_Usuario.Split('/');
                Negocio.P_Tipo_Usuario = Usuario[0].Trim();
                Negocio.P_Gpo_Dependencia_ID = Usuario[2].Trim();
                if (Negocio.P_Tipo_Usuario.Trim().Equals("Usuario"))
                {
                    if (String.IsNullOrEmpty(UR))
                    {
                        Negocio.P_Dependencia_ID = Obtener_Dependencias();
                    }
                    Negocio.P_DependenciaF_ID = String.Empty;
                }
                else if (Negocio.P_Tipo_Usuario.Trim().Equals("Nomina"))
                {
                    Negocio.P_Fte_Financiamiento = FF_Nomina;
                }

                Dt_Egr = Negocio.Consultar_Psp_Egr();

                if (Dt_Egr != null && Dt_Egr.Rows.Count > 0)
                {
                    Dt_Egr_Completo = Generar_Dt_Psp_Egr_Mensual(Dt_Egr, Niv_FF, Niv_PP, Niv_AF, Niv_UR,
                    Niv_Cap, Niv_Con, Niv_Par_Gen, Niv_Par);
                }

                Obtener_Param_Niveles_Mensual_Egresos(Dt_Egr_Completo, Anio);
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Error al Obtener_Reporte_Mensual_Egresos Error[" + ex.Message + "]");
            //}
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles_Mensual
        ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 12/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Obtener_Param_Niveles_Mensual_Egresos(DataTable Dt_Egr, String Anio)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            DataTable Dt_UR = new DataTable();
            String Dependencia = String.Empty;
            String Gpo_Dep = String.Empty;

            //try
            //{
                //obtenemos la dependencia del usuario y el grupo dependencia
                Negocio.P_Busqueda = String.Empty;
                Negocio.P_Anio = Anio;
                Negocio.P_Fte_Financiamiento = String.Empty;
                Negocio.P_Area_Funcional_ID = String.Empty;
                Negocio.P_Programa_ID = String.Empty;
                Negocio.P_Tipo_Usuario = "Usuario";
                Negocio.P_Dependencia_ID = "'" + Cls_Sessiones.Dependencia_ID_Empleado.Trim() + "'";

                Dt_UR = Negocio.Consultar_UR();

                if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                {
                    Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                    Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                }

                if (Dt_Egr != null)
                {
                    if (Dt_Egr.Rows.Count > 0)
                    {
                        Generar_Reporte_Psp_Egresos_Mensual(Dt_Egr, Anio.Trim(), Gpo_Dep, Dependencia);
                    }
                }
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Error al obtener los parametros. Error[" + ex.Message + "]");
            //}
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Egr_Mensual
        ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de egresos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 12/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Generar_Dt_Psp_Egr_Mensual(DataTable Dt_Egr, String Niv_FF, String Niv_PP,
            String Niv_AF, String Niv_UR, String Niv_Cap, String Niv_Con, String Niv_Par_Gen, String Niv_Par)
        {
            DataTable Dt_Psp = new DataTable();
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            #region(Tabla)
            String FF_ID = Dt_Egr.Rows[0]["FF_ID"].ToString().Trim();
            String AF_ID = Dt_Egr.Rows[0]["AF_ID"].ToString().Trim();
            String PP_ID = Dt_Egr.Rows[0]["PP_ID"].ToString().Trim();
            String UR_ID = Dt_Egr.Rows[0]["UR_ID"].ToString().Trim();
            String Cap_ID = Dt_Egr.Rows[0]["CA_ID"].ToString().Trim();
            String Con_ID = Dt_Egr.Rows[0]["CO_ID"].ToString().Trim();
            String PG_ID = Dt_Egr.Rows[0]["PG_ID"].ToString().Trim();

            String FF = "******* " + Dt_Egr.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
            String AF = "****** " + Dt_Egr.Rows[0]["CLAVE_NOM_AF"].ToString().Trim();
            String PP = "***** " + Dt_Egr.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
            String UR = "**** " + Dt_Egr.Rows[0]["CLAVE_NOM_UR"].ToString().Trim();
            String Cap = "*** " + Dt_Egr.Rows[0]["CLAVE_NOM_CAPITULO"].ToString().Trim();
            String Con = "** " + Dt_Egr.Rows[0]["CLAVE_NOM_CONCEPTO"].ToString().Trim();
            String PG = "* " + Dt_Egr.Rows[0]["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

            Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Ene", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Feb", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Mar", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Abr", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_May", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Jun", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Jul", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Ago", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Sep", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Oct", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Nov", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Por_Recaudar_Dic", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Ene", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Feb", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Mar", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Abr", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_May", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Jun", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Jul", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Ago", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Sep", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Oct", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Nov", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Pre_Comprometido_Dic", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Ene", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Feb", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Mar", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Abr", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_May", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Jun", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Jul", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Ago", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Sep", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Oct", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Nov", System.Type.GetType("System.String"));
            Dt_Psp.Columns.Add("Comprometido_Dic", System.Type.GetType("System.String"));
            #endregion

            #region (Registro Totales)
            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = "******** TOTAL";
            Fila["Aprobado"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("APROBADO")));
            Fila["Ampliacion"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("AMPLIACION")));
            Fila["Reduccion"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("REDUCCION")));
            Fila["Modificado"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("MODIFICADO")));
            Fila["Comprometido"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO")));
            Fila["Por_Recaudar"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE")));
            Fila["Tipo"] = "Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO")));
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_ENE")));
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_ENE")));
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_ENE")));
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_FEB")));
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_FEB")));
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_FEB")));
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_MAR")));
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_MAR")));
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_MAR")));
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_ABR")));
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_ABR")));
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_ABR")));
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_MAY")));
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_MAY")));
            Fila["Comprometido_May"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_MAY")));
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_JUN")));
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_JUN")));
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_JUN")));
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_JUL")));
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_JUL")));
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_JUL")));
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_AGO")));
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_AGO")));
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_AGO")));
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_SEP")));
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_SEP")));
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_SEP")));
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_OCT")));
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_OCT")));
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_OCT")));
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_NOV")));
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_NOV")));
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_NOV")));
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("DISPONIBLE_DIC")));
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("PRE_COMPROMETIDO_DIC")));
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Dt_Egr.AsEnumerable().Sum(x => x.Field<Decimal>("COMPROMETIDO_DIC")));
            Dt_Psp.Rows.Add(Fila);
            #endregion

            #region (Registro Fuente)
            if (Niv_FF.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Fuente(Dt_Egr, FF_ID, Dt_Psp, FF);
            }
            #endregion

            #region (Registro Area Funcional)
            if (Niv_AF.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Area_Funcional(Dt_Egr, FF_ID, AF_ID, Dt_Psp, AF);
            }
            #endregion

            #region (Registro Programa)
            if (Niv_PP.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Programa(Dt_Egr, FF_ID, AF_ID, PP_ID, Dt_Psp, PP);
            }
            #endregion

            #region (Registro Dependencia)
            if (Niv_UR.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Unidad_Responsable(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Dt_Psp, UR);
            }
            #endregion

            #region (Registro Capitulo)
            if (Niv_Cap.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
            }
            #endregion

            #region (Registro Concepto)
            if (Niv_Con.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
            }
            #endregion

            #region (Registro Partida Generica)
            if (Niv_Par_Gen.Trim().Equals("S"))
            {
                Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
            }
            #endregion

            foreach (DataRow Dr in Dt_Egr.Rows) 
            {
                if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                {
                    if (AF_ID.Trim().Equals(Dr["AF_ID"].ToString().Trim()))
                    {
                        if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                        {
                            if (UR_ID.Trim().Equals(Dr["UR_ID"].ToString().Trim()))
                            {
                                if (Cap_ID.Trim().Equals(Dr["CA_ID"].ToString().Trim()))
                                {
                                    if (Con_ID.Trim().Equals(Dr["CO_ID"].ToString().Trim()))
                                    {
                                        if (PG_ID.Trim().Equals(Dr["PG_ID"].ToString().Trim()))
                                        {
                                            #region (Registro Partida)
                                            if (Niv_Par.Trim().Equals("S"))
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                Fila["Tipo"] = "P";
                                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                                Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                                Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                                Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                                Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                                Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                                Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                                Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                                Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                                Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                                Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                                Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                                Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                                Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion
                                        }
                                        else //else ¨Partida Generica
                                        {
                                            #region (Partida Generica)

                                            PG_ID = Dr["PG_ID"].ToString().Trim();
                                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                            #region (Registro Partida Generica)
                                            if (Niv_Par_Gen.Trim().Equals("S"))
                                            {
                                                Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                                            }
                                            #endregion

                                            #region (Registro Partida)
                                            if (Niv_Par.Trim().Equals("S"))
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                                Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                                Fila["Tipo"] = "P";
                                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                                Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                                Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                                Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                                Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                                Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                                Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                                Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                                Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                                Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                                Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                                Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                                Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                                Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                                Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion

                                            #endregion
                                        }
                                    }
                                    else //else Concepto
                                    {
                                        #region (Concepto)

                                        Con_ID = Dr["CO_ID"].ToString().Trim();
                                        Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                        PG_ID = Dr["PG_ID"].ToString().Trim();
                                        PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                        #region (Registro Concepto)
                                        if (Niv_Con.Trim().Equals("S"))
                                        {
                                            Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                                        }
                                        #endregion

                                        #region (Registro Partida Generica)
                                        if (Niv_Par_Gen.Trim().Equals("S"))
                                        {
                                            Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                                        }
                                        #endregion

                                        #region (Registro Partida)
                                        if (Niv_Par.Trim().Equals("S"))
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                            Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                            Fila["Tipo"] = "P";
                                            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                            Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                            Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                            Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                            Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                            Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                            Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                            Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                            Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                            Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                            Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                            Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                            Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                            Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        #endregion

                                        #endregion
                                    }
                                }
                                else //else Capitulo
                                {
                                    #region (Capitulo)
                                    Cap_ID = Dr["CA_ID"].ToString().Trim();
                                    Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                    Con_ID = Dr["CO_ID"].ToString().Trim();
                                    Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                    PG_ID = Dr["PG_ID"].ToString().Trim();
                                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                    #region (Registro Capitulo)
                                    if (Niv_Cap.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
                                    }
                                    #endregion

                                    #region (Registro Concepto)
                                    if (Niv_Con.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                                    }
                                    #endregion

                                    #region (Registro Partida Generica)
                                    if (Niv_Par_Gen.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                                    }
                                    #endregion

                                    #region (Registro Partida)
                                    if (Niv_Par.Trim().Equals("S"))
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                        Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                        Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                        Fila["Tipo"] = "P";
                                        Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                        Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                        Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                        Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                        Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                        Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                        Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                        Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                        Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                        Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                        Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                        Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                        Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                        Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                        Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                        Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                        Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                        Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                        Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                        Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                        Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                        Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                        Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                        Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                        Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                        Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    #endregion

                                    #endregion
                                }
                            }
                            else //else Dependencia
                            {
                                #region (Dependencia)
                                UR_ID = Dr["UR_ID"].ToString().Trim();
                                UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                                Cap_ID = Dr["CA_ID"].ToString().Trim();
                                Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                                Con_ID = Dr["CO_ID"].ToString().Trim();
                                Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                                PG_ID = Dr["PG_ID"].ToString().Trim();
                                PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                                #region (Registro Dependencia)
                                if (Niv_UR.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Unidad_Responsable(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Dt_Psp, UR);
                                }
                                #endregion

                                #region (Registro Capitulo)
                                if (Niv_Cap.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
                                }
                                #endregion

                                #region (Registro Concepto)
                                if (Niv_Con.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                                }
                                #endregion

                                #region (Registro Partida Generica)
                                if (Niv_Par_Gen.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                                }
                                #endregion

                                #region (Registro Partida)
                                if (Niv_Par.Trim().Equals("S"))
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                    Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                    Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                    Fila["Tipo"] = "P";
                                    Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                    Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                    Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                    Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                    Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                    Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                    Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                    Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                    Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                    Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                    Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                    Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                    Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                    Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                    Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                    Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                    Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                    Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                    Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                    Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                    Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                    Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                    Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                    Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                    Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                    Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                #endregion

                                #endregion
                            }
                        }
                        else //else Programa
                        {
                            #region (Programa)

                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            UR_ID = Dr["UR_ID"].ToString().Trim();
                            UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                            Cap_ID = Dr["CA_ID"].ToString().Trim();
                            Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                            Con_ID = Dr["CO_ID"].ToString().Trim();
                            Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                            PG_ID = Dr["PG_ID"].ToString().Trim();
                            PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                            #region (Registro Programa)
                            if (Niv_PP.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Programa(Dt_Egr, FF_ID, AF_ID, PP_ID, Dt_Psp, PP);
                            }
                            #endregion

                            #region (Registro Dependencia)
                            if (Niv_UR.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Unidad_Responsable(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Dt_Psp, UR);
                            }
                            #endregion

                            #region (Registro Capitulo)
                            if (Niv_Cap.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
                            }
                            #endregion

                            #region (Registro Concepto)
                            if (Niv_Con.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                            }
                            #endregion

                            #region (Registro Partida Generica)
                            if (Niv_Par_Gen.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                            }
                            #endregion

                            #region (Registro Partida)
                            if (Niv_Par.Trim().Equals("S"))
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                                Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                                Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                                Fila["Tipo"] = "P";
                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                                Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                                Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                                Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                                Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                                Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                                Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                                Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                                Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                                Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                                Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                                Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                                Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                                Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                                Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                                Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                                Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                                Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                                Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                                Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                                Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                                Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                                Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                                Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                                Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                                Dt_Psp.Rows.Add(Fila);
                            }
                            #endregion

                            #endregion
                        }

                    }//fin if area funciinal
                    else //else area funcional
                    {
                        #region (Area Funcional)

                        AF_ID = Dr["AF_ID"].ToString().Trim();
                        AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                        PP_ID = Dr["PP_ID"].ToString().Trim();
                        PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                        UR_ID = Dr["UR_ID"].ToString().Trim();
                        UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                        Cap_ID = Dr["CA_ID"].ToString().Trim();
                        Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                        Con_ID = Dr["CO_ID"].ToString().Trim();
                        Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                        PG_ID = Dr["PG_ID"].ToString().Trim();
                        PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                        #region (Registro Area Funcional)
                        if (Niv_AF.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Area_Funcional(Dt_Egr, FF_ID, AF_ID, Dt_Psp, AF);
                        }
                        #endregion

                        #region (Registro Programa)
                        if (Niv_PP.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Programa(Dt_Egr, FF_ID, AF_ID, PP_ID, Dt_Psp, PP);
                        }
                        #endregion

                        #region (Registro Dependencia)
                        if (Niv_UR.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Unidad_Responsable(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Dt_Psp, UR);
                        }
                        #endregion

                        #region (Registro Capitulo)
                        if (Niv_Cap.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
                        }
                        #endregion

                        #region (Registro Concepto)
                        if (Niv_Con.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                        }
                        #endregion

                        #region (Registro Partida Generica)
                        if (Niv_Par_Gen.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                        }
                        #endregion

                        #region (Registro Partida)
                        if (Niv_Par.Trim().Equals("S"))
                        {
                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                            Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                            Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                            Fila["Tipo"] = "P";
                            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                            Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                            Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                            Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                            Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                            Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                            Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                            Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                            Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                            Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                            Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                            Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                            Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                            Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                            Dt_Psp.Rows.Add(Fila);
                        }
                        #endregion

                        #endregion
                    }
                }//fin if fuente financiamiento
                else //else fuente financiamiento
                {
                    #region (Fuente Financiamiento)

                    FF_ID = Dr["FF_ID"].ToString().Trim();
                    FF = "******* " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                    AF_ID = Dr["AF_ID"].ToString().Trim();
                    AF = "****** " + Dr["CLAVE_NOM_AF"].ToString().Trim();
                    PP_ID = Dr["PP_ID"].ToString().Trim();
                    PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                    UR_ID = Dr["UR_ID"].ToString().Trim();
                    UR = "**** " + Dr["CLAVE_NOM_UR"].ToString().Trim();
                    Cap_ID = Dr["CA_ID"].ToString().Trim();
                    Cap = "*** " + Dr["CLAVE_NOM_CAPITULO"].ToString().Trim();
                    Con_ID = Dr["CO_ID"].ToString().Trim();
                    Con = "** " + Dr["CLAVE_NOM_CONCEPTO"].ToString().Trim();
                    PG_ID = Dr["PG_ID"].ToString().Trim();
                    PG = "* " + Dr["CLAVE_NOM_PARTIDA_GEN"].ToString().Trim();

                    #region (Registro Fuente)
                    if (Niv_FF.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Fuente(Dt_Egr, FF_ID, Dt_Psp, FF);
                    }
                    #endregion

                    #region (Registro Area Funcional)
                    if (Niv_AF.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Area_Funcional(Dt_Egr, FF_ID, AF_ID, Dt_Psp, AF);
                    }
                    #endregion

                    #region (Registro Programa)
                    if (Niv_PP.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Programa(Dt_Egr, FF_ID, AF_ID, PP_ID, Dt_Psp, PP);
                    }
                    #endregion

                    #region (Registro Dependencia)
                    if (Niv_UR.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Unidad_Responsable(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Dt_Psp, UR);
                    }
                    #endregion

                    #region (Registro Capitulo)
                    if (Niv_Cap.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Capitulo(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Dt_Psp, Cap);
                    }
                    #endregion

                    #region (Registro Concepto)
                    if (Niv_Con.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Concepto(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, Dt_Psp, Con);
                    }
                    #endregion

                    #region (Registro Partida Generica)
                    if (Niv_Par_Gen.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Partida_Generica(Dt_Egr, FF_ID, AF_ID, PP_ID, UR_ID, Cap_ID, Con_ID, PG_ID, Dt_Psp, PG);
                    }
                    #endregion

                    #region (Registro Partida)
                    if (Niv_Par.Trim().Equals("S"))
                    {
                        Fila = Dt_Psp.NewRow();
                        Fila["Concepto"] = Dr["CLAVE_NOM_PARTIDA"].ToString().Trim();
                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["APROBADO"].ToString().Trim()) ? "0" : Dr["APROBADO"].ToString().Trim()));
                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE"].ToString().Trim()));
                        Fila["Pre_Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO"].ToString().Trim()));
                        Fila["Comprometido"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO"].ToString().Trim()));
                        Fila["Tipo"] = "P";
                        Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ENE"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ENE"].ToString().Trim()));
                        Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_FEB"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_FEB"].ToString().Trim()));
                        Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAR"].ToString().Trim()));
                        Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_ABR"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_ABR"].ToString().Trim()));
                        Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_MAY"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_MAY"].ToString().Trim()));
                        Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUN"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUN"].ToString().Trim()));
                        Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_JUL"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_JUL"].ToString().Trim()));
                        Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_AGO"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_AGO"].ToString().Trim()));
                        Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_SEP"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_SEP"].ToString().Trim()));
                        Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_OCT"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_OCT"].ToString().Trim()));
                        Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_NOV"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_NOV"].ToString().Trim()));
                        Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DISPONIBLE_DIC"].ToString().Trim()) ? "0" : Dr["DISPONIBLE_DIC"].ToString().Trim()));
                        Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ENE"].ToString().Trim()));
                        Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_FEB"].ToString().Trim()));
                        Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAR"].ToString().Trim()));
                        Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_ABR"].ToString().Trim()));
                        Fila["Pre_Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_MAY"].ToString().Trim()));
                        Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUN"].ToString().Trim()));
                        Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_JUL"].ToString().Trim()));
                        Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_AGO"].ToString().Trim()));
                        Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_SEP"].ToString().Trim()));
                        Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_OCT"].ToString().Trim()));
                        Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_NOV"].ToString().Trim()));
                        Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["PRE_COMPROMETIDO_DIC"].ToString().Trim()));
                        Fila["Comprometido_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ENE"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ENE"].ToString().Trim()));
                        Fila["Comprometido_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_FEB"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_FEB"].ToString().Trim()));
                        Fila["Comprometido_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAR"].ToString().Trim()));
                        Fila["Comprometido_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_ABR"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_ABR"].ToString().Trim()));
                        Fila["Comprometido_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_MAY"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_MAY"].ToString().Trim()));
                        Fila["Comprometido_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUN"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUN"].ToString().Trim()));
                        Fila["Comprometido_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_JUL"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_JUL"].ToString().Trim()));
                        Fila["Comprometido_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_AGO"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_AGO"].ToString().Trim()));
                        Fila["Comprometido_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_SEP"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_SEP"].ToString().Trim()));
                        Fila["Comprometido_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_OCT"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_OCT"].ToString().Trim()));
                        Fila["Comprometido_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_NOV"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_NOV"].ToString().Trim()));
                        Fila["Comprometido_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["COMPROMETIDO_DIC"].ToString().Trim()) ? "0" : Dr["COMPROMETIDO_DIC"].ToString().Trim()));
                        Dt_Psp.Rows.Add(Fila);
                    }
                    #endregion
                    #endregion
                }
            }
            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Psp_Egresos_Mensula
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 13/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Generar_Reporte_Psp_Egresos_Mensual(DataTable Dt_Egr, String Anio, String Gpo_Dep, String Dependencia)
        {
            Double Cantidad = 0.00;
            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "../../Archivos/Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls     ";
            String Nombre_Archivo = "Rpt_Presupuestos_Egreso_" + Anio + "_" + Session.SessionID + ".xls";

            //try
            //{
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                #region Estilos
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo cabecera 2 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                //Creamos el estilo cabecera 3 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Partida = Libro.Styles.Add("Nombre_Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida = Libro.Styles.Add("Cant_Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida_Neg = Libro.Styles.Add("Cant_Partida_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Totales = Libro.Styles.Add("Nombre_Totales");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales = Libro.Styles.Add("Cant_Totales");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales_Neg = Libro.Styles.Add("Cant_Totales_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Total = Libro.Styles.Add("Nombre_Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total = Libro.Styles.Add("Cant_Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total_Neg = Libro.Styles.Add("Cant_Total_Neg");

                //estilo para la cabecera
                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 12;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "Gray";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera2
                Estilo_Cabecera2.Font.FontName = "Tahoma";
                Estilo_Cabecera2.Font.Size = 10;
                Estilo_Cabecera2.Font.Bold = true;
                Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera2.Font.Color = "#FFFFFF";
                Estilo_Cabecera2.Interior.Color = "DarkGray";
                Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera3
                Estilo_Cabecera3.Font.FontName = "Tahoma";
                Estilo_Cabecera3.Font.Size = 12;
                Estilo_Cabecera3.Font.Bold = true;
                Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera3.Font.Color = "#000000";
                Estilo_Cabecera3.Interior.Color = "white";
                Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre de la partida
                Estilo_Nombre_Partida.Font.FontName = "Tahoma";
                Estilo_Nombre_Partida.Font.Size = 9;
                Estilo_Nombre_Partida.Font.Bold = false;
                Estilo_Nombre_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Partida.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Partida.Font.Color = "#000000";
                Estilo_Nombre_Partida.Interior.Color = "White";
                Estilo_Nombre_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe de la partida
                Estilo_Cant_Partida.Font.FontName = "Tahoma";
                Estilo_Cant_Partida.Font.Size = 9;
                Estilo_Cant_Partida.Font.Bold = false;
                Estilo_Cant_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Partida.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Partida.Font.Color = "#000000";
                Estilo_Cant_Partida.Interior.Color = "White";
                Estilo_Cant_Partida.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo de la partida
                Estilo_Cant_Partida_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Partida_Neg.Font.Size = 9;
                Estilo_Cant_Partida_Neg.Font.Bold = false;
                Estilo_Cant_Partida_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Partida_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Partida_Neg.Font.Color = "red";
                Estilo_Cant_Partida_Neg.Interior.Color = "White";
                Estilo_Cant_Partida_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Partida_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre del total
                Estilo_Nombre_Total.Font.FontName = "Tahoma";
                Estilo_Nombre_Total.Font.Size = 9;
                Estilo_Nombre_Total.Font.Bold = false;
                Estilo_Nombre_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Total.Font.Color = "#000000";
                Estilo_Nombre_Total.Interior.Color = "#009933";
                Estilo_Nombre_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe del total
                Estilo_Cant_Total.Font.FontName = "Tahoma";
                Estilo_Cant_Total.Font.Size = 9;
                Estilo_Cant_Total.Font.Bold = false;
                Estilo_Cant_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Total.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Total.Font.Color = "#000000";
                Estilo_Cant_Total.Interior.Color = "#009933";
                Estilo_Cant_Total.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo del total
                Estilo_Cant_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Total_Neg.Font.Size = 9;
                Estilo_Cant_Total_Neg.Font.Bold = false;
                Estilo_Cant_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Total_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Total_Neg.Font.Color = "red";
                Estilo_Cant_Total_Neg.Interior.Color = "#009933";
                Estilo_Cant_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Nombre_Totales.Font.FontName = "Tahoma";
                Estilo_Nombre_Totales.Font.Size = 9;
                Estilo_Nombre_Totales.Font.Bold = false;
                Estilo_Nombre_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Totales.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Totales.Font.Color = "#000000";
                Estilo_Nombre_Totales.Interior.Color = "#33CC33";
                Estilo_Nombre_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Cant_Totales.Font.FontName = "Tahoma";
                Estilo_Cant_Totales.Font.Size = 9;
                Estilo_Cant_Totales.Font.Bold = false;
                Estilo_Cant_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Totales.Font.Color = "#000000";
                Estilo_Cant_Totales.Interior.Color = "#33CC33";
                Estilo_Cant_Totales.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Cant_Totales_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Totales_Neg.Font.Size = 9;
                Estilo_Cant_Totales_Neg.Font.Bold = false;
                Estilo_Cant_Totales_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Totales_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Totales_Neg.Font.Color = "red";
                Estilo_Cant_Totales_Neg.Interior.Color = "#33CC33";
                Estilo_Cant_Totales_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Totales_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                #endregion

                #region (COLUMNAS)
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Presupuesto");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1 = Hoja1.Table.Rows.Add();
                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//APROBADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//AMPLIACION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//REDUCCION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//MODIFICADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//EJERCIDO. 
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PAGADO. /*11*/
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DISPONIBLE DICIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//PRECOMPROMETIDO DICIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//COMPROMETIDO DICIEMBRE.
                #endregion

                #region (Encabezado)
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add(Gpo_Dep.Trim());
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add(Dependencia.Trim());
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //encabezado3
                Renglon1 = Hoja1.Table.Rows.Add();//espacio entre el encabezado y el contenido
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("PRESUPUESTO DE EGRESOS " + Anio.Trim());
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle3";

                Renglon1 = Hoja1.Table.Rows.Add();
                //para concepto
                Celda = Renglon1.Cells.Add("CONCEPTO");
                Celda.StyleID = "HeaderStyle2";
                //para el AMPROBADO
                Celda = Renglon1.Cells.Add("APROBADO");
                Celda.StyleID = "HeaderStyle2";
                //para el AMPLIACION
                Celda = Renglon1.Cells.Add("AMPLIACIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el REDUCCION
                Celda = Renglon1.Cells.Add("REDUCCIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el MODIFICADO
                Celda = Renglon1.Cells.Add("MODIFICACIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el DISPONIBLE
                Celda = Renglon1.Cells.Add("POR EJERCER");
                Celda.StyleID = "HeaderStyle2";
                //para el PRE COMPROMETIDO
                Celda = Renglon1.Cells.Add("PRE COMPROMETIDO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO
                Celda = Renglon1.Cells.Add("COMPROMETIDO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE ENERO
                Celda = Renglon1.Cells.Add("POR EJERCER ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE FEBRERO
                Celda = Renglon1.Cells.Add("POR EJERCER FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE MARZO
                Celda = Renglon1.Cells.Add("POR EJERCER MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE ABRIL
                Celda = Renglon1.Cells.Add("POR EJERCER ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE MAYO
                Celda = Renglon1.Cells.Add("POR EJERCER MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE JUNIO
                Celda = Renglon1.Cells.Add("POR EJERCER JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE JULIO
                Celda = Renglon1.Cells.Add("POR EJERCER JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE AGOSTO
                Celda = Renglon1.Cells.Add("POR EJERCER AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("POR EJERCER SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE OCTUBRE
                Celda = Renglon1.Cells.Add("POR EJERCER OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("POR EJERCER NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR EJERCER DE DICIEMBRE
                Celda = Renglon1.Cells.Add("POR EJERCER DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";

                //para el PRECOMPROMETIDO DE ENERO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE FEBRERO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE MARZO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE ABRIL
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE MAYO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE JUNIO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE JULIO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE AGOSTO
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE OCTUBRE
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el PRECOMPROMETIDO DE DICIEMBRE
                Celda = Renglon1.Cells.Add("PRECOMPROMETIDO DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";

                //para el COMPROMETIDO DE ENERO
                Celda = Renglon1.Cells.Add("COMPROMETIDO ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE FEBRERO
                Celda = Renglon1.Cells.Add("COMPROMETIDO FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE MARZO
                Celda = Renglon1.Cells.Add("COMPROMETIDO MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE ABRIL
                Celda = Renglon1.Cells.Add("COMPROMETIDO ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE MAYO
                Celda = Renglon1.Cells.Add("COMPROMETIDO MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE JUNIO
                Celda = Renglon1.Cells.Add("COMPROMETIDO JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE JULIO
                Celda = Renglon1.Cells.Add("COMPROMETIDO JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE AGOSTO
                Celda = Renglon1.Cells.Add("COMPROMETIDO AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("COMPROMETIDO SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE OCTUBRE
                Celda = Renglon1.Cells.Add("COMPROMETIDO OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("COMPROMETIDO NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el COMPROMETIDO DE DICIEMBRE
                Celda = Renglon1.Cells.Add("COMPROMETIDO DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                #endregion

                foreach (DataRow Dr in Dt_Egr.Rows)
                {
                    Renglon1 = Hoja1.Table.Rows.Add();
                    foreach (DataColumn Columna in Dt_Egr.Columns)
                    {
                        if (Columna is DataColumn)
                        {
                            if (Dr["Tipo"].ToString().Equals("Tot"))
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Total"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Dev_Rec")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Total"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Total_Neg"));
                                        }
                                    }
                                }
                            }
                            else if (Dr["Tipo"].ToString().Equals("P"))
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Partida"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Dev_Rec")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Partida"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Partida_Neg"));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Totales"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Dev_Rec")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Totales"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Totales_Neg"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                Renglon1 = Hoja1.Table.Rows.Add();
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                Celda.MergeAcross = 43;
                Celda.StyleID = "Nombre_Partida";
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                Celda.MergeAcross = 43;
                Celda.StyleID = "Nombre_Partida";

                //Libro.Save(Server.MapPath(Ruta_Archivo.Trim()));

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            //}
            //catch (Exception Ex)
            //{
            //    Ruta_Archivo = "Error al generar el reporte mensual del presupuesto de egresos. Error[" + Ex.Message.ToString() + "]";
            //    throw new Exception("Error al generar el reporte mensual del presupuesto de egresos. Error[" + Ex.Message.ToString() + "]");
            //}
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Partida_Generica(DataTable Dt_Egr, String FF_ID, String AF_ID, String PP_ID, String UR_ID,
            String Cap_ID, String Con_ID, String PG_ID, DataTable Dt_Psp, String PG) 
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                       select Sum.Field<Decimal>("APROBADO")).Sum();
                Amp = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Com = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
                xEje = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE")).Sum();
                PreCom = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
                xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
                PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
                Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
                xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
                PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
                Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
                xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
                PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                               && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                               && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
                Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                           && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
                xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
                PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
                Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
                xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
                PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
                Com_May = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
                xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
                PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
                Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
                xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
                PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
                Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
                xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
                PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
                Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
                xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
                PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
                Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
                xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
                PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
                Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
                xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
                PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
                Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
                xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                            && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                            select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
                PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                              where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                              && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                              select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
                Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                           && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       && Sum.Field<String>("PG_ID").Trim() == PG_ID.Trim()
                           select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

                Fila = Dt_Psp.NewRow();
                Fila["Concepto"] = PG.Trim();
                Fila["Aprobado"] = String.Format("{0:n}", Est);
                Fila["Ampliacion"] = String.Format("{0:n}", Amp);
                Fila["Reduccion"] = String.Format("{0:n}", Red);
                Fila["Modificado"] = String.Format("{0:n}", Mod);
                Fila["Comprometido"] = String.Format("{0:n}", Com);
                Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
                Fila["Tipo"] = "PG_Tot";
                Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
                Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
                Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
                Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
                Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
                Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
                Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
                Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
                Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
                Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
                Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
                Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
                Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
                Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
                Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
                Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
                Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
                Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
                Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
                Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
                Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
                Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
                Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
                Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
                Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
                Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
                Dt_Psp.Rows.Add(Fila);

                return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Concepto(DataTable Dt_Egr, String FF_ID, String AF_ID, String PP_ID, String UR_ID,
            String Cap_ID, String Con_ID, DataTable Dt_Psp, String Con) 
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                    && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                      && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                           && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim() && Sum.Field<String>("CO_ID").Trim() == Con_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = Con.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "Con_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Capitulo(DataTable Dt_Egr, String FF_ID, String AF_ID, String PP_ID, String UR_ID,
                String Cap_ID, DataTable Dt_Psp, String Cap)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                    && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                      && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                           && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   && Sum.Field<String>("CA_ID").Trim() == Cap_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = Cap.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "Cap_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Unidad_Responsable(DataTable Dt_Egr, String FF_ID, String AF_ID, String PP_ID, String UR_ID,
                    DataTable Dt_Psp, String UR)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                    && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                      && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim() && Sum.Field<String>("UR_ID").Trim() == UR_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = UR.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "UR_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Programa(DataTable Dt_Egr, String FF_ID, String AF_ID, String PP_ID, DataTable Dt_Psp, String PP)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                    && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                      && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       && Sum.Field<String>("PP_ID").Trim() == PP_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = PP.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "PP_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Area_Funcional(DataTable Dt_Egr, String FF_ID, String AF_ID, DataTable Dt_Psp, String AF)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("AF_ID").Trim() == AF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = AF.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "AF_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        public DataTable Suma_Fuente(DataTable Dt_Egr, String FF_ID, DataTable Dt_Psp, String FF)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Com = 0;
            Decimal xEje = 0;
            Decimal PreCom = 0;
            Decimal xEje_Ene = 0;
            Decimal PreCom_Ene = 0;
            Decimal Com_Ene = 0;
            Decimal xEje_Feb = 0;
            Decimal PreCom_Feb = 0;
            Decimal Com_Feb = 0;
            Decimal xEje_Mar = 0;
            Decimal PreCom_Mar = 0;
            Decimal Com_Mar = 0;
            Decimal xEje_Abr = 0;
            Decimal PreCom_Abr = 0;
            Decimal Com_Abr = 0;
            Decimal xEje_May = 0;
            Decimal PreCom_May = 0;
            Decimal Com_May = 0;
            Decimal xEje_Jun = 0;
            Decimal PreCom_Jun = 0;
            Decimal Com_Jun = 0;
            Decimal xEje_Jul = 0;
            Decimal PreCom_Jul = 0;
            Decimal Com_Jul = 0;
            Decimal xEje_Ago = 0;
            Decimal PreCom_Ago = 0;
            Decimal Com_Ago = 0;
            Decimal xEje_Sep = 0;
            Decimal PreCom_Sep = 0;
            Decimal Com_Sep = 0;
            Decimal xEje_Oct = 0;
            Decimal PreCom_Oct = 0;
            Decimal Com_Oct = 0;
            Decimal xEje_Nov = 0;
            Decimal PreCom_Nov = 0;
            Decimal Com_Nov = 0;
            Decimal xEje_Dic = 0;
            Decimal PreCom_Dic = 0;
            Decimal Com_Dic = 0;
            #endregion

            Est = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("APROBADO")).Sum();
            Amp = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Com = (from Sum in Dt_Egr.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("COMPROMETIDO")).Sum();
            xEje = (from Sum in Dt_Egr.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                    select Sum.Field<Decimal>("DISPONIBLE")).Sum();
            PreCom = (from Sum in Dt_Egr.AsEnumerable()
                      where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                      select Sum.Field<Decimal>("PRE_COMPROMETIDO")).Sum();
            xEje_Ene = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ENE")).Sum();
            PreCom_Ene = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ENE")).Sum();
            Com_Ene = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ENE")).Sum();
            xEje_Feb = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_FEB")).Sum();
            PreCom_Feb = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_FEB")).Sum();
            Com_Feb = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_FEB")).Sum();
            xEje_Mar = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAR")).Sum();
            PreCom_Mar = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAR")).Sum();
            Com_Mar = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAR")).Sum();
            xEje_Abr = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_ABR")).Sum();
            PreCom_Abr = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_ABR")).Sum();
            Com_Abr = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_ABR")).Sum();
            xEje_May = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_MAY")).Sum();
            PreCom_May = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_MAY")).Sum();
            Com_May = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_MAY")).Sum();
            xEje_Jun = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUN")).Sum();
            PreCom_Jun = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUN")).Sum();
            Com_Jun = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUN")).Sum();
            xEje_Jul = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_JUL")).Sum();
            PreCom_Jul = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_JUL")).Sum();
            Com_Jul = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_JUL")).Sum();
            xEje_Ago = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_AGO")).Sum();
            PreCom_Ago = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_AGO")).Sum();
            Com_Ago = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_AGO")).Sum();
            xEje_Sep = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_SEP")).Sum();
            PreCom_Sep = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_SEP")).Sum();
            Com_Sep = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_SEP")).Sum();
            xEje_Oct = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_OCT")).Sum();
            PreCom_Oct = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_OCT")).Sum();
            Com_Oct = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_OCT")).Sum();
            xEje_Nov = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_NOV")).Sum();
            PreCom_Nov = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_NOV")).Sum();
            Com_Nov = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_NOV")).Sum();
            xEje_Dic = (from Sum in Dt_Egr.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("DISPONIBLE_DIC")).Sum();
            PreCom_Dic = (from Sum in Dt_Egr.AsEnumerable()
                          where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                          select Sum.Field<Decimal>("PRE_COMPROMETIDO_DIC")).Sum();
            Com_Dic = (from Sum in Dt_Egr.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("COMPROMETIDO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = FF.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Comprometido"] = String.Format("{0:n}", Com);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xEje);
            Fila["Tipo"] = "FF_Tot";
            Fila["Pre_Comprometido"] = String.Format("{0:n}", PreCom);
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xEje_Ene);
            Fila["Pre_Comprometido_Ene"] = String.Format("{0:n}", PreCom_Ene);
            Fila["Comprometido_Ene"] = String.Format("{0:n}", Com_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xEje_Feb);
            Fila["Pre_Comprometido_Feb"] = String.Format("{0:n}", PreCom_Feb);
            Fila["Comprometido_Feb"] = String.Format("{0:n}", Com_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xEje_Mar);
            Fila["Pre_Comprometido_Mar"] = String.Format("{0:n}", PreCom_Mar);
            Fila["Comprometido_Mar"] = String.Format("{0:n}", Com_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xEje_Abr);
            Fila["Pre_Comprometido_Abr"] = String.Format("{0:n}", PreCom_Abr);
            Fila["Comprometido_Abr"] = String.Format("{0:n}", Com_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xEje_May);
            Fila["Pre_Comprometido_May"] = String.Format("{0:n}", PreCom_May);
            Fila["Comprometido_May"] = String.Format("{0:n}", Com_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xEje_Jun);
            Fila["Pre_Comprometido_Jun"] = String.Format("{0:n}", PreCom_Jun);
            Fila["Comprometido_Jun"] = String.Format("{0:n}", Com_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xEje_Jul);
            Fila["Pre_Comprometido_Jul"] = String.Format("{0:n}", PreCom_Jul);
            Fila["Comprometido_Jul"] = String.Format("{0:n}", Com_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xEje_Ago);
            Fila["Pre_Comprometido_Ago"] = String.Format("{0:n}", PreCom_Ago);
            Fila["Comprometido_Ago"] = String.Format("{0:n}", Com_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xEje_Sep);
            Fila["Pre_Comprometido_Sep"] = String.Format("{0:n}", PreCom_Sep);
            Fila["Comprometido_Sep"] = String.Format("{0:n}", Com_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xEje_Oct);
            Fila["Pre_Comprometido_Oct"] = String.Format("{0:n}", PreCom_Oct);
            Fila["Comprometido_Oct"] = String.Format("{0:n}", Com_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xEje_Nov);
            Fila["Pre_Comprometido_Nov"] = String.Format("{0:n}", PreCom_Nov);
            Fila["Comprometido_Nov"] = String.Format("{0:n}", Com_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xEje_Dic);
            Fila["Pre_Comprometido_Dic"] = String.Format("{0:n}", PreCom_Dic);
            Fila["Comprometido_Dic"] = String.Format("{0:n}", Com_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        #endregion

    #region (Reportes Psp Ingresos Mensual)
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Reporte_Mensual_Ingresos
        ///DESCRIPCIÓN          : Metodo para obtener los datos para el reporte de ingresos mensual
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Reporte_Mensual_Ingresos(String Anio, String FF_Ing, String PP_Ing,String R,
            String T, String Cl, String Con_Ing, String SubCon, String Niv_FF_Ing, String Niv_PP_Ing,
            String Niv_Rub, String Niv_Tip, String Niv_Cla, String Niv_Con_Ing, String Niv_SubCon)
        {
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            String Json_Datos = "";
            DataTable Dt_Ing = new DataTable();
            DataTable Dt_Ing_Completo = new DataTable();
            String Json_Tot = String.Empty;

            //try
            //{
                //parametros ingresos
                Negocio.P_Anio = Anio;
                Negocio.P_Fte_Financiamiento_Ing = FF_Ing;
                Negocio.P_Programa_Ing_ID = PP_Ing;
                Negocio.P_ProgramaF_Ing_ID = String.Empty;
                Negocio.P_Rubro_ID = R;
                Negocio.P_RubroF_ID = String.Empty;
                Negocio.P_Tipo_ID = T;
                Negocio.P_TipoF_ID = String.Empty;
                Negocio.P_Clase_Ing_ID = Cl;
                Negocio.P_ClaseF_Ing_ID = String.Empty;
                Negocio.P_Concepto_Ing_ID = Con_Ing;
                Negocio.P_ConceptoF_Ing_ID = String.Empty;
                Negocio.P_SubConcepto_ID = SubCon;
                Negocio.P_SubConceptoF_ID = String.Empty;

                Dt_Ing = Negocio.Consultar_Psp_Ing();
                Dt_Ing_Completo = Generar_Dt_Psp_Ing_Mensual(Dt_Ing, Niv_FF_Ing, Niv_PP_Ing, Niv_Rub, Niv_Tip, Niv_Cla,
                    Niv_Con_Ing, Niv_SubCon);

                Json_Datos = Obtener_Param_Niveles_Ingresos(Dt_Ing_Completo, Anio, Niv_FF_Ing, Niv_PP_Ing, Niv_Rub, Niv_Tip, Niv_Cla,
                    Niv_Con_Ing, Niv_SubCon);

                return Json_Datos;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception("Error al Obtener_Nivel_Fuente_Financiamiento_Ingresos Error[" + ex.Message + "]");
        //    }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Obtener_Param_Niveles_Ingresos
        ///DESCRIPCIÓN          : Metodo para obtener los parametros de los niveles del reporte
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private String Obtener_Param_Niveles_Ingresos(DataTable Dt_Ing, String Anio, String Niv_FF_Ing, String Niv_PP_Ing,
            String Niv_Rub, String Niv_Tip, String Niv_Cla, String Niv_Con_Ing, String Niv_SubCon)
        {
            String Nom_Archivo = String.Empty;
            Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos Negocio = new Cls_Ope_Psp_Presupuesto_Negocio_Egresos_Ingresos(); //conexion con la capa de negocios
            DataTable Dt_UR = new DataTable();
            String Dependencia = String.Empty;
            String Gpo_Dep = String.Empty;

            //try
            //{
                //obtenemos la dependencia del usuario y el grupo dependencia
                Negocio.P_Busqueda = String.Empty;
                Negocio.P_Anio = Anio;
                Negocio.P_Fte_Financiamiento = String.Empty;
                Negocio.P_Area_Funcional_ID = String.Empty;
                Negocio.P_Programa_ID = String.Empty;
                Negocio.P_Tipo_Usuario = "Usuario";
                Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.Trim();

                Dt_UR = Negocio.Consultar_UR();

                if (Dt_UR != null && Dt_UR.Rows.Count > 0)
                {
                    Dependencia = Dt_UR.Rows[0]["NOM_DEP"].ToString().Trim();
                    Gpo_Dep = Dt_UR.Rows[0]["NOM_GPO_DEP"].ToString().Trim();
                }

                if (Dt_Ing != null)
                {
                    if (Dt_Ing.Rows.Count > 0)
                    {
                        Nom_Archivo = Generar_Reporte_Psp_Ingresos_Mensual(Dt_Ing, Anio, Dependencia, Gpo_Dep);
                    }
                }
                
            //}
            //catch (Exception ex)
            //{
            //    throw new Exception("Error al obtener los datos del reporte mensual de ingresos. Error[" + ex.Message + "]");
            //}
            return Nom_Archivo;
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Psp_Ing_Mensual
        ///DESCRIPCIÓN          : Metodo para generar la tabla del presupuesto de ingresos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Diciembre/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Generar_Dt_Psp_Ing_Mensual(DataTable Dt_Ing, String Niv_FF_Ing, String Niv_PP_Ing,
            String Niv_Rub, String Niv_Tip, String Niv_Cla, String Niv_Con_Ing, String Niv_SubCon) 
        {
            DataTable Dt_Psp = new DataTable();
            DataRow Fila;
            Boolean SubConcepto = false;
            Boolean Programa = false;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            try
            {
                #region(Tabla)
                    String FF_ID = Dt_Ing.Rows[0]["FF_ID"].ToString().Trim();
                    String PP_ID = Dt_Ing.Rows[0]["PP_ID"].ToString().Trim();
                    String R_ID = Dt_Ing.Rows[0]["R_ID"].ToString().Trim();
                    String T_ID = Dt_Ing.Rows[0]["T_ID"].ToString().Trim();
                    String CL_ID = Dt_Ing.Rows[0]["CL_ID"].ToString().Trim();
                    String C_ID = Dt_Ing.Rows[0]["C_ID"].ToString().Trim();
                    String SC_ID = Dt_Ing.Rows[0]["SC_ID"].ToString().Trim();

                    String FF = "****** " + Dt_Ing.Rows[0]["CLAVE_NOM_FF"].ToString().Trim();
                    String PP = "***** " + Dt_Ing.Rows[0]["CLAVE_NOM_PP"].ToString().Trim();
                    String R = "**** " + Dt_Ing.Rows[0]["CLAVE_NOM_RUBRO"].ToString().Trim();
                    String T = "*** " + Dt_Ing.Rows[0]["CLAVE_NOM_TIPO"].ToString().Trim();
                    String CL = "** " + Dt_Ing.Rows[0]["CLAVE_NOM_CLASE"].ToString().Trim();
                    String C = "* " + Dt_Ing.Rows[0]["CLAVE_NOM_CON"].ToString().Trim();
                    String SC = Dt_Ing.Rows[0]["CLAVE_NOM_SUBCON"].ToString().Trim();

                    if (!String.IsNullOrEmpty(Dt_Ing.Rows[0]["CLAVE_NOM_PP"].ToString().Trim()))
                    {
                        Programa = true;
                    }
                    if (!String.IsNullOrEmpty(Dt_Ing.Rows[0]["CLAVE_NOM_SUBCON"].ToString().Trim()))
                    {
                        SubConcepto = true;
                    }
                    else
                    {
                        C_ID = String.Empty;
                    }

                    Dt_Psp.Columns.Add("Concepto", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Aprobado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Ampliacion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Reduccion", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Modificado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar", System.Type.GetType("System.String"));    
                    Dt_Psp.Columns.Add("Devengado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Tipo", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Ene", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Feb", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Mar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Abr", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_May", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Jun", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Jul", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Ago", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Sep", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Oct", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Nov", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Por_Recaudar_Dic", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Ene", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Feb", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Mar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Abr", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_May", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Jun", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Jul", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Ago", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Sep", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Oct", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Nov", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Devengado_Dic", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Ene", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Feb", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Mar", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Abr", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_May", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Jun", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Jul", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Ago", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Sep", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Oct", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Nov", System.Type.GetType("System.String"));
                    Dt_Psp.Columns.Add("Recaudado_Dic", System.Type.GetType("System.String"));
               
                #endregion

                #region(Registro Total)
                    Fila = Dt_Psp.NewRow();
                    Fila["Concepto"] = "******* TOTAL";
                    Fila["Aprobado"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("ESTIMADO")));
                    Fila["Ampliacion"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("AMPLIACION")));
                    Fila["Reduccion"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("REDUCCION")));
                    Fila["Modificado"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("MODIFICADO")));
                    Fila["Devengado"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO")));
                    Fila["Recaudado"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO")));
                    Fila["Por_Recaudar"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR")));
                    Fila["Tipo"] = "Tot";
                    Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_ENE")));
                    Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_FEB")));
                    Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_MAR")));
                    Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_ABR")));
                    Fila["Por_Recaudar_May"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_MAY")));
                    Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_JUN")));
                    Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_JUL")));
                    Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_AGO")));
                    Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_SEP")));
                    Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_OCT")));
                    Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_NOV")));
                    Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("POR_RECAUDAR_DIC")));
                    Fila["Devengado_Ene"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_ENE")));
                    Fila["Devengado_Feb"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_FEB")));
                    Fila["Devengado_Mar"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_MAR")));
                    Fila["Devengado_Abr"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_ABR")));
                    Fila["Devengado_May"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_MAY")));
                    Fila["Devengado_Jun"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_JUN")));
                    Fila["Devengado_Jul"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_JUL")));
                    Fila["Devengado_Ago"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_AGO")));
                    Fila["Devengado_Sep"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_SEP")));
                    Fila["Devengado_Oct"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_OCT")));
                    Fila["Devengado_Nov"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_NOV")));
                    Fila["Devengado_Dic"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("DEVENGADO_DIC")));
                    Fila["Recaudado_Ene"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_ENE")));
                    Fila["Recaudado_Feb"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_FEB")));
                    Fila["Recaudado_Mar"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_MAR")));
                    Fila["Recaudado_Abr"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_ABR")));
                    Fila["Recaudado_May"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_MAY")));
                    Fila["Recaudado_Jun"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_JUN")));
                    Fila["Recaudado_Jul"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_JUL")));
                    Fila["Recaudado_Ago"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_AGO")));
                    Fila["Recaudado_Sep"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_SEP")));
                    Fila["Recaudado_Oct"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_OCT")));
                    Fila["Recaudado_Nov"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_NOV")));
                    Fila["Recaudado_Dic"] = String.Format("{0:n}", Dt_Ing.AsEnumerable().Sum(x => x.Field<Decimal>("RECAUDADO_DIC")));
                    Dt_Psp.Rows.Add(Fila);
                #endregion

                #region(Registro Fuente Financiamiento)
                    if (Niv_FF_Ing.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Fuente_Ing(FF, Dt_Ing, FF_ID, Dt_Psp);
                    }
                #endregion

                #region(Registro Programa)
                    if (Niv_PP_Ing.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Programa_Ing(PP, Dt_Ing, Programa, FF_ID, PP_ID, Dt_Psp);
                    }
                #endregion

                #region (Registro rubro)
                    if (Niv_Rub.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Rubro(R, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, Dt_Psp);
                    }
                #endregion

                #region(Registro Tipo)
                    if (Niv_Tip.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Tipo(T, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, Dt_Psp);
                    }
                #endregion

                #region(Registro Clase)
                    if (Niv_Cla.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                    }
                #endregion

                #region (Registro Concepto)
                    if (Niv_Con_Ing.Trim().Equals("S"))
                    {
                        Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                    }
                #endregion
                
                foreach (DataRow Dr in Dt_Ing.Rows)
                {
                    if (String.IsNullOrEmpty(Dr["CLAVE_NOM_PP"].ToString().Trim()))
                    { Programa = false; }
                    else { Programa = true; }
                    if (String.IsNullOrEmpty(Dr["CLAVE_NOM_SUBCON"].ToString().Trim()))
                    { SubConcepto = false; }
                    else { SubConcepto = true; }

                    if (FF_ID.Trim().Equals(Dr["FF_ID"].ToString().Trim()))
                    {
                        if (PP_ID.Trim().Equals(Dr["PP_ID"].ToString().Trim()))
                        {
                            if (R_ID.Trim().Equals(Dr["R_ID"].ToString().Trim()))
                            { 
                                if (T_ID.Trim().Equals(Dr["T_ID"].ToString().Trim()))
                                {
                                    if (CL_ID.Trim().Equals(Dr["CL_ID"].ToString().Trim()))
                                    {
                                        if (C_ID.Trim().Equals(Dr["C_ID"].ToString().Trim()))
                                        {
                                            #region (subconcepto)
                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "SC";
                                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                                Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                                Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                                Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                                Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                                Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                                Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                                Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                                Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                                Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                                Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                                Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                                Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                                Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                                Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                                Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                                Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                                Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                                Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                                Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                                Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                                Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                                Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                                Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                                Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));

                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion
                                        }
                                        else /*ELSE CONCEPTO*/
                                        {
                                            #region (Else Concepto)
                                            C_ID = Dr["C_ID"].ToString().Trim();
                                            SC_ID = Dr["SC_ID"].ToString().Trim();

                                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                            #region (Registro Concepto)
                                            if (Niv_Con_Ing.Trim().Equals("S"))
                                            {
                                                Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                                            }
                                            #endregion

                                            #region (subconcepto)
                                            if (SubConcepto)
                                            {
                                                Fila = Dt_Psp.NewRow();
                                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                                Fila["Tipo"] = "SC";
                                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                                Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                                Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                                Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                                Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                                Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                                Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                                Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                                Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                                Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                                Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                                Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                                Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                                Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                                Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                                Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                                Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                                Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                                Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                                Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                                Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                                Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                                Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                                Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                                Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                                                Dt_Psp.Rows.Add(Fila);
                                            }
                                            #endregion
                                            #endregion
                                        }/*FIN ELSE CONCEPTO*/
                                    }
                                    else /*ELSE CLASE*/
                                    {
                                        #region (Else Clase)
                                        CL_ID = Dr["CL_ID"].ToString().Trim();
                                        C_ID = Dr["C_ID"].ToString().Trim();
                                        SC_ID = Dr["SC_ID"].ToString().Trim();

                                        CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                        C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                        SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                        #region(Registro Clase)
                                        if (Niv_Cla.Trim().Equals("S"))
                                        {
                                            Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                                        }
                                        #endregion

                                        #region (Registro Concepto)
                                        if (Niv_Con_Ing.Trim().Equals("S"))
                                        {
                                            Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                                        }
                                        #endregion

                                        #region (subconcepto)
                                        if (SubConcepto)
                                        {
                                            Fila = Dt_Psp.NewRow();
                                            Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                            Fila["Tipo"] = "SC";
                                            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                            Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                            Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                            Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                            Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                            Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                            Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                            Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                            Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                            Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                            Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                            Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                            Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                            Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                            Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                            Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                            Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                            Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                            Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                            Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                            Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                            Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                            Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                            Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                            Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                            Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                                            Dt_Psp.Rows.Add(Fila);
                                        }
                                        #endregion
                                        #endregion
                                    } /*FIN ELSE CLASE*/
                                }
                                else /*ELSE TIPO*/
                                {
                                    #region (Else Tipo)
                                    T_ID = Dr["T_ID"].ToString().Trim();
                                    CL_ID = Dr["CL_ID"].ToString().Trim();
                                    C_ID = Dr["C_ID"].ToString().Trim();
                                    SC_ID = Dr["SC_ID"].ToString().Trim();

                                    T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                    CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                    C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                    SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                    #region(Registro Tipo)
                                    if (Niv_Tip.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Tipo(T, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, Dt_Psp);
                                    }
                                    #endregion

                                    #region(Registro Clase)
                                    if (Niv_Cla.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                                    }
                                    #endregion

                                    #region (Registro Concepto)
                                    if (Niv_Con_Ing.Trim().Equals("S"))
                                    {
                                        Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                                    }
                                    #endregion

                                    #region (subconcepto)
                                    if (SubConcepto)
                                    {
                                        Fila = Dt_Psp.NewRow();
                                        Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                        Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                        Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                        Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                        Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                        Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                        Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                        Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                        Fila["Tipo"] = "SC";
                                        Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                        Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                        Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                        Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                        Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                        Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                        Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                        Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                        Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                        Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                        Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                        Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                        Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                        Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                        Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                        Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                        Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                        Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                        Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                        Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                        Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                        Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                        Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                        Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                        Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                        Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                        Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                        Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                        Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                        Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                        Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                        Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                        Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                        Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                        Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                        Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                                        Dt_Psp.Rows.Add(Fila);
                                    }
                                    #endregion
                                    #endregion
                                }/* FIN ELSE TIPO*/
                            }
                            else /*ELSE RUBRO*/
                            {
                                #region(Else Rubro)
                                R_ID = Dr["R_ID"].ToString().Trim();
                                T_ID = Dr["T_ID"].ToString().Trim();
                                CL_ID = Dr["CL_ID"].ToString().Trim();
                                C_ID = Dr["C_ID"].ToString().Trim();
                                SC_ID = Dr["SC_ID"].ToString().Trim();

                                R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                                T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                                CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                                C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                                SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                                #region (Registro rubro)
                                if (Niv_Rub.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Rubro(R, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, Dt_Psp);
                                }
                                #endregion

                                #region(Registro Tipo)
                                if (Niv_Tip.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Tipo(T, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, Dt_Psp);
                                }
                                #endregion

                                #region(Registro Clase)
                                if (Niv_Cla.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                                }
                                #endregion

                                #region (Registro Concepto)
                                if (Niv_Con_Ing.Trim().Equals("S"))
                                {
                                    Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                                }
                                #endregion

                                #region (subconcepto)
                                if (SubConcepto)
                                {
                                    Fila = Dt_Psp.NewRow();
                                    Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                    Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                    Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                    Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                    Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                    Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                    Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                    Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                    Fila["Tipo"] = "SC";
                                    Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                    Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                    Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                    Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                    Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                    Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                    Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                    Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                    Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                    Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                    Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                    Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                    Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                    Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                    Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                    Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                    Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                    Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                    Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                    Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                    Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                    Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                    Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                    Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                    Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                    Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                    Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                    Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                    Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                    Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                    Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                    Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                    Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                    Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                    Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                    Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                                    Dt_Psp.Rows.Add(Fila);
                                }
                                #endregion
                                #endregion
                            } /*FIN ELSE RUBRO*/ 
                        }
                        else /*ELSE PROGRAMA*/
                        {
                            #region(Else Programa)
                            PP_ID = Dr["PP_ID"].ToString().Trim();
                            R_ID = Dr["R_ID"].ToString().Trim();
                            T_ID = Dr["T_ID"].ToString().Trim();
                            CL_ID = Dr["CL_ID"].ToString().Trim();
                            C_ID = Dr["C_ID"].ToString().Trim();
                            SC_ID = Dr["SC_ID"].ToString().Trim();

                            PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                            R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                            T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                            CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                            C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                            SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                            #region(Registro Programa)
                            if (Niv_PP_Ing.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Programa_Ing(PP, Dt_Ing, Programa, FF_ID, PP_ID, Dt_Psp);
                            }
                            #endregion

                            #region (Registro rubro)
                            if (Niv_Rub.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Rubro(R, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, Dt_Psp);
                            }
                            #endregion

                            #region(Registro Tipo)
                            if (Niv_Tip.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Tipo(T, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, Dt_Psp);
                            }
                            #endregion

                            #region(Registro Clase)
                            if (Niv_Cla.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                            }
                            #endregion

                            #region (Registro Concepto)
                            if (Niv_Con_Ing.Trim().Equals("S"))
                            {
                                Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                            }
                            #endregion

                            #region (subconcepto)
                            if (SubConcepto)
                            {
                                Fila = Dt_Psp.NewRow();
                                Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                                Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                                Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                                Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                                Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                                Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                                Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                                Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                                Fila["Tipo"] = "SC";
                                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                                Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                                Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                                Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                                Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                                Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                                Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                                Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                                Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                                Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                                Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                                Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                                Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                                Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                                Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                                Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                                Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                                Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                                Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                                Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                                Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                                Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                                Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                                Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                                Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                                Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                                Dt_Psp.Rows.Add(Fila);
                            }
                            #endregion
                            #endregion
                        } /*FIN ELSE PROGRAMA*/
                    }
                    else /*ELSE FUENTE FINANCIAMIENTO*/
                    {
                        #region(Else Fuente)
                        FF_ID = Dr["FF_ID"].ToString().Trim();
                        PP_ID = Dr["PP_ID"].ToString().Trim();
                        R_ID = Dr["R_ID"].ToString().Trim();
                        T_ID = Dr["T_ID"].ToString().Trim();
                        CL_ID = Dr["CL_ID"].ToString().Trim();
                        C_ID = Dr["C_ID"].ToString().Trim();
                        SC_ID = Dr["SC_ID"].ToString().Trim();

                        FF = "****** " + Dr["CLAVE_NOM_FF"].ToString().Trim();
                        PP = "***** " + Dr["CLAVE_NOM_PP"].ToString().Trim();
                        R = "**** " + Dr["CLAVE_NOM_RUBRO"].ToString().Trim();
                        T = "*** " + Dr["CLAVE_NOM_TIPO"].ToString().Trim();
                        CL = "** " + Dr["CLAVE_NOM_CLASE"].ToString().Trim();
                        C = "* " + Dr["CLAVE_NOM_CON"].ToString().Trim();
                        SC = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();

                        #region(Registro Fuente Financiamiento)
                        if (Niv_FF_Ing.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Fuente_Ing(FF, Dt_Ing, FF_ID, Dt_Psp);
                        }
                        #endregion

                        #region(Registro Programa)
                        if (Niv_PP_Ing.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Programa_Ing(PP, Dt_Ing, Programa, FF_ID, PP_ID, Dt_Psp);
                        }
                        #endregion

                        #region (Registro rubro)
                        if (Niv_Rub.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Rubro(R, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, Dt_Psp);
                        }
                        #endregion

                        #region(Registro Tipo)
                        if (Niv_Tip.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Tipo(T, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, Dt_Psp);
                        }
                        #endregion

                        #region(Registro Clase)
                        if (Niv_Cla.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Clase(CL, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, Dt_Psp);
                        }
                        #endregion

                        #region (Registro Concepto)
                        if (Niv_Con_Ing.Trim().Equals("S"))
                        {
                            Dt_Psp = Suma_Concepto_Ing(C, Dt_Ing, Programa, FF_ID, PP_ID, R_ID, T_ID, CL_ID, C_ID, Dt_Psp);
                        }
                        #endregion

                        #region (subconcepto)
                        if (SubConcepto)
                        {
                            Fila = Dt_Psp.NewRow();
                            Fila["Concepto"] = Dr["CLAVE_NOM_SUBCON"].ToString().Trim();
                            Fila["Aprobado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ESTIMADO"].ToString().Trim()) ? "0" : Dr["ESTIMADO"].ToString().Trim()));
                            Fila["Ampliacion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AMPLIACION"].ToString().Trim()) ? "0" : Dr["AMPLIACION"].ToString().Trim()));
                            Fila["Reduccion"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["REDUCCION"].ToString().Trim()) ? "0" : Dr["REDUCCION"].ToString().Trim()));
                            Fila["Modificado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MODIFICADO"].ToString().Trim()) ? "0" : Dr["MODIFICADO"].ToString().Trim()));
                            Fila["Devengado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO"].ToString().Trim()) ? "0" : Dr["DEVENGADO"].ToString().Trim()));
                            Fila["Recaudado"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO"].ToString().Trim()) ? "0" : Dr["RECAUDADO"].ToString().Trim()));
                            Fila["Por_Recaudar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR"].ToString().Trim()));
                            Fila["Tipo"] = "SC";
                            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ENE"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ENE"].ToString().Trim()));
                            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_FEB"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_FEB"].ToString().Trim()));
                            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAR"].ToString().Trim()));
                            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_ABR"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_ABR"].ToString().Trim()));
                            Fila["Por_Recaudar_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_MAY"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_MAY"].ToString().Trim()));
                            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUN"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUN"].ToString().Trim()));
                            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_JUL"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_JUL"].ToString().Trim()));
                            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_AGO"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_AGO"].ToString().Trim()));
                            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_SEP"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_SEP"].ToString().Trim()));
                            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_OCT"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_OCT"].ToString().Trim()));
                            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_NOV"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_NOV"].ToString().Trim()));
                            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["POR_RECAUDAR_DIC"].ToString().Trim()) ? "0" : Dr["POR_RECAUDAR_DIC"].ToString().Trim()));
                            Fila["Devengado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ENE"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ENE"].ToString().Trim()));
                            Fila["Devengado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_FEB"].ToString().Trim()) ? "0" : Dr["DEVENGADO_FEB"].ToString().Trim()));
                            Fila["Devengado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAR"].ToString().Trim()));
                            Fila["Devengado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_ABR"].ToString().Trim()) ? "0" : Dr["DEVENGADO_ABR"].ToString().Trim()));
                            Fila["Devengado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_MAY"].ToString().Trim()) ? "0" : Dr["DEVENGADO_MAY"].ToString().Trim()));
                            Fila["Devengado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUN"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUN"].ToString().Trim()));
                            Fila["Devengado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_JUL"].ToString().Trim()) ? "0" : Dr["DEVENGADO_JUL"].ToString().Trim()));
                            Fila["Devengado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_AGO"].ToString().Trim()) ? "0" : Dr["DEVENGADO_AGO"].ToString().Trim()));
                            Fila["Devengado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_SEP"].ToString().Trim()) ? "0" : Dr["DEVENGADO_SEP"].ToString().Trim()));
                            Fila["Devengado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_OCT"].ToString().Trim()) ? "0" : Dr["DEVENGADO_OCT"].ToString().Trim()));
                            Fila["Devengado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_NOV"].ToString().Trim()) ? "0" : Dr["DEVENGADO_NOV"].ToString().Trim()));
                            Fila["Devengado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DEVENGADO_DIC"].ToString().Trim()) ? "0" : Dr["DEVENGADO_DIC"].ToString().Trim()));
                            Fila["Recaudado_Ene"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ENE"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ENE"].ToString().Trim()));
                            Fila["Recaudado_Feb"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_FEB"].ToString().Trim()) ? "0" : Dr["RECAUDADO_FEB"].ToString().Trim()));
                            Fila["Recaudado_Mar"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAR"].ToString().Trim()));
                            Fila["Recaudado_Abr"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_ABR"].ToString().Trim()) ? "0" : Dr["RECAUDADO_ABR"].ToString().Trim()));
                            Fila["Recaudado_May"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_MAY"].ToString().Trim()) ? "0" : Dr["RECAUDADO_MAY"].ToString().Trim()));
                            Fila["Recaudado_Jun"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUN"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUN"].ToString().Trim()));
                            Fila["Recaudado_Jul"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_JUL"].ToString().Trim()) ? "0" : Dr["RECAUDADO_JUL"].ToString().Trim()));
                            Fila["Recaudado_Ago"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_AGO"].ToString().Trim()) ? "0" : Dr["RECAUDADO_AGO"].ToString().Trim()));
                            Fila["Recaudado_Sep"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_SEP"].ToString().Trim()) ? "0" : Dr["RECAUDADO_SEP"].ToString().Trim()));
                            Fila["Recaudado_Oct"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_OCT"].ToString().Trim()) ? "0" : Dr["RECAUDADO_OCT"].ToString().Trim()));
                            Fila["Recaudado_Nov"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_NOV"].ToString().Trim()) ? "0" : Dr["RECAUDADO_NOV"].ToString().Trim()));
                            Fila["Recaudado_Dic"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["RECAUDADO_DIC"].ToString().Trim()) ? "0" : Dr["RECAUDADO_DIC"].ToString().Trim()));
                            Dt_Psp.Rows.Add(Fila);
                        }
                        #endregion
                        #endregion
                    } /*FIN ELSE FUENTE FINANCIAMIENTO*/
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar la table de ingresos. Error[" + Ex.Message + "]");
            }
            return Dt_Psp; 
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Reporte_Psp_Ingresos_Mensual
        ///DESCRIPCIÓN          : metodo para generar el reporte en excel
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 17/Diciembre/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public String Generar_Reporte_Psp_Ingresos_Mensual(DataTable Dt_Ing, String Anio, String Dependencia, String Gpo_Dep)
        {
            Double Cantidad = 0.00;
            WorksheetCell Celda = new WorksheetCell();
            String Ruta_Archivo = "../../Reporte/Rpt_Presupuestos_Ingreso_" + Anio + "_" + Session.SessionID + ".xls     ";
            String Nombre_Archivo = "Rpt_Presupuestos_Ingreso_" + Anio + "_" + Session.SessionID + ".xls";

            //try
            //{
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                //propiedades del libro
                Libro.Properties.Title = "Reporte_Presupuesto";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                #region Estilos
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo cabecera 2 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
                //Creamos el estilo cabecera 3 para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Partida = Libro.Styles.Add("Nombre_Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida = Libro.Styles.Add("Cant_Partida");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Partida_Neg = Libro.Styles.Add("Cant_Partida_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Totales = Libro.Styles.Add("Nombre_Totales");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales = Libro.Styles.Add("Cant_Totales");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Totales_Neg = Libro.Styles.Add("Cant_Totales_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_Total = Libro.Styles.Add("Nombre_Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total = Libro.Styles.Add("Cant_Total");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_Total_Neg = Libro.Styles.Add("Cant_Total_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Nombre_SC = Libro.Styles.Add("Nombre_SC");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_SC = Libro.Styles.Add("Cant_SC");
                //Creamos el estilo contenido del presupuesto para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cant_SC_Neg = Libro.Styles.Add("Cant_SC_Neg");
                //Creamos el estilo contenido del concepto para la hoja de excel. 

                //estilo para la cabecera
                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 12;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "Gray";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera2
                Estilo_Cabecera2.Font.FontName = "Tahoma";
                Estilo_Cabecera2.Font.Size = 10;
                Estilo_Cabecera2.Font.Bold = true;
                Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera2.Font.Color = "#FFFFFF";
                Estilo_Cabecera2.Interior.Color = "DarkGray";
                Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para la cabecera3
                Estilo_Cabecera3.Font.FontName = "Tahoma";
                Estilo_Cabecera3.Font.Size = 12;
                Estilo_Cabecera3.Font.Bold = true;
                Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Cabecera3.Font.Color = "#000000";
                Estilo_Cabecera3.Interior.Color = "white";
                Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre de la partida
                Estilo_Nombre_Partida.Font.FontName = "Tahoma";
                Estilo_Nombre_Partida.Font.Size = 9;
                Estilo_Nombre_Partida.Font.Bold = false;
                Estilo_Nombre_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Partida.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Partida.Font.Color = "#000000";
                Estilo_Nombre_Partida.Interior.Color = "#CCFFCC";
                Estilo_Nombre_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe de la partida
                Estilo_Cant_Partida.Font.FontName = "Tahoma";
                Estilo_Cant_Partida.Font.Size = 9;
                Estilo_Cant_Partida.Font.Bold = false;
                Estilo_Cant_Partida.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Partida.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Partida.Font.Color = "#000000";
                Estilo_Cant_Partida.Interior.Color = "#CCFFCC";
                Estilo_Cant_Partida.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Partida.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Partida.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo de la partida
                Estilo_Cant_Partida_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Partida_Neg.Font.Size = 9;
                Estilo_Cant_Partida_Neg.Font.Bold = false;
                Estilo_Cant_Partida_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Partida_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Partida_Neg.Font.Color = "red";
                Estilo_Cant_Partida_Neg.Interior.Color = "#CCFFCC";
                Estilo_Cant_Partida_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Partida_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Partida_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre del total
                Estilo_Nombre_Total.Font.FontName = "Tahoma";
                Estilo_Nombre_Total.Font.Size = 9;
                Estilo_Nombre_Total.Font.Bold = false;
                Estilo_Nombre_Total.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Total.Font.Color = "#000000";
                Estilo_Nombre_Total.Interior.Color = "#339999";
                Estilo_Nombre_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe del total
                Estilo_Cant_Total.Font.FontName = "Tahoma";
                Estilo_Cant_Total.Font.Size = 9;
                Estilo_Cant_Total.Font.Bold = false;
                Estilo_Cant_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Total.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Total.Font.Color = "#000000";
                Estilo_Cant_Total.Interior.Color = "#339999";
                Estilo_Cant_Total.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Total.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo del total
                Estilo_Cant_Total_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Total_Neg.Font.Size = 9;
                Estilo_Cant_Total_Neg.Font.Bold = false;
                Estilo_Cant_Total_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Total_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Total_Neg.Font.Color = "red";
                Estilo_Cant_Total_Neg.Interior.Color = "#339999";
                Estilo_Cant_Total_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Total_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Total_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Nombre_Totales.Font.FontName = "Tahoma";
                Estilo_Nombre_Totales.Font.Size = 9;
                Estilo_Nombre_Totales.Font.Bold = false;
                Estilo_Nombre_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_Totales.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_Totales.Font.Color = "#000000";
                Estilo_Nombre_Totales.Interior.Color = "#66CCCC";
                Estilo_Nombre_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Cant_Totales.Font.FontName = "Tahoma";
                Estilo_Cant_Totales.Font.Size = 9;
                Estilo_Cant_Totales.Font.Bold = false;
                Estilo_Cant_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Totales.Font.Color = "#000000";
                Estilo_Cant_Totales.Interior.Color = "#66CCCC";
                Estilo_Cant_Totales.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo de los totales de fuente, area, programa, dependencia, partida generica, concepto y capitulo
                Estilo_Cant_Totales_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_Totales_Neg.Font.Size = 9;
                Estilo_Cant_Totales_Neg.Font.Bold = false;
                Estilo_Cant_Totales_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_Totales_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_Totales_Neg.Font.Color = "red";
                Estilo_Cant_Totales_Neg.Interior.Color = "#66CCCC";
                Estilo_Cant_Totales_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_Totales_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_Totales_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el nombre de la partida
                Estilo_Nombre_SC.Font.FontName = "Tahoma";
                Estilo_Nombre_SC.Font.Size = 9;
                Estilo_Nombre_SC.Font.Bold = false;
                Estilo_Nombre_SC.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Nombre_SC.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
                Estilo_Nombre_SC.Font.Color = "#000000";
                Estilo_Nombre_SC.Interior.Color = "White";
                Estilo_Nombre_SC.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Nombre_SC.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_SC.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_SC.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Nombre_SC.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe de la partida
                Estilo_Cant_SC.Font.FontName = "Tahoma";
                Estilo_Cant_SC.Font.Size = 9;
                Estilo_Cant_SC.Font.Bold = false;
                Estilo_Cant_SC.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_SC.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_SC.Font.Color = "#000000";
                Estilo_Cant_SC.Interior.Color = "White";
                Estilo_Cant_SC.NumberFormat = "#,###,##0.00";
                Estilo_Cant_SC.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_SC.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //estilo para el importe negativo de la partida
                Estilo_Cant_SC_Neg.Font.FontName = "Tahoma";
                Estilo_Cant_SC_Neg.Font.Size = 9;
                Estilo_Cant_SC_Neg.Font.Bold = false;
                Estilo_Cant_SC_Neg.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cant_SC_Neg.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cant_SC_Neg.Font.Color = "red";
                Estilo_Cant_SC_Neg.Interior.Color = "White";
                Estilo_Cant_SC_Neg.NumberFormat = "#,###,##0.00";
                Estilo_Cant_SC_Neg.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cant_SC_Neg.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                #endregion

                #region (COLUMNAS)
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja1 = Libro.Worksheets.Add("Presupuesto");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon1 = Hoja1.Table.Rows.Add();
                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(350));//Concepto.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//APROBADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//AMPLIACION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//REDUCCION.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//MODIFICADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO. 
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//POR RECAUDAR DICIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//DEVENGADO DICIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO ENERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO FEBRERO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO MARZO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO ABRIL.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO MAYO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO JUNIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO JULIO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO AGOSTO.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO SEPTIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO OCTUBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO NOVIEMBRE.
                Hoja1.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//RECAUDADO DICIEMBRE.
                #endregion

                #region (Encabezado)
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GTO.");
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add(Gpo_Dep.Trim());
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //se llena el encabezado principal
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add(Dependencia.Trim());
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle";
                //encabezado3
                Renglon1 = Hoja1.Table.Rows.Add();//espacio entre el encabezado y el contenido
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("PRONOSTICO DE INGRESOS " + Anio);
                Celda.MergeAcross = 43;
                Celda.StyleID = "HeaderStyle3";

                Renglon1 = Hoja1.Table.Rows.Add();
                //para concepto
                Celda = Renglon1.Cells.Add("CONCEPTO");
                Celda.StyleID = "HeaderStyle2";
                //para el AMPROBADO
                Celda = Renglon1.Cells.Add("AMPROBADO");
                Celda.StyleID = "HeaderStyle2";
                //para el AMPLIACION
                Celda = Renglon1.Cells.Add("AMPLIACIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el REDUCCION
                Celda = Renglon1.Cells.Add("REDUCCIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el MODIFICADO
                Celda = Renglon1.Cells.Add("MODIFICACIÓN");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR
                Celda = Renglon1.Cells.Add("POR RECAUDAR");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO
                Celda = Renglon1.Cells.Add("DEVENGADO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO
                Celda = Renglon1.Cells.Add("RECAUDADO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE ENERO
                Celda = Renglon1.Cells.Add("POR RECAUDAR ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE FEBRERO
                Celda = Renglon1.Cells.Add("POR RECAUDAR FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE MARZO
                Celda = Renglon1.Cells.Add("POR RECAUDAR MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE ABRIL
                Celda = Renglon1.Cells.Add("POR RECAUDAR ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE MAYO
                Celda = Renglon1.Cells.Add("POR RECAUDAR MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE JUNIO
                Celda = Renglon1.Cells.Add("POR RECAUDAR JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE JULIO
                Celda = Renglon1.Cells.Add("POR RECAUDAR JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE AGOSTO
                Celda = Renglon1.Cells.Add("POR RECAUDAR AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("POR RECAUDAR SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE OCTUBRE
                Celda = Renglon1.Cells.Add("POR RECAUDAR OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("POR RECAUDAR NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el POR RECAUDAR DE DICIEMBRE
                Celda = Renglon1.Cells.Add("POR RECAUDAR DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";

                //para el DEVENGADO DE ENERO
                Celda = Renglon1.Cells.Add("DEVENGADO ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE FEBRERO
                Celda = Renglon1.Cells.Add("DEVENGADO FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE MARZO
                Celda = Renglon1.Cells.Add("DEVENGADO MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE ABRIL
                Celda = Renglon1.Cells.Add("DEVENGADO ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE MAYO
                Celda = Renglon1.Cells.Add("DEVENGADO MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE JUNIO
                Celda = Renglon1.Cells.Add("DEVENGADO JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE JULIO
                Celda = Renglon1.Cells.Add("DEVENGADO JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE AGOSTO
                Celda = Renglon1.Cells.Add("DEVENGADO AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("DEVENGADO SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE OCTUBRE
                Celda = Renglon1.Cells.Add("DEVENGADO OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("DEVENGADO NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el DEVENGADO DE DICIEMBRE
                Celda = Renglon1.Cells.Add("DEVENGADO DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";

                //para el RECAUDADO DE ENERO
                Celda = Renglon1.Cells.Add("RECAUDADO ENERO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE FEBRERO
                Celda = Renglon1.Cells.Add("RECAUDADO FEBRERO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE MARZO
                Celda = Renglon1.Cells.Add("RECAUDADO MARZO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE ABRIL
                Celda = Renglon1.Cells.Add("RECAUDADO ABRIL");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE MAYO
                Celda = Renglon1.Cells.Add("RECAUDADO MAYO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE JUNIO
                Celda = Renglon1.Cells.Add("RECAUDADO JUNIO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE JULIO
                Celda = Renglon1.Cells.Add("RECAUDADO JULIO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE AGOSTO
                Celda = Renglon1.Cells.Add("RECAUDADO AGOSTO");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE SEPTIEMBRE
                Celda = Renglon1.Cells.Add("RECAUDADO SEPTIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE OCTUBRE
                Celda = Renglon1.Cells.Add("RECAUDADO OCTUBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE NOVIEMBRE
                Celda = Renglon1.Cells.Add("RECAUDADO NOVIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                //para el RECAUDADO DE DICIEMBRE
                Celda = Renglon1.Cells.Add("RECAUDADO DICIEMBRE");
                Celda.StyleID = "HeaderStyle2";
                #endregion

                foreach (DataRow Dr in Dt_Ing.Rows)
                {
                    Renglon1 = Hoja1.Table.Rows.Add();
                    foreach (DataColumn Columna in Dt_Ing.Columns)
                    {
                        if (Columna is DataColumn)
                        {
                            if (Dr["Tipo"].ToString().Equals("Tot"))
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Total"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number,  "Cant_Total"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Total_Neg"));
                                        }
                                    }
                                }
                            }
                            else if (Dr["Tipo"].ToString().Equals("C_Tot"))
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Partida"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Partida"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Partida_Neg"));
                                        }
                                    }
                                }
                            }
                            else if (Dr["Tipo"].ToString().Equals("SC"))
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_SC"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_SC"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_SC_Neg"));
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if (Columna.ColumnName.Equals("Concepto"))
                                {
                                    Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Nombre_Totales"));
                                }
                                else
                                {
                                    if (Columna.ColumnName != "Tipo" && Columna.ColumnName != "Elaboro" && Columna.ColumnName != "Dependencia"
                                        && Columna.ColumnName != "Anio" && Columna.ColumnName != "Gpo_Dependencia")
                                    {
                                        Cantidad = Convert.ToDouble(String.IsNullOrEmpty(Dr[Columna.ColumnName].ToString()) ? "0" : Dr[Columna.ColumnName]);
                                        if (Cantidad >= 0)
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Totales"));
                                        }
                                        else
                                        {
                                            Renglon1.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:n}", Cantidad), DataType.Number, "Cant_Totales_Neg"));
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
                //SE OBTIENE EL DATO DEL USUARIO QUE ELABORA EL REPORTE Y LA FECHA
                Renglon1 = Hoja1.Table.Rows.Add();
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("ELABORO: " + Cls_Sessiones.Nombre_Empleado.Trim().ToUpper());
                Celda.MergeAcross = 43;
                Celda.StyleID = "Nombre_SC";
                Renglon1 = Hoja1.Table.Rows.Add();
                Celda = Renglon1.Cells.Add("FECHA: " + String.Format("{0:dd/MMM/yyyy HH:mm:ss}", DateTime.Now));
                Celda.MergeAcross = 43;
                Celda.StyleID = "Nombre_SC";

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            //}
            //catch (Exception Ex)
            //{
            //    throw new Exception("Error al generar el reporte mensual del presupuesto de ingresos. Error[" + Ex.Message.ToString() + "]");
            //}
            return Ruta_Archivo;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Concepto_Ing(String C, DataTable Dt_Ing, bool Programa, String FF_ID, String PP_ID, String R_ID,
            String T_ID, String CL_ID, String C_ID, DataTable Dt_Psp) 
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            if (Programa)
            {
                #region(Suma Con Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                        && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                        && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim() && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }
            else
            {
                #region(Suma Sin Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                        && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                        && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           && Sum.Field<String>("C_ID").Trim() == C_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = C.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Devengado"] = String.Format("{0:n}", Dev);
            Fila["Recaudado"] = String.Format("{0:n}", Rec);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
            Fila["Tipo"] = "C_Tot";
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
            Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
            Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
            Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
            Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
            Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
            Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
            Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
            Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
            Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
            Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
            Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
            Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
            Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
            Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
            Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
            Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
            Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
            Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
            Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
            Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
            Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
            Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
            Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
            Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Clase(String CL, DataTable Dt_Ing, bool Programa, String FF_ID, String PP_ID, String R_ID,
                String T_ID, String CL_ID, DataTable Dt_Psp)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            if (Programa)
            {
                #region(Suma Con Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                        && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                        && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }
            else
            {
                #region(Suma Sin Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                        && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim() && Sum.Field<String>("CL_ID").Trim() == CL_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = CL.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Devengado"] = String.Format("{0:n}", Dev);
            Fila["Recaudado"] = String.Format("{0:n}", Rec);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
            Fila["Tipo"] = "CL_Tot";
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
            Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
            Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
            Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
            Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
            Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
            Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
            Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
            Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
            Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
            Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
            Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
            Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
            Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
            Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
            Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
            Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
            Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
            Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
            Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
            Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
            Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
            Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
            Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
            Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Tipo(String T, DataTable Dt_Ing, bool Programa, String FF_ID, String PP_ID, String R_ID,
                    String T_ID, DataTable Dt_Psp)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            if (Programa)
            {
                #region(Suma Con Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                        && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim() && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }
            else
            {
                #region(Suma Sin Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                        && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           && Sum.Field<String>("T_ID").Trim() == T_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = T.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Devengado"] = String.Format("{0:n}", Dev);
            Fila["Recaudado"] = String.Format("{0:n}", Rec);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
            Fila["Tipo"] = "T_Tot";
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
            Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
            Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
            Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
            Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
            Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
            Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
            Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
            Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
            Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
            Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
            Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
            Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
            Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
            Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
            Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
            Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
            Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
            Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
            Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
            Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
            Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
            Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
            Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
            Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Rubro(String R, DataTable Dt_Ing, bool Programa, String FF_ID, String PP_ID, String R_ID, DataTable Dt_Psp)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            if (Programa)
            {
                #region(Suma Con Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                        && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }
            else
            {
                #region(Suma Sin Programa)
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("R_ID").Trim() == R_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();
                #endregion
            }

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = R.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Devengado"] = String.Format("{0:n}", Dev);
            Fila["Recaudado"] = String.Format("{0:n}", Rec);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
            Fila["Tipo"] = "R_Tot";
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
            Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
            Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
            Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
            Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
            Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
            Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
            Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
            Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
            Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
            Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
            Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
            Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
            Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
            Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
            Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
            Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
            Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
            Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
            Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
            Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
            Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
            Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
            Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
            Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Programa_Ing(String PP, DataTable Dt_Ing, bool Programa, String FF_ID, String PP_ID, DataTable Dt_Psp)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            if (Programa)
            {
                Est = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("ESTIMADO")).Sum();
                Amp = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("AMPLIACION")).Sum();
                Red = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("REDUCCION")).Sum();
                Mod = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("MODIFICADO")).Sum();
                Dev = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO")).Sum();
                Rec = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO")).Sum();
                xRec = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
                xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
                xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
                xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
                xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
                xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
                xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
                xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
                xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
                xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
                xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
                xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
                xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                            where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                            select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
                Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
                Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
                Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
                Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
                Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
                Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
                Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
                Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
                Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
                Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
                Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
                Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
                Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
                Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
                Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
                Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
                Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
                Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
                Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
                Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
                Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
                Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
                Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
                Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                           where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim() && Sum.Field<String>("PP_ID") == PP_ID.Trim()
                           select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();

                Fila = Dt_Psp.NewRow();
                Fila["Concepto"] = PP.Trim();
                Fila["Aprobado"] = String.Format("{0:n}", Est);
                Fila["Ampliacion"] = String.Format("{0:n}", Amp);
                Fila["Reduccion"] = String.Format("{0:n}", Red);
                Fila["Modificado"] = String.Format("{0:n}", Mod);
                Fila["Devengado"] = String.Format("{0:n}", Dev);
                Fila["Recaudado"] = String.Format("{0:n}", Rec);
                Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
                Fila["Tipo"] = "PP_Tot";
                Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
                Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
                Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
                Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
                Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
                Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
                Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
                Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
                Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
                Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
                Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
                Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
                Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
                Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
                Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
                Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
                Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
                Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
                Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
                Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
                Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
                Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
                Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
                Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
                Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
                Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
                Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
                Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
                Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
                Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
                Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
                Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
                Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
                Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
                Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
                Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
                Dt_Psp.Rows.Add(Fila);
            }

            return Dt_Psp;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Suma
        ///DESCRIPCIÓN          : metodo para genrar la suma
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 09/Julio/2013
        ///*******************************************************************************
        private DataTable Suma_Fuente_Ing(String FF, DataTable Dt_Ing, String FF_ID, DataTable Dt_Psp)
        {
            DataRow Fila;

            #region (variables)
            Decimal Est = 0;
            Decimal Amp = 0;
            Decimal Red = 0;
            Decimal Mod = 0;
            Decimal Dev = 0;
            Decimal Rec = 0;
            Decimal xRec = 0;
            Decimal xRec_Ene = 0;
            Decimal xRec_Feb = 0;
            Decimal xRec_Mar = 0;
            Decimal xRec_Abr = 0;
            Decimal xRec_May = 0;
            Decimal xRec_Jun = 0;
            Decimal xRec_Jul = 0;
            Decimal xRec_Ago = 0;
            Decimal xRec_Sep = 0;
            Decimal xRec_Oct = 0;
            Decimal xRec_Nov = 0;
            Decimal xRec_Dic = 0;
            Decimal Rec_Ene = 0;
            Decimal Rec_Feb = 0;
            Decimal Rec_Mar = 0;
            Decimal Rec_Abr = 0;
            Decimal Rec_May = 0;
            Decimal Rec_Jun = 0;
            Decimal Rec_Jul = 0;
            Decimal Rec_Ago = 0;
            Decimal Rec_Sep = 0;
            Decimal Rec_Oct = 0;
            Decimal Rec_Nov = 0;
            Decimal Rec_Dic = 0;
            Decimal Dev_Ene = 0;
            Decimal Dev_Feb = 0;
            Decimal Dev_Mar = 0;
            Decimal Dev_Abr = 0;
            Decimal Dev_May = 0;
            Decimal Dev_Jun = 0;
            Decimal Dev_Jul = 0;
            Decimal Dev_Ago = 0;
            Decimal Dev_Sep = 0;
            Decimal Dev_Oct = 0;
            Decimal Dev_Nov = 0;
            Decimal Dev_Dic = 0;

            #endregion

            Est = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("ESTIMADO")).Sum();
            Amp = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("AMPLIACION")).Sum();
            Red = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("REDUCCION")).Sum();
            Mod = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("MODIFICADO")).Sum();
            Dev = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("DEVENGADO")).Sum();
            Rec = (from Sum in Dt_Ing.AsEnumerable()
                   where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                   select Sum.Field<Decimal>("RECAUDADO")).Sum();
            xRec = (from Sum in Dt_Ing.AsEnumerable()
                    where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                    select Sum.Field<Decimal>("POR_RECAUDAR")).Sum();
            xRec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_ENE")).Sum();
            xRec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_FEB")).Sum();
            xRec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_MAR")).Sum();
            xRec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_ABR")).Sum();
            xRec_May = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_MAY")).Sum();
            xRec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_JUN")).Sum();
            xRec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_JUL")).Sum();
            xRec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_AGO")).Sum();
            xRec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_SEP")).Sum();
            xRec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_OCT")).Sum();
            xRec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_NOV")).Sum();
            xRec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                        where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                        select Sum.Field<Decimal>("POR_RECAUDAR_DIC")).Sum();
            Dev_Ene = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_ENE")).Sum();
            Dev_Feb = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_FEB")).Sum();
            Dev_Mar = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_MAR")).Sum();
            Dev_Abr = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_ABR")).Sum();
            Dev_May = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_MAY")).Sum();
            Dev_Jun = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_JUN")).Sum();
            Dev_Jul = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_JUL")).Sum();
            Dev_Ago = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_AGO")).Sum();
            Dev_Sep = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_SEP")).Sum();
            Dev_Oct = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_OCT")).Sum();
            Dev_Nov = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_NOV")).Sum();
            Dev_Dic = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("DEVENGADO_DIC")).Sum();
            Rec_Ene = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_ENE")).Sum();
            Rec_Feb = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_FEB")).Sum();
            Rec_Mar = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_MAR")).Sum();
            Rec_Abr = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_ABR")).Sum();
            Rec_May = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_MAY")).Sum();
            Rec_Jun = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_JUN")).Sum();
            Rec_Jul = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_JUL")).Sum();
            Rec_Ago = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_AGO")).Sum();
            Rec_Sep = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_SEP")).Sum();
            Rec_Oct = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_OCT")).Sum();
            Rec_Nov = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_NOV")).Sum();
            Rec_Dic = (from Sum in Dt_Ing.AsEnumerable()
                       where Sum.Field<String>("FF_ID").Trim() == FF_ID.Trim()
                       select Sum.Field<Decimal>("RECAUDADO_DIC")).Sum();

            Fila = Dt_Psp.NewRow();
            Fila["Concepto"] = FF.Trim();
            Fila["Aprobado"] = String.Format("{0:n}", Est);
            Fila["Ampliacion"] = String.Format("{0:n}", Amp);
            Fila["Reduccion"] = String.Format("{0:n}", Red);
            Fila["Modificado"] = String.Format("{0:n}", Mod);
            Fila["Devengado"] = String.Format("{0:n}", Dev);
            Fila["Recaudado"] = String.Format("{0:n}", Rec);
            Fila["Por_Recaudar"] = String.Format("{0:n}", xRec);
            Fila["Tipo"] = "FF_Tot";
            Fila["Por_Recaudar_Ene"] = String.Format("{0:n}", xRec_Ene);
            Fila["Por_Recaudar_Feb"] = String.Format("{0:n}", xRec_Feb);
            Fila["Por_Recaudar_Mar"] = String.Format("{0:n}", xRec_Mar);
            Fila["Por_Recaudar_Abr"] = String.Format("{0:n}", xRec_Abr);
            Fila["Por_Recaudar_May"] = String.Format("{0:n}", xRec_May);
            Fila["Por_Recaudar_Jun"] = String.Format("{0:n}", xRec_Jun);
            Fila["Por_Recaudar_Jul"] = String.Format("{0:n}", xRec_Jul);
            Fila["Por_Recaudar_Ago"] = String.Format("{0:n}", xRec_Ago);
            Fila["Por_Recaudar_Sep"] = String.Format("{0:n}", xRec_Sep);
            Fila["Por_Recaudar_Oct"] = String.Format("{0:n}", xRec_Oct);
            Fila["Por_Recaudar_Nov"] = String.Format("{0:n}", xRec_Nov);
            Fila["Por_Recaudar_Dic"] = String.Format("{0:n}", xRec_Dic);
            Fila["Devengado_Ene"] = String.Format("{0:n}", Dev_Ene);
            Fila["Devengado_Feb"] = String.Format("{0:n}", Dev_Feb);
            Fila["Devengado_Mar"] = String.Format("{0:n}", Dev_Mar);
            Fila["Devengado_Abr"] = String.Format("{0:n}", Dev_Abr);
            Fila["Devengado_May"] = String.Format("{0:n}", Dev_May);
            Fila["Devengado_Jun"] = String.Format("{0:n}", Dev_Jun);
            Fila["Devengado_Jul"] = String.Format("{0:n}", Dev_Jul);
            Fila["Devengado_Ago"] = String.Format("{0:n}", Dev_Ago);
            Fila["Devengado_Sep"] = String.Format("{0:n}", Dev_Sep);
            Fila["Devengado_Oct"] = String.Format("{0:n}", Dev_Oct);
            Fila["Devengado_Nov"] = String.Format("{0:n}", Dev_Nov);
            Fila["Devengado_Dic"] = String.Format("{0:n}", Dev_Dic);
            Fila["Recaudado_Ene"] = String.Format("{0:n}", Rec_Ene);
            Fila["Recaudado_Feb"] = String.Format("{0:n}", Rec_Feb);
            Fila["Recaudado_Mar"] = String.Format("{0:n}", Rec_Mar);
            Fila["Recaudado_Abr"] = String.Format("{0:n}", Rec_Abr);
            Fila["Recaudado_May"] = String.Format("{0:n}", Rec_May);
            Fila["Recaudado_Jun"] = String.Format("{0:n}", Rec_Jun);
            Fila["Recaudado_Jul"] = String.Format("{0:n}", Rec_Jul);
            Fila["Recaudado_Ago"] = String.Format("{0:n}", Rec_Ago);
            Fila["Recaudado_Sep"] = String.Format("{0:n}", Rec_Sep);
            Fila["Recaudado_Oct"] = String.Format("{0:n}", Rec_Oct);
            Fila["Recaudado_Nov"] = String.Format("{0:n}", Rec_Nov);
            Fila["Recaudado_Dic"] = String.Format("{0:n}", Rec_Dic);
            Dt_Psp.Rows.Add(Fila);

            return Dt_Psp;
        }
        #endregion
}
