﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" 
CodeFile="Frm_Ope_Psp_Asignar_Tope_Presupuestal.aspx.cs" Inherits="paginas_presupuestos_Frm_Ope_Psp_Asignar_Tope_Presupuestal" 
Title="SIAC Sistema Integral Administrativo y Comercial" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script type="text/javascript">
    function Abrir_Modal_Popup() {
        $find('Busqueda_Empleados').show();
        return false;
    }
    function Cerrar_Modal_Popup() {
        $find('Busqueda_Empleados').hide();
        Limpiar_Ctlr();
        return false;
    } 
    //Metodos para limpiar los controles de la busqueda.
    function Limpiar_Ctlr(){
        $("input[id$=Txt_Busqueda_No_Empleado]").val("");
        $("input[id$=Txt_Busqueda_Nombre_Empleado]").val("");
        $("[id$=Lbl_Numero_Registros]").text("");
        $('[id$=Lbl_Error_Busqueda]').text("");
        $('[id$=Lbl_Error_Busqueda]').css("display", "none");
        $('[id$=Img_Error_Busqueda]').hide();
        $("#grid").remove();
        return false;
    } 
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true"/>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                 <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo">
                               Asignación de Límite Presupuestal y Capítulos 
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    CssClass="Img_Button" ToolTip="Nuevo" onclick="Btn_Nuevo_Click"  />   
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" 
                                    onclick="Btn_Modificar_Click"  />                                                                     
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" onclick="Btn_Salir_Click"  />
                            </td>
                            <td colspan="2">
                                <div id="Div_Busqueda" runat="server" style="width:98%;">
                                    <asp:Label ID="Lbl_Leyenda_Busqueda" runat="server" Text="Busqueda" Style="font-weight:bolder; color:White;"></asp:Label>
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="130px" MaxLength="4"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false" 
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar"  
                                        AlternateText="Consultar" onclick="Btn_Buscar_Click" />
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkText="<Año>" TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda" FilterType="Numbers" >
                                    </cc1:FilteredTextBoxExtender>  
                                 </div>  
                            </td>
                        </tr>
                    </table>
                </div>
                <center>
                  <div id="Div_Listado_Parametros" runat="server" style="width:100%;height:auto; max-height:500px; overflow:auto; vertical-align:top;">
                    <table style="width: 100%;">
                         <tr>
                            <tr>
                                <td><asp:HiddenField ID="Hdf_Parametro_ID" runat="server" /></td>
                            </tr>
                            <td>
                                <asp:GridView ID="Grid_Listado_Parametros" runat="server" CssClass="GridView_1"
                                    AutoGenerateColumns="False"  Width="96%" GridLines= "None" 
                                    onselectedindexchanged="Grid_Listado_Parametros_SelectedIndexChanged">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="PARAMETRO_ID" HeaderText="PARAMETRO_ID" SortExpression="PARAMETRO_ID" >
                                            <ItemStyle HorizontalAlign ="Left"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ANIO_PRESUPUESTAR" HeaderText="Año" SortExpression="ANIO_PRESUPUESTAR" >
                                            <ItemStyle HorizontalAlign ="Left"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_APERTURA" HeaderText="Fecha de Apertura" SortExpression="FECHA_APERTURA" DataFormatString="{0:dd/MMM/yyyy}" >
                                            <ItemStyle HorizontalAlign ="Left"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_CIERRE" HeaderText="Fecha de Cierre" SortExpression="FECHA_CIERRE" DataFormatString="{0:dd/MMM/yyyy}" >
                                            <ItemStyle HorizontalAlign ="Left"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle HorizontalAlign ="Center"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID">
                                            <ItemStyle HorizontalAlign ="Center"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />                                
                                    <AlternatingRowStyle CssClass="GridAltItem" />       
                                </asp:GridView>
                            </td>
                         </tr>
                    </table>
                  </div>
                <%--Div listado de requisiciones--%>
                  <div id="Div_Listado_Requisiciones" runat="server" style="width:100%;">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width:20%; text-align:left;">
                                Año Presupuestal
                            </td>
                            <td  style="width:25%; text-align:left;">
                                <asp:TextBox ID="Txt_Anio_Presupuestal" runat="server" Width="100%"></asp:TextBox>
                            </td>
                            <td style="width:20%; text-align:center" rowspan="2">
                                <asp:Label ID="Lbl_Tipo_Limite" runat="server" Text="* Tipo Límite"></asp:Label>
                            </td>
                            <td style=" text-align:left;" rowspan="2" >
                                <asp:RadioButtonList ID="Rbl_Tipos_Limite" runat="server" AutoPostBack= "true" OnSelectedIndexChanged="Rbl_Tipos_Limite_CheckedChanged">
                                    <asp:ListItem Text="Unidad Responsable" Value="0" Selected></asp:ListItem>
                                    <asp:ListItem Text="Usuario" Value="1"></asp:ListItem>
                                </asp:RadioButtonList>
                            </td>
                        </tr>
                        <tr>
                             <td style="width:25%; text-align:left;">
                                Fecha límite para captura
                            </td>
                            <td style="width:20%; text-align:left;">
                                <asp:TextBox ID="Txt_Fecha_Limite" runat="server" Width="98%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr runat="server" id="Tr_Empleado" >
                            <td style="width:20%; text-align:left">
                                <asp:Label ID="Lbl_Empleado" runat="server" Text="* Empleado"></asp:Label>
                            </td>
                            <td colspan="3" style="text-align:left;">
                                <asp:DropDownList ID="Cmb_Empleado" runat="server" Width= "92%"></asp:DropDownList>
                                &nbsp;
                                <asp:ImageButton ID="Btn_Buscar_Empleado" runat="server" ToolTip="Buscar Empleado" CssClass="Img_Button" TabIndex="5"
                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" OnClientClick="javascript:return Abrir_Modal_Popup();"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%; text-align:left">
                                <asp:Label ID="Lbl_Fuente_Financiamiento" runat="server" Text="* Fuente Financiamiento"></asp:Label>
                            </td>
                            <td style=" text-align:left;" colspan="3">
                               <asp:DropDownList ID="Cmb_Fuente_Financiamiento" runat="server" Width="100%" Enabled ="false">
                                    <asp:ListItem Value="">&lt;--SELECCIONE--&gt;</asp:ListItem>
                                </asp:DropDownList> 
                            </td>
                        </tr>

                        <tr runat="server" id="Tr_Programa">
                            <td style="width:20%; text-align:left;">
                                Programa
                            </td>
                            <td colspan="3" style=" text-align:left;">
                                <asp:DropDownList ID="Cmb_Programa" runat="server" Width="100%"
                                 OnSelectedIndexChanged = "Cmb_Programas_SelectedIndexChanged" AutoPostBack = "true" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        
                        <tr runat="server" id="Tr_UR">
                            <td style="width:20%; text-align:left;">
                                * Unidad Responsable
                            </td>
                            <td colspan="3" style=" text-align:left;">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%" >
                                    <asp:ListItem Value="">&lt;--SELECCIONE--&gt;</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:20%; text-align:left;">
                                * Límite Presupuestal
                            </td>
                            <td style="width:25%; text-align:left;">
                                <asp:TextBox ID="Txt_Tope_Presupuestal" runat="server" Width="100%" MaxLength="12"
                                    onBlur="$('input[id$=Txt_Tope_Presupuestal]').formatCurrency({colorize:true, region: 'es-MX'});"
                                    onClick="$('input[id$=Txt_Tope_Presupuestal]').select();"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTxb_PU" runat="server" 
                                    FilterType="Custom,Numbers"
                                    TargetControlID="Txt_Tope_Presupuestal" ValidChars=".,">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td colspan="2">
                                <asp:HiddenField id="Hf_Fte_Financiamiento" runat="server" />
                                <asp:HiddenField ID="HF_Empleado_ID" runat="server" />
                                <cc1:ModalPopupExtender ID="Mpe_Busqueda_Empleados" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Empleados"
                                    PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open" PopupDragHandleControlID="Pnl_Busqueda_Cabecera" 
                                    CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" />
                            </td>                            
                        </tr>
                    </table>
                    <asp:Panel ID="Pnl_Capitulos" runat="server"  GroupingText="* Capítulos" Width="99%" >
                      <div style="height:auto; max-height:300px; overflow:auto; width:100%; vertical-align:top;">
                        <table style="width:100%;">
                            <tr>
                                <td style="width:100%;">
                                    <asp:GridView ID="Grid_Capitulos" runat="server" style="white-space:normal"
                                        CssClass="GridView_1"
                                        AutoGenerateColumns="false" GridLines="None" Width="100%"
                                        EmptyDataText="No se encontraron capítulos"
                                        DataKeyNames="CAPITULO_ID">
                                        <Columns>
                                            <asp:TemplateField HeaderText="" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                                <ItemTemplate >
                                                    <asp:CheckBox ID="Chk_Capitulo" runat="server" />                                                
                                                </ItemTemplate >
                                                <ControlStyle Width="12px"/>
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:TemplateField>    
                                            <asp:BoundField DataField="CAPITULO_ID" HeaderText="Clave" >
                                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            </asp:BoundField>                                                                          
                                            <asp:BoundField DataField="Clave" HeaderText="Clave" Visible="True" SortExpression="Clave">
                                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                <ItemStyle HorizontalAlign="Left" Width="15%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Descripcion" HeaderText="Capítulo" Visible="True" SortExpression="Descripcion">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>                                
                                </td>
                            </tr>
                        </table>
                       </div>
                    </asp:Panel>
                    <asp:Panel ID="Pnl_Unidades_Responsables" runat="server"  GroupingText="Unidades Responsables Asignadas" Width="99%" >
                        <div style="height:auto; max-height:300px; overflow:auto; width:100%; vertical-align:top;">
                            <table style="width:98%;">
                                <tr>
                                    <td style="width:98%;">
                                        <asp:GridView ID="Grid_Unidades_Responsables" runat="server" style="white-space:normal"
                                            CssClass="GridView_1" 
                                            AutoGenerateColumns="false" GridLines="None" Width="98%"
                                            onselectedindexchanged="Grid_Unidades_Responsables_SelectedIndexChanged"
                                            EmptyDataText="No se han asignado Unidades Responsables">
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                                    <ItemStyle Width="3%" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="Fte_Financiamiento" HeaderText="Fuente Financiamiento"  SortExpression="Fte_Financiamiento">
                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="8pt" />
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" Font-Size="8pt" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CLAVE_NOMBRE" HeaderText="Descripción" SortExpression="Descripcion">
                                                    <HeaderStyle HorizontalAlign="Left" Width="49%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="49%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="DEPENDENCIA_ID" /> 
                                                <asp:BoundField DataField="LIMITE_PRESUPUESTAL" HeaderText="Limite Presupuestal" 
                                                 SortExpression="LIMITE_PRESUPUESTAL" DataFormatString="{0:n}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="16%" Font-Size="8pt"/>
                                                    <ItemStyle HorizontalAlign="Right" Width="16%" Font-Size="8pt"/>
                                                </asp:BoundField>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Eliminar_Unidad" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                            OnClick="Btn_Eliminar_Unidad_Click" CommandArgument='<%# Eval("DEPENDENCIA_ID") %>' ToolTip='<%# Eval("EMPLEADO_ID") %>'
                                                            OnClientClick="return confirm('¿Esta seguro que desea elimina el registro?');"/>
                                                    </ItemTemplate>                                            
                                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="FTE_FINANCIAMIENTO_ID" /> 
                                                <asp:BoundField DataField="PROYECTO_PROGRAMA_ID" />
                                                <asp:BoundField DataField="EMPLEADO_ID" />
                                            </Columns>
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>                                  
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </asp:Panel>                    
                  </div>
                </center>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger   ControlID="Grid_Empleados"/>
            </Triggers>
        </asp:UpdatePanel>
        <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
            style="display:none;border-style:outset;border-color:Silver;max-height:450px; background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
            <%--<asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
                style="cursor:pointer;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                            
            </asp:Panel>--%>                                                                          
                   <div style="color: #5D7B9D">
                    <table width="99%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;">
                           <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                             B&uacute;squeda: Empleados
                        </td>
                        <td align="right" style="width:10%;">
                           <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup();"/>  
                        </td>
                    </tr>
                </table>
                     <table width="100%">
                        <tr>
                            <td align="left" style="text-align: left;" >                                    
                                <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                                    <ContentTemplate>
                                    
                                        <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                            <ProgressTemplate>
                                                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                            </ProgressTemplate>
                                        </asp:UpdateProgress> 
                                                                     
                                          <table width="100%">
                                           <tr>
                                                <td colspan="2">
                                                    <table style="width:80%;">
                                                      <tr>
                                                        <td align="left">
                                                          <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                            Width="24px" Height="24px" style="display:none" />
                                                            <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                        </td>            
                                                      </tr>         
                                                    </table>  
                                                </td>
                                                <td style="width:100%" colspan="2" align="right">
                                                    <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                        ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>
                                                </td>
                                            </tr>     
                                           <tr>
                                                <td style="width:100%" colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>   
                                            <tr>
                                                <td style="width:20%;text-align:left;font-size:11px;">
                                                   No Empleado 
                                                </td>              
                                                <td style="width:30%;text-align:left;font-size:11px;">
                                                   <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                   <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                        TargetControlID="Txt_Busqueda_No_Empleado"/>
                                                    <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" 
                                                        TargetControlID ="Txt_Busqueda_No_Empleado" WatermarkText="Busqueda por No Empleado" 
                                                        WatermarkCssClass="watermarked"/>                                                                                                                                          
                                                </td> 
                                                <td style="width:20%;text-align:left;font-size:11px;">                                            
                                                </td>              
                                                <td style="width:30%;text-align:left;font-size:11px;">                                         
                                                </td>                                         
                                            </tr>                                                                                                   
                                            <tr>
                                                <td style="width:20%;text-align:left;font-size:11px;">
                                                    Nombre
                                                </td>              
                                                <td style="width:30%;text-align:left;" colspan="3">
                                                    <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                   <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                        TargetControlID="Txt_Busqueda_Nombre_Empleado" ValidChars="áéíóúÁÉÍÓÚ "/>
                                                    <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" 
                                                        TargetControlID ="Txt_Busqueda_Nombre_Empleado" WatermarkText="Busqueda por Nombre" 
                                                        WatermarkCssClass="watermarked"/>                                                                                               
                                                </td>                                         
                                            </tr>
                                            <tr>
                                                <td style="width:100%" colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>                                    
                                            <tr>
                                                <td style="width:100%;text-align:left;" colspan="4">
                                                    <center>
                                                       <asp:Button ID="Btn_Busqueda_Empleados" runat="server"  Text="Busqueda de Empleados" CssClass="button"  
                                                        CausesValidation="false" OnClick="Btn_Busqueda_Empleados_Click" Width="200px"/> 
                                                    </center>
                                                </td>                                                     
                                            </tr>
                                             <tr>
                                                <td style="width:100%" colspan="4">
                                                    <hr />
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td style="width:100%" colspan="4">
                                                  <div style="text-align:center;">
                                                        <asp:Label ID="Lbl_Numero_Registros" runat="server" Text=""/>
                                                  </div>
                                                </td>
                                            </tr>   
                                             <tr>
                                                <td style="width:100%" colspan="4">
                                                    <hr />
                                                </td>
                                            </tr> 
                                            <tr>
                                                <td colspan="4" id="grid">
                                                     <div style="overflow:auto; height:180px; vertical-align:top;">
                                                        <asp:GridView ID="Grid_Empleados" runat="server" CssClass="GridView_1" 
                                                            AutoGenerateColumns="False" GridLines="None" Width="96.5%"
                                                            onselectedindexchanged="Grid_Empleados_SelectedIndexChanged" HeaderStyle-CssClass="tblHead">
                                                            <Columns>
                                                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                                    <ItemStyle Width="7%" />
                                                                </asp:ButtonField>
                                                                <asp:BoundField DataField="Empleado_ID" HeaderText="Empleado ID" 
                                                                    Visible="True" SortExpression="Empleado_ID">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="No_Empleado" HeaderText="No Empleado" 
                                                                    Visible="True" SortExpression="No_Empleado">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Empleado" HeaderText="Nombre" 
                                                                    Visible="True" SortExpression="Empleado">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                    <ItemStyle HorizontalAlign="left" Width="60%" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                        </asp:GridView>
                                                     </div>
                                                </td>
                                            </tr>
                                             
                                                                                                                
                                          </table>                                                                                                                                                              
                                    </ContentTemplate>
                                    <Triggers>
                                        <asp:AsyncPostBackTrigger ControlID="Btn_Busqueda_Empleados" EventName="Click"/>
                                    </Triggers>                                                                   
                                </asp:UpdatePanel>
                            </td>
                        </tr>
                        <tr>
                            <td>                                                      
                            </td>
                        </tr>
                     </table>                                                   
                   </div>                 
            </asp:Panel>
    </div>
</asp:Content>

