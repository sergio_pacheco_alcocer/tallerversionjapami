﻿using System;
using JAPAMI.Sessiones;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Presupuesto_Egresos_Ingresos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
    }
}
