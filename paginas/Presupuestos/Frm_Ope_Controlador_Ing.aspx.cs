﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Presupuesto_Ingresos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Nodo_Atributos;
using JAPAMI.Nodo_Arbol;
using Newtonsoft.Json;
using System.Collections.Generic;

public partial class paginas_Presupuestos_Frm_Ope_Controlador_Ing : System.Web.UI.Page
{
    #region PAGE LOAD

        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Controlador_Inicio();
            }
        }

    #endregion

    #region METODOS

        #region (Metodos Generales)
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Controlador_Inicio
            ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Controlador_Inicio()
            {
                String Accion = String.Empty;
                String Anio = String.Empty;
                String FF_ID = String.Empty;
                String Rubro_ID = String.Empty;
                String Tipo_ID = String.Empty;
                String Clase_ID = String.Empty;
                String Concepto_ID = String.Empty;
                String SubConcepto_ID = String.Empty;
                String Json_Cadena = String.Empty;

                Response.Clear();

                try
                {
                    if (this.Request.QueryString["Accion"] != null)
                    {
                        if (!String.IsNullOrEmpty(this.Request.QueryString["Accion"].ToString().Trim()))
                        {
                            if (this.Request.QueryString["Anio"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Anio"].ToString().Trim()))
                            {
                                Anio = this.Request.QueryString["Anio"].ToString().Trim();
                            }
                            if (this.Request.QueryString["FF"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FF"].ToString().Trim()))
                            {
                                FF_ID = this.Request.QueryString["FF"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Rubro"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Rubro"].ToString().Trim()))
                            {
                                Rubro_ID = this.Request.QueryString["Rubro"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Tipo"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipo"].ToString().Trim()))
                            {
                                Tipo_ID = this.Request.QueryString["Tipo"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Clase"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Clase"].ToString().Trim()))
                            {
                                Clase_ID = this.Request.QueryString["Clase"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Concepto"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Concepto"].ToString().Trim()))
                            {
                                Concepto_ID = this.Request.QueryString["Concepto"].ToString().Trim();
                            }
                            if (this.Request.QueryString["SubConcepto"] != null && !String.IsNullOrEmpty(this.Request.QueryString["SubConcepto"].ToString().Trim()))
                            {
                                SubConcepto_ID = this.Request.QueryString["SubConcepto"].ToString().Trim();
                            }
                            if (this.Request.QueryString["FFs"] != null && !String.IsNullOrEmpty(this.Request.QueryString["FFs"].ToString().Trim()))
                            {
                                FF_ID = this.Request.QueryString["FFs"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Rubros"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Rubros"].ToString().Trim()))
                            {
                                Rubro_ID = this.Request.QueryString["Rubros"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Tipos"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Tipos"].ToString().Trim()))
                            {
                                Tipo_ID = this.Request.QueryString["Tipos"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Clases"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Clases"].ToString().Trim()))
                            {
                                Clase_ID = this.Request.QueryString["Clases"].ToString().Trim();
                            }
                            if (this.Request.QueryString["Conceptos"] != null && !String.IsNullOrEmpty(this.Request.QueryString["Conceptos"].ToString().Trim()))
                            {
                                Concepto_ID = this.Request.QueryString["Conceptos"].ToString().Trim();
                            }
                            
                            Accion = this.Request.QueryString["Accion"].ToString().Trim();
                            switch (Accion)
                            {
                                case "Anios":
                                    Json_Cadena = Obtener_Anios();
                                    break;
                                case "FF":
                                    Json_Cadena = Obtener_FF(Anio,FF_ID,Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Rubro":
                                    Json_Cadena = Obtener_Rubro(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Tipo":
                                    Json_Cadena = Obtener_Tipo(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Clase":
                                    Json_Cadena = Obtener_Clase(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Concepto":
                                    Json_Cadena = Obtener_Concepto(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "SubConcepto":
                                    Json_Cadena = Obtener_SubConcepto(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Nivel_FF":
                                    Json_Cadena = Obtener_Nivel_FF(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID, SubConcepto_ID);
                                    break;
                                case "Nivel_Rubro":
                                    Json_Cadena = Obtener_Nivel_Rubro(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Nivel_Tipo":
                                    Json_Cadena = Obtener_Nivel_Tipos(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Nivel_Clase":
                                    Json_Cadena = Obtener_Nivel_Clases(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID);
                                    break;
                                case "Nivel_Concepto":
                                    Json_Cadena = Obtener_Nivel_Conceptos(Anio, FF_ID, Rubro_ID, Tipo_ID, Clase_ID, Concepto_ID, SubConcepto_ID);
                                    break;
                            }
                        }
                    }
                    Response.ContentType = "application/json";
                    Response.Write(Json_Cadena);
                    Response.Flush();
                    Response.Close();
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Controlador_Inicio Error[" + ex.Message + "]");
                }
            }
         #endregion

        #region METODOS JSON
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Anios
            ///DESCRIPCIÓN          : Metodo para obtener los años presupuestados aprobados
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Anios()
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Anios = string.Empty;
                DataTable Dt_Anios = new DataTable();

                try
                {
                    Dt_Anios = Negocio.Consultar_Anios();
                    Dt_Anios.TableName = "anios";
                    if (Dt_Anios != null)
                    {
                        if (Dt_Anios.Rows.Count > 0)
                        {
                            Json_Anios = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_Anios);
                        }
                    }
                    return Json_Anios;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Anios Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_FF(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_FF = string.Empty;
                DataTable Dt_FF = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_FF = Negocio.Consultar_FF();
                    Dt_FF.TableName = "ff";
                    if (Dt_FF != null)
                    {
                        if (Dt_FF.Rows.Count > 0)
                        {
                            Json_FF = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_FF);
                        }
                    }
                    return Json_FF;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Fuentes de financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Rubro
            ///DESCRIPCIÓN          : Metodo para obtener los rubros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Rubro(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_R = string.Empty;
                DataTable Dt_R = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_R = Negocio.Consultar_Rubros();
                    Dt_R.TableName = "rubro";
                    if (Dt_R != null)
                    {
                        if (Dt_R.Rows.Count > 0)
                        {
                            Json_R = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_R);
                        }
                    }
                    return Json_R;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Tipo
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Tipo(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_T = string.Empty;
                DataTable Dt_T = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_T = Negocio.Consultar_Tipos();
                    Dt_T.TableName = "tipo";
                    if (Dt_T != null)
                    {
                        if (Dt_T.Rows.Count > 0)
                        {
                            Json_T = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_T);
                        }
                    }
                    return Json_T;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los tipos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Clase
            ///DESCRIPCIÓN          : Metodo para obtener las clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Clase(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_C = string.Empty;
                DataTable Dt_C = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_C = Negocio.Consultar_Clases();
                    Dt_C.TableName = "clase";
                    if (Dt_C != null)
                    {
                        if (Dt_C.Rows.Count > 0)
                        {
                            Json_C = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_C);
                        }
                    }
                    return Json_C;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener las clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Concepto
            ///DESCRIPCIÓN          : Metodo para obtener las conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 25/Abril/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Concepto(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_C = string.Empty;
                DataTable Dt_C = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_C = Negocio.Consultar_Conceptos();
                    Dt_C.TableName = "concepto";
                    if (Dt_C != null)
                    {
                        if (Dt_C.Rows.Count > 0)
                        {
                            Json_C = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_C);
                        }
                    }
                    return Json_C;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_SubConcepto
            ///DESCRIPCIÓN          : Metodo para obtener las subconceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 19/Mayo/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_SubConcepto(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_C = string.Empty;
                DataTable Dt_C = new DataTable();

                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Dt_C = Negocio.Consultar_SubConceptos();
                    Dt_C.TableName = "concepto";
                    if (Dt_C != null)
                    {
                        if (Dt_C.Rows.Count > 0)
                        {
                            Json_C = Ayudante_JQuery.Crear_Tabla_Formato_JSON(Dt_C);
                        }
                    }
                    return Json_C;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al obtener los conceptos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_FF
            ///DESCRIPCIÓN          : Metodo para obtener las fuentes de financiamiento
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_FF(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID, String SubConcepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Fte_Financiamiento = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Fte_Financiamiento = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                String Json_Total = String.Empty;
                String Json = string.Empty;
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Negocio.P_SubConcepto_ID = SubConcepto_ID; 
                    Dt_Fte_Financiamiento = Negocio.Consultar_FF_Ing();
                    //Json_Total = Obtener_Total(Dt_Fte_Financiamiento);

                    if (Dt_Fte_Financiamiento != null)
                    {
                        if (Dt_Fte_Financiamiento.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Fte_Financiamiento.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "closed";
                                Nodo_Arbol.descripcion1 = String.Format("{0:c}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:c}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:c}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:c}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:c}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:c}", Dr["POR_RECAUDAR"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_FF";
                                Atributos.valor2 = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Atributos.valor3 = "";
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Fte_Financiamiento = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);

                            if (!String.IsNullOrEmpty(Json_Total))
                            {
                                Json = Json_Fte_Financiamiento.Substring(0, Json_Fte_Financiamiento.Length - 1); ;
                                Json += "," + Json_Total.Substring(1, Json_Total.Length - 2);
                                Json += "]";
                            }
                            else
                            {
                                Json = Json_Fte_Financiamiento;
                            }
                        }
                    }

                    return Json;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Fuente_Financiamiento Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Rubro
            ///DESCRIPCIÓN          : Metodo para obtener los rubros
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Rubro(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;

                    Dt_Datos = Negocio.Consultar_Rubros_Ing();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "closed";
                                Nodo_Arbol.descripcion1 = String.Format("{0:c}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:c}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:c}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:c}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:c}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:c}", Dr["POR_RECAUDAR"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Rubro";
                                Atributos.valor2 = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor4 = "";
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Rubros Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Tipos
            ///DESCRIPCIÓN          : Metodo para obtener los tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Tipos(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;

                    Dt_Datos = Negocio.Consultar_Tipos_Ing();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "closed";
                                Nodo_Arbol.descripcion1 = String.Format("{0:c}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:c}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:c}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:c}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:c}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:c}", Dr["POR_RECAUDAR"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Tipo";
                                Atributos.valor2 = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor5 = "";
                                Atributos.valor6 = "";
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Tipo Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Clases
            ///DESCRIPCIÓN          : Metodo para obtener las clases
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Clases(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;

                    Dt_Datos = Negocio.Consultar_Clases_Ing();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "closed";
                                Nodo_Arbol.descripcion1 = String.Format("{0:c}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:c}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:c}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:c}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:c}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:c}", Dr["POR_RECAUDAR"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Clase";
                                Atributos.valor2 = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor6 = "";
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Clases Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Nivel_Conceptos
            ///DESCRIPCIÓN          : Metodo para obtener los conceptos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 27/Abril/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private String Obtener_Nivel_Conceptos(String Anio, String FF_ID, String Rubro_ID, String Tipo_ID, String Clase_ID, String Concepto_ID, String SubConcepto_ID)
            {
                Cls_Ope_Psp_Presupuesto_Ingresos_Negocio Negocio = new Cls_Ope_Psp_Presupuesto_Ingresos_Negocio(); //conexion con la capa de negocios
                String Json_Datos = "{[]}"; //aki almacenaremos el json de las fuentes de financiamiento
                DataTable Dt_Datos = new DataTable(); // dt donde guardaremos las fuentes de financiamiento
                Cls_Nodo_Arbol Nodo_Arbol = new Cls_Nodo_Arbol(); // Objero de la clase de nodo arbol
                List<Cls_Nodo_Arbol> Lista_Nodo_Arbol = new List<Cls_Nodo_Arbol>();
                Cls_Atributos_TreeGrid Atributos = new Cls_Atributos_TreeGrid();
                JsonSerializerSettings Configuracion_Json = new JsonSerializerSettings();
                Configuracion_Json.NullValueHandling = NullValueHandling.Ignore;
                try
                {
                    Negocio.P_Anio = Anio;
                    Negocio.P_Fte_Financiamiento = FF_ID;
                    Negocio.P_Rubro_ID = Rubro_ID;
                    Negocio.P_Tipo_ID = Tipo_ID;
                    Negocio.P_Clase_ID = Clase_ID;
                    Negocio.P_Concepto_ID = Concepto_ID;
                    Negocio.P_SubConcepto_ID = SubConcepto_ID;

                    Dt_Datos = Negocio.Consultar_Conceptos_Ing();

                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Dr in Dt_Datos.Rows)
                            {
                                Nodo_Arbol = new Cls_Nodo_Arbol();

                                Nodo_Arbol.id = Dr["ID"].ToString().Trim();
                                Nodo_Arbol.texto = Dr["CLAVE_NOMBRE"].ToString().Trim();
                                Nodo_Arbol.state = "opened";
                                Nodo_Arbol.descripcion1 = String.Format("{0:c}", Dr["ESTIMADO"]);
                                Nodo_Arbol.descripcion2 = String.Format("{0:c}", Dr["AMPLIACION"]);
                                Nodo_Arbol.descripcion3 = String.Format("{0:c}", Dr["REDUCCION"]);
                                Nodo_Arbol.descripcion4 = String.Format("{0:c}", Dr["MODIFICADO"]);
                                Nodo_Arbol.descripcion5 = String.Format("{0:c}", Dr["RECAUDADO"]);
                                Nodo_Arbol.descripcion6 = String.Format("{0:c}", Dr["POR_RECAUDAR"]);

                                //Agregamos los atributos
                                Atributos = new Cls_Atributos_TreeGrid();
                                Atributos.valor1 = "Nivel_Rubro";
                                Atributos.valor2 = Dr["FUENTE_FINANCIAMIENTO_ID"].ToString().Trim();
                                Atributos.valor3 = Dr["RUBRO_ID"].ToString().Trim();
                                Atributos.valor4 = Dr["TIPO_ID"].ToString().Trim();
                                Atributos.valor5 = Dr["CLASE_ING_ID"].ToString().Trim();
                                Atributos.valor6 = Dr["CONCEPTO_ING_ID"].ToString().Trim();
                                Atributos.valor7 = "";
                                Atributos.valor8 = "";

                                Nodo_Arbol.attributes = Atributos;
                                Lista_Nodo_Arbol.Add(Nodo_Arbol);
                            }
                            Json_Datos = JsonConvert.SerializeObject(Lista_Nodo_Arbol, Formatting.Indented, Configuracion_Json);
                        }
                    }

                    return Json_Datos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Obtener_Nivel_Conceptos Error[" + ex.Message + "]");
                }
            }
        #endregion

    #endregion
}
