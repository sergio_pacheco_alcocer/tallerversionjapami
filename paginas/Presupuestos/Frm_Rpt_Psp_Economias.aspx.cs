﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using JAPAMI.Rpt_Psp_Economias.Negocio;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;

public partial class paginas_Presupuestos_Frm_Rpt_Psp_Economias : System.Web.UI.Page
{
    #region (Page Load)

    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PARAMETROS           : 
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 23/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Reporte_Inicio();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte de economias. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Reporte_Sueldos_Inicio
        ///DESCRIPCIÓN          : Metodo de inicio de la página
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Reporte_Inicio()
        {
            try
            {
                Div_Contenedor_Msj_Error.Visible = false;
                Limpiar_Controles("Todo");
                Llenar_Combo_Anios();
                Llenar_Combo_FF();
                Llenar_Combo_Programa();
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
                Cmb_Anio.SelectedIndex = -1;
                Cmb_FF.SelectedIndex = -1;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del reporte de economias. Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
        ///PARAMETROS           1 Accion: para indicar que parte del codigo limpiara 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Limpiar_Controles(String Accion)
        {
            try
            {
                switch (Accion)
                {
                    case "Todo":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        Cmb_Anio.SelectedIndex = -1;
                        Cmb_FF.SelectedIndex = -1;
                        Cmb_UR.SelectedIndex = -1;
                        Cmb_Programa.SelectedIndex = -1;
                        Cmb_Partida.SelectedIndex = -1;
                        break;
                    case "Error":
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "";
                        break;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anios
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los años
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_Anios()
        {
            Cls_Rpt_Psp_Economias_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Economias_Negocio();
            DataTable Dt_Anios = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Anio.Items.Clear();

                Dt_Anios = Obj_Ingresos.Consultar_Anios();

                Cmb_Anio.DataValueField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataTextField = Ope_Psp_Pronostico_Ingresos.Campo_Anio;
                Cmb_Anio.DataSource = Dt_Anios;
                Cmb_Anio.DataBind();

                Cmb_Anio.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de anios: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_FF
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las fuentes de financiamiento
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_FF()
        {
            Cls_Rpt_Psp_Economias_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Economias_Negocio();
            DataTable Dt_FF = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_FF.Items.Clear();

                if (Cmb_Anio.SelectedIndex > 0) { Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Anio = String.Empty; }

                Obj_Ingresos.P_Programa_ID = String.Empty;
                Obj_Ingresos.P_Dependencia_ID = String.Empty; 
                Obj_Ingresos.P_Partida_ID = String.Empty;

                Dt_FF = Obj_Ingresos.Consultar_FF();

                Cmb_FF.DataValueField = Cat_SAP_Fuente_Financiamiento.Campo_Fuente_Financiamiento_ID;
                Cmb_FF.DataTextField = "CLAVE_NOMBRE";
                Cmb_FF.DataSource = Dt_FF;
                Cmb_FF.DataBind();

                Cmb_FF.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de Fuentes de fianciamiento: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_UR
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las unidades responsables
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_UR()
        {
            Cls_Rpt_Psp_Economias_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Economias_Negocio();
            DataTable Dt = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_UR.Items.Clear();

                if (Cmb_Anio.SelectedIndex > 0) { Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Anio = String.Empty; }

                if (Cmb_Programa.SelectedIndex > 0) { Obj_Ingresos.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Programa_ID = String.Empty; }

                if (Cmb_FF.SelectedIndex > 0) { Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Fte_Financiamiento_ID = String.Empty; }

                Obj_Ingresos.P_Partida_ID = String.Empty;

                Dt = Obj_Ingresos.Consultar_UR();

                Cmb_UR.DataValueField = Cat_Dependencias.Campo_Dependencia_ID ;
                Cmb_UR.DataTextField = "CLAVE_NOMBRE";
                Cmb_UR.DataSource = Dt;
                Cmb_UR.DataBind();

                Cmb_UR.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de unidades responsables: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Programa
        ///DESCRIPCIÓN          : Metodo para llenar el combo de los progrmas
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_Programa()
        {
            Cls_Rpt_Psp_Economias_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Economias_Negocio();
            DataTable Dt = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Programa.Items.Clear();

                if (Cmb_Anio.SelectedIndex > 0) { Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Anio = String.Empty; }

                if (Cmb_FF.SelectedIndex > 0) { Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Fte_Financiamiento_ID = String.Empty; }

                Obj_Ingresos.P_Partida_ID = String.Empty;
                Obj_Ingresos.P_Dependencia_ID = String.Empty;

                Dt = Obj_Ingresos.Consultar_Programas();

                Cmb_Programa.DataValueField = Cat_Sap_Proyectos_Programas.Campo_Proyecto_Programa_Id;
                Cmb_Programa.DataTextField = "CLAVE_NOMBRE";
                Cmb_Programa.DataSource = Dt;
                Cmb_Programa.DataBind();

                Cmb_Programa.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de programas: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
        ///DESCRIPCIÓN          : Metodo para llenar el combo de las partidas
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Combo_Partidas()
        {
            Cls_Rpt_Psp_Economias_Negocio Obj_Ingresos = new Cls_Rpt_Psp_Economias_Negocio();
            DataTable Dt = new DataTable(); //Para almacenar los datos de los tipos de nominas

            try
            {
                Cmb_Partida.Items.Clear();

                if (Cmb_Anio.SelectedIndex > 0) { Obj_Ingresos.P_Anio = Cmb_Anio.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Anio = String.Empty; }

                if (Cmb_FF.SelectedIndex > 0) { Obj_Ingresos.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Fte_Financiamiento_ID = String.Empty; }

                if (Cmb_Programa.SelectedIndex > 0) { Obj_Ingresos.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Programa_ID = String.Empty; }

                if (Cmb_UR.SelectedIndex > 0) { Obj_Ingresos.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim(); }
                else { Obj_Ingresos.P_Dependencia_ID = String.Empty; }

                Dt = Obj_Ingresos.Consultar_Partidas();

                Cmb_Partida.DataValueField = Cat_Sap_Partidas_Especificas.Campo_Partida_ID;
                Cmb_Partida.DataTextField = "CLAVE_NOMBRE";
                Cmb_Partida.DataSource = Dt;
                Cmb_Partida.DataBind();

                Cmb_Partida.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el combo de partidas: Error[" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Dt_Economias
        ///DESCRIPCIÓN          : Metodo para generar la tabla de economias
        ///PARAMETROS           1: Dt tabla que contiene los registros de las economias 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private DataTable Generar_Dt_Economias(DataTable Dt)
        {
            DataTable Dt_Datos = new DataTable();
            String FF_ID = String.Empty;
            String AF_ID = String.Empty;
            String P_ID = String.Empty;
            String UR_ID = String.Empty;
            DataRow Fila;

            Double Tot_Ene = 0.00;
            Double Tot_Feb = 0.00;
            Double Tot_Mar = 0.00;
            Double Tot_Abr = 0.00;
            Double Tot_May = 0.00;
            Double Tot_Jun = 0.00;
            Double Tot_Jul = 0.00;
            Double Tot_Ago = 0.00;
            Double Tot_Sep = 0.00;
            Double Tot_Oct = 0.00;
            Double Tot_Nov = 0.00;
            Double Tot_Dic = 0.00;

            Double Tot_FF_Ene = 0.00;
            Double Tot_FF_Feb = 0.00;
            Double Tot_FF_Mar = 0.00;
            Double Tot_FF_Abr = 0.00;
            Double Tot_FF_May = 0.00;
            Double Tot_FF_Jun = 0.00;
            Double Tot_FF_Jul = 0.00;
            Double Tot_FF_Ago = 0.00;
            Double Tot_FF_Sep = 0.00;
            Double Tot_FF_Oct = 0.00;
            Double Tot_FF_Nov = 0.00;
            Double Tot_FF_Dic = 0.00;

            Double Tot_AF_Ene = 0.00;
            Double Tot_AF_Feb = 0.00;
            Double Tot_AF_Mar = 0.00;
            Double Tot_AF_Abr = 0.00;
            Double Tot_AF_May = 0.00;
            Double Tot_AF_Jun = 0.00;
            Double Tot_AF_Jul = 0.00;
            Double Tot_AF_Ago = 0.00;
            Double Tot_AF_Sep = 0.00;
            Double Tot_AF_Oct = 0.00;
            Double Tot_AF_Nov = 0.00;
            Double Tot_AF_Dic = 0.00;

            Double Tot_P_Ene = 0.00;
            Double Tot_P_Feb = 0.00;
            Double Tot_P_Mar = 0.00;
            Double Tot_P_Abr = 0.00;
            Double Tot_P_May = 0.00;
            Double Tot_P_Jun = 0.00;
            Double Tot_P_Jul = 0.00;
            Double Tot_P_Ago = 0.00;
            Double Tot_P_Sep = 0.00;
            Double Tot_P_Oct = 0.00;
            Double Tot_P_Nov = 0.00;
            Double Tot_P_Dic = 0.00;

            Double Tot_UR_Ene = 0.00;
            Double Tot_UR_Feb = 0.00;
            Double Tot_UR_Mar = 0.00;
            Double Tot_UR_Abr = 0.00;
            Double Tot_UR_May = 0.00;
            Double Tot_UR_Jun = 0.00;
            Double Tot_UR_Jul = 0.00;
            Double Tot_UR_Ago = 0.00;
            Double Tot_UR_Sep = 0.00;
            Double Tot_UR_Oct = 0.00;
            Double Tot_UR_Nov = 0.00;
            Double Tot_UR_Dic = 0.00;

            try
            {
                Dt_Datos.Columns.Add("CLASIFICACION POR OBJETO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("ENERO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("FEBRERO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("MARZO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("ABRIL", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("MAYO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("JUNIO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("JULIO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("AGOSTO", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("SEPTIEMBRE", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("OCTUBRE", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("NOVIEMBRE", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("DICIEMBRE", System.Type.GetType("System.String"));
                Dt_Datos.Columns.Add("TIPO", System.Type.GetType("System.String"));

                FF_ID = Dt.Rows[0]["FTE_FINANCIAMIENTO_ID"].ToString().Trim();
                AF_ID = Dt.Rows[0]["AREA_FUNCIONAL_ID"].ToString().Trim();
                P_ID = Dt.Rows[0]["PROYECTO_PROGRAMA_ID"].ToString().Trim();
                UR_ID = Dt.Rows[0]["DEPENDENCIA_ID"].ToString().Trim();

                Fila = Dt_Datos.NewRow();
                Fila["CLASIFICACION POR OBJETO"] = "***** Total";
                Fila["ENERO"] = "0";
                Fila["FEBRERO"] = "0";
                Fila["MARZO"] = "0";
                Fila["ABRIL"] = "0";
                Fila["MAYO"] = "0";
                Fila["JUNIO"] = "0";
                Fila["JULIO"] = "0";
                Fila["AGOSTO"] = "0";
                Fila["SEPTIEMBRE"] = "0";
                Fila["OCTUBRE"] = "0";
                Fila["NOVIEMBRE"] = "0";
                Fila["DICIEMBRE"] = "0";
                Fila["TIPO"] = "Total";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["CLASIFICACION POR OBJETO"] = "**** " + Dt.Rows[0]["CLAVE_NOMBRE_FF"].ToString().Trim();
                Fila["ENERO"] = "0";
                Fila["FEBRERO"] = "0";
                Fila["MARZO"] = "0";
                Fila["ABRIL"] = "0";
                Fila["MAYO"] = "0";
                Fila["JUNIO"] = "0";
                Fila["JULIO"] = "0";
                Fila["AGOSTO"] = "0";
                Fila["SEPTIEMBRE"] = "0";
                Fila["OCTUBRE"] = "0";
                Fila["NOVIEMBRE"] = "0";
                Fila["DICIEMBRE"] = "0";
                Fila["TIPO"] = "FF";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["CLASIFICACION POR OBJETO"] = "*** " + Dt.Rows[0]["CLAVE_NOMBRE_AF"].ToString().Trim();
                Fila["ENERO"] = "0";
                Fila["FEBRERO"] = "0";
                Fila["MARZO"] = "0";
                Fila["ABRIL"] = "0";
                Fila["MAYO"] = "0";
                Fila["JUNIO"] = "0";
                Fila["JULIO"] = "0";
                Fila["AGOSTO"] = "0";
                Fila["SEPTIEMBRE"] = "0";
                Fila["OCTUBRE"] = "0";
                Fila["NOVIEMBRE"] = "0";
                Fila["DICIEMBRE"] = "0";
                Fila["TIPO"] = "AF";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["CLASIFICACION POR OBJETO"] = "** " + Dt.Rows[0]["CLAVE_NOMBRE_PROGRAMA"].ToString().Trim(); 
                Fila["ENERO"] = "0";
                Fila["FEBRERO"] = "0";
                Fila["MARZO"] = "0";
                Fila["ABRIL"] = "0";
                Fila["MAYO"] = "0";
                Fila["JUNIO"] = "0";
                Fila["JULIO"] = "0";
                Fila["AGOSTO"] = "0";
                Fila["SEPTIEMBRE"] = "0";
                Fila["OCTUBRE"] = "0";
                Fila["NOVIEMBRE"] = "0";
                Fila["DICIEMBRE"] = "0";
                Fila["TIPO"] = "PP";
                Dt_Datos.Rows.Add(Fila);

                Fila = Dt_Datos.NewRow();
                Fila["CLASIFICACION POR OBJETO"] = "* " + Dt.Rows[0]["CLAVE_NOMBRE_UR"].ToString().Trim();
                Fila["ENERO"] = "0";
                Fila["FEBRERO"] = "0";
                Fila["MARZO"] = "0";
                Fila["ABRIL"] = "0";
                Fila["MAYO"] = "0";
                Fila["JUNIO"] = "0";
                Fila["JULIO"] = "0";
                Fila["AGOSTO"] = "0";
                Fila["SEPTIEMBRE"] = "0";
                Fila["OCTUBRE"] = "0";
                Fila["NOVIEMBRE"] = "0";
                Fila["DICIEMBRE"] = "0";
                Fila["TIPO"] = "UR";
                Dt_Datos.Rows.Add(Fila);

                foreach (DataRow Dr in Dt.Rows)
                {
                    Tot_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                    Tot_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                    Tot_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                    Tot_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                    Tot_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                    Tot_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                    Tot_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                    Tot_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                    Tot_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                    Tot_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                    Tot_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                    Tot_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                    if (FF_ID.Equals(Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim()))
                    {
                        Tot_FF_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                        Tot_FF_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                        Tot_FF_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                        Tot_FF_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                        Tot_FF_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                        Tot_FF_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                        Tot_FF_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                        Tot_FF_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                        Tot_FF_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                        Tot_FF_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                        Tot_FF_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                        Tot_FF_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                        if (AF_ID.Equals(Dr["AREA_FUNCIONAL_ID"].ToString().Trim()))
                        {
                            Tot_AF_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                            Tot_AF_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                            Tot_AF_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                            Tot_AF_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                            Tot_AF_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                            Tot_AF_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                            Tot_AF_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                            Tot_AF_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                            Tot_AF_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                            Tot_AF_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                            Tot_AF_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                            Tot_AF_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                            if (P_ID.Equals(Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim()))
                            {
                                Tot_P_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                                Tot_P_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                                Tot_P_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                                Tot_P_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                                Tot_P_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                                Tot_P_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                                Tot_P_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                                Tot_P_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                                Tot_P_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                                Tot_P_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                                Tot_P_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                                Tot_P_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                                if (UR_ID.Equals(Dr["DEPENDENCIA_ID"].ToString().Trim()))
                                {
                                    Tot_UR_Ene += Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                                    Tot_UR_Feb += Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                                    Tot_UR_Mar += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                                    Tot_UR_Abr += Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                                    Tot_UR_May += Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                                    Tot_UR_Jun += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                                    Tot_UR_Jul += Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                                    Tot_UR_Ago += Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                                    Tot_UR_Sep += Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                                    Tot_UR_Oct += Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                                    Tot_UR_Nov += Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                                    Tot_UR_Dic += Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                                    Fila = Dt_Datos.NewRow();
                                    Fila["CLASIFICACION POR OBJETO"] = Dr["CLAVE_NOMBRE_PARTIDA"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim()));
                                    Fila["FEBRERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim()));
                                    Fila["MARZO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim()));
                                    Fila["ABRIL"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim()));
                                    Fila["MAYO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim()));
                                    Fila["JUNIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim()));
                                    Fila["JULIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim()));
                                    Fila["AGOSTO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim()));
                                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim()));
                                    Fila["OCTUBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim()));
                                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim()));
                                    Fila["DICIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim()));
                                    Fila["TIPO"] = "PARTIDA";
                                    Dt_Datos.Rows.Add(Fila);
                                }
                                else
                                {
                                    foreach (DataRow Dr_P in Dt_Datos.Rows)
                                    {
                                        if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                                        {
                                            Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                            Dr_P["ENERO"] = String.Format("{0:n}", Tot_UR_Ene);
                                            Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_UR_Feb);
                                            Dr_P["MARZO"] = String.Format("{0:n}" , Tot_UR_Mar);
                                            Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_UR_Abr);
                                            Dr_P["MAYO"] = String.Format("{0:n}" , Tot_UR_May);
                                            Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_UR_Jun);
                                            Dr_P["JULIO"] = String.Format("{0:n}" , Tot_UR_Jul);
                                            Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_UR_Ago);
                                            Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_UR_Sep);
                                            Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_UR_Oct);
                                            Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_UR_Nov);
                                            Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_UR_Dic);
                                            Dr_P["TIPO"] = "URT";
                                            break;
                                        }
                                    }

                                    Tot_UR_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                                    Tot_UR_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                                    Tot_UR_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                                    Tot_UR_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                                    Tot_UR_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                                    Tot_UR_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                                    Tot_UR_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                                    Tot_UR_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                                    Tot_UR_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                                    Tot_UR_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                                    Tot_UR_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                                    Tot_UR_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                                    Fila = Dt_Datos.NewRow();
                                    Fila["CLASIFICACION POR OBJETO"] = "* " + Dr["CLAVE_NOMBRE_UR"].ToString().Trim();
                                    Fila["ENERO"] = "0";
                                    Fila["FEBRERO"] = "0";
                                    Fila["MARZO"] = "0";
                                    Fila["ABRIL"] = "0";
                                    Fila["MAYO"] = "0";
                                    Fila["JUNIO"] = "0";
                                    Fila["JULIO"] = "0";
                                    Fila["AGOSTO"] = "0";
                                    Fila["SEPTIEMBRE"] = "0";
                                    Fila["OCTUBRE"] = "0";
                                    Fila["NOVIEMBRE"] = "0";
                                    Fila["DICIEMBRE"] = "0";
                                    Fila["TIPO"] = "UR";
                                    Dt_Datos.Rows.Add(Fila);

                                    UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                                    Fila = Dt_Datos.NewRow();
                                    Fila["CLASIFICACION POR OBJETO"] = Dr["CLAVE_NOMBRE_PARTIDA"].ToString().Trim();
                                    Fila["ENERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim()));
                                    Fila["FEBRERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim()));
                                    Fila["MARZO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim()));
                                    Fila["ABRIL"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim()));
                                    Fila["MAYO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim()));
                                    Fila["JUNIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim()));
                                    Fila["JULIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim()));
                                    Fila["AGOSTO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim()));
                                    Fila["SEPTIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim()));
                                    Fila["OCTUBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim()));
                                    Fila["NOVIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim()));
                                    Fila["DICIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim()));
                                    Fila["TIPO"] = "PARTIDA";
                                    Dt_Datos.Rows.Add(Fila);
                                }
                            }
                            else
                            {
                                foreach (DataRow Dr_P in Dt_Datos.Rows)
                                {
                                    if (Dr_P["TIPO"].ToString().Trim().Equals("PP"))
                                    {
                                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_P_Ene);
                                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_P_Feb);
                                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_P_Mar);
                                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_P_Abr);
                                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_P_May);
                                        Dr_P["JUNIO"] = String.Format("{0:n}", Tot_P_Jun);
                                        Dr_P["JULIO"] = String.Format("{0:n}", Tot_P_Jul);
                                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_P_Ago);
                                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_P_Sep);
                                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_P_Oct);
                                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_P_Nov);
                                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_P_Dic);
                                        Dr_P["TIPO"] = "PPT";
                                    }
                                    else if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                                    {
                                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                        Dr_P["ENERO"] = String.Format("{0:n}", Tot_UR_Ene);
                                        Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_UR_Feb);
                                        Dr_P["MARZO"] = String.Format("{0:n}", Tot_UR_Mar);
                                        Dr_P["ABRIL"] = String.Format("{0:n}", Tot_UR_Abr);
                                        Dr_P["MAYO"] = String.Format("{0:n}", Tot_UR_May);
                                        Dr_P["JUNIO"] = String.Format("{0:n}", Tot_UR_Jun);
                                        Dr_P["JULIO"] = String.Format("{0:n}", Tot_UR_Jul);
                                        Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_UR_Ago);
                                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_UR_Sep);
                                        Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_UR_Oct);
                                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_UR_Nov);
                                        Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_UR_Dic);
                                        Dr_P["TIPO"] = "URT";
                                        break;
                                    }
                                }

                                Tot_P_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                                Tot_P_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                                Tot_P_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                                Tot_P_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                                Tot_P_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                                Tot_P_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                                Tot_P_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                                Tot_P_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                                Tot_P_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                                Tot_P_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                                Tot_P_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                                Tot_P_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                                Fila = Dt_Datos.NewRow();
                                Fila["CLASIFICACION POR OBJETO"] = "** " + Dr["CLAVE_NOMBRE_PROGRAMA"].ToString().Trim();
                                Fila["ENERO"] = "0";
                                Fila["FEBRERO"] = "0";
                                Fila["MARZO"] = "0";
                                Fila["ABRIL"] = "0";
                                Fila["MAYO"] = "0";
                                Fila["JUNIO"] = "0";
                                Fila["JULIO"] = "0";
                                Fila["AGOSTO"] = "0";
                                Fila["SEPTIEMBRE"] = "0";
                                Fila["OCTUBRE"] = "0";
                                Fila["NOVIEMBRE"] = "0";
                                Fila["DICIEMBRE"] = "0";
                                Fila["TIPO"] = "PP";
                                Dt_Datos.Rows.Add(Fila);

                                P_ID = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();

                                Tot_UR_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                                Tot_UR_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                                Tot_UR_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                                Tot_UR_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                                Tot_UR_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                                Tot_UR_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                                Tot_UR_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                                Tot_UR_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                                Tot_UR_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                                Tot_UR_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                                Tot_UR_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                                Tot_UR_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                                Fila = Dt_Datos.NewRow();
                                Fila["CLASIFICACION POR OBJETO"] = "* " + Dr["CLAVE_NOMBRE_UR"].ToString().Trim();
                                Fila["ENERO"] = "0";
                                Fila["FEBRERO"] = "0";
                                Fila["MARZO"] = "0";
                                Fila["ABRIL"] = "0";
                                Fila["MAYO"] = "0";
                                Fila["JUNIO"] = "0";
                                Fila["JULIO"] = "0";
                                Fila["AGOSTO"] = "0";
                                Fila["SEPTIEMBRE"] = "0";
                                Fila["OCTUBRE"] = "0";
                                Fila["NOVIEMBRE"] = "0";
                                Fila["DICIEMBRE"] = "0";
                                Fila["TIPO"] = "UR";
                                Dt_Datos.Rows.Add(Fila);

                                UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                                Fila = Dt_Datos.NewRow();
                                Fila["CLASIFICACION POR OBJETO"] = Dr["CLAVE_NOMBRE_PARTIDA"].ToString().Trim();
                                Fila["ENERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim()));
                                Fila["FEBRERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim()));
                                Fila["MARZO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim()));
                                Fila["ABRIL"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim()));
                                Fila["MAYO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim()));
                                Fila["JUNIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim()));
                                Fila["JULIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim()));
                                Fila["AGOSTO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim()));
                                Fila["SEPTIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim()));
                                Fila["OCTUBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim()));
                                Fila["NOVIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim()));
                                Fila["DICIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim()));
                                Fila["TIPO"] = "PARTIDA";
                                Dt_Datos.Rows.Add(Fila);
                            }
                        }
                        else 
                        {
                            foreach (DataRow Dr_P in Dt_Datos.Rows)
                            {
                                if (Dr_P["TIPO"].ToString().Trim().Equals("AF")) 
                                {
                                    Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                    Dr_P["ENERO"] = String.Format("{0:n}" , Tot_AF_Ene);
                                    Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_AF_Feb);
                                    Dr_P["MARZO"] = String.Format("{0:n}" , Tot_AF_Mar);
                                    Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_AF_Abr);
                                    Dr_P["MAYO"] = String.Format("{0:n}" , Tot_AF_May);
                                    Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_AF_Jun);
                                    Dr_P["JULIO"] = String.Format("{0:n}" , Tot_AF_Jul);
                                    Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_AF_Ago);
                                    Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_AF_Sep);
                                    Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_AF_Oct);
                                    Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_AF_Nov);
                                    Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_AF_Dic);
                                    Dr_P["TIPO"] = "AFT";
                                }
                                else if (Dr_P["TIPO"].ToString().Trim().Equals("PP"))
                                {
                                    Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                    Dr_P["ENERO"] = String.Format("{0:n}", Tot_P_Ene);
                                    Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_P_Feb);
                                    Dr_P["MARZO"] = String.Format("{0:n}", Tot_P_Mar);
                                    Dr_P["ABRIL"] = String.Format("{0:n}", Tot_P_Abr);
                                    Dr_P["MAYO"] = String.Format("{0:n}", Tot_P_May);
                                    Dr_P["JUNIO"] = String.Format("{0:n}", Tot_P_Jun);
                                    Dr_P["JULIO"] = String.Format("{0:n}", Tot_P_Jul);
                                    Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_P_Ago);
                                    Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_P_Sep);
                                    Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_P_Oct);
                                    Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_P_Nov);
                                    Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_P_Dic);
                                    Dr_P["TIPO"] = "PPT";
                                }
                                else if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                                {
                                    Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                    Dr_P["ENERO"] = String.Format("{0:n}", Tot_UR_Ene);
                                    Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_UR_Feb);
                                    Dr_P["MARZO"] = String.Format("{0:n}", Tot_UR_Mar);
                                    Dr_P["ABRIL"] = String.Format("{0:n}", Tot_UR_Abr);
                                    Dr_P["MAYO"] = String.Format("{0:n}", Tot_UR_May);
                                    Dr_P["JUNIO"] = String.Format("{0:n}", Tot_UR_Jun);
                                    Dr_P["JULIO"] = String.Format("{0:n}", Tot_UR_Jul);
                                    Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_UR_Ago);
                                    Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_UR_Sep);
                                    Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_UR_Oct);
                                    Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_UR_Nov);
                                    Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_UR_Dic);
                                    Dr_P["TIPO"] = "URT";
                                    break;
                                }
                            }

                            Tot_AF_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                            Tot_AF_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                            Tot_AF_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                            Tot_AF_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                            Tot_AF_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                            Tot_AF_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                            Tot_AF_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                            Tot_AF_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                            Tot_AF_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                            Tot_AF_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                            Tot_AF_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                            Tot_AF_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                            Fila = Dt_Datos.NewRow();
                            Fila["CLASIFICACION POR OBJETO"] = "*** " + Dr["CLAVE_NOMBRE_AF"].ToString().Trim();
                            Fila["ENERO"] = "0";
                            Fila["FEBRERO"] = "0";
                            Fila["MARZO"] = "0";
                            Fila["ABRIL"] = "0";
                            Fila["MAYO"] = "0";
                            Fila["JUNIO"] = "0";
                            Fila["JULIO"] = "0";
                            Fila["AGOSTO"] = "0";
                            Fila["SEPTIEMBRE"] = "0";
                            Fila["OCTUBRE"] = "0";
                            Fila["NOVIEMBRE"] = "0";
                            Fila["DICIEMBRE"] = "0";
                            Fila["TIPO"] = "AF";
                            Dt_Datos.Rows.Add(Fila);

                            AF_ID = Dr["AREA_FUNCIONAL_ID"].ToString().Trim();

                            Tot_P_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                            Tot_P_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                            Tot_P_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                            Tot_P_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                            Tot_P_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                            Tot_P_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                            Tot_P_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                            Tot_P_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                            Tot_P_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                            Tot_P_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                            Tot_P_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                            Tot_P_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                            Fila = Dt_Datos.NewRow();
                            Fila["CLASIFICACION POR OBJETO"] = "** " + Dr["CLAVE_NOMBRE_PROGRAMA"].ToString().Trim();
                            Fila["ENERO"] = "0";
                            Fila["FEBRERO"] = "0";
                            Fila["MARZO"] = "0";
                            Fila["ABRIL"] = "0";
                            Fila["MAYO"] = "0";
                            Fila["JUNIO"] = "0";
                            Fila["JULIO"] = "0";
                            Fila["AGOSTO"] = "0";
                            Fila["SEPTIEMBRE"] = "0";
                            Fila["OCTUBRE"] = "0";
                            Fila["NOVIEMBRE"] = "0";
                            Fila["DICIEMBRE"] = "0";
                            Fila["TIPO"] = "PP";
                            Dt_Datos.Rows.Add(Fila);

                            P_ID = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();

                            Tot_UR_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                            Tot_UR_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                            Tot_UR_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                            Tot_UR_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                            Tot_UR_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                            Tot_UR_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                            Tot_UR_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                            Tot_UR_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                            Tot_UR_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                            Tot_UR_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                            Tot_UR_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                            Tot_UR_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                            Fila = Dt_Datos.NewRow();
                            Fila["CLASIFICACION POR OBJETO"] = "* " + Dr["CLAVE_NOMBRE_UR"].ToString().Trim();
                            Fila["ENERO"] = "0";
                            Fila["FEBRERO"] = "0";
                            Fila["MARZO"] = "0";
                            Fila["ABRIL"] = "0";
                            Fila["MAYO"] = "0";
                            Fila["JUNIO"] = "0";
                            Fila["JULIO"] = "0";
                            Fila["AGOSTO"] = "0";
                            Fila["SEPTIEMBRE"] = "0";
                            Fila["OCTUBRE"] = "0";
                            Fila["NOVIEMBRE"] = "0";
                            Fila["DICIEMBRE"] = "0";
                            Fila["TIPO"] = "UR";
                            Dt_Datos.Rows.Add(Fila);

                            UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                            Fila = Dt_Datos.NewRow();
                            Fila["CLASIFICACION POR OBJETO"] = Dr["CLAVE_NOMBRE_PARTIDA"].ToString().Trim();
                            Fila["ENERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim()));
                            Fila["FEBRERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim()));
                            Fila["MARZO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim()));
                            Fila["ABRIL"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim()));
                            Fila["MAYO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim()));
                            Fila["JUNIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim()));
                            Fila["JULIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim()));
                            Fila["AGOSTO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim()));
                            Fila["SEPTIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim()));
                            Fila["OCTUBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim()));
                            Fila["NOVIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim()));
                            Fila["DICIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim()));
                            Fila["TIPO"] = "PARTIDA";
                            Dt_Datos.Rows.Add(Fila);
                        }
                    }
                    else
                    {
                        foreach (DataRow Dr_P in Dt_Datos.Rows)
                        {
                            if (Dr_P["TIPO"].ToString().Trim().Equals("FF"))
                            {
                                Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                Dr_P["ENERO"] = String.Format("{0:n}", Tot_FF_Ene);
                                Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_FF_Feb);
                                Dr_P["MARZO"] = String.Format("{0:n}", Tot_FF_Mar);
                                Dr_P["ABRIL"] = String.Format("{0:n}", Tot_FF_Abr);
                                Dr_P["MAYO"] = String.Format("{0:n}", Tot_FF_May);
                                Dr_P["JUNIO"] = String.Format("{0:n}", Tot_FF_Jun);
                                Dr_P["JULIO"] = String.Format("{0:n}", Tot_FF_Jul);
                                Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_FF_Ago);
                                Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_FF_Sep);
                                Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_FF_Oct);
                                Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_FF_Nov);
                                Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_FF_Dic);
                                Dr_P["TIPO"] = "FFT";
                            }
                            else if (Dr_P["TIPO"].ToString().Trim().Equals("AF"))
                            {
                                Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                Dr_P["ENERO"] = String.Format("{0:n}", Tot_AF_Ene);
                                Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_AF_Feb);
                                Dr_P["MARZO"] = String.Format("{0:n}", Tot_AF_Mar);
                                Dr_P["ABRIL"] = String.Format("{0:n}", Tot_AF_Abr);
                                Dr_P["MAYO"] = String.Format("{0:n}", Tot_AF_May);
                                Dr_P["JUNIO"] = String.Format("{0:n}", Tot_AF_Jun);
                                Dr_P["JULIO"] = String.Format("{0:n}", Tot_AF_Jul);
                                Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_AF_Ago);
                                Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_AF_Sep);
                                Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_AF_Oct);
                                Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_AF_Nov);
                                Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_AF_Dic);
                                Dr_P["TIPO"] = "AFT";
                            }
                            else if (Dr_P["TIPO"].ToString().Trim().Equals("PP"))
                            {
                                Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                Dr_P["ENERO"] = String.Format("{0:n}", Tot_P_Ene);
                                Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_P_Feb);
                                Dr_P["MARZO"] = String.Format("{0:n}", Tot_P_Mar);
                                Dr_P["ABRIL"] = String.Format("{0:n}", Tot_P_Abr);
                                Dr_P["MAYO"] = String.Format("{0:n}", Tot_P_May);
                                Dr_P["JUNIO"] = String.Format("{0:n}", Tot_P_Jun);
                                Dr_P["JULIO"] = String.Format("{0:n}", Tot_P_Jul);
                                Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_P_Ago);
                                Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_P_Sep);
                                Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_P_Oct);
                                Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_P_Nov);
                                Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_P_Dic);
                                Dr_P["TIPO"] = "PPT";
                            }
                            else if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                            {
                                Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                                Dr_P["ENERO"] = String.Format("{0:n}", Tot_UR_Ene);
                                Dr_P["FEBRERO"] = String.Format("{0:n}", Tot_UR_Feb);
                                Dr_P["MARZO"] = String.Format("{0:n}", Tot_UR_Mar);
                                Dr_P["ABRIL"] = String.Format("{0:n}", Tot_UR_Abr);
                                Dr_P["MAYO"] = String.Format("{0:n}", Tot_UR_May);
                                Dr_P["JUNIO"] = String.Format("{0:n}", Tot_UR_Jun);
                                Dr_P["JULIO"] = String.Format("{0:n}", Tot_UR_Jul);
                                Dr_P["AGOSTO"] = String.Format("{0:n}", Tot_UR_Ago);
                                Dr_P["SEPTIEMBRE"] = String.Format("{0:n}", Tot_UR_Sep);
                                Dr_P["OCTUBRE"] = String.Format("{0:n}", Tot_UR_Oct);
                                Dr_P["NOVIEMBRE"] = String.Format("{0:n}", Tot_UR_Nov);
                                Dr_P["DICIEMBRE"] = String.Format("{0:n}", Tot_UR_Dic);
                                Dr_P["TIPO"] = "URT";
                                break;
                            }
                        }

                        Tot_FF_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                        Tot_FF_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                        Tot_FF_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                        Tot_FF_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                        Tot_FF_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                        Tot_FF_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                        Tot_FF_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                        Tot_FF_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                        Tot_FF_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                        Tot_FF_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                        Tot_FF_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                        Tot_FF_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["CLASIFICACION POR OBJETO"] = "**** " + Dr["CLAVE_NOMBRE_FF"].ToString().Trim();
                        Fila["ENERO"] = "0";
                        Fila["FEBRERO"] = "0";
                        Fila["MARZO"] = "0";
                        Fila["ABRIL"] = "0";
                        Fila["MAYO"] = "0";
                        Fila["JUNIO"] = "0";
                        Fila["JULIO"] = "0";
                        Fila["AGOSTO"] = "0";
                        Fila["SEPTIEMBRE"] = "0";
                        Fila["OCTUBRE"] = "0";
                        Fila["NOVIEMBRE"] = "0";
                        Fila["DICIEMBRE"] = "0";
                        Fila["TIPO"] = "FF";
                        Dt_Datos.Rows.Add(Fila);

                        FF_ID = Dr["FTE_FINANCIAMIENTO_ID"].ToString().Trim();

                        Tot_AF_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                        Tot_AF_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                        Tot_AF_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                        Tot_AF_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                        Tot_AF_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                        Tot_AF_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                        Tot_AF_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                        Tot_AF_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                        Tot_AF_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                        Tot_AF_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                        Tot_AF_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                        Tot_AF_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["CLASIFICACION POR OBJETO"] = "*** " + Dr["CLAVE_NOMBRE_AF"].ToString().Trim();
                        Fila["ENERO"] = "0";
                        Fila["FEBRERO"] = "0";
                        Fila["MARZO"] = "0";
                        Fila["ABRIL"] = "0";
                        Fila["MAYO"] = "0";
                        Fila["JUNIO"] = "0";
                        Fila["JULIO"] = "0";
                        Fila["AGOSTO"] = "0";
                        Fila["SEPTIEMBRE"] = "0";
                        Fila["OCTUBRE"] = "0";
                        Fila["NOVIEMBRE"] = "0";
                        Fila["DICIEMBRE"] = "0";
                        Fila["TIPO"] = "AF";
                        Dt_Datos.Rows.Add(Fila);

                        AF_ID = Dr["AREA_FUNCIONAL_ID"].ToString().Trim();

                        Tot_P_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                        Tot_P_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                        Tot_P_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                        Tot_P_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                        Tot_P_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                        Tot_P_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                        Tot_P_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                        Tot_P_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                        Tot_P_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                        Tot_P_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                        Tot_P_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                        Tot_P_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["CLASIFICACION POR OBJETO"] = "** " + Dr["CLAVE_NOMBRE_PROGRAMA"].ToString().Trim();
                        Fila["ENERO"] = "0";
                        Fila["FEBRERO"] = "0";
                        Fila["MARZO"] = "0";
                        Fila["ABRIL"] = "0";
                        Fila["MAYO"] = "0";
                        Fila["JUNIO"] = "0";
                        Fila["JULIO"] = "0";
                        Fila["AGOSTO"] = "0";
                        Fila["SEPTIEMBRE"] = "0";
                        Fila["OCTUBRE"] = "0";
                        Fila["NOVIEMBRE"] = "0";
                        Fila["DICIEMBRE"] = "0";
                        Fila["TIPO"] = "PP";
                        Dt_Datos.Rows.Add(Fila);

                        P_ID = Dr["PROYECTO_PROGRAMA_ID"].ToString().Trim();

                        Tot_UR_Ene = Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim());
                        Tot_UR_Feb = Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim());
                        Tot_UR_Mar = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim());
                        Tot_UR_Abr = Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim());
                        Tot_UR_May = Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim());
                        Tot_UR_Jun = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim());
                        Tot_UR_Jul = Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim());
                        Tot_UR_Ago = Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim());
                        Tot_UR_Sep = Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim());
                        Tot_UR_Oct = Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim());
                        Tot_UR_Nov = Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim());
                        Tot_UR_Dic = Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim());

                        Fila = Dt_Datos.NewRow();
                        Fila["CLASIFICACION POR OBJETO"] = "* " + Dr["CLAVE_NOMBRE_UR"].ToString().Trim();
                        Fila["ENERO"] = "0";
                        Fila["FEBRERO"] = "0";
                        Fila["MARZO"] = "0";
                        Fila["ABRIL"] = "0";
                        Fila["MAYO"] = "0";
                        Fila["JUNIO"] = "0";
                        Fila["JULIO"] = "0";
                        Fila["AGOSTO"] = "0";
                        Fila["SEPTIEMBRE"] = "0";
                        Fila["OCTUBRE"] = "0";
                        Fila["NOVIEMBRE"] = "0";
                        Fila["DICIEMBRE"] = "0";
                        Fila["TIPO"] = "UR";
                        Dt_Datos.Rows.Add(Fila);

                        UR_ID = Dr["DEPENDENCIA_ID"].ToString().Trim();

                        Fila = Dt_Datos.NewRow();
                        Fila["CLASIFICACION POR OBJETO"] = Dr["CLAVE_NOMBRE_PARTIDA"].ToString().Trim();
                        Fila["ENERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ENE"].ToString().Trim()) ? "0" : Dr["ENE"].ToString().Trim()));
                        Fila["FEBRERO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["FEB"].ToString().Trim()) ? "0" : Dr["FEB"].ToString().Trim()));
                        Fila["MARZO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAR"].ToString().Trim()) ? "0" : Dr["MAR"].ToString().Trim()));
                        Fila["ABRIL"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["ABR"].ToString().Trim()) ? "0" : Dr["ABR"].ToString().Trim()));
                        Fila["MAYO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["MAY"].ToString().Trim()) ? "0" : Dr["MAY"].ToString().Trim()));
                        Fila["JUNIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUN"].ToString().Trim()) ? "0" : Dr["JUN"].ToString().Trim()));
                        Fila["JULIO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["JUL"].ToString().Trim()) ? "0" : Dr["JUL"].ToString().Trim()));
                        Fila["AGOSTO"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["AGO"].ToString().Trim()) ? "0" : Dr["AGO"].ToString().Trim()));
                        Fila["SEPTIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["SEP"].ToString().Trim()) ? "0" : Dr["SEP"].ToString().Trim()));
                        Fila["OCTUBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["OCT"].ToString().Trim()) ? "0" : Dr["OCT"].ToString().Trim()));
                        Fila["NOVIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["NOV"].ToString().Trim()) ? "0" : Dr["NOV"].ToString().Trim()));
                        Fila["DICIEMBRE"] = String.Format("{0:n}", Convert.ToDouble(String.IsNullOrEmpty(Dr["DIC"].ToString().Trim()) ? "0" : Dr["DIC"].ToString().Trim()));
                        Fila["TIPO"] = "PARTIDA";
                        Dt_Datos.Rows.Add(Fila);
                    }
                }

                foreach (DataRow Dr_P in Dt_Datos.Rows)
                {
                    if (Dr_P["TIPO"].ToString().Trim().Equals("FF"))
                    {
                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_FF_Ene);
                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_FF_Feb);
                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_FF_Mar);
                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_FF_Abr);
                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_FF_May);
                        Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_FF_Jun);
                        Dr_P["JULIO"] = String.Format("{0:n}" , Tot_FF_Jul);
                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_FF_Ago);
                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_FF_Sep);
                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_FF_Oct);
                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_FF_Nov);
                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_FF_Dic);
                        Dr_P["TIPO"] = "FFT";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("AF")) 
                    {
                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_AF_Ene);
                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_AF_Feb);
                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_AF_Mar);
                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_AF_Abr);
                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_AF_May);
                        Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_AF_Jun);
                        Dr_P["JULIO"] = String.Format("{0:n}" , Tot_AF_Jul);
                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_AF_Ago);
                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_AF_Sep);
                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_AF_Oct);
                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_AF_Nov);
                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_AF_Dic);
                        Dr_P["TIPO"] = "AFT";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("PP"))
                    {
                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_P_Ene);
                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_P_Feb);
                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_P_Mar);
                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_P_Abr);
                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_P_May);
                        Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_P_Jun);
                        Dr_P["JULIO"] = String.Format("{0:n}" , Tot_P_Jul);
                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_P_Ago);
                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_P_Sep);
                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_P_Oct);
                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_P_Nov);
                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_P_Dic);
                        Dr_P["TIPO"] = "PPT";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("UR"))
                    {
                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_UR_Ene);
                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_UR_Feb);
                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_UR_Mar);
                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_UR_Abr);
                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_UR_May);
                        Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_UR_Jun);
                        Dr_P["JULIO"] = String.Format("{0:n}" , Tot_UR_Jul);
                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_UR_Ago);
                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_UR_Sep);
                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_UR_Oct);
                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_UR_Nov);
                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_UR_Dic);
                        Dr_P["TIPO"] = "URT";
                    }
                    else if (Dr_P["TIPO"].ToString().Trim().Equals("Total"))
                    {
                        Dr_P["CLASIFICACION POR OBJETO"] = Dr_P["CLASIFICACION POR OBJETO"].ToString().Trim();
                        Dr_P["ENERO"] = String.Format("{0:n}" , Tot_Ene);
                        Dr_P["FEBRERO"] = String.Format("{0:n}" , Tot_Feb);
                        Dr_P["MARZO"] = String.Format("{0:n}" , Tot_Mar);
                        Dr_P["ABRIL"] = String.Format("{0:n}" , Tot_Abr);
                        Dr_P["MAYO"] = String.Format("{0:n}" , Tot_May);
                        Dr_P["JUNIO"] = String.Format("{0:n}" , Tot_Jun);
                        Dr_P["JULIO"] = String.Format("{0:n}" , Tot_Jul);
                        Dr_P["AGOSTO"] = String.Format("{0:n}" , Tot_Ago);
                        Dr_P["SEPTIEMBRE"] = String.Format("{0:n}" , Tot_Sep);
                        Dr_P["OCTUBRE"] = String.Format("{0:n}" , Tot_Oct);
                        Dr_P["NOVIEMBRE"] = String.Format("{0:n}" , Tot_Nov);
                        Dr_P["DICIEMBRE"] = String.Format("{0:n}" , Tot_Dic);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar los datos de economias: Error[" + Ex.Message + "]");
            }
            return Dt_Datos;
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Generar_Rpt_Ingresos
        ///DESCRIPCIÓN          : Metodo generar el reporte de economias
        ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 02/Marzo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        public void Generar_Rpt_Economias(DataTable Dt_Datos)
        {
            String Ruta_Archivo = "Reporte_Econimias.xls";
            try
            {
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
                Libro.Properties.Title = "Reporte de Economias";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "JAPAMI";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Reporte de Economias");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad = Libro.Styles.Add("BodyStyleCant");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cantidad2 = Libro.Styles.Add("BodyStyleCant2");


                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 9;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "#7DCC7D";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad.Font.FontName = "Tahoma";
                Estilo_Cantidad.Font.Size = 9;
                Estilo_Cantidad.Font.Bold = true;
                Estilo_Cantidad.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad.Font.Color = "#000000";
                Estilo_Cantidad.Interior.Color = "White";
                Estilo_Cantidad.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Contenido2.Font.Size = 9;
                Estilo_Contenido2.Font.Bold = true;
                Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Left;
                Estilo_Contenido2.Font.Color = "#000000";
                Estilo_Contenido2.Interior.Color = "#FFFF99";
                Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                Estilo_Cantidad2.Font.FontName = "Tahoma";
                Estilo_Cantidad2.Font.Size = 9;
                Estilo_Cantidad2.Font.Bold = true;
                Estilo_Cantidad2.Alignment.Horizontal = StyleHorizontalAlignment.Right;
                Estilo_Cantidad2.Font.Color = "#000000";
                Estilo_Cantidad2.Interior.Color = "#FFFF99";
                Estilo_Cantidad2.NumberFormat = "#,###,##0.00";
                Estilo_Cantidad2.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cantidad2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cantidad2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//COG.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//enero.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//febrero.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//marzo.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//abril.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//may.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//jun.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//jul.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//ago.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//sep.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//oct.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//nov.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//dic.
 
                if (Dt_Datos is DataTable)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        foreach (DataColumn Columna in Dt_Datos.Columns)
                        {
                            if (Columna is DataColumn)
                            {
                                if (Columna.ColumnName != "TIPO") 
                                {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Columna.ColumnName.Replace("_", " "), "HeaderStyle"));
                                }
                            }
                        }

                        foreach (DataRow Dr in Dt_Datos.Rows)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (DataColumn Columna in Dt_Datos.Columns)
                            {
                                if (Columna is DataColumn)
                                {
                                    if (Dr["TIPO"].ToString().Equals("PARTIDA"))
                                    {
                                        if (Columna.ColumnName.Equals("CLASIFICACION POR OBJETO"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant"));
                                            }
                                        }
                                    }
                                    else 
                                    {
                                        if (Columna.ColumnName.Equals("CLASIFICACION POR OBJETO"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "BodyStyle2"));
                                        }
                                        else
                                        {
                                            if (Columna.ColumnName != "TIPO")
                                            {
                                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.Number, "BodyStyleCant2"));
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception ex)
            {
                throw new Exception("Error al generar el reporte en excel Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Validar_Campos
        ///DESCRIPCIÓN          : Metodo para validar los campos del formulario
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private Boolean Validar_Campos()
        {
            Boolean Datos_Validos = true;
            Limpiar_Controles("Error");
            Lbl_Ecabezado_Mensaje.Text = "Favor de:";
            try
            {
                if (Cmb_Anio.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccionar un año <br />";
                    Datos_Validos = false;
                }

                return Datos_Validos;
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al valodar los campos del formulario Error [" + Ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Registros
        ///DESCRIPCIÓN          : Metodo para llenar el grid de los registros
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 15/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        private void Llenar_Grid_Registros(DataTable Dt_Registros)
        {
            try
            {
                if (Dt_Registros != null)
                {
                    Grid_Registros.Columns[13].Visible = true;
                    Grid_Registros.DataSource = Dt_Registros;
                    Grid_Registros.DataBind();
                    Grid_Registros.Columns[13].Visible = false;
                }
                else
                {
                    Grid_Registros.DataSource = new DataTable();
                    Grid_Registros.DataBind();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el grid de los registros: Error[" + Ex.Message + "]");
            }
        }

    #endregion

    #region EVENTOS

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de salir
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Trim().Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 28/Diciembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Generar_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Economias_Negocio Obj = new Cls_Rpt_Psp_Economias_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;

            try
            {
                if (Validar_Campos())
                {
                    if (Cmb_UR.SelectedIndex > 0) { Obj.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim(); }
                    else { Obj.P_Dependencia_ID = String.Empty; }

                    if (Cmb_Programa.SelectedIndex > 0) { Obj.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim(); }
                    else { Obj.P_Programa_ID = String.Empty; }

                    if (Cmb_FF.SelectedIndex > 0) { Obj.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                    else { Obj.P_Fte_Financiamiento_ID = String.Empty; }

                    if (Cmb_Partida.SelectedIndex > 0) { Obj.P_Partida_ID = Cmb_Partida.SelectedItem.Value.Trim(); }
                    else { Obj.P_Partida_ID = String.Empty; }

                    Obj.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Dt = Obj.Consultar_Economias();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Dt = Generar_Dt_Economias(Dt);
                            Generar_Rpt_Economias(Dt);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Generar_Reporte_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Economias_Negocio Obj = new Cls_Rpt_Psp_Economias_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;
            DataSet Ds_Registros = null;
            String Mes = String.Format("{0:MM}", DateTime.Now);

            try
            {
                if (Validar_Campos())
                {
                    if (Cmb_UR.SelectedIndex > 0) { Obj.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim(); }
                    else { Obj.P_Dependencia_ID = String.Empty; }

                    if (Cmb_Programa.SelectedIndex > 0) { Obj.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim(); }
                    else { Obj.P_Programa_ID = String.Empty; }

                    if (Cmb_FF.SelectedIndex > 0) { Obj.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                    else { Obj.P_Fte_Financiamiento_ID = String.Empty; }

                    if (Cmb_Partida.SelectedIndex > 0) { Obj.P_Partida_ID = Cmb_Partida.SelectedItem.Value.Trim(); }
                    else { Obj.P_Partida_ID = String.Empty; }

                    Obj.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Dt = Obj.Consultar_Economias();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Dt = Generar_Dt_Economias(Dt);
                            Llenar_Grid_Registros(Dt);

                            Ds_Registros = new DataSet();
                            Dt.TableName = "Dt_Registros";
                            Ds_Registros.Tables.Add(Dt.Copy());
                            Generar_Reporte(ref Ds_Registros, "Cr_Rpt_Psp_Analitico_Presupuesto_Ing.rpt",
                                "Rpt_Analitico_Presupuesto_Ingresos" + Session.SessionID + ".pdf");
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Consultar_Click
        ///DESCRIPCIÓN          : Ejecuta la operacion de click en el boton de generar
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Mayo/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Btn_Consultar_Click(object sender, EventArgs e)
        {
            Cls_Rpt_Psp_Economias_Negocio Obj = new Cls_Rpt_Psp_Economias_Negocio(); //Conexion con la capa de negocios
            DataTable Dt = new DataTable(); //Para almacenar los datos de las ordenes de compras
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            String Complemento = String.Empty;

            try
            {
                if (Validar_Campos())
                {
                    if (Cmb_UR.SelectedIndex > 0) { Obj.P_Dependencia_ID = Cmb_UR.SelectedItem.Value.Trim(); }
                    else { Obj.P_Dependencia_ID = String.Empty; }

                    if (Cmb_Programa.SelectedIndex > 0) { Obj.P_Programa_ID = Cmb_Programa.SelectedItem.Value.Trim(); }
                    else { Obj.P_Programa_ID = String.Empty; }

                    if (Cmb_FF.SelectedIndex > 0) { Obj.P_Fte_Financiamiento_ID = Cmb_FF.SelectedItem.Value.Trim(); }
                    else { Obj.P_Fte_Financiamiento_ID = String.Empty; }

                    if (Cmb_Partida.SelectedIndex > 0) { Obj.P_Partida_ID = Cmb_Partida.SelectedItem.Value.Trim(); }
                    else { Obj.P_Partida_ID = String.Empty; }

                    Obj.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                    Dt = Obj.Consultar_Economias();

                    if (Dt != null)
                    {
                        if (Dt.Rows.Count > 0)
                        {
                            Dt = Generar_Dt_Economias(Dt);
                            Llenar_Grid_Registros(Dt);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "", "alert('No hay datos registrados.');", true);
                    }
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del boton de generar el reporte. Error[" + ex.Message + "]");
            }
        }

        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Registros_RowDataBound
        ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Mayo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Grid_Registros_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            Double Ene = 0.00;
            Double Feb = 0.00;
            Double Mar = 0.00;
            Double Abr = 0.00;
            Double May = 0.00;
            Double Jun = 0.00;
            Double Jul = 0.00;
            Double Ago = 0.00;
            Double Sep = 0.00;
            Double Oct = 0.00;
            Double Nov = 0.00;
            Double Dic = 0.00;
            try
            {
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    Ene = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[1].Text.Trim()) ? "0" : e.Row.Cells[1].Text.Trim());
                    Feb = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[2].Text.Trim()) ? "0" : e.Row.Cells[2].Text.Trim());
                    Mar = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) ? "0" : e.Row.Cells[3].Text.Trim());
                    Abr = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[4].Text.Trim()) ? "0" : e.Row.Cells[4].Text.Trim());
                    May = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[5].Text.Trim()) ? "0" : e.Row.Cells[5].Text.Trim());
                    Jun = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[6].Text.Trim()) ? "0" : e.Row.Cells[6].Text.Trim());
                    Jul = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[7].Text.Trim()) ? "0" : e.Row.Cells[7].Text.Trim());
                    Ago = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[8].Text.Trim()) ? "0" : e.Row.Cells[8].Text.Trim());
                    Sep = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[9].Text.Trim()) ? "0" : e.Row.Cells[9].Text.Trim());
                    Oct = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[10].Text.Trim()) ? "0" : e.Row.Cells[10].Text.Trim());
                    Nov = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[11].Text.Trim()) ? "0" : e.Row.Cells[11].Text.Trim());
                    Dic = Convert.ToDouble(String.IsNullOrEmpty(e.Row.Cells[12].Text.Trim()) ? "0" : e.Row.Cells[12].Text.Trim());

                   
                    if (e.Row.Cells[13].Text.Trim().Equals("Total"))
                    {
                        e.Row.Style.Add("background-color", "#0066FF");
                        e.Row.Style.Add("color", "black");
                    }

                    if (e.Row.Cells[13].Text.Trim().Equals("FFT"))
                    {
                        e.Row.Style.Add("background-color", "#009999");
                        e.Row.Style.Add("color", "black");
                    }

                    if (e.Row.Cells[13].Text.Trim().Equals("AFT"))
                    {
                        e.Row.Style.Add("background-color", "#0099FF");
                        e.Row.Style.Add("color", "black");
                    }

                    if (e.Row.Cells[13].Text.Trim().Equals("PPT"))
                    {
                        e.Row.Style.Add("background-color", "#00CCCC");
                        e.Row.Style.Add("color", "black");
                    }

                    if (e.Row.Cells[13].Text.Trim().Equals("URT"))
                    {
                        e.Row.Style.Add("background-color", "#99FFFF");
                        e.Row.Style.Add("color", "black");
                    }

                    if (Ene < 0)
                        e.Row.Cells[1].Style.Add("color", "red");
                    if (Feb < 0)
                        e.Row.Cells[2].Style.Add("color", "red");
                    if (Mar < 0)
                        e.Row.Cells[3].Style.Add("color", "red");
                    if (Abr < 0)
                        e.Row.Cells[4].Style.Add("color", "red");
                    if (May < 0)
                        e.Row.Cells[5].Style.Add("color", "red");
                    if (Jun < 0)
                        e.Row.Cells[6].Style.Add("color", "red");
                    if (Jul < 0)
                        e.Row.Cells[7].Style.Add("color", "red");
                    if (Ago < 0)
                        e.Row.Cells[8].Style.Add("color", "red");
                    if (Sep < 0)
                        e.Row.Cells[9].Style.Add("color", "red");
                    if (Oct < 0)
                        e.Row.Cells[10].Style.Add("color", "red");
                    if (Nov < 0)
                        e.Row.Cells[11].Style.Add("color", "red");
                    if (Dic < 0)
                        e.Row.Cells[12].Style.Add("color", "red");
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error:[" + Ex.Message + "]");
            }
        }
    #endregion

    #region EVENTOS COMBOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_FF_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_FF_SelectedIndexChanged(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            try
            {
                Llenar_Combo_Programa();
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_UR_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_UR_SelectedIndexChanged(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            try
            {
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Programa_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento del combo 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 23/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///******************************************************************************* 
        protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Limpiar_Controles("Error");
            try
            {
                Llenar_Combo_UR();
                Llenar_Combo_Partidas();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en el evento del combo Error[" + ex.Message + "]");
            }
        }

    #endregion

    #region (Reportes)
        /// *************************************************************************************
        /// NOMBRE: Generar_Reporte
        /// 
        /// DESCRIPCIÓN: Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
        ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
        {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

            try
            {
                Ruta = @Server.MapPath("../Rpt/Presupuestos/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE: Exportar_Reporte_PDF
        /// 
        /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
        ///              especificada.
        ///              
        /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///             Nombre_Reporte.- Nombre que se le dará al reporte.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE: Mostrar_Reporte
        /// 
        /// DESCRIPCIÓN: Muestra el reporte en pantalla.
        ///              
        /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
        /// 
        /// USUARIO CREO: Juan Alberto Hernández Negrete.
        /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Nominas_Negativas",
                    "window.open('" + Pagina + "', 'Busqueda_Empleados','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion
}
