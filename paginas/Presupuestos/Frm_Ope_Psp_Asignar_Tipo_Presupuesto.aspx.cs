﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Tipo_Presupuesto.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;

public partial class paginas_Presupuestos_Frm_Ope_Psp_Asignar_Tipo_Presupuesto : System.Web.UI.Page
{
    #region PAGE LOAD

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Asignar_Tipo_Presupuesto_Inicio();
        }
    }

    #endregion

    #region METODOS

    #region (Metodos Generales)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Asignar_Tipo_Presupuesto_Inicio
    ///DESCRIPCIÓN          : Metodo para el inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Asignar_Tipo_Presupuesto_Inicio()
    {
        try
        {
            Mostrar_Ocultar_Error(false);
            Limpiar_Formulario();
            Llenar_Combo_Anio();
            Llenar_Grid_Parametros();
            Habilitar_Forma(false);
            Estado_Botones("inicial");
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Asignar_Tipo_Presupuesto_Inicio ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
    ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
    ///PROPIEDADES          1 Estatus true o false para habilitar los controles
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Habilitar_Forma(Boolean Estatus)
    {
        try
        {
            Cmb_Anio.Enabled = Estatus;
            Rbl_Tipos_Presupuestos.Enabled = Estatus;
            Grid_Parametros.Enabled = !Estatus;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Habilitar_Forma ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Formulario
    ///DESCRIPCIÓN          : Metodo para limpiar los controles de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Formulario()
    {
        try
        {
            Rbl_Tipos_Presupuestos.SelectedIndex = -1;
            Cmb_Anio.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Limpiar_Formulario ERROR[" + ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Estado_Botones
    ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
    ///PARAMETROS:          1.- String Estado: El estado de los botones solo puede tomar 
    ///                        + inicial
    ///                        + nuevo
    ///                        + modificar
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Estado_Botones(String Estado)
    {
        try
        {
            switch (Estado)
            {
                case "inicial":
                 
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";

                    //Boton Salir
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    break;
                case "nuevo":
                    //Boton Nuevo
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Nuevo.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    //Boton Salir
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Enabled = true;
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    break;
            }//fin del switch
        }
        catch (Exception ex)
        {
            throw new Exception("Error al habilitar el  Estado_Botones ERROR[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Sessiones
    ///DESCRIPCIÓN          : Metodo para limpiar las sessiones del formulario
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = String.Empty;

        try
        {
            if (Cmb_Anio.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un año. <br />";
                Datos_Validos = false;
            }
            if (Rbl_Tipos_Presupuestos.SelectedIndex < 0)
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un tipo de presupuesto. <br />";
                Datos_Validos = false;
            }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }

    //********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mostrar_Ocultar_Error
    ///DESCRIPCIÓN          : Metodo para mostrar u ocultar los errores
    ///PROPIEDADES          1 Estatus ocultar o mostrar
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Mostrar_Ocultar_Error(Boolean Estatus)
    {
        Lbl_Mensaje_Error.Visible = Estatus;
        Lbl_Ecabezado_Mensaje.Visible = Estatus;
        IBtn_Imagen_Error.Visible = Estatus;
        Div_Contenedor_Msj_Error.Visible = Estatus;
    }

    #endregion

    #region (Metodos Combos/Grid)
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Anio
    ///DESCRIPCIÓN          : Metodo para llenar el combo del año activo a presupuestar
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Combo_Anio()
    {
        Cls_Ope_Psp_Tipo_Presupuesto_Negocio Tipo_Presupuesto_Negocio = new Cls_Ope_Psp_Tipo_Presupuesto_Negocio();
        DataTable Dt_Anio = new DataTable();
        try
        {
            Dt_Anio = Tipo_Presupuesto_Negocio.Consultar_Anio();
            Cmb_Anio.Items.Clear();
            if (Dt_Anio != null)
            {
                if (Dt_Anio.Rows.Count > 0)
                {
                    Cmb_Anio.Items.Clear();
                    Cmb_Anio.DataValueField = Cat_Psp_Parametros.Campo_Anio_Presupuestar;
                    Cmb_Anio.DataTextField = Cat_Psp_Parametros.Campo_Anio_Presupuestar;
                    Cmb_Anio.DataSource = Dt_Anio;
                    Cmb_Anio.DataBind();
                }
                else {
                    Lbl_Mensaje_Error.Text = "No existen años para seleccionar el tipo de presupuesto";
                    Lbl_Mensaje_Error.Visible = true;
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Mensaje_Error.Text = "No existen años para seleccionar el tipo de presupuesto";
                Lbl_Mensaje_Error.Visible = true;
                Div_Contenedor_Msj_Error.Visible = true;
            }
            Cmb_Anio.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Anio ERROR[" + ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Parametros
    ///DESCRIPCIÓN          : Metodo para llenar el grid de los parametros
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Llenar_Grid_Parametros()
    {
        Cls_Ope_Psp_Tipo_Presupuesto_Negocio Tipo_Presupuesto_Negocio = new Cls_Ope_Psp_Tipo_Presupuesto_Negocio();
        DataTable Dt_Parametros = new DataTable();
        try
        {
            Dt_Parametros = Tipo_Presupuesto_Negocio.Consultar_Parametros();
            Grid_Parametros.DataBind();
            if (Dt_Parametros != null)
            {
                    Grid_Parametros.DataSource = Dt_Parametros;
                    Grid_Parametros.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Llenar_Combo_Anio ERROR[" + ex.Message + "]");
        }
    }

    #endregion

    #endregion
    
    #region EVENTOS

    #region EVENTOS GENERALES
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton de salir
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Febrero/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        switch (Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Asignar_Tipo_Presupuesto_Inicio();
                break;
            case "Salir":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                break;
        }//fin del switch
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
    ///DESCRIPCIÓN          : Evento del boton nuevo
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 10/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        Cls_Ope_Psp_Tipo_Presupuesto_Negocio Tipo_Presupuesto_Negocio = new Cls_Ope_Psp_Tipo_Presupuesto_Negocio();
        Cls_Ope_Psp_Manejo_Presupuesto Manejo_Presupuesto = new Cls_Ope_Psp_Manejo_Presupuesto();
        Mostrar_Ocultar_Error(false);
        
        try
        {
            switch (Btn_Nuevo.ToolTip)
            {
                case "Nuevo":
                        Estado_Botones("nuevo");
                        Limpiar_Formulario();
                        Habilitar_Forma(true);
                    break;
                case "Guardar":
                    if (Validar_Datos())
                    {
                        Tipo_Presupuesto_Negocio.P_Anio = Cmb_Anio.SelectedItem.Value.Trim();
                        Tipo_Presupuesto_Negocio.P_Tipo_Presupuesto = Rbl_Tipos_Presupuestos.SelectedItem.Text.Trim();
                        if (Tipo_Presupuesto_Negocio.Alta_Parametros())
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta", "alert('Alta Exitosa');", true);
                            Asignar_Tipo_Presupuesto_Inicio();
                        }
                        else 
                        {
                            Lbl_Ecabezado_Mensaje.Text = "Error al tratar de dar de alta los datos.";
                            Lbl_Mensaje_Error.Text = "";
                            Mostrar_Ocultar_Error(true);
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "Favor de:";
                        Mostrar_Ocultar_Error(true);
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                    break;
            }//fin del swirch
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de dar de alta los datos ERROR[" + ex.Message + "]");
        }
    }//fin del boton Nuevo

    #endregion

    #endregion
}
