﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Almacen_Recepcion_Material.Negocio;
using JAPAMI.Almacen_Registrar_Factura.Negocio;
using JAPAMI.Rpt_Alm_Orden_Entrada.Negocio;
using JAPAMI.Reportes;
using JAPAMI.Listado_Ordenes_Compra.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;

public partial class paginas_Almacen_Frm_Ope_Alm_Recepcion_Material : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Configuracion_Formulario(true);
                Llenar_Grid_Ordenes_Compra(0);
                Llena_Combo_Proveedores();
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN         : Carga una configuracion de los controles del Formulario
        ///PARAMETROS          : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                  de los controles.
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Div_Busqueda.Visible = Estatus;
            Div_Orden_Compra.Visible = !Estatus;
            Btn_Exportar_PDF.Visible = !Estatus;
            Btn_Recepcion_Material.Visible = !Estatus;
            Grid_Ordenes_Compra.SelectedIndex = -1;

            Btn_Salir.AlternateText = Estatus ? "Salir" : "Cancelar";

            if (!Estatus)
            {
                //DataTable Dt_Facturas = new DataTable();
                //DataRow Dr_Factura;

                //Dt_Facturas.Columns.Add("NO_REGISTRO", typeof(System.Int64));
                //Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrement = true;
                //Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrementSeed = 1;
                //Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrementStep = 1;
                //Dt_Facturas.Columns.Add("NO_FACTURA", typeof(System.String));
                //Dt_Facturas.Columns.Add("IMPORTE", typeof(System.Double));
                //Dt_Facturas.Columns.Add("IVA_FACTURA", typeof(System.Double));
                //Dt_Facturas.Columns.Add("TOTAL_FACTURA", typeof(System.Double));
                //Dt_Facturas.Columns.Add("FECHA", typeof(System.String));
                //Dr_Factura = Dt_Facturas.NewRow();

                //Dr_Factura["IMPORTE"] = Double.Parse(Lbl_SubTotal.Text);
                //Dr_Factura["IVA_FACTURA"] = Double.Parse(Lbl_IVA.Text);
                //Dr_Factura["TOTAL_FACTURA"] = Double.Parse(Lbl_Total.Text);
                //Dr_Factura["FECHA"] = DateTime.Now.ToString("dd/MMM/yyyy");
                //Dt_Facturas.Rows.Add(Dr_Factura);

                //Session["Dt_Facturas"] = Dt_Facturas;
                //Grid_Facturas.DataSource = Dt_Facturas;
                //Grid_Facturas.Columns[0].Visible = true;
                //Grid_Facturas.DataBind();
                //Grid_Facturas.Columns[0].Visible = false;

                Cls_Ope_Alm_Registrar_Factura_Negocio Negocio = new Cls_Ope_Alm_Registrar_Factura_Negocio();
                Cmb_Doc_Soporte.DataSource = Negocio.Consulta_Documentos_Soporte();
                Cmb_Doc_Soporte.DataTextField = Cat_Com_Documentos.Campo_Nombre;
                Cmb_Doc_Soporte.DataValueField = Cat_Com_Documentos.Campo_Documento_ID;
                Cmb_Doc_Soporte.DataBind();
            }
            else
            {
                Limpiar_Catalogo();
                Llenar_Grid_Ordenes_Compra(0);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Ordenes_Compra
        ///DESCRIPCIÓN         : Llena el Listado con una consulta que puede o no
        ///                      tener Filtros.
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Ordenes_Compra(int Pagina)
        {
            Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();

            if (!String.IsNullOrEmpty(Txt_Producto.Text))
            { Negocio.P_Producto = Txt_Producto.Text.Trim(); }
            if (!String.IsNullOrEmpty(Txt_Busqueda_Requisicion.Text))
            { Negocio.P_No_Requisicion = Txt_Busqueda_Requisicion.Text.Trim(); }

            if (!String.IsNullOrEmpty(Txt_Busqueda_Orden_Compra.Text))
            { Negocio.P_No_Orden_Compra = Txt_Busqueda_Orden_Compra.Text.Trim(); }

            if (Chk_Proveedor.Checked)
            { Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedValue.Trim(); }

            if (Chk_Fecha.Checked)
            {
                Negocio.P_Fecha_Inicio_B = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(Txt_Fecha_Inicio.Text)) + " 00:00:00";
                Negocio.P_Fecha_Fin_B = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(Txt_Fecha_Fin.Text)) + " 23:59:59";
            }

            Negocio.P_Almacen_General = Chk_Almacen_General.Checked ? "'SI'" : "'NO'";
            Negocio.P_Estatus = "'PROVEEDOR_ENTERADO', 'SURTIDA_PARCIAL'";
            Grid_Ordenes_Compra.DataSource = Negocio.Consulta_Ordenes_Compra();
            Grid_Ordenes_Compra.Columns[7].Visible = true;
            Grid_Ordenes_Compra.Columns[8].Visible = true;
            Grid_Ordenes_Compra.Columns[9].Visible = true;
            Grid_Ordenes_Compra.Columns[10].Visible = true;
            Grid_Ordenes_Compra.Columns[11].Visible = true;
            Grid_Ordenes_Compra.Columns[12].Visible = true;
            Grid_Ordenes_Compra.DataBind();
            Grid_Ordenes_Compra.Columns[7].Visible = false;
            Grid_Ordenes_Compra.Columns[8].Visible = false;
            Grid_Ordenes_Compra.Columns[9].Visible = false;
            Grid_Ordenes_Compra.Columns[10].Visible = false;
            Grid_Ordenes_Compra.Columns[11].Visible = false;
            Grid_Ordenes_Compra.Columns[12].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Chk_Almacen_General.Checked = false;
            Chk_Fecha.Checked = false;
            Chk_Proveedor.Checked = false;
            Chk_Recepcion_Completa.Checked = false;
            Txt_Fecha_Fin.Text = String.Empty;
            Txt_Fecha_Inicio.Text = String.Empty;
            Txt_Busqueda_Orden_Compra.Text = String.Empty;
            Txt_Busqueda_Requisicion.Text = String.Empty;
            Txt_Folio.Text = String.Empty;
            Txt_Fecha_Construccion.Text = String.Empty;
            Txt_Requisicion.Text = String.Empty;
            Txt_Estatus.Text = String.Empty;
            Txt_Proveedor.Text = String.Empty;
            Txt_Observaciones.Text = String.Empty;
            Txt_Elaboro.Text = String.Empty;
            Txt_Dependencia.Text = String.Empty;
            Txt_Justificacion.Text = String.Empty;
            Hdf_Listado_Almacen.Value = String.Empty;
            Lbl_SubTotal.Text = "$ 0.00";
            Lbl_IEPS.Text = "$ 0.00";
            Lbl_IVA.Text = "$ 0.00";
            Lbl_Total.Text = "$ 0.00";

            Session["Dt_Documentos"] = null;
            Session["Dt_Facturas"] = null;

            Lbl_Mensaje_Error.Text = String.Empty;
            Div_Contenedor_Msj_Error.Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llena_Combo_Proveedores
        ///DESCRIPCIÓN         : 
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llena_Combo_Proveedores()
        {
            Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();
            Cmb_Proveedores.DataSource = Negocio.Consulta_Proveedores();
            Cmb_Proveedores.DataTextField = Cat_Com_Proveedores.Campo_Nombre;
            Cmb_Proveedores.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
            Cmb_Proveedores.DataBind();
        }

    #endregion

    #region Validaciones

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Facturas
        ///DESCRIPCIÓN         : 
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private Boolean Validar_Facturas()
        {
            Boolean Validos = true;
            DataTable Dt_Facturas = new DataTable();
            Double Total_OC = 0;
            Double Importe_Factura = 0;

            Dt_Facturas = (DataTable)Session["Dt_Facturas"];
            //obtenemos el total de la factura
            TextBox Txt_Total_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Total_Factura");
            Importe_Factura = Double.Parse( Txt_Total_Factura.Text.Trim());
            Double.TryParse(Lbl_Total.Text, out Total_OC);

            //foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
            //{
            //    Importe_Factura += Double.Parse(Dr_Factura["TOTAL_FACTURA"].ToString());
            //}

            if (Importe_Factura > Total_OC)
            {
                Lbl_Mensaje_Error.Text = "+ Revisar el importe de la factuta ya que sobrepasa al monto de la orden de compra. Diferencia: " + Math.Round((Importe_Factura - Total_OC), 3);
                Div_Contenedor_Msj_Error.Visible = true;
                Validos = false;
            }
            else
            {
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
            }
            return Validos;
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Recepcion_Material_Click
        ///DESCRIPCIÓN         : Realiza la recepcion de material.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Recepcion_Material_Click(object sender, EventArgs e)
        {
            try
            {
                if (Validar_Facturas())
                {
                    Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();

                    DataTable Dt_Productos = new DataTable();
                    DataTable Dt_Documentos = new DataTable();
                    DataTable Dt_Facturas = new DataTable();
                    String Estatus = String.Empty;
                    Double Ctd_Solicitada = 0;
                    Double Ctd_Entregada = 0;
                    Double Ctd_Recibir = 0;

                    if (!String.IsNullOrEmpty(Hdf_Listado_Almacen.Value) && Hdf_Listado_Almacen.Value == "SI")
                    {
                        Estatus = "COMPLETA";
                    }
                    else
                    {
                        Estatus = "SURTIDA_REGISTRO";
                    }

                    Dt_Productos.Columns.Add("CLAVE", typeof(System.String));
                    Dt_Productos.Columns.Add("CANTIDAD_A_RECIBIR", typeof(System.String));
                    Dt_Productos.Columns.Add("COSTO_REAL", typeof(System.String));
                    Dt_Productos.Columns.Add("MONTO", typeof(System.String));

                    Dt_Documentos.Columns.Add("DOCUMENTO_ID", typeof(System.String));

                    foreach (GridViewRow Dr_Producto in Grid_Orden_Compra_Detalles.Rows)
                    {
                        TextBox Txt_Cantidad_Recibida = (TextBox)Dr_Producto.Cells[5].FindControl("Txt_Cantidad_Recibida");
                        Double.TryParse(HttpUtility.HtmlDecode(Dr_Producto.Cells[3].Text), out Ctd_Solicitada);
                        Double.TryParse(HttpUtility.HtmlDecode(Dr_Producto.Cells[4].Text), out Ctd_Entregada);
                        Double.TryParse(HttpUtility.HtmlDecode(Txt_Cantidad_Recibida.Text), out Ctd_Recibir);

                        if (Ctd_Entregada + Ctd_Recibir < Ctd_Solicitada)
                        { Estatus = "SURTIDA_PARCIAL"; }

                        DataRow Dr_Renglon = Dt_Productos.NewRow();

                        Dr_Renglon["CLAVE"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[0].Text);
                        Dr_Renglon["CANTIDAD_A_RECIBIR"] = Ctd_Recibir;
                        Dr_Renglon["COSTO_REAL"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[7].Text.Replace("$", "").Replace(",", ""));
                        Dr_Renglon["MONTO"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[8].Text.Replace("$", "").Replace(",", ""));

                        if (Ctd_Recibir > 0)
                        {
                            Dt_Productos.Rows.Add(Dr_Renglon);
                            Dt_Productos.AcceptChanges();
                        }
                    }

                    foreach (GridViewRow Dr_Documento in Grid_Doc_Soporte.Rows)
                    {
                        DataRow Dr_Renglon = Dt_Documentos.NewRow();
                        Dr_Renglon["DOCUMENTO_ID"] = HttpUtility.HtmlDecode(Dr_Documento.Cells[1].Text.ToString().Trim());
                        Dt_Documentos.Rows.Add(Dr_Renglon);
                        Dt_Documentos.AcceptChanges();
                    }
                    //Agregamos el numero de factura
                    Dt_Facturas = (DataTable)Session["Dt_Facturas"];
                    TextBox Txt_No_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_No_Factura");

                    if (Txt_No_Factura != null)
                    {
                        Dt_Facturas.Rows[0]["NO_FACTURA"] = Txt_No_Factura.Text;

                    }//fin del IF
                    Session["Dt_Facturas"] = Dt_Facturas;
                    
                    Dt_Facturas = (DataTable)Session["Dt_Facturas"];
                    TextBox Txt_Importe_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Importe_Factura");
                    TextBox Txt_IVA_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_IVA_Factura");
                    TextBox Txt_Total_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Total_Factura");
                    TextBox Txt_Fecha_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Fecha_Factura");

                    //Modificamos el datatable de las facturas para cargar nuevamente los montos 
                    Dt_Facturas.Rows[0]["IMPORTE"] =  Double.Parse( Txt_Importe_Factura.Text.Trim());
                    Dt_Facturas.Rows[0]["IVA_FACTURA"] = Double.Parse(Txt_IVA_Factura.Text.Trim());
                    Dt_Facturas.Rows[0]["TOTAL_FACTURA"] = Double.Parse(Txt_Total_Factura.Text.Trim());
                    Dt_Facturas.Rows[0]["FECHA"] = DateTime.Parse(Txt_Fecha_Factura.Text.Trim()).ToString("dd/MMM/yyyy");

                    Negocio.P_No_Orden_Compra = Txt_Folio.Text.Replace("OC-", "");
                    Negocio.P_Estatus = Estatus;
                    Negocio.P_Dt_Productos = Dt_Productos;
                    Negocio.P_Dt_Facturas_Proveedores = Dt_Facturas;
                    Negocio.P_Fecha_Factura = DateTime.Parse(Dt_Facturas.Rows[0]["FECHA"].ToString());
                    Negocio.P_Dt_Documentos = Dt_Documentos;
                    Negocio.P_Observaciones = Txt_Observaciones.Text.Trim().Length > 249 ? Txt_Observaciones.Text.Substring(0, 249) : Txt_Observaciones.Text.Trim();

                    String Mensaje = Negocio.Actualizar_Orden_Compra();

                    if (Mensaje.Contains("OE-"))
                    {
                        Cls_Rpt_Alm_Orden_Entrada_Negocio Reporte = new Cls_Rpt_Alm_Orden_Entrada_Negocio();
                        Reporte.P_Orden_Entrada = Mensaje.Replace("OE-", "");
                        DataSet Ds_OE = Reporte.Detalles_Orden_Entrada();

                        String Reporte_Crystal = "../Rpt/Almacen/Rpt_Alm_Orden_Entrada.rpt";
                        String Nombre_Reporte = "Orden_Entrada_" + Reporte.P_Orden_Entrada + ".pdf";
                        Cls_Reportes Reportes = new Cls_Reportes(); //variable para la generacion de lols reportes

                        Reportes.Generar_Reporte(ref Ds_OE, Reporte_Crystal, Nombre_Reporte, "PDF");
                        Mostrar_Reporte(Nombre_Reporte, "PDF");
                    }
                    Configuracion_Formulario(true);
                }
            }
            catch (Exception Ex)
            {
               bool Error_Cuenta = Ex.Message.Contains('*');
                if(Error_Cuenta)
                    Lbl_Mensaje_Error.Text = "Asignar cuenta contable al Proveedor, no se puede completar la operación";
                else
                    Lbl_Mensaje_Error.Text = Ex.Message;
               Div_Contenedor_Msj_Error.Visible = true;
                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Calcular_Totales_Facturas
        ///DESCRIPCIÓN         : Calcula, Subtotal, IVa y total de la Factura.
        ///PROPIEDADES         :
        ///CREO                : Susana Trigueros Armenta
        ///FECHA_CREO          : 11/Sep/2013
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        public void Calcular_Totales_Facturas()
        {       
            DataTable Dt_Productos = new DataTable();
            Double Ctd_Solicitada = 0;
            Double Ctd_Entregada = 0;
            Double Ctd_Recibir = 0;
            Dt_Productos.Columns.Add("CLAVE", typeof(System.String));
            Dt_Productos.Columns.Add("CANTIDAD_A_RECIBIR", typeof(System.String));
            Dt_Productos.Columns.Add("COSTO_REAL", typeof(System.String));
            Dt_Productos.Columns.Add("MONTO", typeof(System.String));
            //Recorremos el grid para hacer el calculo de todos los productos que se van a recibir
            foreach (GridViewRow Dr_Producto in Grid_Orden_Compra_Detalles.Rows)
            {
                TextBox Txt_Cantidad_Recibida = (TextBox)Dr_Producto.Cells[5].FindControl("Txt_Cantidad_Recibida");
                Double.TryParse(HttpUtility.HtmlDecode(Dr_Producto.Cells[3].Text), out Ctd_Solicitada);
                Double.TryParse(HttpUtility.HtmlDecode(Dr_Producto.Cells[4].Text), out Ctd_Entregada);
                Double.TryParse(HttpUtility.HtmlDecode(Txt_Cantidad_Recibida.Text), out Ctd_Recibir);

                DataRow Dr_Renglon = Dt_Productos.NewRow();

                Dr_Renglon["CLAVE"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[0].Text);
                Dr_Renglon["CANTIDAD_A_RECIBIR"] = Ctd_Recibir;
                Dr_Renglon["COSTO_REAL"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[7].Text.Replace("$", "").Replace(",", ""));
                Dr_Renglon["MONTO"] = HttpUtility.HtmlDecode(Dr_Producto.Cells[8].Text.Replace("$", "").Replace(",", ""));

                if (Ctd_Recibir > 0)
                {
                    Dt_Productos.Rows.Add(Dr_Renglon);
                    Dt_Productos.AcceptChanges();
                }
            }
            Cls_Alm_Com_Recepcion_Material_Negocio negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();
            DataTable Dt_Imp = new DataTable();
            Double Subtotal = 0.0;
            Double Monto_Total = 0.0;
            Double IVA_ENTRADA = 0.0;
            Double Precio_C_IVA = 0.0;
            Double Precio_S_IVA = 0.0;
            Double Cantidad = 0.0;
            Double Porcentaje_Impuesto = 0;
            double auxSubtotal = 0;
            double auxtotal = 0;
            foreach (DataRow Dr_Producto in Dt_Productos.Rows)
            {
                //Consultar el IVA del producto
                negocio.P_Producto = Dr_Producto["CLAVE"].ToString().Trim();
                Dt_Imp = new DataTable();
                Dt_Imp = negocio.Consultar_IVA_Producto();
                //Ejecutar consulta
                Porcentaje_Impuesto = double.Parse(Dt_Imp.Rows[0][0].ToString()) * 0.01;
                Cantidad = Double.Parse(Dr_Producto["CANTIDAD_A_RECIBIR"].ToString().Trim());
                Precio_C_IVA = Double.Parse(Dr_Producto["MONTO"].ToString().Trim());
                Precio_S_IVA = Double.Parse(Dr_Producto["COSTO_REAL"].ToString().Trim());               
                auxSubtotal = Cantidad * Precio_S_IVA;
                if (Porcentaje_Impuesto != 0)
                {
                    auxtotal = ((Precio_S_IVA * Porcentaje_Impuesto) + Precio_S_IVA) * Cantidad;
                }
                else
                {
                    auxtotal = auxSubtotal;
                }
                Subtotal = Subtotal + auxSubtotal;
                Monto_Total = Monto_Total + auxtotal;              
            }

            if (Subtotal.ToString().Contains("."))
                if (Subtotal.ToString().Substring(Subtotal.ToString().IndexOf("."), Subtotal.ToString().Length - Subtotal.ToString().IndexOf(".")).Length >= 3)
                    Subtotal = double.Parse(Subtotal.ToString().Substring(0, Subtotal.ToString().IndexOf(".") + 3));

            if (Monto_Total.ToString().Contains("."))
                if (Monto_Total.ToString().Substring(Monto_Total.ToString().IndexOf("."), Monto_Total.ToString().Length - Monto_Total.ToString().IndexOf(".")).Length >= 3)
                    Monto_Total = double.Parse(Monto_Total.ToString().Substring(0, Monto_Total.ToString().IndexOf(".") + 3));

            IVA_ENTRADA = Monto_Total - Subtotal;

            if (IVA_ENTRADA.ToString().Contains("."))
                if (IVA_ENTRADA.ToString().Substring(IVA_ENTRADA.ToString().IndexOf("."), IVA_ENTRADA.ToString().Length - IVA_ENTRADA.ToString().IndexOf(".")).Length >= 3)
                    IVA_ENTRADA = double.Parse(IVA_ENTRADA.ToString().Substring(0, IVA_ENTRADA.ToString().IndexOf(".") + 3));
            //Asignamos los valores al Grid de Facturas
            DataTable Dt_Facturas = new DataTable();
            DataRow Dr_Factura;

            Dt_Facturas.Columns.Add("NO_REGISTRO", typeof(System.Int64));
            Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrement = true;
            Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrementSeed = 1;
            Dt_Facturas.Columns["NO_REGISTRO"].AutoIncrementStep = 1;
            Dt_Facturas.Columns.Add("NO_FACTURA", typeof(System.String));
            Dt_Facturas.Columns.Add("IMPORTE", typeof(System.Double));
            Dt_Facturas.Columns.Add("IVA_FACTURA", typeof(System.Double));
            Dt_Facturas.Columns.Add("TOTAL_FACTURA", typeof(System.Double));
            Dt_Facturas.Columns.Add("FECHA", typeof(System.String));
            Dr_Factura = Dt_Facturas.NewRow();

            Dr_Factura["IMPORTE"] = Subtotal;
            Dr_Factura["IVA_FACTURA"] = IVA_ENTRADA;
            Dr_Factura["TOTAL_FACTURA"] = Monto_Total;
            Dr_Factura["FECHA"] = DateTime.Now.ToString("dd/MMM/yyyy");
            Dt_Facturas.Rows.Add(Dr_Factura);

            Session["Dt_Facturas"] = Dt_Facturas;
            Grid_Facturas.DataSource = Dt_Facturas;
            Grid_Facturas.Columns[0].Visible = true;
            Grid_Facturas.DataBind();
            Grid_Facturas.Columns[0].Visible = false;
            //Habilitamos o no el campo de las facturas
            TextBox Txt_Importe_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Importe_Factura");
            TextBox Txt_IVA_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_IVA_Factura");
            TextBox Txt_Total_Factura = (TextBox)Grid_Facturas.Rows[0].FindControl("Txt_Total_Factura");
            //Consultamos los permisos de el usuario logueado 
            Cls_Alm_Com_Recepcion_Material_Negocio clase_neg = new Cls_Alm_Com_Recepcion_Material_Negocio();
            clase_neg.P_Empleado_Almacen_ID = Cls_Sessiones.Empleado_ID;
            DataTable dt_empleados = clase_neg.Consultar_Permisos_Empleados();

            if (dt_empleados.Rows.Count > 0)
            {
                Txt_Importe_Factura.ReadOnly = false;
                Txt_IVA_Factura.ReadOnly = false;
                Txt_Total_Factura.ReadOnly = false;
            }
            else
            {
                Txt_Importe_Factura.ReadOnly = true;
                Txt_IVA_Factura.ReadOnly = true;
                Txt_Total_Factura.ReadOnly = true;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_PDF_Click
        ///DESCRIPCIÓN         : Muestra la Orden de Compra.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Exportar_PDF_Click(object sender, EventArgs e)
        {
            try
            {
                Cls_Ope_Com_Listado_Ordenes_Compra_Negocio Listado_Negocio = new Cls_Ope_Com_Listado_Ordenes_Compra_Negocio();

                DataTable Dt_Cabecera_OC = new DataTable();
                DataTable Dt_Detalles_OC = new DataTable();

                Listado_Negocio.P_No_Orden_Compra = Txt_Folio.Text.Replace("OC-", "");
                // Consultar Cabecera de la Orden de compra
                Dt_Cabecera_OC = Listado_Negocio.Consulta_Cabecera_Orden_Compra();
                // Consultar los detalles de la Orden de compra
                Dt_Detalles_OC = Listado_Negocio.Consulta_Detalles_Orden_Compra();
                // Instanciar el DataSet Fisico
                if (Convert.ToInt32(Dt_Cabecera_OC.Rows[0]["TOTAL"]) >= 50000)
                {
                    Ds_Ope_Com_Orden_Compra_Tes Ds_Orden_Compra = new Ds_Ope_Com_Orden_Compra_Tes();
                    // Instanciar al método que muestra el reporte
                    Generar_Reporte(Dt_Cabecera_OC, Dt_Detalles_OC, Ds_Orden_Compra, "PDF", true);
                }
                else
                {
                    Ds_Ope_Com_Orden_Compra Ds_Orden_Compra = new Ds_Ope_Com_Orden_Compra();
                    // Instanciar al método que muestra el reporte
                    Generar_Reporte(Dt_Cabecera_OC, Dt_Detalles_OC, Ds_Orden_Compra, "PDF", false);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, EventArgs e)
        {
           Busqueda();
        }

        public void Busqueda()
        {
            String Mensaje = "";

            if (Chk_Fecha.Checked &&
                (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && String.IsNullOrEmpty(Txt_Fecha_Fin.Text)) ||
                (String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && !String.IsNullOrEmpty(Txt_Fecha_Fin.Text)))
            {
                Mensaje += "+ Ingresar ambas fechas.";
            }
            

            if (String.IsNullOrEmpty(Mensaje))
            {
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
                Llenar_Grid_Ordenes_Compra(0);
            }
            else
            {
                Lbl_Mensaje_Error.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE: Txt_No_Factura_TextChanged
        /// DESCRIPCION       : Evento utilizado para validar las cantidades que asigna el usuario                    
        /// PARAMETROS        :
        /// CREO              : Salvador Vázquez Camacho.
        /// FECHA_CREO        : 21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Txt_No_Factura_TextChanged(object sender, EventArgs e)
        {
            //TextBox Txt_No_Factura = (TextBox)sender;
            //GridViewRow Grid_Row = (GridViewRow)Txt_No_Factura.Parent.Parent;
            //String No_Registro = HttpUtility.HtmlDecode(Grid_Row.Cells[0].Text.Trim());

            //DataTable Dt_Facturas = new DataTable();
            //DataTable Dt_Factura_Existente = new DataTable();

            //Dt_Facturas = (DataTable)Session["Dt_Facturas"];
            //Dt_Facturas.DefaultView.RowFilter = "NO_FACTURA = '" + Txt_No_Factura.Text.Trim() + "'";
            //Dt_Factura_Existente = Dt_Facturas.DefaultView.ToTable();

            //if (Dt_Factura_Existente.Rows.Count < 1)
            //{
            //    foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
            //    {
            //        if (Dr_Factura["NO_REGISTRO"].ToString() == No_Registro)
            //        {
            //            Dr_Factura["NO_FACTURA"] = Txt_No_Factura.Text.Trim();
            //            Dr_Factura.AcceptChanges();
            //            Dt_Facturas.AcceptChanges();
            //            break;
            //        }
            //    }
            //    Session["Dt_Facturas"] = Dt_Facturas;
            //}
            //else
            //{
            //    Txt_No_Factura.Text = String.Empty;
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Recepción de Mercancias", "alert('El No. de Factura ya existe.');", true);
            //}
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE: Txt_Importe_Factura_TextChanged
        /// DESCRIPCION       : Evento utilizado para validar las cantidades que asigna el usuario                    
        /// PARAMETROS        :
        /// CREO              : Salvador Vázquez Camacho.
        /// FECHA_CREO        : 21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Txt_Importe_Factura_TextChanged(object sender, EventArgs e)
        {
            //TextBox Txt_Importe = (TextBox)sender;
            //GridViewRow Grid_Row = (GridViewRow)Txt_Importe.Parent.Parent;
            //String No_Registro = HttpUtility.HtmlDecode(Grid_Row.Cells[0].Text.Trim());
            //DataTable Dt_Facturas = new DataTable();

            //try
            //{
            //    Dt_Facturas = (DataTable)Session["Dt_Facturas"];

            //    foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
            //    {
            //        if (Dr_Factura["NO_REGISTRO"].ToString() == No_Registro)
            //        {
            //            Double Importe = 0;
            //            Double IVA = 0;
            //            Double Total = 0;

            //            Double.TryParse(Txt_Importe.Text, out Importe);
            //            IVA = Importe * 0.16;
            //            Total = Importe + IVA;

            //            Dr_Factura["IMPORTE"] = Importe;
            //            Dr_Factura["IVA_FACTURA"] = IVA;
            //            Dr_Factura["TOTAL_FACTURA"] = Total;
            //            Dr_Factura.AcceptChanges();
            //            Dt_Facturas.AcceptChanges();
            //            break;
            //        }
            //    }
            //    Session["Dt_Facturas"] = Dt_Facturas;
            //    Grid_Facturas.DataSource = Dt_Facturas;
            //    Grid_Facturas.Columns[0].Visible = true;
            //    Grid_Facturas.DataBind();
            //    Grid_Facturas.Columns[0].Visible = false;

            //    Validar_Facturas();
            //}
            //catch (Exception Ex)
            //{
            //    Lbl_Mensaje_Error.Text = Ex.Message;
            //    Div_Contenedor_Msj_Error.Visible = true;
            //}
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE: Txt_IVA_Factura_TextChanged
        /// DESCRIPCION       : Evento utilizado para validar las cantidades que asigna el usuario                    
        /// PARAMETROS        :
        /// CREO              : Salvador Vazquez Camacho.
        /// FECHA_CREO        : 21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Txt_IVA_Factura_TextChanged(object sender, EventArgs e)
        {
            //TextBox Txt_IVA_Factura = (TextBox)sender;
            //GridViewRow Grid_Row = (GridViewRow)Txt_IVA_Factura.Parent.Parent;
            //String No_Registro = HttpUtility.HtmlDecode(Grid_Row.Cells[0].Text.Trim());

            //DataTable Dt_Facturas = new DataTable();

            //try
            //{
            //    Dt_Facturas = (DataTable)Session["Dt_Facturas"];

            //    foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
            //    {
            //        if (Dr_Factura["NO_REGISTRO"].ToString() == No_Registro)
            //        {
            //            Double Importe = 0;
            //            Double IVA = 0;
            //            Double Total = 0;

            //            Double.TryParse(Dr_Factura["IMPORTE"].ToString(), out Importe);
            //            Double.TryParse(Txt_IVA_Factura.Text, out IVA);
            //            Total = Importe + IVA;

            //            Dr_Factura["IVA_FACTURA"] = IVA;
            //            Dr_Factura["TOTAL_FACTURA"] = Total;
            //            Dr_Factura.AcceptChanges();
            //            Dt_Facturas.AcceptChanges();
            //            break;
            //        }
            //    }
            //    Session["Dt_Facturas"] = Dt_Facturas;
            //    Grid_Facturas.DataSource = Dt_Facturas;
            //    Grid_Facturas.Columns[0].Visible = true;
            //    Grid_Facturas.DataBind();
            //    Grid_Facturas.Columns[0].Visible = false;

            //    Validar_Facturas();
            //}
            //catch (Exception Ex)
            //{
            //    Lbl_Mensaje_Error.Text = Ex.Message;
            //    Div_Contenedor_Msj_Error.Visible = true;
            //}
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE: Txt_Fecha_Factura_TextChanged
        /// DESCRIPCION       : Evento utilizado para validar las cantidades que asigna el usuario                    
        /// PARAMETROS        :
        /// CREO              : Salvador Vázquez Camacho.
        /// FECHA_CREO        : 21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Txt_Fecha_Factura_TextChanged(object sender, EventArgs e)
        {
            //TextBox Txt_Fecha_Factura = (TextBox)sender;
            //GridViewRow Grid_Row = (GridViewRow)Txt_Fecha_Factura.Parent.Parent;
            //String No_Registro = HttpUtility.HtmlDecode(Grid_Row.Cells[0].Text.Trim());
            
            //DataTable Dt_Facturas = new DataTable();

            //try
            //{
            //    Dt_Facturas = (DataTable)Session["Dt_Facturas"];

            //    foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
            //    {
            //        if (Dr_Factura["NO_REGISTRO"].ToString() == No_Registro)
            //        {
            //            Dr_Factura["FECHA"] = Txt_Fecha_Factura.Text;
            //            Dr_Factura.AcceptChanges();
            //            Dt_Facturas.AcceptChanges();
            //            break;
            //        }
            //    }
            //    Session["Dt_Facturas"] = Dt_Facturas;
            //}
            //catch (Exception Ex)
            //{
            //    Lbl_Mensaje_Error.Text = Ex.Message;
            //    Div_Contenedor_Msj_Error.Visible = true;
            //}
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE: Txt_Fecha_Factura_TextChanged
        /// DESCRIPCION       : Evento utilizado para validar las cantidades que asigna el usuario                    
        /// PARAMETROS        :
        /// CREO              : Susana Trigueros Armenta 
        /// FECHA_CREO        : 11/Sep/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Txt_Cantidad_Recibida_TextChanged(object sender, EventArgs e)
        {            
            //Realizamos los calculos automaticos de la Factura
            Calcular_Totales_Facturas();
        }


        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Btn_Agregar_Facturas_Click
        /// DESCRIPCION:            Evento utilizado para agregar registros al Grid
        ///                         donde se va asignar los datos de las facturas
        /// PARAMETROS :            
        /// CREO       :            Salvador Vázquez Camacho.
        /// FECHA_CREO :            21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Btn_Agregar_Facturas_Click(object sender, ImageClickEventArgs e)
        {
            DataTable Dt_Facturas = new DataTable();
            DataRow Dr_Factura;
            Int64 No_Registro = new Int64();

            try
            {
                Dt_Facturas.Columns.Add("NO_REGISTRO", typeof(System.Int64));
                Dt_Facturas.Columns.Add("NO_FACTURA", typeof(System.String));
                Dt_Facturas.Columns.Add("IMPORTE", typeof(System.Double));
                Dt_Facturas.Columns.Add("IVA_FACTURA", typeof(System.Double));
                Dt_Facturas.Columns.Add("TOTAL_FACTURA", typeof(System.Double));
                Dt_Facturas.Columns.Add("FECHA", typeof(System.String));
                
                foreach (GridViewRow Dr_Renglon in Grid_Facturas.Rows)
                {
                    TextBox Txt_No_Factura = new TextBox();
                    TextBox Txt_Importe_Factura = new TextBox();
                    TextBox Txt_IVA_Factura = new TextBox();
                    TextBox Txt_Total_Factura = new TextBox();
                    TextBox Txt_Fecha_Factura = new TextBox();

                    Txt_No_Factura = (TextBox)Dr_Renglon.Cells[1].FindControl("Txt_No_Factura");
                    Txt_Importe_Factura = (TextBox)Dr_Renglon.Cells[2].FindControl("Txt_Importe_Factura");
                    Txt_IVA_Factura = (TextBox)Dr_Renglon.Cells[3].FindControl("Txt_IVA_Factura");
                    Txt_Total_Factura = (TextBox)Dr_Renglon.Cells[4].FindControl("Txt_Total_Factura");
                    Txt_Fecha_Factura = (TextBox)Dr_Renglon.Cells[5].FindControl("Txt_Fecha_Factura");


                    Dr_Factura = Dt_Facturas.NewRow();
                    Dr_Factura["NO_REGISTRO"] = HttpUtility.HtmlDecode(Dr_Renglon.Cells[0].Text);
                    Dr_Factura["NO_FACTURA"] = Txt_No_Factura.Text;
                    Dr_Factura["IMPORTE"] = String.IsNullOrEmpty(Txt_Importe_Factura.Text) ? 0 : Double.Parse(Txt_Importe_Factura.Text);
                    Dr_Factura["IVA_FACTURA"] = String.IsNullOrEmpty(Txt_IVA_Factura.Text) ? 0 : Double.Parse(Txt_IVA_Factura.Text);
                    Dr_Factura["TOTAL_FACTURA"] = String.IsNullOrEmpty(Txt_Total_Factura.Text) ? 0 : Double.Parse(Txt_Total_Factura.Text);
                    Dr_Factura["FECHA"] = Txt_Fecha_Factura.Text;
                    Dt_Facturas.Rows.Add(Dr_Factura);
                    Dt_Facturas.AcceptChanges();

                    No_Registro = Int64.Parse(Dr_Factura["NO_REGISTRO"].ToString()) > No_Registro ? Int64.Parse(Dr_Factura["NO_REGISTRO"].ToString()) : 0;
                }
                No_Registro++;

                Dr_Factura = Dt_Facturas.NewRow();
                Dr_Factura["NO_REGISTRO"] = No_Registro;
                Dr_Factura["IMPORTE"] = 0.0;
                Dr_Factura["IVA_FACTURA"] = 0.0;
                Dr_Factura["TOTAL_FACTURA"] = 0.0;
                Dr_Factura["FECHA"] = DateTime.Now.ToString("dd/MMM/yyyy");
                Dt_Facturas.Rows.Add(Dr_Factura);
                Dt_Facturas.AcceptChanges();

                Session["Dt_Facturas"] = Dt_Facturas;
                Grid_Facturas.DataSource = Dt_Facturas;
                Grid_Facturas.Columns[0].Visible = true;
                Grid_Facturas.DataBind();
                Grid_Facturas.Columns[0].Visible = false;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Btn_Agregar_Facturas_Click
        /// DESCRIPCION:            Evento utilizado para agregar registros al Grid
        ///                         donde se va asignar los datos de las facturas
        /// PARAMETROS :            
        /// CREO       :            Salvador Vázquez Camacho.
        /// FECHA_CREO :            21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Btn_Quitar_Factura_Click(object sender, ImageClickEventArgs e)
        {
            DataTable Dt_Facturas = new DataTable();
            int No_Registro;

            try
            {
                Dt_Facturas = (DataTable)Session["Dt_Facturas"];

                No_Registro = Dt_Facturas.Rows.Count;
                No_Registro--;

                if (No_Registro > 0)
                {
                    Dt_Facturas.Rows.RemoveAt(No_Registro);
                    Dt_Facturas.AcceptChanges();

                    Session["Dt_Facturas"] = Dt_Facturas;
                    Grid_Facturas.DataSource = Dt_Facturas;
                    Grid_Facturas.Columns[0].Visible = true;
                    Grid_Facturas.DataBind();
                    Grid_Facturas.Columns[0].Visible = false;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Btn_Agregar_Doc_Soporte_Click
        /// DESCRIPCION:            Evento utilizado para agregar los documentos seleccionados por
        ///                         el usuario.
        /// PARAMETROS :            
        /// CREO       :            Salvador Vázquez Camacho.
        /// FECHA_CREO :            21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Btn_Agregar_Doc_Soporte_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Alm_Registrar_Factura_Negocio Negocio = new Cls_Ope_Alm_Registrar_Factura_Negocio();
            DataTable Dt_Detalles_Doc_Soporte = new DataTable();
            DataTable Dt_Doc_Soporte = new DataTable();

            try
            {
                DataColumn[] keys = new DataColumn[1];
                DataColumn column;

                if (Session["Dt_Documentos"] == null)
                {
                    column = new DataColumn();
                    column.DataType = Type.GetType("System.String");
                    column.ColumnName = "DOCUMENTO_ID";

                    Dt_Doc_Soporte = new DataTable();
                    Dt_Doc_Soporte.Columns.Add(column);
                    Dt_Doc_Soporte.Columns.Add("NOMBRE", Type.GetType("System.String"));
                    Dt_Doc_Soporte.Columns.Add("DESCRIPCION", Type.GetType("System.String"));
                }
                else
                {
                    Dt_Doc_Soporte = (DataTable)Session["Dt_Documentos"];
                    column = Dt_Doc_Soporte.Columns["DOCUMENTO_ID"];
                }

                keys[0] = column;
                Dt_Doc_Soporte.PrimaryKey = keys;

                if (!Dt_Doc_Soporte.Rows.Contains(Cmb_Doc_Soporte.SelectedValue))
                {
                    Negocio.P_Documento_ID = Cmb_Doc_Soporte.SelectedValue;
                    Dt_Detalles_Doc_Soporte = Negocio.Consulta_Documentos_Soporte();

                    if (Dt_Detalles_Doc_Soporte.Rows.Count > 0)
                    {
                        DataRow Fila = Dt_Doc_Soporte.NewRow();
                        Fila["DOCUMENTO_ID"] = Dt_Detalles_Doc_Soporte.Rows[0]["DOCUMENTO_ID"].ToString();
                        Fila["NOMBRE"] = Dt_Detalles_Doc_Soporte.Rows[0]["NOMBRE"].ToString();
                        Fila["DESCRIPCION"] = Dt_Detalles_Doc_Soporte.Rows[0]["DESCRIPCION"].ToString();
                        Dt_Doc_Soporte.Rows.Add(Fila);
                        Dt_Doc_Soporte.AcceptChanges();
                    }

                    Grid_Doc_Soporte.DataSource = Dt_Doc_Soporte;
                    Grid_Doc_Soporte.Columns[1].Visible = true;
                    Grid_Doc_Soporte.DataBind();
                    Grid_Doc_Soporte.Columns[1].Visible = false;
                    Grid_Doc_Soporte.Visible = true;
                    Session["Dt_Documentos"] = Dt_Doc_Soporte;
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Recepción de Mercancias", "alert('El documento ya fue Agregado');", true);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Btn_Quitar_Doc_Soporte_Click
        /// DESCRIPCION:            Evento utilizado para quitar los documentos seleccionados por
        ///                         el usuario.
        /// PARAMETROS :            
        /// CREO       :            Salvador Vázquez Camacho.
        /// FECHA_CREO :            21/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Btn_Quitar_Doc_Soporte_Click(object sender, ImageClickEventArgs e)
        {
            if (Grid_Doc_Soporte.SelectedIndex > -1)
            {
                String Documento_ID = Grid_Doc_Soporte.SelectedRow.Cells[1].Text.Trim();
                DataTable Dt_Documentos = (DataTable)Session["Dt_Documentos"];
                Dt_Documentos.DefaultView.RowFilter = "DOCUMENTO_ID NOT IN ('" + Documento_ID + "')";
                Dt_Documentos = Dt_Documentos.DefaultView.ToTable();

                Grid_Doc_Soporte.DataSource = Dt_Documentos;
                Grid_Doc_Soporte.Columns[1].Visible = true;
                Grid_Doc_Soporte.DataBind();
                Grid_Doc_Soporte.Columns[1].Visible = false;
                Grid_Doc_Soporte.Visible = true;
                Session["Dt_Documentos"] = Dt_Documentos;
                Grid_Doc_Soporte.SelectedIndex = -1;
            }
            else
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Recepción de Mercancias", "alert('Seleccione el documento que desea eliminar.');", true);
            }
        }

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja la paginación del GridView
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Compra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Llenar_Grid_Ordenes_Compra(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Compra_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (Chk_Almacen_General.Checked == false)
            {
                Session["Almacen_Papeleria"] = "SI";
            }
            else
            {
                Session["Almacen_Papeleria"] = "NO";
            }
            try
            {
                if (Grid_Ordenes_Compra.SelectedIndex > (-1))
                {
                    GridViewRow SelectedRow;
                    DataTable Dt_Detalles = new DataTable();
                    DataTable Dt_Montos_OC = new DataTable();
                    Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();

                    Limpiar_Catalogo();
                    SelectedRow = Grid_Ordenes_Compra.Rows[Grid_Ordenes_Compra.SelectedIndex];
                    Txt_Folio.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[1].Text.Trim());
                    Txt_Fecha_Construccion.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[4].Text.Trim());
                    Txt_Requisicion.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[2].Text.Trim());
                    Txt_Estatus.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[6].Text.Trim());
                    Txt_Proveedor.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[3].Text.Trim());
                    Txt_Elaboro.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[10].Text.Trim());
                    Txt_Dependencia.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[11].Text.Trim());
                    Txt_Justificacion.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[12].Text.Trim());
                    Hdf_Listado_Almacen.Value = HttpUtility.HtmlDecode(SelectedRow.Cells[9].Text.Trim());

                    Negocio.P_No_Orden_Compra = HttpUtility.HtmlDecode(SelectedRow.Cells[7].Text.Trim());
                    Negocio.P_No_Requisicion = HttpUtility.HtmlDecode(SelectedRow.Cells[8].Text.Trim());

                    Dt_Detalles = Negocio.Consulta_Orden_Compra_Detalles();
                    Grid_Orden_Compra_Detalles.DataSource = Dt_Detalles;
                    Grid_Orden_Compra_Detalles.Columns[2].Visible = true;
                    Grid_Orden_Compra_Detalles.DataBind();
                    Grid_Orden_Compra_Detalles.Columns[2].Visible = false;
                    //Limpiamos el Grid_Facturas 
                    Session["Dt_Facturas"] = null;
                    Grid_Facturas.DataSource = new DataTable();
                    Grid_Facturas.DataBind();

                    Dt_Montos_OC = Negocio.Montos_Orden_Compra();

                    if (Dt_Montos_OC.Rows.Count > 0)
                    {
                        Lbl_SubTotal.Text = "" + Dt_Montos_OC.Rows[0]["SUBTOTAL"];
                        Lbl_IEPS.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL_IEPS"];
                        Lbl_IVA.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL_IVA"];
                        Lbl_Total.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL"];
                    }
                    
                    Configuracion_Formulario(false);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Reportes

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      16-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                if (Formato == "PDF")
                {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                else if (Formato == "Excel")
                {
                    String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN:          Carga el data set físico con el cual se genera el Reporte especificado
        ///PARAMETROS:           1.- Dt_Cabecera.- Contiene la informacion general de la orden de compra
        ///                      2.- Dt_Detalles.- Contiene los productos de la orden de compra
        ///                      3.- Ds_Recibo.- Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           19/Abril/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataTable Dt_Cabecera, DataTable Dt_Detalles, DataSet Ds_Reporte, String Formato, bool Tesorero)
        {

            DataRow Renglon;
            String Ruta_Reporte_Crystal = "";
            String Nombre_Reporte_Generar = "";

            DataTable Dt_Dir;
            Cls_Ope_Com_Listado_Ordenes_Compra_Negocio Listado = new Cls_Ope_Com_Listado_Ordenes_Compra_Negocio();
            Dt_Dir = Listado.Consulta_Directores();

            if (Dt_Dir.Rows.Count > 0)
            {
                Dt_Cabecera.Columns.Add("DIRECTOR_ADQUISICIONES", typeof(String));
                Dt_Cabecera.Columns.Add("OFICIALIA_MAYOR", typeof(String));
                Dt_Cabecera.Columns.Add("TESORERO", typeof(String));

                Dt_Cabecera.Rows[0]["DIRECTOR_ADQUISICIONES"] = ((Dt_Cabecera.Rows[0]["TIPO_REQUISICION"].ToString().Trim().Equals("PRODUCTO")) ? Dt_Dir.Rows[0]["DIRECTOR_ADQUISICIONES"].ToString().Trim() : Dt_Dir.Rows[0]["DIRECTOR_SERVICIOS_GENERALES"].ToString().Trim());
                Dt_Cabecera.Rows[0]["OFICIALIA_MAYOR"] = Dt_Dir.Rows[0]["OFICIALIA_MAYOR"];
                Dt_Cabecera.Rows[0]["TESORERO"] = Dt_Dir.Rows[0]["TESORERO"];

                Renglon = Dt_Cabecera.Rows[0];
                Ds_Reporte.Tables[0].ImportRow(Renglon);

                String Folio = Dt_Cabecera.Rows[0]["FOLIO"].ToString();

                for (int Cont_Elementos = 0; Cont_Elementos < Dt_Detalles.Rows.Count; Cont_Elementos++)
                {
                    Renglon = Dt_Detalles.Rows[Cont_Elementos]; //Instanciar renglon e importarlo
                    Ds_Reporte.Tables[1].ImportRow(Renglon);
                    Ds_Reporte.Tables[1].Rows[Cont_Elementos].SetField("FOLIO", Folio);
                }

                // Ruta donde se encuentra el reporte Crystal
                if (Tesorero)
                {
                    Ruta_Reporte_Crystal = ((Dt_Cabecera.Rows[0]["TIPO_REQUISICION"].ToString().Trim().Equals("PRODUCTO")) ? "../Rpt/Compras/Rpt_Ope_Com_Orden_Compra_Tes.rpt" : "../Rpt/Compras/Rpt_Ope_Com_Orden_Servicio.rpt");
                }
                else
                {
                    Ruta_Reporte_Crystal = ((Dt_Cabecera.Rows[0]["TIPO_REQUISICION"].ToString().Trim().Equals("PRODUCTO")) ? "../Rpt/Compras/Rpt_Ope_Com_Orden_Compra.rpt" : "../Rpt/Compras/Rpt_Ope_Com_Orden_Servicio.rpt");
                }

                // Se crea el nombre del reporte
                String Nombre_Reporte = "Rpt_List_OrdenC_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss"));

                // Se da el nombre del reporte que se va generar
                if (Formato == "PDF")
                    Nombre_Reporte_Generar = Nombre_Reporte + ".pdf";  // Es el nombre del reporte PDF que se va a generar
                else if (Formato == "Excel")
                    Nombre_Reporte_Generar = Nombre_Reporte + ".xls";  // Es el nombre del repote en Excel que se va a generar

                Cls_Reportes Reportes = new Cls_Reportes();
                Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
                Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
            }
            else
            {
                throw new Exception("Error al Intentar consultar los Directores que autorizarán la Orden de compra ");
            }
        }

    #endregion
        protected void Txt_Busqueda_Orden_Compra_TextChanged(object sender, EventArgs e)
        {
            Busqueda();
        }
        protected void Txt_Busqueda_Requisicion_TextChanged(object sender, EventArgs e)
        {
            Busqueda();
        }
        protected void Txt_Producto_TextChanged(object sender, EventArgs e)
        {
            Busqueda();
        }
        protected void Chk_Recepcion_Completa_CheckedChanged(object sender, EventArgs e)
        {
            
            //Realizamos los calculos automaticos de la Factura
            Calcular_Totales_Facturas();
        }
}
