﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Inventarios_De_Stock.Negocio;
using JAPAMI.Sessiones;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;

using Excel_Documentos = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Windows.Forms;
using JAPAMI.Reportes;
using System.Text;

public partial class paginas_Almacen_Frm_Ope_Alm_Inventario_Stock : System.Web.UI.Page
{
    private static String P_Dt_Inventario = "INVENTARIO";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        //Cls_Sessiones.Mostrar_Menu = true;
        if (!IsPostBack) 
        {
            Btn_Guardar.Visible = false;
            Btn_Salir.Visible = true;
            Cargar_Partidas();
            Cmb_Tipo.Items.Clear();
            Cmb_Tipo.Items.Add("EXISTENCIAS");
            Cmb_Tipo.Items.Add("COSTEADO");
        }
        Mostrar_Informacion("",false);
    }
    private DataTable Consultar_Inventario_Stock() 
    {
        Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio = new Cls_Ope_Alm_Inventarios_Stock_Negocio();
        Negocio.P_Clave_Producto = Txt_Clave.Text.Trim();
        Negocio.P_Clave_Anterior = Txt_Clave_Anterior.Text.Trim();
        Negocio.P_Nombre_Producto = Txt_Producto.Text.Trim();
        Negocio.P_Partida_ID = Cmb_Partida.SelectedValue.Trim();
        Negocio.P_Ordenar = Cmb_Ordenar.SelectedValue.Trim();
        if (Chk_Almacen_General.Checked == true)
        {
            Negocio.P_Almacen_General ="SI";
        }
        if (Chk_Almacen_Papeleria.Checked == true)
        {
            Negocio.P_Almacen_General = "NO";
        }
        if (Chk_Existencias_Cero.Checked == true)
        {
            Negocio.P_Existencias_Cero = true;
        }
        else
        {
            Negocio.P_Existencias_Cero = false;
        }
        if (Cmb_Partida.SelectedIndex == 0)
        {
            Negocio.P_Partida_ID = null;
        }
        DataTable Dt_Stock = Negocio.Consultar_Inventario_Stock();
        if (Dt_Stock != null && Dt_Stock.Rows.Count > 0)
        {
            Lbl_Registros.Visible = true;
            Lbl_Registros.Text = "Registros Encontrados [" + Dt_Stock.Rows.Count + "]";
            //Lbl_Total_Acumulado.Text = "Total Acumulado: $ " +
            //                    String.Format("{0:n}", Negocio.Consultar_Costeo_Inventario()).ToString();
            if (Cmb_Tipo.SelectedIndex == 1)
            {
                double COSTO_PROMEDIO_T = 0;
                double ULTIMO_COSTO_T = 0;
                foreach (DataRow Renglon in Dt_Stock.Rows)
                {
                    COSTO_PROMEDIO_T += double.Parse(Renglon["COSTO_PROMEDIO_T"].ToString());
                    ULTIMO_COSTO_T += double.Parse(Renglon["ULTIMO_COSTO"].ToString());
                }
                Lbl_costo_promedio_t.Text = "Costo Promedio: $ " +
                    String.Format("{0:n}", COSTO_PROMEDIO_T).ToString();

                Lbl_ultimo_costo_t.Text = "Ultimo Costo: $ " +
                    String.Format("{0:n}", ULTIMO_COSTO_T).ToString();
                if (Cmb_Tipo.SelectedValue == "COSTEADO")
                {
                    Lbl_costo_promedio_t.Visible = true;
                    Lbl_ultimo_costo_t.Visible = true;
                }
                else
                {
                    Lbl_costo_promedio_t.Visible = false;
                    Lbl_ultimo_costo_t.Visible = false;
                }
            }
        }
        else 
        {
            Lbl_Registros.Visible = false;
        }
        Session[P_Dt_Inventario] = Dt_Stock;
        Grid_Inventario.DataSource = Dt_Stock;
        Grid_Inventario.DataBind();
        return Dt_Stock;
    }
    private void Cargar_Partidas() 
    {
        Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio = new Cls_Ope_Alm_Inventarios_Stock_Negocio();
        DataTable Dt_Partidas = Negocio.Consultar_Partidas_Stock();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Partida, Dt_Partidas, 1, 0);
    }
    protected void Btn_Buscar_Click(object sender, EventArgs e)
    {
        Consultar_Inventario_Stock();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Producto_Click
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Producto_Click(object sender, ImageClickEventArgs e)
    {
        String Producto_ID = ((ImageButton)sender).CommandArgument;
        Hdf_Producto_ID.Value = Producto_ID;
        Btn_Salir.ToolTip = "Regresar";
        Btn_Imprimir.Visible = false;
        Div_Listado.Visible = false;
        Div_Contenido.Visible = true;
        Btn_Guardar.Visible = true;
        Btn_Salir.Visible = true;
        //Cargar datos en interfaz
        DataRow Renglon = ((DataTable)Session[P_Dt_Inventario]).Select("PRODUCTO_ID = '" + Producto_ID + "'")[0];
        Txt_Clave_Modificar.Text = Renglon["CLAVE"].ToString();
        Txt_Nombre_Modificar.Text = Renglon["NOMBRE"].ToString();
        Txt_Descripcion_Modificar.Text = Renglon["DESCRIPCION"].ToString();

        Txt_Existencia_Modificar.Text = Renglon["EXISTENCIA"].ToString();
        Txt_Disponible_Modificar.Text = Renglon["DISPONIBLE"].ToString();
        Txt_Comprometido_Modificar.Text = Renglon["COMPROMETIDO"].ToString();
        Txt_Minimo_Modificar.Text = Renglon["MINIMO"].ToString();
        Txt_Maximo_Modificar.Text = Renglon["MAXIMO"].ToString();
        Txt_Reorden_Modificar.Text = Renglon["REORDEN"].ToString();
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Inicio")
        {
            Session.Remove(P_Dt_Inventario);
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {

            Div_Listado.Visible = true;
            Div_Contenido.Visible = false;
            Btn_Salir.Visible = true;
            Btn_Salir.ToolTip = "Inicio";
            Btn_Guardar.Visible = false;
            Btn_Imprimir.Visible = true;
        }
    }
    protected void Btn_Guardar_Click(object sender, ImageClickEventArgs e)
    {
        if (Modificar_Producto())
        {
            Consultar_Inventario_Stock();
            ScriptManager.RegisterStartupScript(
                               this, this.GetType(),
                               "Requisiciones", "alert('Datos Actualizados');", true);
            Div_Contenido.Visible = false;
            Div_Listado.Visible = true;
            Btn_Salir.Visible = true;
            Btn_Salir.ToolTip = "Inicio";
            Btn_Guardar.Visible = false;
            Btn_Imprimir.Visible = true;
            //Btn_Salir.Visible = false;
        }
    }

    private bool Modificar_Producto() 
    {
        bool respuesta = false;
        try
        {
            double Minimo = Convert.ToDouble(Txt_Minimo_Modificar.Text.Trim());
            double Maximo = Convert.ToDouble(Txt_Maximo_Modificar.Text.Trim());
            double Pto_Reorden = Convert.ToDouble(Txt_Reorden_Modificar.Text.Trim());
            if (Minimo < Maximo && Pto_Reorden < Maximo && Pto_Reorden > Minimo)
            {
                Cls_Ope_Alm_Inventarios_Stock_Negocio Negocio = new Cls_Ope_Alm_Inventarios_Stock_Negocio();
                Negocio.P_Producto_ID = Hdf_Producto_ID.Value.Trim();
                Negocio.P_Minimo = Txt_Minimo_Modificar.Text.Trim();
                Negocio.P_Maximo = Txt_Maximo_Modificar.Text.Trim();
                Negocio.P_Reorden = Txt_Reorden_Modificar.Text.Trim();
                respuesta = Negocio.Modificar_Producto();
            }
            else
            {
                String Mensaje = "Verifique los datos <br>";
                Mensaje += "&nbsp;&nbsp; Minimo deberá ser menor a Máximo <br>";
                Mensaje += "&nbsp;&nbsp; Punto de Reorden deberá ser menor a Máximo <br>";
                Mensaje += "&nbsp;&nbsp; Punto de Reorden deberá ser mayor a Minimo";
                Mostrar_Informacion(Mensaje  , true);
            }
            
        }
        catch(Exception Ex)
        {
            respuesta = false;
        }
        return respuesta;
    }



    public void Generar_Reporte(DataTable data_set, DataSet ds_reporte, string nombre_reporte)
    {

        ReportDocument reporte = new ReportDocument();
        string filePath = Server.MapPath("../Rpt/Almacen/" + nombre_reporte);

        reporte.Load(filePath);
        DataRow renglon;

        for (int i = 0; i < data_set.Rows.Count; i++)
        {
            renglon = data_set.Rows[i];
            ds_reporte.Tables["INVENTARIO"].ImportRow(renglon);
        }
        reporte.SetDataSource(ds_reporte);

        //1
        ExportOptions exportOptions = new ExportOptions();
        //2
        DiskFileDestinationOptions diskFileDestinationOptions = new DiskFileDestinationOptions();
        //3
        //4
        diskFileDestinationOptions.DiskFileName = Server.MapPath("../../Reporte/Rpt_Inventario_Stock_General.pdf");
        //5
        exportOptions.ExportDestinationOptions = diskFileDestinationOptions;
        //6
        exportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
        //7
        exportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
        //8
        reporte.Export(exportOptions);
        //9
        string ruta = "../../Reporte/Rpt_Inventario_Stock_General.pdf";
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        if (Session[P_Dt_Inventario] != null)
        {
            DataSet Ds_Informacion = new DataSet("INVENTARIO");
            DataTable Dt_Productos = (DataTable)Session[P_Dt_Inventario];
            // Ds_Informacion.Tables.Add(Dt_Productos);
            Ds_Alm_Inventario_Stock_General Ds_Fisico = new Ds_Alm_Inventario_Stock_General();
            if (Cmb_Tipo.SelectedValue == "EXISTENCIAS")
            {
                Generar_Reporte(Dt_Productos, Ds_Fisico, "Rpt_Alm_Inventarios_Generales.rpt");
            }
            else
                if (Cmb_Tipo.SelectedValue == "COSTEADO") 
                {
                    Generar_Reporte(Dt_Productos, Ds_Fisico, "Rpt_Alm_Inventarios_Generales_Costeado.rpt");
                }
        }
        else
        {
            ScriptManager.RegisterStartupScript(
                               this, this.GetType(),
                               "Requisiciones", "alert('Debe realizar una búsqueda de productos');", true);            
        }
    }

    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        if (Session[P_Dt_Inventario] != null)
        {
            DataSet Ds_Informacion = new DataSet("INVENTARIO");
            DataTable Dt_Productos = (DataTable)Session[P_Dt_Inventario];
            // Ds_Informacion.Tables.Add(Dt_Productos);
            Ds_Alm_Inventario_Stock_General Ds_Fisico = new Ds_Alm_Inventario_Stock_General();
            if (Cmb_Tipo.SelectedValue == "EXISTENCIAS")
            {
                Llenar_DataSet_Reporte(Dt_Productos, Ds_Fisico, "Rpt_Alm_Inventarios_Generales.rpt", "Excel");
            }
            else
                if (Cmb_Tipo.SelectedValue == "COSTEADO")
                {
                    Llenar_DataSet_Reporte(Dt_Productos, Ds_Fisico, "Rpt_Alm_Inventarios_Generales_Costeado.rpt", "Excel");
                }
        }
        else
        {
            ScriptManager.RegisterStartupScript(
                               this, this.GetType(),
                               "Requisiciones", "alert('Debe realizar una búsqueda de productos');", true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_DataSet_Reporte
    ///DESCRIPCIÓN:          Metodo utilizado para llenar el Dataset e instanciar el método Exportar_Reporte
    ///PARAMETROS:           1.- DataTable Dt_Consulta, Esta tabla contiene los datos de la 
    ///                          consulta que se realizó a la base de datos
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           06/Mayo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llenar_DataSet_Reporte(DataTable Dt_Consulta, DataSet Ds_Reporte, String Rpt, String Formato)
    {
        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;
        String Usuario = Cls_Sessiones.Nombre_Empleado;

        int Cont_Elementos = 0;
        try
        {
            // Se llena la tabla Detalles del DataSet
            for (Cont_Elementos = 0; Cont_Elementos < Dt_Consulta.Rows.Count; Cont_Elementos++)
            {
                Renglon = Dt_Consulta.Rows[Cont_Elementos]; // Instanciar renglon e importarlo
                Ds_Reporte.Tables[0].ImportRow(Renglon);
            }

            // Ruta donde se encuentra el reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Almacen/" + Rpt;

            // Se crea el nombre del reporte
            String Nombre_Reporte = "Rpt_Contrarecibos_" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss"));

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = Nombre_Reporte + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = Nombre_Reporte + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            //Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
            Mostrar_Excel(Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar), "application/vnd.ms-excel");

        }
        catch (Exception ex)
        {
            if (ex.Message != "Error al mostrar el reporte en excel. Error: [Subproceso anulado.]")
                throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      17-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE: Mostrar_Excel
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en excel.
    ///              
    /// PARÁMETROS: No Aplica
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 10/Diciembre/2011.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
    {
        try
        {
            System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
            if (ArchivoExcel.Exists)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = Contenido;
                Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.WriteFile(ArchivoExcel.FullName);
                Response.End();
            }
        }
        catch (Exception Ex)
        {
            //// Response.End(); siempre genera una excepción (http://support.microsoft.com/kb/312629/EN-US/)
            throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
        }
    }

    protected void Button1_Click1(object sender, EventArgs e)
    {
        //Response.Clear(); 
        //Response.Buffer = true; 
        //Response.ContentType = "application/vnd.ms-excel"; 
        //Response.Charset = ""; 
        //this.EnableViewState = false;
        //System.IO.StringWriter oStringWriter = new System.IO.StringWriter(); 
        //System.Web.UI.HtmlTextWriter oHtmlTextWriter = new System.Web.UI.HtmlTextWriter(oStringWriter);
        ////Aqui va el Nombre de tu Datagrid en este ejemplo mi datagrid se llama 
        ////slolamente dg
        ////
        ////this.ChildControlsCreated ClearControls(dg);
        //Grid_Inventario.RenderControl(oHtmlTextWriter);
        ////dg.RenderControl(oHtmlTextWriter);
        //Response.Write(oStringWriter.ToString());
        //Response.End();

        Excel_Documentos.Application oXL;
        Excel_Documentos._Workbook oWB;
        Excel_Documentos._Worksheet oSheet;
        Excel_Documentos.Range oRng;
        try
        {
            //Start Excel and get Application object.
            oXL = new Excel_Documentos.Application();
            oXL.Visible = true;
            //Get a new workbook.
            oWB = (Excel_Documentos._Workbook)(oXL.Workbooks.Add(Missing.Value));
            oSheet = (Excel_Documentos._Worksheet)oWB.ActiveSheet;
            //Add table headers going cell by cell.
            oSheet.Cells[1, 1] = "First Name";
            oSheet.Cells[1, 2] = "Last Name";
            oSheet.Cells[1, 3] = "Full Name";
            oSheet.Cells[1, 4] = "Salary";
            //Format A1:D1 as bold, vertical alignment = center.
            oSheet.get_Range("A1", "D1").Font.Bold = true;
            oSheet.get_Range("A1", "D1").VerticalAlignment =
                Excel_Documentos.XlVAlign.xlVAlignCenter;


            //oRng = oSheet.get_Range(oSheet.Cells[1, 5], oSheet.Cells[1, 6]);
            oRng = oSheet.get_Range("E1","G1");
            oRng.Merge(true);

            // Create an array to multiple values at once.
            string[,] saNames = new string[5, 2];

            saNames[0, 0] = "John";
            saNames[0, 1] = "Smith";
            saNames[1, 0] = "Tom";
            saNames[1, 1] = "Brown";
            saNames[2, 0] = "Sue";
            saNames[2, 1] = "Thomas";
            saNames[3, 0] = "Jane";
            saNames[3, 1] = "Jones";
            saNames[4, 0] = "Adam";
            saNames[4, 1] = "Johnson";
            //Fill A2:B6 with an array of values (First and Last Names).
            oSheet.get_Range("A2", "B6").Value2 = saNames;
            //Fill C2:C6 with a relative formula (=A2 & " " & B2).
            oRng = oSheet.get_Range("C2", "C6");
            oRng.Formula = "=A2 & \" \" & B2";
            //Fill D2:D6 with a formula(=RAND()*100000) and apply format.
            oRng = oSheet.get_Range("D2", "D6");
            oRng.Formula = "=RAND()*100000";
            oRng.NumberFormat = "$0.00";
            //AutoFit columns A:D.
            oRng = oSheet.get_Range("A1", "D1");
            oRng.EntireColumn.AutoFit();
            //Manipulate a variable number of columns for Quarterly Sales Data.
            //DisplayQuarterlySales(oSheet);
            //Make sure Excel is visible and give the user control
            //of Microsoft Excel's lifetime.
            oXL.Visible = true;
            oXL.UserControl = true;
        }
        catch (Exception theException)
        {
            String errorMessage;
            errorMessage = "Error: ";
            errorMessage = String.Concat(errorMessage, theException.Message);
            errorMessage = String.Concat(errorMessage, " Line: ");
            errorMessage = String.Concat(errorMessage, theException.Source);
            //MessageBox.Show(errorMessage, "Error");
        }
    }
    //private void DisplayQuarterlySales(Excel._Worksheet oWS)
    //{
    //    Excel._Workbook oWB;
    //    Excel.Series oSeries;
    //    Excel.Range oResizeRange;
    //    Excel._Chart oChart;
    //    String sMsg;
    //    int iNumQtrs;
    //    //Determine how many quarters to display data for.
    //    for (iNumQtrs = 4; iNumQtrs >= 2; iNumQtrs--)
    //    {
    //        sMsg = "Enter sales data for ";
    //        sMsg = String.Concat(sMsg, iNumQtrs);
    //        sMsg = String.Concat(sMsg, " quarter(s)?");
    //        DialogResult iRet = MessageBox.Show(sMsg, "Quarterly Sales?",
    //            MessageBoxButtons.YesNo);
    //        if (iRet == DialogResult.Yes)
    //            break;
    //    }
    //    sMsg = "Displaying data for ";
    //    sMsg = String.Concat(sMsg, iNumQtrs);
    //    sMsg = String.Concat(sMsg, " quarter(s).");
    //    MessageBox.Show(sMsg, "Quarterly Sales");
    //    //Starting at E1, fill headers for the number of columns selected.
    //    oResizeRange = oWS.get_Range("E1", "E1").get_Resize(Missing.Value, iNumQtrs);
    //    oResizeRange.Formula = "=\"Q\" & COLUMN()-4 & CHAR(10) & \"Sales\"";
    //    //Change the Orientation and WrapText properties for the headers.
    //    oResizeRange.Orientation = 38;
    //    oResizeRange.WrapText = true;
    //    //Fill the interior color of the headers.
    //    oResizeRange.Interior.ColorIndex = 36;
    //    //Fill the columns with a formula and apply a number format.
    //    oResizeRange = oWS.get_Range("E2", "E6").get_Resize(Missing.Value, iNumQtrs);
    //    oResizeRange.Formula = "=RAND()*100";
    //    oResizeRange.NumberFormat = "$0.00";
    //    //Apply borders to the Sales data and headers.
    //    oResizeRange = oWS.get_Range("E1", "E6").get_Resize(Missing.Value, iNumQtrs);
    //    oResizeRange.Borders.Weight = Excel.XlBorderWeight.xlThin;
    //    //Add a Totals formula for the sales data and apply a border.
    //    oResizeRange = oWS.get_Range("E8", "E8").get_Resize(Missing.Value, iNumQtrs);
    //    oResizeRange.Formula = "=SUM(E2:E6)";
    //    oResizeRange.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).LineStyle
    //        = Excel.XlLineStyle.xlDouble;
    //    oResizeRange.Borders.get_Item(Excel.XlBordersIndex.xlEdgeBottom).Weight
    //        = Excel.XlBorderWeight.xlThick;
    //    //Add a Chart for the selected data.
    //    oWB = (Excel._Workbook)oWS.Parent;
    //    oChart = (Excel._Chart)oWB.Charts.Add(Missing.Value, Missing.Value,
    //        Missing.Value, Missing.Value);
    //    //Use the ChartWizard to create a new chart from the selected data.
    //    oResizeRange = oWS.get_Range("E2:E6", Missing.Value).get_Resize(
    //        Missing.Value, iNumQtrs);
    //    oChart.ChartWizard(oResizeRange, Excel.XlChartType.xl3DColumn, Missing.Value,
    //        Excel.XlRowCol.xlColumns, Missing.Value, Missing.Value, Missing.Value,
    //        Missing.Value, Missing.Value, Missing.Value, Missing.Value);
    //    oSeries = (Excel.Series)oChart.SeriesCollection(1);
    //    oSeries.XValues = oWS.get_Range("A2", "A6");
    //    for (int iRet = 1; iRet <= iNumQtrs; iRet++)
    //    {
    //        oSeries = (Excel.Series)oChart.SeriesCollection(iRet);
    //        String seriesName;
    //        seriesName = "=\"Q";
    //        seriesName = String.Concat(seriesName, iRet);
    //        seriesName = String.Concat(seriesName, "\"");
    //        oSeries.Name = seriesName;
    //    }

    //    oChart.Location(Excel.XlChartLocation.xlLocationAsObject, oWS.Name);
    //    //Move the chart so as not to cover your data.
    //    oResizeRange = (Excel.Range)oWS.Rows.get_Item(10, Missing.Value);
    //    oWS.Shapes.Item("Chart 1").Top = (float)(double)oResizeRange.Top;
    //    oResizeRange = (Excel.Range)oWS.Columns.get_Item(2, Missing.Value);
    //    oWS.Shapes.Item("Chart 1").Left = (float)(double)oResizeRange.Left;
    //}
    protected void Cmb_Tipo_SelectedIndexChanged(object sender, EventArgs e)
    {
        Grid_Inventario.DataSource = new DataTable();
        Grid_Inventario.DataBind();
        //limpiamos cajas de texto
        Txt_Clave_Anterior.Text = "";
        Txt_Clave.Text = "";
        Txt_Producto.Text = "";
        if (Cmb_Tipo.SelectedValue.Trim() == "EXISTENCIAS")
        {
            Lbl_ultimo_costo_t.Visible = false;
            Lbl_costo_promedio_t.Visible = false;
            Td_Promedio.Visible = false;
            Td_Promedio.Visible = false;
            Td_U_Costo.Visible = false;
            Td_Costo_Promedio.Visible = false;
            Td_Ultimo_Costo.Visible = false;
            
            Grid_Inventario.Columns[11].Visible = false;
            Grid_Inventario.Columns[12].Visible = false;
            Grid_Inventario.Columns[13].Visible = false;
            Grid_Inventario.Columns[14].Visible = false;
        }
        else
            if (Cmb_Tipo.SelectedValue.Trim() == "COSTEADO") 
            {
                Lbl_ultimo_costo_t.Visible = true;
                Lbl_costo_promedio_t.Visible = true;
                Td_Promedio.Visible = true;
                Td_Promedio.Visible = true;
                Td_U_Costo.Visible = true;
                Td_Costo_Promedio.Visible = true;
                Td_Ultimo_Costo.Visible = true;
                
                Grid_Inventario.Columns[11].Visible = true;
                Grid_Inventario.Columns[12].Visible = true;
                Grid_Inventario.Columns[13].Visible = true;
                Grid_Inventario.Columns[14].Visible = true;
                
            }        
    }
    ///*******************************************************************************
    //NOMBRE DE LA FUNCIÓN: Mostrar_Información
    //DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
    //RETORNA: 
    //CREO: Gustavo Angeles Cruz
    //FECHA_CREO: 24/Agosto/2010 
    //MODIFICO:
    //FECHA_MODIFICO:
    //CAUSA_MODIFICACIÓN:
    //********************************************************************************/

    private void Mostrar_Informacion(String txt, Boolean mostrar)
    {
        Lbl_Informacion.Style.Add("color", "#990000");
        Lbl_Informacion.Visible = mostrar;
        Img_Warning.Visible = mostrar;
        Lbl_Informacion.Text = txt;
    }
    protected void Chk_Almacen_General_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Almacen_General.Checked == true)
        {
            Chk_Almacen_Papeleria.Checked = false;
        }
    }
    protected void Chk_Almacen_Papeleria_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Almacen_Papeleria.Checked == true)
        {
            Chk_Almacen_General.Checked = false;
        }
    }
}
