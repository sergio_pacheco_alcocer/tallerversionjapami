<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"  CodeFile="Frm_Ope_Alm_Alerta_Punto_Reorden.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Alerta_Punto_Reorden" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
               <%-- <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
            </ProgressTemplate>
        </asp:UpdateProgress>
         <div id="Div_Contenido" style="width:97%;height:100%;">
            <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                <tr>
                    <td class="label_titulo">Alerta Producto en Punto de Reorden</td>
                </tr>
                <%--Fila de div de Mensaje de Error --%>
                <tr>
                    <td>
                        <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                        <table style="width:100%;">
                            <tr>
                                <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                    <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                Width="24px" Height="24px"/>
                                </td>            
                                <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                </td>
                            </tr> 
                        </table>                   
                        </div>
                    </td>
                </tr>
                 <%--Fila de Busqueda y Botones Generales --%>
                <tr class="barra_busqueda">
                    <td style="width:20%;" colspan="4">
                        <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" />
                        </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" >
                        <asp:CheckBox ID="Chk_Almacen_Papeleria" runat="server" 
                            Text="Productos de Almacen de Papeleria" AutoPostBack="true" 
                            oncheckedchanged="Chk_Almacen_Papeleria_CheckedChanged" />
                    </td>
                    <td colspan="2" align="right" >
                        <asp:CheckBox ID="Chk_Almacen_General" runat="server" 
                            Text="Productos de Almacen General" AutoPostBack="true" 
                            oncheckedchanged="Chk_Almacen_General_CheckedChanged" />
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                    <div ID="Div_Grid_Productos" runat="server" style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                     <asp:GridView ID="Grid_Productos" runat="server" AllowSorting="True" OnSorting="Grid_Productos_Sorting"
                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" OnRowDataBound="Grid_Productos_RowDataBound"
                    Width="99%"  
                    HeaderStyle-CssClass="tblHead" >
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                    <asp:BoundField DataField="CLAVE" HeaderText="Clave" Visible="true" SortExpression="CLAVE">
                    <FooterStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" Visible="true" SortExpression ="NOMBRE">
                    <FooterStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="UNIDAD" HeaderText="Unidad" Visible="true" SortExpression ="UNIDAD">
                    <FooterStyle HorizontalAlign="Left" />
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                     </asp:BoundField>
                    <asp:BoundField DataField="Existencia" HeaderText="Existencia" Visible="true" SortExpression = "EXISTENCIA" >
                    <FooterStyle HorizontalAlign="Right" />
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                    </asp:BoundField>
                    <asp:BoundField DataField="REORDEN" HeaderText="Pto. Reorden" Visible="true" SortExpression="REORDEN">
                    <FooterStyle HorizontalAlign="Right" />
                    <HeaderStyle HorizontalAlign="Right" />
                    <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                    </asp:BoundField>
                    <asp:TemplateField HeaderText="">
                    <ItemTemplate>
                    <asp:ImageButton ID="Btn_Alerta" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_yellow.png"/>
                    </ItemTemplate>
                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                    </asp:TemplateField>
                    </Columns>
                    <SelectedRowStyle CssClass="GridSelected" />
                    <PagerStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                    
                    </div>
                    </td>
                </tr>
                
            </table>
        </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>