﻿<%@ Page Title="Registro de Fechas Pago" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Fechas_Pago.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Alm_Fechas_Pago" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                   <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>            
                <%--<div id="Div_Cat_Com_Cotizadores_Botones" style="background-color:#ffffff; width:100%; height:100px;">--%>                
                        <table id="Tbl_Comandos" border="0" cellspacing="0" class="estilo_fuente" width="98%">                        
                            <tr>
                                <td colspan="2" class="label_titulo">
                                    Registro de Fechas Pago
                                </td>
                            </tr>
                            
                            <tr>
                                <div id="Div_Contenedor_error" runat="server">
                                <td colspan="2">
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                                    <br />
                                    <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                                </td>
                                </div>
                            </tr>
                            
                            <tr class="barra_busqueda">
                                <td class="style2">
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                        CssClass="Img_Button" onclick="Btn_Nuevo_Click"/>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                        CssClass="Img_Button" onclick="Btn_Modificar_Click"/>
                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" 
                                        CssClass="Img_Button" TabIndex="5"
                                        ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" 
                                        OnClientClick="return confirm('&iquest;Est&aacute; seguro de eliminar el elemento seleccionado?');" 
                                        onclick="Btn_Eliminar_Click"/>
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                        CssClass="Img_Button" onclick="Btn_Salir_Click"/>
                                </td>
                                <td align="right" style="width:50%">
                                    A&ntilde;o
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180" 
                                    ToolTip="Buscar" TabIndex="1" ></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                                             runat="server" FilterType="Custom, Numbers" 
                                                            TargetControlID="Txt_Busqueda" ValidChars="1234567890">
                                     </cc1:FilteredTextBoxExtender>
                                    <asp:ImageButton ID="Btn_Busqueda" runat="server" ToolTip="Consultar"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                        TabIndex="2" onclick="Btn_Busqueda_Click"/>
                                        <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Buscar>" TargetControlID="Txt_Busqueda"/>
                                </td>                         
                            </tr>                            
                        </table>
                    <%--</div>--%>
                <div id="Div_Datos_Cotizador" runat="server" style=" height:140px">                 
                    <div id="Div_Cat_Com_Cotizadores_Controles" runat="server" style="background-color:#ffffff; width:100%; height:100%;">        
                       <table id="Datos Generales_Inner" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                            
                            <tr>
                                <td>                                    
                                    Fecha L&iacute;mite Recepci&oacute;n</td>
                                <td class="style3">
                                    <asp:TextBox ID="Txt_Fecha_Limite_Recepcion" runat="server" Width="60%" 
                                        Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Limite_Recepcion_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Limite_Recepcion" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Limite_Recepcion_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Limite_Recepcion" PopupButtonID="Btn_Fecha_Limite_Recepcion" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Limite_Recepcion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Recepción" />
                                </td>
                                <td class="style4">
                                    
                                    Fecha Pago</td>
                                 <td>
                                    <asp:TextBox ID="Txt_Fecha_Pago" runat="server" Width="35%" Enabled="false"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Pago" PopupButtonID="Btn_Fecha_Pago" Format="dd/MMM/yyyy" />
                                    <asp:ImageButton ID="Btn_Fecha_Pago" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Pago" />
                                    
                                    &nbsp;&nbsp;<asp:ImageButton ID="Btn_Agregar" runat="server" 
                                    AlternateText="Agregar" ToolTip="Agregar Fecha" Visible="false"
                                    ImageUrl="~/paginas/imagenes/gridview/add_grid.png" style="display: none;"
                                    OnClick="Btn_Agregar_Click" Enabled="False" />
                                    &nbsp;
                                    </td>
                                 <td>
                                 </td>
                               
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    <asp:Label ID="Lbl_Comentarios" runat="server" Text="Comentarios"></asp:Label>
                                </td>
                                
                                 <td colspan="4">
                                     <asp:TextBox ID="Txt_Comentarios" runat="server" Width="80%" MaxLength="100" TextMode="MultiLine"></asp:TextBox>
                                     <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" 
                                                             runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Comentarios" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                     </cc1:FilteredTextBoxExtender>
                                     <asp:TextBox ID="Txt_No" runat="server" Width="0%" Visible="false"></asp:TextBox>                                     
                                </td>    
                            </tr>                           
                          </table>                            
                    </div>
                </div>
            <br>            
            <div ID="Div_Listado_Cotizadores" runat="server" 
                style="background-color:#ffffff; width:100%; height:100%;">
                <table id="Tbl_Grid_Cotizadores" border="0" cellspacing="0" 
                    class="estilo_fuente" style="width:98%;">
                    <tr>
                        <td align="center">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:GridView ID="Grid_Fechas_Pago" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" CssClass="GridView_1" 
                                GridLines="None" AllowSorting="true" 
                                onpageindexchanging="Grid_Fechas_Pago_PageIndexChanging" 
                                onselectedindexchanged="Grid_Fechas_Pago_SelectedIndexChanged" 
                                Style="white-space:normal" Width="96%" 
                                onsorting="Grid_Fechas_Pago_Sorting">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="5%" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="ANIO_NO_FECHA_PAGO" HeaderText="No">
                                        <HeaderStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                        <ItemStyle Font-Size="0px" ForeColor="Transparent" HorizontalAlign="Left" 
                                            Width="0%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_LIMITE_RECEPCION" SortExpression="FECHA_LIMITE_RECEPCION" 
                                        DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Fecha Limite Recepción">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_PAGO" DataFormatString="{0:dd/MMM/yyyy}" SortExpression="FECHA_PAGO" 
                                        HeaderText="Fecha Pago">
                                        <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                            <asp:GridView ID="Grid_Fechas" runat="server" AllowPaging="True" 
                                AutoGenerateColumns="False" CssClass="GridView_1" 
                                GridLines="None" 
                                onpageindexchanging="Grid_Fechas_PageIndexChanging" 
                                Style="white-space:normal" Width="96%">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Img_Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" CommandArgument='<%# Eval("RENGLON") %>' ToolTip="Quitar" />
                                        </ItemTemplate>
                                        <ItemStyle Width="5%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FECHA_LIMITE_RECEPCION" 
                                        DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Fecha Limite Recepción">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_PAGO" DataFormatString="{0:dd/MMM/yyyy}" 
                                        HeaderText="Fecha Pago">
                                        <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios" />
                                    <asp:BoundField DataField="RENGLON" HeaderText="Renglon" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>                    
            </ContentTemplate> 
                 
    </asp:UpdatePanel>
          

</asp:Content>