﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Almacen.Contrarecibos.Negocio;
using JAPAMI.Sessiones;

public partial class paginas_Almacen_Frm_Ope_Alm_Cancelar_Contrarecibos : System.Web.UI.Page
{
    #region (Variables Locales)
    private const String P_Dt_Contrarecibos = "Dt_Contrarecibos";
    private const String P_Dt_Facturas = "Dt_Facturas";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Page_Load) " + ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 09:47
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Mensaje_Error.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Estado_Inicial
    ///DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              16/Febrero/2013 14:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Estado_Inicial()
    {
        try
        {
            Mostrar_Error("", false);
            Grid_Contrarecibos.SelectedIndex = -1;
            Llena_Grid_Contrarecibos(0, 0, String.Empty, String.Empty, "01/01/1900", "01/01/1900", -1);
            Habilita_Controles("Inicial");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
    ///DESCRIPCION:             Eliminar las sesiones utilizadas en la pagina
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              15/Febrero/2013 16:59
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            HttpContext.Current.Session.Remove(P_Dt_Contrarecibos);
            HttpContext.Current.Session.Remove(P_Dt_Facturas);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Habilita_Controles
    ///DESCRIPCION:             Habilitar/Deshabilitar controles de acuerdo al modo
    ///PARAMETROS:              Modo: Cadena de texto que indica el modo
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              15/Febrero/2013 17:58
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Habilita_Controles(String Modo)
    {
        //Declaracion de variables
        Boolean Navegacion = false; //variable que indica si es modo de navegacion

        try
        {
            //Seleccionar el modo de operacion
            switch (Modo)
            {
                case "Inicial":
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Navegacion = true;
                    break;

                case "Modificar":
                    Btn_Modificar.ToolTip = "Guardar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Navegacion = false;
                    break;

                default:
                    break;
            }

            //Habilitar/Deshabilitar la navegacion
            Div_Busquedas_Avanzadas.Visible = Navegacion;
            Div_Contenido_Contrarecibo.Visible = Navegacion;
            Div_Contenido_Contrarecibo.Visible = !Navegacion;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Grid)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Grid_Contrarecibos
    ///DESCRIPCION:             Llenar el Grid de los contrarecibos de acuerdo a los criterios de busqueda
    ///PARAMETROS:              1. No_Contra_Recibo: Entero que contiene el numero de contrarecibo a buscar
    ///                         2. No_Orden_Compra
    ///                         3. Proveedor_ID: Cadena de texto que contiene el ID del proveedor
    ///                         4. Estatus: Cadena de texto que contiene el Estatus
    ///                         5. Fecha_Inicio: Fecha de inicio para un intervalo de tiempo de la busqueda
    ///                         6. Fecha_Fin: Fecha fin para un intervalo de tiempo de la busqueda
    ///                         7. Pagina: Entero que indica la pagina a mostrar
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 12:40
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Grid_Contrarecibos(Int64 No_Contra_Recibo, Int64 No_Orden_Compra, String Proveedor_ID, String Estatus, String Fecha_Inicio, String Fecha_Fin, int Pagina)
    {
        //Declaracion de variables
        DataTable Dt_contrarecibos = new DataTable(); //tabla para los contrarecibos
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //Variable apra la capa de negocios

        try
        {
            //Verificar si hay un numero de contrarecibo
            if (No_Contra_Recibo > 0 || No_Orden_Compra > 0)
            {
                //Asignar propiedades
                Contrarecibos_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
                Contrarecibos_Negocio.P_No_Orden_Compra = No_Orden_Compra.ToString();

                //Ejecutar consulta
                Dt_contrarecibos = Contrarecibos_Negocio.Consulta_Solo_Contrarecibos();
            }
            else
            {
                //Verificar si existe la variable de sesion
                if (HttpContext.Current.Session[P_Dt_Contrarecibos] == null)
                {
                    //Asignar propiedades
                    Contrarecibos_Negocio.P_Estatus = Estatus;
                    Contrarecibos_Negocio.P_Proveedor_ID = Proveedor_ID;
                    Contrarecibos_Negocio.P_Fecha_Inicio = Fecha_Inicio;
                    Contrarecibos_Negocio.P_Fecha_Fin = Fecha_Fin;

                    //Ejecutar consulta
                    Dt_contrarecibos = Contrarecibos_Negocio.Consulta_Solo_Contrarecibos();
                }
                else
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_contrarecibos = ((DataTable)HttpContext.Current.Session[P_Dt_Contrarecibos]);
                }
            }

            //llenar el Grid
            Grid_Contrarecibos.DataSource = Dt_contrarecibos;

            //Verificar si hay pagina
            if (Pagina > -1)
            {
                Grid_Contrarecibos.PageIndex = Pagina;
            }

            Grid_Contrarecibos.Columns[7].Visible = true;
            Grid_Contrarecibos.DataBind();
            Grid_Contrarecibos.Columns[7].Visible = false;

            //Colocar la tabla en una variable de sesion
            HttpContext.Current.Session[P_Dt_Contrarecibos] = Dt_contrarecibos;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Cancela_Contrarecibo(Int64 No_Contra_Recibo, String Fecha_Cancelacion, String Motivo_Cancelacion)
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable para la capa de negocios

        try
        {
            //Asignar propiedades
            Contrarecibos_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
            Contrarecibos_Negocio.P_Fecha_Cancelacion = Fecha_Cancelacion;
            Contrarecibos_Negocio.P_Motivo_Cancelacion = Motivo_Cancelacion;
            Contrarecibos_Negocio.Cancelar_Contrarecibo();

            //Mostrar mensaje de que se cancelo
            ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Contrarecibos", "alert('El contrarecibo " + No_Contra_Recibo.ToString().Trim() + " fue cancelado.');", true);

            //Colocar la pagina en el estado inicial
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private String Valida_Datos()
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        String Mensaje = String.Empty; //variable para el mensaje de la validacion
        Boolean Mostrar_Mensaje = false; //variable que indica si se tiene que mostrar el mensaje de la validacion

        try
        {
            //Asignar mensaje de requerimientos
            Mensaje = "Favor de proporcionar:<br />" +
                "<table border='0'>" +
                "<tr>" +
                "<td align='left'>+Fecha de Cancelaci&oacute;n</td>" +
                "<td align='left'>+Motivo de Cancelaci&oacute;n</td>" +
                "</tr>" +
                "</table>";

            //verificar si se tienen los datos requeridos
            if (String.IsNullOrEmpty(Txt_Fecha_Cancelacion.Text.Trim()) == true)
            {
                Mostrar_Mensaje = true;
            }

            if (String.IsNullOrEmpty(Txt_Motivo_Cancelacion.Text.Trim()) == true)
            {
                Mostrar_Mensaje = true;
            }

            //Verificar si se tiene que mostrar el mensaje de validacion
            if (Mostrar_Mensaje == true)
            {
                Resultado = Mensaje;
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Eventos)
    protected void Img_Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        Int64 No_Contra_Recibo = 0; //variable apra el numero del contrarecibo
        Int64 No_Orden_Compra = 0; //variable para el numero de la orden de compra
        String Fecha_Inicio = "01/01/1900"; //Variable para la fecha de inicio
        String Fecha_Fin = "01/01/1900"; //variable para la fecha fin
        String Estatus = String.Empty; //variable para el estatus
        String Proveedor_ID = String.Empty; //vasriable para el ID del proveedor
            
        try
        {
            //Verificar si hay datos en los controles de la busqueda
            if (String.IsNullOrEmpty(Txt_No_Orden_Compra_Busqueda.Text.Trim()) == false)
            {
                if (Cls_Util.EsNumerico(Txt_No_Orden_Compra_Busqueda.Text.Trim()) == true)
                {
                    No_Orden_Compra = Convert.ToInt64(Txt_No_Orden_Compra_Busqueda.Text.Trim());
                }
            }

            if (String.IsNullOrEmpty(Txt_No_Contra_Recibo_Busqueda.Text.Trim()) == false)
            {
                if (Cls_Util.EsNumerico(Txt_No_Contra_Recibo_Busqueda.Text.Trim()) == true)
                {
                    No_Contra_Recibo = Convert.ToInt64(Txt_No_Contra_Recibo_Busqueda.Text.Trim());
                }
            }

            if (String.IsNullOrEmpty(Txt_Fecha_Inicio.Text.Trim()) == false)
            {
                Fecha_Inicio = Txt_Fecha_Inicio.Text.Trim();

                if (String.IsNullOrEmpty(Txt_Fecha_Fin.Text.Trim()) == false)
                {
                    Fecha_Fin = Txt_Fecha_Fin.Text.Trim();
                }
            }

            if (Cmb_Proveedores.SelectedIndex > 0)
            {
                Proveedor_ID = Cmb_Proveedores.SelectedItem.Value;
            }

            if (Cmb_Estatus_Busqueda.SelectedIndex > 0)
            {
                Estatus = Cmb_Estatus_Busqueda.SelectedItem.Value;
            }

            //Llenar el grid de los contrarecibos
            Llena_Grid_Contrarecibos(No_Contra_Recibo, No_Orden_Compra, Proveedor_ID, Estatus, Fecha_Inicio, Fecha_Fin, -1);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Img_Btn_Buscar_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //Verificar el tooltip
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Inicial();
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion

        try
        {
            //Verificar el tooltip del boton
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                //Verificar si se ha seleccionado un contrarecibo del grid
                if (Grid_Contrarecibos.SelectedIndex > -1)
                {
                    //validar los datos
                    Validacion = Valida_Datos();

                    if (String.IsNullOrEmpty(Validacion) == true)
                    {
                        Cancela_Contrarecibo(Convert.ToInt64(Txt_No_Contrarecibo.Text.Trim()), Txt_Fecha_Cancelacion.Text.Trim(), Txt_Motivo_Cancelacion.Text.Trim());
                    }
                    else
                    {
                        Mostrar_Error(Validacion, true);
                    }
                }
                else
                {
                    Mostrar_Error("Seleccione el contrarecibo a cancelar.", true);
                }
            }
            else
            {

            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Modificar_Click) " + ex.Message, true);
        }
    }

    #endregion
}