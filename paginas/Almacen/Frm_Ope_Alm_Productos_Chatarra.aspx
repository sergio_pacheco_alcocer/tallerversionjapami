﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Productos_Chatarra.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Productos_Chatarra" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        /*
        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }*/

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                  <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">Productos Chatarra</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>          
                    <tr class="barra_busqueda">
                        <td colspan="4" style="width:20%;">
                        <!--Bloque de la busqueda-->
                            <asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" 
                                onclick="Btn_Consultar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click" />
                            <cc1:ModalPopupExtender ID="MPE_Comentarios_Chatarra" runat="server" BackgroundCssClass="popUpStyle" PopupControlID="Pnl_Comentarios_Chatarra" TargetControlID="Btn_Open" PopupDragHandleControlID="Pnl_Cabecera_Seleccionar_Archivo" CancelControlID="Btn_Cerrar" DropShadow="true" DynamicServicePath="" Enabled="true"></cc1:ModalPopupExtender>
                            <asp:Button ID="Btn_Open" runat="server" Text="" style="background-color: transparent; border-style:none; visibility:hidden" />
                            <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                        </td>                                 
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Tipo B&uacute;squeda</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Reporte" runat="server" Width="150px" 
                                AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Tipo_Reporte_SelectedIndexChanged">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Filtros" Value="Filtro"></asp:ListItem>
                                <asp:ListItem Text="Clave" Value="Clave"></asp:ListItem>
                                <asp:ListItem Text="Referencia JAPAMI" Value="Ref_JAPAMI"></asp:ListItem>
                                <asp:ListItem Text="Nombre" Value="Nombre"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left"><asp:Label ID="Lbl_Tipo_Busqueda" runat="server"></asp:Label></td>
                        <td align="right"><asp:TextBox ID="Txt_Busqueda" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">Unidad</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Unidades" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left">Resguardo</td>
                        <td align="right">
                            <asp:DropDownList ID="Cmb_Resguardo" runat="server" Width="150px">
                                <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                                <asp:ListItem Text="Si" Value="SI"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Partida Gen&eacute;rica</td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Partidas_Genericas" 
                                runat="server" Width="100%" AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Partidas_Genericas_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Partida Espec&iacute;fica</td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Partidas_Especificas" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Impuesto</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Impuestos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left">Estatus</td>
                        <td align="right">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Sin valor" Value="NULO"></asp:ListItem>
                                <asp:ListItem Text="Activo" Value="ACTIVO"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="INACTIVO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Tipo</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Tipos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left">Stock</td>
                        <td align="right">
                            <asp:DropDownList ID="Cmb_Stock" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Sin valor" Value="NULO"></asp:ListItem>
                                <asp:ListItem Text="Si" Value="SI"></asp:ListItem>
                                <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="Grid_Productos" runat="server" style="white-space:normal;" 
                                AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" 
                                GridLines="None" PageSize="5" Width="100%" AllowPaging="true">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Chatarra" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Btn_Chatarra" runat="server" OnClick="Btn_Chatarra_Click" CommandArgument='<%# (Convert.ToString(Eval("Producto_ID")) + "_" + Convert.ToString(Eval("Disponible")) + "_" + Convert.ToString(Eval("Resguardo")) + "_" + Convert.ToString(Eval("Nombre_Completo")))  %>' 
                                                ImageUrl="../imagenes/gridview/grid_garbage.png" ToolTip="Ponerlo como chatarra" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Ref_JAPAMI" HeaderText="Ref. JAPAMI" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Nombre_Completo" HeaderText="Producto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Resguardo" HeaderText="Resguardo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Resguardante" HeaderText="Resguardante" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Stock" HeaderText="Stock" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Partida" HeaderText="Partida" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                    <asp:BoundField DataField="Existencia" HeaderText="Exist" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Comprometido" HeaderText="Comp" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Disponible" HeaderText="Disp" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Producto_ID" HeaderText="Producto_ID" Visible="false" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

    <!--Panel para los comentarios del producto Chatarra-->
    <asp:Panel ID="Pnl_Comentarios_Chatarra" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
        <asp:UpdatePanel ID="Upd_Pnl_Comentarios_Chatarra" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:UpdateProgress ID="Progress_Upnl_Archivos_Requisicion" runat="server" AssociatedUpdatePanelID="Upd_Pnl_Comentarios_Chatarra" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>                                        
                    </ProgressTemplate>
                </asp:UpdateProgress>

                <div style="color: #5D7B9D">
                    <table width="100%">
                        <tr>
                            <td colspan="2" style="color:Black;font-size:12;font-weight:bold;"><asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />Proporcione la cantidad y los comentarios</td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:TextBox ID="Txt_Producto" runat="server" Width="100%" Enabled="false"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td colspan="2"><asp:HiddenField ID="Txt_Cadena_Control" runat="server" /></td>
                        </tr>
                        <tr>
                            <td align="left">Cantidad</td>
                            <td align="left">
                                <asp:TextBox ID="Txt_Cantidad" runat="server" Width="100px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Fte_Cantidad" runat="server" TargetControlID="Txt_Cantidad" FilterType="Custom, Numbers" ValidChars="0123456789"></cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Comentarios</td>
                            <td align="left">
                                <asp:TextBox ID="Txt_Comentarios" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="Txt_Comentarios"
                                    watermarktext="&lt;L&iacute;mite de Caracteres 500&gt;" watermarkcssclass="watermarked">
                                    </cc1:textboxwatermarkextender>
                                <cc1:filteredtextboxextender id="FTE_Justificacion" runat="server" targetcontrolid="Txt_Comentarios"
                                    filtertype="Custom, UppercaseLetters, LowercaseLetters, Numbers" validchars="ÑñáéíóúÁÉÍÓÚ./*-!$%&()=,[]{}+<>@?¡?¿# ">
                                    </cc1:filteredtextboxextender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left"><asp:Button ID="Btn_Aceptar" runat="server" Text="Aceptar" 
                                    CssClass="button" CausesValidation="false" Width="200px" 
                                    onclick="Btn_Aceptar_Click" /></td>
                            <td align="right"><asp:Button ID="Btn_Cancelar" runat="server" Text="Cancelar" 
                                    CssClass="button" CausesValidation="false" Width="200px" 
                                    onclick="Btn_Cancelar_Click" /></td>
                        </tr>
                    </table>
                </div>
                
            </ContentTemplate>
        </asp:UpdatePanel>
    </asp:Panel>
</asp:Content>

