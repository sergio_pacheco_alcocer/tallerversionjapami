﻿<%@ Page Title="Catálogo Cotizadores" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Alm_Parametros.aspx.cs" Inherits="paginas_Almacen_Frm_Cat_Alm_Parametros" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 644px;
        }
        .style2
        {
            width: 24%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" />
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        
                <div id="Div_Cat_Com_Cotizadores_Botones" style="background-color:#ffffff; width:100%; height:100%;">                
                        <table id="Tbl_Comandos" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                        
                            <tr>
                                <td colspan="2" class="label_titulo">
                                    Parametros Almacen
                                </td>
                            </tr>
                            
                            <tr>
                                <div id="Div_Contenedor_error" runat="server">
                                <td colspan="2">
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                                    <br />
                                    <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                                </td>
                                </div>
                            </tr>
                            
                            <tr class="barra_busqueda">
                                <td style="width:50%">
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    CssClass="Img_Button" onclick="Btn_Nuevo_Click"/>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" onclick="Btn_Modificar_Click"/>
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    CssClass="Img_Button" onclick="Btn_Salir_Click"/>
                                </td>
                                <td align="right" style="width:50%">                                    
                                </td>                        
                            </tr>
                            
                        </table>
                    </div>

            <div id="Div_Datos_Cotizador" runat="server">
                 
                <div id="Div_Cat_Com_Cotizadores_Controles" runat="server" style="background-color:#ffffff; width:100%; height:100%;">        
                       <table id="Datos Generales_Inner" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                            
                            <tr>
                                <td class="style2">
                                    
                                    &nbsp; Dependencia Almacen </td>
                                <td>
                                    <asp:Panel ID="Panel1" runat="server" DefaultButton="Btn_Dependencia">
                                        <asp:TextBox ID="Txt_Dependencia" runat="server" MaxLength="10" 
                                            style="width:95%;" />
                                        <cc1:FilteredTextBoxExtender ID="Txt_Dependencia_FilteredTextBoxExtender" 
                                            runat="server" TargetControlID="Txt_Dependencia" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ" />
                                            <asp:Button ID="Btn_Dependencia" runat="server" style="display:none;" onclick="Btn_Dependencia_Click" />
                                   </asp:Panel>
                               </td>
                               <td>
                                   <asp:DropDownList ID="Cmb_Dependencia" runat="server" Width="98%" AutoPostBack="true" onselectedindexchanged="Cmb_Dependencia_SelectedIndexChanged">
                                    </asp:DropDownList>                                
                               </td>
                            </tr>
                            <tr>
                                <td class="style2">
                                    
                                    &nbsp; Programa Almacen</td>
                                 <td>
                                    <asp:Panel ID="Panel2" runat="server" DefaultButton="Btn_Programa">
                                        <asp:TextBox ID="Txt_Programa" runat="server" MaxLength="10" 
                                            style="width:95%;" />
                                        <cc1:FilteredTextBoxExtender ID="Txt_Programa_FilteredTextBoxExtender" 
                                            runat="server" TargetControlID="Txt_Programa" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="áéíóúÁÉÍÓÚñÑ" />
                                            <asp:Button ID="Btn_Programa" runat="server" style="display:none;" onclick="Btn_Programa_Click" />
                                   </asp:Panel>       
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="Cmb_Programa" runat="server" Width="98%" AutoPostBack="true" onselectedindexchanged="Cmb_Programa_SelectedIndexChanged">
                                    </asp:DropDownList>
                                 </td>                               
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>                                
                                 <td style="width:18%">
                                     &nbsp;</td>
                                 <td> 
                                    
                                 </td>    
                            </tr>
                           
                            </table>
                            
                    </div>
            </div>
            <div id="Div_Listado_Cotizadores" runat="server" style="background-color:#ffffff; width:100%; height:100%;">
                    <table id="Tbl_Grid_Cotizadores" border="0" cellspacing="0" class="estilo_fuente" style="width:98%;">                        
                            <tr>
                                <td align="center" class="style1">
                                &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <asp:GridView ID="Grid_Parametros" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" 
                                        EmptyDataText="&quot;No se encontraron registros&quot;" GridLines="None" 
                                        onpageindexchanging="Grid_Parametros_PageIndexChanging" 
                                        onselectedindexchanged="Grid_Parametros_SelectedIndexChanged" PageSize="5" 
                                        Style="white-space:normal" Width="70%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="ID">
                                                <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                <ItemStyle HorizontalAlign="Left" ForeColor="Transparent" Font-Size="0px" Width="0%"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>                                
                            </tr>                        
                    </table>
            </div>
         </ContentTemplate>                  
    </asp:UpdatePanel>
</asp:Content>