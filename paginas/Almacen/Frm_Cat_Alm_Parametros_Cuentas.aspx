﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Alm_Parametros_Cuentas.aspx.cs" Inherits="paginas_Almacen_Frm_Cat_Alm_Parametros_Cuentas" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
<!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

<script type="text/javascript" language="javascript">
    function Limpiar_Controles_Busqueda() {
        document.getElementById("<%=Txt_Busqueda_Cuenta.ClientID%>").value = "";
        return false;
    }
    function Cerrar_Busqueda_Cuenta() {
        $find('Busqueda_Cuenta').Hide();
        return false;
    }
</script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" AsyncPostBackTimeout="360000"
        EnableScriptLocalization="true" />
        
    <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td class="label_titulo">
                        Catalogo Parámetros Cuentas Almacen
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <div id="Div_Contenedor_Msj_Error" runat="server">
                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr class="barra_busqueda" align="right">
                    <td align="left" valign="middle">
                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar"
                            OnClick="Btn_Modificar_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div id="Div_Datos_Generales" runat="server" style="width: 100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="100%">
                    <tr>
                        <td colspan="2" align="center">
                            Datos Generales
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="2" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Cuenta Contable Almacen General
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_Cuenta_General" runat="server" Width="95%">
                            </asp:DropDownList>
                            <asp:ImageButton ID="Btn_Buscar_General" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Consultar" OnClick="Btn_Buscar_General_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Cuenta Contable Almacen Papeleria
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_Cuenta_Papeleria" runat="server" Width="95%">
                            </asp:DropDownList>
                            <asp:ImageButton ID="Btn_Buscar_Papeleria" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Consultar" OnClick="Btn_Buscar_Papeleria_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Cuenta IVA Pendiente de Acreditar
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_IVA_Pendiente" runat="server" Width="95%">
                            </asp:DropDownList>
                            <asp:ImageButton ID="Btn_Buscar_IVA_Pendiente" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Consultar" onclick="Btn_Buscar_IVA_Pendiente_Click"/>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Cuenta IVA Acreditable
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_IVA_Acreditable" runat="server" Width="95%">
                            </asp:DropDownList>
                            <asp:ImageButton ID="Btn_Buscar_IVA_Acreditable" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Consultar" onclick="Btn_Buscar_IVA_Acreditable_Click" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:UpdatePanel ID="Aux_Mpe_Busqueda_Cuenta" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Open_Busqueda_Cuenta" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Busqueda_Cuenta" runat="server" BehaviorID="Busqueda_Cuenta" 
                TargetControlID="Btn_Open_Busqueda_Cuenta" PopupControlID="Pnl_Busqueda_Cuenta" 
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    
    <asp:Panel ID="Pnl_Busqueda_Cuenta" runat="server" CssClass="drag" HorizontalAlign="Center" Width="850px" 
        Style="display: none; border-style: outset; border-color: Silver; background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        
        <asp:Panel ID="Pnl_Busqueda_Cuenta_Cabecera" runat="server" Style="cursor: move; background-color: Silver;
            color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" Style="cursor: pointer;"
                            ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png"
                            OnClientClick="javascript:return Cerrar_Busqueda_Cuenta();" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuenta" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuenta" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuenta"
                                    DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                    <tr>
                                        <td colspan="4">
                                            <table style="width: 80%;">
                                                <tr>
                                                    <td align="left">
                                                        <asp:ImageButton ID="Img_Error_Busqueda_Cuenta" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                            Width="24px" Height="24px" Visible="false" />
                                                        <asp:Label ID="Lbl_Error_Busqueda_Cuenta" runat="server" Text="" CssClass="estilo_fuente_mensaje_error"
                                                            Visible="false" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr" runat="server" OnClientClick="javascript:return Limpiar_Controles_Busqueda();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de B&uacute;squeda" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 20%; text-align: left; font-size: 11px;">
                                            Cuenta
                                        </td>
                                        <td colspan="3" style="text-align: left; font-size: 11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta" runat="server" Width="98%" TabIndex="11" MaxLength="100" />
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Cuenta"
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td colspan="2" style="width: 50%; text-align: left; font-size: 11px;">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <hr />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%; text-align: left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server" Text="Busqueda de Cuenta"
                                                    CssClass="button" CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuenta_Contable_Click" />
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 100%" colspan="4">
                                            <div id="Div_Busqueda" runat="server" style="overflow: auto; max-height: 200px; width: 99%">
                                                <table style="width: 100%">
                                                    <tr>
                                                        <td>
                                                            <asp:GridView ID="Grid_Cuentas" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                                                                GridLines="None" Width="99.9%" AllowSorting="True" HeaderStyle-CssClass="tblHead" EmptyDataText="No se encontraron Registros" >
                                                                <Columns>
                                                                    <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Seleccionar_Cuenta" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                CommandArgument='<%# Eval("Cuenta_Contable_ID") %>' OnClick="Btn_Seleccionar_Cuenta_Click" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_ID">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUENTA" HeaderText="No. Cuenta" SortExpression="CUENTA">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="Descripcion">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    
</asp:Content>
