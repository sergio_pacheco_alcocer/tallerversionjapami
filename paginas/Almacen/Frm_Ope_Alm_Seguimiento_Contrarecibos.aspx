﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Seguimiento_Contrarecibos.aspx.cs" Inherits="paginas_Compras_Frm_Alm_Com_Seguimiento_Contrarecibos" 
Title="Seguimiento a Requisiciones" Culture="es-MX"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 25%;
        }
        .style2
        {
            width: 121px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>                                    
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                
                
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo">
                                Seguimiento a Contra Recibos
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Listar_Contrarecibos" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Listar Requisiciones" OnClick="Btn_Listar_Contrarecibos_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td colspan="2">
                                <asp:HiddenField ID="Hdf_Tipo_Contrarecibo" runat="server" />
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                No. Contrarecibo
                            </td>
                            <td style="width: 43%;">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="13" Width="15%" AutoPostBack="true"
                                    ontextchanged="Txt_Busqueda_TextChanged"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
                                    TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked" 
                                    WatermarkText="&lt;No&gt;">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" 
                                    FilterType="Custom" TargetControlID="Txt_Busqueda" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 12%;" align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>                    
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>
                        <tr>
                        
                            <td style="width: 15%;">
                                &nbsp;</td>
                            <td style="width: 43%;">
                                &nbsp;</td>
                            <td align="right" visible="false" colspan="2">
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" 
                                    ToolTip="Consultar" />
                            </td>
                            <tr>
                                <td align="center" colspan="4" style="width: 99%">
                                    <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;">
                                        <asp:GridView ID="Grid_Contra_Recibos" runat="server" AllowSorting="True" 
                                            AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="NO_CONTRA_RECIBO,TIPO" 
                                            EmptyDataText="No se encontraron contrarecibos" GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" Width="100%" 
                                            onpageindexchanging="Grid_Contra_Recibos_PageIndexChanging"
                                            onselectedindexchanged="Grid_Contrarecibos_SelectedIndexChanged">
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                            <asp:ButtonField HeaderText="" ButtonType="Image" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" ItemStyle-HorizontalAlign="Left" HeaderStyle-Width="2%" ItemStyle-Width="2%" CommandName="Select" ItemStyle-Font-Size="X-Small" HeaderStyle-Font-Size="X-Small"/>
                                                <asp:BoundField DataField="NO_CONTRA_RECIBO" HeaderText="N.C." 
                                                    Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="OC" 
                                                    HeaderText="OC" Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" Wrap="true" Width="10%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE_PROVEEDOR" HeaderText="Proveedor" Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FECHA_RECEPCION" HeaderText="Fecha Recepción" 
                                                    Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <FooterStyle HorizontalAlign="Left" Width="10%"/>
                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FECHA_PAGO" HeaderText="Fecha Pago" 
                                                    DataFormatString="{0:dd/MMM/yyyy}">
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:N2}">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="TIPO" HeaderText="Tipo" Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;" align="left">
                                No. Contra Recibo</td>
                            <td align="left" class="style1">
                                <asp:TextBox ID="Txt_No_Contra_Recibo" runat="server" Width="46%"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Fecha Recepción
                            </td>
                            <td align="left">
                                <asp:TextBox ID="Txt_Fecha_Recepcion" runat="server" Width="46%"></asp:TextBox>
                            </td>
                            <td class="style2">
                                Fecha Pago
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Fecha_Pago" runat="server" Width="46%"></asp:TextBox>
                            </td>
                        </tr>                        
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Facturas" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="100%"                                     
                                    style="white-space:normal" 
                                    onpageindexchanging="Grid_Facturas_PageIndexChanging" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="NO_FACTURA_PROVEEDOR" HeaderText="No. Factura" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_FACTURA" HeaderText="Fecha" Visible="True" 
                                            DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="IMPORTE" HeaderText="Importe" Visible="True" 
                                            DataFormatString="{0:N2}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="height:20px;">
                            <td ></td>                        
                        </tr>
                        <tr>                            
                            <td align="center" colspan="4">                        
                                <asp:GridView ID="Grid_Seguimiento" runat="server" AutoGenerateColumns="False"
                                    GridLines="None" PageSize="3" Width="100%" 
                                    Style="font-size: xx-small; white-space: normal" 
                                    onpageindexchanging="Grid_Seguimiento_PageIndexChanging" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>

                                        <asp:BoundField DataField="EMPLEADO" HeaderText="Empleado" >
                                            <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                            <ItemStyle HorizontalAlign="Left" Width="40%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS_ENTRADA" HeaderText="Estatus Inicio" 
                                            Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="11%" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS_SALIDA" HeaderText="Estatus Fin" 
                                            Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Width="11%" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="FECHA_ENTRADA" HeaderText="Fecha Entrada" 
                                            Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Left" Width="24%" />
                                            <ItemStyle HorizontalAlign="Left" Width="24%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA_SALIDA" HeaderText="Fecha Salida" 
                                            Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Left" Width="68%" />
                                            <ItemStyle HorizontalAlign="Left" Width="68%"  />
                                        </asp:BoundField>                                        
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>    
</asp:Content>

