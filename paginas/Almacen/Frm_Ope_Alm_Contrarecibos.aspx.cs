﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Almacen.Contrarecibos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Parametros_Contabilidad.Negocio;

public partial class paginas_Almacen_Frm_Ope_Alm_Contrarecibos : System.Web.UI.Page
{
    #region (Variables Locales)
    private const String P_Dt_Contrarecibos = "Dt_Contrarecibos"; //tabla para el grid de los contrarecibos
    private const String P_Dt_Facturas = "Dt_Facturas"; //tabla para el grid de las facturas
    private const String P_Dt_Facturas_Eliminadas = "Dt_Facturas_Eliminadas"; //tabla para el grid de las facturas eliminadas
    private const String P_Archivo_XML = "Archivo_XML"; //Archivo adjunto para la factura
    private const String P_Comentarios = "Comentarios";
    private const String P_Factura = "Factura";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy");
                Txt_Fecha_Fin.Text = DateTime.Now.ToString("dd/MMM/yyyy");
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error(ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Mostrar_Error
    ///DESCRIPCION:             Mostrar el mensaje de error
    ///PARAMETROS:              1. Mensaje: Cadena de texto con el mensaje a mostrar
    ///                         2. Mostrar: Booleano que indica si se va a mostrar el mensaje de error
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 09:47
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Mostrar_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Informacion.Text = Mensaje;
            Div_Contenedor_Msj_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Estado_Inicial
    ///DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 10:06
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Estado_Inicial()
    {
        //Declaracion de variables
        string Fecha_Inicial = string.Empty; //variable para la fecha inicial

        try
        {
            //Obtener la fecha inicial
            Fecha_Inicial = "01/";

            if (DateTime.Now.Month.ToString().Trim().Length == 1)
            {
                Fecha_Inicial += "0";
            }

            Fecha_Inicial += DateTime.Now.Month.ToString().Trim() + "/" + DateTime.Now.Year.ToString().Trim();

            Elimina_Sesiones();
            Grid_Contrarecibos.SelectedIndex = -1;
            Llena_Grid_Contrarecibos(-1, "",String.Empty, "RECHAZADO_COMPRAS','RECHAZADO_CONTABILIDAD", true, Fecha_Inicial, string.Format("{0:dd/MM/yyyy}", DateTime.Now), -1);
            Mostrar_Error("", false);

            //Mostrar los div de navegacion
            Habilitar_Controles("Inicial");
        }
        catch (Exception ex)
        {
            Mostrar_Error(ex.Message, true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
    ///DESCRIPCION:             Eliminar las sesiones utilizadas en la pagina
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 10:33
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Elimina_Sesiones()
    {
        try
        {
            //Eliminar las sesiones
            HttpContext.Current.Session.Remove(P_Dt_Contrarecibos);
            HttpContext.Current.Session.Remove(P_Dt_Facturas);
            HttpContext.Current.Session.Remove(P_Comentarios);
            HttpContext.Current.Session.Remove(P_Archivo_XML);
            HttpContext.Current.Session.Remove(P_Factura);
            HttpContext.Current.Session.Remove(P_Dt_Facturas_Eliminadas);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Habilitar_Controles
    ///DESCRIPCION:             Habilitar los controles de acuerdo al modo de operacion
    ///PARAMETROS:              Modo: Cadena de texto que indica el modo de operacion
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 14:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Habilitar_Controles(String Modo)
    {
        //Declaracion de variables
        Boolean Navegacion = false; //variable que indica si es el modo de navegacion
        Boolean Habilitar_Controles = false; //variable que indica si se tienen que habilitar los controles de los detalles
        try
        {
            //Seleccionar el modo de operacion
            switch (Modo)
            {
                case "Inicial":
                    Btn_Modificar.Visible = false;
                    Btn_Imprimir_Contrarecibo.Visible = false;
                    Btn_Imprimir_Solicitud_Pago.Visible = false;
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Navegacion = true;
                    Habilitar_Controles = false;
                    Grid_Facturas.Columns[0].Visible = false;
                    break;

                case "Seleccionar":
                    Btn_Modificar.Visible = true;
                    Btn_Imprimir_Contrarecibo.Visible = true;
                    Btn_Imprimir_Solicitud_Pago.Visible = true;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.ToolTip = "Modificar";
                    Navegacion = false;
                    Habilitar_Controles = false;
                    Grid_Facturas.Columns[0].Visible = false;
                    break;

                case "Modificar":
                    Btn_Modificar.Visible = true;
                    Btn_Imprimir_Contrarecibo.Visible = false;
                    Btn_Imprimir_Solicitud_Pago.Visible = false;
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.ToolTip = "Guardar";
                    Navegacion = false;
                    Habilitar_Controles = true;
                    Grid_Facturas.Columns[0].Visible = true;
                    Cmb_Estatus.SelectedIndex = 1;
                    break;

                default:
                    break;
            }

            //Mostrar Div
            Div_Busquedas_Avanzadas.Visible = Navegacion;
            Div_Detalles.Visible = !Navegacion;

            //Habilitar controles
            Cmb_Estatus.Enabled = Habilitar_Controles;
            Img_Btn_Agregar.Enabled = Habilitar_Controles;
            Img_Btn_Fecha_Factura.Enabled = Habilitar_Controles;
            Img_Btn_Cargar.Enabled = Habilitar_Controles;
            Btn_Fecha_Recepcion.Enabled = Habilitar_Controles;
            Txt_No_Factura.Enabled = Habilitar_Controles;
            Txt_Monto_Factura.Enabled = Habilitar_Controles;
            Txt_IVA_Factura.Enabled = Habilitar_Controles;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Datos_Controles
    ///DESCRIPCION:             Llenar los datos de los controles al seleccionar un contrarecibo
    ///PARAMETROS:              1. No_Contra_Recibo: Numero de contrarecibo seleccionado
    ///                         2. No_Orden_Compra: Numero de la orden de compra
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              30/Enero/2013 18:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Datos_Controles(Int64 No_Contra_Recibo, String No_Orden_Compra)
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable apra la capa de negocios
        DataSet Ds_Datos_Contrarecibo = new DataSet(); //Dataset con el resultado de la consulta

        try
        {
            //Colocar el numero de contrarecibo a la caja de texto escondida
            Txt_No_Contrarecibo_Escondido.Value = No_Contra_Recibo.ToString().Trim();

            //Asignar propiedades
            Contrarecibos_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
            Contrarecibos_Negocio.P_No_Orden_Compra = No_Orden_Compra;
            Contrarecibos_Negocio.P_Tipo_OC = Hdf_Tipo_Orden_Compra.Value;
            //Ejecutar consulta
            Ds_Datos_Contrarecibo = Contrarecibos_Negocio.Consulta_Datos_Contrarecibo();
            //Limpia Grid
            Grid_Facturas.DataBind();

            //Verificar si se se tiene un contrarecibo
            if (No_Contra_Recibo == 0)
            {
                No_Contra_Recibo = Contrarecibos_Negocio.Proximo_No_Contrarecibo();
                Btn_Imprimir_Contrarecibo.Visible = false;
                Btn_Imprimir_Solicitud_Pago.Visible = false;
            }
            else
            {
                Btn_Imprimir_Contrarecibo.Visible = true;
                Btn_Imprimir_Solicitud_Pago.Visible = true;
            }

            //Verificar si hay datos
            if (Ds_Datos_Contrarecibo.Tables.Count == 2)
            {
                //Verificar si la cabecera tiene datos
                if (Ds_Datos_Contrarecibo.Tables[0].Rows.Count > 0)
                {
                    //verificar si se tiene una orden de compra
                    if (No_Orden_Compra == "0")
                    {
                        No_Orden_Compra = Ds_Datos_Contrarecibo.Tables[0].Rows[0]["NO_ORDEN_COMPRA"].ToString();
                    }

                    //Verificar si existe una solicitud de pago
                    if (Ds_Datos_Contrarecibo.Tables[0].Rows[0]["NO_SOLICITUD_PAGO"] != null)
                    {
                        if (Ds_Datos_Contrarecibo.Tables[0].Rows[0]["NO_SOLICITUD_PAGO"] != DBNull.Value)
                        {
                            Hdf_No_Solicitud_Pago.Value = Ds_Datos_Contrarecibo.Tables[0].Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                        }
                        else
                        {
                            Hdf_No_Solicitud_Pago.Value = "";
                        }
                    }
                    else
                    {
                        Hdf_No_Solicitud_Pago.Value = "";
                    }

                    //Colocar los datos en los controles                                    
                    Txt_No_Contrarecibo.Text = No_Contra_Recibo.ToString().Trim();
                    Txt_No_Orden_Compra.Text = No_Orden_Compra.ToString().Trim();
                    if (!String.IsNullOrEmpty(Ds_Datos_Contrarecibo.Tables[0].Rows[0]["FECHA_RECEPCION"].ToString()))
                        Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", Ds_Datos_Contrarecibo.Tables[0].Rows[0]["FECHA_RECEPCION"]);
                    else
                        Txt_Fecha_Recepcion.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
                    Calcular_Fecha_Pago();
                    Txt_Proveedor.Text = Ds_Datos_Contrarecibo.Tables[0].Rows[0]["PROVEEDOR"].ToString().Trim();
                    Txt_Proveedor_ID.Value = Ds_Datos_Contrarecibo.Tables[0].Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                    Txt_Tipo_Contrarecibo.Text = Hdf_Tipo_Orden_Compra.Value;
                }

                //Verificar si los detalles tienen datos
                if (Ds_Datos_Contrarecibo.Tables[1].Rows.Count > 0)
                {
                    //Llenar el grid de los detalles de las facturas
                    Grid_Facturas.DataSource = Ds_Datos_Contrarecibo.Tables[1];

                    //Mostrar las columnas 9, 10, 11
                    Grid_Facturas.Columns[9].Visible = true;
                    Grid_Facturas.Columns[10].Visible = true;
                    Grid_Facturas.Columns[11].Visible = true;
                    Grid_Facturas.DataBind();

                    //Ocultar las columnas 9, 10, 11
                    Grid_Facturas.Columns[9].Visible = false;
                    Grid_Facturas.Columns[10].Visible = false;
                    Grid_Facturas.Columns[11].Visible = false;
                    //Colocar la tabla en una variable de sesion
                    HttpContext.Current.Session[P_Dt_Facturas] = Ds_Datos_Contrarecibo.Tables[1];
                    Btn_Modificar.Enabled = true;
                    //if (Hdf_Estatus_Contrarecibo.Value == "GENERADO")
                    //    Btn_Modificar.Enabled = false;
                }
            }
            else
            {
                Mostrar_Error("El contrarecibo no contiene datos.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Calcular_Fecha_Pago()
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable apra la capa de negocios
        if (!string.IsNullOrEmpty(Txt_Fecha_Recepcion.Text.Trim()))
        {
            Contrarecibos_Negocio.P_Fecha_Recepcion_dt = Convierte_Fecha_DT(Txt_Fecha_Recepcion.Text.Trim());
            Txt_Fecha_Pago.Text = String.Format("{0:dd/MMM/yyyy}", Contrarecibos_Negocio.Consulta_Fecha_Pago());
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Validacion_Datos
    ///DESCRIPCION:             Validar que esten todos los datos para el alta o la modificacion de un contrarecibo
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              30/Enero/2013 19:31
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private DataTable Generar_Tabla_Facturas()
    {
        DataTable Dt_Facturas = new DataTable();
        //Agregar las columnas a la tabla
        Dt_Facturas.Columns.Add("NO_FACTURA_PROVEEDOR", typeof(String));
        Dt_Facturas.Columns.Add("FECHA_FACTURA", typeof(DateTime));
        Dt_Facturas.Columns.Add("IMPORTE", typeof(Double));
        Dt_Facturas.Columns.Add("DESCRIPCION", typeof(String));
        Dt_Facturas.Columns.Add("ARCHIVO_XML", typeof(String));
        Dt_Facturas.Columns.Add("FACTURA_ID", typeof(int));
        Dt_Facturas.Columns.Add("ESTATUS", typeof(String));
        Dt_Facturas.Columns.Add("TIPO_ORDEN_COMPRA", typeof(String));
        return Dt_Facturas;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Validacion_Datos
    ///DESCRIPCION:             Validar que esten todos los datos para el alta o la modificacion de un contrarecibo
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              30/Enero/2013 19:31
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private String Validacion_Datos()
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        String Mensaje = String.Empty; //variable para el mensaje del error
        Boolean Mostrar_Mensaje = false; //variable que indica si se tiene que mostrar el mensaje de validacion
        DataTable Dt_Facturas = new DataTable(); //tabla con las facturas
        int Cont_Elementos = 0; //Contador
        int Cont_Facturas = 0; //Contador de las facturas

        try
        {
            //Construir el mensaje de error
            Mensaje = "Es necesario:";

            //Verificar si se tiene el estatus
            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Mensaje += "<br />+ Seleccionar un valor para el campo Estatus.";
                Mostrar_Mensaje = true;
            }

            //Verificar si se tiene la fecha de recepcion
            if (String.IsNullOrEmpty(Txt_Fecha_Recepcion.Text.Trim()))
            {
                Mensaje += "<br />+ Seleccionar un valor para la fecha de recepcción.";
                Mostrar_Mensaje = true;
            }

            //verificar si existe la tabla de las facturas
            if ((DataTable)HttpContext.Current.Session[P_Dt_Facturas] == null)
            {
                Mensaje += "<br />+ Agregar facturas.";
                Mostrar_Mensaje = true;
            }
            else
            {
                //Colocar variable de sesion en tabla
                Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);

                //verificar si la tabla tiene elementos
                if (Dt_Facturas.Rows.Count > 0)
                {
                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Facturas.Rows.Count; Cont_Elementos++)
                    {
                        //verificar si la factura es alta o cambio
                        if (Dt_Facturas.Rows[Cont_Elementos]["ESTATUS"].ToString().Trim() != "BAJA")
                        {
                            Cont_Facturas++;
                        }
                    }

                    //Verificar si hay facturas activas
                    if (Cont_Facturas == 0)
                    {
                        Mensaje += "<br />+ Agregar facturas.";
                        Mostrar_Mensaje = true;
                    }
                }
                else
                {
                    Mensaje += "<br />+ Agregar facturas.";
                    Mostrar_Mensaje = true;
                }
            }

            //Verificar si se tiene que mostrar el mensaje
            if (Mostrar_Mensaje == true)
            {
                Resultado = Mensaje;
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Guardar_Contrarecibo(Int64 No_Contra_Recibo)
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable apra la capa de negocios
        DataTable Dt_Facturas = new DataTable(); //Tabla para las facturas
        DataTable Dt_Facturas_Eliminadas = new DataTable(); //Tabla para las facturas eliminadas
        Double Total_Importe = 0; //variable para el calculo del importe
        Int64 No_Contrarecibo_Nuevo = 0; //variable para el nuevo numero de contrarecibo
        string Contrarecibo_Solicitud = string.Empty; //variable para el contrarecibo y solicitud de pago
        string[] vec_Contrarecibo_Solicitud; //vector para el contrarecibo y la solicitud de pago
        Int64 No_Solicitud = 0; //variable para el numero de solicitud

        try
        {
            //Colocar la variable de sesion en la tabla
            Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);
            foreach (DataRow Dr_Renglon in Dt_Facturas.Rows)
            {
                Total_Importe += Convert.ToDouble(Dr_Renglon["TOTAL_FACTURA"]);
            }
            //Asignar propiedades
            if (Txt_No_Contrarecibo_Escondido.Value.Trim() != "0")
            {
                Contrarecibos_Negocio.P_No_Contra_Recibo = Convert.ToInt64(Txt_No_Contrarecibo_Escondido.Value.Trim());
            }
            if (HttpContext.Current.Session[P_Dt_Facturas_Eliminadas] != null)
                Dt_Facturas_Eliminadas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas_Eliminadas]);
            else
                Dt_Facturas_Eliminadas = Generar_Tabla_Facturas();
            Contrarecibos_Negocio.P_Dt_Facturas_Eliminadas = Dt_Facturas_Eliminadas;
            Contrarecibos_Negocio.P_No_Orden_Compra = Txt_No_Orden_Compra.Text.Trim();
            Contrarecibos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
            Contrarecibos_Negocio.P_Fecha_Recepcion = Txt_Fecha_Recepcion.Text.Trim();
            Contrarecibos_Negocio.P_Fecha_Recepcion_dt = Convierte_Fecha_DT(Txt_Fecha_Recepcion.Text.Trim());
            Contrarecibos_Negocio.P_Fecha_Pago = String.Format("{0:dd/MMM/yyyy}", Contrarecibos_Negocio.Consulta_Fecha_Pago());
            Contrarecibos_Negocio.P_Proveedor_ID = Txt_Proveedor_ID.Value.Trim();
            Contrarecibos_Negocio.P_Importe_Total = Total_Importe;
            Contrarecibos_Negocio.P_Dt_Facturas = Dt_Facturas;
            Contrarecibos_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Contrarecibos_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
            Contrarecibos_Negocio.P_Proveedor = Txt_Proveedor.Text.Trim();

            //Verificar si se modifica o se guarda un nuevo contrarecibo
            if (Txt_No_Contrarecibo_Escondido.Value.Trim() != "0")
            {
                if (Hdf_Tipo_Orden_Compra.Value == "TALLER")
                    Contrarecibos_Negocio.Modificar_Contrarecibo_Taller();
                else if (Hdf_Tipo_Orden_Compra.Value == "COMPRAS" || Hdf_Tipo_Orden_Compra.Value == "SERVICIOS GENERALES")
                    Contrarecibos_Negocio.Modificar_Contrarecibo();
                else if (Hdf_Tipo_Orden_Compra.Value == "TALLER_EXTERNO" || Hdf_Tipo_Orden_Compra.Value == "TARJETAS GASOLINA")
                    Contrarecibos_Negocio.Modificar_Contrarecibo_Taller_Externo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Se modificó el contrarecibo: " + Txt_No_Contrarecibo_Escondido.Value.Trim() + " ');", true);
            }
            else
            {
                if (Hdf_Tipo_Orden_Compra.Value == "TALLER")
                    Contrarecibo_Solicitud = Contrarecibos_Negocio.Alta_Contrarecibo_Taller();
                else if (Hdf_Tipo_Orden_Compra.Value == "COMPRAS" || Hdf_Tipo_Orden_Compra.Value == "SERVICIOS GENERALES")
                    Contrarecibo_Solicitud = Contrarecibos_Negocio.Alta_Contrarecibo();
                else if (Hdf_Tipo_Orden_Compra.Value == "TALLER_EXTERNO" || Hdf_Tipo_Orden_Compra.Value == "TARJETAS GASOLINA")
                {
                    Contrarecibos_Negocio.P_Tipo_OC = Hdf_Tipo_Orden_Compra.Value.Trim();
                    Contrarecibo_Solicitud = Contrarecibos_Negocio.Alta_Contrarecibo_Taller_Externo();
                }

                //Obtener el vector del contrarecibo y solicitud
                vec_Contrarecibo_Solicitud = Contrarecibo_Solicitud.Split(',');
                
                //asignar el contrarecibo y la solicitud de pago
                No_Contrarecibo_Nuevo = Convert.ToInt64(vec_Contrarecibo_Solicitud[0]);
                No_Solicitud = Convert.ToInt64(vec_Contrarecibo_Solicitud[1]);

                //Imprimir el contrarecibo y la solcitud de pago
                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Se generó el contrarecibo: " + No_Contrarecibo_Nuevo.ToString() + ", y la solicitud de pago no:" + No_Solicitud.ToString() + " ');", true);
                Reporte_Contra_Recibo(No_Contrarecibo_Nuevo);

                ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('Operacion Exitosa: Se generó la solicitud de pagos: " + No_Solicitud.ToString() + " ');", true);
                Imprimir(vec_Contrarecibo_Solicitud[1].Trim());
            }

            //Volver al estado inicial
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Limpiar_Controles_Busqueda
    ///DESCRIPCION:             Limpiar los controles de la busqueda
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              30/Enero/2013 18:07
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Limpiar_Controles_Busqueda()
    {
        try
        {
            Txt_No_Contra_Recibo_Busqueda.Text = String.Empty;
            Txt_Busqueda_Proveedor.Text = String.Empty;
            Txt_Fecha_Inicio.Text = String.Empty;
            Txt_Fecha_Fin.Text = string.Empty;
            Txt_No_Contrarecibo_Escondido.Value = String.Empty;
            Txt_No_Orden_Compra_Busqueda.Text = String.Empty;
            Cmb_Estatus_Busqueda.SelectedIndex = 0;
            Cmb_Proveedores.Items.Clear();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Validacion_Facturas_Repetidas
    /// DESCRIPCIÓN:         valida que este la informacion de una factura no este repetida
    /// PARÁMETROS:          No_Factura: Cadena de texto con la factura a buscar
    /// USUARIO CREO:        Noe Mosqueda Valadez
    /// FECHA CREO:          06/Febrero/2013 14:00 
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    private String Validacion_Facturas_Repetidas(String No_Factura)
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        DataTable Dt_Facturas = new DataTable(); //tabla para las facturas
        int Cont_Elementos = 0; //variable para el contador

        try
        {
            //verificar si la variable de sesion no es nula
            if (HttpContext.Current.Session[P_Dt_Facturas] != null)
            {
                //Colocar la variable de sesion en la tabla
                Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);

                //verificar si la tabla tiene elementos
                if (Dt_Facturas.Rows.Count > 0)
                {
                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Facturas.Rows.Count; Cont_Elementos++)
                    {
                        //Verificar si existe la factura en la tabla
                        if (String.Equals(No_Factura.ToUpper().Trim(), Dt_Facturas.Rows[Cont_Elementos]["NO_FACTURA_PROVEEDOR"].ToString().ToUpper().Trim()) == true)
                        {
                            Resultado = "La factura " + No_Factura.Trim() + " ya fue agregada previamente.";
                            break;
                        }
                    }
                }
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Validacion_Facturas
    /// DESCRIPCIÓN:         valida que este la informacion de una factura a ingresar a la orden de compra/contrarecibo
    /// PARÁMETROS:          
    /// USUARIO CREO:        Noe Mosqueda Valadez
    /// FECHA CREO:          06/Febrero/2013 13:21 
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    private String Validacion_Facturas()
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        String Mensaje = String.Empty; //variable apra el mensaje de error
        Boolean Mostrar_Mensaje = false; //variable que indica si se tiene que mostrar le mensaje de error

        try
        {
            //Construir el mensaje de error
            Mensaje = "Es necesario:";

            //Verificar si estan todos los datos requeridos para agregar una factura
            if (String.IsNullOrEmpty(Txt_No_Factura.Text.Trim()) == true)
            {
                Mensaje += "<br />+ Ingresar el número de factura.";
                Mostrar_Mensaje = true;
            }
            if (String.IsNullOrEmpty(Txt_Fecha_Factura.Text.Trim()) == true)
            {
                Mensaje += "<br />+ Ingresar la fecha de la factura.";
                Mostrar_Mensaje = true;
            }
            if (String.IsNullOrEmpty(Txt_Monto_Factura.Text.Trim()) == true)
            {
                Mensaje += "<br />+ Ingresar el monto de la factura.";
                Mostrar_Mensaje = true;
            }

            //Verificar si se tiene que mostrar el mensaje de error
            if (Mostrar_Mensaje == true)
            {
                Resultado = Mensaje;
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Colocar_Proximo_Contrarecibo
    ///DESCRIPCION:             Colocar el proximo numero de contrarecibo
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              31/Enero/2013 10:49
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Colocar_Proximo_Contrarecibo()
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable apra la capa de negocios

        try
        {
            //Colocar el numero de contrarecibo en la caja de texto
            Txt_No_Contrarecibo.Text = Contrarecibos_Negocio.Proximo_No_Contrarecibo().ToString().Trim();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Convierte_Fecha_DT
    ///DESCRIPCION:             Convertir la fecha de una cadena de texto en DateTime
    ///PARAMETROS:              Busqueda: Cadena de texto con el texto a buscar de los proveedores
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              28/Febrero/2013 12:00
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private DateTime Convierte_Fecha_DT(String Fecha)
    {
        //Declaracion de variables
        int Dia = 0; //variable para el dia
        int Mes = 0; //variable para el mes
        int Anio = 0; //Variable para el año
        DateTime Resultado = new DateTime(1900, 1, 1); //Variable para el resultado

        try
        {
            //Obtener el dia
            Dia = Convert.ToInt32(Fecha.Substring(0, 2));

            //Obtener el año
            Anio = Convert.ToInt32(Fecha.Substring(7, 4));

            //Obtener el mes
            switch (Fecha.Substring(3, 3).ToUpper())
            {
                case "ENE":
                case "JAN":
                    Mes = 1;
                    break;

                case "FEB":
                    Mes = 2;
                    break;

                case "MAR":
                    Mes = 3;
                    break;

                case "ABR":
                case "APR":
                    Mes = 4;
                    break;

                case "MAY":
                    Mes = 5;
                    break;

                case "JUN":
                    Mes = 6;
                    break;

                case "JUL":
                    Mes = 7;
                    break;

                case "AGO":
                case "AUG":
                    Mes = 8;
                    break;

                case "SEP":
                    Mes = 9;
                    break;

                case "OCT":
                    Mes = 10;
                    break;

                case "NOV":
                    Mes = 11;
                    break;

                case "DIC":
                case "DEC":
                    Mes = 12;
                    break;

                default:
                    Mes = 1;
                    break;
            }

            //Construir el resultado
            Resultado = new DateTime(Anio, Mes, Dia);

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Limpiar_Controles_Detalles
    ///DESCRIPCION:             Limpiar los controles de los detalles (facturas)
    ///PARAMETROS:              
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              30/Enero/2013 18:10
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Limpiar_Controles_Detalles()
    {
        try
        {
            Txt_Fecha_Factura.Text = "";
            Txt_Monto_Factura.Text = "";
            Txt_IVA_Factura.Text = "";
            Txt_Total_Factura.Text = "";
            Txt_No_Factura.Text = "";
            Hdf_No_Factura.Value = String.Empty;
            Txt_Comentarios_Archivo.Text = String.Empty;
            HttpContext.Current.Session.Remove(P_Archivo_XML);
            HttpContext.Current.Session.Remove(P_Comentarios);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Reporte_Contra_Recibo(Int64 No_Contra_Recibo)
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //variable apra la capa de negocios
        DataSet Ds_Reporte_Contrarecibo = new DataSet(); //Dataset para la consulta del reporte
        Ds_Alm_Contrarecibos Ds_Alm_Contrarecibos_src = new Ds_Alm_Contrarecibos(); //Dataset archivo para el llenadp del reporte
        int Cont_Elementos = 0; //variable para el contador
        DataRow Renglon; //renglon para el llenado de las tablas
        String Nombre_Reporte = "Rpt_Alm_Contrarecibo_CCopia.rpt"; //variable para el nombre del reporte de Crystal
        ReportDocument Reporte = new ReportDocument(); //Variable para el reporte de Crystal
        String File_Path = String.Empty; //Variable para la ruta del archivo
        String Nombre_PDF = "Contrarecibo_" + No_Contra_Recibo.ToString().Trim() + ".pdf"; //variable para el nombre del dopcumento pdf

        try
        {
            //Asignar propiedades
            Contrarecibos_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
            Contrarecibos_Negocio.P_Tipo_OC = Hdf_Tipo_Orden_Compra.Value;
            //Ejecutar consulta
            Ds_Reporte_Contrarecibo = Contrarecibos_Negocio.Consulta_Reporte_Contrarecibo();

            //Verificar si la consulta arrojo resultados
            if (Ds_Reporte_Contrarecibo.Tables.Count == 2)
            {
                if (Ds_Reporte_Contrarecibo.Tables[0].Rows.Count > 0 && Ds_Reporte_Contrarecibo.Tables[1].Rows.Count > 0)
                {
                    //Ciclos para el llenado de las tablas del reporte
                    for (Cont_Elementos = 0; Cont_Elementos < Ds_Reporte_Contrarecibo.Tables[0].Rows.Count; Cont_Elementos++)
                    {
                        //Instanciar renglon y colocarlo en la tabla del reporte
                        Renglon = Ds_Reporte_Contrarecibo.Tables[0].Rows[Cont_Elementos];
                        Ds_Alm_Contrarecibos_src.Tables[0].ImportRow(Renglon);
                    }

                    for (Cont_Elementos = 0; Cont_Elementos < Ds_Reporte_Contrarecibo.Tables[1].Rows.Count; Cont_Elementos++)
                    {
                        //Instanciar renglon y colocarlo en la tabla del reporte
                        Renglon = Ds_Reporte_Contrarecibo.Tables[1].Rows[Cont_Elementos];
                        Ds_Alm_Contrarecibos_src.Tables[1].ImportRow(Renglon);
                    }

                    File_Path = Server.MapPath("../Rpt/Almacen/" + Nombre_Reporte);
                    Reporte.Load(File_Path);
                    //Ds_Reporte = Data_Set_Consulta_DB;
                    Reporte.SetDataSource(Ds_Alm_Contrarecibos_src);
                    ExportOptions Export_Options = new ExportOptions();
                    DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                    Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
                    Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                    Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                    Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Export_Options);
                    String Ruta = "../../Reporte/" + Nombre_PDF;
                    Mostrar_Reporte(Nombre_PDF);
                }
                else
                {
                    Mostrar_Error("El reporte no contiene datos.", true);
                }
            }
            else
            {
                Mostrar_Error("El reporte no contiene datos.", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Pagina = "../../Reporte/";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Contrarecibo','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    private void Mostrar_Archivo(String URL)
    {
        string Pagina = "../../Reporte/";
        try
        {
            Pagina = Pagina + URL;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "B1",
                "window.open('" + Pagina + "', 'Solicitud Pago','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "B", "window.location='Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL + "';", true);
            //Response.Redirect("Frm_Con_Mostrar_Archivos.aspx?Documento=" + URL, false);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el archivo de dispersion generado generado. Error: [" + Ex.Message + "]");
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Mostrar_Error(Ex.Message, true);
        }

    }

    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    //Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                    Mostrar_Archivo(Nombre_Reporte_Generar + Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    #region (Grid)
    #region (Contrarecibos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Llena_Grid_Contrarecibos
    ///DESCRIPCION:             Llenar el Grid de los contrarecibos de acuerdo a los criterios de busqueda
    ///PARAMETROS:              1. No_Contra_Recibo: Entero que contiene el numero de contrarecibo a buscar
    ///                         2. No_Orden_Compra: Entero que contiene el numero de la orden de compra a buscar
    ///                         2. Proveedor_ID: Cadena de texto que contiene el ID del proveedor
    ///                         3. Estatus: Cadena de texto que contiene el Estatus
    ///                         4. Fecha_Inicio: Fecha de inicio para un intervalo de tiempo de la busqueda
    ///                         5. Fecha_Fin: Fecha fin para un intervalo de tiempo de la busqueda
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              29/Enero/2013 12:40
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Llena_Grid_Contrarecibos(Int64 No_Contra_Recibo, String No_Orden_Compra, String Proveedor_ID, String Estatus, bool Incluir_Estatus_Nulo, String Fecha_Inicio, String Fecha_Fin, int Pagina)
    {
        //Declaracion de variables
        DataTable Dt_contrarecibos = new DataTable(); //tabla para los contrarecibos
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //Variable apra la capa de negocios

        try
        {
            Contrarecibos_Negocio.P_Incluir_Estatus_Pendientes = Incluir_Estatus_Nulo;
            //Verificar si hay paginacion
            if (Pagina > -1 && HttpContext.Current.Session[P_Dt_Contrarecibos] != null)
            {
                //Colocar la variable de sesion en la tabla
                Dt_contrarecibos = ((DataTable)HttpContext.Current.Session[P_Dt_Contrarecibos]);
            }
            else
            {
                //Verificar si hay un numero de contrarecibo u orden de compra
                if (No_Contra_Recibo > 0 || No_Orden_Compra != "")
                {
                    Contrarecibos_Negocio.P_Fecha_Inicio = Fecha_Inicio;
                    Contrarecibos_Negocio.P_Fecha_Fin = Fecha_Fin;
                    //verificar si hay numero de contrarecibo
                    if (No_Contra_Recibo > 0)
                    {
                        //Asignar propiedades
                        Contrarecibos_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
                    }
                    else
                    {
                        //Asignar propiedades
                        Contrarecibos_Negocio.P_No_Orden_Compra = No_Orden_Compra;
                    }

                    //Ejecutar consulta
                    Dt_contrarecibos = Contrarecibos_Negocio.Consulta_Contrarecibos();
                }
                else
                {
                    //Asignar propiedades
                    Contrarecibos_Negocio.P_No_Contra_Recibo = -1;
                    Contrarecibos_Negocio.P_Estatus = Estatus;
                    Contrarecibos_Negocio.P_Proveedor_ID = Proveedor_ID;
                    Contrarecibos_Negocio.P_Fecha_Inicio = Fecha_Inicio;
                    Contrarecibos_Negocio.P_Fecha_Fin = Fecha_Fin;
                    //Ejecutar consulta
                    Dt_contrarecibos = Contrarecibos_Negocio.Consulta_Contrarecibos();
                }
            }
            //Ordenar DataTable
            Dt_contrarecibos.DefaultView.Sort = "TIPO ASC";
            //llenar el Grid
            Grid_Contrarecibos.DataSource = Dt_contrarecibos;

            //Verificar si hay pagina
            if (Pagina > -1)
            {
                Grid_Contrarecibos.PageIndex = Pagina;
            }

            Grid_Contrarecibos.Columns[7].Visible = true;
            Grid_Contrarecibos.DataBind();
            Grid_Contrarecibos.Columns[7].Visible = false;

            //Colocar la tabla en una variable de sesion
            HttpContext.Current.Session[P_Dt_Contrarecibos] = Dt_contrarecibos;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    protected void Grid_Contrarecibos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //Verirficar si hay valor en el numero del contrarecibo
            if (String.IsNullOrEmpty(Grid_Contrarecibos.SelectedRow.Cells[2].Text) == false)
            {
                //Habilitar el modo de edicion
                Habilitar_Controles("Seleccionar");
                //Definir Tipo de Orden de Compra Taller/Compras
                Hdf_Tipo_Orden_Compra.Value = Grid_Contrarecibos.SelectedDataKey["TIPO"].ToString();
                //if (Hdf_Tipo_Orden_Compra.Value.Trim().Equals("SERVICIOS GENERALES")) Hdf_Tipo_Orden_Compra.Value = "COMPRAS";
                Hdf_No_Contrarecibo.Value = Grid_Contrarecibos.SelectedDataKey["NO_CONTRA_RECIBO"].ToString();
                Hdf_Estatus_Contrarecibo.Value = Grid_Contrarecibos.SelectedDataKey["ESTATUS"].ToString(); ;
                //Verificar si es numerico
                if (Cls_Util.EsNumerico(Grid_Contrarecibos.SelectedRow.Cells[2].Text) == true)
                {
                    Llena_Datos_Controles(Convert.ToInt64(Grid_Contrarecibos.SelectedRow.Cells[2].Text),Grid_Contrarecibos.SelectedRow.Cells[7].Text.Trim());
                }
                else
                {
                    Llena_Datos_Controles(0, Grid_Contrarecibos.SelectedRow.Cells[7].Text.Trim());
                }
            }
            else
            {
                Llena_Datos_Controles(0, Grid_Contrarecibos.SelectedRow.Cells[7].Text.Trim());
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Grid_Contrarecibos_SelectedIndexChanged)" + ex.Message, true);
        }
    }
    #endregion

    #region (Facturas)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Agregar_Factura
    ///DESCRIPCION:             Agregar la factura al grid de las facturas
    ///PARAMETROS:              1. No_Factura_Proveedor: Cadena de texto que contiene el numero de la factura del proveedor
    ///                         2. Fecha_Factura: Fecha de la factura del proveedor
    ///                         2. Importe: Monto de la factura
    ///                         3. Archivo_XML: Cadena de texto con el nombre del archivo
    ///                         4. Factura_ID: ID de la factura (BD)
    ///                         5. Estatus: Cadena de texto que indica si la factura es nueva
    ///                         6. IVA_Factura: IVA de la factura
    ///                         7. Total_Factura: Total de la factura
    ///CREO:                    Noe Mosqueda Valadez
    ///FECHA_CREO:              28/Febrero/2013 11:45
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    private void Agregar_Factura(String No_Factura_Proveedor, DateTime Fecha_Factura, Double Importe, String Archivo_XML, int Factura_ID, String Estatus, String Comentarios, double IVA_Factura, double Total_Factura)
    {
        //Declaracion de variables
        DataTable Dt_Facturas = new DataTable(); //Tabla para el llenado del grid
        DataRow Renglon; //Renglon para el llenado de la tabla
        try
        {
            //Verificar si existe la variable de sesion
            if (HttpContext.Current.Session[P_Dt_Facturas] != null)
            {
                //Colocar la variable de sesion en la tabla
                Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);
            }
            else
            {
                //Agregar las columnas a la tabla
                Dt_Facturas.Columns.Add("NO_FACTURA_PROVEEDOR", typeof(String));
                Dt_Facturas.Columns.Add("FECHA_FACTURA", typeof(DateTime));
                Dt_Facturas.Columns.Add("IMPORTE", typeof(Double));
                Dt_Facturas.Columns.Add("DESCRIPCION", typeof(String));
                Dt_Facturas.Columns.Add("ARCHIVO_XML", typeof(String));
                Dt_Facturas.Columns.Add("FACTURA_ID", typeof(int));
                Dt_Facturas.Columns.Add("ESTATUS", typeof(String));
                Dt_Facturas.Columns.Add("TIPO_ORDEN_COMPRA", typeof(String));
                Dt_Facturas.Columns.Add("IVA_FACTURA", typeof(Double));
                Dt_Facturas.Columns.Add("TOTAL_FACTURA", typeof(Double));
            }

            //Instanciar el renglon
            Renglon = Dt_Facturas.NewRow();

            //Llenar el renglon
            Renglon["NO_FACTURA_PROVEEDOR"] = No_Factura_Proveedor;
            Renglon["FECHA_FACTURA"] = Fecha_Factura;
            Renglon["IMPORTE"] = Importe;
            Renglon["DESCRIPCION"] = Comentarios;
            Renglon["ARCHIVO_XML"] = Archivo_XML;
            Renglon["FACTURA_ID"] = Factura_ID;
            Renglon["ESTATUS"] = Estatus;
            Renglon["TIPO_ORDEN_COMPRA"] = Hdf_Tipo_Orden_Compra.Value;
            Renglon["IVA_FACTURA"] = IVA_Factura;
            Renglon["TOTAL_FACTURA"] = Total_Factura;

            //Colocar el renglon en la tabla 
            Dt_Facturas.Rows.Add(Renglon);

            //Llenar el grid
            Grid_Facturas.DataSource = Dt_Facturas;
            Grid_Facturas.Columns[9].Visible = true;
            Grid_Facturas.Columns[10].Visible = true;
            Grid_Facturas.Columns[11].Visible = true;
            Grid_Facturas.DataBind();
            Grid_Facturas.Columns[9].Visible = false;
            Grid_Facturas.Columns[10].Visible = false;
            Grid_Facturas.Columns[11].Visible = false;
            //Colocar la tabla en la variable de sesion
            HttpContext.Current.Session[P_Dt_Facturas] = Dt_Facturas;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    protected void Img_Btn_Agregar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion
        String Ruta_Factura = "";

        try
        {
            if (HttpContext.Current.Session[P_Archivo_XML] != null)
            {
                Ruta_Factura = HttpContext.Current.Session[P_Archivo_XML].ToString();
            }
            Grid_Facturas.Columns[0].Visible = true;
            //Validar que existan todos los datos
            Validacion = Validacion_Facturas();
            if (String.IsNullOrEmpty(Validacion) == true)
            {
                //validar que la factura no este repetida
                Validacion = String.Empty;
                Validacion = Validacion_Facturas_Repetidas(Txt_No_Factura.Text.Trim());
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    Agregar_Factura(Txt_No_Factura.Text.Trim(), Convierte_Fecha_DT(Txt_Fecha_Factura.Text.Trim()), Convert.ToDouble(Txt_Monto_Factura.Text.Trim()), Ruta_Factura, 0, "ALTA", Txt_Comentarios_Archivo.Text.Trim(), Convert.ToDouble(Txt_IVA_Factura.Text.Trim()), Convert.ToDouble(Txt_Total_Factura.Text.Trim()));
                    Limpiar_Controles_Detalles();
                }
                else
                {
                    Mostrar_Error(Validacion, true);
                }
            }
            else
            {
                Mostrar_Error(Validacion, true);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Img_Btn_Buscar_Click)" + ex.Message, true);
        }
    }


    protected void Grid_Facturas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        //Declaracion de variables
        String Ruta = String.Empty; //variable para la ruta del archivo
        HyperLink Hyp_Lnk_Archivo_src; //variable para el hyperlink del archivo
        ImageButton Img_Btn_Cargar_Archivo_Factura_src; //variable para el boton de subir archivo

        try
        {
            //Verificar el tipo de renglon
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                //Instanciar el control
                Hyp_Lnk_Archivo_src = ((HyperLink)e.Row.Cells[4].FindControl("Hyp_Lnk_Archivo"));
                Img_Btn_Cargar_Archivo_Factura_src = ((ImageButton)e.Row.Cells[5].FindControl("Img_Btn_Cargar_Archivo_Factura"));

                String ARCHIVO_XML = HttpUtility.HtmlDecode(e.Row.Cells[7].Text);
                ARCHIVO_XML = ARCHIVO_XML.Trim();

                //Verificar si deebe haber hyperlink
                if (!String.IsNullOrEmpty(ARCHIVO_XML))
                {
                    //Colocarle los detalles de la navegacion
                    Hyp_Lnk_Archivo_src.Text = ARCHIVO_XML;
                    Ruta = "~/Contrarecibos_Archivos/";

                    //Colocar el resto de la ruta
                    Ruta += "CR-" + Txt_No_Contrarecibo.Text.Trim() + "/" + e.Row.Cells[1].Text.Trim() + "/" + ARCHIVO_XML;

                    //Asignar la URL
                    Hyp_Lnk_Archivo_src.NavigateUrl = Ruta;
                    Hyp_Lnk_Archivo_src.Target = "_blank";
                }

                //Verificar si esta en modo de edicion
                Img_Btn_Cargar_Archivo_Factura_src.Visible = Btn_Modificar.ToolTip == "Guardar" ? true : false;
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Grid_Facturas_RowDataBound)" + ex.Message, true);
        }
    }

    #endregion

    #endregion

    #region (Eventos)
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //Verificar el tooltip
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                DataTable Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);

                if (Dt_Facturas != null)
                {
                    foreach (DataRow Dr_Factura in Dt_Facturas.Rows)
                    {
                        if (Dr_Factura["ESTATUS"].ToString().ToUpper().Trim() != "ORIGINAL")
                        {
                            String Ruta = HttpContext.Current.Server.MapPath("~");
                            //verificar si el directorio ya ha sido creado
                            if (Directory.Exists(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.ToString() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString()))
                            {
                                String[] Files = Directory.GetFiles(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.ToString() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString());

                                foreach (String File in Files)
                                {
                                    System.IO.File.Delete(File);
                                }
                            }
                        }
                    }
                }

                DataTable Dt_Facturas_Eliminadas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas_Eliminadas]);

                if (Dt_Facturas_Eliminadas != null)
                {
                    foreach (DataRow Dr_Factura in Dt_Facturas_Eliminadas.Rows)
                    {
                        String Ruta = HttpContext.Current.Server.MapPath("~");
                        //verificar si el directorio ya ha sido creado
                        if (Directory.Exists(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.Trim() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString()))
                        {
                            String[] Files = Directory.GetFiles(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.Trim() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString());

                            foreach (String File in Files)
                            {
                                System.IO.File.Delete(File);
                            }
                        }
                    }
                }

                Estado_Inicial();
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Salir_Click)" + ex.Message, true);
        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para al validacion
        DataTable Dt_Facturas = new DataTable();
        try
        {
            //Verificar el tooltip del boton
            if (Btn_Modificar.ToolTip == "Guardar")
            {
                //validar que esten todos los datos
                Validacion = Validacion_Datos();
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    DataTable Dt_Facturas_Eliminadas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas_Eliminadas]);

                    if (Dt_Facturas_Eliminadas != null)
                    {
                        foreach (DataRow Dr_Factura in Dt_Facturas_Eliminadas.Rows)
                        {
                            String Ruta = HttpContext.Current.Server.MapPath("~");
                            //verificar si el directorio ya ha sido creado
                            if (Directory.Exists(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.Trim() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString()))
                            {
                                String[] Files = Directory.GetFiles(Ruta + "\\Contrarecibos_Archivos\\CR-" + Txt_No_Contrarecibo.Text.Trim() + "\\" + Dr_Factura["NO_FACTURA_PROVEEDOR"].ToString());

                                foreach (String File in Files)
                                {
                                    System.IO.File.Delete(File);
                                }
                            }
                        }
                    }

                    Guardar_Contrarecibo(Convert.ToInt64(Txt_No_Contrarecibo_Escondido.Value));
                }
                else
                {
                    Mostrar_Error(Validacion, true);
                }
            }
            else
            {
                Habilitar_Controles("Modificar");
                //Verificar si existe la variable de sesion
                if (HttpContext.Current.Session[P_Dt_Facturas] != null)
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);
                    Grid_Facturas.DataSource = Dt_Facturas;
                    Grid_Facturas.DataSource = Dt_Facturas;
                    Grid_Facturas.Columns[9].Visible = true;
                    Grid_Facturas.Columns[10].Visible = true;
                    Grid_Facturas.Columns[11].Visible = true;
                    Grid_Facturas.DataBind();
                    Grid_Facturas.Columns[9].Visible = false;
                    Grid_Facturas.Columns[10].Visible = false;
                    Grid_Facturas.Columns[11].Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Btn_Modificar_Click)" + ex.Message, true);
        }
    }

    protected void Img_Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        Int64 No_Contra_Recibo = -1; //variable para el numero del contrarecibo
        String No_Orden_Compra = ""; //variable para el numero de la orden de compra
        String Proveedor_ID = String.Empty; //variable para el ID del proveedor
        String Estatus = String.Empty; //Variable para el estatus
        String Fecha_Inicio = String.Empty; //variable para la fecha de inicio
        String Fecha_Fin = String.Empty; //variable para la fecha fin

        try
        {
            //Obtener la fecha inicial
            Fecha_Inicio = "01/";

            if (DateTime.Now.Month.ToString().Trim().Length == 1)
            {
                Fecha_Inicio += "0";
            }

            Fecha_Inicio += DateTime.Now.Month.ToString().Trim() + "/" + DateTime.Now.Year.ToString().Trim();
            Fecha_Fin = string.Format("{0:dd/MM/yyyy}", DateTime.Now);
            //Verificar los filtros
            if (String.IsNullOrEmpty(Txt_No_Contra_Recibo_Busqueda.Text.Trim()) == false)
            {
                No_Contra_Recibo = Convert.ToInt64(Txt_No_Contra_Recibo_Busqueda.Text.Trim());
            }

            if (String.IsNullOrEmpty(Txt_No_Orden_Compra_Busqueda.Text.Trim()) == false)
            {
                No_Orden_Compra = Txt_No_Orden_Compra_Busqueda.Text.Trim().Trim();
            }

            if (String.IsNullOrEmpty(Txt_Fecha_Inicio.Text.Trim()) == false)
            {
                Fecha_Inicio = Txt_Fecha_Inicio.Text.Trim();
            }

            if (String.IsNullOrEmpty(Txt_Fecha_Fin.Text.Trim()) == false)
            {
                Fecha_Fin = Txt_Fecha_Fin.Text.Trim();
            }

            if (Cmb_Proveedores.SelectedIndex > 0)
            {
                Proveedor_ID = Cmb_Proveedores.SelectedItem.Value;
            }

            if (Cmb_Estatus_Busqueda.SelectedIndex > 0)
            {
                Estatus = Cmb_Estatus_Busqueda.SelectedItem.Value;
            }

            //Llenar el grid de los contrarecibos
            Llena_Grid_Contrarecibos(No_Contra_Recibo, No_Orden_Compra, Proveedor_ID, Estatus, false, Fecha_Inicio, Fecha_Fin, -1);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Img_Btn_Buscar_Click)" + ex.Message, true);
        }
    }

    protected void Img_Btn_Cargar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Txt_Comentarios_Archivo.Text = "";
            HttpContext.Current.Session.Remove(P_Archivo_XML);
            HttpContext.Current.Session.Remove(P_Comentarios);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Img_Btn_Buscar_Click)" + ex.Message, true);
        }
    }

    protected void AFil_Archivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        //Declaracion de variables
        String Ruta = String.Empty; //variable para la ruta
        String Numero_CR = Txt_No_Contrarecibo.Text.Trim();
        try
        {
            //Verificar si se tiene un archivo
            if (AFil_Archivo.HasFile)
            {
                String No_Factura = "";
                No_Factura = String.IsNullOrEmpty(Hdf_No_Factura.Value) ? Txt_No_Factura.Text.Trim() : Hdf_No_Factura.Value;

                //obtener la ruta del servidor
                Ruta = HttpContext.Current.Server.MapPath("~");
                if (Txt_No_Contrarecibo_Escondido.Value != "0")
                    Numero_CR = Txt_No_Contrarecibo_Escondido.Value;
                //verificar si el directorio ya ha sido creado
                if (Directory.Exists(Ruta + "\\Contrarecibos_Archivos\\CR-" + Numero_CR.Trim() + "\\" + No_Factura.Trim()))
                {
                    String[] Files = Directory.GetFiles(Ruta + "\\Contrarecibos_Archivos\\CR-" + Numero_CR.Trim() + "\\" + No_Factura.Trim());

                    foreach (String File in Files)
                    {
                        System.IO.File.Delete(File);
                    }
                }
                else
                {
                    Directory.CreateDirectory(Ruta + "\\Contrarecibos_Archivos\\CR-" + Numero_CR.Trim() + "\\" + No_Factura.Trim());
                }

                Ruta = Ruta + "\\Contrarecibos_Archivos\\CR-" + Numero_CR.Trim() + "\\" + No_Factura.Trim() + "\\" + System.IO.Path.GetFileName(AFil_Archivo.FileName);

                //Colocar los datos en los controles ocultos
                HttpContext.Current.Session[P_Archivo_XML] = AFil_Archivo.FileName;
                Session["Ruta_Factura"] = Ruta;
                AFil_Archivo.SaveAs(Ruta);
            }

        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (AFil_Archivo_UploadedComplete)" + ex.Message, true);
        }
    }

    protected void Img_Btn_Buscar_Proveedor_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        Cls_Ope_Alm_Contrarecibos_Negocio Contrarecibos_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio(); //Variable para la capa de negocios
        DataTable Dt_Proveedores = new DataTable(); //tabla para el resultado de la consulta

        try
        {
            //Ejecutar la consulta
            Contrarecibos_Negocio.P_Busqueda = Txt_Busqueda_Proveedor.Text.Trim();
            Dt_Proveedores = Contrarecibos_Negocio.Consulta_Proveedores();

            //Llenar el combo con el resultado de la consulta
            Cmb_Proveedores.Items.Clear();
            Cmb_Proveedores.DataSource = Dt_Proveedores;
            Cmb_Proveedores.DataTextField = "PROVEEDOR";
            Cmb_Proveedores.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
            Cmb_Proveedores.DataBind();
            Cmb_Proveedores.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
            Cmb_Proveedores.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            Mostrar_Error(ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Contrarecibo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Hdf_No_Contrarecibo.Value))
                Reporte_Contra_Recibo(Int64.Parse(Hdf_No_Contrarecibo.Value));
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: " + ex.Message, true);
        }
    }

    protected void Btn_Imprimir_Solicitud_Pago_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(Hdf_No_Solicitud_Pago.Value))
                Imprimir(Hdf_No_Solicitud_Pago.Value);
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: " + ex.Message, true);
        }
    }

    protected void Txt_Monto_Factura_TextChanged(object sender, EventArgs e)
    {
        //Declaracion de variables
        double Monto = 0; //variable para el monto de la factura
        double IVA = 0; //variable para el IVA
        double Porcentaje_IVA = 0; //variable para el porcentaje del IVA
        double Total_Factura = 0; //variable para el total del la factura
        Cls_Cat_Con_Parametros_Negocio Parametros_Negocio = new Cls_Cat_Con_Parametros_Negocio(); //variable para los parametros
        DataTable Dt_Parametros = new DataTable(); //tabla para los parametros

        try
        {
            //Consultar los parametros
            Dt_Parametros = Parametros_Negocio.Consulta_Datos_Parametros_2();

            //Verificar si arrojo datos
            if (Dt_Parametros.Rows.Count > 0)
            {
                if (Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_IVA] != null)
                {
                    if (Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_IVA] != DBNull.Value)
                    {
                        IVA = Convert.ToDouble(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_IVA]);
                    }
                }
            }

            //Colocar el monto en la variable
            Monto = Convert.ToDouble(Txt_Monto_Factura.Text.Trim());

            //Calcular el IVA
            IVA = (Monto * IVA) / 100;

            //Calcular el total
            Total_Factura = Monto + IVA;

            //Colocar los valores en las cajas de texto
            Txt_IVA_Factura.Text = IVA.ToString().Trim();
            Txt_Total_Factura.Text = Total_Factura.ToString().Trim();
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: " + ex.Message, true);
        }
    }

    protected void Txt_IVA_Factura_TextChanged(object sender, EventArgs e)
    {
        //Declaracion de variables
        double Monto = 0; //variable para el monto de la factura
        double IVA = 0; //variable para el IVA
        double Total_Factura = 0; //variable para el total del la factura

        try
        {
            //Colocar el monto en la variable
            Monto = Convert.ToDouble(Txt_Monto_Factura.Text.Trim());
            IVA = Convert.ToDouble(Txt_IVA_Factura.Text.Trim());

            //Calcular el total
            Total_Factura = Monto + IVA;

            //Colocar los valores en las cajas de texto
            Txt_Total_Factura.Text = Total_Factura.ToString().Trim();
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: " + ex.Message, true);
        }
    }

    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Grid_Facturas_RowCommand
    ///DESCRIPCION:             Evento para eliminar factura seleccionada
    ///PARAMETROS:              Sender y Evento Command del Grid
    ///CREO:                    Jesus Toledo Rdz
    ///FECHA_CREO:              15/Abril/2013 06:24
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACION
    ///*******************************************************************************
    protected void Grid_Facturas_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Cmd_Cargar_Archivo")
            {
                Hdf_No_Factura.Value = e.CommandArgument.ToString();
                Txt_Comentarios_Archivo.Text = String.Empty;
                HttpContext.Current.Session.Remove(P_Archivo_XML);
                HttpContext.Current.Session.Remove(P_Comentarios);
                MPE_Archivos_Requisicion.Show();
            }
        }
        catch (Exception ex) { Mostrar_Error(ex.Message, true); }
    }

    protected void Txt_Fecha_Recepcion_TextChanged(object sender, EventArgs e)
    {
        Calcular_Fecha_Pago();
    }

    protected void Btn_Archivos_Requisicion_Click(object sender, EventArgs e)
    {
        MPE_Archivos_Requisicion.Hide();

        if (!String.IsNullOrEmpty(Hdf_No_Factura.Value) &&
            (DataTable)HttpContext.Current.Session[P_Dt_Facturas] != null &&
            HttpContext.Current.Session[P_Archivo_XML] != null)
        {
            DataTable Dt_Facturas = Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);

            foreach (DataRow Dr_Renglon in Dt_Facturas.Rows)
            {
                if (Dr_Renglon["NO_FACTURA_PROVEEDOR"].ToString().Trim() == Hdf_No_Factura.Value)
                {
                    Dr_Renglon["ARCHIVO_XML"] = HttpContext.Current.Session[P_Archivo_XML].ToString().Trim();
                    Dr_Renglon["DESCRIPCION"] = Txt_Comentarios_Archivo.Text.Trim();
                    Dr_Renglon["ESTATUS"] = Dr_Renglon["FACTURA_ID"].ToString().Trim() == "0" ? "ALTA" : "CAMBIO";
                    Dr_Renglon.AcceptChanges();
                    Dt_Facturas.AcceptChanges();
                    break;
                }
            }
            HttpContext.Current.Session[P_Dt_Facturas] = Dt_Facturas;
            Grid_Facturas.Columns[0].Visible = true;
            Grid_Facturas.DataSource = Dt_Facturas;
            Grid_Facturas.Columns[7].Visible = true;
            Grid_Facturas.Columns[8].Visible = true;
            Grid_Facturas.Columns[9].Visible = true;
            Grid_Facturas.DataBind();
            Grid_Facturas.Columns[7].Visible = false;
            Grid_Facturas.Columns[8].Visible = false;
            Grid_Facturas.Columns[9].Visible = false;
            Limpiar_Controles_Detalles();
        }
    }

    protected void Grid_Facturas_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        String No_Factura = Grid_Facturas.Rows[e.RowIndex].Cells[1].Text.ToString().Trim();
        DataTable Dt_Facturas = new DataTable();
        DataTable Dt_Facturas_Eliminadas = new DataTable();
        DataRow[] Renglon; //Renglon para el llenado de la tabla
        try
        {
            //Registrar la Factura Eliminada
            if (HttpContext.Current.Session[P_Dt_Facturas_Eliminadas] != null)
                Dt_Facturas_Eliminadas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas_Eliminadas]);
            else
                Dt_Facturas_Eliminadas = Generar_Tabla_Facturas();


            Grid_Facturas.Columns[0].Visible = true;
            //Verificar si existe la variable de sesion// Otra validacionsilla no hace daño
            if (HttpContext.Current.Session[P_Dt_Facturas] != null)
            {
                //Colocar la variable de sesion en la tabla
                Dt_Facturas = ((DataTable)HttpContext.Current.Session[P_Dt_Facturas]);

                Renglon = Dt_Facturas.Select("NO_FACTURA_PROVEEDOR = '" + No_Factura + "'");

                if (Renglon.Length > 0)
                {
                    Dt_Facturas_Eliminadas.ImportRow(Renglon[0]);
                    Dt_Facturas.Rows.Remove(Renglon[0]);
                }
                Dt_Facturas.AcceptChanges();
                Dt_Facturas_Eliminadas.AcceptChanges();
                HttpContext.Current.Session[P_Dt_Facturas_Eliminadas] = Dt_Facturas_Eliminadas;
                Grid_Facturas.DataSource = Dt_Facturas;
                Grid_Facturas.Columns[7].Visible = true;
                Grid_Facturas.Columns[8].Visible = true;
                Grid_Facturas.Columns[9].Visible = true;
                Grid_Facturas.DataBind();
                Grid_Facturas.Columns[7].Visible = false;
                Grid_Facturas.Columns[8].Visible = false;
                Grid_Facturas.Columns[9].Visible = false;
            }
            else
            {
                throw new Exception("La session Expiró...");
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error(ex.Message, true);
        }
    }
}