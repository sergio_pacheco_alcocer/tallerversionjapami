﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using JAPAMI.Sessiones;
using JAPAMI.Rpt_Alm_Orden_Entrada.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Reportes;

public partial class paginas_Almacen_Frm_Rpt_Alm_Orden_Entrada : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN         : Carga una configuracion de los controles del Formulario
        ///PARAMETROS          : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                  de los controles.
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Div_Busqueda.Visible = Estatus;
            Div_Orden_Compra.Visible = !Estatus;
            Btn_Imprimir.Visible = !Estatus;
            Grid_Ordenes_Entrada.SelectedIndex = -1;

            if (Estatus)
            {
                Cls_Cat_Dependencias_Negocio Dependencias = new Cls_Cat_Dependencias_Negocio();
                DataTable Dt_Dependencias = new DataTable();
                Dt_Dependencias = Dependencias.Consulta_Dependencias();
                Cmb_Dependencia.DataSource = Dt_Dependencias;
                Cmb_Dependencia.DataValueField = "DEPENDENCIA_ID";
                Cmb_Dependencia.DataTextField = "CLAVE_NOMBRE";
                Cmb_Dependencia.DataBind();
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado
        ///DESCRIPCIÓN         : Llena el Listado con una consulta que puede o no
        ///                      tener Filtros.
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            Cls_Rpt_Alm_Orden_Entrada_Negocio Negocio = new Cls_Rpt_Alm_Orden_Entrada_Negocio();

            if (Chk_Dependencia_B.Checked)
            {
                Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && !String.IsNullOrEmpty(Txt_Fecha_Fin.Text))
            {
                Negocio.P_Fecha_Inicio = DateTime.Parse(Txt_Fecha_Inicio.Text);
                Negocio.P_Fecha_Fin = DateTime.Parse(Txt_Fecha_Fin.Text);
            }
            if (!String.IsNullOrEmpty(Txt_No_OrdenE_Buscar.Text))
            {
                Negocio.P_Orden_Entrada = Txt_No_OrdenE_Buscar.Text;
            }
            if (!String.IsNullOrEmpty(Txt_OrdenC_Buscar.Text))
            {
                Negocio.P_Orden_Compra = Txt_OrdenC_Buscar.Text;
            }
            if (!String.IsNullOrEmpty(Txt_Req_Buscar.Text))
            {
                Negocio.P_Requisicion_ID = Txt_Req_Buscar.Text;
            }
            Grid_Ordenes_Entrada.DataSource = Negocio.Consultar_Ordenes_Entrada();
            Grid_Ordenes_Entrada.PageIndex = Pagina;
            Grid_Ordenes_Entrada.DataBind();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Txt_Comentario.Text = String.Empty;
            Txt_Estatus.Text = String.Empty;
            Txt_Fecha_Fin.Text = String.Empty;
            Txt_Fecha_Inicio.Text = String.Empty;
            Txt_No_OrdenE_Buscar.Text = String.Empty;
            Txt_Orden_Compra.Text = String.Empty;
            Txt_Orden_Entrada.Text = String.Empty;
            Txt_OrdenC_Buscar.Text = String.Empty;
            Txt_Recibio.Text = String.Empty;
            Txt_Req_Buscar.Text = String.Empty;
            Txt_Requisicion.Text = String.Empty;
            Txt_Solicito.Text = String.Empty;
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, EventArgs e)
        {
            String Mensaje = "";

            if ((!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && String.IsNullOrEmpty(Txt_Fecha_Fin.Text)) ||
                (String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && !String.IsNullOrEmpty(Txt_Fecha_Fin.Text)))
            {
                Mensaje += "Es necesario:<br/> + Ingresar ambas fechas.";
            }

            if (String.IsNullOrEmpty(Mensaje))
            {
                Lbl_Informacion.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
                Llenar_Grid_Listado(0);
            }
            else
            {
                Lbl_Informacion.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Click
        ///DESCRIPCIÓN         : Muestra los resultado en Excel.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Rpt_Alm_Orden_Entrada_Negocio Negocio = new Cls_Rpt_Alm_Orden_Entrada_Negocio();
                Negocio.P_Orden_Entrada = Txt_Orden_Entrada.Text.Trim();
                DataSet Ds_OE = Negocio.Detalles_Orden_Entrada();

                String Reporte_Crystal = "../Rpt/Almacen/Rpt_Alm_Orden_Entrada.rpt"; //variable para el nombre del reporte de crystal
                String Nombre_Reporte = "Orden_Entrada_" + Negocio.P_Orden_Entrada + ".pdf"; //variable para el nombre del reporte de PDF
                Cls_Reportes Reportes = new Cls_Reportes(); //variable para la generacion de lols reportes

                Reportes.Generar_Reporte(ref Ds_OE, Reporte_Crystal, Nombre_Reporte, "PDF");
                Mostrar_Reporte(Nombre_Reporte, "PDF");
            }
            catch (Exception Ex)
            {
                Lbl_Informacion.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Entrada_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja la paginación del GridView
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Entrada_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Llenar_Grid_Listado(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Informacion.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Entrada_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Entrada_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Ordenes_Entrada.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    String Orden_Entrada = HttpUtility.HtmlDecode(Grid_Ordenes_Entrada.SelectedRow.Cells[1].Text.Trim());
                    Cls_Rpt_Alm_Orden_Entrada_Negocio Negocio = new Cls_Rpt_Alm_Orden_Entrada_Negocio();
                    Negocio.P_Orden_Entrada = Orden_Entrada;

                    DataSet Ds_Orden_Compra = Negocio.Detalles_Orden_Entrada();

                    if (Ds_Orden_Compra.Tables["Cabecera"].Rows.Count > 0)
                    {
                        Txt_Orden_Entrada.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["NO_ENTRADA"].ToString();
                        Txt_Orden_Compra.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["NO_ORDEN_COMPRA"].ToString();
                        Txt_Requisicion.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["FOLIO"].ToString();
                        Txt_Estatus.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["ESTATUS"].ToString();
                        Txt_Solicito.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["SOLICITO"].ToString();
                        Txt_Recibio.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["RECIBIO"].ToString();
                        Txt_Comentario.Text = Ds_Orden_Compra.Tables["Cabecera"].Rows[0]["COMENTARIO"].ToString();
                    }

                    Grid_Productos.DataSource = Ds_Orden_Compra.Tables["Detalles"];
                    Grid_Productos.DataBind();

                    Configuracion_Formulario(false);

                    Btn_Salir.AlternateText = "Cancelar";
                }
            }
            catch (Exception Ex)
            {
                Lbl_Informacion.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Reportes

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      16-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                if (Formato == "PDF")
                {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                else if (Formato == "Excel")
                {
                    String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

}
