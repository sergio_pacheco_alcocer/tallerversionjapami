﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" 
CodeFile="Frm_Ope_Alm_Salidas_Sin_Registro.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Salidas_Sin_Registro" %>


<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    </asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
<!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Principal" runat="server" style="height: 900px">
            <table width="97%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo">
                            Órdenes de Salida Sin Registro
                        </td>
                    </tr>
                    <!--Bloque del mensaje de error-->
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" style="font-size: 12px; color: Red; font-family: Tahoma; text-align: left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" Width="100%" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <!--Bloque de la busqueda-->
                    <tr class="barra_busqueda">
                        <td style="width: 20%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                CssClass="Img_Button" TabIndex="20"
                                                            
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click"/>                            
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                AlternateText="Salir" ToolTip="Salir" onclick="Btn_Salir_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                             <table width="99.3%" class="estilo_fuente">
                                <tr>    
                                    <td style="width:25%">    
                                        No. Salida
                                    </td>
                                    <td style="width:25%">
                                        <asp:TextBox ID="Txt_No_Salida" runat="server"></asp:TextBox>
                                    </td> 
                                    <td style="width:25%">
                                    
                                    </td> 
                                    <td style="width:25%">
                                    
                                    </td>  
                                </tr>
                                <tr>
                                    <td>
                                        Unidad Responsable
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_UR" runat="server" Width="100%" TabIndex="1"
                                            ontextchanged="Txt_UR_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                    </td> 
                                    <td colspan="2">
                                        <asp:DropDownList ID="Cmb_UR" Width="100%" runat="server" AutoPostBack="true"
                                            onselectedindexchanged="Cmb_UR_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Partida Presupuestal
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Partida" runat="server" Width="100%" TabIndex="2"
                                            ontextchanged="Txt_Partida_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                    </td> 
                                    <td colspan="2">
                                        <asp:DropDownList ID="Cmb_Partida" Width="100%" runat="server" AutoPostBack="true"
                                            onselectedindexchanged="Cmb_Partida_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                 <tr>
                                    <td>
                                        Empleado Solicito
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Empleado" runat="server" Width="100%" AutoPostBack="true" TabIndex="3"
                                            ontextchanged="Txt_Empleado_TextChanged"></asp:TextBox>
                                    </td> 
                                    <td colspan="2">
                                        <asp:DropDownList ID="Cmb_Empleado" Width="100%" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Producto
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Producto" runat="server" Width="100%" AutoPostBack="true" TabIndex="4"
                                            ontextchanged="Txt_Producto_TextChanged" ></asp:TextBox>
                                    </td> 
                                    <td>
                                        <asp:DropDownList ID="Cmb_Producto" Width="100%" runat="server" AutoPostBack="true"
                                            onselectedindexchanged="Cmb_Producto_SelectedIndexChanged">
                                        </asp:DropDownList>
                                    </td>
                                    <td>
                                        <asp:TextBox ID="Txt_Cantidad" runat="server" Width="70%" AutoPostBack="false" TabIndex="5"
                                            ontextchanged="Txt_Cantidad_TextChanged"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cantidad" runat="server" 
                                        FilterType="Custom, Numbers" TargetControlID="Txt_Cantidad" ValidChars=".," />
                                        <asp:ImageButton ID="Btn_Agregar_Producto" runat="server"
                                        ToolTip="Agregar producto" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                            onclick="Btn_Agregar_Producto_Click" />
                                    </td> 
                                </tr>
                                <tr>
                                    <td colspan="4" align="center">
                                        <div style="font-family: arial, Helvetica, sans-serif; color: #0000FF;">
                                            ### Existencia:
                                            <asp:Label ID="Lbl_Existencia" runat="server" Text=""></asp:Label>
                                            &nbsp;&nbsp;&nbsp;&nbsp; Disponible:
                                            <asp:Label ID="Lbl_Disponible" runat="server" Text=""></asp:Label>
                                            ###
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                    <asp:GridView ID="Grid_Productos" runat="server" AutoGenerateColumns="False"
                                    CssClass="GridView_1" PageSize="5" GridLines="None" Width="100%" 
                                    DataKeyNames="Producto_ID" Visible="true"
                                    Style="white-space: normal">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Eliminar_Producto" ToolTip="Eliminar" OnClick="Btn_Eliminar_Producto_Click" CommandArgument="<%# ((GridViewRow) Container).RowIndex %>"
                                                runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" Height="16px" Width="16px"/>                                            
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        
                                        <asp:BoundField DataField="Producto_ID" HeaderText="Producto_ID"
                                            Visible="false">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Nombre" HeaderText="Producto"
                                            Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                        </asp:BoundField>                                                             
                                        <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" Visible="True">
                                            <FooterStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="6%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="UNIDAD" HeaderText="Unidad" Visible="True">
                                            <FooterStyle HorizontalAlign="Center" />
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="6%" Font-Size="X-Small" />
                                        </asp:BoundField>                                        

                                        <asp:BoundField DataField="Costo_Promedio" HeaderText="$ Costo" Visible="True"
                                            DataFormatString="{0:C}">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Importe" HeaderText="$ Importe" Visible="true">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Right" Width="0%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Monto_IVA" HeaderText="IVA" Visible="False">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="0%" Font-Size="X-Small" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Porcentaje_IVA" HeaderText="Porcentaje_IVA" Visible="False">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="0%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Monto_Total" HeaderText="$ Importe" Visible="false" DataFormatString="{0:C}">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        Observaciones:</td>
                                    <td colspan="2" rowspan="2">                                    
                                        <asp:TextBox ID="Txt_Observaciones" TextMode="MultiLine" runat="server" Width="99%"></asp:TextBox>
                                    </td>
                                    
                                    
                                    <td align="right">
                                        Subtotal 
                                        <asp:TextBox ID="Txt_Subtotal" runat="server" Style="text-align: right" ReadOnly="true" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    
                                    
                                    <td>
                                    
                                    </td>
                                    <td align="right">
                                        IVA 
                                        <asp:TextBox ID="Txt_IVA" runat="server" Style="text-align: right" ReadOnly="true" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    
                                    </td>
                                    <td>
                                    
                                    </td>
                                    <td>
                                    
                                    </td>
                                    <td align="right">
                                        Total 
                                        <asp:TextBox ID="Txt_Total" runat="server" Style="text-align: right" ReadOnly="true" Width="50%"></asp:TextBox>
                                    </td>
                                </tr>
                             </table>
                        </td>
                    </tr>
                    
             </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>