﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Salidas_S_Registro.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Requisiciones_Transitorias.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Globalization;


public partial class paginas_Almacen_Frm_Ope_Alm_Salidas_Sin_Registro : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region PAGE_LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Session["Activa"] = true;
            Configurar_Formulario("Inicio");
            Limpiar_Componentes();
            Habilitar_Componentes(false);
            Llenar_Combo_UR();
            Llenar_Combo_Partida();
            Llenar_Combo_Empleado();

        }

    }
    #endregion 
    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region METODOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_UR
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Llenar_Combo_UR()
    {
        Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
        if (Txt_UR.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Clave_UR = Txt_UR.Text.Trim();

        }
        DataTable Dt = Clase_Negocio.Consulta_UR();
        if (Dt.Rows.Count > 0)
        {
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_UR, Dt);
            Cmb_UR.SelectedIndex = 1;
        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Informacion.Text = "No se encontro el departamento";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Partida
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Llenar_Combo_Partida()
    {
        Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
        if (Txt_Partida.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Clave_Partida = Txt_Partida.Text.Trim();

        }
        DataTable Dt = Clase_Negocio.Consulta_Partida();
        if (Dt.Rows.Count > 0)
        {
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Partida, Dt);
            Cmb_Partida.SelectedIndex = 1;
        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Informacion.Text = "No se encontro la partida";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Partida
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Llenar_Combo_Empleado()
    {
        Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
        if (Txt_Empleado.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Empleado = Txt_Empleado.Text.Trim();

        }
        DataTable Dt = Clase_Negocio.Consulta_Empleado();
        if (Dt.Rows.Count > 0)
        {
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Empleado, Dt);
            Cmb_Empleado.SelectedIndex = 1;
        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Informacion.Text = "No se encontro el Empleado";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Partida
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Llenar_Combo_Producto()
    {
        Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
        if (Cmb_Partida.SelectedIndex != 0)
        {
            Clase_Negocio.P_Partida_ID = Cmb_Partida.SelectedValue;
        }
        if (Txt_Producto.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Producto = Txt_Producto.Text.Trim();
        }        
        DataTable Dt = Clase_Negocio.Consulta_Producto();
        if (Dt.Rows.Count > 0)
        {
            Session["Dt_Producto"] = Dt;
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Producto, Dt);
            Cmb_Producto.SelectedIndex = 1;
            int posicion = Cmb_Producto.SelectedIndex;
            Lbl_Disponible.Text = Dt.Rows[posicion - 1]["DISPONIBLE"].ToString();
            Lbl_Existencia.Text = Dt.Rows[posicion - 1]["EXISTENCIA"].ToString();
        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Informacion.Text = "No se encontro el producto";
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Limpiar_Componentes()
    {
        Txt_No_Salida.Text = "";
        Txt_UR.Text = "";
        Txt_Partida.Text = "";
        Txt_Empleado.Text = "";
        Txt_Producto.Text = "";
        Txt_Cantidad.Text = "";
        Txt_Observaciones.Text = "";
        Txt_Subtotal.Text = "0";
        Txt_IVA.Text = "0";
        Txt_Total.Text = "0";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    public void Habilitar_Componentes(bool estado)
    {
        Txt_No_Salida.ReadOnly = true;
        Txt_UR.Enabled = estado;
        Cmb_UR.Enabled = estado;
        Txt_Partida.Enabled = estado;
        Cmb_Partida.Enabled = estado;
        Txt_Empleado.Enabled = estado;
        Cmb_Empleado.Enabled = estado;
        Txt_Producto.Enabled = estado;
        Cmb_Producto.Enabled = estado;
        Txt_Cantidad.Enabled = estado;
        Txt_Observaciones.Enabled = estado;
        Grid_Productos.Enabled = estado;
        Btn_Agregar_Producto.Enabled = estado;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///´de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 30/MAYO/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Configurar_Formulario(String Estatus)
    {
        switch (Estatus)
        {
            case "Inicio":
                //Boton Modificar
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Nuevo.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                break;
            case "Nuevo":

                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Nuevo.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                break;
        }
    }

    public void Agregar_Producto()
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        //Creamos la session por primera ves
        DataTable Dt_Productos = new DataTable();
        if (double.Parse(Txt_Cantidad.Text.Trim()) <= double.Parse(Lbl_Disponible.Text.Trim()))
        {
            if (Session["Dt_Productos"] == null)
            {                
                Dt_Productos.Columns.Add("Producto_ID", typeof(System.String));                
                Dt_Productos.Columns.Add("Nombre", typeof(System.String));
                Dt_Productos.Columns.Add("Cantidad", typeof(System.String));
                Dt_Productos.Columns.Add("Unidad", typeof(System.String));
                Dt_Productos.Columns.Add("Costo_Promedio", typeof(System.String));
                Dt_Productos.Columns.Add("Importe", typeof(System.String));
                Dt_Productos.Columns.Add("Monto_IVA", typeof(System.String));
                Dt_Productos.Columns.Add("Porcentaje_IVA", typeof(System.String));
                Dt_Productos.Columns.Add("Monto_Total", typeof(System.String));
                Session["Dt_Productos"] = Dt_Productos;
            }
                Dt_Productos = (DataTable)Session["Dt_Productos"];
                //Llenamos el grid
                Grid_Productos.DataSource = (DataTable)Session["Dt_Productos"];
                Grid_Productos.DataBind();
                String Id = Cmb_Producto.SelectedValue;
                DataRow[] Filas;
                Filas = Dt_Productos.Select("Producto_ID='" + Id + "'");
                if (Filas.Length > 0)
                {
                    //Si se encontro algun coincidencia entre el grupo a agregar con alguno agregado anteriormente, se avisa
                    //al usuario que elemento ha agregar ya existe en la tabla de grupos.
                    Lbl_Informacion.Text += "+ No se puede agregar el Producto " + Id + " ya que este ya se ha agregado<br/>";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                else
                {
                    //Agregar producto
                    Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
                    //consultamos el Producto seleccionado
                    Clase_Negocio.P_Producto_ID = Cmb_Producto.SelectedValue;
                    DataTable Dt_Datos_Producto = Clase_Negocio.Consulta_Producto();
                    if (!(Dt_Datos_Producto == null))
                    {
                        if (Dt_Datos_Producto.Rows.Count > 0)
                        {
                            DataRow Fila_Nueva = Dt_Productos.NewRow();
                            Double Costo_Promedio = Double.Parse(Dt_Datos_Producto.Rows[0]["Costo_Promedio"].ToString());
                            Double Importe = Double.Parse(Dt_Datos_Producto.Rows[0]["Costo_Promedio"].ToString()) * Double.Parse(Txt_Cantidad.Text.Trim());
                            Double Porcentaje_IVA = 0.01 * Double.Parse(Dt_Datos_Producto.Rows[0]["Porcentaje_IVA"].ToString());
                            Double Monto_IVA = ((Costo_Promedio) * Porcentaje_IVA) * Double.Parse(Txt_Cantidad.Text.Trim());
                            //Asignamos los valores a la fila
                            Fila_Nueva["Producto_ID"] = Dt_Datos_Producto.Rows[0]["Producto_ID"].ToString();
                            Fila_Nueva["Nombre"] = Dt_Datos_Producto.Rows[0]["Nombre"].ToString();
                            Fila_Nueva["Cantidad"] = Txt_Cantidad.Text.Trim();
                            Fila_Nueva["Unidad"] = Dt_Datos_Producto.Rows[0]["Unidad"].ToString();
                            Fila_Nueva["Costo_Promedio"] = Costo_Promedio;
                            Fila_Nueva["Importe"] = Importe;
                            Fila_Nueva["Monto_IVA"] = Monto_IVA;
                            Fila_Nueva["Porcentaje_IVA"] = Porcentaje_IVA;
                            Fila_Nueva["Monto_Total"] = Monto_IVA + Importe;
                            Dt_Productos.Rows.Add(Fila_Nueva);
                            Dt_Productos.AcceptChanges();
                            Session["Dt_Productos"] = Dt_Productos;
                            Grid_Productos.DataSource = Dt_Productos;
                            Grid_Productos.DataBind();
                            Grid_Productos.Visible = true;
                            Txt_Subtotal.Text = (Double.Parse(Txt_Subtotal.Text.Trim()) + Importe).ToString();
                            Txt_IVA.Text = (Double.Parse(Txt_IVA.Text.Trim()) + Monto_IVA).ToString();
                            Txt_Total.Text = (Double.Parse(Txt_Total.Text.Trim()) + Monto_IVA + Importe).ToString();
                        }
                    }
                }//fin del else            

        }
        else
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Informacion.Text = "La cantidad a salir es mayor que la disponible";
        }
        
        Txt_Subtotal.Text = double.Parse( Txt_Subtotal.Text).ToString("#.##", CultureInfo.InvariantCulture);
        Txt_IVA.Text = double.Parse(Txt_IVA.Text).ToString("#.##", CultureInfo.InvariantCulture);
        Txt_Total.Text = double.Parse(Txt_Total.Text).ToString("#.##", CultureInfo.InvariantCulture);
    }

    public void Validar_Contenido()
    {
        if (Cmb_Empleado.SelectedIndex == 0)
        {
            Lbl_Informacion.Text = "Es necesario seleccionar el Empleado";
        }
        if (Cmb_Partida.SelectedIndex == 0)
        {
            Lbl_Informacion.Text = Lbl_Informacion.Text + "Es necesario seleccionar una partida";
        }
        if (Cmb_UR.SelectedIndex == 0)
        {
            Lbl_Informacion.Text =  Lbl_Informacion.Text + "Es necesario seleccionar un departamento";
        }
        if (Grid_Productos.Rows.Count <= 0)
        {
            Lbl_Informacion.Text = Lbl_Informacion.Text + "Es necesario agregar por lo menos un producto ";
        }
        if (Lbl_Informacion.Text.Trim() != String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
        }
        
        

    }


    ///*******************************************************************************
    /// NOMBRE DE LA CLASE:     Mostrar_Orden_Salida
    /// DESCRIPCION:            Método utilizado consultar la información utilizada para mostrar la orden de salida
    /// PARAMETROS :            
    /// CREO       :            Salvador Hernández Ramírez
    /// FECHA_CREO :            24/Junio/2011  
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************/
    private void Mostrar_Orden_Salida(Int64 No_Orden_Salida)
    {
        DataTable Dt_Cabecera = new DataTable(); // Contendrá los datos generales arrojados de la consulta
        DataTable Dt_Detalles = new DataTable(); // Contendrá los detalles arrojados de la consulta
        DataTable Dt_Cabecera_Completa = new DataTable();  // Contendra lso datos completos para la cabecera

        Double SubTotal_Req = 0;
        Double IVA_Req = 0;
        Double Total_Req = 0;


        try
        {
            Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio(); // Se crea el objeto de la clase de negocios
            Clase_Negocio.P_No_Orden_Salida = No_Orden_Salida;
            Clase_Negocio.P_Partida_ID = Cmb_Partida.SelectedValue;
            Clase_Negocio.P_Unidad_R_ID = Cmb_UR.SelectedValue;
            Clase_Negocio.P_Empleado_Recibio_ID = Cmb_Empleado.SelectedValue;
            Clase_Negocio.P_Empleado_Almacen_ID = Cls_Sessiones.Empleado_ID;

            Dt_Cabecera = Clase_Negocio.Consultar_Informacion_General_OS(); // Se consulta la información general de la Orden de Salida
            Dt_Detalles = Clase_Negocio.Consultar_Detalles_Orden_Salida();  // Se consultan los detalles de la Orden de salida

            if (Dt_Detalles.Rows.Count > 0)
            {
                for (int i = 0; i < Dt_Detalles.Rows.Count; i++)
                {
                    SubTotal_Req = SubTotal_Req + (Convert.ToDouble(Dt_Detalles.Rows[i]["SUBTOTAL"].ToString()));
                    IVA_Req = IVA_Req + (Convert.ToDouble(Dt_Detalles.Rows[i]["IVA"].ToString()));
                    Total_Req = Total_Req + (Convert.ToDouble(Dt_Detalles.Rows[i]["TOTAL"].ToString()));
                }
            }

            // Se  Crea una nueva tabla para agregar los detalles faltantes de la cabecera
            Dt_Cabecera_Completa.Columns.Add("NO_ORDEN_SALIDA");
            Dt_Cabecera_Completa.Columns.Add("UNIDAD_RESPONSABLE");
            Dt_Cabecera_Completa.Columns.Add("F_FINANCIAMIENTO");
            Dt_Cabecera_Completa.Columns.Add("PROGRAMA");
            Dt_Cabecera_Completa.Columns.Add("FOLIO");
            Dt_Cabecera_Completa.Columns.Add("FECHA_AUTORIZACION");
            Dt_Cabecera_Completa.Columns.Add("SUBTOTAL");
            Dt_Cabecera_Completa.Columns.Add("IVA");
            Dt_Cabecera_Completa.Columns.Add("TOTAL");
            Dt_Cabecera_Completa.Columns.Add("ENTREGO");
            Dt_Cabecera_Completa.Columns.Add("RECIBIO");
            Dt_Cabecera_Completa.Columns.Add("OBSERVACIONES");
            Dt_Cabecera_Completa.Columns.Add("FECHA_CREO");

            DataRow Registro = Dt_Cabecera_Completa.NewRow(); // Se crea un nuevo registro

            Registro["NO_ORDEN_SALIDA"] = Dt_Cabecera.Rows[0]["NO_ORDEN_SALIDA"].ToString().Trim();
            Registro["UNIDAD_RESPONSABLE"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["UNIDAD_RESPONSABLE"].ToString().Trim());
            Registro["F_FINANCIAMIENTO"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["F_FINANCIAMIENTO"].ToString().Trim());
            Registro["PROGRAMA"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["PROGRAMA"].ToString().Trim());
            Registro["FOLIO"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["FOLIO"].ToString().Trim());

            if (Dt_Cabecera.Rows[0]["FECHA_AUTORIZACION"].ToString().Trim() != "")
                Registro["FECHA_AUTORIZACION"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["FECHA_AUTORIZACION"].ToString().Trim());
            else
                Registro["FECHA_AUTORIZACION"] = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);

            Registro["SUBTOTAL"] = Convert.ToString(SubTotal_Req);
            Registro["IVA"] = Convert.ToString(IVA_Req);
            Registro["TOTAL"] = Convert.ToString(Total_Req);

            if (Dt_Cabecera.Rows[0]["ENTREGO"].ToString().Trim() != "")
                Registro["ENTREGO"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["ENTREGO"].ToString().Trim());
            else
                Registro["ENTREGO"] = "";

            if (Dt_Cabecera.Rows[0]["RECIBIO"].ToString().Trim() != "")
                Registro["RECIBIO"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["RECIBIO"].ToString().Trim());
            else
                Registro["RECIBIO"] = "";

            //Verificar si hay observaciones
            if (String.IsNullOrEmpty(Txt_Observaciones.Text.Trim()) == false)
            {
                Registro["OBSERVACIONES"] = Txt_Observaciones.Text.Trim();
            }
            Registro["FECHA_CREO"] = HttpUtility.HtmlDecode(Dt_Cabecera.Rows[0]["FECHA_CREO"].ToString().Trim());
            Dt_Cabecera_Completa.Rows.InsertAt(Registro, 0); // Se Inserta el Registro

            String Formato = "PDF";
            Ds_Alm_Com_Orden_Salida Ds_Orden_Salida = new Ds_Alm_Com_Orden_Salida();
            String Nombre_Reporte_Crystal = "Rpt_Alm_Com_Orden_Salida.rpt";

            Generar_Reporte(Dt_Cabecera_Completa, Dt_Detalles, Ds_Orden_Salida, Nombre_Reporte_Crystal, Formato);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Carga el data set físico con el cual se genera el Reporte especificado
    ///PARAMETROS:           1.- Dt_Cabecera.- Contiene la informacion de la consulta a la base de datos
    ///                      2.- Dt_Detalles.- Contiene los detalles de la consulta a la BD
    ///                      2.- Ds_Orden_Salida, Objeto que contiene la instancia del DataSet fisico del Reporte a generar
    ///                      3.- Nombre_Reporte_Crystal, contiene el nombre del Reporte  que se creó en Crystal Report
    ///                      4.- Formato, Es el formato con el que se va a generar el reporte, ya sea PDF o Excel
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           24/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataTable Dt_Cabecera, DataTable Dt_Detalles, DataSet Ds_Orden_Salida, String Nombre_Reporte_Crystal, String Formato)
    {
        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;

        try
        {
            // Se llena la tabla Cabecera del DataSet
            Renglon = Dt_Cabecera.Rows[0];
            Ds_Orden_Salida.Tables[0].ImportRow(Renglon);

            // Se llena la tabla Detalles del DataSet
            for (int Cont_Elementos = 0; Cont_Elementos < Dt_Detalles.Rows.Count; Cont_Elementos++)
            {
                Renglon = Dt_Detalles.Rows[Cont_Elementos]; //Instanciar renglon e importarlo
                Ds_Orden_Salida.Tables[1].ImportRow(Renglon);
            }

            // Ruta donde se encuentra el reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Almacen/" + Nombre_Reporte_Crystal;

            // Se crea el nombre del reporte
            String Nombre_Reporte = "Rpt_Orden_Salida" + Cls_Sessiones.No_Empleado + "_" + Convert.ToString(DateTime.Now.ToString("yyyy'-'MM'-'dd'_t'HH'-'mm'-'ss"));

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = Nombre_Reporte + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = Nombre_Reporte + ".xls";  // Es el nombre del repote en Excel que se va a generar

            //Cls_Reportes Reportes = new Cls_Reportes();
            Generar_Reporte(ref Ds_Orden_Salida, Nombre_Reporte_Crystal, Nombre_Reporte_Generar);
            //Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
        }
    }

    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 
        try
        {

            Ruta = @Server.MapPath("../Rpt/Almacen/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {

                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region EVENTOS

    
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        switch (Btn_Nuevo.ToolTip)
        {
            case "Nuevo":
                Lbl_Informacion.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
                Configurar_Formulario("Nuevo");
                Limpiar_Componentes();
                Habilitar_Componentes(true);
                Session["Dt_Productos"] = null;
                break;
            case "Guardar":

                Validar_Contenido();
                try
                {
                    if (Lbl_Informacion.Text.Trim() == String.Empty)
                    {
                        Cls_Ope_Alm_Salidas_Sin_Registro_Negocio Clase_Negocio = new Cls_Ope_Alm_Salidas_Sin_Registro_Negocio();
                        Clase_Negocio.P_Empleado_Recibio_ID = Cmb_Empleado.SelectedValue;
                        Clase_Negocio.P_Partida_ID = Cmb_Partida.SelectedValue;
                        Clase_Negocio.P_Unidad_R_ID = Cmb_UR.SelectedValue;
                        Clase_Negocio.P_Dt_Productos = (DataTable)Session["Dt_Productos"];
                        Clase_Negocio.P_Subtotal = Txt_Subtotal.Text.Trim();
                        Clase_Negocio.P_IVA = Txt_IVA.Text.Trim();
                        Clase_Negocio.P_Total = Txt_Total.Text.Trim();
                        if (Txt_Observaciones.Text != String.Empty)
                        {
                            Clase_Negocio.P_Observaciones = Txt_Observaciones.Text.Trim();
                        }
                        Int64 No_Orden_Salida = Clase_Negocio.Insertar_Salida();
                        if (No_Orden_Salida != null)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Ordenes de Salida", "alert('Se creo la Orden de Salida" + No_Orden_Salida + " ');", true);
                            Mostrar_Orden_Salida(No_Orden_Salida);
                            Configurar_Formulario("Inicio");
                            Limpiar_Componentes();
                            Habilitar_Componentes(false);
                            Llenar_Combo_UR();
                            Llenar_Combo_Partida();
                            Llenar_Combo_Empleado();

                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Ordenes de Salida", "alert('No se pudo crear la Orden de Salida');", true);
                        }
                    }

                }
                catch(Exception ex)
                {
                    if (ex.Message == "No se tiene cuenta contable para la partida")
                    {
                        Lbl_Informacion.Text = " No se tiene cuenta contable para la partida " + Cmb_Partida.SelectedItem.Text;
                        Lbl_Informacion.Visible = true;
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                break;
        }
    }
    protected void Txt_UR_TextChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        Llenar_Combo_UR();
        Cmb_Partida.DataSource = new DataTable();
        Cmb_Partida.DataBind();
        Cmb_Producto.DataSource = new DataTable();
        Cmb_Producto.DataBind();
        Txt_Producto.Text = "";
        Txt_Partida.Text = "";
        Txt_Empleado.Text = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Session["Dt_Producto"] = new DataTable();
        Txt_Subtotal.Text = "0";
        Txt_Total.Text = "0";
        Txt_IVA.Text = "0";
    }
    protected void Txt_Partida_TextChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        Llenar_Combo_Partida();       
        Cmb_Producto.DataSource = new DataTable();
        Cmb_Producto.DataBind();
        Txt_Producto.Text = "";
        Txt_Partida.Text = "";
        Txt_Empleado.Text = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Session["Dt_Producto"] = new DataTable();
        Txt_Subtotal.Text = "0";
        Txt_Total.Text = "0";
        Txt_IVA.Text = "0";
    }
    protected void Txt_Empleado_TextChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        Llenar_Combo_Empleado();
    }
    protected void Txt_Producto_TextChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        Llenar_Combo_Producto();
    }

    protected void Txt_Cantidad_TextChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
       
    }

    protected void Btn_Agregar_Producto_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        Agregar_Producto();
    }

    protected void Cmb_Producto_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt = (DataTable)Session["Dt_Producto"];
        int posicion = Cmb_Producto.SelectedIndex;
        Lbl_Disponible.Text = Dt.Rows[posicion-1]["DISPONIBLE"].ToString();
        Lbl_Existencia.Text = Dt.Rows[posicion-1]["EXISTENCIA"].ToString();
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
         Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                break;
            case "Cancelar":
                Configurar_Formulario("Inicio");
                Limpiar_Componentes();
                Habilitar_Componentes(false);
                Llenar_Combo_UR();
                Llenar_Combo_Partida();
                Llenar_Combo_Empleado();
                break;
        }
    }

    protected void Btn_Eliminar_Producto_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Int32 indice = int.Parse(((ImageButton)sender).CommandArgument);
            Grid_Productos.SelectedIndex = indice;
            String Producto_ID = Grid_Productos.SelectedDataKey["Producto_ID"].ToString().Trim();
            DataRow[] _DataRow = ((DataTable)Session["Dt_Productos"]).Select("Producto_ID = '" + Producto_ID + "'");
            if (_DataRow != null && _DataRow.Length > 0)
            {
                Double Monto = Double.Parse(_DataRow[0]["Importe"].ToString().Trim());
                Double Monto_Total = Double.Parse(_DataRow[0]["Monto_Total"].ToString().Trim());                
                Double Monto_IVA = Double.Parse(_DataRow[0]["Monto_IVA"].ToString().Trim());
                Txt_IVA.Text = (double.Parse(Txt_IVA.Text) - Monto_IVA).ToString();
                Txt_Subtotal.Text = (double.Parse(Txt_Subtotal.Text) - Monto).ToString();
                Txt_Total.Text = (double.Parse(Txt_Total.Text) - Monto_Total).ToString();
                ((DataTable)Session["Dt_Productos"]).Rows.Remove(_DataRow[0]);
                ((DataTable)Session["Dt_Productos"]).AcceptChanges();

                Grid_Productos.DataSource = (DataTable)Session["Dt_Productos"];
                Grid_Productos.DataBind();
                //Modificamos el total
                    
                    
            }
        }
            catch (Exception Ex)
            {
                
            }
    }
    #endregion  
   
    protected void Cmb_UR_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        //Limpiamos la parida y el producto
        Cmb_Partida.DataSource = new DataTable();
        Cmb_Partida.DataBind();
        Cmb_Producto.DataSource = new DataTable();
        Cmb_Producto.DataBind();
        Txt_Producto.Text = "";
        Txt_Partida.Text = "";
        Txt_Empleado.Text = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Session["Dt_Producto"] = new DataTable();
        Txt_Subtotal.Text = "0";
        Txt_Total.Text = "0";
        Txt_IVA.Text = "0";

    }
    protected void Cmb_Partida_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Informacion.Text = "";
        //Limpiamos la parida y el producto
        Txt_Producto.Text = "";
        Cmb_Producto.DataSource = new DataTable();
        Cmb_Producto.DataBind();
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Session["Dt_Producto"] = new DataTable();
        Txt_Subtotal.Text = "0";
        Txt_Total.Text = "0";
        Txt_IVA.Text = "0";
    }
}