﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Correo.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Ope_Alm_Fechas_Pago.Negocio;
using System.Text.RegularExpressions;

public partial class paginas_Compras_Frm_Ope_Alm_Fechas_Pago : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Grid_Cotizador = 2;
        private const int Const_Estado_Modificar = 3;

        private static DataTable Dt_Fechas_Pago = new DataTable();    
    #endregion

    #region Page Load / Init

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Limpiar_Controles();
                Cargar_Grid_Fechas_Pago();
                ViewState["SortDirection"] = "ASC";
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
        
    }

    #endregion
    
    #region Metodos

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Mensaje_Error
        ///DESCRIPCION : Muestra el error
        ///PARAMETROS  : P_Texto: texto de un TextBox
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 15/Enero/2012
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Mensaje_Error(String P_Mensaje)
        {
            Img_Error.Visible = true;
            Lbl_Error.Text += P_Mensaje + "</br>";
            Div_Contenedor_error.Visible = true;
        }
        private void Mensaje_Error()
        {
            Img_Error.Visible = false;
            Lbl_Error.Text = "";
        }

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Limpiar_Controles
        ///DESCRIPCION : Limpia los valores de los controles
        ///PARAMETROS  : 
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 30/Enero/2012
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Limpiar_Controles()
        {
            //Limpia los controles
            Txt_No.Text = "";
            Txt_Fecha_Limite_Recepcion.Text = "";
            Txt_Fecha_Pago.Text = "";
            Txt_Comentarios.Text = "";
            Txt_Busqueda.Text = "";

            //Limpia las sesiones
            Session.Remove("Dt_Fechas");
            //Limpiamos el grid
            Grid_Fechas.DataSource = null;
            Grid_Fechas.DataBind();
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Estado_Botones
        ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
        ///PARAMETROS: P_Estado: El estado de la pagina
        ///CREO: David herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Estado_Botones(int P_Estado)
        {
            switch (P_Estado)
            {
                case 0: //Estado inicial                    

                    Div_Listado_Cotizadores.Visible = true;
                    //Div_Datos_Cotizador.Visible = true;
                    Grid_Fechas_Pago.Visible = true;
                    Grid_Fechas_Pago.Enabled = true;
                    Grid_Fechas_Pago.SelectedIndex = (-1);

                    Grid_Fechas.Visible = false;
                    Grid_Fechas.SelectedIndex = (-1);

                    Btn_Fecha_Limite_Recepcion.Enabled = false;
                    Btn_Fecha_Pago.Enabled = false;
                    Txt_Comentarios.Visible = true;
                    Lbl_Comentarios.Visible = true;
                    Txt_Comentarios.Enabled = false;
                    Txt_Comentarios.Text = "";
                    Btn_Agregar.Enabled = false;
                    Txt_Busqueda.Enabled = true;
                    Btn_Busqueda.Enabled = true;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Inicio";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Btn_Modificar.Enabled = false;
                    Btn_Eliminar.Enabled = false;
                    Btn_Eliminar.Visible = true;
                    Btn_Nuevo.Enabled = true;
                    Btn_Salir.Enabled = true;

                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;

                    Configuracion_Acceso("Frm_Ope_Alm_Fechas_Pago.aspx");
                    break;

                case 1: //Nuevo
                    Div_Listado_Cotizadores.Visible = true;
                    //Div_Datos_Cotizador.Visible = true;                    
                    Grid_Fechas.Visible = true;
                    Grid_Fechas.SelectedIndex = (-1);                    
                    Grid_Fechas_Pago.Visible = true;
                    Grid_Fechas_Pago.SelectedIndex = (-1);
                    Grid_Fechas_Pago.Enabled = false;
                    Btn_Fecha_Limite_Recepcion.Enabled = true;
                    Btn_Fecha_Pago.Enabled = true;
                    Txt_Comentarios.Visible = true;
                    Txt_Comentarios.Text = "";
                    Lbl_Comentarios.Visible = true;
                    Btn_Agregar.Enabled = true;
                    Txt_Busqueda.Enabled = false;
                    Btn_Busqueda.Enabled = false;
                    Btn_Modificar.Visible = false;                                                            
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";
                    Txt_Fecha_Limite_Recepcion.Text = "";
                    Txt_Fecha_Pago.Text = "";
                    Txt_Comentarios.Enabled = true;
                    Btn_Eliminar.Visible = false;
                    break;

                case 2: //Grid Cotizador
                    Div_Listado_Cotizadores.Visible = true;
                    //Div_Datos_Cotizador.Visible = true;
                    Grid_Fechas_Pago.Enabled = true;
                    Grid_Fechas.Visible = false;

                    Btn_Modificar.Visible = true;
                    Btn_Modificar.Enabled = true;
                    Btn_Eliminar.Enabled = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Nuevo.Visible = true;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Listado";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Listado";
                    Txt_Comentarios.Visible = true;
                    Lbl_Comentarios.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Txt_Comentarios.Enabled = false;
                    break;

                case 3: //Modificar

                    Div_Listado_Cotizadores.Visible = true;
                    Grid_Fechas.Visible = false;
                    Grid_Fechas_Pago.Enabled = false;
                    //Div_Datos_Cotizador.Visible = true;

                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Fecha_Limite_Recepcion.Enabled = true;
                    Btn_Fecha_Pago.Enabled = true;
                    Txt_Comentarios.Visible = true;
                    Lbl_Comentarios.Visible = true;
                    Btn_Agregar.Enabled = false;
                    Txt_Busqueda.Enabled = false;
                    Btn_Busqueda.Enabled = false;

                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_Nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Txt_Comentarios.Enabled = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Eliminar.Enabled = true;
                    break;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar_Fechas
        ///DESCRIPCIÓN: se actualizan los datos de la fecha seleccionado
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************

        private void Modificar_Fechas(Boolean Bln_Nuevo_Correo)
        {
            Div_Contenedor_error.Visible = false;
            Lbl_Error.Text = "";
            try
            {
                Cls_Ope_Alm_Fechas_Pago_Negocio Negocio = new Cls_Ope_Alm_Fechas_Pago_Negocio();                                             
                if (Div_Contenedor_error.Visible == false)
                {

                    Negocio.P_Anio_No_Fecha_Pago = Txt_No.Text;
                    Negocio.P_Fecha_Recepcion = Txt_Fecha_Limite_Recepcion.Text;
                    Negocio.P_Fecha_Pago = Txt_Fecha_Pago.Text;
                    Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();

                    Negocio.Actualizar_Fecha();
                    if (!Bln_Nuevo_Correo)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Fechas Pago", "alert('La Modificacion fue Exitosa');", true);
                        Estado_Botones(Const_Estado_Inicial);
                        Cargar_Grid_Fechas_Pago();
                        Limpiar_Controles();
                    }
                }//Fin del if Div_Contenedor_error.Visible == false
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }//fin de Modificar  

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
        ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
        ///PARAMETROS: Page_Index: Numero de pagina del grid
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Cargar_Grid_Fechas_Pago()
        {
            try
            {
                Cls_Ope_Alm_Fechas_Pago_Negocio Negocio = new Cls_Ope_Alm_Fechas_Pago_Negocio();
                
                //Por si busca por año
                if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
                    Negocio.P_Anio_No_Fecha_Pago = Txt_Busqueda.Text;

                //Realizamos la consulta
                Dt_Fechas_Pago = Negocio.Consultar_Fechas_Pago();
                Session["Dt_Fechas_Pago"] = Dt_Fechas_Pago;
                //Asignamos los datos
                Grid_Fechas_Pago.DataSource = Dt_Fechas_Pago;
                Grid_Fechas_Pago.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Agregar_Fecha
        /// DESCRIPCION:            Método utilizado para agregar la fecha 
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            31/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public void Agregar_Fecha()
        {            
            try
            {                
                DataTable Dt_Fechas = (DataTable)Grid_Fechas.DataSource;
                //Validamos las fechas
                String Validar = String.Empty;
                //String Validar = Validar_Fechas(Txt_Fecha_Limite_Recepcion.Text, Txt_Fecha_Pago.Text);
                //Validamos si ya tiene datos la session
                if (Session["Dt_Fechas"] == null)
                {
                    Dt_Fechas = new DataTable();
                    Dt_Fechas.Columns.Add("FECHA_LIMITE_RECEPCION", Type.GetType("System.String"));
                    Dt_Fechas.Columns.Add("FECHA_PAGO", Type.GetType("System.String"));
                    Dt_Fechas.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                    Dt_Fechas.Columns.Add("RENGLON", Type.GetType("System.Int64"));
                }
                else
                {
                    Dt_Fechas = (DataTable)Session["Dt_Fechas"];
                }

                if (String.IsNullOrEmpty(Validar))
                {
                    DataRow Fila = Dt_Fechas.NewRow();
                    Fila["FECHA_LIMITE_RECEPCION"] = Txt_Fecha_Limite_Recepcion.Text;
                    Fila["FECHA_PAGO"] = Txt_Fecha_Pago.Text;
                    Fila["COMENTARIOS"] = Txt_Comentarios.Text.Trim();
                    Fila["RENGLON"] = String.Format("{0:yyyyMMddHHmmss}", DateTime.Now);

                    Dt_Fechas.Rows.Add(Fila);
                    Grid_Fechas.DataSource = Dt_Fechas;                    
                    Grid_Fechas.DataBind();                    
                    Grid_Fechas.Visible = true;
                    Session["Dt_Fechas"] = Dt_Fechas;
                }
                else
                {
                    Lbl_Error.Text = Validar;
                    Div_Contenedor_error.Visible = true;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al agregar al grid las fechas. Error: [" + ex.Message + "]");

            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Quitar_Fecha
        /// DESCRIPCION:            Método utilizado quitar del grid la fecha
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            31/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        public void Quitar_Fecha(int Row)
        {
            try
            {
                if (Grid_Fechas.Rows.Count > 0)
                {
                    if (Session["Dt_Fechas"] != null)
                    {
                        DataTable Tabla = (DataTable)Session["Dt_Fechas"];
                        Tabla.Rows.RemoveAt(Row);
                        Session["Dt_Fechas"] = Tabla;
                        Grid_Fechas.SelectedIndex = (-1);
                        Llenar_Grid_Fechas(Grid_Fechas.PageIndex, Tabla);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al quitar las fechas. Error: [" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Fechas
        ///DESCRIPCIÓN:          Llena la tabla de Fechas
        ///PROPIEDADES:     
        ///                      1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
        ///                      2.  Tabla.  Tabla que se va a cargar en el Grid.    
        ///CREO       :            David Herrera Rincon
        ///FECHA_CREO :            31/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Fechas(Int32 Pagina, DataTable Tabla)
        {            
            Grid_Fechas.DataSource = Tabla;
            Grid_Fechas.PageIndex = Pagina;
            Grid_Fechas.DataBind();            
            Session["Dt_Fechas"] = Tabla;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Fechas
        ///DESCRIPCIÓN: Metodo que valida que las cajas tengan contenido. 
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public String Validar_Fechas(String Fecha_Limite, String Fecha_Pago)
        {
            DataTable Dt_Temp = (DataTable)Session["Dt_Fechas"];

            try
            {
                //Validamos que la fecha pago sea mayor
                if (DateTime.Parse(Fecha_Limite) < DateTime.Parse(Fecha_Pago))
                {
                    //Validamos que la sesision contenga datos
                    if ((DataTable)Session["Dt_Fechas"] != null)
                    {
                        if (Dt_Temp.Rows.Count > 0)
                        {
                            if ((Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_LIMITE_RECEPCION"].ToString().Equals(Fecha_Limite))
                                || (Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_PAGO"].ToString().Equals(Fecha_Limite)))
                            {
                                return "La fecha ya existe";
                            }
                            else 
                            {
                                if ((DateTime.Parse(Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_LIMITE_RECEPCION"].ToString()) > DateTime.Parse(Fecha_Limite))
                                    || (DateTime.Parse(Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_PAGO"].ToString()) > DateTime.Parse(Fecha_Limite)))
                                {
                                    return "La fecha debe ser mayor a las anteriores";
                                }
                                else
                                {
                                    if ((Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_LIMITE_RECEPCION"].ToString().Equals(Fecha_Pago))
                                    || (Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_PAGO"].ToString().Equals(Fecha_Pago)))
                                    {
                                        return "La fecha ya existe";
                                    }
                                    else 
                                    {
                                        if ((DateTime.Parse(Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_LIMITE_RECEPCION"].ToString()) > DateTime.Parse(Fecha_Pago))
                                            || (DateTime.Parse(Dt_Temp.Rows[Dt_Temp.Rows.Count - 1]["FECHA_PAGO"].ToString()) > DateTime.Parse(Fecha_Pago)))
                                        {
                                            return "La fecha debe ser mayor a las anteriores";
                                        }
                                        else { return ""; }
                                    }
                                }
                            }
                        }
                        else { return ""; }
                    }
                    else { return ""; }
                }
                else {return "La fecha pago no puede ser menor a la fecha limite recepcion";}
            }
            catch (Exception ex)
            {
                throw new Exception("Error al validar las fechas. Error: [" + ex.Message + "]");
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Ope_Alm_Fechas_Pago_Negocio Negocio = new Cls_Ope_Alm_Fechas_Pago_Negocio();

                if (Btn_Nuevo.AlternateText == "Nuevo")
                {                    
                    Estado_Botones(Const_Estado_Nuevo);
                }
                else if (Btn_Nuevo.AlternateText == "Dar de Alta")
                {
                    Div_Contenedor_error.Visible = false;
                    //Si pasa todas las Validaciones damos de alta las fechas
                    if (Div_Contenedor_error.Visible == false)
                    {
                        try
                        {
                            //Validamos que tenga datos el grid                            
                            if (Txt_Fecha_Pago.Text.Trim() != String.Empty && Txt_Fecha_Limite_Recepcion.Text.Trim() != String.Empty)
                            {                               
                                //Asignamos los valores
                                Negocio.P_Anio_No_Fecha_Pago = Convert.ToDateTime( Txt_Fecha_Pago.Text.Trim()).Year.ToString();
                                Negocio.P_Fecha_Recepcion = Txt_Fecha_Limite_Recepcion.Text.Trim();
                                Negocio.P_Fecha_Pago = Txt_Fecha_Pago.Text.Trim();
                                Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
                                //Agregamos el registro
                                Negocio.Agregar_Fechas();
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Fechas Pago", "alert('El alta fue Exitosa');", true);
                                Estado_Botones(Const_Estado_Inicial);
                                Cargar_Grid_Fechas_Pago();
                                Limpiar_Controles();
                            }
                            else
                            {
                                Mensaje_Error("Debe agregar fechas.");
                            }
                        }
                        catch
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Fechas Pago", "alert('El alta no fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid_Fechas_Pago();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Evento del Boton Modificar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Boolean Modificar=true;
                String Mensaje = "";
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    if (DateTime.Parse(Txt_Fecha_Pago.Text) >= DateTime.Parse(Txt_Fecha_Limite_Recepcion.Text))
                    {   
                        Modificar_Fechas(false);                        
                    }
                    else
                    {
                        Mensaje_Error("La fecha pago no puede ser menor a la fecha limite recepcion.");
                    }                                       
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Salir_Click
        ///DESCRIPCION:             Boton para SALIR
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO:             15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Salir.AlternateText == "Inicio")
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
                else
                {
                    Estado_Botones(Const_Estado_Inicial);
                    Limpiar_Controles();
                    Cargar_Grid_Fechas_Pago();
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA CLASE:     Btn_Agregar_Click
        /// DESCRIPCION:            Evento utilizado para agregar las fechas
        /// PARAMETROS :            
        /// CREO       :            David Herrera Rincon
        /// FECHA_CREO :            30/Enero/2013 
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************/
        protected void Btn_Agregar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Ope_Alm_Fechas_Pago_Negocio Negocio = new Cls_Ope_Alm_Fechas_Pago_Negocio();
                DataTable Dt_Ultimo_Agregado = Negocio.Consultar_Ultimo();
                //Verificar si las fechas son validas
                if (DateTime.Parse(Txt_Fecha_Pago.Text.Trim()) > DateTime.Parse(Txt_Fecha_Limite_Recepcion.Text.Trim()))
                {
                    Agregar_Fecha();
                }
                else
                {
                    Mensaje_Error("La fecha de pago debe ser mayor a la fecha de recepcion");
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }
        protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
        {
            try{
                Cargar_Grid_Fechas_Pago();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }

        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            //Declaracion de variables
            Cls_Ope_Alm_Fechas_Pago_Negocio Fechas_Pago_Negocio = new Cls_Ope_Alm_Fechas_Pago_Negocio(); //Variable para la capa de negocio

            try
            {
                //Eliminar el elemento seleccionado
                Fechas_Pago_Negocio.P_Anio_No_Fecha_Pago = Txt_No.Text;
                Fechas_Pago_Negocio.Eliminar_Fecha();

                //Estado inicial
                Estado_Botones(0);
                Cargar_Grid_Fechas_Pago();
            }
            catch (Exception ex)
            {
                Mensaje_Error(ex.Message);
            }
        }
    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Correo_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Fechas_Pago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Estado_Botones(Const_Grid_Cotizador);

                Txt_No.Text = Grid_Fechas_Pago.SelectedRow.Cells[1].Text;
                Txt_Fecha_Limite_Recepcion.Text = Grid_Fechas_Pago.SelectedRow.Cells[2].Text;
                Txt_Fecha_Pago.Text = Grid_Fechas_Pago.SelectedRow.Cells[3].Text;
                if (Grid_Fechas_Pago.SelectedRow.Cells[4].Text.Trim() != String.Empty && Grid_Fechas_Pago.SelectedRow.Cells[4].Text.Trim() !="&nbsp;") 
                    Txt_Comentarios.Text = Grid_Fechas_Pago.SelectedRow.Cells[4].Text.Trim();
                else
                    Txt_Comentarios.Text = "";
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Fechas_Pago_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para cargar los datos de la pagina seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Fechas_Pago_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Fechas_Pago.DataSource = ((DataTable)Session["Dt_Fechas_Pago"]);
                Grid_Fechas_Pago.PageIndex = e.NewPageIndex;
                Grid_Fechas_Pago.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Fechas_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para manejar el renglon seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Fechas_SelectedIndexChanged(object sender, EventArgs e)
        {
            Quitar_Fecha(Grid_Fechas.SelectedIndex);
        }


        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Correo_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Fechas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Fechas.DataSource = ((DataTable)Session["Dt_Fechas"]);
                Grid_Fechas.PageIndex = e.NewPageIndex;
                Grid_Fechas.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        protected void Grid_Fechas_Pago_Sorting(object sender, GridViewSortEventArgs e)
        {
            //Declaracion de variables
            DataTable Dt_Fechas_Pagos_Local = new DataTable(); //tabla para los datos
            DataView Dv_Fechas_Pagos; //Dataview para la rodenacion de los datos
            string orden = string.Empty; //Variable apra el ordenamiento

            try
            {
                //Llenar el grid
                Cargar_Grid_Fechas_Pago();

                //Obtener el datatable
                Dt_Fechas_Pagos_Local = (Grid_Fechas_Pago.DataSource as DataTable);

                //obtener el dataview
                Dv_Fechas_Pagos = new DataView(Dt_Fechas_Pagos_Local);

                //Obtener el orden
                orden = ViewState["SortDirection"].ToString();

                //Verificar el orden
                if (orden.Equals("ASC"))
                {
                    ViewState["SortDirection"] = "DESC";
                    Dv_Fechas_Pagos.Sort = e.SortExpression + " DESC";
                }
                else
                {
                    ViewState["SortDirection"] = "ASC";
                    Dv_Fechas_Pagos.Sort = e.SortExpression + " ASC";
                }

                //Volver a llenar el grid
                Grid_Fechas_Pago.DataSource = Dv_Fechas_Pagos.ToTable();
                Grid_Fechas_Pago.DataBind();

                //Colocar tabla ordenada en variable de sesion
                HttpContext.Current.Session["Dt_Fechas_Pago"] = Dv_Fechas_Pagos.ToTable();
            }
            catch (Exception ex)
            {
                Mensaje_Error(ex.Message);
            }
        }

    #endregion   

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:       Configuracion_Acceso
    /// DESCRIPCIÓN:  Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:   URL_Pagina: Nombre de la pagina
    /// USUARIO CREÓ: David Herrera Rincon
    /// FECHA CREÓ:   15/Enero/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);            

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 15/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion    
}
