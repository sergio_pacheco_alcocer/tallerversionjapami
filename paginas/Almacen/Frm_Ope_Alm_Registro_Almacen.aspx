﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Registro_Almacen.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Registro_Almacen" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript" language="javascript">

        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }

        function Limpiar_Ctlr_Busqueda() {
            document.getElementById("<%=Txt_Fecha_Inicio.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Fin.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Orden_Compra.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Requisicion.ClientID%>").value = "";
            document.getElementById("<%=Chk_Proveedor.ClientID%>").checked = false;
            document.getElementById("<%=Chk_Fecha.ClientID%>").checked = false;
            document.getElementById("<%=Chk_Almacen_General.ClientID%>").checked = false;
        }
    
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">
                                Registro de Almacen
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="Es necesario:" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                                <asp:ImageButton ID="Btn_Guardar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png" OnClick="Btn_Guardar_Click"
                                    Width="24px" CssClass="Img_Button" ToolTip="Guardar" OnClientClick="return confirm('¿Desea Guardar los Datos?');" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ToolTip="Salir" OnClick="Btn_Salir_Click"
                                    AlternateText="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" />
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div id="Div_Busqueda" runat="server">
                <table width="99%">
                    <tr>
                        <td align="left" style="width: 15%;">
                            <asp:CheckBox ID="Chk_Proveedor" runat="server" Text="Proveedor" />
                        </td>
                        <td colspan="2" align="left" style="width: 90%;">
                            <asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="93%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="Chk_Fecha" runat="server" Text="Fecha" />
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="110px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Inicial" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Inicio_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicio"
                                OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Inicio" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Width="110px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Fin_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Fin"
                                PopupButtonID="Btn_Fecha_Fin" OnClientShown="calendarShown" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Busqueda_Orden_Compra" runat="server" MaxLength="10" AutoPostBack="true"
                                Width="110px" ontextchanged="Txt_Busqueda_Orden_Compra_TextChanged" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Orden_Compra" runat="server" TargetControlID="Txt_Busqueda_Orden_Compra"
                                InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Orden_Compra" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<No. Orden Compra>" TargetControlID="Txt_Busqueda_Orden_Compra" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="Txt_Busqueda_Requisicion" runat="server" MaxLength="10" AutoPostBack="true"
                                Width="110px" ontextchanged="Txt_Busqueda_Requisicion_TextChanged" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Requisicion" runat="server" TargetControlID="Txt_Busqueda_Requisicion"
                                InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Requisicion" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<- No. Requisición ->" TargetControlID="Txt_Busqueda_Requisicion" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:CheckBox ID="Chk_Almacen_General" runat="server" Text="Almacen General" />
                        </td>
                        <td align="right" colspan="2" >
                            <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Buscar" OnClick="Btn_Buscar_Click" />&nbsp;
                            <asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                ToolTip="Limpiar" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Ordenes_Compra" runat="server" AutoGenerateColumns="False"
                    CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Ordenes_Compra_PageIndexChanging"
                    PageSize="20" Width="99%" OnSelectedIndexChanged="Grid_Ordenes_Compra_SelectedIndexChanged">
                    <RowStyle CssClass="GridItem" Font-Size="Larger" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                            Text="Ver" />
                        <asp:BoundField DataField="FOLIO" HeaderText="No. OC">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="7%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO_REQ" HeaderText="RQ">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="7%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PROVEEDOR" HeaderStyle-HorizontalAlign="Left" HeaderText="Proveedor">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA" DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:c}">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderStyle-HorizontalAlign="Left" HeaderText="Estatus"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ORDEN_COMPRA" />
                        <asp:BoundField DataField="NO_REQUISICION" />
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Orden_Compra" runat="server" style="height: 900px;">
                <table style="width: 99%;" class="estilo_fuente">
                    <tr>
                        <td align="left" style="width: 15%;">
                            Folio
                        </td>
                        <td align="left" style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="90%" ReadOnly="true" />
                        </td>
                        <td style="width: 15%;" align="left">
                            Fecha
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Construccion" runat="server" Width="90%" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Requisición
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Requisicion" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                        <td align="left">
                            Estatus
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Estatus" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Proveedor
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Proveedor" runat="server" Width="96%" ReadOnly="true" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Orden_Compra_Detalles" runat="server" Style="white-space: normal;"
                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99%"
                    PageSize="5">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="PRODUCTO_ID" />
                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="5%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PRODUCTO" HeaderStyle-HorizontalAlign="Left" HeaderText="Producto">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="12%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CANTIDAD" HeaderText="Ctd." ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="UNIDAD" HeaderStyle-HorizontalAlign="Left" HeaderText="Unidad">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MONTO" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="P. U." ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="COSTO_REAL" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Importe" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO" HeaderStyle-HorizontalAlign="Left" HeaderText="Tipo"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Almacén" >
                            <ItemTemplate>
                                <asp:DropDownList ID="Cmb_Almacen" runat="server" Font-Size="X-Small">
                                    <asp:ListItem Text="Ninguno" Value="N" />
                                    <asp:ListItem Text="Custodia" Value="C" />
                                    <asp:ListItem Text="Resguardo" Value="R" />
                                </asp:DropDownList>
                            </ItemTemplate>
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="12%" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
                <table width="99%">
                    <tr>
                        <td style="width: 98%;" align="right">
                            Subtotal
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_SubTotal" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            IEPS&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_IEPS" runat="server" align="right" Text=" $ 0.00" ForeColor="Blue"
                                BorderColor="Blue" BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            IVA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_IVA" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Total&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_Total" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
