﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;

public partial class paginas_Almacen_Frm_Cat_Alm_Parametros_Cuentas : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Configuracion_Formulario(false);
                Consultar_Cuentas();
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Formulario
        ///DESCRIPCIÓN          : Carga una configuracion de los controles del Formulario
        ///PARAMETROS           : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                   de los controles.
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Cmb_Cuenta_General.Enabled = Estatus;
            Cmb_Cuenta_Papeleria.Enabled = Estatus;
            Cmb_IVA_Pendiente.Enabled = Estatus;
            Cmb_IVA_Acreditable.Enabled = Estatus;
            Btn_Buscar_General.Enabled = Estatus;
            Btn_Buscar_Papeleria.Enabled = Estatus;
            Btn_Buscar_IVA_Pendiente.Enabled = Estatus;
            Btn_Buscar_IVA_Acreditable.Enabled = Estatus;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Consultar_Cuentas
        ///DESCRIPCIÓN          : Carga los valores para las cuentas del Formulario
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Consultar_Cuentas()
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();
            //Cuentas.P_Cuenta = "8";
            Cuentas.P_Afectable = "SI";
            DataTable Dt_Cuentas = Cuentas.Consulta_Cuentas_Contables();
            
            Cmb_Cuenta_General.DataSource = Dt_Cuentas;
            Cmb_Cuenta_General.DataValueField = "CUENTA_CONTABLE_ID";
            Cmb_Cuenta_General.DataTextField = "Cuenta_Descripcion";
            Cmb_Cuenta_General.DataBind();
            Cmb_Cuenta_General.Items.Insert(0, new ListItem("<< SELECIONE >>", ""));

            Cmb_Cuenta_Papeleria.DataSource = Dt_Cuentas;
            Cmb_Cuenta_Papeleria.DataValueField = "CUENTA_CONTABLE_ID";
            Cmb_Cuenta_Papeleria.DataTextField = "Cuenta_Descripcion";
            Cmb_Cuenta_Papeleria.DataBind();
            Cmb_Cuenta_Papeleria.Items.Insert(0, new ListItem("<< SELECIONE >>", ""));

            Cmb_IVA_Pendiente.DataSource = Dt_Cuentas;
            Cmb_IVA_Pendiente.DataValueField = "CUENTA_CONTABLE_ID";
            Cmb_IVA_Pendiente.DataTextField = "Cuenta_Descripcion";
            Cmb_IVA_Pendiente.DataBind();
            Cmb_IVA_Pendiente.Items.Insert(0, new ListItem("<< SELECIONE >>", ""));

            Cmb_IVA_Acreditable.DataSource = Dt_Cuentas;
            Cmb_IVA_Acreditable.DataValueField = "CUENTA_CONTABLE_ID";
            Cmb_IVA_Acreditable.DataTextField = "Cuenta_Descripcion";
            Cmb_IVA_Acreditable.DataBind();
            Cmb_IVA_Acreditable.Items.Insert(0, new ListItem("<< SELECIONE >>", ""));

            Cls_Cat_Alm_Parametros_Cuentas_Negocio Parametros_Cuentas = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
            DataTable Cuentas_Alm = Parametros_Cuentas.Consultar_Cuentas();
            if (Cuentas_Alm.Rows.Count > 0)
            {
                Cmb_Cuenta_General.SelectedIndex = Cmb_Cuenta_General.Items.IndexOf(
                            Cmb_Cuenta_General.Items.FindByValue(Cuentas_Alm.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_General_ID].ToString().Trim()));
                Cmb_Cuenta_Papeleria.SelectedIndex = Cmb_Cuenta_Papeleria.Items.IndexOf(
                            Cmb_Cuenta_Papeleria.Items.FindByValue(Cuentas_Alm.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_Con_Alm_Papeleria_ID].ToString().Trim()));
                Cmb_IVA_Pendiente.SelectedIndex = Cmb_IVA_Pendiente.Items.IndexOf(
                            Cmb_IVA_Pendiente.Items.FindByValue(Cuentas_Alm.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim()));
                Cmb_IVA_Acreditable.SelectedIndex = Cmb_IVA_Acreditable.Items.IndexOf(
                            Cmb_IVA_Acreditable.Items.FindByValue(Cuentas_Alm.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim()));
            }
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Componentes
            ///DESCRIPCIÓN          : Hace una validacion de que haya datos en los componentes
            ///                       antes de hacer una operación.
            ///PROPIEDADES          :
            ///CREO                 : Salvador Vázquez Camacho.
            ///FECHA_CREO           : 20/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                String Mensaje_Error = "Es necesario.";
                Boolean Validacion = true;

                if (Cmb_Cuenta_General.SelectedIndex < 1)
                {
                    Mensaje_Error += "<br/>+ Seleccionar una cuenta General.";
                    Validacion = false;
                }
                if (Cmb_Cuenta_Papeleria.SelectedIndex < 1)
                {
                    Mensaje_Error += "<br/>+ Seleccionar una cuenta de Papeleria.";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Componentes_Busqueda
            ///DESCRIPCIÓN          : Hace una validacion de que haya datos en los componentes
            ///                       antes de hacer una operación.
            ///PROPIEDADES          :
            ///CREO                 : Salvador Vázquez Camacho.
            ///FECHA_CREO           : 20/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private Boolean Validar_Componentes_Busqueda()
            {
                String Mensaje_Error = "Es necesario.";
                Boolean Validacion = true;

                if (String.IsNullOrEmpty(Txt_Busqueda_Cuenta.Text))
                {
                    Mensaje_Error += "<br/>+ Ingresar un criterio para la Busqueda.";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Error_Busqueda_Cuenta.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Lbl_Error_Busqueda_Cuenta.Visible = true;
                    Img_Error_Busqueda_Cuenta.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    Configuracion_Formulario(true);
                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Cat_Alm_Parametros_Cuentas_Negocio Parametros_Cuentas = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
                        Parametros_Cuentas.P_Cta_Cont_Alm_General_ID = Cmb_Cuenta_General.SelectedValue;
                        Parametros_Cuentas.P_Cta_Cont_Alm_Papeleria_ID = Cmb_Cuenta_Papeleria.SelectedValue;
                        Parametros_Cuentas.P_Cta_IVA_Pendiente = Cmb_IVA_Pendiente.SelectedValue;
                        Parametros_Cuentas.P_Cta_IVA_Acreditable = Cmb_IVA_Acreditable.SelectedValue;
                        Parametros_Cuentas.Modificar_Cuentas();
                        Configuracion_Formulario(false);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Cancela la operación que esta en proceso 
        ///                       (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Btn_Salir.AlternateText == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            }
            else
            {
                Configuracion_Formulario(false);
                Consultar_Cuentas();
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Click
        /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 02/Mayo/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Cerrar_Ventana_Click(object sender, ImageClickEventArgs e)
        {
            Txt_Busqueda_Cuenta.Text = "";
            Mpe_Busqueda_Cuenta.Hide();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Cuenta_Contable_Click
        ///DESCRIPCIÓN          : Consulta las cuentas contables tomando los filtros.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Busqueda_Cuenta_Contable_Click(object sender, EventArgs e)
        {
            Cls_Cat_Con_Cuentas_Contables_Negocio Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();

            Lbl_Error_Busqueda_Cuenta.Visible = false;
            Img_Error_Busqueda_Cuenta.Visible = false;
            try
            {
                if (Validar_Componentes_Busqueda())
                {
                    Double Cuenta = -1;
                    Double.TryParse(Txt_Busqueda_Cuenta.Text, out Cuenta);

                    if (Cuenta > 0)
                        Cuentas.P_Cuenta = Txt_Busqueda_Cuenta.Text.ToString().Trim();
                    else
                        Cuentas.P_Descripcion = Txt_Busqueda_Cuenta.Text.ToString().ToUpper();

                    Cuentas.P_Afectable = "SI";

                    Grid_Cuentas.Columns[1].Visible = true;
                    Grid_Cuentas.DataSource = Cuentas.Consulta_Cuentas_Contables();
                    Grid_Cuentas.DataBind();
                    Grid_Cuentas.Visible = true;
                    Grid_Cuentas.Columns[1].Visible = false;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Error_Busqueda_Cuenta.Text = Ex.Message;
                Lbl_Error_Busqueda_Cuenta.Visible = true;
                Img_Error_Busqueda_Cuenta.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Seleccionar_Cuenta_Click
        ///DESCRIPCIÓN          : Consulta las cuentas contables tomando los filtros.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Seleccionar_Cuenta_Click(object sender, ImageClickEventArgs e)
        {
            String Cuenta = ((ImageButton)sender).CommandArgument;

            if (Session["CUENTA"].ToString() == "GENERAL")
                Cmb_Cuenta_General.SelectedIndex = Cmb_Cuenta_General.Items.IndexOf(Cmb_Cuenta_General.Items.FindByValue(Cuenta));
            if (Session["CUENTA"].ToString() == "PAPELERIA")
                Cmb_Cuenta_Papeleria.SelectedIndex = Cmb_Cuenta_Papeleria.Items.IndexOf(Cmb_Cuenta_Papeleria.Items.FindByValue(Cuenta));
            if (Session["CUENTA"].ToString() == "IVA_PENDIENTE")
                Cmb_IVA_Pendiente.SelectedIndex = Cmb_IVA_Pendiente.Items.IndexOf(Cmb_IVA_Pendiente.Items.FindByValue(Cuenta));
            if (Session["CUENTA"].ToString() == "IVA_ACREDITABLE")
                Cmb_IVA_Acreditable.SelectedIndex = Cmb_IVA_Acreditable.Items.IndexOf(Cmb_IVA_Acreditable.Items.FindByValue(Cuenta));
            Mpe_Busqueda_Cuenta.Hide();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_General_Click
        ///DESCRIPCIÓN          : Obtiene el boton del cual se produce el evento
        ///                       y se almacena en sesion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_General_Click(object sender, ImageClickEventArgs e)
        {
            Session["CUENTA"] = "GENERAL";
            Mpe_Busqueda_Cuenta.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Papeleria_Click
        ///DESCRIPCIÓN          : Obtiene el boton del cual se produce el evento
        ///                       y se almacena en sesion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Papeleria_Click(object sender, ImageClickEventArgs e)
        {
            Session["CUENTA"] = "PAPELERIA";
            Mpe_Busqueda_Cuenta.Show();
        }

    #endregion
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_IVA_Pendiente_Click
        ///DESCRIPCIÓN          : Obtiene el boton del cual se produce el evento
        ///                       y se almacena en sesion.
        ///PROPIEDADES          :
        ///CREO                 : Susana Trigueros Armenta.
        ///FECHA_CREO           : 29/May/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_IVA_Pendiente_Click(object sender, ImageClickEventArgs e)
        {
            Session["CUENTA"] = "IVA_PENDIENTE";
            Mpe_Busqueda_Cuenta.Show();
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_IVA_Acreditable_Click
        ///DESCRIPCIÓN          : Obtiene el boton del cual se produce el evento
        ///                       y se almacena en sesion.
        ///PROPIEDADES          :
        ///CREO                 : Susana Trigueros Armenta.
        ///FECHA_CREO           : 29/May/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_IVA_Acreditable_Click(object sender, ImageClickEventArgs e)
        {
            Session["CUENTA"] = "IVA_ACREDITABLE";
            Mpe_Busqueda_Cuenta.Show();
        }
}
