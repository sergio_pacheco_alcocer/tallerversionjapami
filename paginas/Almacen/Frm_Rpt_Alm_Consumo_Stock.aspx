﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Alm_Consumo_Stock.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Alm_Consumo_Stock" Title="Reporte Consumo de Stock"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    //<!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Productos" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                    <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Productos" style="background-color:#ffffff; width:100%; height:100%;">          
                <table  id="Tabla_Generar" width="100%" class="estilo_fuente"> 
                    <tr>
                  <td style="width:98%">
                       <table width="100%" class="estilo_fuente">
                            <tr align="center">
                                <td class="label_titulo">
                                    Reporte Consumo Stock
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"/>&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                                </td>
                            </tr>
                       </table>
            
                       <table width="98%"  border="0" cellspacing="0">
                                 <tr align="center">
                                     <td>                
                                         <div  style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                                              <table style="width:100%;height:28px;">
                                                <tr><td align="left" style="width:100%;">  
                                                        <%--<asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="20"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />--%>
                                                        <%--<asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="6"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />--%>
                                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                            CssClass="Img_Button" TabIndex="4"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                            onclick="Btn_Salir_Click" />
                                                  </td>
                                                 </tr>
                                              </table>
                                            </div>
                                     </td>
                                 </tr>
                       </table>  
                 </td>
                </tr>  
                <tr>
                    <td >
                     <table style="width: 100%;">
                        <tr>
                            <td style="width: 20%;">
                                 Producto
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Producto" runat="server" Width="75%">
                                </asp:DropDownList>  &nbsp;  &nbsp; &nbsp;  
                                <asp:ImageButton ID="Ibtn_Buscar_Producto" runat="server" CssClass="Img_Button" 
                                                    ToolTip="Búscar producto ó servicio" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                    onclick="Ibtn_Buscar_Producto_Click" />
                            </td>
                        <%--</td>--%>
                        </tr>
                        <tr>
                            <td style="width: 20%;">
                                Mes
                            </td>
                            <td style="width: 30%;">
                                <asp:DropDownList ID="Cmb_Mes" runat="server" Width="98%">
                                </asp:DropDownList>
                            </td>
                            <td style="width: 20%;">
                               &nbsp; &nbsp;  &nbsp;  Año
                            </td>
                            <td style="width: 30%;">
                                <asp:TextBox ID="Txt_Anio" runat="server" Width="50%"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Anio" runat="server" FilterType="Numbers" 
                                TargetControlID="Txt_Anio" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr> 
                            <td style="width: 20%;">
                                Gerencia
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Gerencia" runat="server" Width="81.8%" 
                                    onselectedindexchanged="Cmb_Gerencia_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                         <tr>
                            <td style="width: 20%;">
                                Unidad Responsable
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="81.8%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td colspan= "4" style="width:880px" align="right">
                                <asp:ImageButton ID="Btn_Buscar_Conultar" runat="server" CssClass="Img_Button"
                                             ToolTip="Generar Reporte"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                    onclick="Btn_Buscar_Conultar_Click" />
                                <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                                                            ToolTip="Exportar Excel"  
                                    AlternateText="Exportar a Excel" onclick="Btn_Exportar_Excel_Click" 
                                                           />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            <div style="height:auto;  overflow:auto; width:100%; vertical-align:top;">
                                <asp:GridView ID="Grid_Reporte" runat="server" CssClass="GridView_1">
                                <RowStyle CssClass="GridItem" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />                                
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            </td>
                        </tr>
                         <tr>
                            <td colspan="4" align="center">
                            <div style="height:auto;  overflow:auto; width:70%; vertical-align:top; text-align:center;">
                                <asp:GridView ID="Grid_Detalle_Reporte" runat="server" CssClass="GridView_1">
                                <RowStyle CssClass="GridItem" />
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />                                
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                </div>
                            </td>
                        </tr>
                     </table>
                    </td>
                </tr>
                </table>
             </div>
        </ContentTemplate>
    </asp:UpdatePanel>
       <asp:UpdatePanel ID="UPnl_Busqueda" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="Button" runat="server" Text="Button" Style="display: none;" />
                <cc1:modalpopupextender id="Modal_Busqueda_Prod_Serv" runat="server" targetcontrolid="Btn_Comodin_Busqueda_Productos_Srv"
                    popupcontrolid="Modal_Productos_Servicios" cancelcontrolid="Btn_Cerrar" dynamicservicepath=""
                    dropshadow="True" backgroundcssclass="progressBackgroundFilter" />
                <%--<asp:Button ID="Btn_Comodin_1" runat="server" Text="Button" style="display:none;" /> --%>
                <asp:Button ID="Btn_Comodin_Busqueda_Productos_Srv" runat="server" Text="Button"
                    Style="display: none" />
                <%--<asp:Button ID="Btn_Comodin_Close" runat="server" Text="Button" style="display:none;" />--%>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="Modal_Productos_Servicios" runat="server" CssClass="drag" HorizontalAlign="Center"
            Style="display: none; border-style: outset; border-color: Silver; width: 860px;">
            <asp:Panel ID="Panel2" runat="server" CssClass="estilo_fuente" Style="cursor: move;
                background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
                <table class="estilo_fuente">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Buscar Producto
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <center>
                <asp:UpdatePanel ID="MP_UDPpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <table width="100%" style="color: Black;">
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td style="width: 10%;">
                                        Nombre
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Nombre_B" runat="server" Width="98.5%" 
                                            >
                                        </asp:TextBox>
                                    </td>
                                    <td style="width: 10%;">
                                        <asp:ImageButton ID="IBtn_MDP_Prod_Serv_Buscar" runat="server" CssClass="Img_Button"
                                             ToolTip="Búscar producto ó servicio" OnClick="IBtn_MDP_Prod_Serv_Buscar_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" />
                                        <asp:ImageButton ID="IBtn_MDP_Prod_Serv_Cerrar" runat="server" CssClass="Img_Button"
                                            OnClick="IBtn_MDP_Prod_Serv_Cerrar_Click" ToolTip="Búscar producto ó servicio"
                                            ImageUrl="~/paginas/imagenes/paginas/quitar.png" />
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td style="width: 10%;">
                                            Clave
                                    </td>
                                    <td align ="left" style="width: 20%;">
                                    
                                        <asp:TextBox ID="Txt_Clave_B" runat="server" Width="90%" >
                                        </asp:TextBox>
                                        <cc1:filteredtextboxextender id="Ftb_Txt_Clave" runat="server" targetcontrolid="Txt_Clave_B"
                                                    filtertype="Numbers">
                                            </cc1:filteredtextboxextender>
                                    </td>
                                    <td style="width: 10%;">
                                            Categoría
                                    </td>
                                    <td align ="left" style="width: 40%;">
                                        <asp:DropDownList ID="Cmb_Categorioa_B" runat="server" Width="98.5%" >
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3">
                                        &nbsp;
                                    </td>
                                </tr>
                            </table>
                            <div style="overflow: auto; height: 260px; width: 99%; vertical-align: top; border-style: outset;
                                border-color: Silver;">
                                <table width="100%" style="color: Black;">
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="Grid_Productos_Servicios_Modal" runat="server" AllowPaging="false"
                                                AutoGenerateColumns="false" CssClass="GridView_1" DataKeyNames="ID,NOMBRE,COSTO"
                                                GridLines="Vertical"  Width="98%" 
                                                Style="white-space: normal" EmptyDataText="No se encontraron registros">
                                                <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="Btn_Seleccionar_Producto" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                 OnClick="Btn_Seleccionar_Producto_Click" CommandArgument='<%# Eval("ID") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CLAVE" HeaderText="Clave" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="12%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="10%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UNIDAD" HeaderText="Uni." Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="7%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ID" HeaderText="Producto_ID" Visible="False">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <PagerStyle CssClass="GridHeader" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
        </center>
    </asp:Panel>
        <asp:Panel ID="Modal_Productos_ServiciosX" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Button ID="Btn_Realizar_Busqueda" runat="server" Text="Buscar" CssClass="button" />
        <asp:Button ID="Btn_Cerrar" runat="server" Text="Cerrar" CssClass="button" />
    </asp:Panel>
</asp:Content>
