﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Movimientos_Productos.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Movimientos_Productos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Generar_Kardex_Productos" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />  
    <asp:UpdatePanel ID="Upd_Panel_Generar_Kardex_Productos" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Panel_Generar_Kardex_Productos" runat="server" AssociatedUpdatePanelID="Upd_Panel_Generar_Kardex_Productos" DisplayAfter="0">
                <ProgressTemplate>
                    <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Contenido" visible="true" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="4">Kardex de Productos</td>
                    </tr>
                    <tr>
                        <td  colspan="4">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />                                    
                                </td>            
                              </tr>
                                    
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%;">Productos</td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Producto" runat="server" Width="85%" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Producto" runat="server" WatermarkText="<- Clave o Nombre Producto ->"
                                        TargetControlID="Txt_Producto" WatermarkCssClass="watermarked">
                                    </cc1:TextBoxWatermarkExtender>
                        </td>
                        <td style="width:15%;"></td>
                        <td style="width:35%;"></td>
                    </tr>
                    <tr>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Fecha_Inicial" runat="server" Text="Fecha Inicial" CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td style="width:32%">
                                    <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85%" MaxLength="20" Enabled="true"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicial" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy">
                                    </cc1:CalendarExtender>
                                    <cc1:MaskedEditExtender ID="MEE_Txt_Fecha_Inicio" Mask="99/LLL/9999" runat="server" MaskType="None" 
                                            UserDateFormat="DayMonthYear" UserTimeFormat="None" Filtered="/" TargetControlID="Txt_Fecha_Inicial" 
                                            Enabled="True" ClearMaskOnLostFocus="false"/>  
                                    <cc1:MaskedEditValidator ID="MEV_MEE_Txt_Fecha_Inicio" runat="server" ControlToValidate="Txt_Fecha_Inicial" ControlExtender="MEE_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida" InvalidValueMessage="Fecha no valida" IsValidEmpty="false" 
                                            TooltipMessage="Ingresar Fecha" Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                </td>
                                <td style="width:18%; text-align:left;">
                                    <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="Fecha Final" CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td style="width:32%" style="text-align:left;">
                                    <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85%" MaxLength="20" Enabled="true"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy">
                                    </cc1:CalendarExtender>
                                    <cc1:MaskedEditExtender ID="MaskedEditExtender1" Mask="99/LLL/9999" runat="server" MaskType="None" 
                                            UserDateFormat="DayMonthYear" UserTimeFormat="None" Filtered="/" TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" ClearMaskOnLostFocus="false"/>  
                                    <cc1:MaskedEditValidator ID="MaskedEditValidator1" runat="server" ControlToValidate="Txt_Fecha_Final" ControlExtender="MEE_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida" InvalidValueMessage="Fecha no valida" IsValidEmpty="false" 
                                            TooltipMessage="Ingresar Fecha" Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                </td>
                            </tr>
                    <tr>
                        <td style="width:15%;">Almacenes</td>
                        <td colspan="3"> 
                            <asp:DropDownList ID="DropDownList1" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width:15%;">Almacenes</td>
                        <td colspan="3"> 
                            <asp:DropDownList ID="Cmb_Almacenes" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    <tr>
                        <td style="width:15%;">Partidas</td>
                        <td style="width:35%;">
                            <asp:TextBox ID="Txt_Partida" runat="server" Width="100%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Partida" runat="server" WatermarkText="<- Clave o Nombre Partida ->"
                                        TargetControlID="Txt_Partida" WatermarkCssClass="watermarked">
                                    </cc1:TextBoxWatermarkExtender>
                        </td>
                        <td colspan="2"> 
                            <asp:DropDownList ID="Cmb_Partida" runat="server" Width="100%">
                            </asp:DropDownList>
                        </td>
                        
                    </tr>
                    
                </table>                
            </div>            
            </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>
