﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Alerta_Pto_Reorden.Negocio;
using System.Data;

public partial class paginas_Almacen_Frm_Ope_Alm_Alerta_Punto_Reorden : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Cls_Ope_Alm_Alerta_Punto_Reorden_Negocio Negocio = new Cls_Ope_Alm_Alerta_Punto_Reorden_Negocio();
            DataTable Dt_Productos_Reorden = Negocio.Consultar_Productos_Reorden();
            Session["Dt_Productos_Reorden"] = Dt_Productos_Reorden;
            Llenar_Grid();
        }//fin del if
    }

    protected void Grid_Productos_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable Dt_Productos_Reorden = (DataTable)Session["Dt_Productos_Reorden"];

        if (ViewState["SortDirection"] == null) {
            ViewState["SortDirection"] = "ASC";
        }
        if (Dt_Productos_Reorden != null)
        {
            DataView Dv_Productos_Reorden = new DataView(Dt_Productos_Reorden);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Productos_Reorden.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Productos_Reorden.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Productos.DataSource = Dv_Productos_Reorden;
            Grid_Productos.DataBind();
        }
    }

    protected void Grid_Productos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String Clave = e.Row.Cells[0].Text.Trim();
            DataRow[] Renglon = ((DataTable)Session["Dt_Productos_Reorden"]).Select("CLAVE = '" + Clave + "'");
            if (Renglon.Length > 0)
            {

                ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
               
                int Existencia = Int32.Parse(Renglon[0]["EXISTENCIA"].ToString().Trim());
                int Reorden = Int32.Parse(Renglon[0]["REORDEN"].ToString().Trim());

                if (Reorden > Existencia)
                {
                    Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                    Boton.Visible = true;
                    Boton.ToolTip = "Alerta Existencia menor al Pto Reorden";

                }

                else if (Reorden == Existencia)
                {
                    Boton.ImageUrl = "../imagenes/gridview/circle_yellow.png";
                    Boton.Visible = true;
                    Boton.ToolTip = "Producto en Pto de Reorden.";

                }
                else
                {
                    Boton.Visible = false;
                }

            }
            //}
        }

    }

    protected void Chk_Almacen_General_CheckedChanged(object sender, EventArgs e)
    {
        Llenar_Grid();
    }

    protected void Chk_Almacen_Papeleria_CheckedChanged(object sender, EventArgs e)
    {
        Llenar_Grid();
    }

    public void Llenar_Grid()
    {
        DataTable Dt_Productos_Reorden = new DataTable();
        Dt_Productos_Reorden = (DataTable)Session["Dt_Productos_Reorden"];

        if (Dt_Productos_Reorden.Rows.Count > 0)
        {
            if (Chk_Almacen_General.Checked && !Chk_Almacen_Papeleria.Checked )
            {
                Dt_Productos_Reorden.DefaultView.RowFilter = "ALMACEN_GENERAL = 'SI'";
                Dt_Productos_Reorden = Dt_Productos_Reorden.DefaultView.ToTable();
            }
            if (Chk_Almacen_Papeleria.Checked && !Chk_Almacen_General.Checked)
            {
                Dt_Productos_Reorden.DefaultView.RowFilter = "ALMACEN_GENERAL = 'NO'";
                Dt_Productos_Reorden = Dt_Productos_Reorden.DefaultView.ToTable();
            }
            if (Chk_Almacen_General.Checked == Chk_Almacen_Papeleria.Checked)
            {
                Dt_Productos_Reorden.DefaultView.RowFilter = null;
            }
            Grid_Productos.DataSource = Dt_Productos_Reorden;
        }
        else
        {
            Grid_Productos.EmptyDataText = "No se encontraron Productos en Punto de Reorden.";
        }
        Grid_Productos.DataBind();
    }

}
