﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Resguardos_Recibos.aspx.cs" Inherits="paginas_Frm_Ope_Alm_Resguardos_Recibos"
    Title="Resguardos / Custodias" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            return false;
        }
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <%--liberias pera autocompletar--%>

    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet"
        type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="99.5%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo">
                            Resguardos / Custodias
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" valign="top" style="font-size: 12px; color: Red; font-family: Tahoma;
                                            text-align: left;">
                                            <asp:ImageButton ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td style="width: 20%;">
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                OnClick="Btn_Salir_Click" ToolTip="Salir" AlternateText="Salir" />
                        </td>
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Busqueda"
                            InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                            WatermarkText="<No. Orden Compra>" TargetControlID="Txt_Busqueda" />
                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Txt_Req_Buscar"
                            InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="watermarked"
                            WatermarkText="<No. Requisición>" TargetControlID="Txt_Req_Buscar" />
                    </tr>
                    <tr>
                        <td>
                            <asp:Panel ID="Div_Busqueda_Av" runat="server" Style="width: 98%; height: 100%;"
                                DefaultButton="Btn_Buscar">
                                <table border="0" cellspacing="0" class="estilo_fuente" width="99.5%">
                                    <tr>
                                        <td align="left">
                                            <asp:CheckBox ID="Chk_Fecha_B" runat="server" Text="Fecha" OnCheckedChanged="Chk_Fecha_B_CheckedChanged"
                                                AutoPostBack="true" />
                                            &nbsp;&nbsp;
                                        </td>
                                        <td align="left" style="width: 40%;">
                                            &nbsp;<asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Img_Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                                Enabled="False" ToolTip="Seleccione la Fecha Inicial" />
                                            &nbsp;&nbsp;&nbsp;&nbsp;
                                            <cc1:CalendarExtender ID="CalendarExtender1" OnClientShown="calendarShown" runat="server"
                                                TargetControlID="Txt_Fecha_Inicio" PopupButtonID="Img_Btn_Fecha_Inicio" Format="dd/MMM/yyyy">
                                            </cc1:CalendarExtender>
                                            <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Width="110px" Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Img_Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                                Enabled="False" ToolTip="Seleccione la Fecha Final" />
                                            <cc1:CalendarExtender ID="CalendarExtender2" runat="server" TargetControlID="Txt_Fecha_Fin"
                                                PopupButtonID="Img_Btn_Fecha_Fin" OnClientShown="calendarShown" Format="dd/MMM/yyyy">
                                            </cc1:CalendarExtender>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                                            &nbsp;&nbsp;
                                            <asp:TextBox ID="Txt_Req_Buscar" runat="server" MaxLength="10" Width="150px"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                OnClick="Btn_Buscar_Click" ToolTip="Buscar" />
                                            &nbsp;<asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                OnClick="Btn_Limpiar_Click" ToolTip="Limpiar" />
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <div id="Div_Ordenes_Compra" runat="server" style="width: 99.5%; height: 100%;">
                        <!--Div Ordenes de Compra-->
                        <tr>
                            <td style="text-align: center;">
                                <asp:GridView ID="Grid_Ordenes_Compra" runat="server" Style="white-space: normal;"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" PageSize="1"
                                    Width="99%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Seleccionar_Orden_Compra" runat="server" CommandArgument='<%# Eval("NO_ORDEN_COMPRA") %>'
                                                    CommandName="Seleccionar_Orden_Compra" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                    OnClick="Btn_Seleccionar_Orden_Compra_Click" ToolTip="Ver" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="FOLIO_OC" HeaderText="O. Compra">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FOLIO_REQ" HeaderStyle-HorizontalAlign="Left" HeaderText="Requisición">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="FECHA" DataFormatString="{0:dd/MMM/yyyy}" HeaderText="Fecha">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="PROVEEDOR" HeaderStyle-HorizontalAlign="Left" HeaderText="Proveedor">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:c}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="No  Orden Compra">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NO_CONTRA_RECIBO" HeaderText="Contra Recibo" Visible="False" />
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <EditRowStyle Font-Size="Smaller" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </div>
                    <div id="Div_Detalles_Orden_Compra" runat="server" style="width: 97%; height: 100%;">
                        <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td>
                                    <b>
                                        <asp:Label ID="Lbl_Detalles" runat="server" Text="Detalles"></asp:Label></b>
                                    <asp:HiddenField ID="Hdf_Tipo_Busqueda" runat="server" />
                                    <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                                </td>
                            </tr>
                            <tr align="right" class="barra_delgada">
                                <td align="center" colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:Label ID="Lbl_Orden_Compra" runat="server" Width="80%" Text="Orden de Compra"></asp:Label>
                                </td>
                                <td align="left" style="width: 32%;">
                                    <asp:TextBox ID="Txt_Orden_Compra" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                </td>
                                <td align="left" style="width: 18%;">
                                    &nbsp;Importe [S/IVA]
                                </td>
                                <td align="left" style="width: 32%;">
                                    <asp:TextBox ID="Txt_Importe" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:Label ID="Lbl_Requisición" runat="server" Width="80%" Text="Requisición"></asp:Label>
                                </td>
                                <td align="left" style="width: 32%;">
                                    <asp:TextBox ID="Txt_Requisicion" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                </td>
                                <td align="left" style="width: 18%;">
                                    &nbsp;Fecha
                                </td>
                                <td align="left" style="width: 32%;">
                                    <asp:TextBox ID="Txt_Fecha_Surtido" runat="server" Enabled="false" Width="99%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:Label ID="Lbl_Proveedor" runat="server" Width="80%" Text="Proveedor"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="Txt_Proveedor" runat="server" Enabled="false" Width="99.5%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:HiddenField ID="Hdf_Cuenta_Activo_ID" runat="server" />
                                    <asp:Label ID="Lbl_Cuenta_Activo" runat="server" Text="Cuenta de Activo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Cuenta_Activo" runat="server" Width="99.5%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:HiddenField ID="Hdf_Cuenta_Gasto_ID" runat="server" />
                                    <asp:Label ID="Lbl_Cuenta_Gasto" runat="server" Text="Cuenta de Gasto"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Cuenta_Gasto" runat="server" Width="99.5%"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase de Activo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Tipo_Activo" runat="server" Text="Tipo de Activo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Tipo_Activo" runat="server" Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" width="18%">
                                    <asp:Label ID="Lbl_Procedencia_Bien" runat="server" Text="Procedencia"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Procedencia" runat="server" Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%; text-align: left;">
                                    <asp:Label ID="Lbl_Gerencia" runat="server" Text="Gerencia "></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="Cmb_Gerencias" runat="server" AutoPostBack="True" OnSelectedIndexChanged="Cmb_Gerencias_SelectedIndexChanged"
                                        Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Unidad_Responsable" runat="server" Text="Unidad Responsable"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" AutoPostBack="true"
                                        Font-Size="Small" OnSelectedIndexChanged="Cmb_Unidad_Responsable_SelectedIndexChanged"
                                        Width="100%">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%;">
                                    <asp:Label ID="Lbl_Responsable" runat="server" Text="Responsable"></asp:Label>
                                </td>
                                <td align="left" colspan="3">
                                    <asp:DropDownList ID="Cmb_Responsable" runat="server" Font-Size="Small" Width="95%">
                                    </asp:DropDownList>
                                    <asp:ImageButton ID="Btn_Busqueda_Avanzada_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Busqueda_Avanzada_Resguardante_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%; text-align: left;">
                                    <asp:Label ID="Lbl_Zona" runat="server" Text="Ubicación Fisica"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:DropDownList ID="Cmb_Zonas" runat="server" Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td align="left" style="width: 18%;">
                                    Observaciones
                                </td>
                                <td align="left" colspan="3">
                                    <asp:TextBox ID="Txt_Observaciones" runat="server" MaxLength="249" Rows="3" TextMode="MultiLine"
                                        Width="99.5%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Observaciones"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <b>Firmas para Resguardo</b>
                                </td>
                            </tr>
                            <tr align="right" class="barra_delgada">
                                <td align="center" colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:HiddenField ID="Hdf_Resguardo_Completo_Autorizo" runat="server" />
                                    <asp:Label ID="Lbl_Resguardo_Completo_Autorizo" runat="server" Text="Autorizó"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Resguardo_Completo_Autorizo" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Resguardo_Completo_Autorizo" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Autorizo_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:HiddenField ID="Hdf_Resguardo_Completo_Operador" runat="server" />
                                    <asp:Label ID="Lbl_Resguardo_Completo_Operador" runat="server" Text="Entregó"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Resguardo_Completo_Operador" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Resguardo_Completo_Operador" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Operador_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:HiddenField ID="Hdf_Resguardo_Completo_Funcionario_Recibe" runat="server" />
                                    <asp:Label ID="Lbl_Resguardo_Completo_Funcionario_Recibe" runat="server" Text="Revisó"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Resguardo_Completo_Funcionario_Recibe" runat="server" Width="95%"
                                        Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Resguardo_Completo_Funcionario_Recibe" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                        Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Funcionario_Recibe_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <br />
                                    <br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <b>Productos Orden de Compra</b>
                                </td>
                            </tr>
                            <tr align="right" class="barra_delgada">
                                <td align="center" colspan="4">
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:GridView ID="Grid_Productos" runat="server" AutoGenerateColumns="False" CellPadding="1"
                                        CssClass="GridView_1" GridLines="None" PageSize="100" Style="white-space: normal;"
                                        Width="99%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:CheckBox ID="Chk_Producto" runat="server" AutoPostBack="true" OnCheckedChanged="Chk_Producto_CheckedChanged" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Center" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="PRODUCTO_ID" HeaderText="Producto_ID">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="Inv.">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="8%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRODUCTO" HeaderText="Producto" Visible="True">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COLOR" HeaderText="Color">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MATERIAL" HeaderText="Material">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_SERIE" HeaderText="No. Serie">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COSTO" HeaderText="Importe" DataFormatString="{0:c}">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="8%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OPERACION" HeaderText="Operación">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="5%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="OBSERVACIONES" HeaderText="Observaciones">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Imprimir" runat="server" CommandArgument='<%# Eval("NO_INVENTARIO") %>'
                                                        CommandName="Generar_Recibo" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                                        OnClick="Btn_Imprimir_Click" ToolTip="Resguardo/Recibo" />
                                                    <asp:ImageButton ID="Btn_Imprimir_Resguardo" runat="server" CommandArgument='<%# Eval("NO_INVENTARIO") %>'
                                                        CommandName="Mostrar_Reporte" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                                        OnClick="Btn_Imprimir_Resguardo_Click" ToolTip="Imprimir" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                    &nbsp;<br />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <asp:Button ID="Btn_Aceptar" runat="server" OnClick="Btn_Aceptar_Click" Text="AÑADIR CARACTERISTICAS Y RESGUARDATARIO"
                                        ToolTip="AÑADIR CARACTERISTICAS Y RESGUARDATARIO" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                </td>
                            </tr>
                        </table>
                    </div>
                </table>
            </div>

            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Gasto']").autocomplete("../Control_Patrimonial/Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Gasto']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Gasto_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Gasto']").val("");
                        }
                        });

                        $("[id$='Txt_Cuenta_Activo']").autocomplete("../Control_Patrimonial/Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Activo_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Activo_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Activo']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Activo_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Activo']").val("");
                        }
                        });
                    }
                }
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Panel_Filtro_Busqueda_Empleados" runat="server" Width="100%" DefaultButton="Btn_Busqueda_Empleados">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Empleado
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Empleado" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                    WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                RFC
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                    TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                    WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Nombre
                                            </td>
                                            <td style="width: 30%; text-align: left;" colspan="3">
                                                <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                    FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    ValidChars="áéíóúÁÉÍÓÚ .-" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
</asp:Content>
