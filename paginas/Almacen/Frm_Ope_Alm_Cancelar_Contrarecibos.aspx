﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Cancelar_Contrarecibos.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Cancelar_Contrarecibos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                 <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                 <div  class="processMessage" id="div_progress"><img alt="" src="../imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
           </asp:UpdateProgress>
        
            <div id="Div_Contenido" style="width:97%;height:100%;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan ="4" class="label_titulo">Cancelar Contrarecibos</td>
                    </tr>
                    <tr>
                        <td colspan ="4">
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                Width="24px" Height="24px"/>
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" >
                        <td style="width:20%;" colspan ="4">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png" 
                                onclick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" 
                                onclick="Btn_Salir_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Busquedas_Avanzadas" runat="server" visible="true">
                                <table border="0" cellpadding="0" cellspacing="0" style="width:100%;">
                                    <tr>
                                        <td align="left">N. C.</td>
                                        <td align="left">
                                            <asp:TextBox ID="Txt_No_Contra_Recibo_Busqueda" runat="server" Width="150px"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="Txt_No_Contra_Recibo_Busqueda_TWE" runat="server" TargetControlID="Txt_No_Contra_Recibo_Busqueda" WatermarkCssClass="watermarked" WatermarkText="&lt;NC-&gt;"></cc1:TextBoxWatermarkExtender>
                                            <cc1:FilteredTextBoxExtender ID="Txt_No_Contra_Recibo_Busqueda_FTE" runat="server" TargetControlID="Txt_No_Contra_Recibo_Busqueda" FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9" Enabled="true" InvalidChars="<,>,&,',!"></cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td align="left">O. C.</td>
                                        <td align="left">
                                            <asp:TextBox ID="Txt_No_Orden_Compra_Busqueda" runat="server" Width="150px"></asp:TextBox>
                                            <cc1:TextBoxWatermarkExtender ID="Txt_No_Orden_Compra_Busqueda_TWE" runat="server" TargetControlID="Txt_No_Orden_Compra_Busqueda" WatermarkCssClass="watermarked" WatermarkText="&lt;OC-&gt;"></cc1:TextBoxWatermarkExtender>
                                            <cc1:FilteredTextBoxExtender ID="Txt_No_Orden_Compra_Busqueda_FTE" runat="server" TargetControlID="Txt_No_Orden_Compra_Busqueda" FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9" Enabled="true" InvalidChars="<,>,&,',!"></cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td>&nbsp;</td>
                                        <td align="left"><asp:ImageButton ID="Img_Btn_Buscar" runat="server" 
                                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                                onclick="Img_Btn_Buscar_Click" /></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Proveedor</td>
                                        <td align="left"><asp:TextBox ID="Txt_Busqueda_Proveedor" runat="server"></asp:TextBox></td>
                                        <td colspan="4" align="left"><asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="100%"></asp:DropDownList></td>
                                    </tr>
                                    <tr>
                                        <td align="left">De</td>                                    
                                        <td align="left">
                                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                            <asp:ImageButton ID="Img_Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la fecha inicial" />
                                            <cc1:CalendarExtender ID="Txt_Fecha_Inicio_CE" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Img_Btn_Fecha_Inicio" TargetControlID="Txt_Fecha_Inicio"></cc1:CalendarExtender>
                                        </td>
                                        <td align="left">Al</td>
                                        <td align="left">
                                            <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Enabled="false" Width="150px"></asp:TextBox>
                                            <asp:ImageButton ID="Img_Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la fecha final" />
                                            <cc1:CalendarExtender ID="Txt_Fecha_Fin_CE" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Img_Btn_Fecha_Fin" TargetControlID="Txt_Fecha_Fin"></cc1:CalendarExtender>                                        
                                        </td>
                                        <td align="left">Estatus</td>
                                        <td align="left">
                                            <asp:DropDownList ID="Cmb_Estatus_Busqueda" runat="server" Width="150px">
                                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Generada" Value="GENERADA"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Contrarecibos" runat="server" style="overflow:auto;height:400px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                                <asp:GridView ID="Grid_Contrarecibos" runat="server" Width="100%" AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" HeaderStyle-CssClass="tblHead">
                                    <Columns>
                                        <asp:ButtonField HeaderText="" ButtonType="Image" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" ItemStyle-HorizontalAlign="Center" CommandName="Select" />
                                        <asp:BoundField DataField="OC" HeaderText="O. C." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="NO_CONTRA_RECIBO" HeaderText="C. R." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="FECHA_RECEPCION" HeaderText="Fecha<br />Recepci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:/dd/MMM/yyyy}" HtmlEncode="false" />
                                        <asp:BoundField DataField="FECHA_PAGO" HeaderText="Fecha Pago" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:/dd/MMM/yyyy}" />
                                        <asp:BoundField DataField="PROVEEDOR" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="IMPORTE_TOTAL" HeaderText="Importe" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Contenido_Contrarecibo" runat="server">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left">No Contrarecibo</td>
                                        <td align="left"><asp:TextBox ID="Txt_No_Contrarecibo" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                                        <td align="left">No Orden Compra</td>
                                        <td align="left"><asp:TextBox ID="Txt_No_Orden_Compra" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                                        <td align="left">No Reserva</td>
                                        <td align="left"><asp:TextBox ID="Txt_No_Reserva" runat="server" Width="150px" Enabled="false"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Estatus</td>
                                        <td align="left">
                                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                                <asp:ListItem Text="Cancelada" Value="CANCELADA"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td align="left">Fecha Cancelaci&oacute;n</td>
                                        <td align="left">
                                            <asp:TextBox ID="Txt_Fecha_Cancelacion" runat="server" Width="150px"></asp:TextBox>
                                            <asp:ImageButton ID="Img_Btn_Fecha_Cancelacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la fecha de cancelaci&oacute;" />
                                            <cc1:CalendarExtender ID="Txt_Fecha_Cancelacion_CE" runat="server" Format="dd/MMM/yyyy" PopupButtonID="Img_Btn_Fecha_Cancelacion" TargetControlID="Txt_Fecha_Cancelacion"></cc1:CalendarExtender>
                                        </td>
                                        <td colspan="2">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="left">Proveedor</td>
                                        <td align="left" colspan="5"><asp:TextBox ID="Txt_Proveedor" runat="server" Width="100%" Enabled="false"></asp:TextBox></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Motivo Cancelaci&oacute;n</td>
                                        <td align="left" colspan="5">
                                            <asp:TextBox ID="Txt_Motivo_Cancelacion" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="6">&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td align="left" colspan="6">
                                            <asp:GridView ID="Grid_Facturas" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                                CssClass="GridView_1" GridLines="None" Width="100%" HeaderStyle-CssClass="tblHead" Font-Size="X-Small">
                                                <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:BoundField DataField="NO_FACTURA" HeaderText="No Factura" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                                    <asp:BoundField DataField="FECHA_FACTURA" HeaderText="Fecha" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}" />
                                                    <asp:BoundField DataField="IMPORTE" HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:c}" />
                                                    <asp:BoundField DataField="FACTURA_ID" HeaderText="Factura_ID" />
                                                </Columns>
                                                <PagerStyle CssClass="GridHeader" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView>                                        
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

