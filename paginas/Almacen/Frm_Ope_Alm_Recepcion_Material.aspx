﻿<%@ Page Title="Recepci&oacute;n de Material del Proveedor" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Recepcion_Material.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Recepcion_Material" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript" language="javascript">

        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }

        function Recepcion_Completa(Estatus) {
            var table = document.getElementById('<%=Grid_Orden_Compra_Detalles.ClientID %>');

            for (var i = 1; i < table.rows.length; i++) {
                var Row = table.rows[i];
                var Ctd_Solicitada = parseFloat(Row.cells[2].innerText);
                var Ctd_Entregada = parseFloat(Row.cells[3].innerText);

                Row.cells[4].children[0].value = Estatus.checked ? Ctd_Solicitada - Ctd_Entregada : '0';
            }
        }

        function Limpiar_Ctlr_Busqueda() {
            document.getElementById("<%=Txt_Fecha_Inicio.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Fin.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Orden_Compra.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Requisicion.ClientID%>").value = "";
            document.getElementById("<%=Chk_Proveedor.ClientID%>").checked = false;
            document.getElementById("<%=Chk_Fecha.ClientID%>").checked = false;
            document.getElementById("<%=Chk_Almacen_General.ClientID%>").checked = false;
            return false;
        }

        function Validar_Cantidades() {
            var Ctd_Solicitada = 0.0;
            var Ctd_Entregada = 0.0;
            var Ctd_Recibir = 0.0;
            var Bandera = true;
            var Mensaje = 'Revisar las cantidades del producto: \n\n';
            var table = document.getElementById('<%=Grid_Orden_Compra_Detalles.ClientID %>');
            var Renglones = table.rows.length;

            for (var i = 1; i < Renglones; i++) {
                var Row = table.rows[i];
                Ctd_Solicitada = parseFloat(Row.cells[2].innerText);
                Ctd_Entregada = parseFloat(Row.cells[3].innerText);
                Ctd_Recibir = parseFloat(Row.cells[4].children[0].value);

                if (Ctd_Solicitada < Ctd_Entregada + Ctd_Recibir) {
                    Bandera = false;
                    Mensaje += Row.cells[0].innerText + ' ' + Row.cells[1].innerText + '\n';
                }
            }

            if (Renglones == 2 && Ctd_Recibir == 0) {
                Mensaje = 'Es necesario Indicar la cantidad a Recibir.';
                Bandera = false;
            }

            if (!Bandera) {
                alert(Mensaje);
            }
            else {
                table = document.getElementById('<%=Grid_Facturas.ClientID %>');
                Renglones = table.rows.length;

                for (var i = 1; i < Renglones; i++) {
                    var Row = table.rows[i];

                    if (Row.cells[0].children[0].value == '' ||
                        Row.cells[1].children[0].value == '' || 
                        Row.cells[2].children[0].value == '' ||
                        Row.cells[3].children[0].value == '' ||
                        Row.cells[4].children[0].value == '') {
                        Bandera = false;
                        Mensaje = 'Ingrese los datos de la factura.';
                        alert(Mensaje);
                        break;
                    }
                }
            }
            return Bandera;
        }
       //SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
        
    </script>
    
   

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">
                                Recepci&oacute;n de Material del Proveedor
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="Es necesario:" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                                <asp:ImageButton ID="Btn_Recepcion_Material" runat="server" CssClass="Img_Button"
                                    OnClick="Btn_Recepcion_Material_Click" ToolTip="Entrada Almacen"
                                    OnClientClick="return Validar_Cantidades();" ImageUrl="~/paginas/imagenes/gridview/grid_docto.png" />
                                <asp:ImageButton ID="Btn_Exportar_PDF" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                    Width="24px" CssClass="Img_Button" ToolTip="Imprimir Orden de Compra" OnClick="Btn_Exportar_PDF_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div id="Div_Busqueda" runat="server">
                <table width="99%">
                    <tr>
                        <td align="left" style="width: 16%;">
                            <asp:CheckBox ID="Chk_Proveedor" runat="server" Text="Proveedor" />
                        </td>
                        <td colspan="2" align="left" style="width: 84%;">
                            <asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="91%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:CheckBox ID="Chk_Fecha" runat="server" Text="Fecha" />
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="110px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Inicial" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Inicio_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicio"
                                OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Inicio" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Width="110px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Fin_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Fin"
                                PopupButtonID="Btn_Fecha_Fin" OnClientShown="calendarShown" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Busqueda_Orden_Compra" runat="server" MaxLength="10" AutoPostBack="true"
                                Width="110px" ontextchanged="Txt_Busqueda_Orden_Compra_TextChanged" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Orden_Compra" runat="server" TargetControlID="Txt_Busqueda_Orden_Compra"
                                InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Orden_Compra" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<No. Orden Compra>" TargetControlID="Txt_Busqueda_Orden_Compra" />
                            &nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:TextBox ID="Txt_Busqueda_Requisicion" runat="server" MaxLength="10" AutoPostBack="true"
                                Width="110px" ontextchanged="Txt_Busqueda_Requisicion_TextChanged" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Requisicion" runat="server" TargetControlID="Txt_Busqueda_Requisicion"
                                InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Requisicion" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<- No. Requisición ->" TargetControlID="Txt_Busqueda_Requisicion" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Producto
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Producto" Width="100%" runat="server" AutoPostBack="true" 
                                ontextchanged="Txt_Producto_TextChanged"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Nombre o Clave Producto>" TargetControlID="Txt_Producto" />
                        </td>
                        <td align="right">
                            <asp:CheckBox ID="Chk_Almacen_General" runat="server" Text="Almacen General"  />
                            <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                            &nbsp;<asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                ToolTip="Limpiar" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Ordenes_Compra" runat="server" AutoGenerateColumns="False"
                    CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Ordenes_Compra_PageIndexChanging"
                    PageSize="20" Width="99%" OnSelectedIndexChanged="Grid_Ordenes_Compra_SelectedIndexChanged">
                    <RowStyle CssClass="GridItem" Font-Size="Larger" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                            Text="Ver" />
                        <asp:BoundField DataField="FOLIO" HeaderText="O. Compra">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO_REQ" HeaderText="Requisici&oacute;n">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PROVEEDOR" HeaderStyle-HorizontalAlign="Left" HeaderText="Proveedor">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA" DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:c}">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderStyle-HorizontalAlign="Left" HeaderText="Estatus"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_ORDEN_COMPRA" />
                        <asp:BoundField DataField="NO_REQUISICION" />
                        <asp:BoundField DataField="LISTADO_ALMACEN" />
                        <asp:BoundField DataField="USUARIO_CREO" />
                        <asp:BoundField DataField="DEPENDENCIA" />
                        <asp:BoundField DataField="JUSTIFICACION_COMPRA" />
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Orden_Compra" runat="server" style="height:900px;" >
                <table style="width: 99%;" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_Listado_Almacen" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            Folio
                        </td>
                        <td align="left" style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="90%" ReadOnly="true" />
                        </td>
                        <td style="width: 15%;" align="left">
                            Fecha
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Construccion" runat="server" Width="90%" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Requisici&oacute;n
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Requisicion" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                        <td align="left">
                            Estatus
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Estatus" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Elabor&oacute;
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Elaboro" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                        <td align="left">
                            Dependencia
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Dependencia" runat="server" ReadOnly="true" Width="90%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Proveedor
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Proveedor" runat="server" Width="96%" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Justificaci&oacute;n
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Justificacion" runat="server" Width="96%" ReadOnly="true" Height="50px" TextMode="MultiLine" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">
                            Observaciones
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Observaciones" runat="server" Width="96%" Height="50px" TextMode="MultiLine"
                                MaxLength="249" />
                            <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Observaciones"
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                            </cc1:FilteredTextBoxExtender>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                                WatermarkText="Límite de Caracteres 250" WatermarkCssClass="watermarked" Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Recepci&oacute;n Completa
                        </td>
                        <td>
                            <asp:CheckBox ID="Chk_Recepcion_Completa" runat="server" 
                                OnClick="Recepcion_Completa(this);" AutoPostBack="true" 
                                oncheckedchanged="Chk_Recepcion_Completa_CheckedChanged"/>
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Orden_Compra_Detalles" runat="server" Style="white-space: normal;"
                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99%"
                    PageSize="5">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="5%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PRODUCTO" HeaderStyle-HorizontalAlign="Left" HeaderText="Producto">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="20%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci&oacute;n">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" Width="12%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CANTIDAD" HeaderText="Ctd." ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CANTIDAD_RECIBIDA" HeaderText="Entrada" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField HeaderText="Ctd. Recibida">
                            <ItemTemplate>
                                <asp:TextBox ID="Txt_Cantidad_Recibida" runat="server" Width="90%" MaxLength="30" AutoPostBack="true"
                                    Font-Size="X-Small" Style="text-align: right" Text="0" OnTextChanged="Txt_Cantidad_Recibida_TextChanged" ></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" TargetControlID="Txt_Cantidad_Recibida"
                                    FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9,." Enabled="True" InvalidChars="<,>,&,',!,">
                                </cc1:FilteredTextBoxExtender>
                            </ItemTemplate>
                            <ItemStyle Width="8%" />
                        </asp:TemplateField>
                        <asp:BoundField DataField="UNIDAD" HeaderStyle-HorizontalAlign="Left" HeaderText="Unidad">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MONTO" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="P. Unitario SIN/IVA" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="COSTO_REAL" DataFormatString="{0:c}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="P. Unitario CON/IVA" ItemStyle-HorizontalAlign="Right">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Width="8%" Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
                <table width="99%">
                    <tr>
                        <td style="width: 98%;" align="right">
                            Subtotal
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_SubTotal" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            IEPS&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_IEPS" runat="server" align="right" Text=" $ 0.00" ForeColor="Blue"
                                BorderColor="Blue" BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            IVA&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_IVA" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            Total&nbsp;&nbsp;&nbsp;&nbsp;
                        </td>
                        <td align="right">
                            <asp:Label ID="Lbl_Total" runat="server" Text=" $ 0.00" ForeColor="Blue" BorderColor="Blue"
                                BorderWidth="2px" Width="90px">
                            </asp:Label>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="99%">
                    <tr align="right" class="barra_delgada">
                        <td colspan="2">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 50%;">
                            <asp:Label ID="Lbl_Facturas" runat="server" Text="Facturas" />
                        </td>
                        <td style="display:none;" >
                            <asp:Label ID="Lbl_Doc_Soporte" runat="server" Text="Documentos" />
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:DropDownList ID="Cmb_Doc_Soporte" runat="server" AutoPostBack="true" Width="200px" />
                            &nbsp;&nbsp;<asp:ImageButton ID="Btn_Agregar_Doc_Soporte" runat="server" AlternateText="Agregar"
                                ToolTip="Agregar Documento" ImageUrl="~/paginas/imagenes/gridview/add_grid.png" OnClick="Btn_Agregar_Doc_Soporte_Click" />
                            &nbsp;<asp:ImageButton ID="Btn_Quitar_Doc_Soporte" runat="server" AlternateText="Quitar"
                                ToolTip="Quitar Documento" ImageUrl="~/paginas/imagenes/gridview/minus_grid.png"
                                OnClick="Btn_Quitar_Doc_Soporte_Click"  />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 70%;">
                            <asp:GridView ID="Grid_Facturas" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                                GridLines="None" PageSize="1" Width="90%">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="NO_REGISTRO" HeaderText="No Registro" />
                                    <asp:TemplateField HeaderText="No. Factura">
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_No_Factura" runat="server" MaxLength="49" Width="80%" AutoPostBack="false"
                                                Text='<%# Eval("NO_FACTURA") %>' OnTextChanged="Txt_No_Factura_TextChanged"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="'"
                                                TargetControlID="Txt_No_Factura" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                            </cc1:FilteredTextBoxExtender>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Importe">
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_Importe_Factura" runat="server" AutoPostBack="false"  CssClass="text_cantidades_grid" 
                                                MaxLength="10" ReadOnly="true" Text='<%# Eval("IMPORTE") %>' OnTextChanged="Txt_Importe_Factura_TextChanged"
                                                Width="80%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Letra_Inicial" runat="server" Enabled="True"
                                                FilterType="Custom" InvalidChars="&lt;,&gt;,',!," TargetControlID="Txt_Importe_Factura"
                                                ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>
                                            <itemstyle horizontalalign="Right" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" Width="20%" />
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="I.V.A." HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_IVA_Factura" runat="server" AutoPostBack="false" CssClass="text_cantidades_grid"
                                                MaxLength="10" ReadOnly="true" Text='<%# Eval("IVA_FACTURA") %>' OnTextChanged="Txt_IVA_Factura_TextChanged"
                                                Width="80%"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_IVA_Factura" runat="server" Enabled="True"
                                                FilterType="Custom" InvalidChars="&lt;,&gt;,',!," TargetControlID="Txt_IVA_Factura"
                                                ValidChars="0123456789.">
                                            </cc1:FilteredTextBoxExtender>                                        
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" ItemStyle-Width="20%">
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_Total_Factura" Text='<%# Eval("TOTAL_FACTURA") %>' ReadOnly="true" runat="server" AutoPostBack="false" CssClass="text_cantidades_grid"
                                                MaxLength="10" Width="80%"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="Fecha">
                                        <ItemTemplate>
                                            <asp:TextBox ID="Txt_Fecha_Factura" runat="server" Enabled="False" Width="65%" Text='<%# Eval("FECHA") %>'
                                                Style="text-align: center;" OnTextChanged="Txt_Fecha_Factura_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Fecha_Factura" runat="server" Enabled="true" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                                ToolTip="Seleccionar Fecha de Pago" />
                                            <cc1:CalendarExtender ID="CalendarExtender1" runat="server" Format="dd/MMM/yyyy"
                                                OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Factura" TargetControlID="Txt_Fecha_Factura">
                                            </cc1:CalendarExtender>
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle Width="20%" HorizontalAlign="Right" />
                                    </asp:TemplateField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                            <asp:ImageButton ID="Btn_Agregar_Facturas" runat="server" AlternateText="Agregar" Visible="false"
                                ToolTip="Agregar Facturas" ImageUrl="~/paginas/imagenes/gridview/add_grid.png"
                                OnClick="Btn_Agregar_Facturas_Click" />
                            <asp:ImageButton ID="Btn_Quitar_Factura" runat="server" AlternateText="Quitar" ToolTip="Eliminar Factura" Visible="false"
                                ImageUrl="~/paginas/imagenes/gridview/minus_grid.png" OnClick="Btn_Quitar_Factura_Click" />
                        </td>
                        <td style="width: 50%; text-align: right; vertical-align: top;">
                            <asp:GridView ID="Grid_Doc_Soporte" runat="server" Style="white-space: normal;" AutoGenerateColumns="False"
                                CssClass="GridView_1" GridLines="None" PageSize="1" Width="98%" >
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:TemplateField FooterText="Select">
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Btn_Seleccionar_Documento" runat="server" CommandArgument='<%# Eval("DOCUMENTO_ID") %>'
                                                CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                ToolTip="Ver" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Width="5%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DOCUMENTO_ID" />
                                    <asp:BoundField DataField="NOMBRE" HeaderStyle-HorizontalAlign="Left" HeaderText="Nombre "
                                        ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION" HeaderStyle-HorizontalAlign="Left" HeaderText="Descripci&oacute;n">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
