﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Productos_Almacen.Negocio;

public partial class paginas_Almacen_Frm_Rpt_Alm_Productos : System.Web.UI.Page
{
    #region Variables Locales
    private const String P_Dt_Productos = "Dt_Productos";
    #endregion

    #region Page Load
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Estado_Inicial();
                }
                else
                {
                    Mostrar_Error("", false);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Page_Load) " + ex.ToString(), true);
            }
        }
    #endregion

    #region Metodos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Mostrar_Error
        ///DESCRIPCION:             Mostra/Ocultar el mensaje de error
        ///PARAMETROS:              1. Mensaje: Cadena de texto que contiene el mensaje a mostrar
        ///                         2. Mostrar: Booleano que indica si el mensaje se va a mostrar o no
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:30
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Mostrar_Error(String Mensaje, Boolean Mostrar)
        {
            try
            {
                Lbl_Informacion.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = Mostrar;
            }
            catch (Exception ex)
            {
                Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Estado_Inicial
        ///DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:30
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Estado_Inicial()
        {
            try
            {

                Llena_Combo_Impuestos();
                Llena_Combo_Unidades();
                Llena_Combo_Tipos();
                Llena_Combo_Partidas_Genericas();

                Elimina_Sesiones();

                Habilitar_Controles(false);
                Lbl_Tipo_Busqueda.Visible = false;
                Txt_Busqueda.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
        ///DESCRIPCION:             Eliminar las sesiones utilizadas en la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:42
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Elimina_Sesiones()
        {
            try
            {
                HttpContext.Current.Session.Remove(P_Dt_Productos);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Limpiar_Controles
        ///DESCRIPCION:             Limpiar los controles de la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:42
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Txt_Busqueda.Text = "";
                Cmb_Estatus.SelectedIndex = 0;
                Cmb_Impuestos.SelectedIndex = 0;
                Cmb_Partidas_Especificas.Items.Clear();
                Cmb_Partidas_Genericas.SelectedIndex = 0;
                Cmb_Stock.SelectedIndex = 0;
                Cmb_Tipo_Reporte.SelectedIndex = 0;
                Cmb_Tipos.SelectedIndex = 0;
                Cmb_Unidades.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Unidades
        ///DESCRIPCION:             Llenar el combo de las unidades
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Unidades()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Unidades = new DataTable(); //variable para la tabla

            try
            {
                //Asignar propiedades
                Dt_Unidades = Productos_Negocio.Consulta_Unidades();
                Cmb_Unidades.Items.Clear();
                Cmb_Unidades.DataSource = Dt_Unidades;
                Cmb_Unidades.DataTextField = "Nombre";
                Cmb_Unidades.DataValueField = "Unidad_ID";
                Cmb_Unidades.DataBind();
                Cmb_Unidades.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Unidades.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Unidades.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Impuestos
        ///DESCRIPCION:             Llenar el combo de los impuestos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Impuestos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Impuestos = new DataTable(); //Tabla para los impuestos

            try
            {
                //Asignar propiedades
                Dt_Impuestos = Productos_Negocio.Consulta_Impuestos();
                Cmb_Impuestos.Items.Clear();
                Cmb_Impuestos.DataSource = Dt_Impuestos;
                Cmb_Impuestos.DataTextField = "Nombre";
                Cmb_Impuestos.DataValueField = "Impuesto_ID";
                Cmb_Impuestos.DataBind();
                Cmb_Impuestos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Impuestos.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Impuestos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Tipos
        ///DESCRIPCION:             Llenar el combo de los tipos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Tipos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Tipos = new DataTable(); //Tabla para los tipos

            try
            {
                //Asignar propiedades
                Dt_Tipos = Productos_Negocio.Consulta_Tipos();
                Cmb_Tipos.Items.Clear();
                Cmb_Tipos.DataSource = Dt_Tipos;
                Cmb_Tipos.DataTextField = "Tipo";
                Cmb_Tipos.DataValueField = "Tipo";
                Cmb_Tipos.DataBind();
                Cmb_Tipos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Tipos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Genericas
        ///DESCRIPCION:             Llenar el combo de las partidas genericas
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Genericas()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Genericas = new DataTable(); //Tabla para las partidas genericas

            try
            {
                //Asignar propiedades
                Dt_Partidas_Genericas = Productos_Negocio.Consulta_Partidas_Genericas();
                Cmb_Partidas_Genericas.Items.Clear();
                Cmb_Partidas_Genericas.DataSource = Dt_Partidas_Genericas;
                Cmb_Partidas_Genericas.DataTextField = "Nombre";
                Cmb_Partidas_Genericas.DataValueField = "Partida_Generica_ID";
                Cmb_Partidas_Genericas.DataBind();
                Cmb_Partidas_Genericas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Genericas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Genericas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Especificas
        ///DESCRIPCION:             Llenar el combo de las partidas especificas
        ///PARAMETROS:              Partida_Generica_ID: Cadena de texto que contiene el ID de la partida Generica
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Especificas(String Partida_Generica_ID)
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Especificas = new DataTable(); //tabla para las partidas especificas

            try
            {
                //Asignar propiedades
                Productos_Negocio.P_Partida_Generica_ID = Partida_Generica_ID;
                Dt_Partidas_Especificas = Productos_Negocio.Consulta_Partidas_Especificas();
                Cmb_Partidas_Especificas.DataSource = Dt_Partidas_Especificas;
                Cmb_Partidas_Especificas.DataTextField = "Nombre";
                Cmb_Partidas_Especificas.DataValueField = "Partida_ID";
                Cmb_Partidas_Especificas.DataBind();
                Cmb_Partidas_Especificas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Especificas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Especificas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Habilitar_Controles
        ///DESCRIPCION:             Habilitar los controles de acuerdo si es filtro o bosqueda
        ///PARAMETROS:              Filtro: Booleano que indica si es busqueda por filtro
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 13:22
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Habilitar_Controles(Boolean Filtro)
        {
            try
            {
                Lbl_Tipo_Busqueda.Visible = !Filtro;
                Txt_Busqueda.Visible = !Filtro;
                Cmb_Estatus.Enabled = Filtro;
                Cmb_Impuestos.Enabled = Filtro;
                Cmb_Partidas_Especificas.Enabled = Filtro;
                Cmb_Partidas_Genericas.Enabled = Filtro;
                Cmb_Stock.Enabled = Filtro;
                Cmb_Tipos.Enabled = Filtro;
                Cmb_Unidades.Enabled = Filtro;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
        ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO: Susana Trigueros Armenta
        ///FECHA_CREO: 01/Mayo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
        {

            ReportDocument Reporte = new ReportDocument();
            DataRow Renglon; //Renglon para el llenado de las tablas
            int Cont_Elementos; //Variable para el contador

            //Ciclo para el barrido de la cabecera
            for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[0].Rows.Count; Cont_Elementos++)
            {
                //Instanciar el renglon
                Renglon = Data_Set_Consulta_DB.Tables[0].Rows[Cont_Elementos];

                //Importar el renglon
                Ds_Reporte.Tables[0].ImportRow(Renglon);
            }

            //Ciclo para el barrido de los detalles
            for (Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Tables[1].Rows.Count; Cont_Elementos++)
            {
                //Instanciar el renglon
                Renglon = Data_Set_Consulta_DB.Tables[1].Rows[Cont_Elementos];

                //Importar el renglon
                Ds_Reporte.Tables[1].ImportRow(Renglon);
            }

            String File_Path = Server.MapPath("../Rpt/Almacen/" + Nombre_Reporte);
            Reporte.Load(File_Path);
            //Ds_Reporte = Data_Set_Consulta_DB;
            Reporte.SetDataSource(Ds_Reporte);
            ExportOptions Export_Options = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
            Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
            Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
            Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Export_Options);
            String Ruta = "../../Reporte/" + Nombre_PDF;
            Mostrar_Reporte(Nombre_PDF, "PDF");
            //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }

        /// *************************************************************************************
        /// NOMBRE:              Mostrar_Reporte
        /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
        /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
        ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
        /// USUARIO CREO:        Juan Alberto Hernández Negrete.
        /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
        /// USUARIO MODIFICO:    Salvador Hernández Ramírez
        /// FECHA MODIFICO:      16-Mayo-2011
        /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
        /// *************************************************************************************
        protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                if (Formato == "PDF")
                {
                    Pagina = Pagina + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                else if (Formato == "Excel")
                {
                    String Ruta = "../../Exportaciones/" + Nombre_Reporte_Generar;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        /// *************************************************************************************
        /// NOMBRE:              Llena_Reporte_PDF
        /// DESCRIPCION:         Llenar el reporte PDF con el resultado de la consulta
        /// PARAMETROS:          
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          23/Abril/2012 13:50
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        private void Llena_Reporte_PDF()
        {
            //Declaracion de variables
            DataTable Dt_Cabecera = new DataTable(); //Tabla para la cabecera del reporte
            DataTable Dt_Detalles = new DataTable(); //Tabla para los detalles del reporte
            DataTable Dt_Productos = new DataTable(); //Tabla para la consulta de los productos
            DataSet Ds_Reporte = new DataSet(); //Dataset patra colocar las tablas del reporte
            Ds_Alm_Productos Ds_Reporte_src = new Ds_Alm_Productos(); //Dataset archivo para el llenado del reporte de Crystal
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            String Encabezado = String.Empty; //variable para los datos del encabezado
            DataRow Renglon; //renglon para el llenado de la tabla
            int Cont_Elementos; //variable para el contador

            try
            {
                //Verificar el tipo de reporte
                if (Cmb_Tipo_Reporte.SelectedIndex == 1)
                {
                    //Unidad
                    if (Cmb_Unidades.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Unidad_ID = Cmb_Unidades.SelectedItem.Value.Trim();
                    }

                    //Impuestos
                    if (Cmb_Impuestos.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Impuesto_ID = Cmb_Impuestos.SelectedItem.Value.Trim();
                    }

                    //Estatus
                    if (Cmb_Estatus.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                    }

                    //Tipo
                    if (Cmb_Tipos.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value.Trim();
                    }
                }
                else
                {
                    //Colocar el tipo de filtro
                    Productos_Negocio.P_Tipo_Consulta = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                    //Seleccionar el tipo de consulta de busqueda
                    switch (Cmb_Tipo_Reporte.SelectedItem.Value.Trim())
                    {
                        case "Clave":
                            Productos_Negocio.P_Clave = Txt_Busqueda.Text.Trim();
                            break;

                        case "Ref_JAPAMI":
                            Productos_Negocio.P_Ref_JAPAMI = Convert.ToInt64(Txt_Busqueda.Text.Trim());
                            break;

                        case "Nombre":
                            Productos_Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                            break;

                        default:
                            break;
                    }
                }

                //Llenar la tabla de los detalles
                Dt_Productos = Productos_Negocio.Consulta_Productos();

                //verificar si la consulta arrojo resultados
                if (Dt_Productos.Rows.Count > 0)
                {
                    //Colocar las columnas a la tabla de la cabecera
                    Dt_Cabecera.Columns.Add("ID", typeof(int));
                    Dt_Cabecera.Columns.Add("Datos_Filtro", typeof(String));

                    //Colocar las columnas de la tabla de los detalles
                    Dt_Detalles.Columns.Add("ID", typeof(int));
                    Dt_Detalles.Columns.Add("Clave", typeof(String));
                    Dt_Detalles.Columns.Add("Ref_JAPAMI", typeof(Int64));
                    Dt_Detalles.Columns.Add("Nombre_Completo", typeof(String));
                    Dt_Detalles.Columns.Add("Estatus", typeof(String));
                    Dt_Detalles.Columns.Add("Stock", typeof(String));
                    Dt_Detalles.Columns.Add("Unidad", typeof(String));
                    Dt_Detalles.Columns.Add("Partida", typeof(String));
                    Dt_Detalles.Columns.Add("Existencia", typeof(Int64));
                    Dt_Detalles.Columns.Add("Comprometido", typeof(Int64));
                    Dt_Detalles.Columns.Add("Disponible", typeof(Int64));
                    Dt_Detalles.Columns.Add("Tipo", typeof(String));

                    //Instanciar el renglon de la cabecera
                    Renglon = Dt_Cabecera.NewRow();

                    //Llenar y colocar renglon de la cabecera
                    Renglon["ID"] = 1;
                    Renglon["Datos_Filtro"] = Encabezado;
                    Dt_Cabecera.Rows.Add(Renglon);

                    //Ciclo para el barrido de la tabla de los productos
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Productos.Rows.Count; Cont_Elementos++)
                    {
                        //Instanciar renglon de los detalles
                        Renglon = Dt_Detalles.NewRow();

                        //Colocar el dato del ID
                        Renglon["ID"] = 1;

                        //Llenar el resto del renglon
                        Renglon["Clave"] = Dt_Productos.Rows[Cont_Elementos]["Clave"];
                        Renglon["Ref_JAPAMI"] = Dt_Productos.Rows[Cont_Elementos]["Ref_JAPAMI"];
                        Renglon["Nombre_Completo"] = Dt_Productos.Rows[Cont_Elementos]["Nombre_Completo"];
                        Renglon["Estatus"] = Dt_Productos.Rows[Cont_Elementos]["Estatus"];
                        Renglon["Stock"] = Dt_Productos.Rows[Cont_Elementos]["Stock"];
                        Renglon["Unidad"] = Dt_Productos.Rows[Cont_Elementos]["Unidad"];
                        Renglon["Partida"] = Dt_Productos.Rows[Cont_Elementos]["Partida_Especifica"];
                        Renglon["Existencia"] = Dt_Productos.Rows[Cont_Elementos]["Existencia"];
                        Renglon["Comprometido"] = Dt_Productos.Rows[Cont_Elementos]["Comprometido"];
                        Renglon["Disponible"] = Dt_Productos.Rows[Cont_Elementos]["Disponible"];
                        Renglon["Tipo"] = Dt_Productos.Rows[Cont_Elementos]["Tipo"];
                        
                        //Colocar renglon en la tabla
                        Dt_Detalles.Rows.Add(Renglon);
                    }

                    //Colocar tablas en el dataset
                    Ds_Reporte.Tables.Add(Dt_Cabecera);
                    Ds_Reporte.Tables.Add(Dt_Detalles);

                    //Generar el reporte
                    Generar_Reporte(Ds_Reporte, Ds_Reporte_src, "Rpt_Alm_Productos.rpt", "Reporte_Productos.pdf");
                }
                else
                {
                    Mostrar_Error("La consulta no arroj&oacute; resultados", true);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        /// *************************************************************************************
        /// NOMBRE:              Llena_Reporte_Excel
        /// DESCRIPCION:         Llenar un documento de Excel con el resultado de la consulta
        /// PARAMETROS:          
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          23/Abril/2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        private void Llena_Reporte_Excel()
        {
            //Declaracion de variables
            DataTable Dt_Productos = new DataTable(); //Tabla para la consulta de los productos
            String Encabezado = String.Empty; //variable para los datos del encabezado
            int Cont_Elementos; //variable para el contador
            String Ruta_Archivo = String.Empty; //variable para la ruta del archivo
            String Nombre_Archivo = "Reporte_Productos.xls"; //variable para el nombre del archivo
            Workbook book = new Workbook(); //Variable para el libro
            WorksheetStyle style; //Variable para el estilo
            Worksheet sheet; //variable para la hoja
            WorksheetRow row; //variable para el renglon
            WorksheetCell cell; //Variable para la celda
            String Script_js = String.Empty; //variable para el Script de javascript
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios

            try
            {
                //Verificar el tipo de reporte
                if (Cmb_Tipo_Reporte.SelectedIndex == 1)
                {
                    //Unidad
                    if (Cmb_Unidades.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Unidad_ID = Cmb_Unidades.SelectedItem.Value.Trim();
                    }

                    //Impuestos
                    if (Cmb_Impuestos.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Impuesto_ID = Cmb_Impuestos.SelectedItem.Value.Trim();
                    }

                    //Estatus
                    if (Cmb_Estatus.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                    }

                    //Tipo
                    if (Cmb_Tipos.SelectedIndex > 0)
                    {
                        Productos_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value.Trim();
                    }
                }
                else
                {
                    //Colocar el tipo de filtro
                    Productos_Negocio.P_Tipo_Consulta = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                    //Seleccionar el tipo de consulta de busqueda
                    switch (Cmb_Tipo_Reporte.SelectedItem.Value.Trim())
                    {
                        case "Clave":
                            Productos_Negocio.P_Clave = Txt_Busqueda.Text.Trim();
                            break;

                        case "Ref_JAPAMI":
                            Productos_Negocio.P_Ref_JAPAMI = Convert.ToInt64(Txt_Busqueda.Text.Trim());
                            break;

                        case "Nombre":
                            Productos_Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                            break;

                        default:
                            break;
                    }
                }

                //Llenar la tabla de los detalles
                Dt_Productos = Productos_Negocio.Consulta_Productos();

                //Verificar si la consulta arrojo resultados
                if (Dt_Productos.Rows.Count > 0)
                {
                    //Asignar la ruta del archivo
                    Ruta_Archivo = HttpContext.Current.Server.MapPath("~") + "\\Exportaciones\\" + Nombre_Archivo;

                    // Especificar qué hoja debe ser abierto y el tamaño de la ventana por defecto
                    book.ExcelWorkbook.ActiveSheetIndex = 0;
                    book.ExcelWorkbook.WindowTopX = 100;
                    book.ExcelWorkbook.WindowTopY = 200;
                    book.ExcelWorkbook.WindowHeight = 7000;
                    book.ExcelWorkbook.WindowWidth = 8000;

                    // Propiedades del documento
                    book.Properties.Author = "CONTEL";
                    book.Properties.Title = "REPORTE";
                    book.Properties.Created = DateTime.Now;

                    // Se agrega estilo al libro
                    style = book.Styles.Add("HeaderStyle");
                    style.Font.FontName = "Tahoma";
                    style.Font.Size = 14;
                    style.Font.Bold = true;
                    style.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                    style.Font.Color = "White";
                    style.Interior.Color = "Blue";
                    style.Interior.Pattern = StyleInteriorPattern.DiagCross;

                    // Se Crea el estilo a usar
                    style = book.Styles.Add("Default");
                    style.Font.FontName = "Tahoma";
                    style.Font.Size = 10;

                    //Asignar el nombre a la hoja
                    sheet = book.Worksheets.Add("Hoja1");

                    //Agregar renglon para el encabezado
                    row = sheet.Table.Rows.Add();
                    row.Index = 0;//Para saltarse Filas

                    // Se agrega el encabezado
                    row.Cells.Add(new WorksheetCell(Encabezado, "HeaderStyle"));

                    //Agregar renglones para separar
                    row = sheet.Table.Rows.Add();
                    row = sheet.Table.Rows.Add();
                    row = sheet.Table.Rows.Add();

                    //Encabezado de la tabla
                    row.Cells.Add(new WorksheetCell("Clave", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Ref JAPAMI", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Producto", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Tipo", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Estatus", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Stock", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Unidad", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Partida", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Exist", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Comp", "HeaderStyle"));
                    row.Cells.Add(new WorksheetCell("Disp", "HeaderStyle"));

                    //Ciclo para el barrido del grid
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Productos.Rows.Count; Cont_Elementos++)
                    {
                        //Agregar renglon
                        row = sheet.Table.Rows.Add();

                        //Agregar las columnas
                        if (Dt_Productos.Rows[Cont_Elementos]["Clave"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Clave"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Ref_JAPAMI"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("0", DataType.Number));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Ref_JAPAMI"].ToString().Trim(), DataType.Number));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Nombre_Completo"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Nombre_Completo"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Tipo"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Tipo"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Estatus"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Estatus"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Stock"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Stock"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Unidad"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Unidad"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Partida_Especifica"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("", DataType.String));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Partida_Especifica"].ToString().Trim(), DataType.String));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Existencia"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("0", DataType.Number));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Existencia"].ToString().Trim(), DataType.Number));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Comprometido"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("0", DataType.Number));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Comprometido"].ToString().Trim(), DataType.Number));
                        }

                        if (Dt_Productos.Rows[Cont_Elementos]["Disponible"] == DBNull.Value)
                        {
                            row.Cells.Add(new WorksheetCell("0", DataType.Number));
                        }
                        else
                        {
                            row.Cells.Add(new WorksheetCell(Dt_Productos.Rows[Cont_Elementos]["Disponible"].ToString().Trim(), DataType.Number));
                        }
                    }

                    // Se Guarda el archivo                
                    book.Save(Ruta_Archivo);
                    string script = @"<script type='text/javascript'>alert('Registros Exportados a Excel');</script>";
                    ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

                    Mostrar_Reporte(Nombre_Archivo, "Excel");
                }
                else
                {
                    Mostrar_Error("La consulta no arroj&oacute; resultados", true);
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Grid
        /// *************************************************************************************
        /// NOMBRE:              Llena_Grid_Productos
        /// DESCRIPCION:         Llenar el Grid de los Productos
        /// PARÁMETROS:          Pagina: Entero que indica la pagina del Grid
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          22/Marzo/2012 20:00
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        private void Llena_Grid_Productos(int Pagina)
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Productos = new DataTable(); //tabla para la consulta de los productos

            try
            {
                //Verificar si existe la sesion
                if (HttpContext.Current.Session[P_Dt_Productos] != null)
                {
                    //Colocar variable de sesion en la tabla
                    Dt_Productos = ((DataTable)HttpContext.Current.Session[P_Dt_Productos]);
                }
                else
                {
                    //Asignar propiedades
                    //Verificar el tipo de reporte
                    if (Cmb_Tipo_Reporte.SelectedIndex == 1)
                    {
                        //Unidad
                        if (Cmb_Unidades.SelectedIndex > 0)
                        {
                            Productos_Negocio.P_Unidad_ID = Cmb_Unidades.SelectedItem.Value.Trim();
                        }

                        //Impuestos
                        if (Cmb_Impuestos.SelectedIndex > 0)
                        {
                            Productos_Negocio.P_Impuesto_ID = Cmb_Impuestos.SelectedItem.Value.Trim();
                        }

                        //Estatus
                        if (Cmb_Estatus.SelectedIndex > 0)
                        {
                            Productos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                        }

                        //Tipo
                        if (Cmb_Tipos.SelectedIndex > 0)
                        {
                            Productos_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value.Trim();
                        }

                    }
                    else
                    {
                        //Colocar el tipo de filtro
                        Productos_Negocio.P_Tipo_Consulta = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                        //Seleccionar el tipo de consulta de busqueda
                        switch (Cmb_Tipo_Reporte.SelectedItem.Value.Trim())
                        {
                            case "Clave":
                                Productos_Negocio.P_Clave = Txt_Busqueda.Text.Trim();
                                break;

                            case "Ref_JAPAMI":
                                Productos_Negocio.P_Ref_JAPAMI = Convert.ToInt64(Txt_Busqueda.Text.Trim());
                                break;

                            case "Nombre":
                                Productos_Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                                break;

                            default:
                                break;
                        }
                    }

                    //Ejecutar consulta
                    Dt_Productos = Productos_Negocio.Consulta_Productos();
                }


                //Verificar si la consulta arrojo resultados
                if (Dt_Productos.Rows.Count > 0)
                {

                    //Llenar el grid
                    Grid_Productos.DataSource = Dt_Productos;

                    //Verificar si hay pagina
                    if (Pagina > -1)
                    {
                        Grid_Productos.PageIndex = Pagina;
                    }

                    Grid_Productos.DataBind();

                    //Colocar los datos en la variable de sesion
                    HttpContext.Current.Session[P_Dt_Productos] = Dt_Productos;
                }
                else
                {
                    Mostrar_Error("La consulta no arroj&oacute; resultados.", true);
                    Grid_Productos.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        protected void Grid_Productos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Llena_Grid_Productos(e.NewPageIndex);
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Grid_Productos_PageIndexChanging) " + ex.ToString(), true);
            }
        }
    #endregion

    #region Eventos
        protected void Cmb_Partidas_Genericas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Verificar el indice
                if (Cmb_Partidas_Genericas.SelectedIndex > 1)
                {
                    Llena_Combo_Partidas_Especificas(Cmb_Partidas_Genericas.SelectedItem.Value);
                }
                else
                {
                    //Limpiar el combo de las partidas especificas
                    Cmb_Partidas_Especificas.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Partidas_Genericas_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Cmb_Tipo_Reporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //verificar el indice
                switch (Cmb_Tipo_Reporte.SelectedIndex)
                {
                    case 1:
                        Habilitar_Controles(true);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Clave";
                        break;

                    case 2:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Referencia de JAPAMI";
                        break;

                    case 3:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Clave";
                        break;

                    case 4:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione un Nombre del Producto";
                        break;

                    default:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Visible = false;
                        Txt_Busqueda.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Tipo_Reporte_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un reporte
                if (Cmb_Tipo_Reporte.SelectedIndex > 0)
                {
                    //verificar si no es por filtro
                    if (Cmb_Tipo_Reporte.SelectedIndex != 1)
                    {
                        //verificar si se escribio algo en la caja de texto
                        if (Txt_Busqueda.Text.Trim() == null || Txt_Busqueda.Text.Trim() == "" || Txt_Busqueda.Text.Trim() == String.Empty)
                        {
                            Mostrar_Error("Escriba el criterio de b&uacute;squeda para el reporte", true);
                        }
                        else
                        {
                            Elimina_Sesiones();
                            Llena_Grid_Productos(-1);
                        }
                    }
                    else
                    {
                        Elimina_Sesiones();
                        Llena_Grid_Productos(-1);
                    }
                }
                else
                {
                    Mostrar_Error("Seleccione alguna opci&oacute;n para el reporte", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un reporte
                if (Cmb_Tipo_Reporte.SelectedIndex > 0)
                {
                    //verificar si no es por filtro
                    if (Cmb_Tipo_Reporte.SelectedIndex != 1)
                    {
                        //verificar si se escribio algo en la caja de texto
                        if (Txt_Busqueda.Text.Trim() == null || Txt_Busqueda.Text.Trim() == "" || Txt_Busqueda.Text.Trim() == String.Empty)
                        {
                            Mostrar_Error("Escriba el criterio de b&uacute;squeda para el reporte", true);
                        }
                        else
                        {
                            Elimina_Sesiones();
                            Llena_Reporte_PDF();
                        }
                    }
                    else
                    {
                        Elimina_Sesiones();
                        Llena_Reporte_PDF();
                    }
                }
                else
                {
                    Mostrar_Error("Seleccione alguna opci&oacute;n para el reporte", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Imprimir_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un reporte
                if (Cmb_Tipo_Reporte.SelectedIndex > 0)
                {
                    //verificar si no es por filtro
                    if (Cmb_Tipo_Reporte.SelectedIndex != 1)
                    {
                        //verificar si se escribio algo en la caja de texto
                        if (Txt_Busqueda.Text.Trim() == null || Txt_Busqueda.Text.Trim() == "" || Txt_Busqueda.Text.Trim() == String.Empty)
                        {
                            Mostrar_Error("Escriba el criterio de b&uacute;squeda para el reporte", true);
                        }
                        else
                        {
                            Elimina_Sesiones();
                            Llena_Reporte_Excel();
                        }
                    }
                    else
                    {
                        Elimina_Sesiones();
                        Llena_Reporte_Excel();
                    }
                }
                else
                {
                    Mostrar_Error("Seleccione alguna opci&oacute;n para el reporte", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Salir_Click) " + ex.ToString(), true);
            }
        }

    #endregion
}