﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Alm_Productos.aspx.cs" Inherits="paginas_Almacen_Frm_Rpt_Alm_Productos" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                  <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>

            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="6" class="label_titulo">Reporte de Productos</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="6" style="width:20%;">
                            <!--Bloque de la busqueda-->
                            <asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" 
                                onclick="Btn_Consultar_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="NUEVO" ToolTip="Exportar PDF" 
                                onclick="Btn_Imprimir_Click" />  
                                <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Imprimir Excel" 
                                ToolTip="Exportar Excel" onclick="Btn_Imprimir_Excel_Click" />  
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click"/>
                        </td>                                 
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Tipo B&uacute;squeda</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Reporte" runat="server" Width="150px" 
                                AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Tipo_Reporte_SelectedIndexChanged">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Filtros" Value="Filtro"></asp:ListItem>
                                <asp:ListItem Text="Clave" Value="Clave"></asp:ListItem>
                                <asp:ListItem Text="Referencia JAPAMI" Value="Ref_JAPAMI"></asp:ListItem>
                                <asp:ListItem Text="Nombre" Value="Nombre"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td colspan="3" align="right"><asp:Label ID="Lbl_Tipo_Busqueda" runat="server"></asp:Label></td>
                        <td align="right"><asp:TextBox ID="Txt_Busqueda" runat="server" Width="150px"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td align="left">Unidad</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Unidades" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Partida Gen&eacute;rica</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Partidas_Genericas" 
                                runat="server" Width="100%" AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Partidas_Genericas_SelectedIndexChanged"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Partida Espec&iacute;fica</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Partidas_Especificas" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Impuesto</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Impuestos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td colspan="2">&nbsp;</td>
                        <td align="left">Estatus</td>
                        <td align="right">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Sin valor" Value="NULO"></asp:ListItem>
                                <asp:ListItem Text="Activo" Value="ACTIVO"></asp:ListItem>
                                <asp:ListItem Text="Inactivo" Value="INACTIVO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Tipo</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Tipos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td colspan="2">&nbsp;</td>
                        <td align="left">Stock</td>
                        <td align="right">
                            <asp:DropDownList ID="Cmb_Stock" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Sin valor" Value="NULO"></asp:ListItem>
                                <asp:ListItem Text="Si" Value="SI"></asp:ListItem>
                                <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6">
                            <asp:GridView ID="Grid_Productos" runat="server" style="white-space:normal;" 
                                AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" 
                                GridLines="None" PageSize="5" Width="100%" AllowPaging="true" 
                                onpageindexchanging="Grid_Productos_PageIndexChanging">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Ref_JAPAMI" HeaderText="Ref. JAPAMI" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Nombre_Completo" HeaderText="Producto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Tipo" HeaderText="Tipo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Stock" HeaderText="Stock" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Partida" HeaderText="Partida" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" HtmlEncode="false" />
                                    <asp:BoundField DataField="Existencia" HeaderText="Exist" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Comprometido" HeaderText="Comp" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Disponible" HeaderText="Disp" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

