﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Rpt_Alm_Orden_Entrada.aspx.cs" Inherits="paginas_Almacen_Frm_Rpt_Alm_Orden_Entrada" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        function Limpiar_Ctlr_Busqueda() {
            document.getElementById("<%=Txt_Fecha_Inicio.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Fin.ClientID%>").value = "";
            document.getElementById("<%=Txt_No_OrdenE_Buscar.ClientID%>").value = "";
            document.getElementById("<%=Txt_OrdenC_Buscar.ClientID%>").value = "";
            document.getElementById("<%=Txt_Req_Buscar.ClientID%>").value = "";
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <%--<asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
            </asp:UpdateProgress>--%>
            <div id="Div_Principal" runat="server" style="height: 900px">
                <table width="97%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo">
                            Órdenes de Entrada
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" style="font-size: 12px; color: Red; font-family: Tahoma; text-align: left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" Width="100%" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td style="width: 20%;">
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                Width="24px" CssClass="Img_Button" ToolTip="Exportar PDF" OnClick="Btn_Imprimir_Click" />
                        </td>
                    </tr>
                </table>
                <div id="Div_Busqueda" runat="server">
                    <table width="100%">
                        <tr>
                            <td align="left" style="width: 20%;">
                                <asp:CheckBox ID="Chk_Dependencia_B" runat="server" Text="Unidad Responsable" />
                            </td>
                            <td align="left" style="width: 80%;">
                                <asp:DropDownList ID="Cmb_Dependencia" runat="server" Width="97%" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="120px" Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="Img_Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                                <cc1:CalendarExtender ID="Img_Btn_Fecha_Inicio_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicio"
                                    PopupButtonID="Img_Btn_Fecha_Inicio" Format="dd / MMMM / yyyy">
                                </cc1:CalendarExtender>
                                &nbsp;
                                <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Width="120px" Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="Img_Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Final" />
                                <cc1:CalendarExtender ID="Img_Btn_Fecha_Fin_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Fin"
                                    PopupButtonID="Img_Btn_Fecha_Fin" Format="dd / MMMM / yyyy">
                                </cc1:CalendarExtender>
                                &nbsp;
                                <asp:TextBox ID="Txt_No_OrdenE_Buscar" runat="server" MaxLength="10" Width="120px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_No_OrdenE_Buscar"
                                    InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                                    WatermarkText="<No. Orden Entrada>" TargetControlID="Txt_No_OrdenE_Buscar" />
                                &nbsp;
                                <asp:TextBox ID="Txt_OrdenC_Buscar" runat="server" MaxLength="10" Width="120px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" TargetControlID="Txt_Req_Buscar"
                                    InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" WatermarkCssClass="watermarked"
                                    WatermarkText="<No. Requisición>" TargetControlID="Txt_Req_Buscar" />
                                &nbsp;
                                <asp:TextBox ID="Txt_Req_Buscar" runat="server" MaxLength="10" Width="120px"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" runat="server" TargetControlID="Txt_OrdenC_Buscar"
                                    InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" WatermarkCssClass="watermarked"
                                    WatermarkText="<No. Orden Compra>" TargetControlID="Txt_OrdenC_Buscar" />
                                &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    ToolTip="Buscar" AlternateText="CONSULTAR" OnClick="Btn_Buscar_Click" />
                                <asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                    ToolTip="Limpiar" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" />
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="Grid_Ordenes_Entrada" runat="server" AutoGenerateColumns="False"
                        CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Ordenes_Entrada_PageIndexChanging"
                        AllowPaging="true" PageSize="30" Width="98%" OnSelectedIndexChanged="Grid_Ordenes_Entrada_SelectedIndexChanged" >
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                Text="Ver">
                                <ItemStyle Width="3%" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="NO_ENTRADA" HeaderText="No. Entrada">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="No. OC">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FOLIO" HeaderText="Requisición">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Solicito">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" DataFormatString="{0:dd/MMM/yyyy HH:mm:ss tt}">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="TOTAL" HeaderText="Total">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
                <div id="Div_Orden_Compra" runat="server">
                    <table width="97%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:10%;" >
                                No. OE
                            </td>
                            <td style="width:15%;">
                                <asp:TextBox ID="Txt_Orden_Entrada" runat="server" ReadOnly="true"  Width="80px" />
                            </td>
                            <td style="width:8%;" >
                                No. OC
                            </td>
                            <td style="width:15%;" >
                                <asp:TextBox ID="Txt_Orden_Compra" runat="server" ReadOnly="true" Width="80px" />
                            </td>
                            <td style="width:10%;">
                                Solicito
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Solicito" runat="server" ReadOnly="true" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:10%;" >
                                RQ.
                            </td>
                            <td style="width:15%;" >
                                <asp:TextBox ID="Txt_Requisicion" runat="server" ReadOnly="true" Width="80px" />
                            </td>
                            <td style="width:8%;" >
                                Estatus
                            </td>
                            <td style="width:15%;" >
                                <asp:TextBox ID="Txt_Estatus" runat="server" ReadOnly="true" Width="80px" />
                            </td>
                            <td style="width:10%;">
                                Recibio
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Recibio" runat="server" ReadOnly="true" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:10%;" >
                                Comentario
                            </td>
                            <td colspan="5">
                                <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" ReadOnly="true" Width="100%" />
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="Grid_Productos" runat="server" AutoGenerateColumns="False"
                        CssClass="GridView_1" GridLines="None" PageSize="30" Width="98%" >
                        <Columns>
                            <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NOMBRE" HeaderText="Producto">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CANTIDAD" HeaderText="Ctd.">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ABREVIATURA" HeaderText="Unidad.">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COSTO_PROMEDIO" HeaderText="$ Unitario" DataFormatString="{0:C}" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                            <asp:BoundField DataField="COSTO_COMPRA" HeaderText="Importe" DataFormatString="{0:C}" >
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
