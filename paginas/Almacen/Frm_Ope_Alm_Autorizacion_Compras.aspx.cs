﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Ope_Alm_Autorizacion_Compras.Negocio;

public partial class paginas_Almacen_Frm_Ope_Alm_Autorizacion_Compras : System.Web.UI.Page
{
    #region PAGE LOAD
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Page_Load
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 26/Diciembre/2012 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                //Establecer la fechas inicial y final
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                _DateTime = _DateTime.AddMonths(-1);
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                //Estado Inicial
                Autorizacion_Inicio();
                Acciones();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }
    #endregion

    #region METODOS

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Autorizacion_Inicio
    ///DESCRIPCIÓN          : Inicio de la pagina
    ///PROPIEDADES          :
    ///CREO                 : David Herrera Rincon
    ///FECHA_CREO           : 28/Enero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Autorizacion_Inicio()
    {
        try
        {
            Limpiar_Controles();            
            Llenar_Grid_Contrarecibos();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de autorización de compras Error[" + Ex.Message + "]");
        }
    }

    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
    ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
    ///PROPIEDADES          :
    ///CREO                 : David Herrera Rincon
    ///FECHA_CREO           : 28/Enero/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Hf_No_Contrarecibo_Autorizar.Value = "";
            Hf_Rechazo.Value = "";
            Txt_Comentario.Text = "";
            Grid_Solicitud_Contrarecibo.DataSource = null;
            Grid_Solicitud_Contrarecibo.DataBind();            
            Txt_Folio_Busqueda.Text = "";
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Grid_Contrarecibos
    /// DESCRIPCION : Llena el grid Solicitudes de contrarecibos
    /// PARAMETROS  : 
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 28/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Grid_Contrarecibos()
    {
        try
        {
            Cls_Ope_Alm_Autorizacion_Compras_Negocio Autorizar_Negocio = new Cls_Ope_Alm_Autorizacion_Compras_Negocio();
            DataTable Dt_Cheques = new DataTable();
            //Verificar si tiene algo el filtro de No_Cheque
            if (!String.IsNullOrEmpty(Txt_Folio_Busqueda.Text))
            {
                Autorizar_Negocio.P_No_Contra_Recibo = Txt_Folio_Busqueda.Text.ToString().Trim();
            }            
            //Obtener los fitlros de fecha
            Autorizar_Negocio.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
            Autorizar_Negocio.P_Fecha_Fin = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);

            Dt_Cheques = Autorizar_Negocio.Consultar_Contra_Recibos();
            Grid_Solicitud_Contrarecibo.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
            Grid_Solicitud_Contrarecibo.DataBind();    // Se ligan los datos.;
            if (Dt_Cheques.Rows.Count > 0)
            {
                Grid_Solicitud_Contrarecibo.Columns[0].Visible = true;
                Grid_Solicitud_Contrarecibo.DataSource = Dt_Cheques;   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Contrarecibo.DataBind();    // Se ligan los datos.;
                //Grid_Solicitud_Contrarecibo.Columns[0].Visible = false;
            }            
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
        }
    }

    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:Accion
    //'DESCRIPCION : realiza la modificacion la preautorización del contrarecibo o el rechazo del contrarecibo
    //'PARAMETROS  : 
    //'CREO        : David Herrera Rincon
    //'FECHA_CREO  : 28/Enero/2013
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Acciones()
    {
        String Accion = String.Empty;
        String No_Contra_Recibo = String.Empty;
        String Comentario = String.Empty;
        DataTable Dt_Consulta_Estatus = new DataTable();
        if (Request.QueryString["Accion"] != null)
        {
            Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
            if (Request.QueryString["id"] != null)
            {
                No_Contra_Recibo = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
            }
            if (Request.QueryString["x"] != null)
            {
                Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
            }
            //Response.Clear()
            switch (Accion)
            {
                case "Autorizar_Contra_Recibo":
                    Cls_Ope_Alm_Autorizacion_Compras_Negocio Autorizar_Negocio = new Cls_Ope_Alm_Autorizacion_Compras_Negocio();
                    Autorizar_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
                    Autorizar_Negocio.P_Estatus_Salida = "GENERADO";
                    String Mensaje = Autorizar_Negocio.Autorizar_Contra_Recibo();
                    
                    break;
                case "Rechazar_Solicitud":
                    Cancelar_Contra_Recibo(No_Contra_Recibo, Comentario);
                    break;
            }
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cancelar_Contra_Recibo
    /// DESCRIPCION : Modifica los datos del contrarecibo para actualizarlo
    /// PARAMETROS  : 
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 28/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cancelar_Contra_Recibo(String No_Contra_Recibo, String Comentario)
    {
        Cls_Ope_Alm_Autorizacion_Compras_Negocio Autorizar_Negocio = new Cls_Ope_Alm_Autorizacion_Compras_Negocio();
        try
        {
            Autorizar_Negocio.P_No_Contra_Recibo = No_Contra_Recibo;
            Autorizar_Negocio.P_Comentarios = Comentario;

            String Mensaje = Autorizar_Negocio.Rechazar_Contra_Recibo();
            
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
    }    
    
    #endregion

    #region EVENTOS

    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        try
        {
            Autorizacion_Inicio();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
        }
    }

    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Alm_Autorizacion_Compras_Negocio Autorizar_Cheques_Negocio = new Cls_Ope_Alm_Autorizacion_Compras_Negocio(); //Variable de conexión hacia la capa de Negocios
        try
        {
            if (!String.IsNullOrEmpty(Txt_Comentario.Text.ToString()))
            {
                Cancelar_Contra_Recibo(Hf_No_Contrarecibo_Autorizar.Value, Txt_Comentario.Text);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Autorización de contrarecibos", "alert('La cancelación del contrarecibo fue exitosa');", true);
                Autorizacion_Inicio();
            }
            else
            {
                Autorizacion_Inicio();
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    #region eventos
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton de salir
    ///PROPIEDADES          :
    ///CREO                 : David Herrera rincon
    ///FECHA_CREO           : 28/Enero/2013 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Autorizacion_Inicio();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
       
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_No_Contra_Recibo_Click
    ///DESCRIPCIÓN          : Evento del Boton_Buscar
    ///PROPIEDADES          :
    ///CREO                 : David Herrera rincon
    ///FECHA_CREO           : 28/Enero/2013 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Buscar_No_Contra_Recibo_Click(object sender, ImageClickEventArgs e)
    {
        try 
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Llenar_Grid_Contrarecibos();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region"Grid"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Solicitud_Contrarecibo_RowDataBound
    /// DESCRIPCION : 
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 28/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Solicitud_Contrarecibo_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
            CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazado");
            if (e.Row.RowType == DataControlRowType.DataRow)
            {                
                chek.Enabled = true;
                chek2.Enabled = true;            
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion

    #endregion
}

