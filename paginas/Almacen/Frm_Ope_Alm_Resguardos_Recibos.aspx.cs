﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Xml.Linq;
using JAPAMI.Resguardos_Recibos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Reportes;
using System.Collections.Generic;
using JAPAMI.Almacen_Resguardos.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;

public partial class paginas_Frm_Ope_Alm_Resguardos_Recibos : System.Web.UI.Page
{

    #region Variables Globales

    Cls_Ope_Alm_Resguardos_Recibos_Negocio Resguardo_Recibo;

    #endregion

    # region Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN:          Page_Load
    ///PARAMETROS: 
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        if (String.IsNullOrEmpty(Cls_Sessiones.Empleado_ID) || String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado))
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Estatus_Inicial();
        }

    }

    #endregion

    # region  Métodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Procedencias
    ///DESCRIPCIÓN: Llena el combo de Procedencias.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 07/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Procedencias()
    {
        Cls_Cat_Pat_Com_Procedencias_Negocio Procedencia_Negocio = new Cls_Cat_Pat_Com_Procedencias_Negocio();
        Procedencia_Negocio.P_Estatus = "VIGENTE";
        Procedencia_Negocio.P_Tipo_DataTable = "PROCEDENCIAS";
        Cmb_Procedencia.DataSource = Procedencia_Negocio.Consultar_DataTable();
        Cmb_Procedencia.DataTextField = "NOMBRE";
        Cmb_Procedencia.DataValueField = "PROCEDENCIA_ID";
        Cmb_Procedencia.DataBind();
        Cmb_Procedencia.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Ubicar_Gerencia
    ///DESCRIPCIÓN: Coloca el combo gerencia en la que le corresponde al empleado seleccionado del grid de búsqueda
    ///PROPIEDADES: el identificador de la dependencia para buscar la gerencia
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Ubicar_Gerencia(String Dependencia_ID)
    {

        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";// Se llena el combo de dependencias para obtener la gerencia ID y asi colocar la gerencia a la que pertence el empleado seleccionado en la búsqueda
        DataTable Dependencias = Combos.Consultar_DataTable();
        DataRow DR_Fila;
        String Gerencia_ID = "";//variable para almacenar la gerencia_ID cuando se encuentre
        for (int i = 0; i < Dependencias.Rows.Count; i++)
        {//Ciclo para encontrar la dependencia_ID y obtener la gerencia_id cuando se encuentre
            DR_Fila = Dependencias.Rows[i];
            if (DR_Fila["DEPENDENCIA_ID"].ToString().Equals(Dependencia_ID))
            {
                Gerencia_ID = DR_Fila["GERENCIA_ID"].ToString();
            }
        }
        if (!String.IsNullOrEmpty(Gerencia_ID))//se valida que contenga valor la variable gerencia_id
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Gerencia_ID));//Se coloca el combo gerencia en la posicion encontrada encontrada

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Empleados
    ///DESCRIPCIÓN:          Metodo utilizado para consultar y llenar el combo con los empleados que pertenecen a la dependencia
    ///PROPIEDADES:     
    ///                      1.  Dependencia_ID. el identificador de la dependencia en base a la que se va hacer la consulta
    ///              
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO: 02/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Consultar_Empleados(String Dependencia_ID)
    {
        try
        {
            if (Cmb_Unidad_Responsable.SelectedIndex > 0)
            {
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combo = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Combo.P_Tipo_DataTable = "EMPLEADOS";
                Combo.P_Dependencia_ID = Dependencia_ID;
                DataTable Tabla = Combo.Consultar_DataTable();
                Llenar_Combo_Empleados(Tabla);
            }
            else
            {
                DataTable Tabla = new DataTable();
                Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                Tabla.Columns.Add("NOMBRE", Type.GetType("System.String"));
                Llenar_Combo_Empleados(Tabla);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
    ///DESCRIPCIÓN: Llena el combo de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados(DataTable Tabla)
    {
        try
        {
            DataRow Fila_Empleado = Tabla.NewRow();
            Fila_Empleado["EMPLEADO_ID"] = "SELECCIONE";
            Fila_Empleado["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Tabla.Rows.InsertAt(Fila_Empleado, 0);
            Cmb_Responsable.DataSource = Tabla;
            Cmb_Responsable.DataValueField = "EMPLEADO_ID";
            Cmb_Responsable.DataTextField = "NOMBRE";
            Cmb_Responsable.DataBind();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
    ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias(String Gerencia_ID)
    {
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Gerencia_ID = Gerencia_ID;
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";
        DataTable Dependencias = Combos.Consultar_DataTable();
        Cmb_Unidad_Responsable.DataSource = Dependencias;//Se especifica la fuente de información que llenará el combo
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";//Se especifica el campo de la tabla dependencias que contendrá el valor de los renglones del combo
        Cmb_Unidad_Responsable.DataTextField = "NOMBRE";//Se especifica el campo de la tabla dependencias que contendrá el texto que mostrará el combo
        Cmb_Unidad_Responsable.DataBind();//Se unen los datos del combo
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));//Se inserta el renglón de <-SELECCIONE-> en la posición 0
        Cmb_Unidad_Responsable_SelectedIndexChanged(Cmb_Unidad_Responsable, null);
    }

    ///*******************************************************************************
    /// NOMBRE DE LA CLASE:     Estado_Inicial_Busqueda_Avanzada
    /// DESCRIPCION:            Colocar la ventana de la busqueda avanzada en un estado inicial
    /// PARAMETROS :            
    /// CREO       :            Salvador Hernández Ramírez
    /// FECHA_CREO :            19/Marzo/2010  
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************/
    private void Estado_Inicial_Busqueda_Avanzada()
    {
        try
        {
            Txt_Fecha_Inicio.Text = "";
            Txt_Fecha_Fin.Text = "";
            Txt_Busqueda.Text = "";
            Txt_Req_Buscar.Text = "";
            Chk_Fecha_B.Checked = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:           1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           15/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataTable Data_Set_Consulta_DB, DataSet Ds_Reporte, String Formato, String Operacion)
    {

        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;

        try
        {
            Renglon = Data_Set_Consulta_DB.Rows[0];
            String Cantidad = Data_Set_Consulta_DB.Rows[0]["CANTIDAD"].ToString();
            String Costo = Data_Set_Consulta_DB.Rows[0]["COSTO_UNITARIO"].ToString();
            Double Resultado = (Convert.ToDouble(Cantidad)) * (Convert.ToDouble(Costo));
            Ds_Reporte.Tables[1].ImportRow(Renglon);
            Ds_Reporte.Tables[1].Rows[0].SetField("COSTO_TOTAL", ((!String.IsNullOrEmpty(Data_Set_Consulta_DB.Rows[0]["COSTO_TOTAL"].ToString()) ? Data_Set_Consulta_DB.Rows[0]["COSTO_TOTAL"].ToString() : "0")));

            if (Operacion != null)
            {
                if (Operacion.ToString().Trim() == "RESGUARDO")
                    Ds_Reporte.Tables[1].Rows[0].SetField("OPERACION", "CONTROL DE RESGUARDOS DE BIENES MUEBLES");

                else if (Operacion.ToString().Trim() == "CUSTODIA")
                    Ds_Reporte.Tables[1].Rows[0].SetField("OPERACION", "CONTROL DE CUSTODIAS DE BIENES MUEBLES");
            }

            if ((string.IsNullOrEmpty(Ds_Reporte.Tables[1].Rows[0]["MARCA"].ToString())) | (Ds_Reporte.Tables[1].Rows[0]["MARCA"].ToString().Trim() == ""))
                Ds_Reporte.Tables[1].Rows[0].SetField("MARCA", "INDISTINTA");

            if (string.IsNullOrEmpty(Ds_Reporte.Tables[1].Rows[0]["MODELO"].ToString()))
                Ds_Reporte.Tables[1].Rows[0].SetField("MODELO", "INDISTINTO");

            for (int Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Rows.Count; Cont_Elementos++)
            {
                Renglon = Data_Set_Consulta_DB.Rows[Cont_Elementos];
                String No_Empleado = Data_Set_Consulta_DB.Rows[Cont_Elementos]["NO_EMPLEADO"].ToString();
                String Nombre_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["NOMBRE_E"].ToString();
                String Apellido_Paterno_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["APELLIDO_PATERNO_E"].ToString();
                String Apellido_Materno_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["APELLIDO_MATERNO_E"].ToString();
                String RFC_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["RFC_E"].ToString();
                String Resguardante = Nombre_E + " " + Apellido_Paterno_E + " " + Apellido_Materno_E + " " + "(" + RFC_E + ")";
                if (!Resguardante.Trim().Equals("()"))
                {
                    Ds_Reporte.Tables[0].ImportRow(Renglon);
                    Ds_Reporte.Tables[0].Rows[Cont_Elementos].SetField("RESGUARDANTES", "[" + No_Empleado.Trim() + "] " + Resguardante);
                }
                else
                {
                    Ds_Reporte.Tables[0].ImportRow(Renglon);
                }
            }
            // Ruta donde se encuentra el reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Almacen/Rpt_Alm_Com_Resguardos_Bienes_Almacen.rpt";

            // Se crea el nombre del reporte
            //String Nombre_Reporte;

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = "Rpt_Alm_Com_Resguardos_Bienes_Almacen" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = "Rpt_Alm_Com_Resguardos_Bienes_Almacen" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        try
        {
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Estatus_Inicial
    ///DESCRIPCIÓN:          Se establece el estatus inicial de la página
    ///PARAMETROS: 
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estatus_Inicial()
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Div_Ordenes_Compra.Visible = false;
        Div_Detalles_Orden_Compra.Visible = false;
        try
        {
            Estatus_Inicial_Botones(false);
            Cargar_Ordenes_Compra();
            Llenar_Combo_Dependencias();
            Llenar_Combo_Procedencias();
            Llenar_Combos();
            Div_Busqueda_Av.Visible = true;
            Limpiar_Campos_Formulario();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Ordenes_Compra
    ///DESCRIPCIÓN:     Se consultan las ordenes de compra
    ///PARAMETROS: 

    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Ordenes_Compra()
    {
        Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();
        DataTable Dt_Ordenes_Compra = new DataTable();

        try
        {
            if (Txt_Busqueda.Text.Trim() != "")
                Resguardo_Recibo.P_No_Orden_Compra = Txt_Busqueda.Text.Trim();
            else
                Resguardo_Recibo.P_No_Orden_Compra = null;

            if (Txt_Req_Buscar.Text.Trim() != "")
                Resguardo_Recibo.P_No_Requisicion = Txt_Req_Buscar.Text.Trim();
            else
                Resguardo_Recibo.P_No_Requisicion = null;

            if (Chk_Fecha_B.Checked) // Si esta activado el Check
            {
                DateTime Date1 = new DateTime();  //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
                DateTime Date2 = new DateTime();

                if ((Txt_Fecha_Inicio.Text.Length != 0))
                {
                    if ((Txt_Fecha_Inicio.Text.Length == 11) && (Txt_Fecha_Fin.Text.Length == 11))
                    {
                        //Convertimos el Texto de los TextBox fecha a dateTime
                        Date1 = DateTime.Parse(Txt_Fecha_Inicio.Text);
                        Date2 = DateTime.Parse(Txt_Fecha_Fin.Text);

                        //Validamos que las fechas sean iguales o la final sea mayor que la inicial, de lo contrario se manda un mensaje de error 
                        if ((Date1 < Date2) | (Date1 == Date2))
                        {
                            if (Txt_Fecha_Fin.Text.Length != 0)
                            {
                                //Se convierte la fecha seleccionada por el usuario a un formato valido por oracle. 
                                Resguardo_Recibo.P_Fecha_Inicio_B = Formato_Fecha(Txt_Fecha_Inicio.Text.Trim());
                                Resguardo_Recibo.P_Fecha_Fin_B = Formato_Fecha(Txt_Fecha_Fin.Text.Trim());
                                Div_Contenedor_Msj_Error.Visible = false;
                            }
                            else
                            {
                                String Fecha = Formato_Fecha(Txt_Fecha_Inicio.Text.Trim()); //Se convierte la fecha seleccionada por el usuario a un formato valido por oracle. 
                                Resguardo_Recibo.P_Fecha_Inicio_B = Fecha;
                                Resguardo_Recibo.P_Fecha_Fin_B = Fecha;
                                Div_Contenedor_Msj_Error.Visible = false;
                            }
                        }
                        else
                        {
                            Lbl_Informacion.Text = " Fecha no valida ";
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                    }
                    else
                    {
                        Lbl_Informacion.Text = " Fecha no valida ";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
            }
            Dt_Ordenes_Compra = Resguardo_Recibo.Consultar_Ordenes_Compra();

            if (Dt_Ordenes_Compra.Rows.Count > 0) // Si la tabla contiene datos
            {
                Session["Dt_Ordenes_Compra"] = Dt_Ordenes_Compra;

                Grid_Ordenes_Compra.Columns[7].Visible = true;
                Grid_Ordenes_Compra.Columns[8].Visible = true; // No_Contra Recibo
                Grid_Ordenes_Compra.DataSource = Dt_Ordenes_Compra;
                Grid_Ordenes_Compra.DataBind();
                Grid_Ordenes_Compra.Columns[7].Visible = false;
                Grid_Ordenes_Compra.Columns[8].Visible = false; // No Contra Recibo
                Div_Ordenes_Compra.Visible = true;
                Div_Detalles_Orden_Compra.Visible = false;
                Div_Contenedor_Msj_Error.Visible = false;
            }
            else
            {
                Div_Detalles_Orden_Compra.Visible = false;
                Div_Ordenes_Compra.Visible = false;
                Lbl_Informacion.Text = " No se encontraron productos a resguardar";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Fecha
    ///DESCRIPCIÓN:     Metodo que cambia el mes dic a dec para que oracle lo acepte
    ///PARAMETROS:      1.- String Fecha, es la fecha a la cual se le cambiara el formato 
    ///                     en caso de que cumpla la condicion del if
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public String Formato_Fecha(String Fecha)
    {
        String Fecha_Valida = Fecha;
        //Se le aplica un split a la fecha 
        String[] aux = Fecha.Split('/');
        //Se modifica a mayusculas para que oracle acepte el formato. 
        switch (aux[1])
        {
            case "dic":
                aux[1] = "DEC";
                break;
        }
        //Concatenamos la fecha, y se cambia el orden a DD-MMM-YYYY para que sea una fecha valida para oracle
        Fecha_Valida = aux[0] + "-" + aux[1] + "-" + aux[2];
        return Fecha_Valida;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Productos_Requisicion
    ///DESCRIPCIÓN:          Método utilizado para consultar los productos de la requisición
    ///PROPIEDADES:          No_Orden_Compra: El numero de la orden de compra
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           29-Julio-11 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Consultar_Productos_Requisicion(String No_Orden_Compra)
    {
        Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();
        DataTable Dt_Productos = new DataTable();

        try
        {
            Resguardo_Recibo.P_No_Orden_Compra = No_Orden_Compra.Trim();
            Dt_Productos = Resguardo_Recibo.Consultar_Productos_Requisicion();

            for (int i = 0; i < Dt_Productos.Rows.Count; i++)
            {
                String Resguardo = Dt_Productos.Rows[i]["RESGUARDO"].ToString().Trim();
                String Recibo = Dt_Productos.Rows[i]["CUSTODIA"].ToString().Trim();

                if (Resguardo == "SI")
                    Dt_Productos.Rows[i].SetField("OPERACION", "RESGUARDO");
                else if (Recibo == "SI")
                    Dt_Productos.Rows[i].SetField("OPERACION", "CUSTODIA");
            }

            if (Dt_Productos.Rows.Count > 0) // Si la tabla contiene datos
            {
                Grid_Productos.Columns[1].Visible = true;
                Grid_Productos.Columns[4].Visible = true;
                Grid_Productos.DataSource = Dt_Productos;
                Grid_Productos.DataBind();
                Grid_Productos.Columns[1].Visible = false;
                Grid_Productos.Columns[4].Visible = false;

                for (int i = 0; i < Grid_Productos.Rows.Count; i++) // Se agrega el numero de registro
                {
                    Dt_Productos.Rows[i].SetField("NO_REGISTRO", i); // Se agrega un numero de registro
                    String Resguardado = "";

                    if (Dt_Productos.Rows[i]["RESGUARDADO"].ToString().Trim() != "") // Se optiene el valor de la tabla para determinar si ya esta resgaurdado el bien
                        Resguardado = Dt_Productos.Rows[i]["RESGUARDADO"].ToString().Trim();

                    if (Resguardado == "SI")
                    {
                        Grid_Productos.Rows[i].Cells[0].Enabled = false; // Se deshabilita el registro donde esta el chech
                        Grid_Productos.Rows[i].FindControl("Btn_Imprimir").Visible = false;
                    }
                    else if ((Resguardado == "") | (Resguardado == "NO"))
                    {
                        Grid_Productos.Rows[i].Cells[0].Enabled = true; // Se deshabilita el registro donde esta el chech
                        Grid_Productos.Rows[i].FindControl("Btn_Imprimir_Resguardo").Visible = false;
                        Grid_Productos.Rows[i].FindControl("Btn_Imprimir").Visible = false;
                    }
                }

                Session["Dt_Productos_RR"] = Dt_Productos;  // Se agrega la tabla a la variable de session

                Div_Contenedor_Msj_Error.Visible = false;
                Grid_Productos.Visible = true;
                Div_Detalles_Orden_Compra.Visible = true;
                Div_Ordenes_Compra.Visible = false;
                Estatus_Inicial_Botones(true);
            }
            else
            {
                Lbl_Informacion.Text = " No se encontraron productos a resguardar";
                Div_Contenedor_Msj_Error.Visible = true;
                Div_Detalles_Orden_Compra.Visible = false;
                Div_Ordenes_Compra.Visible = true;
                Div_Busqueda_Av.Visible = true;
                Estatus_Inicial_Botones(false);
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo de Dependencias.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 15/Marzo/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias()
    {
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();

        try
        {
            Resguardo_Recibo.P_Tipo_Combo = "DEPENDENCIAS";
            DataTable Dependencias = Resguardo_Recibo.Llenar_Combo();

            if (Dependencias.Rows.Count > 0)
            {
                DataRow Fila_Dependencia = Dependencias.NewRow();
                Fila_Dependencia["DEPENDENCIA_ID"] = "";
                Fila_Dependencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
                Dependencias.Rows.InsertAt(Fila_Dependencia, 0);
                Cmb_Busqueda_Dependencia.DataSource = Dependencias;
                Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
                Cmb_Busqueda_Dependencia.DataTextField = "NOMBRE";
                Cmb_Busqueda_Dependencia.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Estatus_Inicial_Botones
    ///DESCRIPCIÓN:          Método utilizado asignarle el estatus correspondiente a los botones
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           24/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Estatus_Inicial_Botones(Boolean Estatus)
    {
        if (Estatus)
        {
            Btn_Salir.AlternateText = "Atras";
            Btn_Salir.ToolTip = "Atras";
        }
        else
        {
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ToolTip = "Salir";
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
    ///DESCRIPCIÓN: Llena el combo de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados(String Unidad_ResponSable)
    {
        // SE LLENA EL COMBO DE DEPENDENCIAS
        Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();

        try
        {
            Resguardo_Recibo.P_Unidad_Responsable_ID = Unidad_ResponSable.Trim();
            Resguardo_Recibo.P_Tipo_Combo = "EMPLEADOS";
            DataTable Tabla = Resguardo_Recibo.Llenar_Combo();

            if (Tabla.Rows.Count > 0)
            {
                DataRow Fila_Empleado = Tabla.NewRow();
                Fila_Empleado["EMPLEADO_ID"] = "SELECCIONE";
                Fila_Empleado["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
                Tabla.Rows.InsertAt(Fila_Empleado, 0);
                Cmb_Responsable.DataSource = Tabla;
                Cmb_Responsable.DataValueField = "EMPLEADO_ID";
                Cmb_Responsable.DataTextField = "NOMBRE";
                Cmb_Responsable.DataBind();
            }
            else
            {
                Cmb_Responsable.Items.Clear();
                Cmb_Responsable.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
            }
        }
        catch (Exception Ex)
        {
            //Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            //Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Resguardo_Recibo
    ///DESCRIPCIÓN:          Metodo utilizado para consultaro los datos generales del resguardo o recibo
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           30-Julio-11 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Consultar_Datos_Resguardo_Recibo(String No_Inventario)
    {
        try{
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            Mueble.P_Numero_Inventario = No_Inventario.Trim();
            Mueble = Mueble.Consultar_Detalles_Bien_Mueble();
            Llenar_DataSet_Resguardos_Bienes(Mueble);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Datos_Resguardo_Recibo_Vehiculo
    ///DESCRIPCIÓN:          Metodo utilizado para consultaro los datos generales del resguardo o recibo
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           30-Julio-11 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Consultar_Datos_Resguardo_Recibo_Vehiculo(String No_Inventario)
    {
        try
        {
            Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
            Vehiculo.P_Numero_Inventario = Convert.ToInt64(No_Inventario);
            Vehiculo.P_Buscar_Numero_Inventario = true;
            Vehiculo = Vehiculo.Consultar_Detalles_Vehiculo();
            Llenar_DataSet_Resguardos_Vehiculos(Vehiculo);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_DataSet_Resguardos_Bienes()
    ///DESCRIPCIÓN: Llena el dataSet "Data_Set_Resguardos_Bienes" con las personas a las que se les asigno el
    ///bien mueble y sus detalles, para que con estos datos se genere el reporte.
    ///PARAMETROS:  
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 17/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_DataSet_Resguardos_Bienes(Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Id)
    {
        String Formato = "PDF";
        Cls_Alm_Com_Resguardos_Negocio Consulta_Resguardos_Negocio = new Cls_Alm_Com_Resguardos_Negocio();
        Bien_Id.P_Producto_Almacen = false;
        DataTable Data_Set_Resguardos_Bienes;
        Consulta_Resguardos_Negocio.P_Operacion = Bien_Id.P_Operacion.Trim();
        Data_Set_Resguardos_Bienes = Consulta_Resguardos_Negocio.Consulta_Resguardos_Bienes(Bien_Id);
        Ds_Alm_Com_Resguardos_Bienes Ds_Consulta_Resguardos_Bienes = new Ds_Alm_Com_Resguardos_Bienes();
        Generar_Reporte(Data_Set_Resguardos_Bienes, Ds_Consulta_Resguardos_Bienes, Formato, Consulta_Resguardos_Negocio.P_Operacion);
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_DataSet_Resguardos_Vehiculos
    ///DESCRIPCIÓN: Llena el dataSet "Data_Set_Resguardos_Vehiculos" con las personas a las que se les asigno el
    ///vehiculo, sus detalles generales y especificos, para que con estos datos se genere el reporte.
    ///PARAMETROS:  
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 23/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_DataSet_Resguardos_Vehiculos(Cls_Ope_Pat_Com_Vehiculos_Negocio Id_Vehiculo)
    {
        try
        {

            String Formato = "PDF";
            Cls_Alm_Com_Resguardos_Negocio Consulta_Resguardos_Vehiculos = new Cls_Alm_Com_Resguardos_Negocio();
            DataSet Data_Set_Resguardos_Vehiculos, Data_Set_Vehiculos_Asegurados;
            Id_Vehiculo.P_Producto_Almacen = false;
            Data_Set_Resguardos_Vehiculos = Consulta_Resguardos_Vehiculos.Consulta_Resguardos_Vehiculos(Id_Vehiculo);
            Data_Set_Vehiculos_Asegurados = Consulta_Resguardos_Vehiculos.Consulta_Vehiculos_Asegurados(Id_Vehiculo);
            Ds_Alm_Com_Resguardos_Vehiculos Ds_Consulta_Resguardos_Vehiculos = new Ds_Alm_Com_Resguardos_Vehiculos();
            Generar_Reporte_Vehiculos(Data_Set_Vehiculos_Asegurados, Data_Set_Resguardos_Vehiculos, Ds_Consulta_Resguardos_Vehiculos, Formato);
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Vehiculos
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 15/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Vehiculos(DataSet Data_Set_Consulta_Vehiculos_A, DataSet Data_Set_Consulta_Resguardos_V, DataSet Ds_Reporte, String Formato)
    {
        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;

        try
        {
            if (Data_Set_Consulta_Resguardos_V.Tables[0].Rows.Count > 0)
            {
                String Cantidad = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0]["CANTIDAD"].ToString();
                String Costo = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0]["COSTO_UNITARIO"].ToString();
                Double Resultado = (Convert.ToDouble(Cantidad)) * (Convert.ToDouble(Costo));

                String Total = "" + Resultado;
                Renglon = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0];
                Ds_Reporte.Tables[1].ImportRow(Renglon);
                Ds_Reporte.Tables[1].Rows[0].SetField("COSTO_TOTAL", Total);

                for (int Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_Resguardos_V.Tables[0].Rows.Count; Cont_Elementos++)
                {
                    Renglon = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]; //Instanciar renglon e importarlo

                    String No_Empleado = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["NO_EMPLEADO"].ToString();
                    String Nombre_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["NOMBRE_E"].ToString();
                    String Apellido_Paterno_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["APELLIDO_PATERNO_E"].ToString();
                    String Apellido_Materno_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["APELLIDO_MATERNO_E"].ToString();
                    String RFC_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["RFC_E"].ToString();
                    String Resguardante = Nombre_E + " " + Apellido_Paterno_E + " " + Apellido_Materno_E + " " + "(" + RFC_E + ")";
                    if (!Resguardante.Trim().Equals("()"))
                    {
                        Ds_Reporte.Tables[0].ImportRow(Renglon);
                        Ds_Reporte.Tables[0].Rows[Cont_Elementos].SetField("RESGUARDANTES", "[" + No_Empleado.Trim() + "] " + Resguardante);
                    }
                }

                if (Data_Set_Consulta_Vehiculos_A.Tables[0].Rows.Count > 0)
                {
                    String Nombre_Aeguradora = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["NOMBRE_ASEGURADORA"].ToString();
                    String No_Poliza = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["NO_POLIZA"].ToString();
                    String Descripcion_Seguro = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["DESCRIPCION_SEGURO"].ToString();
                    String Cobertura = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["COBERTURA"].ToString();
                    Ds_Reporte.Tables[1].Rows[0].SetField("NOMBRE_ASEGURADORA", Nombre_Aeguradora);
                    Ds_Reporte.Tables[1].Rows[0].SetField("NO_POLIZA", No_Poliza);
                    Ds_Reporte.Tables[1].Rows[0].SetField("DESCRIPCION_SEGURO", Descripcion_Seguro);
                    Ds_Reporte.Tables[1].Rows[0].SetField("COBERTURA", Cobertura);
                }
            }

            // Ruta donde se encuentra el Reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Compras/Rpt_Alm_Com_Resguardos_Vehiculos.rpt";

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = "Resguardo_Vehiculos" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = "Resguardo_Vehiculos" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            //Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
    ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
    ///PROPIEDADES:     
    ///CREO:                 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Busqueda_Empleados_Resguardo()
    {
        Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        Negocio.P_Estatus_Empleado = "ACTIVO";
        if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado_Resguardante = Txt_Busqueda_No_Empleado.Text.Trim(); }
        if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Resguardante = Txt_Busqueda_RFC.Text.Trim(); }
        if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Resguardante = Txt_Busqueda_Nombre_Empleado.Text.Trim(); }
        if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
        Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados_Resguardos();
        Grid_Busqueda_Empleados_Resguardo.DataBind();
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combos
    ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 29/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combos()
    {
        try
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();

            //SE LLENA EL COMBO DE GERENCIAS
            Combos.P_Tipo_DataTable = "GERENCIAS";
            DataTable Gerencias = Combos.Consultar_DataTable();
            DataRow Fila_Gerencia = Gerencias.NewRow();
            Fila_Gerencia["GERENCIA_ID"] = "SELECCIONE";
            Fila_Gerencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;- SELECCIONE -&gt;");
            Gerencias.Rows.InsertAt(Fila_Gerencia, 0);
            Cmb_Gerencias.DataSource = Gerencias;
            Cmb_Gerencias.DataValueField = "GERENCIA_ID";
            Cmb_Gerencias.DataTextField = "NOMBRE";
            Cmb_Gerencias.DataBind();
            Gerencias.Rows.RemoveAt(0);
            Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);

            Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            CA_Negocio.P_Estatus = "VIGENTE";
            CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
            CA_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
            Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
            Cmb_Clase_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Clase_Activo.DataBind();
            Cmb_Clase_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));

            Cls_Cat_Pat_Com_Clasificaciones_Negocio Clasificaciones_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            Clasificaciones_Negocio.P_Estatus = "VIGENTE";
            Clasificaciones_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
            Clasificaciones_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Tipo_Activo.DataSource = Clasificaciones_Negocio.Consultar_DataTable();
            Cmb_Tipo_Activo.DataValueField = "CLASIFICACION_ID";
            Cmb_Tipo_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Tipo_Activo.DataBind();
            Cmb_Tipo_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));

            Combos.P_Tipo_DataTable = "ZONAS";
            DataTable Zonas = Combos.Consultar_DataTable();
            Cmb_Zonas.DataSource = Zonas;
            Cmb_Zonas.DataTextField = "DESCRIPCION";
            Cmb_Zonas.DataValueField = "ZONA_ID";
            Cmb_Zonas.DataBind();
            Cmb_Zonas.Items.Insert(0, new ListItem("<--SELECCIONE-->", ""));


        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Campos_Formulario
    ///DESCRIPCIÓN: Limpiar_Campos_Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 29/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Campos_Formulario()
    {
        Txt_Orden_Compra.Text = "";
        Txt_Importe.Text = "";
        Txt_Requisicion.Text = "";
        Txt_Fecha_Surtido.Text = "";
        Hdf_Cuenta_Activo_ID.Value = "";
        Txt_Cuenta_Activo.Text = "";
        Hdf_Cuenta_Gasto_ID.Value = "";
        Txt_Cuenta_Gasto.Text = "";
        Txt_Proveedor.Text = "";
        Hdf_Proveedor_ID.Value = "";
        if (Cmb_Clase_Activo.Items.Count > 0) Cmb_Clase_Activo.SelectedIndex = 0;
        if (Cmb_Tipo_Activo.Items.Count > 0) Cmb_Tipo_Activo.SelectedIndex = 0;
        if (Cmb_Gerencias.Items.Count > 0) { Cmb_Gerencias.SelectedIndex = 0; Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null); }
        Txt_Observaciones.Text = "";
        Txt_Resguardo_Completo_Autorizo.Text = "";
        Hdf_Resguardo_Completo_Autorizo.Value = "";
        Txt_Resguardo_Completo_Funcionario_Recibe.Text = "";
        Hdf_Resguardo_Completo_Funcionario_Recibe.Value = "";
        Txt_Resguardo_Completo_Operador.Text = "";
        Hdf_Resguardo_Completo_Operador.Value = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
    }

    #endregion

    #region Grid

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
            {
                String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Hdf_Tipo_Busqueda.Value == null || Hdf_Tipo_Busqueda.Value.Trim().Length == 0)
                {
                    String Dependencia_ID = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString() : null;
                    Llenar_Combo_Dependencias("");//Llenar el combo dependencias con todas las dependencias registradas para buscar la del empleado seleccionado del grid
                    Int32 Index_Combo = (-1);
                    if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
                    {
                        Index_Combo = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dependencia_ID));
                        if (Index_Combo > (-1))
                        {
                            if (Index_Combo == Cmb_Unidad_Responsable.SelectedIndex)
                            {
                                Cmb_Responsable.SelectedIndex = Cmb_Responsable.Items.IndexOf(Cmb_Responsable.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));
                            }
                            else
                            {
                                Ubicar_Gerencia(Dependencia_ID);
                                Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue);
                                Cmb_Unidad_Responsable.SelectedIndex = Cmb_Unidad_Responsable.Items.IndexOf(Cmb_Unidad_Responsable.Items.FindByValue(Dependencia_ID));
                                Consultar_Empleados(Dependencia_ID);
                                Cmb_Responsable.SelectedIndex = Cmb_Responsable.Items.IndexOf(Cmb_Responsable.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));
                            }
                        }
                    }
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("OPERADOR"))
                {
                    Hdf_Resguardo_Completo_Operador.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : "";
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Operador.Text = Texto.Trim();
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("FUNCIONARIO_RECIBE"))
                {
                    Hdf_Resguardo_Completo_Funcionario_Recibe.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Funcionario_Recibe.Text = Texto.Trim();
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("AUTORIZO"))
                {
                    Hdf_Resguardo_Completo_Autorizo.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Autorizo.Text = Texto.Trim();
                }

                MPE_Resguardante.Hide();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    # region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Resguardante_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Avanzada_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:          Botón utilizado para salir de la aplicación o para configurar la pagina en su estado inicial
    ///PARAMETROS: 
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.AlternateText == "Salir")
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Estatus_Inicial();
            Estado_Inicial_Busqueda_Avanzada();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN:          Botón utilizado para buscar las ordenes de compra
    ///PARAMETROS: 
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Cargar_Ordenes_Compra();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Orden_Compra_Click
    ///DESCRIPCIÓN:     En este botón se  asignan los datos del resguardante al resguardo
    ///PARAMETROS: 
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Orden_Compra_Click(object sender, ImageClickEventArgs e)
    {
        Limpiar_Campos_Formulario();
        // Declaración de Objetos y Variables
        ImageButton Btn_Selec_Requisicion = null;
        Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio(); // Se crea el objeto para el manejo de los m{etodos
        String No_Orden_Compra = String.Empty;
        String No_Contra_Recibo = String.Empty;
        DataTable Dt_Produtos_OC = new DataTable();
        DataTable Dt_Ordenes_Compra = new DataTable();
        DataRow[] Dr_Orden;

        try
        {
            Btn_Selec_Requisicion = (ImageButton)sender;
            No_Orden_Compra = Btn_Selec_Requisicion.CommandArgument; // Se selecciona el No de Orden de Compra
            Session["NO_ORDEN_COMPRA"] = No_Orden_Compra.Trim(); // Se asigna a la variable de session

            if (Session["Dt_Ordenes_Compra"] != null) // Se carga la tabla con las Ordenes de Compra
                Dt_Ordenes_Compra = (DataTable)Session["Dt_Ordenes_Compra"];

            if (Dt_Ordenes_Compra.Rows.Count > 0) // Si la tabla contiene productos
            {
                Dr_Orden = Dt_Ordenes_Compra.Select("NO_ORDEN_COMPRA='" + No_Orden_Compra.Trim() + "'");

                if (Dr_Orden.Length > 0) // Si el renglon tiene información
                {
                    if (!string.IsNullOrEmpty(Dr_Orden[0]["PROVEEDOR"].ToString()))
                        Txt_Proveedor.Text = HttpUtility.HtmlDecode(Dr_Orden[0]["PROVEEDOR"].ToString().Trim());

                    if (!string.IsNullOrEmpty(Dr_Orden[0]["PROVEEDOR_ID"].ToString()))
                        Hdf_Proveedor_ID.Value = HttpUtility.HtmlDecode(Dr_Orden[0]["PROVEEDOR_ID"].ToString().Trim());

                    if (!string.IsNullOrEmpty(Dr_Orden[0]["FECHA"].ToString()))
                    {
                        String Fecha = HttpUtility.HtmlDecode(Dr_Orden[0]["FECHA"].ToString().Trim());  // Se optiene la fecha
                        DateTime Fecha_Convertida = Convert.ToDateTime(Fecha);  // Se conviente la fecha
                        Txt_Fecha_Surtido.Text = String.Format("{0:dd/MMM/yyyy}", Fecha_Convertida);
                    }
                    if (!string.IsNullOrEmpty(Dr_Orden[0]["NO_CONTRA_RECIBO"].ToString()))
                    {
                        No_Contra_Recibo = HttpUtility.HtmlDecode(Dr_Orden[0]["NO_CONTRA_RECIBO"].ToString().Trim());
                        Session["NO_CONTRA_RECIBO_RESG"] = No_Contra_Recibo.Trim();
                    }
                    if (!string.IsNullOrEmpty(Dr_Orden[0]["FOLIO_OC"].ToString()))
                        Txt_Orden_Compra.Text = HttpUtility.HtmlDecode(Dr_Orden[0]["FOLIO_OC"].ToString().Trim());

                    if (!string.IsNullOrEmpty(Dr_Orden[0]["FOLIO_REQ"].ToString()))
                        Txt_Requisicion.Text = HttpUtility.HtmlDecode(Dr_Orden[0]["FOLIO_REQ"].ToString().Trim());

                    if (!string.IsNullOrEmpty(Dr_Orden[0]["TOTAL_SIN_IVA"].ToString()))
                        Txt_Importe.Text = HttpUtility.HtmlDecode(Dr_Orden[0]["TOTAL_SIN_IVA"].ToString().Trim());

                    Div_Detalles_Orden_Compra.Visible = true;
                    Div_Busqueda_Av.Visible = false;

                    Consultar_Productos_Requisicion(No_Orden_Compra.Trim());
                    Cls_Ope_Alm_Resguardos_Recibos_Negocio Negocio = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();
                    Negocio.P_No_Orden_Compra = (Convert.ToInt32(No_Orden_Compra.Replace(".00",""))).ToString().Trim();
                    DataTable Dt_Datos = Negocio.Consultar_Clase_Activo_Orden_Compra();
                    if (Dt_Datos != null) {
                        if (Dt_Datos.Rows.Count > 0) {
                            Cmb_Clase_Activo.SelectedIndex = Cmb_Clase_Activo.Items.IndexOf(Cmb_Clase_Activo.Items.FindByValue(Dt_Datos.Rows[0]["CLASE_ACTIVO_ID"].ToString().Trim()));
                        }
                    }
                    Cmb_Procedencia.SelectedIndex = Cmb_Procedencia.Items.IndexOf(Cmb_Procedencia.Items.FindByText("COMPRA DIRECTA"));

                    Dt_Datos = null;
                    String No_Requisicion = Dt_Ordenes_Compra.Rows[0]["NO_REQUISICION"].ToString().Trim();
                    Negocio = new Cls_Ope_Alm_Resguardos_Recibos_Negocio();
                    Negocio.P_No_Requisicion = No_Requisicion.Trim();
                    Dt_Datos = Negocio.Consultar_Cuenta_Contable_Requisicion();
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Hdf_Cuenta_Activo_ID.Value = Dt_Datos.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
                            Txt_Cuenta_Activo.Text = Dt_Datos.Rows[0]["CUENTA_CONTABLE_DESCRIPCION"].ToString().Trim();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Click
    ///DESCRIPCIÓN:     En este botón se  asignan los datos del resguardante al resguardo
    ///PARAMETROS: 
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      19/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
    {
        // Declaración de Objetos y Variables
        ImageButton Btn_Selec_Producto = null;
        Cls_Ope_Alm_Resguardos_Recibos_Negocio Resguardo_Recibo = new Cls_Ope_Alm_Resguardos_Recibos_Negocio(); // Se crea el objeto para el manejo de los métodos   

        String No_Inventario = String.Empty;
        DataTable Dt_Productos = new DataTable();
        DataTable Dt_Dts_Gnrls_OC = new DataTable();

        String Producto_ID = "";
        String Operacion = "";
        String Descripcion = "";
        String Producto = "";
        String No_Serie = "";
        String Material = "";

        String Color = "";
        String No_Registro = "";
        String Material_ID = "";
        String Color_ID = "";
        String Costo = "0";
        String No_Resguardo_Recibo = "";

        String Dependencia_ID = "";
        String Area_ID = "";
        String Responsable_ID = "";

        String Tipo_Activo_ID = "";
        String Clase_Activo_ID = "";
        String Observaciones = "";
        String Procedencia = "";
        String Proveedor = "";
        String Nombre_Producto = "";
        String Zona_ID = "";
        String Gerencia_ID = "";
        String Empleado_Autorizo_ID = "";
        String Empleado_Entrego_ID = "";
        String Empleado_Reviso_ID = "";

        String Proveedor_ID = "";
        String No_Factura_Proveedor = "";
        String Fecha_Factura_Proveedor = "";

        String Marca_ID = ""; // Nuevos campos agregados
        String Modelo = "";
        String Garantia = "";
        String Cuenta_Activo_ID = "";
        String Cuenta_Gasto_ID = "";
        String Tipo_Bien = "";

        DataRow[] Dr_Orden_Compra;

        try
        {
            Dt_Productos = (DataTable)Session["Dt_Productos_RR"];

            Btn_Selec_Producto = (ImageButton)sender;
            No_Inventario = Btn_Selec_Producto.CommandArgument; // Se selecciona el No de Orden de Compra

            Dr_Orden_Compra = Dt_Productos.Select("NO_INVENTARIO='" + No_Inventario.Trim() + "'");

            Resguardo_Recibo.P_No_Orden_Compra = Txt_Orden_Compra.Text.Trim().Replace("OC-", "");
            Dt_Dts_Gnrls_OC = Resguardo_Recibo.Consultar_Datos_G_Ordenes_Compra();

            if ((!string.IsNullOrEmpty(Dr_Orden_Compra[0]["DEPENDENCIA_ID"].ToString())) & (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["EMPLEADO_ID"].ToString())))
            {
                if (Dr_Orden_Compra.Length > 0)
                {
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["PRODUCTO_ID"].ToString()))
                        Producto_ID = Dr_Orden_Compra[0]["PRODUCTO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["OPERACION"].ToString()))
                        Operacion = Dr_Orden_Compra[0]["OPERACION"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["PRODUCTO"].ToString()))
                        Producto = Dr_Orden_Compra[0]["PRODUCTO"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["DESCRIPCION"].ToString()))
                        Descripcion = Dr_Orden_Compra[0]["DESCRIPCION"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["NO_SERIE"].ToString()))
                        No_Serie = Dr_Orden_Compra[0]["NO_SERIE"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["COLOR"].ToString()))
                        Color = Dr_Orden_Compra[0]["COLOR"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["MATERIAL"].ToString()))
                        Material = Dr_Orden_Compra[0]["MATERIAL"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["NO_REGISTRO"].ToString()))
                        No_Registro = Dr_Orden_Compra[0]["NO_REGISTRO"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["MATERIAL_ID"].ToString()))
                        Material_ID = Dr_Orden_Compra[0]["MATERIAL_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["COLOR_ID"].ToString()))
                        Color_ID = Dr_Orden_Compra[0]["COLOR_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["DEPENDENCIA_ID"].ToString()))
                        Dependencia_ID = Dr_Orden_Compra[0]["DEPENDENCIA_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["AREA_ID"].ToString()))
                        Area_ID = Dr_Orden_Compra[0]["AREA_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["EMPLEADO_ID"].ToString()))
                        Responsable_ID = Dr_Orden_Compra[0]["EMPLEADO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["COSTO"].ToString()))
                        Costo = Dr_Orden_Compra[0]["COSTO"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["GARANTIA"].ToString()))
                        Garantia = Dr_Orden_Compra[0]["GARANTIA"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["MODELO"].ToString()))
                        Modelo = Dr_Orden_Compra[0]["MODELO"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["MARCA_ID"].ToString()))
                        Marca_ID = Dr_Orden_Compra[0]["MARCA_ID"].ToString().Trim();


                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["TIPO_ACTIVO_ID"].ToString()))
                        Tipo_Activo_ID = Dr_Orden_Compra[0]["TIPO_ACTIVO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["CLASE_ACTIVO_ID"].ToString()))
                        Clase_Activo_ID = Dr_Orden_Compra[0]["CLASE_ACTIVO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["OBSERVACIONES"].ToString()))
                        Observaciones = Dr_Orden_Compra[0]["OBSERVACIONES"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["PROCEDENCIA"].ToString()))
                        Procedencia = Dr_Orden_Compra[0]["PROCEDENCIA"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Txt_Proveedor.Text.Trim()))
                        Proveedor = Txt_Proveedor.Text.Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["PRODUCTO"].ToString()))
                        Nombre_Producto = Dr_Orden_Compra[0]["PRODUCTO"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["ZONA_ID"].ToString()))
                        Zona_ID = Dr_Orden_Compra[0]["ZONA_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["GERENCIA_ID"].ToString()))
                        Gerencia_ID = Dr_Orden_Compra[0]["GERENCIA_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["EMPLEADO_AUTORIZO_ID"].ToString()))
                        Empleado_Autorizo_ID = Dr_Orden_Compra[0]["EMPLEADO_AUTORIZO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["EMPLEADO_ENTREGO_ID"].ToString()))
                        Empleado_Entrego_ID = Dr_Orden_Compra[0]["EMPLEADO_ENTREGO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["EMPLEADO_REVISO_ID"].ToString()))
                        Empleado_Reviso_ID = Dr_Orden_Compra[0]["EMPLEADO_REVISO_ID"].ToString().Trim();

                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["CUENTA_ACTIVO_ID"].ToString()))
                        Cuenta_Activo_ID = Dr_Orden_Compra[0]["CUENTA_ACTIVO_ID"].ToString().Trim();
                    if (!string.IsNullOrEmpty(Dr_Orden_Compra[0]["CUENTA_GASTO_ID"].ToString()))
                        Cuenta_Gasto_ID = Dr_Orden_Compra[0]["CUENTA_GASTO_ID"].ToString().Trim();
                    if (!String.IsNullOrEmpty(Dr_Orden_Compra[0]["TIPO_ARTICULO"].ToString()))
                        Tipo_Bien = Dr_Orden_Compra[0]["TIPO_ARTICULO"].ToString().Trim();

                    if (Dt_Dts_Gnrls_OC.Rows.Count > 0) // Se guardan los datos generales de la factura
                    {
                        if (!string.IsNullOrEmpty(Dt_Dts_Gnrls_OC.Rows[0]["PROVEEDOR_ID"].ToString()))
                            Proveedor_ID = Dt_Dts_Gnrls_OC.Rows[0]["PROVEEDOR_ID"].ToString().Trim();
                        if (!string.IsNullOrEmpty(Dt_Dts_Gnrls_OC.Rows[0]["NO_FACTURA_PROVEEDOR"].ToString()))
                            No_Factura_Proveedor = Dt_Dts_Gnrls_OC.Rows[0]["NO_FACTURA_PROVEEDOR"].ToString().Trim();
                        if (!string.IsNullOrEmpty(Dt_Dts_Gnrls_OC.Rows[0]["FECHA_FACTURA"].ToString()))
                        {
                            String Fecha = Dt_Dts_Gnrls_OC.Rows[0]["FECHA_FACTURA"].ToString().Trim();
                            DateTime Fecha_Convertida = Convert.ToDateTime(Fecha);
                            Fecha_Factura_Proveedor = String.Format("{0:dd/MMM/yyyy}", Fecha_Convertida);
                        }
                    }

                    // Se deshabilita el boton para poner el check
                    Grid_Productos.Rows[(Convert.ToInt32(No_Registro.ToString().Trim()))].Cells[0].Enabled = false;

                    // Se oculta el Boton para resguardar y se muestra el boton para imprimir
                    Grid_Productos.Rows[(Convert.ToInt32(No_Registro.ToString().Trim()))].FindControl("Btn_Imprimir").Visible = false; // Se pone no visible el boton para el resguardo
                    Grid_Productos.Rows[(Convert.ToInt32(No_Registro.ToString().Trim()))].FindControl("Btn_Imprimir_Resguardo").Visible = true; // Se pone no visible el boton para el resguardo

                    Resguardo_Recibo.P_Producto_ID = Producto_ID.Trim();

                    if (Txt_Requisicion.Text.Trim() != "")
                        Resguardo_Recibo.P_No_Requisicion = Txt_Requisicion.Text.Trim().Replace("RQ-", "");
                    else
                        Resguardo_Recibo.P_No_Requisicion = "";

                    if (Txt_Orden_Compra.Text.Trim() != "")
                        Resguardo_Recibo.P_No_Orden_Compra = Txt_Orden_Compra.Text.Trim().Replace("OC-", "");
                    else
                        Resguardo_Recibo.P_No_Orden_Compra = "";

                    Resguardo_Recibo.P_Razon_Social_Proveedor = Proveedor.Trim();
                    Resguardo_Recibo.P_Clase_Activo_ID = Clase_Activo_ID.Trim();
                    Resguardo_Recibo.P_Tipo_Activo_ID = Tipo_Activo_ID.Trim();
                    Resguardo_Recibo.P_Estado = "BUENO";
                    Resguardo_Recibo.P_Observaciones = Observaciones.Trim();
                    Resguardo_Recibo.P_Procedencia_ID = Procedencia.Trim();
                    Resguardo_Recibo.P_Nombre = Nombre_Producto.Trim();
                    Resguardo_Recibo.P_Zona_ID = Zona_ID.Trim();
                    Resguardo_Recibo.P_Gerencia_ID = Gerencia_ID.Trim();
                    Resguardo_Recibo.P_Empleado_Autorizo_ID = Empleado_Autorizo_ID.Trim();
                    Resguardo_Recibo.P_Empleado_Entrego_ID = Empleado_Entrego_ID.Trim();
                    Resguardo_Recibo.P_Empleado_Reviso_ID = Empleado_Reviso_ID.Trim();
                    Resguardo_Recibo.P_Cuenta_Activo_ID = Cuenta_Activo_ID;
                    Resguardo_Recibo.P_Cuenta_Gasto_ID = Cuenta_Gasto_ID;
                    Resguardo_Recibo.P_Tipo_Bien = Tipo_Bien;

                    Resguardo_Recibo.P_No_Inventario = No_Inventario.Trim();
                    Resguardo_Recibo.P_Operacion = Operacion.Trim();
                    Resguardo_Recibo.P_Producto = Producto.Trim();
                    Resguardo_Recibo.P_Descripcion = Descripcion.Trim();
                    Resguardo_Recibo.P_No_Serie = No_Serie.Trim();
                    Resguardo_Recibo.P_Color_ID = Color_ID.Trim();
                    Resguardo_Recibo.P_Material_ID = Material_ID.Trim();
                    Resguardo_Recibo.P_Area_ID = Area_ID.Trim();
                    Resguardo_Recibo.P_Unidad_Responsable_ID = Dependencia_ID.Trim();
                    Resguardo_Recibo.P_Responsable_ID = Responsable_ID.Trim();
                    Resguardo_Recibo.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado.Trim();
                    Resguardo_Recibo.P_Empleado_Almacen_ID = Cls_Sessiones.Empleado_ID.Trim();
                    Resguardo_Recibo.P_Proveedor_ID = Proveedor_ID.Trim();
                    Resguardo_Recibo.P_No_Factura = No_Factura_Proveedor.Trim();
                    Resguardo_Recibo.P_Fecha_Adquisicion = Fecha_Factura_Proveedor.Trim();
                    Resguardo_Recibo.P_Fecha_Inventario = "" + DateTime.Now.ToString("dd/MMM/yyyy");
                    Resguardo_Recibo.P_Costo = Costo.Trim();
                    Resguardo_Recibo.P_Marca_ID = Marca_ID.Trim(); // Nuevos Campos que se agregaron
                    Resguardo_Recibo.P_Garantia = Garantia.Trim();
                    Resguardo_Recibo.P_Modelo = Modelo.Trim();
                    Resguardo_Recibo.P_Operacion = Operacion;

                    No_Resguardo_Recibo = Resguardo_Recibo.Alta_Resguardo_Recibo();  // Se sa de alta el recibo o resguardo
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Se dio de Alta el bien exitosamente.');", true);
                    if (Resguardo_Recibo.P_Tipo_Bien.Equals("BIEN_MUEBLE")) {
                        Consultar_Datos_Resguardo_Recibo(No_Inventario.Trim()); // Se imprime el Resguardo o Recibo
                    } else {
                        Consultar_Datos_Resguardo_Recibo_Vehiculo(No_Inventario.Trim()); // Se imprime el Resguardo o Recibo
                    }

                    for (int g = 0; g < Grid_Productos.Rows.Count; g++)
                    {
                        if ((Grid_Productos.Rows[g].FindControl("Btn_Imprimir").Visible != false) & (Grid_Productos.Rows[g].FindControl("Btn_Imprimir_Resguardo").Visible != true))
                        {
                            g = Grid_Productos.Rows.Count;
                        }
                    }
                }
                Div_Contenedor_Msj_Error.Visible = false;
            }
            else
            {
                Lbl_Informacion.Text = "Seleccionar Unidad y Responsable";
                Div_Contenedor_Msj_Error.Visible = true;
            }

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Gerencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Evento utilizado para llenar el combo de empleados al seleccionar la gerencia
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           31/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e) {
        Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedItem.Value);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Unidad_Responsable_SelectedIndexChanged
    ///DESCRIPCIÓN:          Evento utilizado para llenar el combo de empleados al seleccionar la unidad responsable
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           31/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Unidad_Responsable_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            String Unidad = Cmb_Unidad_Responsable.SelectedValue;
            Llenar_Combo_Empleados(Unidad);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Aceptar_Click
    ///DESCRIPCIÓN:          Evento de boton utilizado para agregar los resguardantes
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           31/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Aceptar_Click(object sender, EventArgs e)
    {
        DataTable Dt_Productos = new DataTable();
        DataTable Dt_Productos_Resguardar = new DataTable();

        Dt_Productos = (DataTable)Session["Dt_Productos_RR"];

        try
        {
            if ((Cmb_Responsable.SelectedIndex != 0) 
                & (Cmb_Unidad_Responsable.SelectedIndex != 0) 
                & (Cmb_Gerencias.SelectedIndex != 0) 
                & (Cmb_Procedencia.SelectedIndex != 0) 
                & (Hdf_Resguardo_Completo_Autorizo.Value.Trim().Length > 0) 
                & (Hdf_Resguardo_Completo_Funcionario_Recibe.Value.Trim().Length > 0) 
                & (Hdf_Resguardo_Completo_Operador.Value.Trim().Length > 0)
                & (Hdf_Cuenta_Gasto_ID.Value.Trim().Length > 0)
                & (Txt_Cuenta_Gasto.Text.Trim().Length > 0))
            {
                Boolean V_CkeckBox = false;
                for (int i = 0; i < Grid_Productos.Rows.Count; i++)
                {
                    CheckBox Chk_Producto = (CheckBox)Grid_Productos.Rows[i].FindControl("Chk_Producto");
                    ImageButton Btn_Imprimir = (ImageButton)Grid_Productos.Rows[i].FindControl("Btn_Imprimir");
                    ImageButton Btn_Imprimir_Resguardo = (ImageButton)Grid_Productos.Rows[i].FindControl("Btn_Imprimir_Resguardo");

                    if (Chk_Producto.Checked && !Btn_Imprimir_Resguardo.Visible)
                    {
                        V_CkeckBox = true;
                        String Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value.Trim();
                        String Area_ID = Cls_Sessiones.Area_ID_Empleado; //Cmb_Area.SelectedItem.Value.Trim();
                        String Empleado_ID = Cmb_Responsable.SelectedItem.Value.Trim();


                        String Tipo_Activo_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
                        String Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
                        String Observaciones = Txt_Observaciones.Text.Trim();
                        String Procedencia = Cmb_Procedencia.SelectedItem.Value.Trim();
                        String Zona_ID = Cmb_Zonas.SelectedItem.Value.Trim();
                        String Gerencia_ID = Cmb_Gerencias.SelectedItem.Value.Trim();
                        String Empleado_Autorizo_ID = Hdf_Resguardo_Completo_Autorizo.Value.Trim();
                        String Empleado_Entrego_ID = Hdf_Resguardo_Completo_Operador.Value.Trim();
                        String Empleado_Reviso_ID = Hdf_Resguardo_Completo_Funcionario_Recibe.Value.Trim();
                        String Cuenta_Activo_ID = Hdf_Cuenta_Activo_ID.Value.Trim();
                        String Cuenta_Gasto_ID = Hdf_Cuenta_Gasto_ID.Value.Trim();

                        if (Clase_Activo_ID != "")
                            Dt_Productos.Rows[i].SetField("CLASE_ACTIVO_ID", Clase_Activo_ID);
                        if (Tipo_Activo_ID != "")
                            Dt_Productos.Rows[i].SetField("TIPO_ACTIVO_ID", Tipo_Activo_ID);
                        if (Procedencia != "")
                            Dt_Productos.Rows[i].SetField("PROCEDENCIA", Procedencia);
                        if (Zona_ID != "")
                            Dt_Productos.Rows[i].SetField("ZONA_ID", Zona_ID);
                        if (Observaciones != "")
                            Dt_Productos.Rows[i].SetField("OBSERVACIONES", Observaciones);

                        Dt_Productos.Rows[i].SetField("AREA_ID", Area_ID);
                        Dt_Productos.Rows[i].SetField("EMPLEADO_ID", Empleado_ID);

                        Dt_Productos.Rows[i].SetField("DEPENDENCIA_ID", Dependencia_ID);
                        Dt_Productos.Rows[i].SetField("GERENCIA_ID", Gerencia_ID);
                        Dt_Productos.Rows[i].SetField("EMPLEADO_AUTORIZO_ID", Empleado_Autorizo_ID);
                        Dt_Productos.Rows[i].SetField("EMPLEADO_ENTREGO_ID", Empleado_Entrego_ID);
                        Dt_Productos.Rows[i].SetField("EMPLEADO_REVISO_ID", Empleado_Reviso_ID);
                        Dt_Productos.Rows[i].SetField("CUENTA_ACTIVO_ID", Cuenta_Activo_ID);
                        Dt_Productos.Rows[i].SetField("CUENTA_GASTO_ID", Cuenta_Gasto_ID);

                        Chk_Producto.BackColor = System.Drawing.Color.SlateGray; // Se le agrega el color para saber que el check box ya ha sifo seleccionado
                        Chk_Producto.Visible = false; // Se le quita la palomita al check
                        Btn_Imprimir.Visible = true;
                    }
                }

                Session["Dt_Productos_RR"] = Dt_Productos; // Nuevamente se asigna la tabla a la variable de session

                if (V_CkeckBox == false)
                {
                    Lbl_Informacion.Text = " Seleccionar el producto a resguardar";
                    Lbl_Informacion.Visible = true;
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                String Mensaje = "Los siguientes campos son Obligatorios: <br />";
                if (Hdf_Cuenta_Gasto_ID.Value.Length == 0 || Txt_Cuenta_Gasto.Text.Length == 0) Mensaje += " - La Cuenta de Gasto <br />";
                if (Cmb_Procedencia.SelectedIndex == 0) Mensaje += " - Procendencia <br />";
                if (Cmb_Gerencias.SelectedIndex == 0) Mensaje += " - Gerencia <br />";
                if (Cmb_Unidad_Responsable.SelectedIndex == 0) Mensaje += " - Unidad Responsable <br />";
                if (Cmb_Responsable.SelectedIndex == 0) Mensaje += " - Resguardatario <br />";
                if (Hdf_Resguardo_Completo_Autorizo.Value.Length == 0) Mensaje += " - Empleado que Autoriza <br />";
                if (Hdf_Resguardo_Completo_Operador.Value.Length == 0) Mensaje += " - Empleado que Entrega <br />";
                if (Hdf_Resguardo_Completo_Funcionario_Recibe.Value.Length == 0) Mensaje += " - Empleado que Revisa <br />";
                Lbl_Informacion.Text = HttpUtility.HtmlDecode(Mensaje);
                Lbl_Informacion.Visible = true;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Producto_CheckedChanged
    ///DESCRIPCIÓN:          Evento de boton utilizado para agregar los resguardantes
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           31/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Chk_Producto_CheckedChanged(object sender, EventArgs e)
    {
        Boolean Boton_Habilitado = false;

        for (int i = 0; i < Grid_Productos.Rows.Count; i++)
        {
            CheckBox Chk_Producto = (CheckBox)Grid_Productos.Rows[i].FindControl("Chk_Producto");

            if (Chk_Producto.Checked)
            {
                Boton_Habilitado = true;
            }
        }

        if (Boton_Habilitado == true)
            Btn_Aceptar.Enabled = true;
        else
            Btn_Aceptar.Enabled = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Producto_CheckedChanged
    ///DESCRIPCIÓN:          Se muestra en PDF el resguardo para reimprimirse
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           31/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Imprimir_Resguardo_Click(object sender, ImageClickEventArgs e)
    {
        // Declaración de Objetos y Variables
        ImageButton Btn_Selec_Producto = null;
        String No_Inventario = ""; // Variable que contendra el No_Inventario
        String Tipo_Bien = "";
        DataTable Dt_Productos = new DataTable();
        try
        {
            Dt_Productos = (DataTable)Session["Dt_Productos_RR"];
            Btn_Selec_Producto = (ImageButton)sender;
            No_Inventario = Btn_Selec_Producto.CommandArgument; // Se selecciona el No de Orden de Compra
            Tipo_Bien = Dt_Productos.Select("NO_INVENTARIO='" + No_Inventario.Trim() + "'")[0]["TIPO_ARTICULO"].ToString().Trim();
            if (Tipo_Bien.Equals("BIEN_MUEBLE"))
            {
                Consultar_Datos_Resguardo_Recibo(No_Inventario.Trim()); // Se imprime el Resguardo o Recibo
            }
            else
            {
                Consultar_Datos_Resguardo_Recibo_Vehiculo(No_Inventario.Trim()); // Se imprime el Resguardo o Recibo
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Fecha_B_CheckedChanged
    ///DESCRIPCIÓN:          Evento utilizado para establecer la fecha de búsqueda 
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           30-Julio-11 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Chk_Fecha_B_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Fecha_B.Checked == true)
        {
            Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            Txt_Fecha_Fin.Text = DateTime.Now.ToString("dd/MMM/yyyy");

            Img_Btn_Fecha_Inicio.Enabled = true;
            Img_Btn_Fecha_Fin.Enabled = true;
        }
        else
        {
            Txt_Fecha_Inicio.Text = "";
            Txt_Fecha_Fin.Text = "";

            Img_Btn_Fecha_Inicio.Enabled = false;
            Img_Btn_Fecha_Fin.Enabled = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Click
    ///DESCRIPCIÓN:          Botón utilizado para configurar el estatus inicial de la búsqueda avanzada
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez.
    ///FECHA_CREO:           30-Julio-11 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
    {
        Estado_Inicial_Busqueda_Avanzada();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {            
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Funcionario_Recibe_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "FUNCIONARIO_RECIBE";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Autorizo_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Autorizo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "AUTORIZO";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Operador_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "OPERADOR";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Informacion.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

}