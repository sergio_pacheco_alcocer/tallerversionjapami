﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Inventario_Stock.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Inventario_Stock"
    Title="Inventarios" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Inventario de Stock
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <%--                                <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" CssClass="Img_Button" ToolTip="Nuevo" OnClick="Btn_Nuevo_Click" />
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" OnClick="Btn_Modificar_Click" />--%>
                                <asp:Button ID="Button1" runat="server" Text="Button" OnClick="Button1_Click1" Visible="false" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                                <asp:ImageButton ID="Btn_Guardar" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png"
                                    ToolTip="Guardar" OnClick="Btn_Guardar_Click" />
                                <asp:ImageButton ID="Btn_Imprimir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                    CssClass="Img_Button" ToolTip="Imprimir" OnClick="Btn_Imprimir_Click" />
                                <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                    CssClass="Img_Button" ToolTip="Exportar Excel" OnClick="Btn_Imprimir_Excel_Click" />
                                <asp:HiddenField ID="Hdf_Producto_ID" runat="server" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                Tipo
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="70%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Tipo_SelectedIndexChanged" />
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Clave Anterior
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="Txt_Clave_Anterior" runat="server" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Clave
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="Txt_Clave" runat="server" Width="90%"></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="Chk_Almacen_General" runat="server" Text="Almacen General" AutoPostBack="True"
                                    OnCheckedChanged="Chk_Almacen_General_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Producto
                            </td>
                            <td colspan="2">
                                <asp:TextBox ID="Txt_Producto" runat="server" Width="90%"></asp:TextBox>
                            </td>
                            <td>
                                <asp:CheckBox ID="Chk_Almacen_Papeleria" runat="server" Text="Almacen Papeleria"
                                    AutoPostBack="True" OnCheckedChanged="Chk_Almacen_Papeleria_CheckedChanged" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Partida
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Partida" runat="server" Width="90%">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:CheckBox ID="Chk_Existencias_Cero" runat="server" Text="Mostrar existencia cero" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Ordenar por
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Ordenar" runat="server" Width="90%">
                                    <asp:ListItem Value="CLAVE_ANTERIOR">CLAVE ANTERIOR</asp:ListItem>
                                    <asp:ListItem Value="CLAVE_SIAC">CLAVE SIAC</asp:ListItem>
                                    <asp:ListItem Value="NOMBRE">NOMBRE</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Button ID="Btn_Buscar" runat="server" Text="Buscar" CssClass="button" OnClick="Btn_Buscar_Click" />
                            </td>
                        <tr>
                            <td colspan="2" style="width: 50%;">
                                <asp:Label ID="Lbl_Registros" runat="server" Text="" ForeColor="DarkBlue"></asp:Label>
                            </td>
                            <td colspan="2">
                            </td>
                            <td align="right">
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 4px;">
                                <hr class="linea" />
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr class="barra_busqueda">
                            <td style="width: 3%;" align="left">
                            </td>
                            <td style="width: 5%;" align="left">
                                Clave
                            </td>
                            <td style="width: 5%;" align="left">
                                Antigua
                            </td>
                            <td style="width: 13%;" align="left">
                                Producto
                            </td>                            
                            <td style="width: 5%;" align="left">
                                Unidad
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Existencia">
                                Exist.
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Disponible">
                                Dis.
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Comprometido">
                                Com.
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Minimo">
                                Min.
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Maximo">
                                Max.
                            </td>
                            <td style="width: 5%" align="left" runat="server" id="Td_Reorden">
                                P.R.
                            </td>
                            <td style="width: 8%" align="left" runat="server" id="Td_Promedio" visible="false">
                                $ Prom.
                            </td>
                            <td style="width: 8%" align="left" runat="server" id="Td_U_Costo" visible="false">
                                $ U Costo.
                            </td>
                            <td style="width: 8%" align="left" runat="server" id="Td_Costo_Promedio" visible="false">
                                $ Costo Prom.
                            </td>
                            <td style="width: 8%" align="left" runat="server" id="Td_Ultimo_Costo" visible="false">
                                $ Ultimo Costo.
                            </td>
                        </tr>
                    </table>
                    <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; border-style: outset;
                        border-color: Silver;">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 99%" align="center">
                                    <asp:GridView ID="Grid_Inventario" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                                        GridLines="None" Width="100%" EmptyDataText="No se encontraron productos" ShowHeader="false">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Seleccionar_Requisicion" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                        OnClick="Btn_Seleccionar_Producto_Click" CommandArgument='<%# Eval("PRODUCTO_ID") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CLAVE" HeaderText="Clave" Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REF_JAPAMI" HeaderText="Antigua" Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Producto" Visible="True" SortExpression="NOMBRE">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                            </asp:BoundField>                                           
                                            <asp:BoundField DataField="UNIDAD" HeaderText="Unidad" Visible="True">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EXISTENCIA" HeaderText="Exist." Visible="True">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DISPONIBLE" HeaderText="Disp.">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COMPROMETIDO" HeaderText="Comp.">
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MINIMO" HeaderText="Min.">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MAXIMO" HeaderText="Max.">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REORDEN" HeaderText="Reorden">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" Width="4%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            
                                            <asp:BoundField DataField="PROMEDIO" HeaderText="Promedio" DataFormatString="{0:n}"
                                                Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="U_COSTO" HeaderText="U. Costo" DataFormatString="{0:n}"
                                                Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COSTO_PROMEDIO_T" HeaderText="Costo Promedio" DataFormatString="{0:n}"
                                                Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            
                                            <asp:BoundField DataField="ULTIMO_COSTO" HeaderText="Ultimo Costo" DataFormatString="{0:n}"
                                                Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Right" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRODUCTO_ID" HeaderText="ID" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div>
                    <table>
                        <tr align="right">
                            <td style="width: 75%">
                            </td>
                            <td align="right" style="width: 25%; text-align: right;">
                                <asp:Label Style="text-align: right;" Width="100%" ID="Lbl_costo_promedio_t" runat="server"
                                    Text="" Visible="true" ForeColor="Blue"></asp:Label>
                             <asp:Label Style="text-align: right;" Width="100%" ID="Lbl_ultimo_costo_t" runat="server"
                                    Text="" Visible="true" ForeColor="Blue"></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server" visible="false">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%">
                                Clave
                            </td>
                            <td style="width: 35%">
                                <asp:TextBox ID="Txt_Clave_Modificar" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                            <td style="width: 15%">
                                Nombre
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Nombre_Modificar" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Descripción
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Descripcion_Modificar" runat="server" Enabled="false" TextMode="MultiLine"
                                    Width="98%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 4px;">
                                <hr class="linea" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Existencia
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Existencia_Modificar" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Disponible
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Disponible_Modificar" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Comprometido
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Comprometido_Modificar" runat="server" Width="50%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="height: 4px;">
                                <hr class="linea" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Minimo
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Minimo_Modificar" runat="server" Width="50%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Máximo
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Maximo_Modificar" runat="server" Width="50%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%">
                                Reorden
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Reorden_Modificar" runat="server" Width="50%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:PostBackTrigger ControlID="Btn_Imprimir_Excel" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
</asp:Content>
