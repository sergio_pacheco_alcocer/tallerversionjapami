﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Productos_Almacen.Negocio;
using JAPAMI.Productos_Chatarra.Negocio;


public partial class paginas_Almacen_Frm_Ope_Alm_Productos_Chatarra : System.Web.UI.Page
{
    #region Variables Locales
    private const String P_Dt_Productos_Chatarra = "Dt_Productos_Chatarra";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Page_Load) " + ex.ToString(), true);
        }
    }
    #endregion

    #region Metodos
        private void Mostrar_Error(String Mensaje, Boolean Mostrar)
        {
            try
            {
                Lbl_Informacion.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = Mostrar;
            }
            catch (Exception ex)
            {
                Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        private void Estado_Inicial()
        {
            try
            {
                Llena_Combo_Impuestos();
                Llena_Combo_Unidades();
                Llena_Combo_Tipos();
                Llena_Combo_Partidas_Genericas();

                Elimina_Sesiones();

                Habilitar_Controles(false);
                Lbl_Tipo_Busqueda.Visible = false;
                Txt_Busqueda.Visible = false;

                Llena_Grid_Productos();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
        ///DESCRIPCION:             Eliminar las sesiones utilizadas en la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:42
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Elimina_Sesiones()
        {
            try
            {
                HttpContext.Current.Session.Remove(P_Dt_Productos_Chatarra);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Limpiar_Controles
        ///DESCRIPCION:             Limpiar los controles de la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:42
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Txt_Busqueda.Text = "";
                Cmb_Estatus.SelectedIndex = 0;
                Cmb_Impuestos.SelectedIndex = 0;
                Cmb_Partidas_Especificas.Items.Clear();
                Cmb_Partidas_Genericas.SelectedIndex = 0;
                Cmb_Stock.SelectedIndex = 0;
                Cmb_Tipo_Reporte.SelectedIndex = 0;
                Cmb_Tipos.SelectedIndex = 0;
                Cmb_Unidades.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Unidades
        ///DESCRIPCION:             Llenar el combo de las unidades
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Unidades()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Unidades = new DataTable(); //variable para la tabla

            try
            {
                //Asignar propiedades
                Dt_Unidades = Productos_Negocio.Consulta_Unidades();
                Cmb_Unidades.Items.Clear();
                Cmb_Unidades.DataSource = Dt_Unidades;
                Cmb_Unidades.DataTextField = "Nombre";
                Cmb_Unidades.DataValueField = "Unidad_ID";
                Cmb_Unidades.DataBind();
                Cmb_Unidades.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Unidades.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Unidades.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Impuestos
        ///DESCRIPCION:             Llenar el combo de los impuestos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Impuestos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Impuestos = new DataTable(); //Tabla para los impuestos

            try
            {
                //Asignar propiedades
                Dt_Impuestos = Productos_Negocio.Consulta_Impuestos();
                Cmb_Impuestos.Items.Clear();
                Cmb_Impuestos.DataSource = Dt_Impuestos;
                Cmb_Impuestos.DataTextField = "Nombre";
                Cmb_Impuestos.DataValueField = "Impuesto_ID";
                Cmb_Impuestos.DataBind();
                Cmb_Impuestos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Impuestos.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Impuestos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Tipos
        ///DESCRIPCION:             Llenar el combo de los tipos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Tipos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Tipos = new DataTable(); //Tabla para los tipos

            try
            {
                //Asignar propiedades
                Dt_Tipos = Productos_Negocio.Consulta_Tipos();
                Cmb_Tipos.Items.Clear();
                Cmb_Tipos.DataSource = Dt_Tipos;
                Cmb_Tipos.DataTextField = "Tipo";
                Cmb_Tipos.DataValueField = "Tipo";
                Cmb_Tipos.DataBind();
                Cmb_Tipos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Tipos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Genericas
        ///DESCRIPCION:             Llenar el combo de las partidas genericas
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Genericas()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Genericas = new DataTable(); //Tabla para las partidas genericas

            try
            {
                //Asignar propiedades
                Dt_Partidas_Genericas = Productos_Negocio.Consulta_Partidas_Genericas();
                Cmb_Partidas_Genericas.Items.Clear();
                Cmb_Partidas_Genericas.DataSource = Dt_Partidas_Genericas;
                Cmb_Partidas_Genericas.DataTextField = "Nombre";
                Cmb_Partidas_Genericas.DataValueField = "Partida_Generica_ID";
                Cmb_Partidas_Genericas.DataBind();
                Cmb_Partidas_Genericas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Genericas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Genericas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Especificas
        ///DESCRIPCION:             Llenar el combo de las partidas especificas
        ///PARAMETROS:              Partida_Generica_ID: Cadena de texto que contiene el ID de la partida Generica
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Especificas(String Partida_Generica_ID)
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Especificas = new DataTable(); //tabla para las partidas especificas

            try
            {
                //Asignar propiedades
                Productos_Negocio.P_Partida_Generica_ID = Partida_Generica_ID;
                Dt_Partidas_Especificas = Productos_Negocio.Consulta_Partidas_Especificas();
                Cmb_Partidas_Especificas.DataSource = Dt_Partidas_Especificas;
                Cmb_Partidas_Especificas.DataTextField = "Nombre";
                Cmb_Partidas_Especificas.DataValueField = "Partida_ID";
                Cmb_Partidas_Especificas.DataBind();
                Cmb_Partidas_Especificas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Especificas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Especificas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Habilitar_Controles
        ///DESCRIPCION:             Habilitar los controles de acuerdo si es filtro o bosqueda
        ///PARAMETROS:              Filtro: Booleano que indica si es busqueda por filtro
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 13:22
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Habilitar_Controles(Boolean Filtro)
        {
            try
            {
                Lbl_Tipo_Busqueda.Visible = !Filtro;
                Txt_Busqueda.Visible = !Filtro;
                Cmb_Estatus.Enabled = Filtro;
                Cmb_Impuestos.Enabled = Filtro;
                Cmb_Partidas_Especificas.Enabled = Filtro;
                Cmb_Partidas_Genericas.Enabled = Filtro;
                Cmb_Stock.Enabled = Filtro;
                Cmb_Tipos.Enabled = Filtro;
                Cmb_Unidades.Enabled = Filtro;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        private String Validar_Datos()
        {
            //Declaracion de variables
            String Resultado = String.Empty; //Variable para el resultado
            String[] Vec_Datos; //Vector con los datos
            char Separador = '_'; //Variable para el separador para el vector

            try
            {
                //Obtener el vector de los datos
                Vec_Datos = Txt_Cadena_Control.Value.Split(Separador);

                //Verificar si hay comentarios
                if (Txt_Comentarios.Text.Trim() == null || Txt_Comentarios.Text.Trim() == "" || Txt_Comentarios.Text.Trim() == String.Empty)
                {
                    Resultado = "Favor de proporcionar comentarios<br />";
                }

                //Verificar si puso cantidad
                if (Txt_Cantidad.Text.Trim() == null || Txt_Cantidad.Text.Trim() == "" || Txt_Cantidad.Text.Trim() == String.Empty)
                {
                    Resultado += "Favor de proporcionar una cantidad<br />";
                }
                else
                {
                    //Verificar que la cantidad no sea mayor a la que hay
                    if (Convert.ToInt32(Txt_Cantidad.Text.Trim()) > Convert.ToInt32(Vec_Datos[1]))
                    {
                        Resultado += "Favor de proporcionar una cantidad menor a la existencia<br />";
                    }
                }

                //Entregar resultado
                return Resultado;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        private Int64 Agregar_Producto_Chatarra(String Producto_ID, int Cantidad, Int64 No_Resguardo)
        {
            //Declaracion de variables
            Int64 No_Salida = 0; //variable para el numero de la salida
            Cls_Ope_Alm_Productos_Chatarra_Negocio Productos_Chatarra_Negocio = new Cls_Ope_Alm_Productos_Chatarra_Negocio(); //Variable para la capa de negocios
            DataTable Dt_Productos = new DataTable(); //tabla para la consulta de los productos
            Double IVA = 0; //variable para el IVA
            Double Importe = 0; //variable para el importe
            Double Subtotal = 0; //variable para el subtotal
            Double Total = 0; //Variable para el total
            Double Costo = 0; //variable para el costo
            Double Impuesto = 0; //variable para el porcentaje del impuesto
            Double aux = 0; //variable auxiliar para los calculos

            try
            {
                //Consulta para los datos del producto
                Productos_Chatarra_Negocio.P_Producto_ID = Producto_ID;
                Dt_Productos = Productos_Chatarra_Negocio.Consulta_Productos_Simple();

                //verificar si la consulta arrojo resultados
                if (Dt_Productos.Rows.Count > 0)
                {
                    //Asignar propiedades
                    Productos_Chatarra_Negocio.P_Producto_ID = Producto_ID;
                    Productos_Chatarra_Negocio.P_Cantidad = Cantidad;
                    Productos_Chatarra_Negocio.P_No_Resguardo = No_Resguardo;

                    //Asignar el costo del producto
                    if (Dt_Productos.Rows[0]["Costo"] != DBNull.Value)
                    {
                        Costo = Convert.ToDouble(Dt_Productos.Rows[0]["Costo"]);
                        Productos_Chatarra_Negocio.P_Costo = Costo;
                    }

                    if (Dt_Productos.Rows[0]["Costo_Promedio"] != DBNull.Value)
                    {
                        Productos_Chatarra_Negocio.P_Costo_Promedio = Convert.ToDouble(Dt_Productos.Rows[0]["Costo_Promedio"]);
                    }

                    //Calculo de los totales
                    Importe = Cantidad * Costo;
                    
                    //verificar si tiene impuestos
                    if (Dt_Productos.Rows[0]["Impuesto_ID"] != DBNull.Value)
                    {
                        //Consultar el impuesto
                        Productos_Chatarra_Negocio.P_Impuesto_ID = Dt_Productos.Rows[0]["Impuesto_ID"].ToString().Trim();
                        Impuesto = Productos_Chatarra_Negocio.Consulta_Impuestos();

                        aux = (Importe * Impuesto) / 100;
                        IVA = aux;
                    }

                    Impuesto = 0;
                    aux = 0;
                    if (Dt_Productos.Rows[0]["Impuesto_2_ID"] != DBNull.Value)
                    {
                        //Consultar el impuesto
                        Productos_Chatarra_Negocio.P_Impuesto_ID = Dt_Productos.Rows[0]["Impuesto_2_ID"].ToString().Trim();
                        Impuesto = Productos_Chatarra_Negocio.Consulta_Impuestos();

                        aux = (Importe * Impuesto) / 100;
                        IVA = IVA + aux;
                    }

                    Subtotal = Importe + IVA;
                    Total = Subtotal;

                    //Asignar resto de las propiedades
                    Productos_Chatarra_Negocio.P_IVA = IVA;
                    Productos_Chatarra_Negocio.P_Subtotal = Subtotal;
                    Productos_Chatarra_Negocio.P_Importe = Importe;
                    Productos_Chatarra_Negocio.P_Total = Total;

                    //Dar de alta la salida
                    No_Salida = Productos_Chatarra_Negocio.Entrada_Chatarra();
                }

                //Entregar resultado
                return No_Salida;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Grid
        /// *************************************************************************************
        /// NOMBRE:              Llena_Grid_Productos
        /// DESCRIPCION:         Llenar el Grid de los Productos
        /// PARÁMETROS:          
        /// USUARIO CREO:        Noe Mosqueda Valadez
        /// FECHA CREO:          22/Marzo/2012 20:00
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACION:  
        /// *************************************************************************************
        private void Llena_Grid_Productos()
        {
            //Declaracion de variables
            Cls_Ope_Alm_Productos_Chatarra_Negocio Productos_Chatarra_Negocio = new Cls_Ope_Alm_Productos_Chatarra_Negocio(); //variable para la capa de negocios
            DataTable Dt_Productos = new DataTable(); //tabla para la consulta de los productos

            try
            {
                //Verificar si existe la sesion
                if (HttpContext.Current.Session[P_Dt_Productos_Chatarra] != null)
                {
                    //Colocar variable de sesion en la tabla
                    Dt_Productos = ((DataTable)HttpContext.Current.Session[P_Dt_Productos_Chatarra]);
                }
                else
                {
                    //Asignar propiedades
                    //Verificar el tipo de reporte
                    if (Cmb_Tipo_Reporte.SelectedIndex == 1)
                    {
                        //Unidad
                        if (Cmb_Unidades.SelectedIndex > 0)
                        {
                            Productos_Chatarra_Negocio.P_Unidad_ID = Cmb_Unidades.SelectedItem.Value.Trim();
                        }

                        //Impuestos
                        if (Cmb_Impuestos.SelectedIndex > 0)
                        {
                            Productos_Chatarra_Negocio.P_Impuesto_ID = Cmb_Impuestos.SelectedItem.Value.Trim();
                        }

                        //Estatus
                        if (Cmb_Estatus.SelectedIndex > 0)
                        {
                            Productos_Chatarra_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                        }

                        //Tipo
                        if (Cmb_Tipos.SelectedIndex > 0)
                        {
                            Productos_Chatarra_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value.Trim();
                        }
                    }
                    else
                    {
                        //Colocar el tipo de filtro
                        Productos_Chatarra_Negocio.P_Tipo_Consulta = Cmb_Tipo_Reporte.SelectedItem.Value.Trim();

                        //Seleccionar el tipo de consulta de busqueda
                        switch (Cmb_Tipo_Reporte.SelectedItem.Value.Trim())
                        {
                            case "Clave":
                                Productos_Chatarra_Negocio.P_Clave = Txt_Busqueda.Text.Trim();
                                break;

                            case "Ref_JAPAMI":
                                Productos_Chatarra_Negocio.P_Ref_JAPAMI = Convert.ToInt64(Txt_Busqueda.Text.Trim());
                                break;

                            case "Nombre":
                                Productos_Chatarra_Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                                break;

                            default:
                                break;
                        }
                    }

                    //Resguardo
                    if (Cmb_Resguardo.SelectedIndex == 1)
                    {
                        Productos_Chatarra_Negocio.P_Resguardos = true;
                    }

                    //Ejecutar consulta
                    Dt_Productos = Productos_Chatarra_Negocio.Consulta_Productos();
                }

                //Llenar el grid
                Grid_Productos.DataSource = Dt_Productos;

                //Ocultar las columnas del resguardo y el resguardante
                Grid_Productos.Columns[4].Visible = false;
                Grid_Productos.Columns[5].Visible = false;
                Grid_Productos.DataBind();

                //Verificar si hay que tomar en cuenta el resguardo
                if (Cmb_Resguardo.SelectedIndex == 1)
                {
                    //Mostrar las columnas del resguardo y el resguardante
                    Grid_Productos.Columns[4].Visible = true;
                    Grid_Productos.Columns[5].Visible = true;
                }

                //Colocar los datos en la variable de sesion
                HttpContext.Current.Session[P_Dt_Productos_Chatarra] = Dt_Productos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        protected void Btn_Chatarra_Click(object sender, ImageClickEventArgs e)
        {
            //Declaracion de variables
            int Cantidad = 0; //Variable para la cantidad
            String[] vec_Cadena_Control; //vector para la cadena de control
            String Cadena_Control = String.Empty; //Cadena para la cadena de control
            char limitador = '_';

            try
            {
                //Obtener la cadena de control
                Txt_Cadena_Control.Value = ((ImageButton)sender).CommandArgument;

                //Construir el vector con los datos
                vec_Cadena_Control = Txt_Cadena_Control.Value.Split(limitador);

                //verificar si el vector tiene elementos
                if (vec_Cadena_Control.Length > 0)
                {
                    //Colocar los datos
                    Txt_Producto.Text = vec_Cadena_Control[vec_Cadena_Control.Length - 1];
                    Txt_Cantidad.Text = vec_Cadena_Control[1];
                }

                Upd_Pnl_Comentarios_Chatarra.Update();

                //Mostrar la ventana modal
                MPE_Comentarios_Chatarra.Show();
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Chatarra_Click) " + ex.ToString(), true);
            }
        }
    #endregion

    #region Eventos
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Salir_Click) " + ex.ToString(), true);
            }
        }

        protected void Cmb_Partidas_Genericas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Verificar el indice
                if (Cmb_Partidas_Genericas.SelectedIndex > 1)
                {
                    Llena_Combo_Partidas_Especificas(Cmb_Partidas_Genericas.SelectedItem.Value);
                }
                else
                {
                    //Limpiar el combo de las partidas especificas
                    Cmb_Partidas_Especificas.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Partidas_Genericas_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Cmb_Tipo_Reporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //verificar el indice
                switch (Cmb_Tipo_Reporte.SelectedIndex)
                {
                    case 1:
                        Habilitar_Controles(true);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Clave";
                        break;

                    case 2:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Referencia de JAPAMI";
                        break;

                    case 3:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione la Clave";
                        break;

                    case 4:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Text = "Proporcione un Nombre del Producto";
                        break;

                    default:
                        Habilitar_Controles(false);
                        Lbl_Tipo_Busqueda.Visible = false;
                        Txt_Busqueda.Visible = false;
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Tipo_Reporte_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un reporte
                if (Cmb_Tipo_Reporte.SelectedIndex > 0)
                {
                    //verificar si no es por filtro
                    if (Cmb_Tipo_Reporte.SelectedIndex != 1)
                    {
                        //verificar si se escribio algo en la caja de texto
                        if (Txt_Busqueda.Text.Trim() == null || Txt_Busqueda.Text.Trim() == "" || Txt_Busqueda.Text.Trim() == String.Empty)
                        {
                            Mostrar_Error("Escriba el criterio de b&uacute;squeda para el reporte", true);
                        }
                        else
                        {
                            Elimina_Sesiones();
                            Llena_Grid_Productos();
                        }
                    }
                    else
                    {
                        Elimina_Sesiones();
                        Llena_Grid_Productos();
                    }
                }
                else
                {
                    Mostrar_Error("Seleccione alguna opci&oacute;n para el reporte", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                MPE_Comentarios_Chatarra.Hide();
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Cancelar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Aceptar_Click(object sender, EventArgs e)
        {
            //Declaracion de variables
            String Validacion = String.Empty; //variable para la validacion de los datos
            String[] Vec_Datos; //vector para los datos
            Int64 No_Salida = 0; 

            try
            {
                //Crear el vector de los datos
                Vec_Datos = Txt_Cadena_Control.Value.Split('_');

                //Verificar si estan los datos y estan bien
                Validacion = Validar_Datos();
                if (Validacion == String.Empty || Validacion == "" || Validacion == null)
                {
                    No_Salida = Agregar_Producto_Chatarra(Vec_Datos[0], Convert.ToInt32(Vec_Datos[1]), Convert.ToInt64(Vec_Datos[2]));

                    //verificar si hubo salida
                    if (No_Salida > 0)
                    {
                        string script = @"<script type='text/javascript'>alert('Se dio de alta el registro de producto chatarra con Salida No: " + No_Salida.ToString().Trim() + "');</script>";
                        ScriptManager.RegisterStartupScript(this, typeof(Page), "alerta", script, false);

                        Estado_Inicial();
                    }
                }
                else
                {
                    Mostrar_Error(Validacion, true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Aceptar_Click) " + ex.ToString(), true);
            }
        }

    #endregion
}