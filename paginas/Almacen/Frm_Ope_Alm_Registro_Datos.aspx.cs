﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Almacen_Registro_Datos.Negocio;
using JAPAMI.Sessiones;

public partial class paginas_Almacen_Frm_Ope_Alm_Registro_Datos : System.Web.UI.Page
{

    #region Variables Globales

    Cls_Ope_Com_Alm_Registro_De_Datos_Negocio Registro_Datos;

    #endregion

    #region Page Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Page_Load
    ///PARAMETROS:      
    ///CREO:Salvador Hernández Ramírez
    ///FECHA_CREO:02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Estatus_Inicial();
        }
    }

    #endregion Page Load

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Estatus_Inicial
    ///DESCRIPCIÓN: Estatus_Inicial
    ///PARAMETROS:      
    ///CREO:Salvador Hernández Ramírez
    ///FECHA_CREO:02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estatus_Inicial()
    {
        Btn_Salir.AlternateText = "Salir";
        Btn_Salir.ToolTip = "Salir";
        Btn_Guardar.Visible = false;
        Div_Ordenes_Compra.Visible = false;
        Div1_Datos_G_OC.Visible = false;
        Div_Busqueda_Av.Visible = true;
        Consultar_Ordenes_Compra();
        Estatus_Inicial_Botones(false);
        Limpiar_Controles();
        Div_Contenedor_Msj_Error.Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Controles
    ///DESCRIPCIÓN: Limpiar_Controles
    ///PARAMETROS:      
    ///CREO:Salvador Hernández Ramírez
    ///FECHA_CREO:02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        Txt_Orden_Compra.Text = "";
        Txt_Proveedor.Text = "";
        DataTable Dt_Limpiar = new DataTable();
        Grid_Registro_Datos.DataSource = Dt_Limpiar;
        Grid_Registro_Datos.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Ordenes_Compra
    ///DESCRIPCIÓN: Consultar_Ordenes_Compra
    ///PARAMETROS:      
    ///CREO:Salvador Hernández Ramírez
    ///FECHA_CREO:02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Consultar_Ordenes_Compra()
    {
        Registro_Datos = new Cls_Ope_Com_Alm_Registro_De_Datos_Negocio(); // Se crea el objeto
        DataTable Dt_Consultar_OC = new DataTable();   // Se crea la Tabla  que contendra las ordenes de compra que tengan productos que deben registrarse
        
        if (Txt_Busqueda.Text.Trim() != "")
            Registro_Datos.P_No_Orden_Compra = Txt_Busqueda.Text.Trim();
        else
            Registro_Datos.P_No_Orden_Compra = null;
        
        if (Txt_Req_Buscar.Text.Trim() != "")
            Registro_Datos.P_No_Requisicion = Txt_Req_Buscar.Text.Trim();
        else
            Registro_Datos.P_No_Requisicion = null;
        Registro_Datos.P_Almacen_General = Chk_Almacen_General.Checked ? "'SI'" : "'NO'";
        if (Chk_Fecha_B.Checked) // Si esta activado el Check
        {
            DateTime Date1 = new DateTime();  //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
            DateTime Date2 = new DateTime();

            if ((Txt_Fecha_Inicio.Text.Length != 0))
            {
                if ((Txt_Fecha_Inicio.Text.Length == 11) && (Txt_Fecha_Fin.Text.Length == 11))
                {
                    //Convertimos el Texto de los TextBox fecha a dateTime
                    Date1 = DateTime.Parse(Txt_Fecha_Inicio.Text);
                    Date2 = DateTime.Parse(Txt_Fecha_Fin.Text);

                    //Validamos que las fechas sean iguales o la final sea mayor que la inicial, de lo contrario se manda un mensaje de error 
                    if ((Date1 < Date2) | (Date1 == Date2))
                    {
                        if (Txt_Fecha_Fin.Text.Length != 0)
                        {
                            //Se convierte la fecha seleccionada por el usuario a un formato valido por oracle. 
                            Registro_Datos.P_Fecha_Inicio_B = Formato_Fecha(Txt_Fecha_Inicio.Text.Trim());
                            Registro_Datos.P_Fecha_Fin_B = Formato_Fecha(Txt_Fecha_Fin.Text.Trim());
                            Div_Contenedor_Msj_Error.Visible = false;
                        }
                        else
                        {
                            String Fecha = Formato_Fecha(Txt_Fecha_Inicio.Text.Trim()); //Se convierte la fecha seleccionada por el usuario a un formato valido por oracle. 
                            Registro_Datos.P_Fecha_Inicio_B = Fecha;
                            Registro_Datos.P_Fecha_Fin_B = Fecha;
                            Div_Contenedor_Msj_Error.Visible = false;
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = " Fecha no valida ";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Text = " Fecha no valida ";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
        }

        Dt_Consultar_OC = Registro_Datos.Consulta_Ordenes_Compra();  // Se consultan las Ordenes de Compra

        if (Dt_Consultar_OC.Rows.Count > 0)
        {
            Grid_Ordenes_Compra.Columns[1].Visible = true;
            Grid_Ordenes_Compra.Columns[8].Visible = true; // NO_REQUISICION
            Grid_Ordenes_Compra.DataSource = Dt_Consultar_OC;
            Grid_Ordenes_Compra.DataBind();
            Grid_Ordenes_Compra.Columns[1].Visible = false;
            Grid_Ordenes_Compra.Columns[8].Visible = false; // NO_REQUISICION
            Div_Ordenes_Compra.Visible = true;
            Div_Contenedor_Msj_Error.Visible = false;
        }
        else
        {
            Lbl_Mensaje_Error.Text = "No se encontraron ordenes de compra con productos para registrarse";
            Div_Contenedor_Msj_Error.Visible = true;
            Div_Ordenes_Compra.Visible = false;

        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Fecha
    ///DESCRIPCIÓN:     Metodo que cambia el mes dic a dec para que oracle lo acepte
    ///PARAMETROS:      1.- String Fecha, es la fecha a la cual se le cambiara el formato 
    ///                     en caso de que cumpla la condicion del if
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public String Formato_Fecha(String Fecha)
    {
        String Fecha_Valida = Fecha;
        //Se le aplica un split a la fecha 
        String[] aux = Fecha.Split('/');
        //Concatenamos la fecha, y se cambia el orden a DD-MMM-YYYY para que sea una fecha valida para oracle
        Fecha_Valida = aux[0] + "-" + aux[1] + "-" + aux[2];
        return Fecha_Valida;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Controles
    ///DESCRIPCIÓN:Validar_Controles
    ///PARAMETROS:
    ///CREO:Salvador Hernández Ramírez
    ///FECHA_CREO:02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Controles()
    {
        Boolean Validacion = true;
        String Mensaje = "";


        // Se valida que el los productos del grid se hayan el color y el material
        for (int i = 0; i < Grid_Registro_Datos.Rows.Count; i++)
        {
            DropDownList Cmb_Marca_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Marca");
            DropDownList Cmb_Color_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Color");
            DropDownList Cmb_Material_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Material");
            TextBox Txt_Modelo_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Modelo");
            TextBox Txt_No_Serie_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_No_Serie");
            TextBox Txt_Garantia_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Garantia");
            TextBox Txt_Observaciones_Producto_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Observaciones_Producto");

            if (Cmb_Marca_T.SelectedIndex == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar la Marca para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Cmb_Color_T.SelectedIndex == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar el Color para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Cmb_Material_T.SelectedIndex == 0 && Cmb_Material_T.Enabled == true)
            {
                Validacion = false;
                Mensaje += " + Seleccionar el Material para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Txt_Modelo_T.Text.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar el Modelo para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Txt_No_Serie_T.Text.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar el No. Serie para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Txt_Garantia_T.Text.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar la Garantia para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }

            if (Txt_Observaciones_Producto_T.Text.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje += " + Seleccionar las Observaciones para todos los productos. <br />";
                i = Grid_Registro_Datos.Rows.Count;
            }
        }

        if (Validacion == false)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje);
            Lbl_Mensaje_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Estatus_Inicial_Botones
    ///DESCRIPCIÓN:          Método utilizado asignarle el estatus correspondiente a los botones
    ///PROPIEDADES:     
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           24/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Estatus_Inicial_Botones(Boolean Estatus)
    {
        if (Estatus)
        {
            Btn_Salir.AlternateText = "Atras";
            Btn_Salir.ToolTip = "Atras";
            Div_Busqueda_Av.Visible = false;
        }
        else
        {
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ToolTip = "Salir";
            Div_Busqueda_Av.Visible = true;
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA CLASE:     Estado_Inicial_Busqueda_Avanzada
    /// DESCRIPCION:            Colocar la ventana de la busqued avanzada en un estado inicial
    /// PARAMETROS :            
    /// CREO       :            Salvador Hernández Ramírez
    /// FECHA_CREO :            18/Enero/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************/
    private void Estado_Inicial_Busqueda_Avanzada()
    {
        try
        {
            Chk_Fecha_B.Checked = false;
            Txt_Fecha_Inicio.Text = "";
            Txt_Fecha_Fin.Text = "";
            Txt_Busqueda.Text = "";
            Txt_Req_Buscar.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Dt_Productos_Serializados
    ///DESCRIPCIÓN:     Metodo utilizado para llenar la tabla con productos serializados
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private DataTable Llenar_Dt_Productos_Serializados()
    {

        DataTable Dt_Productos_Serializados = new DataTable(); // Se crea la tabla utilizada para guardar los productos serialziados

        Dt_Productos_Serializados.Columns.Add("PRODUCTO_ID");
        Dt_Productos_Serializados.Columns.Add("MARCA_ID");
        Dt_Productos_Serializados.Columns.Add("COLOR_ID");
        Dt_Productos_Serializados.Columns.Add("MATERIAL_ID");
        Dt_Productos_Serializados.Columns.Add("MODELO");
        Dt_Productos_Serializados.Columns.Add("NO_SERIE");
        Dt_Productos_Serializados.Columns.Add("GARANTIA");
        Dt_Productos_Serializados.Columns.Add("CANTIDAD");
        Dt_Productos_Serializados.Columns.Add("OBSERVACIONES");
        Dt_Productos_Serializados.Columns.Add("DESCRIPCION");
        Dt_Productos_Serializados.Columns.Add("TIPO");


        for (int i = 0; i < Grid_Registro_Datos.Rows.Count; i++) // Se recorre el grid
        {
            String Producto_ID = HttpUtility.HtmlDecode(Grid_Registro_Datos.Rows[i].Cells[4].Text.Trim()); // Se consulta el ID del Producto
            String Cantidad = HttpUtility.HtmlDecode(Grid_Registro_Datos.Rows[i].Cells[5].Text.Trim()); // Se consulta La cantidad
            String Descripcion = HttpUtility.HtmlDecode(Grid_Registro_Datos.Rows[i].Cells[1].Text.Trim()); // Se consulta La cantidad
            String Tipo = HttpUtility.HtmlDecode(Grid_Registro_Datos.Rows[i].Cells[3].Text.Trim()); // Se consulta La cantidad

            TextBox Txt_No_Serie_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_No_Serie");
            TextBox Txt_Observaciones_T = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Observaciones_Producto");
            TextBox Txt_Modelo = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Modelo");
            TextBox Txt_Garantia = (TextBox)Grid_Registro_Datos.Rows[i].FindControl("Txt_Garantia");
            DropDownList Cmb_Color_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Color");
            DropDownList Cmb_Material_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Material");
            DropDownList Cmb_Marca_T = (DropDownList)Grid_Registro_Datos.Rows[i].FindControl("Cmb_Marca");
            DataRow Registro = Dt_Productos_Serializados.NewRow(); // Se crea un nuevo registro de la tabla "Dt_Productos_Serializados"

            Registro["PRODUCTO_ID"] = Producto_ID.Trim();
            Registro["DESCRIPCION"] = Descripcion.Trim();
            Registro["TIPO"] = Tipo.Trim();

            if (Cmb_Marca_T.SelectedIndex == 0)
                Registro["MARCA_ID"] = "";
            else
                Registro["MARCA_ID"] = "" + Cmb_Marca_T.SelectedValue;

            Registro["COLOR_ID"] = "" + Cmb_Color_T.SelectedValue;
            Registro["MATERIAL_ID"] = "" + Cmb_Material_T.SelectedValue;

            if (Txt_Modelo.Text.Trim() == "") // Se Optiene el Modelo
                Registro["MODELO"] = "INDISTINTO";
            else
                Registro["MODELO"] = Txt_Modelo.Text.Trim();

            if (Txt_No_Serie_T.Text.Trim() == "") // Se Optiene el No. Serie
                Registro["NO_SERIE"] = "SIN SERIE";
            else
                Registro["NO_SERIE"] = Txt_No_Serie_T.Text.Trim();

            if (Txt_Garantia.Text.Trim() == "") // Se optiene La Garantia
                Registro["GARANTIA"] = "INDISTINTA";
            else
                Registro["GARANTIA"] = Txt_Garantia.Text.Trim();

            Registro["CANTIDAD"] = Cantidad.Trim();

            if (Txt_Observaciones_T.Text.Trim() != "")
            {
                String Observaciones = "";
                if (Txt_Observaciones_T.Text.Trim().Length > 249)
                    Observaciones = Txt_Observaciones_T.Text.Trim().Substring(0, 249);
                else
                    Observaciones = Txt_Observaciones_T.Text.Trim();

                Registro["OBSERVACIONES"] = Observaciones;
            }
            else
            {
                Registro["OBSERVACIONES"] = "";
            }
            Dt_Productos_Serializados.Rows.InsertAt(Registro, i); // Se Inserta el Registro
        }
        return Dt_Productos_Serializados;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: LLenar_Combos_Masivos
    ///DESCRIPCIÓN:     LLenar_Combos_Masivos
    ///PARAMETROS:      
    ///CREO:            Francisco A. Gallardo Castañeda
    ///FECHA_CREO:      07/Mayo/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void LLenar_Combos_Masivos(DataTable Dt_Productos)
    {
        Registro_Datos = new Cls_Ope_Com_Alm_Registro_De_Datos_Negocio();
        Registro_Datos.P_Tipo_Tabla = "MATERIALES";
        DataTable Dt_Materiales = Registro_Datos.Consulta_Tablas();

        DataRow Fila_Materiales = Dt_Materiales.NewRow();
        Fila_Materiales["MATERIAL_ID"] = "SELECCIONE";
        Fila_Materiales["MATERIAL"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
        Dt_Materiales.Rows.InsertAt(Fila_Materiales, 0);


        // SE LLENA LA TABLA COLORES
        Registro_Datos.P_Tipo_Tabla = "COLORES";
        DataTable Dt_Colores = Registro_Datos.Consulta_Tablas();

        DataRow Fila_Colores = Dt_Colores.NewRow();
        Fila_Colores["COLOR_ID"] = "SELECCIONE";
        Fila_Colores["COLOR"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
        Dt_Colores.Rows.InsertAt(Fila_Colores, 0);

        // SE LLENA LA TABLA MARCAS
        Registro_Datos.P_Tipo_Tabla = "MARCAS";
        DataTable Dt_Marcas = Registro_Datos.Consulta_Tablas();

        Cmb_Color_Masiva.DataSource = Dt_Colores;
        Cmb_Color_Masiva.DataTextField = "COLOR";
        Cmb_Color_Masiva.DataValueField = "COLOR_ID";
        Cmb_Color_Masiva.DataBind();

        Cmb_Material_Masiva.DataSource = Dt_Materiales;
        Cmb_Material_Masiva.DataTextField = "MATERIAL";
        Cmb_Material_Masiva.DataValueField = "MATERIAL_ID";
        Cmb_Material_Masiva.DataBind();

        Cmb_Marca_Masiva.DataSource = Dt_Marcas;
        Cmb_Marca_Masiva.DataTextField = "MARCA";
        Cmb_Marca_Masiva.DataValueField = "MARCA_ID";
        Cmb_Marca_Masiva.DataBind();
        Cmb_Marca_Masiva.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

        Cmb_Producto_Masivo.DataSource = Dt_Productos;
        Cmb_Producto_Masivo.DataTextField = "PRODUCTO";
        Cmb_Producto_Masivo.DataValueField = "PRODUCTO_ID";
        Cmb_Producto_Masivo.DataBind();
        Cmb_Producto_Masivo.Items.Insert(0, new ListItem("<-- TODOS -->", ""));

        Txt_Modelo_Masiva.Text = "";
        Txt_No_Serie_Masiva.Text = "";
        Txt_Garantia_Masiva.Text = "";
        Txt_Observaciones_Masiva.Text = "";

    }

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Aplicar_Masivo_Click
    ///DESCRIPCIÓN:     Btn_Aplicar_Masivo_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Aplicar_Masivo_Click(object sender, EventArgs e)
    {
        String Producto_ID = String.Empty;
        String Marca_ID = String.Empty;
        String Color_ID = String.Empty;
        String Material_ID = String.Empty;
        String Modelo = String.Empty;
        String No_Serie = String.Empty;
        String Garantia = String.Empty;
        String Observaciones = String.Empty;
        if (Cmb_Producto_Masivo.SelectedIndex > 0) Producto_ID = Cmb_Producto_Masivo.SelectedItem.Value.Trim();
        if (Cmb_Marca_Masiva.SelectedIndex > 0) Marca_ID = Cmb_Marca_Masiva.SelectedItem.Value.Trim();
        if (Cmb_Color_Masiva.SelectedIndex > 0) Color_ID = Cmb_Color_Masiva.SelectedItem.Value.Trim();
        if (Cmb_Material_Masiva.SelectedIndex > 0) Material_ID = Cmb_Material_Masiva.SelectedItem.Value.Trim();
        if (Txt_Modelo_Masiva.Text.Trim().Length > 0) Modelo = Txt_Modelo_Masiva.Text.Trim();
        if (Txt_No_Serie_Masiva.Text.Trim().Length > 0) No_Serie = Txt_No_Serie_Masiva.Text.Trim();
        if (Txt_Garantia_Masiva.Text.Trim().Length > 0) Garantia = Txt_Garantia_Masiva.Text.Trim();
        if (Txt_Observaciones_Masiva.Text.Trim().Length > 0) Observaciones = Txt_Observaciones_Masiva.Text.Trim();
        foreach (GridViewRow Fila_Grid in Grid_Registro_Datos.Rows)
        {
            try
            {
                String Producto_Grid_Row = HttpUtility.HtmlDecode(Fila_Grid.Cells[4].Text).Trim();
                Boolean Aplica_Fila = true;
                if (!String.IsNullOrEmpty(Producto_ID.Trim()) && !Producto_ID.Trim().Equals(Producto_Grid_Row.Trim())) Aplica_Fila = false;
                if (Aplica_Fila)
                {
                    DropDownList Cmb_Tmp_Marca = (DropDownList)Fila_Grid.FindControl("Cmb_Marca");
                    DropDownList Cmb_Tmp_Color = (DropDownList)Fila_Grid.FindControl("Cmb_Color");
                    DropDownList Cmb_Tmp_Material = (DropDownList)Fila_Grid.FindControl("Cmb_Material");
                    TextBox Txt_Tmp_Modelo = (TextBox)Fila_Grid.FindControl("Txt_Modelo");
                    TextBox Txt_Tmp_No_Serie = (TextBox)Fila_Grid.FindControl("Txt_No_Serie");
                    TextBox Txt_Tmp_Garantia = (TextBox)Fila_Grid.FindControl("Txt_Garantia");
                    TextBox Txt_Tmp_Observaciones = (TextBox)Fila_Grid.FindControl("Txt_Observaciones_Producto");
                    if (Cmb_Tmp_Marca != null && !String.IsNullOrEmpty(Marca_ID)) Cmb_Tmp_Marca.SelectedIndex = Cmb_Tmp_Marca.Items.IndexOf(Cmb_Tmp_Marca.Items.FindByValue(Marca_ID));
                    if (Cmb_Tmp_Color != null && !String.IsNullOrEmpty(Color_ID)) Cmb_Tmp_Color.SelectedIndex = Cmb_Tmp_Color.Items.IndexOf(Cmb_Tmp_Color.Items.FindByValue(Color_ID));
                    if (Cmb_Tmp_Material != null && !String.IsNullOrEmpty(Material_ID)) Cmb_Tmp_Material.SelectedIndex = Cmb_Tmp_Material.Items.IndexOf(Cmb_Tmp_Material.Items.FindByValue(Material_ID));
                    if (Txt_Tmp_Modelo != null && !String.IsNullOrEmpty(Modelo)) Txt_Tmp_Modelo.Text = Modelo.Trim();
                    if (Txt_Tmp_No_Serie != null && !String.IsNullOrEmpty(No_Serie)) Txt_Tmp_No_Serie.Text = No_Serie.Trim();
                    if (Txt_Tmp_Garantia != null && !String.IsNullOrEmpty(Garantia)) Txt_Tmp_Garantia.Text = Garantia.Trim();
                    if (Txt_Tmp_Observaciones != null && !String.IsNullOrEmpty(Observaciones)) Txt_Tmp_Observaciones.Text = Observaciones.Trim();
                }
            }
            catch (Exception Ex)
            {
                continue;
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Click
    ///DESCRIPCIÓN:     Btn_Guardar_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Guardar_Click(object sender, ImageClickEventArgs e)
    {
        if (Session["No_Orden_Compra_RD"] != null)
        {
            Registro_Datos = new Cls_Ope_Com_Alm_Registro_De_Datos_Negocio();
            DataTable Dt_Productos_Serializados = new DataTable();

            if (Validar_Controles())
            {

                String No_OC = Session["No_Orden_Compra_RD"].ToString().Trim();
                Registro_Datos.P_No_Orden_Compra = No_OC;

                Dt_Productos_Serializados = Llenar_Dt_Productos_Serializados(); // Se llenan los productos que han sido serializados
                Registro_Datos.P_Dt_Productos_Serializados = Dt_Productos_Serializados;
                Registro_Datos.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;

                if (Session["NO_REQ_RD"] != "")
                    Registro_Datos.P_No_Requisicion = Session["NO_REQ_RD"].ToString().Trim();

                Registro_Datos.Alta_Productos_Inventario();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Registro de Datos", "alert('Registro Completo de Datos');", true);

                Estatus_Inicial();

            }
            else
                Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:     Btn_Salir_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.AlternateText == "Salir")
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else if (Btn_Salir.AlternateText == "Atras")
        {
            Estatus_Inicial();
            Estado_Inicial_Busqueda_Avanzada();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_OC_Click
    ///DESCRIPCIÓN:     Btn_Seleccionar_OC_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_OC_Click(object sender, ImageClickEventArgs e)
    {
        // Declaración de Objetos y Variables
        ImageButton Btn_Selec_Orden_Compra = null;
        String No_Orden_Compra = String.Empty;

        Btn_Selec_Orden_Compra = (ImageButton)sender;
        No_Orden_Compra = Btn_Selec_Orden_Compra.CommandArgument;

        Session["No_Orden_Compra_RD"] = No_Orden_Compra.Trim();

        Registro_Datos = new Cls_Ope_Com_Alm_Registro_De_Datos_Negocio(); // Se crea el objeto para el manejo de los métodos
        DataTable Dt_Productos_OC = new DataTable();
        DataTable Dt_Productos_Registrar = new DataTable();
        DataTable Dt_Materiales = new DataTable();
        DataTable Dt_Colores = new DataTable();
        DataTable Dt_Marcas = new DataTable();
        DataTable Dt_Modelos = new DataTable();
        DataTable Dt_Datos_Generales_OC = new DataTable();

        Registro_Datos.P_No_Orden_Compra = No_Orden_Compra.Trim();
        Dt_Productos_OC = Registro_Datos.Consulta_Productos_Orden_Compra(); // Se consultan los productos a registrar de la orden de compra

        Dt_Productos_Registrar = Dt_Productos_OC.Clone(); // Se clona la tabla

        Cmb_Material_Masiva.Enabled = true;

        if (Dt_Productos_OC.Rows.Count > 0)
        {
            for (int i = 0; i < Dt_Productos_OC.Rows.Count; i++)
            { // Se ahce el recorrio de los productos para ver cuales se van a regsitrar

                Double Cantidad = 0;

                Cantidad = Convert.ToDouble("" + Dt_Productos_OC.Rows[i]["CANTIDAD"].ToString().Trim());

                String Unidad = Convert.ToString("" + Dt_Productos_OC.Rows[i]["UNIDAD"].ToString().Trim());
                String Totalidad = Convert.ToString("" + Dt_Productos_OC.Rows[i]["TOTALIDAD"].ToString().Trim());
                String Producto_ID = Convert.ToString("" + Dt_Productos_OC.Rows[i]["PRODUCTO_ID"].ToString().Trim());

                DataRow[] Registro;

                Double Cantidad_Registros_Agregar = 0;

                if (Totalidad == "SI")
                    Cantidad_Registros_Agregar = 1;
                else if (Unidad == "SI")
                    Cantidad_Registros_Agregar = Cantidad;

                Registro = Dt_Productos_OC.Select("PRODUCTO_ID = '" + Producto_ID.Trim() + "'");// Se crea el registro con la información de la tabla

                for (int h = 0; h < Cantidad_Registros_Agregar; h++) // Se inserta las veces que es comveniente en base a la variable Cantidad_Registros_Agregar
                {
                    Int16 Longitud = Convert.ToInt16(Dt_Productos_Registrar.Rows.Count);
                    DataRow Dr_Producto = Dt_Productos_Registrar.NewRow(); // Se crea un registro para agregarlo a la tabla

                    Dr_Producto["UNIDAD"] = Registro[0]["UNIDAD"].ToString().Trim();
                    Dr_Producto["TOTALIDAD"] = Registro[0]["TOTALIDAD"].ToString().Trim();
                    Dr_Producto["CLAVE"] = Registro[0]["CLAVE"].ToString().Trim();
                    Dr_Producto["PRODUCTO"] = Registro[0]["PRODUCTO"].ToString().Trim();
                    Dr_Producto["DESCRIPCION"] = Registro[0]["DESCRIPCION"].ToString().Trim();

                    if (Totalidad == "SI")
                        Dr_Producto["CANTIDAD"] = Registro[0]["CANTIDAD"].ToString().Trim();
                    else if (Unidad == "SI")
                        Dr_Producto["CANTIDAD"] = "1";

                    Dr_Producto["TIPO"] = Registro[0]["TIPO"].ToString().Trim();
                    Dr_Producto["NO_INVENTARIO"] = Registro[0]["NO_INVENTARIO"].ToString().Trim();
                    Dr_Producto["PRODUCTO_ID"] = Registro[0]["PRODUCTO_ID"].ToString().Trim();

                    if (Longitud == 0)
                        Dt_Productos_Registrar.Rows.InsertAt(Dr_Producto, Longitud);
                    else
                        Dt_Productos_Registrar.Rows.InsertAt(Dr_Producto, (Longitud + 1));
                }
            }
        }

        if (Dt_Productos_Registrar.Rows.Count > 0) // Si la tabla trae registros
        {
            // Se Consulta el No_Invntario Consecutivo 
            Registro_Datos.P_Tipo_Tabla = "BIENES_MUEBLES";
            if (Dt_Productos_Registrar.Rows[0]["TIPO"].ToString().Trim().Equals("VEHICULO")) Registro_Datos.P_Tipo_Tabla = "VEHICULO";
            Int64 Inventario_Productos = Registro_Datos.Consulta_Consecutivo(); // Se consulta el Consecutivo de la tabla de inventarios bienes muebles

            // Se recorre la tabla para insertar su numero de inventario
            for (int h = 0; h < Dt_Productos_Registrar.Rows.Count; h++)
            {
                // String Tipo_Producto = Dt_Productos_Registrar.Rows[h]["TIPO"].ToString().Trim(); // Dependiendo el tipo de dato, se inserta en la tabla
                Dt_Productos_Registrar.Rows[h].SetField("NO_INVENTARIO", Inventario_Productos);
                Inventario_Productos = Inventario_Productos + 1;
            }

            Grid_Registro_Datos.Columns[3].Visible = true;
            Grid_Registro_Datos.Columns[4].Visible = true;
            Grid_Registro_Datos.Columns[5].Visible = true;
            Grid_Registro_Datos.DataSource = Dt_Productos_Registrar; // Se agregan los datos al grid
            Grid_Registro_Datos.DataBind();
            Grid_Registro_Datos.Columns[3].Visible = false;
            Grid_Registro_Datos.Columns[4].Visible = false;
            Grid_Registro_Datos.Columns[5].Visible = false;

            // Son llenadas las tablas que se van utilizar para mostrar los combos en el grid
            // SE LLENA LA TABLA MATERIALES
            Registro_Datos.P_Tipo_Tabla = "MATERIALES";
            Dt_Materiales = Registro_Datos.Consulta_Tablas();

            DataRow Fila_Materiales = Dt_Materiales.NewRow();
            Fila_Materiales["MATERIAL_ID"] = "SELECCIONE";
            Fila_Materiales["MATERIAL"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Dt_Materiales.Rows.InsertAt(Fila_Materiales, 0);


            // SE LLENA LA TABLA COLORES
            Registro_Datos.P_Tipo_Tabla = "COLORES";
            Dt_Colores = Registro_Datos.Consulta_Tablas();

            DataRow Fila_Colores = Dt_Colores.NewRow();
            Fila_Colores["COLOR_ID"] = "SELECCIONE";
            Fila_Colores["COLOR"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Dt_Colores.Rows.InsertAt(Fila_Colores, 0);

            // SE LLENA LA TABLA MARCAS
            Registro_Datos.P_Tipo_Tabla = "MARCAS";
            Dt_Marcas = Registro_Datos.Consulta_Tablas();

            DataRow Fila_Marcas = Dt_Marcas.NewRow();
            Fila_Marcas["MARCA_ID"] = "SELECCIONE";
            Fila_Marcas["MARCA"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Dt_Marcas.Rows.InsertAt(Fila_Marcas, 0);

            for (int n = 0; n < Grid_Registro_Datos.Rows.Count; n++) // Se hace el recorrido del Grid
            {
                DropDownList Cmb_Materiales_Temporal = (DropDownList)Grid_Registro_Datos.Rows[n].FindControl("Cmb_Material");
                DropDownList Cmb_Colores_Temporal = (DropDownList)Grid_Registro_Datos.Rows[n].FindControl("Cmb_Color");
                DropDownList Cmb_Marcas_Temporal = (DropDownList)Grid_Registro_Datos.Rows[n].FindControl("Cmb_Marca");
                String Tipo_Producto = HttpUtility.HtmlDecode(Grid_Registro_Datos.Rows[n].Cells[3].Text).Trim();

                if (Dt_Materiales.Rows.Count > 0) // Se agregan los Materiales al combo que se encuentra en el grid
                {
                    Cmb_Materiales_Temporal.DataSource = Dt_Materiales;
                    Cmb_Materiales_Temporal.DataValueField = "MATERIAL_ID";
                    Cmb_Materiales_Temporal.DataTextField = "MATERIAL";
                    Cmb_Materiales_Temporal.DataBind();
                    Cmb_Materiales_Temporal.SelectedIndex = 0;

                    // Se le agrega un ToolTip a cada elemento del combo, ya que los valores estan muy grandes.
                    if (Cmb_Materiales_Temporal != null)
                        foreach (ListItem li in Cmb_Materiales_Temporal.Items)
                            li.Attributes.Add("title", li.Text);

                    //En caso de ser Vehículo se inhabilita la opción de Material
                    if (Tipo_Producto.Trim().Equals("VEHICULO")) { Cmb_Materiales_Temporal.Enabled = false; Cmb_Material_Masiva.Enabled = false; }
                }

                if (Dt_Colores.Rows.Count > 0) // Se agregan los Colores al combo que se encuentra en el grid
                {
                    Cmb_Colores_Temporal.DataSource = Dt_Colores;
                    Cmb_Colores_Temporal.DataValueField = "COLOR_ID";
                    Cmb_Colores_Temporal.DataTextField = "COLOR";
                    Cmb_Colores_Temporal.DataBind();
                    Cmb_Colores_Temporal.SelectedIndex = 0;

                    // Se le agrega un ToolTip a cada elemento del combo, ya que los valores estan muy grandes.
                    if (Cmb_Colores_Temporal != null)
                        foreach (ListItem li in Cmb_Colores_Temporal.Items)
                            li.Attributes.Add("title", li.Text);
                }

                if (Dt_Marcas.Rows.Count > 0) // Se agregan las Marcas al combo que se encuentra en el grid
                {
                    Cmb_Marcas_Temporal.DataSource = Dt_Marcas;
                    Cmb_Marcas_Temporal.DataValueField = "MARCA_ID";
                    Cmb_Marcas_Temporal.DataTextField = "MARCA";
                    Cmb_Marcas_Temporal.DataBind();
                    Cmb_Marcas_Temporal.SelectedIndex = 0;

                    // Se le agrega un ToolTip a cada elemento del combo, ya que los valores estan muy grandes.
                    if (Cmb_Marcas_Temporal != null)
                        foreach (ListItem li in Cmb_Marcas_Temporal.Items)
                            li.Attributes.Add("title", li.Text);
                }
            }

            Registro_Datos.P_Tipo_Tabla = "DATOS_GENERALES_OC";
            Registro_Datos.P_No_Orden_Compra = No_Orden_Compra;
            Dt_Datos_Generales_OC = Registro_Datos.Consulta_Tablas();

            if (Dt_Datos_Generales_OC.Rows.Count > 0)
            {
                Txt_Orden_Compra.Text = HttpUtility.HtmlDecode("" + Dt_Datos_Generales_OC.Rows[0]["FOLIO"].ToString().Trim());
                Txt_Proveedor.Text = HttpUtility.HtmlDecode("" + Dt_Datos_Generales_OC.Rows[0]["COMPANIA"].ToString().Trim());
                Txt_Requisicion.Text = HttpUtility.HtmlDecode("" + Dt_Datos_Generales_OC.Rows[0]["REQUISICION"].ToString().Trim());

                if (Dt_Datos_Generales_OC.Rows[0]["NO_REQUISICION"].ToString().Trim() != "")
                    Session["NO_REQ_RD"] = HttpUtility.HtmlDecode("" + Dt_Datos_Generales_OC.Rows[0]["NO_REQUISICION"].ToString().Trim());

                Session["NO_CONTRA_RECIBO_RD"] = HttpUtility.HtmlDecode("" + Dt_Datos_Generales_OC.Rows[0]["NO_CONTRA_RECIBO"].ToString().Trim());
            }

            LLenar_Combos_Masivos(Dt_Productos_Registrar.DefaultView.ToTable(true, "PRODUCTO_ID", "PRODUCTO"));

            Div_Busqueda_Av.Visible = false;
            Div_Ordenes_Compra.Visible = false;
            Div1_Datos_G_OC.Visible = true;
            Btn_Guardar.Visible = true;
            Estatus_Inicial_Botones(true);  // Se configuran los botones 
        }
        else
        {
            Div1_Datos_G_OC.Visible = false;
            Btn_Guardar.Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Fecha_B_CheckedChanged
    ///DESCRIPCIÓN:     Chk_Fecha_B_CheckedChanged
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Fecha_B_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Fecha_B.Checked == true)
        {
            Img_Btn_Fecha_Inicio.Enabled = true;
            Img_Btn_Fecha_Fin.Enabled = true;
            Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/yyyy");
            Txt_Fecha_Fin.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        }
        else
        {
            Img_Btn_Fecha_Inicio.Enabled = false;
            Img_Btn_Fecha_Fin.Enabled = false;
            Txt_Fecha_Inicio.Text = "";
            Txt_Fecha_Fin.Text = "";
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN:     Btn_Buscar_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Consultar_Ordenes_Compra(); // Se consultan las ordenes de compra
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Click
    ///DESCRIPCIÓN:     Btn_Limpiar_Click
    ///PARAMETROS:      
    ///CREO:            Salvador Hernández Ramírez
    ///FECHA_CREO:      02/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
    {
        Estado_Inicial_Busqueda_Avanzada();
    }

    #endregion Eventos

}
