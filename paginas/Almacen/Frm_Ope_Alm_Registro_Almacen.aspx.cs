﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Almacen_Recepcion_Material.Negocio;
using JAPAMI.Almacen_Registrar_Factura.Negocio;

public partial class paginas_Almacen_Frm_Ope_Alm_Registro_Almacen : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN         : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Configuracion_Formulario(true);
                Llenar_Grid_Ordenes_Compra(0);
                Llena_Combo_Proveedores();
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
        ///DESCRIPCIÓN         : Carga una configuracion de los controles del Formulario
        ///PARAMETROS          : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                  de los controles.
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Div_Busqueda.Visible = Estatus;
            Div_Orden_Compra.Visible = !Estatus;
            Btn_Guardar.Visible = !Estatus;
            Grid_Ordenes_Compra.SelectedIndex = -1;

            Btn_Salir.AlternateText = Estatus ? "Salir" : "Cancelar";

            if (Estatus)
            {
                Limpiar_Catalogo();
                Llenar_Grid_Ordenes_Compra(0);
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Ordenes_Compra
        ///DESCRIPCIÓN         : Llena el Listado con una consulta que puede o no
        ///                      tener Filtros.
        ///PROPIEDADES         : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llenar_Grid_Ordenes_Compra(int Pagina)
        {
            Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();

            if (!String.IsNullOrEmpty(Txt_Busqueda_Requisicion.Text))
            { Negocio.P_No_Requisicion = Txt_Busqueda_Requisicion.Text.Trim(); }

            if (!String.IsNullOrEmpty(Txt_Busqueda_Orden_Compra.Text))
            { Negocio.P_No_Orden_Compra = Txt_Busqueda_Orden_Compra.Text.Trim(); }

            if (Chk_Proveedor.Checked)
            { Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedValue.Trim(); }

            if (Chk_Fecha.Checked)
            {
                Negocio.P_Fecha_Inicio_B = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(Txt_Fecha_Inicio.Text)) + " 00:00:00";
                Negocio.P_Fecha_Fin_B = String.Format("{0:dd/MM/yyyy}", DateTime.Parse(Txt_Fecha_Fin.Text)) + " 23:59:59";
            }

            Negocio.P_Almacen_General = Chk_Almacen_General.Checked ? "'SI'" : "'NO'";
            Negocio.P_Estatus = "'SURTIDA_REGISTRO', 'SURTIDA_PARCIAL'";
            Grid_Ordenes_Compra.DataSource = Negocio.Consulta_Ordenes_Compra();
            Grid_Ordenes_Compra.Columns[7].Visible = true;
            Grid_Ordenes_Compra.Columns[8].Visible = true;
            Grid_Ordenes_Compra.DataBind();
            Grid_Ordenes_Compra.Columns[7].Visible = false;
            Grid_Ordenes_Compra.Columns[8].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
        ///DESCRIPCIÓN         : Limpia los controles del Formulario
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Chk_Fecha.Checked = false;
            Chk_Proveedor.Checked = false;
            Chk_Almacen_General.Checked = false;
            Txt_Fecha_Fin.Text = String.Empty;
            Txt_Fecha_Inicio.Text = String.Empty;
            Txt_Busqueda_Orden_Compra.Text = String.Empty;
            Txt_Busqueda_Requisicion.Text = String.Empty;
            Txt_Folio.Text = String.Empty;
            Txt_Fecha_Construccion.Text = String.Empty;
            Txt_Requisicion.Text = String.Empty;
            Txt_Estatus.Text = String.Empty;
            Txt_Proveedor.Text = String.Empty;
            Lbl_SubTotal.Text = "$ 0.00";
            Lbl_IEPS.Text = "$ 0.00";
            Lbl_IVA.Text = "$ 0.00";
            Lbl_Total.Text = "$ 0.00";
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llena_Combo_Proveedores
        ///DESCRIPCIÓN         : 
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012 
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        private void Llena_Combo_Proveedores()
        {
            Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();
            Cmb_Proveedores.DataSource = Negocio.Consulta_Proveedores();
            Cmb_Proveedores.DataTextField = Cat_Com_Proveedores.Campo_Nombre;
            Cmb_Proveedores.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
            Cmb_Proveedores.DataBind();
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Click
        ///DESCRIPCIÓN         : Realiza el registro de resguardos.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Guardar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Alm_Registrar_Factura_Negocio Negocio = new Cls_Ope_Alm_Registrar_Factura_Negocio();
            DataTable Dt_Productos = new DataTable();
            Boolean Registro_Datos = true;

            try
            {
                Dt_Productos.Columns.Add("PRODUCTO_ID", typeof(System.String));
                Dt_Productos.Columns.Add("RESGUARDO", typeof(System.String));
                Dt_Productos.Columns.Add("CUSTODIA", typeof(System.String));
                Dt_Productos.Columns.Add("RECIBO", typeof(System.String));
                Dt_Productos.Columns.Add("UNIDAD", typeof(System.String));
                Dt_Productos.Columns.Add("TOTALIDAD", typeof(System.String));
                Dt_Productos.Columns.Add("RECIBO_TRANSITORIO", typeof(System.String));

                foreach (GridViewRow Dr_Renglon in Grid_Orden_Compra_Detalles.Rows)
                {
                    DropDownList Cmb_Almacen = (DropDownList)Dr_Renglon.Cells[9].FindControl("Cmb_Almacen");
                    DataRow Dr_Producto = Dt_Productos.NewRow();
                    Dr_Producto["PRODUCTO_ID"] = HttpUtility.HtmlDecode(Dr_Renglon.Cells[0].Text.Trim());
                    Dr_Producto["RESGUARDO"] = Cmb_Almacen.SelectedValue == "R" ? "SI" : String.Empty;
                    Dr_Producto["CUSTODIA"] = Cmb_Almacen.SelectedValue == "C" ? "SI" : String.Empty;
                    Dr_Producto["RECIBO"] = "";
                    if (Dr_Producto["RESGUARDO"].ToString().Trim() == "SI" || Dr_Producto["CUSTODIA"].ToString().Trim() == "SI")
                        Dr_Producto["UNIDAD"] = "SI";
                    
                    Dr_Producto["TOTALIDAD"] = String.Empty;
                    Dr_Producto["RECIBO_TRANSITORIO"] = "";

                    if (!String.IsNullOrEmpty(Dr_Producto["RESGUARDO"].ToString()) || !String.IsNullOrEmpty(Dr_Producto["CUSTODIA"].ToString()))
                    {
                        if (String.IsNullOrEmpty(Dr_Producto["UNIDAD"].ToString()) && String.IsNullOrEmpty(Dr_Producto["TOTALIDAD"].ToString()))
                        {
                            Registro_Datos = false;
                            break;
                        }
                        else
                        {
                            Dt_Productos.Rows.Add(Dr_Producto);
                            Dt_Productos.AcceptChanges();
                        }
                    }
                }

                if (Registro_Datos)
                {
                    Negocio.P_No_Orden_Compra = Txt_Folio.Text.Replace("OC-", "");
                    Negocio.P_Dt_Productos_OC = Dt_Productos;
                    Negocio.Guardar_Registro_Factura();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Registro Almacen", "alert('Re registraron los datos Exitosamente.');", true);
                    Configuracion_Formulario(true);
                }
                else
                {
                    Lbl_Mensaje_Error.Text = "+ Seleccionar el tipo de registro para todos los productos que de Resguardo o Custodia.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN         : Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(true);
                Limpiar_Catalogo();
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN         : Deja los componentes listos para dar de Alta un registro.
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, EventArgs e)
        {
            Buscar();
        }

        public void Buscar()
        {
            String Mensaje = "";

            if (Chk_Fecha.Checked &&
                (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && String.IsNullOrEmpty(Txt_Fecha_Fin.Text)) ||
                (String.IsNullOrEmpty(Txt_Fecha_Inicio.Text) && !String.IsNullOrEmpty(Txt_Fecha_Fin.Text)))
            {
                Mensaje += "+ Ingresar ambas fechas.";
            }

            if (String.IsNullOrEmpty(Mensaje))
            {
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = false;
                Llenar_Grid_Ordenes_Compra(0);
            }
            else
            {
                Lbl_Mensaje_Error.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region Grids

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_PageIndexChanging
        ///DESCRIPCIÓN         : Maneja la paginación del GridView
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Compra_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Llenar_Grid_Ordenes_Compra(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_SelectedIndexChanged
        ///DESCRIPCIÓN         : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES         :
        ///CREO                : Salvador Vázquez Camacho.
        ///FECHA_CREO          : 14/Junio/2012
        ///MODIFICO            :
        ///FECHA_MODIFICO      :
        ///CAUSA_MODIFICACIÓN  :
        ///*******************************************************************************
        protected void Grid_Ordenes_Compra_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Ordenes_Compra.SelectedIndex > (-1))
                {
                    GridViewRow SelectedRow;
                    DataTable Dt_Detalles = new DataTable();
                    DataTable Dt_Montos_OC = new DataTable();
                    Cls_Alm_Com_Recepcion_Material_Negocio Negocio = new Cls_Alm_Com_Recepcion_Material_Negocio();

                    Limpiar_Catalogo();
                    SelectedRow = Grid_Ordenes_Compra.Rows[Grid_Ordenes_Compra.SelectedIndex];
                    Txt_Folio.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[1].Text.Trim());
                    Txt_Fecha_Construccion.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[4].Text.Trim());
                    Txt_Requisicion.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[2].Text.Trim());
                    Txt_Estatus.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[6].Text.Trim());
                    Txt_Proveedor.Text = HttpUtility.HtmlDecode(SelectedRow.Cells[3].Text.Trim());

                    Negocio.P_No_Orden_Compra = HttpUtility.HtmlDecode(SelectedRow.Cells[7].Text.Trim());
                    Negocio.P_No_Requisicion = HttpUtility.HtmlDecode(SelectedRow.Cells[8].Text.Trim());

                    Dt_Detalles = Negocio.Consulta_Orden_Compra_Detalles();
                    Grid_Orden_Compra_Detalles.DataSource = Dt_Detalles;
                    Grid_Orden_Compra_Detalles.Columns[0].Visible = true;
                    Grid_Orden_Compra_Detalles.Columns[3].Visible = true;
                    Grid_Orden_Compra_Detalles.Columns[8].Visible = true;
                    Grid_Orden_Compra_Detalles.DataBind();
                    Grid_Orden_Compra_Detalles.Columns[0].Visible = false;
                    Grid_Orden_Compra_Detalles.Columns[3].Visible = false;
                    Grid_Orden_Compra_Detalles.Columns[8].Visible = false;

                    Dt_Montos_OC = Negocio.Montos_Orden_Compra();

                    if (Dt_Montos_OC.Rows.Count > 0)
                    {
                        Lbl_SubTotal.Text = "" + Dt_Montos_OC.Rows[0]["SUBTOTAL"];
                        Lbl_IEPS.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL_IEPS"];
                        Lbl_IVA.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL_IVA"];
                        Lbl_Total.Text = "" + Dt_Montos_OC.Rows[0]["TOTAL"];
                    }
                    Configuracion_Formulario(false);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion
        protected void Txt_Busqueda_Orden_Compra_TextChanged(object sender, EventArgs e)
        {
            Buscar();
        }
        protected void Txt_Busqueda_Requisicion_TextChanged(object sender, EventArgs e)
        {
            Buscar();
        }
}
