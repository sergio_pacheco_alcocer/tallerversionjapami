﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Seguimiento_Contrarecibos.Negocio;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Almacen.Contrarecibos.Negocio;
public partial class paginas_Compras_Frm_Alm_Com_Seguimiento_Contrarecibos : System.Web.UI.Page
{

    #region VARIABLES / CONSTANTES

    //objeto de la clase de negocio de Requisicion para acceder a la clase de datos y realizar copnexion
    private Cls_Alm_Seguimiento_Contrarecibos_Negocio Contrarecibo_Negocio;

    private const String MODO_LISTADO = "LISTADO";
    private const String MODO_INICIAL = "INICIAL";
    private const String MODO_MODIFICAR = "MODIFICAR";
    private const String MODO_NUEVO = "NUEVO";

    #endregion

    #region PAGE LOAD / INIT

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valores de primera vez
                if (!IsPostBack)
                {
                    DateTime _DateTime = DateTime.Now;
                    int dias = _DateTime.Day;
                    dias = dias * -1;
                    dias++;
                    _DateTime = _DateTime.AddDays(dias);
                    _DateTime = _DateTime.AddMonths(-1);

                    Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();

                    Llenar_Grid_Contrarecibos();
                    Habilitar_Controles(MODO_LISTADO);
                }
                Mostrar_Informacion("", false);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + ex.Message + "]");
            }
        }    

    #endregion

    #region METODOS

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Habilitar_Controles
        // DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
        // RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 30/Enero/2013 
        // MODIFICO:
        // FECHA_MODIFICO
        // CAUSA_MODIFICACIÓN   
        // *******************************************************************************/
        private void Habilitar_Controles(String Modo)
        {
            try
            {
                switch (Modo)
                {
                    case MODO_LISTADO:

                        Configuracion_Acceso("Frm_Ope_Alm_Seguimiento_Contrarecibos.aspx");
                        Btn_Salir.Visible = true;
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Listar_Contrarecibos.Visible = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Txt_No_Contra_Recibo.Enabled = false;
                        Txt_Fecha_Recepcion.Enabled = false;
                        Txt_Fecha_Pago.Enabled = false;
                        break;

                    case MODO_INICIAL:

                        Configuracion_Acceso("Frm_Ope_Alm_Seguimiento_Contrarecibos.aspx");
                        Btn_Salir.Visible = false;
                        Btn_Salir.ToolTip = "Inicio";

                        Btn_Listar_Contrarecibos.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                        Txt_No_Contra_Recibo.Enabled = false;
                        Txt_Fecha_Recepcion.Enabled = false;
                        Txt_Fecha_Pago.Enabled = false;
                        break;
                    //Estado de Nuevo
                    case MODO_NUEVO:

                        Btn_Listar_Contrarecibos.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Salir.Visible = true;
                        Txt_No_Contra_Recibo.Enabled = false;
                        Txt_Fecha_Recepcion.Enabled = false;
                        Txt_Fecha_Pago.Enabled = false;

                        break;
                    //Estado de Modificar
                    case MODO_MODIFICAR:

                        Btn_Listar_Contrarecibos.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Txt_No_Contra_Recibo.Enabled = false;
                        Txt_Fecha_Recepcion.Enabled = false;
                        Txt_Fecha_Pago.Enabled = false;
                        break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion(ex.ToString(), true);
            }
        }


        ///*******************************************************************************
        //NOMBRE DE LA FUNCIÓN: Mostrar_Información
        //DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
        //RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 30/Enero/2013 
        //MODIFICO:
        //FECHA_MODIFICO:
        //CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            try
            {
                Lbl_Informacion.Visible = mostrar;
                Img_Warning.Visible = mostrar;
                Lbl_Informacion.Text = txt;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Mostrar_Informacion Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Contrarecibos
        // DESCRIPCIÓN: Llena el grid principal de contrarecibos
        // RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 29/Enero/2013 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public void Llenar_Grid_Contrarecibos()
        {
            try
            {
                //Ponemos visibles los divs
                Div_Contenido.Visible = false;
                Div_Listado_Requisiciones.Visible = true;

                //Declaracion de variables
                Contrarecibo_Negocio = new Cls_Alm_Seguimiento_Contrarecibos_Negocio();
                Cls_Ope_Alm_Contrarecibos_Negocio Consulta_Contrarecibo_Negocio = new Cls_Ope_Alm_Contrarecibos_Negocio();

                //Asiganamos valores
                Contrarecibo_Negocio.P_No_Contra_Recibo = Txt_Busqueda.Text;
                Contrarecibo_Negocio.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
                Contrarecibo_Negocio.P_Fecha_Fin = Txt_Fecha_Final.Text;
                Contrarecibo_Negocio.P_Fecha_Fin = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
                //Asiganamos valores de Consulta
                if (!String.IsNullOrEmpty(Txt_Busqueda.Text.Trim()))
                    Consulta_Contrarecibo_Negocio.P_No_Contra_Recibo = Int64.Parse(Txt_Busqueda.Text.Trim());
                Consulta_Contrarecibo_Negocio.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
                Consulta_Contrarecibo_Negocio.P_Fecha_Fin = Txt_Fecha_Final.Text;
                Consulta_Contrarecibo_Negocio.P_Fecha_Fin = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
                //Consulta_Contrarecibo_Negocio.P_Estatus = "GENERADO";
                //Realizamos la consulta
                Session["Dt_Contrarecibo"] = Consulta_Contrarecibo_Negocio.Consulta_Contrarecibos();

                //Valdamos si trae datos la consulta
                if (Session["Dt_Contrarecibo"] != null && ((DataTable)Session["Dt_Contrarecibo"]).Rows.Count > 0)
                {
                    Div_Contenido.Visible = false;
                    Grid_Contra_Recibos.DataSource = Session["Dt_Contrarecibo"] as DataTable;
                    Grid_Contra_Recibos.DataBind();
                }
                else
                {
                    Grid_Contra_Recibos.DataSource = Session["Dt_Contrarecibo"] as DataTable;
                    Grid_Contra_Recibos.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Llenar_Grid_Contrarecibos Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Contrarecibos_Seleccionar
        ///DESCRIPCIÓN: Metodo que llena los controles con los datos relacionados del contrarecibo 
        ///en la busqueda del Modalpopup
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 30/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Evento_Grid_Contrarecibos_Seleccionar(String Dato)
        {
            try
            {
                Habilitar_Controles(MODO_INICIAL);

                Div_Listado_Requisiciones.Visible = false;
                Div_Contenido.Visible = true;

                Contrarecibo_Negocio = new Cls_Alm_Seguimiento_Contrarecibos_Negocio();
                Cls_Ope_Alm_Contrarecibos_Negocio Consulta_Facturas = new Cls_Ope_Alm_Contrarecibos_Negocio();
                String No_Contrarecibo = Dato;
                DataRow[] Contra_Recibo = ((DataTable)Session["Dt_Contrarecibo"]).Select("No_Contra_Recibo = '" + No_Contrarecibo + "'");                
                //Asignamos valores de cabecera
                Txt_No_Contra_Recibo.Text = No_Contrarecibo;
                if (!String.IsNullOrEmpty(Contra_Recibo[0][Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion].ToString()))
                    Txt_Fecha_Recepcion.Text = DateTime.Parse(Contra_Recibo[0][Ope_Alm_Contrarecibos.Campo_Fecha_Recepcion].ToString()).ToString("dd/MMM/yyyy");
                if (!String.IsNullOrEmpty(Contra_Recibo[0][Ope_Alm_Contrarecibos.Campo_Fecha_Pago].ToString()))
                    Txt_Fecha_Pago.Text = DateTime.Parse(Contra_Recibo[0][Ope_Alm_Contrarecibos.Campo_Fecha_Pago].ToString()).ToString("dd/MMM/yyyy");

                //Consultamos los datos de facturacion
                Contrarecibo_Negocio.P_No_Contra_Recibo = No_Contrarecibo;
                Contrarecibo_Negocio.P_No_Orden_Compra = Contra_Recibo[0]["OC"].ToString().Trim().Replace("OC-", "");
                Consulta_Facturas.P_No_Contra_Recibo = Int64.Parse(No_Contrarecibo);
                Consulta_Facturas.P_Tipo_OC = Hdf_Tipo_Contrarecibo.Value;
                DataTable Dt_Facturas = Consulta_Facturas.Consulta_Datos_Contrarecibo().Tables[1];
                //Asignamos los datos de facturacion
                Session["Dt_Facturas"] = Dt_Facturas;
                Grid_Facturas.DataSource = Dt_Facturas;
                Grid_Facturas.DataBind();

                //Consultamos los datos del seguimiento del contrarecibo 
                Contrarecibo_Negocio.P_No_Contra_Recibo = No_Contrarecibo;
                DataTable Dt_Seguimiento = Contrarecibo_Negocio.Consultar_Seguimiento();
                //Asignamos valores
                Session["Dt_Seguimiento"] = Dt_Seguimiento;
                Grid_Seguimiento.DataSource = Dt_Seguimiento;
                Grid_Seguimiento.DataBind();
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Evento_Grid_Contrarecibos_Seleccionar Error[" + ex.Message + "]");
            }
        }

    #endregion

    #region EVENTOS

        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Redireccionamos a la pagina principal
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Salir_Click [" + ex.ToString() +"]", true);
            }
        }
            
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Habilitamos controles y llenamos el grid
                Habilitar_Controles(MODO_INICIAL);
                Llenar_Grid_Contrarecibos();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Buscar_Click [" + ex.ToString() + "]", true);
            }
        }

        protected void Btn_Listar_Contrarecibos_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Habilitamos controles y llenamos el grid
                Llenar_Grid_Contrarecibos();
                Habilitar_Controles(MODO_LISTADO);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Listar_Contrarecibos_Click [" + ex.ToString() + "]", true);
            }
        }
     protected void Grid_Contrarecibos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
                //Llenamos los otros controles para visualizar los datos
                Hdf_Tipo_Contrarecibo.Value = Grid_Contra_Recibos.SelectedDataKey["TIPO"].ToString();
                String No_Requisicion = Grid_Contra_Recibos.SelectedDataKey["NO_CONTRA_RECIBO"].ToString();
                Evento_Grid_Contrarecibos_Seleccionar(No_Requisicion);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Seleccionar_Click [" + ex.ToString() + "]", true);
            }
        }

    #endregion

    #region EVENTOS GRID   
    
        protected void Grid_Contra_Recibos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Contra_Recibos.DataSource = ((DataTable)Session["Dt_Contrarecibo"]);
                Grid_Contra_Recibos.PageIndex = e.NewPageIndex;
                Grid_Contra_Recibos.DataBind();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Grid_Contra_Recibos_PageIndexChanging [" + ex.ToString() + "]", true);
            }
        }

        protected void Grid_Facturas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Facturas.DataSource = ((DataTable)Session["Dt_Facturas"]);
                Grid_Facturas.PageIndex = e.NewPageIndex;
                Grid_Facturas.DataBind();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Grid_Facturas_PageIndexChanging [" + ex.ToString() + "]", true);
            }
        }

        protected void Grid_Seguimiento_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Seguimiento.DataSource = ((DataTable)Session["Dt_Seguimiento"]);
                Grid_Seguimiento.PageIndex = e.NewPageIndex;
                Grid_Seguimiento.DataBind();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Grid_Seguimiento_PageIndexChanging [" + ex.ToString() + "]", true);
            }
        }

    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.            
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion    

    protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
    {
        try
        {
            //Habilitamos controles y llenamos el grid
            Habilitar_Controles(MODO_INICIAL);
            Llenar_Grid_Contrarecibos();
        }
        catch (Exception ex)
        {
            Mostrar_Informacion("Error: Btn_Buscar_Click [" + ex.ToString() + "]", true);
        }
    }
}

