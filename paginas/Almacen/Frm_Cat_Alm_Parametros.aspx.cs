﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using JAPAMI.Parametros_Almacen.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data;
using System.Text.RegularExpressions;

public partial class paginas_Almacen_Frm_Cat_Alm_Parametros : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Grid_Cotizador = 2;
        private const int Const_Estado_Modificar = 3;

        private static DataTable Dt_Imagenes = new DataTable();    
    #endregion

    #region Page Load / Init

    protected void Page_Load(object sender, EventArgs e)
    {        
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial, false);
                Cargar_Grid(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial, false);
        }
        
    }

    #endregion
    
    #region Metodos

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Mensaje_Error
        ///DESCRIPCION : Muestra el error
        ///PARAMETROS  : P_Texto: texto de un TextBox
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 15/Enero/2012
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Mensaje_Error(String P_Mensaje)
        {
            Img_Error.Visible = true;
            Lbl_Error.Text += P_Mensaje + "</br>";
            Div_Contenedor_error.Visible = true;
        }
        private void Mensaje_Error()
        {
            Img_Error.Visible = false;
            Lbl_Error.Text = "";
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Estado_Botones
        ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
        ///PARAMETROS: P_Estado: El estado de la pagina
        ///CREO: David herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Estado_Botones(int P_Estado, Boolean Enable)
        {
            switch (P_Estado)
            {
                case 0: //Estado inicial                                        
                   
                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Parametros.Enabled = true;

                    Grid_Parametros.Enabled = true;
                    Grid_Parametros.SelectedIndex = (-1);

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Inicio";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Btn_Modificar.Enabled = false;
                    Btn_Nuevo.Enabled = true;
                    Btn_Salir.Enabled = true;

                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;

                    Configuracion_Acceso("Frm_Cat_Alm_Parametros.aspx");
                    break;

                case 1: //Nuevo                    

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                                        
                    Btn_Modificar.Visible = false;

                    Grid_Parametros.SelectedIndex = (-1);                   

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";

                    break;

                case 2: //Grid Cotizador

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Parametros.Enabled = true;
                                       
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.Enabled = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Listado";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Listado";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";


                    break;

                case 3: //Modificar
                                        
                    Div_Listado_Cotizadores.Visible = true;
                    Grid_Parametros.Enabled = true;
                    Div_Datos_Cotizador.Visible = true;

                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_Nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                    break;
            }

            Txt_Dependencia.Enabled = Enable;
            Txt_Programa.Enabled = Enable;
            Cmb_Dependencia.Enabled = Enable;
            Cmb_Programa.Enabled = Enable;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar
        ///DESCRIPCIÓN: se actualizan los datos 
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 18/Febrero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************

        private void Modificar(Boolean Bln_Nuevo_Correo)
        {
            Div_Contenedor_error.Visible = false;
            Lbl_Error.Text = "";
            try
            {
                Cls_Cat_Alm_Parametros_Negocio Negocio = new Cls_Cat_Alm_Parametros_Negocio();
                
                if (Div_Contenedor_error.Visible == false)
                {                                                                 
                    if (!Bln_Nuevo_Correo)
                    {
                        //Asignamos los datos
                        if ((String)Session["Programa"] != null)
                            Negocio.P_Programa_ID = (String)Session["Programa"].ToString();
                        if ((String)Session["Dependencia"] != null)
                            Negocio.P_Dependencia_ID =(String)Session["Dependencia"].ToString();

                        //Modificamos el registro
                        Negocio.Modificar();

                        //Regresamos al estado inicial la pagina
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametros", "alert('La Modificación fue Exitosa');", true);
                        Estado_Botones(Const_Estado_Inicial, false);
                        Cargar_Grid(0);
                        Limpiar_Controles();
                    }
                }//Fin del if
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }//fin de Modificar  

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
        ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
        ///PARAMETROS: Page_Index: Numero de pagina del grid
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Cargar_Grid(int Page_Index)
        {
            try
            {
                Cls_Cat_Alm_Parametros_Negocio Negocio = new Cls_Cat_Alm_Parametros_Negocio();

                Dt_Imagenes = Negocio.Consultar_Parametros();
                Session["Dt_Temp"] = Dt_Imagenes;
                Grid_Parametros.PageIndex = Page_Index;
                Grid_Parametros.DataSource = Dt_Imagenes;
                Grid_Parametros.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo
        ///DESCRIPCIÓN: llena el combo con los datos
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 15/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Llenar_Combo(String Combo, String Valor)
        {
            //Declaracion de Variables
            Cls_Cat_Alm_Parametros_Negocio Negocio = new Cls_Cat_Alm_Parametros_Negocio();
            DataTable Dt_Temp = new DataTable();

            if (Combo == "DEPENDENCIA")
            {
                Negocio.P_Dependencia = Valor.Trim();
                //Consultamos las cuentas que correspondan a esa numeracion
                Dt_Temp = Negocio.Consultar_Dependencias();
                //llenamos el combo con las areas funcionales
                Cmb_Dependencia.Items.Clear();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Dependencia, Dt_Temp);
                Cmb_Dependencia.SelectedIndex = 0;
            }
            else
            {
                Negocio.P_Programa = Valor.Trim();
                //Consultamos las cuentas que correspondan a esa numeracion
                Dt_Temp = Negocio.Consultar_Proyectos();
                //llenamos el combo con las areas funcionales
                Cmb_Programa.Items.Clear();
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Programa, Dt_Temp);
                Cmb_Programa.SelectedIndex = 0;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Limpiar_Controles
        ///DESCRIPCIÓN: limpia los controles de la pagina
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO  : 18/Febrero/2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Limpiar_Controles() 
        {
            Txt_Dependencia.Text = "";
            Txt_Programa.Text = "";
            Cmb_Dependencia.SelectedIndex = -1;
            Cmb_Programa.SelectedIndex = -1;

            Session.Remove("Dependencia");
            Session.Remove("Programa");
        }       

    #endregion

    #region Eventos       

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Cat_Alm_Parametros_Negocio Negocio = new Cls_Cat_Alm_Parametros_Negocio();

                if (Btn_Nuevo.AlternateText == "Nuevo")
                {
                    if (Grid_Parametros.Rows.Count < 2)
                    {
                        Estado_Botones(Const_Estado_Nuevo, true);
                        if (Grid_Parametros.Rows.Count == 1)
                        {
                            if (Grid_Parametros.Rows[0].Cells[4].Text.Trim() == "Dependencia Almacen")
                            {
                                Txt_Dependencia.Enabled = false;
                                Cmb_Dependencia.Enabled = false;
                            }
                            else
                            {
                                Txt_Programa.Enabled = false;
                                Cmb_Programa.Enabled = false;
                            }
                        }
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametos", "alert('Ya existen los parametros');", true);
                    
                }
                else if (Btn_Nuevo.AlternateText == "Dar de Alta")
                {
                    Div_Contenedor_error.Visible = false;
                    
                    //Si pasa todas las Validaciones damos de alta el cotizador
                    if (Div_Contenedor_error.Visible == false)
                    {
                        try
                        {
                            if ((!String.IsNullOrEmpty(Txt_Dependencia.Text)) || (!String.IsNullOrEmpty(Txt_Programa.Text)))
                            {
                                if ((String)Session["Programa"] != null)
                                    Negocio.P_Programa_ID = (String)Session["Programa"].ToString();

                                if ((String)Session["Dependencia"] != null)
                                    Negocio.P_Dependencia_ID = (String)Session["Dependencia"].ToString();

                                Negocio.Alta();

                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametos", "alert('El alta fue Exitosa');", true);
                                Estado_Botones(Const_Estado_Inicial, false);
                                Cargar_Grid(0);
                                Limpiar_Controles();
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametos", "alert('Seleccione el progama o la dependencia');", true);
                        }
                        catch
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('El alta de las imagenes no fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial,false);
                            Cargar_Grid(0);
                            Limpiar_Controles();
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Evento del Boton Modificar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {                    
                    Estado_Botones(Const_Estado_Modificar, true);

                    if (!String.IsNullOrEmpty(Txt_Dependencia.Text))
                    {
                        Txt_Programa.Enabled = false;
                        Cmb_Programa.Enabled = false;
                    }
                    else
                    {
                        Txt_Dependencia.Enabled = false;
                        Cmb_Dependencia.Enabled = false;
                    }
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    if ((!String.IsNullOrEmpty(Txt_Dependencia.Text)) || (!String.IsNullOrEmpty(Txt_Programa.Text)))
                    {
                        Modificar(false);
                    }
                    else
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Parametos", "alert('Seleccione el progama o la dependencia');", true);
                        
                    
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Salir_Click
        ///DESCRIPCION:             Boton para SALIR
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO:             15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.AlternateText == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Cargar_Grid(0);
                Limpiar_Controles();
                Estado_Botones(Const_Estado_Inicial, false);               
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cmb_Dependencia_SelectedIndexChanged
        /// DESCRIPCION : Pone la clave en la caja de texto
        /// PARAMETROS  : 
        /// CREO        : David Herrera Rincon
        /// FECHA_CREO  : 15/Febrero/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Cmb_Dependencia_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Asigna el valor de la cuenta en la caja de texto
            String Clave = Cmb_Dependencia.SelectedItem.ToString();
            String[] Claves = Clave.Split(' ');

            Session["Dependencia"] = Cmb_Dependencia.SelectedValue.ToString();
            Txt_Dependencia.Text = Claves[0].ToString();
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Dependencia_Click
        /// DESCRIPCION : Busca las dependencias segun la clave
        /// PARAMETROS  : 
        /// CREO        : David Herrera Rincon
        /// FECHA_CREO  : 15/Febrero/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Dependencia_Click(object sender, EventArgs e)
        {
            Div_Contenedor_error.Visible = false;

            if (Txt_Dependencia.Text.Trim() == String.Empty)
            {
                Div_Contenedor_error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Error.Text = "+ Es necesario Ingresar Clave de dependencia";
            }
            else
            {
                Llenar_Combo("DEPENDENCIA", Txt_Dependencia.Text);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cmb_Programa_SelectedIndexChanged
        /// DESCRIPCION : Pone la clave en la caja de texto
        /// PARAMETROS  : 
        /// CREO        : David Herrera Rincon
        /// FECHA_CREO  : 15/Febrero/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Cmb_Programa_SelectedIndexChanged(object sender, EventArgs e)
        {
            //Asigna el valor de la cuenta en la caja de texto
            String Clave = Cmb_Programa.SelectedItem.ToString();
            String[] Claves = Clave.Split(' ');

            Session["Programa"] = Cmb_Programa.SelectedValue.ToString();
            Txt_Programa.Text = Claves[0].ToString();
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Programa_Click
        /// DESCRIPCION : Busca los programas segun la clave
        /// PARAMETROS  : 
        /// CREO        : David Herrera Rincon
        /// FECHA_CREO  : 15/Febrero/2013
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Programa_Click(object sender, EventArgs e)
        {
            Div_Contenedor_error.Visible = false;

            if (Txt_Programa.Text.Trim() == String.Empty)
            {
                Div_Contenedor_error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Error.Text = "+ Es necesario Ingresar Clave del proyecto";
            }
            else
            {
                Llenar_Combo("", Txt_Programa.Text);
            }
        }

    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Parametros_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Febrero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Parametros_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Validamos la ruta de donde va a mostrar la imagen
                Estado_Botones(Const_Grid_Cotizador, false);
                Llenar_Combo("DEPENDENCIA", "");
                Llenar_Combo("", "");

                if (Grid_Parametros.SelectedRow.Cells[4].Text.Trim() == "Dependencia Almacen")
                {
                    Cmb_Dependencia.SelectedValue = Grid_Parametros.SelectedRow.Cells[1].Text.Trim();
                    Session["Dependencia"] = Grid_Parametros.SelectedRow.Cells[4].Text.Trim();
                    
                    //Ponemos la clave en el txt
                    String Clave = Cmb_Dependencia.SelectedItem.ToString();
                    String[] Claves = Clave.Split(' ');
                    Txt_Dependencia.Text = Claves[0].ToString();
                   
                }
                else 
                {
                    Cmb_Programa.SelectedValue = Grid_Parametros.SelectedRow.Cells[1].Text.Trim();
                    Session["Programa"] = Grid_Parametros.SelectedRow.Cells[4].Text.Trim();

                    //Ponemos la clave en el txt
                    String Clave = Cmb_Programa.SelectedItem.ToString();
                    String[] Claves = Clave.Split(' ');
                    Txt_Programa.Text = Claves[0].ToString();
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Parametros_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Febrero/2013  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Parametros_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Parametros.SelectedIndex = (-1);
                Cargar_Grid(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

    #endregion   

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:       Configuracion_Acceso
    /// DESCRIPCIÓN:  Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:   URL_Pagina: Nombre de la pagina
    /// USUARIO CREÓ: David Herrera Rincon
    /// FECHA CREÓ:   15/Enero/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);            

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 15/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion        
    
}
