﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Alm_Cancelar_Salidas.aspx.cs" Inherits="paginas_Almacen_Frm_Ope_Alm_Cancelar_Salidas" 
Title="Seguimiento a Requisiciones" Culture="es-MX"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 25%;
        }
        .style2
        {
            width: 121px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>                                    
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                
                
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo">
                                Cancelación de Salidas
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Listar" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Listar" OnClick="Btn_Listar_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                No. Salida</td>
                            <td style="width: 43%;">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="13" Width="15%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
                                    TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked" 
                                    WatermarkText="&lt;No&gt;">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" 
                                    FilterType="Custom" TargetControlID="Txt_Busqueda" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 12%;" align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>                    
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>
                        <tr>
                        
                            <td style="width: 15%;">
                                &nbsp;</td>
                            <td style="width: 43%;">
                                &nbsp;</td>
                            <td align="right" visible="false" colspan="2">
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" 
                                    ToolTip="Consultar" />
                            </td>
                            <tr>
                                <td align="center" colspan="4" style="width: 99%">
                                    <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;">
                                        <asp:GridView ID="Grid_Entradas" runat="server" AllowSorting="True" 
                                            AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="NO_ORDEN_SALIDA" 
                                            EmptyDataText="No se encontraron contrarecibos" GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" Width="100%" 
                                            onpageindexchanging="Grid_Entradas_PageIndexChanging">
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Seleccionar" runat="server" 
                                                            CommandArgument='<%# Eval("NO_ORDEN_SALIDA") %>' 
                                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                            OnClick="Btn_Seleccionar_Click" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NO_ORDEN_SALIDA" HeaderText="No. Salida" >
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_REQUISICION" HeaderText="RQ">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS_SALIDA" HeaderText="Estatus">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO_REQUISICION" HeaderText="Tipo" >
                                                <HeaderStyle Width="10%" HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" 
                                                Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="40%" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="AREA" HeaderText="Área" Visible="false">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left"  Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CODIGO_PROGRAMATICO" 
                                                HeaderText="Código Programático" Visible="False">
                                            </asp:BoundField>
                                            <asp:BoundField DataField="EMPLEADO_SURTIO" HeaderText="Entregó" Visible="false" >
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_SURTIDO" HeaderText="F.Surtido" 
                                                DataFormatString="{0:dd/MMM/yyyy}" >
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Width="10%" Font-Size="X-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SUBTOTAL" HeaderText="Subtotal" 
                                                Visible="false" DataFormatString="{0:c}">
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" Wrap="True" />
                                                <ItemStyle HorizontalAlign="Center" Width="11%" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IVA" HeaderText="  IVA  "  DataFormatString="{0:c}" Visible="false">
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="11%" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOTAL" HeaderText="  Total"  DataFormatString="{0:c}" >
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="15%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;" align="left">
                                No. Salida</td>
                            <td align="left" class="style1">
                                <asp:TextBox ID="Txt_No_Salida" runat="server" Width="100%"></asp:TextBox>
                            </td>                                
                            <td>
                                Req
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Req" runat="server" Width="50%"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td class="style2">
                                Estatus</td>
                            <td>
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%"> 
                                    <asp:ListItem>CANCELADA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            
                        </tr>  
                        <tr>
                            <td>
                                Comentarios</td>
                            <td colspan="2">
                                <asp:TextBox ID="Txt_Comentarios" runat="server" Width="96%" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                            <td>
                            </td>                            
                        </tr>                      
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Facturas" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="100%"                                     
                                    style="white-space:normal" 
                                    onpageindexchanging="Grid_Facturas_PageIndexChanging" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="PRODUCTO_ID" HeaderText="Producto_ID" 
                                                Visible="false">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRODUCTO" HeaderText="Producto" 
                                                Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" Visible="true">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD_SOLICITADA" HeaderText="Solicitado" 
                                                Visible="true">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Entregado">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="UNIDAD" HeaderText="Unidad" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small" />
                                                <FooterStyle HorizontalAlign="Center" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MARCA" HeaderText="Marca" Visible="False">
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="15%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="False" >
                                                <HeaderStyle HorizontalAlign="Center" />
                                                <ItemStyle HorizontalAlign="Center" Width="15%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COSTO" HeaderText="Costo" 
                                                Visible="false" DataFormatString="{0:c}">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="SUBTOTAL" HeaderText="Subtotal" Visible="false"
                                                DataFormatString="{0:c}">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="IMPORTE" HeaderText="Total" 
                                                Visible="True" DataFormatString="{0:c}">
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="height:20px;">
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td align="right">SubTotal &nbsp; &nbsp; <asp:TextBox ID="Txt_SubTotal" runat="server" Width="46%" Enabled="false"></asp:TextBox> </td>
                        </tr>
                        
                        <tr style="height:20px;">
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td align="right">IVA &nbsp; &nbsp; <asp:TextBox ID="Txt_IVA" runat="server" Width="46%" Enabled="false"></asp:TextBox> </td>
                        </tr>
                        
                        <tr style="height:20px;">
                            <td ></td>
                            <td ></td>
                            <td ></td>
                            <td align="right">Total &nbsp; &nbsp;<asp:TextBox ID="Txt_Total" runat="server" Width="46%" Enabled="false"></asp:TextBox> </td>
                        </tr>
                            
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>    
</asp:Content>

