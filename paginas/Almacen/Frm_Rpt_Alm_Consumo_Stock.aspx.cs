﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Text;
using JAPAMI.Sessiones;
using JAPAMI.Reporte_Alm_Consumo_Stock.Negocio;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;
public partial class paginas_Compras_Frm_Rpt_Alm_Consumo_Stock : System.Web.UI.Page
{
    #region VARIABLES CONSTANTES
    private static String Prodcto_ID = "ID";
    #endregion
    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
            Mensaje_Error();
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    #endregion

    #region METODOS
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
        Lbl_Mensaje_Error.Visible = true;
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles(); //Limpia los controles del forma
            LLenar_Combo_Productos(); 
            LLenar_Combo_Meses();
            LLenar_Combo_Gerencia();
            LLenar_Combo_Categoria_Producto();
            LLenar_Combo_Unidad_Responsable();
        }
        catch (Exception ex)
        {
            throw new Exception( ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Limpiar_Controles() 
    {
        try
        {
            Txt_Anio.Text = "";
            Cmb_Gerencia.SelectedIndex = 0;
            Cmb_Mes.SelectedIndex = 0;
            Cmb_Producto.SelectedIndex = 0;
            Cmb_Unidad_Responsable.SelectedIndex = 0;
            
        }
         catch (Exception ex)
         {
             throw new Exception("Error al limpiar los controles :" +  ex.Message.ToString() + " </br> ");
         }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Modal_Produtos_Servicios
    /// DESCRIPCION : Limpia los controles de la busqueda del producto
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012 01:06
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Limpiar_Modal_Produtos_Servicios() 
    {
        try
        {
            //Txt_Clave_B.Text = "";
            //Txt_Nombre_B.Text = "";
            //Cmb_Categorioa_B.SelectedIndex = 0;
            Grid_Productos_Servicios_Modal.DataSource = null;// P_Dt_Productos_Servicios_Modal;
            Grid_Productos_Servicios_Modal.DataBind();
            Grid_Productos_Servicios_Modal.Visible = true;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al limpiar los controles :" + ex.Message.ToString() + " </br> ");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo_Meses
    /// DESCRIPCION : Llena el combo con los meses les asigna un  valor a cada mes
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012 01:09
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo_Meses() 
    {
        try
        {
            String [] Datos_Combo = {"ENERO","FEBRERO", "MARZO","ABRIL","MAYO","JUNIO","JULIO",
                                        "AGOSTO","SEPTIEMBRE","OCTUBRE","NOVIEMBRE","DICIEMBRE"};
            Cmb_Mes.Items.Clear();
            Cmb_Mes.Items.Add("TODOS");
            Int32 No_Mes = 1;
            foreach (String _Item in Datos_Combo)
            {
                Cmb_Mes.Items.Add(_Item);
                Cmb_Mes.Items[No_Mes].Value = No_Mes.ToString();
            }
            Cmb_Mes.Items[0].Value = "0";
            Cmb_Mes.Items[0].Selected = true;
        }
        catch (Exception ex)
        {
            throw new Exception("Error llenar el combo mes :" + ex.Message.ToString() + " </br> ");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10-Octubre-2012 01:09
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo_Productos() 
    {
        try 
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
            DataTable Dt_Productos = new DataTable();
            Dt_Productos = Negocio.Consultar_Productos();
            if (Dt_Productos != null && Dt_Productos.Rows.Count > 0) 
            {
                LLenar_Combo(Cmb_Producto, Dt_Productos, Cat_Com_Productos.Campo_Producto_ID, "PRODUCTO", "<-Seleccione->");
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo :" + ex.Message.ToString() + " </br> ");
           
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo_Gerencia
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10-Octubre-2012 11:24
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo_Gerencia()
    {
        try
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
            DataTable Dt_Gerencias = new DataTable();
            Dt_Gerencias = Negocio.Consultar_Gerencias();
            if (Dt_Gerencias != null && Dt_Gerencias.Rows.Count > 0)
            {
                LLenar_Combo(Cmb_Gerencia, Dt_Gerencias, Cat_Grupos_Dependencias.Campo_Grupo_Dependencia_ID, "GERENCIA", "TODOS");
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo :" + ex.Message.ToString() + " </br> ");

        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo_Unidad_Responsable
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10-Octubre-2012 11:24
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo_Unidad_Responsable()
    {
        try
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
            DataTable Dt_Dependencias = new DataTable();
            if (Cmb_Gerencia.SelectedIndex > 0)
            {
                Negocio.P_Grupo_Dependencia_ID = Cmb_Gerencia.SelectedValue.ToString().Trim();
                Dt_Dependencias = Negocio.Consultar_Unidades_Responsables();
                if (Dt_Dependencias != null && Dt_Dependencias.Rows.Count > 0)
                {
                    LLenar_Combo(Cmb_Unidad_Responsable, Dt_Dependencias, Cat_Dependencias.Campo_Dependencia_ID, "DEPENDENCIA", "TODOS");
                }
            }
            else 
            {
                Cmb_Unidad_Responsable.Items.Clear();
                Cmb_Unidad_Responsable.Items.Add("TODOS");
                Cmb_Unidad_Responsable.Items[0].Value = "0";
            }
            
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo :" + ex.Message.ToString() + " </br> ");

        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo_Categoria_Producto
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10-Octubre-2012 11:42
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo_Categoria_Producto()
    {
        try
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
            DataTable Dt_Categorias = new DataTable();
            Dt_Categorias = Negocio.Consultar_Categorias();
            if (Dt_Categorias != null && Dt_Categorias.Rows.Count > 0)
            {
                LLenar_Combo(Cmb_Categorioa_B, Dt_Categorias, "CATEGORIA_ID", "NOMBRE", "<-Seleccione->");
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo :" + ex.Message.ToString() + " </br> ");

        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012 01:09
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo(DropDownList Combo, DataTable Dt_Datos, String Value, String Text, String Item1)
    {
        try
        {
            Combo.Items.Clear();
            Combo.DataSource = Dt_Datos;
            Combo.DataTextField = Text;
            Combo.DataValueField = Value;
            Combo.DataBind();
            Combo.Items.Insert(0, new ListItem(Item1, ""));
            Combo.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al cargar el combo :" + ex.Message.ToString() + " </br> ");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:IBtn_MDP_Prod_Serv_Buscar_Click
    ///DESCRIPCIÓN:  
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 10/octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_IBtn_MDP_Prod_Serv_Buscar()
    {
        Limpiar_Modal_Produtos_Servicios();

        DataTable Dt_Prod_Tmp = null;
        Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio_Reporte_Consumo = new Cls_Rpt_Alm_Consumo_Stock_Negocio();

        if (Txt_Nombre_B.Text.Trim().Length > 0)
        {
            Negocio_Reporte_Consumo.P_Nombre_Producto = Txt_Nombre_B.Text.Trim();
        }
        if (Txt_Clave_B.Text.Trim().Length > 0)
        {
            Negocio_Reporte_Consumo.P_Clave_Producto= Txt_Clave_B.Text.Trim();
        }
        if (Cmb_Categorioa_B.SelectedIndex > 0)
        {
            Negocio_Reporte_Consumo.P_Categoria_Producto = Cmb_Categorioa_B.SelectedValue.ToString().Trim();
        }
        if (!String.IsNullOrEmpty(Txt_Nombre_B.Text) || !String.IsNullOrEmpty(Txt_Clave_B.Text) || Cmb_Categorioa_B.SelectedIndex > 0)
        {
            Dt_Prod_Tmp = Negocio_Reporte_Consumo.Consultar_Productos_Busqueda();
            Grid_Productos_Servicios_Modal.EmptyDataText = "No se encontraron registros";
        }
        else 
        {
            Grid_Productos_Servicios_Modal.EmptyDataText = "Por favor Haga uso de almenos un filtro para iniciar la busqueda";
            Mensaje_Error("Por favor Haga uso de almenos un filtro para iniciar la busqueda </br>");
        }
        
        if (Dt_Prod_Tmp != null && Dt_Prod_Tmp.Rows.Count > 0)
        {
            //Session[P_Dt_Productos_Servicios_Modal] = Dt_Prod_Srv_Tmp;
            Grid_Productos_Servicios_Modal.Columns[6].Visible = true;
            Grid_Productos_Servicios_Modal.DataSource = Dt_Prod_Tmp;
            Grid_Productos_Servicios_Modal.DataBind();
            Grid_Productos_Servicios_Modal.Columns[6].Visible = false;
            Grid_Productos_Servicios_Modal.Visible = true;
        }
        Modal_Busqueda_Prod_Serv.Show();
    }
     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Validar_Datos
    ///DESCRIPCIÓN:  Valida los datos obligatorios
    ///PARAMETROS :
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 11/octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Validacion = true;
        try
        {
            if (Cmb_Producto.SelectedIndex <= 0) 
            {
                Mensaje_Error("Favor de seleccionar el producto");
                Validacion = false;
            }
            if (String.IsNullOrEmpty(Txt_Anio.Text)) 
            {
                Mensaje_Error("Favor de ingresar el año");
                Validacion = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al validar los datos :" + ex.Message.ToString() + " </br> ");
        }
        return Validacion;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Salida_Por_Mes_Especifico
    ///DESCRIPCIÓN         : Consulta las salidas de un producto durante un mes,
    ///                      filtrado por geencia, o dependencia
    ///PARAMETROS          :
    ///CREO                : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO          : 11/octubre/2012 11:28 am
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private DataSet Consultar_Salida_Por_Mes_Especifico()
    {
        DataTable Dt_Salida_Producto = new DataTable();
        DataTable Dt_Detalle = new DataTable();
        DataSet Ds_Salida_Mes_Especifico = new DataSet();
        try
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
            Negocio.P_Producto_ID = Cmb_Producto.SelectedValue.ToString();
            Negocio.P_Mes = Cmb_Mes.SelectedIndex.ToString();
            Negocio.P_Anio = Txt_Anio.Text.Trim();
            Negocio.P_Nombre_Mes = Cmb_Mes.SelectedItem.ToString();
            if (Cmb_Gerencia.SelectedIndex > 0) 
            {
                Negocio.P_Grupo_Dependencia_ID = Cmb_Gerencia.SelectedValue.ToString();
                if (Cmb_Unidad_Responsable.SelectedIndex > 0) 
                {
                    Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.ToString();
                }
            }
            Dt_Salida_Producto = Negocio.Consultar_Reporte_Por_Mes_especifico();
            Dt_Salida_Producto.TableName ="Dt_Consulta";
            Dt_Detalle = Negocio.Consulta_Detalle_Mes_Especifico();
            Dt_Detalle.TableName = "Dt_Detalle";
            Ds_Salida_Mes_Especifico.Clear();
            Ds_Salida_Mes_Especifico.Tables.Add(Dt_Salida_Producto.Copy());
            Ds_Salida_Mes_Especifico.Tables.Add(Dt_Detalle.Copy());
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Realizar el reporte por mes especifico :" + ex.Message.ToString() + " </br> ");
        }
        return Ds_Salida_Mes_Especifico;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Salida_Anual_Por_Producto
    ///DESCRIPCIÓN         : Consulta las salidas de un producto durante cada mes de año
    ///                      seleccionado.
    ///PARAMETROS          : 
    ///CREO                : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO          : 11/octubre/2012 11:55 pm
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private DataSet Consultar_Salida_Anual_Por_Producto()
    {
        DataSet Ds_Salida_Producto = new DataSet();
        DataTable Dt_Salida_Producto = new DataTable();
        try
        {
            Cls_Rpt_Alm_Consumo_Stock_Negocio Negocio = new Cls_Rpt_Alm_Consumo_Stock_Negocio();
          
            Negocio.P_Producto_ID = Cmb_Producto.SelectedValue.ToString();
            Negocio.P_Anio = Txt_Anio.Text.Trim();
            if (Cmb_Gerencia.SelectedIndex > 0)
            {
                Negocio.P_Grupo_Dependencia_ID = Cmb_Gerencia.SelectedValue.ToString();
                if (Cmb_Unidad_Responsable.SelectedIndex > 0)
                {
                    Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedValue.ToString();
                }
            }
            Dt_Salida_Producto = Negocio.Consulta_Reporte_Salidas_Anual_De_Un_Producto();

            Dt_Salida_Producto.TableName = "Dt_Consulta";

            Ds_Salida_Producto.Tables.Add(Dt_Salida_Producto.Copy());
            
        }
        catch (Exception ex)
        {
            throw new Exception("Error al Realizar el reporte mensual-anual :" + ex.Message.ToString() + " </br> ");
        }
        return Ds_Salida_Producto;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Lenar_Grid
    ///DESCRIPCIÓN         : Consulta las salidas de un producto durante cada mes de año
    ///                      seleccionado.
    ///PARAMETROS          : Dt_Datos, DataTAble que contiene los datos  a mostar en la gridview
    ///CREO                : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO          : 12/octubre/2012 01:44 pm
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    private void Llenar_Grid(DataSet Ds_datos)
    {
        try
        {
            DataTable Dt_Cosnulta= Ds_datos.Tables["Dt_Consulta"];
            DataTable Dt_Detalle= new DataTable();
            if (Dt_Cosnulta != null && Dt_Cosnulta.Rows.Count > 0)
            {
                Grid_Reporte.DataSource = Dt_Cosnulta;
                Grid_Reporte.DataBind();
                if (Ds_datos.Tables.Contains("Dt_Detalle")) 
                {
                    Dt_Detalle = Ds_datos.Tables["Dt_Detalle"];
                    if (Dt_Detalle.TableName != null && Dt_Detalle.Rows.Count > 0)
                    {
                        Grid_Detalle_Reporte.DataSource = Dt_Detalle;
                        Grid_Detalle_Reporte.DataBind();
                    }
                    else 
                    {
                        Grid_Detalle_Reporte.DataSource = new DataTable();
                        Grid_Detalle_Reporte.DataBind();
                    }
                }
                else
                {
                    Grid_Detalle_Reporte.DataSource = new DataTable();
                    Grid_Detalle_Reporte.DataBind();
                }
            }
            else
            {
                Grid_Reporte.DataSource = new DataTable();
                Grid_Reporte.DataBind();
                Mensaje_Error("No se encontraron registros");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al llenar el gridview :" + ex.Message.ToString() + " </br> ");
        }
    }
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 24/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Rpt(DataSet Ds_Reporte)
    {
        WorksheetCell Celda = new WorksheetCell();
        DataTable Dt_Reporte = new DataTable(); //Tabla con la consulta general
        DataTable Dt_Detalle_Reporte = new DataTable(); //Tabla con la consulta detalle o complementaria
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Tipo_Estilo = "";
        String Tipo_Estilo_Total = "";
        String Informacion_Registro = "";
        Int32 Contador_Estilo = 0;
        Int32 Operacion = 0;
        try
        {
            Mensaje_Error();
            Dt_Reporte = Ds_Reporte.Tables["Dt_Consulta"];
            if (Ds_Reporte.Tables.Contains("Dt_Detalle")) 
            {
                Dt_Detalle_Reporte = Ds_Reporte.Tables["Dt_Detalle"];
            }

            Nombre_Archivo = "Rpt_Almacen_Salidas.xls";
            Ruta_Archivo = @Server.MapPath("../../Archivos/" + Nombre_Archivo);

            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Reporte_Almacen";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
            //  Creamos el estilo cabecera 3 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
            //Creamos el estilo fecha para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Fecha = Libro.Styles.Add("HeaderStyleFecha");
            //  Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //  Creamos el estilo contenido2 del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");
            //  Creamos el estilo Total del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Total = Libro.Styles.Add("BodyStyleTotal");

            ////  Creamos el estilo contenido del concepto para la hoja de excel. 
            //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales2 = Libro.Styles.Add("Totales2");
            ////  Creamos el estilo contenido del concepto para la hoja de excel.
            //CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Total");


            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera
            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.WrapText = true;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#0066CC";
            Estilo_Cabecera.Interior.Color = "#CCCCFF";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera2
            Estilo_Cabecera2.Font.FontName = "Tahoma";
            Estilo_Cabecera2.Font.Size = 10;
            Estilo_Cabecera2.Font.Bold = true;
            Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera2.Alignment.Rotate = 0;
            Estilo_Cabecera2.Font.Color = "#0066CC";
            Estilo_Cabecera2.Interior.Color = "#FFFFFF";
            Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la HeaderStyle3
            Estilo_Cabecera3.Font.FontName = "Tahoma";
            Estilo_Cabecera3.Font.Size = 10;
            Estilo_Cabecera3.Font.Bold = true;
            Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera3.Alignment.Rotate = 0;
            Estilo_Cabecera3.Font.Color = "#003399";
            Estilo_Cabecera3.Interior.Color = "LightGray";
            Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la fecha
            Estilo_Fecha.Font.FontName = "Tahoma";
            Estilo_Fecha.Font.Size = 10;
            Estilo_Fecha.Font.Bold = false;
            Estilo_Fecha.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Fecha.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Fecha.Alignment.Rotate = 0;
            Estilo_Fecha.Font.Color = "#003399";
            Estilo_Fecha.Interior.Color = "#FFFFFF";
            Estilo_Fecha.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Fecha.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle
            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido.Alignment.Rotate = 0;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            
            //estilo para el BodyStyle2
            Estilo_Contenido2.Font.FontName = "Tahoma";
            Estilo_Contenido2.Font.Size = 9;
            Estilo_Contenido2.Font.Bold = false;
            Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido2.Alignment.Rotate = 0;
            Estilo_Contenido2.Font.Color = "#000000";
            Estilo_Contenido2.Interior.Color = "#FAF0E6";
            Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyleTotal
            Estilo_Total.Font.FontName = "Tahoma";
            Estilo_Total.Font.Size = 9;
            Estilo_Total.Font.Bold = true;
            Estilo_Total.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Total.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Total.Alignment.Rotate = 0;
            Estilo_Total.Font.Color = "#000000";
            Estilo_Total.Interior.Color = "#4682B4";
            Estilo_Total.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //*************************************** fin de los estilos***********************************************************

            //***************************************Inicio del reporte Proveedores por partida Hoja 1***************************

            //  Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("REPORTE DE ALMACEN");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();

            if (Cmb_Mes.SelectedIndex > 0)
            {
                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  1 Gerencia
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  2 Unidad Responsable
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  3 Fecha
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  4 No_Salida
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(55));//  5 Cantidad
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(135));//  6 Nombre
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));// Auxiliar


                //  se llena el encabezado principal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO ");
                Celda.MergeAcross = 5; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle";
                //Se llena el segundo encabezado
                Int32 Clave_Producto = Convert.ToInt32(Cmb_Producto.SelectedValue.ToString());
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("PRODUCTO: " + Cmb_Producto.SelectedItem.ToString() + " ENTREGADOS EN EL MES DE " + Cmb_Mes.SelectedItem.ToString() + " " + Txt_Anio.Text.Trim());
                Celda.MergeAcross = 5; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle2";

                //Renglon con la fecha Acutal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add(DateTime.Now.ToString());
                Celda.MergeAcross = 5; // Merge 6 cells together
                Celda.StyleID = "HeaderStyleFecha";
              

                //  para los titulos de las columnas
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("GERENCIA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("UNIDAD RESPONSABLE");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("FECHA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("No. SALIDA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("CANTIDAD");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("NOMBRE");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add();
                Celda.MergeDown = 1;

            }
            else
            {
                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  1 Gerencia
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  2 Unidad Responsable
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   3 Enero
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   4 Febrero
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   5 Marzo
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   6 Abril
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   7 Mayo
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   8 Junio 
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//   9 Julio
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//  10 Agosto
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//  11 Septiembre
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//  12 Octubre
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//  10 Noviembre
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(30));//  11 Diciembre
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//  12 Total
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(45));//  12 Porcentaje

                //  se llena el encabezado principal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO ");
                Celda.MergeAcross = 15; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle";

                //Se llena el segundo encabezado
                Int32 Clave_Producto = Convert.ToInt32(Cmb_Producto.SelectedValue.ToString());
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("CONSUMO DEL PRODUCTO " + Cmb_Producto.SelectedItem.ToString() + ", DURANTE EL AÑO " + Txt_Anio.Text.Trim());
                Celda.MergeAcross = 15; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle2";

                //Renglon con la fecha Acutal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add(DateTime.Now.ToString());
                Celda.MergeAcross = 15; // Merge 6 cells together
                Celda.StyleID = "HeaderStyleFecha";
              
                //  para los titulos de las columnas
                Renglon = Hoja.Table.Rows.Add();

                Celda = Renglon.Cells.Add("GERENCIA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("UNIDAD RESPONSABLE");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("ENE");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("FEB");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("MAR");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("ABR");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("MAY");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("JUN");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("JUL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("AGS");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("SEP");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("OCT");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("NOV");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("DIC");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("TOTAL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("PORCENTAJE");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

            }
            Renglon = Hoja.Table.Rows.Add();

            //  para llenar el reporte
            if (Dt_Reporte.Rows.Count > 0)
            {
                //  Se comienza a extraer la informaicon de la onsulta
                foreach (DataRow Renglon_Reporte in Dt_Reporte.Rows)
                {
                    Contador_Estilo++;
                    Operacion = Contador_Estilo % 2;
                    if (Operacion == 0)
                    {
                        //Estilo_Concepto.Interior.Color = "LightGray";
                        Tipo_Estilo = "BodyStyle2";
                    }
                    else
                    {
                        Tipo_Estilo = "BodyStyle";
                        //Estilo_Concepto.Interior.Color = "White";
                    }

                    Renglon = Hoja.Table.Rows.Add();
                    foreach (DataColumn Column in Dt_Reporte.Columns)
                    {
                        if (!Column.ColumnName.Equals("CLAVE_PRODUCTO")) 
                        {
                            if (Dt_Reporte.Columns.Contains("Gerencia")) 
                            {
                                //Asigna el estilo de la fila que contiene los totales
                                if (String.IsNullOrEmpty(Renglon_Reporte["Gerencia"].ToString()))
                                    Tipo_Estilo = "BodyStyleTotal";
                            }
                            Informacion_Registro = (Renglon_Reporte[Column.ColumnName].ToString());
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        }
                    }
                    if (Dt_Detalle_Reporte != null && Dt_Reporte.Columns.Contains("CLAVE_PRODUCTO")) 
                    {
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell());
                    }
                    
                }
                if (Dt_Detalle_Reporte != null && Dt_Detalle_Reporte.Rows.Count > 0)
                {
                    Renglon = Hoja.Table.Rows.Add();
                    //  para los titulos de las columnas
                    Renglon = Hoja.Table.Rows.Add();
                    Celda = Renglon.Cells.Add("GERENCIA");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("UNIDAD RESPONSABLE");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("ENTREGADOS EN MARZO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("COSTO EN MARZO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.MergeAcross = 1;
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("ENTREGADOS  HASTA MARZO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("DIFERENCIA");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";

                    Celda = Renglon.Cells.Add("AÑO");
                    Celda.MergeDown = 1; // Merge two cells together
                    Celda.StyleID = "HeaderStyle3";
                    Renglon = Hoja.Table.Rows.Add();
                    Contador_Estilo = 0; //Iguala a cero la varible contador
                    foreach (DataRow Renglon_Detalle in Dt_Detalle_Reporte.Rows)
                    {
                        Contador_Estilo++;
                        Operacion = Contador_Estilo % 2;
                        if (Operacion == 0)
                        {
                            //Estilo_Concepto.Interior.Color = "LightGray";
                            Tipo_Estilo = "BodyStyle2";
                        }
                        else
                        {
                            Tipo_Estilo = "BodyStyle";
                            //Estilo_Concepto.Interior.Color = "White";
                        }
                        
                        Renglon = Hoja.Table.Rows.Add();
                        //Renglon.Cells.Add( new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        Informacion_Registro = (Renglon_Detalle[0].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        Informacion_Registro = (Renglon_Detalle[1].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        Informacion_Registro = (Renglon_Detalle[2].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        Informacion_Registro = (Renglon_Detalle[3].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;
                        Celda.MergeAcross = 1;

                        Informacion_Registro = (Renglon_Detalle[4].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        Informacion_Registro = (Renglon_Detalle[5].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        Informacion_Registro = (Renglon_Detalle[6].ToString());
                        Celda = Renglon.Cells.Add(Informacion_Registro);
                        Celda.StyleID = Tipo_Estilo;

                        //foreach (DataColumn Column in Dt_Detalle_Reporte.Columns)
                        //{
                        //    if (!Column.ColumnName.Equals("Entregados hasta " + Cmb_Mes.SelectedItem.ToString()))
                        //    {
                        //        Informacion_Registro = (Renglon_Detalle[Column.ColumnName].ToString());
                        //        Celda =Renglon.Cells.Add(Informacion_Registro);
                        //        Celda.StyleID = Tipo_Estilo;
                        //    }
                        //    //else 
                        //    //{
                        //    //    Informacion_Registro = (Renglon_Detalle[Column.ColumnName].ToString());
                        //    //    Celda = new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo);
                        //    //    Renglon.Cells.Add(Celda);
                        //    //    Celda.MergeDown = 1;
                        //    //    Celda.MergeAcross = 1;

                        //    //}

                        //}//Fin del for para obtener cada columna

                    }//Fin del for para obtener cada fila de la tabla
                }
            }
           
            //***************************************Fin del reporte Proveedores por partida Hoja 1***************************

            //  se guarda el documento
            Libro.Save(Ruta_Archivo);
            //  mostrar el archivo
            Mostrar_Reporte(Nombre_Archivo);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            //Response.ContentEncoding = Encoding.Default;
            //Libro.Save(Response.OutputStream);
            //Response.End();
        }// fin try

        catch (Exception Ex)
        {
             Mensaje_Error(Ex.Message);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    /// USUARIO CREO:        Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:          18/Enero/2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Ruta = "../../Archivos/" + Nombre_Reporte_Generar;
        try
        {
            Mensaje_Error();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=100,height=100')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #region EVENTOS
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Ibtn_Buscar_Producto_Click
    /// DESCRIPCION :Evento que se dispara la dar clic enel boton Ibtn_Buscar_Producto_Click
    ///              al seleccionarse mostrara un panel en el que se puede hacer una consulta mas
    ///              especifica del producto
    /// PARAMETROS  :
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Ibtn_Buscar_Producto_Click(object sender, ImageClickEventArgs e)
    {
        Mensaje_Error();
        Limpiar_Modal_Produtos_Servicios();
        Modal_Busqueda_Prod_Serv.Show();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: 
    ///DESCRIPCIÓN: Metodo que permite cerrar el panel de busqueda del productos
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 09/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void IBtn_MDP_Prod_Serv_Cerrar_Click(object sender, ImageClickEventArgs e)
    {
        try 
        {
            Limpiar_Modal_Produtos_Servicios();
            Modal_Busqueda_Prod_Serv.Hide();
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:Metodo que permite salor de la pagina y situarse en la pagina de inicio
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 09/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Gerencia_SelectedIndexChanged
    ///DESCRIPCIÓN:Evento Change del como Cmb_Gerencia, dependiendo de la gerencia se cargarn las unidades responsables
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 09/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Gerencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        LLenar_Combo_Unidad_Responsable();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:IBtn_MDP_Prod_Serv_Buscar_Click
    ///DESCRIPCIÓN:  
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void IBtn_MDP_Prod_Serv_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Evento_IBtn_MDP_Prod_Serv_Buscar();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Producto_Click
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Producto_Click(object sender, ImageClickEventArgs e)
    {
        String ID = ((ImageButton)sender).CommandArgument;
        Session[Prodcto_ID] = ID;//Grid_Productos_Servicios_Modal.SelectedDataKey["ID"].ToString();
        if (! String.IsNullOrEmpty(ID)) 
        {
            Cmb_Producto.SelectedIndex = Cmb_Producto.Items.IndexOf(Cmb_Producto.Items.FindByValue(ID));
        }

        Grid_Productos_Servicios_Modal.DataSource = null;//P_Dt_Productos_Servicios_Modal;
        Grid_Productos_Servicios_Modal.DataBind();
        Modal_Busqueda_Prod_Serv.Hide();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Conultar_Click
    ///DESCRIPCIÓN: Evento del boton Btn_Buscar_Conultar, verifica cual de los 2 reportes 
    ///             que se pueden generar se va a realizar y genera el reporte en el gridview.
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Conultar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            DataSet Ds_Datos = new DataSet();
            if (Validar_Datos()) 
            {
                Mensaje_Error();
                if (Cmb_Mes.SelectedIndex > 0)
                {
                   Ds_Datos= Consultar_Salida_Por_Mes_Especifico();
                   Llenar_Grid(Ds_Datos);
                }
                else 
                {
                   Ds_Datos=  Consultar_Salida_Anual_Por_Producto();
                   Llenar_Grid(Ds_Datos);
                }
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
        
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Excel_Click
    ///DESCRIPCIÓN: Evento del boton, el cual se encargara de llamar a los metodos
    ///             necerarios para generar el excel en PDF
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 12/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Exportar_Excel_Click(object sender, ImageClickEventArgs e)
    {
        DataSet Ds_Datos = new DataSet();
        Mensaje_Error();
        if (Validar_Datos())
        {
            
            Mensaje_Error();
            if (Cmb_Mes.SelectedIndex > 0)
            {
                Ds_Datos= Consultar_Salida_Por_Mes_Especifico();
            }
            else
            {
                Ds_Datos = Consultar_Salida_Anual_Por_Producto();
            }
            if (Ds_Datos != null && Ds_Datos.Tables.Count > 0)
            {
                Generar_Rpt(Ds_Datos);
            }
            else 
            {
                Mensaje_Error("No se encontraron registros");
            }
        }
    }
    #endregion

    
}
