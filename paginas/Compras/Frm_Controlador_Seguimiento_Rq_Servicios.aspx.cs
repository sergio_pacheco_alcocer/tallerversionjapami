﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Parametros.EasyUI;
using Newtonsoft.Json;
using System.Web.Services;
using JAPAMI.Sessiones;
using System.Globalization;
using System.Data.SqlClient;
using JAPAMI.Constantes;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Dependencias.Datos;
using JAPAMI.Seguimiento_Requisicion.Negocio;
using JAPAMI.Administrar_Requisiciones.Negocios;
using CrystalDecisions.Shared;
using CrystalDecisions.CrystalReports.Engine;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using JAPAMI.Empleados.Negocios;
using System.Collections.Generic;
using JAPAMI.Compras.Manejo_Historial_Cambios.Negocio;

public partial class Frm_Controlador_Seguimiento_Rq_Servicios : System.Web.UI.Page
{
    int pagina, renglon;
    //Cls_Ope_Pagos_Diversos Obj_Pagos_Diversos = new Cls_Ope_Pagos_Diversos();
    //cls_ope_pagos_anticipados objPagosAnticipados = new cls_ope_pagos_anticipados();
    string nombres_datos, valores;


    protected void Page_Load(object sender, EventArgs e)
    {
        this.controlador(this.Response, this.Request);
    }

    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: controlador
    //DESCRIPCIÓN: distribuye peticiones
    //PARÁMETROS:
    //CREO: Sergio Pacheco Alcocer
    //FECHA_CREO: 05/04/2012
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
    public void controlador(HttpResponse response, HttpRequest request)
    {
        String accion = "";
        String strdatos = "";
        String Combos = "";

        accion = HttpContext.Current.Request["Accion"] == null ? string.Empty : HttpContext.Current.Request["Accion"].ToString().Trim();//Para recuperar Query String Accion
        
        switch (accion)
        {
            case "Cargar_Combo_Dependencia":
                strdatos = Consulta_Combo_Dependencia();
                break;
            case "Consultar_Requisiciones":
                strdatos = Consulta_Requisiciones();
                break;
            case "Consultar_Historial":
                strdatos = Consulta_Historial();
                break;
            case "Consultar_Observaciones":
                strdatos = Consulta_Observaciones();
                break;
            case "Consultar_Contrarecibos":
                strdatos = Consulta_Contrarecibos();
                break;
            case "Consulta_Bienes":
                strdatos = Consulta_Bienes();
                break;
                     
        }

        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(strdatos);
        response.End();
    }
    #region Metodos Consultas
    //*******************************************************************************************************
    //NOMBRE_FUNCIÓN: Consulta_Combo_Dependencia()
    //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
    //             y Llama Metodo para consultar lista de Dependencias
    //PARÁMETROS: 
    //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
    //CREO: Jesus Toledo Rdz
    //FECHA_CREO: 20/Jun/2013
    //MODIFICÓ:
    //FECHA_MODIFICÓ:
    //CAUSA_MODIFICACIÓN:
    //*******************************************************************************************************
        private string Consulta_Combo_Dependencia()
        {
            String strdatos = "{[]}";
            String Catalogo = String.Empty;
            DataTable Dt_Resultado = new DataTable();
            Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();            
            try 
            {
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                Catalogo = HttpContext.Current.Request["Catalogo"] == null ? string.Empty : HttpContext.Current.Request["Catalogo"].ToString().Trim();//Para recuperar Query String Nombre Catalogo
                Dt_Resultado = Cls_Cat_Dependencias_Datos.Consulta_Dependencias(Dependencia_Negocio);
                //strdatos = Ayudante_JQuery.dataTableToJSON(Dt_Resultado);
                strdatos = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Resultado);
                return strdatos;
            }
            catch (Exception Ex) { return Ex.ToString(); };
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Requisiciones()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar lista de Requisiciones
        //PARÁMETROS: 
        //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        private string Consulta_Requisiciones()
        {
            String Str_Datos = "{[]}";
            String Filtro_Unidad_Responsable = String.Empty;
            String Filtro_Fecha_Inicial = String.Empty;
            String Filtro_Fecha_Final = String.Empty;
            String Filtro_Folio = String.Empty;
            String Estatus_Bien = String.Empty;
            String Filtro_Os = String.Empty;
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            try
            {
                //Se recuperan los Parametros del QueryString
                Filtro_Unidad_Responsable = HttpContext.Current.Request["Unidad_Responsable"] == null ? string.Empty : HttpContext.Current.Request["Unidad_Responsable"].ToString().Trim();
                Filtro_Folio = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim();
                Filtro_Fecha_Inicial = HttpContext.Current.Request["Fecha_Inicio"] == null ? string.Empty : HttpContext.Current.Request["Fecha_Inicio"].ToString().Trim();
                Filtro_Fecha_Final = HttpContext.Current.Request["Fecha_Final"] == null ? string.Empty : HttpContext.Current.Request["Fecha_Final"].ToString().Trim();
                Estatus_Bien = HttpContext.Current.Request["Estatus_Bien"] == null ? string.Empty : HttpContext.Current.Request["Estatus_Bien"].ToString().Trim();
                Filtro_Os = HttpContext.Current.Request["Os"] == null ? string.Empty : HttpContext.Current.Request["Os"].ToString().Trim();
                //Se validan parametros y se asignan a la capa de negocio
                if (!String.IsNullOrEmpty(Filtro_Folio))
                {
                    if (Filtro_Folio != "<RQ-000000>")
                    {
                        String No_Requisa = Filtro_Folio;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }
                if (!String.IsNullOrEmpty(Filtro_Unidad_Responsable)) 
                {
                    if (Filtro_Unidad_Responsable!="null" && Filtro_Unidad_Responsable!="-1")
                    Requisicion_Negocio.P_Dependencia_ID = Filtro_Unidad_Responsable; 
                }
                if (!String.IsNullOrEmpty(Filtro_Fecha_Inicial)) { if (Filtro_Fecha_Inicial != "null") Requisicion_Negocio.P_Fecha_Inicial = Filtro_Fecha_Inicial; }//string.Format("{0:MM/dd/yyyy}",Filtro_Fecha_Inicial); }
                if (!String.IsNullOrEmpty(Filtro_Fecha_Final)) { if (Filtro_Fecha_Final != "null") Requisicion_Negocio.P_Fecha_Final = Filtro_Fecha_Final; }//string.Format("{0:MM/dd/yyyy}",Filtro_Fecha_Final); }
                if (!String.IsNullOrEmpty(Filtro_Os)) { if (Filtro_Os != "null") Requisicion_Negocio.P_No_Orden_Compra = Filtro_Os; }//string.Format("{0:MM/dd/yyyy}",Filtro_Fecha_Final); }

                Requisicion_Negocio.P_Tipo_Articulo = "SERVICIO";
                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                if (Estatus_Bien != "0")
                    Requisicion_Negocio.P_Estatus = Estatus_Bien;
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                DataTable Dt_Resultado = Requisicion_Negocio.Consultar_Requisiciones_Fechas_Bienes();
                Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
            }
            catch (Exception ex)
            {
                Str_Datos = "{[]}";
            }
        return Str_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Bienes()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar lista de Bienes en RQ
        //PARÁMETROS: 
        //para Relizar la consulta en la tabla
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 27/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        private string Consulta_Bienes()
        {
            String Str_Datos = "{[]}";
            String Filtro_Unidad_Responsable = String.Empty;
            String Filtro_Estatus = String.Empty;
            String Filtro_Nombre = String.Empty;
            String Filtro_Serie = String.Empty;
            String Filtro_Modelo = String.Empty;
            String Filtro_Inventario = String.Empty;
            String Filtro_Folio = String.Empty;
            String Filtro_Os = String.Empty;
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            try
            {
                //Se recuperan los Parametros del QueryString
                Filtro_Unidad_Responsable = HttpContext.Current.Request["Unidad_Responsable"] == null ? string.Empty : HttpContext.Current.Request["Unidad_Responsable"].ToString().Trim();
                Filtro_Nombre = HttpContext.Current.Request["Nombre"] == null ? string.Empty : HttpContext.Current.Request["Nombre"].ToString().Trim();
                Filtro_Modelo = HttpContext.Current.Request["Modelo"] == null ? string.Empty : HttpContext.Current.Request["Modelo"].ToString().Trim();
                Filtro_Serie = HttpContext.Current.Request["Serie"] == null ? string.Empty : HttpContext.Current.Request["Serie"].ToString().Trim();
                Filtro_Estatus = HttpContext.Current.Request["Estatus"] == null ? string.Empty : HttpContext.Current.Request["Estatus"].ToString().Trim();
                Filtro_Inventario = HttpContext.Current.Request["Inventario"] == null ? string.Empty : HttpContext.Current.Request["Inventario"].ToString().Trim();
                Filtro_Os = HttpContext.Current.Request["Os"] == null ? string.Empty : HttpContext.Current.Request["Os"].ToString().Trim();
                Filtro_Folio = HttpContext.Current.Request["Rq"] == null ? string.Empty : HttpContext.Current.Request["Rq"].ToString().Trim();
                //Se validan parametros y se asignan a la capa de negocio
                if (!String.IsNullOrEmpty(Filtro_Folio))
                {
                    if (Filtro_Folio != "<RQ-000000>")
                    {
                        String No_Requisa = Filtro_Folio;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }
                if (!String.IsNullOrEmpty(Filtro_Unidad_Responsable))
                {
                    if (Filtro_Unidad_Responsable != "null" && Filtro_Unidad_Responsable != "-1")
                        Requisicion_Negocio.P_Dependencia_ID = Filtro_Unidad_Responsable;
                }
                if (!String.IsNullOrEmpty(Filtro_Nombre)) { if (Filtro_Nombre != "null") Requisicion_Negocio.P_Nombre_Bien = Filtro_Nombre; }
                if (!String.IsNullOrEmpty(Filtro_Modelo)) { if (Filtro_Modelo != "null") Requisicion_Negocio.P_Modelo = Filtro_Modelo; }
                if (!String.IsNullOrEmpty(Filtro_Serie)) { if (Filtro_Serie != "null") Requisicion_Negocio.P_No_Serie = Filtro_Serie; }
                if (!String.IsNullOrEmpty(Filtro_Inventario)) { if (Filtro_Inventario != "null") Requisicion_Negocio.P_No_Inventario = Filtro_Inventario; }
                if (!String.IsNullOrEmpty(Filtro_Os)) { if (Filtro_Os != "null") Requisicion_Negocio.P_No_Orden_Compra = Filtro_Os; }

                Requisicion_Negocio.P_Tipo_Articulo = "SERVICIO";
                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                if (Filtro_Estatus != "0")
                    Requisicion_Negocio.P_Estatus_Bien = Filtro_Estatus;
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                DataTable Dt_Resultado = Requisicion_Negocio.Consultar_Bienes_Requisiciones();
                Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
            }
            catch (Exception ex)
            {
                Str_Datos = "{[]}";
            }
            return Str_Datos;
        }
        private string Consulta_Historial()
        {
            String Str_Datos = "{[]}";
            String Filtro_Folio = String.Empty;
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            try
            {
                //Se recuperan los Parametros del QueryString
                Filtro_Folio = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim();
                //Se validan parametros y se asignan a la capa de negocio
                if (!String.IsNullOrEmpty(Filtro_Folio))
                {
                    if (Filtro_Folio != "<RQ-000000>")
                    {
                        String No_Requisa = Filtro_Folio;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }

                Requisicion_Negocio.P_Tipo_Articulo = "SERVICIO";
                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                DataTable Dt_Resultado = Requisicion_Negocio.Consultar_Historial_Requisicion();
                Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
            }
            catch (Exception ex)
            {
                Str_Datos = "{[]}";
            }
            return Str_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Observaciones()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar lista de Observaciones de la RQ
        //PARÁMETROS: 
        //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        private string Consulta_Observaciones()
        {
            String Str_Datos = "{[]}";
            String Filtro_Folio = String.Empty;
            Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
            try
            {
                //Se recuperan los Parametros del QueryString
                Filtro_Folio = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim();
                //Se validan parametros y se asignan a la capa de negocio
                if (!String.IsNullOrEmpty(Filtro_Folio))
                {
                    if (Filtro_Folio != "<RQ-000000>")
                    {
                        String No_Requisa = Filtro_Folio;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }

                Requisicion_Negocio.P_Tipo_Articulo = "SERVICIO";
                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                DataTable Dt_Resultado = Requisicion_Negocio.Consulta_Observaciones().Tables[0];
                Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
            }
            catch (Exception ex)
            {
                Str_Datos = "{[]}";
            }
            return Str_Datos;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Contrarecibos()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar lista de Contrarecibos de la RQ
        //PARÁMETROS: 
        //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        private string Consulta_Contrarecibos()
        {
            String Str_Datos = "{[]}";
            String Filtro_Folio = String.Empty;
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            try
            {
                //Se recuperan los Parametros del QueryString
                Filtro_Folio = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim();
                //Se validan parametros y se asignan a la capa de negocio
                if (!String.IsNullOrEmpty(Filtro_Folio))
                {
                    if (Filtro_Folio != "<RQ-000000>")
                    {
                        String No_Requisa = Filtro_Folio;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }

                Requisicion_Negocio.P_Tipo_Articulo = "SERVICIO";
                Requisicion_Negocio.P_Tipo = "TRANSITORIA";
                pagina = ParamsofEasyUI.page;
                renglon = ParamsofEasyUI.rows;
                DataTable Dt_Resultado = Requisicion_Negocio.Consultar_Contra_Recibos();
                Str_Datos = Ayudante_JQuery.onDataGrid(Dt_Resultado, pagina, renglon);
            }
            catch (Exception ex)
            {
                Str_Datos = "{[]}";
            }
            return Str_Datos;
        }
        
    #endregion
        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Nombre_Proveedor()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar Nombre de Proveedor
        //PARÁMETROS: P_Numero_Orden: Numero de Orden de compra 
        //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        [WebMethod]
        public static string Consulta_Nombre_Proveedor(String P_Numero_Orden)
        {
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;
            String Nombre_Proveedor = String.Empty;
            String Str_Respuesta = "{[]}";
            try
            {
                Requisicion_Negocio.P_No_Orden_Compra = P_Numero_Orden.Trim().Replace("OC-", "").Replace("OS-", "");
                DataTable Dt_proveedor = Requisicion_Negocio.Consultar_Proveedor();
                if ((Dt_proveedor != null) && (Dt_proveedor.Rows.Count > 0))
                {
                    Nombre_Proveedor = Dt_proveedor.Rows[0][Ope_Com_Ordenes_Compra.Campo_Nombre_Proveedor].ToString();
                    Str_Respuesta = JsonConvert.SerializeObject(Nombre_Proveedor, Formatting.Indented, configuracionJson);
                }
            }catch(Exception){}
            return Nombre_Proveedor;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Consulta_Nombre_Proveedor()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para consultar Nombre de Proveedor
        //PARÁMETROS: P_Numero_Orden: Numero de Orden de compra 
        //para Relizar la consulta en la tabla OPE_TAL_ORDENES_COMPRA
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        [WebMethod]
        public static string Consultar_Empleado(String No_Empleado)
        {
            Int32 Numero_Empleado = 0;
            Int32.TryParse(No_Empleado, out Numero_Empleado);
            Cls_Cat_Empleados_Negocios Empleado_Negocio = JAPAMI.Ayudante_Informacion.Cls_Ayudante_Nom_Informacion._Informacion_Empleado(String.Format("{0:000000}", Numero_Empleado));
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;
            String Nombre = String.Empty;
            String Str_Respuesta = "{[]}";
            try
            {
                Nombre = Empleado_Negocio.P_Nombre +" "+ Empleado_Negocio.P_Apellido_Paterno +" "+ Empleado_Negocio.P_Apelldo_Materno; 
                    Str_Respuesta = JsonConvert.SerializeObject(Nombre, Formatting.Indented, configuracionJson);
                
            }
            catch (Exception) { }
            return Nombre;
        }
        //*******************************************************************************************************
        //NOMBRE_FUNCIÓN: Alta_Fechas_Requisicion()
        //DESCRIPCIÓN: Inicializa las propiedades de capa de Negocio 
        //             y Llama Metodo para insertar valores en la Base de datos
        //PARÁMETROS: Folio de la Requisiscion, P_Fecha_Envio, P_Fecha_Recepcion, P_Fecha_Salida, P_Entrega
        //CREO: Jesus Toledo Rdz
        //FECHA_CREO: 20/Jun/2013
        //MODIFICÓ:
        //FECHA_MODIFICÓ:
        //CAUSA_MODIFICACIÓN:
        //*******************************************************************************************************
        [WebMethod]
        public static string Alta_Fechas_Requisicion(String Folio_Rq, String P_Fecha_Envio, String P_Fecha_Recepcion, String P_Fecha_Entrada, String P_Fecha_Entrega, String P_Empleado_Entrega, String P_Empleado_Entrada, String P_Empleado_Recepcion, String P_Empleado_Envio)
        {
            Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();
            JsonSerializerSettings configuracionJson = new JsonSerializerSettings();
            configuracionJson.NullValueHandling = NullValueHandling.Ignore;
            List<Cls_Bean_Seguimiento_Rq> objDetalles = new List<Cls_Bean_Seguimiento_Rq>();
            String Nombre_Proveedor = String.Empty;
            String Empleado_Entrega = P_Empleado_Entrega;
            String Empleado_Recepcion = P_Empleado_Recepcion;
            String Empleado_Entrada = P_Empleado_Entrada;
            String Empleado_Envio = P_Empleado_Envio;
            String Str_Respuesta = "{[]}";
            DateTime Fecha_Envio = new DateTime();
            DateTime Fecha_Recepcion = new DateTime();
            DateTime Fecha_Entrada = new DateTime();
            DateTime Fecha_Entrega = new DateTime();
            try
            {
                //objDetalles = JsonConvert.DeserializeObject<List<Cls_Bean_Seguimiento_Rq>>(P_Bean_Alta.ToString());
                if (!String.IsNullOrEmpty(Folio_Rq))
                {
                    if (Folio_Rq != "<RQ-000000>")
                    {
                        String No_Requisa = Folio_Rq;
                        No_Requisa = No_Requisa.ToUpper();
                        No_Requisa = No_Requisa.Replace("RQ-", "");
                        int Int_No_Requisa = 0;
                        try
                        {
                            Int_No_Requisa = int.Parse(No_Requisa);
                        }
                        catch (Exception Ex)
                        {
                            String Str = Ex.ToString();
                            No_Requisa = "0";
                        }
                        Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                    }
                }
                if (!String.IsNullOrEmpty(P_Fecha_Envio)) { if (P_Fecha_Envio != "null") DateTime.TryParse(string.Format("{0:MM/dd/yyyy}", P_Fecha_Envio), out Fecha_Envio); }
                if (!String.IsNullOrEmpty(P_Fecha_Recepcion)) { if (P_Fecha_Recepcion != "null") DateTime.TryParse(string.Format("{0:MM/dd/yyyy}", P_Fecha_Recepcion), out Fecha_Recepcion); }
                if (!String.IsNullOrEmpty(P_Fecha_Entrada)) { if (P_Fecha_Entrada != "null") DateTime.TryParse(string.Format("{0:MM/dd/yyyy}", P_Fecha_Entrada), out Fecha_Entrada); }
                if (!String.IsNullOrEmpty(P_Fecha_Entrega)) { if (P_Fecha_Entrega != "null") DateTime.TryParse(string.Format("{0:MM/dd/yyyy}", P_Fecha_Entrega), out Fecha_Entrega); }
                if (!String.IsNullOrEmpty(Empleado_Entrega)) { Requisicion_Negocio.P_Empleado_Entrega = Empleado_Entrega; }
                if (!String.IsNullOrEmpty(Empleado_Entrada)) { Requisicion_Negocio.P_Empleado_Entrada = Empleado_Entrada; }
                if (!String.IsNullOrEmpty(Empleado_Envio)) { Requisicion_Negocio.P_Empleado_Envio = Empleado_Envio; }
                if (!String.IsNullOrEmpty(Empleado_Recepcion)) { Requisicion_Negocio.P_Empleado_Recepcion = Empleado_Recepcion; }
                Requisicion_Negocio.P_Fecha_Envio = Fecha_Envio;
                Requisicion_Negocio.P_Fecha_Recepcion = Fecha_Recepcion;
                Requisicion_Negocio.P_Fecha_Entrada = Fecha_Entrada;
                Requisicion_Negocio.P_Fecha_Entrega = Fecha_Entrega;

                Cls_Ope_Com_Manejo_Historial_Cambios_Negocio Tmp_Negocio = new Cls_Ope_Com_Manejo_Historial_Cambios_Negocio();
                Tmp_Negocio.P_No_Requisicion = Requisicion_Negocio.P_Requisicion_ID.Trim();
                Tmp_Negocio.P_Cls_Negocio_Actualizado = Requisicion_Negocio;
                Requisicion_Negocio.P_Dt_Cambios = Tmp_Negocio.Obtener_Tabla_Cambios();

                Requisicion_Negocio.Insertar_Fechas();
                Nombre_Proveedor = "1";
                    //Str_Respuesta = JsonConvert.SerializeObject(Nombre_Proveedor, Formatting.Indented, configuracionJson);                
            }
            catch (Exception) { return "0"; }
            return Nombre_Proveedor;
        }        
}
public class Cls_Bean_Seguimiento_Rq
{
    public string Folio_Rq { set; get; }
    public string P_Fecha_Envio { set; get; }
    public string P_Fecha_Recepcion { set; get; }
    public string P_Fecha_Entrada { set; get; }
    public string P_Fecha_Entrega { set; get; }
    public string P_Empleado_Envio { set; get; }
    public string P_Empleado_Recepcion { set; get; }
    public string P_Empleado_Entrada { set; get; }
    public string P_Empleado_Entrega { set; get; }

    public Cls_Bean_Seguimiento_Rq()
    {
    }
}