﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Rpt_Com_Existencias_productos.Negocio;
using JAPAMI.Constantes;
using CarlosAg.ExcelXmlWriter;

public partial class paginas_Compras_Frm_Rpt_Com_Productos_Existencias : System.Web.UI.Page
{
     #region PAGE LOAD / INIT
        protected void Page_Load(object sender, EventArgs e)
        {
            //Valores por primera vez
            if (!IsPostBack)
            {
                Mostrar_Mensaje();
                Inicializar_Combos_Generico(Cmb_Partida_Especifica);
                
            }
        }
     #endregion

    #region METODOS
     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Inicializar_Combos_Generico 
    ///DESCRIPCIÓN: Metodo limpia un combo y le asigna un item especifico
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 17/Diciembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Inicializar_Combos_Generico(DropDownList Cmb_Combo)
    {
        Cmb_Combo.Items.Clear();
        Cmb_Combo.Items.Add("<SELECCIONAR>");
        Cmb_Combo.Items[0].Value = "0";
        Cmb_Combo.Items[0].Selected = true;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Buscar_Productos 
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 17/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataTable Buscar_Productos() 
    {
        Cls_Rpt_Com_Existencias_Productos_Negocio Existencia_Productos_Negocio = new Cls_Rpt_Com_Existencias_Productos_Negocio();
        DataTable Dt_Produtos= null;
        try 
        {
            if (!String.IsNullOrEmpty(Txt_Producto_Nombre.Text.ToString())) 
            {
                Existencia_Productos_Negocio.P_Nombre_Producto = Txt_Producto_Nombre.Text.ToString().Trim();
            }
            if (Cmb_Partida_Especifica.SelectedIndex > 0) 
            {
                Existencia_Productos_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.ToString().Trim();
            }
            if(Cmb_Tipo_Producto.SelectedIndex >0)
            {
                if (Cmb_Tipo_Producto.SelectedItem.ToString().Contains("ALMACEN GENERAL"))
                {
                    Existencia_Productos_Negocio.P_Alamacen_General = "SI";
                }
                else 
                {
                    Existencia_Productos_Negocio.P_Alamacen_General = "NO";
                }
            }

            Dt_Produtos = Existencia_Productos_Negocio.Consultar_Productos();
        }
        catch(Exception Ex)
        {
            throw new Exception("Error al consultar los productos : " + Ex.Message);
        }
        return Dt_Produtos;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Mensaje 
    ///DESCRIPCIÓN: Metodo que permite mostrar u ocultar los mensajes de error
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 18/Diciembre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Mostrar_Mensaje(String Mensaje) 
    {
        Lbl_Mensaje_Error.Text += Mensaje + " <br/>";
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Visible = true;
    }
    public void Mostrar_Mensaje()
    {
        Lbl_Mensaje_Error.Text  = "";
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Visible = false;
    }
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 24/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Reporte_Excel(DataTable Dt_Reporte)
    {
        WorksheetCell Celda = new WorksheetCell();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Tipo_Estilo = "";
        String Informacion_Registro = "";
        Int32 Contador_Estilo = 0;
        Int32 Operacion = 0;
        try
        {

            Mostrar_Mensaje();
            Nombre_Archivo = "Rpt_Existencia_Productos.xls"; //Nombre que se le dara al reporte de excel;
            Ruta_Archivo = @Server.MapPath("../../Archivos/" + Nombre_Archivo); //lugar enn donde se almacenara el reporte

            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Reporte_Productps";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
            //  Creamos el estilo cabecera 3 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
            //Creamos el estilo fecha para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Fecha = Libro.Styles.Add("HeaderStyleFecha");
            //  Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //  Creamos el estilo contenido2 del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");


            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera
            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 11;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.WrapText = true;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#0066CC";
            Estilo_Cabecera.Interior.Color = "#CCCCFF";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera2
            Estilo_Cabecera2.Font.FontName = "Tahoma";
            Estilo_Cabecera2.Font.Size = 10;
            Estilo_Cabecera2.Font.Bold = true;
            Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera2.Alignment.Rotate = 0;
            Estilo_Cabecera2.Font.Color = "#0066CC";
            Estilo_Cabecera2.Interior.Color = "#FFFFFF";
            Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la HeaderStyle3
            Estilo_Cabecera3.Font.FontName = "Tahoma";
            Estilo_Cabecera3.Font.Size = 10;
            Estilo_Cabecera3.Font.Bold = true;
            Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera3.Alignment.Rotate = 0;
            Estilo_Cabecera3.Font.Color = "#003399";
            Estilo_Cabecera3.Interior.Color = "LightGray";
            Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la fecha
            Estilo_Fecha.Font.FontName = "Tahoma";
            Estilo_Fecha.Font.Size = 10;
            Estilo_Fecha.Font.Bold = false;
            Estilo_Fecha.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Fecha.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Fecha.Alignment.Rotate = 0;
            Estilo_Fecha.Font.Color = "#003399";
            Estilo_Fecha.Interior.Color = "#FFFFFF";
            Estilo_Fecha.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Fecha.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle
            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido.Alignment.Rotate = 0;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle2
            Estilo_Contenido2.Font.FontName = "Tahoma";
            Estilo_Contenido2.Font.Size = 9;
            Estilo_Contenido2.Font.Bold = false;
            Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido2.Alignment.Rotate = 0;
            Estilo_Contenido2.Font.Color = "#000000";
            Estilo_Contenido2.Interior.Color = "#FAF0E6";
            Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //*************************************** fin de los estilos***********************************************************

            //***************************************Inicio del reporte Proveedores por partida Hoja 1***************************

            //  Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("REPORTE DE PEDIDOS");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();

            //Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));// 1 CLAVE
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));// 2 NOMBRE
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  3 DESCRIPCION
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(170));//  4 PARTIDA
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  3 EXISTENCIA
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  4 COMPROMETIDO
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  4 DISPONIBLE

            //  se llena el encabezado principal
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO ");
            Celda.MergeAcross = 6; // Merge 6 cells together
            Celda.MergeDown = 1;
            Celda.StyleID = "HeaderStyle";
            Renglon = Hoja.Table.Rows.Add();

            //Se llena el segundo encabezado
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("Reporte de productos y sus existencias  ");
            Celda.MergeAcross = 6; // Merge 6 cells together
            Celda.StyleID = "HeaderStyle2";

            //Renglon con la fecha Acutal
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add(DateTime.Now.ToString());
            Celda.MergeAcross = 6; // Merge 6 cells together
            Celda.StyleID = "HeaderStyleFecha";


            //  para los titulos de las columnas
            Renglon = Hoja.Table.Rows.Add();
            Celda = Renglon.Cells.Add("CLAVE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("NOMBRE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("DESCRIPCION");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("PARTIDA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("EXISTENCIA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("COMPROMETIDO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Celda = Renglon.Cells.Add("DISPONIBLE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "HeaderStyle3";

            Renglon = Hoja.Table.Rows.Add();

            //  para llenar el reporte
            if (Dt_Reporte.Rows.Count > 0)
            {
                //  Se comienza a extraer la informaicon de la onsulta
                foreach (DataRow Renglon_Reporte in Dt_Reporte.Rows)
                {
                    Contador_Estilo++;
                    Operacion = Contador_Estilo % 2;
                    if (Operacion == 0)
                    {
                        //Estilo_Concepto.Interior.Color = "LightGray";
                        Tipo_Estilo = "BodyStyle2";
                    }
                    else
                    {
                        Tipo_Estilo = "BodyStyle";
                        //Estilo_Concepto.Interior.Color = "White";
                    }

                    Renglon = Hoja.Table.Rows.Add();
                    foreach (DataColumn Column in Dt_Reporte.Columns)
                    {
                        Informacion_Registro = (Renglon_Reporte[Column.ColumnName].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                    }
                }
            }

            //***************************************Fin del reporte Proveedores por partida Hoja 1***************************

            //  se guarda el documento
            Libro.Save(Ruta_Archivo);
            //  mostrar el archivo
            Mostrar_Reporte(Nombre_Archivo);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            //Response.ContentEncoding = Encoding.Default;
            //Libro.Save(Response.OutputStream);
            //Response.End();
        }// fin try
        catch (Exception Ex)
        {
            Mostrar_Mensaje(Ex.Message);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    /// USUARIO CREO:        Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:          18/Enero/2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Ruta = "../../Archivos/" + Nombre_Reporte_Generar;
        try
        {
            Mostrar_Mensaje();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=100,height=100')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    #region EVENTOS
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Conultar_Click 
        ///DESCRIPCIÓN: Evento del botón Buscar
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Buscar_Conultar_Click(object sender, ImageClickEventArgs e)
        {
            DataTable Dt_Productos= null;
            try
            {
                Mostrar_Mensaje();
                Dt_Productos = Buscar_Productos();
                if (Dt_Productos != null && Dt_Productos.Rows.Count > 0)
                {
                    Grid_Productos.DataSource = Dt_Productos;
                    Grid_Productos.DataBind();
                }
                else 
                {
                    Grid_Productos.DataSource =  new DataTable();
                    Grid_Productos.DataBind();
                    Mostrar_Mensaje("No se encontraron productos");
                }
            }
            catch (Exception Ex) 
            {
                Mostrar_Mensaje(Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Excel_Click 
        ///DESCRIPCIÓN: Evento del botón exportar a excel
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Exportar_Excel_Click(object sender, ImageClickEventArgs e)
        {
            DataTable Dt_Productos = null;
            try
            {
                Mostrar_Mensaje();
                Dt_Productos = Buscar_Productos();
                if (Dt_Productos != null && Dt_Productos.Rows.Count > 0)
                {
                    Generar_Reporte_Excel(Dt_Productos);
                }
                else 
                {
                    Mostrar_Mensaje("No se encontraron productos");
                }
            }
            catch (Exception Ex) 
            {
                Mostrar_Mensaje(Ex.Message);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click 
        ///DESCRIPCIÓN: Evento del botón Salir
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.ToolTip.Equals("Inicio"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Txt_Partida_Especifica_TextChanged 
        ///DESCRIPCIÓN: Evento de la caja de texto Partida_Especifica
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Txt_Partida_Especifica_TextChanged(object sender, EventArgs e)
        {
            if (!String.IsNullOrEmpty(Txt_Partida_Especifica.Text))
            {
                Mostrar_Mensaje();
                Cls_Rpt_Com_Existencias_Productos_Negocio Existencias_Prod_Negocio = new Cls_Rpt_Com_Existencias_Productos_Negocio();
                Existencias_Prod_Negocio.P_Clave_Partida = Txt_Partida_Especifica.Text.ToString().Trim();
                DataTable Dt_Partidas = Existencias_Prod_Negocio.Concultar_Partidas_Especificas();
                if (Dt_Partidas != null && Dt_Partidas.Rows.Count > 0)
                {
                    Cmb_Partida_Especifica.Items.Clear();
                    Cmb_Partida_Especifica.DataSource = Dt_Partidas;
                    Cmb_Partida_Especifica.DataTextField = Dt_Partidas.Columns[1].ToString();
                    Cmb_Partida_Especifica.DataValueField = Dt_Partidas.Columns[0].ToString();
                    Cmb_Partida_Especifica.DataBind();
                    Cmb_Partida_Especifica.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                    Cmb_Partida_Especifica.SelectedIndex = 1;
                   
                }
                else
                {
                    Cmb_Partida_Especifica.Items.Clear();
                    Cmb_Partida_Especifica.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                    Cmb_Partida_Especifica.SelectedIndex = 0;
                    Mostrar_Mensaje("No se encontraron partidas ");
                }
            }
            else
            {
                Cmb_Partida_Especifica.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                Cmb_Partida_Especifica.SelectedIndex = 0;
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Partida_Especifica_SelectedIndexChanged 
        ///DESCRIPCIÓN: Evento del combo cmb_Partida_Especifica
        ///CREO: Jennyfer Ivonne Ceja Lemus
        ///FECHA_CREO: 18/Diciembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Cmb_Partida_Especifica_SelectedIndexChanged(object sender, EventArgs e)
        {
            try 
            {
                if (Cmb_Partida_Especifica.SelectedIndex > 0)
                {
                    Cls_Rpt_Com_Existencias_Productos_Negocio Productos_Negocio = new Cls_Rpt_Com_Existencias_Productos_Negocio();
                    Productos_Negocio.P_Partida_ID = Cmb_Partida_Especifica.SelectedValue.ToString().Trim();
                    DataTable Dt_Clave = Productos_Negocio.Consultar_Clave_Partia_Especifica();
                    Txt_Partida_Especifica.Text = Dt_Clave.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Clave].ToString();
                }
                else 
                {
                    Txt_Partida_Especifica.Text = "";
                }
            }
            catch(Exception  Ex)
            {
                Mostrar_Mensaje("Error al cargar el combo :" + Ex.Message);
            }
        }
    #endregion
}
