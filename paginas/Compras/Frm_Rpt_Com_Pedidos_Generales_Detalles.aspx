﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Rpt_Com_Pedidos_Generales_Detalles.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Com_Pedidos_Generales_Detalles" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Requisiciones</title>
    <link href="../estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="../estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    
    <div style="height:600px; overflow: auto; width: 98%; vertical-align: top;">
                                    <asp:GridView ID="Grid_Reporte" runat="server" CssClass="GridView_1" AutoGenerateColumns="false"
                                     GridLines="None" Width="98%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="FOLIO" HeaderText="Folio" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="OC" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TIPO" HeaderText="Tipo" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="GERENCIA" HeaderText="Gerencia" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPARTAMENTO" HeaderText="Departamento" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PARTIDA" HeaderText="Partida" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PROVEEDOR" HeaderText="Proveedor" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRODUCTO" HeaderText="Producto" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cant." NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                             <asp:BoundField DataField="MONTO" HeaderText="Precio Unitario" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MONTO_TOTAL" HeaderText="Total Inicial" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRECIO_U_SIN_IMP_COTIZADO" HeaderText="Precio U. Cotizado" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="TOTAL_COTIZADO" HeaderText="Total Cotizado" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_SALIDA" HeaderText="No. Salida" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha Creo" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario Creo" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RECIBIO" HeaderText="Recibio" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
    </div>
    </form>
</body>
</html>
