﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Com_Asignacion_Fecha_Entrada_Salida.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Com_Seguimiento_Requisiciones_Servicios_Generales"
    Title="Seguimiento a Requisiciones"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <link href="../../jquery-easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script src="../../easyui/ui.datepicker-es.js" type="text/javascript"></script>
    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/Js_Ope_Com_Seguimiento_Rq_Servicios_Generales.js" type="text/javascript"></script>

    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        
                <%--Div Encabezado--%>
                <div id="Div_Encabezado" style="display:none">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Asignación de Fechas de la Requisición de Servicio
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <img id="Img_Warning" style="display:none" src="../../paginas/imagenes/paginas/sias_warning.png" alt="" />
                                     <span id="Lbl_Informacion" style="color:#990000"></span>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <img id="Img_Nuevo" alt="" src="" class="Img_Button" />
                                <img id="Img_Modificar" alt="" src="" class="Img_Button" />
                                <img id="Img_Eliminar" alt="" src="" class="Img_Button" />
                                <img id="Img_Imprimir_Historial_Cambios" alt="" src="" class="Img_Button" />
                                <img id="Img_Listar_Requisiciones" alt="" src="" class="Img_Button" />
                                <img id="Img_Salir" alt="" src="" class="Img_Button" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones" style="display:none">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%;">
                                Unidad Responsable
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Dependencia_Panel" Width="96%" runat="server"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%;">
                                Estatus
                            </td>
                            <td colspan="3">
                                <select id="Cmb_Estatus" style="width:96%"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <input type="text" id="Dtp_Fecha_Inicial"/>                                
                                <input type="text" id="Dtp_Fecha_Final" />                                
                            </td>
                            <td align="left" visible="false" colspan="2">
                                RQ                            
                                <input type="text" id="Txt_Busqueda" style="width:65px"/>                                
                                OS
                                <input type="text" id="Txt_Os" style="width:65px"/>
                                <img id="Img_Buscar" alt="" src="" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="width: 99%">
                                <br />
                            </td>
                            <tr>
                                <td colspan="4" style="width: 99%">
                                    <div style="overflow: visible; height: 320px; width: 99%; vertical-align: top;">
                                        <table id="Tbl_Requisiciones">
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" style="display:none">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Requisición
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="80%" class="Detalles"></asp:TextBox>
                                <img id="Img_Pdf" alt="PDF" title="Imprimir Requisicion" style="cursor:pointer" src="../../paginas/imagenes/paginas/grid_print.png" height="24px" width="24px"/>
                            </td>
                            <td style="width: 15%;" align="left">
                                Orden Servicio
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_OC" runat="server" Width="96%" class="Detalles"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Unidad Responsable
                            </td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <asp:DropDownList ID="Cmb_Dependencia" runat="server" Width="99.1%" class="Combos">
                                </asp:DropDownList>
                            </td>                            
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Proveedor
                            </td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <asp:TextBox ID="Txt_Proveedor" runat="server" Width="98.6%" class="Detalles"></asp:TextBox>
                            </td>                            
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Total
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_Total" runat="server" Width="96%" class="Detalles"></asp:TextBox>
                            </td>
                            <td style="width: 15%;" align="left">
                                Total Cotizado
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_Total_Cotizado" runat="server" Width="96%" class="Detalles"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Tipo
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_Tipo" runat="server" Width="96%" class="Detalles"></asp:TextBox>
                            </td> 
                            <td></td>
                            <td></td>                                                       
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Fecha Recepcion
                            </td>
                            <td style="width: 35%;" align="left">
                                <input type="text" id="Dtp_Fecha_Recepcion" class="Cajas" style="width: 88%;" />
                            </td>
                            <td style="width: 15%;" align="left">
                                No. Empleado 
                            </td>
                            <td style="width: 35%;" align="left">
                                <input type="text" id="Txt_Empleado_Recepcion" class="Cajas" style="width: 96%;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Nombre</td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <input type="text" id="Txt_Nombre_Recepcion" class="Cajas" style="width: 98.9%;" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Fecha Envio
                            </td>
                            <td style="width: 35%;" align="left">
                                <input type="text" id="Dtp_Fecha_Envio" class="Cajas" style="width: 88%;" />                                
                            </td>
                            <td>No. Empleado</td>
                            <td><input type="text" id="Txt_Empleado_Envio" class="Cajas" style="width: 96%;" /></td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Nombre</td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <input type="text" id="Txt_Nombre_Envio" class="Cajas" style="width: 98.9%;" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Fecha Entrada
                            </td>
                            <td style="width: 35%;" align="left">
                                <input type="text" id="Dtp_Fecha_Entrada" class="Cajas" style="width: 88%;" />
                            </td>
                            <td>No. Empleado</td>
                            <td><input type="text" id="Txt_Empleado_Entrada" class="Cajas" style="width:96%;" /></td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Nombre</td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <input type="text" id="Txt_Nombre_Entrada" class="Cajas" style="width: 98.9%;" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <hr />
                            </td>
                        </tr>
                        <tr style="height: 20px;">
                            <td style="width: 15%;" align="left">
                                Fecha Entrega
                            </td>
                            <td style="width: 35%;" align="left">
                                <input type="text" id="Dtp_Fecha_Entrega" class="Cajas" style="width: 88%;" />
                            </td>                                
                            <td>
                            No. Empleado
                            </td>
                            <td>
                            <input type="text" id="Txt_Empleado_Entrega" class="Cajas" style="width: 96%;" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Nombre</td>
                            <td style="width: 35%;" align="left" colspan="3">
                                <input type="text" id="Txt_Nombre_Entrega" class="Cajas" style="width: 98.9%;" /></td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="overflow:visible; height: 220px; width: 99%;">
                                    <table id="Tbl_Historial">
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="overflow:visible; height: 220px; width: 99%; vertical-align: top;">
                                    <table id="Tbl_Comentarios">
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <div style="overflow:visible; height: 120px; width: 99%; vertical-align: top;">
                                    <table id="Tbl_Contrarecibos">
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
    </div>
</asp:Content>
