﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;

using JAPAMI.Dependencias.Negocios;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Administrar_Requisiciones.Negocios;

public partial class paginas_Compras_Frm_Ope_Com_Modificar_Requisicion_Transitoria : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Session["Activa"] = true;
                LLenar_Combos();
                Configuracion_Formulario(false);
                Btn_Buscar_Click(null, null);
                Hdf_Requisicion_ID.Value = String.Empty;
                Hdf_Monto_Inicial.Value = String.Empty;
                Hdf_Productos.Value = String.Empty;
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Formulario
        ///DESCRIPCIÓN          : Carga una configuracion de los controles del Formulario
        ///PARAMETROS           : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                   de los controles.
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Div_Contenido.Visible = Estatus;
            Div_Listado_Requisiciones.Visible = !Estatus;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : LLenar_Combos
        ///DESCRIPCIÓN          : Carga una configuracion de los ComboBox del Formulario
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void LLenar_Combos()
        {
            Cls_Cat_Dependencias_Negocio Dependencias = new Cls_Cat_Dependencias_Negocio();

            DataTable Dt_Aux = new DataTable();
            Dt_Aux = Dependencias.Consulta_Dependencias();

            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Buscar_Unidad_Responsable, Dt_Aux, "CLAVE_NOMBRE", "DEPENDENCIA_ID");
            //Cmb_Buscar_Unidad_Responsable.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN : Llenar_Grid_Requisiciones
        // DESCRIPCIÓN          : Llena el grid principal de requisiciones
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Llenar_Grid_Requisiciones(int Page)
        {
            Div_Contenedor_Msj_Error.Visible = false;

            DataTable Dt_Requisas = new DataTable();
            Cls_Ope_Com_Requisiciones_Negocio Requisiciones = new Cls_Ope_Com_Requisiciones_Negocio();
            Requisiciones.P_Dependencia_ID = Cmb_Buscar_Unidad_Responsable.SelectedValue;
            
            Requisiciones.P_Estatus = Cmb_Busqueda_Estatus.SelectedValue;
            Requisiciones.P_Tipo = "TRANSITORIA";

            if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
            {
                Requisiciones.P_Folio = Txt_Busqueda.Text;
                Requisiciones.P_Requisicion_ID = Txt_Busqueda.Text;
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Requisiciones.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
                Requisiciones.P_Fecha_Final = Txt_Fecha_Final.Text;
            }
            else
            {
                Requisiciones.P_Fecha_Inicial = String.Format("{0:dd/MMM/yyyy}", DateTime.Now.AddYears(-10));
                Requisiciones.P_Fecha_Final = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
            }
            Dt_Requisas = Requisiciones.Consultar_Requisiciones();

            Grid_Requisiciones.DataSource = Dt_Requisas;
            Grid_Requisiciones.PageIndex = Page;
            Grid_Requisiciones.Columns[1].Visible = true;
            Grid_Requisiciones.Columns[5].Visible = true;
            Grid_Requisiciones.DataBind();
            Grid_Requisiciones.Columns[1].Visible = false;
            Grid_Requisiciones.Columns[5].Visible = false;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN : Llenar_Grid_Comentarios
        // DESCRIPCIÓN          : Llena el grid principal de Comentarios
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Llenar_Grid_Comentarios(int Page)
        {
            Cls_Ope_Com_Administrar_Requisiciones_Negocio Administrar_Requisicion = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
            Administrar_Requisicion.P_Requisicion_ID = Hdf_Requisicion_ID.Value;
            DataSet Data_Set = Administrar_Requisicion.Consulta_Observaciones();
            Grid_Comentarios.DataSource = Data_Set;
            Grid_Comentarios.PageIndex = Page;
            Grid_Comentarios.DataBind();
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN : Llenar_Grid_Productos_Servicios
        // DESCRIPCIÓN          : Llena el grid principal de Productos
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 15/Mayo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        public void Llenar_Grid_Productos_Servicios(int Page)
        {
            DataTable Dt_Prod_Ser = new DataTable();
            Dt_Prod_Ser = (DataTable)Session["Dt_Prod_Ser"];

            if (Dt_Prod_Ser != null)
            {
                Grid_Productos_Servicios.DataSource = Dt_Prod_Ser;
                Grid_Productos_Servicios.PageIndex = Page;
                Grid_Productos_Servicios.DataBind();
            }
        }

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (!String.IsNullOrEmpty(Hdf_Requisicion_ID.Value))
                {
                    if (Btn_Modificar.AlternateText.Equals("Modificar"))
                    {
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Grid_Productos_Servicios.Enabled = true;
                    }
                    else
                    {
                        Double Disponible = 0;
                        Double.TryParse(Hdf_Disponible.Value, out Disponible);
                        if (Disponible < 0)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('No existe presupuesto suficiente');", true);
                        }
                        else
                        {
                            Cls_Ope_Com_Requisiciones_Negocio Requisicion = new Cls_Ope_Com_Requisiciones_Negocio();
                            Requisicion.P_Requisicion_ID = Hdf_Requisicion_ID.Value;

                            DataTable Dt_Productos_Servicios = new DataTable();
                            Dt_Productos_Servicios.Columns.Add("Ope_Com_Req_Producto_ID", typeof(System.String));
                            Dt_Productos_Servicios.Columns.Add("Cantidad", typeof(System.Double));
                            Dt_Productos_Servicios.Columns.Add("Monto", typeof(System.Double));
                            Dt_Productos_Servicios.Columns.Add("Monto_IVA", typeof(System.Double));
                            Dt_Productos_Servicios.Columns.Add("Monto_IEPS", typeof(System.Double));
                            Dt_Productos_Servicios.Columns.Add("Monto_Total", typeof(System.Double));

                            String[] Productos = Hdf_Productos.Value.Split(';');

                            foreach (String Prod in Productos)
                            {
                                String[] Cant = Prod.Split(',');

                                if (Cant.Length == 6)
                                {
                                    DataRow Dr_Producto = Dt_Productos_Servicios.NewRow();
                                    Dr_Producto["Ope_Com_Req_Producto_ID"] = Cant[0];
                                    Dr_Producto["Cantidad"] = Double.Parse(Cant[1]);
                                    Dr_Producto["Monto"] = Double.Parse(Cant[2]);
                                    Dr_Producto["Monto_IEPS"] = Double.Parse(Cant[3]);
                                    Dr_Producto["Monto_IVA"] = Double.Parse(Cant[4]);
                                    Dr_Producto["Monto_Total"] = Double.Parse(Cant[5]);

                                    Dt_Productos_Servicios.Rows.Add(Dr_Producto);
                                    Dt_Productos_Servicios.AcceptChanges();
                                }
                                else
                                {
                                    Requisicion.P_Subtotal = Cant[0];
                                    Requisicion.P_IEPS = Cant[1];
                                    Requisicion.P_IVA = Cant[2];
                                    Requisicion.P_Total = Cant[3];
                                }
                            }
                            Requisicion.P_Dt_Productos_Servicios = Dt_Productos_Servicios;
                            String Mensaje = Requisicion.Modificar_Cant_Prod_Req();

                            if (Mensaje.Contains("Actualización Exitosa"))
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                                Btn_Modificar.AlternateText = "Modificar";
                                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                                Btn_Salir.AlternateText = "Salir";
                                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                                Configuracion_Formulario(false);
                            }
                            else
                            {
                                throw new Exception(Mensaje);
                            }
                        }
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Text = "Seleccione el registro a Modificar.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Cancela la operación que esta en proceso 
        ///                       (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            if (Btn_Salir.AlternateText == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Configuracion_Formulario(false);
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
            Hdf_Requisicion_ID.Value = String.Empty;
            Hdf_Monto_Inicial.Value = String.Empty;
            Hdf_Productos.Value = String.Empty;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
        ///DESCRIPCIÓN          : Realiza la busque en la BD.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text) ||
                !String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Lbl_Mensaje_Error.Text = " + Es necesario Ingresar ambas fechas.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
            else
            {
                Llenar_Grid_Requisiciones(0);
            }
        }

    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Requisiciones_PageIndexChanging
        ///DESCRIPCIÓN          : Carga los registros del Grid para una nueva Pagina.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Llenar_Grid_Requisiciones(e.NewPageIndex);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Requisiciones_RowDataBound
        ///DESCRIPCIÓN          : Muestra u Oculta el boton de alerta.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Requisiciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (!String.IsNullOrEmpty(e.Row.Cells[5].Text))
                {
                    ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
                    if (e.Row.Cells[5].Text == "CONSTRUCCION" && e.Row.Cells[5].Text.ToString().Trim() == "AMARILLO")
                    {
                        Boton.ImageUrl = "../imagenes/gridview/circle_yellow.png";
                        Boton.Visible = true;
                    }
                    else
                    {
                        Boton.Visible = false;
                    }
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Requisiciones_SelectedIndexChanged
        ///DESCRIPCIÓN          : Obtiene los datos de un Listado Seleccionada para mostrarlos a detalle
        ///PROPIEDADES          :     
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 14/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Requisiciones_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Requisiciones.SelectedIndex > (-1))
                {
                    Configuracion_Formulario(true);
                    Btn_Salir.AlternateText = "Cancelar";
                    Cls_Ope_Com_Requisiciones_Negocio Requisicion = new Cls_Ope_Com_Requisiciones_Negocio();
                    Hdf_Requisicion_ID.Value = HttpUtility.HtmlDecode(Grid_Requisiciones.SelectedRow.Cells[1].Text.Trim());
                    Requisicion.P_Requisicion_ID = Hdf_Requisicion_ID.Value;
                    DataSet Ds_Detalles_Req = Requisicion.Consultar_Requisicion();
                    DataTable Dt_Prod_Serv = Requisicion.Consultar_Productos_Servicios_Requisiciones();

                    if (Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows.Count > 0)
                    {
                        Txt_Folio.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Folio].ToString().Trim();
                        Txt_Estatus.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString();
                        Txt_Fecha.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Parse(Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Fecha_Generacion].ToString()));
                        Txt_Subtotal.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Subtotal].ToString();
                        Txt_IEPS.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_IEPS].ToString();
                        Txt_IVA.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_IVA].ToString();
                        Txt_Total.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Total].ToString();
                        Hdf_Monto_Inicial.Value = Txt_Total.Text;
                        Txt_Justificacion.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString();
                        Txt_Especificaciones.Text = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Especificacion_Prod_Serv].ToString();
                        Chk_Verificar.Checked = Ds_Detalles_Req.Tables["Dt_Requisiciones"].Rows[0][Ope_Com_Requisiciones.Campo_Verificaion_Entrega].ToString().Trim() == "SI" ? true : false;
                    }

                    Session["Dt_Prod_Ser"] = Ds_Detalles_Req.Tables["Dt_Req_Producto"];
                    Llenar_Grid_Productos_Servicios(0);
                    Llenar_Grid_Comentarios(0);

                    if (Ds_Detalles_Req.Tables["Dt_Detalles"].Rows.Count > 0)
                    {
                        Txt_Unidad_Responsable.Text = Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0]["DEPENDENCIA"].ToString();
                        Txt_Fte_Financiamiento.Text = Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0]["FTE_FINANCIAMIENTO"].ToString();
                        Txt_Programa.Text = Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0]["PRYECTO_PROGRAMA"].ToString();
                        Txt_Partida.Text = Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0]["PARTIDA"].ToString();


                        Double Disponible_Partida = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Disponible_Partida(
                            Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Fuente_Financiamiento_ID].ToString(),
                            Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Proyecto_Programa_ID].ToString(),
                            Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0][Ope_Com_Requisiciones.Campo_Dependencia_ID].ToString(),
                            Ds_Detalles_Req.Tables["Dt_Detalles"].Rows[0][Ope_Com_Req_Producto.Campo_Partida_ID].ToString(),
                            DateTime.Now.Year.ToString(),
                            Cls_Ope_Psp_Manejo_Presupuesto.DISPONIBLE);

                        if (Disponible_Partida > 0)
                        {
                            Lbl_Disponible_Partida.Text = " $ " + String.Format("{0:n}", Disponible_Partida);
                        }
                        else
                        {
                            Lbl_Disponible_Partida.Text = "Sin presupuesto asignado";
                        }
                    }

                }

                Grid_Productos_Servicios.Enabled = false;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Productos_Servicios_PageIndexChanging
        ///DESCRIPCIÓN          : Carga los registros del Grid para una nueva Pagina.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Productos_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            //DataTable Dt_Productos = new DataTable();
            //Dt_Productos.Columns.Add("Ope_Com_Req_Producto_ID", typeof(String));
            //Dt_Productos.Columns.Add("Prod_Serv_ID", typeof(String));
            //Dt_Productos.Columns.Add("Nombre_Producto_Servicio", typeof(String));
            //Dt_Productos.Columns.Add("Cantidad", typeof(String));
            //Dt_Productos.Columns.Add("Precio_Unitario", typeof(String));
            //Dt_Productos.Columns.Add("Monto", typeof(String));
            //Dt_Productos.Columns.Add("Porcentaje_IEPS", typeof(String));
            //Dt_Productos.Columns.Add("Porcentaje_IVA", typeof(String));
            //Dt_Productos.Columns.Add("Monto_IEPS", typeof(String));
            //Dt_Productos.Columns.Add("Monto_IVA", typeof(String));
            //Dt_Productos.Columns.Add("Monto_Total", typeof(String));

            //foreach (GridViewRow Row in Grid_Productos_Servicios.Rows)
            //{
            //    DataRow Producto = Dt_Productos.NewRow();
            //    Producto["Ope_Com_Req_Producto_ID"] = Row.Cells[0].ToString().Trim();
            //    Producto["Prod_Serv_ID"] = Row.Cells[1].ToString().Trim();
            //    Producto["Nombre_Producto_Servicio"] = Row.Cells[2].ToString().Trim();
            //    Producto["Cantidad"] = Row.Cells[3].ToString().Trim();
            //    Producto["Precio_Unitario"] = Row.Cells[5].ToString().Trim();
            //    Producto["Monto"] = Row.Cells[6].ToString().Trim();
            //    Producto["Porcentaje_IEPS"] = Row.Cells[7].ToString().Trim();
            //    Producto["Porcentaje_IVA"] = Row.Cells[8].ToString().Trim();
            //    Producto["Monto_IEPS"] = Row.Cells[9].ToString().Trim();
            //    Producto["Monto_IVA"] = Row.Cells[10].ToString().Trim();
            //    Producto["Monto_Total"] = Row.Cells[11].ToString().Trim();
            //    Dt_Productos.Rows.Add(Producto);
            //    Dt_Productos.AcceptChanges();
            //}

            //Session["Dt_Prod_Ser"] = Dt_Productos;
            Llenar_Grid_Productos_Servicios(e.NewPageIndex);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Productos_Servicios_RowDataBound
        ///DESCRIPCIÓN          : Contro el pintado de cada renglon del Grid.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Productos_Servicios_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                TextBox Txt_Cantidad = (TextBox)e.Row.Cells[4].FindControl("Txt_Cantidad");
                Txt_Cantidad.Text = e.Row.Cells[3].Text;

                Label Lbl_Importe = (Label)e.Row.Cells[12].FindControl("Lbl_Importe");
                Lbl_Importe.Text = e.Row.Cells[6].Text;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Comentarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            Llenar_Grid_Comentarios(e.NewPageIndex);
        }

    #endregion

        protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
        {
            if (String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text) ||
                !String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Lbl_Mensaje_Error.Text = " + Es necesario Ingresar ambas fechas.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
            else
            {
                Llenar_Grid_Requisiciones(0);
            }
        }
}
