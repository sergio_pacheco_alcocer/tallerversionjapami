﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Seguimiento_Reservas.Negocio;
using JAPAMI.Autoriza_Ejercido.Negocio;
using JAPAMI.Polizas.Negocios;

public partial class paginas_Compras_Frm_Ope_Com_Seguimiento_Reservas : System.Web.UI.Page
{

    #region VARIABLES / CONSTANTES
    //objeto de la clase de negocio de dependencias para acceder a la clase de datos y realizar copnexion
    private Cls_Cat_Dependencias_Negocio Dependencia_Negocio;
    //objeto de la clase de negocio de Requisicion para acceder a la clase de datos y realizar copnexion
    //private Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio;
    //objeto en donde se guarda un id de producto o servicio para siempre tener referencia
    private int Contador_Columna;
    private String Informacion;

    private static String P_Dt_Productos_Servicios = "P_Dt_Productos_Servicios_Seguimiento_Compras1";
    //private static String P_Dt_Partidas = "P_Dt_Partidas";
    private static String P_Dt_Requisiciones = "P_Dt_Reservas";
    //private static String P_Dt_Productos_Servicios_Modal = "P_Dt_Productos_Servicios_Modal";

    private const String Operacion_Comprometer = "COMPROMETER";
    private const String Operacion_Descomprometer = "DESCOMPROMETER";
    private const String Operacion_Quitar_Renglon = "QUITAR";
    private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
    private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

    private const String SubFijo_Requisicion = "RQ-";
    private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
    private const String EST_GENERADA = "GENERADA";
    private const String EST_CANCELADA = "CANCELADA";
    private const String EST_REVISAR = "REVISAR";
    private const String EST_RECHAZADA = "RECHAZADA";
    private const String EST_AUTORIZADA = "AUTORIZADA";
    private const String EST_FILTRADA = "FILTRADA";

    private const String TIPO_STOCK = "STOCK";
    private const String TIPO_TRANSITORIA = "TRANSITORIA";

    private const String MODO_LISTADO = "LISTADO";
    private const String MODO_INICIAL = "INICIAL";
    private const String MODO_MODIFICAR = "MODIFICAR";
    private const String MODO_NUEVO = "NUEVO";

    #endregion

    #region PAGE LOAD / INIT
    protected void Page_Load(object sender, EventArgs e)
    {
        //Valores de primera vez
        if (!IsPostBack)
        {
            Cls_Sessiones.Mostrar_Menu = true;
            ViewState["SortDirection"] = "DESC";
            DateTime _DateTime = DateTime.Now;
            int dias = _DateTime.Day;
            dias = dias * -1;
            dias++;
            _DateTime = _DateTime.AddDays(dias);
            _DateTime = _DateTime.AddMonths(-1);
            //Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
            Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            //llenar combo dependencias
            //Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
            //DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
            //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia, Dt_Dependencias, 1, 0);
            //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia_Panel, Dt_Dependencias, 1, 0);
            //Cmb_Dependencia.SelectedIndex = 0;
            //Cmb_Dependencia_Panel.SelectedIndex = 0;
            //Cmb_Dependencia_Panel.Enabled = true;
            //Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
            //Cmb_Dependencia_Panel.Enabled = true;
            //Llenar_Combos_Busqueda();

            Llenar_Grid_Reservas();
            Habilitar_Controles(MODO_LISTADO);
            //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
            //DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
            //if (Dt_Grupo_Rol != null)
            //{
            //    String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
            //    if (Grupo_Rol == "00001" || Grupo_Rol == "00002")
            //    {
            //        Cmb_Dependencia_Panel.Enabled = true;
            //    }
            //    else
            //    {
            //        DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.P_Empleado_ID);
            //        if (Dt_URs.Rows.Count > 1)
            //        {
            //            Cmb_Dependencia_Panel.Enabled = true;
            //            Cls_Util.Llenar_Combo_Con_DataTable_Generico
            //                (Cmb_Dependencia_Panel, Dt_URs, 1, 0);
            //            Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
            //        }
            //    }
            //}
        }

        Mostrar_Informacion("", false);

    }
    #endregion

    #region EVENTOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Llenar_Combo(DropDownList Combo, String[] Items)
    {
        Combo.Items.Clear();
        Combo.Items.Add("<<SELECCIONAR>>");
        foreach (String _Item in Items)
        {
            Combo.Items.Add(_Item);
        }
        Combo.Items[0].Value = "0";
        Combo.Items[0].Selected = true;
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Habilitar_Controles(MODO_INICIAL);
        Llenar_Grid_Reservas();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Listar_Requisiciones_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Grid_Reservas();
       Habilitar_Controles(MODO_LISTADO);
    }
    #endregion

    #region EVENTOS GRID
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_Grid_Requisiciones_Seleccionar(String Dato)
    {
        Habilitar_Controles(MODO_INICIAL);
        //Llenar_Combos_Generales();
        Div_Listado_Requisiciones.Visible = false;
        Div_Contenido.Visible = true;

        String No_Reserva = Dato;
        DataRow[] Requisicion = ((DataTable)Session[P_Dt_Requisiciones]).Select("No_Reserva = '" + No_Reserva + "'");

        DataTable Dt_Historial = 
            Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Historial_Reservas
            (int.Parse(No_Reserva));
        Dt_Historial.DefaultView.Sort = "fecha Asc";
        Txt_Folio.Text = Dato;
        Grid_Productos_Servicios.DataSource = Dt_Historial;
        Grid_Productos_Servicios.DataBind();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Requisicion_Click(object sender, ImageClickEventArgs e)
    {
        String No_Requisicion = ((ImageButton)sender).CommandArgument;
        Evento_Grid_Requisiciones_Seleccionar(No_Requisicion);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Requisiciones.DataSource = ((DataTable)Session[P_Dt_Requisiciones]);
        Grid_Requisiciones.PageIndex = e.NewPageIndex;
        Grid_Requisiciones.DataBind();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Comentarios_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    #endregion
    #region METODOS
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Habilitar_Controles
    // DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 30/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO
    // CAUSA_MODIFICACIÓN   
    // *******************************************************************************/
    private void Habilitar_Controles(String Modo)
    {
        try
        {
            switch (Modo)
            {
                case MODO_LISTADO:
                    Btn_Salir.Visible = true;             
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    //Txt_Fecha.Enabled = false;
                    Txt_Folio.Enabled = false;
                    Cmb_Dependencia.Enabled = false;
                    break;
                case MODO_INICIAL:
                    Btn_Salir.Visible = false; 
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Listar_Requisiciones.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Txt_Folio.Enabled = false;
                    Cmb_Dependencia.Enabled = false;
                    break;
                //Estado de Nuevo
                case MODO_NUEVO:
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Salir.Visible = true;
                    Cmb_Dependencia.Enabled = false;
                    break;
                //Estado de Modificar
                case MODO_MODIFICAR:
                    Btn_Listar_Requisiciones.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Cmb_Dependencia.Enabled = false;
                    break;
                default: break;
            }
        }
        catch (Exception ex)
        {
            Mostrar_Informacion(ex.ToString(), true);
        }
    }
    //*******************************************************************************
    //NOMBRE DE LA FUNCIÓN: Mostrar_Información
    //DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
    //RETORNA: 
    //CREO: Gustavo Angeles Cruz
    //FECHA_CREO: 24/Agosto/2010 
    //MODIFICO:
    //FECHA_MODIFICO:
    //CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private void Mostrar_Informacion(String txt, Boolean mostrar)
    {
        Lbl_Informacion.Visible = mostrar;
        Img_Warning.Visible = mostrar;
        Lbl_Informacion.Text = txt;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Reservas
    // DESCRIPCIÓN: Llena el grid principal de requisiciones
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: Diciembre/2010 
    // MODIFICOHabili

    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Reservas()
    {
        Div_Contenido.Visible = false;
        Div_Listado_Requisiciones.Visible = true;
        String No_Reserva = "";
        int Int_No_Reserva = 0;
        if (Txt_Busqueda.Text.Trim().Length > 0)
        {
            No_Reserva = Txt_Busqueda.Text;            
            try
            {
                Int_No_Reserva = int.Parse(No_Reserva);
            }
            catch (Exception Ex)
            {
                String Str = Ex.ToString();
                Int_No_Reserva = 0;
            }
        }
        //Requisicion_Negocio.P_Estatus = EST_AUTORIZADA;
        //Session[P_Dt_Requisiciones] = Requisicion_Negocio.Consultar_Requisiciones_Generales();
        String Empleado_ID ="";
        String Proveedor_ID = "";
        String Requisicion = "";
        String Orden_Compra = "";
        if (Cmb_Empleado.SelectedIndex > 0)
        {
            Empleado_ID = Cmb_Empleado.SelectedValue;
        }
        if (Cmb_Proveedor.SelectedIndex > 0)
        {
            Proveedor_ID = Cmb_Proveedor.SelectedValue;
        }
        if (!String.IsNullOrEmpty(Txt_No_Requisicion.Text))
        {
            Requisicion = Txt_No_Requisicion.Text;
            Session[P_Dt_Requisiciones] =
            Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Reservas_De_Requisicion(Requisicion);//Requisicion_Negocio.Consultar_Requisiciones_Generales();
        }
        else
        {
            if (!String.IsNullOrEmpty(Txt_Orden_Compra.Text))
            {
                Orden_Compra = Txt_Orden_Compra.Text;
                Session[P_Dt_Requisiciones] =
                    Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Reservas_De_Compras(Orden_Compra);//Requisicion_Negocio.Consultar_Requisiciones_Generales();
            }
            else
            {
                Session[P_Dt_Requisiciones] =
                   Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Reservas(String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicial.Text)), String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text)), Int_No_Reserva, Empleado_ID, Proveedor_ID);//Requisicion_Negocio.Consultar_Requisiciones_Generales();
            }
        }
        if (Session[P_Dt_Requisiciones] != null && ((DataTable)Session[P_Dt_Requisiciones]).Rows.Count > 0)
        {
            Div_Contenido.Visible = false;
            Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
            Grid_Requisiciones.DataBind();
        }
        else
        {
            //Mostrar_Informacion("No se encontraron requisiciones con los criterios de búsqueda", true);
            Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
            Grid_Requisiciones.DataBind();
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: Diciembre/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combos_Generales()
    {
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Busqueda()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: Diciembre/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combos_Busqueda()
    {
        Cmb_Tipo_Busqueda.Items.Clear();
        Cmb_Tipo_Busqueda.Items.Add("TODOS");
        Cmb_Tipo_Busqueda.Items.Add(TIPO_TRANSITORIA);
        Cmb_Tipo_Busqueda.Items.Add(TIPO_STOCK);
        Cmb_Tipo_Busqueda.Items[0].Selected = true;
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Generar_Tabla_Informacion
    // DESCRIPCIÓN: Crea una tabla con la informacion que se requiere ingresar al formulario
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private void Generar_Tabla_Informacion()
    {
        Contador_Columna = Contador_Columna + 1;
        if (Contador_Columna > 2)
        {
            Contador_Columna = 0;
            Informacion += "</tr><tr>";
        }
    }
    #endregion
    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
   
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
    protected void Chk_Empleado_CheckedChanged(object sender, EventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";

        if (Chk_Empleado.Checked == true)
        {
            Div_Busqueda_Empleados.Visible = true;
            Txt_Nombre_Empleado.Text = "";
            Txt_No_Empleado.Text = "";
            //limpiamos la seleccion de proveedores
            Chk_Proveedor.Checked = false;
            Cmb_Proveedor.Items.Clear();
            Div_Busqueda_Proveedores.Visible = false;
            Cmb_Proveedor.Enabled = false;
            Chk_Requisicion.Checked = false;
            Txt_No_Requisicion.Enabled = false;
            Txt_No_Requisicion.Text = "";
            Chk_Orden_Compra.Checked = false;
            Txt_Orden_Compra.Text = "";
            Txt_Orden_Compra.Enabled = false;
        }
        else
        {
            Cmb_Empleado.SelectedIndex = -1;
            Llenar_Grid_Reservas();
            Cmb_Empleado.Enabled = false;
            Div_Busqueda_Empleados.Visible = false;
            Txt_Nombre_Empleado.Text = "";
            Txt_No_Empleado.Text = "";
        }
    }
    protected void Chk_Proveedor_CheckedChanged(object sender, EventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";
        if (Chk_Proveedor.Checked == true)
        {
            Div_Busqueda_Proveedores.Visible = true;
            Txt_Proveedor_Nombre.Text = "";
            Txt_Padron_Prov.Text = "";
            //limpiamos la seleccion de empleado
            Chk_Empleado.Checked = false;
            Chk_Requisicion.Checked = false;
            Txt_No_Requisicion.Text = "";
            Txt_No_Requisicion.Enabled = false;
            Chk_Orden_Compra.Checked = false;
            Txt_Orden_Compra.Text = "";
            Txt_Orden_Compra.Enabled = false;
            Cmb_Empleado.Items.Clear();
            Div_Busqueda_Empleados.Visible = false;
            Cmb_Empleado.Enabled = false;
        }
        else
        {
            Cmb_Proveedor.SelectedIndex = -1;
            Llenar_Grid_Reservas();
            Cmb_Proveedor.Enabled = false;
            Div_Busqueda_Proveedores.Visible = false;
            Txt_Proveedor_Nombre.Text = "";
            Txt_Padron_Prov.Text = "";
        }
    }
    protected void Chk_Requisicion_CheckedChanged(object sender, EventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";
        if (Chk_Requisicion.Checked == true)
        {
            Txt_No_Requisicion.Enabled = true;
            Txt_No_Requisicion.Text = "";
            Txt_No_Requisicion.Focus();
            Chk_Empleado.Checked = false;
            Cmb_Empleado.Items.Clear(); 
            Chk_Proveedor.Checked = false;
            Cmb_Proveedor.Items.Clear();
            Chk_Orden_Compra.Checked = false;
            Txt_Orden_Compra.Enabled = false;
            Div_Busqueda_Proveedores.Visible = false;
            Div_Busqueda_Empleados.Visible = false;
        }
        else
        {
            Txt_No_Requisicion.Enabled = false;
            Txt_No_Requisicion.Text = "";
        }
    }
    protected void Chk_Orden_Compra_CheckedChanged(object sender, EventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";
        if (Chk_Orden_Compra.Checked == true)
        {
            Txt_Orden_Compra.Enabled = true;
            Txt_Orden_Compra.Text = "";
            Txt_Orden_Compra.Focus();
            Chk_Empleado.Checked = false;
            Cmb_Empleado.Items.Clear();
            Chk_Proveedor.Checked = false;
            Cmb_Proveedor.Items.Clear();
            Chk_Requisicion.Checked = false;
            Txt_No_Requisicion.Text = "";
            Txt_No_Requisicion.Enabled = false;
            Div_Busqueda_Proveedores.Visible = false;
            Div_Busqueda_Empleados.Visible = false;
        }
        else
        {
            Txt_No_Requisicion.Enabled = false;
            Txt_No_Requisicion.Text = "";
        }
    }
    protected void Btn_Buscar_Prov_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";
        if (Txt_Proveedor_Nombre.Text.Trim() == String.Empty && Txt_Padron_Prov.Text.Trim() == String.Empty)
        {
            Lbl_Informacion.Visible = true;
            Img_Warning.Visible = true;
            Lbl_Informacion.Text = "Es necesario indicar un Padron de proveedor o razon social del proveedor a buscar";

        }
        else
        {
            Cls_Ope_Com_Seguimiento_Reservas_Negocio Clase_Negocio = new Cls_Ope_Com_Seguimiento_Reservas_Negocio();
            if(Txt_Padron_Prov.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Proveedor_ID = Txt_Padron_Prov.Text.Trim();
            }
            if(Txt_Proveedor_Nombre.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Razon_Social = Txt_Proveedor_Nombre.Text.Trim();
            }
            DataTable Dt_Proveedor = Clase_Negocio.Consultar_Proveedor();
            if (Dt_Proveedor.Rows.Count > 0)
            {
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Proveedor, Dt_Proveedor);
                Cmb_Proveedor.Enabled = true;
                Div_Busqueda_Proveedores.Visible = false;
            }
            else
            {
                Lbl_Informacion.Visible = true;
                Img_Warning.Visible = true;
                Lbl_Informacion.Text = "No se encontro ningun proveedor";
            }
        }
    }
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String fecha = "";
        String Mes_Anio = "";
        if (((LinkButton)sender).CommandArgument != "")
        {
             fecha = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((LinkButton)sender).CommandArgument));
        } 
        if (((LinkButton)sender).ToolTip != "")
        {
            Mes_Anio = ((LinkButton)sender).ToolTip;
        }
        String No_poliza = ((LinkButton)sender).Text;
        String Tipo_Poliza = ((LinkButton)sender).CssClass;
        String Reserva = Txt_Folio.Text;
        Imprimir(No_poliza, Tipo_Poliza, fecha, Reserva,Mes_Anio);
    }
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA, String Reserva,String Mes_Anio)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        String Mes = "";
        String Ano = "";
        String Solicitud = "";
        DateTime Fecha_Poliza;
        DataTable Dt_Beneficiario = new DataTable();
        DataTable Dt_Solicitud = new DataTable();
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Ejercido = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
        //Cls_Ope_Con_Polizas_Negocio Rs_No_Solicitud = new Cls_Ope_Con_Polizas_Negocio();
        try
        {
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            if (FECHA != "")
            {
                Fecha_Poliza = Convert.ToDateTime(FECHA);
                Mes = String.Format("{0:00}", (Fecha_Poliza.Month));
                Ano = Convert.ToString(Fecha_Poliza.Year).Substring(2, 2);
                Poliza.P_Mes_Ano = Mes + Ano;
            }
            else
            {
                Poliza.P_Mes_Ano = Mes_Anio;
            }
            Poliza.P_No_Reserva = Reserva;
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Dt_Solicitud = Poliza.Consulta_Solicitud_Pago();
            if (Dt_Solicitud.Rows.Count > 0)
            {
                Solicitud = Dt_Solicitud.Rows[0]["NO_SOLICITUD_PAGO"].ToString();
            }
            else
            {
                Dt_Solicitud = Poliza.Consulta_Pago();
                if (Dt_Solicitud.Rows.Count > 0)
                {
                    Solicitud = Dt_Solicitud.Rows[0]["SOLICITUD"].ToString();
                }
            }
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            //consulta el proveedor
            Rs_Ejercido.P_No_Solicitud_Pago = Solicitud;
            Dt_Beneficiario = Rs_Ejercido.Consulta_Solicitud_Pago_Completa();
            if (Dt_Pagos.Rows.Count > 0)
            {
                foreach (DataRow registro in Dt_Pagos.Rows)
                {

                    if (Dt_Beneficiario.Rows.Count > 0)
                    {
                        registro.BeginEdit();
                        registro["BENEFICIARIO"] = Dt_Beneficiario.Rows[0]["COMPANIA"].ToString();
                        registro.EndEdit();
                        registro.AcceptChanges();
                        Dt_Pagos.AcceptChanges();
                    }
                }
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Txt_Busqueda_TextChanged
    ///DESCRIPCIÓN: Metodo al presionar enter 
    ///PARAMETROS: 
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/08/12 
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
    {
        if (!String.IsNullOrEmpty(Txt_Busqueda.Text))
        {
            Habilitar_Controles(MODO_INICIAL);
            Llenar_Grid_Reservas();
        }
        else
        {
            if (!String.IsNullOrEmpty(Txt_No_Requisicion.Text))
            {
                Habilitar_Controles(MODO_INICIAL);
                Llenar_Grid_Reservas();
            }
            else
            {
                if (!String.IsNullOrEmpty(Txt_Orden_Compra.Text))
                {
                    Habilitar_Controles(MODO_INICIAL);
                    Llenar_Grid_Reservas();
                }
            }
        }
    }
    protected void Btn_Buscar_empleado_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Informacion.Visible = false;
        Img_Warning.Visible = false;
        Lbl_Informacion.Text = "";
        if (Txt_Nombre_Empleado.Text.Trim() == String.Empty && Txt_No_Empleado.Text.Trim() == String.Empty)
        {
            Lbl_Informacion.Visible = true;
            Img_Warning.Visible = true;
            Lbl_Informacion.Text = "Es necesario indicar un Num. de Empleado o Nombre";

        }
        else
        {
            Cls_Ope_Com_Seguimiento_Reservas_Negocio Clase_Negocio = new Cls_Ope_Com_Seguimiento_Reservas_Negocio();
            if (Txt_No_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_No_Empledo = String.Format("{0:000000}",Convert.ToDouble(Txt_No_Empleado.Text.Trim()));
            }
            if (Txt_Nombre_Empleado.Text.Trim() != String.Empty)
            {
                Clase_Negocio.P_Nombre_Empleado = Txt_Nombre_Empleado.Text.Trim();
            }
            DataTable Dt_Empleado = Clase_Negocio.Consultar_Empleado();
            if (Dt_Empleado.Rows.Count > 0)
            {
                Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Empleado, Dt_Empleado);
                Cmb_Empleado.Enabled = true;
                Div_Busqueda_Empleados.Visible = false;
            }
            else
            {
                Lbl_Informacion.Visible = true;
                Img_Warning.Visible = true;
                Lbl_Informacion.Text = "No se encontro ningun Empleado";
            }


        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Cmb_Proveedor_SelectedIndexChanged
    // DESCRIPCIÓN: evento del combo dropdownlist    
    // RETORNA: 
    // CREO: Luis Daniel Guzmán Malagón
    // FECHA_CREO: 02/Agosto/2012 
    // MODIFICO:
    // FECHA_MODIFICO
    // CAUSA_MODIFICACIÓN   
    // *******************************************************************************
    protected void Cmb_Proveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        Llenar_Grid_Reservas();
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Cmb_Dependencias_Panel_SelectedIndexChanged
    // DESCRIPCIÓN: evento del combo dropdownlist    
    // RETORNA: 
    // CREO: Luis Daniel Guzmán Malagón
    // FECHA_CREO: 02/Agosto/2012 
    // MODIFICO:
    // FECHA_MODIFICO
    // CAUSA_MODIFICACIÓN   
    // *******************************************************************************
    protected void Cmb_Empleado_SelectedIndexChanged(object sender, EventArgs e)
    {
        Llenar_Grid_Reservas();
    }
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar+Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}
