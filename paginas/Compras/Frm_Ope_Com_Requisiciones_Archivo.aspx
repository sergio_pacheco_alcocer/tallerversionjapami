﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Requisiciones_Archivo.aspx.cs" 
    Inherits="paginas_Compras_Frm_Ope_Com_Requisiciones_Archivo" Culture="es-MX" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
   
   
   <style type="text/css">
        a:link {text-decoration:none; color: Green;} /* Link no visitado*/
        a:visited {text-decoration:none; color:Green;} /*Link visitado*/
        a:active {text-decoration:none; color:Green;} /*Link activo*/
        a:hover {text-decoration:underline; color:Green;} /*Mause sobre el link*/
   </style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <!--Div del encabezado-->
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo">Archivos de Requisiciones</td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" 
                                    onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    CssClass="Img_Button" AlternateText="Eliminar" ToolTip="Eliminar" 
                                    OnClientClick="return confirm('¿Esta seguro de Eliminar el presente registro?');" 
                                    onclick="Btn_Eliminar_Click" />
                                <asp:ImageButton ID="Btn_Cargar" runat="server" CssClass="Img_Button" CausesValidation="false"
                                    ImageUrl="~/paginas/imagenes/paginas/subir.png" ToolTip="Cargar Archivo" OnClientClick="javascript:return Abrir_Carga_PopUp();" 
                                    onclick="Btn_Cargar_Click" />
                                <cc1:ModalPopupExtender ID="MPE_Archivos_Requisicion" runat="server" BackgroundCssClass="popUpStyle" BehaviorID="Archivos_Requisicion" PopupControlID="Pnl_Seleccionar_Archivo" TargetControlID="Btn_Open" CancelControlID="Btn_Cerrar" DropShadow="true" DynamicServicePath="" Enabled="true"></cc1:ModalPopupExtender>
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" onclick="Btn_Salir_Click" />
                                <asp:Button ID="Btn_Open" runat="server" Text="" style="background-color: transparent; border-style:none; visibility:hidden" />
                                <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
                <!--Div de las requisiciones-->
                <asp:HiddenField ID="Hdf_Estatus_Requisicion" runat="server" />
                <div id="Div_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                Unidad Responsable
                            </td>
                            <td colspan="4">
                                <asp:DropDownList ID="Cmb_Dependencia_Panel" runat="server" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:filteredtextboxextender id="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server"
                                    targetcontrolid="Txt_Fecha_Inicial" filtertype="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    validchars="/_" />
                                <cc1:calendarextender id="Txt_Fecha_Inicial_CalendarExtender" runat="server" targetcontrolid="Txt_Fecha_Inicial"
                                    popupbuttonid="Btn_Fecha_Inicial" format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:calendarextender id="CalendarExtender3" runat="server" targetcontrolid="Txt_Fecha_Final"
                                    popupbuttonid="Btn_Fecha_Final" format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td style="width: 15%;">
                                Tipo
                            </td>
                            <td>
                                <asp:DropDownList ID="Cmb_Tipo_Busqueda" runat="server" Width="100%" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estatus
                            </td>
                            <td>
                                <asp:DropDownList ID="Cmb_Estatus_Busqueda" runat="server" Width="98%" />
                            </td>
                            <td>
                                Folio
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="85%" MaxLength="13" AutoPostBack="true"
                                    ontextchanged="Txt_Busqueda_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar" 
                                    onclick="Btn_Buscar_Click" />
                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender1" runat="server" targetcontrolid="Txt_Busqueda"
                                    watermarktext="&lt;RQ-000000&gt;" watermarkcssclass="watermarked">
                                </cc1:textboxwatermarkextender>
                                <cc1:filteredtextboxextender id="FTBE_Txt_Busqueda" runat="server" filtertype="Custom"
                                    targetcontrolid="Txt_Busqueda" validchars="rRqQ-0123456789">
                                </cc1:filteredtextboxextender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                                <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; border-style: outset;
                                    border-color: Silver;">
                                    <asp:GridView ID="Grid_Requisiciones" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="GridView_1" GridLines="None" Width="100%" 
                                        DataKeyNames="No_Requisicion" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="Lista de Requisiciones" 
                                        onpageindexchanging="Grid_Requisiciones_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Seleccionar_Requisicion" runat="server" OnClick="Btn_Seleccionar_Requisicion_Click" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" CommandArgument='<%# Eval("No_Requisicion") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="Folio" HeaderText="Folio" Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha Inicial" DataFormatString="{0:dd/MMM/yyyy}"
                                                Visible="True" SortExpression="Fecha_Creo">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE_DEPENDENCIA" HeaderText="Unidad Responsable" Visible="True"
                                                SortExpression="NOMBRE_DEPENDENCIA">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipo" HeaderText="Tipo" Visible="True">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="12%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="16%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="No_Requisicion" HeaderText="ID" Visible="true">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>                
                </div>
                <!--Div del contenido-->
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="2" align="center">
                                <asp:Label ID="Lbl_Archivos_Requision" runat="server" Font-Bold="true"></asp:Label>
                                <asp:HiddenField ID="Txt_No_Requisicion" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left" colspan="2">
                                <asp:GridView ID="Grid_Archivos_Requisiciones" runat="server" 
                                    AutoGenerateColumns="false" CssClass="GridView_1" AllowPaging="True" 
                                    PageSize="100" GridLines="None" Width="100%" 
                                    onrowdatabound="Grid_Archivos_Requisiciones_RowDataBound">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Seleccionar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/delete.png" CommandArgument='<%# Eval("No_Archivo") %>' OnClick="Btn_Seleccionar_Archivo_Click" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="No_Archivo" HeaderText="No Archivo" />
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                        <asp:BoundField DataField="Nombre_Archivo" HeaderText="Archivo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                            <HeaderStyle HorizontalAlign="Center" Width="35%" />
                                            <ItemStyle HorizontalAlign="Left" Width="35%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Usuario_Creo" HeaderText="Agregado por" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha/Hora" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" DataFormatString="{0:dd/MMM/yyyy hh:mm:ss tt}">
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Estatus_Requisicion" HeaderText="Estatus RQ" >
                                            <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                            <ItemStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Archivo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="Hyp_Lnk_Archivo" runat="server" style=""></asp:HyperLink >                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Original" HeaderText="Original" />
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!--Modal para la seleccion del archivo-->
        <asp:Panel ID="Pnl_Seleccionar_Archivo" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
            <asp:Panel ID="Pnl_Cabecera_Seleccionar_Archivo" runat="server">
                <table width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;"><asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />Seleccionar el Archivo para la Requisic&oacute;n</td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="color: #5D7B9D">
                <table width="100%">
                    <tr>
                        <td align="left" style="text-align:left;">
                            <asp:UpdatePanel ID="Upnl_Archivos_Requisicion" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="Progress_Upnl_Archivos_Requisicion" runat="server" AssociatedUpdatePanelID="Upnl_Archivos_Requisicion" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>                                        
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" style="width:100%;"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;font-size:11px;">
                                                <cc1:AsyncFileUpload ID="AFil_Archivo" runat="server" ThrobberID="Lbl_Throbber" 
                                                    onuploadedcomplete="AFil_Archivo_UploadedComplete" />
                                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                    <div  class="processMessage" id="div_progress">
                                                        <center>
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                            <br /><br />
                                                            <span id="spanUploading" runat="server" style="color:White;font-size:30px;font-weight:bold;font-family:Lucida Calligraphy;font-style:italic;">
                                                                Cargando...
                                                            </span>
                                                        </center>
                                                    </div>                                                
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">Comentarios<br />                                           
                                                <asp:TextBox ID="Txt_Comentarios_Archivo" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    watermarktext="&lt;Límite de Caracteres 500&gt;" watermarkcssclass="watermarked">
                                                    </cc1:textboxwatermarkextender>
                                                <cc1:filteredtextboxextender id="FTE_Justificacion" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    filtertype="Custom, UppercaseLetters, LowercaseLetters, Numbers" validchars="ÑñáéíóúÁÉÍÓÚ./*-!$%&()=,[]{}+<>@?¡?¿# ">
                                                    </cc1:filteredtextboxextender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width:100%;text-align:left;">
                                                <center>
                                                    <asp:Button ID="Btn_Archivos_Requisicion" runat="server" Text="Aceptar" 
                                                        CssClass="button" CausesValidation="false" Width="200px" 
                                                        onclick="Btn_Archivos_Requisicion_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="Btn_Archivos_Requisicion" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

