﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Correo.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Catalogo_SAP_Conceptos.Negocio;
using System.Text.RegularExpressions;

public partial class paginas_Compras_Frm_Cat_Com_Correo : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Grid_Cotizador = 2;
        private const int Const_Estado_Modificar = 3;

        private static DataTable Dt_Correo = new DataTable();    
    #endregion

    #region Page Load / Init

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            
            if (!Page.IsPostBack)
            {
                Session["Activa"] = true;
                Estado_Botones(Const_Estado_Inicial);

                Cargar_Grid_Correo(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
        
    }

    #endregion
    
    #region Metodos

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Mensaje_Error
        ///DESCRIPCION : Muestra el error
        ///PARAMETROS  : P_Texto: texto de un TextBox
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 15/Enero/2012
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Mensaje_Error(String P_Mensaje)
        {
            Img_Error.Visible = true;
            Lbl_Error.Text += P_Mensaje + "</br>";
            Div_Contenedor_error.Visible = true;
        }
        private void Mensaje_Error()
        {
            Img_Error.Visible = false;
            Lbl_Error.Text = "";
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Estado_Botones
        ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
        ///PARAMETROS: P_Estado: El estado de la pagina
        ///CREO: David herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Estado_Botones(int P_Estado)
        {
            switch (P_Estado)
            {
                case 0: //Estado inicial                    

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Correo.Enabled = true;

                    Txt_Correo.Text = "";
                    Txt_Password.Text = "";
                    Txt_Password.Attributes.Add("value", Txt_Password.Text);
                    Txt_Direccion_IP.Text = "";
                    Grid_Correo.Enabled = true;
                    Grid_Correo.SelectedIndex = (-1);

                    Txt_Correo.Enabled = false;
                    Txt_Password.Enabled = false;
                    Txt_Direccion_IP.Enabled = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Inicio";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Btn_Modificar.Enabled = false;
                    Btn_Nuevo.Enabled = true;
                    Btn_Salir.Enabled = true;

                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;

                    Configuracion_Acceso("Frm_Cat_Com_Correo.aspx");
                    break;

                case 1: //Nuevo                    

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Correo.Enabled = false;

                    Txt_Correo.Text = "";
                    Txt_Password.Text = "";
                    Txt_Direccion_IP.Text = "";

                    Txt_Correo.Enabled = true;
                    Txt_Password.Enabled = true;
                    Txt_Direccion_IP.Enabled = true;

                    Btn_Modificar.Visible = false;

                    Grid_Correo.SelectedIndex = (-1);
                    Grid_Correo.Enabled = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";

                    break;

                case 2: //Grid Cotizador

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Correo.Enabled = true;

                    Btn_Modificar.Visible = true;
                    Btn_Modificar.Enabled = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Listado";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Listado";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";


                    break;

                case 3: //Modificar

                    Div_Listado_Cotizadores.Visible = true;
                    Grid_Correo.Enabled = false;
                    Div_Datos_Cotizador.Visible = true;

                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;

                    Txt_Correo.Enabled = true;
                    Txt_Password.Enabled = true;
                    Txt_Direccion_IP.Enabled = true;

                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_Nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                    break;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar_Correo
        ///DESCRIPCIÓN: se actualizan los datos del correo seleccionado
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************

        private void Modificar_Correo(Boolean Bln_Nuevo_Correo)
        {
            Div_Contenedor_error.Visible = false;
            Lbl_Error.Text = "";
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();
                Validar_Cajas();
                Validar_Email();
                Validar_IP();
                Txt_Password.Attributes.Add("value", Txt_Password.Text);
                if (Div_Contenedor_error.Visible == false)
                {
                    Correo_Negocio.P_Correo = Txt_Correo.Text.Trim();
                    Correo_Negocio.P_Password = Txt_Password.Text.Trim();
                    Correo_Negocio.P_Direccion_IP = Txt_Direccion_IP.Text.Trim();


                    Correo_Negocio.Modificar_Correo();
                    if (!Bln_Nuevo_Correo)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('La Modificacion del correo fue Exitosa');", true);
                        Estado_Botones(Const_Estado_Inicial);
                        Cargar_Grid_Correo(0);
                    }


                }//Fin del if Div_Contenedor_error.Visible == false
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }//fin de Modificar  

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
        ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
        ///PARAMETROS: Page_Index: Numero de pagina del grid
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Cargar_Grid_Correo(int Page_Index)
        {
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();

                Dt_Correo = Correo_Negocio.Consulta_Correo();
                Grid_Correo.PageIndex = Page_Index;
                Grid_Correo.DataSource = Dt_Correo;
                Grid_Correo.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Cajas
        ///DESCRIPCIÓN: Metodo que valida que las cajas tengan contenido. 
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Validar_Cajas()
        {
            if (Txt_Correo.Text.Trim() == String.Empty)
            {
                Lbl_Error.Text += "El Usuario de Correo es obligatorio <br />";
                Div_Contenedor_error.Visible = true;
            }
            if (Txt_Password.Text.Trim() == String.Empty)
            {
                Lbl_Error.Text += "El Password de Correo es obligatorio <br />";
                Div_Contenedor_error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Email
        ///DESCRIPCIÓN: 
        ///PARAMETROS: Metodo que permite validar el correo ingresado por el usuario
        ///CREO: Susana David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Validar_Email()
        {

            Regex Exp_Regular = new Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
            Match Comparar = Exp_Regular.Match(Txt_Correo.Text);

            if (!Comparar.Success)
            {
                Lbl_Error.Text += "+ El contenido del Correo es incorrecto <br />";
                Div_Contenedor_error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_IP
        ///DESCRIPCIÓN: Valida la direccion Ip
        ///PARAMETROS: Metodo que permite validar el correo ingresado por el usuario
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public void Validar_IP()
        {
            Regex Mascara_IP = new Regex(@"^([1-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5])){2}(\.([0-9]|[1-9][0-9]|1[0-9][0-9]|2[0-4][0-9]|25[0-5]))$");

            Match Comparar = Mascara_IP.Match(Txt_Direccion_IP.Text);

            if (!Comparar.Success)
            {
                Lbl_Error.Text += "+ El contenido de la Direccion IP es incorrecto <br />";
                Div_Contenedor_error.Visible = true;
            }
        }        

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();

                if (Btn_Nuevo.AlternateText == "Nuevo")
                {
                    //Valida que no exista una cuenta de correo
                    if (Grid_Correo.Rows.Count > 0)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('Ya existe un correo.');", true);                 
                    else
                        Estado_Botones(Const_Estado_Nuevo);
                }
                else if (Btn_Nuevo.AlternateText == "Dar de Alta")
                {
                    Div_Contenedor_error.Visible = false;
                    Validar_Cajas();
                    Validar_Email();
                    Validar_IP();
                    Txt_Password.Attributes.Add("value", Txt_Password.Text);
                    //Si pasa todas las Validaciones damos de alta el cotizador
                    if (Div_Contenedor_error.Visible == false)
                    {
                        try
                        {                            
                            Correo_Negocio.P_Correo = Txt_Correo.Text.Trim();
                            Correo_Negocio.P_Password = Txt_Password.Text.Trim();
                            Correo_Negocio.P_Direccion_IP = Txt_Direccion_IP.Text.Trim();
                        
                            Correo_Negocio.Alta_Correo();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('El alta del correo fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid_Correo(0);
                        }
                        catch
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('El alta del correo no fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid_Correo(0);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Evento del Boton Modificar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Modificar_Correo(false);
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Salir_Click
        ///DESCRIPCION:             Boton para SALIR
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO:             15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.AlternateText == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");

            }
            else
            {
                Estado_Botones(Const_Estado_Inicial);
                Txt_Correo.Text = "";
                Txt_Password.Text = "";
                Txt_Password.Attributes.Add("value", Txt_Password.Text);
                Txt_Direccion_IP.Text = "";
            }

        }        

    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Correo_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Correo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Estado_Botones(Const_Grid_Cotizador);

                //Realizamos la consulta para modificar el cotizador
                Cls_Cat_Com_Correo_Negocio Clase_Negocio = new Cls_Cat_Com_Correo_Negocio();

                DataTable Dt_Datos_Cotizador = Clase_Negocio.Consulta_Correo();

                if (Dt_Datos_Cotizador.Rows.Count != 0)
                {
                    Txt_Correo.Text = Dt_Datos_Cotizador.Rows[0]["CORREO_SALIENTE"].ToString().Trim();
                    Txt_Password.Text = Dt_Datos_Cotizador.Rows[0]["PASSWORD_CORREO"].ToString().Trim();
                    Txt_Password.Attributes.Add("value", Txt_Password.Text);
                    Txt_Direccion_IP.Text = Dt_Datos_Cotizador.Rows[0]["SERVIDOR_CORREO"].ToString().Trim();
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Correo_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Correo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Correo.SelectedIndex = (-1);
                Cargar_Grid_Correo(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

    #endregion   

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:       Configuracion_Acceso
    /// DESCRIPCIÓN:  Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:   URL_Pagina: Nombre de la pagina
    /// USUARIO CREÓ: David Herrera Rincon
    /// FECHA CREÓ:   15/Enero/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);            

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 15/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
   
}
