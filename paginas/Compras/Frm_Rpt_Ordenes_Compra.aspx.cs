﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using CarlosAg.ExcelXmlWriter;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Productos_Almacen.Negocio;
using JAPAMI.Reporte_Ordenes_Compra.Negocio;

public partial class paginas_Compras_Frm_Rpt_Ordenes_Compra : System.Web.UI.Page
{
    #region Variables Locales
        private const String P_Dt_Ordenes_Compra = "Dt_Ordenes_Compra";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Estado_Inicial();
            }
            else
            {
                Mostrar_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error("Error: (Page_Load) " + ex.ToString(), true);
        }
    }
    #endregion

    #region Metodos
        private void Mostrar_Error(String Mensaje, Boolean Mostrar)
        {
            try
            {
                Lbl_Informacion.Text = Mensaje;
                Div_Contenedor_Msj_Error.Visible = Mostrar;
            }
            catch (Exception ex)
            {
                Lbl_Informacion.Text = "Error: (Mostrar_Error)" + ex.ToString();
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        private void Estado_Inicial()
        {
            try
            {
                Llena_Combo_Impuestos();
                Llena_Combo_Unidades();
                Llena_Combo_Tipos();
                Llena_Combo_Partidas_Genericas();

                Elimina_Sesiones();

                Habilitar_Controles(false);
                Lbl_Tipo_Busqueda.Visible = false;
                Txt_Busqueda.Visible = false;
                Lbl_Proveedor.Visible = false;
                Cmb_Proveedores.Visible = false;
                Btn_Busqueda_Proveedor.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Elimina_Sesiones
        ///DESCRIPCION:             Limpiar los controles de la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              09/Abril/2012 13:59
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Elimina_Sesiones()
        {
            try
            {
                //Eliminar las sesiones
                HttpContext.Current.Session.Remove(P_Dt_Ordenes_Compra);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Limpiar_Controles
        ///DESCRIPCION:             Limpiar los controles de la pagina
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:42
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Txt_Busqueda.Text = "";
                Cmb_Impuestos.SelectedIndex = 0;
                Cmb_Partidas_Especificas.Items.Clear();
                Cmb_Partidas_Genericas.SelectedIndex = 0;
                Cmb_Stock.SelectedIndex = 0;
                Cmb_Tipo_Reporte.SelectedIndex = 0;
                Cmb_Tipos.SelectedIndex = 0;
                Cmb_Unidades.SelectedIndex = 0;
                Cmb_Tipo_Busqueda.SelectedIndex = 0;
                Cmb_Estatus.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Unidades
        ///DESCRIPCION:             Llenar el combo de las unidades
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Unidades()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Unidades = new DataTable(); //variable para la tabla

            try
            {
                //Asignar propiedades
                Dt_Unidades = Productos_Negocio.Consulta_Unidades();
                Cmb_Unidades.Items.Clear();
                Cmb_Unidades.DataSource = Dt_Unidades;
                Cmb_Unidades.DataTextField = "Nombre";
                Cmb_Unidades.DataValueField = "Unidad_ID";
                Cmb_Unidades.DataBind();
                Cmb_Unidades.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Unidades.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Unidades.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Impuestos
        ///DESCRIPCION:             Llenar el combo de los impuestos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Impuestos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Impuestos = new DataTable(); //Tabla para los impuestos

            try
            {
                //Asignar propiedades
                Dt_Impuestos = Productos_Negocio.Consulta_Impuestos();
                Cmb_Impuestos.Items.Clear();
                Cmb_Impuestos.DataSource = Dt_Impuestos;
                Cmb_Impuestos.DataTextField = "Nombre";
                Cmb_Impuestos.DataValueField = "Impuesto_ID";
                Cmb_Impuestos.DataBind();
                Cmb_Impuestos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Impuestos.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Impuestos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Tipos
        ///DESCRIPCION:             Llenar el combo de los tipos
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Tipos()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Tipos = new DataTable(); //Tabla para los tipos

            try
            {
                //Asignar propiedades
                Dt_Tipos = Productos_Negocio.Consulta_Tipos();
                Cmb_Tipos.Items.Clear();
                Cmb_Tipos.DataSource = Dt_Tipos;
                Cmb_Tipos.DataTextField = "Tipo";
                Cmb_Tipos.DataValueField = "Tipo";
                Cmb_Tipos.DataBind();
                Cmb_Tipos.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Tipos.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Genericas
        ///DESCRIPCION:             Llenar el combo de las partidas genericas
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Genericas()
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Genericas = new DataTable(); //Tabla para las partidas genericas

            try
            {
                //Asignar propiedades
                Dt_Partidas_Genericas = Productos_Negocio.Consulta_Partidas_Genericas();
                Cmb_Partidas_Genericas.Items.Clear();
                Cmb_Partidas_Genericas.DataSource = Dt_Partidas_Genericas;
                Cmb_Partidas_Genericas.DataTextField = "Nombre";
                Cmb_Partidas_Genericas.DataValueField = "Partida_Generica_ID";
                Cmb_Partidas_Genericas.DataBind();
                Cmb_Partidas_Genericas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Genericas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Genericas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Partidas_Especificas
        ///DESCRIPCION:             Llenar el combo de las partidas especificas
        ///PARAMETROS:              Partida_Generica_ID: Cadena de texto que contiene el ID de la partida Generica
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 12:55
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Partidas_Especificas(String Partida_Generica_ID)
        {
            //Declaracion de variables
            Cls_Rpt_Alm_Productos_Negocio Productos_Negocio = new Cls_Rpt_Alm_Productos_Negocio(); //variable para la capa de negocios
            DataTable Dt_Partidas_Especificas = new DataTable(); //tabla para las partidas especificas

            try
            {
                //Asignar propiedades
                Productos_Negocio.P_Partida_Generica_ID = Partida_Generica_ID;
                Dt_Partidas_Especificas = Productos_Negocio.Consulta_Partidas_Especificas();
                Cmb_Partidas_Especificas.DataSource = Dt_Partidas_Especificas;
                Cmb_Partidas_Especificas.DataTextField = "Nombre";
                Cmb_Partidas_Especificas.DataValueField = "Partida_ID";
                Cmb_Partidas_Especificas.DataBind();
                Cmb_Partidas_Especificas.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Partidas_Especificas.Items.Insert(1, new ListItem("Sin Valor", "NULO"));
                Cmb_Partidas_Especificas.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Habilitar_Controles
        ///DESCRIPCION:             Habilitar los controles de acuerdo si es filtro o bosqueda
        ///PARAMETROS:              Filtro: Booleano que indica si es busqueda por filtro
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              22/Marzo/2012 13:22
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Habilitar_Controles(Boolean Filtro)
        {
            try
            {
                Lbl_Tipo_Busqueda.Visible = !Filtro;
                Txt_Busqueda.Visible = !Filtro;
                Cmb_Impuestos.Enabled = Filtro;
                Cmb_Partidas_Especificas.Enabled = Filtro;
                Cmb_Partidas_Genericas.Enabled = Filtro;
                Cmb_Stock.Enabled = Filtro;
                Cmb_Tipos.Enabled = Filtro;
                Cmb_Unidades.Enabled = Filtro;
                Cmb_Estatus.Enabled = Filtro;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Tipo_Consulta
        ///DESCRIPCION:             Llenar el combo con los tipos de consulta de acuerdo al tipo de busqueda
        ///PARAMETROS:              Tipo_Busqueda: Cadena de texto que indica el tipo de busqueda
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              03/Abril/2012 16:19
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Tipo_Consulta(String Tipo_Busqueda)
        {
            try
            {
                //limpiar el combo del tipo de consulta
                Cmb_Tipo_Reporte.Items.Clear();

                //Seleccionar el tipo de busqueda
                switch (Tipo_Busqueda)
                {
                    case "Producto":
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Seleccione", ""));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Filtros", "Filtro"));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Clave", "Clave"));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Referencia JAPAMI", "Ref_JAPAMI"));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Nombre", "Nombre"));
                        Cmb_Tipo_Reporte.SelectedIndex = 0;
                        break;

                    case "Proveedor":
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Seleccione", ""));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Cuenta", "Cuenta"));
                        Cmb_Tipo_Reporte.Items.Add(new ListItem("Proveedor", "Proveedor"));
                        break;

                    default:
                        Estado_Inicial();
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Combo_Proveedores
        ///DESCRIPCION:             Llenar el combo de los proveedores de acuerdo al criterio de busqueda de nombre y cuenta
        ///PARAMETROS:              
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              04/Abril/2012 16:44
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Combo_Proveedores()
        {
            //Declaracion de variables 
            Cls_Rpt_Com_Ordenes_Compra_Negocio Ordenes_Compra_Negocio = new Cls_Rpt_Com_Ordenes_Compra_Negocio(); //variable para la capa de negocios
            DataTable Dt_Proveedores = new DataTable(); //Tabla para el resultado de la consulta

            try
            {
                //Realizar la consulta
                Ordenes_Compra_Negocio.P_Tipo_Consulta = Cmb_Tipo_Reporte.SelectedItem.Value;
                Ordenes_Compra_Negocio.P_Busqueda = Txt_Busqueda.Text.Trim();
                Dt_Proveedores = Ordenes_Compra_Negocio.Consulta_Proveedores();

                //llenar el combo de los proveedores
                Cmb_Proveedores.Items.Clear();
                Cmb_Proveedores.DataSource = Dt_Proveedores;
                Cmb_Proveedores.DataTextField = "Proveedor";
                Cmb_Proveedores.DataValueField = "Proveedor_ID";
                Cmb_Proveedores.DataBind();
                Cmb_Proveedores.Items.Insert(0, new ListItem("Seleccione", ""));
                Cmb_Proveedores.SelectedIndex = 0;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Grid
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Llena_Grid_Ordenes_Compra
        ///DESCRIPCION:             Llenar el Grid de las ordenes de compra por porveedor o por producto
        ///PARAMETROS:              Tipo_Busqueda: Cadena de texto que contiene el tipo de busqueda a realizar
        ///CREO:                    Noe Mosqueda Valadez
        ///FECHA_CREO:              09/Abril/2012 13:53
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACION
        ///*******************************************************************************
        private void Llena_Grid_Ordenes_Compra(String Tipo_Busqueda)
        {
            //Declaracion de variables
            Cls_Rpt_Com_Ordenes_Compra_Negocio Ordenes_Compra_Negocio = new Cls_Rpt_Com_Ordenes_Compra_Negocio(); //Variable de la capa de negocios
            DataTable Dt_Ordenes_Compra = new DataTable(); //tabla para la consulta

            try
            {
                //verificar si existe la variable de sesion
                if (HttpContext.Current.Session[P_Dt_Ordenes_Compra] == null)
                {
                    //Asignar propiedades
                    Ordenes_Compra_Negocio.P_Tipo_Consulta = Tipo_Busqueda;
                    
                    //Verificar el tipo de consulta a realizar
                    if (Tipo_Busqueda == "Proveedor")
                    {
                        //hacer la consulta de las ordenes de compra
                        Ordenes_Compra_Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedItem.Value;                        
                    }
                    else
                    {
                        //verificar el tipo de la consulta
                        switch (Cmb_Tipo_Reporte.SelectedIndex)
                        {
                            case 2: //Clave
                                Ordenes_Compra_Negocio.P_Clave = Txt_Busqueda.Text.Trim();
                                break;

                            case 3: //Referencia
                                Ordenes_Compra_Negocio.P_Ref_JAPAMI = Convert.ToInt64(Txt_Busqueda.Text.Trim());
                                break;

                            case 4: //Nombre
                                Ordenes_Compra_Negocio.P_Nombre = Txt_Busqueda.Text.Trim();
                                break;

                            default:
                                //Verificar si hay una unidad
                                if (Cmb_Unidades.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Unidad_ID = Cmb_Unidades.SelectedItem.Value;
                                }

                                //verificar si hya un impuesto
                                if (Cmb_Impuestos.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Impuesto_ID = Cmb_Impuestos.SelectedItem.Value;
                                }

                                //Verificar si hay un estatus
                                if (Cmb_Estatus.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                                }

                                //verificar si se ha seleccionado un tipo
                                if (Cmb_Tipos.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Tipo = Cmb_Tipos.SelectedItem.Value;
                                }

                                //Verificar si es stock
                                if (Cmb_Stock.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Stock = Cmb_Stock.SelectedItem.Value;
                                }

                                //Verificar si tiene una partida generica
                                if (Cmb_Partidas_Genericas.SelectedIndex > 0)
                                {
                                    Ordenes_Compra_Negocio.P_Partida_Generica_ID = Cmb_Partidas_Genericas.SelectedItem.Value;

                                    //Verificar si tiene una partida especifica
                                    if (Cmb_Partidas_Especificas.SelectedIndex > 0)
                                    {
                                        Ordenes_Compra_Negocio.P_Partida_Especifica_ID = Cmb_Partidas_Especificas.SelectedItem.Value;
                                    }
                                }
                                break;
                        }
                    }

                    //ejecutar consulta
                    Dt_Ordenes_Compra = Ordenes_Compra_Negocio.Consulta_Ordenes_Compra();
                }
                else
                {
                    Dt_Ordenes_Compra = ((DataTable)HttpContext.Current.Session[P_Dt_Ordenes_Compra]);
                }
                
                //Colocar la tabla en la variable de sesion
                HttpContext.Current.Session[P_Dt_Ordenes_Compra] = Dt_Ordenes_Compra;

                //Llenar el grid
                Grid_Ordenes_Compra.DataSource = Dt_Ordenes_Compra;

                //verificar el tipo de la consulta
                if (Tipo_Busqueda == "Proveedor")
                {
                    //Ocultar las columnas de los productos
                    Grid_Ordenes_Compra.Columns[3].Visible = false;
                    Grid_Ordenes_Compra.Columns[4].Visible = false;
                    Grid_Ordenes_Compra.Columns[5].Visible = false;

                    //Mostrar la columna del proveedor
                    Grid_Ordenes_Compra.Columns[2].Visible = true;
                }
                else
                {
                    //Mostrar las columnas de los productos
                    Grid_Ordenes_Compra.Columns[3].Visible = true;
                    Grid_Ordenes_Compra.Columns[4].Visible = true;
                    Grid_Ordenes_Compra.Columns[5].Visible = true;

                    //Ocultar la columna del proveedor
                    Grid_Ordenes_Compra.Columns[2].Visible = false;                    
                }

                Grid_Ordenes_Compra.DataBind();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Eventos
        protected void Cmb_Tipo_Reporte_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //seleccionar el indice
                switch (Cmb_Tipo_Busqueda.SelectedIndex)
                {
                    case 1: //Producto
                        //Seleccionar el indice
                        switch (Cmb_Tipo_Reporte.SelectedIndex)
                        {
                            case 1: //Filtros
                                Habilitar_Controles(true);
                                Cmb_Estatus.Enabled = false;
                                break;

                            case 2: //Clave
                                Habilitar_Controles(false);
                                Lbl_Tipo_Busqueda.Text = "Clave de Producto";
                                break;

                            case 3: //Referencia JAPAMI
                                Habilitar_Controles(false);
                                Lbl_Tipo_Busqueda.Text = "Referencia JAPAMI";
                                break;

                            case 4: //Nombre del producto
                                Habilitar_Controles(false);
                                Lbl_Tipo_Busqueda.Text = "Nombre del producto";
                                break;

                            default:
                                break;
                        }
                        break;

                    case 2: //Proveedor
                        //Seleccionar el indice
                        switch (Cmb_Tipo_Reporte.SelectedIndex)
                        {
                            case 1: //Cuenta del proveedor
                                Habilitar_Controles(false);
                                Lbl_Tipo_Busqueda.Text = "Cuenta de proveedor";
                                Lbl_Proveedor.Visible = true;
                                Cmb_Proveedores.Visible = true;
                                Btn_Busqueda_Proveedor.Visible = true;
                                break;

                            case 2: //Nombre del proveedor
                                Habilitar_Controles(false);
                                Lbl_Tipo_Busqueda.Text = "Nombre de proveedor";
                                Lbl_Proveedor.Visible = true;
                                Cmb_Proveedores.Visible = true;
                                Btn_Busqueda_Proveedor.Visible = true;
                                break;

                            default:
                                break;
                        }
                        break;

                    default:
                        Estado_Inicial();
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Tipo_Reporte_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Cmb_Partidas_Genericas_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //verificar el indice
                if (Cmb_Partidas_Genericas.SelectedIndex > 0)
                {
                    Llena_Combo_Partidas_Especificas(Cmb_Partidas_Genericas.SelectedItem.Value);
                }
                else
                {
                    //Limpiar las partidas especificas
                    Cmb_Partidas_Especificas.Items.Clear();
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Partidas_Genericas_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Btn_Consultar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Seleccionar el indice del tipo de busqueda
                switch (Cmb_Tipo_Busqueda.SelectedIndex)
                {
                    case 1: //Producto
                        //Verificar el tipo de consulta
                        switch (Cmb_Tipo_Reporte.SelectedIndex)
                        {
                            case 2:
                            case 3:
                            case 4:
                                //verificar si hay texto en al caja de texto
                                if (Txt_Busqueda.Text.Trim() == null || Txt_Busqueda.Text.Trim() == "" || Txt_Busqueda.Text.Trim() == String.Empty)
                                {
                                    Mostrar_Error("Favor de proporcionar un criterio de busqueda", true);
                                }
                                else
                                {
                                    Llena_Grid_Ordenes_Compra(Cmb_Tipo_Busqueda.SelectedItem.Value);
                                }
                                break;

                            default:
                                Llena_Grid_Ordenes_Compra(Cmb_Tipo_Busqueda.SelectedItem.Value);
                                break;
                        }
                        break;

                    case 2: //Proveedor
                        //verificar si hay proveedores
                        if (Cmb_Proveedores.Items.Count > 1)
                        {
                            //verificar si se ha seleccionado un proveedor
                            if (Cmb_Proveedores.SelectedIndex > 0)
                            {
                                Llena_Grid_Ordenes_Compra(Cmb_Tipo_Busqueda.SelectedItem.Value);
                            }
                            else
                            {
                                Mostrar_Error("Favor de seleccionar un proveedor.", true);
                            }
                        }
                        else
                        {
                            Mostrar_Error("Favor de realizar otra b&uacute;squeda de proveedores, ya que no hay proveedores en la lista desplegable.", true);
                        }
                         break;

                    default:
                        Mostrar_Error("Favor de seleccionar el tipo de b&uacute;squeda a realizar.", true);
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Consultar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Imprimir_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
        {
            try
            {

            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Imprimir_Excel_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Elimina_Sesiones();
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Salir_Click) " + ex.ToString(), true);
            }
        }

        protected void Cmb_Tipo_Busqueda_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Verificar el indice
                if (Cmb_Tipo_Busqueda.SelectedIndex > 0)
                {
                    Llena_Combo_Tipo_Consulta(Cmb_Tipo_Busqueda.SelectedItem.Value);
                }
                else
                {
                    Estado_Inicial();
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Cmb_Tipo_Busqueda_SelectedIndexChanged) " + ex.ToString(), true);
            }
        }

        protected void Btn_Busqueda_Proveedor_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //verificar si hay texto en la caja de texto
                if (Txt_Busqueda.Text.Trim() != null && Txt_Busqueda.Text.Trim() != "" && Txt_Busqueda.Text.Trim() != String.Empty)
                {
                    Llena_Combo_Proveedores();
                }
                else
                {
                    Mostrar_Error("Favor de proporcionar un criterio de b&uacute;squeda", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Error("Error: (Btn_Busqueda_Proveedor_Click) " + ex.ToString(), true);
            }
        }

    #endregion
}