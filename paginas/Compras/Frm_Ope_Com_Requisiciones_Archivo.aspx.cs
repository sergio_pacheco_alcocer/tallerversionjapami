﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using System.Text;
using JAPAMI.Archivos_Requisiciones.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Generar_Requisicion.Datos;
using JAPAMI.Dependencias.Negocios;



public partial class paginas_Compras_Frm_Ope_Com_Requisiciones_Archivo : System.Web.UI.Page
{
    #region variables y constantes
    private static String P_Dt_Requisiciones = "P_Dt_Requisiciones";
    private const String Operacion_Comprometer = "COMPROMETER";
    private const String Operacion_Descomprometer = "DESCOMPROMETER";
    private const String Operacion_Quitar_Renglon = "QUITAR";
    private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
    private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

    private const String SubFijo_Requisicion = "RQ-";
    private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
    private const String EST_GENERADA = "GENERADA";
    private const String EST_CANCELADA = "CANCELADA";
    private const String EST_REVISAR = "REVISAR";
    private const String EST_RECHAZADA = "RECHAZADA";
    private const String EST_AUTORIZADA = "AUTORIZADA";
    private const String EST_FILTRADA = "FILTRADA";
    private const String EST_COTIZADA = "COTIZADA";
    private const String EST_COMPRA = "COMPRA";
    private const String EST_TERMINADA = "TERMINADA";
    private const String EST_COTIZADA_RECHAZADA = "COTIZADA-RECHAZADA";
    private const String EST_ALMACEN = "ALMACEN";
    private const String P_Dt_Archivos_Requisicion = "Dt_Archivos_Requisicion";
    private const String P_Dt_Archivos_Eliminados = "Dt_Archivos_Eliminados";
    private const String P_Nombre_Archivo = "Nombre_Archivo";
    private const String P_Comentarios = "Comentarios";
    private const String P_No_Requisicion = "No_Requisicion";

    //Variables para respaldar la sesion
    private String S_Empleado_ID = String.Empty;
    private String S_Nombre_Empleado = String.Empty;
    private String S_No_Empleado = String.Empty;
    private String S_Rol_ID = String.Empty;
    private String S_Nombre_Rol = String.Empty;
    private Boolean S_Mostrar_Menu = true;
    private String S_Dependencia_ID_Empleado = String.Empty;
    private String S_Area_ID_Empleado = String.Empty;
    private DataTable S_Datos_Empleado = new DataTable();
    private DataTable S_Totales = new DataTable();
    private StringBuilder S_Historial_Nomina_Generada;
    private DataTable S_Menus_Control_Acceso = new DataTable();
    private DataTable S_Dt_Proveedor = new DataTable();
    private String S_Forma_Cambios = String.Empty;
    private DataTable S_Dt_Conceptos_Cobro_Facturacion = new DataTable();
    private DataTable S_Dt_Predios_Facturacion = new DataTable();
    private DataTable S_Dt_Fechas_Pagos_Estado_Cuenta = new DataTable();
    private DataTable S_Dt_Cuentas_Asignadas_Analista = new DataTable();
    private DataTable S_Dt_Reporte_Dictamenes = new DataTable();
    private DataTable S_Dt_Cuentas_Asignadas_Guardadas = new DataTable();
    private DataTable S_Dt_Reporte_Facturado_Detalles = new DataTable();
    private DataTable S_Dt_Reporte_Contratos_Cuentas_Detalles = new DataTable();
    private String S_Lecturas_registradas = String.Empty;
    private String S_Lecturas_no_registradas = String.Empty;

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Estado_Inicial();
                }
                else
                {
                    Mostrar_Informacion("", false);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Page_Load) " + ex.Message, true);
            }
        }
    #endregion

    #region Metodos
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            Lbl_Informacion.Style.Add("color", "#990000");
            Lbl_Informacion.Visible = mostrar;
            Img_Warning.Visible = mostrar;
            Lbl_Informacion.Text = txt;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Estado_Inicial()
        // DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
        // RETORNA: 
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              05/Marzo/2012 19:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Estado_Inicial()
        {
            try
            {
                Eliminar_Sesiones();
                Limpiar_Controles_Busqueda();
                Habilitar_Controles("Navegacion");
                
                //Colocar las fechas
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);               
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();


                Div_Contenido.Visible = false;
                Div_Requisiciones.Visible = true;
                Txt_No_Requisicion.Value = "";

                Llena_Combo_Dependencias();
                Cmb_Dependencia_Panel.Items[0].Text = "<- TODAS ->";
                Llenar_Combos_Busqueda();
                Llenar_Grid_Requisiciones();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Eliminar_Sesiones()
        // DESCRIPCION:             Eliminar las sesiones utilizadas en esta pagina
        // RETORNA: 
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              05/Marzo/2012 13:31
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Eliminar_Sesiones()
        {
            try
            {
                HttpContext.Current.Session.Remove(P_Dt_Requisiciones);
                HttpContext.Current.Session.Remove(P_Dt_Archivos_Requisicion);
                HttpContext.Current.Session.Remove(P_Dt_Archivos_Eliminados);
                HttpContext.Current.Session.Remove(P_Nombre_Archivo);
                HttpContext.Current.Session.Remove(P_Comentarios);
                HttpContext.Current.Session.Remove(P_No_Requisicion);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Llenar_Combo_Dependencias()
        // DESCRIPCION:             Llenar el combo de las dependencias
        // RETORNA: 
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              05/Marzo/2012 13:22
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Llena_Combo_Dependencias()
        {
            //Declaracion de variables
            Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio(); //Variable para la capa de negocios
            DataTable Dt_Dependencias = new DataTable(); //Variable para la tabla de las dependencias

            try
            {
                Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia_Panel, Dt_Dependencias, 1, 0);
                Cmb_Dependencia_Panel.SelectedIndex = 0;
                DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                if (Dt_URs.Rows.Count > 1)
                {
                    Cmb_Dependencia_Panel.Enabled = true;
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Dependencia_Panel, Dt_URs, 1, 0);
                    Cmb_Dependencia_Panel.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Busqueda()
        // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
        // RETORNA: 
        // CREO: Gustavo Angeles Cruz
        // FECHA_CREO: Diciembre/2010 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public void Llenar_Combos_Busqueda()
        {
            String[] Datos_Combo = 
                {"CONST ,GENERADA, REVISAR", EST_EN_CONSTRUCCION, EST_GENERADA, EST_CANCELADA, EST_AUTORIZADA, 
                  EST_FILTRADA,EST_RECHAZADA,EST_REVISAR, EST_COTIZADA, EST_COTIZADA_RECHAZADA, EST_COMPRA, EST_ALMACEN, EST_TERMINADA };
            Llenar_Combo(Cmb_Estatus_Busqueda, Datos_Combo);
            Cmb_Estatus_Busqueda.SelectedIndex = 1;
            Cmb_Tipo_Busqueda.Items.Clear();
            Cmb_Tipo_Busqueda.Items.Add("STOCK,TRANSITORIA");
            Cmb_Tipo_Busqueda.Items.Add("STOCK");
            Cmb_Tipo_Busqueda.Items.Add("TRANSITORIA");
            Cmb_Tipo_Busqueda.Items[0].Selected = true;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo
        ///DESCRIPCIÓN:
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Llenar_Combo(DropDownList Combo, String[] Items)
        {
            Combo.Items.Clear();
            Combo.Items.Add("<<SELECCIONAR>>");
            foreach (String _Item in Items)
            {
                Combo.Items.Add(_Item);
            }
            Combo.Items[0].Value = "0";
            Combo.Items[0].Selected = true;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///PARAMETROS:   
        ///CREO: Gustavo Angeles
        ///FECHA_CREO: 9/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public bool Verificar_Fecha()
        {
            bool Respuesta = false;
            //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
            DateTime Date1 = new DateTime();
            DateTime Date2 = new DateTime();
            try
            {
                //Convertimos el Texto de los TextBox fecha a dateTime
                Date1 = DateTime.Parse(Txt_Fecha_Inicial.Text);
                Date2 = DateTime.Parse(Txt_Fecha_Final.Text);
                if (Date1 <= Date2)
                {
                    Respuesta = true;
                }
            }
            catch (Exception e)
            {
                String str = e.ToString();
                Respuesta = false;
            }
            return Respuesta;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Habilitar_Controles
        // DESCRIPCIÓN:             Habilitar los controles de aucerdo al modo de operacion
        // PARAMETROS:              Modo: Cadena de texto quie contiene el modo de operacion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 13:05
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Habilitar_Controles(String Modo)
        {
            try
            {
                //Seleccionar el modo de operacion
                switch (Modo)
                {
                    case "Navegacion":                      
                        Div_Contenido.Visible = false;
                        Div_Requisiciones.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Eliminar.Visible = false;
                        Btn_Modificar.Visible = false;
                        Btn_Cargar.Visible = false;
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Eliminar.ToolTip = "Eliminar Archivos";
                        break;

                    case "Operacion":
                        Div_Contenido.Visible = true;
                        Div_Requisiciones.Visible = false;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Eliminar.Visible = true;
                        Btn_Modificar.Visible = true;
                        Btn_Cargar.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Modificar.ToolTip = "Modificar";
                        Grid_Archivos_Requisiciones.Columns[0].Visible = false;
                        break;

                    case "Archivo":
                        Div_Contenido.Visible = true;
                        Div_Requisiciones.Visible = false;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Eliminar.Visible = false;
                        Btn_Modificar.Visible = true;
                        Btn_Cargar.Visible = true;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Modificar.ToolTip = "Guardar";
                        Grid_Archivos_Requisiciones.Columns[0].Visible = true;
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Limpiar_Controles_Busqueda()
        // DESCRIPCIÓN:             Limpiar los controles de la seccíon de busqueda
        // RETORNA: 
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              05/Marzo/2012 13:24
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Limpiar_Controles_Busqueda()
        {
            try
            {
                Cmb_Dependencia_Panel.SelectedIndex = 0;
                Cmb_Estatus_Busqueda.SelectedIndex = 0;
                Cmb_Tipo_Busqueda.SelectedIndex = 0;
                Txt_Busqueda.Text = "";
                Hdf_Estatus_Requisicion.Value = "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }


        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Llena_Datos_Controles
        // DESCRIPCION:             Llenar los datos de los controles cuando se selecciona una requisicion
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              07/Marzo/2012 19:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Llena_Datos_Controles(int No_Requisicion)
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Requisiciones_Negocio Archivos_Requisiciones_Negocio = new Cls_Ope_Archivos_Requisiciones_Negocio(); //variable para la capa de negocios
            DataTable Dt_Archivos_Requisicion = new DataTable(); //Tabla para la consulta de los archivos de la requisicion

            Cls_Ope_Com_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Com_Requisiciones_Negocio();
            Req_Negocio.P_Requisicion_ID = No_Requisicion.ToString().Trim();
            DataTable Dt_Datos = Cls_Ope_Com_Requisiciones_Datos.Consultar_Requisicion_Por_ID(Req_Negocio);
            if (Dt_Datos != null) {
                if (Dt_Datos.Rows.Count > 0) {
                    Hdf_Estatus_Requisicion.Value = Dt_Datos.Rows[0][Ope_Com_Requisiciones.Campo_Estatus].ToString().Trim();
                }
            }
            try
            {
                //Colocar los datos en el formulario
                Lbl_Archivos_Requision.Text = HttpUtility.HtmlDecode("Requisici&oacute;n con Folio: RQ-" + No_Requisicion.ToString().Trim());

                Llena_Grid_Archivos_Requisicion(No_Requisicion);                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private void Alta_Archivos()
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Requisiciones_Negocio Archivos_Requisiciones_Negocio = new Cls_Ope_Archivos_Requisiciones_Negocio(); //variable para la capa de negocios
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos que van a ser ingresados a la requisicion
            DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos a eliminar

            try
            {
                //Obtener las tablas de los archivos eliminados y los archivos a subir
                //asignar propiedades
                Archivos_Requisiciones_Negocio.P_No_Requisicion = Convert.ToInt32(Txt_No_Requisicion.Value);
                Archivos_Requisiciones_Negocio.P_Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Requisicion]);
                Archivos_Requisiciones_Negocio.P_Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);

                //Dar de alta los archivos de la requisicion
                Archivos_Requisiciones_Negocio.Alta_Archivos_Requisicion();

                //Colocar los archivos en el directorio
                Agrega_Archivos_Requisicion(Convert.ToInt32(Txt_No_Requisicion.Value));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        private void Agrega_Archivos_Requisicion(int No_Requisicion)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos a agregar
            DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos eliminados
            int Cont_Elementos; //variable para el contador
            String Ruta_Fija = String.Empty; //variable para la ruta fija
            String[] Archivos_Temporales; //variable para los archivos temporales

            try
            {
                //Asignar la ruta de los archivos
                Ruta = Server.MapPath("~") + "\\Req_Archivos\\RQ-" + No_Requisicion.ToString().Trim();
                Ruta_Fija = Ruta;

                //verificar si la ruta existe
                if (Directory.Exists(Ruta) == false)
                {
                    Directory.CreateDirectory(Ruta);
                } 

                //Verificar si la variable de sesion de los archivos eliminados existe
                if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] != null)
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);

                    //Verificar si la tabla tiene elementos
                    if (Dt_Archivos_Eliminados.Rows.Count > 0)
                    {
                        //Ciclo para eliminar los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos_Eliminados.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar si el archivo tiene que ser eliminado
                            if (Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Original"].ToString().Trim() == "SI")
                            {
                                //Verificar si el archivio existe
                                if (File.Exists(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                {
                                    //Eliminar el archivo
                                    File.Delete(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                }
                            }
                        }
                    }
                }

                //Asignar la ruta de los archivos temporales
                Ruta = Server.MapPath("~") + "\\Req_Archivos\\temporal\\RQ-" + No_Requisicion.ToString().Trim();

                //Verificar si la ruta existe
                if (Directory.Exists(Ruta) == true)
                {
                    //Verificar si la variable de sesion existe
                    if (HttpContext.Current.Session[P_Dt_Archivos_Requisicion] != null)
                    {
                        //Colocar la variable de sesion en la tabla
                        Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Requisicion]);

                        //Verificar si la tabla tiene elementos
                        if (Dt_Archivos.Rows.Count > 0)
                        {
                            //Ciclo para copiar los archivos temporales a la ruta de los archivos
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                            {
                                //Verificar si se tiene que copiar el elemento
                                if (Dt_Archivos.Rows[Cont_Elementos]["Original"].ToString().Trim() == "NO")
                                {
                                    //Verificar si el archivo existe
                                    if (File.Exists(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                    {
                                        //Copiar el archivo
                                        File.Copy(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim(), Ruta_Fija + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                    }
                                }
                            }
                        }
                    }

                    //Obtener el listado de los archivos de la ruta temporal
                    Archivos_Temporales = Directory.GetFiles(Ruta);

                    //Verificar si hay archivos
                    if (Archivos_Temporales.Length > 0)
                    {
                        //Ciclo para la eliminacion de los archivos temporales
                        for (Cont_Elementos = 0; Cont_Elementos < Archivos_Temporales.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Archivos_Temporales[Cont_Elementos].Trim());
                        }
                    }  
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos
        // DESCRIPCION:             Eliminar los archivos de la requisicion
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 11:27
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos(int No_Requisicion)
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Requisiciones_Negocio Archivos_Requisiciones_Negocio = new Cls_Ope_Archivos_Requisiciones_Negocio(); //variable para la capa de negocios
            DataTable Dt_Temp = new DataTable();

            try
            {
                //Asignar propiedades
                Archivos_Requisiciones_Negocio.P_No_Requisicion = No_Requisicion;
                //Consultamos el archivo de la requisicion
                Dt_Temp = Archivos_Requisiciones_Negocio.Consulta_Archivos();
                if (Dt_Temp.Rows.Count > 0)
                {
                    if (Dt_Temp.Rows[0]["USUARIO_CREO"].ToString() == Cls_Sessiones.Nombre_Empleado)
                    {
                        Archivos_Requisiciones_Negocio.Eliminar_Archivos_Requisicion();
                        //Eliminar los archivos
                        Elimina_Archivos_Requisicion(No_Requisicion);
                    }
                    else
                    {
                        Mostrar_Informacion("Lo siento, solo el usuario que agrego los archivos a la requisición puede eliminarlos", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos_Requisicion
        // DESCRIPCION:             Eliminar los archivos de la requisicion
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 11:27
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos_Requisicion(int No_Requisicion)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta de los archivos
            int Cont_Elementos; //variable para el contador
            String[] Vec_Archivos; //Vector para los archivos

            try
            {
                
                //Obtener la ruta de los archivos
                Ruta = Server.MapPath("~") + "\\Req_Archivos\\RQ-" + No_Requisicion.ToString().Trim();

                //Verificar si existe la carpeta
                if (Directory.Exists(Ruta) == true)
                {
                    //Obtener los archivos del directorio
                    Vec_Archivos = Directory.GetFiles(Ruta);

                    //Verificar si existen archivos en el directorio
                    if (Vec_Archivos.Length > 0)
                    {
                        //Ciclo para el barrido de los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Vec_Archivos.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Vec_Archivos[Cont_Elementos].Trim());
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message, ex);
            }
        }
    #endregion

    #region Grid
        #region Grid Requisiciones
            ///*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Requisiciones
            // DESCRIPCIÓN: Llena el grid principal de requisiciones
            // RETORNA: 
            // CREO: Gustavo Angeles Cruz
            // FECHA_CREO: Diciembre/2010 
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            public void Llenar_Grid_Requisiciones()
            {
                Div_Contenido.Visible = false;
                Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Requisiciones_Negocio();
                //Cmb_Dependencia.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado.ToString();
                //Cmb_Dependencia.SelectedValue = ;
                Requisicion_Negocio.P_Dependencia_ID = ((Cmb_Dependencia_Panel.SelectedIndex > 0) ? Cmb_Dependencia_Panel.SelectedValue : Obtener_UR_Empleado());//Cmb_Dependencia.SelectedValue;
                
                //Requisicion_Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
                Requisicion_Negocio.P_Fecha_Inicial = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);

                Requisicion_Negocio.P_Fecha_Final = Txt_Fecha_Final.Text;
                Requisicion_Negocio.P_Fecha_Final = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);

                if (Txt_Busqueda.Text.Trim().Length > 0)
                {
                    String No_Requisa = Txt_Busqueda.Text;
                    No_Requisa = No_Requisa.ToUpper();
                    No_Requisa = No_Requisa.Replace("RQ-", "");
                    int Int_No_Requisa = 0;
                    try
                    {
                        Int_No_Requisa = int.Parse(No_Requisa);
                    }
                    catch (Exception Ex)
                    {
                        String Str = Ex.ToString();
                        No_Requisa = "0";
                    }
                    Requisicion_Negocio.P_Requisicion_ID = No_Requisa;
                }
                Requisicion_Negocio.P_Tipo = Cmb_Tipo_Busqueda.SelectedValue.Trim();
                Requisicion_Negocio.P_Estatus = Cmb_Estatus_Busqueda.SelectedValue.Trim();
                Session[P_Dt_Requisiciones] = Requisicion_Negocio.Consultar_Requisiciones();
                if (Session[P_Dt_Requisiciones] != null && ((DataTable)Session[P_Dt_Requisiciones]).Rows.Count > 0)
                {
                    Div_Contenido.Visible = false;
                    Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
                    Grid_Requisiciones.DataBind();
                }
                else
                {
                    //Mostrar_Informacion("No se encontraron requisiciones con los criterios de búsqueda",true);
                    //Grid_Requisiciones.DataSource = Session[P_Dt_Requisiciones] as DataTable;
                    //Grid_Requisiciones.DataBind();       
                    Session[P_Dt_Requisiciones] = null;
                    Grid_Requisiciones.DataSource = null;
                    Grid_Requisiciones.DataBind();
                }
            }

            private String Obtener_UR_Empleado()
            {
                String UR = "";
                DataTable Dt_URs = Cls_Util.Consultar_URs_De_Empleado(Cls_Sessiones.Empleado_ID);
                foreach (DataRow Fila in Dt_URs.Rows)
                {
                    UR += Fila[0].ToString().Trim() + "','";
                }
                return UR.TrimEnd('\'').TrimEnd(',').TrimEnd('\'');
            }

            protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                try
                {
                    Grid_Requisiciones.DataSource = ((DataTable)Session[P_Dt_Requisiciones]);
                    Grid_Requisiciones.PageIndex = e.NewPageIndex;
                    Grid_Requisiciones.DataBind();
                }
                catch (Exception ex)
                {
                    Mostrar_Informacion("Error: (Grid_Requisiciones_PageIndexChanging) " + ex.Message, true);
                }
            }

            protected void Btn_Seleccionar_Requisicion_Click(object sender, ImageClickEventArgs e)
            {
                //Declaracion de variables
                int No_Requisicion; //variable para el numero de la requisicion

                try
                {
                    //Obtener el numero de la requisicion
                    No_Requisicion = Convert.ToInt32(((ImageButton)sender).CommandArgument);
                    Txt_No_Requisicion.Value = No_Requisicion.ToString().Trim();

                    Llena_Datos_Controles(No_Requisicion);

                    Habilitar_Controles("Operacion");

                    //Colocar le numerio de requisicion en la variable de sesion
                    HttpContext.Current.Session[P_No_Requisicion] = No_Requisicion;
                }
                catch (Exception ex)
                {
                    Mostrar_Informacion("Error: (Grid_Requisiciones_PageIndexChanging) " + ex.Message, true);
                }
            }
        #endregion


    #region Grid Archivos
            ///*******************************************************************************
            // NOMBRE DE LA FUNCION:    Llena_Grid_Archivos_Requisicion
            // DESCRIPCION:             Llenar el Grid con los datos de los archivos de la requisicion
            // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
            // CREO:                    Noe Mosqueda Valadez
            // FECHA_CREO:              07/Marzo/2012 19:00
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            private void Llena_Grid_Archivos_Requisicion(int No_Requisicion)
            {
                //Declaracion de variables
                Cls_Ope_Archivos_Requisiciones_Negocio Archivos_Requisiciones_Negocio = new Cls_Ope_Archivos_Requisiciones_Negocio(); //variable para la capa de negocios
                DataTable Dt_Archivos_Requisicion = new DataTable();
                try
                {
                    //Verificar si existe la sesion de los archivos de la requisicion
                    if (HttpContext.Current.Session[P_Dt_Archivos_Requisicion] == null)
                    {
                        //ejecutar la consulta
                        Archivos_Requisiciones_Negocio.P_No_Requisicion = No_Requisicion;
                        Dt_Archivos_Requisicion = Archivos_Requisiciones_Negocio.Consulta_Archivos();
                    }
                    else
                    {
                        //Colocar la variable de sesion en la tabla
                        Dt_Archivos_Requisicion = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Requisicion]);
                    }

                    //llenar el grid
                    Grid_Archivos_Requisiciones.DataSource = Dt_Archivos_Requisicion;
                    Grid_Archivos_Requisiciones.Columns[1].Visible = true;
                    Grid_Archivos_Requisiciones.Columns[2].Visible = true;
                    Grid_Archivos_Requisiciones.Columns[3].Visible = true;
                    Grid_Archivos_Requisiciones.Columns[9].Visible = true;
                    Grid_Archivos_Requisiciones.DataBind();
                    Grid_Archivos_Requisiciones.Columns[1].Visible = false;
                    Grid_Archivos_Requisiciones.Columns[2].Visible = false;
                    Grid_Archivos_Requisiciones.Columns[3].Visible = false;
                    Grid_Archivos_Requisiciones.Columns[9].Visible = false;

                    //Colocar la tabla en una variable de sesion
                    HttpContext.Current.Session[P_Dt_Archivos_Requisicion] = Dt_Archivos_Requisicion;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            ///*******************************************************************************
            // NOMBRE DE LA FUNCION:    Agregar_Archivo_Tabla
            // DESCRIPCION:             Agregar la informacion del archivo a la tabla para el llenado del grid de los archivos
            // PARAMETROS:              1. Nombre_Archivo: Cadena de texto que contiene el nombre del archivo
            //                          2. Comentarios: Cadena de texto que contiene los comentarios del archivo
            //                          3. No_Requisicion: Entero que contiene el numero de la requisicion a la cual sera colocado el archivo
            // CREO:                    Noe Mosqueda Valadez
            // FECHA_CREO:              07/Marzo/2012 19:00
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            private void Agregar_Archivo_Tabla(String Nombre_Archivo, String Comentarios, int No_Requisicion)
            {
                //Declaracion de variables
                DataTable Dt_Archivos_Requisicion = new DataTable(); //Tabla para los archivos de la requisicion
                DataRow Renglon; //Renglon para el llenado de la tabla
                int Cont_Elementos; //variable para el contador
                Boolean Existente = false; //variable que indica que el archivo ya existe

                try
                {
                    //Verificar si existe la variable de sesion
                    if (HttpContext.Current.Session[P_Dt_Archivos_Requisicion] == null)
                    {
                        //Definir las columnas
                        Dt_Archivos_Requisicion.Columns.Add("No_Archivo", typeof(Int64));
                        Dt_Archivos_Requisicion.Columns.Add("Folio", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Nombre_Archivo", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Descripcion", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Original", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Estatus_Requisicion", typeof(String));
                    }
                    else
                    {
                        //Asignar la variable de sesion a la tabla
                        Dt_Archivos_Requisicion = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Requisicion]);
                    }

                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos_Requisicion.Rows.Count; Cont_Elementos++)
                    {
                        //Verificar que el renglon no haya sido eleiminado
                        if (Dt_Archivos_Requisicion.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                        {
                            //Verificar si el archivo ya se encuentra en los archivos de la requisicion
                            if (Dt_Archivos_Requisicion.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim().ToUpper() == Nombre_Archivo.Trim().ToUpper())
                            {
                                //Indicar que ya existe ese elemento
                                Existente = true;
                                break;
                            }
                        }
                    }

                    //Verificar si se tiene que agregar el archivo en la tabla
                    if (Existente == false)
                    {
                        //Instanciar el renglon
                        Renglon = Dt_Archivos_Requisicion.NewRow();

                        //Llenar el renglon
                        Renglon["No_Archivo"] = Convert.ToInt64(String.Format("{0:yyyyMMddHHmmss}", DateTime.Now));
                        Renglon["Folio"] = "RQ-" + No_Requisicion.ToString().Trim();

                        //verificar si el nombre del archivo no excede de 250 caracteres
                        if (Nombre_Archivo.Trim().Length > 250)
                        {
                            Renglon["Nombre_Archivo"] = Nombre_Archivo.Substring(0, 250);
                        }
                        else
                        {
                            Renglon["Nombre_Archivo"] = Nombre_Archivo;
                        }

                        //verificar si los comentarios exceden de 500
                        if (Comentarios.Trim().Length > 500)
                        {
                            Renglon["Descripcion"] = Comentarios.Substring(0, 500);
                        }
                        else
                        {
                            Renglon["Descripcion"] = Comentarios;
                        }

                        Renglon["Original"] = "NO";
                        Renglon["Estatus_Requisicion"] = Hdf_Estatus_Requisicion.Value.Trim();

                        //Colocar el renglon en la tabla
                        Dt_Archivos_Requisicion.Rows.Add(Renglon);
                    }

                    //Colocar la tabla en la variable de sesion
                    HttpContext.Current.Session[P_Dt_Archivos_Requisicion] = Dt_Archivos_Requisicion;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            private void Elimina_Archivo_Tabla(Int64 No_Archivo)
            {
                //Declaracion de variables
                DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos
                DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos eliminados
                DataRow Renglon; //Renglon para el llenado de la tabla
                int Cont_Elementos; //variable para el contador

                try
                {
                    //verificar si la variable de sesion de los archivos 
                    if (HttpContext.Current.Session[P_Dt_Archivos_Requisicion] != null)
                    {
                        //Colocar la variable se sesion en la tabla
                        Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Requisicion]);

                        //Verificar si la tabla tiene elementos
                        if (Dt_Archivos.Rows.Count > 0)
                        {
                            //Verificar si existe la variable de sesion
                            if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] == null)
                            {
                                //Colocar columnas a la tabla
                                //Definir las columnas
                                Dt_Archivos_Eliminados.Columns.Add("No_Archivo", typeof(Int64));
                                Dt_Archivos_Eliminados.Columns.Add("Folio", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Nombre_Archivo", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Descripcion", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Original", typeof(String));
                            }
                            else
                            {
                                Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);
                            }

                            //ciclo para el barrido de la tabla
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                            {
                                //verificar que el renglon no haya sido eliminado
                                if (Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                                {
                                    //Verificar si el elemento es el que se tiene que eliminar
                                    if (Convert.ToInt64(Dt_Archivos.Rows[Cont_Elementos]["No_Archivo"]) == No_Archivo)
                                    {
                                        //instanciar renglon
                                        Renglon = Dt_Archivos.Rows[Cont_Elementos];

                                        //Colocar renglon en la tabla de los eliminados
                                        Dt_Archivos_Eliminados.ImportRow(Renglon);

                                        //Eliminar el renglon de la tabla de los archivos
                                        Dt_Archivos.Rows.RemoveAt(Cont_Elementos);

                                        //Salir del ciclo
                                        break;
                                    }
                                }
                            }

                            //Actualizar las variables de sesion
                            HttpContext.Current.Session[P_Dt_Archivos_Requisicion] = Dt_Archivos;
                            HttpContext.Current.Session[P_Dt_Archivos_Eliminados] = Dt_Archivos_Eliminados;

                            //Llenar el grid
                            Llena_Grid_Archivos_Requisicion(Convert.ToInt32(Txt_No_Requisicion.Value));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }

            protected void Grid_Archivos_Requisiciones_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                //Declaracion de variables
                HyperLink Hyp_Lnk_Archivo_src; //variable para el hyperlink del archivo
                String Ruta = String.Empty; //variable para la ruta del archivo
                ImageButton Btn_Seleccionar_Archivo_src; //variable para el boton de eliminar

                try
                {
                    //verificar el tipo de renglon
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        //Instanciar el control
                        Hyp_Lnk_Archivo_src = ((HyperLink)e.Row.Cells[5].FindControl("Hyp_Lnk_Archivo"));

                        //Colocarle los detalles de la navegacion
                        Hyp_Lnk_Archivo_src.Text = "Ver";//e.Row.Cells[3].Text;
                        Ruta = "~/Req_Archivos/";

                        //Verificar si es fija o temporal
                        if (e.Row.Cells[6].Text.Trim() == "NO")
                        {
                            Ruta += "Temporal/";
                        }

                        //Colocar el resto de la ruta
                        Ruta += e.Row.Cells[2].Text.Trim() + "/" + e.Row.Cells[3].Text.Trim();

                        //Asignar la URL
                        Hyp_Lnk_Archivo_src.NavigateUrl = Ruta;
                        Hyp_Lnk_Archivo_src.Target = "_blank";

                        //Verificar si esta en modo de edicion
                        Btn_Seleccionar_Archivo_src = ((ImageButton)e.Row.Cells[1].FindControl("Btn_Seleccionar_Archivo"));
                        if (Btn_Modificar.ToolTip == "Guardar")
                        {
                            Btn_Seleccionar_Archivo_src.Enabled = true;
                        }
                        else
                        {
                            Btn_Seleccionar_Archivo_src.Enabled = false;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Mostrar_Informacion("Error: (Btn_Seleccionar_Archivo_Click) " + ex.Message, true);
                }
            }

    #endregion
#endregion

    #region Eventos
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar el tooltip del boton
                if (Btn_Salir.ToolTip == "Cancelar")
                {
                    Estado_Inicial();
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Salir_Click) " + ex.Message, true);
            }
        }

        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Llenar_Grid_Requisiciones();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Buscar_Click) " + ex.Message, true);
            }
        }

        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //seleccionar la operacion a realizar
                switch (Btn_Modificar.ToolTip)
                {
                    case "Guardar":
                        //verificar si hay datos de guardar
                        if (HttpContext.Current.Session[P_Dt_Archivos_Requisicion] != null)
                        {
                            Alta_Archivos();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Actualización de Archivos de Requisición RQ-" + Txt_No_Requisicion.Value.Trim() + " Exitosa');", true);
                            Estado_Inicial();
                        }
                        else
                        {
                            Mostrar_Informacion("No se han seleccionado archivos para la Requisici&oacute;n", true);
                        }
                        break;

                    case "Modificar":
                        Habilitar_Controles("Archivo");
                        Llena_Grid_Archivos_Requisicion(Convert.ToInt32(HttpContext.Current.Session[P_No_Requisicion]));
                        break;

                    default:
                        Estado_Inicial();
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Modificar_Click) " + ex.Message, true);
            }
        }

        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un elemento
                if (Txt_No_Requisicion.Value != null && Txt_No_Requisicion.Value != "" && Txt_No_Requisicion.Value != String.Empty)
                {
                    Elimina_Archivos(Convert.ToInt32(Txt_No_Requisicion.Value));
                    Estado_Inicial();
                }
                else
                {
                    Mostrar_Informacion("Favor de seleccionar la Requisici&oacute;n a la cual se eliminar&aacute;n todos sus archivos", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Modificar_Click) " + ex.Message, true);
            }
        }
    
        protected void AFil_Archivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta

            try
            {
                //Verificar si se tiene un archivo
                if (AFil_Archivo.HasFile)
                {
                    //obtener la ruta del servidor
                    Ruta = HttpContext.Current.Server.MapPath("~");

                    //verificar si el directorio ya ha sido creado
                    if (Directory.Exists(Ruta + "\\Req_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim()) == false)
                    {
                        Directory.CreateDirectory(Ruta + "\\Req_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim());
                    }

                    Ruta = Ruta + "\\Req_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim() + "\\" + System.IO.Path.GetFileName(AFil_Archivo.FileName);

                    //Colocar los datos en los controles ocultos
                    HttpContext.Current.Session[P_Nombre_Archivo] = AFil_Archivo.FileName;

                    AFil_Archivo.SaveAs(Ruta);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (AFil_Archivo_UploadedComplete) " + ex.Message, true);
            }
        }

        protected void Btn_Archivos_Requisicion_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificar si se tiene un archivo
                if (HttpContext.Current.Session[P_Nombre_Archivo] != null)
                {
                    HttpContext.Current.Session[P_Comentarios] = Txt_Comentarios_Archivo.Text.Trim();
                    Agregar_Archivo_Tabla(HttpContext.Current.Session[P_Nombre_Archivo].ToString().Trim(), HttpContext.Current.Session[P_Comentarios].ToString().Trim(), Convert.ToInt32(Txt_No_Requisicion.Value.Trim()));
                    Llena_Grid_Archivos_Requisicion(Convert.ToInt32(Txt_No_Requisicion.Value.Trim()));
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Archivos_Requisicion_Click) " + ex.Message, true);
            }
        }

        protected void Btn_Cargar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Txt_Comentarios_Archivo.Text = "";
                HttpContext.Current.Session.Remove(P_Nombre_Archivo);
                HttpContext.Current.Session.Remove(P_Comentarios);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Archivos_Requisicion_Click) " + ex.Message, true);
            }
        }

        protected void Btn_Seleccionar_Archivo_Click(object sender, EventArgs e)
        {
            //Declaracion de variables
            Int64 No_Archivo; //Variable para el numero del archivo

            try
            {
                //Obtener el numero del archivo
                No_Archivo = Convert.ToInt64(((ImageButton)sender).CommandArgument);
               //Verificar si el usuario logueado es el mismo que agrego el archivo a la requisicion
                Cls_Ope_Archivos_Requisiciones_Negocio Archivos_Negocio = new Cls_Ope_Archivos_Requisiciones_Negocio();
                Archivos_Negocio.P_No_Archivo = No_Archivo;
                DataTable Dt_Archivo = Archivos_Negocio.Consultar_Datos_Archivo(); //Consultamos los datos del archivo seleccionado
                if (Dt_Archivo != null && Dt_Archivo.Rows.Count > 0)
                {
                    String Usuario_Creo = Dt_Archivo.Rows[0]["Usuario_Creo"].ToString().Trim();
                    String Usuario_Loguedo = Cls_Sessiones.Nombre_Empleado;
                    if (Usuario_Creo.Equals(Cls_Sessiones.Nombre_Empleado.Trim()))
                    {
                        Elimina_Archivo_Tabla(No_Archivo); //Colocar el archivo en la tabla de eliminados
                    }
                    else
                    {
                        Mostrar_Informacion("Lo siento, solo el usuario que agrego el archivo a la requisición puede eliminar el archivo ", true);
                    }
                }
                else 
                {
                    Elimina_Archivo_Tabla(No_Archivo); //Colocar el archivo en la tabla de eliminados
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Seleccionar_Archivo_Click) " + ex.Message, true);
            }
        }

        protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Grid_Requisiciones();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Buscar_Click) " + ex.Message, true);
            }
        }
    #endregion
       
}