﻿<%@ Page Title="Catálogo Cotizadores" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Com_Imagenes.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Com_Imagenes" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            width: 644px;
        }
        .style2
        {
            width: 24%;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" />
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        
                <div id="Div_Cat_Com_Cotizadores_Botones" style="background-color:#ffffff; width:100%; height:100%;">                
                        <table id="Tbl_Comandos" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                        
                            <tr>
                                <td colspan="2" class="label_titulo">
                                    Cat&aacute;logo de Firma Imagenes
                                </td>
                            </tr>
                            
                            <tr>
                                <div id="Div_Contenedor_error" runat="server">
                                <td colspan="2">
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                                    <br />
                                    <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                                </td>
                                </div>
                            </tr>
                            
                            <tr class="barra_busqueda">
                                <td style="width:50%">
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    CssClass="Img_Button" onclick="Btn_Nuevo_Click"/>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" onclick="Btn_Modificar_Click"/>
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    CssClass="Img_Button" onclick="Btn_Salir_Click"/>
                                </td>
                                <td align="right" style="width:50%">                                    
                                </td>                        
                            </tr>
                            
                        </table>
                    </div>

            <div id="Div_Datos_Cotizador" runat="server">
                 
                <div id="Div_Cat_Com_Cotizadores_Controles" runat="server" style="background-color:#ffffff; width:100%; height:100%;">        
                       <table id="Datos Generales_Inner" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                            
                            <tr>
                                <td class="style2">
                                    
                                    Firma Jefe Compras</td>
                                <td>
                                    <cc1:AsyncFileUpload ID="AFil_Img_Compras" runat="server"
                                                    onuploadedcomplete="AFil_Img_Compras_UploadedComplete" />                                                    
                                </td>

                            </tr>
                            <tr>
                                <td class="style2">
                                    
                                    Firma Gerente Administración</td>
                                 <td>
                                    <cc1:AsyncFileUpload ID="AFil_Img_Gerente" runat="server" 
                                                    onuploadedcomplete="AFil_Img_Gerente_UploadedComplete" />      
                                 </td>
                               
                            </tr>
                            <tr>
                                <td class="style2">
                                    &nbsp;</td>                                
                                 <td style="width:18%">
                                     &nbsp;</td>
                                 <td> 
                                    <asp:ImageButton ID="Btn_Agregar" runat="server" CssClass="Img_Button" 
                                         ImageUrl="~/paginas/imagenes/paginas/sias_add.png" 
                                         onclick="Btn_Agregar_Click" />
                                 </td>    
                            </tr>
                           
                            </table>
                            
                    </div>
            </div>
            <div id="Div_Listado_Cotizadores" runat="server" style="background-color:#ffffff; width:100%; height:100%;">
                    <table id="Tbl_Grid_Cotizadores" border="0" cellspacing="0" class="estilo_fuente" style="width:98%;">                        
                            <tr>
                                <td align="center" class="style1">
                                &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="left" class="style1">
                                    <asp:GridView ID="Grid_Imagenes" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" 
                                        EmptyDataText="&quot;No se encontraron registros&quot;" GridLines="None" 
                                        onpageindexchanging="Grid_Imagenes_PageIndexChanging" 
                                        onselectedindexchanged="Grid_Imagenes_SelectedIndexChanged" PageSize="5" 
                                        Style="white-space:normal" Width="70%">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="Firma" HeaderText="Firma">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                <ItemStyle HorizontalAlign="Left" Width="25%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Ruta">
                                                <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                <ItemStyle HorizontalAlign="Left" ForeColor="Transparent" Font-Size="0px" Width="0%"/>
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                                <td align="left">
                                    <asp:Image ID="Img_Imagenes" runat="server" ImageUrl="" width="100px" height="100%" alt="" />
                                </td>
                            </tr>                        
                    </table>
            </div>
         </ContentTemplate> 
                 
    </asp:UpdatePanel>
          

</asp:Content>