﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Reporte_Requisiciones.Negocios;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;


public partial class paginas_Compras_Frm_Rpt_Com_Reporte_Requisiciones: System.Web.UI.Page
{

    ///*******************************************************************
    ///VARIABLES
    ///*******************************************************************
    #region Variables
    Cls_Rpt_Com_Reporte_Requisiciones_Negocio Requisicion_Negocio;

    #endregion

    ///*******************************************************************
    ///PAGE_LOAD
    ///*******************************************************************
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Configurar_Formulario("Inicio");
            //Inicializamos la variable de la clase de negocio
            Requisicion_Negocio = new Cls_Rpt_Com_Reporte_Requisiciones_Negocio();
            //Cargamos el grid de Requisiciones
            Llenar_Grid_Requisiciones();
        }        
    }


    #endregion 
    ///*******************************************************************
    ///METODOS
    ///*******************************************************************
    #region Metodos 

    public void Configurar_Formulario(String Estatus)
    {
        switch (Estatus)
        {
            case "Inicio":  
                Btn_Exportar_Excel.Visible = false;
                Btn_Exportar_PDF.Visible = false;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                break;
            case "General":
               
                Btn_Exportar_Excel.Visible = true;
                Btn_Exportar_PDF.Visible = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Listado";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                break;

        }
    }

    public void Habilitar_Formas(bool Estado)
    {
        
        
    }
    #region Metodos ModalPopUp

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combos
    ///DESCRIPCIÓN: Metodo que llena el combo Cmb_Estatus que esta dentro del ModalPopup
    ///PARAMETROS:  
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combos()
    {
        Cls_Rpt_Com_Reporte_Requisiciones_Negocio Obj = new Cls_Rpt_Com_Reporte_Requisiciones_Negocio();
        DataTable dt_dep = Obj.Consulta_Dependencias();
        Cls_Util.Llenar_Combo_Con_DataTable(Cmb_UR, dt_dep);
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Fecha
    ///DESCRIPCIÓN: Metodo que cambia el mes dic a dec para que oracle lo acepte
    ///PARAMETROS:  1.- String Fecha, es la fecha a la cual se le cambiara el formato 
    ///                     en caso de que cumpla la condicion del if
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 2/Septiembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public String Formato_Fecha(String Fecha)
    {

        String Fecha_Valida = Fecha;
        //Se le aplica un split a la fecha 
        String[] aux = Fecha.Split('/');
        //Se modifica el es a solo mayusculas para que oracle acepte el formato. 
        switch (aux[1])
        {
            case "dic":
                aux[1] = "DEC";
                break;
        }
        //Concatenamos la fecha, y se cambia el orden a DD-MMM-YYYY para que sea una fecha valida para oracle
        Fecha_Valida = aux[0] + "-" + aux[1] + "-" + aux[2];
        return Fecha_Valida;
    }// fin de Formato_Fecha

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Estatus_Busqueda
    ///DESCRIPCIÓN: Metodo que valida que seleccione un estatus dentro del modalpopup
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 27/Agosto/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public Cls_Rpt_Com_Reporte_Requisiciones_Negocio Validar_Estatus_Busqueda(Cls_Rpt_Com_Reporte_Requisiciones_Negocio Requisicion_Negocio)
    {

        return Requisicion_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Area
    ///DESCRIPCIÓN: Metodo que valida que seleccione un area dentro del modalpopup
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 27/Agosto/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public Cls_Rpt_Com_Reporte_Requisiciones_Negocio Validar_Dependencia(Cls_Rpt_Com_Reporte_Requisiciones_Negocio Requisicion_Negocio)
    {

     
        return Requisicion_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Area
    ///DESCRIPCIÓN: Metodo que valida que seleccione un area dentro del modalpopup
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 27/Agosto/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public Cls_Rpt_Com_Reporte_Requisiciones_Negocio Validar_Area(Cls_Rpt_Com_Reporte_Requisiciones_Negocio Requisicion_Negocio)
    {

      
        return Requisicion_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Carga_Componentes_Busqueda
    ///DESCRIPCIÓN: Metodo que carga e inicializa los componentes del ModalPopUp
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Carga_Componentes_Busqueda()
    {

    }
    #endregion

    public void Limpiar_Formas()
    {
        ///**********************************************************************************
        ///LLENAMOS LOS DATOS GENERALES
        ///**********************************************************************************
       
        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE HISTORIAL ESTATUS
        ///**********************************************************************************
        
        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE DETALLE DE PRODUCTOS
        ///**********************************************************************************
       

        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE DETALLE COMPRAS
        ///**********************************************************************************
       
        
        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE HISTORIAL DE COMENTARIOS
        ///**********************************************************************************
      
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Con_Sub(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {

        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/Rpt_Requisicion.pdf");
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/Rpt_Requisicion.pdf";
        Mostrar_Reporte("Rpt_Requisicion.pdf", "PDF");
        //ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Exportar_Excel
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.- Data_Set.- contiene la Mostrar_Informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene la Ruta del Reporte a mostrar en pantalla
    ///             4.- Nombre_Xls, nombre con el que se geradara en disco el archivo xls
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 17/MAYO/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Exportar_Excel(DataSet Data_Set, DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Xls)
    {

        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set;
        Reporte.SetDataSource(Ds_Reporte);
        //1
        ExportOptions Export_Options = new ExportOptions();
        //2
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        //3
        //4
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_Xls);
        //5
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        //6
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        //7
        Export_Options.ExportFormatType = ExportFormatType.Excel;
        //8
        Reporte.Export(Export_Options);
        //9
        String Ruta = "../../Reporte/" + Nombre_Xls;
       
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);

    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    ///*******************************************************************
    ///GRID
    ///*******************************************************************
    #region Grid

    #region Grid_Requisiciones
    public void Llenar_Grid_Requisiciones()
    {
        
    }
    protected void Grid_Requisiciones_SelectedIndexChanged(object sender, EventArgs e)
    {
       
        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE DETALLE COMPRAS
        ///**********************************************************************************
      
        ///**********************************************************************************
        ///LLENAMOS LA PESTAÑA DE HISTORIAL DE COMENTARIOS
        ///**********************************************************************************
       

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_PageIndexChanging
    ///DESCRIPCIÓN: Metodo que permite la paginacion del Grid_Requisicion
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Requisiciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
       
    }

    #endregion


    #endregion

    ///*******************************************************************
    ///EVENTOS
    ///*******************************************************************
    #region Eventos

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        switch (Btn_Salir.ToolTip)
        {
            case "Inicio":
                Btn_Salir.ToolTip = "Inicio";
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");

                break;
            case "Listado":
                Configurar_Formulario("Inicio");
                Limpiar_Formas();
                Requisicion_Negocio = new Cls_Rpt_Com_Reporte_Requisiciones_Negocio();
                Llenar_Grid_Requisiciones();

                break;
        }
    }


    #region Componentes ModalPopUp
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Fecha_CheckedChanged
    ///DESCRIPCIÓN: 
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Fecha_CheckedChanged(object sender, EventArgs e)
    {
      

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Estatus_CheckedChanged
    ///DESCRIPCIÓN: Evento del Check Estatus del ModalPOpUP
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Estatus_CheckedChanged(object sender, EventArgs e)
    {
      
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Dependencia_CheckedChanged
    ///DESCRIPCIÓN: evento del Check Areas en el ModalPopUp
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Dependencia_CheckedChanged(object sender, EventArgs e)
    {
      
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Dependencia_SelectedIndexChanged
    ///DESCRIPCIÓN: evento del Cmb_Dependencias en el ModalPopUp
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Dependencia_SelectedIndexChanged(object sender, EventArgs e)
    {
       
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Area_CheckedChanged
    ///DESCRIPCIÓN: evento del Check Areas en el ModalPopUp
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Area_CheckedChanged(object sender, EventArgs e)
    {
       
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Avanzada_Click
    ///DESCRIPCIÓN: Evento del boton busqueda avanzada 
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Avanzada_Click(object sender, EventArgs e)
    {
          

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Cerrar_Click
    ///DESCRIPCIÓN: Evento del Boton de Cerrar, el cual oculta el div de busueda de productos y muestra el 
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Cerrar_Click(object sender, EventArgs e)
    {
     

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Aceptar_Click
    ///DESCRIPCIÓN: Evento del boton Aceptar del ModalPopUp
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 10/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Aceptar_Click(object sender, EventArgs e)
    {
       

    }
    #endregion

    protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
    {
        //Juntamos los 4 datatable en un solo data set
        DataSet Ds_Reporte_Requisicion = new DataSet();
        DataTable Dt_Requisiciones = (DataTable)Session["Dt_Requisiciones"];
        DataTable Dt_Productos = (DataTable)Session["Dt_Productos"];
        DataTable Dt_Compras = (DataTable)Session["Dt_Compras"];
        DataTable Dt_Comentarios = (DataTable)Session["Dt_Comentarios"];
        Dt_Requisiciones.TableName = "Dt_Requisicion";
        Dt_Productos.TableName = "Dt_Prod";
        Dt_Compras.TableName = "Dt_Compras";
        Dt_Comentarios.TableName = "Dt_Comentarios";

        Ds_Reporte_Requisicion.Tables.Add(Dt_Requisiciones.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Productos.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Compras.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Comentarios.Copy());

        Ds_Ope_Rpt_Requisiciones Ds_Objeto_Data_Set = new Ds_Ope_Rpt_Requisiciones();
        Generar_Reporte_Con_Sub(Ds_Reporte_Requisicion, Ds_Objeto_Data_Set, "Rpt_Com_Requisiciones.rpt");
        //limpiamos variables de sesion
        Session["Dt_Requisiciones"] = null;
        Session["Dt_Productos"] = null;
        Session["Dt_Compras"] = null;
        Session["Dt_Comentarios"] = null;
        Configurar_Formulario("Inicio");
        Limpiar_Formas();
    }
    protected void Btn_Exportar_Excel_Click(object sender, EventArgs e)
    {
        //Juntamos los 4 datatable en un solo data set
        DataSet Ds_Reporte_Requisicion = new DataSet();
        DataTable Dt_Requisiciones = (DataTable)Session["Dt_Requisiciones"];
        DataTable Dt_Productos = (DataTable)Session["Dt_Productos"];
        DataTable Dt_Compras = (DataTable)Session["Dt_Compras"];
        DataTable Dt_Comentarios = (DataTable)Session["Dt_Comentarios"];
        Dt_Requisiciones.TableName = "Dt_Requisicion";
        Dt_Productos.TableName = "Dt_Prod";
        Dt_Compras.TableName = "Dt_Compras";
        Dt_Comentarios.TableName = "Dt_Comentarios";

        Ds_Reporte_Requisicion.Tables.Add(Dt_Requisiciones.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Productos.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Compras.Copy());
        Ds_Reporte_Requisicion.Tables.Add(Dt_Comentarios.Copy());

        Ds_Ope_Rpt_Requisiciones Ds_Objeto_Data_Set = new Ds_Ope_Rpt_Requisiciones();
        Exportar_Excel(Ds_Reporte_Requisicion, Ds_Objeto_Data_Set, "Rpt_Com_Requisiciones.rpt", "Rpt_Requisicion.xls");
        //limpiamos variables de sesion
        Session["Dt_Requisiciones"] = null;
        Session["Dt_Productos"] = null;
        Session["Dt_Compras"] = null;
        Session["Dt_Comentarios"] = null;
        Configurar_Formulario("Inicio");
        Limpiar_Formas();
    }

    protected void Btn_Buscar_Click(object sender, EventArgs e)
    {
        Cls_Rpt_Com_Reporte_Requisiciones_Negocio Requisicion_Negocio = new Cls_Rpt_Com_Reporte_Requisiciones_Negocio();
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        
       
        Requisicion_Negocio = new Cls_Rpt_Com_Reporte_Requisiciones_Negocio();
    }

    #endregion


}
