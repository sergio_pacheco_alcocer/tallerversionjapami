<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Ope_Com_Distribuir_Requisiciones_Prov.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Distribuir_Requisiciones_Prov" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script type="text/javascript" language="javascript">
    function calendarShown(sender, args) {
        sender._popupBehavior._element.style.zIndex = 10000005;
    }

    function switchViews(obj, row) {
        var div = document.getElementById(obj);
        var img = document.getElementById('img' + obj);

        if (div.style.display == "none") {
            div.style.display = "inline";
            if (row == 'alt') {
                img.src = "../imagenes/paginas/stocks_indicator_down.png";
            }
            else {
                img.src = "../imagenes/paginas/stocks_indicator_down.png";
            }
            img.alt = "Close to view other customers";
        }
        else {
            div.style.display = "none";
            if (row == 'alt') {
                img.src = "../imagenes/paginas/add_up.png";
            }
            else {
                img.src = "../imagenes/paginas/add_up.png";
            }
            img.alt = "Expand to show orders";
        }
    }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesi�n activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Div de Contenido --%>
        <div id="Div_Contenido" style="width:97%;height:100%;">
        <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
            <tr>
                <td class="label_titulo">Distribuir Requisiciones a Proveedores</td>
            </tr>
            <%--Fila de div de Mensaje de Error --%>
            <tr>
                <td>
                    <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                    <table style="width:100%;">
                        <tr>
                            <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                            Width="24px" Height="24px"/>
                            </td>            
                            <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                            </td>
                        </tr> 
                    </table>                   
                    </div>
                </td>
            </tr>
            <%--Fila de Busqueda y Botones Generales --%>
            <tr class="barra_busqueda">
                <td style="width:20%;">
                    <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:ImageButton ID="Btn_Nuevo" runat="server" CssClass="Img_Button" 
                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                        ToolTip="Nuevo" onclick="Btn_Nuevo_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                            onclick="Btn_Salir_Click"/>
                        <asp:ImageButton ID="Btn_Imprimir" runat="server" ToolTip="Imprimir" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/gridview/grid_print.png" 
                            onclick="Btn_Imprimir_Click"/>
                        <asp:ImageButton ID="Btn_Cancelar" runat="server" ToolTip="Cancelar" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" onclick="Btn_Cancelar_Click" 
                            />                                
                    </ContentTemplate>
                    </asp:UpdatePanel>
                </td>
            </tr>
            <tr>
                <td>
                    <div id="Div_Grid_Requisiciones" style="width:100%;height:100%;" runat="server">
                        <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td style="width: 10%;">
                                    Cotizador(a)
                                </td>
                                <td style="width: 50%;">
                                    <asp:DropDownList ID="Cmb_Cotizadores" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Cotizadores_SelectedIndexChanged"
                                        AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 10%;">
                                    No. Requisici�n
                                </td>
                                <td >
                                    <asp:TextBox ID="Txt_No_Requisicion" runat="server" 
                                        ontextchanged="Txt_No_Requisicion_TextChanged" AutoPostBack="true" ></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    ToolTip="Buscar" onclick="Btn_Buscar_Click" />
                                </td>
                            </tr>  
                        </table>
                        <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                            <tr>
                                <td>
                                <div ID="Div_1" runat="server" style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                                    <asp:GridView ID="Grid_Requisiciones" runat="server"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                    onselectedindexchanged="Grid_Requisiciones_SelectedIndexChanged" 
                                    Width="99%" Enabled ="False" DataKeyNames="No_Requisicion"
                                    AllowSorting="True" OnSorting="Grid_Requisiciones_Sorting" OnRowDataBound="Grid_Requisiciones_RowDataBound"
                                    HeaderStyle-CssClass="tblHead" >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" Text="Ver Requisicion" HeaderText="Ver"
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <a href="javascript:switchViews('div<%# Eval("No_Requisicion") %>', 'one');">
                                                        <img id="imgdiv<%# Eval("No_Requisicion") %>" alt="Click to show/hide orders" border="0"
                                                            src="../imagenes/paginas/add_up.png" />
                                                    </a>
                                                </ItemTemplate>
                                                <AlternatingItemTemplate>
                                                    <a href="javascript:switchViews('div<%# Eval("No_Requisicion") %>', 'alt');">
                                                        <img id="imgdiv<%# Eval("No_Requisicion") %>" alt="Click to show/hide orders" border="0"
                                                            src="../imagenes/paginas/add_up.png" />
                                                    </a>
                                                </AlternatingItemTemplate>
                                                <ItemStyle Width="1%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="No_Requisicion" HeaderText="No_Requisicion" Visible="false">
                                                <FooterStyle HorizontalAlign="Right" />
                                                <HeaderStyle HorizontalAlign="Right" />
                                                <ItemStyle HorizontalAlign="Right" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Folio" HeaderText="Folio" Visible="true" SortExpression="Folio" ItemStyle-Wrap="true">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Dependencia" HeaderText="U.Responsable" SortExpression="Dependencia" ItemStyle-Wrap="true"
                                                Visible="True">
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Left" Width="25%" Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Concepto" HeaderText="Partida" Visible="True" SortExpression="Concepto" 
                                            ItemStyle-Wrap="true">
                                                <FooterStyle HorizontalAlign="Left" Width="25%" Wrap="true"/>
                                                <HeaderStyle HorizontalAlign="Left" Width="25%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Left" Width="25%" Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipo_Articulo" HeaderText="Tipo Articulo" Visible="True" SortExpression="Tipo_Articulo" 
                                            ItemStyle-Wrap="true">
                                                <FooterStyle HorizontalAlign="Left" Width="10%" Wrap="true"/>
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Left" Width="10%"  Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True" SortExpression="Estatus" 
                                            ItemStyle-Wrap="true">
                                                <FooterStyle HorizontalAlign="Left" Width="10%" Wrap="true"/>
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Left" Width="10%"  Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Total" HeaderText="Total" DataFormatString="{0:c}" 
                                                Visible="True" SortExpression="Total" ItemStyle-Wrap="true">
                                                <FooterStyle HorizontalAlign="Right" Width="15%" Wrap="true"/>
                                                <HeaderStyle HorizontalAlign="Right" Width="15%" Wrap="true"/>
                                                <ItemStyle HorizontalAlign="Right" Width="15%" Wrap="true" Font-Size="X-Small"/>
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ALERTA" HeaderText="Alerta" Visible="false" SortExpression="Estatus">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>

                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Alerta" Visible="false" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_grey.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                            </asp:TemplateField>  
                                            <asp:TemplateField>
                                        <ItemTemplate>
                                            <tr>
                                                <td colspan="7">
                                                    <div id="div<%# Eval("No_Requisicion") %>" style="display: none; position: relative;
                                                        left: 25px;">
                                                        <asp:GridView ID="Grid_Requisicion_Detalle" runat="server" AutoGenerateColumns="False"
                                                            CssClass="GridView_1" GridLines="None" Width="102%">
                                                            <Columns>
                                                                <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" Visible="True">
                                                                    <HeaderStyle Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Nombre" HeaderText="Producto/Servicio" SortExpression="Nombre"
                                                                    Visible="True">
                                                                    <HeaderStyle HorizontalAlign="Left" Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion"
                                                                    Visible="false">
                                                                    <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" SortExpression="Cantidad"
                                                                    Visible="True">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Monto" HeaderText="Importe S/I" SortExpression="Monto"
                                                                    DataFormatString="{0:c}">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="Monto_Total" HeaderText="Importe C/I" SortExpression="Monto_Total"
                                                                    DataFormatString="{0:c}">
                                                                    <HeaderStyle HorizontalAlign="Right" Font-Size="XX-Small" />
                                                                    <ItemStyle HorizontalAlign="Right" Width="12%" Font-Size="X-Small" />
                                                                </asp:BoundField>
                                                            </Columns>
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                            <HeaderStyle BackColor="#2F4E7D" ForeColor="White" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <RowStyle CssClass="GridItem" />
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                         
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                <div ID="Div_Detalle_Requisicion" runat="server" style="width:100%;font-size:9px;" 
                    visible="false">
                    <table width="99%">
                        <tr>
                            <td align="center" colspan="4">
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Folio</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Folio" runat="server" Enabled="False" Width="99%"></asp:TextBox>
                            </td>
                            <td style="width:18%">Fecha Autorizacion</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Fecha_Generacion" runat="server" Enabled="False" 
                                Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Unidad Responsable</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Dependencia" runat="server" Enabled="False" Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Partida</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Concepto" runat="server" Enabled="False" Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Tipo</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Tipo" runat="server" Enabled="False" Width="99%"></asp:TextBox>
                            </td>
                            <td style="width:18%">Tipo Articulo</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Tipo_Articulo" runat="server" Enabled="false" 
                                Width="99%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Estatus</td>
                            <td style="width:32%">
                                <asp:TextBox ID="Txt_Estatus" runat="server" Enabled="false" 
                                Width="99%"></asp:TextBox>
                            </td>
                            <td colspan="2">
                                <asp:CheckBox ID="Chk_Verificacion" runat="server" Enabled="false" 
                                Text="Verificar las caracter�sticas, garant�as y p�lizas" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:18%">Justificaci�n de la Compra</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Justificacion" runat="server" Enabled="False" Height="80px"
                                TabIndex="10" TextMode="MultiLine" Width="99%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" 
                                TargetControlID="Txt_Justificacion" WatermarkCssClass="watermarked" 
                                WatermarkText="&lt;Indica el motivo de realizar la requisici�n&gt;" />
                            </td>
                        </tr>
                        <tr style="display:none;">
                            <td style="width:18%">Especificaciones Adicionales</td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Especificacion" runat="server" Enabled="False" 
                                TabIndex="10" TextMode="MultiLine" Width="99%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" 
                                TargetControlID="Txt_Especificacion" WatermarkCssClass="watermarked" 
                                WatermarkText="&lt;Especificaciones de los productos&gt;" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:Panel ID="Pnl_Listado_Bienes" runat="server" Width="99%" GroupingText=" Listado de Bienes para el Servicio " Visible="false">
                                    <asp:GridView ID="Grid_Listado_Bienes" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        GridLines="Both" PageSize="25" Width="100%" EmptyDataText="No hay Bienes seleccionados para esta Requisici�n"
                                        Style="font-size: xx-small; white-space: normal" >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID" >
                                                <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inv." >
                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" > 
                                                <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                                <ItemStyle HorizontalAlign="Center" Width="30%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NUMERO_SERIE" HeaderText="Serie" Visible="True">
                                                <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                                <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                    <br />
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                 <cc1:TabContainer ID="Tab_Detalle_Requisicion" runat="server" 
                                    ActiveTabIndex="0" Width="98%">
                                    <cc1:TabPanel ID="TabPnl_Productos" runat="server" Visible="true" Width="99%">
                                    <HeaderTemplate>Productos/Servicios</HeaderTemplate>
                                    <ContentTemplate>
                                    <table width="99%">
                                        <tr> 
                                             <td align="center">
                                                 <asp:GridView ID="Grid_Productos" runat="server"
                                                     AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                                     Width="95%"
                                                     AllowSorting="True" OnSorting="Grid_Productos_Sorting" >
                                                     <Columns>
                                                         <asp:BoundField DataField="Clave" HeaderText="Clave" SortExpression="Clave" >
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Nombre" HeaderText="Producto/Servicio" 
                                                             SortExpression="Nombre">
                                                             <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" 
                                                             SortExpression="Descripcion">
                                                             <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                          <asp:BoundField DataField="Unidad" HeaderText="Unidad" 
                                                             SortExpression="Unidad">
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" 
                                                             SortExpression="Cantidad">
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Monto_Total" HeaderText="Precio Acumulado" 
                                                             SortExpression="Monto_Total">
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Monto" HeaderText="Importe S/I" 
                                                             SortExpression="Monto">
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                     </Columns>
                                                     <SelectedRowStyle CssClass="GridSelected" />
                                                     <HeaderStyle CssClass="tblHead" />
                                                     <PagerStyle CssClass="GridHeader" />
                                                     <AlternatingRowStyle CssClass="GridAltItem" />
                                                 </asp:GridView>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td align="right">
                                                 Subtotal
                                                 <asp:TextBox ID="Txt_Subtotal" runat="server" Enabled="False" 
                                                     Style="text-align: right"></asp:TextBox>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td align="right">
                                                 IEPS
                                                 <asp:TextBox ID="Txt_IEPS" runat="server" Enabled="False" 
                                                     Style="text-align: right"></asp:TextBox>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td align="right">
                                                 IVA
                                                 <asp:TextBox ID="Txt_IVA" runat="server" Enabled="False" 
                                                     Style="text-align: right"></asp:TextBox>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td align="right">
                                                 Total
                                                 <asp:TextBox ID="Txt_Total" runat="server" Style="text-align: right" 
                                                     Enabled="False"></asp:TextBox>
                                             </td>
                                        </tr>
                                        <tr>
                                             <td align="left">
                                                 <hr class="linea" />
                                             </td>
                                        </tr>
                                    
                                    </table>
                                    </ContentTemplate>
                                    </cc1:TabPanel>
                                    <cc1:TabPanel ID="TabPnl_Proveedores" runat="server" Visible="true" Width="99%">
                                    <HeaderTemplate>Proveedores</HeaderTemplate>
                                    <ContentTemplate>
                                    <table width="99%">
                                        <tr>
                                            <td>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="99%">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        
                                        <tr>
                                            <td colspan="2"><asp:CheckBox ID="Chk_Enviar_Correo" runat="server" Text="Enviar Correo"/>
                                            </td>
                                            <td colspan="2" align="right">
                                                <asp:ImageButton ID="Btn_Agregar_Proveedor" runat="server" 
                                                ToolTip="Agregar Proveedor" CssClass="Img_Button" 
                                                ImageUrl="~/paginas/imagenes/paginas/sias_add.png" 
                                                    onclick="Btn_Agregar_Proveedor_Click" /> 
                                            </td>
                                        </tr>
                                        <tr> 
                                             <td align="center" colspan="4">
                                                 <asp:GridView ID="Grid_Proveedores" runat="server"
                                                     AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                                     Width="95%" DataKeyNames="Proveedor_ID" onselectedindexchanged="Grid_Proveedores_SelectedIndexChanged"
                                                     AllowSorting="True" OnSorting="Grid_Proveedores_Sorting" >
                                                     <Columns>
                                                        <asp:ButtonField ButtonType="Image" CommandName="Select" Text="Ver Requisicion" HeaderText="Borrar"
                                                            ImageUrl="~/paginas/imagenes/paginas/delete.png">
                                                            <ItemStyle Width="5%" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="Proveedor_ID" HeaderText="PROVEEDOR_ID" 
                                                             Visible="False">
                                                            <FooterStyle HorizontalAlign="Right" />
                                                            <HeaderStyle HorizontalAlign="Right" />
                                                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small"/>
                                                        </asp:BoundField>
                                                         <asp:BoundField DataField="Nombre" HeaderText="Raz�n Social" SortExpression="Nombre" >
                                                             <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Compania" HeaderText="Nombre Comercial" 
                                                             SortExpression="Compania">
                                                             <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="Telefonos" HeaderText="Telefonos" 
                                                             SortExpression="Telefonos">
                                                             <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                         <asp:BoundField DataField="E_MAIL" HeaderText="Correo" 
                                                             SortExpression="E_MAIL">
                                                             <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                         </asp:BoundField>
                                                     </Columns>
                                                     <SelectedRowStyle CssClass="GridSelected" />
                                                     <HeaderStyle CssClass="tblHead" />
                                                     <PagerStyle CssClass="GridHeader" />
                                                     <AlternatingRowStyle CssClass="GridAltItem" />
                                                 </asp:GridView>
                                             </td>
                                        </tr>
                                       
                                        
                                    </table>
                                    
                                    </ContentTemplate>
                                    </cc1:TabPanel>
                                    
                                     <cc1:TabPanel ID="Tab_Comentarios" runat="server" Visible="true" Width="99%">
                                    <HeaderTemplate>Comentarios</HeaderTemplate>
                                    <ContentTemplate>
                                    <table  width="99%">
                                    <tr>
                                        <td>
                                        <asp:GridView ID="Grid_Comentarios" runat="server"
                                           AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                           Width="95%">
                                           <RowStyle CssClass="GridItem" />
                                           <Columns>
                                               <asp:BoundField DataField="Comentario" HeaderText="Comentarios" Visible="True">
                                                   <HeaderStyle HorizontalAlign="Left" Width="50%" Font-Size="X-Small"/>
                                                   <ItemStyle HorizontalAlign="Left" Width="50%" Font-Size="X-Small"/>
                                               </asp:BoundField>
                                               <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True">
                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                   <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                               </asp:BoundField>
                                               <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha" Visible="True">
                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                   <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                               </asp:BoundField>
                                               <asp:BoundField DataField="Usuario_Creo" HeaderText="Usuario" Visible="True">
                                                   <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                   <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                               </asp:BoundField>
                                           </Columns>
                                           <PagerStyle CssClass="GridHeader" />
                                           <SelectedRowStyle CssClass="GridSelected" />
                                           <HeaderStyle CssClass="GridHeader" />
                                           <AlternatingRowStyle CssClass="GridAltItem" />
                                       </asp:GridView>
                                        </td>
                                    </tr>
                                    
                                    </table>
                                    
                                    </ContentTemplate>
                                    </cc1:TabPanel>
                                    
                                </cc1:TabContainer>
                            </td>
                        </tr>
                        
                    </table>
                </div>
                </td>
            </tr>
        </table>
        </div>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
