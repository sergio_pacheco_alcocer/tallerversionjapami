﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Rpt_Com_Orden_Servicio.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Com_Orden_Servicio" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
        function Limpiar_Filtros() {
            document.getElementById("<%=Txt_Fecha_Inicial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Final.ClientID%>").value = "";
            document.getElementById("<%=Hdf_Proveedor_ID.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Unidad_Responsable.ClientID%>").value = "";
            document.getElementById("<%=Txt_Proveedor.ClientID%>").value = "";
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Contenido" style="width: 99%; height:300px;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="100%">
                    <tr>
                        <td class="label_titulo">
                            Reporte Ordenes de Servicio
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" style="font-size: 12px; color: Red; font-family: Tahoma; text-align: left;">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td style="width: 20%">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" /><%--onclick="Btn_Salir_Click"--%>
                            <asp:ImageButton ID="Btn_Limpiar" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/sias_clear.png"
                                OnClientClick="javascript:return Limpiar_Filtros();" ToolTip="Limpiar Formulario" />
                            <asp:ImageButton ID="Btn_Exportar_PDF" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                ToolTip="Exportar PDF" OnClick="Btn_Generar_Reporte_Click" AlternateText="Consultar" />
                            <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                ToolTip="Exportar PDF" OnClick="Btn_Exportar_Excel_Click" AlternateText="Consultar" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <table style="width: 100%;">
                                <tr>
                                    <td style="width:18%;">
                                        Unidad Responsable
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Unidad_Responsable" runat="server" Width="100%"></asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:18%;">
                                        <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                                        Proveedor
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Proveedor" runat="server" Width="95%" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Buscar_Proveedor" runat="server" ToolTip="Seleccionar Proveedor" Height="16px" Width="16px" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" OnClick="Btn_Buscar_Proveedor_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:18%;">
                                        Fecha Inicio
                                    </td>
                                    <td style="width:32%;">
                                        <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                            ToolTip="Seleccione la Fecha" Enabled="true" />
                                        <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                            OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Inicio" TargetControlID="Txt_Fecha_Inicial" />
                                    </td>
                                    <td style="width:18%;">
                                        Fecha Final
                                    </td>
                                    <td style="width:32%;">
                                        <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                            ToolTip="Seleccione la Fecha" Enabled="true" />
                                        <cc1:CalendarExtender ID="Txt_Fecha_Final_CalendarExtender" runat="server" Format="dd/MMM/yyyy"
                                            OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Fin" TargetControlID="Txt_Fecha_Final" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Mpe_Proveedores" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Proveedores_Cabecera" runat="server" TargetControlID="Btn_Comodin_Mpe_Proveedores"
                PopupControlID="Pnl_Mpe_Proveedores" CancelControlID="Btn_Cerrar_Mpe_Proveedores"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Proveedores" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Proveedores_Interno" runat="server" CssClass="estilo_fuente"
            Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
            border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Img_Mpe_Proveedores" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda y Selección de Proveedores
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Proveedores" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Proveedores" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Proveedores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <asp:Panel ID="Pnl_Busqueda_Proveedoress" runat="server" Width="98%" DefaultButton="Btn_Ejecutar_Busqueda_Proveedores">
                            <table width="100%">
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Proveedores_Buscar" runat="server" CssClass="estilo_fuente"
                                            Text="Nombre" Style="font-weight: bolder;"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Buscar" runat="server" Width="92%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            TargetControlID="Txt_Nombre_Proveedor_Buscar" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            Enabled="True" TargetControlID="Txt_Nombre_Proveedor_Buscar" WatermarkCssClass="watermarked"
                                            WatermarkText="<-- Razón Social ó Nombre Comercial -->">
                                        </cc1:TextBoxWatermarkExtender>
                                        <asp:ImageButton ID="Btn_Ejecutar_Busqueda_Proveedores" runat="server" OnClick="Btn_Ejecutar_Busqueda_Proveedores_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="Pnl_Listado_Proveedores" runat="server" ScrollBars="Vertical" Style="white-space: normal;"
                            Width="100%" BorderColor="#3366FF" Height="330px">
                            <asp:GridView ID="Grid_Listado_Proveedores" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Listado_Proveedores_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Proveedores_SelectedIndexChanged" PageSize="150"
                                Width="100%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PROVEEDOR_ID" HeaderText="Proveedor ID" SortExpression="PROVEEDOR_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Razón Social" SortExpression="NOMBRE">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMPANIA" HeaderText="Nombre Comercial" SortExpression="COMPANIA">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
