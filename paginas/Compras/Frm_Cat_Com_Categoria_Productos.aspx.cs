﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Sessiones;
using JAPAMI.Categoria_Productos.Negocio;

public partial class paginas_Compras_Frm_Cat_Com_Categoria_Productos : System.Web.UI.Page
{
    #region (Variables)
    private const String P_Dt_Categoria_Productos = "Dt_Categoria_Productos";
    #endregion

    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Estado_Inicial();
            }
            else
            {
                Mostrar_Mensaje_Error("", false);
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Page_Load) " + ex.Message, true);
        }
    }
    #endregion

    #region (Metodos)
    private void Mostrar_Mensaje_Error(String Mensaje, Boolean Mostrar)
    {
        try
        {
            Lbl_Error.Text = Mensaje;
            Lbl_Error.Visible = Mostrar;
            Img_Error.Visible = Mostrar;
        }
        catch (Exception ex)
        {
            Lbl_Error.Text = ex.Message;
            Lbl_Error.Visible = true;
            Img_Error.Visible = true;
        }
    }

    private void Estado_Inicial()
    {
        try
        {
            Limpiar_Controles();
            Elimina_Sesiones();
            Habilita_Controles("Inicial");
            Llena_Grid_Categoria_Productos(1, "");
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Limpiar_Controles()
    {
        try
        {
            Txt_Categoria_ID.Text = "";
            Txt_Comentarios.Text = "";
            Txt_Nombre.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Elimina_Sesiones()
    {
        try
        {
            HttpContext.Current.Session.Remove(P_Dt_Categoria_Productos);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Habilita_Controles(String Modo)
    {
        try
        {
            //Seleccionar el modo
            switch (Modo)
            {
                default:
                    Txt_Busqueda.Enabled = true;
                    Txt_Categoria_ID.Enabled = false;
                    Txt_Nombre.Enabled = false;
                    Txt_Comentarios.Enabled = false;
                    Cmb_Estatus.Enabled = false;
                    Grid_Categoria_Productos.Enabled = true;
                    Btn_Busqueda.Enabled = true;
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.Visible = true;
                    Btn_Eliminar.Enabled = true;
                    Lbl_Error.Visible = false;
                    Img_Error.Visible = false;
                    break;

                case "Nuevo":
                case "Modificar":
                    Txt_Busqueda.Enabled = false;
                    Txt_Nombre.Enabled = true;
                    Txt_Comentarios.Enabled = true;
                   
                    Grid_Categoria_Productos.Enabled = false;
                    Btn_Busqueda.Enabled = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Eliminar.Enabled = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Salir.ToolTip = "Cancelar";

                    //verificar si es el nuevo 
                    if (Modo == "Nuevo")
                    {
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = false;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Nuevo.ToolTip = "Aceptar";
                        Cmb_Estatus.Enabled = true;
                    }
                    else
                    {
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Modificar.ToolTip = "Guardar";
                        Cmb_Estatus.Enabled = true;
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private String Validacion_Datos()
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        String aux = String.Empty; //variable auxiliar

        try
        {
            //llenar la variable auxiliar
            aux = "<table border='0' style='estilo_fuente'>" +
                "<tr><td colspan='2'>Es necesario ingresar:</td></tr>" +
                "<tr><td>+Nombre</td><td>+Estatus</td></tr></table>";
            
            //verificar que esten todos los datos
            if (String.IsNullOrEmpty(Txt_Nombre.Text.Trim()) == true)
            {
                Resultado = aux;
            }

            if (Cmb_Estatus.SelectedIndex <= 0)
            {
                Resultado = aux;
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Grid)
    private void Llena_Grid_Categoria_Productos(int Pagina, String Busqueda)
    {
        //Declaracion de variables
        Cls_Cat_Com_Categoria_Productos_Negocio Categoria_Productos_Negocio = new Cls_Cat_Com_Categoria_Productos_Negocio(); //variable para la capa de negocios
        DataTable Dt_Categoria_Productos = new DataTable(); //tabla para el llenado del Grid
        
        try
        {
            //verificar si la variable de sesion tiene valor
            if (HttpContext.Current.Session[P_Dt_Categoria_Productos] == null)
            {
                //Consultar las categorias
                Categoria_Productos_Negocio.P_Busqueda = Busqueda;
                Dt_Categoria_Productos = Categoria_Productos_Negocio.Consulta_Categoria_Productos();
            }
            else
            {
                
                //Colocar la variable de sesion en la tabla
                Dt_Categoria_Productos= ((DataTable)HttpContext.Current.Session[P_Dt_Categoria_Productos]);
            }

            //Llenar el grid
            Grid_Categoria_Productos.DataSource = Dt_Categoria_Productos;
            
            //verificar si hay pagina
            if (Pagina > -1)
            {
                Grid_Categoria_Productos.PageIndex = Pagina;
            }

            Grid_Categoria_Productos.DataBind();

            //Colocar la tabla en la variable de sesion
            HttpContext.Current.Session[P_Dt_Categoria_Productos] = Dt_Categoria_Productos;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Alta_Categoria_Producto()
    {
        //Declaracion de variables
        Cls_Cat_Com_Categoria_Productos_Negocio Categoria_Productos_Negocio = new Cls_Cat_Com_Categoria_Productos_Negocio(); //variable para la capa de negocios

        try
        {
            //Asignar propiedades
            Categoria_Productos_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
            Categoria_Productos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;

            //Verificar la longitud para los comentarios
            if (Txt_Comentarios.Text.Trim().Length > 100)
            {
                Categoria_Productos_Negocio.P_Descripcion = Txt_Comentarios.Text.Trim().Substring(0, 100);
            }
            else
            {
                Categoria_Productos_Negocio.P_Descripcion = Txt_Comentarios.Text.Trim();
            }

            Categoria_Productos_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;

            //dar de alta la categioria
            Categoria_Productos_Negocio.Alta_Categoria_Productos();
            Estado_Inicial();           
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Baja_Categoria_Productos()
    {
        //Declaracion de variables
        Cls_Cat_Com_Categoria_Productos_Negocio Categoria_Productos_Negocio = new Cls_Cat_Com_Categoria_Productos_Negocio(); //variable para la capa de negocios

        try
        {
            //Asignar propiedades
            Categoria_Productos_Negocio.P_Categoria_ID = Txt_Categoria_ID.Text.Trim();

            //Dar de baja la categoria
            Categoria_Productos_Negocio.Baja_Categoria_Productos();
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    private void Cambio_Categoria_Productos()
    {
        //Declaracion de variables
        Cls_Cat_Com_Categoria_Productos_Negocio Categoria_Productos_Negocio = new Cls_Cat_Com_Categoria_Productos_Negocio(); //variable para la capa de negocios

        try
        {
            //Asignar propiedades
            Categoria_Productos_Negocio.P_Categoria_ID = Txt_Categoria_ID.Text.Trim();
            Categoria_Productos_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
            Categoria_Productos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;

            //Verificar la longitud para los comentarios
            if (Txt_Comentarios.Text.Trim().Length > 100)
            {
                Categoria_Productos_Negocio.P_Descripcion = Txt_Comentarios.Text.Trim().Substring(0, 100);
            }
            else
            {
                Categoria_Productos_Negocio.P_Descripcion = Txt_Comentarios.Text.Trim();
            }

            Categoria_Productos_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;

            //Modificar la categoria del producto
            Categoria_Productos_Negocio.Cambio_Categoria_Productos();
            Estado_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Eventos)      
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion

        try
        {
            //verificar el tooltip
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Habilita_Controles("Nuevo");
            }
            else
            {
                //Verificar la validacion
                Validacion = Validacion_Datos();
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    Alta_Categoria_Producto();
                }
                else
                {
                    Mostrar_Mensaje_Error(Validacion, true);
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Btn_Nuevo_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion

        try
        {
            //verificar el tooltip
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                if (Grid_Categoria_Productos.SelectedIndex > (-1))
                {
                    Habilita_Controles("Modificar");
                }
                else 
                {
                    Validacion = "<table border='0' style='estilo_fuente'>" +
                "<tr><td colspan='2'>Por favor, seleccione una categoria</td></tr>" +
                "</table>";
                    Mostrar_Mensaje_Error(Validacion, true);
                }
                
            }
            else
            {
                //Verificar la validacion
                Validacion = Validacion_Datos();
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    Cambio_Categoria_Productos();
                }
                else
                {
                    Mostrar_Mensaje_Error(Validacion, true);
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Btn_Modificar_Click) " + ex.Message, true);
        }
    }
    
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Categoria_Productos.SelectedIndex > -1)
                Baja_Categoria_Productos();
            else
                Mostrar_Mensaje_Error("Seleccione la categoría que desea eliminar </br>", true);

        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Btn_Eliminar_Click) " + ex.Message, true);
        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            //verificar el tooltip
            if (Btn_Modificar.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Inicial();
            }
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Btn_Salir_Click) " + ex.Message, true);
        }
    }
    protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
    {
        Elimina_Sesiones();
        Llena_Grid_Categoria_Productos(1, Txt_Busqueda.Text.ToString());
    }
    #endregion
    protected void Grid_Categoria_Productos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Txt_Categoria_ID.Text = Grid_Categoria_Productos.SelectedRow.Cells[4].Text.ToString();
            Txt_Nombre.Text = HttpUtility.HtmlDecode( Grid_Categoria_Productos.SelectedRow.Cells[1].Text.ToString());
            Txt_Comentarios.Text = HttpUtility.HtmlDecode(Grid_Categoria_Productos.SelectedRow.Cells[3].Text.ToString());
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Grid_Categoria_Productos.SelectedRow.Cells[2].Text.ToString()));
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Grid_Categoria_Productos_SelectedIndexChanged) " + ex.Message, true);
        }
    }

    protected void Grid_Categoria_Productos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Llena_Grid_Categoria_Productos(e.NewPageIndex, "");
        }
        catch (Exception ex)
        {
            Mostrar_Mensaje_Error("Error: (Grid_Categoria_Productos_PageIndexChanging) " + ex.Message, true);
        }
    }
  
}