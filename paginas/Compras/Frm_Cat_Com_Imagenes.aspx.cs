﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.IO;
using System.Web.UI.WebControls;
using JAPAMI.Correo.Negocio;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data;
using JAPAMI.Catalogo_SAP_Conceptos.Negocio;
using System.Text.RegularExpressions;

public partial class paginas_Compras_Frm_Cat_Com_Imagenes : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Grid_Cotizador = 2;
        private const int Const_Estado_Modificar = 3;

        private static DataTable Dt_Imagenes = new DataTable();    
    #endregion

    #region Page Load / Init

    protected void Page_Load(object sender, EventArgs e)
    {        
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            
            if (!Page.IsPostBack)
            {
                Estado_Botones(Const_Estado_Inicial);
                Cargar_Grid_Imagenes(0);
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones(Const_Estado_Inicial);
        }
        
    }

    #endregion
    
    #region Metodos

        ///****************************************************************************************
        ///NOMBRE DE LA FUNCION: Mensaje_Error
        ///DESCRIPCION : Muestra el error
        ///PARAMETROS  : P_Texto: texto de un TextBox
        ///CREO        : David Herrera rincon
        ///FECHA_CREO  : 15/Enero/2012
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///****************************************************************************************
        private void Mensaje_Error(String P_Mensaje)
        {
            Img_Error.Visible = true;
            Lbl_Error.Text += P_Mensaje + "</br>";
            Div_Contenedor_error.Visible = true;
        }
        private void Mensaje_Error()
        {
            Img_Error.Visible = false;
            Lbl_Error.Text = "";
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Estado_Botones
        ///DESCRIPCIÓN: Metodo para establecer el estado de los botones y componentes del formulario
        ///PARAMETROS: P_Estado: El estado de la pagina
        ///CREO: David herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Estado_Botones(int P_Estado)
        {
            switch (P_Estado)
            {
                case 0: //Estado inicial                                        
                    Btn_Agregar.Enabled = false;

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Imagenes.Enabled = true;
                    
                    Grid_Imagenes.Enabled = true;
                    Grid_Imagenes.SelectedIndex = (-1);

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Inicio";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Inicio";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                    Btn_Modificar.Enabled = false;
                    Btn_Nuevo.Enabled = true;
                    Btn_Salir.Enabled = true;

                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;

                    Configuracion_Acceso("Frm_Cat_Com_Imagenes.aspx");
                    break;

                case 1: //Nuevo                    

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;

                    Btn_Agregar.Enabled = true;

                    Btn_Modificar.Visible = false;

                    Grid_Imagenes.SelectedIndex = (-1);                   

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Dar de Alta";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";

                    break;

                case 2: //Grid Cotizador

                    Div_Listado_Cotizadores.Visible = true;
                    Div_Datos_Cotizador.Visible = true;
                    Grid_Imagenes.Enabled = true;

                    Btn_Agregar.Enabled = false;

                    Btn_Modificar.Visible = true;
                    Btn_Modificar.Enabled = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Listado";

                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Listado";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";


                    break;

                case 3: //Modificar

                    Btn_Agregar.Enabled = true;

                    Div_Listado_Cotizadores.Visible = true;
                    Grid_Imagenes.Enabled = true;
                    Div_Datos_Cotizador.Visible = true;

                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.Visible = false;

                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Salir.AlternateText = "Cancelar";

                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Salir.ToolTip = "Cancelar";

                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_Nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                    break;
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Modificar_Correo
        ///DESCRIPCIÓN: se actualizan los datos del correo seleccionado
        ///PARAMETROS: 
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************

        private void Modificar_Correo(Boolean Bln_Nuevo_Correo)
        {
            Div_Contenedor_error.Visible = false;
            Lbl_Error.Text = "";
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();
                String[] Nombre;               
                Char Letra = '\\';

                if (Div_Contenedor_error.Visible == false)
                {                    
                    if (Grid_Imagenes.Rows.Count > 0)
                    {
                        if ((DataTable)Session["Dt_Imagenes"] != null)
                        {
                            //Validamos que firma es
                            for (int Fila = 0; Fila < Grid_Imagenes.Rows.Count; Fila++)
                            {
                                Nombre = Grid_Imagenes.Rows[Fila].Cells[2].Text.Split(Letra);
                                if (Grid_Imagenes.Rows[Fila].Cells[1].Text == "Jefe Compras")
                                {
                                    Correo_Negocio.P_Campo = "Firma_JEFE_Compras_IMG";
                                    Correo_Negocio.P_Ruta_Compras = "Archivos\\Img_Firmas\\" + Nombre[Nombre.Length - 1];
                                }
                                else
                                {
                                    Correo_Negocio.P_Campo = "Firma_GERENTE_ADMIN_IMG";
                                    Correo_Negocio.P_Ruta_Compras = "Archivos\\Img_Firmas\\" + Nombre[Nombre.Length - 1];
                                }
                                //Modificamos               
                                Correo_Negocio.Modificar_Imagen();
                            }
                            //Agregamos al directorio las imagenes
                            Agrega_Directorio_Imagenes();
                        }
                        
                    }                    
                    if (!Bln_Nuevo_Correo)
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('La Modificacion de las Imagenes fue Exitosa');", true);
                        Estado_Botones(Const_Estado_Inicial);
                        Cargar_Grid_Imagenes(0);
                        Session.Remove("Dt_Imagenes");
                        //Removemos Sesiones de ruta
                        Session.Remove("Ruta_Compras");
                        Session.Remove("Ruta_Gerente");
                    }
                }//Fin del if
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }
        }//fin de Modificar  

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
        ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
        ///PARAMETROS: Page_Index: Numero de pagina del grid
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Cargar_Grid_Imagenes(int Page_Index)
        {
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();

                Dt_Imagenes = Correo_Negocio.Consultar_Imagenes();
                Session["Dt_Temp"] = Dt_Imagenes;
                Grid_Imagenes.PageIndex = Page_Index;
                Grid_Imagenes.DataSource = Dt_Imagenes;                
                Grid_Imagenes.DataBind();
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Imagenes
        ///DESCRIPCIÓN: Carga en un data table los datos 
        ///PARAMETROS: 
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 17/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Llenar_Grid_Imagenes(String Firma, String Ruta)
        {
            Dt_Imagenes = (DataTable)Session["Dt_Imagenes"];
            //Validamos si ya se agrego la firma solo cambiamos la ruta
            if (Valida_Imagenes(Dt_Imagenes, Firma, Ruta) == true)
            {
                //Si es null la tabla creamos las columnas
                if (Dt_Imagenes == null)
                {
                    Dt_Imagenes = new DataTable("Dt_Imagenes");
                    Dt_Imagenes.Columns.Add("Firma", Type.GetType("System.String"));
                    Dt_Imagenes.Columns.Add("Ruta", Type.GetType("System.String"));
                }
                DataRow Fila_Img = Dt_Imagenes.NewRow();
                //Agregamos los datos
                if (Firma == "Jefe Compras")
                {
                    Fila_Img["Firma"] = "Jefe Compras";
                    Fila_Img["Ruta"] = Ruta;
                }
                else
                {
                    Fila_Img["Firma"] = "Gerente Administrativo";
                    Fila_Img["Ruta"] = Ruta;
                }
                Dt_Imagenes.Rows.Add(Fila_Img);
                //Asignar los datos
                Grid_Imagenes.DataSource = Dt_Imagenes;
                Session["Dt_Imagenes"] = Dt_Imagenes;
                Grid_Imagenes.DataBind();
                Grid_Imagenes.SelectedIndex = (-1);                
            }            
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Valida_Imagenes
        ///DESCRIPCIÓN: Valida si ya existe la imagen en el grid 
        ///PARAMETROS: 
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 17/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private Boolean Valida_Imagenes(DataTable Dt_Temp, String Firma, String Ruta)
        {
            Boolean Resultado = true;
            //Validamos que tenga algo el datatable
            if (Dt_Temp != null)
            {
                if (Dt_Temp.Rows.Count > 0)
                {   //Recorremos los datos
                    for (int i = 0; i < Grid_Imagenes.Rows.Count; i++) 
                    {   //Si ya existe el registro solo le cambianos la ruta
                        if (Grid_Imagenes.Rows[i].Cells[1].Text.Trim() == Firma)
                        {
                            Dt_Temp.Rows[i]["Ruta"] = Ruta;
                            Grid_Imagenes.Rows[i].Cells[2].Text = Ruta;
                            Resultado = false;
                            break;
                        }
                    }                    
                }
                else
                    Resultado = true;
            }
            else
                Resultado = true;
            Session["Dt_Imagenes"] = Dt_Temp;
            return Resultado;
        }

        ///******************************************************************************* 
        ///NOMBRE DE LA FUNCIÓN: Eliminar_Directorio_Imagenes
        ///DESCRIPCIÓN: Elimina de la carpeta temporal las imagenes agregadas 
        ///PARAMETROS: 
        ///CREO: David Herrera rincon
        ///FECHA_CREO: 17/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        private void Eliminar_Directorio_Imagenes()
        {
            String Ruta = Server.MapPath("~\\Archivos") + "\\Temporal\\Img_Firmas";
            String[] Archivos_Temporales;

            //Verificar si la ruta existe
            if (Directory.Exists(Ruta) == true)
            {
                //Obtener el listado de los archivos de la ruta temporal
                Archivos_Temporales = Directory.GetFiles(Ruta);
                //Verificar si hay archivos
                if (Archivos_Temporales.Length > 0)
                {
                    //Ciclo para la eliminacion de los archivos temporales
                    for (int Cont_Elementos = 0; Cont_Elementos < Archivos_Temporales.Length; Cont_Elementos++)
                    {
                        //Eliminar el archivo actual
                        File.Delete(Archivos_Temporales[Cont_Elementos].Trim());
                    }
                }
                //Eliminar el directorio donde estan las imagenes
                //Directory.Delete(Ruta);                
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Agrega_Directorio_Imagenes
        // DESCRIPCION:             Guarda los archivos de las firmas en el directorio
        // PARAMETROS:              
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              17/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Agrega_Directorio_Imagenes()
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos a agregar            
            int Cont_Elementos; //variable para el contador
            String Ruta_Fija = String.Empty; //variable para la ruta fija
            String[] Archivos_Temporales; //variable para los archivos temporales
            String[] Nombre;
            Char Letra = '\\';
            try
            {
                //Asignar la ruta de los archivos
                Ruta = Server.MapPath("~\\Archivos") + "\\Img_Firmas";
                Ruta_Fija = Ruta;

                //verificar si la ruta existe
                if (Directory.Exists(Ruta) == false)
                {
                    Directory.CreateDirectory(Ruta);
                }

                //Asignar la ruta de los archivos temporales
                Ruta = Server.MapPath("~\\Archivos") + "\\Temporal\\Img_Firmas\\";

                //Verificar si la ruta existe
                if (Directory.Exists(Ruta) == true)
                {
                    
                    //Colocar la variable de sesion en la tabla
                    Dt_Archivos = (DataTable)Session["Dt_Imagenes"];

                    //Verificar si la tabla tiene elementos
                    if (Dt_Archivos != null)
                    {
                        if (Dt_Archivos.Rows.Count > 0)
                        {
                            //Ciclo para copiar los archivos temporales a la ruta de los archivos
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                            {
                                Nombre = Dt_Archivos.Rows[Cont_Elementos]["Ruta"].ToString().Trim().Split(Letra);
                                //Verificar si el archivo existe
                                if (File.Exists(Ruta + "\\" + Nombre[Nombre.Length - 1]) == true)
                                {
                                    //Copiar el archivo
                                    File.Copy(Ruta + "\\" + Nombre[Nombre.Length - 1], Ruta_Fija + "\\" + Nombre[Nombre.Length - 1]);
                                }
                            }
                        }

                        //Obtener el listado de los archivos de la ruta temporal
                        Archivos_Temporales = Directory.GetFiles(Ruta);

                        //Verificar si hay archivos
                        if (Archivos_Temporales.Length > 0)
                        {
                            //Ciclo para la eliminacion de los archivos temporales
                            for (Cont_Elementos = 0; Cont_Elementos < Archivos_Temporales.Length; Cont_Elementos++)
                            {
                                //Eliminar el archivo actual
                                File.Delete(Archivos_Temporales[Cont_Elementos].Trim());
                            }
                        }
                        //Validamos que sea modo de actualizar
                        if (Btn_Modificar.AlternateText == "Actualizar")
                        {
                            //Asignamos valores a las variables
                            Ruta = Server.MapPath("~\\Archivos") + "\\Img_Firmas";
                            DataTable Dt_Temp = (DataTable)Session["Dt_Temp"];
                            String Arv_Compras = (String)Session["Ruta_Compras"];
                            String Arv_Gerente = (String)Session["Ruta_Gerente"];
                            //Ciclo para saber si se cambio el archivo de compras
                            for (int Fila = 0; Fila < Dt_Imagenes.Rows.Count; Fila++)
                            {//Validamos k sea el registro de compras
                                if ((Dt_Temp.Rows[Fila]["Firma"].ToString() == "Jefe Compras"))
                                {
                                    if (Arv_Compras != Dt_Temp.Rows[Fila]["Ruta"].ToString())
                                    {//eliminamos el archivo
                                        Nombre = Dt_Temp.Rows[Fila]["Ruta"].ToString().Trim().Split(Letra);
                                        File.Delete(Ruta + "\\" + Nombre[Nombre.Length - 1]);
                                        break;
                                    }
                                }
                            }
                            //ciclo para saber si se cambio el archivo de gerente
                            for (int Fila = 0; Fila < Dt_Imagenes.Rows.Count; Fila++)
                            {//validamos que sea el registro de gerencia
                                if ((Dt_Temp.Rows[Fila]["Firma"].ToString() == "Gerente Administrativo"))
                                {
                                    if (Arv_Gerente != Dt_Temp.Rows[Fila]["Ruta"].ToString())
                                    {//eliminamos el archivo
                                        Nombre = Dt_Temp.Rows[Fila]["Ruta"].ToString().Trim().Split(Letra);
                                        File.Delete(Ruta + "\\" + Nombre[Nombre.Length - 1]);
                                        break;
                                    }
                                }
                            }
                        }
                        //Eliminar el directorio
                        //Directory.Delete(Ruta);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Eventos

        protected void AFil_Img_Compras_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta

            try
            {
                if (Btn_Agregar.Enabled == true)
                {
                    //Verificar si se tiene un archivo
                    if (AFil_Img_Compras.HasFile)
                    {
                        //Sacamos la extension del archivo
                        FileInfo Archivo = new FileInfo(AFil_Img_Compras.FileName);
                        String Extension_Archivo = Archivo.Extension;
                        //Validamos que sean imagenes
                        if ((Extension_Archivo == ".jpg") || (Extension_Archivo == ".gif"))
                        {
                            //obtener la ruta del servidor
                            Ruta = HttpContext.Current.Server.MapPath("~\\Archivos");

                            //verificar si el directorio ya ha sido creado
                            if (Directory.Exists(Ruta + "\\Temporal\\Img_Firmas") == false)
                            {
                                Directory.CreateDirectory(Ruta + "\\Temporal\\Img_Firmas");
                            }

                            Ruta = Ruta + "\\Temporal\\Img_Firmas\\" + System.IO.Path.GetFileName(AFil_Img_Compras.FileName);

                            //Colocar los datos en los controles ocultos
                            Session["Ruta_Compras"] = "Archivos\\Temporal\\Img_Firmas\\" + System.IO.Path.GetFileName(AFil_Img_Compras.FileName);

                            AFil_Img_Compras.SaveAs(Ruta);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Tiene que ser imagen');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Tiene que dar clic en nuevo para cargar la imagen');", true);
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }
        protected void AFil_Img_Gerente_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta

            try
            {
                if (Btn_Agregar.Enabled == true)
                {
                    //Verificar si se tiene un archivo
                    if (AFil_Img_Gerente.HasFile)
                    {
                        //Sacamos la extension del archivo
                        FileInfo Archivo = new FileInfo(AFil_Img_Gerente.FileName);
                        String Extension_Archivo = Archivo.Extension;
                        //Validamos que sean imagenes
                        if ((Extension_Archivo == ".jpg") || (Extension_Archivo == ".gif"))
                        {
                            //obtener la ruta del servidor
                            Ruta = HttpContext.Current.Server.MapPath("~\\Archivos");

                            //verificar si el directorio ya ha sido creado
                            if (Directory.Exists(Ruta + "\\Temporal\\Img_Firmas") == false)
                            {
                                Directory.CreateDirectory(Ruta + "\\Temporal\\Img_Firmas");
                            }

                            Ruta = Ruta + "\\Temporal\\Img_Firmas\\" + System.IO.Path.GetFileName(AFil_Img_Gerente.FileName);

                            //Colocar los datos en los controles ocultos
                            Session["Ruta_Gerente"] = "Archivos\\Temporal\\Img_Firmas\\" + System.IO.Path.GetFileName(AFil_Img_Gerente.FileName);

                            AFil_Img_Gerente.SaveAs(Ruta);
                        }
                        else
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Tiene que ser imagen');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Tiene que dar clic en nuevo para cargar la imagen');", true);
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }  

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Evento del Boton Nuevo
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Cls_Cat_Com_Correo_Negocio Correo_Negocio = new Cls_Cat_Com_Correo_Negocio();

                if (Btn_Nuevo.AlternateText == "Nuevo")
                {                    
                    //Valida que no exista una cuenta de correo
                    if (Grid_Imagenes.Rows.Count > 0)
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('Ya existen las firmas.');", true);                 
                    else
                        Estado_Botones(Const_Estado_Nuevo);                    
                }
                else if (Btn_Nuevo.AlternateText == "Dar de Alta")
                {
                    Div_Contenedor_error.Visible = false;
                    String[] Nombre1;
                    String[] Nombre2;
                    Char Letra = '\\';
                    //Si pasa todas las Validaciones damos de alta el cotizador
                    if (Div_Contenedor_error.Visible == false)
                    {
                        try
                        {
                            if (Grid_Imagenes.Rows.Count == 2)
                            {
                                if (Grid_Imagenes.Rows.Count > 0)
                                {
                                    Nombre1 = Grid_Imagenes.Rows[0].Cells[2].Text.Split(Letra);
                                    Nombre2 = Grid_Imagenes.Rows[1].Cells[2].Text.Split(Letra);
                                        //Validamos que firma es
                                    if (Grid_Imagenes.Rows[0].Cells[1].Text == "Jefe Compras")
                                        Correo_Negocio.P_Ruta_Compras = "Archivos\\Img_Firmas\\" + Nombre1[Nombre1.Length-1];
                                    else
                                        Correo_Negocio.P_Ruta_Gerencia = "Archivos\\Img_Firmas\\" + Nombre1[Nombre1.Length - 1];
                                    if (Grid_Imagenes.Rows[1].Cells[1].Text == "Jefe Compras")
                                        Correo_Negocio.P_Ruta_Compras = "Archivos\\Img_Firmas\\" + Nombre2[Nombre2.Length - 1];
                                    else
                                            Correo_Negocio.P_Ruta_Gerencia = "Archivos\\Img_Firmas\\" + Nombre2[Nombre2.Length - 1];
                                    //Damos de alta el registro
                                    Correo_Negocio.Alta_Imagen();
                                    
                                    Agrega_Directorio_Imagenes();
                                }
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('El alta de las imagenes fueron Exitosas');", true);
                                Estado_Botones(Const_Estado_Inicial);
                                Cargar_Grid_Imagenes(0);
                                Session.Remove("Dt_Imagenes");
                                //Removemos Sesiones de ruta
                                Session.Remove("Ruta_Compras");
                                Session.Remove("Ruta_Gerente");
                            }
                            else
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Debe de agregar las dos firmas.');", true);
                        }
                        catch
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Correo", "alert('El alta de las imagenes no fue Exitosa');", true);
                            Estado_Botones(Const_Estado_Inicial);
                            Cargar_Grid_Imagenes(0);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Evento del Boton Modificar
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 15/Enero/2013 
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText == "Modificar")
                {
                    Estado_Botones(Const_Estado_Modificar);
                }
                else if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Modificar_Correo(false);
                }
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message.ToString());
            }

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Salir_Click
        ///DESCRIPCION:             Boton para SALIR
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO:             15/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            if (Btn_Salir.AlternateText == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {   
                Cargar_Grid_Imagenes(0);                
                //Eliminamos Session
                Session.Remove("Dt_Imagenes");
                //Removemos Sesiones de ruta
                Session.Remove("Ruta_Compras");
                Session.Remove("Ruta_Gerente");

                Estado_Botones(Const_Estado_Inicial);
                Eliminar_Directorio_Imagenes();                
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCION:    Btn_Agregar_Click
        ///DESCRIPCION:             Boton para agregar 
        ///PARAMETROS:              
        ///CREO:                   David Herrera Rincon
        ///FECHA_CREO:             17/Enero/2013
        ///MODIFICO          : 
        ///FECHA_MODIFICO    : 
        ///CAUSA_MODIFICACION: 
        ///*******************************************************************************
        protected void Btn_Agregar_Click(object sender, ImageClickEventArgs e)
        {
            String Ruta_Compras = (String)Session["Ruta_Compras"];
            String Ruta_Gerente = (String)Session["Ruta_Gerente"];
            //Validamos k contengan datos las variables
            if ((!String.IsNullOrEmpty(Ruta_Compras)) || (!String.IsNullOrEmpty(Ruta_Gerente)))
            {
                //Solo agregamos dos registros
                if (Grid_Imagenes.Rows.Count <= 2)
                {
                    //validamos k contenga algo la variable
                    if (!String.IsNullOrEmpty(Ruta_Compras))
                        Llenar_Grid_Imagenes("Jefe Compras", Ruta_Compras);
                    if (!String.IsNullOrEmpty(Ruta_Gerente))
                        Llenar_Grid_Imagenes("Gerente Administrativo", Ruta_Gerente);
                    
                }                
                else
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Imagenes", "alert('Ya no se puede agregar otra imagen.');", true);                    
            }
            
        }

    #endregion

    #region Grid

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Imagenes_SelectedIndexChanged
        ///DESCRIPCIÓN: Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 17/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Imagenes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                //Validamos la ruta de donde va a mostrar la imagen
                if (Btn_Agregar.Enabled == false)
                {                   
                    Estado_Botones(Const_Grid_Cotizador);
                    Img_Imagenes.ImageUrl = "../../" + HttpUtility.HtmlDecode(Grid_Imagenes.SelectedRow.Cells[2].Text.Replace("\\", "/"));
                }
                else
                    Img_Imagenes.ImageUrl ="../../" + HttpUtility.HtmlDecode(Grid_Imagenes.SelectedRow.Cells[2].Text.Replace("\\", "/"));
                }
            
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Imagenes_PageIndexChanging
        ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 17/Enero/2013  
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Grid_Imagenes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Imagenes.SelectedIndex = (-1);
                Cargar_Grid_Imagenes(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Mensaje_Error(Ex.Message);
            }
        }

    #endregion   

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:       Configuracion_Acceso
    /// DESCRIPCIÓN:  Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:   URL_Pagina: Nombre de la pagina
    /// USUARIO CREÓ: David Herrera Rincon
    /// FECHA CREÓ:   15/Enero/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);            

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Es_Numero
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : David Herrera Rincon
    /// FECHA_CREO  : 15/Enero/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion        
    
}
