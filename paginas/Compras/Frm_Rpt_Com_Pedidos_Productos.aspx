﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Com_Pedidos_Productos.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Com_Pedidos_Productos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
  <cc1:ToolkitScriptManager ID="ScriptManager_Productos" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
              <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                    <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>   style="background-color:#ffffff; width:100%; height:100%;" --%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Pedido_Productos" style="background-color:#ffffff; width:100%; height:100%;">
                <table  id="Tabla_Generar" width="100%" class="estilo_fuente"> 
                   <tr>
                       <td style="width:98%">
                            <table width="100%" class="estilo_fuente">
                            <tr align="center">
                                <td class="label_titulo">
                                   Reporte de Pedidos por Unidad Responsable
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"/>&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                                </td>
                            </tr>
                            </table>
                            <table width="100%"  border="0" cellspacing="0">
                            <tr align="center">
                                <td>                
                                    <div  style="width:100%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                                        <table style="width:100%;height:28px;">
                                         <tr>
                                            <td align="left" style="width:100%;">  
                                                 <asp:ImageButton ID="Btn_Buscar_Conultar" runat="server" CssClass="Img_Button"
                                                     ToolTip="Generar Reporte" 
                                                     ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Conultar_Click" 
                                                      />
                                                  <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                                                         ToolTip="Exportar Excel"  AlternateText="Exportar a Excel" onclick="Btn_Exportar_Excel_Click" 
                                                     /> 
                                              <%--<asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="20"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />--%>
                                              <%--<asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="6"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />--%>
                                              <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                   CssClass="Img_Button" TabIndex="4"
                                                   ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                   onclick="Btn_Salir_Click" />
                                              <asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                                   onclick="Btn_Limpiar_Click" ToolTip="Limpiar"  />
                                            </td>
                                         </tr>
                                        </table>
                                    </div>
                                  </td>
                            </tr>
                          </table>  
                       </td>
                   </tr>
                   </table>
                   <div>
                    <table width="98%" style="height:100%">
                         <tr>
                            <td style="width:25%">
                                Unidad Responsable
                            </td>
                            <td colspan="3"> 
                                <asp:DropDownList ID="Cmb_Unidad_Responable" Width="90%" runat="server" 
                                    onselectedindexchanged="Cmb_Unidad_Responable_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%">
                                Partida Especifica
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Partida_Especifica" Width="90%" runat="server" 
                                    onselectedindexchanged="Cmb_Partida_Especifica_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:25%">
                                Productos
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Productos" Width="90%" runat="server">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr> 
                         
                           <td style="width:25%">
                                &nbsp;&nbsp;
                            </td>
                            <td style="width:35%; ">
                                Fecha del&nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="95px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />

                            </td>
                            <td style="width:35%">
                                Al &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="95px" Enabled="false"></asp:TextBox>
                                 <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Final_FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Fecha_Final" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                            
                            </td>
                        </tr>
                          <tr>
                            <td colspan="4">
                            <div style="height:500px;  overflow:auto; width:100%; vertical-align:top;">
                                <asp:GridView ID="Grid_Reporte" runat="server" CssClass="GridView_1" AutoGenerateColumns="false"
                                     GridLines="None">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="FOLIO" HeaderText="Folio" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="OC" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PARTIDA" HeaderText="Partida" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="PRODUCTO" HeaderText="Producto" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Cant." NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MONTO_TOTAL" HeaderText="Total Gastado" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_SALIDA" HeaderText="No. Salida" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Center" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_GENERACION" HeaderText="Fecha" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" Width="5%" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="RECIBIO" HeaderText="Recibio" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" Font-Size="Small" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                </br>
                            </td>
                        </tr>
                        </table>
                  </div>
       
            </div>    
        </ContentTemplate> 
    </asp:UpdatePanel>
</asp:Content>
