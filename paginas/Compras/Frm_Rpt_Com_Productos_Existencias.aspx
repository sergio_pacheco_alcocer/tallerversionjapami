﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Com_Productos_Existencias.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Com_Productos_Existencias" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
     <ContentTemplate>
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                    <img alt="" src="../Imagenes/paginas/Updating.gif" /></div>   style="background-color:#ffffff; width:100%; height:100%;" --%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Pedido_Productos" style="background-color:#ffffff; width:100%; height:100%;">
                <table  id="Tabla_Generar" width="100%" class="estilo_fuente"> 
                   <tr>
                       <td style="width:98%">
                            <table width="100%" class="estilo_fuente">
                            <tr align="center">
                                <td class="label_titulo">
                                   Reporte de Pedidos por Unidad Responsable
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"/>&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                                </td>
                            </tr>
                            </table>
                            <table width="100%"  border="0" cellspacing="0">
                            <tr align="center">
                                <td>                
                                    <div  style="width:100%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                                        <table style="width:100%;height:28px;">
                                         <tr>
                                            <td align="left" style="width:100%;">  
                                                 <asp:ImageButton ID="Btn_Buscar_Conultar" runat="server" CssClass="Img_Button"
                                                     ToolTip="Generar Reporte" 
                                                     ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Conultar_Click" 
                                                      />
                                                  <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                                                         ToolTip="Exportar Excel"  AlternateText="Exportar a Excel" onclick="Btn_Exportar_Excel_Click" 
                                                     /> 
                                              <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                   CssClass="Img_Button" TabIndex="4"
                                                   ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                   onclick="Btn_Salir_Click" />
                                            </td>
                                         </tr>
                                        </table>
                                    </div>
                                  </td>
                            </tr>
                          </table>  
                       </td>
                   </tr>
                </table>
               <div>
                <table width="98%" style="height:100%">
                    <tr>
                        <td style="width:15%"> Nombre Producto 
                        </td>
                        <td> <asp:TextBox ID="Txt_Producto_Nombre" Width="90%" runat="server"></asp:TextBox> 
                        </td>
                    </tr>
                    <tr>
                        <td style="width:15%"> Partida Especifica
                        </td>
                        <td> 
                            <asp:TextBox ID="Txt_Partida_Especifica" runat="server" MaxLength="4" 
                                ontextchanged="Txt_Partida_Especifica_TextChanged" AutoPostBack="true" Width="10%"></asp:TextBox>
                            <cc1:filteredtextboxextender id="Ftbe_Cantidad" runat="server" targetcontrolid="Txt_Partida_Especifica"
                                                    filtertype="Numbers">
                            </cc1:filteredtextboxextender>
                            <asp:DropDownList ID="Cmb_Partida_Especifica" Width="80%" runat="server" 
                                onselectedindexchanged="Cmb_Partida_Especifica_SelectedIndexChanged" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td> Tipo de Producto</td>
                        <td> 
                            <asp:DropDownList ID="Cmb_Tipo_Producto" Width="30%" runat="server">
                                <asp:ListItem> <SELECCIONAR> </asp:ListItem>
                                <asp:ListItem> ALMACEN GENERAL</asp:ListItem>
                                <asp:ListItem> ALMACEN PAPELERIA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" align="center"> 
                            <asp:GridView ID="Grid_Productos" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="98%"                                     
                                    style="white-space:normal" EmptyDataText="No se encontaron productos" >
                                    <RowStyle CssClass="GridItem"/>
                                    <Columns>
                                        <asp:BoundField DataField="Clave" HeaderText="Clave" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="7%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Nombre" HeaderText="Nombre" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripción" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                        </asp:BoundField>                                 
                                        <asp:BoundField DataField="Partida" HeaderText="Partida" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="23%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Existencia" HeaderText="Existencia" Visible="True" >
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Comprometido" HeaderText="Comprometido" Visible="True" >
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Disponible" HeaderText="Disponible" Visible="True" >
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                        </td>
                    </tr>
                </table>
               </div>
            </div>
      </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>