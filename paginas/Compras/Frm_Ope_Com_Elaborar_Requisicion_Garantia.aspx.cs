﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Generar_Requisicion.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using System.Text;
using JAPAMI.Requisiciones_Garantia.Negocio;

public partial class paginas_Compras_Frm_Ope_Com_Elaborar_Requisicion_Garantia : System.Web.UI.Page
{
    #region "Page_Load"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Page_Load
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            Mostrar_Informacion("", false, "AZUL");
            if (String.IsNullOrEmpty(Cls_Sessiones.Empleado_ID) && String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                LLenar_Combo_Dependencias();
                Habilitar_Controles("Uno");
                Cmb_Dependencia_Panel.SelectedIndex = Cmb_Dependencia_Panel.Items.IndexOf(Cmb_Dependencia_Panel.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
                Llenar_Grid_Requisiciones_Garantia(); 
                Cmb_Dependencia.SelectedIndex = Cmb_Dependencia.Items.IndexOf(Cmb_Dependencia.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado));
            }
        }

    #endregion 

    #region "Metodos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Alta_Requisicion
        ///DESCRIPCIÓN: Alta_Requisicion
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private Cls_Ope_Com_Requisiciones_Garantia_Negocio Alta_Requisicion()
        {
            Cls_Ope_Com_Requisiciones_Garantia_Negocio RQG_Negocio = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
            RQG_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
            RQG_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedItem.Value;
            RQG_Negocio.P_Fecha_Elaboracion = Convert.ToDateTime(Txt_Fecha.Text);
            RQG_Negocio.P_No_Requisicion_Referencia = Hdf_RQ_Referencia_ID.Value.Trim();
            RQG_Negocio.P_Justificacion = Txt_Justificacion.Text.Trim();
            RQG_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
            RQG_Negocio.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            if (Txt_Comentario.Text.Trim().Length > 0) RQG_Negocio.P_Comentarios = Txt_Comentario.Text.Trim();
            RQG_Negocio = RQG_Negocio.Alta_Requisicion_Garantia();
            return RQG_Negocio;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Modifica_Requisicion
        ///DESCRIPCIÓN: Modifica_Requisicion
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Modifica_Requisicion()
        {
            Cls_Ope_Com_Requisiciones_Garantia_Negocio RQG_Negocio = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
            RQG_Negocio.P_No_Requisicion = Hdf_Requisicion_ID.Value.Trim();
            RQG_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
            RQG_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedItem.Value;
            RQG_Negocio.P_Fecha_Elaboracion = Convert.ToDateTime(Txt_Fecha.Text);
            RQG_Negocio.P_No_Requisicion_Referencia = Hdf_RQ_Referencia_ID.Value.Trim();
            RQG_Negocio.P_Justificacion = Txt_Justificacion.Text.Trim();
            RQG_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
            RQG_Negocio.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            if (Txt_Comentario.Text.Trim().Length > 0) RQG_Negocio.P_Comentarios = Txt_Comentario.Text.Trim();
            RQG_Negocio.Modifica_Requisicion_Garantia();
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Validacion_Requisicion_Garantia
        /// DESCRIPCIÓN: Validacion Generales de Formulario
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private Boolean Validacion_Requisicion_Garantia() {
            Boolean Validacion = true;
            String Mensaje_Validacion = "";
            if (Cmb_Estatus.SelectedIndex < 1) {
                Validacion = false;
                Mensaje_Validacion += "<br /> + Se debe seleccionar algún Estatus.";
            }
            if (Hdf_RQ_Referencia_ID.Value.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje_Validacion += "<br /> + Se debe seleccionar alguna Requisición.";
            }
            if (Txt_Justificacion.Text.Trim().Length == 0)
            {
                Validacion = false;
                Mensaje_Validacion += "<br /> + Se debe justificar la Requisición.";
            }
            if (Cmb_Estatus.SelectedItem.Value.Equals("CANCELADA"))
            {
                if (Txt_Comentario.Text.Trim().Length == 0)
                {
                    Validacion = false;
                    Mensaje_Validacion += "<br /> + Se debe introducir en la parte de Comentarios el motivo de la cancelación de la Requisición.";
                }
            }
            if (!String.IsNullOrEmpty(Mensaje_Validacion))
                Mostrar_Informacion(HttpUtility.HtmlDecode(Mensaje_Validacion), true, "ROJO");
            return Validacion;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Limpiar_Generales_Formulario
        /// DESCRIPCIÓN: Limpiar Generales de Formulario
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private void Limpiar_Generales_Formulario()
        {
            Txt_Folio.Text = "";
            Txt_Fecha.Text = "";
            Hdf_Requisicion_ID.Value = "";
            if (Cmb_Estatus.Items.Count > 0) Cmb_Estatus.SelectedIndex = 0; else Cmb_Estatus.SelectedIndex = -1;
            Hdf_RQ_Referencia_ID.Value = "";
            Txt_RQ_Referencia.Text = "";
            Btn_Imprimir_RQ_Referencia.Visible = false;
            Grid_Servicios_Requisicion.DataSource = new DataTable();
            Grid_Servicios_Requisicion.DataBind();
            Txt_Justificacion.Text = "";
            Grid_Listado_Bienes.DataSource = new DataTable();
            Grid_Listado_Bienes.DataBind();
            Txt_Comentario.Text = "";
            Grid_Comentarios.DataSource = new DataTable();
            Grid_Comentarios.DataBind();
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Llenar_Grid_Servicios_Requisicion
        /// DESCRIPCIÓN: Llena el Grid de los Servicios
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private void Llenar_Grid_Servicios_Requisicion(DataTable Dt_Datos) {
            Grid_Servicios_Requisicion.Columns[0].Visible = true;
            Grid_Servicios_Requisicion.DataSource = Dt_Datos;
            Grid_Servicios_Requisicion.DataBind();
            Grid_Servicios_Requisicion.Columns[0].Visible = false;
        }

        /// *****************************************************************************************
        /// NOMBRE: Llenar_Grid_Bienes
        /// DESCRIPCIÓN: Llena el Grid de los Bienes 
        /// CREÓ: Francisco Antonio Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICÓ:
        /// FECHA MODIFICÓ:
        /// CAUSA MODIFICACIÓN:
        /// *****************************************************************************************
        public void Llenar_Grid_Bienes(DataTable Dt_Datos)
        {
            Grid_Listado_Bienes.Columns[0].Visible = true;
            Grid_Listado_Bienes.DataSource = Dt_Datos;
            Grid_Listado_Bienes.DataBind();
            Grid_Listado_Bienes.Columns[0].Visible = false;
        }

        /// *****************************************************************************************
        /// NOMBRE: Llenar_Grid_Comentarios
        /// DESCRIPCIÓN: Llenar_Grid_Comentarios 
        /// CREÓ: Francisco Antonio Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICÓ:
        /// FECHA MODIFICÓ:
        /// CAUSA MODIFICACIÓN:
        /// *****************************************************************************************
        public void Llenar_Grid_Comentarios(DataTable Dt_Datos)
        {
            Grid_Comentarios.DataSource = Dt_Datos;
            Grid_Comentarios.DataBind();
        } 

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Servicios
        /// DESCRIPCIÓN: Llena el Grid de los Servicios
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private void Llenar_Grid_Busqueda_Servicios()
        {
            DataTable Dt_Prod_Srv_Tmp = null;
            Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Requisiciones_Negocio();

            if (Txt_Nombre.Text.Trim().Length > 0)
            {
                Requisicion_Negocio.P_Nombre_Producto_Servicio = Txt_Nombre.Text;
            }
            if (Txt_Clave.Text.Trim().Length > 0)
            {
                Requisicion_Negocio.P_Producto_ID = Txt_Clave.Text;
            }

            Dt_Prod_Srv_Tmp = Requisicion_Negocio.Consultar_Servicios();

            if (Dt_Prod_Srv_Tmp != null && Dt_Prod_Srv_Tmp.Rows.Count > 0)
            {
                Grid_Productos_Servicios_Modal.DataSource = Dt_Prod_Srv_Tmp;
                Grid_Productos_Servicios_Modal.DataBind();
                Grid_Productos_Servicios_Modal.Visible = true;
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Llenar_Combo_Estatus
        /// DESCRIPCIÓN: Llena el Combo de los Estatus
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private void Llenar_Combo_Estatus(String Estatus) {
            if (Estatus.Equals("NUEVO")) {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem( "<-- SELECCIONAR -->", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("EN CONSTRUCCION"));
                Cmb_Estatus.Items.Insert(2, new ListItem("GENERADA"));
            }
            else if (Estatus.Equals("MODIFICAR"))
            {
                Cmb_Estatus.Items.Clear();
                Cmb_Estatus.Items.Insert(0, new ListItem("<-- SELECCIONAR -->", ""));
                Cmb_Estatus.Items.Insert(1, new ListItem("EN CONSTRUCCION"));
                Cmb_Estatus.Items.Insert(2, new ListItem("GENERADA"));
                Cmb_Estatus.Items.Insert(3, new ListItem("CANCELADA"));
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: LLenar_Combo_Dependencias
        /// DESCRIPCIÓN: Llena el Combo de las Dependencias
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        public void LLenar_Combo_Dependencias() {
            Cls_Ope_Com_Requisiciones_Garantia_Negocio RG_Negocio = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
            RG_Negocio.P_Estatus = "ACTIVO";
            RG_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
            DataTable Dt_Dependencias = RG_Negocio.Consultar_Dependencias_Empleado();
            Cmb_Dependencia_Panel.DataSource = Dt_Dependencias;
            Cmb_Dependencia_Panel.DataTextField = "CLAVE_NOMBRE_DEPENDENCIA";
            Cmb_Dependencia_Panel.DataValueField = "DEPENDENCIA_ID";
            Cmb_Dependencia_Panel.DataBind();
            Cmb_Dependencia_Panel.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
            Cmb_Dependencia.DataSource = Dt_Dependencias;
            Cmb_Dependencia.DataTextField = "CLAVE_NOMBRE_DEPENDENCIA";
            Cmb_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Dependencia.DataBind();
            Cmb_Dependencia.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCIÓN: Habilitar_Controles
        /// DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
        /// RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        /// MODIFICO:
        /// FECHA_MODIFICO
        /// CAUSA_MODIFICACIÓN   
        /// *******************************************************************************/
        private void Habilitar_Controles(String Modo)
        {
            try
            {
                switch (Modo)
                {
                    case "Uno":
                        Btn_Nuevo.Visible = true;
                        //Configuracion_Acceso("Frm_Ope_Elaborar_Requisicion.aspx");
                        Btn_Modificar.Visible = false;
                        Btn_Eliminar.Visible = false;
                        Btn_Salir.Visible = true;
                        Btn_Imprimir_Req.Visible = false;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Eliminar.ToolTip = "Eliminar";
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Txt_Fecha.Enabled = false;

                        Txt_Folio.Enabled = false;
                        Txt_RQ_Referencia.Enabled = false;
                        Cmb_Dependencia.Enabled = false;
                        //Cmb_Area.Enabled = false;
                        Cmb_Estatus.Enabled = false;
                        Grid_Servicios_Requisicion.Enabled = false;
                        Txt_Comentario.Enabled = false;
                        Txt_Justificacion.Enabled = false;
                        Div_Listado_Requisiciones.Style.Add("display", "block");
                        Div_Contenido.Style.Add("display", "none");
                        break;

                    case "Inicial":
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = true;
                        //Configuracion_Acceso("Frm_Ope_Elaborar_Requisicion.aspx");
                        Btn_Eliminar.Visible = false;
                        Btn_Salir.Visible = true;
                        Btn_Imprimir_Req.Visible = true;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Eliminar.ToolTip = "Eliminar";
                        Btn_Salir.ToolTip = "Regresar a Listado";

                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                        Txt_Fecha.Enabled = false;
                        Txt_Folio.Enabled = false;
                        Txt_RQ_Referencia.Enabled = false;
                        Cmb_Dependencia.Enabled = false;
                        Cmb_Estatus.Enabled = false;
                        Grid_Servicios_Requisicion.Enabled = false;
                        Txt_Comentario.Enabled = false;
                        Txt_Justificacion.Enabled = false;
                        Div_Listado_Requisiciones.Style.Add("display", "none");
                        Div_Contenido.Style.Add("display", "block");
                        break;
                    //Estado de Nuevo
                    case "Nuevo":
                        Btn_Nuevo.ToolTip = "Dar de Alta";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Modificar.Visible = false;
                        Btn_Eliminar.Visible = false;
                        Btn_Imprimir_Req.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Salir.Visible = true;
                        Cmb_Dependencia.Enabled = false;
                        Cmb_Estatus.Enabled = true;
                        Grid_Servicios_Requisicion.Enabled = true;
                        Txt_Comentario.Enabled = true;
                        Txt_Comentario.Text = "";
                        Txt_Justificacion.Enabled = true;
                        Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                        Cmb_Estatus.SelectedValue = "EN CONSTRUCCION";
                        Txt_Folio.Text = "Asigna folio al guardar requisición";
                        Txt_RQ_Referencia.Enabled = true;
                        Div_Comentarios.Visible = false;
                        Div_Listado_Requisiciones.Style.Add("display", "none");
                        Div_Contenido.Style.Add("display", "block");
                        Llenar_Combo_Estatus("NUEVO");
                        break;
                    //Estado de Modificar
                    case "Modificar":
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Eliminar.Visible = false;
                        Btn_Imprimir_Req.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Cmb_Dependencia.Enabled = false;
                        Cmb_Estatus.Enabled = true;
                        Grid_Servicios_Requisicion.Enabled = true;
                        Txt_Comentario.Enabled = true;
                        Txt_RQ_Referencia.Enabled = false;
                        Txt_Comentario.Text = "";
                        Txt_Justificacion.Enabled = true;
                        Div_Comentarios.Visible = true;
                        Div_Listado_Requisiciones.Style.Add("display", "none");
                        Div_Contenido.Style.Add("display", "block");
                        break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                //Mostrar_Informacion(ex.ToString(), true);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Información
        ///DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
        ///RETORNA: 
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///********************************************************************************/
        private void Mostrar_Informacion(String txt, Boolean mostrar, String Color)
        {
            if (Color == "AZUL")
            {
                Lbl_Informacion.Style.Add("color", "#0000FF");
            }
            else
            {
                Lbl_Informacion.Style.Add("color", "#990000");
            }
            Lbl_Informacion.Visible = mostrar;
            Img_Warning.Visible = mostrar;
            Lbl_Informacion.Text = txt;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN:IBtn_MDP_Prod_Serv_Buscar_Click
        ///DESCRIPCIÓN:  
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Evento_IBtn_MDP_Prod_Serv_Buscar()
        {
            DataTable Dt_Prod_Srv_Tmp = null;
            Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Requisiciones_Negocio();

            if (Txt_Nombre.Text.Trim().Length > 0)
            {
                Requisicion_Negocio.P_Nombre_Producto_Servicio = Txt_Nombre.Text;
            }
            if (Txt_Clave.Text.Trim().Length > 0)
            {
                Requisicion_Negocio.P_Producto_ID = Txt_Clave.Text;
            }
            Dt_Prod_Srv_Tmp = Requisicion_Negocio.Consultar_Servicios();

            if (Dt_Prod_Srv_Tmp != null && Dt_Prod_Srv_Tmp.Rows.Count > 0)
            {
                Grid_Productos_Servicios_Modal.DataSource = Dt_Prod_Srv_Tmp;
                Grid_Productos_Servicios_Modal.DataBind();
                Grid_Productos_Servicios_Modal.Visible = true;
            }
            Modal_Busqueda_Prod_Serv.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Requisiciones_Garantia
        ///DESCRIPCIÓN: Llenar_Grid_Requisiciones_Garantia
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Llenar_Grid_Requisiciones_Garantia()
        {
            Cls_Ope_Com_Requisiciones_Garantia_Negocio Req_Garantia_Neg = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
            if (Cmb_Dependencia_Panel.SelectedIndex > 0) Req_Garantia_Neg.P_Dependencia_ID = Cmb_Dependencia_Panel.SelectedItem.Value;
            if (Txt_Fecha_Inicial.Text.Trim().Length > 0) Req_Garantia_Neg.P_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
            if (Txt_Fecha_Final.Text.Trim().Length > 0) Req_Garantia_Neg.P_Fecha_Final = Convert.ToDateTime(Txt_Fecha_Final.Text);
            if (Cmb_Estatus_Busqueda.SelectedIndex > 0) Req_Garantia_Neg.P_Estatus = Cmb_Estatus_Busqueda.SelectedItem.Value;
            if (Txt_Busqueda.Text.Trim().Length > 0) Req_Garantia_Neg.P_Folio = Txt_Busqueda.Text.Trim();
            DataTable Dt_Resultado_Rq = Req_Garantia_Neg.Consultar_Requisiciones_Garantia();
            Grid_Requisiciones.Columns[1].Visible = true;
            Grid_Requisiciones.DataSource = Dt_Resultado_Rq;
            Grid_Requisiciones.DataBind();
            Grid_Requisiciones.Columns[1].Visible = false;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Requisicion_Garantia
        ///DESCRIPCIÓN: Mostrar_Detalles_Requisicion_Garantia
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Mostrar_Detalles_Requisicion_Garantia(Cls_Ope_Com_Requisiciones_Garantia_Negocio Req_Garantia_Neg)
        {
            Limpiar_Generales_Formulario();
            Hdf_Requisicion_ID.Value = Req_Garantia_Neg.P_No_Requisicion.Trim();
            Txt_Folio.Text = Req_Garantia_Neg.P_Folio.Trim();
            Txt_Fecha.Text = String.Format("{0:dd/MMM/yyyy}", Req_Garantia_Neg.P_Fecha_Elaboracion).ToUpper();
            Cmb_Estatus.Items.Add(Req_Garantia_Neg.P_Estatus); 
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Req_Garantia_Neg.P_Estatus)); 
            Txt_Justificacion.Text = Req_Garantia_Neg.P_Justificacion.Trim();
            Hdf_RQ_Referencia_ID.Value = Req_Garantia_Neg.P_No_Requisicion_Referencia;
            Txt_RQ_Referencia.Text = Req_Garantia_Neg.P_No_Requisicion_Referencia;
            Btn_Buscar_RQ_Referencia_Click(Btn_Buscar_RQ_Referencia, null);
            Llenar_Grid_Comentarios(Req_Garantia_Neg.P_Dt_Comentarios);
        }

    #endregion "Metodos"

    #region "Eventos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
        ///DESCRIPCIÓN: Btn_Buscar_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            if (Cmb_Dependencia_Panel.SelectedIndex > 0)
                Llenar_Grid_Requisiciones_Garantia();
            else
                Mostrar_Informacion("Debe seleccionarse la Unidad Responsable", true, "ROJO");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_RQ_Referencia
        ///DESCRIPCIÓN: Btn_Imprimir_RQ_Referencia
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Imprimir_RQ_Referencia_Click(object sender, ImageClickEventArgs e)
        {
            if (Hdf_RQ_Referencia_ID.Value.Trim().Length > 0)
            {
                DataSet Ds_Reporte = null;
                //DataTable Dt_Requisicion = null;
                Cls_Ope_Com_Impresion_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Com_Impresion_Requisiciones_Negocio();
                DataTable Dt_Cabecera = new DataTable();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Bienes = new DataTable();
                try
                {
                    String Requisicion_ID = Hdf_RQ_Referencia_ID.Value.Trim(); 
                    Req_Negocio.P_Requisicion_ID = Requisicion_ID.Trim();
                    Dt_Cabecera = Req_Negocio.Consultar_Requisiciones();
                    Dt_Detalles = Req_Negocio.Consultar_Requisiciones_Detalles();
                    Dt_Bienes = Req_Negocio.Consultar_Requisiciones_Bienes();
                    Ds_Reporte = new DataSet();
                    Dt_Cabecera.TableName = "REQUISICION";
                    Dt_Detalles.TableName = "DETALLES";
                    Dt_Bienes.TableName = "BIENES";
                    Ds_Reporte.Tables.Add(Dt_Cabecera.Copy());
                    Ds_Reporte.Tables.Add(Dt_Detalles.Copy());
                    Ds_Reporte.Tables.Add(Dt_Bienes.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Ope_Com_Requisiciones.rpt", "Requisicion " + Session.SessionID + String.Format("{0:ddMMyyhhmmss}", DateTime.Now) + " .pdf");
                }
                catch (Exception Ex)
                {
                    Mostrar_Informacion(Ex.Message.ToString(), true, "ROJO");
                }
            }
            else
            {
                Mostrar_Informacion("Debe Introducirse el No. de la Requisición de Referencia.", true, "ROJO");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_RQ_Referencia
        ///DESCRIPCIÓN: Btn_Buscar_RQ_Referencia
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Buscar_RQ_Referencia_Click(object sender, ImageClickEventArgs e)
        {
            Btn_Imprimir_RQ_Referencia.Visible = false;
            Hdf_RQ_Referencia_ID.Value = "";
            Llenar_Grid_Servicios_Requisicion(new DataTable());
            Llenar_Grid_Bienes(new DataTable());
            if (Txt_RQ_Referencia.Text.Trim().Length > 0)
            {
                Cls_Ope_Com_Requisiciones_Garantia_Negocio RQG_Negocio = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
                RQG_Negocio.P_No_Requisicion = Txt_RQ_Referencia.Text.Trim();
                RQG_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedItem.Value;
                DataTable Dt_Resultado_Busqueda_Req = RQG_Negocio.Consultar_Requisiciones();
                if (Dt_Resultado_Busqueda_Req.Rows.Count > 0)
                {
                    Hdf_RQ_Referencia_ID.Value = Dt_Resultado_Busqueda_Req.Rows[0][Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString().Trim();
                    Btn_Imprimir_RQ_Referencia.Visible = true;
                    Mostrar_Informacion("La Requisición con Folio RQ-" + Txt_RQ_Referencia.Text.Trim() + " se encontró.", true, "AZUL");
                    Img_Warning.Visible = false;
                    DataTable Dt_Listado_Servicios_Rq = RQG_Negocio.Consultar_Servicios_Requisicion();
                    Llenar_Grid_Servicios_Requisicion(Dt_Listado_Servicios_Rq);
                    DataTable Dt_Listado_Bienes_Rq = RQG_Negocio.Consultar_Bienes_Requisicion();
                    Llenar_Grid_Bienes(Dt_Listado_Bienes_Rq);
                }
                else
                {
                    Llenar_Grid_Servicios_Requisicion(new DataTable());
                    Mostrar_Informacion("No se encontro la RQ-" + Txt_RQ_Referencia.Text.Trim() + " para la Unidad Responsable '" + Cmb_Dependencia.SelectedItem.Text.Trim() + "'.", true, "ROJO");
                }
            }
            else
            {
                Mostrar_Informacion("Debe Introducirse el No. de la Requisición de Referencia.", true, "ROJO"); 
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Btn_Nuevo_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Nuevo.ToolTip == "Nuevo") {
                Limpiar_Generales_Formulario();
                if (Cmb_Dependencia_Panel.SelectedIndex != 0)
                    Habilitar_Controles("Nuevo");
                else
                    Mostrar_Informacion("Debe seleccionarse la Unidad Responsable.", true, "ROJO");
            } else {
                if (Validacion_Requisicion_Garantia())
                {
                    Cls_Ope_Com_Requisiciones_Garantia_Negocio Req_Negocio = Alta_Requisicion();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Se dio de Alta la Requisición con el Folio: RQG-" + Req_Negocio.P_No_Requisicion + "');", true);
                    Habilitar_Controles("Uno");
                    Llenar_Grid_Requisiciones_Garantia();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
        ///DESCRIPCIÓN: Btn_Modificar_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Modificar.ToolTip == "Modificar") {
                if (Cmb_Estatus.SelectedItem.Value.Equals("EN CONSTRUCCION") || Cmb_Estatus.SelectedItem.Value.Equals("RECHAZADA"))
                {
                    Llenar_Combo_Estatus("MODIFICAR");
                    Habilitar_Controles("Modificar");
                }
                else
                {
                    Mostrar_Informacion("Solo se pueden modificar las Requisiciones con Estatus 'EN CONSTRUCCION' ó que sea 'RECHAZADA'.", true, "ROJO");
                }
            } else {
                if (Validacion_Requisicion_Garantia())
                {
                    Modifica_Requisicion();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Se Actualizo la Requisición Exitosamente');", true);
                    Habilitar_Controles("Uno");
                    Llenar_Grid_Requisiciones_Garantia();
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Btn_Salir_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Salir.ToolTip == "Inicio") {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            } else {
                Habilitar_Controles("Uno");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Ibtn_Buscar_Producto_Click
        ///DESCRIPCIÓN: Ibtn_Buscar_Producto_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Ibtn_Buscar_Producto_Click(object sender, ImageClickEventArgs e)
        {
            Evento_IBtn_MDP_Prod_Serv_Buscar();
            MP_UDPpdatePanel1.Update();
            Modal_Busqueda_Prod_Serv.Show();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Ibtn_Buscar_Producto_Click
        ///DESCRIPCIÓN: Ibtn_Buscar_Producto_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Abril 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void IBtn_MDP_Prod_Serv_Cerrar_Click(object sender, ImageClickEventArgs e) {
            Modal_Busqueda_Prod_Serv.Hide();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
        ///DESCRIPCIÓN: Btn_Seleccionar_Requisicion_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Seleccionar_Requisicion_Click(object sender, ImageClickEventArgs e)
        {
            ImageButton Btn_Seleccionar = (ImageButton)sender;
            if (Btn_Seleccionar != null)
            {
                if (!String.IsNullOrEmpty(Btn_Seleccionar.CommandArgument))
                {
                    String No_Requisicion = Btn_Seleccionar.CommandArgument.Trim();
                    Cls_Ope_Com_Requisiciones_Garantia_Negocio Req_Garantia_Neg = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
                    Req_Garantia_Neg.P_No_Requisicion = No_Requisicion;
                    Req_Garantia_Neg = Req_Garantia_Neg.Consultar_Detalle_Requisicion_Garantia();
                    Mostrar_Detalles_Requisicion_Garantia(Req_Garantia_Neg);
                    Habilitar_Controles("Inicial");
                    Mostrar_Informacion("", false, "");
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Imprimir_Req_Click
        ///DESCRIPCIÓN: Btn_Imprimir_Req_Click
        /// CREO: Francisco A. Gallardo Castañeda
        /// FECHA_CREO: Mayo 2013
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        protected void Btn_Imprimir_Req_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                DataSet Ds_Consulta = new DataSet();
                Cls_Ope_Com_Requisiciones_Garantia_Negocio Req_Negocio = new Cls_Ope_Com_Requisiciones_Garantia_Negocio();
                DataTable Dt_Cabecera = new DataTable();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Bienes = new DataTable();

                Req_Negocio.P_No_Requisicion = Hdf_Requisicion_ID.Value;
                Dt_Cabecera = Req_Negocio.Consultar_Cabecera_Reporte_Requisiciones_Garantia();
                Req_Negocio.P_No_Requisicion = Hdf_RQ_Referencia_ID.Value;
                Dt_Detalles = Req_Negocio.Consultar_Servicios_Requisicion();
                Dt_Bienes = Req_Negocio.Consultar_Bienes_Requisicion();
                Dt_Cabecera.TableName = "REQUISICION";
                Dt_Detalles.TableName = "DETALLES";
                Dt_Bienes.TableName = "BIENES";
                Ds_Consulta.Tables.Add(Dt_Cabecera.Copy());
                Ds_Consulta.Tables.Add(Dt_Detalles.Copy());
                Ds_Consulta.Tables.Add(Dt_Bienes.Copy());

                Generar_Reporte(ref Ds_Consulta, "Rpt_Com_Requisicion_Garantia.rpt", "Requisicion_Garantia" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf");
            }
            catch (Exception Ex)
            {
                Mostrar_Informacion(Ex.Message, true, "ROJO");
            }
        }

    #endregion

    #region "Reportes"

        protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
        {
            ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
            String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 
            try
            {
                Ruta = @Server.MapPath("../Rpt/Compras/" + Nombre_Plantilla_Reporte);
                Reporte.Load(Ruta);

                if (Ds_Datos is DataSet)
                {
                    if (Ds_Datos.Tables.Count > 0)
                    {
                        if (Ds_Datos.Tables["BIENES"].Rows.Count == 0) Reporte.ReportDefinition.ReportObjects["Rpt_Ope_Com_Requisiciones_Bienes"].ObjectFormat.EnableSuppress = true;
                        Reporte.SetDataSource(Ds_Datos);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                        Mostrar_Reporte(Nombre_Reporte_Generar);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }

    #endregion

}