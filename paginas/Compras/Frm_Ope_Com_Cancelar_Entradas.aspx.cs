﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cancelar_Entradas.Negocio;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Rpt_Alm_Orden_Entrada.Negocio;

public partial class paginas_Compras_Frm_Ope_Com_Cancelar_Entradas : System.Web.UI.Page
{

    #region VARIABLES / CONSTANTES

    //objeto de la clase de negocio de Requisicion para acceder a la clase de datos y realizar copnexion
    private Cls_Ope_Com_Cancelar_Entradas_Negocio Negocio;

    private const String MODO_LISTADO = "LISTADO";
    private const String MODO_INICIAL = "INICIAL";
    private const String MODO_MODIFICAR = "MODIFICAR";
    private const String MODO_NUEVO = "NUEVO";

    #endregion

    #region PAGE LOAD / INIT

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valores de primera vez
                if (!IsPostBack)
                {
                    Session["Activa"] = true;
                    DateTime _DateTime = DateTime.Now;
                    int dias = _DateTime.Day;
                    dias = dias * -1;
                    dias++;
                    _DateTime = _DateTime.AddDays(dias);
                    _DateTime = _DateTime.AddMonths(-1);

                    Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                    
                    Limpiar_Controles();
                    Llenar_Grid_Entradas();
                    Habilitar_Controles(MODO_LISTADO);
                    
                }
                Mostrar_Informacion("", false);
            }
            catch (Exception ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + ex.Message + "]");
            }
        }    

    #endregion

    #region METODOS

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Habilitar_Controles
        // DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
        // RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 30/Enero/2013 
        // MODIFICO:
        // FECHA_MODIFICO
        // CAUSA_MODIFICACIÓN   
        // *******************************************************************************/
        private void Habilitar_Controles(String Modo)
        {
            try
            {
                switch (Modo)
                {
                    case MODO_LISTADO:

                        Configuracion_Acceso("Frm_Ope_Com_Cancelar_Entradas.aspx");

                        Btn_Modificar.Visible = false;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Listar.Visible = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                        Txt_No_Entrada.Enabled = false;
                        Txt_OC.Enabled = false;
                        Txt_Req.Enabled = false;
                        Txt_Subtotal.Enabled = false;
                        Txt_IVA.Enabled = false;
                        Txt_Total.Enabled = false;
                        Txt_Comentarios.Enabled = false;
                        Cmb_Estatus.Enabled = false;

                        break;

                    case MODO_INICIAL:

                        Configuracion_Acceso("Frm_Ope_Com_Cancelar_Entradas.aspx");

                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Salir.Visible = false;
                        Btn_Salir.ToolTip = "Salir";                        
                        Btn_Listar.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                        Txt_No_Entrada.Enabled = false;
                        Txt_OC.Enabled = false;
                        Txt_Req.Enabled = false;
                        Txt_Total.Enabled = false;
                        Txt_Subtotal.Enabled = false;
                        Txt_IVA.Enabled = false;
                        Txt_Comentarios.Enabled = false;
                        Cmb_Estatus.Enabled = false;

                        break;
                    //Estado de Nuevo
                    case MODO_NUEVO:

                        Btn_Listar.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Salir.Visible = true;

                        Txt_No_Entrada.Enabled = false;
                        Txt_OC.Enabled = false;
                        Txt_Req.Enabled = false;
                        Txt_Subtotal.Enabled = false;
                        Txt_IVA.Enabled = false;
                        Txt_Total.Enabled = false;
                        Txt_Comentarios.Enabled = false;
                        Cmb_Estatus.Enabled = false;

                        break;
                    //Estado de Modificar
                    case MODO_MODIFICAR:

                        Btn_Listar.Visible = false;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Modificar.ToolTip = "Guardar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";

                        Txt_No_Entrada.Enabled = false;
                        Txt_OC.Enabled = false;
                        Txt_Req.Enabled = false;
                        Txt_Subtotal.Enabled = false;
                        Txt_IVA.Enabled = false;
                        Txt_Total.Enabled = false;
                        Txt_Comentarios.Enabled = true;
                        Cmb_Estatus.Enabled = true;

                        break;
                    default: break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion(ex.ToString(), true);
            }
        }
    
        ///*******************************************************************************
        //NOMBRE DE LA FUNCIÓN: Mostrar_Información
        //DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
        //RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 30/Enero/2013 
        //MODIFICO:
        //FECHA_MODIFICO:
        //CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            try
            {
                Lbl_Informacion.Visible = mostrar;
                Img_Warning.Visible = mostrar;
                Lbl_Informacion.Text = txt;
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Mostrar_Informacion Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        //NOMBRE DE LA FUNCIÓN: Limpiar_Controles
        //DESCRIPCIÓN: Limpia las cajas de texto
        //RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 05/Febrero/2013 
        //MODIFICO:
        //FECHA_MODIFICO:
        //CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Limpiar_Controles()
        {
            try
            {
                Txt_Comentarios.Text = "";
                Session.Remove("Dt_Entradas");
                Session.Remove("Dt_Detalles");
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Limpiar_Controles Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Contrarecibos
        // DESCRIPCIÓN: Llena el grid principal de contrarecibos
        // RETORNA: 
        // CREO: David Herrera Rincon
        // FECHA_CREO: 29/Enero/2013 
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        public void Llenar_Grid_Entradas()
        {
            try
            {
                //Ponemos visibles los divs
                Div_Contenido.Visible = false;
                Div_Listado_Requisiciones.Visible = true;

                //Declaracion de variables
                Negocio = new Cls_Ope_Com_Cancelar_Entradas_Negocio();

                //Asiganamos valores
                Negocio.P_No_Entrada = Txt_Busqueda.Text;
                Negocio.P_Fecha_Inicio = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicial.Text);
                Negocio.P_Fecha_Fin = Txt_Fecha_Final.Text;
                Negocio.P_Fecha_Fin = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);

                //Realizamos la consulta
                Session["Dt_Entradas"] = Negocio.Consultar_Entradas();

                //Valdamos si trae datos la consulta
                if (Session["Dt_Entradas"] != null && ((DataTable)Session["Dt_Entradas"]).Rows.Count > 0)
                {
                    Div_Contenido.Visible = false;
                    Grid_Entradas.DataSource = Session["Dt_Entradas"] as DataTable;
                    Grid_Entradas.DataBind();
                }
                else
                {
                    Grid_Entradas.DataSource = Session["Dt_Entradas"] as DataTable;
                    Grid_Entradas.DataBind();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Llenar_Grid_Entradas Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Evento_Grid_Contrarecibos_Seleccionar
        ///DESCRIPCIÓN: Metodo que llena los controles con los datos relacionados del contrarecibo 
        ///en la busqueda del Modalpopup
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 30/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Evento_Grid_Seleccionar(String Dato)
        {
            try
            {
                Habilitar_Controles(MODO_INICIAL);

                Div_Listado_Requisiciones.Visible = false;
                Div_Contenido.Visible = true;

                String No_Entrada = Dato;
                DataRow[] Entrada = ((DataTable)Session["Dt_Entradas"]).Select("No_Entrada = '" + No_Entrada + "'");

                //Asignamos valores de cabecera
                Txt_No_Entrada.Text = No_Entrada;
                Txt_OC.Text = Entrada[0][Ope_Com_Ordenes_Compra.Campo_Folio].ToString();
                Txt_Req.Text = Entrada[0]["REQ"].ToString();
                Txt_Total.Text = Entrada[0][Alm_Com_Entradas.Campo_Total].ToString();
                Txt_IVA.Text = Entrada[0][Alm_Com_Entradas.Campo_IVA].ToString();
                Txt_Subtotal.Text = Entrada[0][Alm_Com_Entradas.Campo_Subtotal].ToString();

                Cls_Rpt_Alm_Orden_Entrada_Negocio Orden_Entrada = new Cls_Rpt_Alm_Orden_Entrada_Negocio();

                //Consultamos los datos de facturacion
                Orden_Entrada.P_Orden_Entrada = No_Entrada;
                DataTable Dt_Detalles = Orden_Entrada.Detalles_Orden_Entrada().Tables["Detalles"];
                //Asignamos los datos de facturacion
                Session["Dt_Detalles"] = Dt_Detalles;
                Grid_Facturas.DataSource = Dt_Detalles;
                Grid_Facturas.DataBind();
                
            }
            catch (Exception ex)
            {
                throw new Exception("Error en Evento_Grid_Seleccionar Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        //NOMBRE DE LA FUNCIÓN: Cancelar_Entrada
        //DESCRIPCIÓN: Metodo que cancela las entradas
        //RETORNA: 
        //CREO: David Herrera Rincon
        //FECHA_CREO: 05/Febrero/2013 
        //MODIFICO:
        //FECHA_MODIFICO:
        //CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Cancelar_Entrada()
        {
            //Declaracion de variables
            Negocio = new Cls_Ope_Com_Cancelar_Entradas_Negocio();

            try
            {
                //Asignamos valores
                Negocio.P_No_Requisicion = Txt_Req.Text.Replace("RQ-", "");
                Negocio.P_No_Entrada = int.Parse(Txt_No_Entrada.Text).ToString("0000000000");
                Negocio.P_Total = Txt_Total.Text;
                Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Text;
                Negocio.P_Comentario = Txt_Comentarios.Text;
                //Asignamos los detalles
                Negocio.P_Dt_Detalles = (DataTable)Session["Dt_Detalles"];
                //Cancelamos la entrada
                Negocio.Cancelar_Entrada();

                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cancelar Entrada", "alert('Se cancelo la entrada exitosamente.');", true);

                Limpiar_Controles();
                Llenar_Grid_Entradas();
                Habilitar_Controles(MODO_LISTADO);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }

    #endregion

    #region EVENTOS

        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Salir.ToolTip == "Cancelar")
                {
                    //Habilitamos controles y llenamos el grid
                    Limpiar_Controles();
                    Llenar_Grid_Entradas();
                    Habilitar_Controles(MODO_LISTADO);
                    
                }
                else
                    //Redireccionamos a la pagina principal
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Salir_Click [" + ex.ToString() +"]", true);
            }
        }
            
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Habilitamos controles y llenamos el grid
                Habilitar_Controles(MODO_INICIAL);
                Llenar_Grid_Entradas();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Buscar_Click [" + ex.ToString() + "]", true);
            }
        }

        protected void Btn_Listar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Habilitamos controles y llenamos el grid
                Llenar_Grid_Entradas();
                Habilitar_Controles(MODO_LISTADO);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Listar_Click [" + ex.ToString() + "]", true);
            }
        }

        protected void Btn_Seleccionar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Llenamos los otros controles para visualizar los datos
                String No_Entrada = ((ImageButton)sender).CommandArgument;
                Evento_Grid_Seleccionar(No_Entrada);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Btn_Seleccionar_Click [" + ex.ToString() + "]", true);
            }
        }

        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Btn_Modificar.ToolTip == "Actualizar")
                    Habilitar_Controles(MODO_MODIFICAR);
                else 
                {
                    if (!String.IsNullOrEmpty(Txt_Comentarios.Text))
                        Cancelar_Entrada();
                    else                    
                        Mostrar_Informacion("Faltan los comentarios.", true);
                                        
                }
            }
            catch (Exception ex)
            {
                Lbl_Informacion.Visible = true;
                Img_Warning.Visible = true;
                Lbl_Informacion.Text = ex.Message;
            }
        }

    #endregion

    #region EVENTOS GRID   
    
        protected void Grid_Entradas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Entradas.DataSource = ((DataTable)Session["Dt_Entradas"]);
                Grid_Entradas.PageIndex = e.NewPageIndex;
                Grid_Entradas.DataBind();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Grid_Entradas_PageIndexChanging [" + ex.ToString() + "]", true);
            }
        }

        protected void Grid_Facturas_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                //Llenamos el grid con los datos de la pagina seleccionada
                Grid_Facturas.DataSource = ((DataTable)Session["Dt_Detalles"]);
                Grid_Facturas.PageIndex = e.NewPageIndex;
                Grid_Facturas.DataBind();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: Grid_Facturas_PageIndexChanging [" + ex.ToString() + "]", true);
            }
        }       

    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.            
            Botones.Add(Btn_Buscar);
            Botones.Add(Btn_Modificar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion    

    
}

