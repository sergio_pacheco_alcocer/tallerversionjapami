<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Filtrar_Cotizaciones.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Filtrar_Cotizaciones" 
Title="Filtrado de Cotizaciones" Culture="es-MX"%>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPIRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_sesiones.ashx";

        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesi�n activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
            
                        
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                       <%-- <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>--%>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo" align="center">
                                Filtrar Cotizaciones
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" CssClass="Img_Button" ToolTip="Nuevo" OnClick="Btn_Nuevo_Click" />
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" OnClick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" CssClass="Img_Button" AlternateText="Eliminar" ToolTip="Eliminar" />
                                <asp:ImageButton ID="Btn_Listar_Cotizaciones" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Listar Cotizaciones" OnClick="Btn_Listar_Cotizaciones_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td>
                            </td>
                            <td align = "right">
                            <asp:LinkButton ID="Btn_Busqueda_Avanzada" runat="server" 
                                onclick="Btn_Busqueda_Avanzada_Click">Busqueda Avanzada</asp:LinkButton>
                        </td>
                        </tr>
                    </table>
                </div>
                <p/>
                
                <div ID="Div_Busqueda_Avanzada" runat="server" visible="false" width="99%"
                        style="overflow:auto;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                        <table width="100%" >
                            <tr style="background-color:Silver;width:99%;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                                <td align="right" width= "100%">
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Click"
                                        />
                                    <asp:ImageButton ID="Btn_Limpiar_Busqueda_Avanzada" runat="server" ToolTip="Limpiar"
                                        ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" onclick="Btn_Limpiar_Busqueda_Avanzada_Click" 
                                        />
                                    <asp:ImageButton ID="Btn_Cerrar_Busqueda_Avanzada" runat="server" ToolTip="Cerrar"
                                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" onclick="Btn_Cerrar_Busqueda_Avanzada_Click" 
                                        />     
                                </td>
                            </tr>
                            </table>
                            <table width="100%" >
                            <tr>
                                <td width= "15%">
                                    Del : 
                                </td>
                                <td width = "35%">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                ValidChars="/_" />
                            <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                </td>
                                <td width= "15%">
                                    Al : 
                                </td>
                                <td width = "35%">
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Final_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Final" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                ValidChars="/_" />
                            <cc1:CalendarExtender ID="Txt_Fecha_Final_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                                </td>
                            </tr>
                            <tr>
                                <td width= "15%">
                                    No. Requisicion : 
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="70%" MaxLength="13"></asp:TextBox>
                                       
                                        <%--<cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="Txt_Busqueda"                                                      WatermarkText="&lt;RQ-000000&gt;" WatermarkCssClass="watermarked">
                                        </cc1:TextBoxWatermarkExtender>--%>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender3" runat="server" FilterType="Custom" 
                                    TargetControlID="Txt_Busqueda" ValidChars="0123456789">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td width= "15%">
                                    Estatus : 
                                </td>
                                <td style="width: 35%;">
                                    <asp:DropDownList ID="Cmb_Estatus_Busqueda" runat="server" Width="70%" >
                                    <asp:ListItem Value=""><--Seleccionar--></asp:ListItem>
                                    <asp:ListItem Value="PROVEEDOR">PROVEEDOR</asp:ListItem>
                                    <asp:ListItem Value="FILTRADA">FILTRADA</asp:ListItem>
                                    <asp:ListItem Value="COTIZADA-RECHAZADA">COTIZADA-RECHAZADA</asp:ListItem> 
                                    </asp:DropDownList>
                      
                            </td>
                            </tr>
                        </table>
                                
                    </div> <%--Busqueda avanzada--%>
                    <p/>
                <%--Div listado de Cotizaciones--%>
                <div id="Div_Listado_Cotizaciones" runat="server" >

                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                Unidad Responsable
                            </td>
                            <td>
                                <%--<asp:DropDownList ID="Cmb_Dependencia_Panel" runat="server" Width="98%" />--%>
                                <asp:TextBox ID="Txt_Dependencia" runat="server" Width="96%" />
                            </td>
                            <td style="width: 15%;" align="right" visible="false">
                                Tipo
                            </td>
                            <td visible="false">
                                <%--<asp:DropDownList ID="Cmb_Tipo_Busqueda" runat="server" Width="98%" />--%>
                                <asp:TextBox ID="Txt_Tipo" runat="server" Width="85%" />
                            </td>
                        </tr>                    
                        <tr>
                            <td style="width: 15%;">
                               * Estatus
                            </td>
                            <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" AutoPostBack="true"
                                    onselectedindexchanged="Cmb_Estatus_SelectedIndexChanged" >
                                <asp:ListItem Value=""><--Seleccionar--></asp:ListItem>
                                <asp:ListItem Value="EN CONSTRUCCION">EN CONSTRUCCION</asp:ListItem>  
                            </asp:DropDownList>
                      
                            </td>
                            <td align="right" visible="false">
                                Folio
                            </td>
                            <td visible="false">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="85%" MaxLength="13"></asp:TextBox>
                                <%--<asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                    OnClick="Btn_Buscar_Click" ToolTip="Consultar"/>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="Txt_Folio"                                                      WatermarkText="&lt;RQ-000000&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Folio" runat="server" FilterType="Custom" 
                                    TargetControlID="Txt_Folio" ValidChars="rRqQ-0123456789">
                                </cc1:FilteredTextBoxExtender>--%>
                            </td>
                        </tr>
                        <div id="Div_Comentarios" runat="server">
                            <tr>
                                <td align="left">
                                       * Comentarios
                                    <asp:LinkButton ID="LinkButton1" runat="server"  Visible="false">Mostrar
                                    </asp:LinkButton>
                                </td>
                                    <td align="left" colspan="3">
                                        <asp:TextBox ID="Txt_Comentario" runat="server" MaxLength="250" TabIndex="10" TextMode="MultiLine" 
                                            Width="100%" Height="65px"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender 
                                            ID="TextBoxWatermarkExtender4" runat="server" 
                                            TargetControlID="Txt_Comentario"
                                            WatermarkText="&lt;L�mite de Caracteres 250&gt;" WatermarkCssClass="watermarked">
                                        </cc1:TextBoxWatermarkExtender>
                                        <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" TargetControlID="Txt_Comentario"                                                      FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="������������.,;: ">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                            </tr>
                        </div>
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                              <div style="overflow:auto;height:320px;width:99%;vertical-align:top;border-style:outset;border-color: Silver;" > 
                                <asp:GridView ID="Grid_Cotizaciones" runat="server" AllowPaging="True" AutoGenerateColumns="False" 
                                CssClass="GridView_1" GridLines="None" Width="100%" OnPageIndexChanging="Grid_Cotizaciones_PageIndexChanging"
                                    DataKeyNames="No_Requisicion"
                                    AllowSorting="true" HeaderStyle-CssClass="tblHead" OnSorting="Grid_Cotizaciones_Sorting" 
                                    onrowdatabound="Grid_Cotizaciones_RowDataBound" 
                                      EmptyDataText="No se encontraron Cotizaciones" 
                                      onselectedindexchanged="Grid_Cotizaciones_SelectedIndexChanged">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="5%" />
                                    </asp:ButtonField>                                      
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" Visible="True" SortExpression="No_Requsicion">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>
                                        <%--<asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha Inicial" 
                                            DataFormatString="{0:dd/MMM/yyyy}" Visible="True" SortExpression="Fecha_Creo">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>--%>
                                        <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" Visible="True" SortExpression=                                                      "DEPENDENCIA">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField> 
                                        <asp:BoundField DataField="TOTAL" HeaderText="TOTAL" 
                                             Visible="True" SortExpression="TOTAL">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>                                                                            
                                        <asp:BoundField DataField="TIPO_ARTICULO" HeaderText="Tipo" Visible="True" SortExpression="TIPO_ARTICULO">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="" Visible="false">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Alerta" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/gridview/circle_grey.png"                                                                                                        
                                                    CommandArgument='<%# Eval("No_Requisicion") %>'/>
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="4%" />
                                            <ItemStyle HorizontalAlign="Center" Width="4%" />
                                        </asp:TemplateField>                                                                                   
                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True" SortExpression="Estatus">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto" Visible="True" >
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="No_Requisicion" HeaderText="No_Requisicion" Visible="False" 
                                            SortExpression="No_Requisicion">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                                        </asp:BoundField>                                        
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                              </div><%--sin id--%>  
                            </td>
                        </tr>
                    </table>
                </div><%--Listado--%>
             
                
            </ContentTemplate>
        </asp:UpdatePanel>

    </div>    
</asp:Content>

