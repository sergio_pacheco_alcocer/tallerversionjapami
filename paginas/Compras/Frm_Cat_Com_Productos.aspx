<%@ Page Title="Cat�logo de Productos" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Com_Productos.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Com_Productos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script language="javascript" type="text/javascript">
// <!CDATA[
function get_KeyPress(textbox, evento) {
            //debugger;
            var keyCode;
            if (evento.which || evento.charCode) {
                keyCode = evento.which ? evento.which : evento.charCode;
                //return (keyCode != 13);
            }
            else if (window.event) {
                keyCode = event.keyCode;
                if (keyCode == 13) {
                    if (event.keyCode)
                        event.keyCode = 9;
                }
            }

            if (keyCode == 13) {

                (document.getElementById('<%=Btn_Buscar_Producto.ClientID %>')).click();
                //                alert("se presiono Enter");
                window.focus();
                return false;
            }
            return true;
}
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">

<!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesi�n activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Productos" runat="server" AsyncPostBackTimeout="360000"/>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
        
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
                
            <div id="Div_Productos" style="background-color:#ffffff; width:100%; height:100%;">          
                <table  id="Tabla_Generar" width="100%" class="estilo_fuente"> 
                 <tr>
                  <td>
                       <table width="100%" class="estilo_fuente">
                            <tr align="center">
                                <td class="label_titulo">
                                    Cat�logo de Productos
                                </td>
                            </tr>
                            <tr>
                                <td>&nbsp;
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"/>&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                                </td>
                            </tr>
                       </table>
            
                       <table width="98%"  border="0" cellspacing="0">
                                 <tr align="center">
                                     <td>                
                                         <div align="right" style="width:99%; background-color: #2F4E7D; color: #FFFFFF; font-weight: bold; font-style: normal; font-variant: normal; font-family: fantasy; height:32px"  >                        
                                              <table style="width:100%;height:28px;">
                                                <tr>
                                                  <td align="left" style="width:59%;">  
                                                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" TabIndex="20"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" onclick="Btn_Nuevo_Click" />
                                                        <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" TabIndex="6"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />
                                                        <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                                                            ToolTip="Exportar Excel"  AlternateText="Consultar" 
                                                            onclick="Btn_Exportar_Excel_Click"/>
                                                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" TabIndex="4"
                                                            ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                                                  </td>
                                                   <td align = "right">
                                                    <asp:LinkButton ID="Btn_Busqueda_Avanzada" runat="server" 
                                                        onclick="Btn_Busqueda_Avanzada_Click">Busqueda Avanzada</asp:LinkButton>
                                                    </td>
                                                  
                                                 </tr>
                                              </table>
                                            </div>
                                     </td>
                                 </tr>
                        </table>  
                 </td>
                </tr>                                          
               <tr>
                  <td>
                     <table width="99.3%" class="estilo_fuente">                           
                        <tr>
                          <td> 
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Producto" runat="server" 
                            WatermarkCssClass="watermarked"
                            WatermarkText="<Ingrese Nombre>" TargetControlID="Txt_Nombre_Producto_B" />
                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Producto" 
                            runat="server" TargetControlID="Txt_Nombre_Producto_B" 
                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="��.,:;()����������-_%/[]$@&*+-!?=�� "/>
                            
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" 
                            WatermarkCssClass="watermarked"
                            WatermarkText="<Ingrese Clave>" TargetControlID="Txt_Clave_B" />
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender2" 
                            runat="server" TargetControlID="Txt_Clave_B" 
                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="��.,:;()����������-_%/[]$@&*+-!?=�� "/>
                            
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
                            WatermarkCssClass="watermarked"
                            WatermarkText="<Ingrese la Ref>" TargetControlID="Txt_Ref_B" />
                             <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" 
                            runat="server" TargetControlID="Txt_Ref_B" 
                            FilterType=" Numbers" />
                        <asp:Panel ID="Div_Busqueda_Avanzada" runat="server" DefaultButton="Btn_Buscar_Producto"
                                   style="overflow:auto;width:99%;vertical-align:top;border-style:outset;border-color:Silver;">
                                    <table width="99%" class="estilo_fuente">
                                    <tr style="background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                                        <td colspan="4" align="right">
                                             <asp:ImageButton ID="Btn_Buscar_Producto" runat="server" 
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                        onclick="Btn_Buscar_Producto_Click" ToolTip="Buscar Producto" TabIndex="2"/>
                                        &nbsp;
                                        
                                        <asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" 
                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                        onclick="Btn_Limpiar_Click" ToolTip="Limpiar"  />
                                        
                                        <asp:ImageButton ID="Btn_Cerrar_Busqueda_Avanzada" runat="server" ToolTip="Cerrar"
                                                ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" onclick="Btn_Cerrar_Busqueda_Avanzada_Click" 
                                                />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;width:20%;">Clave</td>
                                        <td style="text-align:left;width:20%;">
                                             <asp:TextBox ID="Txt_Clave_B" runat="server"  MaxLength="10" Width="100%" ></asp:TextBox>
                                        &nbsp;&nbsp;
                                        </td>
                                        <td style="text-align:left;width:20%;">
                                               &nbsp;&nbsp;&nbsp; Nombre 
                                        </td>
                                        <td style="text-align:left;width:40%;">
                                            <asp:TextBox ID="Txt_Nombre_Producto_B" runat="server"  MaxLength="50" Width="100%" ></asp:TextBox>
                                         &nbsp;&nbsp;
                                       </td>
                                          
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;width:20%;">
                                                Estatus &nbsp; 
                                       </td> 
                                       <td style="text-align:left;width:20%;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Estaus" runat="server" Width="100%">
                                                 <asp:ListItem Value="SELECCIONE"> SELECCIONE </asp:ListItem>
                                                 <asp:ListItem Value="ACTIVO">ACTIVO</asp:ListItem>
                                                 <asp:ListItem Value="INACTIVO">INACTIVO</asp:ListItem>
                                                 </asp:DropDownList>
                                       </td> 
                                        <td style="text-align:left;width:20%;">
                                                &nbsp;&nbsp;&nbsp;Categoria &nbsp; 
                                       </td> 
                                       <td style="text-align:left;width:40%;">
                                           <asp:DropDownList ID="Cmb_Busqueda_Categoria" runat="server" Width="100%">
                                           </asp:DropDownList>
                                        </td> 
                                    
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;width:20%;">
                                         Ref
                                        </td>
                                        <td  style="text-align:left;width:20%;">
                                            <asp:TextBox ID="Txt_Ref_B" runat="server"  MaxLength="10" Width="100%" ></asp:TextBox>
                                            &nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    </table>
                                </asp:Panel>  
                             </td>
                            </tr>
                        </table>    
                  </td>
               </tr>
               <tr>
                <td>
                    <br />
                    <asp:HiddenField ID="Txt_Producto_ID" runat="server" />
                    <asp:HiddenField ID="Hdn_Txt_Descripcion" runat="server" />
                    <table class="estilo_fuente" width="98%">
                            <tr> 
                                 <td>
                                      <table class="estilo_fuente" width="98%">
                                                <!-- Campos datos de producto -->
                                                <tr>
                                                    <td style="text-align:left;width:15%">
                                                        <asp:Label ID="Lbl_Txt_Clave" runat="server" AssociatedControlID="Txt_Clave" 
                                                            Text="Clave" ></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;">
                                                        <asp:TextBox ID="Txt_Clave" runat="server" BackColor="#F3F3F3" ReadOnly="True" style="width:246px;" 
                                                            TabIndex="7" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave" runat="server" 
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Clave" ValidChars="������������-" />
                                                    </td>
                                                    <td style="text-align:left;width:15%;">
                                                        <asp:Label ID="Lbl_Tipo" runat="server" 
                                                            AssociatedControlID="Cmb_Tipo" Text="*Tipo"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:DropDownList ID="Cmb_Tipo" runat="server" style="width:260px;" 
                                                            TabIndex="8">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:Label ID="Lbl_Txt_Nombre_Producto" runat="server" 
                                                            AssociatedControlID="Txt_Nombre_Producto" Text="*Nombre"></asp:Label>
                                                    </td>
                                                    <td colspan="3" style="text-align:left;">
                                                        <asp:TextBox ID="Txt_Nombre_Producto" runat="server" MaxLength="100" 
                                                            style="width:655px;" TabIndex="9" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Producto" runat="server" 
                                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Nombre_Producto" ValidChars='��.,:;()����������-_%/[]$@&*+-!?=�� "' />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:Label ID="Lbl_Txt_Descripcion" runat="server" 
                                                            AssociatedControlID="Txt_Descripcion" Text="*Descripci�n"></asp:Label>
                                                    </td>
                                                    <td colspan="3" style="text-align:left;width:30%;" >
                                                        <asp:TextBox ID="Txt_Descripcion" runat="server" Height="65" MaxLength="255" 
                                                            style="width:657px;" TabIndex="10" TextMode="MultiLine" />
                                                        <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Descripcion" runat="server" 
                                                            TargetControlID="Txt_Descripcion" WatermarkCssClass="watermarked" 
                                                            WatermarkText="L�mite de Caractes 250">
                                                        </cc1:TextBoxWatermarkExtender>
                                                        <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" 
                                                            runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                            TargetControlID="Txt_Descripcion" 
                                                            ValidChars='��.,:;()����������-_%/[]$@&*+-!?=�� "'>
                                                        </cc1:FilteredTextBoxExtender>
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:Label ID="Lbl_Chk_Stock" runat="server" Text="*Stock"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:DropDownList ID="Cmb_Stock" runat="server" AutoPostBack="True" 
                                                            onselectedindexchanged="Cmb_Stock_SelectedIndexChanged" style="width:250px" 
                                                            TabIndex="11">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="text-align:left;width:15%;">
                                                        <asp:Label ID="Lbl_Cmb_Estatus" runat="server" 
                                                            AssociatedControlID="Cmb_Estatus" Text="Estatus"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" style="width:260px;" 
                                                            TabIndex="12">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:Label ID="Lbl_Txt_Costo" runat="server" AssociatedControlID="Txt_Costo" 
                                                            Text="*Costo"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:TextBox ID="Txt_Costo" runat="server" AutoPostBack="true" MaxLength="9" 
                                                            ontextchanged="Txt_Costo_TextChanged" style="width:246px;" TabIndex="13" />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Costo" runat="server" 
                                                            FilterType="Custom, Numbers" TargetControlID="Txt_Costo" ValidChars=".," />
                                                    </td>
                                                    <td style="text-align:left;width:15%;">
                                                        <asp:Label ID="Lbl_Txt_Costo_Promedo" runat="server" 
                                                            AssociatedControlID="Txt_Costo_Promedio" Text="Costo promedio" Visible="true"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:TextBox ID="Txt_Costo_Promedio" runat="server" MaxLength="9" Enabled="true" Visible="true"
                                                              style="width:246px;" TabIndex="13"  />
                                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Costo_Promedio" runat="server" 
                                                            FilterType="Custom, Numbers" TargetControlID="Txt_Costo_Promedio" 
                                                            ValidChars="." />
                                                    </td>
                                                </tr>
                                                <!-- Combos tipo de producto-->
                                                <tr>
                                                    <td style="text-align:left;width:15%;">
                                                        *Unidad</td>
                                                    <td style="text-align:left;width:30%;">
                                                        <asp:DropDownList ID="Cmb_Unidad" runat="server"  style="width:100%;" 
                                                            TabIndex="15">
                                                        </asp:DropDownList>
                                                    </td>
                                                    <td style="text-align:left;width:15%;">
                                                    
                                                        Tipo Resguardo</td>
                                                    <td style="text-align:left;width:30%;">
                                                    
                                                        <asp:DropDownList ID="Cmb_Tipo_Resguardo" runat="server" style="width:260px;" >
                                                            <asp:ListItem >--SELECCIONAR--</asp:ListItem>
                                                            <asp:ListItem Value="RESGUARDO">RESGUARDO</asp:ListItem>
                                                            <asp:ListItem Value="CUSTODIA">CUSTODIA</asp:ListItem>
                                                        </asp:DropDownList>
                                                    
                                                    </td>
                                                </tr>                                                
                                                <tr>
                                                    <td style="text-align:left;">
                                                        *<asp:Label ID="Lbl_Capitulo" runat="server" Text="Cap�tulo"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="Cmb_Capitulo" runat="server" AutoPostBack="True" 
                                                            onselectedindexchanged="Cmb_Capitulo_SelectedIndexChanged" 
                                                            TabIndex="16" 
                                                            Width="660px">
                                                        </asp:DropDownList>
                                                    </td>                                                                                                                                                                                                                
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;width:15%;">
                                                        <asp:Label ID="Lbl_Concepto" runat="server" Text="*Concepto"></asp:Label>
                                                    </td>
                                                    <td style="text-align:left;width:30%;" colspan="3">
                                                        <asp:DropDownList ID="Cmb_Conceptos" runat="server" AutoPostBack="True" 
                                                            onselectedindexchanged="Cmb_Conceptos_SelectedIndexChanged" TabIndex="17" 
                                                            Width="660px">
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;">                                                        
                                                        <asp:Label ID="Lbl_Partida_General" runat="server" Text="*Partida Generica"></asp:Label>
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="Cmb_Partida_General" runat="server" AutoPostBack="True" 
                                                            onselectedindexchanged="Cmb_Partida_General_SelectedIndexChanged" TabIndex="18" 
                                                            Width="660px">
                                                        </asp:DropDownList>
                                                    </td>
                                                    
                                                </tr>
                                                <tr>
                                                    <td style="text-align:left;">
                                                        <asp:Label ID="Lbl_Partida_Especifica" runat="server" Text="*Partida Especifica"></asp:Label>
                                                        &nbsp;&nbsp;&nbsp;
                                                        <asp:ImageButton ID="Btn_Descripcion" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_advice.png" onclick="Btn_Descripcion_Click" ToolTip="Descripci�n" Visible="false" />
                                                    </td>
                                                    <td colspan="3">
                                                        <asp:DropDownList ID="Cmb_Partida_Especifica" runat="server" AutoPostBack="True" onselectedindexchanged="Cmb_Partida_Especifica_SelectedIndexChanged" TabIndex="19" Width="660px">
                                                        </asp:DropDownList>
                                                    </td>
                                                 </tr>
                                                  <tr>
                                                            <td align="left">Categor&iacute;a</td>
                                                            <td align="left"><asp:DropDownList ID="Cmb_Categorias" runat="server" Width="100%" TabIndex="20"></asp:DropDownList></td>
                                                             <td align="left">Almac&eacute;n General</td>
                                                             <td>
                	                                            <asp:CheckBox ID="Chx_Almacen_General_ID" runat="server" />
                                                            </td>
    
                                                   </tr>
                                                   <tr>
                                                            <td colspan="4">
                                                                <hr ID="Hr3" runat="server" onclick="return Hr3_onclick() " style="width:95%; text-align:left;" />
                                                            </td>
                                                    </tr>
                                                    <tr>
                                                          <td>
                                                                Impuesto
                                                            </td>
                                                            <td style="text-align:left;width:30%;">
                                                                <asp:DropDownList ID="Cmb_Impuesto" runat="server" AutoPostBack="true" onselectedindexchanged="Cmb_Impuesto_SelectedIndexChanged" style="width:250px;" TabIndex="21">
                                                                </asp:DropDownList>
                                                            </td>
                                                           
                                                            <td style="text-align:left; width:15%;">
                                                                Clave Anterior
                                                            </td>
                                                            <td style="text-align:left;width:30%;">
                                                                <asp:TextBox ID="Txt_clave_anterior" runat="server"></asp:TextBox>
                                                            </td>
                                                    </tr>
                                                    <tr align="center">
                                                        <td colspan="4">
                                                          <div style="overflow:auto;height:590px;width:99%;vertical-align:top;border-style:outset;border-color: Silver;" > 
                                                            <asp:GridView ID="Grid_Productos" runat="server" AllowPaging="false" AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" 
                                                                onpageindexchanging="Grid_Productos_PageIndexChanging" onselectedindexchanged="Grid_Productos_SelectedIndexChanged" 
                                                                OnRowDataBound="Grid_Productos_RowDataBound" Style="white-space:normal;" Width="100%">
                                                                <Columns>
                                                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                                        <ItemStyle HorizontalAlign="Center" Width="7%" />
                                                                    </asp:ButtonField>
                                                                    <asp:BoundField DataField="Producto_ID" HeaderText="Producto ID" Visible="False">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Clave" HeaderText="Clave" Visible="True">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Nombre" HeaderText="Producto" Visible="True">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" Visible="True">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="30%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="MODELOS_NOMBRE" HeaderText="Modelo">
                                                                        <ItemStyle Font-Size="X-Small" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Stock" HeaderText="Stock" Visible="True">
                                                                        <FooterStyle HorizontalAlign="Center" />
                                                                        <HeaderStyle HorizontalAlign="Center" Width="7%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="7%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Disponible" HeaderText="Disponible" Visible="True">
                                                                        <FooterStyle HorizontalAlign="Center" />
                                                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="True">
                                                                        <FooterStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="DESCRIPCION_P_ESPECIFICA" HeaderText="Partida" Visible="True">
                                                                        <FooterStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="REF_JAPAMI" HeaderText="Ref_JAPAMI" Visible="True">
                                                                        <FooterStyle HorizontalAlign="Left" />
                                                                        <HeaderStyle HorizontalAlign="Left" Width="7%" />
                                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField HeaderText="">
                                                                    <ItemTemplate>
                                                                    <asp:ImageButton ID="Btn_Alerta" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_red.png" Visible="false" />
                                                                    </ItemTemplate>
                                                                    <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                    <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField> 
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="GridHeader" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                          </div>  
                                                        </td>
                                                    </tr>
                                            </table>
                                        </td>
                                   </tr>
                                   
                                  <tr>
                                     <td >
                                               
                                    </td>
                                 </tr>
                                 <tr>
                                        <td>
                                          <br />
                                            <table border="0" cellspacing="0" class="estilo_fuente" width="99%">
                                               
                                            </table>
                                        </td>
                                  </tr>
                                </table>
                            </td>
                        </tr>
             </table> 
            </div>
        </ContentTemplate>
        <Triggers>                
                <asp:PostBackTrigger ControlID="Btn_Exportar_Excel" />       
            </Triggers>
    </asp:UpdatePanel>
       <%-- fin del panel  --%>
     <asp:Panel ID="Pnl_Foto_Producto" runat="server" Width="320px" Height="90px"
       style="border-style:outset;border-color:Silver;background-repeat:repeat-y;background-color:White;color:White; display:none;">
       <center>
            <table width="100%" class="estilo_fuente">
              <tr  align="center" class="barra_delgada" >
                        <td align="center" > 
                            <asp:Label ID="Lbl_Foto_Producto" runat="server"  Text=" �Guardar Foto del Producto?" Width="100%"/>
                       </td>
              </tr>
              <tr>
                    <td>
                    <br />
                    </td>
              </tr>
             <tr>
                <td>
                    <center>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Btn_Aceptar_Guardar_Foto" runat="server" CssClass="button" ToolTip="Guardar Foto" 
                            onclick="Btn_Aceptar_Guardar_Foto_Click" Text="Aceptar" Width="100px"/>
                        &nbsp;&nbsp;&nbsp;
                        <asp:Button ID="Btn_Cancelar_Guardar_Foto" runat="server" Text="Cancelar" Width="100px" 
                        CssClass="button" onclick="Btn_Cancelar_Guardar_Foto_Click" ToolTip="Cancelar"/>
                    </center>
                    </td>
            </tr>
        </table>
       </center>
    </asp:Panel>
    
         <!-- Modal Popup Foto Producto-->
          <asp:UpdatePanel ID="Udp_Modal_Popup_Foto" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                    <cc1:ModalPopupExtender ID="Modal_Foto_Producto" runat="server"
                            TargetControlID="Btn_MP_Login"
                            PopupControlID="Pnl_Foto_Producto"                      
                            CancelControlID="Btn_Cancelar_Guardar_Foto"
                            DropShadow="True"
                            BackgroundCssClass="progressBackgroundFilter"/>
                    <asp:Button ID="Btn_MP_Login" runat="server" Text="Button" style="display:none;"/>
              </ContentTemplate>          
         </asp:UpdatePanel>
         
         <!-- Modal Popup Modificar Foto-->
          <asp:UpdatePanel ID="Udp_Modal_Popup_Modificar_Foto" runat="server" UpdateMode="Conditional">
             <ContentTemplate>
                    <cc1:ModalPopupExtender ID="Modal_Modificar_Foto" runat="server"
                            TargetControlID="Btn_MP_Mod_Foto"
                            PopupControlID="Pnl_Modificar_Foto"                      
                            CancelControlID="Btn_Cancelar_Modificar_Foto"
                            DropShadow="True"
                            BackgroundCssClass="progressBackgroundFilter"/>
                    <asp:Button ID="Btn_MP_Mod_Foto" runat="server" Text="Button" style="display:none;"/>
              </ContentTemplate>          
         </asp:UpdatePanel>


       <asp:Panel ID="Pnl_Modificar_Foto" runat="server" Width="320px" Height="90px"
           style="border-style:outset;border-color:Silver;background-repeat:repeat-y;background-color:White;color:White; display:none;">
               <center>
                <table width="100%" class="estilo_fuente">
                  <tr  align="center" class="barra_delgada" >
                            <td align="center" > 
                                <asp:Label ID="Lbl_Modificar_Foto" runat="server"  Text=" �Modificar Foto del Producto?" Width="100%"/>
                           </td>
                  </tr>
                  <tr>
                        <td>
                        <br />
                        </td>
                  </tr>
                 <tr>
                    <td>
                        <center>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Btn_Aceptar_Modificar_Foto" runat="server" CssClass="button" ToolTip="Modificar Foto" 
                                onclick="Btn_Aceptar_Modificar_Foto_Click" Text="Aceptar" Width="100px"/>
                            &nbsp;&nbsp;&nbsp;
                            <asp:Button ID="Btn_Cancelar_Modificar_Foto" runat="server" Text="Cancelar" Width="100px" 
                            CssClass="button" onclick="Btn_Cancelar_Modificar_Foto_Click" ToolTip="Cancelar" />
                        </center>
                        </td>
                </tr>
            </table>
       </center>
    </asp:Panel>



          
</asp:Content>

