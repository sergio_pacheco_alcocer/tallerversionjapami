﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Sessiones;
using System.IO;
using System.Data;
using System.Data.OleDb;
using AjaxControlToolkit;
using JAPAMI.Actualizar_Proveedores.Negocio;
using JAPAMI.Constantes;
using System.Globalization;

public partial class paginas_Compras_Frm_Cat_Com_Actualizar_Proveedores : System.Web.UI.Page
{
    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                //ViewState["SortDirection"] = "ASC";
                //Matriz_Conversion_Inicio();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error en el inicio del catalago de matriz. Error [" + Ex.Message + "]");
        }
    }
    #endregion

    #region METODOS
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mostrar_Ocultar_Error
    /// DESCRIPCION : Muestra u oculta los componentes de Error 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 10/Septiembre/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Mostrar_Ocultar_Error(Boolean Estatus) 
    {

        if (Estatus== false) 
        {
            Lbl_Mensaje_Error.Text = String.Empty;
        }
        Lbl_Mensaje_Error.Visible = Estatus;
        Img_Error.Visible = Estatus;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
    ///DESCRIPCIÓN: Realizar la consulta y llenar el grido con estos datos
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/03/2011 12:14:35 p.m.
    ///MODIFICO          : Susana Trigueros Armenta
    ///FECHA_MODIFICO    : 7/OCT/2011
    ///CAUSA_MODIFICACION: Nuevos campos y estructura en el catalogo de Cotizadores
    ///*******************************************************************************
    private void Cargar_Grid_Proveedores(int Page_Index)
    {
        DataTable Dt_Proveedores = new DataTable();
        try
        {
            Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];
            if (Dt_Proveedores != null && Dt_Proveedores.Rows.Count > 0) 
            {
                Grid_Proveedores.PageIndex = Page_Index;
                Grid_Proveedores.DataSource = Dt_Proveedores;
                Grid_Proveedores.DataBind();
            }
           
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al tratar de llena la grid Error[" + Ex.Message + "]");
        }
    }

    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Limpiar_Varibles_Sesion_Grid
    ///DESCRIPCIÓN          : Metodo limpia las varibles de sesion y limpia el grid
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 12/Septiembre/2012 12:37p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    public void Limpiar_Varibles_Sesion_Grid() 
    {
        Session["Dt_Insertar"] = (DataTable) new DataTable();
        Session["Dt_Actualizar"] = (DataTable)new DataTable();
        Session["Dt_Partidas"] = (DataTable)new DataTable();
        Session["Dt_Proveedores"] = (DataTable)new DataTable();
        Session["Dt_Conceptos"] = (DataTable)new DataTable();
        Cargar_Grid_Proveedores(0);
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Columnas_Excel
    ///DESCRIPCIÓN          : Metodo que 
    ///PROPIEDADES          : Objeto DataTable, Del cual se va a nalizar las columnas que debe de conter el excel
    ///                       Para poder hacer la carga masiva
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 12:34p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    public Boolean Validar_Columnas_Excel(DataTable Dt_Tabla)
    {
        Boolean Validacion = true;
        try
        {
            if (!Dt_Tabla.Columns.Contains("NO_PADRON_PROVEEDOR"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna NO_PADRON_PROVEEDOR. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("FECHA_REGISTRO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FECHA_REGISTRO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("RAZON_SOCIAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna RAZON_SOCIAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("NOMBRE_COMERCIAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna NOMBRE_COMERCIAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("REPRESENTANTE_LEGAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - El archivo no contiene la columna REPRESENTANTE_LEGAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("CONTACTO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna CONTACTO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("RFC"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna RFC. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("ESTATUS"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna ESTATUS. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("DIRECCION"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna DIRECCION. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("COLONIA"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna COLONIA. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("CIUDAD"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna CIUDAD . <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("ESTADO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna ESTADO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("TIPO_FISCAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna TIPO_FISCAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("CODIGO_POSTAL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna CODIGO_POSTAL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("TELEFONO1"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna TELEFONO1. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("TELEFONO2"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna TELEFONO2. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("NEXTEL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna NEXTEL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("FAX"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FAX. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("E_MAIL"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna E_MAIL. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("TIPO_PAGO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna TIPO_PAGO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("DIAS_CREDITO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna DIAS_CREDITO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("FORMA_PAGO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FORMA_PAGO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PORCENTAJE_ANTICIPO"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PORCENTAJE_ANTICIPO. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("COMENTARIOS"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna COMENTARIOS. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("FECHA_ACTUALIZACION"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna FECHA_ACTUALIZACION. <br />";
                Validacion = false;
            }
            if (!Dt_Tabla.Columns.Contains("PARTIDAS"))
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - El archivo no contiene la columna PARTIDAS. <br />";
                Validacion = false;
            }
            return Validacion;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Crear_Dts_Proveedores
    ///DESCRIPCIÓN          : Metodo para crear las columnas de las Data Table que guardaran los datos  a insertar y a actualizar
    ///PROPIEDADES          :
    ///CREO                 : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO           : 10/Septiembre/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    private void Crear_Dt_Proveedores( )
    {
        DataTable Dt_Insertar = new DataTable();
        DataTable Dt_Actualizar =  new DataTable();
        DataTable Dt_Partidas = new DataTable();
        DataTable Dt_Conceptos =  new DataTable();
        try
        {
            Dt_Insertar.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("FECHA_REGISTRO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("RAZON_SOCIAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("NOMBRE_COMERCIAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("REPRESENTANTE_LEGAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("CONTACTO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("RFC", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("TIPO_FISCAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DIRECCION", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("COLONIA", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("CIUDAD", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("ESTADO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("CODIGO_POSTAL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("TELEFONO1", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("TELEFONO2", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("NEXTEL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("FAX", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("E_MAIL", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("TIPO_PAGO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("DIAS_CREDITO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("FORMA_PAGO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("PORCENTAJE_ANTICIPO", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("COMENTARIOS", System.Type.GetType("System.String"));
            Dt_Insertar.Columns.Add("FECHA_ACTUALIZACION", System.Type.GetType("System.String"));


            //Campos de la tabla para actualizar los proveedores
            Dt_Actualizar.Columns.Add("PROVEEDOR_ID", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("FECHA_REGISTRO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("RAZON_SOCIAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("NOMBRE_COMERCIAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("REPRESENTANTE_LEGAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("CONTACTO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("RFC", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ESTATUS", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("TIPO_FISCAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DIRECCION", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("COLONIA", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("CIUDAD", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ESTADO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("CODIGO_POSTAL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("TELEFONO1", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("TELEFONO2", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("NEXTEL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("FAX", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("E_MAIL", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("TIPO_PAGO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("DIAS_CREDITO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("FORMA_PAGO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("PORCENTAJE_ANTICIPO", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("COMENTARIOS", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("FECHA_ACTUALIZACION", System.Type.GetType("System.String"));
            Dt_Actualizar.Columns.Add("ID_ANTERIOR", System.Type.GetType("System.String"));


            Dt_Partidas.Columns.Add("Proveedor_ID", System.Type.GetType("System.String"));
            Dt_Partidas.Columns.Add("Partida_ID", System.Type.GetType("System.String"));

            Dt_Conceptos.Columns.Add("Proveedor_ID", System.Type.GetType("System.String"));
            Dt_Conceptos.Columns.Add("Concepto_ID", System.Type.GetType("System.String"));

            Session["Dt_Insertar"] = (DataTable)Dt_Insertar;
            Session["Dt_Actualizar"] = (DataTable)Dt_Actualizar;
            Session["Dt_Partidas"] = (DataTable)Dt_Partidas;
            Session["Dt_Conceptos"] = (DataTable)Dt_Conceptos;

            //Session["Dt_Partidas_Asignadas"] = (DataTable)Dt_Partidas_Asignadas;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }
     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Datos_Obligatorios
    ///DESCRIPCIÓN: Valida que los datos obligatorios que se van a cargar no esten vacios 
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 10/Septiembre/2010 18:06
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public Boolean Validar_Datos_Obligatorios() 
    {
        DataTable Dt_Proveedores = new DataTable();
        Dt_Proveedores=(DataTable)Session["Dt_Proveedores"];//Tabla que contiene todos los registros extraidos del documento de Excel
        Boolean Validar= true;
        try{
                Int32 Contador = 0;
                 foreach(DataRow Registro in Dt_Proveedores.Rows)
                 {
                    Contador ++;
                     if(String.IsNullOrEmpty(Registro["NO_PADRON_PROVEEDOR"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo NO_PADRON_PROVEEDOR del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["FECHA_REGISTRO"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo FECHA_REGISTRO del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["RAZON_SOCIAL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo RAZON_SOCIAL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                      if(String.IsNullOrEmpty(Registro["NOMBRE_COMERCIAL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo NOMBRE_COMERCIAL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                      if(String.IsNullOrEmpty(Registro["REPRESENTANTE_LEGAL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo REPRESENTANTE_LEGAL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["CONTACTO"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo CONTACTO del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                      if(String.IsNullOrEmpty(Registro["RFC"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo RFC del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["ESTATUS"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo ESTATUS del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["TIPO_FISCAL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo TIPO_FISCAL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["DIRECCION"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo DIRECCION del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["COLONIA"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo COLONIA del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["CIUDAD"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo CIUDAD del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["ESTADO"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo ESTADO del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["CODIGO_POSTAL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo CODIGO_POSTAL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                     if(String.IsNullOrEmpty(Registro["TELEFONO1"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo TELEFONO1 del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                      if(String.IsNullOrEmpty(Registro["E_MAIL"].ToString()))
                     {
                        Lbl_Mensaje_Error.Text += "&nbsp; - El campo E_MAIL del registro " + Contador +" no puede estar vacio <br>";
                        Validar=false;
                     }
                    
                 } 
            return Validar;
        }
        catch(Exception Ex)
        {
            throw new  Exception("Error al Error al validar Datos obligatorios[" + Ex.Message + "]");
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Filas_Dt_Proveedores
    ///DESCRIPCIÓN: Metodo que identifica a los proveedores que se van a actualizar y alos proveedores
    ///             que se van a  insertar 
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 10/Septiembre/2010 17:45
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public Boolean Agregar_Filas_Dt_Proveedores() 
    {
        Cls_Cat_Com_Actualizar_Proveedores_Negocio Act_Prov_Negocio = new Cls_Cat_Com_Actualizar_Proveedores_Negocio(); // Clase de negocios

        DataTable Dt_Proveedores = new DataTable();
        Dt_Proveedores=(DataTable)Session["Dt_Proveedores"];//Tabla que contiene todos los registros extraidos del documento de Excel

        DataTable  Dt_Insertar = new DataTable();
        Dt_Insertar = (DataTable)Session["Dt_Insertar"];//Tabla que contendra los registros que se van a Insertar

        DataTable  Dt_Actualizar = new DataTable();
        Dt_Actualizar = (DataTable)Session["Dt_Actualizar"];//Tabla que contendra los registros que se van a acutliazar

        DataTable  Dt_Partidas = new DataTable();
        Dt_Partidas = (DataTable)Session["Dt_Partidas"];//Tabla que contendra los registros de los detalles de las partidas 
                                                        
        DataTable Dt_Conceptos = new DataTable();
        Dt_Conceptos = (DataTable)Session["Dt_Conceptos"];//Tabla que contendra los registros de los detalles de los conceptos que se van a insertar
    
        DataTable Dt_Usos_Multiples = new DataTable();

        DataRow Fila;
        DataRow Fila_Partida;
        DataRow Fila_Concepto;
        Mostrar_Ocultar_Error(false);
        Boolean Validacion = true;
         Int32 Contador = 0;
         String Conjunto_Partidas = String.Empty;
         Boolean ProveedorProvisional = false;
        #region Formato de fechas
        String[]  Formatos = new String[]{
                    "MM/dd/yyyy",
                    "MMM/dd/yyyy",
                    "dd/MM/yyyy",
                    "dd/MMM/yyyy",
                    "dd/MM/yy",
                    "d/MM/yy",
                    "d/M/yy",
                    "dd-MM-yy",
                    "yyyy-MM-dd",
                    "dd-MMM-yyyy",
                    "d-M-yyyy",
                    "d-M-yy",
                    "yyyy/MMM/dd",
                    "yy/MM/dd",
                    "yy/M/dd",
                    "dd/MMMM/yyyy",
                    "MMMM/dd/yyyy",
                    "yyyy/MMMM/dd",
                    "ddMMyyyy",
                    "MMddyyyy",
                    "ddMMMyyyy",
                    "MMMddyyyy",
                    "dddd, dd MMMM yyyy",
                    "dddd, dd MMMM yyyy HH:mm",
                    "dddd, dd MMMM yyyy HH:mm:ss",
                    "MM/dd/yyyy HH:mm",
                    "MM/dd/yyyy HH:mm:ss",
                    "MMMM dd",
                    "ddd, dd MMM yyyy",
                    "dddd, dd MMMM yyyy HH:mm:ss",
                    "yyyy MMMM",
                    "yyyy-MM-dd HH:mm:ss",
                    "MMMMddyyyy",
                    "ddMMMMyyyy",
                    "yyyyddMMMM",
                    "MMddyy",
                    "ddMMyy",
                    "yyMMdd"};
        #endregion

        try
        {
            if (Dt_Proveedores is DataTable)
            {
                foreach (DataRow Registro in Dt_Proveedores.Rows)
                {
                    if (Validacion == true)
                    {
                        Contador++;
                        int Res;
                        if (Int32.TryParse(Registro["NO_PADRON_PROVEEDOR"].ToString(), out Res))
                        {
                            Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));

                            Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Proveedor();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0) //Si existe el proveedor
                            {
                                Fila = Dt_Actualizar.NewRow(); //Crea un nuevo registro a la tabla Actualizar
                                ProveedorProvisional = false;
                            }
                            else
                            {
                                Act_Prov_Negocio.P_Nombre = Registro["RAZON_SOCIAL"].ToString();
                                Act_Prov_Negocio.P_Compania = Registro["NOMBRE_COMERCIAL"].ToString();
                                Act_Prov_Negocio.P_RFC = Registro["RFC"].ToString();
                                Act_Prov_Negocio.P_Contacto = Registro["CONTACTO"].ToString();
                                Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Proveedor_Por_Datos();
                                if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0) //Si existe el proveedor
                                {
                                    Fila = Dt_Actualizar.NewRow();
                                    Fila["ID_ANTERIOR"] = Dt_Usos_Multiples.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                    ProveedorProvisional = true;
                                }
                                else
                                {
                                    Fila = Dt_Insertar.NewRow(); //Crea un nuevo registro a la tabla insertar
                                    ProveedorProvisional = false;
                                }
                            }

                            Conjunto_Partidas = String.Empty;

                            Fila["FECHA_REGISTRO"] = String.Format("{0:dd/MMM/yyyy}", Registro["FECHA_REGISTRO"].ToString());
                            String fecha = Fila["FECHA_REGISTRO"].ToString();
                            Fila["RAZON_SOCIAL"] = Registro["RAZON_SOCIAL"].ToString();
                            Fila["NOMBRE_COMERCIAL"] = Registro["NOMBRE_COMERCIAL"].ToString();
                            Fila["REPRESENTANTE_LEGAL"] = Registro["REPRESENTANTE_LEGAL"].ToString();
                            String Contacto = Registro["CONTACTO"].ToString();
                            Fila["CONTACTO"] = Registro["CONTACTO"].ToString();
                            Fila["RFC"] = Registro["RFC"].ToString();
                            Fila["ESTATUS"] = Registro["ESTATUS"].ToString();
                            Fila["TIPO_FISCAL"] = Registro["TIPO_FISCAL"].ToString();
                            Fila["DIRECCION"] = Registro["DIRECCION"].ToString();
                            Fila["COLONIA"] = Registro["COLONIA"].ToString();
                            Fila["CIUDAD"] = Registro["CIUDAD"].ToString();
                            Fila["ESTADO"] = Registro["ESTADO"].ToString();
                            Fila["CODIGO_POSTAL"] = Registro["CODIGO_POSTAL"].ToString();
                            Fila["TELEFONO1"] = Registro["TELEFONO1"].ToString();
                            DataTable dt = Dt_Actualizar;
                            //if (!String.IsNullOrEmpty(Registro["TELEFONO2"].ToString()))
                            Fila["TELEFONO2"] = Registro["TELEFONO2"].ToString();
                            Fila["NEXTEL"] = Registro["NEXTEL"].ToString();
                            Fila["FAX"] = Registro["FAX"].ToString();
                            Fila["E_MAIL"] = Registro["E_MAIL"].ToString();
                            Fila["TIPO_PAGO"] = Registro["TIPO_PAGO"].ToString();
                            if (String.IsNullOrEmpty(Registro["DIAS_CREDITO"].ToString()))
                            {
                                Fila["DIAS_CREDITO"] = "NULL";
                            }
                            else
                            {
                                Fila["DIAS_CREDITO"] = Registro["DIAS_CREDITO"].ToString();
                            }

                            Fila["FORMA_PAGO"] = Registro["FORMA_PAGO"].ToString();
                            Fila["PORCENTAJE_ANTICIPO"] = Registro["PORCENTAJE_ANTICIPO"].ToString();
                            Fila["COMENTARIOS"] = Registro["COMENTARIOS"].ToString();
                            Fila["FECHA_ACTUALIZACION"] = Registro["FECHA_ACTUALIZACION"].ToString();
                            //partidas del registro n 
                            if (!String.IsNullOrEmpty(Registro["PARTIDAS"].ToString()))
                            {
                                Conjunto_Partidas = Registro["PARTIDAS"].ToString();
                                String[] Partidas = Conjunto_Partidas.Split(','); //Separar las partidas en un arreglo de Strings
                                if (Partidas.Length > 0)
                                {
                                    //Buscar que las partidas esten registradas en la base de datos
                                    for (int i = 0; i < Partidas.Length; i++)
                                    {
                                        Act_Prov_Negocio.P_Partida_Clave = Partidas[i];
                                        if (ProveedorProvisional == false)
                                        {
                                            Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                                        }
                                        else 
                                        {
                                            Act_Prov_Negocio.P_Proveedor_ID = Fila["ID_ANTERIOR"].ToString();
                                        }
                                            Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Detalles_Partidas(); //Consulta si la partida ya se ha asociado a este proveedor
                                            if (Dt_Usos_Multiples == null || Dt_Usos_Multiples.Rows.Count <= 0)
                                            {
                                                Dt_Usos_Multiples = Act_Prov_Negocio.Obtener_Datos_Parida();
                                                if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                                {
                                                    Fila_Partida = Dt_Partidas.NewRow();
                                                    Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                                                    Fila_Partida["Partida_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID];
                                                    //Fila_Partida["Proveedor_ID"] = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                                                    Fila_Partida["Proveedor_ID"] = Act_Prov_Negocio.P_Proveedor_ID;
                                                    Dt_Partidas.Rows.Add(Fila_Partida);

                                                    //Fila_Partida["Concepto_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID];

                                                    DataRow[] Fila_Nueva;
                                                    Fila_Nueva = Dt_Conceptos.Select("Concepto_ID='" + Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID].ToString() + "' AND Proveedor_ID='" + Act_Prov_Negocio.P_Proveedor_ID + "'");
                                                    if (Fila_Nueva.Length <= 0)
                                                    {
                                                        Act_Prov_Negocio.P_Concepto_ID = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID].ToString();
                                                        Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Conceptos_Detalles();
                                                        if (Dt_Usos_Multiples == null || Dt_Usos_Multiples.Rows.Count <= 0)
                                                        {
                                                            Fila_Concepto = Dt_Conceptos.NewRow();
                                                            Fila_Concepto["Proveedor_ID"] = Act_Prov_Negocio.P_Proveedor_ID;
                                                            Fila_Concepto["Concepto_ID"] = Act_Prov_Negocio.P_Concepto_ID;
                                                            Dt_Conceptos.Rows.Add(Fila_Concepto);
                                                        }
                                                    }
                                                }
                                                else
                                                {
                                                    Validacion = false;
                                                    Lbl_Mensaje_Error.Text += Lbl_Mensaje_Error.Text += "&nbsp; - La partida " + Partidas[i] + " del registro " + Contador + " No se ha registrado en la base de datos <br>";
                                                    Dt_Partidas.Rows.Clear(); //Elimina las filas q se habian insertado a la tabla
                                                    Dt_Conceptos.Rows.Clear();//Elimina las filas q se habian insertado a la tabla
                                                    i = Partidas.Length;
                                                }
                                            }//Fin if Si no esta esa partida registrada con ese proveedo
                                            else 
                                            {
                                                if (ProveedorProvisional == true) 
                                                {
                                                    Dt_Usos_Multiples = Act_Prov_Negocio.Obtener_Datos_Parida();
                                                    if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0)
                                                    {
                                                        Fila_Partida = Dt_Partidas.NewRow();
                                                        Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                                                        Fila_Partida["Partida_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID];
                                                        Fila_Partida["Proveedor_ID"] = Act_Prov_Negocio.P_Proveedor_ID;
                                                        Dt_Partidas.Rows.Add(Fila_Partida);

                                                        //Fila_Partida["Concepto_ID"] = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID];

                                                        DataRow[] Fila_Nueva;
                                                        Fila_Nueva = Dt_Conceptos.Select("Concepto_ID='" + Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID].ToString() + "' AND Proveedor_ID='" + Act_Prov_Negocio.P_Proveedor_ID + "'");
                                                        if (Fila_Nueva.Length <= 0)
                                                        {
                                                            Act_Prov_Negocio.P_Concepto_ID = Dt_Usos_Multiples.Rows[0][Cat_SAP_Partida_Generica.Campo_Concepto_ID].ToString();
                                                            Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Conceptos_Detalles();
                                                            if (Dt_Usos_Multiples == null || Dt_Usos_Multiples.Rows.Count <= 0)
                                                            {
                                                                Fila_Concepto = Dt_Conceptos.NewRow();
                                                                Fila_Concepto["Proveedor_ID"] = Act_Prov_Negocio.P_Proveedor_ID;
                                                                Fila_Concepto["Concepto_ID"] = Act_Prov_Negocio.P_Concepto_ID;
                                                                Dt_Conceptos.Rows.Add(Fila_Concepto);
                                                            }
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Validacion = false;
                                                        Lbl_Mensaje_Error.Text += Lbl_Mensaje_Error.Text += "&nbsp; - La partida " + Partidas[i] + " del registro " + Contador + " No se ha registrado en la base de datos <br>";
                                                        Dt_Partidas.Rows.Clear(); //Elimina las filas q se habian insertado a la tabla
                                                        Dt_Conceptos.Rows.Clear();//Elimina las filas q se habian insertado a la tabla
                                                        i = Partidas.Length;
                                                    }
                                                }
                                            }

                                    }//Fin del for partidas
                                }
                            }//fin de IsNullEmpty Partidas
                            if (ProveedorProvisional == false)
                            {
                                Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                            }
                            else
                            {
                                Act_Prov_Negocio.P_Proveedor_ID = Fila["ID_ANTERIOR"].ToString();
                            }
                            Act_Prov_Negocio.P_Nombre = String.Empty;
                            //Act_Prov_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString()));
                            //Consulta para saber si este proveedor ya existe

                            Dt_Usos_Multiples = Act_Prov_Negocio.Consultar_Proveedor();
                            if (Dt_Usos_Multiples != null && Dt_Usos_Multiples.Rows.Count > 0) //Si existe el proveedor
                            {
                                Fila["PROVEEDOR_ID"] = String.Format("{0:0000000000}", Convert.ToInt32(Registro["NO_PADRON_PROVEEDOR"].ToString())); ;
                                if (Validacion == true)
                                {
                                    if (!String.IsNullOrEmpty(Registro["FECHA_ACTUALIZACION"].ToString()))
                                    {
                                        if (!String.IsNullOrEmpty(Dt_Usos_Multiples.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Actualizacion].ToString()))
                                        {
                                            DateTime FechaActualizacion_Anterior;
                                            DateTime FechaActualizacion_Nueva;
                                            FechaActualizacion_Anterior = Convert.ToDateTime(Dt_Usos_Multiples.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Actualizacion].ToString());
                                            FechaActualizacion_Nueva = DateTime.ParseExact(Registro["FECHA_ACTUALIZACION"].ToString(), Formatos, CultureInfo.CurrentCulture, DateTimeStyles.AllowWhiteSpaces);

                                            //FechaActualizacion_Nueva = Convert.ToDateTime(String.Format("dd") Registro["FECHA_ACTUALIZACION"].ToString());
                                            if (FechaActualizacion_Anterior >= FechaActualizacion_Nueva)
                                            {
                                                Fila["FECHA_ACTUALIZACION"] = String.Empty;
                                            }
                                            else
                                            {
                                                Fila["FECHA_ACTUALIZACION"] = Registro["FECHA_ACTUALIZACION"].ToString();
                                            }
                                        }
                                        else
                                        {
                                            Fila["FECHA_ACTUALIZACION"] = Registro["FECHA_ACTUALIZACION"].ToString();
                                        }

                                    }
                                    Dt_Actualizar.Rows.Add(Fila); //Guardar la fila en la tabla actualizar
                                }//Fin del if Validacion = true
                            }//FIn del if dt_usoMultples != null
                            else //No exsiste el proveedor
                            {
                                Fila["PROVEEDOR_ID"] = Act_Prov_Negocio.P_Proveedor_ID;
                                if (Validacion == true)
                                {
                                    Dt_Insertar.Rows.Add(Fila); //Guardar la fila en la tabla Insertar
                                }
                            }
                        }//Fin del if que valida que el padron sea un numero
                        else 
                        {
                            Validacion = false;
                            Lbl_Mensaje_Error.Text += Lbl_Mensaje_Error.Text += "&nbsp; - El campo NO_PADRON_PROVEEDOR del registro " + Contador + " debe ser un número <br>";
                        }
                    }//Fin del if Validacion ==  true
                    else { break; }
                }//Fin del for each
                if (Validacion == true) 
                {
                    Session["Dt_Insertar"] = (DataTable)Dt_Insertar;
                    Session["Dt_Actualizar"] = (DataTable)Dt_Actualizar;
                    Session["Dt_Partidas"] = (DataTable)Dt_Partidas;
                    Session["Dt_Conceptos"] = (DataTable)Dt_Conceptos;
                }
            }
            return Validacion;

        }
        catch (FormatException e)
        {
             Validacion = false;
             Lbl_Mensaje_Error.Text += Lbl_Mensaje_Error.Text += "&nbsp; - El campo NO_PADRON_PROVEEDOR del registro " + Contador + " debe ser un número <br>";
             throw new Exception("Error al tratar de convertir el NO_PADRON_PROVEEDOR a número  Error[" + e.Message + "]");
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Interpretar_Excel
    /// DESCRIPCION : Interpreta los datos contenidos por el archivo de Excel.
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 13/Octubre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Interpretar_Excel()
    {
        OleDbConnection Conexion = new OleDbConnection();
        OleDbCommand Comando = new OleDbCommand();
        OleDbDataAdapter Adaptador = new OleDbDataAdapter();
        DataSet Ds_Informacion = new DataSet();
        String Query = String.Empty;
        DataSet Ds_Consulta = new DataSet();//Definimos el DataSet donde insertaremos los datos que leemos del excel
        String Tabla = "";
        String Rta = "";
        DataTable Dt_Proveedores = null;    //Almacena los detalles de la poliza
        String Nombre_Archivo = String.Empty; //variable para el nombre del archivo

        //Colocar el nombre del archivo
        Nombre_Archivo = HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim();

        int Caracter_Inicio = 0;    //Posicion del caracter de inicio del nombre del archivo.

        for (int Cont_Caracter = 0; Cont_Caracter < Nombre_Archivo.Length; Cont_Caracter++)
        {
            if (Nombre_Archivo[Cont_Caracter] == Convert.ToChar(92))
            {
                Caracter_Inicio = Cont_Caracter + 1;
            }
        }

        Rta = "C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio);

        try
        {
            if (Rta.Contains(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
            {
                Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";//Almacena si el archivo es 2007
            }
            else if (Rta.Contains(".xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
            {
                Conexion.ConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                        "Data Source=" + Rta + ";" +
                        "Extended Properties=Excel 8.0;"; //Almacena si el archivo es 2003
            }
            // Get the name of the first worksheet:
            Conexion.Open();
            DataTable dbSchema = Conexion.GetOleDbSchemaTable(OleDbSchemaGuid.Tables, null);
            if (dbSchema == null || dbSchema.Rows.Count < 1)
            {
                throw new Exception("Error: Could not determine the name of the first worksheet.");
            }
            String FirstSheetName = dbSchema.Rows[0]["TABLE_NAME"].ToString(); //Obtiene el nombre de la primera Hoja

            Tabla = "SELECT * FROM [" + FirstSheetName + "]";
            OleDbCommand cmd;   // Almacenara el comando a ejecutar.
            cmd = new OleDbCommand(Tabla, Conexion);    // Crea el nuevo adaptador Ole
            Adaptador.SelectCommand = cmd;
            Adaptador.Fill(Ds_Informacion); //Obtiene la informacion en un dataset
            Conexion.Close();

            Dt_Proveedores = Ds_Informacion.Tables[0];    // Liga los datos con el DataTable

            if (Validar_Columnas_Excel(Dt_Proveedores))
            {
                Convertir_Datos_Proveedores(Dt_Proveedores); //Convierte los datos extraidos para que sean visualizados de manera correcta.
            }
            else
            {
                Mostrar_Ocultar_Error(true);
            }
            if (System.IO.File.Exists("C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio)))
            {
                System.IO.File.Delete("C:\\Archivos\\" + Nombre_Archivo.Substring(Caracter_Inicio, Nombre_Archivo.Length - Caracter_Inicio));
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error  Btn_Cargar_Click: " + ex.Message.ToString(), ex);
        }
        finally
        {
            //oledbConn.Close();  // Close connection
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Convertir_Datos_Proveedores
    ///DESCRIPCIÓN          : Metodo que Actualiza o inserta los proveedores segun corresponda
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 12:03 p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    ///*******************************************************************************
    private void Convertir_Datos_Proveedores(DataTable Dt_Proveedores_Detalles)
    {  
        
        Session["Dt_Proveedores"]=(DataTable)Dt_Proveedores_Detalles; //Guarda los datos en la varible de sesion
        DataTable Dt_Actualizar = new DataTable();
        DataTable Dt_Insertar = new DataTable();
        DataTable Dt_Partidas = new DataTable();

        try
        {
            Mostrar_Ocultar_Error(false);
            if (Dt_Proveedores_Detalles.Rows.Count > 0)
            {
                if(Validar_Datos_Obligatorios())
                {
                    Crear_Dt_Proveedores();
                    if (Agregar_Filas_Dt_Proveedores()) 
                    {
                        Cargar_Grid_Proveedores(0);
                    }
                    else
                    {
                        Limpiar_Varibles_Sesion_Grid();
                       Mostrar_Ocultar_Error(true);
                    }
                }
                else
                {
                    Limpiar_Varibles_Sesion_Grid();
                    Mostrar_Ocultar_Error(true);
                }

            }
            else
            {
                Limpiar_Varibles_Sesion_Grid();
                Lbl_Mensaje_Error.Text += "&nbsp; - El arcivo de excel no contiene informacion <br />";
                Mostrar_Ocultar_Error(true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Convertir_Datos_Proveedores " + ex.Message.ToString(), ex);
        }
    }
       ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Actualizar_Proveedores
    ///DESCRIPCIÓN          : Metodo que da de alta y actualiza los proveedores leidos del archivo de excel
    ///PROPIEDADES          : 
    ///CREO                 : Jennyfer Ivonne Ceja lemus
    ///FECHA_CREO           : 10/Septiembre/2012 1:35 p pm
    ///MODIFICO             : 
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...: 
    ///*********************************************************************************************************
    ///*******************************************************************************
    private void Actualizar_Proveedores()
    {
        Cls_Cat_Com_Actualizar_Proveedores_Negocio Negocio = new Cls_Cat_Com_Actualizar_Proveedores_Negocio();
        DataTable Dt_Insertar = new DataTable();
        Dt_Insertar = (DataTable)Session["Dt_Insertar"];//Tabla que contendra los registros que se van a Insertar

        DataTable Dt_Actualizar = new DataTable();
        Dt_Actualizar = (DataTable)Session["Dt_Actualizar"];//Tabla que contendra los registros que se van a acutliazar

        DataTable Dt_Partidas = new DataTable();
        Dt_Partidas = (DataTable)Session["Dt_Partidas"];//Tabla que contendra los registros de los detalles de las partidas 

        DataTable Dt_Conceptos = new DataTable();
        Dt_Conceptos = (DataTable)Session["Dt_Conceptos"];//
        String Mensaje = String.Empty;
        try
        {
            if ((Dt_Insertar != null && Dt_Insertar.Rows.Count > 0) || (Dt_Actualizar != null && Dt_Actualizar.Rows.Count > 0))
            {
                Negocio.P_Dt_Acualizar = Dt_Actualizar;
                Negocio.P_Dt_Insertar = Dt_Insertar;
                Negocio.P_Dt_Partidas = Dt_Partidas;
                Negocio.P_Dt_Conceptos = Dt_Conceptos;
                Mensaje= Negocio.Alta_Proveedores();
                ToolkitScriptManager.RegisterStartupScript(this, this.GetType(), "Actualización Proveedores", "alert('" + Mensaje + "');", true);
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Convertir_Datos_Proveedores " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region EVENTOS
   
    protected void Btn_Cargar_Proveedores_Grid_Click(object sender, EventArgs e)
    {
        try
        {
            if (!String.IsNullOrEmpty(HttpContext.Current.Session["Nombre_Archivo_Excel"].ToString().Trim()))
                Interpretar_Excel();    //Interpreta el archivo de Excel cargado.

            else
            {
                Lbl_Mensaje_Error.Text += "&nbsp; - Ingrese el nombre del objeto que se cargara. <br />";
                Mostrar_Ocultar_Error(true);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text += "&nbsp; - " + Ex.Message.ToString() + " <br />";
            Mostrar_Ocultar_Error(true);
        }
    }
    protected void Btn_Actualizar_Proveedores_Click(object sender, EventArgs e)
    {
        try
        {
            Actualizar_Proveedores();
        }
        catch (Exception Ex)
        {
             Lbl_Mensaje_Error.Text += "&nbsp; - " + Ex.Message.ToString() + " <br />";
             Mostrar_Ocultar_Error(true);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: AFU_Archivo_Excel_UploadedComplete
    /// DESCRIPCION : CUANDO LA CARGA DEL ARCHIVO TERMINA LO GUARDA EN LA UBICACION PRESTABLECIDA
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
    {
        //****************************************
        //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
        //GUARDA EN LA UBICACION PRESTABLECIDA
        //****************************************
        try
        {
            Limpiar_Varibles_Sesion_Grid();
            Mostrar_Ocultar_Error(false);
            if (AFU_Archivo_Excel.HasFile)
            {
                
                string currentDateTime = String.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
                string fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo_Excel.FileName).Replace(" ", "_");

                bool xls = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xls");
                bool xlsx = Path.GetExtension(AFU_Archivo_Excel.PostedFile.FileName).Contains(".xlsx");

                if (xls || xlsx)
                {
                    AFU_Archivo_Excel.SaveAs(@"C:/Archivos/" + fileNameOnServer);

                    //Colocar nombre en variable de sesion
                    HttpContext.Current.Session["Nombre_Archivo_Excel"] = fileNameOnServer;
                }
                else
                    return;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Text += "&nbsp; - " + ex.Message.ToString() + " <br />";
            Mostrar_Ocultar_Error(true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
    ///DESCRIPCIÓN          : Evento del boton Salir 
    ///PARAMETROS           :    
    ///CREO                 : Luis Daniel Guzmán Malagón
    ///FECHA_CREO           : 13/Julio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Limpiar_Varibles_Sesion_Grid();
        Mostrar_Ocultar_Error(false);
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                
    }

    protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        // onpageindexchanging="Grid_Proveedores_PageIndexChanging" 
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Cotizadores_PageIndexChanging
    ///DESCRIPCIÓN: Metodo para manejar la paginacion del Grid_Cotizadores
    ///PARAMETROS:   
    ///CREO: Jacqueline Ramìrez Sierra
    ///FECHA_CREO: 16/Febrero/2011  
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Proveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Proveedores.SelectedIndex = (-1);
            Cargar_Grid_Proveedores(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text += " Error en la Paginacion [" + Ex.Message + "]";
            Mostrar_Ocultar_Error(true);
        }
    }
    #endregion

}
