﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Sessiones;
using JAPAMI.Ope_Com_Entrada_Facturas_Servicios_Generales.Negocio;
using System.Data;

public partial class paginas_Compras_Frm_Ope_Com_Entrada_Facturas_Servicios_Generales : System.Web.UI.Page
{
    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN         : Page_Load
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Page_Load(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        if (!IsPostBack)
        {
            Grid_Ordenes_Compra.PageIndex = 0;
            Llenar_Grid_Ordenes_Servicio();
            Llenar_Combo_Proveedores();
            Configuracion_Formulario(true);
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Facturas
    ///DESCRIPCIÓN         : Metodo para llenar el Grid de Facturas
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Llenar_Grid_Facturas()
    {
        DataTable Dt_Datos = new DataTable();
        if (Session["DT_FACTURAS_REGISTRADAS"] != null) Dt_Datos = (DataTable)Session["DT_FACTURAS_REGISTRADAS"];
        Grid_Facturas.DataSource = Dt_Datos;
        Grid_Facturas.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Ordenes_Servicio
    ///DESCRIPCIÓN         : Metodo para llenar el Grid Inicial
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Llenar_Grid_Ordenes_Servicio()
    {
        Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Cls_Negocio = new Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio();
        Cls_Negocio.P_Estatus = "PROVEEDOR_ENTERADO";
        if (Cmb_Proveedores.SelectedIndex > 0) Cls_Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedItem.Value;
        if (!String.IsNullOrEmpty(Txt_Fecha_Inicio.Text)) Cls_Negocio.P_Fecha_Inicio = Convert.ToDateTime(Txt_Fecha_Inicio.Text);
        if (!String.IsNullOrEmpty(Txt_Fecha_Fin.Text)) Cls_Negocio.P_Fecha_Final = Convert.ToDateTime(Txt_Fecha_Fin.Text);
        if (!String.IsNullOrEmpty(Txt_Busqueda_Orden_Compra.Text)) Cls_Negocio.P_No_Orden_Compra = Txt_Busqueda_Orden_Compra.Text.Trim();
        DataTable Dt_Datos = Cls_Negocio.Consultar_Ordenes_Servicio();
        Grid_Ordenes_Compra.SelectedIndex = -1;
        Grid_Ordenes_Compra.Columns[1].Visible = true;
        Grid_Ordenes_Compra.DataSource = Dt_Datos;
        Grid_Ordenes_Compra.DataBind();
        Grid_Ordenes_Compra.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Proveedores
    ///DESCRIPCIÓN         : Metodo para llenar el Combo de Proveedores
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Llenar_Combo_Proveedores()
    {
        Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Cls_Negocio = new Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio();
        DataTable Dt_Datos = Cls_Negocio.Consultar_Proveedores();
        Cmb_Proveedores.DataSource = Dt_Datos;
        Cmb_Proveedores.DataValueField = "PROVEEDOR_ID";
        Cmb_Proveedores.DataTextField = "NOMBRE_PROVEEDOR";
        Cmb_Proveedores.DataBind();
        Cmb_Proveedores.Items.Insert(0, new ListItem("<-- TODOS -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Detalles_Orden
    ///DESCRIPCIÓN         : Metodo para llenar los campos con la Orden Seleccionada
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Cargar_Detalles_Orden()
    {
        Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Cls_Negocio = new Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio();
        Cls_Negocio.P_No_Orden_Compra = Hdf_No_Orden_Servicio.Value.Trim();
        DataTable Dt_Datos = Cls_Negocio.Consultar_Detalles_Orden_Servicio();
        if (Dt_Datos != null)
        {
            if (Dt_Datos.Rows.Count > 0)
            {
                Hdf_No_Orden_Servicio.Value = Dt_Datos.Rows[0]["NO_ORDEN_COMPRA"].ToString().Trim();
                Txt_Folio.Text = Dt_Datos.Rows[0]["FOLIO"].ToString().Trim();
                Txt_Fecha_Construccion.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Dt_Datos.Rows[0]["FECHA_CREO"]));
                Txt_Requisicion.Text = Dt_Datos.Rows[0]["FOLIO_RQ"].ToString().Trim();
                Txt_Elaboro.Text = Dt_Datos.Rows[0]["USUARIO_CREO"].ToString().Trim();
                Txt_Dependencia.Text = Dt_Datos.Rows[0]["DEPENDENCIA"].ToString().Trim();
                Txt_Proveedor.Text = Dt_Datos.Rows[0]["PROVEEDOR"].ToString().Trim();
                Txt_Justificacion.Text = Dt_Datos.Rows[0]["JUSTIFICACION"].ToString().Trim();
                Txt_Total_Cotizado.Text = String.Format("{0:c}", Convert.ToDouble(Dt_Datos.Rows[0]["TOTAL"]));
                Txt_Importe_Factura.Text = String.Format("{0:#########0.00}", Convert.ToDouble(Dt_Datos.Rows[0]["SUBTOTAL"]));
                Txt_IVA_Factura.Text = String.Format("{0:#########0.00}", Convert.ToDouble(Dt_Datos.Rows[0]["IVA"]));
                Txt_Total_Factura.Text = String.Format("{0:#########0.00}", Convert.ToDouble(Dt_Datos.Rows[0]["TOTAL"]));
                Txt_Fecha_Factura.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Detalles_Orden
    ///DESCRIPCIÓN         : Metodo para Limpiar los campos con la Orden
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Limpiar_Detalles_Orden()
    {
        Hdf_No_Orden_Servicio.Value = "";
        Txt_Folio.Text = "";
        Txt_Fecha_Construccion.Text = "";
        Txt_Requisicion.Text = "";
        Txt_Elaboro.Text = "";
        Txt_Dependencia.Text = "";
        Txt_Proveedor.Text = "";
        Txt_Justificacion.Text = "";
        Session.Remove("DT_FACTURAS_REGISTRADAS");
        Llenar_Grid_Facturas();
        Limpiar_Datos_Facturas();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Datos_Facturas
    ///DESCRIPCIÓN         : Metodo para Limpiar los campos con la Factura
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Limpiar_Datos_Facturas() {
        Txt_No_Factura.Text = "";
        Txt_Fecha_Factura.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
        Txt_Importe_Factura.Text = "";
        Txt_IVA_Factura.Text = "";
        Txt_Total_Factura.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN         : Metodo para Habilitar los campos con la Orden
    ///PROPIEDADES         : Estado_Listado. Estado para habilitar o no los campos.
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Configuracion_Formulario(Boolean Estado_Listado) {
        Div_Busqueda.Visible = Estado_Listado;
        Div_Orden_Compra.Visible = !Estado_Listado;
        Btn_Guardar_Facturas.Visible = !Estado_Listado;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Alta_Facturas
    ///DESCRIPCIÓN         : Da de Alta las Facturas Registradas
    ///PROPIEDADES         : 
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Alta_Facturas() {
        Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio Cls_Negocio = new Cls_Ope_Com_Entrada_Facturas_Servicios_Generales_Negocio();
        Cls_Negocio.P_Dt_Datos = (DataTable)Session["DT_FACTURAS_REGISTRADAS"];
        Cls_Negocio.P_No_Orden_Compra = Hdf_No_Orden_Servicio.Value.Trim();
        Cls_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
        Cls_Negocio.Alta_Facturas();
    }
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Estructura_Facturas
    ///DESCRIPCIÓN         : Crea la Estructura de la Tabla de Facturas
    ///PROPIEDADES         : 
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private void Crear_Estructura_Facturas(ref DataTable Dt_Datos) {
        Dt_Datos = new DataTable();
        Dt_Datos.Columns.Add("NUMERO_FACTURA", Type.GetType("System.String"));
        Dt_Datos.Columns.Add("IMPORTE_FACTURA", Type.GetType("System.Double"));
        Dt_Datos.Columns.Add("FECHA_FACTURA", Type.GetType("System.DateTime"));
        Dt_Datos.Columns.Add("IVA_FACTURA", Type.GetType("System.Double"));
        Dt_Datos.Columns.Add("TOTAL_FACTURA", Type.GetType("System.Double"));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Agregar_Factura
    ///DESCRIPCIÓN         : Valida el agregar la Factura
    ///PROPIEDADES         : 
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    private Boolean Validar_Agregar_Factura() {
        Boolean Pasa_Validacion = true;
        String Mensaje = "";
        if (String.IsNullOrEmpty(Txt_No_Factura.Text))
        {
            Mensaje += "- No. de Factura. <br />";
        }
        if (String.IsNullOrEmpty(Txt_Fecha_Factura.Text))
        {
            Mensaje += "- Fecha de Factura. <br />";
        }
        if (String.IsNullOrEmpty(Txt_Importe_Factura.Text))
        {
            Mensaje += "- Importe. <br />";
        }
        if (String.IsNullOrEmpty(Txt_IVA_Factura.Text))
        {
            Mensaje += "- IVA. <br />";
        }
        if (Convert.ToDouble(Txt_Total_Factura.Text) == 0)
        {
            Mensaje += "- El Monto de la Factura debe ser mayor a 0 [Cero]. <br />";
        }
        if (!String.IsNullOrEmpty(Mensaje)) {
            Pasa_Validacion = false;
            Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir.";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Pasa_Validacion;
    }

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_PageIndexChanging
    ///DESCRIPCIÓN         : Paginacion del Grid Inicial
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Grid_Ordenes_Compra_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        Grid_Ordenes_Compra.PageIndex = e.NewPageIndex;
        Llenar_Grid_Ordenes_Servicio();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Ordenes_Compra_PageIndexChanging
    ///DESCRIPCIÓN         : Paginacion del Grid Inicial
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Grid_Ordenes_Compra_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Limpiar_Detalles_Orden();
            Hdf_No_Orden_Servicio.Value = HttpUtility.HtmlDecode(Grid_Ordenes_Compra.SelectedRow.Cells[1].Text.Trim());
            Cargar_Detalles_Orden();
            Grid_Ordenes_Compra.SelectedIndex = -1;
            Configuracion_Formulario(false);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Facturas_RowDataBound
    ///DESCRIPCIÓN         : DataBound del Grid 
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Grid_Facturas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Int32 No_Fila = e.Row.RowIndex;
            if (e.Row.FindControl("Btn_Quitar_Factura") != null)
            {
                ImageButton Btn_Quitar_Factura = (ImageButton)e.Row.FindControl("Btn_Quitar_Factura");
                Btn_Quitar_Factura.CommandArgument = No_Fila.ToString();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion Grids

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN         : Ejecuta la Busqueda
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 21/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Grid_Ordenes_Compra.PageIndex = 0;
        Llenar_Grid_Ordenes_Servicio();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN         : Botón para salir del formulario y cancelar operacion.
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
        if (Div_Busqueda.Visible) Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        else {
            Limpiar_Detalles_Orden();
            Configuracion_Formulario(true);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Guardar_Facturas_Click
    ///DESCRIPCIÓN         : Botón para Guardar las Facturas
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Btn_Guardar_Facturas_Click(object sender, ImageClickEventArgs e)
    {
        try 
        {
            if (Session["DT_FACTURAS_REGISTRADAS"] != null)
            {
                DataTable Dt_Facturas = (DataTable)Session["DT_FACTURAS_REGISTRADAS"];
                if (Dt_Facturas.Rows.Count > 0) {
                    Alta_Facturas();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Se guardarón las facturas exitosamente');", true);
                    Llenar_Combo_Proveedores();
                    Grid_Ordenes_Compra.PageIndex = 0;
                    Llenar_Grid_Ordenes_Servicio();
                    Configuracion_Formulario(true);
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Se requiere ingresar al menos una factura.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se requiere ingresar al menos una factura.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Factura_Click
    ///DESCRIPCIÓN         : Botón para Agregar las Facturas
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Btn_Agregar_Factura_Click(object sender, EventArgs e)
    {
        try
        {
            if (Validar_Agregar_Factura())
            {
                DataTable Dt_Datos = new DataTable();
                if (Session["DT_FACTURAS_REGISTRADAS"] != null)
                {
                    if (((DataTable)Session["DT_FACTURAS_REGISTRADAS"]).Rows.Count > 0)
                    {
                        Dt_Datos = (DataTable)Session["DT_FACTURAS_REGISTRADAS"];
                    }
                    else
                    {
                        Crear_Estructura_Facturas(ref Dt_Datos);
                    }
                }
                else
                {
                    Crear_Estructura_Facturas(ref Dt_Datos);
                }
                Double Monto_Validar = Convert.ToDouble(((Dt_Datos.Compute("SUM(TOTAL_FACTURA)", "TOTAL_FACTURA<>0")) is DBNull) ? 0 : Dt_Datos.Compute("SUM(TOTAL_FACTURA)", "TOTAL_FACTURA<>0"));
                Double Tope_Validar = Convert.ToDouble(Txt_Total_Cotizado.Text.Trim().Replace("$", "").Replace(",", ""));
                Monto_Validar += Convert.ToDouble(Txt_Total_Factura.Text);
                if (Monto_Validar <= Tope_Validar)
                {
                    DataRow Fila_Nueva = Dt_Datos.NewRow();
                    Fila_Nueva["NUMERO_FACTURA"] = Txt_No_Factura.Text.Trim();
                    Fila_Nueva["FECHA_FACTURA"] = Convert.ToDateTime(Txt_Fecha_Factura.Text.Trim());
                    Fila_Nueva["IMPORTE_FACTURA"] = Convert.ToDouble(Txt_Importe_Factura.Text.Trim());
                    Fila_Nueva["IVA_FACTURA"] = Convert.ToDouble(Txt_IVA_Factura.Text.Trim());
                    Fila_Nueva["TOTAL_FACTURA"] = Convert.ToDouble(Txt_Total_Factura.Text.Trim());
                    Dt_Datos.Rows.Add(Fila_Nueva);
                    Session["DT_FACTURAS_REGISTRADAS"] = Dt_Datos;
                    Llenar_Grid_Facturas();
                    Limpiar_Datos_Facturas();
                } else {
                    throw new Exception("El monto excede el total cotizado.");
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Factura_Click
    ///DESCRIPCIÓN         : Botón para quitar las Facturas
    ///PROPIEDADES         :
    ///CREO                : Francisco Antonio Gallardo Catañeda
    ///FECHA_CREO          : 22/Agosto/2013
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///******************************************************************************* 
    protected void Btn_Quitar_Factura_Click(object sender, EventArgs e)
    {
        try
        {
            ImageButton Btn_Quitar_Anexo = (ImageButton)sender;
            if (!String.IsNullOrEmpty(Btn_Quitar_Anexo.CommandArgument))
            {
                Int32 No_Registro = Convert.ToInt32(Btn_Quitar_Anexo.CommandArgument);
                if (Session["DT_FACTURAS_REGISTRADAS"] != null) {
                    DataTable Dt_Datos = (DataTable)Session["DT_FACTURAS_REGISTRADAS"];
                    Dt_Datos.Rows.RemoveAt(No_Registro);
                    Session["DT_FACTURAS_REGISTRADAS"] = Dt_Datos;
                }
                Llenar_Grid_Facturas();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion Eventos

}