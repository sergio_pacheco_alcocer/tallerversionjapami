﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Ordenes_Compra.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Ordenes_Compra" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        /*

        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }*/

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                  <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="6" class="label_titulo">Reporte Ordenes Compra</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>          
                    <tr class="barra_busqueda">
                        <td colspan="6" style="width:20%;">
                            <!--Bloque de la busqueda-->
                            <asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" 
                                onclick="Btn_Consultar_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="NUEVO" ToolTip="Exportar PDF" 
                                onclick="Btn_Imprimir_Click" />  
                            <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Imprimir Excel" ToolTip="Exportar Excel" 
                                onclick="Btn_Imprimir_Excel_Click" />  
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click" />
                        </td>                                 
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Tipo B&uacute;squeda</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Busqueda" runat="server" Width="150px" 
                                AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Tipo_Busqueda_SelectedIndexChanged">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Producto" Value="Producto"></asp:ListItem>                               
                                <asp:ListItem Text="Proveedor" Value="Proveedor"></asp:ListItem>                                
                            </asp:DropDownList>                        
                        </td>
                        <td align="left">Fecha Inicial</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="130px" Enabled="false"></asp:TextBox>
                            <cc1:filteredtextboxextender id="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server"
                                targetcontrolid="Txt_Fecha_Inicial" filtertype="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                validchars="/_" />
                            <cc1:calendarextender id="Txt_Fecha_Inicial_CalendarExtender" runat="server" targetcontrolid="Txt_Fecha_Inicial"
                                popupbuttonid="Btn_Fecha_Inicial" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                        </td>
                        <td align="left">Fecha Final</td>
                        <td align="left">                                
                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="130px" Enabled="false"></asp:TextBox>
                            <cc1:calendarextender id="CalendarExtender3" runat="server" targetcontrolid="Txt_Fecha_Final"
                                popupbuttonid="Btn_Fecha_Final" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Tipo Consulta</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipo_Reporte" runat="server" Width="150px" 
                                AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Tipo_Reporte_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td align="left"><asp:Label ID="Lbl_Tipo_Busqueda" runat="server"></asp:Label></td>
                        <td align="left"><asp:TextBox ID="Txt_Busqueda" runat="server"></asp:TextBox></td>
                        <td align="left"><asp:ImageButton ID="Btn_Busqueda_Proveedor" runat="server" 
                                CssClass="Img_Button" ToolTip="Buscar Proveedor" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                onclick="Btn_Busqueda_Proveedor_Click" /></td>
                        <td>&nbsp;</td>
                    </tr>
                    <tr runat="server" id="Row_Proveedor">
                        <td align="left"><asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor"></asp:Label></td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Unidad</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Unidades" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left">Impuesto</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Impuestos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td colspan="2">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Partida Gen&eacute;rica</td>
                        <td align="left" colspan="5">
                            <asp:DropDownList ID="Cmb_Partidas_Genericas" runat="server" Width="100%" 
                                AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Partidas_Genericas_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Partida Espec&iacute;fica</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Partidas_Especificas" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Tipo</td>
                        <td align="left"><asp:DropDownList ID="Cmb_Tipos" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left">Estatus</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                <asp:ListItem Text="Todos" Value=""></asp:ListItem>
                                <asp:ListItem Text="Cancelada" Value="CANCELADA"></asp:ListItem>
                                <asp:ListItem Text="Completa" Value="COMPLETA"></asp:ListItem>
                                <asp:ListItem Text="Elaborada" Value="ELABORADA"></asp:ListItem>
                                <asp:ListItem Text="Generada" Value="GENERADA"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left">Stock</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Stock" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Sin valor" Value="NULO"></asp:ListItem>
                                <asp:ListItem Text="Si" Value="SI"></asp:ListItem>
                                <asp:ListItem Text="No" Value="NO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="6">
                            <asp:GridView ID="Grid_Ordenes_Compra" runat="server" style="white-space:normal;" 
                                AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" 
                                GridLines="None" PageSize="5" Width="100%" AllowPaging="true">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="Folio" HeaderText="Folio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Nombre_Proveedor" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Clave" HeaderText="Clave" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Producto" HeaderText="Producto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                    <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Tipo_Articulo" HeaderText="Tipo Art&iacute;culo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Subtotal" HeaderText="Subtotal" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" />
                                    <asp:BoundField DataField="Total_IEPS" HeaderText="IEPS" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" />
                                    <asp:BoundField DataField="Total_IVA" HeaderText="IVA" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" />
                                    <asp:BoundField DataField="Total" HeaderText="Total" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" />
                                    <asp:BoundField DataField="No_Orden_Compra" HeaderText="OC" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

