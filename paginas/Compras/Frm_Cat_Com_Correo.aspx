﻿<%@ Page Title="Catálogo Cotizadores" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Com_Correo.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Com_Correo" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" AsyncPostBackTimeout="360000" />
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
    
        <ContentTemplate>
        
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        
                <div id="Div_Cat_Com_Cotizadores_Botones" style="background-color:#ffffff; width:100%; height:100%;">                
                        <table id="Tbl_Comandos" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                        
                            <tr>
                                <td colspan="2" class="label_titulo">
                                    Cat&aacute;logo de Correo
                                </td>
                            </tr>
                            
                            <tr>
                                <div id="Div_Contenedor_error" runat="server">
                                <td colspan="2">
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                                    <br />
                                    <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                                </td>
                                </div>
                            </tr>
                            
                            <tr class="barra_busqueda">
                                <td style="width:50%">
                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                    CssClass="Img_Button" onclick="Btn_Nuevo_Click"/>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" onclick="Btn_Modificar_Click"/>
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    CssClass="Img_Button" onclick="Btn_Salir_Click"/>
                                </td>
                                <td align="right" style="width:50%">                                    
                                </td>                        
                            </tr>
                            
                        </table>
                    </div>

            <div id="Div_Datos_Cotizador" runat="server">
                 
                <div id="Div_Cat_Com_Cotizadores_Controles" runat="server" style="background-color:#ffffff; width:100%; height:100%;">        
                       <table id="Datos Generales_Inner" border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">                            
                            <tr>
                                <td style="width:18%">
                                    
                                    <asp:Label ID="Lbl_Correo" runat="server" Text="Correo"></asp:Label>
                                    
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Correo" runat="server" Width="49%"></asp:TextBox>
                                </td>

                            </tr>
                            <tr>
                                <td style="width:18%">
                                    
                                    <asp:Label ID="Lbl_Password" runat="server" Text="Password"></asp:Label>
                                    
                                </td>
                                 <td>
                                    <asp:TextBox ID="Txt_Password" runat="server" Width="49%" TextMode="Password"></asp:TextBox>
                                </td>
                               
                            </tr>
                            <tr>
                                <td style="width:18%">
                                    <asp:Label ID="Lbl_Direccion_IP" runat="server" Text="Direccion IP Correo"></asp:Label>
                                </td>
                                
                                 <td>
                                    <asp:TextBox ID="Txt_Direccion_IP" runat="server" Width="49%" ></asp:TextBox>
                                      <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" 
                                      TargetControlID="Txt_Direccion_IP" ValidChars="1234567890.">
                                      </cc1:FilteredTextBoxExtender>
                                </td>
                               
                               
                            </tr>
                           
                            </table>
                            
                    </div>
            </div>
            <div id="Div_Listado_Cotizadores" runat="server" style="background-color:#ffffff; width:100%; height:100%;">
                    <table id="Tbl_Grid_Cotizadores" border="0" cellspacing="0" class="estilo_fuente" style="width:98%;">                        
                            <tr>
                                <td align="center">
                                &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td align="center">
                                    <asp:GridView ID="Grid_Correo" runat="server" AllowPaging="True" 
                                        AutoGenerateColumns="False"  CssClass="GridView_1" 
                                        EmptyDataText="&quot;No se encontraron registros&quot;" GridLines="None"  
                                        Width="96%" Style="white-space:normal" onpageindexchanging="Grid_Correo_PageIndexChanging"
                                        onselectedindexchanged="Grid_Correo_SelectedIndexChanged"
                                        
                                        >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="5%" />
                                            </asp:ButtonField>
                                                <asp:BoundField DataField="USUARIO_CORREO" HeaderText="Nombre" 
                                                Visible="False">
                                                    <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="0%" ForeColor="Transparent" Font-Size="0px"/>
                                                </asp:BoundField>
                                                <asp:BoundField DataField="CORREO_SALIENTE" HeaderText="Correo" >
                                                    <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="PASSWORD_CORREO" HeaderText="Password" 
                                                Visible="False">
                                                    <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SERVIDOR_CORREO" 
                                                HeaderText="Direccion IP Correo">
                                                    <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                    <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                </asp:BoundField>
                                                
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </td>
                            </tr>                        
                    </table>
            </div>
         </ContentTemplate> 
                 
    </asp:UpdatePanel>
          

</asp:Content>