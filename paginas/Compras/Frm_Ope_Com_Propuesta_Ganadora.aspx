﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    CodeFile="Frm_Ope_Com_Propuesta_Ganadora.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Propuesta_Ganadora" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
 <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <%--Div de Contenido --%>
            <div id="Div_Contenido" style="width: 97%; height: 100%;">
                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Cotizacion Ganadora
                        </td>
                    </tr>
                    <%--Fila de div de Mensaje de Error --%>
                    <tr>
                        <td colspan="3">
                            <div id="Div_Contenedor_Msj_Error" style="width: 95%; font-size: 9px;" runat="server"
                                visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td align="left" style="font-size: 12px; color: Red; font-family: Tahoma; text-align: left;">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                        </td>
                                        <td style="font-size: 9px; width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <%--Fila de Busqueda y Botones Generales --%>
                    <tr class="barra_busqueda">
                        <td style="width: 20%;" colspan="4">
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Conditional">
                                <ContentTemplate>
                                    <asp:ImageButton ID="Btn_Modificar" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                        ToolTip="Modificar" OnClick="Btn_Modificar_Click" />
                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button"
                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" OnClick="Btn_Salir_Click" />
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Grid_Requisiciones" runat="server" style="overflow: auto; height: 500px;
                                width: 99%; vertical-align: top; border-style: outset; border-color: Silver;">
                                <asp:TextBox ID="Txt_Requisicion_Busqueda" runat="server" Width="40%" 
                                    MaxLength="10" ontextchanged="Txt_Requisicion_Busqueda_TextChanged" AutoPostBack="true"></asp:TextBox>                                    
                                <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender12" runat="server" 
                                TargetControlID="Txt_Requisicion_Busqueda"  
                                FilterType="Custom" ValidChars="0,1,2,3,4,5,6,7,8,9"
                                Enabled="True" InvalidChars="<,>,&,',!,">   
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Ingrese un Folio>" TargetControlID="Txt_Requisicion_Busqueda" />
                                <asp:ImageButton ID="Btn_Buscar" runat="server" ToolTip="Consultar"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Click"/>
                                
                                <asp:GridView ID="Grid_Requisiciones" runat="server" AutoGenerateColumns="False"
                                    CssClass="GridView_1" GridLines="None" OnSelectedIndexChanged="Grid_Requisiciones_SelectedIndexChanged"
                                    Width="99%" Enabled="False" DataKeyNames="No_Requisicion" AllowSorting="True"
                                    OnSorting="Grid_Requisiciones_Sorting" HeaderStyle-CssClass="tblHead">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" Text="Ver Requisicion" HeaderText="Ver"
                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="No_Requisicion" HeaderText="No_Requisicion" Visible="false">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" />
                                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" Visible="true" SortExpression="Folio"
                                            ItemStyle-Wrap="true">
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="true" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Concepto" HeaderText="Concepto" Visible="True" SortExpression="Concepto"
                                            ItemStyle-Wrap="true">
                                            <FooterStyle HorizontalAlign="Left" Width="25%" Wrap="true" />
                                            <HeaderStyle HorizontalAlign="Left" Width="25%" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="25%" Wrap="true" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Tipo_Articulo" HeaderText="Tipo Articulo" Visible="True"
                                            SortExpression="Tipo_Articulo" ItemStyle-Wrap="true">
                                            <FooterStyle HorizontalAlign="Left" Width="10%" Wrap="true" />
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="true" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha" HeaderText="Fecha Solicitud" Visible="True" SortExpression="Cotizador"
                                            ItemStyle-Wrap="true">
                                            <FooterStyle HorizontalAlign="Left" Width="10%" Wrap="true" />
                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%" Wrap="true" Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Detalle_Requisicion" runat="server" style="width: 100%; font-size: 9px;"
                                visible="false">
                                <table width="99%">
                                    <tr>
                                        <td align="center" colspan="4">
                                            <asp:HiddenField ID="Hdf_Listado_Almacen" runat="server" />
                                        </td>
                                    </tr>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Folio
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Folio" runat="server" ReadOnly="true" Width="99%"></asp:TextBox>
                        </td>
                        <td style="width: 18%">
                            Fecha Generacion
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Fecha_Generacion" runat="server" ReadOnly="true" Width="99%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Unidad Responsable
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Dependencia" runat="server" ReadOnly="true" Width="99.5%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Concepto
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Concepto" runat="server" ReadOnly="true" Width="99.5%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Tipo
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Tipo" runat="server" ReadOnly="true" Width="99%"></asp:TextBox>
                        </td>
                        <td style="width: 18%">
                            Tipo Articulo
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Tipo_Articulo" runat="server" ReadOnly="true" Width="99%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Estatus
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td colspan="2">
                            <asp:CheckBox ID="Chk_Verificacion" runat="server" Enabled="false" Text="Verificar las características, garantías y pólizas" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Justificación de la Compra
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Justificacion" runat="server" ReadOnly="true" Rows="3" TabIndex="10"
                                TextMode="MultiLine" Width="99%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="Txt_Justificacion"
                                WatermarkCssClass="watermarked" WatermarkText="&lt;Indica el motivo de realizar la requisición&gt;" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%">
                            Especificaciones Adicionales
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Especificacion" runat="server" Enabled="False" Rows="3" TabIndex="10"
                                TextMode="MultiLine" Width="99%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender4" runat="server" TargetControlID="Txt_Especificacion"
                                WatermarkCssClass="watermarked" WatermarkText="&lt;Especificaciones de los productos&gt;" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="4">
                            <asp:Panel ID="Pnl_Listado_Bienes" runat="server" Width="100%" GroupingText=" Listado de Bienes para el Servicio "
                                Visible="false">
                                <asp:GridView ID="Grid_Listado_Bienes" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                    GridLines="Both" PageSize="25" Width="100%" EmptyDataText="No hay Bienes seleccionados para esta Requisición"
                                    Style="font-size: xx-small; white-space: normal">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID">
                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inv.">
                                            <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                            <ItemStyle HorizontalAlign="Center" Width="30%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_SERIE" HeaderText="Serie" Visible="True">
                                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                                <br />
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Proveedor
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="99%" OnSelectedIndexChanged="Cmb_Proveedores_SelectedIndexChanged"
                                AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_RowIndex" runat="server" />
                            <asp:GridView ID="Grid_Productos" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                                DataKeyNames="Ope_Com_Req_Producto_ID" GridLines="None" HeaderStyle-CssClass="tblHead"
                                Width="100%" OnRowDataBound="Grid_Productos_RowDataBound">
                                <Columns>
                                    <asp:BoundField DataField="Ope_Com_Req_Producto_ID" HeaderText="Ope_Com_Req_Producto_ID"
                                        Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Porcentaje_Impuesto" HeaderText="Porcentaje_Impuesto"
                                        Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Subtotal_Cotizado" HeaderText="Subtotal_Cotizado" Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IVA_Cotizado" HeaderText="IVA_Cotizado" Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IEPS_Cotizado" HeaderText="IVA_Cotizado" Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Precio_U_Con_Imp_Cotizado" HeaderText="Precio_U_Con_Imp_Cotizado"
                                        Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nombre_Prod_Serv" HeaderText="Producto/Servicio" SortExpression="Nombre_Prod_Serv"
                                        Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Descripcion" HeaderText="Descripcion" SortExpression="Descripcion"
                                        Visible="True">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Cantidad" HeaderText="Cantidad" DataFormatString="{0:0.##}"
                                        SortExpression="Cantidad" Visible="True">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Precio_U_Sin_Imp_Cotizado" DataFormatString="{0:0,0.00}"
                                        HeaderText="Precio Unitario S/I" Visible="true">
                                        <FooterStyle HorizontalAlign="Center" Width="10%" />
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Total_Cotizado" HeaderText="Precio Acumulado" DataFormatString="{0:0,0.00}"
                                        SortExpression="Monto_Total" Visible="True">
                                        <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Proveedor_ID" HeaderText="Proveedor_ID" Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Nombre_Proveedor" HeaderText="Nombre_Proveedor" Visible="false">
                                        <FooterStyle HorizontalAlign="Right" />
                                        <HeaderStyle HorizontalAlign="Right" />
                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Right" />
                                    </asp:BoundField>
                                    <asp:TemplateField HeaderText="Proveedor">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="Cmb_Proveedor" runat="server" Width="99%" OnSelectedIndexChanged="Cmb_Proveedor_SelectedIndexChanged"
                                                AutoPostBack="true">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                        <ItemStyle Width="30%" />
                                    </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                    <tr>
                        <td align="right" colspan="2">
                            <asp:Button ID="Btn_Calcular_Precios_Cotizados" runat="server" CssClass="button"
                                OnClick="Btn_Calcular_Precios_Cotizados_Click" Style="display: none" Text="Calcular Precios Cotizados"
                                Width="200px" />
                        </td>
                        <td align="right" colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="right" style="width: 50%">
                                        Subtotal Cotizado
                                    </td>
                                    <td align="right" style="width: 50%">
                                        <asp:TextBox ID="Txt_SubTotal_Cotizado_Requisicion" runat="server" ReadOnly="true"
                                            Style="text-align: right;" Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="right" style="width: 50%">
                                        IVA Cotizado
                                    </td>
                                    <td align="right" style="width: 50%">
                                        <asp:TextBox ID="Txt_IVA_Cotizado" runat="server" ReadOnly="true" Style="text-align: right;"
                                            Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="right" style="width: 50%">
                                        IEPS Cotizado
                                    </td>
                                    <td align="right" style="width: 50%">
                                        <asp:TextBox ID="Txt_IEPS_Cotizado" runat="server" ReadOnly="true" Style="text-align: right;"
                                            Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                        </td>
                        <td colspan="2">
                            <table width="100%">
                                <tr>
                                    <td align="right" style="width: 50%">
                                        Total Cotizado
                                    </td>
                                    <td align="right" style="width: 50%">
                                        <asp:TextBox ID="Txt_Total_Cotizado_Requisicion" runat="server" ReadOnly="true" Style="text-align: right;"
                                            Width="99%"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            </td> </tr> </table> </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
