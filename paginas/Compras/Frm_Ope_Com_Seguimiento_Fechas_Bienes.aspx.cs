﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Seguimiento_Requisicion.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
using JAPAMI.Administrar_Requisiciones.Negocios;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;

public partial class paginas_Compras_Frm_Ope_Com_Seguimiento_Requisiciones_Servicios_Generales : System.Web.UI.Page
{    

    #region PAGE LOAD / INIT
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado) || String.IsNullOrEmpty(Cls_Sessiones.No_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        //Valores de primera vez
        if (!IsPostBack)
        {            
        }
    }
    #endregion

   
}
