﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"  AutoEventWireup="true" CodeFile="Frm_Cat_Com_Actualizar_Proveedores.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Com_Actualizar_Proveedores" Title="Actualizar Proveedores" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
       <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
        <asp:UpdatePanel ID="Upd_Panel" runat="server">
            <ContentTemplate>
                <div id="Div_Actializar_ProveedoresS" >
                    <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">Actualizar Proveedores</td>
                        </tr>
                        <tr>
                            <td colspan="2">&nbsp;
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>
                        </tr>               
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                    TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                            </td>
                            <td style="width:50%"></td> 
                        </tr>
                    </table>
                     <table width="99.5%"  border="0" cellspacing="0" class="estilo_fuente">
                        <tr>
                            <td style="width:25%">
                                       Ruta del Archivo
                            </td>
                            <td colspan="2" align="left"  >
                                <cc1:AsyncFileUpload ID="AFU_Archivo_Excel" runat="server" Width="45%"  UploadingBackColor="LightBlue"
                                            ThrobberID="Throbber" onuploadedcomplete="AFU_Archivo_Excel_UploadedComplete" />
                            </td> 
                        </tr> 
                         <tr>
                            <td colspan="3">

                            </td>
                        </tr> 
                        <tr>
                            <td style="width:25%">
                                 <asp:Button ID="Btn_Cargar_Proveedores_Grid" runat="server"  
                                     Text="Cargar Proveedores" CssClass="button"  
                                              Width="200px" onclick="Btn_Cargar_Proveedores_Grid_Click"/> 
                            </td>
                             <td style="width:25%">
                                 <asp:Button ID="Btn_Actualizar_Proveedores" runat="server"  
                                     Text="Actualizar Proveedores" CssClass="button"  
                                              Width="200px" onclick="Btn_Actualizar_Proveedores_Click"/> 
                            </td>
                             <td style="width:25%">

                            </td>
                        </tr> 
                         
                     </table>
                     <div style="height:auto; max-height:400px; overflow:auto; width:100%; vertical-align:top;">
                          <table style="width:100%;">
                                <tr>
                            <td>
                                <asp:GridView ID="Grid_Proveedores" runat="server" AutoGenerateColumns="False" 
                                CssClass="GridView_1" AllowPaging="True" PageSize="10" 
                                GridLines="None" Width = "98%" OnPageIndexChanging = "Grid_Proveedores_PageIndexChanging"
                                onselectedindexchanged="Grid_Proveedores_SelectedIndexChanged"> 
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="NO_PADRON_PROVEEDOR" HeaderText="Padron Proveedor">
                                        <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="XX-Small" />                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_REGISTRO" HeaderText="Fecha de Registro">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />                                        
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RAZON_SOCIAL" HeaderText="Razon Social">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em" />
                                        <ItemStyle HorizontalAlign="Left" Width="20%"  Font-Size="XX-Small" />                                                                            
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE_COMERCIAL" HeaderText="Nombre Comercial">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="REPRESENTANTE_LEGAL" HeaderText="Representante Legal">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="CONTACTO" HeaderText="Contacto">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>  
                                     <asp:BoundField DataField="RFC" HeaderText="RFC">
                                        <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="12%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                        <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="12%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TIPO_FISCAL" HeaderText="Persona">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="DIRECCION" HeaderText="Direccion">
                                        <HeaderStyle HorizontalAlign="Left" Width="35%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="35%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="COLONIA" HeaderText="Colonia">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="CIUDAD" HeaderText="Ciudad">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                     <asp:BoundField DataField="ESTADO" HeaderText="Estado">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="CODIGO_POSTAL" HeaderText="CP">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="TELEFONO1" HeaderText="Telefono1">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TELEFONO2" HeaderText="Telefono2">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NEXTEL" HeaderText="Nextel">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FAX" HeaderText="Fax">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                       <asp:BoundField DataField="E_MAIL" HeaderText="Correo">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TIPO_PAGO" HeaderText="Tipo pago">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="DIAS_CREDITO" HeaderText="Dias Credito">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                     <asp:BoundField DataField="FORMA_PAGO" HeaderText="Forma de Pago">
                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>
                                    <asp:BoundField DataField="PORCENTAJE_ANTICIPO" HeaderText="%Anticipo">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>  
                                      <asp:BoundField DataField="COMENTARIOS" HeaderText="Comentarios">
                                        <HeaderStyle HorizontalAlign="Left" Width="25%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="FECHA_ACTUALIZACION" HeaderText="Fecha Actualizacion">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>   
                                     <asp:BoundField DataField="PARTIDAS" HeaderText="Partidas">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="0.7em"/>
                                        <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />                                                                                                                
                                    </asp:BoundField>                                  
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />                                
                                <AlternatingRowStyle CssClass="GridAltItem" />                                
                            </asp:GridView>
                            </td>
                        </tr>
                          </table>
                     </div>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
       
</asp:Content>