﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Parametros_Monto_Compra.Negocios;

public partial class paginas_Compras_Frm_Cat_Com_Parametros_Monto_Compra : System.Web.UI.Page
{
  
    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!Page.IsPostBack)
            {
                Estado_Botones("Inicial");
                Cargar_Monto_Compra();
            }
            Mensaje_Error();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
            Estado_Botones("Inicial");
            //Cargar_Monto_Compra();
        }
    }

    #endregion

    #region Metodos
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Error.Text = "";
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Estado_Botones
    ///DESCRIPCIÓN:    Metodo para establecer el estado de los botones y componentes del formulario
    ///PARAMETROS:     String P_Estado, estado que ayud a identificar en que estatus se van a 
    ///                establecer los botones 
    ///CREO:           Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO:     08/11/2012 06:22:00 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Estado_Botones(String P_Estado)
    {
        Boolean Habilitado = false;
        switch (P_Estado)
        {
            case "Inicial": //Estado inicial
                Habilitado = false;
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Salir.AlternateText = "Inicio";

                Btn_Modificar.ToolTip = "Modificar";
                Btn_Salir.ToolTip = "Inicio";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Txt_Monto_Compra.Text = "0";
                break;
            case "Modificar": //Modificar
                Habilitado = true;
                Btn_Modificar.Visible = true;

                Btn_Modificar.AlternateText = "Actualizar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                break;
        }
        Txt_Monto_Compra.Enabled = Habilitado;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar
    ///DESCRIPCIÓN:    Metodo que valida que exista el dato  por guardar
    ///PARAMETROS:     
    ///CREO:           Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO:     08/11/2012 06:45:00 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Validacion = true;
        if (String.IsNullOrEmpty(Txt_Monto_Compra.Text))
        {
            Validacion = false;
            Mensaje_Error("Monto esta vacio, por favor introduzca el monto");
        }
        else
        {
            double Monto_Compra = 0;
            if (!double.TryParse(Txt_Monto_Compra.Text.ToString().Trim(), out Monto_Compra))
            {
                Validacion = false;
                Mensaje_Error("Lo siento solo se admiten números, por favor introduzca el monto");
            }
            else
            {
                if (Monto_Compra < 100000)
                {
                    Validacion = false;
                    Mensaje_Error("Lo siento la cantida debe ser mayo  o igual a 100000.00, por favor modificque el monto");
                }
            }
        }
        return Validacion;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Monto_Compra
    ///DESCRIPCIÓN:    Metodo que carga los datos del parametro a la pantalla
    ///PARAMETROS:     
    ///CREO:           Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO:     10/11/2012 01:32:00 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Monto_Compra()
    {
        try
        {
            Cls_Cat_Com_Parametros_Monto_Compra_Negocio Parametros_Negocio = new Cls_Cat_Com_Parametros_Monto_Compra_Negocio();
            DataTable Dt_Parametro = null;
            Dt_Parametro = Parametros_Negocio.Consultar_Parametro_Monto_Compras();
            if (Dt_Parametro != null && Dt_Parametro.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Parametro.Rows[0][Cat_Com_Parametros.Campo_Monto_Compra].ToString()))
                {
                    Txt_Monto_Compra.Text = string.Format("{0:00000}", Convert.ToDouble(Dt_Parametro.Rows[0][Cat_Com_Parametros.Campo_Monto_Compra].ToString())); ;
                }
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Actualizar_Monto_Compra
    ///DESCRIPCIÓN:    Metodo que inserta o actualiza el parametro de compra
    ///PARAMETROS:     
    ///CREO:           Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO:     12/11/2012 08:32:00 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Actualizar_Monto_Compra()
    {
        try
        {
            Cls_Cat_Com_Parametros_Monto_Compra_Negocio Parametros_Negocio = new Cls_Cat_Com_Parametros_Monto_Compra_Negocio();
            DataTable Dt_Parametro = null;
            Boolean Operacion = false;
            if (Validar_Datos())
            {
                Parametros_Negocio.P_Monto_Compra = Txt_Monto_Compra.Text.ToString().Trim();
                Dt_Parametro = Parametros_Negocio.Consultar_Parametro_Monto_Compras();
                if (Dt_Parametro != null && Dt_Parametro.Rows.Count > 0)
                {
                    Operacion = Parametros_Negocio.Modificar_Monto_Compra();
                }
                else
                {
                    Operacion = Parametros_Negocio.Alta_Monto_Compra();
                }
                if (Operacion == true)
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Parametros SAP", "alert('La Modificación de los parametros fue Exitosa');", true);
                    Estado_Botones("Inicial");
                    Cargar_Monto_Compra();
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(" Error al guardar el parametro : " + Ex.Message);

        }
    }
    #endregion
    #region Eventos
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Evento para modificar los parametros SAP en la base de datos
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 20/Abril/2011 01:59:32 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************  
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText == "Modificar")
            {
                Estado_Botones("Modificar");
            }
            else if (Btn_Modificar.AlternateText == "Actualizar")
            {
                Actualizar_Monto_Compra();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message.ToString());
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento para salir o cancelar la accion
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 20/Abril/2011 02:00:04 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.AlternateText == "Inicio")
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Estado_Botones("Inicial");
            Cargar_Monto_Compra();
        }
    }
    #endregion

}
