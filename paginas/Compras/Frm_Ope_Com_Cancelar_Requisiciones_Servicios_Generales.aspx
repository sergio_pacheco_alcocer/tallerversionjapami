﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Com_Cancelar_Requisiciones_Servicios_Generales.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Com_Cancelar_Requisiciones" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td colspan="2" class="label_titulo">
                        Cancelar Requisiciones
                    </td>
                </tr>
                <tr align="left">
                    <td colspan="2">
                        <div id="Div_Contenedor_Msj_Error" runat="server">
                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr class="barra_busqueda" align="right">
                    <td align="left" valign="middle">
                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" OnClick="Btn_Modificar_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                    </td>
                    <td style="width: 20%" align="right">
                        <asp:TextBox ID="Txt_Busqueda" runat="server" Width="80%" MaxLength="13"></asp:TextBox>
                        <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                            ToolTip="Consultar" OnClick="Btn_Buscar_Click" />
                        <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="Txt_Busqueda"
                            WatermarkText="&lt;RQ-000000&gt;" WatermarkCssClass="watermarked">
                        </cc1:TextBoxWatermarkExtender>
                        <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" FilterType="Custom"
                            TargetControlID="Txt_Busqueda" ValidChars="rRqQ-0123456789">
                        </cc1:FilteredTextBoxExtender>
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div id="Div_Datos_Generales" runat="server" style="width: 99%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="99%">
                    <tr>
                        <td colspan="24">
                            <asp:HiddenField ID="Hdf_Requisicion_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            Datos Generales
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            Folio
                        </td>
                        <td style="width: 40%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 10%;">
                            Codigo Programatico
                        </td>
                        <td style="width: 40%;">
                            <asp:TextBox ID="Txt_Codigo_Programatico" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            Justificación
                        </td>
                        <td style="width: 40%;">
                            <asp:TextBox ID="Txt_Justificacion" runat="server" Width="97%" TextMode="MultiLine" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 10%;">
                            Total
                        </td>
                        <td style="width: 40%;">
                            <asp:TextBox ID="Txt_Total" runat="server" Enabled="false" Visible="false" ></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Estatus
                        </td>
                        <td>
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Enabled="false" Width="99%">
                                <asp:ListItem Value="SELECCIONE">SELECCIONE</asp:ListItem>
                                <asp:ListItem Value="EN CONSTRUCCION">RECHAZAR</asp:ListItem>
                                <asp:ListItem Value="CANCELADA">CANCELAR</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td>
                            Tipo
                        </td>
                        <td>
                            <asp:TextBox ID="Txt_Tipo" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            Comentarios
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Comentario" runat="server" TabIndex="10" MaxLength="250" TextMode="MultiLine"
                                Width="100%" Enabled="false"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Límite de Caracteres 250>" TargetControlID="Txt_Comentario" />
                        </td>
                        <cc1:FilteredTextBoxExtender ID="Txt_Comentarios_FilteredTextBoxExtender" runat="server"
                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" TargetControlID="Txt_Comentario"
                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                        </cc1:FilteredTextBoxExtender>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            Productos
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Productos" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                    GridLines="None" Width="99%" Enabled="false" AllowSorting="True" 
                    HeaderStyle-CssClass="tblHead">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:BoundField DataField="OPE_COM_REQ_PRODUCTO_ID" HeaderText="Producto_ID" Visible="false">
                        </asp:BoundField>
                        <asp:BoundField DataField="NOMBRE_CONCEPTO" HeaderText="Concepto" SortExpression="NOMBRE_CONCEPTO">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOMBRE_PRODUCTO_SERVICIO" HeaderText="Producto/Servicio" SortExpression="NOMBRE_PRODUCTO_SERVICIO"
                            Visible="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PRECIO_UNITARIO" HeaderText="Precio Unitario" SortExpression="PRECIO_UNITARIO">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="15%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad" SortExpression="CANTIDAD">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Width="8%" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="MONTO_TOTAL" HeaderText="Total" SortExpression="MONTO_TOTAL" >
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <SelectedRowStyle CssClass="GridSelected" />
                    <PagerStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <asp:GridView ID="Grid_Listado" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                    DataKeyNames="No_Requisicion" EmptyDataText="No se encontraron requisiciones"
                    GridLines="None" Width="100%" AllowPaging="true" PageSize="20" 
                    OnPageIndexChanging="Grid_Listado_PageIndexChanging" 
                    onselectedindexchanged="Grid_Listado_SelectedIndexChanged">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" >
                            <ItemStyle Width="2%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="No_Requisicion" HeaderText="Folio" SortExpression="No_Requisicion">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" SortExpression="Folio">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="12%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TIPO" HeaderText="Tipo" SortExpression="TIPO" Visible="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" SortExpression="Estatus"
                            Visible="True">
                            <FooterStyle HorizontalAlign="Left" />
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="No_Requisicion" HeaderText="ID" Visible="false">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL" HeaderText="Total" SortExpression="TOTAL" Visible="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL_COTIZADO" HeaderText="Tot. Cotizado" SortExpression="TOTAL_COTIZADO"
                            Visible="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="8%" />
                        </asp:BoundField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GridAltItem" />
                    <HeaderStyle BackColor="#2F4E7D" ForeColor="White" />
                    <PagerStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridItem" />
                    <SelectedRowStyle CssClass="GridSelected" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
