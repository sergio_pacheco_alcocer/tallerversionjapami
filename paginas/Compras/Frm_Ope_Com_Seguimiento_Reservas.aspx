﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Seguimiento_Reservas.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Seguimiento_Reservas" 
Title="Seguimiento a Reservas" Culture="es-MX"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>                                   
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                    
                       <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                       
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                
                
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Seguimiento a Reservas (Contabilidad)
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                                
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Listar_Requisiciones" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Listar Requisiciones" OnClick="Btn_Listar_Requisiciones_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td>

                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div ID="Div_Listado_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr style="display:none;">
                            <td style="width: 15%;">
                                Unidad Responsable
                            </td>
                            <td style="width: 43%;">
                                <asp:DropDownList ID="Cmb_Dependencia_Panel" runat="server" Width="98%" 
                                     />
                            </td>
                            <td style="width: 12%;" align="right" visible="false">
                                Tipo
                            </td>
                            <td visible="false">
                                <asp:DropDownList ID="Cmb_Tipo_Busqueda" runat="server" Width="98%" />
                            </td>
                        </tr>                    
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td align="right" visible="false">
                                No. Reserva
                            </td>
                            <td visible="false">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="85%"  MaxLength="10" AutoPostBack="true" ontextchanged="Txt_Busqueda_TextChanged" ></asp:TextBox>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" ToolTip="Consultar"/>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" TargetControlID="Txt_Busqueda" WatermarkText="&lt;Reserva&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" FilterType="Custom" 
                                    TargetControlID="Txt_Busqueda" ValidChars="rRqQ-0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="Chk_Empleado" Text="Empleado" runat="server" AutoPostBack="true"
                                    oncheckedchanged="Chk_Empleado_CheckedChanged" />
                               
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Empleado" runat="server" Width="80%" Enabled="false" 
                                    onselectedindexchanged="Cmb_Empleado_SelectedIndexChanged" AutoPostBack = "true" >
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr >
                            <td colspan="4">
                            <div id="Div_Busqueda_Empleados" runat="server" visible="false">
                                <table width="100%" bgcolor="silver">
                                    <tr>
                                        <td width="10%">
                                            Nombre
                                        </td>
                                        <td width="35%">
                                            <asp:TextBox ID="Txt_Nombre_Empleado" runat="server" Width="90%"></asp:TextBox>
                                        </td>
                                        <td width="10%">
                                            No. Empleado
                                        </td>
                                        <td width="25%">
                                            <asp:TextBox ID="Txt_No_Empleado" runat="server" ></asp:TextBox>
                                        </td>
                                        <td width="10%">
                                            <asp:ImageButton ID="Btn_Buscar_empleado" runat="server" Height="15pt" 
                                                ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" 
                                                ToolTip="Buscar Empleado" Width="15pt" 
                                                onclick="Btn_Buscar_empleado_Click" />
                                        </td>
                                    </tr>
                                
                                </table>
                            </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:CheckBox ID="Chk_Proveedor" Text="Proveedor" runat="server" AutoPostBack="true"
                                    oncheckedchanged="Chk_Proveedor_CheckedChanged" />
                               
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Proveedor" runat="server" Width="80%" Enabled="false" 
                                    onselectedindexchanged="Cmb_Proveedor_SelectedIndexChanged" AutoPostBack = "true">
                                </asp:DropDownList>
                            </td>
                            
                        </tr>
                        <tr>
                        <td colspan="4">
                            <div id="Div_Busqueda_Proveedores" runat="server" visible="false" >
                                <table width="100%" bgcolor="silver">
                                <tr>
                                    <td width="10%">
                                    Razon Social
                                    </td>
                                    <td width="35%">
                                        <asp:TextBox ID="Txt_Proveedor_Nombre" runat="server" Width="30%"></asp:TextBox>
                                    </td>
                                    <td width="10%">
                                        Padron Proveedor
                                    </td>
                                    <td width="25%">
                                        <asp:TextBox ID="Txt_Padron_Prov" runat="server"></asp:TextBox>
                                    </td>
                                    <td width="10%">
                                        <asp:ImageButton ID="Btn_Buscar_Prov" runat="server" 
                                            ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Width="15pt" 
                                            Height="15pt"  ToolTip="Buscar Empleado" onclick="Btn_Buscar_Prov_Click"/>
                                    </td>
                             </tr>
                                </table>
                            </div>
                            </td>
                       
                        </tr>
                        <tr>
                            <td colspan="4">
                                <table width="100%">
                                    <tr>
                                        <td style="width:11%">
                                            <asp:CheckBox ID="Chk_Requisicion" Text="No.Requisicion" runat="server" AutoPostBack="true"
                                                oncheckedchanged="Chk_Requisicion_CheckedChanged" />
                                        </td>
                                        <td style="width:20%">
                                            <asp:TextBox ID="Txt_No_Requisicion" ontextchanged="Txt_Busqueda_TextChanged" runat="server" Enabled="false" AutoPostBack="true"></asp:TextBox> 
                                            <cc1:FilteredTextBoxExtender  ID="Txt_No_Requisicion_FilteredTextBoxExtender" 
                                             runat="server" FilterType="Numbers" TargetControlID="Txt_No_Requisicion">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width:20%">
                                            <asp:CheckBox ID="Chk_Orden_Compra" Text="Orden de Compra" runat="server" AutoPostBack="true" 
                                                oncheckedchanged="Chk_Orden_Compra_CheckedChanged" />
                                        </td>
                                        <td style="width:20%">
                                            <asp:TextBox ID="Txt_Orden_Compra"  runat="server" AutoPostBack="true" Enabled="false" ontextchanged="Txt_Busqueda_TextChanged"></asp:TextBox> 
                                            <cc1:FilteredTextBoxExtender  ID="Txt_Orden_Compra_Filtered" 
                                             runat="server" FilterType="Numbers" TargetControlID="Txt_Orden_Compra">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                              <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;" > 
                                <asp:GridView ID="Grid_Requisiciones" runat="server" AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="100%"
                                  
                                    AllowSorting="true" HeaderStyle-CssClass="tblHead" 
                             
                                    EmptyDataText="No se encontraron reservas"
                                    >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Seleccionar_Requisicion" runat="server" 
                                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                    OnClick="Btn_Seleccionar_Requisicion_Click"                                                    
                                                    CommandArgument='<%# Eval("No_Reserva") %>'/>
                                            </ItemTemplate >
                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%" />
                                        </asp:TemplateField>  
                                                                         
                                        <asp:BoundField DataField="NO_RESERVA" HeaderText="Folio" Visible="True" SortExpression="No_Requisicion">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha" 
                                            DataFormatString="{0:dd/MMM/yyyy}" Visible="True" SortExpression="Fecha_Creo">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Hora" 
                                            DataFormatString="{0:hh:mm:ss tt}" Visible="True" SortExpression="Fecha_Creo">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>                                        
                                       <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto" 
                                            Visible="True" SortExpression="Concepto">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="50%" />
                                        </asp:BoundField>                                          
                                       <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                                            Visible="True" SortExpression="Estatus">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>  
                                                                                                                                                        
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                              </div>  
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;" align="left">
                                No. Reserva
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="96%"></asp:TextBox>
                            </td>

                        </tr>
                        <tr style="display:none;">
                            <td style="width: 15%;" align="left">
                                Unidad Responsable
                            </td>
                            <td style="width: 35%;" align="left">
                                <asp:DropDownList ID="Cmb_Dependencia" runat="server" Width="98%"  AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                        </tr>                        
                        <tr>
                            <td align="center" colspan="2">
                                <asp:GridView ID="Grid_Productos_Servicios" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="100%"                                     
                                    style="white-space:normal" EmptyDataText="No hay historial para mostrar">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="Fecha" HeaderText="Fecha" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="XX-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha" HeaderText="Hora" Visible="True" DataFormatString="{0:hh:mm:ss tt}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="XX-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Usuario_Creo" HeaderText="Modificó" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Left" Width="20%" Font-Size="XX-Small"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Cargo" HeaderText="Cargo" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Abono" HeaderText="Abono" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" Font-Size="XX-Small" />
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Importe" HeaderText="Importe" Visible="True" DataFormatString="{0:C}">
                                            <FooterStyle HorizontalAlign="Right" />
                                            <HeaderStyle HorizontalAlign="Right" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="XX-Small" />
                                        </asp:BoundField>                                        
                                        <asp:TemplateField HeaderText="No.Póliza" Visible="True">
                                             <ItemTemplate>
                                                <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("No_Poliza") %>'
                                                OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("Fecha") %>' ForeColor="Blue"  CssClass='<%# Eval("Tipo_Poliza_ID") %>' />
                                            </ItemTemplate >
                                            <HeaderStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Center"  Width="12%" Font-Size="XX-Small" />
                                        </asp:TemplateField>                                        
                                        <asp:TemplateField HeaderText="Póliza Presupuestal" Visible="True">
                                             <ItemTemplate>
                                                <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza_Presupuestal" runat="server" Text= '<%# Eval("No_Poliza_Presupuestal") %>'
                                                OnClick="Btn_Poliza_Click" ToolTip='<%#Eval("MES_ANIO_PRESUPUESTAL")%>' ForeColor="Blue"  CssClass='<%# Eval("Tipo_Poliza_ID_Presupuestal") %>' />
                                            </ItemTemplate >
                                            <HeaderStyle HorizontalAlign="Center"  Font-Size="X-Small" />
                                            <ItemStyle HorizontalAlign="Center"  Width="12%" Font-Size="XX-Small" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr style="height:20px;">
                            <td ></td>                        
                        </tr>
                        <tr>                            
                            <td align="center" colspan="2">                        
                                <asp:GridView ID="Grid_Comentarios" runat="server" AllowPaging="false" AutoGenerateColumns="false"
                                    GridLines="None" PageSize="3" Width="100%" 
                                    Style="font-size: xx-small; white-space: normal" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                            Visible="false">
                                            <ItemStyle Width="5%" />
                                        </asp:ButtonField>

                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus" Visible="false">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Hora" Visible="True" DataFormatString="{0:hh:mm:ss tt}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%" Font-Size="X-Small"/>
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="Usuario_Creo" HeaderText="Usuario" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="24%" />
                                            <ItemStyle HorizontalAlign="Left" Width="24%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Comentario" HeaderText="Comentarios" Visible="True">
                                            <HeaderStyle HorizontalAlign="Left" Width="68%" />
                                            <ItemStyle HorizontalAlign="Left" Width="68%" Font-Size="X-Small" />
                                        </asp:BoundField>                                        
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>    
</asp:Content>

