﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using JAPAMI.Archivos_Ordenes_Compra.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Orden_Compra.Negocio;
using JAPAMI.Dependencias.Negocios;

public partial class paginas_Compras_Frm_Ope_Com_Ordenes_Compra_Archivo : System.Web.UI.Page
{
    #region variables y constantes
    private static String P_Dt_Ordenes_Compra = "P_Dt_Ordenes_Compra";
    private const String Operacion_Comprometer = "COMPROMETER";
    private const String Operacion_Descomprometer = "DESCOMPROMETER";
    private const String Operacion_Quitar_Renglon = "QUITAR";
    private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
    private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

    private const String SubFijo_Requisicion = "RQ-";
    private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
    private const String EST_GENERADA = "GENERADA";
    private const String EST_CANCELADA = "CANCELADA";
    private const String EST_REVISAR = "REVISAR";
    private const String EST_RECHAZADA = "RECHAZADA";
    private const String EST_AUTORIZADA = "AUTORIZADA";
    private const String EST_FILTRADA = "FILTRADA";
    private const String EST_COTIZADA = "COTIZADA";
    private const String EST_COMPRA = "COMPRA";
    private const String EST_TERMINADA = "TERMINADA";
    private const String EST_COTIZADA_RECHAZADA = "COTIZADA-RECHAZADA";
    private const String EST_ALMACEN = "ALMACEN";
    private const String P_Dt_Archivos_Ordenes_Compra = "Dt_Archivos_Ordenes_Compra";
    private const String P_Dt_Archivos_Eliminados = "Dt_Archivos_Eliminados";
    private const String P_Nombre_Archivo = "Nombre_Archivo";
    private const String P_Comentarios = "Comentarios";

    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (!IsPostBack)
                {
                    Session["Activa"] = true;
                    Estado_Inicial();
                }
                else
                {
                    Mostrar_Informacion("", false);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Page_Load) " + ex.ToString(), true);
            }
        }
    #endregion

    #region Metodos

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Mostrar_Informacion()
        // DESCRIPCION:             Muestra los mensajes requeridos
        // RETORNA: 
        // CREO:                    David Herrera rincon
        // FECHA_CREO:              14/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            Lbl_Informacion.Style.Add("color", "#990000");
            Lbl_Informacion.Visible = mostrar;
            Img_Warning.Visible = mostrar;
            Lbl_Informacion.Text = txt;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Estado_Inicial()
        // DESCRIPCION:             Colocar la pagina en un estado inicial para su navegacion
        // RETORNA: 
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              14/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Estado_Inicial()
        {
            try
            {
                Eliminar_Sesiones();
                Limpiar_Controles_Busqueda();
                Habilitar_Controles("Navegacion");
                
                //Colocar las fechas
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);               
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();

                Div_Contenido.Visible = false;
                Div_Requisiciones.Visible = true;
                Txt_No_Orden_Compra.Value = "";
                
                Llenar_Grid_Ordenes_Compras();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Eliminar_Sesiones()
        // DESCRIPCION:             Eliminar las sesiones utilizadas en esta pagina
        // RETORNA: 
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACION:
        //********************************************************************************/
        private void Eliminar_Sesiones()
        {
            try
            {
                Session.Remove(P_Dt_Ordenes_Compra);
                Session.Remove(P_Dt_Archivos_Ordenes_Compra);
                Session.Remove(P_Nombre_Archivo);
                Session.Remove(P_Comentarios);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }               

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
        ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
        ///en la busqueda del Modalpopup
        ///PARAMETROS:   
        ///CREO: David Herrera Rincon
        ///FECHA_CREO: 12/Enero/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        public bool Verificar_Fecha()
        {
            bool Respuesta = false;
            //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
            DateTime Date1 = new DateTime();
            DateTime Date2 = new DateTime();
            try
            {
                //Convertimos el Texto de los TextBox fecha a dateTime
                Date1 = DateTime.Parse(Txt_Fecha_Inicio.Text);
                Date2 = DateTime.Parse(Txt_Fecha_Final.Text);
                if (Date1 <= Date2)
                {
                    Respuesta = true;
                }
            }
            catch (Exception e)
            {
                String str = e.ToString();
                Respuesta = false;
            }
            return Respuesta;
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Habilitar_Controles
        // DESCRIPCIÓN:             Habilitar los controles de aucerdo al modo de operacion
        // PARAMETROS:              Modo: Cadena de texto quie contiene el modo de operacion
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Habilitar_Controles(String Modo)
        {
            try
            {
                //Seleccionar el modo de operacion
                switch (Modo)
                {
                    case "Navegacion":                      
                        Div_Contenido.Visible = false;
                        Div_Requisiciones.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Eliminar.Visible = false;
                        Btn_Modificar.Visible = false;
                        Btn_Cargar.Visible = false;
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Eliminar.ToolTip = "Eliminar Archivos";
                        break;

                    case "Operacion":
                        Div_Contenido.Visible = true;
                        Div_Requisiciones.Visible = false;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Eliminar.Visible = true;
                        Btn_Modificar.Visible = true;
                        Btn_Cargar.Visible = false;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Modificar.ToolTip = "Modificar";
                        break;

                    case "Archivo":
                        Div_Contenido.Visible = true;
                        Div_Requisiciones.Visible = false;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Eliminar.Visible = false;
                        Btn_Modificar.Visible = true;
                        Btn_Cargar.Visible = true;
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Modificar.ToolTip = "Guardar";
                        break;

                    default:
                        break;
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Limpiar_Controles_Busqueda()
        // DESCRIPCIÓN:             Limpiar los controles de la seccíon de busqueda
        // RETORNA: 
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              05/Marzo/2012 13:24
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Limpiar_Controles_Busqueda()
        {
            try
            {                
                Cmb_Estatus_Busqueda.SelectedIndex = 0;
                Txt_Orden_Compra_Busqueda.Text = "";
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Llena_Datos_Controles
        // DESCRIPCION:             Llenar los datos de los controles cuando se selecciona una requisicion
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Llena_Datos_Controles(int No_Orden)
        {
            //Declaracion de variables
            DataTable Dt_Archivos_Requisicion = new DataTable(); //Tabla para la consulta de los archivos de la requisicion

            try
            {
                //Colocar los datos en el formulario
                Lbl_Archivos_Orden_Compra.Text = HttpUtility.HtmlDecode("Archivos de la Orden Compra OC-" + No_Orden.ToString().Trim());

                Llena_Grid_Archivos_Ordenes_Compra(No_Orden);                
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Alta_Archivos
        // DESCRIPCION:             Guarda los archivos de la orden de compra
        // PARAMETROS:              
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Alta_Archivos()
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Ordenes_Compra_Negocio Archivos_Orden_Compra_Negocio = new Cls_Ope_Archivos_Ordenes_Compra_Negocio(); //variable para la capa de negocios
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos que van a ser ingresados a la requisicion
            DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos a eliminar

            try
            {
                //Obtener las tablas de los archivos eliminados y los archivos a subir
                //asignar propiedades
                Archivos_Orden_Compra_Negocio.P_No_Orden_Compra = Convert.ToInt32(Txt_No_Orden_Compra.Value);
                Archivos_Orden_Compra_Negocio.P_Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra]);
                Archivos_Orden_Compra_Negocio.P_Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);

                //Dar de alta los archivos de la requisicion
                Archivos_Orden_Compra_Negocio.Alta_Archivos_Onden_Compra();

                //Colocar los archivos en el directorio
                Agrega_Archivos_Ordenes_Compra(Convert.ToInt32(Txt_No_Orden_Compra.Value));
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Agrega_Archivos_Ordenes_Compra
        // DESCRIPCION:             Guarda los archivos de la orden de compra en el directorio
        // PARAMETROS:              No_Orden: Agrega los archivos segun el numero de orden de compra
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              14/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Agrega_Archivos_Ordenes_Compra(int No_Orden)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos a agregar
            DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos eliminados
            int Cont_Elementos; //variable para el contador
            String Ruta_Fija = String.Empty; //variable para la ruta fija
            String[] Archivos_Temporales; //variable para los archivos temporales

            try
            {
                //Asignar la ruta de los archivos
                Ruta = Server.MapPath("~") + "\\Ord_Com_Archivos\\OC-" + No_Orden.ToString().Trim();
                Ruta_Fija = Ruta;

                //verificar si la ruta existe
                if (Directory.Exists(Ruta) == false)
                {
                    Directory.CreateDirectory(Ruta);
                } 

                //Verificar si la variable de sesion de los archivos eliminados existe
                if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] != null)
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);

                    //Verificar si la tabla tiene elementos
                    if (Dt_Archivos_Eliminados.Rows.Count > 0)
                    {
                        //Ciclo para eliminar los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos_Eliminados.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar si el archivo tiene que ser eliminado
                            if (Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Original"].ToString().Trim() == "SI")
                            {
                                //Verificar si el archivio existe
                                if (File.Exists(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                {
                                    //Eliminar el archivo
                                    File.Delete(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                }
                            }
                        }
                    }
                }

                //Asignar la ruta de los archivos temporales
                Ruta = Server.MapPath("~") + "\\Ord_Com_Archivos\\temporal\\OC-" + No_Orden.ToString().Trim();

                //Verificar si la ruta existe
                if (Directory.Exists(Ruta) == true)
                {
                    //Verificar si la variable de sesion existe
                    if (HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] != null)
                    {
                        //Colocar la variable de sesion en la tabla
                        Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra]);

                        //Verificar si la tabla tiene elementos
                        if (Dt_Archivos.Rows.Count > 0)
                        {
                            //Ciclo para copiar los archivos temporales a la ruta de los archivos
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                            {
                                //Verificar si se tiene que copiar el elemento
                                if (Dt_Archivos.Rows[Cont_Elementos]["Original"].ToString().Trim() == "NO")
                                {
                                    //Verificar si el archivo existe
                                    if (File.Exists(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                    {
                                        //Copiar el archivo
                                        File.Copy(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim(), Ruta_Fija + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                    }
                                }
                            }
                        }
                    }

                    //Obtener el listado de los archivos de la ruta temporal
                    Archivos_Temporales = Directory.GetFiles(Ruta);

                    //Verificar si hay archivos
                    if (Archivos_Temporales.Length > 0)
                    {
                        //Ciclo para la eliminacion de los archivos temporales
                        for (Cont_Elementos = 0; Cont_Elementos < Archivos_Temporales.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Archivos_Temporales[Cont_Elementos].Trim());
                        }
                    }  
                  
                    //Eliminar el directorio
                    Directory.Delete(Ruta);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos
        // DESCRIPCION:             Eliminar los archivos de la orden de compra
        // PARAMETROS:              No_Orden: Entero que contiene el numero de la orden
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos(int No_Orden)
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Ordenes_Compra_Negocio Archivos_Orden_Compra_Negocio = new Cls_Ope_Archivos_Ordenes_Compra_Negocio(); //variable para la capa de negocios
            DataTable Dt_Temp = new DataTable();

            try
            {
                //Asignar propiedades
                Archivos_Orden_Compra_Negocio.P_No_Orden_Compra = No_Orden;
                //Consultamos el archivo de la requisicion
                Dt_Temp = Archivos_Orden_Compra_Negocio.Consulta_Archivos();
                if (Dt_Temp.Rows.Count > 0)
                {
                    if (Dt_Temp.Rows[0]["USUARIO_CREO"].ToString() == Cls_Sessiones.Nombre_Empleado)
                    {
                        Archivos_Orden_Compra_Negocio.Eliminar_Archivos_Orden_Compra();
                        //Eliminar los archivos
                        Elimina_Archivos_Ordenes_Compra(No_Orden);
                    }
                    else
                    {
                        Mostrar_Informacion("Lo siento, solo el usuario que agrego los archivos a la orden de compra puede eliminarlos", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos_Ordenes_Compra
        // DESCRIPCION:             Eliminar los archivos de la orden de compra
        // PARAMETROS:              No_Orden: Entero que contiene el numero de la orden de compra
        // CREO:                    David Herrera Rincon
        // FECHA_CREO:              12/Enero/2013
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos_Ordenes_Compra(int No_Orden)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta de los archivos
            int Cont_Elementos; //variable para el contador
            String[] Vec_Archivos; //Vector para los archivos

            try
            {
                //Obtener la ruta de los archivos
                Ruta = Server.MapPath("~") + "\\Ord_Com_Archivos\\OC-" + No_Orden.ToString().Trim();

                //Verificar si existe la carpeta
                if (Directory.Exists(Ruta) == true)
                {
                    //Obtener los archivos del directorio
                    Vec_Archivos = Directory.GetFiles(Ruta);

                    //Verificar si existen archivos en el directorio
                    if (Vec_Archivos.Length > 0)
                    {
                        //Ciclo para el barrido de los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Vec_Archivos.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Vec_Archivos[Cont_Elementos].Trim());
                        }
                    }

                    //Eliminar la carpeta de los archivos
                    Directory.Delete(Ruta);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Grid
        #region Grid Ordenes Compra
            ///*******************************************************************************
            // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Ordenes_Compras
            // DESCRIPCIÓN: Llena el grid principal de las ordenes de compra
            // RETORNA: 
            // CREO: David Herrera Rincon
            // FECHA_CREO: 12/Enero/2013 
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            public void Llenar_Grid_Ordenes_Compras()
            {
                Div_Contenido.Visible = false;
                Cls_Ope_Com_Orden_Compra_Negocio Orden_Compra_Negocio = new Cls_Ope_Com_Orden_Compra_Negocio();

                Orden_Compra_Negocio.P_Fecha_Inicial = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Inicio.Text);

                Orden_Compra_Negocio.P_Fecha_Final = Txt_Fecha_Final.Text;
                Orden_Compra_Negocio.P_Fecha_Final = String.Format("{0:dd-MMM-yyyy}", Txt_Fecha_Final.Text);
                Orden_Compra_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;
                if (Txt_Orden_Compra_Busqueda.Text.Trim().Length > 0)
                {
                    String No_Orden = Txt_Orden_Compra_Busqueda.Text;
                    No_Orden = No_Orden.ToUpper();
                    No_Orden = No_Orden.Replace("CO-", "");
                    int Int_No_Orden = 0;
                    try
                    {
                        Int_No_Orden = int.Parse(No_Orden);
                    }
                    catch (Exception Ex)
                    {
                        String Str = Ex.ToString();
                        No_Orden = "0";
                    }
                    Orden_Compra_Negocio.P_Folio = No_Orden;
                }
                Orden_Compra_Negocio.P_Estatus = Cmb_Estatus_Busqueda.SelectedValue.Trim();
                if (Orden_Compra_Negocio.P_Estatus == "0")
                    Orden_Compra_Negocio.P_Estatus = "";
                Session[P_Dt_Ordenes_Compra] = Orden_Compra_Negocio.Consultar_Ordenes_Compra();
                if (Session[P_Dt_Ordenes_Compra] != null && ((DataTable)Session[P_Dt_Ordenes_Compra]).Rows.Count > 0)
                {
                    Div_Contenido.Visible = false;
                    Grid_Ordenes_Compra.DataSource = Session[P_Dt_Ordenes_Compra] as DataTable;
                    Grid_Ordenes_Compra.DataBind();
                }
                else
                {                         
                    Session[P_Dt_Ordenes_Compra] = null;
                    Grid_Ordenes_Compra.DataSource = null;
                    Grid_Ordenes_Compra.DataBind();
                }
            }

            protected void Grid_Ordenes_Compra_PageIndexChanging(object sender, GridViewPageEventArgs e)
            {
                try
                {
                    Grid_Ordenes_Compra.DataSource = ((DataTable)Session[P_Dt_Ordenes_Compra]);
                    Grid_Ordenes_Compra.PageIndex = e.NewPageIndex;
                    Grid_Ordenes_Compra.DataBind();
                }
                catch (Exception ex)
                {
                    Mostrar_Informacion("Error: (Grid_Ordenes_Compra_PageIndexChanging) " + ex.ToString(), true);
                }
            }

            protected void Btn_Seleccionar_Orden_Compra_Click(object sender, ImageClickEventArgs e)
            {
                //Declaracion de variables
                int No_Requisicion; //variable para el numero de la requisicion

                try
                {
                    //Obtener el numero de la requisicion
                    No_Requisicion = Convert.ToInt32(((ImageButton)sender).CommandArgument);
                    Txt_No_Orden_Compra.Value = No_Requisicion.ToString().Trim();

                    Llena_Datos_Controles(No_Requisicion);

                    Habilitar_Controles("Operacion");
                }
                catch (Exception ex)
                {
                    Mostrar_Informacion("Error: (Grid_Ordenes_Compra_PageIndexChanging) " + ex.ToString(), true);
                }
            }
        #endregion

    #region Grid Archivos
            ///*******************************************************************************
            // NOMBRE DE LA FUNCION:    Llena_Grid_Archivos_Ordenes_Compra
            // DESCRIPCION:             Llenar el Grid con los datos de los archivos de la Orden de Compra
            // PARAMETROS:              No_Orden: Entero que contiene el numero de al Orden de compra
            // CREO:                    David Herrera Rincon
            // FECHA_CREO:              12/Enero/2013
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            private void Llena_Grid_Archivos_Ordenes_Compra(int No_Orden)
            {
                //Declaracion de variables
                Cls_Ope_Archivos_Ordenes_Compra_Negocio Archivos_Orden_Compra_Negocio = new Cls_Ope_Archivos_Ordenes_Compra_Negocio(); //variable para la capa de negocios
                DataTable Dt_Archivos_Requisicion = new DataTable();
                try
                {
                    //Verificar si existe la sesion de los archivos de la requisicion
                    if (HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] == null)
                    {
                        //ejecutar la consulta
                        Archivos_Orden_Compra_Negocio.P_No_Orden_Compra = No_Orden;
                        Dt_Archivos_Requisicion = Archivos_Orden_Compra_Negocio.Consulta_Archivos();
                    }
                    else
                    {
                        //Colocar la variable de sesion en la tabla
                        Dt_Archivos_Requisicion = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra]);
                    }

                    //llenar el grid
                    Grid_Archivos_Requisiciones.DataSource = Dt_Archivos_Requisicion;
                    Grid_Archivos_Requisiciones.DataBind();

                    //Colocar la tabla en una variable de sesion
                    HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] = Dt_Archivos_Requisicion;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString(), ex);
                }
            }

            ///*******************************************************************************
            // NOMBRE DE LA FUNCION:    Agregar_Archivo_Tabla
            // DESCRIPCION:             Agregar la informacion del archivo a la tabla para el llenado del grid de los archivos
            // PARAMETROS:              1. Nombre_Archivo: Cadena de texto que contiene el nombre del archivo
            //                          2. Comentarios: Cadena de texto que contiene los comentarios del archivo
            //                          3. No_Requisicion: Entero que contiene el numero de la requisicion a la cual sera colocado el archivo
            // CREO:                    David Herrera Rincon
            // FECHA_CREO:              14/Enero/2013
            // MODIFICO:
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            private void Agregar_Archivo_Tabla(String Nombre_Archivo, String Comentarios, int No_Requisicion)
            {
                //Declaracion de variables
                DataTable Dt_Archivos_Requisicion = new DataTable(); //Tabla para los archivos de la requisicion
                DataRow Renglon; //Renglon para el llenado de la tabla
                int Cont_Elementos; //variable para el contador
                Boolean Existente = false; //variable que indica que el archivo ya existe

                try
                {
                    //Verificar si existe la variable de sesion
                    if (HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] == null)
                    {
                        //Definir las columnas
                        Dt_Archivos_Requisicion.Columns.Add("No_Archivo", typeof(Int64));
                        Dt_Archivos_Requisicion.Columns.Add("Folio", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Nombre_Archivo", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Descripcion", typeof(String));
                        Dt_Archivos_Requisicion.Columns.Add("Original", typeof(String));
                    }
                    else
                    {
                        //Asignar la variable de sesion a la tabla
                        Dt_Archivos_Requisicion = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra]);
                    }

                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos_Requisicion.Rows.Count; Cont_Elementos++)
                    {
                        //Verificar que el renglon no haya sido eleiminado
                        if (Dt_Archivos_Requisicion.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                        {
                            //Verificar si el archivo ya se encuentra en los archivos de la requisicion
                            if (Dt_Archivos_Requisicion.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim().ToUpper() == Nombre_Archivo.Trim().ToUpper())
                            {
                                //Indicar que ya existe ese elemento
                                Existente = true;
                                break;
                            }
                        }
                    }

                    //Verificar si se tiene que agregar el archivo en la tabla
                    if (Existente == false)
                    {
                        //Instanciar el renglon
                        Renglon = Dt_Archivos_Requisicion.NewRow();

                        //Llenar el renglon
                        Renglon["No_Archivo"] = Convert.ToInt64(String.Format("{0:yyyyMMddHHmmss}", DateTime.Now));
                        Renglon["Folio"] = "OC-" + No_Requisicion.ToString().Trim();

                        //verificar si el nombre del archivo no excede de 250 caracteres
                        if (Nombre_Archivo.Trim().Length > 250)
                        {
                            Renglon["Nombre_Archivo"] = Nombre_Archivo.Substring(0, 250);
                        }
                        else
                        {
                            Renglon["Nombre_Archivo"] = Nombre_Archivo;
                        }

                        //verificar si los comentarios exceden de 500
                        if (Comentarios.Trim().Length > 500)
                        {
                            Renglon["Descripcion"] = Comentarios.Substring(0, 500);
                        }
                        else
                        {
                            Renglon["Descripcion"] = Comentarios;
                        }

                        Renglon["Original"] = "NO";

                        //Colocar el renglon en la tabla
                        Dt_Archivos_Requisicion.Rows.Add(Renglon);
                    }

                    //Colocar la tabla en la variable de sesion
                    HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] = Dt_Archivos_Requisicion;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString(), ex);
                }
            }

            ///*******************************************************************************
            // NOMBRE DE LA FUNCION:    Elimina_Archivo_Tabla
            // DESCRIPCION:             Elimina el archivo seleccionado de la tabla
            // PARAMETROS:              No_Archivo: Entero que contiene el numero del archivo
            // CREO:                    David Herrera Rincon
            // FECHA_CREO:              14/Enero/2013
            // FECHA_MODIFICO:
            // CAUSA_MODIFICACIÓN:
            //********************************************************************************/
            private void Elimina_Archivo_Tabla(Int64 No_Archivo)
            {
                //Declaracion de variables
                DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos
                DataTable Dt_Archivos_Eliminados = new DataTable(); //Tabla para los archivos eliminados
                DataRow Renglon; //Renglon para el llenado de la tabla
                int Cont_Elementos; //variable para el contador

                try
                {
                    //verificar si la variable de sesion de los archivos 
                    if (HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] != null)
                    {
                        //Colocar la variable se sesion en la tabla
                        Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra]);

                        //Verificar si la tabla tiene elementos
                        if (Dt_Archivos.Rows.Count > 0)
                        {
                            //Verificar si existe la variable de sesion
                            if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] == null)
                            {
                                //Colocar columnas a la tabla
                                //Definir las columnas
                                Dt_Archivos_Eliminados.Columns.Add("No_Archivo", typeof(Int64));
                                Dt_Archivos_Eliminados.Columns.Add("Folio", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Nombre_Archivo", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Descripcion", typeof(String));
                                Dt_Archivos_Eliminados.Columns.Add("Original", typeof(String));
                            }
                            else
                            {
                                Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);
                            }

                            //ciclo para el barrido de la tabla
                            for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                            {
                                //verificar que el renglon no haya sido eliminado
                                if (Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                                {
                                    //Verificar si el elemento es el que se tiene que eliminar
                                    if (Convert.ToInt64(Dt_Archivos.Rows[Cont_Elementos]["No_Archivo"]) == No_Archivo)
                                    {
                                        //instanciar renglon
                                        Renglon = Dt_Archivos.Rows[Cont_Elementos];

                                        //Colocar renglon en la tabla de los eliminados
                                        Dt_Archivos_Eliminados.ImportRow(Renglon);

                                        //Eliminar el renglon de la tabla de los archivos
                                        Dt_Archivos.Rows.RemoveAt(Cont_Elementos);

                                        //Salir del ciclo
                                        break;
                                    }
                                }
                            }

                            //Actualizar las variables de sesion
                            HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] = Dt_Archivos;
                            HttpContext.Current.Session[P_Dt_Archivos_Eliminados] = Dt_Archivos_Eliminados;

                            //Llenar el grid
                            Llena_Grid_Archivos_Ordenes_Compra(Convert.ToInt32(Txt_No_Orden_Compra.Value));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.ToString(), ex);
                }
            }
    #endregion
#endregion

    #region Eventos
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar el tooltip del boton
                if (Btn_Salir.ToolTip == "Cancelar")
                {
                    Estado_Inicial();
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Salir_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Llenar_Grid_Ordenes_Compras();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Buscar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //seleccionar la operacion a realizar
                switch (Btn_Modificar.ToolTip)
                {
                    case "Guardar":
                        //verificar si hay datos de guardar
                        if (HttpContext.Current.Session[P_Dt_Archivos_Ordenes_Compra] != null)
                        {
                            Alta_Archivos();
                            Estado_Inicial();
                        }
                        else
                        {
                            Mostrar_Informacion("No se han seleccionado archivos para la Orden Compra", true);
                        }
                        break;

                    case "Modificar":
                        Habilitar_Controles("Archivo");
                        break;

                    default:
                        Estado_Inicial();
                        break;
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Modificar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Verificar si se selecciono un elemento
                if (Txt_No_Orden_Compra.Value != null && Txt_No_Orden_Compra.Value != "" && Txt_No_Orden_Compra.Value != String.Empty)
                {
                    Elimina_Archivos(Convert.ToInt32(Txt_No_Orden_Compra.Value));
                    Estado_Inicial();
                }
                else
                {
                    Mostrar_Informacion("Favor de seleccionar la Orden Compra a la cual se eliminaran todos sus archivos", true);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Modificar_Click) " + ex.ToString(), true);
            }
        }
    
        protected void AFil_Archivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta

            try
            {
                //Verificar si se tiene un archivo
                if (AFil_Archivo.HasFile)
                {
                    //obtener la ruta del servidor
                    Ruta = HttpContext.Current.Server.MapPath("~");

                    //verificar si el directorio ya ha sido creado
                    if (Directory.Exists(Ruta + "\\Ord_Com_Archivos\\temporal\\OC-" + Txt_No_Orden_Compra.Value.Trim()) == false)
                    {
                        Directory.CreateDirectory(Ruta + "\\Ord_Com_Archivos\\temporal\\OC-" + Txt_No_Orden_Compra.Value.Trim());
                    }

                    Ruta = Ruta + "\\Ord_Com_Archivos\\temporal\\OC-" + Txt_No_Orden_Compra.Value.Trim() + "\\" + System.IO.Path.GetFileName(AFil_Archivo.FileName);

                    //Colocar los datos en los controles ocultos
                    HttpContext.Current.Session[P_Nombre_Archivo] = AFil_Archivo.FileName;

                    AFil_Archivo.SaveAs(Ruta);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (AFil_Archivo_UploadedComplete) " + ex.ToString(), true);
            }
        }

        protected void Btn_Archivos_Orden_Compra_Click(object sender, EventArgs e)
        {
            try
            {
                //Verificar si se tiene un archivo
                if (HttpContext.Current.Session[P_Nombre_Archivo] != null)
                {
                    HttpContext.Current.Session[P_Comentarios] = Txt_Comentarios_Archivo.Text.Trim();
                    Agregar_Archivo_Tabla(HttpContext.Current.Session[P_Nombre_Archivo].ToString().Trim(), HttpContext.Current.Session[P_Comentarios].ToString().Trim(), Convert.ToInt32(Txt_No_Orden_Compra.Value.Trim()));
                    Llena_Grid_Archivos_Ordenes_Compra(Convert.ToInt32(Txt_No_Orden_Compra.Value.Trim()));
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Archivos_Requisicion_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Cargar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Txt_Comentarios_Archivo.Text = "";
                HttpContext.Current.Session.Remove(P_Nombre_Archivo);
                HttpContext.Current.Session.Remove(P_Comentarios);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Cargar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Seleccionar_Archivo_Click(object sender, EventArgs e)
        {
            //Declaracion de variables
            Int64 No_Archivo; //Variable para el numero del archivo

            try
            {
                //Obtener el numero del archivo
                No_Archivo = Convert.ToInt64(((ImageButton)sender).CommandArgument);
               //Verificar si el usuario logueado es el mismo que agrego el archivo a la requisicion
                Cls_Ope_Archivos_Ordenes_Compra_Negocio Archivos_Negocio = new Cls_Ope_Archivos_Ordenes_Compra_Negocio();
                Archivos_Negocio.P_No_Archivo = No_Archivo;
                DataTable Dt_Archivo = Archivos_Negocio.Consultar_Datos_Archivo(); //Consultamos los datos del archivo seleccionado
                if (Dt_Archivo != null && Dt_Archivo.Rows.Count > 0)
                {
                    String Usuario_Creo = Dt_Archivo.Rows[0]["Usuario_Creo"].ToString().Trim();
                    String Usuario_Loguedo = Cls_Sessiones.Nombre_Empleado;
                    if (Usuario_Creo.Equals(Cls_Sessiones.Nombre_Empleado.Trim()))
                    {
                        Elimina_Archivo_Tabla(No_Archivo); //Colocar el archivo en la tabla de eliminados
                    }
                    else
                    {
                        Mostrar_Informacion("Lo siento, solo el usuario que agrego el archivo a la orden compra puede eliminar el archivo ", true);
                    }
                }
                else 
                {
                    Elimina_Archivo_Tabla(No_Archivo); //Colocar el archivo en la tabla de eliminados
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Seleccionar_Archivo_Click) " + ex.ToString(), true);
            }
        }

        protected void Txt_Orden_Compra_Busqueda_TextChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Grid_Ordenes_Compras();
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Buscar_Click) " + ex.ToString(), true);
            }
        }
    #endregion
        
}