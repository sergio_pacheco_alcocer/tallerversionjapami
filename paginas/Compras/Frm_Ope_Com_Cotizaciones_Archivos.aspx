﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Cotizaciones_Archivos.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Cotizaciones_Archivos" Culture="es-MX" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
   <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
    <link href="~/paginas/estilos/estilo_masterpage.css" rel="stylesheet" type="text/css" />
    <link href="~/paginas/estilos/estilo_paginas.css" rel="stylesheet" type="text/css" />
    <link href="~/paginas/estilos/estilo_ajax.css" rel="stylesheet" type="text/css" />
    <link href="~/paginas/estilos/TabContainer.css" rel="stylesheet" type="text/css" />
    <link href="~/easyui/themes/smoothness/jquery-ui-1.8.6.custom.css" rel="stylesheet" type="text/css" />

</head>
<body>
    <form id="form1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <!--Div del ecnabezado-->
                <div id="Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="4" class="label_titulo"><asp:Label ID="Lbl_Titulo_Pagina" runat="server"></asp:Label></td>
                        </tr>
                        <tr align="left">
                            <td colspan="4">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle" colspan="2">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png"
                                    CssClass="Img_Button" AlternateText="Modificar" ToolTip="Guardar" 
                                    onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    CssClass="Img_Button" AlternateText="Eliminar" ToolTip="Eliminar" 
                                    
                                    OnClientClick="return confirm('¿Esta seguro de Eliminar los archivos de la cotizaci&oacute;n?');" 
                                    onclick="Btn_Eliminar_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" onclick="Btn_Salir_Click" />
                            </td>
                            <td colspan="2">
                            </td>
                        </tr>
                    </table>
                </div>
                <!--Div del contenido-->
                <div id="Div_Contenido" runat="server">
                    <table style="width:100%;">
                        <tr>
                            <td align="left">Proveedor</td>
                            <td colspan="3"><asp:TextBox ID="Txt_Proveedor" runat="server" Enabled="false" Width="100%"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="3" align="left">
                                <cc1:AsyncFileUpload ID="AFil_Archivo" runat="server" ThrobberID="Lbl_Throbber" 
                                    onuploadedcomplete="AFil_Archivo_UploadedComplete" />
                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                    <div  class="processMessage" id="div_progress">
                                        <center>
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                            <br /><br />
                                            <span id="spanUploading" runat="server" style="color:White;font-size:30px;font-weight:bold;font-family:Lucida Calligraphy;font-style:italic;">
                                                Cargando...
                                            </span>
                                        </center>
                                    </div>
                                </asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Comentarios</td>
                            <td align="left" colspan="3">
                                <asp:TextBox ID="Txt_Comentarios_Archivo" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                    watermarktext="&lt;Límite de Caracteres 500&gt;" watermarkcssclass="watermarked">
                                    </cc1:textboxwatermarkextender>
                                <cc1:filteredtextboxextender id="FTE_Justificacion" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                    filtertype="Custom, UppercaseLetters, LowercaseLetters, Numbers" validchars="ÑñáéíóúÁÉÍÓÚ./*-!$%&()=,[]{}+<>@?¡?¿# ">
                                    </cc1:filteredtextboxextender>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4" style="width:100%;text-align:left;">
                                <center>
                                    <asp:Button ID="Btn_Archivos_Requisicion" runat="server" Text="Aceptar" 
                                        CssClass="button" CausesValidation="false" Width="200px" 
                                        onclick="Btn_Archivos_Requisicion_Click" />
                                </center>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Txt_No_Requisicion" runat="server" />
                                <asp:HiddenField ID="Txt_Proveedor_ID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td colspan="4">
                                <asp:GridView ID="Grid_Archivos_Cotizaciones" runat="server" 
                                    AutoGenerateColumns="false" CssClass="GridView_1" AllowPaging="false" 
                                    PageSize="100" GridLines="None" Width="100%" 
                                    onrowdatabound="Grid_Archivos_Cotizaciones_RowDataBound">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Seleccionar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/delete.png" CommandArgument='<%# Eval("No_Archivo") %>' OnClick="Btn_Seleccionar_Archivo_Click" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="No_Archivo" HeaderText="No Archivo" />
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="Proveedor" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Nombre_Archivo" HeaderText="Archivo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci&oacute;n" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="Proveedor_ID" HeaderText="Proveedor_ID" />
                                        <asp:TemplateField HeaderText="Archivo" ItemStyle-HorizontalAlign="Center" HeaderStyle-HorizontalAlign="Center">
                                            <ItemTemplate>
                                                <asp:HyperLink ID="Hyp_Lnk_Archivo" runat="server" style=""></asp:HyperLink>                                                
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns> 
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>                            
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
            <Triggers>
                <asp:AsyncPostBackTrigger EventName="Click" ControlID="Btn_Archivos_Requisicion" />
            </Triggers>
        </asp:UpdatePanel>
    </div>
    </form>
</body>
</html>
