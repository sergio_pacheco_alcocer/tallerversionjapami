﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Parametros.reportes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Sessiones;
using JAPAMI.Numalet;
using System.Globalization;
using JAPAMI.Ayudante_JQuery;
using Newtonsoft.Json;
using JAPAMI.Parametros_Busqueda.CargaTrabajo;
using JAPAMI.Reportes;
using JAPAMI.Compras.Impresion_Requisiciones.Negocio;
using AjaxControlToolkit;
using JAPAMI.Seguimiento_Requisicion.Negocio;
using JAPAMI.Compras.Manejo_Historial_Cambios.Negocio;


public partial class Sistema_Generales_frm_controlador_crystal : System.Web.UI.Page
{
    string nombre_reporte="";
    ReportDocument objReporte = new ReportDocument();
    string ubicacion;
    DataRow renglon;

    protected void Page_Load(object sender, EventArgs e)
    {
        controlador();
    }

    public void controlador() {

        nombre_reporte = HttpContext.Current.Request["nombre_reporte"] == null ? string.Empty : HttpContext.Current.Request["nombre_reporte"].ToString().Trim();
       
        switch (nombre_reporte) {            
            case "Imprimir_Requisicion":
                Imprimir_Requisicion();
                break;
            case "Consultar_Requisiciones_Bienes":
                Listado_Bienes();
                break; 
            case "Imprimir_Historico_Cambios":
                Imprimir_Historico_Cambios();
                break;

        }
    }

    public String Iniciales()
    {
        String Nombre = Cls_Sessiones.Nombre_Empleado;
        char[] Delimitador = { ' ' };
        string[] Arreglo = Nombre.Split(Delimitador);
        String Final = "";

        for (int conta = 0; conta < Arreglo.Length; conta++)
        {
            if (!String.IsNullOrEmpty(Arreglo[conta].ToString()))
            {
                Final += Arreglo[conta].Substring(0, 1);
            }
        }
        return Final;
    }
    /// *************************************************************************************
    /// NOMBRE              : Imprimir_Requisicion
    /// DESCRIPCIÓN         : Genera reporte de Requisicion.
    /// PARÁMETROS          : 
    /// USUARIO CREO        : Jesus Toledo Rdz.
    /// FECHA CREO          : 01/Junio/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public string Imprimir_Requisicion()
    {
        String Requisicion_ID = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim(); ;
        DataSet Ds_Reporte = null;
        Cls_Ope_Com_Impresion_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Com_Impresion_Requisiciones_Negocio();
        DataTable Dt_Cabecera = new DataTable();
        DataTable Dt_Detalles = new DataTable();
        DataTable Dt_Bienes = new DataTable();
        ReportDocument objReporte = new ReportDocument();
        string ubicacion;
        String No_Requisa = "0";
        CultureInfo culture = new CultureInfo("es-ES");

        if (!String.IsNullOrEmpty(Requisicion_ID))
        {
            if (Requisicion_ID != "<RQ-000000>")
            {
                No_Requisa = Requisicion_ID;
                No_Requisa = No_Requisa.ToUpper();
                No_Requisa = No_Requisa.Replace("RQ-", "");
                int Int_No_Requisa = 0;
                try
                {
                    Int_No_Requisa = int.Parse(No_Requisa);
                }
                catch (Exception Ex)
                {
                    String Str = Ex.ToString();
                    No_Requisa = "0";
                }
                Req_Negocio.P_Requisicion_ID = No_Requisa;
            }

            //Txt_Folio.Text.Replace("RQ-","");
            Dt_Cabecera = Req_Negocio.Consultar_Requisiciones();
            Dt_Detalles = Req_Negocio.Consultar_Requisiciones_Detalles();
            Dt_Bienes = Req_Negocio.Consultar_Requisiciones_Bienes();
            //Ds_Ope_Com_Requisiciones Ds_Requisiciones = new Ds_Ope_Com_Requisiciones();
            //    Generar_Reporte(Dt_Cabecera, Dt_Detalles, Ds_Requisiciones);
            Ds_Reporte = new DataSet();
            //Dt_Requisicion = Req_Negocio.Consultar_Requisiciones();
            Dt_Cabecera.TableName = "REQUISICION";
            Dt_Detalles.TableName = "DETALLES";
            Dt_Bienes.TableName = "BIENES";
            Ds_Reporte.Tables.Add(Dt_Cabecera.Copy());
            Ds_Reporte.Tables.Add(Dt_Detalles.Copy());
            Ds_Reporte.Tables.Add(Dt_Bienes.Copy());

            ubicacion = Server.MapPath("../../Paginas/Rpt/Compras/Rpt_Ope_Com_Requisiciones.rpt");
            objReporte.Load(ubicacion);

            CrystalDecisions.Shared.PageMargins margenes = new CrystalDecisions.Shared.PageMargins();
            margenes.leftMargin = 563;
            margenes.topMargin = 900;

            objReporte.PrintOptions.PaperSize = PaperSize.PaperLetter;
            //objReporte.PrintOptions.ApplyPageMargins(margenes);

            objReporte.SetDataSource(Ds_Reporte);
            Exportar_Reporte_PDF(objReporte, "Requisiciones_Servicio_Bien.pdf");
            Mostrar_Reporte("Requisiciones_Servicio_Bien.pdf");
            //objReporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, Requisicion_ID + "_" + DateTime.Now.ToShortTimeString());
        }
        return "{[]}";
    }
    /// *************************************************************************************
    /// NOMBRE              : Listado_Bienes
    /// DESCRIPCIÓN         : Genera reporte de Listado Bienes.
    /// PARÁMETROS          : 
    /// USUARIO CREO        : Jesus Toledo Rdz.
    /// FECHA CREO          : 01/Junio/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public string Listado_Bienes()
    {
        String Filtro_Bien_Id = HttpContext.Current.Request["Bien_Id"] == null ? string.Empty : HttpContext.Current.Request["Bien_Id"].ToString().Trim();
        DataSet Ds_Reporte = null;
        Cls_Ope_Com_Seguimiento_Requisiciones_Negocio Req_Negocio = new Cls_Ope_Com_Seguimiento_Requisiciones_Negocio();        
        DataTable Dt_Bienes = new DataTable();
        ReportDocument objReporte = new ReportDocument();        
        CultureInfo culture = new CultureInfo("es-ES");
        Req_Negocio.P_Tipo_Articulo = "SERVICIO";
        Req_Negocio.P_Tipo = "TRANSITORIA";
           Req_Negocio.P_Bien_Id = Filtro_Bien_Id.Trim().Substring(0,Filtro_Bien_Id.Trim().Length-3);
            Dt_Bienes = Req_Negocio.Consultar_Bienes_Requisiciones();
            Ds_Reporte = new DataSet();
            Dt_Bienes.TableName = "Dt_Servicio";
            Ds_Reporte.Tables.Add(Dt_Bienes.Copy());

            ubicacion = Server.MapPath("../../Paginas/Rpt/Compras/Rpt_Com_Seguimiento_Servicio_Bien.rpt");
            objReporte.Load(ubicacion);

            CrystalDecisions.Shared.PageMargins margenes = new CrystalDecisions.Shared.PageMargins();
            margenes.leftMargin = 563;
            margenes.topMargin = 900;

            objReporte.PrintOptions.PaperSize = PaperSize.PaperLetter;
            //objReporte.PrintOptions.ApplyPageMargins(margenes);

            objReporte.SetDataSource(Ds_Reporte);
            Exportar_Reporte_PDF(objReporte, "Seguimiento_Servicio_Bien.pdf");
            Mostrar_Reporte("Seguimiento_Servicio_Bien.pdf");
            //objReporte.ExportToHttpResponse(ExportFormatType.PortableDocFormat, Response, true, Requisicion_ID + "_" + DateTime.Now.ToShortTimeString());        
        return "{[]}";
    }
    /// *************************************************************************************
    /// NOMBRE              : Exportar_Reporte_PDF
    /// DESCRIPCIÓN         : Método que guarda el reporte generado en formato PDF en la ruta
    ///                       especificada.
    /// PARÁMETROS          : Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                       Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO        : Juan Alberto Hernández Negrete.
    /// FECHA CREO          : 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE              : Listado_Bienes
    /// DESCRIPCIÓN         : Genera reporte de Listado Bienes.
    /// PARÁMETROS          : 
    /// USUARIO CREO        : Jesus Toledo Rdz.
    /// FECHA CREO          : 01/Junio/2013
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public string Imprimir_Historico_Cambios()
    {
        String Filtro_No_RQ = HttpContext.Current.Request["Folio_Rq"] == null ? string.Empty : HttpContext.Current.Request["Folio_Rq"].ToString().Trim();
        DataSet Ds_Reporte = null;
        Cls_Ope_Com_Manejo_Historial_Cambios_Negocio Req_Negocio = new Cls_Ope_Com_Manejo_Historial_Cambios_Negocio();
        DataTable Dt_Resultados = new DataTable();
        ReportDocument objReporte = new ReportDocument();
        CultureInfo culture = new CultureInfo("es-ES");
        Req_Negocio.P_No_Requisicion = Filtro_No_RQ.Replace("RQ", "").Replace("-", "").Trim();
        Dt_Resultados = Req_Negocio.Consultar_Cambios_Bien();
        Ds_Reporte = new DataSet();
        Dt_Resultados.TableName = "DT_DATOS";
        Ds_Reporte.Tables.Add(Dt_Resultados.Copy());

        ubicacion = Server.MapPath("../../Paginas/Rpt/Compras/Rpt_Com_Historial_Cambios.rpt");
        objReporte.Load(ubicacion);

        CrystalDecisions.Shared.PageMargins margenes = new CrystalDecisions.Shared.PageMargins();
        margenes.leftMargin = 563;
        margenes.topMargin = 900;

        objReporte.PrintOptions.PaperSize = PaperSize.PaperLetter;

        objReporte.SetDataSource(Ds_Reporte);
        Exportar_Reporte_PDF(objReporte, "Rpt_Com_Historial_Cambios.pdf");
        Mostrar_Reporte("Rpt_Com_Historial_Cambios.pdf");
        return "{[]}";
    }

}
