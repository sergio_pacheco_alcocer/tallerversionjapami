﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Com_Entrada_Facturas_Servicios_Generales.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Com_Entrada_Facturas_Servicios_Generales" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script language="javascript" type="text/javascript">
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
        function Limpiar_Ctlr_Busqueda() {
            document.getElementById("<%=Txt_Fecha_Inicio.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Fin.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Orden_Compra.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Proveedores.ClientID%>").value = "";
            return false;
        }
        function Recalcular_Total_Factura() {
            var importe = document.getElementById("<%=Txt_Importe_Factura.ClientID %>").value.replace('$', '').replace(',', '');
            var iva = document.getElementById("<%=Txt_IVA_Factura.ClientID %>").value.replace('$', '').replace(',', '');
            if (importe.lenght == 0) importe = 0;
            if (iva.lenght == 0) iva = 0;
            importe = parseFloat(importe);
            iva = parseFloat(iva);
            var total = importe + iva;
            document.getElementById("<%=Txt_Total_Factura.ClientID%>").value = total;
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Loading" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Area_Trabajo" style="background-color: #ffffff; width: 100%; height: 100%;">
                <center>
                    <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">
                                Recepción de Facturas
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                    <table style="width: 100%;">
                                        <tr>
                                            <td colspan="2" align="left">
                                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                    Width="24px" Height="24px" />
                                                <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="Es necesario:" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 10%;">
                                            </td>
                                            <td style="width: 90%; text-align: left;" valign="top">
                                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" Style="font-size: 12px;
                                                    color: Red; font-family: Tahoma; text-align: left;" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                                <asp:ImageButton ID="Btn_Guardar_Facturas" runat="server" CssClass="Img_Button" OnClick="Btn_Guardar_Facturas_Click"
                                    ToolTip="Guardar Facturas" ImageUrl="~/paginas/imagenes/paginas/icono_guardar.png" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
            <div id="Div_Busqueda" runat="server">
                <table width="99%">
                    <tr>
                        <td align="left" style="width: 16%;">
                            <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor" />
                        </td>
                        <td colspan="3" align="left" style="width: 84%;">
                            <asp:DropDownList ID="Cmb_Proveedores" runat="server" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 16%;">
                            <asp:Label ID="Lbl_Fecha" runat="server" Text="Fecha" />
                        </td>
                        <td align="left" style="width: 34%;">
                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="100px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Inicial" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Inicio_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicio"
                                OnClientShown="calendarShown" PopupButtonID="Btn_Fecha_Inicio" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                            &nbsp;&nbsp;
                            <asp:TextBox ID="Txt_Fecha_Fin" runat="server" Width="100px" Enabled="False" />
                            <asp:ImageButton ID="Btn_Fecha_Fin" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                            <cc1:CalendarExtender ID="Btn_Fecha_Fin_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Fin"
                                PopupButtonID="Btn_Fecha_Fin" OnClientShown="calendarShown" Format="dd/MMM/yyyy">
                            </cc1:CalendarExtender>
                        </td>
                        <td align="left" style="width: 16%;">
                            <asp:Label ID="Lbl_No_Orden_Compra" runat="server" Text="No. Orden Servicio" />
                        </td>
                        <td align="left" style="width: 34%;">
                            <asp:TextBox ID="Txt_Busqueda_Orden_Compra" runat="server" MaxLength="10" Width="99%" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Orden_Compra" runat="server" TargetControlID="Txt_Busqueda_Orden_Compra"
                                InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Orden_Compra" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<No. Orden Servicio>" TargetControlID="Txt_Busqueda_Orden_Compra" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                            <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Buscar" OnClick="Btn_Buscar_Click" />
                            &nbsp;<asp:ImageButton ID="Btn_Limpiar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                OnClientClick="javascript:return Limpiar_Ctlr_Busqueda();" ToolTip="Limpiar"
                                Width="20px" />
                        </td>
                    </tr>
                </table>
                <asp:GridView ID="Grid_Ordenes_Compra" runat="server" AutoGenerateColumns="False"
                    OnPageIndexChanging="Grid_Ordenes_Compra_PageIndexChanging" OnSelectedIndexChanged="Grid_Ordenes_Compra_SelectedIndexChanged"
                    CssClass="GridView_1" GridLines="None" PageSize="20" Width="99%" AllowPaging="true">
                    <RowStyle CssClass="GridItem" Font-Size="Larger" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                            Text="Ver" />
                        <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="NO_ORDEN_COMPRA">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FOLIO" HeaderText="Folio">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PROVEEDOR" HeaderText="Proveedor">
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="FECHA" DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-HorizontalAlign="Left"
                            HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TOTAL" HeaderText="Total" DataFormatString="{0:c}">
                            <HeaderStyle HorizontalAlign="Right" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ESTATUS" HeaderStyle-HorizontalAlign="Left" HeaderText="Estatus"
                            ItemStyle-HorizontalAlign="Center">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Font-Size="X-Small" />
                        </asp:BoundField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <div id="Div_Orden_Compra" runat="server" style="height: 900px;">
                <table style="width: 99%;" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_No_Orden_Servicio" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            Folio OS
                        </td>
                        <td align="left" style="width: 35%;">
                            <asp:TextBox ID="Txt_Folio" runat="server" Width="100%" ReadOnly="true" />
                        </td>
                        <td align="left">
                            &nbsp;Total
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Total_Cotizado" runat="server" ReadOnly="true" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;" align="left">
                            Fecha
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Construccion" runat="server" Width="100%" ReadOnly="true" />
                        </td>
                        <td align="left">
                            &nbsp;Folio RQ
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Requisicion" runat="server" ReadOnly="true" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            Elabor&oacute;
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Elaboro" runat="server" ReadOnly="true" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            U. Responsable
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Dependencia" runat="server" ReadOnly="true" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            Proveedor
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Proveedor" runat="server" Width="100%" ReadOnly="true" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="width: 15%;">
                            Justificaci&oacute;n
                        </td>
                        <td align="left" colspan="3">
                            <asp:TextBox ID="Txt_Justificacion" runat="server" Width="100%" ReadOnly="true" Height="50px"
                                TextMode="MultiLine" />
                        </td>
                    </tr>
                </table>
                <br />
                <table style="width: 99%;" class="estilo_fuente">
                    <tr class="barra_busqueda" align="right">
                        <td align="center" style="background-color: #C0C0C0; color: Black;" colspan="6">
                            REGISTRO DE FACTURAS
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            No. Factura
                        </td>
                        <td style="width: 23.3%;">
                            <asp:TextBox ID="Txt_No_Factura" runat="server" Width="99%"></asp:TextBox>
                        </td>
                        <td style="width: 10%;">
                            &nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            &nbsp;
                        </td>
                        <td style="width: 10%; text-align: right;">
                            Fecha&nbsp;&nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            <asp:TextBox ID="Txt_Fecha_Factura" runat="server" Enabled="false" Width="85%"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Factura" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha" />
                            <cc1:CalendarExtender ID="CE_Fecha_Factura" runat="server" Format="dd/MMM/yyyy" OnClientShown="calendarShown"
                                PopupButtonID="Btn_Fecha_Factura" TargetControlID="Txt_Fecha_Factura">
                            </cc1:CalendarExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            Importe
                        </td>
                        <td style="width: 23.3%;">
                            <asp:TextBox ID="Txt_Importe_Factura" runat="server" Width="99%" onkeyup="javascript:return Recalcular_Total_Factura();"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Importe_Factura" runat="server" TargetControlID="Txt_Importe_Factura"
                                FilterType="Numbers, Custom" ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 10%; text-align: right;">
                            IVA &nbsp;&nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            <asp:TextBox ID="Txt_IVA_Factura" runat="server" Width="99%" onkeyup="javascript:return Recalcular_Total_Factura();"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_IVA_Factura" runat="server" TargetControlID="Txt_IVA_Factura"
                                FilterType="Numbers, Custom" ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 10%; text-align: right;">
                            Total&nbsp;&nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            <asp:TextBox ID="Txt_Total_Factura" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Total_Factura" runat="server" TargetControlID="Txt_Total_Factura"
                                FilterType="Numbers, Custom" ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 10%;">
                            &nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            &nbsp;
                        </td>
                        <td style="width: 10%; text-align: right;">
                            &nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            &nbsp;
                        </td>
                        <td style="width: 10%; text-align: right;">
                            &nbsp;
                        </td>
                        <td style="width: 23.3%;">
                            <asp:Button ID="Btn_Agregar_Factura" runat="server" Text="Agregar" CssClass="button"
                                OnClick="Btn_Agregar_Factura_Click" Width="100%" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <asp:GridView ID="Grid_Facturas" runat="server" AutoGenerateColumns="False" OnRowDataBound="Grid_Facturas_RowDataBound"
                                CssClass="GridView_1" GridLines="None" Width="99%">
                                <RowStyle CssClass="GridItem" Font-Size="Larger" />
                                <Columns>
                                    <asp:TemplateField HeaderText="">
                                        <ItemStyle Width="40px" HorizontalAlign="Center" />
                                        <ItemTemplate>
                                            <asp:ImageButton ID="Btn_Quitar_Factura" runat="server" ToolTip="Eliminar Anexo"
                                                AlternateText="Quitar Factura" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                OnClick="Btn_Quitar_Factura_Click" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="NUMERO_FACTURA" HeaderText="Factura">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="FECHA_FACTURA" DataFormatString="{0:dd/MMM/yyyy}" HeaderStyle-HorizontalAlign="Left"
                                        HeaderText="Fecha" ItemStyle-HorizontalAlign="Center">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IMPORTE_FACTURA" HeaderText="Importe" DataFormatString="{0:c}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="IVA_FACTURA" HeaderText="Iva" DataFormatString="{0:c}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TOTAL_FACTURA" HeaderText="Total" DataFormatString="{0:c}">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
