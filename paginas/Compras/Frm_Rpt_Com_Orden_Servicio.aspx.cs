﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Rpt_Orden_Compra.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Compras_Frm_Rpt_Com_Orden_Servicio : System.Web.UI.Page
{

    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Page_Load
    ///PARAMETROS:  
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 06/jun/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado) || String.IsNullOrEmpty(Cls_Sessiones.No_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack) {
            Llenar_Combo_Dependencias();
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
    ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias()
    {
        Cls_Com_Orden_Compra_Negocio Negocio = new Cls_Com_Orden_Compra_Negocio();
        Negocio.P_Estatus = "ACTIVO";
        DataTable Dependencias = Negocio.Consultar_Dependencias();
        Cmb_Unidad_Responsable.DataSource = Dependencias;//Se especifica la fuente de información que llenará el combo
        Cmb_Unidad_Responsable.DataValueField = "DEPENDENCIA_ID";//Se especifica el campo de la tabla dependencias que contendrá el valor de los renglones del combo
        Cmb_Unidad_Responsable.DataTextField = "NOMBRE";//Se especifica el campo de la tabla dependencias que contendrá el texto que mostrará el combo
        Cmb_Unidad_Responsable.DataBind();//Se unen los datos del combo
        Cmb_Unidad_Responsable.Items.Insert(0, new ListItem("<- TODAS ->", ""));//Se inserta el renglón de <-SELECCIONE-> en la posición 0

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Nombre_PDF)
    {

        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_PDF);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        String Ruta = "../../Reporte/" + Nombre_PDF;
        //Mostrar_Reporte(Nombre_PDF, "PDF");
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Exportar_Excel
    ///DESCRIPCIÓN: caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.- Data_Set.- contiene la Mostrar_Informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene la Ruta del Reporte a mostrar en pantalla
    ///             4.- Nombre_Xls, nombre con el que se geradara en disco el archivo xls
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 17/MAYO/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Exportar_Excel(DataSet Data_Set, DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Xls)
    {

        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set;
        Reporte.SetDataSource(Ds_Reporte);
        //1
        ExportOptions Export_Options = new ExportOptions();
        //2
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        //3
        //4
        Disk_File_Destination_Options.DiskFileName = Server.MapPath("../../Reporte/" + Nombre_Xls);
        //5
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        //6
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        //7
        Export_Options.ExportFormatType = ExportFormatType.Excel;
        //8
        Reporte.Export(Export_Options);
        //9
        String Ruta = "../../Reporte/" + Nombre_Xls;

        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Proveedores
    ///DESCRIPCIÓN:      Llena el Grid de los Proveedores para que el usuario lo seleccione
    ///PARAMETROS:       Pagina. Pagina del Grid que se mostrará.     
    ///CREO:             Salvador Hernández Ramírez
    ///FECHA_CREO:       08/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Proveedores(Int32 Pagina)
    {
        Grid_Listado_Proveedores.SelectedIndex = (-1);
        Grid_Listado_Proveedores.Columns[1].Visible = true;
        Cls_Cat_Com_Proveedores_Negocio Proveedores_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        if (Txt_Nombre_Proveedor_Buscar.Text.Trim() != "")
        {
            Proveedores_Negocio.P_Busqueda = Txt_Nombre_Proveedor_Buscar.Text.Trim();
        }
        DataTable Dt_Proveedores = Proveedores_Negocio.Consulta_Datos_Proveedores();
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Proveedor_ID].ColumnName = "PROVEEDOR_ID";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Nombre].ColumnName = "NOMBRE";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_RFC].ColumnName = "RFC";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Compañia].ColumnName = "COMPANIA";
        Grid_Listado_Proveedores.DataSource = Dt_Proveedores;
        Grid_Listado_Proveedores.PageIndex = Pagina;
        Grid_Listado_Proveedores.DataBind();
        Grid_Listado_Proveedores.Columns[1].Visible = false;
    }

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Productos del
    ///             Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Julio/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Proveedores.SelectedIndex > (-1))
            {
                String Proveedor_ID = Grid_Listado_Proveedores.SelectedRow.Cells[1].Text.Trim();

                Cls_Cat_Com_Proveedores_Negocio Proveedor_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                Proveedor_Negocio.P_Proveedor_ID = Proveedor_ID;
                DataTable Dt_Proveedor_Seleccionado = Proveedor_Negocio.Consulta_Datos_Proveedores();
                if (Dt_Proveedor_Seleccionado != null && Dt_Proveedor_Seleccionado.Rows.Count > 0)
                {
                    Hdf_Proveedor_ID.Value = Proveedor_ID.Trim();

                    Txt_Proveedor.Text = Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim() + " / " + Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString().Trim();

                    Mpe_Proveedores_Cabecera.Hide();
                }
                System.Threading.Thread.Sleep(500);
                Grid_Listado_Proveedores.SelectedIndex = (-1);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Proveedores_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Proveedores del Modal de Busqueda
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Proveedores.SelectedIndex = (-1);
            Llenar_Grid_Proveedores(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    
    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN: Btn_Generar_Reporte_Click
    ///PARAMETROS: 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 17/MAYO/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_Click(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Cls_Com_Orden_Compra_Negocio Orden_Compra_Negocio = new Cls_Com_Orden_Compra_Negocio();
        Orden_Compra_Negocio.P_Tipo_Articulo = "SERVICIO";
        if (Txt_Fecha_Inicial.Text.Trim().Length > 0) Orden_Compra_Negocio.P_Filtro_Fecha_Inicial = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
        if (Txt_Fecha_Final.Text.Trim().Length > 0) Orden_Compra_Negocio.P_Filtro_Fecha_Final = Convert.ToDateTime(Txt_Fecha_Final.Text);
        if (Cmb_Unidad_Responsable.SelectedIndex > 0) Orden_Compra_Negocio.P_Dependencia_ID = Cmb_Unidad_Responsable.SelectedItem.Value;
        if (Hdf_Proveedor_ID.Value.Trim().Length > 0) Orden_Compra_Negocio.P_Proveedor_ID = Hdf_Proveedor_ID.Value.Trim();
        Ds_Com_Orden_Servicio Ds_Obj_Orden_Compra = new Ds_Com_Orden_Servicio();
        DataTable Ds_Orden_Compra = Orden_Compra_Negocio.Consultar_Ordenes_Servicio();
        Ds_Orden_Compra.TableName = "DataTable1";
        DataSet Ds_Datos = new DataSet();
        Ds_Datos.Tables.Add(Ds_Orden_Compra.Copy());
        Generar_Reporte(Ds_Datos, Ds_Obj_Orden_Compra, "Rpt_Com_Orden_Servicio.rpt", "Rpt_Com_Orden_Servicio" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Today) + ".pdf");

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Btn_Salir_Click
    ///PARAMETROS:  
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 17/MAYO/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Excel_Click
    ///DESCRIPCIÓN:Btn_Exportar_Excel_Click
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 17/MAYO/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Exportar_Excel_Click(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Cls_Com_Orden_Compra_Negocio Orden_Compra_Negocio = new Cls_Com_Orden_Compra_Negocio();
        Orden_Compra_Negocio.P_Tipo_Articulo = "SERVICIO";
        Ds_Com_Orden_Servicio Ds_Obj_Orden_Compra = new Ds_Com_Orden_Servicio();
        DataTable Ds_Orden_Compra = Orden_Compra_Negocio.Consultar_Ordenes_Servicio();
        Ds_Orden_Compra.TableName = "DataTable1";
        DataSet Ds_Datos = new DataSet();
        Ds_Datos.Tables.Add(Ds_Orden_Compra.Copy());
        Exportar_Excel(Ds_Datos, Ds_Obj_Orden_Compra, "Rpt_Com_Orden_Servicio.rpt", "Rpt_Com_Orden_Servicio" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Today) + ".xls");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_Proveedores_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda de los proveedores.
    ///PARAMETROS:     
    ///CREO:        Salvador Hernández Ramírez
    ///FECHA_CREO:  22/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ejecutar_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Llenar_Grid_Proveedores(0);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Proveedor_Click
    ///DESCRIPCIÓN: Lanza buscador de proveedor.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 18/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Proveedor_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            Mpe_Proveedores_Cabecera.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

}