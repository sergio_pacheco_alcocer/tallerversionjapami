﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Rpt_Com_Pedidos_Proproductos.Negocios;
using CarlosAg.ExcelXmlWriter;
public partial class paginas_Compras_Frm_Rpt_Com_Pedidos_Productos : System.Web.UI.Page
{
    #region PAGE LOAD
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Mensaje_Error();
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
           
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    #endregion

    #region METODOS
    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Toledo Rodriguez Jesus S.
    ///FECHA_CREO  : 04-Septiembre-2010
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text += P_Mensaje + "</br>";
        Lbl_Mensaje_Error.Visible = true;
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Llenar_Combo_Unidad_Responsable();
            Llenar_Combo_Partida();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Lenar_Combo_Unidad_Responsable
    /// DESCRIPCION : Llena el como Cmb_Unidad_Responasble
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 16-Octubre-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Llenar_Combo_Unidad_Responsable() 
    {
        try
        {
            Cls_Rpt_Com_Pedidos_Productos_Negocio Pedidos_Product_Negocio = new Cls_Rpt_Com_Pedidos_Productos_Negocio();
            DataTable Dt_Unidad_Responsabe = new DataTable();
            Pedidos_Product_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Dt_Unidad_Responsabe = Pedidos_Product_Negocio.Consulta_Unidad_Responsable();

            Cmb_Unidad_Responable.Items.Clear();
            Cmb_Unidad_Responable.DataSource = Dt_Unidad_Responsabe;
            Cmb_Unidad_Responable.DataTextField = "DEPENDENCIA";
            Cmb_Unidad_Responable.DataValueField = "DEPENDENCIA_ID";
            Cmb_Unidad_Responable.DataBind();

            Cmb_Unidad_Responable.SelectedIndex=(Cmb_Unidad_Responable.Items.IndexOf(Cmb_Unidad_Responable.Items.FindByValue(Cls_Sessiones.Dependencia_ID_Empleado))); //Selecciona la unidad resonsable del usuario logeado
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo Unidad Responsable :" + ex.Message.ToString());
        }
    }
   
   
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combo_Partida
    /// DESCRIPCION : Llena el como Cmb_Partida_Especifica
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 16-Octubre-2012 06:59 pm
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Llenar_Combo_Partida()
    {
        try
        {
            Cls_Rpt_Com_Pedidos_Productos_Negocio Pedidos_Product_Negocio = new Cls_Rpt_Com_Pedidos_Productos_Negocio();
            DataTable Dt_Partida_Especifica = new DataTable();
            Pedidos_Product_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Dt_Partida_Especifica = Pedidos_Product_Negocio.Consulta_Partidas_Presupuestadas();
            LLenar_Combo(Cmb_Partida_Especifica, Dt_Partida_Especifica, Cat_Sap_Partidas_Especificas.Campo_Partida_ID, "PARTIDA");
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo Partida_Especifica :" + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combo_Producto
    /// DESCRIPCION : Llena el como Cmb_Productos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 17-Octubre-2012 09:19 am
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void Llenar_Combo_Producto()
    {
        try
        {
            Cls_Rpt_Com_Pedidos_Productos_Negocio Pedidos_Product_Negocio = new Cls_Rpt_Com_Pedidos_Productos_Negocio();
            DataTable Dt_Productos = new DataTable();
            Pedidos_Product_Negocio.P_Partida_Especifica_ID = Cmb_Partida_Especifica.SelectedValue;
            Dt_Productos = Pedidos_Product_Negocio.Consulta_Productos_Por_Partida();
            LLenar_Combo(Cmb_Productos, Dt_Productos, Cat_Com_Productos.Campo_Producto_ID, "PRODUCTO");
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo Producto :" + ex.Message.ToString() );
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Combo
    /// DESCRIPCION : Llena el combo con los datos recibidos
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012 01:09
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Combo(DropDownList Combo, DataTable Dt_Datos, String Value, String Text)
    {
        try
        {
            Combo.Items.Clear();
            Combo.DataSource = Dt_Datos;
            Combo.DataTextField = Text;
            Combo.DataValueField = Value;
            Combo.DataBind();
            Combo.Items.Insert(0, new ListItem("<-Seleccione->", ""));
            Combo.SelectedIndex = -1;
        }
        catch (Exception ex)
        {
            throw new Exception("Error al cargar el combo :" + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: LLenar_Grid_Reporte
    /// DESCRIPCION : Llena el gridview con los datos obtenidos
    ///               seleccionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 17-Octubre-2012 04:32
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public void LLenar_Grid_Reporte()
    {
        try
        {
            DataTable Dt_Productos_Pedidos = new DataTable();
            Dt_Productos_Pedidos = Consultar_Pedidos_Productos();
            if (Dt_Productos_Pedidos != null && Dt_Productos_Pedidos.Rows.Count > 0)
            {
                Grid_Reporte.DataSource = Dt_Productos_Pedidos;
                Grid_Reporte.DataBind();
            }
            else 
            {
                Grid_Reporte.DataSource = new DataTable();
                Grid_Reporte.DataBind();
                Mensaje_Error("No se encontraron registros");
            }

        }
        catch (Exception Ex)
        {
            throw new Exception("Error Llenar la Gridview" + Ex.Message );
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Pedidos_Productos
    /// DESCRIPCION : Consulta los productos pedidos de aacuerdo a los filtros 
    ///               seleccionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 09-Octubre-2012 04:15
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public DataTable Consultar_Pedidos_Productos() 
    {
        DataTable Dt_Productos_Pedidos = new DataTable();
        try
        {
            Cls_Rpt_Com_Pedidos_Productos_Negocio Pedidos_Negocio = new Cls_Rpt_Com_Pedidos_Productos_Negocio();
            Pedidos_Negocio.P_Dependencia_ID = Cmb_Unidad_Responable.SelectedValue.ToString();
            if (Cmb_Partida_Especifica.SelectedIndex > 0) 
            {
                Pedidos_Negocio.P_Partida_Especifica_ID = Cmb_Partida_Especifica.SelectedValue.ToString().Trim();
            }
            if (Cmb_Productos.SelectedIndex > 0)
            {
                Pedidos_Negocio.P_Producto_ID = Cmb_Productos.SelectedValue.ToString().Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text)) 
            {
                Pedidos_Negocio.P_Fecha_Inicio = Txt_Fecha_Inicial.Text.Trim();
            }
            if (!String.IsNullOrEmpty(Txt_Fecha_Final.Text))
            {
                Pedidos_Negocio.P_Fecha_Fin = Txt_Fecha_Final.Text.Trim();
            }
            Dt_Productos_Pedidos = Pedidos_Negocio.Consulta_Pedido_Productos();
        }
        catch (Exception Ex) 
        {
            throw new Exception("Error al Consultar el Pedido" + Ex.Message);
        }
        return Dt_Productos_Pedidos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Fecha
    /// DESCRIPCION : Valida que se elija correctamente el rango de fecha, es decir 
    ///               que la fecha inicio sea menor a la fecha final
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 18-Octubre-2012 10:15
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Fechas() 
    {
        Boolean Validacion = true;
        try 
        {
            if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) || !String.IsNullOrEmpty(Txt_Fecha_Final.Text)) 
            {
                if (!String.IsNullOrEmpty(Txt_Fecha_Inicial.Text) && !String.IsNullOrEmpty(Txt_Fecha_Final.Text))
                {
                    //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
                    DateTime Date1 = new DateTime();
                    DateTime Date2 = new DateTime();
                    //Convertimos el Texto de los TextBox fecha a dateTime
                    Date1 = DateTime.Parse(Txt_Fecha_Inicial.Text);
                    Date2 = DateTime.Parse(Txt_Fecha_Final.Text);
                    if (Date1 > Date2)
                    {
                        Validacion = false;
                        Mensaje_Error("La fecha inicial no puede ser mayor a la fecha final  ");
                    }
                }
                else 
                {
                    Mensaje_Error("Favor de introducir ambas fechas ");
                }
            }
        }
        catch(Exception Ex)
        {
            throw new Exception("Error a validar las fechas " + Ex.Message);
        }
        return Validacion;
    }
    ///*****************************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Generar
    ///DESCRIPCIÓN          : Metodo generar el reporte analitico de ingresos
    ///PARAMETROS           1 Dt_Datos: datos del pronostico de ingresos del reporte que se pasaran en excel  
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 24/Marzo/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*****************************************************************************************************************
    public void Generar_Reporte_Excel(DataTable Dt_Reporte)
    {
        WorksheetCell Celda = new WorksheetCell();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Tipo_Estilo = "";
        String Informacion_Registro = "";
        Int32 Contador_Estilo = 0;
        Int32 Operacion = 0;
        try
        {
            
            Mensaje_Error();
            Nombre_Archivo = "Rpt_Almacen_Salidas.xls"; //Nombre que se le dara al reporte de excel;
            Ruta_Archivo = @Server.MapPath("../../Archivos/" + Nombre_Archivo); //lugar enn donde se almacenara el reporte

            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Reporte_Almacen";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
            //  Creamos el estilo cabecera 3 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera3 = Libro.Styles.Add("HeaderStyle3");
            //Creamos el estilo fecha para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Fecha = Libro.Styles.Add("HeaderStyleFecha");
            //  Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //  Creamos el estilo contenido2 del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");


            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera
            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 11;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.WrapText = true;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#0066CC";
            Estilo_Cabecera.Interior.Color = "#CCCCFF";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera2
            Estilo_Cabecera2.Font.FontName = "Tahoma";
            Estilo_Cabecera2.Font.Size = 10;
            Estilo_Cabecera2.Font.Bold = true;
            Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera2.Alignment.Rotate = 0;
            Estilo_Cabecera2.Font.Color = "#0066CC";
            Estilo_Cabecera2.Interior.Color = "#FFFFFF";
            Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la HeaderStyle3
            Estilo_Cabecera3.Font.FontName = "Tahoma";
            Estilo_Cabecera3.Font.Size = 10;
            Estilo_Cabecera3.Font.Bold = true;
            Estilo_Cabecera3.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera3.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera3.Alignment.Rotate = 0;
            Estilo_Cabecera3.Font.Color = "#003399";
            Estilo_Cabecera3.Interior.Color = "LightGray";
            Estilo_Cabecera3.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera3.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera3.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la fecha
            Estilo_Fecha.Font.FontName = "Tahoma";
            Estilo_Fecha.Font.Size = 10;
            Estilo_Fecha.Font.Bold = false;
            Estilo_Fecha.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Fecha.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Fecha.Alignment.Rotate = 0;
            Estilo_Fecha.Font.Color = "#003399";
            Estilo_Fecha.Interior.Color = "#FFFFFF";
            Estilo_Fecha.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Fecha.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Fecha.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle
            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido.Alignment.Rotate = 0;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle2
            Estilo_Contenido2.Font.FontName = "Tahoma";
            Estilo_Contenido2.Font.Size = 9;
            Estilo_Contenido2.Font.Bold = false;
            Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido2.Alignment.Rotate = 0;
            Estilo_Contenido2.Font.Color = "#000000";
            Estilo_Contenido2.Interior.Color = "#FAF0E6";
            Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //*************************************** fin de los estilos***********************************************************

            //***************************************Inicio del reporte Proveedores por partida Hoja 1***************************

            //  Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("REPORTE DE PEDIDOS");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  1 Folio
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(130));// 2 Partida
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));// 3 Producto
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(60));//  4 Cantidad
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  5 Total Gastado
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  6 No_Salida
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  7 Fecha
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));// 8 Recibio

                //  se llena el encabezado principal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO ");
                Celda.MergeAcross = 7; // Merge 6 cells together
                Celda.MergeDown = 1;
                Celda.StyleID = "HeaderStyle";
                Renglon = Hoja.Table.Rows.Add();

                //Se llena el segundo encabezado
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("Reporte de compras de la unidad responsable  " + Cmb_Unidad_Responable.SelectedItem.ToString());
                Celda.MergeAcross = 7; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle2";

                //Renglon con la fecha Acutal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add(DateTime.Now.ToString());
                Celda.MergeAcross = 7; // Merge 6 cells together
                Celda.StyleID = "HeaderStyleFecha";

                //  para los titulos de las columnas
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("FOLIO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";
                            
                Celda = Renglon.Cells.Add("PARTIDA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("PRODUCTO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("CANTIDAD");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("$TOTAL GASTADO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("NO SALIDA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("FECHA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";

                Celda = Renglon.Cells.Add("RECIBIO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle3";
            
                Renglon = Hoja.Table.Rows.Add();

            //  para llenar el reporte
            if (Dt_Reporte.Rows.Count > 0)
            {
                //  Se comienza a extraer la informaicon de la onsulta
                foreach (DataRow Renglon_Reporte in Dt_Reporte.Rows)
                {
                    Contador_Estilo++;
                    Operacion = Contador_Estilo % 2;
                    if (Operacion == 0)
                    {
                        //Estilo_Concepto.Interior.Color = "LightGray";
                        Tipo_Estilo = "BodyStyle2";
                    }
                    else
                    {
                        Tipo_Estilo = "BodyStyle";
                        //Estilo_Concepto.Interior.Color = "White";
                    }

                    Renglon = Hoja.Table.Rows.Add();
                    foreach (DataColumn Column in Dt_Reporte.Columns)
                    {
                            Informacion_Registro = (Renglon_Reporte[Column.ColumnName].ToString());
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                    }
                }
            }

            //***************************************Fin del reporte Proveedores por partida Hoja 1***************************

            //  se guarda el documento
            Libro.Save(Ruta_Archivo);
            //  mostrar el archivo
            Mostrar_Reporte(Nombre_Archivo);

            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            //Response.ContentEncoding = Encoding.Default;
            //Libro.Save(Response.OutputStream);
            //Response.End();
        }// fin try
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    /// USUARIO CREO:        Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:          18/Enero/2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Ruta = "../../Archivos/" + Nombre_Reporte_Generar;
        try
        {
            Mensaje_Error();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=100,height=100')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    #region EVENTOS
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Unidad_Responable_SelectedIndexChanged
    /// DESCRIPCION : Evento onchange del combo
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 17-Octubre-2012 09:32
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Unidad_Responable_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_Responable.SelectedIndex > 0)
        {
            Llenar_Combo_Partida();
        }
        else 
        {
            Cmb_Partida_Especifica.Items.Clear();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Partida_Especifica_SelectedIndexChanged
    /// DESCRIPCION : Evento onchange del combo
    /// PARAMETROS  : 
    /// CREO        : Jennyfer Ivonne Ceja Lemus
    /// FECHA_CREO  : 17-Octubre-2012 09:33
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Partida_Especifica_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Unidad_Responable.SelectedIndex > 0)
        {
            Llenar_Combo_Producto();
        }
        else
        {
            Cmb_Productos.Items.Clear();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN:Metodo que permite salor de la pagina y situarse en la pagina de inicio
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 09/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Conultar_Click
    ///DESCRIPCIÓN:Metodo que permite salir de la pagina y situarse en la pagina de inicio
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 09/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Conultar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Mensaje_Error();
            if (Validar_Fechas())
            {
                LLenar_Grid_Reporte();
            }
            
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo Producto :" + ex.Message.ToString() );
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Excel_Click
    ///DESCRIPCIÓN:Metodo que permite generar el excel
    ///CREO: Jennyer Ivonne Ceja Lemus
    ///FECHA_CREO: 18/Octubre/2012 10:12
    ///MODIFICO:
    ///FECHA_MODIFICO: 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Exportar_Excel_Click(object sender, ImageClickEventArgs e)
    { 
        try
        {
            DataTable Dt_Productos_Pedidos = null;
            Mensaje_Error();
            if (Validar_Fechas())
            {
                Dt_Productos_Pedidos = Consultar_Pedidos_Productos();
                if (Dt_Productos_Pedidos != null && Dt_Productos_Pedidos.Rows.Count > 0)
                {
                    Generar_Reporte_Excel(Dt_Productos_Pedidos);
                }
                else 
                {
                    Mensaje_Error("No se encontraron registros ");
                }
            }
            
        }
        catch (Exception ex)
        {
            Mensaje_Error("Error al cargar el combo Producto :" + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION:    Btn_Limpiar_Click
    ///DESCRIPCION:             Evento utilizado para asignar en su estatus inicial los componentes de la búsqueda 
    ///PARAMETROS:              
    ///CREO:                    Salvador Hernández Ramírez
    ///FECHA_CREO:              08/Agosto/2011 
    ///MODIFICO:                
    ///FECHA_MODIFICO:          
    ///CAUSA_MODIFICACION:      
    ///*******************************************************************************
    protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
    {
        Cmb_Unidad_Responable.SelectedIndex = 0;
        Cmb_Partida_Especifica.SelectedIndex = 0;
        Cmb_Productos.SelectedIndex = 0;
        Txt_Fecha_Inicial.Text = String.Empty;
        Txt_Fecha_Final.Text = String.Empty;
    }
    #endregion


    
}
