﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data.OleDb;


public partial class paginas_Compras_Migracion_Datos_Servicios_Generales : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataSet Leer_Excel(String sqlExcel, String Archivo)
    {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        String Rta = @MapPath("../../Archivos/" + Archivo);
        String sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" + "Data Source=" + Rta + ";Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        oledbConn.Close();
        return DS;
    }
    
    
    protected void Btn_Migracion_Proveedores_Click(object sender, EventArgs e)
    {
        try 
        {
            DataSet Ds_Datos = new DataSet();
            String SqlExcel = "Select * From [TALLER$]";
            Ds_Datos = Leer_Excel(SqlExcel, "DATOS_PROVEEDORES.xlsx");
            DataTable Dt_Datos = Ds_Datos.Tables[0];
            foreach (DataRow Fila_Actual in Dt_Datos.Rows) 
            {
                String Proveedor_ID = Obtener_ID_Consecutivo(Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores, Cat_Com_Proveedores.Campo_Proveedor_ID, 10);
                String Partida_ID = ((Fila_Actual["PARTIDA"] != null) ? Consultar_Partida_ID(Fila_Actual["PARTIDA"].ToString()) : String.Empty);
                String Concepto_ID = ((!String.IsNullOrEmpty(Partida_ID)) ? Consultar_Concepto_ID(Partida_ID.Trim()) : String.Empty);
                String Mi_SQL = "INSERT INTO " + Cat_Com_Proveedores.Tabla_Cat_Com_Proveedores;
                Mi_SQL += "( " + Cat_Com_Proveedores.Campo_Proveedor_ID;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_No_Padron;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Nombre;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Compañia;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_RFC;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Contacto;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Estatus;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Direccion;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Colonia;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Ciudad;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Estado;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_CP;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Telefono_1;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Telefono_2;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Nextel;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Fax;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Correo_Electronico;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Comentarios;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Representante_Legal;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Tipo_Fiscal;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Usuario_Creo;
                Mi_SQL += ", " + Cat_Com_Proveedores.Campo_Fecha_Creo;
                Mi_SQL += ") VALUES";
                Mi_SQL += "( '" + Proveedor_ID + "'";
                Mi_SQL += ", '" + Convert.ToInt32(Proveedor_ID) + "'";
                Mi_SQL += ", '" + ((Fila_Actual["NOMBRE"] != null) ? Fila_Actual["NOMBRE"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["COMPANIA"] != null) ? Fila_Actual["COMPANIA"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["RFC"] != null) ? Fila_Actual["RFC"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["CONTACTO"] != null) ? Fila_Actual["CONTACTO"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["ESTATUS"] != null) ? Fila_Actual["ESTATUS"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["DIRECCION"] != null) ? Fila_Actual["DIRECCION"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["COLONIA"] != null) ? Fila_Actual["COLONIA"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["CIUDAD"] != null) ? Fila_Actual["CIUDAD"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["ESTADO"] != null) ? Fila_Actual["ESTADO"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["CODIGO_POSTAL"] != null) ? Fila_Actual["CODIGO_POSTAL"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["TELEFONO1"] != null) ? Fila_Actual["TELEFONO1"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["TELEFONO2"] != null) ? Fila_Actual["TELEFONO2"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["NEXTEL"] != null) ? Fila_Actual["NEXTEL"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["FAX"] != null) ? Fila_Actual["FAX"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["E_MAIL"] != null) ? Fila_Actual["E_MAIL"].ToString().Trim() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["COMENTARIOS"] != null) ? Fila_Actual["COMENTARIOS"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["REPRESENTANTE_LEGAL"] != null) ? Fila_Actual["REPRESENTANTE_LEGAL"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '" + ((Fila_Actual["TIPO_FISCAL"] != null) ? Fila_Actual["TIPO_FISCAL"].ToString().Trim().ToUpper() : "") + "'";
                Mi_SQL += ", '[MIGRACIÓN CONTEL]'";
                Mi_SQL += ", GETDATE()";
                Mi_SQL += ")";
                SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);

                if (!String.IsNullOrEmpty(Partida_ID))
                {
                    Mi_SQL = "INSERT INTO " + Cat_Com_Det_Part_Prov.Tabla_Cat_Com_Det_Part_Prov;
                    Mi_SQL += "( " + Cat_Com_Det_Part_Prov.Campo_Proveedor_ID;
                    Mi_SQL += ", " + Cat_Com_Det_Part_Prov.Campo_Partida_Generica_ID;
                    Mi_SQL += ", " + Cat_Com_Det_Part_Prov.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Cat_Com_Det_Part_Prov.Campo_Fecha_Creo;
                    Mi_SQL += ") VALUES";
                    Mi_SQL += "( '" + Proveedor_ID + "'";
                    Mi_SQL += ", '" + Partida_ID + "'";
                    Mi_SQL += ", '[MIGRACIÓN CONTEL]'";
                    Mi_SQL += ", GETDATE()";
                    Mi_SQL += ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                if (!String.IsNullOrEmpty(Concepto_ID))
                {
                    Mi_SQL = "INSERT INTO " + Cat_Com_Giro_Proveedor.Tabla_Cat_Com_Giro_Proveedor;
                    Mi_SQL += "( " + Cat_Com_Giro_Proveedor.Campo_Proveedor_ID;
                    Mi_SQL += ", " + Cat_Com_Giro_Proveedor.Campo_Giro_ID;
                    Mi_SQL += ", " + Cat_Com_Giro_Proveedor.Campo_Usuario_Creo;
                    Mi_SQL += ", " + Cat_Com_Giro_Proveedor.Campo_Fecha_Creo;
                    Mi_SQL += ") VALUES";
                    Mi_SQL += "( '" + Proveedor_ID + "'";
                    Mi_SQL += ", '" + Concepto_ID + "'";
                    Mi_SQL += ", '[MIGRACIÓN CONTEL]'";
                    Mi_SQL += ", GETDATE()";
                    Mi_SQL += ")";
                    SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }

            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('EXITO');", true);
        }
        catch (Exception Ex) 
        { 
            
        }
    }



    private String Consultar_Partida_ID(String Clave) {
        String Partida_ID = String.Empty;
        try {
            String MI_SQL = "SELECT " + Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID
                            + " FROM " + Cat_Sap_Partidas_Especificas.Tabla_Cat_SAP_Partidas_Especificas
                            + " WHERE " + Cat_Sap_Partidas_Especificas.Campo_Clave + " = '" + Clave + "'";
            DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL);
            if (Ds_Datos != null)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    DataTable Dt_Datos = Ds_Datos.Tables[0];
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Partida_ID = Dt_Datos.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_Generica_ID].ToString().Trim();
                        }
                    }
                }
            }
        }catch (Exception Ex) {
            throw new Exception(Ex.Message);
        }
        return Partida_ID;
    }

    private String Consultar_Concepto_ID(String Partida_ID)
    {
        String Concepto_ID = String.Empty;
        try
        {
            String MI_SQL = "SELECT " + Cat_Sap_Partidas_Genericas.Campo_Concepto_ID
                            + " FROM " + Cat_Sap_Partidas_Genericas.Tabla_Cat_Sap_Partidas_Genericas
                            + " WHERE " + Cat_Sap_Partidas_Genericas.Campo_Partida_Generica_ID + " = '" + Partida_ID + "'";
            DataSet Ds_Datos = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, MI_SQL);
            if (Ds_Datos != null)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    DataTable Dt_Datos = Ds_Datos.Tables[0];
                    if (Dt_Datos != null)
                    {
                        if (Dt_Datos.Rows.Count > 0)
                        {
                            Concepto_ID = Dt_Datos.Rows[0][Cat_Sap_Partidas_Genericas.Campo_Concepto_ID].ToString().Trim();
                        }
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
        return Concepto_ID;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
    ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
    ///PARÁMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Marzo/2010 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
    {
        String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
        try
        {
            String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
            Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
            {
                Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
            }
        }
        catch (SqlException Ex)
        {
            new Exception(Ex.Message);
        }
        return Id;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
    ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
    ///PARÁMETROS:     
    ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
    ///             2. Longitud_ID. Longitud que tendra el ID. 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Marzo/2010 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
    {
        String Retornar = "";
        String Dato = "" + Dato_ID;
        for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
        {
            Retornar = Retornar + "0";
        }
        Retornar = Retornar + Dato;
        return Retornar;
    }


}
