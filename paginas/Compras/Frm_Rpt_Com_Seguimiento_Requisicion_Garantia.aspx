﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Rpt_Com_Seguimiento_Requisicion_Garantia.aspx.cs"
    Inherits="paginas_Compras_Frm_Rpt_Com_Seguimiento_Requisicion_Garantia" Culture="es-MX" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script src="../../javascript/Js_Ope_Com_Requisiciones_Garantia.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">

        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }

        function Limpiar_Ctrl_Busqueda_Bienes_Muebles() {
            document.getElementById("<%=Txt_Busqueda_Numero_Inventario.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Producto.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Modelo.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Numero_Serie.ClientID%>").value = "";
            return false;
        }

        function Limpiar_Ctrl_Busqueda_Listado() {
            document.getElementById("<%=Cmb_Dependencia_Panel.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Inicial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Final.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda.ClientID%>").value = "";
            return false;
        }

        function pageLoad() {
            Inicializar_Controles_GRG();
        }
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->

    <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                               Seguimiento Requisicion de Garantia
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Imprimir_Req" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/gridview/grid_print.png"
                                    ToolTip="Imprimir Requisición" OnClick="Btn_Imprimir_Req_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones" runat="server">
                    <asp:Panel ID="Pnl_Busqueda_Principal" runat="server" Width="100%" DefaultButton="Btn_Buscar">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 15%;">
                                    Unidad Responsable
                                </td>
                                <td style="width: 85%;" colspan="3">
                                    <asp:DropDownList ID="Cmb_Dependencia_Panel" runat="server" Width="99%" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    Fecha
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server"
                                        TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                        ValidChars="/_" />
                                    <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial"
                                        PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                    <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                        ToolTip="Seleccione la Fecha Inicial" />
                                    :&nbsp;&nbsp;
                                    <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                    <cc1:CalendarExtender ID="Txt_Fecha_Final_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Final"
                                        PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                    <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                        ToolTip="Seleccione la Fecha Final" />
                                </td>
                                <td style="width: 15%;">
                                    Folio
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Busqueda" runat="server" Width="70%" MaxLength="13"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        OnClick="Btn_Buscar_Click" ToolTip="Consultar" />
                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Listado" runat="server" Width="24px" Height="24px"
                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" ToolTip="Limpiar" OnClientClick="javascript:return Limpiar_Ctrl_Busqueda_Listado();" />
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                        WatermarkText="&lt;RQG-000000&gt;" WatermarkCssClass="watermarked">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" FilterType="Custom"
                                        TargetControlID="Txt_Busqueda" ValidChars="rRqQgG-0123456789">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                                <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; border-style: outset;
                                    border-color: Silver;">
                                    <asp:GridView ID="Grid_Requisiciones" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="GridView_1" GridLines="None" Width="100%" DataKeyNames="No_Requisicion"
                                        HeaderStyle-CssClass="tblHead" Font-Size="X-Small" EmptyDataText="Lista de Requisiciones">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Seleccionar_Requisicion" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                        CommandArgument='<%# Eval("NO_REQUISICION") %>' OnClick="Btn_Seleccionar_Requisicion_Click" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NO_REQUISICION" HeaderText="NO_REQUISICION" SortExpression="NO_REQUISICION">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FOLIO" HeaderText="FOLIO" SortExpression="FOLIO">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_ELABORACION" HeaderText="Fecha Elaboración" DataFormatString="{0:dd/MMM/yyyy}"
                                                SortExpression="FECHA_ELABORACION">
                                                <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA">
                                                <FooterStyle HorizontalAlign="Left" />
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="REQUISICION_REFERENCIADA" HeaderText="Rq Referencia" SortExpression="REQUISICION_REFERENCIADA">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Width="11%" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server">
                    <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de Requisición"
                        Width="100%">
                        <table style="width: 100%;">
                            <tr>
                                <td style="width: 15%;" align="left">
                                    <asp:HiddenField ID="Hdf_Requisicion_ID" runat="server" />
                                    Folio
                                </td>
                                <td style="width: 35%;" align="left">
                                    <asp:TextBox ID="Txt_Folio" runat="server" Width="96%"></asp:TextBox>
                                </td>
                                <td style="width: 15%;" align="left">
                                    Fecha
                                </td>
                                <td style="width: 35%;" align="left">
                                    <asp:TextBox ID="Txt_Fecha" runat="server" Width="98%" />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;" align="left">
                                    Estatus
                                </td>
                                <td style="width: 35%;" align="left">
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%" />
                                </td>
                                <td style="width: 15%;" align="left">
                                    RQ Referencia
                                </td>
                                <td style="width: 35%;" align="left">
                                    <asp:Panel ID="Pnl_Requisicion_Referencia" runat="server" Width="99%" DefaultButton="Btn_Buscar_RQ_Referencia">
                                        <asp:HiddenField ID="Hdf_RQ_Referencia_ID" runat="server" />
                                        <asp:TextBox ID="Txt_RQ_Referencia" runat="server" Width="200px"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_RQ_Referencia" runat="server" TargetControlID="Txt_RQ_Referencia"
                                            FilterType="Numbers">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Buscar_RQ_Referencia" runat="server" CssClass="Img_Button"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Requisición de Referencia"
                                            Height="16px" Width="16px" Style="display: none;" OnClick="Btn_Buscar_RQ_Referencia_Click" />
                                        <asp:ImageButton ID="Btn_Imprimir_RQ_Referencia" runat="server" CssClass="Img_Button"
                                            ImageUrl="~/paginas/imagenes/gridview/grid_print.png" ToolTip="Imprimir Requisición de Referencia"
                                            Height="16px" Width="16px" OnClick="Btn_Imprimir_RQ_Referencia_Click" />
                                    </asp:Panel>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;" align="left">
                                    U. Responsable
                                </td>
                                <td style="width: 85%;" align="left" colspan="3">
                                    <asp:DropDownList ID="Cmb_Dependencia" runat="server" Width="100%" AutoPostBack="true">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;" align="left">
                                    Proveedor
                                </td>
                                <td style="width: 85%;" align="left" colspan="3">
                                    <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                                    <asp:TextBox runat="server" ID="Txt_Proveedor" Width="99.5%" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;" align="left">
                                    Resultado
                                </td>
                                <td style="width: 35%;" align="left">
                                    <asp:DropDownList ID="Cmb_Resultado" runat="server" Width="100%" Enabled="false">
                                        <asp:ListItem Value="APLICA GARANTIA" Text="APLICA GARANTIA" />
                                        <asp:ListItem Value="NO APLICA GARANTIA" Text="NO APLICA GARANTIA" />
                                    </asp:DropDownList>
                                </td>
                                <td style="width: 15%;" align="left">&nbsp;</td>
                                <td style="width: 35%;" align="left">&nbsp;</td>
                            </tr>
                            <tr>
                                <td style="width: 15%;" align="left">
                                    Diagnostico
                                </td>
                                <td style="width: 85%;" align="left" colspan="3">
                                    <asp:TextBox runat="server" ID="Txt_Diagnostico" Width="99.5%" Rows="3" TextMode="MultiLine" Enabled="false" ></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>
                    <table style="width: 100%;">
                        <tr style="background-color: #3366CC">
                            <td style="text-align: left; font-size: 15px; color: #FFFFFF; height: 1px;">
                            </td>
                        </tr>
                    </table>
                    <table style="width: 100%;">
                        <tr>
                            <td align="center">
                                <asp:Panel ID="Pnl_Listado_Servicios_Requisicion" runat="server" Width="99%" GroupingText="Servicios de la Requisición">
                                    <asp:GridView ID="Grid_Servicios_Requisicion" runat="server" AutoGenerateColumns="False"
                                        CssClass="GridView_1" PageSize="100" GridLines="None" Width="100%" Style="white-space: normal">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="SERVICIO_ID" HeaderText="SERVICIO_ID">
                                                <ItemStyle HorizontalAlign="Left" Width="0%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CLAVE_SERVICIO" HeaderText="Clave">
                                                <ItemStyle HorizontalAlign="Center" Width="8%" />
                                                <ItemStyle HorizontalAlign="Center" Width="8%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Producto/Servicio">
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField HeaderText="Especificaciones" DataField="ESPECIFICACION_ADICIONAL">
                                                <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                <ItemStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="CANTIDAD" HeaderText="Ctd.">
                                                <ItemStyle HorizontalAlign="Center" Width="8%" />
                                                <ItemStyle HorizontalAlign="Center" Width="8%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="center">
                                <asp:Panel ID="Pnl_Listado_Bienes" runat="server" Width="99%" GroupingText="Bienes para el Servicio">
                                    <asp:GridView ID="Grid_Listado_Bienes" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                        GridLines="Both" PageSize="100" Width="100%" Style="font-size: xx-small; white-space: normal">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID">
                                                <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                <ItemStyle HorizontalAlign="Center" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inv.">
                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                                <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                                <ItemStyle HorizontalAlign="Center" Width="30%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NUMERO_SERIE" HeaderText="Serie" Visible="True">
                                                <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                                <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </asp:Panel>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <hr class="linea" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                *Justificación
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="Txt_Justificacion" runat="server" MaxLength="3000" TabIndex="10"
                                    TextMode="MultiLine" Width="100%" Height="105px"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="Txt_Justificacion"
                                    WatermarkText="&lt;Límite de Caracteres 3000&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTE_Justificacion" runat="server" TargetControlID="Txt_Justificacion"
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ.:/*-!$%&()=,[]{}+<>@?¡?¿# ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="Div_Comentarios" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:GridView ID="Grid_Comentarios" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                    GridLines="None" PageSize="100" Width="100%" Style="font-size: xx-small; white-space: normal">
                                                    <RowStyle CssClass="GridItem" />
                                                    <Columns>
                                                        <asp:BoundField DataField="COMENTARIO" HeaderText="Comentarios">
                                                            <HeaderStyle HorizontalAlign="Left" Width="68%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="68%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FECHA_COMENTO" HeaderText="Fecha" DataFormatString="{0:dd/MMM/yyyy}">
                                                            <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="USUARIO_COMENTO" HeaderText="Usuario">
                                                            <HeaderStyle HorizontalAlign="Left" Width="24%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="24%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div id="Div_Historial" runat="server">
                                    <table width="100%">
                                        <tr>
                                            <td align="left" valign="top">
                                                <asp:GridView ID="Grid_Historial" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                                                    GridLines="None" PageSize="100" Width="100%" Style="font-size: xx-small; white-space: normal">
                                                    <RowStyle CssClass="GridItem" />
                                                    <Columns>
                                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                                            <HeaderStyle HorizontalAlign="Center" />
                                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" DataFormatString="{0:dd/MMM/yyyy hh:mm:sstt}">
                                                            <HeaderStyle HorizontalAlign="Center" Width="35%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="35%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="USUARIO_CREO" HeaderText="Usuario">
                                                            <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="50%" Font-Size="X-Small" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:UpdatePanel ID="UPnl_Busqueda" runat="server" UpdateMode="Conditional">
            <ContentTemplate>
                <asp:Button ID="Button" runat="server" Text="Button" Style="display: none;" />
                <cc1:ModalPopupExtender ID="Modal_Busqueda_Prod_Serv" runat="server" TargetControlID="Btn_Comodin_Busqueda_Productos_Srv"
                    PopupControlID="Modal_Productos_Servicios" CancelControlID="Btn_Cerrar" DynamicServicePath=""
                    DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
                <%--<asp:Button ID="Btn_Comodin_1" runat="server" Text="Button" style="display:none;" /> --%>
                <asp:Button ID="Btn_Comodin_Busqueda_Productos_Srv" runat="server" Text="Button"
                    Style="display: none" />
                <%--<asp:Button ID="Btn_Comodin_Close" runat="server" Text="Button" style="display:none;" />--%>
            </ContentTemplate>
        </asp:UpdatePanel>
        <asp:Panel ID="Modal_Productos_Servicios" runat="server" CssClass="drag" HorizontalAlign="Center"
            Style="display: none; border-style: outset; border-color: Silver; width: 860px;">
            <asp:Panel ID="Panel2" runat="server" CssClass="estilo_fuente" Style="cursor: move;
                background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
                <table class="estilo_fuente">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold;">
                            <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Buscar Servicio
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <center>
                <asp:UpdatePanel ID="MP_UDPpdatePanel1" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <div>
                            <asp:Panel ID="Pnl_Busqueda_Servicios" runat="server" Width="98%" DefaultButton="">
                                <table width="100%" style="color: Black;">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr align="left">
                                        <td style="width: 10%;">
                                            Nombre
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="98.5%" AutoPostBack="true">
                                            </asp:TextBox>
                                        </td>
                                        <td style="width: 10%;">
                                            <asp:ImageButton ID="IBtn_MDP_Prod_Serv_Buscar" runat="server" CssClass="Img_Button"
                                                ToolTip="Búscar producto ó servicio" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" />
                                            <asp:ImageButton ID="IBtn_MDP_Prod_Serv_Cerrar" runat="server" CssClass="Img_Button"
                                                ToolTip="Cerrar Búsqueda" OnClick="IBtn_MDP_Prod_Serv_Cerrar_Click" ImageUrl="~/paginas/imagenes/paginas/quitar.png" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                            Clave
                                        </td>
                                        <td align="left">
                                            <asp:TextBox ID="Txt_Clave" runat="server" Width="50.5%" AutoPostBack="true">
                                            </asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="Ftb_Txt_Clave" runat="server" TargetControlID="Txt_Clave"
                                                FilterType="Custom,Numbers" ValidChars="0123456789">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3">
                                            &nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                            <div style="overflow: auto; height: 260px; width: 99%; vertical-align: top; border-style: outset;
                                border-color: Silver;">
                                <table width="100%" style="color: Black;">
                                    <tr>
                                        <td align="center">
                                            <asp:GridView ID="Grid_Productos_Servicios_Modal" runat="server" AllowPaging="false"
                                                AutoGenerateColumns="false" CssClass="GridView_1" DataKeyNames="ID,NOMBRE,DISPONIBLE,COSTO"
                                                GridLines="Vertical" Width="98%" Style="white-space: normal" EmptyDataText="No se encontraron registros">
                                                <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:TemplateField HeaderText="">
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="Btn_Seleccionar_Producto" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                CommandArgument='<%# Eval("ID") %>' />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CLAVE" HeaderText="Clave" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Width="7%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="12%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="10%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="UNIDAD" HeaderText="Uni." Visible="True">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Wrap="true" Width="7%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="COSTO" HeaderText="P. Unitario" Visible="True" DataFormatString="{0:C}">
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DISPONIBLE" HeaderText="Disp." Visible="True">
                                                        <FooterStyle HorizontalAlign="Right" />
                                                        <HeaderStyle HorizontalAlign="Right" />
                                                        <ItemStyle HorizontalAlign="Center" Width="8%" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="ID" HeaderText="Producto_Servicio_ID" Visible="False">
                                                        <FooterStyle HorizontalAlign="Left" />
                                                        <HeaderStyle HorizontalAlign="Left" />
                                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <PagerStyle CssClass="GridHeader" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </center>
        </asp:Panel>
        <%-- Panel del ModalPopUp--%>
    </div>
    <asp:Panel ID="Modal_Productos_ServiciosX" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Button ID="Btn_Realizar_Busqueda" runat="server" Text="Buscar" CssClass="button" />
        <asp:Button ID="Btn_Cerrar" runat="server" Text="Cerrar" CssClass="button" />
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_Modal" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Bien_Mueble" runat="server" TargetControlID="Btn_Comodin"
                BackgroundCssClass="progressBackgroundFilter" PopupControlID="Pnl_Busqueda_Bien_Mueble"
                CancelControlID="Btn_Cerrar_Busqueda_Bien_Mueble" DropShadow="True" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Bien_Mueble" runat="server" HorizontalAlign="Center"
        Width="800px" Style="display: none; border-style: outset; border-color: Silver;
        background-repeat: repeat-y; background-color: White" DefaultButton="Btn_Buscar_Bienes_Muebles">
        <asp:Panel ID="Pnl_Interno" runat="server" Style="background-color: Silver; color: Black;
            font-size: 12; font-weight: bold; border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%; border-color: Black;">
                        <asp:Image ID="Img_Encabezado_Busqueda_Bienes_Muebles" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda de Bienes Muebles
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Busqueda_Bien_Mueble" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Busqueda" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <div style="width: 98%; height: 120px; background-color: White;">
                        <table width="100%">
                            <tr>
                                <td style="text-align: left;" colspan="2">
                                    <asp:Label ID="Lbl_Titulo_Busqueda" runat="server" Text="Filtros para la Búsqueda"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: left;">
                                    <asp:Label ID="Lbl_Busqueda_Numero_Inventario" runat="server" Text="No. Inventario"
                                        CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td style="width: 30%; text-align: left;">
                                    <asp:TextBox ID="Txt_Busqueda_Numero_Inventario" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Numero_Inventario" runat="server"
                                        FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="Txt_Busqueda_Numero_Inventario"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-_%/$# " Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td colspan="2">
                                    &nbsp;
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: left;">
                                    <asp:Label ID="Lbl_Busqueda_Producto" runat="server" Text="Nombre Producto" CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td colspan="3" style="text-align: left;">
                                    <asp:TextBox ID="Txt_Busqueda_Producto" runat="server" Width="98%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Producto" runat="server" TargetControlID="Txt_Busqueda_Producto"
                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: left;">
                                    <asp:Label ID="Lbl_Busqueda_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td style="width: 30%; text-align: left;">
                                    <asp:TextBox ID="Txt_Busqueda_Modelo" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Modelo" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        TargetControlID="Txt_Busqueda_Modelo" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-_%/$# "
                                        Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td style="width: 20%; text-align: left;">
                                    <asp:Label ID="Lbl_Busqueda_Numero_Serie" runat="server" Text="No. Serie" CssClass="estilo_fuente"></asp:Label>
                                </td>
                                <td style="width: 30%; text-align: left;">
                                    <asp:TextBox ID="Txt_Busqueda_Numero_Serie" runat="server" Width="95%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Numero_Serie" runat="server" TargetControlID="Txt_Busqueda_Numero_Serie"
                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-+$%&" Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4" style="text-align: right;">
                                    <asp:ImageButton ID="Btn_Buscar_Bienes_Muebles" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                        CausesValidation="False" ToolTip="Buscar" />
                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos" runat="server" CausesValidation="False"
                                        OnClientClick="javascript:return Limpiar_Ctrl_Busqueda_Bienes_Muebles();" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                        ToolTip="Limpiar Filtros" Width="20px" />
                                    &nbsp;&nbsp;
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div style="width: 97%; height: 160px; overflow: auto; border-style: outset; background-color: White;">
                        <center>
                            <caption>
                                <asp:GridView ID="Grid_Resultado_Busqueda_Bienes_Muebles" runat="server" AutoGenerateColumns="False"
                                    CssClass="GridView_1" GridLines="None" AllowPaging="true" PageSize="100" Width="98%">
                                    <RowStyle CssClass="GridItem" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID">
                                            <HeaderStyle HorizontalAlign="Center" Width="1%" />
                                            <ItemStyle HorizontalAlign="Left" Width="1%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="Inv.">
                                            <HeaderStyle HorizontalAlign="Center" Width="14%" />
                                            <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" Width="14%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre">
                                            <HeaderStyle HorizontalAlign="Center" Width="30%" />
                                            <ItemStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_SERIE" HeaderText="Serie" Visible="True">
                                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" Visible="True">
                                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                            <ItemStyle HorizontalAlign="Center" Width="25%" Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                </asp:GridView>
                            </caption>
                        </center>
                    </div>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
