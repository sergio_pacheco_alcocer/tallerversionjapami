﻿<%@ Page Title="Catálogo de Tipos Resguardos" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Com_Tipos_Resguardos.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Com_Tipos_Resguardos" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
             <asp:UpdateProgress ID="Uprg_Tipos_Resguardos" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                    </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Tipos_Resguardo" style="background-color:#ffffff; width:100%; height:100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">
                    <tr>
                        <td colspan="4" class="label_titulo">
                            Catálogo de Tipos Resguardos
                        </td>                        
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:Image ID="Img_Error" runat="server" ImageUrl = "../imagenes/paginas/sias_warning.png"/>
                            <br />
                            <asp:Label ID="Lbl_Error" runat="server" ForeColor="Red" Text="" TabIndex="0"></asp:Label>
                        </td> 
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="2" style="width:50%">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                            CssClass="Img_Button" onclick="Btn_Nuevo_Click"  />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                            CssClass="Img_Button" onclick="Btn_Modificar_Click"  />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                            CssClass="Img_Button" OnClientClick="return confirm('¿Esta seguro de Eliminar el presente registro?');" onclick="Btn_Eliminar_Click" 
                                />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                            CssClass="Img_Button" onclick="Btn_Salir_Click"   />
                        </td>
                        <td colspan="2" align="right" style="width:50%">
                            Búsqueda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180"
                            ToolTip="Buscar" TabIndex="1" ></asp:TextBox>
                            <asp:ImageButton ID="Btn_Busqueda" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                TabIndex="2" onclick="Btn_Busqueda_Click" />
                                <cc1:TextBoxWatermarkExtender ID="Twe_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Buscar>" TargetControlID="Txt_Busqueda" />
                        </td>                        
                    </tr>
                    <tr>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                    </tr>
                     <tr>
                        <td style="width:25%">
                            <asp:Label ID="Lbl_ID" runat="server" Text="ID Tipo Resguardo"></asp:Label>
                        </td>
                        <td style="width:25%">
                            <asp:TextBox ID="Txt_ID" runat="server" Width="97%"></asp:TextBox>
                        </td>
                        <td style="width:25%" align="center">
                            <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus" ></asp:Label>
                        </td>
                        <td style="width:25%">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="97%" >
                                <asp:ListItem Value="-1"> << Selecciona una opcion >> </asp:ListItem>
                                <asp:ListItem  Value="Activo"> Activo </asp:ListItem>
                                <asp:ListItem Value="Inactivo"> Inactivo </asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%">
                           <asp:Label ID="Lbl_Nombre" runat="server" Text="Nombre"></asp:Label> 
                        </td>
                        <td  style="width:25%">
                            <asp:TextBox ID="Txt_Nombre" Width="97%" runat="server" MaxLength="100"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%">
                            <asp:Label ID="Lbl_Descripcion" runat="server" Text="Descripción"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Descripcion" TextMode="MultiLine" Width="97%" 
                           Height="65" runat="server" MaxLength="250"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                        <td style="width:25%">
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" style="width:100%" align="center">
                            <asp:GridView ID="Grid_Tipos_Resguardo" runat="server" CssClass="GridView_1"
                            AutoGenerateColumns="False" Width="96%" OnSelectedIndexChanged="Grid_Tipos_Resguardo_SelectedIndexChanged"
                            GridLines="none">
                             <RowStyle CssClass="GridItem" />
                             <Columns>
                                <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                    ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                    <ItemStyle Width="3%" />
                                </asp:ButtonField>
                                <asp:BoundField DataField="TIPO_RESGUARDO_ID" HeaderText="Tipo Resguardo ID"
                                    SortExpression="Tipo Resguardo ID" Visible="true">
                                    <HeaderStyle HorizontalAlign="Left" Width="27%"/>
                                    <ItemStyle HorizontalAlign="Left" Width="27%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" 
                                    SortExpression="NOMBRE" >
                                    <HeaderStyle HorizontalAlign="Left" Width="35%"/>
                                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                                </asp:BoundField>                                
                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                                    SortExpression="ESTATUS" >
                                    <HeaderStyle HorizontalAlign="Left" Width="35%"/>
                                    <ItemStyle HorizontalAlign="Left" Width="35%" />
                                </asp:BoundField>                            
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" /> 
                            <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
