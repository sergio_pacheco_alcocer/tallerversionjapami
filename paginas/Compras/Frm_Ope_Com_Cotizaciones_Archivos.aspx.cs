﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.IO;
using JAPAMI.Archivos_Cotizaciones.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;

public partial class paginas_Compras_Frm_Ope_Com_Cotizaciones_Archivos : System.Web.UI.Page
{
    #region Variables y Constantes
        private const String P_Dt_Archivos_Cotizaciones = "Dt_Archivos_Cotizaciones";
        private const String P_Dt_Archivos_Eliminados = "Dt_Archivos_Eliminados";
        private const String P_Nombre_Archivo = "Nombre_Archivo";
        private const String P_Comentarios = "Comentarios";
    #endregion

    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        //Declaracion de variables
        int No_Requisicion; //variable para el numero de la requisicion
        String Proveedor_ID = String.Empty; //Variable para el ID del proveedor
        String Proveedor = String.Empty; //variable para el nombre del proveedor

        try
        {
            if (!IsPostBack)
            {
                //Veruificar si existe numero de requisicion
                if (HttpContext.Current.Request.QueryString["No_Requisicion"] != null)
                {
                    //Obtener el numero de la requisicion y colocarlo en la caja de texto
                    No_Requisicion = Convert.ToInt32(HttpContext.Current.Request.QueryString["No_Requisicion"]);

                    //Colocar el numero de la requisicion en la etiqueta y en la caja oculta
                    Lbl_Titulo_Pagina.Text = "Archivos de la Requisici&oacute;n " + No_Requisicion.ToString().Trim();
                    Txt_No_Requisicion.Value = No_Requisicion.ToString().Trim();

                    Estado_Inicial();
                }

                //Verificar si existe el ID del proveedor
                if (HttpContext.Current.Request.QueryString["Proveedor_ID"] != null)
                {
                    //Obtener el ID del Proveedor y colocarlo en la caja de texto
                    Proveedor_ID = HttpContext.Current.Request.QueryString["Proveedor_ID"].ToString().Trim();
                    Txt_Proveedor_ID.Value = Proveedor_ID;
                }

                //Verificar si existe el ID del proveedor
                if (HttpContext.Current.Request.QueryString["Proveedor"] != null)
                {
                    //Obtener el ID del Proveedor y colocarlo en la caja de texto
                    Proveedor = HttpContext.Current.Request.QueryString["Proveedor"].ToString().Trim();
                    Txt_Proveedor.Text = Proveedor;
                }
            }
        }
        catch (Exception ex)
        {
            Mostrar_Informacion("Error: (Page_Load) " + ex.ToString(), true);
        }
    }
    #endregion

    #region Metodos
        private void Mostrar_Informacion(String txt, Boolean mostrar)
        {
            Lbl_Informacion.Style.Add("color", "#990000");
            Lbl_Informacion.Visible = mostrar;
            Img_Warning.Visible = mostrar;
            Lbl_Informacion.Text = txt;
        }

        private void Estado_Inicial()
        {
            //Declaracion de variables
            int No_Requisicion; //Variable para el numero de la requisicion

            try
            {
                //Obtener el numero de la requisicion
                No_Requisicion = Convert.ToInt32(Txt_No_Requisicion.Value);

                Elimina_Sesiones();
                Llena_Grid_Archivos(No_Requisicion);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        private void Elimina_Sesiones()
        {
            try
            {
                //Eliminar las sesiones de la pagina
                HttpContext.Current.Session.Remove(P_Dt_Archivos_Cotizaciones);
                HttpContext.Current.Session.Remove(P_Dt_Archivos_Eliminados);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos
        // DESCRIPCION:             Eliminar el registro de los archivos en la base de datos
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              20/Marzo/2012 14:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos(int No_Requisicion)
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Cotizaciones_Negocio Archivos_Cotizaciones_Negocio = new Cls_Ope_Archivos_Cotizaciones_Negocio(); //Variable para la capa de negocios

            try
            {
                //Asignar propiedades
                Archivos_Cotizaciones_Negocio.P_No_Requisicion = No_Requisicion;
                Archivos_Cotizaciones_Negocio.Eliminar_Archivos_Cotizaciones();

                //Eliminar los archivos de la ruta
                Elimina_Archivos_Ruta(No_Requisicion);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Elimina_Archivos_Ruta
        // DESCRIPCION:             Eliminar los archivos de la cotizacion
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              20/Marzo/2012 14:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivos_Ruta(int No_Requisicion)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta de los archivos
            int Cont_Elementos; //Variable para el contador
            String[] Vec_Archivos; //Vector para los archivos

            try
            {
                //Obtener la ruta de los archivos
                Ruta = HttpContext.Current.Server.MapPath("~") + "\\Cot_Archivos\\RQ-" + No_Requisicion.ToString().Trim();

                //Verificar si existe la carpeta
                if (Directory.Exists(Ruta) == true)
                {
                    //Obtener los archivos del directorio
                    Vec_Archivos = Directory.GetFiles(Ruta);

                    //Verificar si existen archivos en el directorio
                    if (Vec_Archivos.Length > 0)
                    {
                        //Ciclo para el barrido de los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Vec_Archivos.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Vec_Archivos[Cont_Elementos].Trim());
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }


        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Alta_Archivos
        // DESCRIPCION:             Dar de alta los archivos en la base de datos
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 20:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Alta_Archivos(int No_Requisicion)
        {
            //Declaracion de variables 
            Cls_Ope_Archivos_Cotizaciones_Negocio Archivos_Cotizaciones_Negocio = new Cls_Ope_Archivos_Cotizaciones_Negocio(); //variable para la capa de negocios
            DataTable Dt_Archivos = new DataTable(); //tabla para loa archivos a subir
            DataTable Dt_Archivos_Eliminados = new DataTable(); //tabla para los archivos eliminados

            try
            {
                //verificar si la variable de sesion de los archivos no esta vacia
                if (HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] != null)
                {
                    //Asignar propiedades
                    Archivos_Cotizaciones_Negocio.P_No_Requisicion = No_Requisicion;
                    
                    //Convertir variable de sesion en tabla
                    Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones]);

                    Archivos_Cotizaciones_Negocio.P_Dt_Archivos = Dt_Archivos;

                    //Verificar si existen archivos eliminados
                    if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] != null)
                    {
                        //Convertir variable de sesion en tabla
                        Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);
                        Archivos_Cotizaciones_Negocio.P_Dt_Archivos_Eliminados = Dt_Archivos_Eliminados;
                    }

                    //Dar de alta los archivos
                    Archivos_Cotizaciones_Negocio.Agregar_Archivos_Cotizacion();

                    //Agregar los archivos a la ruta correspondiente
                    Alta_Archivos_Ruta(No_Requisicion, Dt_Archivos, Dt_Archivos_Eliminados);
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Alta_Archivos_Ruta
        // DESCRIPCION:             Dar de alta los archivos en la ruta correspondiente
        // PARAMETROS:              1. No_Requisicion: Entero que contiene el numero de al requisicion
        //                          2. Dt_Archivos: Tabla que contiene los archivos para dar de alta
        //                          3. Dt_Archivos_Eliminados: Tabla que contiene los archivos a eliminar
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 20:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Alta_Archivos_Ruta(int No_Requisicion, DataTable Dt_Archivos, DataTable Dt_Archivos_Eliminados)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta
            int Cont_Elementos; //Variable para el contador
            String Ruta_Fija = String.Empty; //variable para la ruta fija
            String[] Archivos_Temporales; //Variable para los archivos temporales

            try
            {
                //Asignar la ruta de los archivos
                Ruta = HttpContext.Current.Server.MapPath("~") + "\\Cot_Archivos\\RQ-" + No_Requisicion.ToString().Trim();
                Ruta_Fija = Ruta;

                //Verificar si la ruta existe
                if (Directory.Exists(Ruta) == false)
                {
                    Directory.CreateDirectory(Ruta);
                }

                //Verificar si la tabla de los archivos eliminados no es nula
                if (Dt_Archivos_Eliminados != null)
                {
                    //Verificar si la tabla de los archivos eliminado tiene elementos
                    if (Dt_Archivos_Eliminados.Rows.Count > 0)
                    {
                        //Ciclo para eliminar los archivos
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos_Eliminados.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar si el archivo tiene que ser eliminado
                            if (Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Original"].ToString().Trim() == "SI")
                            {
                                //Verificar si el archivo existe
                                if (File.Exists(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                {
                                    //Eliminar el archivo
                                    File.Delete(Ruta + "\\" + Dt_Archivos_Eliminados.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                }
                            }
                        }
                    }
                }

                //Asignar la ruta de los archivos temporales
                Ruta = HttpContext.Current.Server.MapPath("~") + "\\Cot_Archivos\\temporal\\RQ-" + No_Requisicion.ToString().Trim();

                //Verificar si la ruta existe
                if (Directory.Exists(Ruta) == true)
                {
                    //Verificar si la tabla tiene elementos
                    if (Dt_Archivos.Rows.Count > 0)
                    {
                        //Ciclo para el barrido de la tabla de los archivos temporales
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                        {
                            //Verificar si se tiene que copiar el elemento
                            if (Dt_Archivos.Rows[Cont_Elementos]["Original"].ToString().Trim() == "NO")
                            {
                                //Verificar si el archivo existe
                                if (File.Exists(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim()) == true)
                                {
                                    //Copiar archivo
                                    File.Copy(Ruta + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim(), Ruta_Fija + "\\" + Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim());
                                }
                            }
                        }
                    }

                    //Obtener el listado de los archivos de la ruta temporal
                    Archivos_Temporales = Directory.GetFiles(Ruta);

                    //Verificar si hay archivos
                    if (Archivos_Temporales.Length > 0)
                    {
                        //Ciclo para la eliminacion de los archivos temporales
                        for (Cont_Elementos = 0; Cont_Elementos < Archivos_Temporales.Length; Cont_Elementos++)
                        {
                            //Eliminar el archivo actual
                            File.Delete(Archivos_Temporales[Cont_Elementos].Trim());
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Grid
        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Llena_Grid_Archivos
        // DESCRIPCION:             Llenar el grid de los archivos
        // PARAMETROS:              No_Requisicion: Entero que contiene el numero de al requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 20:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Llena_Grid_Archivos(int No_Requisicion)
        {
            //Declaracion de variables
            Cls_Ope_Archivos_Cotizaciones_Negocio Archivos_Cotizaciones_Negocio = new Cls_Ope_Archivos_Cotizaciones_Negocio(); //variable para la capa de negocios
            DataTable Dt_Archivos_Cotizaciones = new DataTable(); //tabla para el llenado del Grid

            try
            {
                //Verificar si la variable de sesion existe
                if (HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] == null)
                {
                    //Ejecutar ls consulta
                    Archivos_Cotizaciones_Negocio.P_No_Requisicion = No_Requisicion;
                    Dt_Archivos_Cotizaciones = Archivos_Cotizaciones_Negocio.Consulta_Archivos();
                }
                else
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_Archivos_Cotizaciones = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones]);
                }

                //Llenar el grid
                Grid_Archivos_Cotizaciones.Columns[1].Visible = true;
                Grid_Archivos_Cotizaciones.Columns[4].Visible = true;
                Grid_Archivos_Cotizaciones.Columns[6].Visible = true;
                Grid_Archivos_Cotizaciones.DataSource = Dt_Archivos_Cotizaciones;
                Grid_Archivos_Cotizaciones.DataBind();
                Grid_Archivos_Cotizaciones.Columns[1].Visible = false;
                Grid_Archivos_Cotizaciones.Columns[4].Visible = false;
                Grid_Archivos_Cotizaciones.Columns[6].Visible = false;
                //Colocar la table ne la variable de sesion
                HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] = Dt_Archivos_Cotizaciones;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Agregar_Archivo_Tabla
        // DESCRIPCION:             Agregar la informacion del archivo al Grid
        // PARAMETROS:              1. Nombre_Archivo: Cadena de texto que contiene el nombre del archivo
        //                          2. Comentarios: Cadena de texto que contiene los comentarios del archivo
        //                          3. Proveedor_ID: Cadena de texto que contiene el ID del proveedor
        //                          4. Proveedor: Cadena de texto que contiene el nombre del proveedor
        //                          5. No_Requisicion: Entero que contiene el numero de la requisicion
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 20:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Agregar_Archivo_Tabla(String Nombre_Archivo, String Comentarios, String Proveedor_ID, String Proveedor, int No_Requisicion)
        {
            //Declaracion de variables
            DataTable Dt_Archivos = new DataTable(); //tabla para los archivos
            DataRow Renglon; //Renglon para el llenado de la tabla
            int Cont_Elementos; //variable para el contador
            Boolean Existente = false; //variable que indica si el archivo ya existe

            try
            {
                //verificar si existe la variable de sesion
                if (HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] == null)
                {
                    //Definir las columnas
                    Dt_Archivos.Columns.Add("No_Archivo", typeof(Int64));
                    Dt_Archivos.Columns.Add("Folio", typeof(String));
                    Dt_Archivos.Columns.Add("Proveedor", typeof(String));
                    Dt_Archivos.Columns.Add("Nombre_Archivo", typeof(String));
                    Dt_Archivos.Columns.Add("Descripcion", typeof(String));
                    Dt_Archivos.Columns.Add("Proveedor_ID", typeof(String));
                    Dt_Archivos.Columns.Add("Original", typeof(String));
                }
                else
                {
                    //Colocar variable de sesion en la tabla
                    Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones]);
                }

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                {
                    //Verificar que el renglon no haya sido eliminado
                    if (Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                    {
                        //Verificar si el archivo ya se encuentra en lops archivos de la cotizacion
                        if (Dt_Archivos.Rows[Cont_Elementos]["Nombre_Archivo"].ToString().Trim().ToUpper() == Nombre_Archivo.Trim().ToUpper() && Dt_Archivos.Rows[Cont_Elementos]["Proveedor_ID"].ToString().Trim() == Proveedor_ID.Trim())
                        {
                            //indicar que ya existe el elemento y salir del ciclo
                            Existente = true;
                            break;
                        }
                    }
                }

                //Verificar si se tiene que agregar el archivo a la tabla
                if (Existente == false)
                {
                    //instanciar el renglon
                    Renglon = Dt_Archivos.NewRow(); 

                    //Llenar el renglon
                    Renglon["No_Archivo"] = Convert.ToInt64(String.Format("{0:yyyyMMddHHmmss}", DateTime.Now));
                    Renglon["Folio"] = "RQ-" + No_Requisicion.ToString().Trim();
                    Renglon["Proveedor"] = Proveedor.Trim();

                    //verificar si el nombre del archivo no excede de 250 caracteres
                    if (Nombre_Archivo.Trim().Length > 250)
                    {
                        Renglon["Nombre_Archivo"] = Nombre_Archivo.Substring(0, 250);
                    }
                    else
                    {
                        Renglon["Nombre_Archivo"] = Nombre_Archivo;
                    }

                    //verificar si los comentarios exceden de 500
                    if (Comentarios.Trim().Length > 500)
                    {
                        Renglon["Descripcion"] = Comentarios.Substring(0, 500);
                    }
                    else
                    {
                        Renglon["Descripcion"] = Comentarios;
                    }

                    Renglon["Proveedor_ID"] = Proveedor_ID;
                    Renglon["Original"] = "NO";

                    //Colocar archivo en la tabla
                    Dt_Archivos.Rows.Add(Renglon);
                }

                //Colocar la tabla en la variable de sesion
                HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] = Dt_Archivos;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }

        ///*******************************************************************************
        // NOMBRE DE LA FUNCION:    Agregar_Archivo_Tabla
        // DESCRIPCION:             Agregar la informacion del archivo al Grid
        // PARAMETROS:              No_Archivo: Entero que contiene el numero del archivo
        // CREO:                    Noe Mosqueda Valadez
        // FECHA_CREO:              16/Marzo/2012 20:00
        // MODIFICO:
        // FECHA_MODIFICO:
        // CAUSA_MODIFICACIÓN:
        //********************************************************************************/
        private void Elimina_Archivo_Tabla(Int64 No_Archivo)
        {
            //Declaracion de variables
            DataTable Dt_Archivos = new DataTable(); //Tabla para los archivos a agregar
            DataTable Dt_Archivos_Eliminados = new DataTable(); //tabla para los archivos a eliminar
            DataRow Renglon; //renglon para el llenado de la tabla
            int Cont_Elementos; //variable para el contador

            try
            {
                //Verificar si la variable de sesion de los archivos a agregar existe
                if (HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] != null)
                {
                    //Colocar la variable de sesion en la tabla
                    Dt_Archivos = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones]);

                    //Verificar si la tabla tiene elementos
                    if (Dt_Archivos.Rows.Count > 0)
                    {
                        //Verificar si existe la variable de sesion
                        if (HttpContext.Current.Session[P_Dt_Archivos_Eliminados] == null)
                        {
                            //Definir las columnas
                            Dt_Archivos_Eliminados.Columns.Add("No_Archivo", typeof(Int64));
                            Dt_Archivos_Eliminados.Columns.Add("Folio", typeof(String));
                            Dt_Archivos_Eliminados.Columns.Add("Proveedor", typeof(String));
                            Dt_Archivos_Eliminados.Columns.Add("Nombre_Archivo", typeof(String));
                            Dt_Archivos_Eliminados.Columns.Add("Descripcion", typeof(String));
                            Dt_Archivos_Eliminados.Columns.Add("Proveedor_ID", typeof(String));
                            Dt_Archivos_Eliminados.Columns.Add("Original", typeof(String));
                        }
                        else
                        {
                            //Colocar variable de sesion en la tabla
                            Dt_Archivos_Eliminados = ((DataTable)HttpContext.Current.Session[P_Dt_Archivos_Eliminados]);
                        }

                        //ciclo para el barrido de la tabla
                        for (Cont_Elementos = 0; Cont_Elementos < Dt_Archivos.Rows.Count; Cont_Elementos++)
                        {
                            //verificar que el renglon no haya sido eliminado
                            if (Dt_Archivos.Rows[Cont_Elementos].RowState != DataRowState.Deleted)
                            {
                                //Verificar si el elemento es el que se tiene que eliminar
                                if (Convert.ToInt64(Dt_Archivos.Rows[Cont_Elementos]["No_Archivo"]) == No_Archivo)
                                {                                   
                                    //Instanciar el renglon
                                    Renglon = Dt_Archivos.Rows[Cont_Elementos];

                                    //Colocar el renglon en la tabla de los eliminados
                                    Dt_Archivos_Eliminados.ImportRow(Renglon);

                                    //Eliminar el renglon de la tabla de los archivos
                                    Dt_Archivos.Rows.RemoveAt(Cont_Elementos);

                                    //Salir del ciclo
                                    break;
                                }
                            }
                        }

                        //actualizar las variables de sesion
                        HttpContext.Current.Session[P_Dt_Archivos_Cotizaciones] = Dt_Archivos;
                        HttpContext.Current.Session[P_Dt_Archivos_Eliminados] = Dt_Archivos_Eliminados;

                        //Llenar el grid
                        Llena_Grid_Archivos(Convert.ToInt32(Txt_No_Requisicion.Value));
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.ToString(), ex);
            }
        }
    #endregion

    #region Eventos
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                //Salir
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "window.close();", true);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Salir_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
        {
            //Declaracion de variables
            int No_Requisicion; //variable para el numero de la requisicion
            
            try
            {
                //Verificar el tooltip del boton
                if (Btn_Eliminar.ToolTip == "Cancelar")
                {
                    Estado_Inicial();
                }
                else
                {
                    //Obtener el numero de la requisicion
                    No_Requisicion = Convert.ToInt32(Txt_No_Requisicion.Value);

                    Elimina_Archivos(No_Requisicion);

                    Estado_Inicial();
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Eliminar_Click) " + ex.ToString(), true);
            }
        }

        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            //Declaracion de variables
            int No_Requisicion; //variable para el numero de la requisicion

            try
            {
                //Obtener el numero de la requisicion
                No_Requisicion = Convert.ToInt32(Txt_No_Requisicion.Value);

                Alta_Archivos(No_Requisicion);

                Estado_Inicial();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Actualización Exitosa');", true);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Eliminar_Click) " + ex.ToString(), true);
            }
        }

        protected void AFil_Archivo_UploadedComplete(object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
        {
            //Declaracion de variables
            String Ruta = String.Empty; //variable para la ruta de los archivos

            try
            {
                //Verificar si se tiene un archivo
                if (AFil_Archivo.HasFile)
                {
                    //Obtener la truta del servidor
                    Ruta = HttpContext.Current.Server.MapPath("~");

                    //Verificar si el directorio ya ha sido creado
                    if (Directory.Exists(Ruta + "\\Cot_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim()) == false)
                    {
                        Directory.CreateDirectory(Ruta + "\\Cot_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim());
                    }

                    Ruta = Ruta + "\\Cot_Archivos\\temporal\\RQ-" + Txt_No_Requisicion.Value.Trim() + "\\" + System.IO.Path.GetFileName(AFil_Archivo.FileName);

                    //Colocar los datos en los controles ocultos
                    HttpContext.Current.Session[P_Nombre_Archivo] = AFil_Archivo.FileName;

                    AFil_Archivo.SaveAs(Ruta);
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (AFil_Archivo_UploadedComplete) " + ex.ToString(), true);
            }
        }

        protected void Btn_Archivos_Requisicion_Click(object sender, EventArgs e)
        {
            try
            {
                //verificar si hay comentarios y si se subio el archivo
                if (Txt_Comentarios_Archivo.Text.Trim() == null || Txt_Comentarios_Archivo.Text.Trim() == String.Empty || HttpContext.Current.Session[P_Nombre_Archivo] == null)
                {
                    Mostrar_Informacion("Favor de seleccionar un archivo y proporcionar los comentarios del mismo", true);
                }
                else
                {
                    Agregar_Archivo_Tabla(HttpContext.Current.Session[P_Nombre_Archivo].ToString().Trim(), Txt_Comentarios_Archivo.Text.Trim(), Txt_Proveedor_ID.Value.Trim(), Txt_Proveedor.Text.Trim(), Convert.ToInt32(Txt_No_Requisicion.Value));
                    Llena_Grid_Archivos(Convert.ToInt32(Txt_No_Requisicion.Value));

                    //Limpiar la caja de texto de los comentarios
                    Txt_Comentarios_Archivo.Text = "";
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (AFil_Archivo_UploadedComplete) " + ex.ToString(), true);
            }
        }

        protected void Btn_Seleccionar_Archivo_Click(object sender, EventArgs e)
        {
            //Declaracion de variables
            Int64 No_Archivo; //Variable para el numero del archivo

            try
            {
                //Obtener el numero del archivo
                No_Archivo = Convert.ToInt64(((ImageButton)sender).CommandArgument);

                Elimina_Archivo_Tabla(No_Archivo);
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Seleccionar_Archivo_Click) " + ex.ToString(), true);
            }
        }

    #endregion
        protected void Grid_Archivos_Cotizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            //Declaracion de variables
            HyperLink Hyp_Lnk_Archivo_src; //variable para el hyperlink del archivo
            String Ruta = String.Empty; //variable para la ruta del archivo
            ImageButton Btn_Seleccionar_Archivo_src; //variable para el boton de eliminar

            try
            {
                //verificar el tipo de renglon
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    //Instanciar el control
                    Hyp_Lnk_Archivo_src = ((HyperLink)e.Row.Cells[5].FindControl("Hyp_Lnk_Archivo"));

                    //Colocarle los detalles de la navegacion
                    Hyp_Lnk_Archivo_src.Text = "Ver";//e.Row.Cells[3].Text;
                    Ruta = "~/Cot_Archivos/";

                    //Verificar si es fija o temporal
                    if (e.Row.Cells[6].Text.Trim() == "NO")
                    {
                        Ruta += "Temporal/";
                    }

                    //Colocar el resto de la ruta
                    Ruta += e.Row.Cells[2].Text.Trim() + "/" + e.Row.Cells[4].Text.Trim();

                    //Asignar la URL
                    Hyp_Lnk_Archivo_src.NavigateUrl = Ruta;
                    Hyp_Lnk_Archivo_src.Target = "_blank";

                    //Verificar si esta en modo de edicion
                    Btn_Seleccionar_Archivo_src = ((ImageButton)e.Row.Cells[1].FindControl("Btn_Seleccionar_Archivo"));
                    if (Btn_Modificar.ToolTip == "Guardar")
                    {
                        Btn_Seleccionar_Archivo_src.Enabled = true;
                    }
                    else
                    {
                        Btn_Seleccionar_Archivo_src.Enabled = false;
                    }
                }
            }
            catch (Exception ex)
            {
                Mostrar_Informacion("Error: (Btn_Seleccionar_Archivo_Click) " + ex.Message, true);
            }
        }
}