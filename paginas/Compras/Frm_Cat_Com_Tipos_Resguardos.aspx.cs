﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Cat_Tipos_Resguardos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;

public partial class paginas_Compras_Frm_Cat_Com_Tipos_Resguardos : System.Web.UI.Page
{
    #region Variables
        private const int Const_Estado_Inicial = 0;
        private const int Const_Estado_Nuevo = 1;
        private const int Const_Estado_Modificar = 2;
        private const int Const_Estado_Buscar = 3;
        private static DataTable Dt_Tipos_Resguardo = new DataTable();
        private static string M_Busqueda = "";
    #endregion

    #region Page_Load
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //Habilitar Estatus Botones
            Estado_Componentes(0);
            Cargar_Grid();
        }
    }
    #endregion 

    #region Metodos

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Grid
    ///DESCRIPCIÓN: Realizar la consulta y llenar el grid con estos datos
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 05/Julio/2012 06:41:00 p.m.
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Grid()
    {
        try
        {
            Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocio = new Cls_Cat_Com_Tipos_Resguardos_Negocio();
            Tipos_Resguardos_Negocio.P_Nombre = M_Busqueda;
            Dt_Tipos_Resguardo = Tipos_Resguardos_Negocio.Consultar_Tipo_Resguardo();
            Grid_Tipos_Resguardo.DataSource = Dt_Tipos_Resguardo;
            Grid_Tipos_Resguardo.DataBind();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }

    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Cargar_Datos
    ///DESCRIPCIÓN: metodo que recibe el DataRow seleccionado de la grilla y carga los datos en los componetes del formulario
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 06/Julio/2012 12:39:54 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Cargar_Datos(DataRow Dr_Tipo_Resguardo)
    {
        try
        {
            Txt_ID.Text = Dr_Tipo_Resguardo[Cat_Com_Tipos_Resguardo.Campo_Tipo_Resguardo_ID].ToString();
            Txt_Nombre.Text = Dr_Tipo_Resguardo[Cat_Com_Tipos_Resguardo.Campo_Nombre].ToString();
            Txt_Descripcion.Text = Dr_Tipo_Resguardo[Cat_Com_Tipos_Resguardo.Campo_Descripcion].ToString();
            Cmb_Estatus.SelectedValue = Dr_Tipo_Resguardo[Cat_Com_Tipos_Resguardo.Campo_Estatus].ToString();
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }

    ///****************************************************************************************
    ///NOMBRE DE LA FUNCION:Mensaje_Error
    ///DESCRIPCION : Muestra el error
    ///PARAMETROS  : P_Texto: texto de un TextBox
    ///CREO        : Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO  : 05/Julio/2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACION:
    ///****************************************************************************************
    private void Mensaje_Error(String P_Mensaje)
    {
        Img_Error.Visible = true;
        Lbl_Error.Text += P_Mensaje + "</br>";
    }
    private void Mensaje_Error()
    {
        Img_Error.Visible = false;
        Lbl_Error.Text = "";
    }
    ///****************************************************************
    ///NOMBRE DE LA FUNCIION:Estado_Componentes
    ///DESCRIPCIÓN:Metodo para estableceer el estado de los componentes del formulario segun corresponda 
    ///PARAMETROS: P_Estado
    ///CREO:
    ///FECHA_CREO:04/Julio/2012 05:23 pm
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///***************************************************************
    private void Estado_Componentes(int P_Estado) 
    {
        switch (P_Estado) 
        {
            case(0): //Estado Inicial
                Mensaje_Error();
                Txt_Busqueda.Text = String.Empty;
                Txt_ID.Text = String.Empty;
                Txt_Nombre.Text = String.Empty;
                Txt_Descripcion.Text = String.Empty;
                Cmb_Estatus.SelectedIndex = -1;

                Txt_ID.Enabled = false;
                Txt_Nombre.Enabled = false;
                Txt_Descripcion.Enabled = false;
                Cmb_Estatus.Enabled = false;


                Btn_Busqueda.Enabled = true;
                Btn_Eliminar.Enabled = true;
                Btn_Modificar.Enabled = true;
                Btn_Nuevo.Enabled = true;
                Btn_Salir.Enabled = true;
                Btn_Busqueda.AlternateText = "Buscar";
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Salir.AlternateText = "Salir";


                Btn_Busqueda.ToolTip = "Consultar";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Eliminar.ToolTip = "Eliminar";
               
                Btn_Salir.ToolTip = "Salir";

                Grid_Tipos_Resguardo.Enabled = true;
                Grid_Tipos_Resguardo.Visible = true;
                Grid_Tipos_Resguardo.SelectedIndex = (-1);

                Btn_Busqueda.ImageUrl = "~/paginas/imagenes/paginas/busqueda.png";
                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";

                
                Btn_Nuevo.Visible = true;
                Btn_Modificar.Visible = true;
                Btn_Eliminar.Visible = true;
                Btn_Salir.Visible = true;

                //Configuracion_Acceso("Frm_Cat_Com_Unidades.aspx");
                break;
            case 1://Nuevo
                Mensaje_Error();
                Txt_ID.Text = String.Empty;
                Txt_Nombre.Text = String.Empty;
                Txt_Descripcion.Text = String.Empty;
                Cmb_Estatus.SelectedIndex = -1;

                Txt_Nombre.Enabled = true;
                Txt_Descripcion.Enabled = true;
                Cmb_Estatus.Enabled = true;

                Btn_Modificar.Enabled = false;
                Btn_Eliminar.Enabled = false;
                Btn_Salir.Enabled = true;
                Btn_Nuevo.Enabled = true;

                Grid_Tipos_Resguardo.SelectedIndex = (-1);
                Grid_Tipos_Resguardo.Enabled = false;

                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Modificar";
                Btn_Nuevo.AlternateText = "Guardar";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Nuevo.ToolTip = "Guardar";
                Btn_Salir.ToolTip = "Cancelar";

                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar_deshabilitado.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar_deshabilitado.png";

                Btn_Eliminar.Visible = false;
                Btn_Modificar.Visible = false;
                Btn_Nuevo.Visible = true;
                Btn_Salir.Visible = true;
                break;
            case 2: //Modificar
                Mensaje_Error();
                Txt_Nombre.Enabled = true;
                Txt_Descripcion.Enabled = true;
                Cmb_Estatus.Enabled = true;

                Btn_Eliminar.Enabled = false;
                Btn_Nuevo.Enabled = false;
                Btn_Modificar.Enabled = true;
                Btn_Salir.Enabled = true;

                Btn_Eliminar.AlternateText = "Eliminar";
                Btn_Modificar.AlternateText = "Guardar";
                Btn_Nuevo.AlternateText = "Nuevo";
                Btn_Salir.AlternateText = "Cancelar";

                Btn_Eliminar.ToolTip = "Eliminar";
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Salir.ToolTip = "Cancelar";

                Grid_Tipos_Resguardo.Enabled = false;


                Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar_deshabilitado.png";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo_deshabilitado.png";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";

                Btn_Eliminar.Visible = false;
                Btn_Salir.Visible = true;
                Btn_Nuevo.Visible = false;
                Btn_Modificar.Visible = true;
                break;
            case 3: //Buscar
                Mensaje_Error();
                Txt_Nombre.Enabled = false;
                Txt_Descripcion.Enabled = false;
                Cmb_Estatus.Enabled = false;

                Grid_Tipos_Resguardo.Enabled = true;

                break;
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Validar_Datos
    ///DESCRIPCIÓN: Guardar datos para dar de alta un nuevo registro de un servicio
    ///PARAMETROS: 
    ///CREO: jtoledo
    ///FECHA_CREO: 02/03/2011 10:45:17 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Resultado = true;
        try
        {
            if (Txt_Nombre.Text.Trim() == "")
            {
                Resultado = false;
                Mensaje_Error("Favor de ingresar el Nombre del Resguardo");
            }
            if (Txt_Descripcion.Text.Trim() == "")
            {
                Resultado = false;
                Mensaje_Error("Favor de ingresar la Descripción  del Resguardo");
            }
            if (Cmb_Estatus.SelectedIndex == 0)
            {
                Resultado = false;
                Mensaje_Error("Favor de seleccionar el Estatus");
            }
            if (!Txt_Descripcion.Text.Trim().Equals(""))
            {
                if (Txt_Descripcion.Text.Trim().Length >= 250)
                {
                    Txt_Descripcion.Text = Txt_Descripcion.Text.Trim().Substring(0, 250);
                }
            }

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
        return Resultado;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Alta_Tipo_Resguardo
    ///DESCRIPCIÓN: Realiza el alta de un nuevo registro de un Tipo Resguardo
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 06/Julio/2012 10:01:19
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Alta_Tipo_Resguardo()
    {
        try
        {
            if (Validar_Datos())
            {
                Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocios = new Cls_Cat_Com_Tipos_Resguardos_Negocio();
                Tipos_Resguardos_Negocios.P_Estatus = Cmb_Estatus.SelectedValue;
                Tipos_Resguardos_Negocios.P_Nombre = Txt_Nombre.Text.Trim();
                Tipos_Resguardos_Negocios.P_Descripcion = Txt_Descripcion.Text.Trim();
                Tipos_Resguardos_Negocios.Alta_Tipo_Resguardo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Tipos Resguardo", "alert('El Alta del Resguardo fue Exitoso');", true);
                Estado_Componentes(Const_Estado_Inicial);
                Txt_Busqueda.Text = "";
                Cargar_Grid();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }

    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Baja_Tipo_Resguardo
    ///DESCRIPCIÓN: dar de baja un registro de la base de datos de la tabla TIPOS_RESGUARDO
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 06/Julio/2012 10:36:53 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Baja_Tipo_Resguardo()
    {
        try
        {
            Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardos_Negocios = new Cls_Cat_Com_Tipos_Resguardos_Negocio();
            Tipos_Resguardos_Negocios.P_Tipo_Resguardo_ID = HttpUtility.HtmlDecode(Grid_Tipos_Resguardo.SelectedRow.Cells[1].Text.Trim());
            Tipos_Resguardos_Negocios.Elimnar_Tipo_Resguardo();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Tipos Resguardo", "alert('La Baja del Resguardo fue Exitosa');", true);
            Estado_Componentes(Const_Estado_Inicial);
            Txt_Busqueda.Text = "";
            Cargar_Grid();
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }

    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN: Modificar_Tipo_Resguardo
    ///DESCRIPCIÓN: Modifica un registro y lo guarda en la base de datos
    ///PARAMETROS: 
    ///CREO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_CREO: 02/04/2011 12:14:00 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Tipo_Resguardo()
    {
        try
        {
            if (Validar_Datos())
            {
                Cls_Cat_Com_Tipos_Resguardos_Negocio Tipos_Resguardo_Negocio = new Cls_Cat_Com_Tipos_Resguardos_Negocio();
                Tipos_Resguardo_Negocio.P_Tipo_Resguardo_ID= HttpUtility.HtmlDecode(Grid_Tipos_Resguardo.SelectedRow.Cells[1].Text.Trim());
                Tipos_Resguardo_Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
                Tipos_Resguardo_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                Tipos_Resguardo_Negocio.P_Descripcion = Txt_Descripcion.Text.Trim();
                Tipos_Resguardo_Negocio.Modifiacar_Tipo_resguardo();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Tipos Resguardo", "alert('La modificación del Resguardo fue Exitosa');", true);
                Estado_Componentes(Const_Estado_Inicial);
                Txt_Busqueda.Text = "";
                Cargar_Grid();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message, Ex);
        }
    }
    #endregion 

    #region Eventos 
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
            {
                Estado_Componentes(Const_Estado_Nuevo);
            }
            else
            {
                Alta_Tipo_Resguardo();
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Tipos_Resguardo.SelectedIndex > (-1))
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    Estado_Componentes(Const_Estado_Modificar);
                }
                else
                {
                    Modificar_Tipo_Resguardo();
                }
            }
            else
            {
                Mensaje_Error("Favor de seleccionar el Resguardo a modificar");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);

        }
    }
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Tipos_Resguardo.SelectedIndex > (-1))
            {
                Baja_Tipo_Resguardo();
            }
            else
            {
                Mensaje_Error("Favor de seleccionar el Resguardo a eliminar");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.AlternateText.Equals("Salir"))
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Estado_Componentes(Const_Estado_Inicial);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    protected void Btn_Busqueda_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Estado_Componentes(Const_Estado_Buscar);
            Grid_Tipos_Resguardo.SelectedIndex = (-1);
            M_Busqueda = Txt_Busqueda.Text.Trim();
            Cargar_Grid();

        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    #endregion 

    #region Eventos Grid
    protected void Grid_Tipos_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Tipos_Resguardo.SelectedIndex > (-1))
            {
                Cargar_Datos(Dt_Tipos_Resguardo.Rows[Grid_Tipos_Resguardo.SelectedIndex]);
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.Message);
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    #endregion


}
