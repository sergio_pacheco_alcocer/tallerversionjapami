﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Com_Seguimiento_Fechas_Bienes.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Com_Seguimiento_Requisiciones_Servicios_Generales"
    Title="Seguimiento a Requisiciones"%>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <link href="../../jquery-easyui/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../jquery-easyui/themes/icon.css" rel="stylesheet" type="text/css" />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script src="../../easyui/ui.datepicker-es.js" type="text/javascript"></script>
    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>
    <script src="../../javascript/Js_Ope_Com_Seguimiento_Rq_Bienes.js" type="text/javascript"></script>

    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        
                <%--Div Encabezado--%>
                <div id="Div_Encabezado">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Seguimiento de Fechas de la Requisición de Servicio por Bien
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <img id="Img_Warning" style="display:none" src="../../paginas/imagenes/paginas/sias_warning.png" alt="" />
                                     <span id="Lbl_Informacion" style="color:#990000"></span>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <img id="Img_Listar_Requisiciones" alt="" src="../../paginas/imagenes/paginas/icono_salir.png" class="Img_Button" />
                                <img id="Img_Salir" alt="" src="../../paginas/imagenes/paginas/icono_salir.png" class="Img_Button" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones">
                    <table style="width: 100%;">
                        <tr>
                            <td colspan="4">
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%;">
                                Unidad Responsable
                            </td>
                            <td colspan="3">
                                <select id="Cmb_Dependencia_Panel" style="width:95.9%;"/>
                            </td>
                        </tr>                        
                        <tr>
                            <td style="width: 18%;">
                                Nombre
                            </td>
                            <td colspan="3">
                                <input type="text" id="Txt_Nombre_Bien" style="width: 95.3%;"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%;">
                                Estatus
                            </td>
                            <td>
                                <select id="Cmb_Estatus" style="width:96%"/>
                            </td>
                            <td align="left" colspan="2">
                                Modelo                            
                                <input type="text" id="Txt_Modelo" style="width: 29.3%;"/>
                                &nbsp;&nbsp;
                                Número Inventario
                                <input type="text" id="Txt_No_Inventario" style="width: 29.3%;"/>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;">
                                Número de Serie
                            </td>
                            <td style="width: 35%;">
                                <input type="text" id="Txt_No_Serie" style="width: 94.3%;" />                
                            </td>
                            <td  align="left" colspan="2">
                                RQ    
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;                        
                                <input type="text" id="Txt_Rq" style="width:29%"/>   
                                &nbsp;&nbsp;                             
                                OS
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;  
                                <input type="text" id="Txt_Os" style="width:23%"/>
                                <img id="Img_Buscar" alt="" src="../../paginas/imagenes/paginas/busqueda.png" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4" style="width: 99%">
                                <br />
                            </td>
                        </tr>
                            <tr>
                                <td colspan="4" style="width: 99%">
                                    <div style="overflow: visible; height: 320px; width: 99%; vertical-align: top;">
                                        <table id="Tbl_Requisiciones">
                                        </table>
                                    </div>
                                </td>
                            </tr>
                    </table>
                </div>                
    </div>
</asp:Content>
