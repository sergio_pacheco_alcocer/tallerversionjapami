﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cancelar_Requisiciones.Negocio;

public partial class paginas_Compras_Frm_Ope_Com_Cancelar_Requisiciones : System.Web.UI.Page
{
    #region Page_Load

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Metodo que se carga cada que ocurre un PostBack de la Página
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Session["Activa"] = true;
                Configuracion_Formulario(true);
                Llenar_Grid_Listado(0);
            }
        }

    #endregion

    #region Metodos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Configuracion_Formulario
        ///DESCRIPCIÓN          : Carga una configuracion de los controles del Formulario
        ///PARAMETROS           : 1. Estatus. Estatus en el que se cargara la configuración
        ///                                   de los controles.
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Configuracion_Formulario(Boolean Estatus)
        {
            Btn_Modificar.Visible = true;
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Buscar.Visible = Estatus;
            Div_Contenedor_Msj_Error.Visible = false;
            Div_Datos_Generales.Visible = !Estatus;
            Div_Campos.Visible = Estatus;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Listado
        ///DESCRIPCIÓN          : Llena el Listado con una consulta que puede o no
        ///                       tener Filtros.
        ///PARAMETROS           : 1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Llenar_Grid_Listado(int Pagina)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Cls_Ope_Com_Cancelar_Requisiciones_Negocio Negocio = new Cls_Ope_Com_Cancelar_Requisiciones_Negocio();
                Negocio.P_Folio = Txt_Busqueda.Text.Trim();
                Negocio.P_Tipo = "TRANSITORIA";
                Negocio.P_Tipo_Articulo = "PRODUCTO";
                Negocio.P_Estatus = "AUTORIZADA', 'PROVEEDOR', 'FILTRADA";
                Grid_Listado.Columns[1].Visible = true;
                Grid_Listado.DataSource = Negocio.Consultar_Requisiciones();
                Grid_Listado.PageIndex = Pagina;
                Grid_Listado.DataBind();
                Grid_Listado.Columns[1].Visible = false;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Catalogo
        ///DESCRIPCIÓN          : Limpia los controles del Formulario
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        private void Limpiar_Catalogo()
        {
            Txt_Codigo_Programatico.Text = "";
            Txt_Comentario.Text = "";
            Txt_Folio.Text = "";
            Txt_Justificacion.Text = "";
            Txt_Tipo.Text = "";
            Txt_Total.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
            Hdf_Requisicion_ID.Value = String.Empty;
        }

        #region Validaciones

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Componentes
            ///DESCRIPCIÓN          : Hace una validacion de que haya datos en los componentes
            ///                       antes de hacer una operación.
            ///PROPIEDADES          :
            ///CREO                 : Salvador Vázquez Camacho.
            ///FECHA_CREO           : 20/Marzo/2013
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private Boolean Validar_Componentes()
            {
                Lbl_Mensaje_Error.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                
                if (Cmb_Estatus.SelectedIndex == 0)
                {
                    Mensaje_Error += "<br/>+ Seleccionar una opcion del Combo de Estatus.";
                    Validacion = false;
                }
                if (String.IsNullOrEmpty(Txt_Comentario.Text))
                {
                    Mensaje_Error = Mensaje_Error + "<br/>+ Ingresar un comentario.";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

        #endregion

    #endregion

    #region Eventos

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Deja los componentes listos para hacer la modificacion.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, EventArgs e)
        {
            try
            {
                if (Btn_Modificar.AlternateText.Equals("Modificar"))
                {
                    if (Grid_Listado.Rows.Count > 0 && Grid_Listado.SelectedIndex > (-1))
                    {
                        Configuracion_Formulario(false);
                        Cmb_Estatus.Enabled = true;
                        Txt_Comentario.Enabled = true;
                        Btn_Modificar.AlternateText = "Actualizar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Salir.AlternateText = "Cancelar";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "Debe seleccionar el Registro que se desea Modificar.";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    if (Validar_Componentes())
                    {
                        Cls_Ope_Com_Cancelar_Requisiciones_Negocio Negocio = new Cls_Ope_Com_Cancelar_Requisiciones_Negocio();
                        Negocio.P_Requisicion_ID = Hdf_Requisicion_ID.Value;
                        Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
                        Negocio.P_Comentarios = Txt_Comentario.Text;
                        Negocio.Modificar_Requisicion();
                        Configuracion_Formulario(true);
                        Limpiar_Catalogo();
                        Llenar_Grid_Listado(Grid_Listado.PageIndex);
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo", "alert('Actualización Exitosa');", true);
                        Cmb_Estatus.Enabled = false;
                        Txt_Comentario.Enabled = false;
                        Btn_Modificar.AlternateText = "Modificar";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Btn_Salir.AlternateText = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Cancela la operación que esta en proceso 
        ///                       (Alta o Actualizar) o Sale del Formulario.
        ///PROPIEDADES          :
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, EventArgs e)
        {
            Div_Contenedor_Msj_Error.Visible = false;
            if (Div_Datos_Generales.Visible)
            {
                if (Btn_Modificar.AlternateText == "Actualizar")
                {
                    Cmb_Estatus.Enabled = false;
                    Txt_Comentario.Enabled = false;
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
                else
                    Response.Redirect("../Compras/Frm_Ope_Com_Cancelar_Requisiciones.aspx");
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Listado_PageIndexChanging
        ///DESCRIPCIÓN          : Maneja la pagiación del Grid.
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Listado_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado.SelectedIndex = (-1);
                Llenar_Grid_Listado(e.NewPageIndex);
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Click
        ///DESCRIPCIÓN          : Evento que controla la busqueda de requisicion por folio.
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
        {
            Llenar_Grid_Listado(0);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Listado_SelectedIndexChanged
        ///DESCRIPCIÓN          : Metodo para cargar los datos del elemento seleccionado
        ///PARAMETROS           : 
        ///CREO                 : Salvador Vázquez Camacho.
        ///FECHA_CREO           : 20/Marzo/2013
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Listado_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado.SelectedIndex > (-1))
                {
                    Limpiar_Catalogo();
                    Configuracion_Formulario(false);
                    Cls_Ope_Com_Cancelar_Requisiciones_Negocio Negocio = new Cls_Ope_Com_Cancelar_Requisiciones_Negocio();
                    Hdf_Requisicion_ID.Value = HttpUtility.HtmlDecode(Grid_Listado.SelectedRow.Cells[1].Text.Trim());
                    Negocio.P_Requisicion_ID = Hdf_Requisicion_ID.Value;
                    DataTable Dt_Requisicion = Negocio.Consultar_Requisiciones();

                    if (Dt_Requisicion.Rows.Count > 0)
                    {
                        Txt_Codigo_Programatico.Text = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Codigo_Programatico].ToString();
                        Txt_Folio.Text = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Folio].ToString();
                        Txt_Justificacion.Text = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Justificacion_Compra].ToString();
                        Txt_Tipo.Text = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Tipo].ToString();
                        Txt_Total.Text = Dt_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Total].ToString();
                    }

                    Grid_Productos.DataSource = Negocio.Consultar_Productos_Requisicion();
                    Grid_Productos.DataBind();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message;
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

        protected void Txt_Busqueda_TextChanged(object sender, EventArgs e)
        {
            Llenar_Grid_Listado(0);
        }
}
