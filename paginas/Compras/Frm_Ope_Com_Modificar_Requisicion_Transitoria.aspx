﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Com_Modificar_Requisicion_Transitoria.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Com_Modificar_Requisicion_Transitoria" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
 <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript" language="javascript">

        function Limpiar_Filtro_Fehca() {
            document.getElementById("<%=Txt_Fecha_Inicial.ClientID%>").value = "";
            document.getElementById("<%=Txt_Fecha_Final.ClientID%>").value = "";
            return false;
        }
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">

    <script src="../../jquery-easyui/jquery.easyui.min.js" type="text/javascript"></script>

    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                DisplayAfter="0">
                <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div style="height: 900px;">
                <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Modificar Requisici&oacute;n Transitoria
                        </td>
                    </tr>
                    <tr align="left">
                        <td>
                            <div id="Div_Contenedor_Msj_Error" runat="server">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" />
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" valign="middle">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar"
                                OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <div id="Div_Listado_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                Unidad Responsable
                            </td>
                            <td>
                                <asp:DropDownList ID="Cmb_Buscar_Unidad_Responsable" runat="server" Width="98%" />
                            </td>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicial" runat="server" TargetControlID="Txt_Fecha_Inicial"
                                    PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" TargetControlID="Txt_Fecha_Final"
                                    PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Final" />
                                <asp:ImageButton ID="Btn_Limpiar" runat="server" Width="20px" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                    ToolTip="Limpiar Fechas" OnClientClick="javascript:return Limpiar_Filtro_Fehca();" />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                Estatus
                            </td>
                            <td>
                                <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="98%">
                                    <asp:ListItem Value="TODAS" Text="TODAS" />
                                    <asp:ListItem Value="AUTORIZADA" Text="AUTORIZADA" />
                                    <asp:ListItem Value="COTIZADA" Text="COTIZADA" />
                                    <asp:ListItem Value="EN CONSTRUCCION" Text="EN CONSTRUCCION" />
                                    <asp:ListItem Value="GENERADA" Text="GENERADA" />
                                    <asp:ListItem Value="CANCELADA" Text="CANCELADA" />
                                    <asp:ListItem Value="CERRADA" Text="CERRADA" />
                                    <asp:ListItem Value="SURTIDA_PARCIAL" Text="SURTIDA PARCIAL" />
                                    <asp:ListItem Value="FILTRADA" Text="FILTRADA" />
                                    <asp:ListItem Value="CANCELA" Text="CANCELA" />
                                    <asp:ListItem Value="COMPRA" Text="COMPRA" />
                                    <asp:ListItem Value="SURTIDA" Text="SURTIDA" />
                                    <asp:ListItem Value="INACTIVA" Text="INACTIVA" />
                                    <asp:ListItem Value="COMPLETA" Text="COMPLETA" />
                                    <asp:ListItem Value="CONFIRMADA" Text="CONFIRMADA" />
                                    <asp:ListItem Value="PROVEEDOR" Text="PROVEEDOR" />
                                    <asp:ListItem Value="ALMACEN" Text="ALMACEN" />
                                </asp:DropDownList>
                            </td>
                            <td>
                                Folio
                            </td>
                            <td>
                                <asp:TextBox ID="Txt_Busqueda" runat="server" Width="85%" MaxLength="13" AutoPostBack="true"
                                    ontextchanged="Txt_Busqueda_TextChanged"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Buscar" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    OnClick="Btn_Buscar_Click" ToolTip="Consultar" />
                                <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                    WatermarkText="&lt;RQ-000000&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" FilterType="Custom"
                                    TargetControlID="Txt_Busqueda" ValidChars="rRqQ-0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                                <asp:GridView ID="Grid_Requisiciones" runat="server" AllowPaging="false" CssClass="GridView_1"
                                    AutoGenerateColumns="false" GridLines="None" Width="100%" 
                                     OnSelectedIndexChanged="Grid_Requisiciones_SelectedIndexChanged"
                                    OnRowDataBound="Grid_Requisiciones_RowDataBound" EmptyDataText="No se encontraron requisiciones">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle HorizontalAlign="Center" Width="2%" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="No_Requisicion"></asp:BoundField>
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" SortExpression="No_Requisicion">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha Inicial" DataFormatString="{0:dd/MMM/yyyy}"
                                            SortExpression="Fecha_Creo">
                                            <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Alerta" runat="server" ImageUrl="~/paginas/imagenes/gridview/circle_grey.png"
                                                    CommandArgument='<%# Eval("No_Requisicion") %>' />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="ALERTA" HeaderText="Alerta" SortExpression="Estatus">
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
                <div id="Div_Contenido" runat="server">
                    <asp:HiddenField ID="Hdf_Requisicion_ID" runat="server" />
                    <asp:HiddenField ID="Hdf_Monto_Inicial" runat="server" />
                    <asp:HiddenField ID="Hdf_Productos" runat="server" />
                    <asp:HiddenField ID="Hdf_Disponible" runat="server" />
                    <table style="width: 100%;">
                        <tr>
                            <td align="left">
                                Folio
                            </td>
                            <td align="left">
                                <asp:TextBox ID="Txt_Folio" runat="server" Width="96%" ReadOnly="true" />
                            </td>
                            <td align="left" style="width: 10%;" >
                                Estatus
                            </td>
                            <td align="left" style="width: 40%;"  >
                                <asp:TextBox ID="Txt_Estatus" runat="server" Width="38%" ReadOnly="true" />
                                &nbsp;&nbsp;&nbsp; Fecha
                                <asp:TextBox ID="Txt_Fecha" runat="server" Width="39%" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 20%;" align="left">
                                Unidad Responsable
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="Txt_Unidad_Responsable" runat="server" Width="99%" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Fuente de Financiamiento
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="Txt_Fte_Financiamiento" runat="server" Width="99%" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Programa
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="Txt_Programa" runat="server" Width="99%" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Partida
                            </td>
                            <td colspan="3" align="left" valign="top">
                                <asp:TextBox ID="Txt_Partida" runat="server" Width="99%" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Disponible
                            </td>
                            <td colspan="3" align="left">
                                <asp:Label ID="Lbl_Disponible_Partida" runat="server" Text=" $ 0.00" ForeColor="Blue"
                                    BorderColor="Blue" BorderWidth="2px" Width="38%">
                                </asp:Label>
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="Grid_Productos_Servicios" runat="server" AutoGenerateColumns="False"
                        CssClass="GridView_1" AllowPaging="false" PageSize="5" GridLines="None" Width="100%"
                        DataKeyNames="Tipo,Prod_Serv_ID,Monto_IVA,Monto_IEPS" OnPageIndexChanging="Grid_Productos_Servicios_PageIndexChanging"
                        OnRowDataBound="Grid_Productos_Servicios_RowDataBound">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:BoundField DataField="Ope_Com_Req_Producto_ID" ItemStyle-Font-Size="0%" /><%-- 0 --%>
                            <asp:BoundField DataField="Prod_Serv_ID" ItemStyle-Font-Size="0%" /><%-- 1 --%>
                            <asp:BoundField DataField="Nombre_Producto_Servicio" HeaderText="Producto/Servicio"><%-- 2 --%>
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="56%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Cantidad" ItemStyle-Font-Size="0%" /><%-- 3 --%>
                            <asp:TemplateField HeaderText="Ctd."><%-- 4 --%>
                                <ItemTemplate>
                                    <asp:TextBox ID="Txt_Cantidad" runat="server" Font-Size="X-Small" />
                                </ItemTemplate>
                                <ItemStyle Width="20%" />
                            </asp:TemplateField>
                            <asp:BoundField DataField="Precio_Unitario" HeaderText="$ Unitario S/I"><%-- 5 --%>
                                <HeaderStyle HorizontalAlign="Right" />
                                <ItemStyle HorizontalAlign="Right" Width="12%" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Monto" ItemStyle-Font-Size="0%" /><%-- 6 --%>
                            <asp:BoundField DataField="Porcentaje_IEPS" ItemStyle-Font-Size="0%" /><%-- 7 --%>
                            <asp:BoundField DataField="Porcentaje_IVA" ItemStyle-Font-Size="0%" /><%-- 8 --%>
                            <asp:BoundField DataField="Monto_IEPS" ItemStyle-Font-Size="0%" /><%-- 9 --%>
                            <asp:BoundField DataField="Monto_IVA" ItemStyle-Font-Size="0%" /><%-- 10 --%>
                            <asp:BoundField DataField="Monto_Total" ItemStyle-Font-Size="0%" /><%-- 11 --%>
                            <asp:TemplateField HeaderText="$ Importe"><%-- 12 --%>
                                <ItemTemplate>
                                    <asp:Label ID="Lbl_Importe" runat="server"></asp:Label>
                                </ItemTemplate>
                                <ItemStyle HorizontalAlign="Right" Width="12%" />
                            </asp:TemplateField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                    <table width="100%">
                        <tr>
                            <td align="right">
                                Subtotal
                                <asp:TextBox ID="Txt_Subtotal" runat="server" Style="text-align: right" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                IEPS
                                <asp:TextBox ID="Txt_IEPS" runat="server" Style="text-align: right" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                IVA
                                <asp:TextBox ID="Txt_IVA" runat="server" Style="text-align: right" ReadOnly="true"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="right">
                                Total
                                <asp:TextBox ID="Txt_Total" runat="server" Style="text-align: right" ReadOnly="true" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <hr class="linea" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:CheckBox ID="Chk_Verificar" runat="server" Text="Verificar características de productos"
                                    Enabled="false" ToolTip="Verificar las características, garantía y pólizas de mantenimiento de la mercancía cuando se reciba del proveedor" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                *Justificación de la compra
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="Txt_Justificacion" runat="server" MaxLength="1500" TabIndex="10"
                                    TextMode="MultiLine" Width="99%" Height="35px" ReadOnly="true"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender3" runat="server" TargetControlID="Txt_Justificacion"
                                    WatermarkText="&lt;Límite de Caracteres 1500&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTE_Justificacion" runat="server" TargetControlID="Txt_Justificacion"
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                Especificaciones adicionales
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:TextBox ID="Txt_Especificaciones" runat="server" MaxLength="1500" TabIndex="10"
                                    TextMode="MultiLine" Width="99%" Height="35px" ReadOnly="true"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender2" runat="server" TargetControlID="Txt_Especificaciones"
                                    WatermarkText="&lt;Límite de Caracteres 1500&gt;" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTE_Especificaciones" runat="server" TargetControlID="Txt_Especificaciones"
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                    </table>
                    <asp:GridView ID="Grid_Comentarios" runat="server" AllowPaging="true" AutoGenerateColumns="false"
                        GridLines="None" PageSize="10" Width="100%" OnPageIndexChanging="Grid_Comentarios_PageIndexChanging"
                        Style="font-size: xx-small; white-space: normal">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:BoundField DataField="Comentario" HeaderText="Comentarios" Visible="True">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Font-Size="Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                <HeaderStyle HorizontalAlign="Left" />
                                <ItemStyle HorizontalAlign="Left" Width="10%" Font-Size="Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Fecha_Creo" HeaderText="Fecha" Visible="True">
                                <HeaderStyle HorizontalAlign="Left" Width="18%" />
                                <ItemStyle HorizontalAlign="Left" Width="18%" Font-Size="X-Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="Usuario_Creo" HeaderText="Usuario" Visible="True">
                                <HeaderStyle HorizontalAlign="Left" Width="25%" />
                                <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="X-Small" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />
                        <AlternatingRowStyle CssClass="GridAltItem" />
                    </asp:GridView>
                </div>
            </div>

            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {

                        $("[id$='Txt_Cantidad']").numberbox({ min: 0, max: 1000000, precision: 2 });

                        $("#<%=Grid_Productos_Servicios.ClientID%> [id*='Txt_Cantidad']").change(function() {

                            var tr = $(this).parent().parent();
                            var Cantidad = $(this).val();
                            var Precio_Unitario = $("td:eq(5)", tr).html();
                            var Porcentaje_IEPS = $("td:eq(7)", tr).html();
                            var Porcentaje_IVA = $("td:eq(8)", tr).html();

                            if (parseFloat(Cantidad) < 0) {
                                Cantidad = 0;
                            }

                            var Monto = Cantidad * Precio_Unitario;
                            var Monto_IEPS = Monto * (Porcentaje_IEPS / 100);
                            var Monto_IVA = Monto * (Porcentaje_IVA / 100);
                            var Monto_Total = Monto + Monto_IEPS + Monto_IVA;

                            $("td:eq(3)", tr).html(Cantidad);
                            $("td:eq(6)", tr).html(Monto);
                            $("td:eq(9)", tr).html(Monto_IEPS);
                            $("td:eq(10)", tr).html(Monto_IVA);
                            $("td:eq(11)", tr).html(Monto_Total);
                            $("td:eq(12) span", tr).html(decimal(Monto, 2));

                            CalcularTotal();
                        });
                    });

                    function CalcularTotal() {

                        var Subtotal = 0;
                        var Total_IEPS = 0;
                        var Total_IVA = 0;
                        var Monto_Total = 0;
                        var Disponible = parseFloat($("#<%=Lbl_Disponible_Partida.ClientID %>").text().replace("$", "").replace(",", ""));
                        var Monto_Inicial = parseFloat($("#<%=Hdf_Monto_Inicial.ClientID %>").val());
                        var Productos = '';

                        $("#<%=Grid_Productos_Servicios.ClientID%> [id*='Lbl_Importe']").each(function() {
                            var tr = $(this).parent().parent();
                            var Ope_Com_Req_Producto_ID = parseInt($("td:eq(0)", tr).html());
                            var Cantidad = parseFloat($("td:eq(3)", tr).html());
                            var Monto = parseFloat($("td:eq(6)", tr).html());
                            var IEPS = parseFloat($("td:eq(9)", tr).html());
                            var IVA = parseFloat($("td:eq(10)", tr).html());
                            var Total = parseFloat($("td:eq(11)", tr).html());

                            if (isNaN(Ope_Com_Req_Producto_ID)) {
                                Ope_Com_Req_Producto_ID = 0;
                            }
                            if (isNaN(Cantidad)) {
                                Cantidad = 0;
                            }
                            if (!isNaN(Monto)) {
                                Subtotal += Monto;
                            }
                            if (!isNaN(IEPS)) {
                                Total_IEPS += IEPS;
                            }
                            if (!isNaN(IVA)) {
                                Total_IVA += IVA;
                            }
                            if (!isNaN(Total)) {
                                Monto_Total += Total;
                            }
                            Productos += Ope_Com_Req_Producto_ID + ',' + Cantidad + ',' + Monto + ',' + IEPS + ',' + IVA + ',' + Total + ';';
                        });

                        Subtotal = decimal(Subtotal, 2);
                        Total_IEPS = decimal(Total_IEPS, 2);
                        Total_IVA = decimal(Total_IVA, 2);
                        Monto_Total = decimal(Monto_Total, 2);

                        Productos += Subtotal + ',' + Total_IEPS + ',' + Total_IVA + ',' + Monto_Total;

                        document.getElementById("<%=Hdf_Productos.ClientID%>").value = Productos;
                        document.getElementById("<%=Txt_Subtotal.ClientID%>").value = Subtotal;
                        document.getElementById("<%=Txt_IEPS.ClientID%>").value = Total_IEPS;
                        document.getElementById("<%=Txt_IVA.ClientID%>").value = Total_IVA;
                        document.getElementById("<%=Txt_Total.ClientID%>").value = Monto_Total;

                        if (isNaN(Disponible)) {
                            Disponible = 0.0;
                        }

                        var Diferencia = 0;

                        if (Monto_Inicial < Monto_Total) {
                            Diferencia = Disponible - (Monto_Total - Monto_Inicial);
                        }
                        else {
                            Diferencia = Disponible + (Monto_Inicial - Monto_Total);
                        }


                        if (Diferencia < 0) {
                            alert("El presupuesto disponible es insuficiente.");
                        }
                        else {
                            document.getElementById("<%=Lbl_Disponible_Partida.ClientID%>").innerHTML = '$ ' + decimal(Diferencia, 2);
                            document.getElementById("<%=Hdf_Monto_Inicial.ClientID%>").value = Monto_Total;
                        }
                        document.getElementById("<%=Hdf_Disponible.ClientID%>").value = decimal(Diferencia, 2);
                    }
                }

                Number.prototype.decimal = function(num) {
                    pot = Math.pow(10, num);
                    return parseInt(this * pot) / pot;
                }

                function decimal(valor, no_decimales) {
                    n = eval(valor);
                    return n.decimal(no_decimales);
                } 
                
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
