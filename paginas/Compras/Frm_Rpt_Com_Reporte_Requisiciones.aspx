﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" CodeFile="Frm_Rpt_Com_Reporte_Requisiciones.aspx.cs" Inherits="paginas_Compras_Frm_Rpt_Com_Reporte_Requisiciones" %>

<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<script runat="server">  
   
</script>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function calendarShown(sender, args){
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
    </script> 
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True"/>
<asp:UpdatePanel ID="Upd_Panel" runat="server">
    <ContentTemplate>
        <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
               <%-- <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>--%>
        </ProgressTemplate>
        </asp:UpdateProgress>
        <%--Contenido del Formulario--%>
        <div id="Div_Contenido" style="width:99%;">
            <table border="0" cellspacing="0" class="estilo_fuente" width="100%">
                <tr>
                    <td colspan ="4" class="label_titulo">Reporte Requisicion</td>
                </tr>
                <%--Fila de div de Mensaje de Error --%>
                <tr>
                    <td colspan ="4">
                        <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                        <table style="width:100%;">
                            <tr>
                                <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                Width="24px" Height="24px"/>
                                </td>            
                                <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="Red" />
                                </td>
                            </tr> 
                        </table>                   
                        </div>
                    </td>
                </tr>
                <%--Renglon de barra de Busqueda--%>
                <tr class="barra_busqueda">
                    <td style="width:25%">
                    
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                            CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                            onclick="Btn_Salir_Click"/>
                        <asp:ImageButton ID="Btn_Exportar_PDF" runat="server" CssClass="Img_Button" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                            ToolTip="Exportar PDF" onclick="Btn_Generar_Reporte_Click" AlternateText="Consultar"/>
                        <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" CssClass="Img_Button" 
                            ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                            ToolTip="Exportar Excel" onclick="Btn_Exportar_Excel_Click" AlternateText="Consultar"/>
                            
                    </td>
                    <td align="right" colspan="3" style="width:75%;">
                        
                    </td>
                </tr> 
                <tr>
                    <td>
                        Gerencia
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Gerencia" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        UR
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_UR" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Cotizador
                    </td>
                    <td colspan="3">
                        <asp:DropDownList ID="Cmb_Cotizador" runat="server" Width="100%">
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td>
                        Productos
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Productos" runat="server" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Proovedores
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Proveedores" runat="server" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        Empleado Elaboro
                    </td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Empleado" runat="server" Width="100%"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td style="width:25%;">
                        Fecha
                    </td>
                    <td colspan="2">
                        <asp:TextBox ID="Txt_Fecha_Inicial" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="calentar_inicial" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                        <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                         &nbsp;&nbsp;&nbsp;&nbsp; al&nbsp;&nbsp;&nbsp;
                        <asp:TextBox ID="Txt_Fecha_Fin" runat="server"></asp:TextBox>
                        <cc1:CalendarExtender ID="calentar_final" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                        <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                    </td>
                    <td style="width:10%;" align="right">
                        <asp:ImageButton ID="Btn_Buscar" runat="server" 
                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" />
                    </td>
                </tr>
        </table>
      </div>
</ContentTemplate>
    
</asp:UpdatePanel>
</asp:Content>