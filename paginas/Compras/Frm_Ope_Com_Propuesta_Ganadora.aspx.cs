﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Propuesta_Ganadora.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Administrar_Requisiciones.Negocios;
using JAPAMI.Generar_Requisicion.Negocio;
public partial class paginas_Compras_Frm_Ope_Com_Propuesta_Ganadora : System.Web.UI.Page
{
    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region Page_Load
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (String.IsNullOrEmpty(Cls_Sessiones.No_Empleado) || String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Session["Activa"] = true;
            ViewState["SortDirection"] = "ASC";
            Configurar_Formulario("Inicio");
            Llenar_Grid_Requisiciones();
        }
    }

    #endregion


    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que configura el formulario con respecto al estado de habilitado o visible
    ///´de los componentes de la pagina
    ///PARAMETROS: 1.- String Estatus: Estatus que puede tomar el formulario con respecto a sus componentes, ya sea "Inicio" o "Nuevo"
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Configurar_Formulario(String Estatus)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";

        switch (Estatus)
        {
            case "Inicio":

                Div_Detalle_Requisicion.Visible = false;
                Div_Grid_Requisiciones.Visible = true;                
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                //
                Grid_Requisiciones.Visible = true;
                Grid_Requisiciones.Enabled = true;
                Div_Detalle_Requisicion.Visible = false;
                Grid_Productos.Enabled = false;
                Cmb_Estatus.Enabled = false;
                Cmb_Proveedores.Enabled = false;
                break;
            case "Nuevo":

                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Guardar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Modificar.Enabled = true;
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                //
                Div_Grid_Requisiciones.Visible = false;
                Div_Detalle_Requisicion.Visible = true;
                Grid_Productos.Enabled = true;
                Cmb_Estatus.Enabled = true;
                Cmb_Proveedores.Enabled = true;
                break;
        }//fin del switch

    }


    public void Limpiar_Componentes()
    {

        Session["Concepto_ID"] = null;
        Session["Dt_Productos"] = null;
        Session["Dt_Requisiciones"] = null;
        Session["No_Requisicion"] = null;
        Session["TIPO_ARTICULO"] = null;
        Session["Concepto_ID"] = null;

        Txt_Dependencia.Text = "";
        Txt_Concepto.Text = "";
        Txt_Folio.Text = "";
        Txt_Concepto.Text = "";
        Txt_Fecha_Generacion.Text = "";
        Txt_Tipo.Text = "";
        Txt_Tipo_Articulo.Text = "";
        Chk_Verificacion.Checked = false;
        Txt_Justificacion.Text = "";
        Txt_Especificacion.Text = "";
        Grid_Productos.DataSource = new DataTable();
        Grid_Productos.DataBind();
        Txt_SubTotal_Cotizado_Requisicion.Text = "";
        Txt_Total_Cotizado_Requisicion.Text = "";
        Txt_IEPS_Cotizado.Text = "";
        Txt_IVA_Cotizado.Text = "";
        Hdf_Listado_Almacen.Value = String.Empty;
        Hdf_RowIndex.Value = String.Empty;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Estatus
    ///DESCRIPCIÓN: Metodo que carga el combo Cmb_Estatus
    ///PARAMETROS:  
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Combo_Estatus()
    {
        Cmb_Estatus.Items.Clear();
        Cmb_Estatus.Items.Add(new ListItem("PROVEEDOR", "PROVEEDOR"));
        Cmb_Estatus.Items.Add(new ListItem("PROVEEDOR ASIGNADO", "PROVEEDOR_ASIGNADO"));
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Calcular_Importe_Total
    ///DESCRIPCIÓN: Metodo que calcula el importe total de la cotización
    ///PARAMETROS:  
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Calcular_Importe_Total()
    {
        DataTable Dt_Productos = (DataTable)Session["Dt_Productos"];
        double Total_Cotizado = 0;
        double IVA_Cotizado = 0;
        double IEPS_Cotizado = 0;
        double Subtotal_Cotizado = 0;
        if (Dt_Productos.Rows.Count != 0)
        {
            for (int i = 0; i < Dt_Productos.Rows.Count; i++)
            {
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim() != String.Empty)
                {
                    IVA_Cotizado = IVA_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado].ToString().Trim());
                }
                else
                {
                    IVA_Cotizado = IVA_Cotizado + 0;
                }

                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim() != String.Empty)
                {
                    IEPS_Cotizado = IEPS_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado].ToString().Trim());
                }
                else
                {
                    IEPS_Cotizado = IEPS_Cotizado + 0;
                }
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim() != String.Empty)
                {
                    Subtotal_Cotizado = Subtotal_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado].ToString().Trim());
                }
                else
                {
                    Subtotal_Cotizado = Subtotal_Cotizado + 0;
                }
                if (Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim() != String.Empty)
                {
                    Total_Cotizado = Total_Cotizado + double.Parse(Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado].ToString().Trim());
                }
                else
                {
                    Total_Cotizado = Total_Cotizado + 0 ;
                }
            }
        }
        Txt_Total_Cotizado_Requisicion.Text = String.Format("{0:c}", Total_Cotizado);
        Txt_SubTotal_Cotizado_Requisicion.Text = String.Format("{0:c}", Subtotal_Cotizado);
        Txt_IVA_Cotizado.Text = String.Format("{0:c}", IVA_Cotizado);
        Txt_IEPS_Cotizado.Text = String.Format("{0:c}", IEPS_Cotizado);
    }
    #endregion

    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #region Grid_Requisicion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Llenar_Grid_Requisiciones
    ///DESCRIPCIÓN: Metodo que permite llenar el Grid_Requisiciones
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Grid_Requisiciones()
    {
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        
        if (Txt_Requisicion_Busqueda.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_No_Requisicion = Txt_Requisicion_Busqueda.Text.Trim();
        }
        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Requisiciones.DataSource = Dt_Requisiciones;
            Grid_Requisiciones.DataBind();
        }
        else
        {
            Grid_Requisiciones.DataSource = new DataTable();
            Grid_Requisiciones.DataBind();
        }
        Txt_Requisicion_Busqueda.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Requisiciones_SelectedIndexChanged
    ///DESCRIPCIÓN: Evento del Grid_Requisiciones al seleccionar
    ///PARAMETROS:   
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Requisiciones_SelectedIndexChanged(object sender, EventArgs e)
    {

        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();

        GridViewRow Row = Grid_Requisiciones.SelectedRow;
        Clase_Negocio.P_No_Requisicion = Grid_Requisiciones.SelectedDataKey["No_Requisicion"].ToString();
        Session["No_Requisicion"] = Clase_Negocio.P_No_Requisicion;
        //llenamos combo estatus
        Llenar_Combo_Estatus();
        //Consultamos los detalles del producto seleccionado 
        DataTable Dt_Detalle_Requisicion = Clase_Negocio.Consultar_Detalle_Requisicion();
        //Mostramos el div de detalle y el grid de Requisiciones
        Div_Grid_Requisiciones.Visible = false;
        Div_Detalle_Requisicion.Visible = true;
        Btn_Salir.ToolTip = "Listado";
        //llenamos la informacion del detalle de la requisicion seleccionada
        Txt_Dependencia.Text = Dt_Detalle_Requisicion.Rows[0]["DEPENDENCIA"].ToString().Trim();
        Txt_Folio.Text = Dt_Detalle_Requisicion.Rows[0]["FOLIO"].ToString().Trim();
        Txt_Concepto.Text = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO"].ToString().Trim();
        Txt_Fecha_Generacion.Text = Dt_Detalle_Requisicion.Rows[0]["FECHA_GENERACION"].ToString().Trim();
        Txt_Tipo.Text = Dt_Detalle_Requisicion.Rows[0]["TIPO"].ToString().Trim();
        Txt_Tipo_Articulo.Text = Dt_Detalle_Requisicion.Rows[0]["TIPO_ARTICULO"].ToString().Trim();
        Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByText(Dt_Detalle_Requisicion.Rows[0]["ESTATUS"].ToString().Trim()));
        Txt_Justificacion.Text = Dt_Detalle_Requisicion.Rows[0]["JUSTIFICACION_COMPRA"].ToString().Trim();
        Txt_Especificacion.Text = Dt_Detalle_Requisicion.Rows[0]["ESPECIFICACION_PROD_SERV"].ToString().Trim();
        Txt_IEPS_Cotizado.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_IEPS_Cotizado].ToString().Trim();
        Txt_IVA_Cotizado.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_IVA_Cotizado].ToString().Trim();
        Txt_IEPS_Cotizado.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_IEPS_Cotizado].ToString().Trim();
        Txt_Total_Cotizado_Requisicion.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Total_Cotizado].ToString().Trim();
        Txt_SubTotal_Cotizado_Requisicion.Text = Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Subtotal_Cotizado].ToString().Trim();
        Hdf_Listado_Almacen.Value = String.IsNullOrEmpty(Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim())
            ? "NO"
            : Dt_Detalle_Requisicion.Rows[0][Ope_Com_Requisiciones.Campo_Listado_Almacen].ToString().Trim();
        Session["TIPO_ARTICULO"] = Txt_Tipo_Articulo.Text.Trim();
        Session["Concepto_ID"] = Dt_Detalle_Requisicion.Rows[0]["CONCEPTO_ID"].ToString().Trim();
        //VALIDAMOS EL CAMPO DE VERIFICAR CARACTERISTICAS, GARANTIA Y POLIZAS
        if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "NO" || Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == String.Empty)
        {
            Chk_Verificacion.Checked = false;
        }
        if (Dt_Detalle_Requisicion.Rows[0]["VERIFICACION_ENTREGA"].ToString().Trim() == "SI")
        {
            Chk_Verificacion.Checked = true;
        }
        //Consultamos los productos de esta requisicion
        Clase_Negocio.P_Tipo_Articulo = Session["TIPO_ARTICULO"].ToString().Trim();
        DataTable Dt_Productos = Clase_Negocio.Consultar_Productos_Servicios();
        if (Dt_Productos.Rows.Count != 0)
        {
            //Consultamos los proveedores
            Clase_Negocio.P_Concepto_ID = Session["Concepto_ID"].ToString().Trim();
            DataTable Dt_Proveedores = Clase_Negocio.Consultar_Proveedores();
            Session["Dt_Proveedores"] = Dt_Proveedores;

            Cmb_Proveedores.DataValueField = Dt_Proveedores.Columns[0].ColumnName;
            Cmb_Proveedores.DataTextField = Dt_Proveedores.Columns[1].ColumnName;
            Cmb_Proveedores.DataSource = Dt_Proveedores;
            Cmb_Proveedores.DataBind();
            Cmb_Proveedores.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode(" << SELECCIONAR >> "), ""));

            Session["Dt_Productos"] = Dt_Productos;
            Grid_Productos.DataSource = Dt_Productos;
            Grid_Productos.DataBind();
            Grid_Productos.Visible = true;
            Grid_Productos.Enabled = false;
            //Llenamos los Text Box con los datos del Dt_Productos
        }
        Div_Grid_Requisiciones.Visible = false;
        Div_Detalle_Requisicion.Visible = true;
        Btn_Salir.ToolTip = "Listado";

        Cls_Ope_Com_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Requisiciones_Negocio();
        Requisicion_Negocio.P_Requisicion_ID = Clase_Negocio.P_No_Requisicion;
        Llenar_Grid_Bienes(Requisicion_Negocio.Consultar_Bienes_Requisicion());
        Configurar_Formulario("Nuevo");
        Cmb_Estatus.Enabled = true;
    }

    /// *****************************************************************************************
    /// NOMBRE: Llenar_Grid_Bienes
    /// DESCRIPCIÓN: Llena el Grid de los Bienes 
    /// CREÓ: Francisco Antonio Gallardo Castañeda
    /// FECHA CREÓ: 19/Febrero/2013 11:13am
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    public void Llenar_Grid_Bienes(DataTable Dt_Datos)
    {
        Grid_Listado_Bienes.Columns[0].Visible = true;
        Grid_Listado_Bienes.DataSource = Dt_Datos;
        Grid_Listado_Bienes.DataBind();
        Grid_Listado_Bienes.Columns[0].Visible = false;
        if (Grid_Listado_Bienes.Rows.Count > 0) Pnl_Listado_Bienes.Visible = true; else Pnl_Listado_Bienes.Visible = false;
    } 

     ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Grid_Requisiciones_Sorting
    ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Requisiciones
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Requisiciones_Sorting(object sender, GridViewSortEventArgs e)
    {
    }
    #endregion Grid_Requisiciones

    #region Grid_Productos

    public void Llenar_Grid_Productos()
    {
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Requisiciones.DataSource = Dt_Requisiciones;
            Grid_Requisiciones.DataBind();
        }
        else
        {
            Grid_Requisiciones.DataSource = new DataTable();
            Grid_Requisiciones.DataBind();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:Grid_Productos_Sorting
    ///DESCRIPCIÓN: Evento para ordenar por columna seleccionada en el Grid_Productos
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Productos_Sorting(object sender, GridViewSortEventArgs e)
    {
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Productos_RowDataBound
    ///DESCRIPCIÓN         : Selecciona el cotizador de acuerdo al producto
    ///PARAMETROS          :
    ///CREO                : Susana Trigueros Armenta
    ///FECHA_CREO          : 01/JULIO/2011
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Grid_Productos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow && Session["Dt_Proveedores"] != null)
        {
            DropDownList Cmb_Proveedor = (DropDownList)e.Row.Cells[13].FindControl("Cmb_Proveedor");
            DataTable Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];

            Cmb_Proveedor.DataValueField = Dt_Proveedores.Columns[0].ColumnName;
            Cmb_Proveedor.DataTextField = Dt_Proveedores.Columns[1].ColumnName;
            Cmb_Proveedor.DataSource = Dt_Proveedores;
            Cmb_Proveedor.DataBind();
            Cmb_Proveedor.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode(" << SELECCIONAR >> "), ""));

            DataTable Dt_Productos = new DataTable();
            Dt_Productos = (DataTable)Session["Dt_Productos"];

            if (!String.IsNullOrEmpty(Hdf_RowIndex.Value) && e.Row.RowIndex == Convert.ToInt16(Hdf_RowIndex.Value))
            {
                String Proveedor_ID = Dt_Productos.Rows[Convert.ToInt16(Hdf_RowIndex.Value)]["Proveedor_ID"].ToString().Trim();

                Cmb_Proveedor.SelectedIndex = String.IsNullOrEmpty(Proveedor_ID) ? 0
                    : Cmb_Proveedor.Items.IndexOf(Cmb_Proveedor.Items.FindByValue(Proveedor_ID));
                Hdf_RowIndex.Value = String.Empty;
            }
            else
            {
                String Proveedor_ID = Dt_Productos.Rows[e.Row.RowIndex]["Proveedor_ID"].ToString().Trim();

                Cmb_Proveedor.SelectedIndex = String.IsNullOrEmpty(Proveedor_ID) ? 0
                    : Cmb_Proveedor.Items.IndexOf(Cmb_Proveedor.Items.FindByValue(Proveedor_ID));
            }
        }
    }

    #endregion

    #endregion

    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Evento del Boton Salir
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                //LIMPIAMOS VARIABLES DE SESSION
                Session["Dt_Requisiciones"] = null;
                Session["No_Requisicion"] = null;
                break;
            case "Cancelar":
                Configurar_Formulario("Inicio");
                Llenar_Grid_Requisiciones();
                Limpiar_Componentes();
                break;
            case "Listado":
                Configurar_Formulario("Inicio");
                Llenar_Grid_Requisiciones();
                Limpiar_Componentes();

                break;
        }
    }

    protected void Btn_Calcular_Precios_Cotizados_Click(object sender, EventArgs e)
    {
        DataTable Dt_Productos = new DataTable();
        Dt_Productos = (DataTable)Session["Dt_Productos"];
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();

        for (int i = 0; i < Dt_Productos.Rows.Count; i++)
        {
            Dt_Productos = (DataTable)Session["Dt_Productos"];
            DropDownList Cmb_Temporal_Proveedores = (DropDownList)Grid_Productos.Rows[i].FindControl("Cmb_Proveedor");
            //Asignamos los valores que se cotizaron en la propuesta del cotizador
            Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[i]["PROD_SERV_ID"].ToString().Trim();

            //Asignamos el proveedor seleccionado al grid
            if (Cmb_Temporal_Proveedores.SelectedIndex != 0)
            {
                Clase_Negocio.P_Proveedor_ID = Cmb_Temporal_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Proveedor_ID"] = Cmb_Temporal_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Nombre_Proveedor"] = Cmb_Temporal_Proveedores.SelectedItem;
                //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado del producto k corresponde a la posicion del for
                DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
            }
            Session["Dt_Productos"] = Dt_Productos;
        }
        //llenamos nuevamente el grid de productos
        Grid_Productos.DataSource = Dt_Productos;
        Grid_Productos.DataBind();
        //Cargamos nuevamente el grid con los proveedores y el proveedor seleccionado 
        DataTable Dt_Proveedores = Clase_Negocio.Consultar_Proveedores();
        for (int i = 0; i < Grid_Productos.Rows.Count; i++)
        {
            DropDownList Cmb_Temporal_Proveedores = (DropDownList)Grid_Productos.Rows[i].FindControl("Cmb_Proveedor");
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Temporal_Proveedores, Dt_Proveedores);
            Cmb_Temporal_Proveedores.SelectedIndex = 0;
            //Llenamos los Combos de acuerdo al proveedor que tiene asignado  && Cmb_Temporal_Proveedores.SelectedItem.Text.Contains(Dt_Productos.Rows[i]["Proveedor_ID"].ToString().Trim())
            if (Dt_Productos.Rows[i]["Proveedor_ID"].ToString().Trim() != String.Empty)
            {
                try
                {
                    Cmb_Temporal_Proveedores.SelectedIndex = Cmb_Temporal_Proveedores.Items.IndexOf(Cmb_Temporal_Proveedores.Items.FindByText(Dt_Productos.Rows[i]["Nombre_Proveedor"].ToString().Trim()));
                }
                catch
                {
                    //En caso de no encontrar el proveedor se selecciona vacio el combo
                    Cmb_Temporal_Proveedores.SelectedIndex = 0;
                }
            }
        }//Fin del for

        //CAlculamos el total cotizado
        Calcular_Importe_Total();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Evento del Boton Modificar
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 01/JULIO/2011
    ///MODIFICO: Jennyfer Ivonne Ceja Lemus
    ///FECHA_MODIFICO: 21/Noviembre/2012
    ///CAUSA_MODIFICACIÓN: Se agrego la validacion del monto de compra, ya que dependiendo 
    ///                    del monto de la compra se autorizara  o no por el usuario
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        String Mensaje_Error = String.Empty;
        String Mensaje_Descriptivo = "";
        String Proveedor_ID = String.Empty;
        Boolean Por_Dividir = false;
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        switch (Btn_Modificar.ToolTip)
        {
            case "Modificar":
                if (Txt_Folio.Text != String.Empty)
                {
                    Configurar_Formulario("Nuevo");
                    Cmb_Estatus.Enabled = true;
                }
                else
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario selecciona una Requisicion";
                }
                break;
            case "Guardar":
                Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                Clase_Negocio.P_Dt_Productos = (DataTable)Session["Dt_Productos"];
                Clase_Negocio.P_IEPS_Cotizado = String.IsNullOrEmpty(Txt_IEPS_Cotizado.Text.Trim()) ? "0" : Txt_IEPS_Cotizado.Text.Replace("$", "").Replace(",", "");
                Clase_Negocio.P_IVA_Cotizado = String.IsNullOrEmpty(Txt_IVA_Cotizado.Text.Trim()) ? "0" : Txt_IVA_Cotizado.Text.Replace("$", "").Replace(",", "");
                Clase_Negocio.P_Total_Cotizado = String.IsNullOrEmpty(Txt_Total_Cotizado_Requisicion.Text.Trim()) ? "0" : Txt_Total_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                Clase_Negocio.P_Subtotal_Cotizado = String.IsNullOrEmpty(Txt_SubTotal_Cotizado_Requisicion.Text.Trim()) ? "0" : Txt_SubTotal_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");

                foreach (DataRow Dr_Producto in Clase_Negocio.P_Dt_Productos.Rows)
                {
                    if (String.IsNullOrEmpty(Dr_Producto["Proveedor_ID"].ToString().Trim()))
                    {
                        Mensaje_Error += "</br>+ Es necesario asignar cotizador a todos los productos";
                        break;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Proveedor_ID))
                        {
                            Proveedor_ID = Dr_Producto["Proveedor_ID"].ToString().Trim();
                        }
                        else
                        {
                            if (!Por_Dividir)
                                Por_Dividir = (Proveedor_ID == Dr_Producto["Proveedor_ID"].ToString().Trim()) ? false : true;
                        }
                    }
                }

                if (Txt_Total_Cotizado_Requisicion.Text.Trim() == "0" || Txt_Total_Cotizado_Requisicion.Text.Trim() == "0.00")
                {
                    Mensaje_Error += "</br>+ No se puede guardar con un total cotizado en cero.";
                }

                if (String.IsNullOrEmpty(Mensaje_Error))
                {
                    Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("PROVEEDOR_ASIGNADO"));
                }
                
                    if (Cmb_Estatus.SelectedValue.Trim() == "PROVEEDOR")
                    {
                        if (String.IsNullOrEmpty(Mensaje_Error))
                        {
                            Clase_Negocio.P_Estatus = "PROVEEDOR";
                            if (Clase_Negocio.Modificar_Requisicion())
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Modifico la Cotizacion Exitosamente');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Modifico la Cotizacion');", true);
                            }
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Text = Mensaje_Error;
                            Div_Contenedor_Msj_Error.Visible = true;
                            break;
                        }
                    }
                    else
                    {
                        Double Total_Cotizado = 0;
                        //Convierte el total cotizado a tipo de dato Double
                        Double.TryParse(Txt_Total_Cotizado_Requisicion.Text.ToString(), out Total_Cotizado);
                        Double Parametro_Monto_Compra = Clase_Negocio.Consultar_Parametro_Monto_Compra();

                        if (String.IsNullOrEmpty(Txt_Total_Cotizado_Requisicion.Text.Trim()))
                        {
                            Mensaje_Error += "</br>+ Es necesario calcular los precios";
                        }


                        if (Parametro_Monto_Compra <= 0)
                        {
                            Mensaje_Error += "</br>+ Es necesario asignar el monto del parametro Monto de Compra en el catálogo ''Parametro Monto de Compra''";
                        }

                        if (Total_Cotizado < Parametro_Monto_Compra || Hdf_Listado_Almacen.Value.ToUpper().Trim() == "SI")
                        {
                            //Validar presupusto
                            Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                            //Checamos si existe presupuesto
                            Requisicion_Negocio.P_Total_Cotizado = Txt_Total_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                            Requisicion_Negocio.P_Requisicion_ID = Session["No_Requisicion"].ToString();

                            if (Requisicion_Negocio.Consultamos_Presupuesto_Existente())
                            {
                                Clase_Negocio.P_Estatus = Por_Dividir ? "POR_DIVIDIR" : "CONFIRMADA";
                                Mensaje_Descriptivo = " y se Confirmo ";
                            }
                            else
                            {
                                Mensaje_Error += "</br>+ No existe presupuesto suficiente.";
                            }
                        }
                        else
                        {
                            Clase_Negocio.P_Estatus = "COTIZADA";
                        }

                    
                    if (!String.IsNullOrEmpty(Mensaje_Error))
                    {
                        Lbl_Mensaje_Error.Text = Mensaje_Error;
                        Div_Contenedor_Msj_Error.Visible = true;
                        break;
                    }

                    if (Clase_Negocio.Agregar_Cotizaciones())
                    {
                        if (Clase_Negocio.P_Estatus == "CONFIRMADA")
                        {
                            Clase_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                            if (Clase_Negocio.Modificar_Requisicion_Monto_Menor_A_Parametro())
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Guardo la Cotizacion Ganadora Exitosamente " + Mensaje_Descriptivo + " ');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                            }
                        }
                        if (Clase_Negocio.P_Estatus == "COTIZADA" || Clase_Negocio.P_Estatus == "POR_DIVIDIR")
                        {
                            if (Clase_Negocio.Modificar_Requisicion())
                            {
                                if (Clase_Negocio.P_Estatus == "COTIZADA")
                                    Mensaje_Descriptivo = " y se envio a Autorizar ya que el total es mayor o igual a " + Parametro_Monto_Compra;
                                else
                                    Mensaje_Descriptivo = ". Requiere dividirse ya que se asigno a mas de un proveedor.";
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Guardo la Cotizacion Ganadora Exitosamente " + Mensaje_Descriptivo + "');", true);
                            }
                            else
                            {
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                            }
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                    }
                }

                Limpiar_Componentes();
                Llenar_Grid_Requisiciones();
                Configurar_Formulario("Inicio");
                break;

        }//fin del switch
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Proveedor_SelectedIndexChanged
    ///DESCRIPCIÓN         : Obtiene los precios cotizados para un producto.
    ///PARAMETROS          :
    ///CREO                : Susana Trigueros Armenta
    ///FECHA_CREO          : 01/JULIO/2011
    ///MODIFICO            :
    ///FECHA_MODIFICO      :
    ///CAUSA_MODIFICACIÓN  :
    ///*******************************************************************************
    protected void Cmb_Proveedor_SelectedIndexChanged(object sender, EventArgs e)
    {
        DropDownList Cmb_Proveedor = (DropDownList)sender;
        GridViewRow row = (GridViewRow)Cmb_Proveedor.Parent.Parent;

        int Row = row.RowIndex;
        //Sacamos el index que se esta modificando
        int Row_Producto = (Grid_Productos.PageIndex * Grid_Productos.PageSize) + Row;
        Hdf_RowIndex.Value = Row_Producto.ToString().Trim();

        DataTable Dt_Productos = new DataTable();
        Dt_Productos = (DataTable)Session["Dt_Productos"];
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
        Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[Row_Producto]["PROD_SERV_ID"].ToString().Trim();

        //Asignamos el proveedor seleccionado al grid
        if (Cmb_Proveedor.SelectedIndex > -1)
        {
            Clase_Negocio.P_Proveedor_ID = Cmb_Proveedor.SelectedValue;
            Dt_Productos.Rows[Row_Producto]["Proveedor_ID"] = Cmb_Proveedor.SelectedValue;
            Dt_Productos.Rows[Row_Producto]["Nombre_Proveedor"] = Cmb_Proveedor.SelectedItem;
            //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado
            DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
            if (Dt_Producto_Propuesta.Rows.Count > 0)
            {
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
            }
            else
            {
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = 0.0;
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = 0.0;
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = 0.0;
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Total_Cotizado] = 0.0;
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = 0.0;
                Dt_Productos.Rows[Row_Producto][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = 0.0;
            }
        }
        Session["Dt_Productos"] = Dt_Productos;

        Grid_Productos.DataSource = Dt_Productos;
        Grid_Productos.DataBind();

        Calcular_Importe_Total();
    }

    protected void Cmb_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Productos = new DataTable();
        Dt_Productos = (DataTable)Session["Dt_Productos"];
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();
        Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();

        for (int i = 0; i < Dt_Productos.Rows.Count; i++)
        {
            Dt_Productos = (DataTable)Session["Dt_Productos"];
            //Asignamos los valores que se cotizaron en la propuesta del cotizador
            Clase_Negocio.P_Producto_ID = Dt_Productos.Rows[i]["PROD_SERV_ID"].ToString().Trim();

            //Asignamos el proveedor seleccionado al grid
            if (Cmb_Proveedores.SelectedIndex > -1)
            {
                Clase_Negocio.P_Proveedor_ID = Cmb_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Proveedor_ID"] = Cmb_Proveedores.SelectedValue;
                Dt_Productos.Rows[i]["Nombre_Proveedor"] = Cmb_Proveedores.SelectedItem;
                //Consutamos el producto seleccionado para pasar los valores calculados de lo ya cotizado del producto k corresponde a la posicion del for
                DataTable Dt_Producto_Propuesta = Clase_Negocio.Consultar_Productos_Propuesta();
                if (Dt_Producto_Propuesta.Rows.Count > 0)
                {
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IEPS_Cotizado].ToString().Trim();
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_IVA_Cotizado].ToString().Trim();
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Subtota_Cotizado].ToString().Trim();
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Total_Cotizado].ToString().Trim();
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Sin_Imp_Cotizado].ToString().Trim();
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = Dt_Producto_Propuesta.Rows[0][Ope_Com_Propuesta_Cotizacion.Campo_Precio_U_Con_Imp_Cotizado].ToString().Trim();
                }
                else
                {
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IEPS_Cotizado] = 0.0;
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_IVA_Cotizado] = 0.0;
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Subtota_Cotizado] = 0.0;
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Total_Cotizado] = 0.0;
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Sin_Imp_Cotizado] = 0.0;
                    Dt_Productos.Rows[i][Ope_Com_Req_Producto.Campo_Precio_U_Con_Imp_Cotizado] = 0.0;
                }
            }
            Session["Dt_Productos"] = Dt_Productos;
        }
        //llenamos nuevamente el grid de productos
        Grid_Productos.DataSource = Dt_Productos;
        Grid_Productos.DataBind();
        Calcular_Importe_Total();
    }

    public void Guardar()
    {
        String Mensaje_Error = "";
        Cls_Ope_Com_Propuesta_Ganadora_Negocio Clase_Negocio = new Cls_Ope_Com_Propuesta_Ganadora_Negocio();


        if (Txt_Total_Cotizado_Requisicion.Text.Trim() == String.Empty)
        {
            Mensaje_Error += "</br>+ Es necesario calcular los precios";
        }
        Double Parametro_Monto_Compra = Clase_Negocio.Consultar_Parametro_Monto_Compra();
        if (Parametro_Monto_Compra <= 0)
        {
            Mensaje_Error += "</br>+ Es necesario asignar el monto del parametro Monto de Compra en el catálogo ''Parametro Monto de Compra''";
        }
        
        //Validamos en caso de seleccionar el estatus de PROVEEDOR_ASIGNADO
        if (Cmb_Estatus.SelectedValue.Trim() == "PROVEEDOR_ASIGNADO")
        {
            //Validamos que todos los productos sean cotizados
            foreach(DataRow Dr_Producto in Grid_Productos.Rows)
            {
                if (String.IsNullOrEmpty(Dr_Producto[Ope_Com_Req_Producto.Campo_Proveedor_ID].ToString().Trim()))
                {
                    Mensaje_Error += "</br>+ Es necesario asignar cotizador a todos los productos";
                    break;
                }
            }
        }
        

        if (!String.IsNullOrEmpty(Mensaje_Error))
        {
            Lbl_Mensaje_Error.Text = Mensaje_Error;
            Div_Contenedor_Msj_Error.Visible = true;
            //break;
        }

        if (Div_Contenedor_Msj_Error.Visible == false)
        {
            Double Total_Cotizado = 0;
            Double.TryParse(Txt_Total_Cotizado_Requisicion.Text.ToString(), out Total_Cotizado); //Convierte el total cotizado a tipo de dato Double
            //Validacion del monto de compra
            String Mensaje_Descriptivo = "";
            if (Total_Cotizado < Parametro_Monto_Compra || Hdf_Listado_Almacen.Value.ToUpper().Trim() == "SI")
            {
                if (Cmb_Estatus.SelectedValue.Trim() == "PROVEEDOR_ASIGNADO") // Si el estatus seleccionado es "Cotizada"
                {
                    Clase_Negocio.P_Estatus = "CONFIRMADA";         //Colocamos el estatus como confirmada
                    Mensaje_Descriptivo = " y se Confirmo ";
                }
                else
                {
                    Clase_Negocio.P_Estatus = "CONFIRMADA";
                }
                //Validar presupusto
                Cls_Ope_Com_Administrar_Requisiciones_Negocio Requisicion_Negocio = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                //Checamos si existe presupuesto
                Requisicion_Negocio.P_Total_Cotizado = Txt_Total_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                Requisicion_Negocio.P_Requisicion_ID = Session["No_Requisicion"].ToString();


                bool Existe_Presupuesto = Requisicion_Negocio.Consultamos_Presupuesto_Existente();
                if (Existe_Presupuesto == false)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "</br>+ No existe presupuesto suficiente.";
                    //break;
                }
                else
                {
                    //Guardamos los datos de la cotizacion
                    //Cargamos datos al objeto de clase negocio
                    Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                    Clase_Negocio.P_Dt_Productos = (DataTable)Session["Dt_Productos"];
                    if (Txt_IEPS_Cotizado.Text.Trim() != String.Empty)
                        Clase_Negocio.P_IEPS_Cotizado = Txt_IEPS_Cotizado.Text.Replace("$", "").Replace(",", "");
                    else
                        Clase_Negocio.P_IEPS_Cotizado = "0";
                    if (Txt_IVA_Cotizado.Text.Trim() != String.Empty)
                        Clase_Negocio.P_IVA_Cotizado = Txt_IVA_Cotizado.Text.Replace("$", "").Replace(",", "");
                    else
                        Clase_Negocio.P_IVA_Cotizado = "0";
                    if (Txt_Total_Cotizado_Requisicion.Text.Trim() != String.Empty)
                        Clase_Negocio.P_Total_Cotizado = Txt_Total_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                    else
                        Clase_Negocio.P_Total_Cotizado = "0";
                    if (Txt_SubTotal_Cotizado_Requisicion.Text.Trim() != String.Empty)
                        Clase_Negocio.P_Subtotal_Cotizado = Txt_SubTotal_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                    else
                        Clase_Negocio.P_Subtotal_Cotizado = "0";

                    bool Operacion_Realizada = false;
                    Operacion_Realizada = Clase_Negocio.Agregar_Cotizaciones();
                    if (Operacion_Realizada == true)
                    {
                        Clase_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
                        Operacion_Realizada = Clase_Negocio.Modificar_Requisicion_Monto_Menor_A_Parametro();
                        if (Operacion_Realizada == true)
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Guardo la Cotizacion Ganadora Exitosamente " + Mensaje_Descriptivo + " ');", true);
                        }
                        else
                        {
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                        }
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                    }
                }
            }
            else
            {
                //Si el Total_Cotizado > Monto_Compra, se cambia al estatus Cotizada y se manda al proceso de autorizacion.
                //Cargamos datos al objeto de clase negocio
                Clase_Negocio.P_No_Requisicion = Session["No_Requisicion"].ToString().Trim();
                Clase_Negocio.P_Dt_Productos = (DataTable)Session["Dt_Productos"];
                Clase_Negocio.P_Estatus = "COTIZADA";
                if (Txt_IEPS_Cotizado.Text.Trim() != String.Empty)
                    Clase_Negocio.P_IEPS_Cotizado = Txt_IEPS_Cotizado.Text.Replace("$", "").Replace(",", "");
                else
                    Clase_Negocio.P_IEPS_Cotizado = "0";
                if (Txt_IVA_Cotizado.Text.Trim() != String.Empty)
                    Clase_Negocio.P_IVA_Cotizado = Txt_IVA_Cotizado.Text.Replace("$", "").Replace(",", "");
                else
                    Clase_Negocio.P_IVA_Cotizado = "0";
                if (Txt_Total_Cotizado_Requisicion.Text.Trim() != String.Empty)
                    Clase_Negocio.P_Total_Cotizado = Txt_Total_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                else
                    Clase_Negocio.P_Total_Cotizado = "0";
                if (Txt_SubTotal_Cotizado_Requisicion.Text.Trim() != String.Empty)
                    Clase_Negocio.P_Subtotal_Cotizado = Txt_SubTotal_Cotizado_Requisicion.Text.Replace("$", "").Replace(",", "");
                else
                    Clase_Negocio.P_Subtotal_Cotizado = "0";

                bool Operacion_Realizada = false;
                Operacion_Realizada = Clase_Negocio.Agregar_Cotizaciones();

                if (Operacion_Realizada == true)
                {
                    Operacion_Realizada = Clase_Negocio.Modificar_Requisicion();
                    if (Operacion_Realizada == true)
                    {
                        Mensaje_Descriptivo = " y se envio a Autoizar debio a que el total es mayor o igual a " + Total_Cotizado;
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('Se Guardo la Cotizacion Ganadora Exitosamente " + Mensaje_Descriptivo + "');", true);
                    }
                    else
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                    }
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Cotizacion Ganadora", "alert('No se Guardo la Cotizacion Ganadora');", true);
                }
            }
        }
    }

    #endregion

    protected void Txt_Requisicion_Busqueda_TextChanged(object sender, EventArgs e)
    {
        Configurar_Formulario("Inicio");
        Llenar_Grid_Requisiciones();
    }
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Configurar_Formulario("Inicio");
        Llenar_Grid_Requisiciones();
    }
}
