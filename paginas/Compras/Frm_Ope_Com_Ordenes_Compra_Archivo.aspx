﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Ordenes_Compra_Archivo.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Ordenes_Compra_Archivo" Culture="es-MX" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <style type="text/css">
        .style1
        {
            height: 292px;
        }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        function Abrir_Carga_PopUp() 
        {
            $find('Archivos_Requisicion').show();
            return false;
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
   </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                <!--Div del encabezado-->
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">Archivos Ordenes de Compra</td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png"
                                    Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                    CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar" 
                                    onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Eliminar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                    CssClass="Img_Button" AlternateText="Eliminar" ToolTip="Eliminar" 
                                    OnClientClick="return confirm('¿Esta seguro de Eliminar el presente registro?');" 
                                    onclick="Btn_Eliminar_Click" />
                                <asp:ImageButton ID="Btn_Cargar" runat="server" CssClass="Img_Button" CausesValidation="false"
                                    ImageUrl="~/paginas/imagenes/paginas/subir.png" ToolTip="Cargar Archivo" OnClientClick="javascript:return Abrir_Carga_PopUp();" 
                                    onclick="Btn_Cargar_Click" />
                                <cc1:ModalPopupExtender ID="MPE_Archivos_Requisicion" runat="server" BackgroundCssClass="popUpStyle" BehaviorID="Archivos_Requisicion" PopupControlID="Pnl_Seleccionar_Archivo" TargetControlID="Btn_Open" CancelControlID="Btn_Cerrar" DropShadow="true" DynamicServicePath="" Enabled="true"></cc1:ModalPopupExtender>
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                    ToolTip="Inicio" onclick="Btn_Salir_Click" />
                                <asp:Button ID="Btn_Open" runat="server" Text="" style="background-color: transparent; border-style:none; visibility:hidden" />
                                <asp:Button Style="background-color: transparent; border-style:none; visibility:hidden" ID="Btn_Cerrar" runat="server" Text="" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <!--Div de las requisiciones-->
                <div id="Div_Requisiciones" runat="server">
                    <table style="width: 100%;">                        
                        <tr>
                            <td>
                                De&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="80px" Enabled="false"></asp:TextBox>           
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicio_FilteredTextBoxExtender" 
                                    runat="server" TargetControlID="Txt_Fecha_Inicio" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters" ValidChars="/_"/>
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicio_CalendarExtender" runat="server" 
                                    TargetControlID="Txt_Fecha_Inicio" PopupButtonID="Btn_Fecha_Inicio" Format="dd/MMM/yyyy"/>
                                <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial"/>
                            </td>                                                        
                            <td>
                                Al&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="80px" Enabled="false"></asp:TextBox>                            
                                <cc1:CalendarExtender ID="CalendarExtender1" runat="server" 
                                    TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy"/>
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Final"/>
                            </td>        
                            <td>
                                Estatus&nbsp;&nbsp;
                                <asp:DropDownList ID="Cmb_Estatus_Busqueda" runat="server">
                                    <asp:ListItem Value="0">&lt;&lt;SELECCIONE&gt;&gt;</asp:ListItem>
                                    <asp:ListItem>GENERADA</asp:ListItem>
                                    <asp:ListItem Value="AUTORIZADA_COMPRAS">AUTORIZADA COMPRAS</asp:ListItem>
                                    <asp:ListItem Value="AUTORIZADA_CONTABILIDAD">AUTORIZADA CONTABILIDAD</asp:ListItem>
                                </asp:DropDownList>
                            </td>                                                          
                            <td>
                                Folio&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Orden_Compra_Busqueda" runat="server" AutoPostBack="true"
                                    ontextchanged="Txt_Orden_Compra_Busqueda_TextChanged"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked" WatermarkText="<OC-0>" TargetControlID="Txt_Orden_Compra_Busqueda" />
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                    OnClick="Btn_Buscar_Click" AlternateText="Consultar"/>
                            </td>                                                      
                        </tr>
                        <tr>
                            <td style="width: 99%" align="center" colspan="4">
                                <div style="overflow: auto; height: 320px; width: 99%; vertical-align: top; border-style: outset;
                                    border-color: Silver;">
                                    <asp:GridView ID="Grid_Ordenes_Compra" runat="server" AllowPaging="false" AutoGenerateColumns="False"
                                        CssClass="GridView_1" GridLines="None" Width="100%" 
                                        DataKeyNames="No_Orden_Compra" HeaderStyle-CssClass="tblHead" 
                                        EmptyDataText="Lista de Ordenes Compra" 
                                        onpageindexchanging="Grid_Ordenes_Compra_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:TemplateField HeaderText="">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Seleccionar_Orden_Compra" runat="server" OnClick="Btn_Seleccionar_Orden_Compra_Click" ImageUrl="~/paginas/imagenes/gridview/blue_button.png" CommandArgument='<%# Eval("NO_ORDEN_COMPRA") %>' />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                <ItemStyle HorizontalAlign="Center" Width="3%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="NO_ORDEN_COMPRA" HeaderText="numero" Visible="false">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Center" Width="5%"/>
                                        </asp:BoundField>                                    
                                        <asp:BoundField DataField="FOLIO" HeaderText="Folio" Visible="true">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="9%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="LISTA_REQUISICIONES" HeaderText="Req." Visible="true" DataFormatString="{0:RQ-0}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="9%"/>
                                        </asp:BoundField>                                          
                                        <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha" Visible="true" DataFormatString="{0:dd/MMM/yyy}">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="10%"/>
                                        </asp:BoundField>                                        
                                        <asp:BoundField DataField="NOMBRE_PROVEEDOR" HeaderText="Proveedor" Visible="true">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="SUBTOTAL" HeaderText="Subtotal" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                                        </asp:BoundField>                                                    
                                        <asp:BoundField DataField="TOTAL_IEPS" HeaderText="IEPS" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="TOTAL_IVA" HeaderText="IVA" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                                        </asp:BoundField>              
                                        <asp:BoundField DataField="TOTAL" HeaderText="Total" Visible="true" DataFormatString="{0:C}">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="8%"/>
                                        </asp:BoundField>            
                                        <asp:BoundField DataField="NO_RESERVA" HeaderText="No. Reserva" Visible="true">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" Width="12%"/>
                                        </asp:BoundField>                                                              
                                                                                                                                                                                                                                                                                                                                 
                                        <asp:BoundField DataField="TIPO_ARTICULO" HeaderText="Tipo" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>                           
                                        <asp:BoundField DataField="TIPO_PROCESO" HeaderText="Tipo Proceso" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>                                                                   
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CODIGO" HeaderText="Codigo" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>    
                                        <asp:BoundField DataField="JUSTIFICACION_COMPRA" HeaderText="Justify" Visible="false">
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </td>
                        </tr>
                    </table>                
                </div>
                <!--Div del contenido-->
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td align="center">
                                <asp:Label ID="Lbl_Archivos_Orden_Compra" runat="server"></asp:Label>
                                <asp:HiddenField ID="Txt_No_Orden_Compra" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td align="left">
                                <asp:GridView ID="Grid_Archivos_Requisiciones" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1" AllowPaging="True" 
                                    PageSize="5" GridLines="None" Width="100%">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:TemplateField HeaderText="">
                                            <ItemTemplate>
                                                <asp:ImageButton ID="Btn_Seleccionar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/delete.png" CommandArgument='<%# Eval("No_Archivo") %>' OnClick="Btn_Seleccionar_Archivo_Click" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="No_Archivo" HeaderText="No Archivo" />
                                        <asp:BoundField DataField="Folio" HeaderText="Folio" 
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" >
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Right" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Archivo" SortExpression="Archivo">
                                            <ItemTemplate>
                                                <asp:HyperLink id="Lnk_Archivo" runat="server" text='<%# Eval("Nombre_Archivo") %>' NavigateUrl='<%# String.Format("~/Ord_Com_Archivos/{0}/{1}", Eval("Folio"), Eval("Nombre_Archivo")) %>' Target="_blank" ></asp:HyperLink >                                                
                                            </ItemTemplate>
                                        </asp:TemplateField> 
                                        <asp:BoundField DataField="Descripcion" HeaderText="Descripci&oacute;n" 
                                            HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" >   
                                            <HeaderStyle HorizontalAlign="Center" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
        <!--Modal para la seleccion del archivo-->
        <asp:Panel ID="Pnl_Seleccionar_Archivo" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
            <asp:Panel ID="Pnl_Cabecera_Seleccionar_Archivo" runat="server">
                <table width="100%">
                    <tr>
                        <td style="color:Black;font-size:12;font-weight:bold;"><asp:Image ID="Image1" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />Seleccionar el Archivo para la Orden Compra</td>
                    </tr>
                </table>
            </asp:Panel>
            <div style="color: #5D7B9D">
                <table width="100%">
                    <tr>
                        <td align="left" style="text-align:left;">
                            <asp:UpdatePanel ID="Upnl_Archivos_Requisicion" runat="server">
                                <ContentTemplate>
                                    <asp:UpdateProgress ID="Progress_Upnl_Archivos_Requisicion" runat="server" AssociatedUpdatePanelID="Upnl_Archivos_Requisicion" DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                            <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>                                        
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <table width="100%">
                                        <tr>
                                            <td colspan="4" style="width:100%;"><hr /></td>
                                        </tr>
                                        <tr>
                                            <td style="width:20%;text-align:left;font-size:11px;">
                                                <cc1:AsyncFileUpload ID="AFil_Archivo" runat="server" ThrobberID="Lbl_Throbber" 
                                                    onuploadedcomplete="AFil_Archivo_UploadedComplete" />
                                                <asp:Label ID="Lbl_Throbber" runat="server" Text="Espere" Width="30px">
                                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                    <div  class="processMessage" id="div_progress">
                                                        <center>
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                            <br /><br />
                                                            <span id="spanUploading" runat="server" style="color:White;font-size:30px;font-weight:bold;font-family:Lucida Calligraphy;font-style:italic;">
                                                                Cargando...
                                                            </span>
                                                        </center>
                                                    </div>                                                
                                                </asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td align="left" colspan="4">Comentarios<br />                                           
                                                <asp:TextBox ID="Txt_Comentarios_Archivo" runat="server" TextMode="MultiLine" Width="100%"></asp:TextBox>
                                                <cc1:textboxwatermarkextender id="TextBoxWatermarkExtender3" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    watermarktext="&lt;Límite de Caracteres 500&gt;" watermarkcssclass="watermarked">
                                                    </cc1:textboxwatermarkextender>
                                                <cc1:filteredtextboxextender id="FTE_Justificacion" runat="server" targetcontrolid="Txt_Comentarios_Archivo"
                                                    filtertype="Custom, UppercaseLetters, LowercaseLetters, Numbers" validchars="ÑñáéíóúÁÉÍÓÚ./*-!$%&()=,[]{}+<>@?¡?¿# ">
                                                    </cc1:filteredtextboxextender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="width:100%;text-align:left;">
                                                <center>
                                                    <asp:Button ID="Btn_Archivos_Orden_Compra" runat="server" Text="Aceptar" 
                                                        CssClass="button" CausesValidation="false" Width="200px" 
                                                        onclick="Btn_Archivos_Orden_Compra_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:AsyncPostBackTrigger EventName="Click" ControlID="Btn_Archivos_Orden_Compra" />
                                </Triggers>
                            </asp:UpdatePanel>
                        </td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
    </div>
</asp:Content>

