﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Catalogo_Compras_Giro_Proveedor.Negocio;
using JAPAMI.Catalogo_Compras_Giros.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.Xml.Linq;
using JAPAMI.Catalogo_SAP_Conceptos.Negocio;
using System.Collections.Generic;
using CarlosAg.ExcelXmlWriter;
using System.Text;
using System.Text.RegularExpressions;


public partial class paginas_Compras_Frm_Cat_Com_Proveedores : System.Web.UI.Page
{

    ///*******************************************************************************
    ///PAGE_LOAD
    ///*******************************************************************************
    #region PAGE_LOAD

    protected void Page_Load(object sender, EventArgs e)
    {

        Txt_Password.Attributes.Add("value", Txt_Password.Text);
        if (!IsPostBack)
        {
            Session["Activa"] = true;
            ViewState["SortDirection"] = "ASC";
            Configurar_Formulario("Inicio");

        }

    }
    #endregion

    ///*******************************************************************************
    ///METODOS
    ///*******************************************************************************
    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Configurar_Formulario
    ///DESCRIPCIÓN: Metodo que ayuda a configuarar el formulario 
    ///PARAMETROS:Estatus, Puede Ser Inicio, Nuevo, Modificar 
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    ///
    public void Configurar_Formulario(String Estatus)
    {
        switch (Estatus)
        {
            case "Inicio":
                //Limpiar_Componentes();
                Habilitar_Componentes(false);
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = true;
                Grid_Correos.Enabled = false;
                Btn_Busqueda_Avanzada.Enabled = true;
                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Inicio";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                Div_Proveedores.Visible = false;

                break;
            case "Nuevo":
                Limpiar_Componentes();
                Habilitar_Componentes(true);
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = false;
                Grid_Correos.Enabled = true;
                Btn_Busqueda_Avanzada.Enabled = false;
                //Boton Nuevo
                Btn_Nuevo.Visible = true;
                Btn_Nuevo.ToolTip = "Dar de Alta";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                //Boton Modificar
                Btn_Modificar.Visible = false;
                Btn_Modificar.ToolTip = "Modificar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Div_Proveedores.Visible = false;

                break;
            case "Modificar":
                
                Habilitar_Componentes(true);
                Chk_Provisional.Enabled = false;
                Div_Busqueda_Avanzada.Visible = false;
                Grid_Proveedores.Enabled = false;
                Grid_Correos.Enabled = true;
                Btn_Busqueda_Avanzada.Enabled = false;
                //Boton Nuevo
                Btn_Nuevo.Visible = false;
                Btn_Nuevo.ToolTip = "Nuevo";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                //Boton Modificar
                Btn_Modificar.Visible = true;
                Btn_Modificar.ToolTip = "Actualizar";
                Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                //Boton Salir
                Btn_Salir.Visible = true;
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Div_Proveedores.Visible = false;

                break;


        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Limpiar_Componentes
    ///DESCRIPCIÓN: Metodo que limpia las cajas de texto del catalogo de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Limpiar_Componentes()
    {
        Txt_Proveedor_ID.Text = "";
        Txt_Fecha_Registro.Text = "";
        Txt_Razon_Social.Text = "";
        Txt_Nombre_Comercial.Text = "";
        Txt_Representante_Legal.Text = "";
        Txt_Contacto.Text = "";
        Txt_RFC.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
        Chk_Fisica.Checked = false;
        Chk_Moral.Checked = false;
        Txt_Direccion.Text = "";
        Txt_Colonia.Text = "";
        Txt_Ciudad.Text = "";
        Txt_Estado.Text = "";
        Txt_Codigo_Postal.Text = "";
        Txt_Telefono1.Text = "";
        Txt_Telefono2.Text = "";
        Txt_Nextel.Text = "";
        Txt_Fax.Text = "";
        Cmb_Tipo_Pago.SelectedIndex = 0;
        Txt_Dias_Credito.Text = "";
        Cmb_Forma_Pago.SelectedIndex = 0;
        Txt_Correo.Text = "";
        Txt_Password.Text = "";
        Txt_Comentarios.Text = "";
        Txt_Por_Anticipado.Text = "";
        Txt_Ultima_Actualizacion.Text = "";
        Chk_Actualizacion.Checked = false;
        Chk_Provisional.Checked = false;
        //Limpiamos los Grid
        Grid_Conceptos_Proveedor.DataSource = new DataTable();
        Grid_Conceptos_Proveedor.DataBind();

        Grid_Partidas_Generales.DataSource = new DataTable();
        Grid_Partidas_Generales.DataBind();

        Grid_Correos.DataSource = new DataTable();
        Grid_Correos.DataBind();
        //Variables de Session
        Session["Proveedor_ID"] = null;
        Session["Dt_Conceptos_Proveedor"] = null;
        Session["Dt_Consulta_Partidas_Proveedores"] = null;
        Session["Dt_Correos"] = null;
    }

    public void Limpiar_Controles_Busqueda_Avanzada()
    {
        //Limpiamos las Cajas y variable de session del Div de Busqueda avanzada
        Txt_Busqueda_Padron_Proveedor.Text = "";
        Txt_Busqueda_Nombre_Comercial.Text = "";
        Txt_Busqueda_RFC.Text = "";
        Txt_Busqueda_Razon_Social.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
          
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Habilitar_Componentes
    ///DESCRIPCIÓN: Metodo que  Habilita o Deshabilita los componentes
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Habilitar_Componentes(bool Habilitar)
    {

        Txt_Proveedor_ID.Enabled = false;
        Txt_Fecha_Registro.Enabled = false;
        Txt_Razon_Social.Enabled = Habilitar;
        Txt_Nombre_Comercial.Enabled = Habilitar;
        Txt_Representante_Legal.Enabled = Habilitar;
        Txt_Contacto.Enabled = Habilitar;
        Txt_RFC.Enabled = Habilitar;
        Cmb_Estatus.Enabled = Habilitar;
        Chk_Fisica.Enabled = Habilitar;
        Chk_Moral.Enabled = Habilitar;
        Txt_Direccion.Enabled = Habilitar;
        Txt_Colonia.Enabled =Habilitar;
        Txt_Ciudad.Enabled = Habilitar;
        Txt_Estado.Enabled = Habilitar;
        Txt_Codigo_Postal.Enabled = Habilitar;
        Txt_Telefono1.Enabled = Habilitar;
        Txt_Telefono2.Enabled = Habilitar;
        Txt_Nextel.Enabled = Habilitar;
        Txt_Fax.Enabled = Habilitar;
        Cmb_Tipo_Pago.Enabled= Habilitar;
        Txt_Dias_Credito.Enabled = Habilitar;
        Cmb_Forma_Pago.Enabled = Habilitar;
        Txt_Correo.Enabled = Habilitar;
        Txt_Password.Enabled = Habilitar;
        Txt_Comentarios.Enabled = Habilitar;
        Txt_Por_Anticipado.Enabled = Habilitar;
        Txt_Ultima_Actualizacion.Enabled = false;
        Chk_Actualizacion.Enabled = Habilitar;
        Cmb_Conceptos.Enabled = Habilitar;
        Cmb_Partidas_Generales.Enabled = Habilitar;
        Btn_Agregar_Partida.Enabled = Habilitar;
        Grid_Conceptos_Proveedor.Enabled = Habilitar;
        Grid_Partidas_Generales.Enabled = Habilitar;
        Chk_Provisional.Enabled = Habilitar;
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Conceptos
    ///DESCRIPCIÓN          : Llena el Combo de Conceptos con los existentes en la Base de Datos.
    ///PARAMETROS           :
    ///CREO: Susana Trigueros A.
    ///FECHA_CREO: 7/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************      
    public void Llenar_Combo_Conceptos()
    {
        try
        {
            Cls_Cat_Com_Proveedores_Negocio Negocio = new Cls_Cat_Com_Proveedores_Negocio();
            DataTable Data_Table = Negocio.Consultar_Conceptos();
            Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Conceptos, Data_Table);
            
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Partidas
    ///DESCRIPCIÓN          : Llena el Combo de Partidas con los existentes en la Base de Datos.
    ///PARAMETROS           :
    ///CREO: Susana Trigueros A.
    ///FECHA_CREO: 7/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************      
    public void Llenar_Combo_Partidas()
    {
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedValue;
        DataTable Dt_Partidas = Clase_Negocio.Consultar_Partidas_Especificas();
        Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Partidas_Generales,Dt_Partidas);


    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Validar_Contenido_Controles
    ///DESCRIPCIÓN          : Verifica si la informacion ingresada en las cajas de texto por el usuario es valida
    ///PARAMETROS           :
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 8/NOV/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Validar_Contenido_Controles()
    {
        if (Txt_Razon_Social.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Razon Social<br/>";
        }

        if(Txt_Nombre_Comercial.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Nombre Comercial<br/>";
        }

        if (Txt_Representante_Legal.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Representante Legal<br/>";
        }

        if (Txt_Contacto.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Contacto<br/>";
        }

        if (Txt_RFC.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el RFC<br/>";
        }

        if (Chk_Fisica.Checked == false && Chk_Moral.Checked == false)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar si es persona Fisica o Moral<br/>";
        }

        if (Txt_Direccion.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la dirección<br/>";
        }

        if (Txt_Colonia.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Colonia<br/>";
        }

        if (Txt_Ciudad.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la Ciudad<br/>";
        }
        if (Txt_Estado.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Estado<br/>";
        }

        if (Txt_Codigo_Postal.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar la CP<br/>";
        }
        if (Txt_Telefono1.Text.Trim() == String.Empty)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario indicar el Telefono <br/>";
        }
        //Validamos el Correo del Proveedor que el correo sea correcto
       
        //if (Txt_Password.Text.Trim() == String.Empty)
        //{
        //    Div_Contenedor_Msj_Error.Visible = true;
        //    Lbl_Mensaje_Error.Visible = true;
        //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Es necesario asignar un Password<br/>";
        //}



    }

    public void Agregar_Concepto_Proveedor()
    {
        DataTable Dt_Conceptos_Proveedor = new DataTable();
        if (Session["Dt_Conceptos_Proveedor"] != null)
        {
            Dt_Conceptos_Proveedor = (DataTable)Session["Dt_Conceptos_Proveedor"];
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Conceptos_Proveedor.Select("CONCEPTO_ID='" + Cmb_Conceptos.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {

            }
            else
            {
                DataRow Fila = Dt_Conceptos_Proveedor.NewRow();
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Fila["CONCEPTO"] = Cmb_Conceptos.SelectedItem.Text;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Dt_Conceptos_Proveedor.Rows.Add(Fila);
                Dt_Conceptos_Proveedor.AcceptChanges();
                Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Grid_Conceptos_Proveedor.DataBind();
                //Agregamos el Concepto al Grid de Conceptos 


            }
        }
        else
        {
            Dt_Conceptos_Proveedor.Columns.Add("CONCEPTO_ID", typeof(System.String));
            Dt_Conceptos_Proveedor.Columns.Add("CONCEPTO", typeof(System.String));
            Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
            DataRow[] Fila_Nueva;
            Fila_Nueva = Dt_Conceptos_Proveedor.Select("CONCEPTO_ID='" + Cmb_Conceptos.SelectedValue.Trim() + "'");
            if (Fila_Nueva.Length > 0)
            {

            }
            else
            {
                DataRow Fila = Dt_Conceptos_Proveedor.NewRow();
                Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                Fila["CONCEPTO"] = Cmb_Conceptos.SelectedItem.Text;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Dt_Conceptos_Proveedor.Rows.Add(Fila);
                Dt_Conceptos_Proveedor.AcceptChanges();
                Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
                Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
                Grid_Conceptos_Proveedor.DataBind();
            }

        }

    }//fin del metodo de Agregar_Concepto_Proveedor

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Email
    ///DESCRIPCIÓN: 
    ///PARAMETROS: Metodo que permite validar el correo ingresado por el usuario
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 07/Octubre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************

    public void Validar_Email()
    {

        Regex Exp_Regular = new Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
        Match Comparar = Exp_Regular.Match(Txt_Correo.Text);

        if (!Comparar.Success)
        {
            Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ El contenido del Correo Electronico es incorrecto <br />";
            Lbl_Mensaje_Error.Visible = true;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    public Cls_Cat_Com_Proveedores_Negocio Cargar_Datos_Proveedor(Cls_Cat_Com_Proveedores_Negocio Clase_Negocio)
    {
        if(Txt_Proveedor_ID.Text.Trim() != String.Empty)
            Clase_Negocio.P_Proveedor_ID = Txt_Proveedor_ID.Text.Trim();
        Clase_Negocio.P_Razon_Social = Txt_Razon_Social.Text.Trim();
        Clase_Negocio.P_Nombre_Comercial = Txt_Nombre_Comercial.Text.Trim();
        Clase_Negocio.P_Representante_Legal = Txt_Representante_Legal.Text.Trim();
        Clase_Negocio.P_Contacto = Txt_Contacto.Text.Trim();
        Clase_Negocio.P_RFC = Txt_RFC.Text.Trim();
        Clase_Negocio.P_Estatus = Cmb_Estatus.SelectedValue;
        
        if (Chk_Fisica.Checked == true)
        {
            Clase_Negocio.P_Tipo_Persona_Fiscal = "FISICA";
        }
        if (Chk_Moral.Checked == true)
        {
            Clase_Negocio.P_Tipo_Persona_Fiscal = "MORAL";
        }
        Clase_Negocio.P_Direccion = Txt_Direccion.Text.Trim();
        Clase_Negocio.P_Colonia = Txt_Colonia.Text.Trim();
        Clase_Negocio.P_Ciudad = Txt_Ciudad.Text.Trim();
        Clase_Negocio.P_Estado = Txt_Estado.Text.Trim();
        Clase_Negocio.P_CP = int.Parse( Txt_Codigo_Postal.Text.Trim());
        Clase_Negocio.P_Telefono_1 = Txt_Telefono1.Text.Trim();
        if(Txt_Telefono2.Text.Trim() != String.Empty)
            Clase_Negocio.P_Telefono_2 = Txt_Telefono2.Text.Trim();
        if (Txt_Nextel.Text.Trim() != String.Empty)
            Clase_Negocio.P_Nextel = Txt_Nextel.Text.Trim();
        if (Txt_Fax.Text.Trim() != String.Empty)
            Clase_Negocio.P_Fax = Txt_Fax.Text.Trim();
        if (Cmb_Tipo_Pago.SelectedIndex != 0)
        {
            Clase_Negocio.P_Tipo_Pago = Cmb_Tipo_Pago.SelectedValue;
        }
        if (Cmb_Forma_Pago.SelectedIndex != 0)
        {
            Clase_Negocio.P_Forma_Pago = Cmb_Forma_Pago.SelectedValue;
        }
        if (Txt_Dias_Credito.Text != String.Empty)
        {
            Clase_Negocio.P_Dias_Credito = int.Parse(Txt_Dias_Credito.Text.Trim());
            
        }
        DataTable Dt_Correos = new DataTable();
        Dt_Correos =(DataTable)Session["Dt_Correos"];
        if (Dt_Correos.Rows.Count > 0)
        {

            //Recorremos el Grid de los correos 
            for (int i = 0; i < Dt_Correos.Rows.Count; i++)
            {
                Clase_Negocio.P_Correo_Electronico = Clase_Negocio.P_Correo_Electronico + Dt_Correos.Rows[i][0].ToString().Trim();
                Clase_Negocio.P_Correo_Electronico = Clase_Negocio.P_Correo_Electronico + ";";
             
            }            

        }
        if (Txt_Password.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Password = Txt_Password.Text.Trim();
        }
        else
        {
            Clase_Negocio.P_Password = "123456";
        }
        if (Txt_Comentarios.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
        }
        
        if(Txt_Por_Anticipado.Text.Trim() != String.Empty)
        {
            Clase_Negocio.P_Porciento_Anticipado = Int32.Parse(Txt_Por_Anticipado.Text.Trim());
        }

        if (Chk_Provisional.Checked == true)
        {
            Clase_Negocio.P_Provisional = "SI";
        }
        else
        {
            Clase_Negocio.P_Provisional = "NO";
        }

        // Agregamos a la clase de negocio si existe una nueva Actualizacion 
        if (Chk_Actualizacion.Checked == true)
            Clase_Negocio.P_Nueva_Actualizacion = true;
        else
            Clase_Negocio.P_Nueva_Actualizacion = false;
        //Cargamos el Dt de Conceptos y el Dt de partidas
        Clase_Negocio.P_Dt_Partidas_Proveedor = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
        Clase_Negocio.P_Dt_Conceptos_Proveedor = (DataTable)Session["Dt_Conceptos_Proveedor"];


        return Clase_Negocio;
    }
    #endregion


    ///*******************************************************************************
    ///GRID
    ///*******************************************************************************
    #region Grid

    #region Grid_Proveedores


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Habilitar_Componentes
    ///DESCRIPCIÓN: Metodo que  Habilita o Deshabilita los componentes
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        //Consultamos el Proveedor ID que selecciono
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Proveedor_ID = Grid_Proveedores.SelectedDataKey["Proveedor_ID"].ToString();
        //Consultamos los datos dep Proveedor seleccionado 

        DataTable Dt_Datos_Proveedor = Clase_Negocio.Consulta_Proveedores();
        Session["Proveedor_ID"]= Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim();
        Txt_Proveedor_ID.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim();

        if (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim() != String.Empty)
        {
            try
            {
                Txt_Fecha_Registro.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Registro].ToString().Trim()));
            }
            catch
            {
                Txt_Fecha_Registro.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Registro].ToString().Trim();
            }
        }
        Txt_Razon_Social.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim();
        Txt_Nombre_Comercial.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString().Trim();
        Txt_Representante_Legal.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Representante_Legal].ToString().Trim();
        Txt_Contacto.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Contacto].ToString().Trim();
        Txt_RFC.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString().Trim();

        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Fiscal].ToString().Trim())
        {
            case "FISICA":
                Chk_Fisica.Checked = true;
                Chk_Moral.Checked = false;
                break;
            case "MORAL":
                Chk_Fisica.Checked = false;
                Chk_Moral.Checked = true;
                break;
        }
        Txt_Direccion.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Direccion].ToString().Trim();
        Txt_Colonia.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Colonia].ToString().Trim();
        Txt_Ciudad.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Ciudad].ToString().Trim();
        Txt_Estado.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Estado].ToString().Trim();
        Txt_Codigo_Postal.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_CP].ToString().Trim();
        Txt_Telefono1.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Telefono_1].ToString().Trim();
        Txt_Telefono2.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Telefono_2].ToString().Trim();
        Txt_Nextel.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Nextel].ToString().Trim();
        Txt_Fax.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fax].ToString().Trim();
        Txt_Dias_Credito.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Dias_Credito].ToString().Trim();
        
        string correo = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim();
        string [] array = correo.Split(';');
        //llenamos el grid del correo. 
        DataTable Dt_Correos = new DataTable();
        Dt_Correos.Columns.Add("correo", typeof(System.String));

        for (int i = 0; i < array.Length; i++)
        {
            if (array[i] != String.Empty)
            {
                DataRow Fila = Dt_Correos.NewRow();
                Fila["correo"] = array[i].ToString().Trim();
                Dt_Correos.Rows.Add(Fila);
                Dt_Correos.AcceptChanges();
            }
        }
        Session["Dt_Correos"] = Dt_Correos;
        Grid_Correos.DataSource = Dt_Correos;
        Grid_Correos.DataBind();

            Txt_Password.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Password].ToString().Trim();
        Txt_Password.Attributes.Add("value", Txt_Password.Text);
        Txt_Comentarios.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Comentarios].ToString().Trim();
        Txt_Ultima_Actualizacion.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Fecha_Actualizacion].ToString().Trim();
        if(Txt_Ultima_Actualizacion.Text.Trim() != String.Empty)
            Session["Ultima_Actualizacion"] = Txt_Ultima_Actualizacion.Text.Trim();
        else
            Session["Ultima_Actualizacion"] = null;
        Chk_Actualizacion.Checked = false;

        if (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Provisional].ToString().Equals("SI"))
            Chk_Provisional.Checked = true;
        else
            Chk_Provisional.Checked = false;
        //Asignamos valor del combo Estatus
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Estatus].ToString().Trim())
        {
            case "ACTIVO":
                Cmb_Estatus.SelectedValue = "ACTIVO";
            break;
            case "INACTIVO":
                Cmb_Estatus.SelectedValue = "INACTIVO";
            break;
        }
        // Asignamos valor del combo Tipo de Pago
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Pago].ToString().Trim())
        {
            case "CREDITO":
                Cmb_Tipo_Pago.SelectedValue = "CREDITO";
                Txt_Dias_Credito.Text = Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Dias_Credito].ToString().Trim();
                break;
            case "CONTADO":
                Cmb_Tipo_Pago.SelectedValue = "CONTADO";
                Txt_Dias_Credito.Text = "";
                break;
        }
        //Asignar el valor del combo Forma de pago
        switch (Dt_Datos_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Forma_Pago].ToString().Trim())
        {
            case "TRANSFERENCIA":
                Cmb_Forma_Pago.SelectedValue = "TRANSFERENCIA";
                break;
            case "CHEQUE":
                Cmb_Forma_Pago.SelectedValue = "CHEQUE";
                break;
            case "EFECTIVO":
                Cmb_Forma_Pago.SelectedValue = "EFECTIVO";
                break;
        }
        //Consultamos los Conceptos asignado al Proveedor.
        Clase_Negocio.P_Proveedor_ID = Session["Proveedor_ID"].ToString().Trim();
        DataTable Dt_Conceptos_Proveedor = Clase_Negocio.Consultar_Detalles_Conceptos();
        if (Dt_Conceptos_Proveedor.Rows.Count != 0)
        {
            Grid_Conceptos_Proveedor.DataSource = Dt_Conceptos_Proveedor;
            Grid_Conceptos_Proveedor.DataBind();
            Session["Dt_Conceptos_Proveedor"] = Dt_Conceptos_Proveedor;
        }
        else
        {
            Grid_Conceptos_Proveedor.EmptyDataText = "No se encontraron Conceptos de Este Proveedor";
            Grid_Conceptos_Proveedor.DataSource = new DataTable();
            Grid_Conceptos_Proveedor.DataBind();
        }
        //Consultar las Partidas asignadas al Proveedor.
        DataTable Dt_Partidas_Proveedor = Clase_Negocio.Consultar_Detalle_Partidas();
        if (Dt_Partidas_Proveedor.Rows.Count != 0)
        {
            Grid_Partidas_Generales.DataSource = Dt_Partidas_Proveedor;
            Grid_Partidas_Generales.DataBind();
            Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Partidas_Proveedor;
        }
        else
        {
            Grid_Partidas_Generales.EmptyDataText = "No se encontraron partidas de este Proveedor";
            Grid_Partidas_Generales.DataSource = new DataTable();
            Grid_Partidas_Generales.DataBind();
        }
        //Consultamos si hay Historial de Actualizaciones
        DataTable Dt_Historia_Actualizaciones = Clase_Negocio.Consultar_Actualizaciones_Proveedores();
        if (Dt_Historia_Actualizaciones.Rows.Count != 0)
        {
            Grid_Historial_Act.DataSource = Dt_Historia_Actualizaciones;
            Grid_Historial_Act.DataBind();
        }
        else
        {
            Grid_Historial_Act.EmptyDataText = "No se encontro Historial de Actualizaciones de este Proveedor";
            Grid_Historial_Act.DataSource = new DataTable();
            Grid_Historial_Act.DataBind();
        }


        //llenamos los combos de Conceptos y Partidas
        Llenar_Combo_Conceptos();
        Llenar_Combo_Partidas();

        Div_Proveedores.Visible = false;
        Session["Dt_Proveedores"] = null;

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Llenar_Grid_Proveedores
    ///DESCRIPCIÓN: Metodo que  llena el Grid de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 28/May/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            String Padron_Proveedor = e.Row.Cells[1].Text.Trim();
            Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
            int num = 0;
            if (int.TryParse(Padron_Proveedor, out num))//si puede lo convertira
            {
                Clase_Negocio.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Padron_Proveedor.Trim()));
            }
            else 
            {
                Clase_Negocio.P_Proveedor_ID = Padron_Proveedor;
            }
           
            DataTable Dt_Partidas = Clase_Negocio.Consultar_Existencia_Partidas_Especificas();
            ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
            if (Dt_Partidas.Rows.Count <= 0)
            {
                Boton.ToolTip = "Alerta: Es necesario seleccionar una partida al proveedor";
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = true;
                
            }
            else
            {
                Boton.ImageUrl = "../imagenes/gridview/circle_red.png";
                Boton.Visible = false;
                
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Llenar_Grid_Proveedores
    ///DESCRIPCIÓN: Metodo que  llena el Grid de Proveedores
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 4/NOV/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_Grid_Proveedores(Cls_Cat_Com_Proveedores_Negocio Clase_Negocio)
    {
        DataTable Dt_Proveedores = Clase_Negocio.Consulta_Proveedores();
        if (Dt_Proveedores.Rows.Count != 0)
        {
            Grid_Proveedores.DataSource = Dt_Proveedores;
            Grid_Proveedores.DataBind();
            Session["Dt_Proveedores"] = Dt_Proveedores;
        }
        else
        {
            Grid_Proveedores.EmptyDataText = "No se han encontrado registros.";
            //Lbl_Mensaje_Error.Text = "+ No se encontraron datos <br />";
            Grid_Proveedores.DataSource = new DataTable();
            Grid_Proveedores.DataBind();
        }
    }

    protected void Grid_Partidas_Generales_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataRow[] Renglones;
        DataRow[] Renglon_Concepto;
        DataRow Renglon;
        //Obtenemos el Id del producto seleccionado
        GridViewRow selectedRow = Grid_Partidas_Generales.Rows[Grid_Partidas_Generales.SelectedIndex];
        String Id = Convert.ToString(selectedRow.Cells[1].Text);
        int num_fila = Grid_Partidas_Generales.SelectedIndex -1;
        DataTable Dt_Consulta_Giros_Proveedore = (DataTable)Session["Dt_Conceptos_Proveedor"];
        DataTable Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];

        //Validacion para cuando se elimina el unico registro
        if (num_fila == -1)
            num_fila = 0;

        String Concepto_ID = Dt_Consulta_Partidas_Proveedores.Rows[num_fila]["CONCEPTO_ID"].ToString().Trim();

        Renglones = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "='" + Id + "'");
        Renglon_Concepto = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Consulta_Partidas_Proveedores"] = Tabla;
            Grid_Partidas_Generales.SelectedIndex = (-1);
            Grid_Partidas_Generales.DataSource = Tabla;
            Grid_Partidas_Generales.DataBind();
        }

        if (Renglon_Concepto.Length == 1)
        {
            //Eliminamos el concepto
            DataTable Tabla = (DataTable)Session["Dt_Conceptos_Proveedor"];
            Renglon_Concepto = ((DataTable)Session["Dt_Conceptos_Proveedor"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
            if (Renglon_Concepto.Length > 0)
            {
                Renglon = Renglon_Concepto[0];
                Tabla.Rows.Remove(Renglon);
            }
            Session["Dt_Conceptos_Proveedor"] = Tabla;
            Grid_Conceptos_Proveedor.SelectedIndex = (-1);
            Grid_Conceptos_Proveedor.DataSource = Tabla;
            Grid_Conceptos_Proveedor.DataBind();
        }
    }

      ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Menos_Click
    ///DESCRIPCIÓN: Btn_Eliminar_Partida_Click
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Mar/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Eliminar_Correo_Click(object sender, ImageClickEventArgs e)
    {

        DataRow[] Renglones;
        
        DataRow Renglon;

        //Asignamos el valor del row
        Grid_Correos.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument);
        // GridViewRow selectedRow = Grid_Partidas_Generales.Rows[Grid_Partidas_Generales.SelectedIndex];
        String Id = Grid_Correos.SelectedDataKey["correo"].ToString().Trim();
        int num_fila = Grid_Correos.SelectedIndex - 1;
        DataTable Dt_Correo = (DataTable)Session["Dt_Correos"];
        if (num_fila == -1)
            num_fila = 0;
        Renglones = ((DataTable)Session["Dt_Correos"]).Select("correo='" + Id + "'");

        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["Dt_Correos"];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Correos"] = Tabla;
            Grid_Correos.SelectedIndex = (-1);
            Grid_Correos.DataSource = Tabla;
            Grid_Correos.DataBind();
        }
     
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Menos_Click
    ///DESCRIPCIÓN: Btn_Eliminar_Partida_Click
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 22/Mar/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Eliminar_Partida_Click(object sender, ImageClickEventArgs e)
    {
        DataRow[] Renglones;
        DataRow[] Renglon_Concepto;
        DataRow Renglon;
        
        //Asignamos el valor del row
        Grid_Partidas_Generales.SelectedIndex = int.Parse(((ImageButton)sender).CommandArgument); 
       // GridViewRow selectedRow = Grid_Partidas_Generales.Rows[Grid_Partidas_Generales.SelectedIndex];
        String Id = Grid_Partidas_Generales.SelectedDataKey["PARTIDA_GENERICA_ID"].ToString().Trim();
        int num_fila = Grid_Partidas_Generales.SelectedIndex - 1;
        DataTable Dt_Consulta_Giros_Proveedore = (DataTable)Session["Dt_Conceptos_Proveedor"];
        DataTable Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];

        //Validacion para cuando se elimina el unico registro
        if (num_fila == -1)
            num_fila = 0;

        String Concepto_ID = Dt_Consulta_Partidas_Proveedores.Rows[num_fila]["CONCEPTO_ID"].ToString().Trim();

        Renglones = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Partida_Generica_ID + "='" + Id + "'");
        Renglon_Concepto = ((DataTable)Session["Dt_Consulta_Partidas_Proveedores"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
        if (Renglones.Length > 0)
        {
            Renglon = Renglones[0];
            DataTable Tabla = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
            Tabla.Rows.Remove(Renglon);
            Session["Dt_Consulta_Partidas_Proveedores"] = Tabla;
            Grid_Partidas_Generales.SelectedIndex = (-1);
            Grid_Partidas_Generales.DataSource = Tabla;
            Grid_Partidas_Generales.DataBind();
        }

        if (Renglon_Concepto.Length == 1)
        {
            //Eliminamos el concepto
            DataTable Tabla = (DataTable)Session["Dt_Conceptos_Proveedor"];
            Renglon_Concepto = ((DataTable)Session["Dt_Conceptos_Proveedor"]).Select(Cat_SAP_Partida_Generica.Campo_Concepto_ID + "='" + Concepto_ID + "'");
            if (Renglon_Concepto.Length > 0)
            {
                Renglon = Renglon_Concepto[0];
                Tabla.Rows.Remove(Renglon);
            }
            Session["Dt_Conceptos_Proveedor"] = Tabla;
            Grid_Conceptos_Proveedor.SelectedIndex = (-1);
            Grid_Conceptos_Proveedor.DataSource = Tabla;
            Grid_Conceptos_Proveedor.DataBind();
        }        
    }

    protected void Grid_Proveedores_Sorting(object sender, GridViewSortEventArgs e)
    {
        DataTable Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];

        if (Dt_Proveedores != null)
        {
            DataView Dv_Proveedores = new DataView(Dt_Proveedores);
            String Orden = ViewState["SortDirection"].ToString();

            if (Orden.Equals("ASC"))
            {
                Dv_Proveedores.Sort = e.SortExpression + " " + "DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Proveedores.Sort = e.SortExpression + " " + "ASC";
                ViewState["SortDirection"] = "ASC";
            }

            Grid_Proveedores.DataSource = Dv_Proveedores;
            Grid_Proveedores.DataBind();
            //Guardamos el cambio dentro de la variable de session de Dt_Requisiciones
            Session["Dt_Proveedores"] = (DataTable)Dv_Proveedores.Table;
            Dt_Proveedores = (DataTable)Session["Dt_Proveedores"];

        }
    }

    #endregion


    #endregion


    ///*******************************************************************************
    ///EVENTOS
    ///*******************************************************************************
    #region Eventos

    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Nuevo.ToolTip)
        {
            case "Nuevo":
                Configurar_Formulario("Nuevo");
                Limpiar_Componentes();
                Habilitar_Componentes(true);
                //Asignamos por default el Password con el valor 123456 cuando es un proveedor nuevo
                //Txt_Password.Text = "123456";
                //Txt_Password.Attributes.Add("value", Txt_Password.Text);
                //Llenamos el combo de Conceptos
                Llenar_Combo_Conceptos();
                //Deshabilitamos el combo de Partidas
                Cmb_Partidas_Generales.Enabled = false;
                Btn_Agregar_Partida.Enabled = false;
                Div_Proveedores.Visible = false;
                Grid_Proveedores.DataSource = new DataTable();
                Grid_Proveedores.DataBind();
                Session["Dt_Proveedores"] = null;
                //No se debe permir modificar el Password
                //Txt_Password.Enabled = false;
                break;
            case "Dar de Alta":
                //Validamos que se llenen todos los campos requeridos
                Validar_Contenido_Controles();
                //En caso de que pase las validaciones
                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    if (Grid_Correos.Rows.Count < 1)
                    {
                        Ibtn_agregar_Correo_Click(sender, null);
                    }
                    //Como primer paso cargamos los datos del Proveedor
                    Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                    Clase_Negocio = Cargar_Datos_Proveedor(Clase_Negocio);
                   
                    //Damos de Alta el Proveedor
                    String Mensaje = Clase_Negocio.Alta_Proveedor();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('" + Mensaje + "');", true);
                    Configurar_Formulario("Inicio");
                }


                break;
        }


    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Modificar.ToolTip)
        {
            case "Modificar":
                if (Txt_Proveedor_ID.Text.Trim() == String.Empty)
                {

                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Es necesario seleccionar un Proveedor";
                    Lbl_Mensaje_Error.Visible = true;
                }
                else
                {
                    Configurar_Formulario("Modificar");
                       
                }
                break;  
            case "Actualizar":
                //Validamos que se llenen todos los campos requeridos
                Validar_Contenido_Controles();

                if (Grid_Correos.Rows.Count < 1)
                {
                    Ibtn_agregar_Correo_Click(sender, null);
                }

                //En caso de que pase las validaciones
                if (Div_Contenedor_Msj_Error.Visible == false)
                {
                    //Como primer paso cargamos los datos del Proveedor
                    Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                    Clase_Negocio = Cargar_Datos_Proveedor(Clase_Negocio);

                    //Modificamos el Proveedor
                    String Mensaje = Clase_Negocio.Modificar_Proveedor();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('" + Mensaje + "');", true);
                    Configurar_Formulario("Inicio");

                }


                break;
        }
    }

    protected void  Chk_Actualizacion_CheckedChanged(object sender, EventArgs e)
    {
        if(Chk_Actualizacion.Checked == true)
        {
            if(Txt_Ultima_Actualizacion.Text != String.Empty)
            {
                Session["Ultima_Actualizacion"]= Txt_Ultima_Actualizacion.Text.Trim();
            }
            Txt_Ultima_Actualizacion.Text = DateTime.Now.ToString("dd/MMM/yyyy");
        }

        if(Chk_Actualizacion.Checked == false)
        {
            if(Session["Ultima_Actualizacion"] != null)
                Txt_Ultima_Actualizacion.Text = Session["Ultima_Actualizacion"].ToString().Trim();
            else 
                Txt_Ultima_Actualizacion.Text = "";
        }
    }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        switch (Btn_Salir.ToolTip)
        {
            case "Cancelar":
                Configurar_Formulario("Inicio");
                Limpiar_Componentes();
                Habilitar_Componentes(false);


                break;
            case "Inicio":
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                Limpiar_Componentes();
                Habilitar_Componentes(false);

                break;
        }
    }

    protected void Cmb_Conceptos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        //LLENAMOS EL GRID DE PARTIDAS PARA AGREGARLAS AL PROVEEDOR
        Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        Clase_Negocio.P_Concepto_ID = Cmb_Conceptos.SelectedValue;
        DataTable Dt_Partidas = Clase_Negocio.Consultar_Partidas_Especificas();
        Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Partidas_Generales, Dt_Partidas);
        if (Cmb_Conceptos.SelectedIndex == 0)
        {
            //Deshabilitamos el combo de Partidas 
            Cmb_Partidas_Generales.Enabled = false;
            Btn_Agregar_Partida.Enabled = false;
            Cmb_Partidas_Generales.Items.Clear();

        }
        else
        {
            Btn_Agregar_Partida.Enabled = true;
            Cmb_Partidas_Generales.Enabled = true;
        }
    }

    protected void Btn_Agregar_Partida_Click(object sender, ImageClickEventArgs e)
    {
        //Si se selecciona se agrega al grid.
        if (Cmb_Partidas_Generales.SelectedIndex > 0)
        {
            Cls_Cat_Com_Proveedores_Negocio Clase_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
            DataTable Dt_Consulta_Partidas_Proveedores = new DataTable();
            if (Session["Dt_Consulta_Partidas_Proveedores"] != null)
            {
                Dt_Consulta_Partidas_Proveedores = (DataTable)Session["Dt_Consulta_Partidas_Proveedores"];
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {
                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "",
                    "alert('No se puede agregar la dependencia, ya se ha agregado');", true);
                }
                else
                {
                    DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                    Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                    Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                    Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                    Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                    Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Grid_Partidas_Generales.DataBind();
                    //Agregamos el Concepto al Grid de Conceptos 
                    Agregar_Concepto_Proveedor();
                }
            }
            else
            {
                Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA_GENERICA_ID", typeof(System.String));
                Dt_Consulta_Partidas_Proveedores.Columns.Add("PARTIDA", typeof(System.String));
                Dt_Consulta_Partidas_Proveedores.Columns.Add("CONCEPTO_ID", typeof(System.String));
                Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Consulta_Partidas_Proveedores.Select("PARTIDA_GENERICA_ID='" + Cmb_Partidas_Generales.SelectedValue.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {
                    Div_Contenedor_Msj_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "+ No se puede agregar la Partida, pues ya esta agregada";
                }
                else
                {
                    DataRow Fila = Dt_Consulta_Partidas_Proveedores.NewRow();
                    Fila["PARTIDA_GENERICA_ID"] = Cmb_Partidas_Generales.SelectedValue.ToString();
                    Fila["PARTIDA"] = Cmb_Partidas_Generales.SelectedItem.Text;
                    Fila["CONCEPTO_ID"] = Cmb_Conceptos.SelectedValue.ToString();
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Dt_Consulta_Partidas_Proveedores.Rows.Add(Fila);
                    Dt_Consulta_Partidas_Proveedores.AcceptChanges();
                    Grid_Partidas_Generales.DataSource = Dt_Consulta_Partidas_Proveedores;
                    Session["Dt_Consulta_Partidas_Proveedores"] = Dt_Consulta_Partidas_Proveedores;
                    Grid_Partidas_Generales.DataBind();
                    Agregar_Concepto_Proveedor();
                }
            }
            Cmb_Partidas_Generales.SelectedIndex = 0;
        }
        else
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Proveedores", "alert('Seleccione una Partida General.');", true);
    }    

    protected void Btn_Busqueda_Avanzada_Click(object sender, EventArgs e)
    {
        Div_Busqueda_Avanzada.Visible = true;
    }

    protected void Btn_Imprimir_Excel_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Consulta = new DataTable();
        Cls_Cat_Com_Proveedores_Negocio Partida = new Cls_Cat_Com_Proveedores_Negocio();
        Dt_Consulta = Partida.Consulta_Datos_Reporte();
        Llenar_Reporte_Excel(Dt_Consulta);
    }

    private void Llenar_Reporte_Excel(DataTable Dt_Reporte)
     {
        Cls_Cat_Com_Proveedores_Negocio Partida = new Cls_Cat_Com_Proveedores_Negocio();
        WorksheetCell Celda = new WorksheetCell();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        String Nombre_Archivo = "";
        String Ruta_Archivo = "";
        String Tipo_Estilo = "";
        String Tipo_Estilo_Total = "";
        String Tipo_Ventas = "";
        Double Importe = 0.0;
        String Informacion_Registro = "";
        Int32 Contador_Estilo= 0;
        Int32 Operacion = 0;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            IBtn_Imagen_Error.Visible = false;

            Nombre_Archivo = "Rpt_Proveedores.xls";
            Ruta_Archivo = @Server.MapPath("../../Archivos/" + Nombre_Archivo);

            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "Reporte_Proveedores";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera2 = Libro.Styles.Add("HeaderStyle2");
            //  Creamos el estilo cabecera 3 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido2 = Libro.Styles.Add("BodyStyle2");
            //  Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");
            //  Creamos el estilo contenido del presupuesto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales = Libro.Styles.Add("Totales");
            //  Creamos el estilo contenido del concepto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Totales2 = Libro.Styles.Add("Totales2");
            //  Creamos el estilo contenido del concepto para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Presupuesto_Total = Libro.Styles.Add("Total");
            //Creamos el estilo contenido del concepto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Concepto = Libro.Styles.Add("Concepto");
            //Creamos el estilo contenido del concepto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Ventas = Libro.Styles.Add("Ventas");
            //Creamos el estilo contenido del concepto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Ventas2 = Libro.Styles.Add("Ventas2");
            //  Creamos el estilo contenido del concepto para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Sin_Contenido = Libro.Styles.Add("Sin_Contenido");


            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera
            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "Gray";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la cabecera2
            Estilo_Cabecera2.Font.FontName = "Tahoma";
            Estilo_Cabecera2.Font.Size = 10;
            Estilo_Cabecera2.Font.Bold = true;
            Estilo_Cabecera2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera2.Alignment.Rotate = 0;
            Estilo_Cabecera2.Font.Color = "#FFFFFF";
            Estilo_Cabecera2.Interior.Color = "DarkGray";
            Estilo_Cabecera2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para la BodyStyle2
            Estilo_Contenido2.Font.FontName = "Tahoma";
            Estilo_Contenido2.Font.Size = 9;
            Estilo_Contenido2.Font.Bold = false;
            Estilo_Contenido2.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido2.Alignment.Rotate = 0;
            Estilo_Contenido2.Font.Color = "#000000";
            Estilo_Contenido2.Interior.Color = "LightGray";
            Estilo_Contenido2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el BodyStyle
            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 9;
            Estilo_Contenido.Font.Bold = false;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido.Alignment.Rotate = 0;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");


            //  estilo para el presupuesto (importe)
            Estilo_Totales.Font.FontName = "Tahoma";
            Estilo_Totales.Font.Size = 9;
            Estilo_Totales.Font.Bold = false;
            Estilo_Totales.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales.Alignment.Rotate = 0;
            Estilo_Totales.Font.Color = "#000000";
            Estilo_Totales.Interior.Color = "White";
            Estilo_Totales.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Totales.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para el presupuesto (importe)
            Estilo_Totales2.Font.FontName = "Tahoma";
            Estilo_Totales2.Font.Size = 9;
            Estilo_Totales2.Font.Bold = false;
            Estilo_Totales2.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Totales2.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Totales2.Alignment.Rotate = 0;
            Estilo_Totales2.Font.Color = "#000000";
            Estilo_Totales2.Interior.Color = "LightGray";
            Estilo_Totales2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Totales2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Totales2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            
            //estilo para el  (Total del importe)
            Estilo_Presupuesto_Total.Font.FontName = "Tahoma";
            Estilo_Presupuesto_Total.Font.Size = 9;
            Estilo_Presupuesto_Total.Font.Bold = true;
            Estilo_Presupuesto_Total.Alignment.Horizontal = StyleHorizontalAlignment.Right;
            Estilo_Presupuesto_Total.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Presupuesto_Total.Alignment.Rotate = 0;
            Estilo_Presupuesto_Total.Font.Color = "#000000";
            Estilo_Presupuesto_Total.Interior.Color = "Yellow";
            Estilo_Presupuesto_Total.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Presupuesto_Total.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Presupuesto_Total.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Presupuesto_Total.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Presupuesto_Total.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //estilo para el Concepto
            Estilo_Concepto.Font.FontName = "Tahoma";
            Estilo_Concepto.Font.Size = 9;
            Estilo_Concepto.Font.Bold = false;
            Estilo_Concepto.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Concepto.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Concepto.Font.Color = "#000000";
            Estilo_Concepto.Interior.Color = "White";
            Estilo_Concepto.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Concepto.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Concepto.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Concepto.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Concepto.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  para ventas
            Estilo_Ventas.Font.FontName = "Tahoma";
            Estilo_Ventas.Font.Size = 9;
            Estilo_Ventas.Font.Bold = false;
            Estilo_Ventas.Alignment.Horizontal = StyleHorizontalAlignment.JustifyDistributed;
            Estilo_Ventas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Ventas.Font.Color = "#000000";
            Estilo_Ventas.Interior.Color = "White";
            Estilo_Ventas.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Ventas.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  para ventas2
            Estilo_Ventas2.Font.FontName = "Tahoma";
            Estilo_Ventas2.Font.Size = 9;
            Estilo_Ventas2.Font.Bold = false;
            Estilo_Ventas2.Alignment.Horizontal = StyleHorizontalAlignment.JustifyDistributed;
            Estilo_Ventas2.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Ventas2.Font.Color = "#000000";
            Estilo_Ventas2.Interior.Color = "LightGray";
            Estilo_Ventas2.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Ventas2.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas2.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas2.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Ventas2.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");

            //  estilo para los sin documentos
            Estilo_Sin_Contenido.Font.FontName = "Arial";
            Estilo_Sin_Contenido.Font.Size = 9;
            Estilo_Sin_Contenido.Font.Bold = true;
            Estilo_Sin_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Sin_Contenido.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Sin_Contenido.Alignment.Rotate = 0;
            Estilo_Sin_Contenido.Font.Color = "#000000";
            Estilo_Sin_Contenido.Interior.Color = "White";
            Estilo_Sin_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Sin_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Sin_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Sin_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Sin_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            //*************************************** fin de los estilos***********************************************************

            //***************************************Inicio del reporte Proveedores por partida Hoja 1***************************
            
            //  Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("REPORTE DE PROVEEDORES");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();

            //if (Chk_Partida.Checked == true)
            //{
                //  Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  1 Partida
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  2 nombre del proveedor.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  3 compañia.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  4 RFC
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(125));//  5 Contacto.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//  6 Estatus.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  7 Direccion.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  8 Colonia.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  9 Ciudad.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  10 Estado.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  11 Codigo postal.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  12 telefono 1.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  13 telefono 2.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  14 nextel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  15 fax.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  16 e-mail.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  17 Representante legal.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  18 tipo fiscal.

                //  se llena el encabezado principal
                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("REPORTE DE PROVEEDORES");
                Celda.MergeAcross = 8; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle";

                Renglon = Hoja.Table.Rows.Add();
                Celda = Renglon.Cells.Add("JUNTA DE AGUA POTABLE, DRENAJE, ALCANTARILLADO Y SANEAMIENTO DEL MUNICIPIO DE IRAPUATO, GUANAJUATO, " + DateTime.Now);
                Celda.MergeAcross = 8; // Merge 6 cells together
                Celda.StyleID = "HeaderStyle";

                Dt_Consulta = Partida.Consulta_Datos_Reporte();

                //  para los titulos de las columnas
                Renglon = Hoja.Table.Rows.Add();
                Renglon = Hoja.Table.Rows.Add();

                Celda = Renglon.Cells.Add("PARTIDA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("NOMBRE DEL PROVEEDOR");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("COMPAÑIA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("RFC");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("CONTACTO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("ESTATUS");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("DIRECCION");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("COLONIA");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("CIUDAD");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("ESTADO");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("CODIGO_POSTAL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("TELEFONO1");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("TELEFONO2");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("NEXTEL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("FAX");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("E_MAIL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("TIPO_FISCAL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";

                Celda = Renglon.Cells.Add("REPRESENTANTE_LEGAL");
                Celda.MergeDown = 1; // Merge two cells together
                Celda.StyleID = "HeaderStyle2";


                Renglon = Hoja.Table.Rows.Add();

                //  para llenar el reporte
                if (Dt_Reporte.Rows.Count > 0)
                {
                    //  Se comienza a extraer la informaicon de la onsulta
                    foreach (DataRow Renglon_Reporte in Dt_Reporte.Rows)
                    {
                        Contador_Estilo++;
                        Operacion = Contador_Estilo % 2;
                        if (Operacion == 0)
                        {
                            //Estilo_Concepto.Interior.Color = "LightGray";
                            Tipo_Estilo = "BodyStyle2";
                        }
                        else
                        {
                            Tipo_Estilo = "BodyStyle";
                            //Estilo_Concepto.Interior.Color = "White";
                        }

                        Renglon = Hoja.Table.Rows.Add();
                        //  1 para LA PARTIDA
                        Informacion_Registro = (Renglon_Reporte["PARTIDA"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  2 para el NOMBRE del PROVEEDOR 
                        Informacion_Registro = (Renglon_Reporte["NOMBRE"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  3 para la compañia
                        Informacion_Registro = (Renglon_Reporte["COMPANIA"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  4 para el rfc 
                        Informacion_Registro = (Renglon_Reporte["RFC"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  5 para el contacto
                        Informacion_Registro = (Renglon_Reporte["CONTACTO"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  6 para el estatus
                        Informacion_Registro = (Renglon_Reporte["ESTATUS"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  7 para LA DIRECCION
                        Informacion_Registro = (Renglon_Reporte["DIRECCION"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  8 para LA COLONIA
                        Informacion_Registro = (Renglon_Reporte["COLONIA"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  9 para LA CIUDAD
                        Informacion_Registro = (Renglon_Reporte["CIUDAD"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  10 para el ESTADO
                        Informacion_Registro = (Renglon_Reporte["ESTADO"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  11 para el CODIGO_POSTAL
                        Informacion_Registro = (Renglon_Reporte["CODIGO_POSTAL"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  12 para el telefono 1
                        Informacion_Registro = (Renglon_Reporte["TELEFONO1"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  13 para el telefono 2
                        Informacion_Registro = (Renglon_Reporte["TELEFONO2"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  14 para el NEXTEL
                        Informacion_Registro = (Renglon_Reporte["NEXTEL"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  15 para el FAX
                        Informacion_Registro = (Renglon_Reporte["FAX"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  16 para el E-MAIL
                        Informacion_Registro = (Renglon_Reporte["E_MAIL"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  17 para el TIPO FISCAL
                        Informacion_Registro = (Renglon_Reporte["TIPO_FISCAL"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                        //  18 para el representante legal
                        Informacion_Registro = (Renglon_Reporte["REPRESENTANTE_LEGAL"].ToString());
                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("" + Informacion_Registro, "" + Tipo_Estilo));
                    }
                }
                else
                {
                    Renglon = Hoja.Table.Rows.Add();
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell("---", "Sin_Contenido"));
                }
            //}
            //***************************************Fin del reporte Proveedores por partida Hoja 1***************************

            //  se guarda el documento
            Libro.Save(Ruta_Archivo);
            //  mostrar el archivo
            Mostrar_Reporte(Nombre_Archivo);



            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta_Archivo);
            Response.Charset = "UTF-8";
            //Response.ContentEncoding = Encoding.Default;
            //Libro.Save(Response.OutputStream);
            //Response.End();
        }// fin try

        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            IBtn_Imagen_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
        }
     }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    /// USUARIO CREO:        Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:          18/Enero/2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar)
    {
        String Ruta = "../../Archivos/" + Nombre_Reporte_Generar;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            IBtn_Imagen_Error.Visible = false;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=100,height=100')", true);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            IBtn_Imagen_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
        

    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            
            // Consulta_Proveedores(); //Consultar los proveedores que coincidan con el nombre porporcionado por el usuario
            Cls_Cat_Com_Proveedores_Negocio RS_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Proveedores; //Variable que obtendrá los datos de la consulta 
            if (Txt_Busqueda_Nombre_Comercial.Text != "")
            {
                RS_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Busqueda_Nombre_Comercial.Text;
            }
            if (Txt_Busqueda_Padron_Proveedor.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Proveedor_ID = String.Format("{0:0000000000}", Convert.ToInt32(Txt_Busqueda_Padron_Proveedor.Text.Trim()));
            }
            if (Txt_Busqueda_Razon_Social.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Razon_Social = Txt_Busqueda_Razon_Social.Text.Trim();
            }
            if (Txt_Busqueda_RFC.Text.Trim() != String.Empty)
            {
                RS_Consulta_Cat_Com_Proveedores.P_RFC = Txt_Busqueda_RFC.Text.Trim();
            }
            if (Cmb_Busqueda_Estatus.SelectedIndex != 0)
            {
                RS_Consulta_Cat_Com_Proveedores.P_Estatus = Cmb_Busqueda_Estatus.SelectedItem.Text.Trim();
            }
            //Consulta los Proveedores con sus datos generales
            Dt_Proveedores = RS_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor();
            if (Dt_Proveedores.Rows.Count != 0)
            {
                Grid_Proveedores.DataSource = Dt_Proveedores;
                Grid_Proveedores.DataBind();
                Session["Dt_Proveedores"] = Dt_Proveedores;
            }
            else
            {
                Grid_Proveedores.EmptyDataText = "No se han encontrado registros.";
                //Lbl_Mensaje_Error.Text = "+ No se encontraron datos <br />";
                Grid_Proveedores.DataSource = new DataTable();
                Grid_Proveedores.DataBind();
            }


            Limpiar_Controles_Busqueda_Avanzada(); //Limpia los controles de la forma
            Limpiar_Componentes();
            //Si no se encontraron Proveedores con un nombre similar al proporcionado por el usuario entonces manda un mensaje al usuario
            Btn_Salir.ToolTip = "Regresar";
            if (Grid_Proveedores.Rows.Count <= 0)
            {
                Div_Contenedor_Msj_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontraron Proveedores con el nombre proporcionado <br />";
                Div_Proveedores.Visible = false;
            }
            else
            {
                Div_Proveedores.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        Div_Busqueda_Avanzada.Visible = false;
    }

    protected void Btn_Limpiar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Limpiar_Controles_Busqueda_Avanzada();
    }

    protected void Btn_Cerrar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {
        Div_Busqueda_Avanzada.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Limpiar_Controles_Busqueda_Avanzada();
    }


    protected void Chk_Fisica_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Fisica.Checked == true)
            Chk_Moral.Checked = false;

    }

    protected void Chk_Moral_CheckedChanged(object sender, EventArgs e)
    {
        if (Chk_Moral.Checked == true)
            Chk_Fisica.Checked = false;
    }
    #endregion



    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN:  Ibtn_agregar_Correo_Click
    ///DESCRIPCIÓN:Evento del boton agregar correos
    ///PARAMETROS:
    ///CREO: Susana Trigueros Armenta
    ///FECHA_CREO: 26/MAR/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Ibtn_agregar_Correo_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;

        Validar_Email();
        if(Lbl_Mensaje_Error.Text ==String.Empty)
        {
            //Agregamos el correo. 
            DataTable Dt_Correos = new DataTable();
            if (Session["Dt_Correos"] != null)
            {
                Dt_Correos = (DataTable)Session["Dt_Correos"];
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Correos.Select("correo='" + Txt_Correo.Text.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {

                }
                else
                {
                    DataRow Fila = Dt_Correos.NewRow();
                    Fila["correo"] = Txt_Correo.Text.Trim();
                    Session["Dt_Correos"] = Dt_Correos;
                    Dt_Correos.Rows.Add(Fila);
                    Dt_Correos.AcceptChanges();
                    Grid_Correos.DataSource = Dt_Correos;
                    Session["Dt_Correos"] = Dt_Correos;
                    Grid_Correos.DataBind();


                }
            }
            else
            {
                Dt_Correos.Columns.Add("correo", typeof(System.String));

                Session["Dt_Correos"] = Dt_Correos;
                DataRow[] Fila_Nueva;
                Fila_Nueva = Dt_Correos.Select("correo='" + Txt_Correo.Text.Trim() + "'");
                if (Fila_Nueva.Length > 0)
                {

                }
                else
                {
                    DataRow Fila = Dt_Correos.NewRow();
                    Fila["correo"] = Txt_Correo.Text.Trim();
                    Session["Dt_Correos"] = Dt_Correos;
                    Dt_Correos.Rows.Add(Fila);
                    Dt_Correos.AcceptChanges();
                    Grid_Correos.DataSource = Dt_Correos;
                    Session["Dt_Correos"] = Dt_Correos;
                    Grid_Correos.DataBind();
                }
            }

        }
    }
    protected void Grid_Correos_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
}
