﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;

public partial class paginas_Compras_Frm_Rpt_Com_Pedidos_Generales_Detalles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Grid_Reporte.DataSource = (DataTable)Session["Reporte_Pedidos"];
            Grid_Reporte.DataBind();
        }
    }
}
