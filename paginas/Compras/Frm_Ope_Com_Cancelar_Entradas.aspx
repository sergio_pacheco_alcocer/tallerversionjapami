<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Com_Cancelar_Entradas.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Com_Cancelar_Entradas" 
Title="Seguimiento a Requisiciones" Culture="es-MX"%>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<!--SCRIPT PARA LA VALIDACION QUE NO EXPiRE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesi�n
        var CONTROLADOR = "../../Mantenedor_Session.ashx";
        //Ejecuta el script en segundo plano evitando as� que caduque la sesi�n de esta p�gina
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }
        //Temporizador para matener la sesi�n activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);        
    //-->
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
        <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>                                    
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                <%--Div Encabezado--%>
                
                
                <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Cancelaci�n de Entradas
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" CssClass="Img_Button" onclick="Btn_Modificar_Click" />
                                <asp:ImageButton ID="Btn_Listar" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Listar" OnClick="Btn_Listar_Click" />
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                </div>
                <%--Div listado de requisiciones--%>
                <div id="Div_Listado_Requisiciones" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;">
                                No. Entrada</td>
                            <td style="width: 43%;">
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="13" Width="15%"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" 
                                    TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked" 
                                    WatermarkText="&lt;No&gt;">
                                </cc1:TextBoxWatermarkExtender>
                                <cc1:FilteredTextBoxExtender ID="FTBE_Txt_Busqueda" runat="server" 
                                    FilterType="Custom" TargetControlID="Txt_Busqueda" ValidChars="0123456789">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 12%;" align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>                    
                        <tr>
                            <td style="width: 15%;">
                                Fecha
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                :&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                            <td align="right" visible="false">
                                &nbsp;</td>
                            <td visible="false">
                                &nbsp;</td>
                        </tr>
                        <tr>
                        
                            <td style="width: 15%;">
                                &nbsp;</td>
                            <td style="width: 43%;">
                                &nbsp;</td>
                            <td align="right" visible="false" colspan="2">
                                <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" 
                                    ToolTip="Consultar" />
                            </td>
                            <tr>
                                <td align="center" colspan="4" style="width: 99%">
                                    <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;">
                                        <asp:GridView ID="Grid_Entradas" runat="server" AllowSorting="True" 
                                            AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="NO_ENTRADA" 
                                            EmptyDataText="No se encontraron contrarecibos" GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" Width="100%" 
                                            onpageindexchanging="Grid_Entradas_PageIndexChanging">
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Seleccionar" runat="server" 
                                                            CommandArgument='<%# Eval("NO_ENTRADA") %>' 
                                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                            OnClick="Btn_Seleccionar_Click" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                <asp:BoundField DataField="NO_ENTRADA" HeaderText="No. Entrada" 
                                                    Visible="True" DataFormatString="{0:N0}">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FOLIO" 
                                                    HeaderText="OC" Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                                    <ItemStyle HorizontalAlign="Left" Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="REQ" HeaderText="Req" Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="FECHA" HeaderText="Fecha Entrada" 
                                                    Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="ESTATUS" HeaderText="Estatus">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="SUBTOTAL" DataFormatString="{0:N2}" 
                                                    HeaderText="SubTotal" />
                                                <asp:BoundField DataField="IVA" DataFormatString="{0:N2}" 
                                                    HeaderText="IVA" />
                                                <asp:BoundField DataField="TOTAL" DataFormatString="{0:N2}" 
                                                    HeaderText="Total" />
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                                    </div>
                                </td>
                            </tr>
                        
                        </tr>
                    </table>
                </div>
                <%--Div Contenido--%>
                <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;" align="left">
                                No. Entrada</td>
                            <td align="left" style="width: 35%;">
                                <asp:TextBox ID="Txt_No_Entrada" runat="server" Width="46%"></asp:TextBox>
                            </td>                                
                            <td style="width: 15%;">
                                Req
                            </td>
                            <td style="width: 35%;">
                                <asp:TextBox ID="Txt_Req" runat="server" Width="46%"></asp:TextBox>
                            </td>

                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                OC</td>
                            <td align="left">
                                <asp:TextBox ID="Txt_OC" runat="server" Width="46%"></asp:TextBox>
                            </td>
                            <td>
                                Estatus
                            </td>
                            <td>
                                
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="46%"> 
                                    <asp:ListItem>CANCELADA</asp:ListItem>
                                </asp:DropDownList>
                                
                            </td>
                        </tr>  
                        <tr>                            
                            <td>
                                Comentarios
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Comentarios" runat="server" Width="96%" 
                                    TextMode="MultiLine"></asp:TextBox>
                            </td>
                        </tr>                      
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Facturas" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="100%"                                     
                                    style="white-space:normal" 
                                    onpageindexchanging="Grid_Facturas_PageIndexChanging" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Producto">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="35%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ABREVIATURA" HeaderText="Unidad">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="CANTIDAD" HeaderText="Cantidad"
                                            DataFormatString="{0:N0}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Center" Width="11%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COSTO_PROMEDIO" HeaderText="$ Unitario" 
                                            DataFormatString="{0:N2}">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COSTO_COMPRA" HeaderText="SubTotal" 
                                            DataFormatString="{0:N2}">
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left"  />
                                        </asp:BoundField>                                        
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                        <tr>    
                            <td></td>                        
                            <td></td>                        
                            <td >
                                </td>
                            <td align="right">
                                SubTotal&nbsp;&nbsp; <asp:TextBox ID="Txt_Subtotal" runat="server" Width="46%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td></td>                        
                            <td></td>                                                    
                            <td >
                               </td>
                            <td align="right">
                                 IVA&nbsp;&nbsp;<asp:TextBox ID="Txt_IVA" runat="server" Width="46%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="height:20px;">
                            <td></td>                        
                            <td></td>                        
                            <td >
                                </td>
                            <td align="right">
                                Total&nbsp;&nbsp;<asp:TextBox ID="Txt_Total" runat="server" Width="46%"></asp:TextBox>
                            </td>
                        </tr>
                        
                    </table>
                </div>
            </ContentTemplate>
        </asp:UpdatePanel>
    </div>    
</asp:Content>

