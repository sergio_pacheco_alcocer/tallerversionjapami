﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
//using JAPAMI.Generar_Cotizacion.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Areas.Negocios;
//using JAPAMI.Administrar_Cotizaciones.Negocios;
using System.Text.RegularExpressions;
using System.Collections.Generic;
using JAPAMI.Filtrar_Cotizaciones.Negocio;
using JAPAMI.Distribuir_a_Proveedores.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Generar_Requisicion.Datos;
using JAPAMI.Administrar_Requisiciones.Negocios;



public partial class paginas_Compras_Frm_Ope_Com_Filtrar_Cotizaciones : System.Web.UI.Page
{
    #region VARIABLES / CONSTANTES
    //objeto de la clase de negocio de dependencias para acceder a la clase de datos y realizar copnexion
    private Cls_Cat_Dependencias_Negocio Dependencia_Negocio;
    //objeto de la clase de negocio de Cotizacion para acceder a la clase de datos y realizar copnexion
    private Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Cotizacion_Negocio;
    //objeto en donde se guarda un id de producto o servicio para siempre tener referencia
    //private static String PS_ID;
    // private static String PS_ID = "PS_ID";
    //private Cls_Ope_Com_Administrar_Cotizaciones_Negocio Administrar_Cotizacion;
    private int Contador_Columna;
    private String Informacion;
    private static DataTable Dt_Requisiciones;
    private static String P_Dt_Productos_Servicios = "P_Dt_Productos_Servicios_Filtrar";
    //private static String P_Dt_Partidas = "P_Dt_Partidas";
    //private static String P_Dt_Productos = "P_Dt_Productos_Filtrar";
    //private static String P_Dt_Cotizaciones = "P_Dt_Cotizaciones_Transitorias_Filtrar";
    //private static String P_Dt_Productos_Servicios_Modal = "P_Dt_Productos_Servicios_Modal";

    //private const String Operacion_Comprometer = "COMPROMETER";
    //private const String Operacion_Descomprometer = "DESCOMPROMETER";
    //private const String Operacion_Quitar_Renglon = "QUITAR";
    //private const String Operacion_Agregar_Renglon_Nuevo = "AGREGAR_NUEVO";
    //private const String Operacion_Agregar_Renglon_Copia = "AGREGAR_COPIA";

    private const String SubFijo_Cotizacion = "RQ-";
    //private const String EST_EN_CONSTRUCCION = "EN CONSTRUCCION";
    //private const String EST_GENERADA = "GENERADA";
    //private const String EST_CANCELADA = "CANCELADA";
    //private const String EST_REVISAR = "REVISAR";
    //private const String EST_RECHAZADA = "RECHAZADA";
    //private const String EST_AUTORIZADA = "AUTORIZADA";
    //private const String EST_PROCESAR = "PROCESAR";
    //private const String EST_FILTRADA = "FILTRADA";

    //private const String TIPO_STOCK = "STOCK";
    //private const String TIPO_TRANSITORIA = "TRANSITORIA";

    private const String MODO_LISTADO = "LISTADO";
    private const String MODO_INICIAL = "INICIAL";
    private const String MODO_MODIFICAR = "MODIFICAR";
    private const String MODO_SELECTED_GRID = "SELECTED";

    private static DateTime _DateTime;
    private static int dias;

    #endregion

    #region PAGE LOAD / INIT
    protected void Page_Load(object sender, EventArgs e)
    {
        //Valores de primera vez
        if (!IsPostBack)
        {
            Session["Activa"] = true;
            ViewState["SortDirection"] = "DESC";
            
            //Limites_Fechas();
           
            Llenar_Grid_Cotizaciones();
            Habilitar_Controles(MODO_LISTADO);
        }
       
    }
   
    #endregion

    #region EVENTOS

   
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        //Limpiar_Modal_Produtos_Servicios();
        if (Btn_Modificar.ToolTip == "Modificar")
        {
            Habilitar_Controles(MODO_MODIFICAR);
        }
        else if (Btn_Modificar.ToolTip == "Actualizar")
        {
            String Mensaje = "";
            //Proceso para modificar
            if (Cmb_Estatus.SelectedIndex != 0)
            {
                if (!String.IsNullOrEmpty(Txt_Comentario.Text))
                {
                    Cls_Ope_Com_Requisiciones_Datos.Registrar_Historial(Cmb_Estatus.SelectedValue.Trim(), Grid_Cotizaciones.SelectedDataKey["No_Requisicion"].ToString().Trim());

                    Cls_Ope_Com_Administrar_Requisiciones_Negocio Administrar_Requisicion = new Cls_Ope_Com_Administrar_Requisiciones_Negocio();
                    Administrar_Requisicion.P_Requisicion_ID = Txt_Folio.Text.Replace("RQ-", "");
                    Administrar_Requisicion.P_Comentario = Txt_Comentario.Text;
                    Administrar_Requisicion.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Administrar_Requisicion.P_Estatus = Cmb_Estatus.SelectedValue.Trim();
                    Administrar_Requisicion.Alta_Observaciones();

                    
                    Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
                    //Clase_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;
                    Clase_Negocio.P_Estatus = Cmb_Estatus.SelectedValue.Trim();
                    Clase_Negocio.P_Requisicion = Txt_Folio.Text.Replace("RQ-","");
                     Clase_Negocio.Modificacion_Estatus();

                    Habilitar_Controles(MODO_LISTADO);
                    Llenar_Grid_Cotizaciones();
                    
                }else
                    Mostrar_Error("Es necesario: <br> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Introducir un Comentario <br>", true);
                
            }
            else
            {
                Mostrar_Error("Es necesario: <br> &nbsp &nbsp &nbsp &nbsp &nbsp &nbsp Seleccionar un estatus", true);
            }
        }
    }

    

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Cancelar")
        {
            //if (Btn_Nuevo.ToolTip == "Dar de Alta")
            //{
            //    Limpiar_Formulario();
            //}
            //Habilitar_Controles(MODO_INICIAL);
            //String[] Datos_Combo = { EST_AUTORIZADA };
            //Llenar_Combo(Cmb_Estatus, Datos_Combo);
            //Cmb_Estatus.SelectedValue = EST_AUTORIZADA;
            Habilitar_Controles(MODO_LISTADO);
        }
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Evento_Combo_Dependencia()
    {
        //Cargar los programas de la dependencia seleccionada
        // Cmb_Programa.Items.Clear();
        Cotizacion_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
        //Cotizacion_Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedValue;
        //DataTable Data_Table_Proyectos = Cotizacion_Negocio.Consultar_Proyectos_Programas();
        //Cls_Util.Llenar_Combo_Con_DataTable(Cmb_Programa, Data_Table_Proyectos);
        // Cmb_Programa.SelectedIndex = 0;
        //Limpiar las partidas
        // Cmb_Partida.Items.Clear();
        //Cmb_Partida.Items.Add("<<SELECCIONAR>>");
        //Cmb_Partida.SelectedIndex = 0;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Dependencia_SelectedIndexChanged(object sender, EventArgs e)
    {
        Evento_Combo_Dependencia();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    //protected void Btn_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    //{
    //    //Habilitar_Controles(MODO_INICIAL);
    //    Llenar_Grid_Cotizaciones();
    //    Div_Busqueda_Avanzada.Visible = true;
    //}
    protected void Btn_Busqueda_Avanzada_Click(object sender, EventArgs e)
    {
        Div_Busqueda_Avanzada.Visible = true;
    }

    protected void Btn_Limpiar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {

        Limpiar_Campos_Busqueda();
    }

    private void Limpiar_Campos_Busqueda() {
        Txt_Busqueda.Text = "";
        Txt_Fecha_Final.Text = "";
        Txt_Fecha_Inicial.Text = "";
        Cmb_Estatus_Busqueda.SelectedIndex = 0;
        //Limites_Fechas();
        Llenar_Grid_Cotizaciones();
    }

    private void Limites_Fechas() {
        _DateTime = DateTime.Now;
        dias = _DateTime.Day;
        dias = dias * -1;
        dias++;
        _DateTime = _DateTime.AddDays(dias);
        Txt_Fecha_Inicial.Text = String.Format("{0:dd/MMM/yyyy}", _DateTime);

        _DateTime = _DateTime.AddMonths(-1);
        Txt_Fecha_Final.Text = String.Format("{0:dd/MMM/yyyy}", _DateTime);
    }

    protected void Btn_Cerrar_Busqueda_Avanzada_Click(object sender, ImageClickEventArgs e)
    {
        Limpiar_Campos_Busqueda();
        Div_Busqueda_Avanzada.Visible = false;
    }


    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        //consultar filtros
        Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();

        Clase_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;
        Clase_Negocio.P_Estatus = Cmb_Estatus_Busqueda.SelectedValue.Trim();
        Clase_Negocio.P_Requisicion = Txt_Busqueda.Text.Trim();
        Clase_Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text.Trim();
        Clase_Negocio.P_Fecha_Final = Txt_Fecha_Final.Text.Trim();

        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones_Busqueda();

        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Cotizaciones.DataSource = Dt_Requisiciones;
            Grid_Cotizaciones.DataBind();
        }
        else
        {
            Session["Dt_Requisiciones"] = null;
            Grid_Cotizaciones.DataSource = new DataTable();
            Grid_Cotizaciones.DataBind();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private double Formato_Double(double numero)
    {
        try
        {
            String Str_Numero = numero.ToString("#.##");
            numero = Convert.ToDouble(Str_Numero);
        }
        catch (Exception Ex)
        {
            String Str = Ex.ToString();
            numero = 0;
        }
        return numero;
    }



    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Cerrar_Click(object sender, EventArgs e)
    {
        // Limpiar_Modal_Produtos_Servicios();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Listar_Cotizaciones_Click(object sender, ImageClickEventArgs e)
    {
        Llenar_Grid_Cotizaciones();
        Habilitar_Controles(MODO_LISTADO);
    }

    #endregion

    #region EVENTOS GRID
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    //private void Evento_Grid_Cotizaciones_Seleccionar(String Dato)
    //{

    //    Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
    //    //Clase_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;
    //    DataRow[] Registros; //Variable que obtendrá los datos de la consulta
    //    DataRow Registro;
    //    DataTable Dt_Requisiciones;

    //    try
    //    {
    //        Mostrar_Error("", false);
    //        Clase_Negocio.P_Requisicion = Grid_Cotizaciones.SelectedDataKey["No_Requisicion"].ToString();
    //        Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
    //        Registros = Dt_Requisiciones.Select("REQUISICION_ID = " + Grid_Cotizaciones.SelectedRow.Cells[1].Text);
    //        Registro = Registros[0];

    //        if (Registro != null)
    //        {

    //            Txt_Dependencia.Text = Registro[Cat_Dependencias.Campo_Nombre].ToString();
    //            Txt_Tipo.Text = Registro[Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString();
    //            Txt_Folio.Text = Registro[Ope_Com_Requisiciones.Campo_Requisicion_ID].ToString();

    //        }
    //        else
    //            Mostrar_Error("Registro nulo", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        //Lbl_Mensaje_Error.Visible = true;
    //        //Img_Error.Visible = true;
    //        //Lbl_Mensaje_Error.Text = ex.Message.ToString();
    //        Mostrar_Error(ex.Message, true);
    //    }


    //    //Actualizar_Grid_Partidas_Productos();
    //    //DataTable Dt_Comentarios = (DataTable)Grid_Comentarios.DataSource;
    //    Habilitar_Controles(MODO_INICIAL);
    //    //Txt_Justificacion.Visible = true;
    //    Construir_DataTables();
    //    //Llenar_Combos_Generales();
    //    Div_Listado_Cotizaciones.Visible = false;
    //    //Div_Contenido.Visible = true;
    //    //GridViewRow Row = Grid_Cotizaciones.SelectedRow;
    //    //String ID = Row.Cells[1].Text;
    //    Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
    //    Cotizacion_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
    //    String No_Cotizacion = Dato;//Grid_Cotizaciones.SelectedDataKey["No_Cotizacion"].ToString();
    //    DataRow[] Cotizacion = ((DataTable)Session[P_Dt_Cotizaciones]).Select("No_Cotizacion = '" + No_Cotizacion + "'");

    //    //Txt_Folio.Text = Cotizacion[0][Ope_Com_Cotizaciones.Campo_Folio].ToString();
    //    String Fecha = Cotizacion[0][Ope_Com_Cotizaciones.Campo_Fecha_Creo].ToString();
    //    Fecha = string.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fecha));
    //    //Txt_Fecha.Text = Fecha.ToUpper();
    //    //Seleccionar combo dependencia
    //    //Cmb_Dependencia.SelectedValue =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Dependencia_ID].ToString().Trim();
    //    //Seleccionar Tipo
    //    //Cmb_Tipo.SelectedValue =
    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Tipo].ToString().Trim();
    //    //Seleccionar estatus
    
    //    //Seleccionar Categoria
    //    //Cmb_Producto_Servicio.SelectedValue =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Tipo_Articulo].ToString().Trim();
    //    //Poner Justificación
    //    //Txt_Justificacion.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Justificacion_Compra].ToString();
    //    //Poner especificaciones
    //    //Txt_Especificaciones.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Especificacion_Prod_Serv].ToString();
    //    //Txt_Subtotal.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Subtotal].ToString();
    //    //Txt_IEPS.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_IEPS].ToString();
    //    //Txt_IVA.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_IVA].ToString();
    //    ////Total de la requisición
    //    //Txt_Total.Text =
    //    //    Cotizacion[0][Ope_Com_Cotizaciones.Campo_Total].ToString();
    //    //LLenar DataTable P_Dt_Productos_Servicios
    //    //Cotizacion_Negocio.P_Cotizacion_ID = No_Cotizacion;
    //    //Cotizacion_Negocio.P_Tipo_Articulo = Cmb_Producto_Servicio.SelectedValue.Trim();
    //    //Session[P_Dt_Productos_Servicios] = Cotizacion_Negocio.Consultar_Productos_Servicios_Cotizaciones();
    //    String Str_Partidas_IDs = "";
    //    String Str_Productos_IDs = "";
    //    if (Session[P_Dt_Productos_Servicios] != null && ((DataTable)Session[P_Dt_Productos_Servicios]).Rows.Count > 0)
    //    {
    //        //Grid_Productos_Servicios.DataSource = Session[P_Dt_Productos_Servicios] as DataTable;
    //        //Grid_Productos_Servicios.DataBind();
    //        ////Recorrer P_Dt_Productos_Servicios para obtener las partidas y los producos
    //        foreach (DataRow Row in ((DataTable)Session[P_Dt_Productos_Servicios]).Rows)
    //        {
    //            Str_Partidas_IDs = Str_Partidas_IDs + Row["PARTIDA_ID"].ToString() + ",";
    //            Str_Productos_IDs = Str_Productos_IDs + Row["PROD_SERV_ID"].ToString() + ",";
    //        }
    //        if (Str_Partidas_IDs.Length > 0)
    //        {
    //            Str_Partidas_IDs = Str_Partidas_IDs.Substring(0, Str_Partidas_IDs.Length - 1);
    //        }
    //        if (Str_Productos_IDs.Length > 0)
    //        {
    //            Str_Productos_IDs = Str_Productos_IDs.Substring(0, Str_Productos_IDs.Length - 1);
    //        }
    //    }
    //    //Llenar DataTable P_Dt_Productos
    //    Cotizacion_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
    //    //Cotizacion_Negocio.P_Producto_ID = Str_Productos_IDs;
    //    //Session[P_Dt_Productos] = Cotizacion_Negocio.Consultar_Poducto_Por_ID();
    //    //Actualizar 
    //    //Cls_Ope_Com_Administrar_Cotizaciones_Negocio Administrar = new Cls_Ope_Com_Administrar_Cotizaciones_Negocio();
    //    //Administrar.P_Cotizacion_ID = Txt_Folio.Text.Replace(SubFijo_Cotizacion, "");
    //    //DataSet Data_Set = Administrar.Consulta_Observaciones();
    //    //if (Data_Set != null && Data_Set.Tables[0].Rows.Count != 0)
    //    //{
    //    //    Div_Comentarios.Visible = true;
    //    //    Grid_Comentarios.DataSource = Data_Set;
    //    //    Grid_Comentarios.DataBind();
    //    //}
    //    //else
    //    //{
    //    //    Div_Comentarios.Visible = false;
    //    //    Grid_Comentarios.DataSource = null; ;
    //    //    Grid_Comentarios.DataBind();
    //    //}

    //    //Formato_Importes();



    //}
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    //protected void Btn_Seleccionar_Cotizacion_Click(object sender, ImageClickEventArgs e)
    //{
    //    String No_Cotizacion = ((ImageButton)sender).CommandArgument;
    //    Evento_Grid_Cotizaciones_Seleccionar(No_Cotizacion);
    //}


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************* 
    protected void Grid_Cotizaciones_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //Grid_Cotizaciones.DataSource = ((DataTable)Session[P_Dt_Cotizaciones]);
        //Grid_Cotizaciones.PageIndex = e.NewPageIndex;
        //Grid_Cotizaciones.DataBind();
    }
   
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Comentarios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //Administrar_Cotizacion = new Cls_Ope_Com_Administrar_Cotizaciones_Negocio();
        //Administrar_Cotizacion.P_Cotizacion_ID = Txt_Folio.Text.Replace(SubFijo_Cotizacion, "");
        //DataSet Data_Set = Administrar_Cotizacion.Consulta_Observaciones();
        //Grid_Comentarios.DataSource = Data_Set;
        //Grid_Comentarios.PageIndex = e.NewPageIndex;
        //Grid_Comentarios.DataBind();
    }
    

    #endregion

    #region METODOS

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///PARAMETROS:   
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public bool Verificar_Fecha()
    {
        bool Respuesta = false;
        //Variables que serviran para hacer la convecion a datetime las fechas y poder validarlas 
        DateTime Date1 = new DateTime();
        DateTime Date2 = new DateTime();
        try
        {
            //Convertimos el Texto de los TextBox fecha a dateTime
            //Date1 = DateTime.Parse(Txt_Fecha_Inicial.Text);
            //Date2 = DateTime.Parse(Txt_Fecha_Final.Text);
            if (Date1 <= Date2)
            {
                Respuesta = true;
            }
        }
        catch (Exception e)
        {
            String str = e.ToString();
            Respuesta = false;
        }
        return Respuesta;
    }

    ///*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Habilitar_Controles
    // DESCRIPCIÓN: Habilita la configuracion de acuerdo a la operacion     
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 30/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO
    // CAUSA_MODIFICACIÓN   
    // *******************************************************************************/
    private void Habilitar_Controles(String Modo)
    {
        try
        {
            switch (Modo)
            {
                case MODO_LISTADO:

                    Configuracion_Acceso("Frm_Ope_Com_Filtrar_Cotizaciones.aspx");
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.Visible = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Listar_Cotizaciones.Visible = false;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Limpiar_Formulario();
                    Cmb_Estatus.Enabled = false;
                    Limpiar_Campos_Busqueda();
                    Div_Busqueda_Avanzada.Visible = false;
                    break;

                case MODO_INICIAL:
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Configuracion_Acceso("Frm_Ope_Com_Filtrar_Cotizaciones.aspx");
                    Btn_Nuevo.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.Visible = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Eliminar.ToolTip = "Eliminar";
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Listar_Cotizaciones.Visible = true;
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Eliminar.ImageUrl = "~/paginas/imagenes/paginas/icono_eliminar.png";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Limpiar_Campos_Busqueda();
                    Div_Busqueda_Avanzada.Visible = false;
                    
                    break;
 
                case MODO_SELECTED_GRID:
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Modificar.Visible = true;
                    
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Salir.Visible = true;

                    Txt_Folio.Enabled = false;
                    Txt_Dependencia.Enabled = false;
                    Txt_Tipo.Enabled = false;
                    Cmb_Estatus.Enabled = false;
                    Cmb_Estatus.SelectedIndex = 0;
                    Limpiar_Campos_Busqueda();
                    Div_Busqueda_Avanzada.Visible = false;

                    break;
                //Estado de Modificar
                case MODO_MODIFICAR:
                    Btn_Listar_Cotizaciones.Visible = false;
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.Visible = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    
                    Cmb_Estatus.Enabled = true;
                    //Cmb_Estatus.Enabled = true;
                    Limpiar_Campos_Busqueda();
                    Div_Busqueda_Avanzada.Visible = false;
                   
                    break;
                default: break;
            }
        }
        catch (Exception ex)
        {
            Mostrar_Error(ex.ToString(), true);
        }
    }


    ///*******************************************************************************
    //NOMBRE DE LA FUNCIÓN: Mostrar_Información
    //DESCRIPCIÓN: Llena las areas de texto con el registro seleccionado del grid
    //RETORNA: 
    //CREO: Gustavo Angeles Cruz
    //FECHA_CREO: 24/Agosto/2010 
    //MODIFICO:
    //FECHA_MODIFICO:
    //CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private void Mostrar_Error(String txt, Boolean mostrar)
    {
        Lbl_Mensaje_Error.Visible = mostrar;
        Img_Error.Visible = mostrar;
        Lbl_Mensaje_Error.Text = txt;
    }

    ///*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Limpiar_Formulario
    // DESCRIPCIÓN: Limpia las areas de texto y deja los combos en su valor inical
    // RETORNA: 
    // CREO: 
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private void Limpiar_Formulario()
    {
        Txt_Tipo.Text = "";
        Txt_Folio.Text = "";
        Txt_Comentario.Text = "";
        Txt_Dependencia.Text = "";
        Div_Comentarios.Visible = false;
        Cmb_Estatus.SelectedIndex = 0;
             
    }

    ///*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Cotizaciones
    // DESCRIPCIÓN: Llena el grid principal de Cotizaciones
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: Diciembre/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Cotizaciones()
    {
       
        Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();

        Clase_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;

        DataTable Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
        
        if (Dt_Requisiciones.Rows.Count != 0)
        {
            Session["Dt_Requisiciones"] = Dt_Requisiciones;
            Grid_Cotizaciones.DataSource = Dt_Requisiciones;
            Grid_Cotizaciones.DataBind();
        }
        else
        {
            Session["Dt_Requisiciones"] = null;
            Grid_Cotizaciones.DataSource = new DataTable();
            Grid_Cotizaciones.DataBind();
        }
    }




    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Longitud
    ///DESCRIPCIÓN: Valida la longitud del texto que se recibe en un TextBox
    ///PARAMETROS:             
    ///CREO: Gustavo Angeles Cruz
    ///FECHA_CREO: 20/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public Boolean Validar_Longitud(TextBox Txt_Control, int Int_Tamaño)
    {
        Boolean Bln_Bandera;
        Bln_Bandera = false;
        //Verifica el tamaño de el control
        if (Txt_Control.Text.Length >= Int_Tamaño)
            Bln_Bandera = true;
        return Bln_Bandera;
    }

    ///*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Generar_Tabla_Informacion
    // DESCRIPCIÓN: Crea una tabla con la informacion que se requiere ingresar al formulario
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private void Generar_Tabla_Informacion()
    {
        Contador_Columna = Contador_Columna + 1;
        if (Contador_Columna > 2)
        {
            Contador_Columna = 0;
            Informacion += "</tr><tr>";
        }
    }

    ///*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Validaciones
    // DESCRIPCIÓN: Genera el String con la informacion que falta y ejecuta la 
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Gustavo Angeles Cruz
    // FECHA_CREO: 24/Agosto/2010 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    private Boolean Validaciones(bool Validar_Completo)
    {
        Boolean Bln_Bandera;
        Contador_Columna = 0;
        Bln_Bandera = true;
        Informacion += "<table style='width: 100%;font-size:9px;' >" +
            "<tr colspan='3'>Es necesario:</tr>" +
            "<tr>";
        //Verifica que campos esten seleccionados o tengan valor valor
        //if (Cmb_Tipo.SelectedIndex == 0)
        //{
        //    Informacion += "<td>+ Seleccionar Tipo. </td>";
        //    Bln_Bandera = false;
        //    Generar_Tabla_Informacion();
        //}
        //if (Cmb_Estatus.SelectedValue.Trim() == "")
        //{
        //    Informacion += "<td>+ Seleccionar Estatus.</td>";
        //    Bln_Bandera = false;
        //    Generar_Tabla_Informacion();
        //}
       
        Informacion += "</tr></table>";
        return Bln_Bandera;
    }




    #endregion


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Agregar_Tooltip_Combo(DropDownList Combo)
    {
        foreach (ListItem Item in Combo.Items)
        {
            Item.Attributes.Add("Title", Item.Text);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Verificar_Fecha
    ///DESCRIPCIÓN: Metodo que permite generar la cadena de la fecha y valida las fechas 
    ///en la busqueda del Modalpopup
    ///CREO: Gustavo Angeles
    ///FECHA_CREO: 9/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Productos_Servicios_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        //Grid_Productos_Servicios.DataSource = ((DataTable)Session[P_Dt_Productos_Servicios]);
        //Grid_Productos_Servicios.PageIndex = e.NewPageIndex;
        //Grid_Productos_Servicios.DataBind();
    }


    /// ******************************************************************************************
    /// NOMBRE: Grid_Cotizaciones_Sorting
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// CREÓ: Gustavo Angeles Cruz
    /// FECHA CREÓ: 11/Junio/2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// ******************************************************************************************
    protected void Grid_Cotizaciones_Sorting(object sender, GridViewSortEventArgs e)
    {
        //Grid_Sorting(Grid_Cotizaciones, ((DataTable)Session[P_Dt_Cotizaciones]), e);
    }

    /// *****************************************************************************************
    /// NOMBRE: Grid_Sorting
    /// DESCRIPCIÓN: Ordena las columnas en orden ascendente o descendente.
    /// CREÓ: Gustavo Angeles Cruz
    /// FECHA CREÓ: 11/Junio/2011
    /// MODIFICÓ:
    /// FECHA MODIFICÓ:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************
    private void Grid_Sorting(GridView Grid, DataTable Dt_Table, GridViewSortEventArgs e)
    {
        if (Dt_Table != null)
        {
            DataView Dv_Vista = new DataView(Dt_Table);
            String Orden = ViewState["SortDirection"].ToString();
            if (Orden.Equals("ASC"))
            {
                Dv_Vista.Sort = e.SortExpression + " DESC";
                ViewState["SortDirection"] = "DESC";
            }
            else
            {
                Dv_Vista.Sort = e.SortExpression + " ASC";
                ViewState["SortDirection"] = "ASC";
            }
            Grid.DataSource = Dv_Vista;
            Grid.DataBind();
        }
    }

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            //Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    protected void Grid_Cotizaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        //if (e.Row.RowType == DataControlRowType.DataRow)
        //{
        //    String Folio = e.Row.Cells[1].Text.Trim();

        //    DataRow[] Renglon = ((DataTable)Session[P_Dt_Cotizaciones]).Select("FOLIO = '" + Folio + "'");
        //    if (Renglon.Length > 0)
        //    {
        //        ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Alerta");
        //        String Estatus = Renglon[0]["ESTATUS"].ToString().Trim();
        //        if (Estatus == EST_EN_CONSTRUCCION && Renglon[0]["ALERTA"].ToString().Trim() == "AMARILLO")
        //        {                    
        //            Boton.ImageUrl = "../imagenes/gridview/circle_yellow.png";
        //            Boton.Visible = true;
        //        }
        //        else 
        //        {
        //            Boton.Visible = false;
        //        }
        //    }
        //}
    }
    protected void Grid_Cotizaciones_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Com_Filtrar_Cotizaciones_Negocio Clase_Negocio = new Cls_Ope_Com_Filtrar_Cotizaciones_Negocio();
        //Clase_Negocio.P_Cotizador_ID = Cls_Sessiones.Empleado_ID;
       
        

        try {
            Mostrar_Error("", false);
            //Clase_Negocio.P_Requisicion = Grid_Cotizaciones.SelectedRow.Cells[8].Text.Trim();
            Clase_Negocio.P_Requisicion = Grid_Cotizaciones.SelectedDataKey["No_Requisicion"].ToString();
            Dt_Requisiciones = Clase_Negocio.Consultar_Requisiciones();
            //Registros = Dt_Requisiciones.Select("NO_REQUISICION = '" + Grid_Cotizaciones.SelectedRow.Cells[8].Text.Trim()+"'");
            //Registro = Registros[0];

            if (Dt_Requisiciones.Rows.Count != null)
            {

                Txt_Dependencia.Text = Dt_Requisiciones.Rows[0]["DEPENDENCIA"].ToString().Trim();
                Txt_Tipo.Text = Dt_Requisiciones.Rows[0][Ope_Com_Requisiciones.Campo_Tipo_Articulo].ToString().Trim();
                Txt_Folio.Text = Dt_Requisiciones.Rows[0]["FOLIO"].ToString().Trim();

            }
            else
                Mostrar_Error("Registro nulo",true);
            Habilitar_Controles(MODO_SELECTED_GRID);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }


    protected void Cmb_Estatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(Cmb_Estatus.SelectedIndex != 0){
            Div_Comentarios.Visible = true;
        }else
            Div_Comentarios.Visible = false;
    }
}