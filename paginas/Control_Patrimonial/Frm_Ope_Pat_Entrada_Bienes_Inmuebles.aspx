﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx.cs" Inherits="paginas_Control_Patrimonial_Frm_Ope_Pat_Entrada_Bienes_Inmuebles" Title="Control de Bienes Inmuebles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <script type="text/javascript" language="javascript">
        
    function Limpiar_Ctlr_Campos(){
        document.getElementById("<%=Txt_Calle.ClientID%>").value=""; 
        document.getElementById("<%=Txt_Colonia.ClientID%>").value="";
        document.getElementById("<%=Cmb_Distrito.ClientID%>").value = "";
        document.getElementById("<%=Cmb_Uso.ClientID%>").value = ""; 
        document.getElementById("<%=Cmb_Destino.ClientID%>").value=""; 
        document.getElementById("<%=Txt_Numero_Cuenta_Predial.ClientID%>").value=""; 
        document.getElementById("<%=Txt_Superficie_Hasta.ClientID%>").value=""; 
        document.getElementById("<%=Txt_Superficie_Desde.ClientID%>").value=""; 
        document.getElementById("<%=Txt_Escritura.ClientID%>").value="";
        document.getElementById("<%=Txt_Bien_Mueble_ID.ClientID%>").value = ""; 
        return false;
    }  
    
    //Valida que los campos tengan el formato decimal correcto
    function Validar_Valores_Decimales(){  
        var regEx = /^[0-9]{1,50}(\.[0-9]{0,2})?$/;
        var Superficie_Desde = document.getElementById("<%=Txt_Superficie_Desde.ClientID%>").value;
        var Superficie_Hasta = document.getElementById("<%=Txt_Superficie_Hasta.ClientID%>").value;
        var Resultado = true;
        if(Superficie_Desde.length>0 && Resultado){
            Valido = Superficie_Desde.match(regEx);
            if(!Valido){
                alert('Formato Incorrecto para el Campo \"Superficie Inicial\".'); 
                Resultado = false; 
            }
        }
        if(Superficie_Hasta.length>0 && Resultado){
            Valido = Superficie_Hasta.match(regEx);
            if(!Valido){
                alert('Formato Incorrecto para el Campo \"Superficie Final\".'); 
                Resultado = false; 
            }
        }
        return Resultado;
      }  
</script>  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    
    <cc1:ToolkitScriptManager ID="ScptM_Control_Patrimonial" runat="server" />  
    
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                    </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Control de Bienes Inmuebles</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" Width="24px" CssClass="Img_Button" AlternateText="Nuevo" ToolTip="Nuevo" onclick="Btn_Nuevo_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" onclick="Btn_Salir_Click" />
                        </td>         
                        <td style="width:50%;">&nbsp;</td>                        
                    </tr>
                </table>   
                <asp:Panel ID="Pnl_Filtros_Listado" runat="server" DefaultButton="Btn_Ejecutar_Busqueda_General" Width="100%">
                    <table border="0" width="98%" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:Panel ID="Pnl_Filtros_Busqueda" runat="server" BorderColor="#3366FF" 
                                    BorderStyle="Outset" Height="250px" HorizontalAlign="Center" 
                                    style="white-space:normal;" Width="100%">
                                    <table border="0" class="estilo_fuente" width="100%">
                                        <tr>
                                            <td colspan="4" 
                                                style="background-color:#4F81BD; color:White; font-weight:bolder; text-align:center;">
                                                FILTROS PARA LA BUSQUEDA</td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Bien_Mueble_ID" runat="server" Text="No. Inventario"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Bien_Mueble_ID" runat="server" style="width:100%;"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Bien_Mueble_ID" runat="server" 
                                                    FilterType="Numbers" TargetControlID="Txt_Bien_Mueble_ID">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;<asp:Label ID="Lbl_Escritura" runat="server" Text="No. Escritura"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Escritura" runat="server" style="width:98%;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Calle" runat="server" Text="Calle"></asp:Label>
                                            </td>
                                            <td colspan="4">
                                                <asp:TextBox ID="Txt_Calle" runat="server" style="width:99.5%;"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Calle" runat="server" TargetControlID="Txt_Calle"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Colonia" runat="server" Text="Colonia"></asp:Label>
                                            </td>
                                            <td colspan="4">
                                                <asp:TextBox ID="Txt_Colonia" runat="server" style="width:99.5%;"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Colonia" runat="server" TargetControlID="Txt_Colonia"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Uso" runat="server" Text="Uso"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:DropDownList ID="Cmb_Uso" runat="server" style="width:100%;">
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;<asp:Label ID="Lbl_Destino" runat="server" Text="Destino"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:DropDownList ID="Cmb_Destino" runat="server" style="width:100%;">
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Numero_Cuenta_Predial" runat="server" Text="Cuenta Predial"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Numero_Cuenta_Predial" runat="server" MaxLength="25" 
                                                    style="width:100%;"></asp:TextBox>
                                            </td>
                                            <td style="width:15%; text-align:right;"><asp:Label ID="Lbl_Distrito" runat="server" Text="Distrito"></asp:Label></td>
                                            <td style="width:35%;"><asp:DropDownList ID="Cmb_Distrito" runat="server" style="width:100%;"></asp:DropDownList></td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%;">
                                                <asp:Label ID="Lbl_Superficie_Desde" runat="server" Text="Superficie ≥"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Superficie_Desde" runat="server" style="width:80%;"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Superficie_Desde" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers" InvalidChars="'" 
                                                    TargetControlID="Txt_Superficie_Desde" ValidChars=".">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:Label ID="Lbl_Construccion_Registrada_M2" runat="server" Text="[m2]"></asp:Label>
                                            </td>
                                            <td style="width:15%;">
                                                &nbsp;&nbsp;<asp:Label ID="Lbl_Superficie_Hasta" runat="server" Text="Superficie ≤"></asp:Label>
                                            </td>
                                            <td style="width:35%;">
                                                <asp:TextBox ID="Txt_Superficie_Hasta" runat="server" style="width:80%;"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Superficie_Hasta" runat="server" 
                                                    Enabled="True" FilterType="Custom, Numbers" InvalidChars="'" 
                                                    TargetControlID="Txt_Superficie_Hasta" ValidChars=".">
                                                </cc1:FilteredTextBoxExtender>
                                                <asp:Label ID="Lbl_Superficie_Hasta_M2" runat="server" Text="[m2]"></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align:right;">
                                                <asp:ImageButton ID="Btn_Ejecutar_Busqueda_General" runat="server" 
                                                    AlternateText="Ejecutar Busqueda" CssClass="Img_Button" 
                                                    ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" 
                                                    OnClick="Btn_Ejecutar_Busqueda_General_Click" 
                                                    OnClientClick="javascript:return Validar_Valores_Decimales();" 
                                                    ToolTip="Ejecutar Busqueda" Width="24px" />
                                                <asp:ImageButton ID="Btn_Limpiar_Campos" runat="server" 
                                                    AlternateText="Limpiar Resguardante" 
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" 
                                                    OnClientClick="javascript:return Limpiar_Ctlr_Campos();" 
                                                    ToolTip="Limpiar Campos" Width="24px" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <table border="0" width="98%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <asp:Panel ID="Listado_Busqueda_Bienes_Inmuebles" runat="server" ScrollBars="Vertical" style="white-space:normal;" Width="100%" BorderColor="#3366FF" Height="450px">
                               <asp:GridView ID="Grid_Listado_Busqueda_Bienes_Inmuebles" runat="server" 
                                 AutoGenerateColumns="False" CellPadding="4" ForeColor="#333333" 
                                 OnPageIndexChanging="Grid_Listado_Busqueda_Bienes_Inmuebles_PageIndexChanging"
                                 OnSelectedIndexChanged="Grid_Listado_Busqueda_Bienes_Inmuebles_SelectedIndexChanged"
                                 GridLines="None"
                                 PageSize="100" Width="100%" CssClass="GridView_1" AllowPaging="true">
                                 <AlternatingRowStyle CssClass="GridAltItem" />
                                 <Columns>
                                     <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                         ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                         <ItemStyle Width="30px" />
                                     </asp:ButtonField>
                                     <asp:BoundField DataField="BIEN_INMUEBLE_ID" HeaderText="No. Inventario" SortExpression="BIEN_INMUEBLE_ID"  >
                                         <ItemStyle Width="30px" Font-Size="X-Small" />
                                     </asp:BoundField>
                                     <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE">
                                        <ItemStyle Width="40px" Font-Size="X-Small"/>
                                     </asp:BoundField>
                                     <asp:BoundField DataField="NOMBRE_COMUN" HeaderText="Nombre Común" SortExpression="NOMBRE_COMUN">
                                        <ItemStyle Width="120px" Font-Size="X-Small"/>
                                     </asp:BoundField>
                                     <asp:BoundField DataField="NO_ESCRITURA" HeaderText="No. Escritura" SortExpression="NO_ESCRITURA"  >
                                         <ItemStyle Width="30px" Font-Size="X-Small" />
                                     </asp:BoundField>
                                     <asp:BoundField DataField="VIALIDAD_CALLE" HeaderText="Calle" SortExpression="CALLE">
                                        <ItemStyle Font-Size="X-Small"/>
                                     </asp:BoundField>
                                     <asp:BoundField DataField="COLONIA" HeaderText="Colonia" SortExpression="COLONIA" >
                                         <ItemStyle Font-Size="X-Small" />
                                     </asp:BoundField>
                                     <asp:BoundField DataField="USO_INMUEBLE" HeaderText="Uso" SortExpression="USO_INMUEBLE">
                                        <ItemStyle Width="40px" Font-Size="X-Small"/>
                                     </asp:BoundField>
                                 </Columns>
                                 <HeaderStyle CssClass="GridHeader" />
                                 <PagerStyle CssClass="GridHeader" />
                                 <RowStyle CssClass="GridItem" />
                                 <SelectedRowStyle CssClass="GridSelected" />
                             </asp:GridView>
                           </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
    
</asp:Content>