﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Com_Actualizacion_Vehiculos.aspx.cs"
    Inherits="paginas_Compras_Frm_Ope_Pat_Com_Actualizacion_Vehiculos" Title="Operación - Actualización de Vehículos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <%--liberias pera autocompletar--%>

    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>

    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet"
        type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            return false;
        }

        function get_KeyPress(textbox, evento) {
            //debugger;
            var keyCode;
            if (evento.which || evento.charCode) {
                keyCode = evento.which ? evento.which : evento.charCode;
                //return (keyCode != 13);
            }
            else if (window.event) {
                keyCode = event.keyCode;
                if (keyCode == 13) {
                    if (event.keyCode)
                        event.keyCode = 9;
                }
            }

            if (keyCode == 13) {
                //                if (textbox.Equals("Txt_Busqueda"))
                (document.getElementById('<%=Btn_Buscar.ClientID %>')).click();
                //                alert("se presiono Enter");
                window.focus();
                return false;
            }
            return true;
        }

        function Carga_Completa(sender, args) {
            var archivo = args.get_fileName(); //Se asigna el archivo a la variable archivo
            if (archivo != "") {
                // se obtiene la extension del archivo
                var arreglo = new Array;
                arreglo = archivo.split("\\");
                var tamano = arreglo.length;
                var imagen = arreglo[tamano - 1];
                var extension = imagen.substring(imagen.lastIndexOf(".") + 1);
                if (extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG") {
                    if (args.get_length() > 2621440) {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("El Archivo " + archivo + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                        Limpiar_Contenido();
                        return false;
                    }
                    return true;
                } else {
                    var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                    alert("Tipo de archivo inválido " + archivo + "\n\nFormatos Validos [.jpg, .jpeg]" + mensaje);
                    Limpiar_Contenido();
                    return false;
                }
            } else {
                Limpiar_Contenido();
                return false;
            }
            return true;
        }

        function Limpiar_Contenido() {
            var AsyncFileUpload = $get("<%=AFU_Archivo.ClientID%>"); //Se obtiene el componente para cargar el archivo
            var texto = AsyncFileUpload.getElementsByTagName("input"); //Se almacena el texto del componente para cargar el archivo
            for (var i = 0; i < texto.length; i++) {
                if (texto[i].type == "file") {
                    texto[i].style.backgroundColor = "red";
                }
            }
        }


        $(function() {
            $("[id$='Txt_Busqueda_Directa_No_Economico']").live({ 'keyup': function() {
                var Str_Economico = $("[id$='Txt_Busqueda_Directa_No_Economico']").val();
                var Longitud_Palabra = Str_Economico.length;
                if (!isNaN(Str_Economico[0]))
                    Str_Economico = 'U' + Str_Economico;
                else
                    Str_Economico = Str_Economico.toUpperCase();
                $("[id$='Txt_Busqueda_Directa_No_Economico']").val(Str_Economico);
            }, 'blur': function() {
                if ($("[id$='Txt_Busqueda_Directa_No_Economico']").val().length > 0) {
                    var Str_Economico = $("[id$='Txt_Busqueda_Directa_No_Economico']").val();
                    var Str_Economico_Copia = new Array(6);
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                    Longitud_Palabra = Str_Economico.length;
                    if (Longitud_Palabra < 6) {
                        for (var i = 1; i < Longitud_Palabra; i++) {
                            Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                        }
                        $("[id$='Txt_Busqueda_Directa_No_Economico']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
                    }
                }
            }
            });
        });

        $(function() {
            $("[id$='Txt_Numero_Economico']").live({ 'keyup': function() {
                var Str_Economico = $("[id$='Txt_Numero_Economico']").val();
                var Longitud_Palabra = Str_Economico.length;
                if (!isNaN(Str_Economico[0]))
                    Str_Economico = 'U' + Str_Economico;
                else
                    Str_Economico = Str_Economico.toUpperCase();
                $("[id$='Txt_Numero_Economico']").val(Str_Economico);
            }, 'blur': function() {
                if ($("[id$='Txt_Numero_Economico']").val().length > 0) {
                    var Str_Economico = $("[id$='Txt_Numero_Economico']").val();
                    var Str_Economico_Copia = new Array(6);
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                    Longitud_Palabra = Str_Economico.length;
                    if (Longitud_Palabra < 6) {
                        for (var i = 1; i < Longitud_Palabra; i++) {
                            Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                        }
                        $("[id$='Txt_Numero_Economico']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
                    }
                }
            }
            });
        });

        $(function() {
            $("[id$='Txt_Busqueda_Numero_Economico']").live({ 'keyup': function() {
                var Str_Economico = $("[id$='Txt_Busqueda_Numero_Economico']").val();
                var Longitud_Palabra = Str_Economico.length;
                if (!isNaN(Str_Economico[0]))
                    Str_Economico = 'U' + Str_Economico;
                else
                    Str_Economico = Str_Economico.toUpperCase();
                $("[id$='Txt_Busqueda_Numero_Economico']").val(Str_Economico);
            }, 'blur': function() {
                if ($("[id$='Txt_Busqueda_Numero_Economico']").val().length > 0) {
                    var Str_Economico = $("[id$='Txt_Busqueda_Numero_Economico']").val();
                    var Str_Economico_Copia = new Array(6);
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                    Longitud_Palabra = Str_Economico.length;
                    if (Longitud_Palabra < 6) {
                        for (var i = 1; i < Longitud_Palabra; i++) {
                            Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                        }
                        $("[id$='Txt_Busqueda_Numero_Economico']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
                    }
                }
            }
            });
        });
        
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Vehiculos" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="4">
                            Actualización de Vehículos
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" colspan="2">
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Modificar" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Generar_Reporte" runat="server" ToolTip="Ver Resguardo"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Reporte" OnClick="Btn_Generar_Reporte_Click" />
                            <asp:ImageButton ID="Btn_Exportar_Acta" runat="server" ToolTip="Exportar Acta" ImageUrl="~/paginas/imagenes/mime/docx.ico"
                                Width="24px" CssClass="Img_Button" AlternateText="Exportar Acta" OnClick="Btn_Exportar_Acta_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td align="right" colspan="2">
                            <div id="Div_Busqueda" runat="server">
                                <asp:LinkButton ID="Btn_Busqueda_Avanzada" runat="server" ForeColor="White" OnClick="Btn_Avanzada_Click"
                                    ToolTip="Avanzada">Busqueda</asp:LinkButton>
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" Width="150px" Style="text-align: center;"
                                    onkeypress="return get_KeyPress(this,event);"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                    WatermarkText="<- No. de Inventario ->" TargetControlID="Txt_Busqueda" />
                                &nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Busqueda_Directa_No_Economico" runat="server" MaxLength="100"
                                    Style="text-align: center;" Width="150px" onkeypress="return get_KeyPress(this,event);"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Directa_No_Economico" runat="server"
                                    WatermarkCssClass="watermarked" WatermarkText="<- No. de Económico ->" TargetControlID="Txt_Busqueda_Directa_No_Economico" />
                                <asp:ImageButton ID="Btn_Buscar" runat="server" OnClick="Btn_Buscar_Click " ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    AlternateText="Consultar" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Directa_No_Economico" runat="server"
                                    TargetControlID="Txt_Busqueda_Directa_No_Economico" InvalidChars="<,>,&,',!,"
                                    FilterType="Custom, LowercaseLetters, UppercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_"
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right" runat="server" id="TR_Codigo_Barras">
                        <td align="left" colspan="2">
                            <asp:Label ID="Lbl_Tamano_Etiqueta" runat="server" Text="Tamaño de la Etiqueta"></asp:Label>&nbsp;&nbsp;
                            <asp:DropDownList ID="Cmb_Tamano_Etiqueta" runat="server" Width="150px">
                                <asp:ListItem Value="NORMAL">NORMAL</asp:ListItem>
                                <asp:ListItem Value="PEQUENO">PEQUEÑA</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:ImageButton ID="Btn_Imprimir_Codigo_Barras" runat="server" ToolTip="Imprimir Codigo de Barras"
                                ImageUrl="~/paginas/imagenes/paginas/barcode.png" Width="24px" Height="24px"
                                CssClass="Img_Button" AlternateText="Codigo_Barras" OnClick="Btn_Imprimir_Codigo_Barras_Click" />
                        </td>
                        <td align="right" colspan="2">
                            <div id="Div_Busqueda_No_Inventario_CONAC" runat="server">
                                <asp:TextBox ID="Txt_Busqueda_No_Inventario_CONAC" runat="server" MaxLength="12"
                                    Width="320px" Style="text-align: center;" onkeypress="return get_KeyPress(this,event);"></asp:TextBox>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_No_Inventario_CONAC" runat="server"
                                    WatermarkCssClass="watermarked" WatermarkText="<- Código de Barras [CONAC] ->"
                                    TargetControlID="Txt_Busqueda_No_Inventario_CONAC" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Inventario_CONAC" runat="server"
                                    TargetControlID="Txt_Busqueda_No_Inventario_CONAC" InvalidChars="<,>,&,',!,"
                                    FilterType="Numbers" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            </div>
                        </td>
                    </tr>
                </table>
                <br />
                <cc1:TabContainer ID="Tab_Contenedor_Pestagnas" runat="server" Width="98%" ActiveTabIndex="0"
                    CssClass="Tab" Style="visibility: visible;">
                    <cc1:TabPanel runat="server" HeaderText="Tab_Bienes_Detalles" ID="Tab_Bienes_Detalles"
                        Width="100%">
                        <HeaderTemplate>
                            Generales</HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td colspan="4">
                                        <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                                        <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Numero_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Numero_Inventario" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        &nbsp;
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre" runat="server" Text="Nombre del Producto"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Nombre" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase de Activo"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        <asp:Label ID="Lbl_Tipo_Activo" runat="server" Text="Tipo de Activo"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Tipo_Activo" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        <asp:HiddenField ID="Hdf_Cuenta_Contable_ID" runat="server" />
                                        <asp:Label ID="Lbl_Cuenta_Contable" runat="server" Text="Cuenta Activo"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" Width="99.5%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        <asp:HiddenField ID="Hdf_Cuenta_Gasto_ID" runat="server" />
                                        <asp:Label ID="Lbl_Cuenta_Gasto" runat="server" Text="Cuenta Gasto"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Cuenta_Gasto" runat="server" Width="99.5%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td width="15%">
                                        <asp:HiddenField ID="Hdf_Partida_Reparacion_ID" runat="server" />
                                        <asp:Label ID="Lbl_Partida_Reparacion" runat="server" Text="Partida Reparación"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:Panel runat="server" ID="Pnl_Partida_Reparacion" DefaultButton="Btn_Busqueda_Partida_Reparacion">
                                            <asp:TextBox ID="Txt_Clave_Partida_Reparacion" runat="server" Width="10%" MaxLength="4"></asp:TextBox>&nbsp;
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Partida_Reparacion" 
                                                runat="server" FilterType="Numbers" 
                                                TargetControlID="Txt_Clave_Partida_Reparacion" Enabled="True"></cc1:FilteredTextBoxExtender>
                                             <asp:ImageButton ID="Btn_Busqueda_Partida_Reparacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Busqueda_Partida_Reparacion_Click" 
                                                AlternateText="Buscar Partida Reparación" ToolTip="Buscar Partida Reparación" Width="20px" />
                                            <asp:TextBox ID="Txt_Nombre_Partida_Reparacion" runat="server" Width="83.5%" 
                                                Enabled="False"></asp:TextBox>
                                        </asp:Panel>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Label1" runat="server" Text="Gerencia "></asp:Label>
                                    </td>
                                    <td colspan="3" align="left">
                                        <asp:DropDownList ID="Cmb_Gerencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Gerencias_SelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Dependencia" runat="server" Text="Unidad Responsable"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Dependencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Dependencias_SelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Zonas" runat="server" Text="Ubicación Fisica"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Zonas" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Marca" runat="server" Text="Marca"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Marca" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Tipos_Vehiculos" runat="server" Text="Tipo"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Tipos_Vehiculos" runat="server" Width="100%" AutoPostBack="True"
                                            OnSelectedIndexChanged="Cmb_Tipos_Vehiculos_SelectedIndexChanged">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Modelo" runat="server" Text="Modelo"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Modelo" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Modelo" runat="server" TargetControlID="Txt_Modelo"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Color" runat="server" Text="Color"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Colores" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Tipo_Combustible" runat="server" Text=" Combustible"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Tipo_Combustible" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Placas" runat="server" Text="Placas"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Placas" runat="server" Width="97%" MaxLength="15"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Inventario" runat="server" TargetControlID="Txt_Placas"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Numero_Economico" runat="server" Text="No. Económico"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Numero_Economico" runat="server" Width="98%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Economico" runat="server" TargetControlID="Txt_Numero_Economico"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%;">
                                        <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="93%" Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Lanzar_Mpe_Proveedores" runat="server" OnClick="Btn_Lanzar_Mpe_Proveedores_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Busqueda y Selección de Proveedor"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Procedencia_Bien" runat="server" Text="Procedencia"></asp:Label>
                                    </td>
                                    <td style="width: 32%" align="left">
                                        <asp:DropDownList ID="Cmb_Procedencia" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_No_Factura" runat="server" Text="No Factura"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_No_Factura" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Factura" runat="server" TargetControlID="Txt_No_Factura"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Serie_Carroceria" runat="server" Text="Serie"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Serie_Carroceria" runat="server" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Serie" runat="server" TargetControlID="Txt_Serie_Carroceria"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="text-align: left; vertical-align: top; width: 15%">
                                        <asp:Label ID="Lbl_Fecha_Inventario" runat="server" Text="Fecha Resguardo"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Fecha_Inventario" runat="server" Width="85%" MaxLength="20"
                                            Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Fecha_Inventario" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inventario" runat="server" TargetControlID="Txt_Fecha_Inventario"
                                            PopupButtonID="Btn_Fecha_Inventario" Format="dd/MMM/yyyy" Enabled="True">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Costo_Inicial" runat="server" Text="Costo Inicial [$]"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Costo_Inicial" runat="server" Width="98%" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Costo_Actual" runat="server" Text="Costo Actual [$]"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Costo_Actual" runat="server" Width="98%" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Capacidad_Carga" runat="server" Text="Capacidad"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Capacidad_Carga" runat="server" Width="98%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Capacidad_Carga" runat="server" TargetControlID="Txt_Capacidad_Carga"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Fecha_Adquisicion" runat="server" Text="Fecha Facturación"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Fecha_Adquisicion" runat="server" Width="85%" MaxLength="20"
                                            Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Fecha_Adquisicion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Adquisicion" runat="server" TargetControlID="Txt_Fecha_Adquisicion"
                                            PopupButtonID="Btn_Fecha_Adquisicion" Format="dd/MMM/yyyy" Enabled="True">
                                        </cc1:CalendarExtender>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Anio_Fabricacion" runat="server" Text="Año Fabricación"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Anio_Fabricacion" runat="server" Width="98%" MaxLength="4"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anio_Fabricacion" runat="server" TargetControlID="Txt_Anio_Fabricacion"
                                            FilterType="Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Numero_Cilindros" runat="server" Text="No. Cilindros"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Numero_Cilindros" runat="server" Width="98%" MaxLength="2"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Cilindros" runat="server" TargetControlID="Txt_Numero_Cilindros"
                                            FilterType="Numbers" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Kilometraje" runat="server" Text="Kilometraje"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="98%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" TargetControlID="Txt_Kilometraje"
                                            FilterType="Numbers" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                                    </td>
                                    <td style="width: 35%;">
                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                            <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                            <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Estado" runat="server" Text="Odometro"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Odometro" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            <asp:ListItem Value="FUNCIONA">FUNCIONA</asp:ListItem>
                                            <asp:ListItem Value="NO_FUNCIONA">NO FUNCIONA</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Tipo_Baja" runat="server" Text="Tipo Baja"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Tipo_Baja" runat="server" Width="100%">
                                            <asp:ListItem>&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; width: 15%;">
                                        <asp:Label ID="Lbl_Motivo_Baja" runat="server" Text="Motivo Baja"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Motivo_Baja" runat="server" Width="99%" Rows="3" Font-Size="Smaller"
                                            TextMode="MultiLine"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Motivo_Baja" runat="server" TargetControlID="Txt_Motivo_Baja"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-_+$%&!#$%&¡?¿[]{}" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; width: 15%;">
                                        <asp:Label ID="Lbl_Observaciones" runat="server" Text="Observaciones"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Observaciones" runat="server" Width="99%" Rows="3" Font-Size="Smaller"
                                            TextMode="MultiLine"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-_+$%&!#$%&¡?¿[]{}" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                            </table>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td>
                                        <div runat="server" id="Div1">
                                            <table width="100%" class="estilo_fuente">
                                                <tr>
                                                    <td class="label_titulo" colspan="4">
                                                        Firmas
                                                    </td>
                                                </tr>
                                                <tr align="right" class="barra_delgada">
                                                    <td colspan="4" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 18%;">
                                                        <asp:HiddenField ID="Hdf_Resguardo_Completo_Autorizo" runat="server" />
                                                        <asp:Label ID="Lbl_Resguardo_Completo_Autorizo" runat="server" Text="Autorizó"></asp:Label>
                                                    </td>
                                                    <td style="width: 82%;">
                                                        <asp:TextBox ID="Txt_Resguardo_Completo_Autorizo" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                                        <asp:ImageButton ID="Btn_Resguardo_Completo_Autorizo" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                            Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Autorizo_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 18%;">
                                                        <asp:HiddenField ID="Hdf_Resguardo_Completo_Operador" runat="server" />
                                                        <asp:Label ID="Lbl_Resguardo_Completo_Operador" runat="server" Text="Entregó"></asp:Label>
                                                    </td>
                                                    <td style="width: 82%;">
                                                        <asp:TextBox ID="Txt_Resguardo_Completo_Operador" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                                        <asp:ImageButton ID="Btn_Resguardo_Completo_Operador" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                            Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Operador_Click" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 18%;">
                                                        <asp:HiddenField ID="Hdf_Resguardo_Completo_Funcionario_Recibe" runat="server" />
                                                        <asp:Label ID="Lbl_Resguardo_Completo_Funcionario_Recibe" runat="server" Text="Revisó"></asp:Label>
                                                    </td>
                                                    <td style="width: 82%;">
                                                        <asp:TextBox ID="Txt_Resguardo_Completo_Funcionario_Recibe" runat="server" Width="95%"
                                                            Enabled="False"></asp:TextBox>
                                                        <asp:ImageButton ID="Btn_Resguardo_Completo_Funcionario_Recibe" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                            Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Funcionario_Recibe_Click" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                            <asp:HiddenField ID="Hdf_Tipo_Busqueda" runat="server" />
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td>
                                        <div runat="server" id="Div3">
                                            <table width="100%" class="estilo_fuente">
                                                <tr>
                                                    <td class="label_titulo" colspan="4">
                                                        Resguardatarios
                                                    </td>
                                                </tr>
                                                <tr align="right" class="barra_delgada">
                                                    <td colspan="4" align="center">
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; text-align: left;">
                                                        <asp:Label ID="Lbl_Resguardantes" runat="server" Text="Empleados"></asp:Label>
                                                    </td>
                                                    <td style="width: 85%; text-align: left;">
                                                        <asp:DropDownList ID="Cmb_Empleados" runat="server" Width="100%">
                                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; text-align: left; vertical-align: top;">
                                                        <asp:Label ID="Lbl_Comentarios" runat="server" Text="Comentarios"></asp:Label>
                                                    </td>
                                                    <td style="width: 85%; text-align: left;">
                                                        <asp:TextBox ID="Txt_Cometarios" runat="server" TextMode="MultiLine" Rows="3" Width="99%"></asp:TextBox>
                                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cometarios" runat="server" TargetControlID="Txt_Cometarios"
                                                            WatermarkText="Límite de Caracteres 150" WatermarkCssClass="watermarked" Enabled="True" />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="width: 15%; text-align: left; vertical-align: top;">
                                                        &nbsp;&nbsp;&nbsp;
                                                        <asp:ImageButton ID="Btn_Busqueda_Avanzada_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                            Width="24px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Busqueda_Avanzada_Resguardante_Click" />
                                                    </td>
                                                    <td style="width: 85%; text-align: right;">
                                                        <asp:ImageButton ID="Btn_Agregar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                                            ToolTip="Agregar" AlternateText="Agregar" OnClick="Btn_Agregar_Resguardante_Click" />
                                                        <asp:ImageButton ID="Btn_Quitar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/quitar.png"
                                                            ToolTip="Quitar" AlternateText="Quitar" OnClick="Btn_Quitar_Resguardante_Click" />
                                                        &nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td colspan="2">
                                                        <hr />
                                                    </td>
                                                </tr>
                                            </table>
                                            <br />
                                            <asp:GridView ID="Grid_Resguardantes" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="#333333" GridLines="None" Width="98%" OnRowDataBound="Grid_Resguardantes_RowDataBound">
                                                <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                        <ItemStyle Width="30px" />
                                                    </asp:ButtonField>
                                                    <asp:BoundField DataField="BIEN_RESGUARDO_ID" HeaderText="BIEN_RESGUARDO_ID" SortExpression="BIEN_RESGUARDO_ID">
                                                        <ItemStyle Width="110px" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="EMPLEADO_ID" HeaderText="Empleado ID" SortExpression="EMPLEADO_ID">
                                                        <ItemStyle Width="90px" HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                        <ItemStyle Width="90px" HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre  del Empleado" SortExpression="NOMBRE_EMPLEADO">
                                                        <ItemStyle HorizontalAlign="Center" Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:ImageButton ID="Btn_Ver_Informacion_Resguardo" runat="server" Width="16px" OnClick="Btn_Ver_Informacion_Resguardo_Click"
                                                                ImageUrl="~/paginas/imagenes/gridview/grid_info.png" ToolTip="Comentarios del Resguardo" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="50px" />
                                                    </asp:TemplateField>
                                                </Columns>
                                                <PagerStyle CssClass="GridHeader" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Tab_Detalles_Vehiculo" ID="Tab_Detalles_Vehiculo"
                        Width="100%">
                        <HeaderTemplate>
                            Detalles de Vehículo</HeaderTemplate>
                        <ContentTemplate>
                            <div id="Div_Detalles_Vehiculos" runat="server" style="width: 99%; height: 500px;
                                overflow: auto;">
                                <hr style="width: 99%; text-align: center;" />
                                <asp:GridView ID="Grid_Detalles_Vehiculo" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" ForeColor="#333333" GridLines="None" Width="99%" PageSize="5">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="DETALLE_ID" HeaderText="DETALLE_ID" SortExpression="DETALLE_ID">
                                            <ItemStyle Width="110px" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE" HeaderText="Parte de Vehículo" SortExpression="NOMBRE">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:TemplateField HeaderText="Estado">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="Cmb_Estado_Detalle" runat="server" Width="99%">
                                                    <asp:ListItem Value="N">NO TIENE (N)</asp:ListItem>
                                                    <asp:ListItem Value="B">BUENO (B)</asp:ListItem>
                                                    <asp:ListItem Value="R">REGULAR (R)</asp:ListItem>
                                                    <asp:ListItem Value="M">MALO (M)</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                            <ItemStyle Width="110px" />
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Tab_Datos_Seguro" ID="Tab_Datos_Seguro"
                        Width="100%">
                        <HeaderTemplate>
                            Seguro</HeaderTemplate>
                        <ContentTemplate>
                            <center>
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td colspan="4">
                                            <asp:HiddenField ID="Hdf_Vehiculo_Aseduradora_ID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Aseguradoras" runat="server" Text="Aseguradora"></asp:Label>
                                        </td>
                                        <td style="width: 85%; text-align: left;">
                                            <asp:DropDownList ID="Cmb_Aseguradoras" runat="server" Width="100%" Enabled="false">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Numero_Poliza_Seguro" runat="server" Text="Número Poliza"></asp:Label>
                                        </td>
                                        <td style="width: 85%; text-align: left;">
                                            <asp:TextBox ID="Txt_Numero_Poliza_Seguro" Enabled="false" runat="server" Width="98%"
                                                MaxLength="30"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Poliza_Seguro" runat="server" TargetControlID="Txt_Numero_Poliza_Seguro"
                                                InvalidChars="<,>,&,',!," FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="Ññ.,:;-/()áéíóúÁÉÍÓÚ "
                                                Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Numero_Inciso" runat="server" Text="No. Inciso"></asp:Label>
                                        </td>
                                        <td style="width: 85%; text-align: left;">
                                            <asp:TextBox ID="Txt_Numero_Inciso" Enabled="false" runat="server" Width="98%" MaxLength="150"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inciso" runat="server" TargetControlID="Txt_Numero_Inciso"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Cobertura_Seguro" runat="server" Text="Cobertura"></asp:Label>
                                        </td>
                                        <td style="width: 85%; text-align: left;">
                                            <asp:TextBox ID="Txt_Cobertura_Seguro" Enabled="false" runat="server" Width="98%"
                                                Rows="2" TextMode="MultiLine"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cobertura_Seguro" runat="server" TargetControlID="Txt_Cobertura_Seguro"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cobertura_Seguro" runat="server" WatermarkText="Longitud Maxima de 150 Caracteres"
                                                TargetControlID="Txt_Cobertura_Seguro" WatermarkCssClass="watermarked">
                                            </cc1:TextBoxWatermarkExtender>
                                        </td>
                                    </tr>
                                </table>
                            </center>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Tab_Historial_Resguardos" ID="Tab_Historial_Resguardos"
                        Width="100%">
                        <HeaderTemplate>
                            Historial</HeaderTemplate>
                        <ContentTemplate>
                            <center>
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td class="label_titulo" colspan="4">
                                            Modificaciones
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top; width: 15%">
                                            <asp:Label ID="Lbl_Usuario_Creo" runat="server" Text="Creación"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Usuario_creo" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top; width: 15%">
                                            <asp:Label ID="Lbl_Usuario_Modifico" runat="server" Text="Ultima Modificación"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Usuario_Modifico" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr style="background-color: Silver;">
                                        <td class="label_titulo" style="color: Black; text-align: right; vertical-align: middle;"
                                            colspan="4">
                                            Ver Reporte de Cambios&nbsp;&nbsp;
                                            <asp:ImageButton ID="Btn_Generar_Reporte_Cambios" runat="server" ToolTip="Ver Reporte de Cambios"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Height="16px" Width="16px"
                                                CssClass="Img_Button" AlternateText="Ver Reporte de Cambios" OnClick="Btn_Generar_Reporte_Cambios_Click" />&nbsp;&nbsp;
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td class="label_titulo" colspan="4">
                                            Historial de Resguardatarios
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Historial_Empleado_Resguardo" Width="98%" runat="server" Text="Empleado"></asp:Label>
                                        </td>
                                        <td colspan="3" style="width: 35%; text-align: left;">
                                            <asp:TextBox ID="Txt_Historial_Empleado_Resguardo" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Historial_Fecha_Inicial_Resguardo" runat="server" Text="Fecha Inicial"></asp:Label>
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <asp:TextBox ID="Txt_Historial_Fecha_Inicial_Resguardo" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Historial_Fecha_Final_Resguardo" runat="server" Text="Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width: 35%; text-align: left;">
                                            <asp:TextBox ID="Txt_Historial_Fecha_Final_Resguardo" runat="server" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Historial_Comentarios_Resguardo" Width="98%" runat="server" Text="Comentarios"></asp:Label>
                                        </td>
                                        <td colspan="3" style="width: 35%; text-align: left;">
                                            <asp:TextBox ID="Txt_Historial_Comentarios_Resguardo" runat="server" TextMode="MultiLine"
                                                Rows="3" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:GridView ID="Grid_Historial_Resguardantes" runat="server" AutoGenerateColumns="False"
                                    CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="98%"
                                    OnPageIndexChanging="Grid_Historial_Resguardantes_PageIndexChanging" OnSelectedIndexChanged="Grid_Historial_Resguardantes_SelectedIndexChanged"
                                    PageSize="5">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="20px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="BIEN_RESGUARDO_ID" HeaderText="BIEN_RESGUARDO_ID" SortExpression="BIEN_RESGUARDO_ID" />
                                        <asp:BoundField DataField="EMPLEADO_ID" HeaderText="Empleado ID" SortExpression="EMPLEADO_ID" />
                                        <asp:BoundField DataField="NO_EMPLEADO" HeaderText="Empleado ID" SortExpression="NO_EMPLEADO">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre" SortExpression="NOMBRE_EMPLEADO">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </center>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="Tab_Archivos" runat="server" HeaderText="Tab_Archivos">
                        <HeaderTemplate>
                            Archivos</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="Upd_Subir" runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 13%;">
                                                <asp:Label ID="Lbl_Archivo" runat="server" Text="Archivo" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 87%;">
                                                <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" Width="690px" CompleteBackColor="LightBlue"
                                                    ThrobberID="Lbl_Progress_File" UploadingBackColor="LightGray" FailedValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="Lbl_Progress_File" runat="server" Text="">
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div class="processMessage" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                        </div>
                                    </asp:Label>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="Btn_Agregar_Archivo" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <br />
                            <asp:UpdatePanel ID="UpdatePanel2" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:ImageButton ID="Btn_Agregar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                                    Width="18px" Height="18px" CssClass="Img_Button" AlternateText="Agregar" ToolTip="Agregar Archivo"
                                                    OnClick="Btn_Agregar_Archivo_Click" />
                                                <asp:ImageButton ID="Btn_Limpiar_FileUpload" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    Width="16px" Height="16px" CssClass="Img_Button" AlternateText="Limpiar Archivo"
                                                    ToolTip="Limpiar Archivo" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:UpdateProgress ID="Upd_Grid_Archivos" runat="server" AssociatedUpdatePanelID="Upd_Subir"
                                        DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                            </div>
                                            <div class="processMessage" id="div_progress">
                                                <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:GridView ID="Grid_Archivos" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                        ForeColor="#333333" GridLines="None" AllowPaging="True" Width="98%" PageSize="100"
                                        OnPageIndexChanging="Grid_Archivos_PageIndexChanging" OnRowDataBound="Grid_Archivos_RowDataBound">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="ARCHIVO_BIEN_ID" HeaderText="ARCHIVO_BIEN_ID" SortExpression="ARCHIVO_BIEN_ID" />
                                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ARCHIVO" HeaderText="Archivo" SortExpression="ARCHIVO"
                                                NullDisplayText="-">
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="90%" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Ver">
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Ver_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png"
                                                        Width="24px" CssClass="Img_Button" AlternateText="Ver Archivo" OnClick="Btn_Ver_Archivo_Click" />
                                                </ItemTemplate>
                                                <ItemStyle Width="50px" />
                                            </asp:TemplateField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel ID="Tab_Listado_Partes" runat="server" HeaderText="Tab_Listado_Partes">
                        <HeaderTemplate>
                            Listado de Partes</HeaderTemplate>
                        <ContentTemplate>
                            <div id="Div_Partes_Vehiculos_Campos" runat="server">
                                <table width="98%">
                                    <tr>
                                        <td colspan="4">
                                            <asp:HiddenField ID="Hdf_Parte_ID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Numero_Inventario_Parte" runat="server" Text="No. Inventario"
                                                CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:TextBox ID="Txt_Numero_Inventario_Parte" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario_Parte" runat="server"
                                                TargetControlID="Txt_Numero_Inventario_Parte" InvalidChars="<,>,&,',!," FilterType="Numbers"
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Cantidad_Parte" runat="server" Text="Cantidad" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:TextBox ID="Txt_Cantidad_Parte" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender4" runat="server" TargetControlID="Txt_Cantidad_Parte"
                                                FilterType="Numbers" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Material_Parte" runat="server" Text="Material" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:DropDownList runat="server" ID="Cmb_Material_Parte" Width="99%" Enabled="false">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Color_Parte" runat="server" Text="Color" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:DropDownList runat="server" ID="Cmb_Color_Parte" Width="98%" Enabled="false">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Costo_Parte" runat="server" Text="Costo" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:TextBox ID="Txt_Costo_Parte" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                            <cc1:MaskedEditExtender ID="MEE_Txt_Costo_Parte" runat="server" TargetControlID="Txt_Costo_Parte"
                                                Mask="9,999,999.99" MaskType="Number" InputDirection="RightToLeft" AcceptNegative="Left"
                                                ErrorTooltipEnabled="True" CultureAMPMPlaceholder="" CultureCurrencySymbolPlaceholder=""
                                                CultureDateFormat="" CultureDatePlaceholder="" CultureDecimalPlaceholder="" CultureThousandsPlaceholder=""
                                                CultureTimePlaceholder="" Enabled="True" />
                                        </td>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Fecha_Adquisicion_Parte" runat="server" Text="Adquisición" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:TextBox ID="Txt_Fecha_Adquisicion_Parte" runat="server" Width="98%" Enabled="False"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Estado_Parte" runat="server" Text="Estado" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:DropDownList ID="Cmb_Estado_Parte" runat="server" Width="98%" Enabled="false">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                                <asp:ListItem Value="BUENO">BUENO</asp:ListItem>
                                                <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>
                                                <asp:ListItem Value="MALO">MALO</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 15%; text-align: left;">
                                            <asp:Label ID="Lbl_Estatus_Parte" runat="server" Text="Estatus" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 35%">
                                            <asp:DropDownList ID="Cmb_Estatus_Parte" runat="server" Width="98%" Enabled="False">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                                <asp:ListItem Value="VIGENTE" Selected="True">VIGENTE</asp:ListItem>
                                                <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                                <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Comentarios_Parte" runat="server" Text="Comentarios" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Comentarios_Parte" runat="server" Width="99%" Rows="2" TextMode="MultiLine"
                                                Enabled="false"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Parte" runat="server" TargetControlID="Txt_Comentarios_Parte"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios_Parte" runat="server" TargetControlID="Txt_Comentarios_Parte"
                                                WatermarkText="Límite de Caracteres 500" WatermarkCssClass="watermarked" Enabled="True" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <asp:GridView ID="Grid_Partes" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                ForeColor="#333333" GridLines="None" AllowPaging="True" Width="98%" OnPageIndexChanging="Grid_Partes_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Partes_SelectedIndexChanged" PageSize="5">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID" SortExpression="BIEN_MUEBLE_ID">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_INVENTARIO_ANTERIOR" HeaderText="No. Inventario" SortExpression="NO_INVENTARIO_ANTERIOR">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_INVENTARIO_SIAS" HeaderText="No. Inventario [SIAS]"
                                        SortExpression="NO_INVENTARIO_SIAS">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="Nombre">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA" NullDisplayText="Marca">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO" NullDisplayText="Modelo">
                                        <ItemStyle Width="120px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTADO" HeaderText="Estado" SortExpression="ESTADO" NullDisplayText="Modelo">
                                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS"
                                        NullDisplayText="Modelo">
                                        <ItemStyle Width="50px" HorizontalAlign="Center" Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </div>
            <br />
            <br />
            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                        Auto_Completado_Cuentas_Gasto();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Contable']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Contable']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Contable_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Contable']").val("");
                        }
                        });
                    }
                    function Auto_Completado_Cuentas_Gasto() {
                        $("[id$='Txt_Cuenta_Gasto']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Gasto']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Gasto_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Gasto']").val("");
                        }
                        });
                    }
                } 
            </script>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Exportar_Acta" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Vehiculo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Vehiculo" runat="server" TargetControlID="Btn_Comodin"
                PopupControlID="Pnl_Busqueda_Vehiculo" CancelControlID="Btn_Cerrar" DropShadow="True"
                BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" HorizontalAlign="Center" Width="800px"
        Style="display: none; border-style: outset; border-color: Silver; background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Interno" runat="server" Style="background-color: Silver; color: Black;
            font-size: 12; font-weight: bold; border-style: outset;">
            <center>
                <asp:UpdatePanel ID="UpPnl_Busqueda" runat="server">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="UpdateProgress1" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                </div>
                                <div id="div_progress" class="processMessage">
                                    <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda" runat="server" ActiveTabIndex="0"
                            Width="98%">
                            <cc1:TabPanel ID="Tab_Panel_Datos_Generales_Busqueda" runat="server" HeaderText="Tab_Panel_Datos_Generales"
                                Height="400px" Width="100%">
                                <HeaderTemplate>
                                    Generales
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="Pnl_Btn_Buscar_Datos" runat="server" DefaultButton="Btn_Buscar_Datos"
                                        Style="border-style: outset; width: 98%; height: 200px;">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">
                                                    <asp:Label ID="Lbl_Titulo_Busqueda" runat="server" Text="Búsqueda"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Numero_Inventario" runat="server" CssClass="estilo_fuente"
                                                        Text="Número Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Numero_Inventario" runat="server" Width="97%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Numero_Inventario" runat="server"
                                                        Enabled="True" FilterType="Numbers" TargetControlID="Txt_Busqueda_Numero_Inventario">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Numero_Economico" runat="server" CssClass="estilo_fuente"
                                                        Text="Número Económico"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Numero_Economico" runat="server"
                                                        Enabled="True" FilterType="Custom, LowercaseLetters, UppercaseLetters, Numbers"
                                                        InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Numero_Economico"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Modelo" runat="server" CssClass="estilo_fuente" Text="Modelo"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Modelo_Busqueda" runat="server" Width="97%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Modelo_Busqueda" runat="server" Enabled="True"
                                                        FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                                        TargetControlID="Txt_Modelo_Busqueda" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Marca" runat="server" CssClass="estilo_fuente" Text="Marca"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Marca" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Tipo_Vehiculo" runat="server" CssClass="estilo_fuente"
                                                        Text="Tipo Vehículo"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Tipo_Vehiculo" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODOS &gt;" Value="SELECCIONE"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Tipo_Combustible" runat="server" CssClass="estilo_fuente"
                                                        Text="Combustible"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Tipo_Combustible" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Anio_Fabricacion" runat="server" CssClass="estilo_fuente"
                                                        Text="Año Fabricación"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Anio_Fabricacion" runat="server" MaxLength="4" Width="97%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Anio_Fabricacion" runat="server"
                                                        Enabled="True" FilterType="Numbers" TargetControlID="Txt_Busqueda_Anio_Fabricacion">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Color" runat="server" CssClass="estilo_fuente" Text="Color"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Color" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Zonas" runat="server" CssClass="estilo_fuente" Text="Zonas"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Zonas" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Estatus" runat="server" CssClass="estilo_fuente" Text="Estatus"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                        <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                                        <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                                        <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Dependencias" runat="server" CssClass="estilo_fuente"
                                                        Text="U. Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3" style="width: 80%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Dependencias" runat="server" Width="85%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:ImageButton ID="Btn_Buscar_Datos" runat="server" CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                        OnClick="Btn_Buscar_Datos_Click" ToolTip="Buscar Contrarecibos" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Click"
                                                        ToolTip="Limpiar Filtros" Width="20px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel ID="Tab_Panel_Resguardantes_Busqueda" runat="server" HeaderText="Tab_Panel_Reguardantes"
                                Width="100%">
                                <HeaderTemplate>
                                    Resguardatarios
                                </HeaderTemplate>
                                <ContentTemplate>
                                    <asp:Panel ID="Pnl_Btn_Buscar_Resguardante" runat="server" DefaultButton="Btn_Buscar_Resguardante"
                                        Style="border-style: outset; width: 98%; height: 200px;">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Listado" runat="server" Text="Búsqueda"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_RFC_Resguardante" runat="server" CssClass="estilo_fuente"
                                                        Text="RFC "></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_RFC_Resguardante" runat="server" Width="95%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_RFC_Resguardante" runat="server"
                                                        Enabled="True" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                        InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_RFC_Resguardante"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_No_Empleado_Resguardante" runat="server" Text="No Empleado"
                                                        CssClass="estilo_fuente"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_No_Empleado_Resguardante" runat="server" Width="95%"
                                                        MaxLength="6"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Empleado_Resguardante" runat="server"
                                                        TargetControlID="Txt_Busqueda_RFC_Resguardante" InvalidChars="<,>,&,',!," FilterType="Numbers"
                                                        Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Resguardantes_Dependencias" runat="server" CssClass="estilo_fuente"
                                                        Text="U. Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3" style="width: 80%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Resguardantes_Dependencias" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="Cmb_Busqueda_Resguardantes_Dependencias_SelectedIndexChanged"
                                                        Width="100%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Nombre_Resguardante" runat="server" CssClass="estilo_fuente"
                                                        Text="Reguardatario"></asp:Label>
                                                </td>
                                                <td colspan="3" style="width: 80%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Nombre_Resguardante" runat="server" Width="100%">
                                                        <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: right">
                                                    <asp:ImageButton ID="Btn_Buscar_Resguardante" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Resguardante_Click"
                                                        ToolTip="Buscar Listados" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Click"
                                                        ToolTip="Limpiar Filtros" Width="20px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </asp:Panel>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                        <div style="width: 97%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                            <center>
                                <caption>
                                    <asp:GridView ID="Grid_Listado_Vehiculos" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                        CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Listado_Vehiculos_PageIndexChanging"
                                        OnSelectedIndexChanged="Grid_Listado_Vehiculos_SelectedIndexChanged" PageSize="100"
                                        Width="98%">
                                        <RowStyle CssClass="GridItem" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                            <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="VEHICULO" HeaderText="Vehículo" SortExpression="VEHICULO">
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="150px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ANIO" DataFormatString="{0:####}" HeaderText="Año" SortExpression="ANIO">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="70px" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Center" Width="90px" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="GridHeader" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </caption>
                            </center>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <table width="95%">
                    <tr>
                        <td style="width: 100%">
                            <center>
                                <asp:Button ID="Btn_Cerrar" runat="server" TabIndex="202" Text="Cerrar" Width="80px"
                                    Height="26px" />
                            </center>
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Panel_Filtro_Busqueda_Empleados" runat="server" Width="100%" DefaultButton="Btn_Busqueda_Empleados">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Empleado
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Empleado" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                    WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                RFC
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                    TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                    WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Nombre
                                            </td>
                                            <td style="width: 30%; text-align: left;" colspan="3">
                                                <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                    FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    ValidChars="áéíóúÁÉÍÓÚ ñÑ.-" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Mpe_Proveedores" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Proveedores_Cabecera" runat="server" TargetControlID="Btn_Comodin_Mpe_Proveedores"
                PopupControlID="Pnl_Mpe_Proveedores" CancelControlID="Btn_Cerrar_Mpe_Proveedores"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Proveedores" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Proveedores_Interno" runat="server" CssClass="estilo_fuente"
            Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
            border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Img_Mpe_Proveedores" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda y Selección de Proveedores
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Proveedores" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Proveedores" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Proveedores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <asp:Panel ID="Pnl_Buscar_Proveedor" runat="server" Width="98%" DefaultButton="Btn_Ejecutar_Busqueda_Proveedores">
                            <table width="100%">
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Proveedores_Buscar" runat="server" CssClass="estilo_fuente"
                                            Text="Nombre" Style="font-weight: bolder;"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Buscar" runat="server" Width="92%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            TargetControlID="Txt_Nombre_Proveedor_Buscar" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            Enabled="True" TargetControlID="Txt_Nombre_Proveedor_Buscar" WatermarkCssClass="watermarked"
                                            WatermarkText="<-- Razón Social ó Nombre Comercial -->">
                                        </cc1:TextBoxWatermarkExtender>
                                        <asp:ImageButton ID="Btn_Ejecutar_Busqueda_Proveedores" runat="server" OnClick="Btn_Ejecutar_Busqueda_Proveedores_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="Pnl_Listado_Proveedores" runat="server" ScrollBars="Vertical" Style="white-space: normal;"
                            Width="100%" BorderColor="#3366FF" Height="330px">
                            <asp:GridView ID="Grid_Listado_Proveedores" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Listado_Proveedores_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Proveedores_SelectedIndexChanged" PageSize="150"
                                Width="100%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PROVEEDOR_ID" HeaderText="Proveedor ID" SortExpression="PROVEEDOR_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Razón Social" SortExpression="NOMBRE">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMPANIA" HeaderText="Nombre Comercial" SortExpression="COMPANIA">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
