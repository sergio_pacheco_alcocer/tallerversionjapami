<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Migrar_Bienes.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Pat_Migrar_Bienes" Title="Migración de Bienes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server">
    </cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Migración de Bienes</td>
                    </tr>
                    <tr>
                        <td>
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td>&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="center">
                        <td colspan="2">MIGRACIÓN</td>                        
                    </tr>
                </table>   
                <br />
                <center>
     <div style="text-align:center;">
        <asp:Button ID="Btn_Actualizar_Costos" runat="server" Text="Actualizar Costos" OnClick="Btn_Actualizar_Costos_Click" />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <asp:Button ID="Btn_Subir_Bienes_Muebles" runat="server" Text="SUBIR BIENES MUEBLES" onclick="Btn_Subir_Bienes_Muebles_Click" Visible="false" />
        <asp:Label ID="Lbl_Contador" runat="server" Text="Registros Leidos: 0"></asp:Label>
        <br />
        <br />
        <hr />
        <asp:Button ID="Btn_Subir_Vehiculos" runat="server" Text="SUBIR VEHICULOS" 
             onclick="Btn_Subir_Vehiculos_Click" Visible="False" />
        <br />
        <br />
        <hr />
        <asp:Button ID="Btn_Subir_Animales" runat="server" Text="SUBIR ANIMALES" 
             onclick="Btn_Subir_Animales_Click" Visible="False" />
        <br />
        <br />
        <hr />
        <asp:Button ID="Btn_Subir_Bienes_Muebles_2" runat="server" 
            Text="SUBIR BIENES MUEBLES" onclick="Btn_Subir_Bienes_Muebles_2_Click" 
             Visible="False"  />
        <br />
        <br />
        <hr />
         <asp:Label ID="Lbl_Archivo" runat="server" Text="ARCHIVO"></asp:Label>
         <asp:TextBox ID="Txt_Archivo" runat="server" Visible="False"></asp:TextBox>
         <br />
         <asp:Label ID="Lbl_Hoja" runat="server" Text="HOJA"></asp:Label>
         <asp:TextBox ID="Txt_Hoja" runat="server" Visible="False"></asp:TextBox>
         <br />
        <asp:Button ID="Btn_Subir_Resguardos_Bienes_Muebles" runat="server" 
             Text="SUBIR RESGUARDOS BIENES MUEBLES" 
             onclick="Btn_Subir_Resguardos_Bienes_Muebles_Click" Visible="False" />
        <br />
        <br />
        <hr />
        <asp:Button ID="Btn_Actualizar_Estatus_Bienes" runat="server" 
             Text="ACTUALIZAR ESTATUS" onclick="Btn_Actualizar_Estatus_Bienes_Click" 
             Visible="False"  />
        <br />
        <br />
        <hr />
         <asp:Label ID="Lbl_Archivo_Actualizacion" runat="server" Text="ARCHIVO"></asp:Label>
         <asp:TextBox ID="Txt_Archivo_Actualizacion" runat="server" Width="350px"></asp:TextBox>
         <br />
         <asp:Label ID="Lbl_Hoja_Actualizacion" runat="server" Text="HOJA"></asp:Label>
         <asp:TextBox ID="Txt_Hoja_Actualizacion" runat="server" Width="350px"></asp:TextBox>
         <br />
        <asp:Button ID="Btn_Cargar_Actualizacion" runat="server" 
             Text="CARGAR ACTUALIZACIÓN DE BIENES" 
             onclick="Btn_Cargar_Actualizacion_Click" Visible="False" />
        <br />
        <br />
        <hr />
    </div>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <asp:Button ID="Btn_Actualizar_Inventarios_Anteriores" runat="server" Text="ACTUALIZAR NUMEROS DE INVENTARIO" onclick="Btn_Actualizar_Inventarios_Anteriores_Click" Visible="false" />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <br />
            <hr />
            <asp:Button ID="Btn_Actualizar_Proveedores" runat="server" Text="ACTUALIZAR PROVEEDORES UBICACION FISICA" onclick="Btn_Actualizar_Proveedores_Click" Visible="false" /><br />
        <br />
        <hr />
        <hr />
        <hr />
        <asp:Button runat="server" ID="Btn_Baja_Activos_Economicos" 
                Text="BAJA DE ACTIVOS ECONOMICOS" OnClick="Btn_Baja_Activos_Economicos_Click" 
                Visible="False" /><br />
        <asp:Button runat="server" ID="Btn_Migracion_Resguardos_Economicos" 
                Text="MIGRACION DE ACTIVOS ECONOMICOS" 
                OnClick="Btn_Migracion_Resguardos_Economicos_Click" Visible="False" />
    
    </div>
                </center>      
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

