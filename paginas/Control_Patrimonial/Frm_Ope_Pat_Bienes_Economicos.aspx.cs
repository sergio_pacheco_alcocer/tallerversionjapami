﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Control_Patrimonial_Bienes_Economicos.Negocio;
using JAPAMI.Catalogo_Compras_Marcas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Materiales.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Vehiculo.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Catalogo_Compras_Productos.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using JAPAMI.Almacen_Resguardos.Negocio;
using JAPAMI.Reportes;

public partial class paginas_Compras_Frm_Ope_Pat_Bienes_Economicos : System.Web.UI.Page
{
    
    #region "Page Load"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Se carga por default en la pagina al iniciar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e){
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Trim().Length == 0) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack) {
                Llenar_Combos_Independientes();
                Habilitacion_Componentes("");
                Llenar_MPE_Listado_Productos(0);
            }
        }

    #endregion

    #region "Metodos"

        #region "Limpiar"

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Historial_Resguardantes
            ///DESCRIPCIÓN: Se Limpian los campos de Historial de los Resguardantes de los 
            ///             Bienes Muebles.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 13/Diciembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Limpiar_Historial_Resguardantes()
            {
                try
                {
                    Txt_Historial_Empleado_Resguardo.Text = "";
                    Txt_Historial_Fecha_Inicial_Resguardo.Text = "";
                    Txt_Historial_Fecha_Final_Resguardo.Text = "";
                    Txt_Historial_Comentarios_Resguardo.Text = "";
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Campos_Generales
            ///DESCRIPCIÓN          : Limpiar TODOS los campos en el Formulario.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Limpiar_Campos_Generales() {
                Hdf_Bien_ID.Value = "";
                Txt_Clave_Resguardo.Text = "";
                Hdf_Producto_ID.Value = "";
                Txt_Nombre_Bien.Text = "";
                Cmb_Marca.SelectedIndex = 0;
                Txt_Modelo.Text = "";
                Cmb_Material.SelectedIndex = 0;
                Cmb_Color.SelectedIndex = 0;
                Txt_Cantidad.Text = "";
                Txt_Costo.Text = "";
                Txt_Fecha_Adquisicion.Text = "";
                Txt_Numero_Serie.Text = "";
                Cmb_Estado.SelectedIndex = 0;
                Cmb_Estatus.SelectedIndex = 0;
                Txt_Comentarios.Text = "";
                Txt_Motivo_Baja.Text = "";
                Txt_Usuario_creo.Text = "";
                Txt_Usuario_Modifico.Text = "";
                Cmb_Gerencias.SelectedIndex = 0;
                Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);
                Cmb_Dependencias.SelectedIndex = -1;
                Cmb_Dependencias_SelectedIndexChanged(Cmb_Dependencias, null);
                Limpiar_Resguardante();
                Session.Remove("Dt_Resguardantes_BM");
                Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, new DataTable());
                Limpiar_Historial_Resguardantes();
                Session.Remove("Dt_Historial_Resguardos");
                Llenar_Grid_Historial_Resguardos(Grid_Resguardantes.PageIndex, new DataTable());
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Limpiar_Resguardante
            ///DESCRIPCIÓN: Limpiar_Resguardante
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Limpiar_Resguardante() {
                Grid_Resguardantes.SelectedIndex = (-1);
                Cmb_Empleados.SelectedIndex = 0;
                Txt_Cometarios.Text = "";
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Resguardantes
            ///DESCRIPCIÓN: Llena la tabla de Resguardantes
            ///PROPIEDADES:     
            ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
            ///             2.  tabla.  Tabla que se va a cargar en el Grid.    
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Resguardantes(Int32 Pagina, DataTable Tabla)
            {
                Session["Dt_Resguardantes_BM"] = Tabla;
                Grid_Resguardantes.Columns[1].Visible = true;
                Grid_Resguardantes.Columns[2].Visible = true;
                Grid_Resguardantes.DataSource = Tabla;
                Grid_Resguardantes.PageIndex = Pagina;
                Grid_Resguardantes.DataBind();
                Grid_Resguardantes.Columns[1].Visible = false;
                Grid_Resguardantes.Columns[2].Visible = false;
            }
            
        #endregion

        #region "Interaccion con Base de Datos [Mostrar Datos y Llenado de Componentes]"

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Ubicar_Gerencia
            ///DESCRIPCIÓN: Coloca el combo gerencia en la que le corresponde al empleado seleccionado del grid de búsqueda
            ///PROPIEDADES: el identificador de la dependencia para buscar la gerencia
            ///CREO: Luis Daniel Guzmán Malagón.
            ///FECHA_CREO: 30/Octubre/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private String Ubicar_Gerencia(String Dependencia_ID)
            {
                
                Cls_Ope_Pat_Bienes_Economicos_Negocio Combos = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                //SE LLENA EL COMBO DE DEPENDENCIAS
                Combos.P_Tipo_DataTable = "DEPENDENCIAS";// Se llena el combo de dependencias para obtener la gerencia ID y asi colocar la gerencia a la que pertence el empleado seleccionado en la búsqueda
                DataTable Dependencias = Combos.Consultar_DataTable();
                DataRow DR_Fila;
                String Gerencia_ID = "";//variable para almacenar la gerencia_ID cuando se encuentre
                for (int i = 0; i < Dependencias.Rows.Count; i++)
                {//Ciclo para encontrar la dependencia_ID y obtener la gerencia_id cuando se encuentre
                    DR_Fila = Dependencias.Rows[i];
                    if (DR_Fila["DEPENDENCIA_ID"].ToString().Equals(Dependencia_ID))
                    {
                        Gerencia_ID = DR_Fila["GERENCIA_ID"].ToString();
                    }
                }
                return Gerencia_ID;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
            ///DESCRIPCIÓN: Llena el combo de Empleados.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 30/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Combo_Empleados(DataTable Tabla, ref DropDownList Cmb_Tmp_Empleados)
            {
                try
                {
                    DataRow Fila_Empleado = Tabla.NewRow();
                    Fila_Empleado["EMPLEADO_ID"] = "SELECCIONE";
                    Fila_Empleado["NOMBRE_COMPLETO"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
                    Tabla.Rows.InsertAt(Fila_Empleado, 0);
                    Cmb_Tmp_Empleados.DataSource = Tabla;
                    Cmb_Tmp_Empleados.DataValueField = "EMPLEADO_ID";
                    Cmb_Tmp_Empleados.DataTextField = "NOMBRE_COMPLETO";
                    Cmb_Tmp_Empleados.DataBind();
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Consultar_Empleados
            ///DESCRIPCIÓN:          Metodo utilizado para consultar y llenar el combo con los empleados que pertenecen a la dependencia
            ///PROPIEDADES:     
            ///                      1.  Dependencia_ID. el identificador de la dependencia en base a la que se va hacer la consulta
            ///              
            ///CREO:                 Salvador Hernández Ramírez
            ///FECHA_CREO: 02/Febrero/2011 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            public void Consultar_Empleados(String Dependencia_ID, ref DropDownList Cmb_Tmp_Empleados)
            {
                try
                {
                    Session.Remove("Dt_Resguardantes");
                    Llenar_Grid_Resguardantes(0, new DataTable());
                    Session.Remove("Dt_Resguardantes_BM"); 
                    if (!String.IsNullOrEmpty(Dependencia_ID))
                    {
                        Cls_Ope_Pat_Bienes_Economicos_Negocio Combo = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                        Combo.P_Tipo_DataTable = "EMPLEADOS";
                        Combo.P_Dependencia_ID = Dependencia_ID;
                        DataTable Tabla = Combo.Consultar_DataTable();
                        Llenar_Combo_Empleados(Tabla, ref Cmb_Tmp_Empleados);
                    }
                    else
                    {
                        DataTable Tabla = new DataTable();
                        Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                        Tabla.Columns.Add("NOMBRE_COMPLETO", Type.GetType("System.String"));
                        Llenar_Combo_Empleados(Tabla, ref Cmb_Tmp_Empleados);
                    }
                }
                catch (Exception Ex)
                {
                    Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Independientes
            ///DESCRIPCIÓN          : Llena los Combos en el Formulario.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combos_Independientes(){
                Cls_Ope_Pat_Bienes_Economicos_Negocio Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                
                //Combo de Marcas
                Negocio.P_Tipo_DataTable = "MARCAS";
                DataTable Dt_Marcas = Negocio.Consultar_DataTable();
                Cmb_Marca.DataSource = Dt_Marcas;
                Cmb_Marca.DataTextField = "TEXTO";
                Cmb_Marca.DataValueField = "IDENTIFICADOR";
                Cmb_Marca.DataBind();
                Cmb_Marca.Items.Insert(0, new ListItem("< SELECCIONE >", ""));

                Cmb_Busqueda_Bien_Mueble_Marca.DataSource = Dt_Marcas;
                Cmb_Busqueda_Bien_Mueble_Marca.DataTextField = "TEXTO";
                Cmb_Busqueda_Bien_Mueble_Marca.DataValueField = "IDENTIFICADOR";
                Cmb_Busqueda_Bien_Mueble_Marca.DataBind();
                Cmb_Busqueda_Bien_Mueble_Marca.Items.Insert(0, new ListItem("< SELECCIONE >", ""));

                //Combo de Materiales
                Negocio.P_Tipo_DataTable = "MATERIALES";
                DataTable Dt_Materiales = Negocio.Consultar_DataTable();
                Cmb_Material.DataSource = Dt_Materiales;
                Cmb_Material.DataTextField = "TEXTO";
                Cmb_Material.DataValueField = "IDENTIFICADOR";
                Cmb_Material.DataBind();
                Cmb_Material.Items.Insert(0, new ListItem("< SELECCIONE >", ""));

                //Combo de Colores
                Negocio.P_Tipo_DataTable = "COLORES";
                DataTable Dt_Colores = Negocio.Consultar_DataTable();
                Cmb_Color.DataSource = Dt_Colores;
                Cmb_Color.DataTextField = "TEXTO";
                Cmb_Color.DataValueField = "IDENTIFICADOR";
                Cmb_Color.DataBind();
                Cmb_Color.Items.Insert(0, new ListItem("< SELECCIONE >", ""));

                //Combo de Dependencias
                Negocio.P_Tipo_DataTable = "DEPENDENCIAS_COMBOS";
                DataTable Dt_Dependencias = Negocio.Consultar_DataTable();
                Cmb_Busqueda_Dependencia.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Dependencia.DataTextField = "TEXTO";
                Cmb_Busqueda_Dependencia.DataValueField = "IDENTIFICADOR";
                Cmb_Busqueda_Dependencia.DataBind();
                Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< TODAS >", ""));

                Cmb_Busqueda_Bien_Mueble_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Bien_Mueble_Dependencias.DataTextField = "TEXTO";
                Cmb_Busqueda_Bien_Mueble_Dependencias.DataValueField = "IDENTIFICADOR";
                Cmb_Busqueda_Bien_Mueble_Dependencias.DataBind();
                Cmb_Busqueda_Bien_Mueble_Dependencias.Items.Insert(0, new ListItem("< TODAS >", ""));

                Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.DataSource = Dt_Dependencias;
                Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.DataTextField = "TEXTO";
                Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.DataValueField = "IDENTIFICADOR";
                Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.DataBind();
                Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.Items.Insert(0, new ListItem("< TODAS >", ""));

                //SE LLENA EL COMBO DE GERENCIAS
                Negocio.P_Tipo_DataTable = "GERENCIAS";
                DataTable Gerencias = Negocio.Consultar_DataTable();
                DataRow Fila_Gerencia = Gerencias.NewRow();
                Fila_Gerencia["GERENCIA_ID"] = "SELECCIONE";
                Fila_Gerencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
                Gerencias.Rows.InsertAt(Fila_Gerencia, 0);
                Cmb_Gerencias.DataSource = Gerencias;
                Cmb_Gerencias.DataValueField = "GERENCIA_ID";
                Cmb_Gerencias.DataTextField = "NOMBRE";
                Cmb_Gerencias.DataBind();
                Gerencias.Rows.RemoveAt(0);
            }
                
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Empleados
            ///DESCRIPCIÓN          : Llena el Combo con los empleados de una dependencia.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 22/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_Combo_Empleados(String Dependencia_ID, ref DropDownList Combo_Empleados) {
                Combo_Empleados.Items.Clear();
                if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0) {
                    Cls_Ope_Pat_Bienes_Economicos_Negocio BSI_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                    BSI_Negocio.P_Tipo_DataTable = "EMPLEADOS";
                    BSI_Negocio.P_Dependencia_ID = Dependencia_ID.Trim();
                    DataTable Dt_Datos = BSI_Negocio.Consultar_DataTable();
                    Combo_Empleados.DataSource = Dt_Datos;
                    Combo_Empleados.DataValueField = "EMPLEADO_ID";
                    Combo_Empleados.DataTextField = "EMPLEADO";
                    Combo_Empleados.DataBind();
                } 
                Combo_Empleados.Items.Insert(0, new ListItem("< TODOS >", ""));
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Obtener_Descripcion_Identificador
            ///DESCRIPCIÓN          : Obtiene la Descripcion de un identificador.
            ///PARAMETROS           : 1.Tipo. Tipo de Busqueda.
            ///                       2.Identificador. Identificador a Buscar su Descripción.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private String Obtener_Descripcion_Identificador(String Tipo, String Identificador) {
                String Descripcion = null;
                Cls_Ope_Pat_Bienes_Economicos_Negocio Negocio_Tmp = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                Negocio_Tmp.P_Tipo_DataTable = Tipo;
                Negocio_Tmp.P_Identificador_Generico = Identificador;
                DataTable Dt_Datos = Negocio_Tmp.Consultar_DataTable();
                if (Dt_Datos != null && Dt_Datos.Rows.Count > 0) {
                    Descripcion = Dt_Datos.Rows[0]["TEXTO"].ToString();
                }
                return Descripcion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_MPE_Listado_Productos
            ///DESCRIPCIÓN          : Llena el Grid de los Productos.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 18/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Llenar_MPE_Listado_Productos(Int32 Pagina) {
                try {
                    Grid_Listado_Productos.SelectedIndex = (-1);
                    Grid_Listado_Productos.Columns[1].Visible = true;
                    Cls_Cat_Com_Productos_Negocio Productos_Negocio = new Cls_Cat_Com_Productos_Negocio();
                    if (Txt_Nombre_Producto_Buscar.Text.Trim() != "") {
                        Productos_Negocio.P_Nombre = Txt_Nombre_Producto_Buscar.Text.Trim();
                    }
                    Productos_Negocio.P_Estatus = "ACTIVO";
                    DataTable Dt_Productos = Productos_Negocio.Consulta_Datos_Producto();
                    Dt_Productos.Columns[Cat_Com_Productos.Campo_Producto_ID].ColumnName = "PRODUCTO_ID";
                    Dt_Productos.Columns[Cat_Com_Productos.Campo_Clave].ColumnName = "CLAVE_PRODUCTO";
                    Dt_Productos.Columns[Cat_Com_Productos.Campo_Nombre].ColumnName = "NOMBRE_PRODUCTO";
                    Dt_Productos.Columns[Cat_Com_Productos.Campo_Descripcion].ColumnName = "DESCRIPCION_PRODUCTO";
                    Grid_Listado_Productos.DataSource = Dt_Productos;
                    Grid_Listado_Productos.PageIndex = Pagina;
                    Grid_Listado_Productos.DataBind();
                    Grid_Listado_Productos.Columns[1].Visible = false;
                } catch (Exception Ex) {
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
            ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
            ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
            ///CREO: Luis Daniel Guzmán Malagón.
            ///FECHA_CREO: 30/Octubre/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Combo_Dependencias(String Gerencia_ID)
            {
                Cls_Ope_Pat_Bienes_Economicos_Negocio Combos = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                //SE LLENA EL COMBO DE DEPENDENCIAS
                Combos.P_Gerencia_ID = Gerencia_ID;
                Combos.P_Tipo_DataTable = "DEPENDENCIAS";
                DataTable Dependencias = Combos.Consultar_DataTable();
                Cmb_Dependencias.DataSource = Dependencias;//Se especifica la fuente de información que llenará el combo
                Cmb_Dependencias.DataValueField = "DEPENDENCIA_ID";//Se especifica el campo de la tabla dependencias que contendrá el valor de los renglones del combo
                Cmb_Dependencias.DataTextField = "NOMBRE";//Se especifica el campo de la tabla dependencias que contendrá el texto que mostrará el combo
                Cmb_Dependencias.DataBind();//Se unen los datos del combo
                Cmb_Dependencias.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));//Se inserta el renglón de <-SELECCIONE-> en la posición 0
                Cmb_Dependencias_SelectedIndexChanged(Cmb_Dependencias, null);
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
            ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
            ///PROPIEDADES:     
            ///CREO:                 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 24/Octubre/2011 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Busqueda_Empleados_Resguardo()
            {
                Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
                Cls_Ope_Pat_Bienes_Economicos_Negocio Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                Negocio.P_Estatus = "ACTIVO";
                if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado_Resguardante = Txt_Busqueda_No_Empleado.Text.Trim(); }
                if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Resguardante = Txt_Busqueda_RFC.Text.Trim(); }
                if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre = Txt_Busqueda_Nombre_Empleado.Text.Trim(); }
                if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
                Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados_Resguardos();
                Grid_Busqueda_Empleados_Resguardo.DataBind();
                Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Bienes_Economicos
            ///DESCRIPCIÓN: Llena el Grid con los bienes economicos que cumplan el filtro
            ///PROPIEDADES:     
            ///CREO:                 
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 24/Octubre/2011 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Busqueda_Bienes_Economicos() { 
                Cls_Ope_Pat_Bienes_Economicos_Negocio BE_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                BE_Negocio.P_Tipo_DataTable = "BIENES_ECONOMICOS";
                String Tipo_Busqueda = Session["TIPO_BUSQUEDA_BIENES"].ToString().Trim();
                if (Tipo_Busqueda.Equals("GENERAL"))
                {
                    if (Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS.Text.Trim().Length > 0) BE_Negocio.P_Bien_ID = Convert.ToInt32(Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS.Text.Trim());
                    if (Txt_Busqueda_Bien_Mueble_Producto.Text.Trim().Length > 0) BE_Negocio.P_Nombre = Txt_Busqueda_Bien_Mueble_Producto.Text.Trim();
                    if (Cmb_Busqueda_Bien_Mueble_Dependencias.SelectedIndex > 0) BE_Negocio.P_Dependencia_ID = Cmb_Busqueda_Bien_Mueble_Dependencias.SelectedItem.Value.Trim();
                    if (Txt_Busqueda_Bien_Mueble_Modelo.Text.Trim().Length > 0) BE_Negocio.P_Modelo = Txt_Busqueda_Bien_Mueble_Modelo.Text.Trim();
                    if (Cmb_Busqueda_Bien_Mueble_Marca.SelectedIndex > 0) BE_Negocio.P_Marca = Cmb_Busqueda_Bien_Mueble_Marca.SelectedItem.Value.Trim();
                    if (Cmb_Busqueda_Bien_Mueble_Estatus.SelectedIndex > 0) BE_Negocio.P_Estatus = Cmb_Busqueda_Bien_Mueble_Estatus.SelectedItem.Value.Trim();
                    if (Txt_Busqueda_Bien_Mueble_Numero_Serie.Text.Trim().Length > 0) BE_Negocio.P_Numero_Serie = Txt_Busqueda_Bien_Mueble_Numero_Serie.Text.Trim();
                }
                else if (Tipo_Busqueda.Equals("RESGUARDATARIOS"))
                {
                    if (Txt_Busqueda_Bien_Mueble_No_Empleado.Text.Trim().Length > 0) BE_Negocio.P_No_Empleado_Resguardante = String.Format("{0:000000}", (Convert.ToInt32(Txt_Busqueda_Bien_Mueble_No_Empleado.Text.Trim())));
                    if (Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.SelectedIndex > 0) BE_Negocio.P_Dependencia_ID = Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                    if (Cmb_Busqueda_Bien_Mueble_Nombre_Resguardante.SelectedIndex > 0) BE_Negocio.P_Resguardante_ID = Cmb_Busqueda_Bien_Mueble_Nombre_Resguardante.SelectedItem.Value.Trim();
                }
                DataTable Dt_Bienes_Economicos = BE_Negocio.Consultar_DataTable();
                Grid_Listado_Bienes_Bien_Mueble.DataSource = Dt_Bienes_Economicos;
                Grid_Listado_Bienes_Bien_Mueble.DataBind();
            }


            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Historial_Resguardos
            ///DESCRIPCIÓN: Llena la tabla de Historial de Resguardantes
            ///PROPIEDADES:     
            ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
            ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 13/Diciembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private void Llenar_Grid_Historial_Resguardos(Int32 Pagina, DataTable Tabla)
            {
                Grid_Historial_Resguardantes.Columns[1].Visible = true;
                Grid_Historial_Resguardantes.Columns[2].Visible = true;
                Grid_Historial_Resguardantes.DataSource = Tabla;
                Grid_Historial_Resguardantes.PageIndex = Pagina;
                Grid_Historial_Resguardantes.DataBind();
                Grid_Historial_Resguardantes.Columns[1].Visible = false;
                Grid_Historial_Resguardantes.Columns[2].Visible = false;
                Session["Dt_Historial_Resguardos"] = Tabla;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Cambios
            ///DESCRIPCIÓN: Generar el reporte de Cambios.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 11/enero/2013
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************
            private void Generar_Reporte_Cambios()
            {
                Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio Negocio = new Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio();
                Negocio.P_Tipo_Bien = "BIEN_ECONOMICO";
                Negocio.P_Bien_ID = Hdf_Bien_ID.Value.Trim();
                DataTable Dt_Registros = Negocio.Consultar_Cambios_Bien();
                if (Dt_Registros.Rows.Count > 0)
                {
                    Dt_Registros.TableName = "DT_DATOS";
                    DataSet Ds_Consulta = new DataSet();
                    Ds_Consulta.Tables.Add(Dt_Registros.Copy());
                    Ds_Pat_Historial_Cambios_Bienes Ds_Reporte = new Ds_Pat_Historial_Cambios_Bienes();
                    Generar_Reporte_Cambios(Ds_Consulta, Ds_Reporte);
                }
                else
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No se han registrado cambios para este Bien');", true);
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
            ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
            ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
            ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
            ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
            ///CREO: Susana Trigueros Armenta.
            ///FECHA_CREO: 01/Mayo/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            private void Generar_Reporte_Cambios(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte)
            {
                ReportDocument Reporte = new ReportDocument();
                String File_Path = Server.MapPath("../Rpt/Compras/Rpt_Pat_Historial_Cambios_Bienes.rpt");
                Reporte.Load(File_Path);
                Ds_Reporte = Data_Set_Consulta_DB;
                Reporte.SetDataSource(Ds_Reporte);
                ExportOptions Export_Options = new ExportOptions();
                DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
                String Ruta = "../../Reporte/Rpt_Pat_Historial_Cambios_Bienes" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
                Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
                Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
                Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
                Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.SetParameterValue("USUARIO_GENERO", Cls_Sessiones.Nombre_Empleado);
                Reporte.Export(Export_Options);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            }

        #endregion 

        #region "Interaccion con Base de Datos [Alteración de Datos]"

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Alta_Bien
            ///DESCRIPCIÓN          : Da de Alta la información de un Bien.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private Cls_Ope_Pat_Bienes_Economicos_Negocio Alta_Bien() {
                Cls_Ope_Pat_Bienes_Economicos_Negocio Bien_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                Bien_Negocio.P_Producto_ID = Hdf_Producto_ID.Value;
                Bien_Negocio.P_Nombre = Txt_Nombre_Bien.Text.Trim();
                Bien_Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value;
                Bien_Negocio.P_Dependencia_ID = Cmb_Dependencias.SelectedItem.Value;
                Bien_Negocio.P_Marca = Cmb_Marca.SelectedItem.Value;
                Bien_Negocio.P_Modelo = Txt_Modelo.Text.Trim();
                Bien_Negocio.P_Material = Cmb_Material.SelectedItem.Value;
                Bien_Negocio.P_Color = Cmb_Color.SelectedItem.Value;
                Bien_Negocio.P_Costo_Inicial = Convert.ToDouble(Txt_Costo.Text.Trim());
                Bien_Negocio.P_Cantidad = Convert.ToInt32(Txt_Cantidad.Text.Trim());
                if (!String.IsNullOrEmpty(Txt_Fecha_Adquisicion.Text)) Bien_Negocio.P_Fecha_Adquisicion = Convert.ToDateTime(Txt_Fecha_Adquisicion.Text.Trim());
                Bien_Negocio.P_Numero_Serie = Txt_Numero_Serie.Text.Trim();
                Bien_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                Bien_Negocio.P_Estado = Cmb_Estado.SelectedItem.Value;
                Bien_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
                Bien_Negocio.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
                Bien_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
                Bien_Negocio.P_Dt_Resguardantes = (DataTable)Session["Dt_Resguardantes_BM"];
                Bien_Negocio = Bien_Negocio.Alta_Bien_Economico();
                Limpiar_Campos_Generales();
                return Bien_Negocio;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Modificar_Bien
            ///DESCRIPCIÓN          : Actualiza la información de un Bien.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Modificar_Bien() {
                Cls_Ope_Pat_Bienes_Economicos_Negocio Bien_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                Bien_Negocio.P_Bien_ID = Convert.ToInt32(Hdf_Bien_ID.Value);
                Bien_Negocio.P_Producto_ID = Hdf_Producto_ID.Value;
                Bien_Negocio.P_Nombre = Txt_Nombre_Bien.Text.Trim();
                Bien_Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value;
                Bien_Negocio.P_Dependencia_ID = Cmb_Dependencias.SelectedItem.Value;
                Bien_Negocio.P_Marca = Cmb_Marca.SelectedItem.Value;
                Bien_Negocio.P_Modelo = Txt_Modelo.Text.Trim();
                Bien_Negocio.P_Material = Cmb_Material.SelectedItem.Value;
                Bien_Negocio.P_Color = Cmb_Color.SelectedItem.Value;
                Bien_Negocio.P_Cantidad = Convert.ToInt32(Txt_Cantidad.Text.Trim());
                Bien_Negocio.P_Costo_Inicial = Convert.ToDouble(Txt_Costo.Text.Trim());
                if (!String.IsNullOrEmpty(Txt_Fecha_Adquisicion.Text)) Bien_Negocio.P_Fecha_Adquisicion = Convert.ToDateTime(Txt_Fecha_Adquisicion.Text.Trim());
                Bien_Negocio.P_Numero_Serie = Txt_Numero_Serie.Text.Trim();
                Bien_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                Bien_Negocio.P_Estado = Cmb_Estado.SelectedItem.Value;
                Bien_Negocio.P_Comentarios = Txt_Comentarios.Text.Trim();
                Bien_Negocio.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
                Bien_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
                Bien_Negocio.P_Dt_Resguardantes = (DataTable)Session["Dt_Resguardantes_BM"];
                if (Txt_Motivo_Baja.Visible) Bien_Negocio.P_Motivo_Baja = Txt_Motivo_Baja.Text.Trim(); else Bien_Negocio.P_Motivo_Baja = "";

                Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio Tmp_Negocio = new Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio();
                Tmp_Negocio.P_Tipo_Bien = "BIEN_ECONOMICO";
                Tmp_Negocio.P_Bien_ID = Hdf_Bien_ID.Value.Trim();
                Tmp_Negocio.P_BE_Actualizado = Bien_Negocio;
                Bien_Negocio.P_Dt_Cambios = Tmp_Negocio.Obtener_Tabla_Cambios();

                Bien_Negocio.Modifica_Bien_Economico();
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Mostrar_Detalles_Bien
            ///DESCRIPCIÓN          : Muestra la información de un Bien.
            ///PARAMETROS           : Bien. Datos a Cargar.
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 11/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Mostrar_Detalles_Bien(Cls_Ope_Pat_Bienes_Economicos_Negocio Bien) {
                Limpiar_Campos_Generales();
                Hdf_Bien_ID.Value = Bien.P_Bien_ID.ToString();
                Txt_Clave_Resguardo.Text = Bien.P_Bien_ID.ToString();
                Hdf_Producto_ID.Value = Bien.P_Producto_ID.ToString();
                Txt_Nombre_Bien.Text = Bien.P_Nombre;
                Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Bien.P_Gerencia_ID));
                Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);
                Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Bien.P_Dependencia_ID));
                Cmb_Dependencias_SelectedIndexChanged(Cmb_Dependencias, null);
                Cmb_Marca.SelectedIndex = Cmb_Marca.Items.IndexOf(Cmb_Marca.Items.FindByValue(Bien.P_Marca));
                Txt_Modelo.Text = Bien.P_Modelo;
                Cmb_Material.SelectedIndex = Cmb_Material.Items.IndexOf(Cmb_Material.Items.FindByValue(Bien.P_Material));
                Cmb_Color.SelectedIndex = Cmb_Color.Items.IndexOf(Cmb_Color.Items.FindByValue(Bien.P_Color));
                Txt_Costo.Text = Bien.P_Costo_Inicial.ToString("#######0.00");
                Txt_Cantidad.Text = Bien.P_Cantidad.ToString("#######0");
                if (!String.Format("{0:ddMMyyyy}", Bien.P_Fecha_Adquisicion).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()).Trim()))
                    Txt_Fecha_Adquisicion.Text = String.Format("{0:dd/MMM/yyyy}", Bien.P_Fecha_Adquisicion);
                Txt_Numero_Serie.Text = Bien.P_Numero_Serie;
                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Bien.P_Estatus));
                Cmb_Estado.SelectedIndex = Cmb_Estado.Items.IndexOf(Cmb_Estado.Items.FindByValue(Bien.P_Estado));
                Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                if (Bien.P_Motivo_Baja != null && Bien.P_Motivo_Baja.Length>0) {
                    Txt_Motivo_Baja.Text = Bien.P_Motivo_Baja.Trim();
                }
                Txt_Comentarios.Text = Bien.P_Comentarios;
                Llenar_Grid_Resguardantes(0, Bien.P_Dt_Resguardantes);
                Txt_Usuario_creo.Text=Bien.P_Dato_Creo;
                Txt_Usuario_Modifico.Text = Bien.P_Dato_Modifico;
                Llenar_Grid_Historial_Resguardos(0, Bien.P_Dt_Historial_Resguardos);
            }

        #endregion

        #region "Configuración de Componentes"

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitacion_Componentes
            ///DESCRIPCIÓN          : Habilita e Inhabilita los componentes del Formulario.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 04/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            private void Habilitacion_Componentes(String Tipo) {
                if (Tipo.Trim().Equals("NUEVO")) {
                    Btn_Nuevo.ToolTip = "Guardar";
                    Btn_Nuevo.AlternateText = "Guardar";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Modificar.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar Operación de Alta";
                    Btn_Salir.AlternateText = "Cancelar Operación de Alta";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Div_Busqueda.Visible = false; 
                    Btn_Imprimir.Visible = false;
                    Btn_Generar_Reporte_Cambios.Visible = false;
                    Btn_Lanzar_Buscar_Producto.Visible = true;
                    Cmb_Marca.Enabled = true;
                    Txt_Modelo.Enabled = true;
                    Cmb_Gerencias.Enabled = true;
                    Cmb_Dependencias.Enabled = true;
                    Cmb_Material.Enabled = true;
                    Cmb_Empleados.Enabled = true;
                    Btn_Busqueda_Avanzada_Resguardante.Visible = true;
                    Btn_Agregar_Resguardante.Visible = true;
                    Btn_Quitar_Resguardante.Visible = true;
                    Grid_Resguardantes.Enabled = true;
                    Cmb_Color.Enabled = true;
                    Txt_Costo.Enabled = true;
                    Txt_Cantidad.Enabled = true;
                    Btn_Fecha_Adquisicion.Enabled = true;
                    Txt_Numero_Serie.Enabled = true;
                    Cmb_Estado.Enabled = true;
                    Cmb_Estado.SelectedIndex = 1;
                    Cmb_Estatus.Enabled = false;
                    Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                    Cmb_Estatus.SelectedIndex = 1;
                    Txt_Cantidad.Text = "1";
                    Txt_Comentarios.Enabled = true;
                    Txt_Motivo_Baja.Enabled = true;
                    Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                } else if (Tipo.Trim().Equals("MODIFICAR")) {
                    Btn_Modificar.ToolTip = "Guardar";
                    Btn_Modificar.AlternateText = "Guardar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Btn_Nuevo.Visible = false;
                    Btn_Salir.ToolTip = "Cancelar Operación de Actualización";
                    Btn_Salir.AlternateText = "Cancelar Operación de Actualización";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Div_Busqueda.Visible = false;
                    Btn_Imprimir.Visible = false;
                    Btn_Generar_Reporte_Cambios.Visible = false;
                    Btn_Lanzar_Buscar_Producto.Visible = false;
                    Cmb_Marca.Enabled = true;
                    Txt_Modelo.Enabled = true;
                    Cmb_Material.Enabled = true;
                    Cmb_Gerencias.Enabled = true;
                    Cmb_Dependencias.Enabled = true;
                    Cmb_Empleados.Enabled = true;
                    Btn_Busqueda_Avanzada_Resguardante.Visible = true;
                    Btn_Agregar_Resguardante.Visible = true;
                    Btn_Quitar_Resguardante.Visible = true;
                    Grid_Resguardantes.Enabled = true;
                    Cmb_Color.Enabled = true;
                    Txt_Costo.Enabled = true;
                    Txt_Cantidad.Enabled = true;
                    Btn_Fecha_Adquisicion.Enabled = true;
                    Txt_Numero_Serie.Enabled = true;
                    Cmb_Estado.Enabled = true;
                    Cmb_Estatus.Enabled = true;
                    Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                    Txt_Comentarios.Enabled = true;
                    Txt_Motivo_Baja.Enabled = true;
                } else {
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevO.png";
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Div_Busqueda.Visible = true;
                    Btn_Imprimir.Visible = true;
                    Btn_Generar_Reporte_Cambios.Visible = true;
                    Btn_Lanzar_Buscar_Producto.Visible = false;
                    Cmb_Marca.Enabled = false;
                    Txt_Modelo.Enabled = false;
                    Cmb_Material.Enabled = false;
                    Cmb_Gerencias.Enabled = false;
                    Cmb_Dependencias.Enabled = false;
                    Cmb_Color.Enabled = false;
                    Txt_Costo.Enabled = false;
                    Cmb_Empleados.Enabled = false;
                    Txt_Cantidad.Enabled = false;
                    Btn_Busqueda_Avanzada_Resguardante.Visible = false;
                    Btn_Agregar_Resguardante.Visible = false;
                    Btn_Quitar_Resguardante.Visible = false;
                    Grid_Resguardantes.Enabled = false;
                    Btn_Fecha_Adquisicion.Enabled = false;
                    Txt_Numero_Serie.Enabled = false;
                    Cmb_Estado.Enabled = false;
                    Cmb_Estatus.Enabled = false;
                    Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                    Txt_Comentarios.Enabled = false;
                    Txt_Motivo_Baja.Enabled = false;
                    Cmb_Estatus_SelectedIndexChanged(Cmb_Estatus, null);
                }
            }

        #endregion

        #region "Validaciones"
            
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Bien
            ///DESCRIPCIÓN          : Valida el Bien que se dara de Alta.
            ///PARAMETROS           : 
            ///CREO                 : Francisco Antonio Gallardo Castañeda
            ///FECHA_CREO           : 11/Noviembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public Boolean Validar_Bien() { 
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Hdf_Producto_ID.Value.Trim().Length == 0) {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Producto.";
                    Validacion = false;
                }
                if (Cmb_Gerencias.SelectedIndex == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una Gerencia.";
                    Validacion = false;
                }
                if (Cmb_Dependencias.SelectedIndex == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una Unidad Responsable.";
                    Validacion = false;
                }
                if (Cmb_Marca.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar una Marca.";
                    Validacion = false;
                }
                if (Txt_Modelo.Text.Trim().Length == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Indroducir el Modelo.";
                    Validacion = false;
                }
                if (Cmb_Material.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar un Material.";
                    Validacion = false;
                }
                if (Cmb_Color.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar un Color.";
                    Validacion = false;
                }
                if (Txt_Costo.Text.Trim().Length == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Indroducir el Costo.";
                    Validacion = false;
                }
                //if (Txt_Fecha_Adquisicion.Text.Trim().Length == 0) {
                //    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                //    Mensaje_Error = Mensaje_Error + "+ Indroducir la Fecha de Adquisición.";
                //    Validacion = false;
                //}
                if (Txt_Numero_Serie.Text.Trim().Length == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Indroducir el Número de Serie.";
                    Validacion = false;
                }
                if (Txt_Cantidad.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Indroducir la Cantidad.";
                    Validacion = false;
                }
                if (Cmb_Estado.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar un Estado.";
                    Validacion = false;
                } 
                if (Cmb_Estatus.SelectedIndex == 0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar un Estatus.";
                    Validacion = false;
                }
                if (Txt_Motivo_Baja.Visible && Txt_Motivo_Baja.Text.Trim().Length==0) {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir el Motivo de la Baja.";
                    Validacion = false;
                }
                if (Cmb_Estatus.SelectedItem.Value.Trim().Equals("VIGENTE"))
                {
                    if (Grid_Resguardantes.Rows.Count == 0)
                    {
                        if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
                        Mensaje_Error = Mensaje_Error + "+ Debe tener un resguardante como minimo.";
                        Validacion = false;
                    }
                }
                if (!Validacion) {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Validar_Componentes_Resguardos
            ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
            ///             una operación de la pestaña de Resguardos.
            ///PROPIEDADES:     
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Validar_Componentes_Resguardos()
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                String Mensaje_Error = "";
                Boolean Validacion = true;
                if (Cmb_Empleados.SelectedIndex == 0)
                {
                    Mensaje_Error = Mensaje_Error + "+ Seleccionar el Empleado para Resguardo.";
                    Validacion = false;
                }
                if (!Validacion)
                {
                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
                    Div_Contenedor_Msj_Error.Visible = true;
                }
                return Validacion;
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Buscar_Clave_DataTable
            ///DESCRIPCIÓN: Busca una Clave en un DataTable, si la encuentra Retorna 'true'
            ///             en caso contrario 'false'.
            ///PROPIEDADES:  
            ///             1.  Clave.  Clave que se buscara en el DataTable
            ///             2.  Tabla.  Datatable donde se va a buscar la clave.
            ///             3.  Columna.Columna del DataTable donde se va a buscar la clave.
            ///CREO: Francisco Antonio Gallardo Castañeda.
            ///FECHA_CREO: 29/Noviembre/2010 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            private Boolean Buscar_Clave_DataTable(String Clave, DataTable Tabla, Int32 Columna)
            {
                Boolean Resultado_Busqueda = false;
                if (Tabla != null && Tabla.Rows.Count > 0 && Tabla.Columns.Count > 0)
                {
                    if (Tabla.Columns.Count > Columna)
                    {
                        for (Int32 Contador = 0; Contador < Tabla.Rows.Count; Contador++)
                        {
                            if (Tabla.Rows[Contador][Columna].ToString().Trim().Equals(Clave.Trim()))
                            {
                                Resultado_Busqueda = true;
                                break;
                            }
                        }
                    }
                }
                return Resultado_Busqueda;
            }

        #endregion

    #endregion

    #region "Grids"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los Productos
        ///PROPIEDADES:     
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 18/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Productos_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            try {
                Llenar_MPE_Listado_Productos(e.NewPageIndex);
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Productos del
        ///             Modal de Busqueda.
        ///PROPIEDADES:     
        ///CREO     : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO   : 18/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Productos_SelectedIndexChanged(object sender, EventArgs e) {
            try {
                if (Grid_Listado_Productos.SelectedIndex > (-1)) {
                    String Producto_ID = Grid_Listado_Productos.SelectedRow.Cells[1].Text.Trim();
                    Cls_Cat_Com_Productos_Negocio Producto_Negocio = new Cls_Cat_Com_Productos_Negocio();
                    Producto_Negocio.P_Producto_ID = Producto_ID;
                    Producto_Negocio.P_Estatus = "ACTIVO";
                    DataTable Dt_Productos = Producto_Negocio.Consulta_Datos_Producto();
                    if (Dt_Productos != null && Dt_Productos.Rows.Count > 0) {
                        Hdf_Producto_ID.Value = Dt_Productos.Rows[0][Cat_Com_Productos.Campo_Producto_ID].ToString();
                        Txt_Nombre_Bien.Text = Dt_Productos.Rows[0][Cat_Com_Productos.Campo_Nombre].ToString().Trim();
                    }
                    Mpe_Productos_Cabecera.Hide();
                    Grid_Listado_Productos.SelectedIndex = (-1);
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Resguardantes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de los reguardantesa
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 17/Marzo/2011
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Resguardantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                DataTable Tabla = new DataTable();
                if (Session["Dt_Resguardantes_BM"] != null)
                {
                    Tabla = (DataTable)Session["Dt_Resguardantes_BM"];
                }
                Llenar_Grid_Resguardantes(e.NewPageIndex, Tabla);
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
                {
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                    Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                    DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                    String Dependencia_ID = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString() : null;
                    if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
                    {
                        
                        String Gerencia_ID = Ubicar_Gerencia(Dependencia_ID);
                        if (!String.IsNullOrEmpty(Gerencia_ID)) {
                            if (!Gerencia_ID.Trim().Equals(Cmb_Gerencias.SelectedItem.Value))
                            {
                                Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Gerencia_ID));//Se coloca el combo gerencia en la posicion encontrada encontrada
                                Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue);
                                Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Dependencia_ID));
                                Consultar_Empleados(Dependencia_ID, ref Cmb_Empleados);
                            }
                            else
                            {
                                if (!Dependencia_ID.Trim().Equals(Cmb_Dependencias.SelectedItem.Value))
                                {
                                    Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Dependencia_ID));
                                    Consultar_Empleados(Dependencia_ID, ref Cmb_Empleados);
                                }
                            }
                        }
                        Cmb_Empleados.SelectedIndex = Cmb_Empleados.Items.IndexOf(Cmb_Empleados.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));

                    }

                    MPE_Resguardante.Hide();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Bienes_Bien_Mueble_PageIndexChanging
        ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
        ///             de empleados.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Bienes_Bien_Mueble_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Grid_Listado_Bienes_Bien_Mueble.PageIndex = e.NewPageIndex;
                Llenar_Grid_Busqueda_Bienes_Economicos();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Bienes_Bien_Mueble_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
        ///             de bienes.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Listado_Bienes_Bien_Mueble_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Listado_Bienes_Bien_Mueble.SelectedIndex > (-1))
                {
                    String Clave = (HttpUtility.HtmlDecode(Grid_Listado_Bienes_Bien_Mueble.SelectedRow.Cells[1].Text)).Trim();
                    Cls_Ope_Pat_Bienes_Economicos_Negocio BE_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                    BE_Negocio.P_Bien_ID = Int32.Parse(Clave);
                    BE_Negocio = BE_Negocio.Consultar_Detalles_Bien_Economico();
                    Mostrar_Detalles_Bien(BE_Negocio);
                    Grid_Listado_Bienes_Bien_Mueble.SelectedIndex = (-1);
                    MPE_Busqueda_Bien.Hide();
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Historial_Resguardantes_PageIndexChanging
        ///DESCRIPCIÓN: Maneja la paginación del GridView de Historial de Resguardos
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 13/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Historial_Resguardantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                if (Session["Dt_Historial_Resguardos"] != null)
                {
                    Grid_Historial_Resguardantes.SelectedIndex = (-1);
                    Llenar_Grid_Historial_Resguardos(e.NewPageIndex, (DataTable)Session["Dt_Historial_Resguardos"]);
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Historial_Resguardantes_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Historial
        ///             de Resguardos.
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 13/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Grid_Historial_Resguardantes_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                if (Grid_Historial_Resguardantes.SelectedIndex > (-1))
                {
                    Limpiar_Historial_Resguardantes();
                    if (Session["Dt_Historial_Resguardos"] != null)
                    {
                        Int32 Registro = ((Grid_Historial_Resguardantes.PageIndex) * Grid_Historial_Resguardantes.PageSize) + (Grid_Historial_Resguardantes.SelectedIndex);
                        DataTable Tabla = (DataTable)Session["Dt_Historial_Resguardos"];
                        Txt_Historial_Empleado_Resguardo.Text = "[" + Tabla.Rows[Registro]["NO_EMPLEADO"].ToString().Trim() + "] " + Tabla.Rows[Registro]["NOMBRE_EMPLEADO"].ToString().Trim();
                        Txt_Historial_Comentarios_Resguardo.Text = Tabla.Rows[Registro]["COMENTARIOS"].ToString().Trim();
                        Txt_Historial_Fecha_Inicial_Resguardo.Text = String.Format("{0:dd/MMM/yyyy}", Tabla.Rows[Registro]["FECHA_INICIAL"]);
                        Txt_Historial_Fecha_Final_Resguardo.Text = String.Format("{0:dd/MMM/yyyy}", Tabla.Rows[Registro]["FECHA_FINAL"]);
                    }
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

    #region "Eventos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias_SelectedIndexChanged
        ///DESCRIPCIÓN          : Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias_SelectedIndexChanged
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 17/Mayo/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            String Dependencia_ID = Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.SelectedItem.Value;
            Consultar_Empleados(Dependencia_ID, ref Cmb_Busqueda_Bien_Mueble_Nombre_Resguardante);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Imprimir_Click
        ///DESCRIPCIÓN          : Btn_Imprimir_Click
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 17/Mayo/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {
            if (!String.IsNullOrEmpty(Hdf_Bien_ID.Value))
            {
                Cls_Alm_Com_Resguardos_Negocio Consulta_Resguardos_Negocio = new Cls_Alm_Com_Resguardos_Negocio();
                Consulta_Resguardos_Negocio.P_Bien_ID = Hdf_Bien_ID.Value.Trim();
                DataTable Dt_Bienes = Consulta_Resguardos_Negocio.Consulta_Resguardos_Bienes_Economicos();
                Dt_Bienes.TableName = "DT_PRINCIPAL";
                Ds_Pat_Resguardo_Economico Ds_Consulta_Resguardos_Bienes = new Ds_Pat_Resguardo_Economico();
                Generar_Reporte(Dt_Bienes, Ds_Consulta_Resguardos_Bienes, "PDF");
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Es necesario seleccionar el Bien.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
        ///DESCRIPCIÓN:          Caraga el data set fisoco con el cual se genera el Reporte especificado
        ///PARAMETROS:           1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
        ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
        ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO:           15/Diciembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private void Generar_Reporte(DataTable Data_Set_Consulta_DB, DataSet Ds_Reporte, String Formato)
        {
            try
            {
                // Ruta donde se encuentra el reporte Crystal
                String Ruta_Reporte_Crystal = "../Rpt/Compras/Rpt_Pat_Resguardo_Economico.rpt";
                String Nombre_Reporte_Generar = "";

                Ds_Reporte.Tables["DT_PRINCIPAL"].Merge(Data_Set_Consulta_DB);

                // Se da el nombre del reporte que se va generar
                if (Formato == "PDF")
                    Nombre_Reporte_Generar = "Rpt_Pat_Resguardos_Economicos" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";  // Es el nombre del reporte PDF que se va a generar
                else if (Formato == "Excel")
                    Nombre_Reporte_Generar = "Rpt_Pat_Resguardos_Economicos" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";  // Es el nombre del repote en Excel que se va a generar

                Cls_Reportes Reportes = new Cls_Reportes();
                Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Datos_Bien_Mueble_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Buscar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Datos_Bien_Mueble_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Grid_Listado_Bienes_Bien_Mueble.PageIndex = 0;
                Session["TIPO_BUSQUEDA_BIENES"] = "GENERAL";
                Llenar_Grid_Busqueda_Bienes_Economicos();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Buscar_Resguardante_Bien_Mueble_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Buscar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Buscar_Resguardante_Bien_Mueble_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Grid_Listado_Bienes_Bien_Mueble.PageIndex = 0;
                Session["TIPO_BUSQUEDA_BIENES"] = "RESGUARDATARIOS";
                Llenar_Grid_Busqueda_Bienes_Economicos();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Busqueda_Directa_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Buscar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 16/Mayo/2013 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Busqueda_Directa_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                if (Txt_Busqueda_Clave.Text.Trim().Length > 0)
                {
                    Cls_Ope_Pat_Bienes_Economicos_Negocio BE_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                    BE_Negocio.P_Bien_ID = Convert.ToInt32(Txt_Busqueda_Clave.Text.Trim());
                    BE_Negocio = BE_Negocio.Consultar_Detalles_Bien_Economico();
                    if (BE_Negocio.P_Bien_ID > 0)
                    {
                        Mostrar_Detalles_Bien(BE_Negocio);
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                        Lbl_Mensaje_Error.Text = "No se encontro el Bien con la Clave: " + Txt_Busqueda_Clave.Text.Trim();
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Introducir la Clave del Bien a Buscar.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Nuevo.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
            try { 
                if (Btn_Nuevo.AlternateText.Trim().Equals("Nuevo")) {
                    Limpiar_Campos_Generales();
                    Habilitacion_Componentes("NUEVO");
                } else {
                    if (Validar_Bien()) {
                        Cls_Ope_Pat_Bienes_Economicos_Negocio Bien_Mueble = Alta_Bien();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Alta Exitosa!!!');", true);
                        Habilitacion_Componentes("");
                        Bien_Mueble = Bien_Mueble.Consultar_Detalles_Bien_Economico();
                        Mostrar_Detalles_Bien(Bien_Mueble);
                    }       
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Excepción al dar de alta el bien.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Modificar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Modificar.AlternateText.Trim().Equals("Modificar")) {
                if (!String.IsNullOrEmpty(Hdf_Bien_ID.Value)) {
                    if (!Cmb_Estatus.SelectedItem.Value.Equals("DEFINITIVA"))
                    {
                        Habilitacion_Componentes("MODIFICAR");
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                        Lbl_Mensaje_Error.Text = "No puede actualizarse un bien dado de Baja definitivamente.";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                } else {
                    Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                    Lbl_Mensaje_Error.Text = "Es necesario seleccionar el Bien a Modificar.";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            } else {
                if (Validar_Bien()) {
                    Modificar_Bien();
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Actualización Exitosa!!!');", true);
                    String Bien_ID = Hdf_Bien_ID.Value.Trim();
                    Limpiar_Campos_Generales();
                    Habilitacion_Componentes("");
                    Cls_Ope_Pat_Bienes_Economicos_Negocio BSI_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                    BSI_Negocio.P_Bien_ID = Convert.ToInt32(Bien_ID);
                    BSI_Negocio = BSI_Negocio.Consultar_Detalles_Bien_Economico();
                    Mostrar_Detalles_Bien(BSI_Negocio);
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
        ///DESCRIPCIÓN          : Evento Click del Botón Salir.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 04/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            if (Btn_Salir.AlternateText.Trim().Equals("Salir")) {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            } else {
                String Bien_Cargardo = null;
                if (Hdf_Bien_ID.Value != null && Hdf_Bien_ID.Value.Trim().Length > 0) {
                    Bien_Cargardo = Hdf_Bien_ID.Value.Trim();
                }
                Limpiar_Campos_Generales();
                Habilitacion_Componentes("");
                if (Bien_Cargardo != null) {
                    Cls_Ope_Pat_Bienes_Economicos_Negocio BSI_Negocio = new Cls_Ope_Pat_Bienes_Economicos_Negocio();
                    BSI_Negocio.P_Bien_ID = Convert.ToInt32(Bien_Cargardo);
                    BSI_Negocio = BSI_Negocio.Consultar_Detalles_Bien_Economico();
                    Mostrar_Detalles_Bien(BSI_Negocio);
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cmb_Estatus_SelectedIndexChanged
        ///DESCRIPCIÓN          : Maneja el evento del Combo de Estatus.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 11/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Cmb_Estatus_SelectedIndexChanged(object sender, EventArgs e) {
            if (Cmb_Estatus.SelectedIndex > 1) {
                Lbl_Motivo_Baja.Visible = true;
                Txt_Motivo_Baja.Visible = true;
                Grid_Resguardantes.DataSource = new DataTable();
                Grid_Resguardantes.DataBind();
                Cmb_Empleados.Enabled = false;
                Btn_Agregar_Resguardante.Visible = false;
                Btn_Quitar_Resguardante.Visible = false;
                Btn_Busqueda_Avanzada_Resguardante.Visible = false;
            } else {
                Lbl_Motivo_Baja.Visible = false;
                Txt_Motivo_Baja.Visible = false;
                if (Session["Dt_Resguardantes_BM"] != null)
                    Llenar_Grid_Resguardantes(0, ((DataTable)Session["Dt_Resguardantes_BM"]));
                if (Cmb_Estatus.Enabled) { 
                    Cmb_Empleados.Enabled = true;
                    Btn_Agregar_Resguardante.Visible = true;
                    Btn_Quitar_Resguardante.Visible = true;
                    Btn_Busqueda_Avanzada_Resguardante.Visible = true;                
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Btn_Lanzar_Buscar_Producto_Click
        ///DESCRIPCIÓN          : Busca el Producto a agregar.
        ///PARAMETROS           : 
        ///CREO                 : Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO           : 18/Noviembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Btn_Lanzar_Buscar_Producto_Click(object sender, ImageClickEventArgs e)  {
            try {
                Mpe_Productos_Cabecera.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_Productos_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda de los productos.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 18/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ejecutar_Busqueda_Productos_Click(object sender, ImageClickEventArgs e) {
            try {
                Llenar_MPE_Listado_Productos(0);
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Lnk_Busqueda_Avanzada_Click
        ///DESCRIPCIÓN: Lanza la Ventana de la busqueda de Bienes Sin Inventario.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 21/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Lnk_Busqueda_Avanzada_Click(object sender, EventArgs e) {
            try {
                MPE_Busqueda_Bien.Show();
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Excepción.";
                Lbl_Mensaje_Error.Text = "Ex:['" + Ex.Message + "']";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_Bienes_Sin_Inventario_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda de los Bienes Sin Inventario.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 22/Noviembre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ejecutar_Busqueda_Bienes_Sin_Inventario_Click(object sender, ImageClickEventArgs e) {
            try { 

            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Grencias_SelectedIndexChanged
        ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Gerencias
        ///PROPIEDADES:     
        ///CREO: Luis Daniel Guzmán Malagón.
        ///FECHA_CREO: 30/Octubre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue.ToString());
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cmb_Dependencias_SelectedIndexChanged
        ///DESCRIPCIÓN:          Evento utilizado para obtener el identificador de la dependencia que se selecciono y acceder al metodo para llenar el combo de los empleados
        ///PROPIEDADES:     
        ///                      1.  Dependencia_ID. el identificador de la dependencia en base a la que se va hacer la consulta
        ///              
        ///CREO:                 Salvador Hernández Ramírez
        ///FECHA_CREO: 02/Febrero/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Cmb_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
        {
            String Depencencia_Id = Cmb_Dependencias.SelectedItem.Value.Trim();
            Consultar_Empleados(Depencencia_Id, ref Cmb_Empleados);
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Resguardante_Click
        ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Avanzada_Resguardante_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                MPE_Resguardante.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Resguardante_Click
        ///DESCRIPCIÓN: Agrega una nuevo Empleado Resguardante para este Bien Mueble.
        ///             (No aun en la Base de Datos)
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 29/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO: Salvador Hernàndez Ramírez
        ///CAUSA_MODIFICACIÓN: El DataGrid debe mostrar: Numero de Empleado, Clave del Producto
        ///Nombre de la Dependencia, Nombre del Area Y Comentarios
        ///*******************************************************************************
        protected void Btn_Agregar_Resguardante_Click(object sender, ImageClickEventArgs e)
        {
            if (Validar_Componentes_Resguardos())
            {
                DataTable Tabla = (DataTable)Grid_Resguardantes.DataSource;
                if (Tabla == null)
                {
                    if (Session["Dt_Resguardantes_BM"] == null)
                    {
                        Tabla = new DataTable("Resguardos");
                        Tabla.Columns.Add("BIEN_RESGUARDO_ID", Type.GetType("System.Int32"));
                        Tabla.Columns.Add("EMPLEADO_ALMACEN_ID", Type.GetType("System.String"));
                        Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                        Tabla.Columns.Add("NO_EMPLEADO", Type.GetType("System.String"));
                        Tabla.Columns.Add("NOMBRE_EMPLEADO", Type.GetType("System.String"));
                        Tabla.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                    }
                    else
                    {
                        Tabla = (DataTable)Session["Dt_Resguardantes_BM"];
                        if (Tabla.Columns.Count == 0)
                        {
                            Tabla = new DataTable("Resguardos");
                            Tabla.Columns.Add("BIEN_RESGUARDO_ID", Type.GetType("System.Int32"));
                            Tabla.Columns.Add("EMPLEADO_ALMACEN_ID", Type.GetType("System.String"));
                            Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                            Tabla.Columns.Add("NO_EMPLEADO", Type.GetType("System.String"));
                            Tabla.Columns.Add("NOMBRE_EMPLEADO", Type.GetType("System.String"));
                            Tabla.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                        }
                    }
                }
                if (!Buscar_Clave_DataTable(Cmb_Empleados.SelectedItem.Value, Tabla, 1))
                {
                    Cls_Cat_Empleados_Negocios Empleados_Negocio = new Cls_Cat_Empleados_Negocios();
                    Empleados_Negocio.P_Empleado_ID = Cmb_Empleados.SelectedItem.Value;
                    DataTable Dt_Empleado = Empleados_Negocio.Consulta_Datos_Empleado();
                    if (Dt_Empleado != null && Dt_Empleado.Rows.Count > 0)
                    {
                        DataRow Fila = Tabla.NewRow();
                        Fila["BIEN_RESGUARDO_ID"] = 0;
                        Fila["EMPLEADO_ALMACEN_ID"] = HttpUtility.HtmlDecode(Cls_Sessiones.Empleado_ID); // Se debe realizar una consulta para obtenerlo
                        Fila["EMPLEADO_ID"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim();
                        Fila["NO_EMPLEADO"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim();
                        Fila["NOMBRE_EMPLEADO"] = HttpUtility.HtmlDecode(Cmb_Empleados.SelectedItem.Text);
                        Fila["COMENTARIOS"] = HttpUtility.HtmlDecode(Txt_Cometarios.Text.Trim());
                        Tabla.Rows.Add(Fila);
                    }
                    Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
                    Grid_Resguardantes.SelectedIndex = (-1);
                    Cmb_Empleados.SelectedIndex = 0;
                    Txt_Cometarios.Text = "";
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Empleado ya esta Agregado.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Resguardante_Click
        ///DESCRIPCIÓN: Quita un Empleado resguardante para este bien (No en la Base de datos
        ///             aun).
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 29/Noviembre/2010 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Quitar_Resguardante_Click(object sender, ImageClickEventArgs e)
        {
            if (Grid_Resguardantes.Rows.Count > 0 && Grid_Resguardantes.SelectedIndex > (-1))
            {
                Int32 Registro = ((Grid_Resguardantes.PageIndex) * Grid_Resguardantes.PageSize) + (Grid_Resguardantes.SelectedIndex);
                if (Session["Dt_Resguardantes_BM"] != null)
                {
                    DataTable Tabla = (DataTable)Session["Dt_Resguardantes_BM"];
                    Tabla.Rows.RemoveAt(Registro);
                    Session["Dt_Resguardantes_BM"] = Tabla;
                    Grid_Resguardantes.SelectedIndex = (-1);
                    Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Quitar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
        {
            try
            {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }
            catch (Exception Ex)
            {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Generar_Reporte_Cambios_Click(object sender, ImageClickEventArgs e)
        {
            if (!String.IsNullOrEmpty(Hdf_Bien_ID.Value))
            {
                Generar_Reporte_Cambios();
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "No hay Bien Seleccionado";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion

}