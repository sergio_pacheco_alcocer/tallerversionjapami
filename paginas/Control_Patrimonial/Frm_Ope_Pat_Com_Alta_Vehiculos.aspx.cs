﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Almacen_Resguardos.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Donadores.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Partes_Vehiculos.Negocio;
using System.Collections.Generic;
using System.IO;
using AjaxControlToolkit;
using JAPAMI.Catalogo_Compras_Productos.Negocio;
using JAPAMI.Reportes;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using System.Security.Cryptography;
using JAPAMI.Control_Patrimonial_Reporte_Listado_Bienes.Negocio;

public partial class paginas_predial_Frm_Ope_Pat_Com_Alta_Vehiculos : System.Web.UI.Page
{

    #region Variables Internas

    String Fecha_Adquisicion = null;
    private static AsyncFileUpload Archivo;

    #endregion

    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Page_Load(object sender, EventArgs e) {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        Div_Contenedor_Msj_Error.Visible = false;
        if (!IsPostBack) {
            Configuracion_Formulario(true, "VEHICULOS");
            Llenar_Combos();
            Llenar_Combo_Procedencias();
            Llenar_Grid_Proveedores(0);
            Div_Generales_Otra_Procedencia.Visible = true;
            Lbl_Capacidad_Carga.Visible = false;
            Txt_Capacidad_Carga.Visible = false;
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Reporte_Codigo_Barras
    ///DESCRIPCIÓN: Genera el reporte de Codigo de Barras.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Crear_Reporte_Codigo_Barras(String No_Inventario)
    {
        DataTable Dt_Datos = new DataTable("DT_DATOS");
        Dt_Datos.Columns.Add("Numero_Inventario", Type.GetType("System.String"));
        Dt_Datos.Columns.Add("Numero_Inventario_Codigo", Type.GetType("System.String"));
        Dt_Datos.Columns.Add("Numero_Inventario_Imagen", Type.GetType("System.Byte[]"));
        DataRow Fila = Dt_Datos.NewRow();
        Fila["Numero_Inventario"] = No_Inventario.Trim();
        Fila["Numero_Inventario_Codigo"] = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.code128(No_Inventario.Trim());
        Fila["Numero_Inventario_Imagen"] = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.Imagen_Codigo_Barras(No_Inventario.Trim());
        Dt_Datos.Rows.Add(Fila);
        DataSet Ds_Consulta = new DataSet();
        Ds_Consulta.Tables.Add(Dt_Datos);
        Ds_Rpt_Pat_Codigo_Barras Ds_Reporte = new Ds_Rpt_Pat_Codigo_Barras();
        Generar_Reporte_Codigo_Barras(Ds_Consulta, Ds_Reporte, "Rpt_Pat_Codigo_Barras.rpt");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Reporte_Codigo_Barras
    ///DESCRIPCIÓN: Genera el reporte de Codigo de Barras.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Generar_Reporte_Codigo_Barras(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, String Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Nombre_Reporte_Generar = "Rpt_Pat_Cod_Bar_" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
        String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Proveedores
    ///DESCRIPCIÓN:      Llena el Grid de los Proveedores para que el usuario lo seleccione
    ///PARAMETROS:       Pagina. Pagina del Grid que se mostrará.     
    ///CREO:             Salvador Hernández Ramírez
    ///FECHA_CREO:       08/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Proveedores(Int32 Pagina)
    {
        Grid_Listado_Proveedores.SelectedIndex = (-1);
        Grid_Listado_Proveedores.Columns[1].Visible = true;
        Cls_Cat_Com_Proveedores_Negocio Proveedores_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        if (Txt_Nombre_Proveedor_Buscar.Text.Trim() != "")
        {
            Proveedores_Negocio.P_Busqueda = Txt_Nombre_Proveedor_Buscar.Text.Trim();
        }
        DataTable Dt_Proveedores = Proveedores_Negocio.Consulta_Datos_Proveedores();
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Proveedor_ID].ColumnName = "PROVEEDOR_ID";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Nombre].ColumnName = "NOMBRE";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_RFC].ColumnName = "RFC";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Compañia].ColumnName = "COMPANIA";
        Grid_Listado_Proveedores.DataSource = Dt_Proveedores;
        Grid_Listado_Proveedores.PageIndex = Pagina;
        Grid_Listado_Proveedores.DataBind();
        Grid_Listado_Proveedores.Columns[1].Visible = false;
    }
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
    ///DESCRIPCIÓN: Llena el combo de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados(DataTable Tabla)
    {
        try
        {
            DataRow Fila_Empleado = Tabla.NewRow();
            Fila_Empleado["EMPLEADO_ID"] = "SELECCIONE";
            Fila_Empleado["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Tabla.Rows.InsertAt(Fila_Empleado, 0);
            Cmb_Empleados.DataSource = Tabla;
            Cmb_Empleados.DataValueField = "EMPLEADO_ID";
            Cmb_Empleados.DataTextField = "NOMBRE";
            Cmb_Empleados.DataBind();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
    ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias(String Gerencia_ID) {
        Cls_Ope_Pat_Com_Vehiculos_Negocio Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Gerencia_ID = ((!String.IsNullOrEmpty(Gerencia_ID)) ? Gerencia_ID : "00000").Trim();
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";
        DataTable Dependencias = Combos.Consultar_DataTable();
        DataRow Fila_Dependencia = Dependencias.NewRow();//Se agrega el renglón <SELECCIONE> en la tabla
        Fila_Dependencia["DEPENDENCIA_ID"] = "SELECCIONE";//Se especifica el valor del renglón en la columna ""DEPENDENCIA_ID"
        Fila_Dependencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");//Se especifica el texto del renglón en la columna "NOMBRE"
        Dependencias.Rows.InsertAt(Fila_Dependencia, 0);//Se inserta el renglón en la posición 0 de la tabla
        Cmb_Dependencias.DataSource = Dependencias;//Se especifica la fuente de información que llenará el combo
        Cmb_Dependencias.DataValueField = "DEPENDENCIA_ID";//Se especifica el campo de la tabla dependencias que contendrá el valor de los renglones del combo
        Cmb_Dependencias.DataTextField = "NOMBRE";//Se especifica el campo de la tabla dependencias que contendrá el texto que mostrará el combo
        Cmb_Dependencias.DataBind();//Se unen los datos del combo
        Dependencias.Rows.RemoveAt(0);//Se elimina la posición 0 de la tabla
        //Llenar combo dependencias de la búsqueda avanzada
        Cmb_Busqueda_Dependencia.DataSource = Dependencias;
        Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
        Cmb_Busqueda_Dependencia.DataTextField = "NOMBRE";
        Cmb_Busqueda_Dependencia.DataBind();
        Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< TODAS >", ""));

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combos
    ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combos()
    {
        try
        {
            Cls_Ope_Pat_Com_Vehiculos_Negocio Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
            //SE LLENA EL COMBO DE GERENCIAS
            Combos.P_Tipo_DataTable = "GERENCIAS";
            DataTable Gerencias = Combos.Consultar_DataTable();
            DataRow Fila_Gerencia = Gerencias.NewRow();
            Fila_Gerencia["GERENCIA_ID"] = "";
            Fila_Gerencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Gerencias.Rows.InsertAt(Fila_Gerencia, 0);
            Cmb_Gerencias.DataSource = Gerencias;
            Cmb_Gerencias.DataValueField = "GERENCIA_ID";
            Cmb_Gerencias.DataTextField = "NOMBRE";
            Cmb_Gerencias.DataBind();
            Gerencias.Rows.RemoveAt(0);

            Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
            //SE LLENA EL COMBO DE GERENCIAS
            Combos.P_Tipo_DataTable = "DEPENDENCIAS";
            DataTable Dependencias = Combos.Consultar_DataTable();
            DataRow Fila_Dependencia = Dependencias.NewRow();
            Fila_Dependencia["DEPENDENCIA_ID"] = "";
            Fila_Dependencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Dependencias.Rows.InsertAt(Fila_Dependencia, 0);
            Cmb_Busqueda_Dependencia.DataSource = Dependencias;
            Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencia.DataTextField = "NOMBRE";
            Cmb_Busqueda_Dependencia.DataBind();
            Dependencias.Rows.RemoveAt(0);
           

            //SE LLENA EL COMBO DE TIPOS VEHICULOS
            Combos.P_Tipo_DataTable = "TIPOS_VEHICULOS";
            DataTable Tipos_Vehiculos = Combos.Consultar_DataTable();
            DataRow Fila_Tipo_Vehiculo = Tipos_Vehiculos.NewRow();
            Fila_Tipo_Vehiculo["TIPO_VEHICULO_ID"] = "SELECCIONE";
            Fila_Tipo_Vehiculo["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Tipos_Vehiculos.Rows.InsertAt(Fila_Tipo_Vehiculo, 0);
            Cmb_Tipos_Vehiculos.DataSource = Tipos_Vehiculos;
            Cmb_Tipos_Vehiculos.DataValueField = "TIPO_VEHICULO_ID";
            Cmb_Tipos_Vehiculos.DataTextField = "DESCRIPCION";
            Cmb_Tipos_Vehiculos.DataBind();

            //SE LLENA EL COMBO DE TIPOS COMBUSTIBLE
            Combos.P_Tipo_DataTable = "TIPOS_COMBUSTIBLE";
            DataTable Tipos_Combustible = Combos.Consultar_DataTable();
            DataRow Fila_Tipo_Combustible = Tipos_Combustible.NewRow();
            Fila_Tipo_Combustible["TIPO_COMBUSTIBLE_ID"] = "SELECCIONE";
            Fila_Tipo_Combustible["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Tipos_Combustible.Rows.InsertAt(Fila_Tipo_Combustible, 0);
            Cmb_Tipo_Combustible.DataSource = Tipos_Combustible;
            Cmb_Tipo_Combustible.DataValueField = "TIPO_COMBUSTIBLE_ID";
            Cmb_Tipo_Combustible.DataTextField = "DESCRIPCION";
            Cmb_Tipo_Combustible.DataBind();

            //SE LLENA EL COMBO DE ZONAS
            Combos.P_Tipo_DataTable = "ZONAS";
            DataTable Zonas = Combos.Consultar_DataTable();
            DataRow Fila_Zona = Zonas.NewRow();
            Fila_Zona["ZONA_ID"] = "SELECCIONE";
            Fila_Zona["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Zonas.Rows.InsertAt(Fila_Zona, 0);
            Cmb_Zonas.DataSource = Zonas;
            Cmb_Zonas.DataTextField = "DESCRIPCION";
            Cmb_Zonas.DataValueField = "ZONA_ID";
            Cmb_Zonas.DataBind();

            //SE LLENA EL COMBO DE MARCAS
            Combos.P_Tipo_DataTable = "MARCAS";
            DataTable Marcas = Combos.Consultar_DataTable();
            DataRow Fila_Marca = Marcas.NewRow();
            Fila_Marca["MARCA_ID"] = "SELECCIONE";
            Fila_Marca["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Marcas.Rows.InsertAt(Fila_Marca, 0);
            Cmb_Marca.DataSource = Marcas;
            Cmb_Marca.DataTextField = "NOMBRE";
            Cmb_Marca.DataValueField = "MARCA_ID";
            Cmb_Marca.DataBind();

            //SE LLENA EL COMBO DE MATERIALES
            Combos.P_Tipo_DataTable = "MATERIALES";
            DataTable Materiales = Combos.Consultar_DataTable();
            DataRow Fila_Material = Materiales.NewRow();
            Fila_Material["MATERIAL_ID"] = "SELECCIONE";
            Fila_Material["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Materiales.Rows.InsertAt(Fila_Material, 0);

            //SE LLENA LOS COMBOS DE COLORES
            Combos.P_Tipo_DataTable = "COLORES";
            DataTable Colores = Combos.Consultar_DataTable();
            DataRow Fila_Color = Colores.NewRow();
            Fila_Color["COLOR_ID"] = "SELECCIONE";
            Fila_Color["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Colores.Rows.InsertAt(Fila_Color, 0);
            Cmb_Colores.DataSource = Colores;
            Cmb_Colores.DataValueField = "COLOR_ID";
            Cmb_Colores.DataTextField = "DESCRIPCION";
            Cmb_Colores.DataBind();

            Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            CA_Negocio.P_Estatus = "VIGENTE";
            CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
            CA_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
            Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
            Cmb_Clase_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Clase_Activo.DataBind();
            Cmb_Clase_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));

            Cls_Cat_Pat_Com_Clasificaciones_Negocio Clasificaciones_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            Clasificaciones_Negocio.P_Estatus = "VIGENTE";
            Clasificaciones_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
            Clasificaciones_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Tipo_Activo.DataSource = Clasificaciones_Negocio.Consultar_DataTable();
            Cmb_Tipo_Activo.DataValueField = "CLASIFICACION_ID";
            Cmb_Tipo_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Tipo_Activo.DataBind();
            Cmb_Tipo_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Procedencias
    ///DESCRIPCIÓN: Llena el combo de Procedencias.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 07/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Procedencias()
    {
        Cls_Cat_Pat_Com_Procedencias_Negocio Procedencia_Negocio = new Cls_Cat_Pat_Com_Procedencias_Negocio();
        Procedencia_Negocio.P_Estatus = "VIGENTE";
        Procedencia_Negocio.P_Tipo_DataTable = "PROCEDENCIAS";
        Cmb_Procedencia.DataSource = Procedencia_Negocio.Consultar_DataTable();
        Cmb_Procedencia.DataTextField = "NOMBRE";
        Cmb_Procedencia.DataValueField = "PROCEDENCIA_ID";
        Cmb_Procedencia.DataBind();
        Cmb_Procedencia.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
    ///PROPIEDADES:     
    ///             1. Estatus. Estatus en el que se cargara la configuración de los 
    ///                         controles.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Configuracion_Formulario(Boolean Estatus, String Tipo_Formulario)
    {
        
        Div_Resguardos.Visible = true;
        Div_Datos_Generales.Visible = true;
        if (Estatus)
        {
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ToolTip = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ToolTip = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            Btn_Generar_Reporte.Visible = true; 
            TR_Codigo_Barras.Visible = true;
        }
        else
        {
            Btn_Nuevo.AlternateText = "Dar de Alta";
            Btn_Nuevo.ToolTip = "Dar de Alta";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
            Btn_Salir.AlternateText = "Cancelar";
            Btn_Salir.ToolTip = "Cancelar";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
            Btn_Generar_Reporte.Visible = false; 
            TR_Codigo_Barras.Visible = false;
        }
        Btn_Busqueda_Partida_Reparacion.Enabled = !Estatus;
        Txt_Clave_Partida_Reparacion.Enabled = !Estatus;
        Txt_Cuenta_Gasto.Enabled = !Estatus;
        Txt_Cuenta_Contable.Enabled = !Estatus;
        Cmb_Tipo_Activo.Enabled = !Estatus;
        Cmb_Clase_Activo.Enabled = !Estatus;
        Btn_Agregar_Donador.Visible = !Estatus;
        Txt_Nombre_Producto_Donado.Enabled = !Estatus;
        Btn_Buscar_RFC_Donador.Visible = !Estatus;
        Txt_Modelo.Enabled = !Estatus;
        Cmb_Marca.Enabled = !Estatus;
        Cmb_Gerencias.Enabled = !Estatus;
        Cmb_Dependencias.Enabled = !Estatus;
        Cmb_Tipos_Vehiculos.Enabled = !Estatus;
        Cmb_Tipo_Combustible.Enabled = !Estatus;
        Cmb_Colores.Enabled = !Estatus;
        Cmb_Zonas.Enabled = !Estatus;
        Txt_Numero_Inventario.Enabled = false;
        Txt_Numero_Economico.Enabled = !Estatus;
        Txt_Placas.Enabled = !Estatus;
        Txt_Costo.Enabled = !Estatus;
        Txt_Capacidad_Carga.Enabled = !Estatus;
        Txt_Anio_Fabricacion.Enabled = !Estatus;
        Txt_Serie_Carroceria.Enabled = !Estatus;
        Txt_Numero_Cilindros.Enabled = !Estatus;
        Btn_Fecha_Adquisicion.Enabled = !Estatus;
        Btn_Fecha_Inventario.Enabled = !Estatus;
        Txt_Kilometraje.Enabled = !Estatus;
        Txt_No_Factura.Enabled = !Estatus;
        Btn_Lanzar_Mpe_Proveedores.Visible = !Estatus;
        Cmb_Odometro.Enabled = !Estatus;
        Txt_Observaciones.Enabled = !Estatus;
        Cmb_Empleados.Enabled = !Estatus;
        Txt_Cometarios.Enabled = !Estatus;
        Btn_Agregar_Resguardante.Visible = !Estatus;
        Btn_Quitar_Resguardante.Visible = !Estatus;
        Cmb_Procedencia.Enabled = !Estatus;
        Grid_Resguardantes.Columns[0].Visible = !Estatus;
        Btn_Busqueda_Avanzada_Resguardante.Visible = !Estatus;
        Btn_Resguardo_Completo_Operador.Visible = !Estatus;
        Btn_Resguardo_Completo_Funcionario_Recibe.Visible = !Estatus;
        Btn_Resguardo_Completo_Autorizo.Visible = !Estatus;
        Btn_Limpiar_Donador.Visible = !Estatus;
        Btn_Limpiar_FileUpload.Visible = !Estatus;
        Btn_Agregar_Archivo.Visible = !Estatus;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Detalles
    ///DESCRIPCIÓN: Limpia los controles de detalles del Formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Detalles()
    {
        Hdf_Cuenta_Gasto_ID.Value = "";
        Txt_Cuenta_Gasto.Text = "";
        Hdf_Partida_Reparacion_ID.Value = "";
        Txt_Clave_Partida_Reparacion.Text = "";
        Txt_Nombre_Partida_Reparacion.Text = "";
        Hdf_Producto_ID.Value = "";
        Hdf_Vehiculo_ID.Value = "";
        Cmb_Procedencia.SelectedIndex = 0;
        Txt_Nombre_Producto_Donado.Text = "";
        Cmb_Marca.SelectedIndex = 0;
        Cmb_Procedencia.SelectedIndex = 0;
        Txt_Nombre_Producto_Donado.Text = "";
        Cmb_Marca.SelectedIndex = 0;
        Txt_Modelo.Text = "";
        Hdf_Donador_ID.Value = "";
        Txt_RFC_Donador.Text = "";
        Txt_Nombre_Donador.Text = "";
        Hdf_Cuenta_Contable_ID.Value = "";
        Txt_Cuenta_Contable.Text = "";
        Cmb_Gerencias.SelectedIndex = 0;
        Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);
        Cmb_Dependencias.SelectedIndex = 0;
        Cmb_Clase_Activo.SelectedIndex = 0;
        Cmb_Tipo_Activo.SelectedIndex = 0;
        Cmb_Tipos_Vehiculos.SelectedIndex = 0;
        Cmb_Tipo_Combustible.SelectedIndex = 0;
        Cmb_Colores.SelectedIndex = 0;
        Cmb_Zonas.SelectedIndex = 0;
        Txt_Numero_Inventario.Text = "";
        Txt_Numero_Economico.Text = "";
        Txt_Placas.Text = "";
        Txt_Costo.Text = "";
        Txt_Capacidad_Carga.Text = "";
        Txt_Anio_Fabricacion.Text = "";
        Txt_Serie_Carroceria.Text = "";
        Txt_Numero_Cilindros.Text = "";
        Txt_Fecha_Adquisicion.Text = "";
        Txt_Fecha_Inventario.Text = "";
        Txt_Kilometraje.Text = "";
        Txt_No_Factura.Text = "";
        Txt_Nombre_Proveedor.Text = "";
        Hdf_Proveedor_ID.Value = "";
        Cmb_Estatus.SelectedIndex = 0;
        Cmb_Odometro.SelectedIndex = 0;
        Txt_Observaciones.Text = "";
        Cmb_Empleados.SelectedIndex = 0;
        Txt_Cometarios.Text = "";
        Limpiar_Firmas();
        Grid_Resguardantes.SelectedIndex = -1;
        Grid_Resguardantes.DataSource = new DataTable();
        Grid_Resguardantes.DataBind();
        Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
        Session.Remove("Diccionario_Archivos");
        Session.Remove("Tabla_Archivos");
        Grid_Archivos.DataSource = new DataTable();
        Grid_Archivos.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo_Donadores
    ///DESCRIPCIÓN: Limpia los controles del Formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 22/Enero/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Catalogo_Donadores()
    {
        Txt_Nombre_Donador_MPE.Text = "";
        Txt_Apellido_Paterno_Donador.Text = "";
        Txt_Apellido_Materno_Donador.Text = "";
        Txt_Direccion_Donador.Text = "";
        Txt_Ciudad_Donador.Text = "";
        Txt_Estado_Donador.Text = "";
        Txt_Telefono_Donador.Text = "";
        Txt_Celular_Donador.Text = "";
        Txt_CURP_Donador.Text = "";
        Txt_RFC_Donador_MPE.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Firmas
    ///DESCRIPCIÓN: Se Limpian los campos de la parte de Firmas.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 20/Octubre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Firmas()
    {
        Hdf_Resguardo_Completo_Operador.Value = "";
        Hdf_Resguardo_Completo_Funcionario_Recibe.Value = "";
        Hdf_Resguardo_Completo_Autorizo.Value = "";
        Txt_Resguardo_Completo_Operador.Text = "";
        Txt_Resguardo_Completo_Funcionario_Recibe.Text = "";
        Txt_Resguardo_Completo_Autorizo.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Remover_Sesiones_Control_AsyncFileUpload
    ///DESCRIPCIÓN: Limpia un control de AsyncFileUpload
    ///PROPIEDADES:     
    ///CREO: Juan Alberto Hernandez Negrete
    ///FECHA_CREO: 16/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Remover_Sesiones_Control_AsyncFileUpload(String Cliente_ID)
    {
        HttpContext Contexto;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            Contexto = HttpContext.Current;
        }
        else
        {
            Contexto = null;
        }
        if (Contexto != null)
        {
            foreach (String key in Contexto.Session.Keys)
            {
                if (key.Contains(Cliente_ID))
                {
                    Contexto.Session.Remove(key);
                    break;
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Buscar_Clave_DataTable
    ///DESCRIPCIÓN: Busca una Clave en un DataTable, si la encuentra Retorna 'true'
    ///             en caso contrario 'false'.
    ///PROPIEDADES:  
    ///             1.  Clave.  Clave que se buscara en el DataTable
    ///             2.  Tabla.  Datatable donde se va a buscar la clave.
    ///             3.  Columna.Columna del DataTable donde se va a buscar la clave.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Buscar_Clave_DataTable(String Clave, DataTable Tabla, Int32 Columna)
    {
        Boolean Resultado_Busqueda = false;
        if (Tabla != null && Tabla.Rows.Count > 0 && Tabla.Columns.Count > 0)
        {
            if (Tabla.Columns.Count > Columna)
            {
                for (Int32 Contador = 0; Contador < Tabla.Rows.Count; Contador++)
                {
                    if (Tabla.Rows[Contador][Columna].ToString().Trim().Equals(Clave.Trim()))
                    {
                        Resultado_Busqueda = true;
                        break;
                    }
                }
            }
        }
        return Resultado_Busqueda;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_DataSet_Resguardos_Vehiculos
    ///DESCRIPCIÓN: Llena el dataSet "Data_Set_Resguardos_Vehiculos" con las personas a las que se les asigno el
    ///vehiculo, sus detalles generales y especificos, para que con estos datos se genere el reporte.
    ///PARAMETROS:  
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 23/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_DataSet_Resguardos_Vehiculos(Cls_Ope_Pat_Com_Vehiculos_Negocio Id_Vehiculo)
    {
        try
        {

            String Formato = "PDF";
            Id_Vehiculo.P_Producto_Almacen = false;
            Cls_Alm_Com_Resguardos_Negocio Consulta_Resguardos_Vehiculos = new Cls_Alm_Com_Resguardos_Negocio();
            DataSet Data_Set_Resguardos_Vehiculos, Data_Set_Vehiculos_Asegurados;
            Data_Set_Resguardos_Vehiculos = Consulta_Resguardos_Vehiculos.Consulta_Resguardos_Vehiculos(Id_Vehiculo);
            Data_Set_Vehiculos_Asegurados = Consulta_Resguardos_Vehiculos.Consulta_Vehiculos_Asegurados(Id_Vehiculo);

            Ds_Alm_Com_Resguardos_Vehiculos Ds_Consulta_Resguardos_Vehiculos = new Ds_Alm_Com_Resguardos_Vehiculos();
            Generar_Reporte(Data_Set_Vehiculos_Asegurados, Data_Set_Resguardos_Vehiculos, Ds_Consulta_Resguardos_Vehiculos, Formato);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Jefe_Dependencia
    ///DESCRIPCIÓN: Regresa el nombre del empleado nombrado jefe de dependencia en la tabla organigrama
    ///PARAMETROS:  el identificador de la dependencia
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Jefe_Dependencia(String Dependencia_ID)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Jefe_Departamento(Dependencia_ID);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Jefe_Almacen
    ///DESCRIPCIÓN: Regresa el nombre del empleado nombrado jefe de almacen en la tabla organigrama
    ///PARAMETROS:  el tipo de puesto determinado en el campo tipo de la tabla organigrama
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Jefe_Almacen(String Almacen)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Jefe_Almacen(Almacen);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Almacen
    ///DESCRIPCIÓN: Regresa el valor del campo ALMACEN_GENERAL de la tabla productos para conocer de que almacen proviene el producto
    ///PARAMETROS:  el identificador del producto
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Almacen(String Bien_ID)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Almacen(Bien_ID);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 15/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_Vehiculos_A, DataSet Data_Set_Consulta_Resguardos_V, DataSet Ds_Reporte, String Formato)
    {
        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;

        try
        {
            if (Data_Set_Consulta_Resguardos_V.Tables[0].Rows.Count > 0)
            {
                String Cantidad = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0]["CANTIDAD"].ToString();
                String Costo = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0]["COSTO_UNITARIO"].ToString();
                Double Resultado = (Convert.ToDouble(Cantidad)) * (Convert.ToDouble(Costo));

                String Total = "" + Resultado;
                Renglon = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[0];
                Ds_Reporte.Tables[1].ImportRow(Renglon);
                Ds_Reporte.Tables[1].Rows[0].SetField("COSTO_TOTAL", Total);

                for (int Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_Resguardos_V.Tables[0].Rows.Count; Cont_Elementos++)
                {
                    Renglon = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]; //Instanciar renglon e importarlo
                    Ds_Reporte.Tables[0].ImportRow(Renglon);

                    String No_Empleado = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["NO_EMPLEADO"].ToString();
                    String Nombre_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["NOMBRE_E"].ToString();
                    String Apellido_Paterno_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["APELLIDO_PATERNO_E"].ToString();
                    String Apellido_Materno_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["APELLIDO_MATERNO_E"].ToString();
                    String RFC_E = Data_Set_Consulta_Resguardos_V.Tables[0].Rows[Cont_Elementos]["RFC_E"].ToString();
                    String Resguardante = Nombre_E + " " + Apellido_Paterno_E + " " + Apellido_Materno_E + " " + "(" + RFC_E + ")";
                    Ds_Reporte.Tables[0].Rows[Cont_Elementos].SetField("RESGUARDANTES", "[" + No_Empleado.Trim() + "] " + Resguardante);
                }

                if (Data_Set_Consulta_Vehiculos_A.Tables[0].Rows.Count > 0)
                {
                    String Nombre_Aeguradora = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["NOMBRE_ASEGURADORA"].ToString();
                    String No_Poliza = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["NO_POLIZA"].ToString();
                    String Descripcion_Seguro = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["DESCRIPCION_SEGURO"].ToString();
                    String Cobertura = Data_Set_Consulta_Vehiculos_A.Tables[0].Rows[0]["COBERTURA"].ToString();
                    Ds_Reporte.Tables[1].Rows[0].SetField("NOMBRE_ASEGURADORA", Nombre_Aeguradora);
                    Ds_Reporte.Tables[1].Rows[0].SetField("NO_POLIZA", No_Poliza);
                    Ds_Reporte.Tables[1].Rows[0].SetField("DESCRIPCION_SEGURO", Descripcion_Seguro);
                    Ds_Reporte.Tables[1].Rows[0].SetField("COBERTURA", Cobertura);
                }
            }

            // Ruta donde se encuentra el Reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Compras/Rpt_Alm_Com_Resguardos_Vehiculos.rpt";

            // Se crea el nombre del reporte
            String Nombre_Reporte = "Resguardo_Vehiculos" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now);

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = Nombre_Reporte + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = Nombre_Reporte + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
            //Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
        }
    }

    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      23-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Resguardantes
    ///DESCRIPCIÓN: Llena la tabla de Resguardantes
    ///PROPIEDADES:     
    ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
    ///             2.  tabla.  Tabla que se va a cargar en el Grid.    
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Resguardantes(Int32 Pagina, DataTable Tabla)
    {
        Session["Dt_Resguardantes"] = Tabla;
        Grid_Resguardantes.Columns[1].Visible = true;
        Grid_Resguardantes.Columns[2].Visible = true;
        Grid_Resguardantes.DataSource = Tabla;
        Grid_Resguardantes.PageIndex = Pagina;
        Grid_Resguardantes.DataBind();
        Grid_Resguardantes.Columns[1].Visible = false;
        Grid_Resguardantes.Columns[2].Visible = false;
    }

    #endregion

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes_Generales
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación de la pestaña de Generales.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Componentes_Generales()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Procedencia.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Procedencia.";
            Validacion = false;
        }
        if (Hdf_Producto_ID.Value.Trim().Length == 0 || Txt_Nombre_Producto_Donado.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Seleccionar el Producto.";
            Validacion = false;
        }
        if (Cmb_Marca.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Marca.";
            Validacion = false;
        }
        if (Txt_Modelo.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Modelo.";
            Validacion = false;
        }
        if (Cmb_Gerencias.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo Gerencia.";
            Validacion = false;
        }
        if (Cmb_Dependencias.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Unidad Responsable.";
            Validacion = false;
        }
        if (Cmb_Tipos_Vehiculos.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Tipos de Vehículos.";
            Validacion = false;
        }
        if (Cmb_Tipo_Combustible.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Tipos de Combustible.";
            Validacion = false;
        }
        if (Cmb_Colores.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Colores.";
            Validacion = false;
        }
        if (Cmb_Zonas.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Ubicación Fisica.";
            Validacion = false;
        }

        if (Txt_Numero_Economico.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Número Económico del Vehículo.";
            Validacion = false;
        }
        if (Txt_Placas.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir las Placas del Vehículo.";
            Validacion = false;
        }
        if (Txt_Costo.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Costo Actual del Vehículo.";
            Validacion = false;
        }
        if (Txt_Anio_Fabricacion.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Año de Fabricación del Vehículo.";
            Validacion = false;
        }
        if (Txt_Serie_Carroceria.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Número de Serie del Vehículo.";
            Validacion = false;
        }
        if (Txt_Numero_Cilindros.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Número de Cilindros del Vehículo.";
            Validacion = false;
        }
        if (Txt_Fecha_Inventario.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Fecha de Resguardo del Vehículo.";
            Validacion = false;
        }
        if (Txt_Fecha_Adquisicion.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Fecha de Adquisición del Vehículo.";
            Validacion = false;
        }
        if (Txt_Kilometraje.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Kilometraje del Vehículo.";
            Validacion = false;
        }
        if (Cmb_Odometro.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Odomentro.";
            Validacion = false;
        }
        if (Grid_Resguardantes.Rows.Count == 0 || Session["Dt_Resguardantes"] == null)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe haber como minimo un empleado como resguardatario del Vehículo.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes_Resguardos
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación de la pestaña de Resguardos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Componentes_Resguardos()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Empleados.SelectedIndex == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Seleccionar el Empleado para Resguardo.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Catalogo_Donador
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación para dar de alta un donador.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 22/Enero/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Catalogo_Donador()
    {
        Lbl_MPE_Donadores_Cabecera_Error.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Txt_Nombre_Donador_MPE.Text.Trim().Length == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre del Donador.";
            Validacion = false;
        }
        if (Txt_RFC_Donador_MPE.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br/>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el RFC del Donador.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_MPE_Donadores_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_MPE_Donadores_Mensaje_Error.Visible = true;
            MPE_Donadores.Show();
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
    ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private Boolean Validar_Archivo()
    {
        String Mensaje_Error = "";
        Boolean Validacion = true;
        String[] Extensiones_Permitidas = { ".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".ppt", ".pptx", ".pdf" };
        String Extension_Archivo = Path.GetExtension(AFU_Archivo.FileName).ToLower();

        if (AFU_Archivo.HasFile)
        {
            if (AFU_Archivo.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                Mensaje_Error += "+ El tamaño del archivo excede el limite permitido: " + AFU_Archivo.FileName;
                Mensaje_Error += " <br />";
                Validacion = false;
            }
            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                Mensaje_Error += "+ No se permite subir archivos con extensión: " + Extension_Archivo;
                Mensaje_Error += " <br />";
                Validacion = false;
            }
        }
        else
        {
            Mensaje_Error += "+ Debe seleccionar un archivo para subir.";
            Mensaje_Error += " <br />";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Vehiculo
    ///DESCRIPCIÓN: Muestra a Detalle un Vehiculo.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Mostrar_Detalles_Vehiculo(Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo)
    {
        try
        {
            Hdf_Vehiculo_ID.Value = Vehiculo.P_Vehiculo_ID;
            Cmb_Clase_Activo.SelectedIndex = Cmb_Clase_Activo.Items.IndexOf(Cmb_Clase_Activo.Items.FindByValue(Vehiculo.P_Clase_Activo_ID));
            Cmb_Tipo_Activo.SelectedIndex = Cmb_Tipo_Activo.Items.IndexOf(Cmb_Tipo_Activo.Items.FindByValue(Vehiculo.P_Clasificacion_ID));
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Vehiculo.P_Gerencia_ID));
            Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Vehiculo.P_Dependencia_ID));
            Cmb_Tipos_Vehiculos.SelectedIndex = Cmb_Tipos_Vehiculos.Items.IndexOf(Cmb_Tipos_Vehiculos.Items.FindByValue(Vehiculo.P_Tipo_Vehiculo_ID));
            Cmb_Tipo_Combustible.SelectedIndex = Cmb_Tipo_Combustible.Items.IndexOf(Cmb_Tipo_Combustible.Items.FindByValue(Vehiculo.P_Tipo_Combustible_ID));
            Cmb_Colores.SelectedIndex = Cmb_Colores.Items.IndexOf(Cmb_Colores.Items.FindByValue(Vehiculo.P_Color_ID));
            Cmb_Zonas.SelectedIndex = Cmb_Zonas.Items.IndexOf(Cmb_Zonas.Items.FindByValue(Vehiculo.P_Zona_ID));
            Txt_Numero_Inventario.Text = Vehiculo.P_Numero_Inventario.ToString();
            Txt_Numero_Economico.Text = Vehiculo.P_Numero_Economico.ToString();
            Txt_Placas.Text = Vehiculo.P_Placas;
            Txt_Costo.Text = Vehiculo.P_Costo_Actual.ToString("#,###,###.00");
            Txt_Capacidad_Carga.Text = Vehiculo.P_Capacidad_Carga.ToString();
            Txt_Anio_Fabricacion.Text = Vehiculo.P_Anio_Fabricacion.ToString();
            Txt_Serie_Carroceria.Text = Vehiculo.P_Serie_Carroceria;
            Txt_Numero_Cilindros.Text = Vehiculo.P_Numero_Cilindros.ToString();
            Txt_Fecha_Adquisicion.Text = String.Format("{0:dd/MMM/yyyy}", Vehiculo.P_Fecha_Adquisicion);
            Txt_Fecha_Inventario.Text = String.Format("{0:dd/MMM/yyyy}", Vehiculo.P_Fecha_Inventario);
            Txt_Kilometraje.Text = Vehiculo.P_Kilometraje.ToString("#,###,###.00");
            Txt_No_Factura.Text = Vehiculo.P_No_Factura_.Trim();
            Hdf_Proveedor_ID.Value = Vehiculo.P_Proveedor_ID;
            if (!String.IsNullOrEmpty(Hdf_Proveedor_ID.Value)) {
                Cls_Cat_Com_Proveedores_Negocio Proveedor_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                Proveedor_Negocio.P_Proveedor_ID = Hdf_Proveedor_ID.Value;
                DataTable Dt_Proveedor = Proveedor_Negocio.Consulta_Datos_Proveedores();
                if (Dt_Proveedor != null && Dt_Proveedor.Rows.Count > 0) {
                    Txt_Nombre_Proveedor.Text = Dt_Proveedor.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString();
                }
            }
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Vehiculo.P_Estatus));
            Cmb_Odometro.SelectedIndex = Cmb_Odometro.Items.IndexOf(Cmb_Odometro.Items.FindByValue(Vehiculo.P_Odometro));
            Txt_Observaciones.Text = Vehiculo.P_Observaciones;
            Llenar_Grid_Resguardantes(0, Vehiculo.P_Resguardantes);
            Grid_Resguardantes.Columns[0].Visible = false;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Empleados
    ///DESCRIPCIÓN: Llena el Grid de los empleados para seleccionarlo.
    ///PARAMETROS: Pagina. Pagina del Grid que se mostrará.     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Julio/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    private void Consultar_Empleados(String Dependencia_ID) { 
        try {
            Session.Remove("Dt_Resguardantes");
            Grid_Resguardantes.DataSource = new DataTable();
            Grid_Resguardantes.DataBind();
            if (Cmb_Dependencias.SelectedIndex > 0) {
                Cls_Ope_Pat_Com_Vehiculos_Negocio Combo = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                Combo.P_Tipo_DataTable = "EMPLEADOS";
                Combo.P_Dependencia_ID = Dependencia_ID;
                DataTable Tabla = Combo.Consultar_DataTable();
                Llenar_Combo_Empleados(Tabla);
            } else {
                DataTable Tabla = new DataTable();
                Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                Tabla.Columns.Add("NOMBRE", Type.GetType("System.String"));
                Llenar_Combo_Empleados(Tabla);
            }
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    
    #region "Busqueda Resguardantes"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
        ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Busqueda_Empleados_Resguardo() {
            Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
            Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
            Cls_Ope_Pat_Com_Vehiculos_Negocio Negocio = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
            Negocio.P_Estatus_Empleado = "ACTIVO";
            if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado_Resguardante = Txt_Busqueda_No_Empleado.Text.Trim(); }
            if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Resguardante = Txt_Busqueda_RFC.Text.Trim(); }
            if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Resguardante = Txt_Busqueda_Nombre_Empleado.Text.Trim(); }
            if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
            Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados_Resguardos();
            Grid_Busqueda_Empleados_Resguardo.DataBind();
            Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
        }

    #endregion

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Historial_Archivos
        ///DESCRIPCIÓN: Llena la tabla de Historial de Archivos
        ///PARAMETROS:     
        ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
        ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 01/Julio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Grid_Historial_Archivos(Int32 Pagina, DataTable Tabla)
        {
            Grid_Archivos.Columns[0].Visible = true;
            Grid_Archivos.Columns[1].Visible = true;
            Grid_Archivos.DataSource = Tabla;
            Grid_Archivos.PageIndex = Pagina;
            Grid_Archivos.DataBind();
            Grid_Archivos.Columns[0].Visible = false;
            Grid_Archivos.Columns[1].Visible = false;
            Session["Tabla_Archivos"] = Tabla;
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Generar_Tabla_Archivos
        /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
        /// PARAMETROS: 
        /// CREO: Jesus Toledo Rodriguez
        /// FECHA_CREO: 04-may-2012
        /// MODIFICO:
        /// FECHA_MODIFICO:
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private DataTable Generar_Tabla_Archivos()
        {
            DataTable Tabla_Nueva = new DataTable();
            DataColumn Columna1;
            DataColumn Columna2;
            DataColumn Columna3;
            DataColumn Columna4;
            DataColumn Columna5;
            DataColumn Columna6;
            DataColumn Columna7;
            DataColumn Columna8;
            DataColumn Columna9;

            // ---------- Inicializar columnas
            Columna1 = new DataColumn();
            Columna1.DataType = System.Type.GetType("System.String");
            Columna1.ColumnName = "ARCHIVO_BIEN_ID";
            Tabla_Nueva.Columns.Add(Columna1);
            Columna2 = new DataColumn();
            Columna2.DataType = System.Type.GetType("System.String");
            Columna2.ColumnName = "BIEN_ID";
            Tabla_Nueva.Columns.Add(Columna2);
            Columna3 = new DataColumn();
            Columna3.DataType = System.Type.GetType("System.String");
            Columna3.ColumnName = "TIPO";
            Tabla_Nueva.Columns.Add(Columna3);
            Columna4 = new DataColumn();
            Columna4.DataType = System.Type.GetType("System.DateTime");
            Columna4.ColumnName = "FECHA";
            Tabla_Nueva.Columns.Add(Columna4);
            Columna5 = new DataColumn();
            Columna5.DataType = System.Type.GetType("System.String");
            Columna5.ColumnName = "ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna5);
            Columna6 = new DataColumn();
            Columna6.DataType = System.Type.GetType("System.String");
            Columna6.ColumnName = "TIPO_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna6);
            Columna7 = new DataColumn();
            Columna7.DataType = System.Type.GetType("System.String");
            Columna7.ColumnName = "DESCRIPCION_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna7);
            Columna8 = new DataColumn();
            Columna8.DataType = System.Type.GetType("System.String");
            Columna8.ColumnName = "CHECKSUM";
            Tabla_Nueva.Columns.Add(Columna8);
            Columna9 = new DataColumn();
            Columna9.DataType = System.Type.GetType("System.String");
            Columna9.ColumnName = "RUTA_ARCHIVO";
            Tabla_Nueva.Columns.Add(Columna9);

            return Tabla_Nueva;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
        /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
        /// 	            regresa un diccionario vacio
        /// 	PARÁMETROS:
        /// 	CREO: Roberto González Oseguera
        /// 	FECHA_CREO: 04-may-2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
        {
            Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

            // si existe el diccionario en variable de sesion
            if (Session["Diccionario_Archivos"] != null)
            {
                Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
            }

            return Diccionario_Archivos;
        }

        ///*******************************************************************************************************
        /// 	NOMBRE_FUNCIÓN: Guardar_Archivos
        /// 	DESCRIPCIÓN: Guardar en el servidor los archivos que se hayan recibido
        /// 	PARÁMETROS:
        /// 	CREO: Roberto González Oseguera
        /// 	FECHA_CREO: 10-may-2011
        /// 	MODIFICÓ: 
        /// 	FECHA_MODIFICÓ: 
        /// 	CAUSA_MODIFICACIÓN: 
        ///*******************************************************************************************************
        private void Guardar_Archivos()
        {
            DataTable Tabla_Tramites = (DataTable)Session["Tabla_Archivos"];
            Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
            String Directorio = MapPath("../../" + Ope_Pat_Archivos_Bienes.Campo_Ruta_Fisica_Archivos + "/VEHICULOS/" + Hdf_Vehiculo_ID.Value.ToString().Trim());
            String Rura_Archivo = "";

            try
            {
                if (Tabla_Tramites != null)     //si la tabla tramites contiene datos
                {
                    foreach (DataRow Fila_Tramite in Tabla_Tramites.Rows)   // recorrer la tabla
                    {
                        if (!String.IsNullOrEmpty(Fila_Tramite["CHECKSUM"].ToString()))
                        {
                            if (!Directory.Exists(Directorio))//si el directorio no existe, crearlo
                                Directory.CreateDirectory(Directorio);

                            Rura_Archivo = Directorio + "/" + HttpUtility.HtmlDecode(Fila_Tramite["ARCHIVO"].ToString());
                            //crear filestream y binarywriter para guardar archivo
                            FileStream Escribir_Archivo = new FileStream(Rura_Archivo, FileMode.Create, FileAccess.Write);
                            BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);

                            // Guardar archivo (escribir datos en el filestream)                            
                            Datos_Archivo.Write(Diccionario_Archivos[Fila_Tramite["CHECKSUM"].ToString()]);
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Cargar_Nombre_Partida
        ///DESCRIPCIÓN: Cargar_Nombre_Partida
        ///PARAMETROS: 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 01/Julio/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Cargar_Nombre_Partida(String Filtro, String Tipo_Filtro)
        {
            Cls_Rpt_Pat_Listado_Bienes_Negocio Cls_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            if (Tipo_Filtro.Equals("CLAVE")) Cls_Negocio.P_Clave_Partida = Filtro.Trim();
            if (Tipo_Filtro.Equals("IDENTIFICADOR")) Cls_Negocio.P_Partida_ID = Filtro.Trim();
            DataTable Dt_Partidas = Cls_Negocio.Consultar_Paridas_Especificas();
            if (Dt_Partidas != null)
            {
                if (Dt_Partidas.Rows.Count == 1)
                {
                    Hdf_Partida_Reparacion_ID.Value = Dt_Partidas.Rows[0]["PARTIDA_ID"].ToString().Trim();
                    Txt_Nombre_Partida_Reparacion.Text = Dt_Partidas.Rows[0]["NOMBRE"].ToString().Trim();
                }
            }
        }

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resguardantes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el Cambio de Pagina de la Resguardantes
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Resguardantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Resguardantes"] != null)
            {
                DataTable Tabla = (DataTable)Session["Dt_Resguardantes"];
                Llenar_Grid_Resguardantes(e.NewPageIndex, Tabla);
                Grid_Resguardantes.SelectedIndex = (-1);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resguardantes_RowDataBound
    ///DESCRIPCIÓN: Maneja el Evento RowDataBound del Grid de Resguardos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Resguardantes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.FindControl("Btn_Ver_Informacion_Resguardo") != null)
                {
                    if (Session["Dt_Resguardantes"] != null)
                    {
                        ImageButton Btn_Informacion = (ImageButton)e.Row.FindControl("Btn_Ver_Informacion_Resguardo");
                        Btn_Informacion.CommandArgument = ((DataTable)Session["Dt_Resguardantes"]).Rows[e.Row.RowIndex]["COMENTARIOS"].ToString();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Ecabezado_Mensaje.Text = "[Excepción: '" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
      
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e) { 
        try {
            if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1)) {
                String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Hdf_Tipo_Busqueda.Value == null || Hdf_Tipo_Busqueda.Value.Trim().Length == 0) { 
                    String Dependencia_ID = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString() : null;
                    Ubicar_Gerencia(Dependencia_ID);
                    Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue);
                    Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Dependencia_ID));
                    Consultar_Empleados(Dependencia_ID);
                    Cmb_Empleados.SelectedIndex = Cmb_Empleados.Items.IndexOf(Cmb_Empleados.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));
                } else if(Hdf_Tipo_Busqueda.Value.Trim().Equals("OPERADOR")) {
                    Hdf_Resguardo_Completo_Operador.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : "";
                    String Texto =((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Operador.Text = Texto.Trim();
                } else if(Hdf_Tipo_Busqueda.Value.Trim().Equals("FUNCIONARIO_RECIBE")) {
                    Hdf_Resguardo_Completo_Funcionario_Recibe.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Funcionario_Recibe.Text = Texto.Trim();
                } else if(Hdf_Tipo_Busqueda.Value.Trim().Equals("AUTORIZO")) {
                    Hdf_Resguardo_Completo_Autorizo.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Autorizo.Text = Texto.Trim();
                }

                MPE_Resguardante.Hide();
            }
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Ubicar_Gerencia
    ///DESCRIPCIÓN: Coloca el combo gerencia en la que le corresponde al empleado seleccionado del grid de búsqueda
    ///PROPIEDADES: el identificador de la dependencia para buscar la gerencia
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Ubicar_Gerencia(String Dependencia_ID) {

        Cls_Ope_Pat_Com_Vehiculos_Negocio Combos = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";// Se llena el combo de dependencias para obtener la gerencia ID y asi colocar la gerencia a la que pertence el empleado seleccionado en la búsqueda
        DataTable Dependencias = Combos.Consultar_DataTable();
        DataRow DR_Fila;
        String Gerencia_ID = "";//variable para almacenar la gerencia_ID cuando se encuentre
        for (int i = 0; i < Dependencias.Rows.Count; i++ ) {//Ciclo para encontrar la dependencia_ID y obtener la gerencia_id cuando se encuentre
            DR_Fila = Dependencias.Rows[i];
            if (DR_Fila["DEPENDENCIA_ID"].ToString().Equals(Dependencia_ID))
            {
                Gerencia_ID = DR_Fila["GERENCIA_ID"].ToString();
            }
        }
        if(!String.IsNullOrEmpty(Gerencia_ID))//se valida que contenga valor la variable gerencia_id
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Gerencia_ID));//Se coloca el combo gerencia en la posicion encontrada encontrada
        
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Productos del
    ///             Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Julio/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Proveedores.SelectedIndex > (-1))
            {
                String Proveedor_ID = Grid_Listado_Proveedores.SelectedRow.Cells[1].Text.Trim();
       
                Cls_Cat_Com_Proveedores_Negocio Proveedor_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                Proveedor_Negocio.P_Proveedor_ID = Proveedor_ID;
                DataTable Dt_Proveedor_Seleccionado = Proveedor_Negocio.Consulta_Datos_Proveedores();
                if (Dt_Proveedor_Seleccionado != null && Dt_Proveedor_Seleccionado.Rows.Count > 0)
                {
                    Hdf_Proveedor_ID.Value = Proveedor_ID.Trim();

                        Txt_Nombre_Proveedor.Text = Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim();

                    Mpe_Proveedores_Cabecera.Hide();
                }
                System.Threading.Thread.Sleep(500);
                Grid_Listado_Proveedores.SelectedIndex = (-1);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Proveedores_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Proveedores del Modal de Busqueda
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_PageIndexChanging(object sender, GridViewPageEventArgs e) {
        try {
            Grid_Listado_Proveedores.SelectedIndex = (-1);
            Llenar_Grid_Proveedores(e.NewPageIndex);
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Archivos_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Historial de Archivos
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Archivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Tabla_Archivos"] != null)
            {
                Grid_Archivos.SelectedIndex = (-1);
                Llenar_Grid_Historial_Archivos(e.NewPageIndex, (DataTable)Session["Tabla_Archivos"]);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Partida_Reparacion_Click
    ///DESCRIPCIÓN: Btn_Busqueda_Partida_Reparacion_Click
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Busqueda_Partida_Reparacion_Click(object sender, ImageClickEventArgs e)
    {
        Hdf_Partida_Reparacion_ID.Value = "";
        Txt_Nombre_Partida_Reparacion.Text = "";
        if (Txt_Clave_Partida_Reparacion.Text.Trim().Length == 4)
        {
            Cargar_Nombre_Partida(Txt_Clave_Partida_Reparacion.Text.Trim(), "CLAVE");
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "La Clave de la Partida para Reparación no es correcta.";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN: Genera el reporte detallado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Imprimir_Codigo_Barras_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Hdf_Vehiculo_ID.Value.Trim().Length > 0) {
                Cls_Ope_Pat_Com_Vehiculos_Negocio Mueble = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                Mueble.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;
                Mueble = Mueble.Consultar_Detalles_Vehiculo();
                String BarCode = "";
                if (!String.IsNullOrEmpty(Mueble.P_Clase_Activo_ID))
                {
                    Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                    CA_Negocio.P_Clase_Activo_ID = Mueble.P_Clase_Activo_ID.Trim();
                    CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                    DataTable Dt_Clase_Activo = CA_Negocio.Consultar_DataTable();
                    if (Dt_Clase_Activo != null)
                    {
                        if (Dt_Clase_Activo.Rows.Count > 0)
                        {
                            BarCode = Dt_Clase_Activo.Rows[0]["CLAVE"].ToString().Trim();
                        }
                    }
                }
                if (String.IsNullOrEmpty(BarCode)) BarCode = "0000";
                BarCode += String.Format("{0:00000000}", Convert.ToInt32(Mueble.P_Numero_Inventario));
                String Codigo_Barras_Embebido = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.Obtener_Codigo_Barras_Embebido(JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.CrearCodigo(BarCode.Trim()), System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["Tamano_Codigo_Barras"] = Cmb_Tamano_Etiqueta.SelectedItem.Value.Trim();
                Session["Numero_Codigo_Barras"] = BarCode;
                Session["Imagen_Codigo_Barras"] = Codigo_Barras_Embebido;
                String Pagina = "../Control_Patrimonial/Frm_Ope_Pat_Mostrar_Codigo_Barras.aspx";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt", "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                Lbl_Mensaje_Error.Text = "Tener un Vehículo para Imprimir el Codigo de Barras";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Donador_Click
    ///DESCRIPCIÓN: Lanza el Modal para agregar un Nuevo Donador.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 22/Enero/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Donador_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_MPE_Donadores_Cabecera_Error.Text = "";
            Lbl_MPE_Donadores_Mensaje_Error.Text = "";
            Div_MPE_Donadores_Mensaje_Error.Visible = false;
            Limpiar_Catalogo_Donadores();
            Div_MPE_Donadores_Mensaje_Error.Visible = false;
            MPE_Donadores.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_RFC_Donador_Click
    ///DESCRIPCIÓN: Realiza un busqueda de Donador por RFC.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 22/Enero/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_RFC_Donador_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Donador_ID.Value = "";
            if (Txt_RFC_Donador.Text.Trim().Length > 0)
            {
                String RFC_Donador = Txt_RFC_Donador.Text.Trim();
                Cls_Cat_Pat_Com_Donadores_Negocio Donador = new Cls_Cat_Pat_Com_Donadores_Negocio();
                Donador.P_RFC = Txt_RFC_Donador.Text.Trim();
                Donador = Donador.Consultar_Datos_Donador();
                if (Donador.P_Donador_ID != null && Donador.P_Donador_ID.Trim().Length > 0)
                {
                    Hdf_Donador_ID.Value = Donador.P_Donador_ID;
                    Txt_Nombre_Donador.Text = Donador.P_Apellido_Paterno + " " + Donador.P_Apellido_Materno + " " + Donador.P_Nombre;
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Donador no ha sido encontrado.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario introducir el RFC del Donador";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Prepara y da de Alta un Vehículo con uno o mas resguardantes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
            {
                Configuracion_Formulario(false, "VEHICULOS");
                Limpiar_Detalles();
                Session.Remove("Dt_Resguardantes");
                Cmb_Estatus.SelectedIndex = 1;
            }
            else
            {
                if (Validar_Componentes_Generales())
                {
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    
                    if (Cmb_Procedencia.SelectedIndex > 0) Vehiculo.P_Procedencia = Cmb_Procedencia.SelectedItem.Value;
                    if (Txt_Cuenta_Contable.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_ID.Value.Trim().Length > 0) Vehiculo.P_Cuenta_Contable_ID = Hdf_Cuenta_Contable_ID.Value;
                    if (Txt_Cuenta_Gasto.Text.Trim().Length > 0 && Hdf_Cuenta_Gasto_ID.Value.Trim().Length > 0) Vehiculo.P_Cuenta_Gasto_ID = Hdf_Cuenta_Gasto_ID.Value;
                    Vehiculo.P_Partida_ID = Hdf_Partida_Reparacion_ID.Value;
                    Vehiculo.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
                    Vehiculo.P_Clasificacion_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
                    Vehiculo.P_Marca_ID = Cmb_Marca.SelectedItem.Value;
                    Vehiculo.P_Modelo_ID = Txt_Modelo.Text.Trim();
                    Vehiculo.P_Donador_ID = Hdf_Donador_ID.Value;
                    Vehiculo.P_Producto_ID = Hdf_Producto_ID.Value.Trim();
                    Vehiculo.P_Nombre_Producto = Txt_Nombre_Producto_Donado.Text.Trim();
                    Vehiculo.P_Gerencia_ID = Cmb_Gerencias.SelectedValue.Trim();
                    Vehiculo.P_Dependencia_ID = Cmb_Dependencias.SelectedItem.Value.Trim();
                    Vehiculo.P_Tipo_Vehiculo_ID = Cmb_Tipos_Vehiculos.SelectedItem.Value.Trim();
                    Vehiculo.P_Tipo_Combustible_ID = Cmb_Tipo_Combustible.SelectedItem.Value.Trim();
                    Vehiculo.P_Color_ID = Cmb_Colores.SelectedItem.Value.Trim();
                    Vehiculo.P_Zona_ID = Cmb_Zonas.SelectedItem.Value.Trim();
                    Vehiculo.P_Numero_Economico_ = Txt_Numero_Economico.Text.Trim();
                    Vehiculo.P_Placas = Txt_Placas.Text.Trim();
                    Vehiculo.P_Costo_Actual = Convert.ToDouble(Txt_Costo.Text.Trim());
                    Vehiculo.P_Empleado_Autorizo = Hdf_Resguardo_Completo_Autorizo.Value.Trim();
                    Vehiculo.P_Empleado_Operador = Hdf_Resguardo_Completo_Operador.Value.Trim();
                    Vehiculo.P_Empleado_Funcionario_Recibe = Hdf_Resguardo_Completo_Funcionario_Recibe.Value.Trim();
                    Vehiculo.P_Anio_Fabricacion = Convert.ToInt32(Txt_Anio_Fabricacion.Text.Trim());
                    Vehiculo.P_Serie_Carroceria = Txt_Serie_Carroceria.Text.Trim();
                    Vehiculo.P_Numero_Cilindros = Convert.ToInt32(Txt_Numero_Cilindros.Text.Trim());
                    Vehiculo.P_Fecha_Adquisicion = Convert.ToDateTime(Txt_Fecha_Adquisicion.Text.Trim());
                    Vehiculo.P_Fecha_Inventario = Convert.ToDateTime(Txt_Fecha_Inventario.Text.Trim());
                    Vehiculo.P_Kilometraje = Convert.ToDouble(Txt_Kilometraje.Text.Trim());
                    Vehiculo.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                    Vehiculo.P_Odometro = Cmb_Odometro.SelectedItem.Value.Trim();
                    Vehiculo.P_Observaciones = Txt_Observaciones.Text.Trim();

                    if (Session["Tabla_Archivos"] != null)
                    {
                        Vehiculo.P_Dt_Historial_Archivos = (DataTable)Session["Tabla_Archivos"];
                    }
                    else
                    {
                        Vehiculo.P_Dt_Historial_Archivos = new DataTable();
                    }

                    Vehiculo.P_Resguardantes = (DataTable)Session["Dt_Resguardantes"];
                    if (Txt_No_Factura.Text.Trim().Length > 0)
                    {
                        Vehiculo.P_No_Factura_ = Txt_No_Factura.Text.Trim();
                    }
                    Vehiculo.P_Proveedor_ID = Hdf_Proveedor_ID.Value;
                    Vehiculo.P_Razon_Social_Proveedor = Txt_Nombre_Proveedor.Text.Trim();
                    Vehiculo.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
                    Vehiculo.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
                    Vehiculo.P_Cantidad = 1;
                    Vehiculo.Alta_Vehiculo();
                    Hdf_Vehiculo_ID.Value = Vehiculo.P_Vehiculo_ID;
                    Txt_Numero_Inventario.Text = Vehiculo.P_Numero_Inventario.ToString();
                    Guardar_Archivos();
                    Vehiculo = Vehiculo.Consultar_Detalles_Vehiculo();
                    Mostrar_Detalles_Vehiculo(Vehiculo);
                    Session.Remove("Dt_Resguardantes");
                    Configuracion_Formulario(true, "VEHICULOS");

                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta de Vehiculos", "alert('Alta de Vehículo Exitosa');", true);
                    Llenar_DataSet_Resguardos_Vehiculos(Vehiculo);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale
    ///             del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
        try {
            if (Btn_Salir.AlternateText.Equals("Salir")) {
                Session["Dt_Resguardantes"] = null;
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            } else {
                Grid_Resguardantes.Enabled = true;
                Configuracion_Formulario(true, "VEHICULOS");
                Limpiar_Detalles();
                Session.Remove("Dt_Resguardantes");
                Btn_Salir.AlternateText = "Salir";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            }
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN: Genera el reporte simple.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_Click(object sender, ImageClickEventArgs e) {
        try {
            if (Hdf_Vehiculo_ID.Value.Trim().Length > 0) {
                Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                Vehiculo.P_Vehiculo_ID = Hdf_Vehiculo_ID.Value;
                Vehiculo = Vehiculo.Consultar_Detalles_Vehiculo();
                Llenar_DataSet_Resguardos_Vehiculos(Vehiculo);
            } else {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                Lbl_Mensaje_Error.Text = "Seleccionar el Vehículo a Generar el Reporte.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Grencias_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Gerencias
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {   
            Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue.ToString());
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Dependencias
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            String Dependencia_ID = Cmb_Dependencias.SelectedItem.Value;
            Consultar_Empleados(Dependencia_ID);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_MPE_Donador_Aceptar_Click
    ///DESCRIPCIÓN: Da de alta el donador.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 22/Enero/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_MPE_Donador_Aceptar_Click(object sender, ImageClickEventArgs e) {
        try {
            if (Validar_Catalogo_Donador()) {
                Cls_Cat_Pat_Com_Donadores_Negocio Donador = new Cls_Cat_Pat_Com_Donadores_Negocio();
                Donador.P_RFC = Txt_RFC_Donador_MPE.Text.Trim();
                Donador = Donador.Consultar_Datos_Donador();
                if (Donador.P_Donador_ID != null && Donador.P_Donador_ID.Trim().Length > 0)  {
                    Lbl_MPE_Donadores_Cabecera_Error.Text = "Ya existe un donador con ese RFC.";
                    Lbl_MPE_Donadores_Mensaje_Error.Text = "";
                    Div_MPE_Donadores_Mensaje_Error.Visible = true;
                    MPE_Donadores.Show();
                } else {
                    Donador.P_Nombre = Txt_Nombre_Donador_MPE.Text.Trim();
                    Donador.P_Apellido_Paterno = Txt_Apellido_Paterno_Donador.Text.Trim();
                    Donador.P_Apellido_Materno = Txt_Apellido_Materno_Donador.Text.Trim();
                    Donador.P_Direccion = Txt_Direccion_Donador.Text.Trim();
                    Donador.P_Cuidad = Txt_Ciudad_Donador.Text.Trim();
                    Donador.P_Estado = Txt_Estado_Donador.Text.Trim();
                    Donador.P_Telefono = Txt_Telefono_Donador.Text.Trim();
                    Donador.P_Celular = Txt_Celular_Donador.Text.Trim();
                    Donador.P_CURP = Txt_CURP_Donador.Text.Trim();
                    Donador.P_RFC = Txt_RFC_Donador_MPE.Text.Trim();
                    Donador.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Donador = Donador.Alta_Donador();
                    Donador = Donador.Consultar_Datos_Donador();
                    Hdf_Donador_ID.Value = Donador.P_Donador_ID;
                    Txt_RFC_Donador.Text = Donador.P_RFC;
                    Txt_Nombre_Donador.Text = (Txt_Nombre_Donador.Text = Donador.P_Apellido_Paterno + " " + Donador.P_Apellido_Materno + " " + Donador.P_Nombre).Trim();
                    Limpiar_Catalogo_Donadores();
                    Div_MPE_Donadores_Mensaje_Error.Visible = false;
                    MPE_Donadores.Hide();
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_MPE_Donadores_Cabecera_Error.Text = Ex.Message;
            Lbl_MPE_Donadores_Mensaje_Error.Text = "";
            Div_MPE_Donadores_Mensaje_Error.Visible = true;
            MPE_Donadores.Show();
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_FileUpload_Click
    ///DESCRIPCIÓN: Limpia el FileUpload que carga los archivos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Limpiar_FileUpload_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Informacion_Resguardo_Click
    ///DESCRIPCIÓN: Manda Visualizar los Comentarios del Resguardo.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************
    protected void Btn_Ver_Informacion_Resguardo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton Btn_Ver_Informacion_Resguardo = (ImageButton)sender;
        String Comentarios = "Sin Comentarios";
        if (Btn_Ver_Informacion_Resguardo.CommandArgument.Trim().Length > 0) { Comentarios = "Comentarios: " + Btn_Ver_Informacion_Resguardo.CommandArgument.Trim(); }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('" + Comentarios + "');", true);
    }

    #region Vehículos Detalles

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Resguardante_Click
    ///DESCRIPCIÓN: Agrega una nuevo Empleado Resguardante para este Vehículo.
    ///             (No aun en la Base de Datos)
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        if (Validar_Componentes_Resguardos())
        {
            DataTable Tabla = (DataTable)Grid_Resguardantes.DataSource;
            if (Tabla == null)
            {
                if (Session["Dt_Resguardantes"] == null)
                {
                    Tabla = new DataTable("Resguardos");
                    Tabla.Columns.Add("BIEN_RESGUARDO_ID", Type.GetType("System.String"));
                    Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                    Tabla.Columns.Add("NO_EMPLEADO", Type.GetType("System.String"));
                    Tabla.Columns.Add("NOMBRE_EMPLEADO", Type.GetType("System.String"));
                    Tabla.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                    
                }
                else
                {
                    Tabla = (DataTable)Session["Dt_Resguardantes"];
                   
                }
            }
            if (!Buscar_Clave_DataTable(Cmb_Empleados.SelectedItem.Value, Tabla, 1)) {
                Cls_Cat_Empleados_Negocios Empleados_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleados_Negocio.P_Empleado_ID = Cmb_Empleados.SelectedItem.Value;
                DataTable Dt_Empleado = Empleados_Negocio.Consulta_Datos_Empleado();
                if (Dt_Empleado != null && Dt_Empleado.Rows.Count > 0) {
                    DataRow Fila = Tabla.NewRow();
                    Fila["BIEN_RESGUARDO_ID"] = HttpUtility.HtmlDecode("");
                    Fila["EMPLEADO_ID"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim();
                    Fila["NO_EMPLEADO"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim();
                    Fila["NOMBRE_EMPLEADO"] = HttpUtility.HtmlDecode(Cmb_Empleados.SelectedItem.Text);
                    if (Txt_Cometarios.Text.Trim().Length > 255) { Fila["COMENTARIOS"] = HttpUtility.HtmlDecode(Txt_Cometarios.Text.Trim().Substring(0, 254)); }
                    else { Fila["COMENTARIOS"] = HttpUtility.HtmlDecode(Txt_Cometarios.Text); }
                    Tabla.Rows.Add(Fila);
                }
                Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
                Grid_Resguardantes.SelectedIndex = (-1);
                Cmb_Empleados.SelectedIndex = 0;
                Txt_Cometarios.Text = "";
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "El Empleado ya esta Agregado.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Resguardante_Click
    ///DESCRIPCIÓN: Quita un Empleado resguardante para este Vehículo (No en la Base de datos
    ///             aun).
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 03/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Quitar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Grid_Resguardantes.Rows.Count > 0 && Grid_Resguardantes.SelectedIndex > (-1))
            {
                Int32 Registro = ((Grid_Resguardantes.PageIndex) * Grid_Resguardantes.PageSize) + (Grid_Resguardantes.SelectedIndex);
                if (Session["Dt_Resguardantes"] != null)
                {
                    DataTable Tabla = (DataTable)Session["Dt_Resguardantes"];
                    Tabla.Rows.RemoveAt(Registro);
                    Session["Dt_Resguardantes"] = Tabla;
                    Grid_Resguardantes.SelectedIndex = (-1);
                    Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Quitar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion
    
    #region "Busqueda Resguardantes"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Resguardante_Click
        ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Avanzada_Resguardante_Click(object sender, ImageClickEventArgs e) {
            try {
                Hdf_Tipo_Busqueda.Value = "";
                MPE_Resguardante.Show();
            }catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
        ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Resguardo_Completo_Operador_Click(object sender, ImageClickEventArgs e) {
            try {
                Hdf_Tipo_Busqueda.Value = "OPERADOR";
                MPE_Resguardante.Show();
            }catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
        ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Resguardo_Completo_Funcionario_Recibe_Click(object sender, ImageClickEventArgs e) {
            try {
                Hdf_Tipo_Busqueda.Value = "FUNCIONARIO_RECIBE";
                MPE_Resguardante.Show();
            }catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
    
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Autorizo_Click
        ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Resguardo_Completo_Autorizo_Click(object sender, ImageClickEventArgs e) {
            try {
                Hdf_Tipo_Busqueda.Value = "AUTORIZO";
                MPE_Resguardante.Show();
            }catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
        ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 24/Octubre/2011 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************    
        protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e) {
            try {
                Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
                Llenar_Grid_Busqueda_Empleados_Resguardo();
                MPE_Resguardante.Show();
            }  catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = Ex.Message;
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

    #endregion
    

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Asy_Cargar_Archivo_Complete
    ///DESCRIPCIÓN: Maneja el evento de cuando se cargo completamente el archivo
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 01/Noviembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try {
            string savePath = MapPath("~/Uploads/" + Path.GetFileName(e.FileName));//Se obtiene la ruta del archivo cargado
            //Se valida la extension del archivo
            if (Path.GetExtension(e.FileName).Contains(".jpg") || Path.GetExtension(e.FileName).Contains(".JPG") || Path.GetExtension(e.FileName).Contains(".jpeg") || Path.GetExtension(e.FileName).Contains(".JPEG"))
                Archivo = (AsyncFileUpload)sender; //Se almacena el archivo en la variable global llamada Archivo 
        } catch (Exception ex) {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al cargar la imagen " + ex.Message;
        }
    }





    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Lanzar_Mpe_Proveedores_Click
    ///DESCRIPCIÓN: Lanza buscador de producto.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 18/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Lanzar_Mpe_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        try  {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            Mpe_Proveedores_Cabecera.Show();
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_Proveedores_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda de los proveedores.
    ///PARAMETROS:     
    ///CREO:        Salvador Hernández Ramírez
    ///FECHA_CREO:  22/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ejecutar_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e) {
        try {
            Llenar_Grid_Proveedores(0);
        } catch (Exception Ex)  {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Nombre_Proveedor_Buscar_TextChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Texto del Nombre del proveedor.
    ///PARAMETROS:     
    ///CREO:        Salvador Hernández Ramírez
    ///FECHA_CREO:  08/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Txt_Nombre_Proveedor_Buscar_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Proveedores(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Archivo_Click
    ///DESCRIPCIÓN: Se agrega un archivo a la sesion
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Archivo_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Tabla_Archivos;
        if (Session["Tabla_Archivos"] != null)
        {
            Tabla_Archivos = (DataTable)Session["Tabla_Archivos"];
        }
        else
        {
            Tabla_Archivos = Generar_Tabla_Archivos();
        }
        String Checksum_Archivo = null;

        try
        {
            if (Validar_Archivo())
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");//No se para que demonios hace esto
                Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(AFU_Archivo.FileBytes));   //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo

                String Directorio = "../../" + Ope_Pat_Archivos_Bienes.Campo_Ruta_Fisica_Archivos + "/VEHICULOS/" + Hdf_Vehiculo_ID.Value.ToString().Trim();
                String Ruta_archivo = Server.MapPath(Directorio);
                if (!Directory.Exists(Ruta_archivo))
                {
                    Directory.CreateDirectory(Ruta_archivo);
                }
                Ruta_archivo = @Directorio + AFU_Archivo.FileName;

                if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                {
                    Diccionario_Archivos.Add(Checksum_Archivo, AFU_Archivo.FileBytes);
                    Session["Diccionario_Archivos"] = Diccionario_Archivos;

                    DataRow Nueva_Fila = Tabla_Archivos.NewRow();
                    Nueva_Fila["ARCHIVO_BIEN_ID"] = @AFU_Archivo.PostedFile.FileName;
                    Nueva_Fila["BIEN_ID"] = "";
                    Nueva_Fila["TIPO"] = Cmb_Tipo_Activo.SelectedItem.ToString().Trim();
                    Nueva_Fila["FECHA"] = DateTime.Today;
                    Nueva_Fila["ARCHIVO"] = AFU_Archivo.FileName;
                    Nueva_Fila["TIPO_ARCHIVO"] = "NORMAL";
                    Nueva_Fila["DESCRIPCION_ARCHIVO"] = "";
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;
                    Nueva_Fila["RUTA_ARCHIVO"] = @AFU_Archivo.PostedFile.FileName;
                    Tabla_Archivos.Rows.Add(Nueva_Fila);
                }
                Llenar_Grid_Historial_Archivos(0, Tabla_Archivos);
                Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    #endregion


}