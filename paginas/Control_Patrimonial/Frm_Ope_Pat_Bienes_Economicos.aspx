﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Bienes_Economicos.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Pat_Bienes_Economicos"
    Title="Control de Resguardos para Bienes Ecónomicos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        //Metodo para mantener los calendarios en una capa mas alat.
        function calendarShown(sender, args) {
            sender._popupBehavior._element.style.zIndex = 10000005;
        }
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            return false;
        }
        function Limpiar_Ctlr_Busqueda_Bien_Generales() {
            document.getElementById("<%=Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Bien_Mueble_Producto.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Bien_Mueble_Dependencias.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Bien_Mueble_Modelo.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Bien_Mueble_Marca.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Bien_Mueble_Estatus.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Bien_Mueble_Numero_Serie.ClientID%>").value = "";
            return false;
        }
        function Limpiar_Ctlr_Busqueda_Bien_Resguardantes() {
            document.getElementById("<%=Txt_Busqueda_Bien_Mueble_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias.ClientID%>").value = "";
            Inicializar_Combo(document.getElementById("<%=Cmb_Busqueda_Bien_Mueble_Nombre_Resguardante.ClientID%>"));  
            return false;
        }
        function Inicializar_Combo(Combo) {
            var No_items = Combo.options.length;
            for (i = No_items - 1; i >= 1; i--) {
                Combo.options.remove(i);
            }
            return false;
        }
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Bienes_Sin_Inventarios" runat="server"
        EnableScriptGlobalization="true" EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="UpD_Panel_General" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="UpP_Panel_General" runat="server" AssociatedUpdatePanelID="UpD_Panel_General"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Cabecera" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                            Control de Resguardos para Bienes Ecónomicos
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;" class="estilo_fuente">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr class="barra_busqueda">
                        <td style="width: 50%; text-align: left;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                Width="24px" CssClass="Img_Button" ToolTip="Nuevo" AlternateText="Nuevo" OnClick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                Width="24px" CssClass="Img_Button" ToolTip="Modificar" AlternateText="Modificar"
                                OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                Width="24px" CssClass="Img_Button" ToolTip="Imprimir" AlternateText="Imprimir" OnClick="Btn_Imprimir_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" ToolTip="Salir" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width: 50%; text-align: right;">
                            <asp:Panel ID="Div_Busqueda" runat="server" DefaultButton="Btn_Busqueda_Directa">
                                <asp:LinkButton ID="Lnk_Busqueda_Avanzada" runat="server" ForeColor="White" OnClick="Lnk_Busqueda_Avanzada_Click"
                                    ToolTip="Avanzada">Busqueda</asp:LinkButton>
                                &nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Busqueda_Clave" runat="server" Width="180px" Style="text-align: center;"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Clave" runat="server" TargetControlID="Txt_Busqueda_Clave"
                                    FilterType="Numbers">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Clave" runat="server" TargetControlID="Txt_Busqueda_Clave"
                                    WatermarkText="<- Clave ->" WatermarkCssClass="watermarked">
                                </cc1:TextBoxWatermarkExtender>
                                <asp:ImageButton ID="Btn_Busqueda_Directa" runat="server" ToolTip="Buscar Resguardo"
                                    AlternateText="Buscar Resguardo" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                    OnClick="Btn_Busqueda_Directa_Click" />
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <cc1:TabContainer ID="Tab_Contenedor_Pestagnas" runat="server" Width="98%" ActiveTabIndex="0"
                CssClass="Tab" Style="visibility: visible;">
                <cc1:TabPanel runat="server" HeaderText="Tab_Bienes_Detalles" ID="Tab_Bienes_Detalles"
                    Width="100%">
                    <HeaderTemplate>
                        Datos Generales</HeaderTemplate>
                    <ContentTemplate>
                        <div id="Div_Generales" style="background-color: #ffffff; width: 100%; height: 100%;">
                            <table width="98%" class="estilo_fuente">
                                <caption class="label_titulo">
                                    Datos Generales</caption>
                                <tr>
                                    <td colspan="4">
                                        <asp:HiddenField ID="Hdf_Bien_ID" runat="server" />
                                        <asp:HiddenField ID="Hdf_Producto_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td align="center" colspan="4">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Clave_Resguardo" runat="server" Text="Clave de Resguardo"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Clave_Resguardo" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                    </td>
                                    <td colspan="2">
                                        <asp:Label ID="Lbl_Clave_Resguardo_Automatico" runat="server" Text="[La Clave se da automaticamente al Guardar el Bien]"
                                            Font-Size="XX-Small" Font-Bold="True"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Bien" runat="server" Text="Nombre"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Nombre_Bien" runat="server" Width="92%" Enabled="False"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Lanzar_Buscar_Producto" runat="server" AlternateText="Buscar Producto a dar da Alta"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Lanzar_Buscar_Producto_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Label1" runat="server" Text="Gerencia "></asp:Label>
                                    </td>
                                    <td colspan="3" align="left">
                                        <asp:DropDownList ID="Cmb_Gerencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Gerencias_SelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Dependencia" runat="server" Text="U. Responsable"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:DropDownList ID="Cmb_Dependencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Dependencias_SelectedIndexChanged"
                                            AutoPostBack="True">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Marca" runat="server" Text="Marca"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList runat="server" ID="Cmb_Marca" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt; SELECCIONE &gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Modelo" runat="server" Text="Modelo"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Modelo" runat="server" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Modelo" runat="server" TargetControlID="Txt_Modelo"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_!%&/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Material" runat="server" Text="Material"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList runat="server" ID="Cmb_Material" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt; SELECCIONE &gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Color" runat="server" Text="Color"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList runat="server" ID="Cmb_Color" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt; SELECCIONE &gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Numero_Serie" runat="server" Text="Número Serie"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Numero_Serie" runat="server" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Serie" runat="server" TargetControlID="Txt_Numero_Serie"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_!%&/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Cantidad" runat="server" Text="Cantidad"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Cantidad" runat="server" Width="99%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cantidad" runat="server" Enabled="True" FilterType="Numbers" TargetControlID="Txt_Cantidad">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Costo" runat="server" Text="Costo [$]"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Costo" runat="server" Width="50%"></asp:TextBox>
                                        <asp:RegularExpressionValidator ID="REV_Txt_Costo" runat="server" ErrorMessage="[No Valido Verificar]"
                                            ControlToValidate="Txt_Costo" ValidationExpression="^\d+(\.\d\d)?$" Font-Size="X-Small"></asp:RegularExpressionValidator>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Costo" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                            ValidChars="." TargetControlID="Txt_Costo">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Costo" runat="server" TargetControlID="Txt_Costo"
                                            WatermarkText="<- 0.00 ->" WatermarkCssClass="watermarked" Enabled="True">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Fecha_Adquisicion" runat="server" Text="F. Adquisición"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:TextBox ID="Txt_Fecha_Adquisicion" runat="server" Width="90%" Enabled="False"
                                            MaxLength="20"></asp:TextBox>
                                        <asp:ImageButton ID="Btn_Fecha_Adquisicion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Adquisicion" runat="server" TargetControlID="Txt_Fecha_Adquisicion"
                                            PopupButtonID="Btn_Fecha_Adquisicion" Format="dd/MMM/yyyy" Enabled="True">
                                        </cc1:CalendarExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%" AutoPostBack="True"
                                            OnSelectedIndexChanged="Cmb_Estatus_SelectedIndexChanged">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                            <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                            <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Estado" runat="server" Text="Estado"></asp:Label>
                                    </td>
                                    <td style="width: 35%">
                                        <asp:DropDownList ID="Cmb_Estado" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            <asp:ListItem Value="BUENO">BUENO</asp:ListItem>
                                            <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>
                                            <asp:ListItem Value="MALO">MALO</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:Label ID="Lbl_Comentarios" runat="server" Text="Observaciones"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Comentarios" runat="server" Width="99%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios" runat="server" TargetControlID="Txt_Comentarios"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_!%&/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios" runat="server" TargetControlID="Txt_Comentarios"
                                            WatermarkText="Límite de Caractes 500" WatermarkCssClass="watermarked" Enabled="True" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top;">
                                        <asp:Label ID="Lbl_Motivo_Baja" runat="server" Text="Motivo Baja"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Motivo_Baja" runat="server" Width="99%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Motivo_Baja" runat="server" TargetControlID="Txt_Motivo_Baja"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_!%&/" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Motivo_Baja" runat="server" TargetControlID="Txt_Motivo_Baja"
                                            WatermarkText="Límite de Caractes 250" WatermarkCssClass="watermarked" Enabled="True" />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div id="Div_Resguardos" runat="server" style="width: 97%;">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td class="label_titulo" colspan="4">
                                        Resguardatarios
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td colspan="4" align="center">
                                    </td>
                                </tr>
                                <tr align="left">
                                    <td style="width: 18%; text-align: left;">
                                        <asp:Label ID="Lbl_Resguardantes" runat="server" Text="Empleados"></asp:Label>
                                    </td>
                                    <td colspan="3" style="text-align: left;">
                                        <asp:DropDownList ID="Cmb_Empleados" runat="server" Width="100%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 18%; text-align: left; vertical-align: top;">
                                        <asp:Label ID="Lbl_Comentarios_Resguardo" runat="server" Text="Comentarios"></asp:Label>
                                    </td>
                                    <td colspan="3" style="text-align: right;">
                                        <asp:TextBox ID="Txt_Cometarios" runat="server" Rows="3" TextMode="MultiLine" Width="99%"
                                            MaxLength="100"></asp:TextBox>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cometarios" runat="server" Enabled="True"
                                            TargetControlID="Txt_Cometarios" WatermarkCssClass="watermarked" WatermarkText="Límite de Caracteres 255">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <hr />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 18%; text-align: left; vertical-align: top;">
                                        <asp:ImageButton ID="Btn_Busqueda_Avanzada_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                            Width="24px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Busqueda_Avanzada_Resguardante_Click" />
                                    </td>
                                    <td colspan="3" style="text-align: right;">
                                        <asp:ImageButton ID="Btn_Agregar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                            ToolTip="Agregar" AlternateText="Agregar" OnClick="Btn_Agregar_Resguardante_Click" />
                                        <asp:ImageButton ID="Btn_Quitar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/quitar.png"
                                            ToolTip="Quitar" AlternateText="Quitar" OnClick="Btn_Quitar_Resguardante_Click" />
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                        <hr />
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <br />
                        <div id="Div_Listado_Resguardantes" runat="server" style="width: 97%;">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td align="center" colspan="4">
                                        <br />
                                        <asp:GridView ID="Grid_Resguardantes" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                            CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Resguardantes_PageIndexChanging"
                                            PageSize="5" Width="97.5%" CssClass="GridView_1">
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                            <Columns>
                                                <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                    <ItemStyle Width="30px" />
                                                </asp:ButtonField>
                                                <asp:BoundField DataField="EMPLEADO_ALMACEN_ID" HeaderText="EMPLEADO ALMACEN" SortExpression="EMPLEADO_RESGUARDO_ID">
                                                    <ItemStyle Width="30px" Font-Size="X-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="X-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle Width="100px" HorizontalAlign="Left" Font-Size="X-Small" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre del Empleado" SortExpression="NOMBRE_EMPLEADO">
                                                    <HeaderStyle HorizontalAlign="Center" />
                                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                                </asp:BoundField>
                                            </Columns>
                                            <HeaderStyle CssClass="GridHeader" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <RowStyle CssClass="GridItem" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                        </asp:GridView>
                                        <br />
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </ContentTemplate>
                </cc1:TabPanel>
                <cc1:TabPanel runat="server" HeaderText="Tab_Historial_Resguardos" ID="Tab_Historial_Resguardos"
                    Width="100%">
                    <HeaderTemplate>
                        Historial</HeaderTemplate>
                    <ContentTemplate>
                        <center>
                            <table width="100%">
                                <tr>
                                    <td class="label_titulo" colspan="4">
                                        Modificaciones
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td colspan="4" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; width: 18%">
                                        <asp:Label ID="Lbl_Usuario_Creo" runat="server" Text="Creación" CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Usuario_creo" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="text-align: left; vertical-align: top; width: 18%">
                                        <asp:Label ID="Lbl_Usuario_Modifico" runat="server" Text="Ultima Modificación" CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td colspan="3">
                                        <asp:TextBox ID="Txt_Usuario_Modifico" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td colspan="4" align="center">
                                    </td>
                                </tr>
                                <tr style="background-color: Silver;">
                                    <td class="label_titulo" style="color: Black; text-align: right; vertical-align: middle;"
                                        colspan="4">
                                        Ver Reporte de Cambios&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                        <asp:ImageButton ID="Btn_Generar_Reporte_Cambios" runat="server" ToolTip="Ver Reporte de Cambios"
                                            ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Height="16px" Width="16px"
                                            CssClass="Img_Button" AlternateText="Ver Reporte de Cambios" OnClick="Btn_Generar_Reporte_Cambios_Click" />&nbsp;&nbsp;
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td colspan="4" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td class="label_titulo" colspan="4">
                                        Historial de Resguardatarios
                                    </td>
                                </tr>
                                <tr align="right" class="barra_delgada">
                                    <td colspan="4" align="center">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 18%; text-align: left;">
                                        <asp:Label ID="Lbl_Historial_Empleado_Resguardo" runat="server" Text="Empleado" CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td colspan="3" style="text-align: left;">
                                        <asp:TextBox ID="Txt_Historial_Empleado_Resguardo" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 18%; text-align: left;">
                                        <asp:Label ID="Lbl_Historial_Fecha_Inicial_Resguardo" runat="server" Text="Fecha Inicial"
                                            CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td style="width: 32%; text-align: left;">
                                        <asp:TextBox ID="Txt_Historial_Fecha_Inicial_Resguardo" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                    <td style="width: 18%; text-align: left;">
                                        <asp:Label ID="Lbl_Historial_Fecha_Final_Resguardo" runat="server" Text="Fecha Final"
                                            CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td style="width: 32%; text-align: left;">
                                        <asp:TextBox ID="Txt_Historial_Fecha_Final_Resguardo" runat="server" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 18%; text-align: left; vertical-align: top;">
                                        <asp:Label ID="Lbl_Historial_Comentarios_Resguardo" runat="server" Text="Comentarios"
                                            CssClass="estilo_fuente"></asp:Label>
                                    </td>
                                    <td colspan="3" style="text-align: left;">
                                        <asp:TextBox ID="Txt_Historial_Comentarios_Resguardo" runat="server" TextMode="MultiLine"
                                            Rows="3" Width="98%" Enabled="false"></asp:TextBox>
                                    </td>
                                </tr>
                            </table>
                            <br />
                            <asp:GridView ID="Grid_Historial_Resguardantes" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="98%"
                                OnPageIndexChanging="Grid_Historial_Resguardantes_PageIndexChanging" OnSelectedIndexChanged="Grid_Historial_Resguardantes_SelectedIndexChanged"
                                PageSize="10">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="20px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="BIEN_RESGUARDO_ID" HeaderText="BIEN_RESGUARDO_ID" SortExpression="BIEN_RESGUARDO_ID" />
                                    <asp:BoundField DataField="EMPLEADO_ID" HeaderText="Empleado ID" SortExpression="EMPLEADO_ID" />
                                    <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                        <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre" SortExpression="NOMBRE_EMPLEADO">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </center>
                    </ContentTemplate>
                </cc1:TabPanel>
            </cc1:TabContainer>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Productos" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Mpe_Productos_Cabecera" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Productos_Cabecera" runat="server" TargetControlID="Btn_Comodin_Mpe_Productos_Cabecera"
                PopupControlID="Pnl_Mpe_Productos" CancelControlID="Btn_Cerrar_Mpe_Productos"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Productos" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Productos_Interno" runat="server" Style="cursor: move; background-color: Silver;
            color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Img_Mpe_Productos" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda y Selección de Productos
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Productos" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Productos" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Productos" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Productos"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <asp:Panel ID="Pnl_Busqueda_Productos" runat="server" Width="99%" DefaultButton="Btn_Ejecutar_Busqueda_Productos">
                            <table width="98%">
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Producto_Buscar" runat="server" Text="Nombre" Style="font-weight: bolder;"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="Txt_Nombre_Producto_Buscar" runat="server" Width="90%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Producto_Buscar" runat="server" TargetControlID="Txt_Nombre_Producto_Buscar"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Nombre_Producto_Buscar" runat="server"
                                            Enabled="True" TargetControlID="Txt_Nombre_Producto_Buscar" WatermarkCssClass="watermarked"
                                            WatermarkText="<-- TODOS -->">
                                        </cc1:TextBoxWatermarkExtender>
                                        <asp:ImageButton ID="Btn_Ejecutar_Busqueda_Productos" runat="server" OnClick="Btn_Ejecutar_Busqueda_Productos_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="Pnl_Listado_Bienes" runat="server" ScrollBars="Vertical" Style="white-space: normal;"
                            Width="95%" BorderColor="#3366FF" Height="300px">
                            <asp:GridView ID="Grid_Listado_Productos" runat="server" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Listado_Productos_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Productos_SelectedIndexChanged" PageSize="5"
                                Width="97.5%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PRODUCTO_ID" HeaderText="Producto ID" SortExpression="PRODUCTO_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="CLAVE_PRODUCTO" HeaderText="Clave" SortExpression="CLAVE_PRODUCTO">
                                        <ItemStyle Width="100px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE_PRODUCTO" HeaderText="Nombre del Producto" SortExpression="NOMBRE_PRODUCTO">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="DESCRIPCION_PRODUCTO" HeaderText="Descripcion" SortExpression="DESCRIPCION_PRODUCTO">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Panel_Filtro_Busqueda_Empleados" runat="server" Width="100%" DefaultButton="Btn_Busqueda_Empleados">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Empleado
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Empleado" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                    WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                RFC
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                    TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                    WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Nombre
                                            </td>
                                            <td style="width: 30%; text-align: left;" colspan="3">
                                                <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                    FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    ValidChars="áéíóúÁÉÍÓÚ .-" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Bien" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Busqueda_Bien" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Bien" runat="server" TargetControlID="Btn_Comodin_Busqueda_Bien"
                BackgroundCssClass="progressBackgroundFilter" CancelControlID="Btn_Cerrar_Busqueda_Bien"
                PopupControlID="Pnl_Busqueda_Bien" DropShadow="True" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Bien" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="border-style: outset; border-color: Silver; width: 760px;">
        <center>
            <asp:Panel ID="Pnl_Busqueda_Bien_Interno" runat="server" CssClass="estilo_fuente"
                Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
                border-style: outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold; width: 90%; border-color: Black;">
                            <asp:Image ID="Img_Encabezado_Busqueda_Bienes_Muebles" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Busqueda de Bienes Muebles
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="Btn_Cerrar_Busqueda_Bien" CausesValidation="false" runat="server"
                                Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Bien" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrg_Busqueda_Bien" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Bien"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Bien" runat="server" ActiveTabIndex="0"
                        CssClass="Tab" Width="100%">
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Datos_Generales" ID="Tab_Panel_Datos_Generales_Busqueda_Bien"
                            Width="100%" Height="400px" BackColor="White">
                            <HeaderTemplate>
                                Datos Generales</HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="Pnl_Busqueda_Bien_Generales" runat="server" DefaultButton="Btn_Buscar_Datos_Bien_Mueble" style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Numero_Inventario_SIAS" runat="server" Text="Clave de Resguardo"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS" runat="server"
                                                    Width="95%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS"
                                                    runat="server" TargetControlID="Txt_Busqueda_Bien_Mueble_Numero_Inventario_SIAS"
                                                    InvalidChars="<,>,&,',!," FilterType="Numbers" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Producto" runat="server" Text="Nombre Producto"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Bien_Mueble_Producto" runat="server" Width="98%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bien_Mueble_Producto" runat="server"
                                                    TargetControlID="Txt_Busqueda_Bien_Mueble_Producto" InvalidChars="<,>,&,',!,"
                                                    FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Dependencias" runat="server" Text="U. Responsable"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Bien_Mueble_Dependencias" runat="server" Width="99%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Bien_Mueble_Modelo" runat="server" Width="98%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bien_Mueble_Modelo" runat="server"
                                                    FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" TargetControlID="Txt_Busqueda_Bien_Mueble_Modelo"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-_%/$# " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Marca" runat="server" Text="Marca" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Bien_Mueble_Marca" runat="server" Width="98%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Estatus" runat="server" Text="Estatus" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Bien_Mueble_Estatus" runat="server" Width="98%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                    <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                                    <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                                    <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Numero_Serie" runat="server" Text="No. Serie"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Bien_Mueble_Numero_Serie" runat="server" Width="98%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bien_Mueble_Numero_Serie" runat="server"
                                                    TargetControlID="Txt_Busqueda_Bien_Mueble_Numero_Serie" InvalidChars="<,>,&,',!,"
                                                    FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-+$%&"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right;">
                                                <asp:ImageButton ID="Btn_Buscar_Datos_Bien_Mueble" runat="server" OnClick="Btn_Buscar_Datos_Bien_Mueble_Click"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" CausesValidation="False" ToolTip="Buscar" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Bien_Mueble" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Bien_Generales();"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    ToolTip="Limpiar Filtros" Width="20px" />
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Busqueda_Bien_Mueble_Reguardantes"
                            ID="Tab_Panel_Busqueda_Bien_Mueble_Resguardantes_Busqueda" Width="100%">
                            <HeaderTemplate>
                                Resguardatario</HeaderTemplate>
                            <ContentTemplate>
                                <asp:Panel ID="Pnl_Busqueda_Bien_Resguadantes" runat="server" DefaultButton="Btn_Buscar_Resguardante_Bien_Mueble" style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left;" colspan="4">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_No_Empleado" runat="server" Text="No. Empleado"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Bien_Mueble_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Bien_Mueble_No_Empleado" runat="server"
                                                    TargetControlID="Txt_Busqueda_Bien_Mueble_No_Empleado" InvalidChars="<,>,&,',!,"
                                                    FilterType="Numbers" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                &nbsp;
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Resguardantes_Dependencias" runat="server"
                                                    Text="U. Responsable" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias" runat="server"
                                                    Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Busqueda_Bien_Mueble_Resguardantes_Dependencias_SelectedIndexChanged">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Bien_Mueble_Nombre_Resguardante" runat="server" Text="Resguardante"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Bien_Mueble_Nombre_Resguardante" runat="server"
                                                    Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right;">
                                                <asp:ImageButton ID="Btn_Buscar_Resguardante_Bien_Mueble" runat="server" OnClick="Btn_Buscar_Resguardante_Bien_Mueble_Click"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Bien_Mueble" runat="server" OnClientClick="javascript:return Limpiar_Ctlr_Busqueda_Bien_Resguardantes();"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    Width="20px" ToolTip="Limpiar Filtros" />
                                                &nbsp;&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                    <div style="width: 99%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                        <center>
                            <caption>
                                <asp:GridView ID="Grid_Listado_Bienes_Bien_Mueble" runat="server" AutoGenerateColumns="False"
                                    CssClass="GridView_1" GridLines="None" OnSelectedIndexChanged="Grid_Listado_Bienes_Bien_Mueble_SelectedIndexChanged"
                                    OnPageIndexChanging="Grid_Listado_Bienes_Bien_Mueble_PageIndexChanging" AllowPaging="true"
                                    PageSize="100">
                                    <RowStyle CssClass="GridItem" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NOMBRE_PRODUCTO" HeaderText="Nombre" SortExpression="NOMBRE_PRODUCTO">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MATERIAL" HeaderText="Material" SortExpression="MATERIAL">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="COLOR" HeaderText="Color" SortExpression="COLOR">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_SERIE" HeaderText="No. Serie" SortExpression="NUMERO_SERIE">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                </asp:GridView>
                            </caption>
                        </center>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
