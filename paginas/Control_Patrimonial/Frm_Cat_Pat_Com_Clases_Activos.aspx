﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Pat_Com_Clases_Activos.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Pat_Com_Clases_Activos" Title="Catalogo de Clases de Activos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <%--liberias pera autocompletar--%>
    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" language="javascript">
        //Funcion que ejecuta el evento del boton de buscar mediante la tecla enter en la caja de texto que implementa la funcion
        function get_KeyPress(textbox, evento, tipo) {
            //debugger;
            var keyCode;
            if (evento.which || evento.charCode) {
                keyCode = evento.which ? evento.which : evento.charCode;
                //return (keyCode != 13);
            }
            else if (window.event) {
                keyCode = event.keyCode;
                if (keyCode == 13) {
                    if (event.keyCode)
                        event.keyCode = 9;
                }
            }

            if (keyCode == 13) {
                if (tipo == "BUSQUEDA_GENERAL") { document.getElementById('<%=Btn_Buscar.ClientID %>').click(); }
                if (tipo == "BUSQUEDA_PARTIDA") { document.getElementById('<%=Btn_Buscar_Partida.ClientID %>').click(); }
                    
                window.focus();
                return false;
            }
            return true;
        }
</script> 
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
   <cc1:ToolkitScriptManager ID="ScptM_Clases_Activos" runat="server" />  
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                    </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:98%; height:100%;">
                <table width="100%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Catálogo de Clases de Activos</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo"
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Nuevo" OnClick="Btn_Nuevo_Click"/>
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar"
                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" onclick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar"
                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Eliminar" OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro de la Base de Datos?');"
                                onclick="Btn_Eliminar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" onclick="Btn_Salir_Click"/>
                        </td>
                        <td>Busqueda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180px" onkeypress="return get_KeyPress(this,event, 'BUSQUEDA_GENERAL');"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Consultar"
                                onclick="Btn_Buscar_Click" ToolTip="Buscar" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" 
                                WatermarkText="<- Clave ó Descripción ->" TargetControlID="Txt_Busqueda" 
                                WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" 
                                TargetControlID="Txt_Busqueda" InvalidChars="<,>,&,',!," 
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>                                  
                        </td>                        
                    </tr>
                </table>   
                <br />
                <center>
                    <table width="100%" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_Clase_Activo_ID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width:13%; text-align:left;">
                                <asp:Label ID="Lbl_Clasificacion_ID" runat="server" Text="Clase Activo ID" ></asp:Label></td>
                            <td style="width:37%; text-align:left;">
                                <asp:TextBox ID="Txt_Clase_Activo_ID" runat="server" Width="50%" MaxLength="10" Enabled="False"></asp:TextBox>
                            </td>
                            <td colspan="2">&nbsp;</td>
                        </tr>  
                        <tr>
                            <td style="width:13%; text-align:left;">
                               <asp:Label ID="Lbl_Clave" runat="server" Text="* Clave" ></asp:Label>
                            </td>
                            <td style="width:37%; text-align:left; ">
                                 <asp:TextBox ID="Txt_Clave" runat="server" Width="99%"></asp:TextBox>
                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave" runat="server" TargetControlID="Txt_Clave" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:MaskedEditExtender DisplayMoney="None" ID="MEE_Txt_Clave"  runat="server" TargetControlID="Txt_Clave" Mask="9999" InputDirection="LeftToRight" ></cc1:MaskedEditExtender>
                            </td>
                            <td style="width:13%; text-align:left;">
                               <asp:Label ID="Lbl_Estatus" runat="server" Text="* Estatus" ></asp:Label>
                            </td>
                            <td style="width:37%; text-align:left; ">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%">
                                    <asp:ListItem Text ="&lt;SELECCIONE&gt;" Value="SELECCIONE"></asp:ListItem>
                                    <asp:ListItem Text ="VIGENTE" Value="VIGENTE"></asp:ListItem>
                                    <asp:ListItem Text ="BAJA" Value="BAJA"></asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>                                  
                        <tr>
                            <td style="width:13%; text-align:left; ">
                               <asp:Label ID="Lbl_Descripcion" runat="server" Text="* Descripción" ></asp:Label>
                            </td>
                            <td colspan="3" style="text-align:left;">
                                <asp:TextBox ID="Txt_Descripcion" runat="server" Width="99.5%" MaxLength="50"></asp:TextBox>
                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Descripcion" runat="server" 
                                    TargetControlID="Txt_Descripcion" InvalidChars="<,>,&,',!," 
                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_!#$%">
                                </cc1:FilteredTextBoxExtender>   
                            </td>
                        </tr>
                        <tr>
                            <td style="width:13%; text-align:left; ">
                               <asp:Label ID="Lbl_Vida_Util" runat="server" Text="* Vida Útil" ></asp:Label>
                            </td>
                            <td style="text-align:left;">
                                <asp:TextBox ID="Txt_Anios_Vida_Util" runat="server" Width="80%" MaxLength="4" style="text-align:center;"></asp:TextBox>
                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anios_Vida_Util" runat="server" 
                                    TargetControlID="Txt_Anios_Vida_Util" 
                                    FilterType="Numbers" >
                                </cc1:FilteredTextBoxExtender>   
                                <asp:Label ID="Lbl_Anios_Vida_Util" runat="server" Text="[Años]" ></asp:Label>
                            </td>
                            <td >
                               <asp:Label ID="Lbl_Porcentaje_Depreciacion" runat="server" Text="* Depreciación" ></asp:Label>
                            </td>
                            <td  style="text-align:left;">
                                <asp:TextBox ID="Txt_Porcentaje_Depreciacion" runat="server" Width="75%" MaxLength="6" style="text-align:center;"></asp:TextBox>
                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Porcentaje_Depreciacion" runat="server" 
                                    TargetControlID="Txt_Porcentaje_Depreciacion"  
                                    FilterType="Custom, Numbers" 
                                    ValidChars="." >
                                </cc1:FilteredTextBoxExtender>   
                                <asp:Label ID="Lbl_Porcentaje_Anual" runat="server" Text="[% Anual]" ></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td style="width:13%; text-align:left; ">
                                <asp:HiddenField ID="Hdf_Partida_ID" runat="server" />
                               <asp:Label ID="Lbl_Partida" runat="server" Text="* Partida" ></asp:Label>
                            </td>
                            <td  colspan="3">
                                <asp:TextBox ID="Txt_Clave_Partida" runat="server" Width="10%" MaxLength="4"  onkeypress="return get_KeyPress(this,event, 'BUSQUEDA_PARTIDA');"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Buscar_Partida" runat="server" CausesValidation="false"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" AlternateText="Consultar" Width="20px"
                                onclick="Btn_Buscar_Partida_Click" ToolTip="Buscar" />
                                 <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Partida" runat="server" 
                                    TargetControlID="Txt_Clave_Partida" 
                                    FilterType="Numbers" >
                                </cc1:FilteredTextBoxExtender>&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Nombre_Partida" runat="server" Width="82%" Enabled="false"></asp:TextBox>
                            </td>
                        </tr>                                  
                        <tr>
                            <td style="width:13%; text-align:left; ">
                                <asp:HiddenField ID="Hdf_Cuenta_Contable_ID" runat="server" />
                                <asp:Label ID="Lbl_Cuenta_Depreciacion" runat="server" Text="* Cuenta Depreciación" ></asp:Label>
                            </td>
                            <td colspan="3" style="text-align:left;">
                                <asp:TextBox ID="Txt_Cuenta_Depreciacion" runat="server" Width="99.5%"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
                    <br />                                
                    <asp:GridView ID="Grid_Clases_Activos" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                        GridLines= "None"
                        onselectedindexchanged="Grid_Clases_Activos_SelectedIndexChanged" 
                        onpageindexchanging="Grid_Clases_Activos_PageIndexChanging">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                <ItemStyle Width="30px" />
                            </asp:ButtonField>
                            <asp:BoundField DataField="CLASE_ACTIVO_ID" HeaderText="Clase ID" SortExpression="CLASE_ACTIVO_ID" >
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="110px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CLAVE" HeaderText="Clave" SortExpression="CLAVE" >
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="110px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="DESCRIPCION" >
                                <ItemStyle Font-Size="X-Small" />
                                <HeaderStyle HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" 
                                SortExpression="ESTATUS" >
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="110px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="ANIOS_VIDA_UTIL" HeaderText="Vida Útil(Años)" 
                                SortExpression="ANIOS_VIDA_UTIL" >
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="110px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PORCENTAJE_DEPRECIACION" HeaderText=" % Depreciación" 
                                SortExpression="PORCENTAJE_DEPRECIACION"  >
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="110px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="PARTIDA_ID" HeaderText="PARTIDA_ID" 
                                SortExpression="PARTIDA_ID"  >
                                <ItemStyle Width="1px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="1px" HorizontalAlign="Center" />
                            </asp:BoundField>
                            <asp:BoundField DataField="CUENTA_DEPRECIACION_ID" HeaderText="CUENTA_DEPRECIACION_ID" 
                                SortExpression="CUENTA_DEPRECIACION_ID"  >
                                <ItemStyle Width="1px" HorizontalAlign="Center" Font-Size="X-Small" />
                                <HeaderStyle Width="1px" HorizontalAlign="Center" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </center>        
            </div>
            <br />
            <br />
            <br />
            <br />
            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Depreciacion']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
                        });
                    }
                } 
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>