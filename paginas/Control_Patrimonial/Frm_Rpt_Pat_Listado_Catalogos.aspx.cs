﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Sessiones;
using JAPAMI.Control_Patrimonial_Catalogo_Aseguradoras.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones_Zonas_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Destinos_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Detalles_Vehiculos.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Materiales.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Orientaciones_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Origenes_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Bajas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Combustible.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Vehiculo.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Siniestros.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Zonas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Usos_Inmuebles.Negocio;
using CarlosAg.ExcelXmlWriter;
using System.Reflection;
using System.Text;

public partial class paginas_Control_Patrimonial_Frm_Rpt_Pat_Listado_Catalogos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        if (String.IsNullOrEmpty(Cls_Sessiones.Empleado_ID) && String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
    }


    protected void Btn_Limpiar_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        try
        {
            Txt_Nombre.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "No se pudo Generar el Listado.";
            Lbl_Mensaje_Error.Text = "Verificar [" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    protected void Btn_Exportar_Excel_Click(object sender, ImageClickEventArgs e) 
    {
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        try {
            String Catalogo_Seleccionado = Cmb_Catalogo.SelectedItem.Value;
            DataTable Dt_Resultado = new DataTable();
            switch (Catalogo_Seleccionado) {
                case "ASEGURADORAS":
                    Cls_Cat_Pat_Com_Aseguradoras_Negocio Aseguradoras_Negocio = new Cls_Cat_Pat_Com_Aseguradoras_Negocio();
                    Aseguradoras_Negocio.P_Tipo_DataTable = "ASEGURADORAS";
                    Aseguradoras_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                    Aseguradoras_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Aseguradoras_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("ASEGURADORA_ID");
                    break;
                case "CLASES_ACTIVO":
                    Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                    CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                    CA_Negocio.P_Clave = Txt_Nombre.Text.Trim();
                    CA_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    CA_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = CA_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("CLASE_ACTIVO_ID");
                    Dt_Resultado.Columns.Remove("CLAVE_DESCRIPCION");
                    Dt_Resultado.Columns.Remove("DESCRIPCION_CLAVE");
                    Dt_Resultado.Columns.Remove("PARTIDA_ID");
                    Dt_Resultado.Columns.Remove("PARTIDA_CLAVE");
                    Dt_Resultado.Columns.Remove("PARTIDA_NOMBRE");
                    break;
                case "CLASIFICACION_ZONA":
                    Cls_Cat_Pat_Com_Clasificaciones_Zonas_Inmuebles_Negocio CZI_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Zonas_Inmuebles_Negocio();
                    CZI_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    CZI_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = CZI_Negocio.Consultar_Clasificaciones();
                    Dt_Resultado.Columns.Remove("CLASIFICACION_ID");
                    break;
                case "COLORES":
                    Cls_Cat_Pat_Com_Colores_Negocio Colores_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                    Colores_Negocio.P_Tipo_DataTable = "COLORES";
                    Colores_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Colores_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Colores_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("COLOR_ID");
                    break;
                case "DESTINOS_INMUEBLES":
                    Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio Destinos_Negocio = new Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio();
                    Destinos_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Destinos_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Destinos_Negocio.Consultar_Destinos();
                    Dt_Resultado.Columns.Remove("DESTINO_ID");
                    break;
                case "DETALLES_VEHICULOS":
                    Cls_Cat_Pat_Com_Detalles_Vehiculos_Negocio Detalles_Negocio = new Cls_Cat_Pat_Com_Detalles_Vehiculos_Negocio();
                    Detalles_Negocio.P_Tipo_DataTable = "DETALLES_VEHICULOS";
                    Detalles_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                    Detalles_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Detalles_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("DETALLE_ID");
                    break;
                case "MATERIALES":
                    Cls_Cat_Pat_Com_Materiales_Negocio Materiales_Negocio = new Cls_Cat_Pat_Com_Materiales_Negocio();
                    Materiales_Negocio.P_Tipo_DataTable = "MATERIALES";
                    Materiales_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Materiales_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Materiales_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("MATERIAL_ID");
                    break;
                case "ORIENTACIONES":
                    Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio Orientaciones_Negocio = new Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio();
                    Orientaciones_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Orientaciones_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Orientaciones_Negocio.Consultar_Orientaciones();
                    Dt_Resultado.Columns.Remove("ORIENTACION_ID");
                    break;
                case "ORIGENES_INMUEBLES":
                    Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio Origenes_Negocio = new Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio();
                    Origenes_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                    Origenes_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Origenes_Negocio.Consultar_Origenes();
                    Dt_Resultado.Columns.Remove("ORIGEN_ID");
                    break;
                case "PROCEDENCIAS":
                    Cls_Cat_Pat_Com_Procedencias_Negocio Procedencias_Negocio = new Cls_Cat_Pat_Com_Procedencias_Negocio();
                    Procedencias_Negocio.P_Tipo_DataTable = "PROCEDENCIAS";
                    Procedencias_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                    Procedencias_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Procedencias_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("PROCEDENCIA_ID");
                    break;
                case "TIPOS_ACTIVOS":
                    Cls_Cat_Pat_Com_Clasificaciones_Negocio TA_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
                    TA_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
                    TA_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    TA_Negocio.P_Clave = Txt_Nombre.Text.Trim();
                    TA_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = TA_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("CLASIFICACION_ID");
                    Dt_Resultado.Columns.Remove("CLAVE_DESCRIPCION");
                    Dt_Resultado.Columns.Remove("DESCRIPCION_CLAVE");
                    break;
                case "TIPOS_BAJAS":
                    Cls_Cat_Pat_Com_Tipos_Bajas_Negocio TB_Negocio = new Cls_Cat_Pat_Com_Tipos_Bajas_Negocio();
                    TB_Negocio.P_Tipo_DataTable = "TIPOS_BAJAS";
                    TB_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    TB_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = TB_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("TIPO_BAJA_ID");
                    break;
                case "TIPOS_COMBUSTIBLES":
                    Cls_Cat_Pat_Com_Tipos_Combustible_Negocio TC_Negocio = new Cls_Cat_Pat_Com_Tipos_Combustible_Negocio();
                    TC_Negocio.P_Tipo_DataTable = "TIPOS_COMBUSTIBLE";
                    TC_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    TC_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = TC_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("TIPO_COMBUSTIBLE_ID");
                    break;
                case "TIPOS_VEHICULOS":
                    Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio TV_Negocio = new Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio();
                    TV_Negocio.P_Tipo_DataTable = "TIPOS_VEHICULOS";
                    TV_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    TV_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = TV_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("TIPO_VEHICULO_ID");
                    Dt_Resultado.Columns.Remove("ASEGURADORA_ID");
                    break;
                case "TIPOS_SINIESTROS":
                    Cls_Cat_Pat_Com_Tipos_Siniestros_Negocio TS_Negocio = new Cls_Cat_Pat_Com_Tipos_Siniestros_Negocio();
                    TS_Negocio.P_Tipo_DataTable = "TIPOS_SINIESTROS";
                    TS_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    TS_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = TS_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("TIPO_SINIESTRO_ID");
                    break;
                case "UBICACIONES_FISICAS":
                    Cls_Cat_Pat_Com_Zonas_Negocio Zonas_Negocio = new Cls_Cat_Pat_Com_Zonas_Negocio();
                    Zonas_Negocio.P_Tipo_DataTable = "ZONAS";
                    Zonas_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Zonas_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Zonas_Negocio.Consultar_DataTable();
                    Dt_Resultado.Columns.Remove("ZONA_ID");
                    break;
                case "USOS_INMUEBLES":
                    Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio Usos_Inmuebles_Negocio = new Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio();
                    Usos_Inmuebles_Negocio.P_Descripcion = Txt_Nombre.Text.Trim();
                    Usos_Inmuebles_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
                    Dt_Resultado = Usos_Inmuebles_Negocio.Consultar_Usos();
                    Dt_Resultado.Columns.Remove("USO_ID");
                    break;
                default: break;
            };
            if (Dt_Resultado != null) {
                if (Dt_Resultado.Rows.Count > 0)
                {
                    Pasar_DataTable_A_Excel(Dt_Resultado, Cmb_Catalogo.SelectedItem.Value);
                }
                else
                {
                    throw new Exception("No hay registros que cumplan esos Filtros");
                }
            }
            else
            {
                throw new Exception("No hay registros que cumplan esos Filtros");
            }
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = "No se pudo Generar el Listado.";
            Lbl_Mensaje_Error.Text = "Verificar [" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    /// *************************************************************************************************************************
    /// Nombre: Pasar_DataTable_A_Excel
    /// 
    /// Descripción: Pasa DataTable a Excel.
    /// 
    /// Parámetros: Dt_Reporte.- DataTable que se pasara a excel.
    /// 
    /// Usuario Creo: Juan Alberto Hernández Negrete.
    /// Fecha Creó: 18/Octubre/2011.
    /// Usuario Modifico:
    /// Fecha Modifico:
    /// Causa Modificación:
    /// *************************************************************************************************************************
    public void Pasar_DataTable_A_Excel(System.Data.DataTable Dt_Reporte, String Nombre_Listado)
    {
        String Ruta = "SIAC_LISTADO_" + Nombre_Listado.Trim() + "_" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";

        try
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            Libro.Properties.Title = "SIAC_CONTROL_PATRIMONIAL";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "Departamento de Control Interno";

            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "#193d61";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true;

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 8;
            Estilo_Contenido.Font.Bold = true;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Alignment.WrapText = true;

            //Agregamos las columnas que tendrá la hoja de excel.
            for (Int32 Cont_Registros = 0; Cont_Registros < Dt_Reporte.Columns.Count; Cont_Registros++) {
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));
            }

            if (Dt_Reporte is System.Data.DataTable)
            {
                if (Dt_Reporte.Rows.Count > 0)
                {
                    foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                    {
                        if (COLUMNA is System.Data.DataColumn)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                        }
                        Renglon.Height = 15;
                    }

                    foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                    {
                        if (FILA is System.Data.DataRow)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    if (COLUMNA.ColumnName.Trim().Equals("IMPORTE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:#,###,##0.00}", Convert.ToDouble(FILA[COLUMNA.ColumnName])), DataType.String, "BodyStyle"));
                                    }
                                    else
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                }
                            }
                            Renglon.Height = 15;
                            Renglon.AutoFitHeight = true;
                        }
                    }
                }
            }

            //Abre el archivo de excel
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }


}
