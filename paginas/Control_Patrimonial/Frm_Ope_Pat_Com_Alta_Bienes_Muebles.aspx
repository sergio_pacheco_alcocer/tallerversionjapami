﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Com_Alta_Bienes_Muebles.aspx.cs"
    Inherits="paginas_predial_Frm_Ope_Pat_Com_Alta_Bienes_Muebles" Title="Operación - Asignación de Bienes Muebles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
    <%--liberias pera autocompletar--%>
    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" language="javascript">
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            return false;
        }
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr_Donador() {
            document.getElementById("<%=Hdf_Donador_ID.ClientID%>").value = "";
            document.getElementById("<%=Txt_Nombre_Donador.ClientID%>").value = "";
            return false;
        }

        function Carga_Completa(sender, args) {
            var archivo = args.get_fileName(); //Se asigna el archivo a la variable archivo
            if (archivo != "") {
                // se obtiene la extension del archivo
                var arreglo = new Array;
                arreglo = archivo.split("\\");
                var tamano = arreglo.length;
                var imagen = arreglo[tamano - 1];
                var extension = imagen.substring(imagen.lastIndexOf(".") + 1);
                if (extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG") {
                    if (args.get_length() > 2621440) {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("El Archivo " + archivo + ". Excedio el Tamaño Permitido:\n\nTamaño del Archivo: [" + args.get_length() + " Bytes]\nTamaño Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                        Limpiar_Contenido();
                        return false;
                    }
                    return true;
                } else {
                    var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                    alert("Tipo de archivo inválido " + archivo + "\n\nFormatos Validos [.jpg, .jpeg]" + mensaje);
                    Limpiar_Contenido();
                    return false;
                }
            } else {
                Limpiar_Contenido();
                return false;
            }
            return true;
        }

        function Limpiar_Contenido() {
            var AsyncFileUpload = $get("<%=AFU_Archivo.ClientID%>"); //Se obtiene el componente para cargar el archivo
            var texto = AsyncFileUpload.getElementsByTagName("input"); //Se almacena el texto del componente para cargar el archivo
            for (var i = 0; i < texto.length; i++) {
                if (texto[i].type == "file") {
                    texto[i].style.backgroundColor = "red";
                }
            }
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_General" style="background-color: #ffffff; width: 98%;">
                <%--Div General--%>
                <table border="0" cellspacing="0" class="estilo_fuente" style="width: 98%;">
                    <tr align="center">
                        <td class="label_titulo">
                            Asignaci&oacute;n de Bienes Muebles
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" OnClick="IBtn_Imagen_Error_Click" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" style="width: 98%;">
                        <td align="left" style="width: 100%">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Nuevo" OnClick="Btn_Nuevo_Click"
                                ToolTip="Nuevo" />
                            <asp:ImageButton ID="Btn_Generar_Reporte" runat="server" ToolTip="Ver Resguardo"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Ver Resguardo" OnClick="Btn_Generar_Reporte_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" CausesValidation="false" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click"
                                ToolTip="Salir" />
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right" runat="server" id="TR_Codigo_Barras">
                        <td align="left" colspan="2">
                            <asp:Label ID="Lbl_Tamano_Etiqueta" runat="server" Text="Tamaño de la Etiqueta"></asp:Label>&nbsp;&nbsp;
                            <asp:DropDownList ID="Cmb_Tamano_Etiqueta" runat="server" Width="150px">
                                <asp:ListItem Value="NORMAL">NORMAL</asp:ListItem>
                                <asp:ListItem Value="PEQUENO">PEQUEÑA</asp:ListItem>
                            </asp:DropDownList>
                            &nbsp;
                            <asp:ImageButton ID="Btn_Imprimir_Codigo_Barras" runat="server" ToolTip="Imprimir Codigo de Barras"
                                ImageUrl="~/paginas/imagenes/paginas/barcode.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Codigo_Barras" OnClick="Btn_Imprimir_Codigo_Barras_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="Div_Generales_Otra_Procedencia" runat="server" style="width: 97%;">
                <%--Div Otra prosedencia--%>
                <table width="100%" class="estilo_fuente">
                    <%-- Tabla Otra procedencia--%>
                    <tr>
                        <td class="label_titulo" colspan="4">
                            <asp:HiddenField ID="Hdf_Producto_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                            <asp:Label ID="Lbl_Producto_ID" runat="server" Text="Producto ID"></asp:Label>
                            <asp:HiddenField ID="Hdf_Tipo_Busqueda" runat="server" />
                            Generales<br />
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 18%;">
                            <asp:Label ID="Lbl_Operación" runat="server" Text="* Operación"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Operacion" runat="server" Width="100%" Enabled="false">
                                <asp:ListItem Value="CUSTODIA">CUSTODIA</asp:ListItem>
                                <asp:ListItem Value="RESGUARDO">RESGUARDO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%;">
                            <asp:HiddenField ID="Hdf_Bien_Mueble_ID" runat="server" />
                            <asp:Label ID="Lbl_Producto" runat="server" Text="* Producto"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Nombre_Producto_Donado" runat="server" Width="99.5%"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase Activo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Tipo_Activo" runat="server" Text="Tipo Activo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Tipo_Activo" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:HiddenField ID="Hdf_Cuenta_Contable_ID" runat="server" />
                            <asp:Label ID="Lbl_Cuenta_Contable" runat="server" Text="Cuenta Activo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" Width="99.5%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:HiddenField ID="Hdf_Cuenta_Gasto_ID" runat="server" />
                            <asp:Label ID="Lbl_Cuenta_Gasto" runat="server" Text="Cuenta Gasto"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Cuenta_Gasto" runat="server" Width="99.5%">
                            </asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:HiddenField ID="Hdf_Partida_Reparacion_ID" runat="server" />
                            <asp:Label ID="Lbl_Partida_Reparacion" runat="server" Text="Partida Reparación"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:Panel runat="server" ID="Pnl_Partida_Reparacion" DefaultButton="Btn_Busqueda_Partida_Reparacion">
                                <asp:TextBox ID="Txt_Clave_Partida_Reparacion" runat="server" Width="10%" MaxLength="4">
                                </asp:TextBox>&nbsp;
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Partida_Reparacion" runat="server" FilterType="Numbers" TargetControlID="Txt_Clave_Partida_Reparacion"></cc1:FilteredTextBoxExtender>
                                 <asp:ImageButton ID="Btn_Busqueda_Partida_Reparacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Busqueda_Partida_Reparacion_Click" 
                                    AlternateText="Buscar Partida Reparación" ToolTip="Buscar Partida Reparación" Width="20px" />
                                <asp:TextBox ID="Txt_Nombre_Partida_Reparacion" runat="server" Width="83.5%" Enabled="false">
                                </asp:TextBox>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Label1" runat="server" Text="* Gerencia "></asp:Label>
                        </td>
                        <td colspan="3" align="left">
                            <asp:DropDownList ID="Cmb_Gerencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Gerencias_SelectedIndexChanged"
                                AutoPostBack="True">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Dependencia" runat="server" Text="* Unidad Responsable"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Dependencia" runat="server" Visible="false" Width="98%" Enabled="false"></asp:TextBox>
                            <asp:DropDownList ID="Cmb_Dependencias" runat="server" Width="100%" Visible="false"
                                OnSelectedIndexChanged="Cmb_Dependencias_SelectedIndexChanged" AutoPostBack="true">
                                <asp:ListItem Value="SELECCIONE">&lt;- SELECCIONE -&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Zona" runat="server" Text="Ubicación Fisica"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Zonas" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Bien_Inmueble" runat="server" Text="Bien Inmueble"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Bien_Inmueble" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Numero_Serie" runat="server" Text="* No. Serie"></asp:Label>
                        </td>
                        <td style="width: 32%;">
                            <asp:TextBox ID="Txt_Numero_Serie" runat="server" Enabled="true" Width="98%" MaxLength="49"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Serie" runat="server" Enabled="True"
                                FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                TargetControlID="Txt_Numero_Serie" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td width="18%">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Marca" runat="server" Text="* Marca"></asp:Label>
                        </td>
                        <td width="32%">
                            <asp:DropDownList ID="Cmb_Marca" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Modelo" runat="server" Text="* Modelo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Modelo" runat="server" Width="99%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Modelo" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                TargetControlID="Txt_Modelo" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-_%/$# ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            <asp:Label ID="Lbl_Material" runat="server" Text="* Material"></asp:Label>
                        </td>
                        <td width="32%">
                            <asp:DropDownList ID="Cmb_Materiales" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td width="18%">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Color" runat="server" Text="* Color"></asp:Label>
                        </td>
                        <td width="32%">
                            <asp:DropDownList ID="Cmb_Colores" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 18%;">
                            <asp:Label ID="Lbl_RFC_Donador" runat="server" Text="Donador"></asp:Label>
                            <asp:HiddenField runat="server" ID="Hdf_Donador_ID" />
                        </td>
                        <td style="text-align: left; width: 32%;">
                            <asp:TextBox ID="Txt_RFC_Donador" runat="server" Width="70%"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar_RFC_Donador" runat="server" Visible="false" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                AlternateText="Buscar" OnClick="Btn_Buscar_RFC_Donador_Click" Width="16px" />
                            <asp:ImageButton ID="Btn_Agregar_Donador" runat="server" Visible="false" AlternateText="Buscar"
                                ImageUrl="~/paginas/imagenes/paginas/sias_add.png" OnClick="Btn_Agregar_Donador_Click"
                                Width="16px" />
                            <asp:ImageButton ID="Btn_Limpiar_Donador" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                Height="16px" Width="16px" CssClass="Img_Button" AlternateText="Limpiar Donador"
                                OnClientClick="javascript:return Limpiar_Ctlr_Donador();" ToolTip="Limpiar Donador" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_RFC_Donador" runat="server" Enabled="True"
                                TargetControlID="Txt_RFC_Donador" WatermarkCssClass="watermarked" WatermarkText="RFC de Donador">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                        <td colspan="2">
                            <asp:TextBox ID="Txt_Nombre_Donador" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="Div_Detalles" runat="server" style="width: 97%;">
                <asp:UpdatePanel ID="Upd_Archivos" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <table width="100%" class="estilo_fuente">
                            <tr>
                                <td class="label_titulo" colspan="4">
                                    Detalles
                                </td>
                            </tr>
                            <tr align="right" class="barra_delgada">
                                <td colspan="4" align="center">
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Numero_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:TextBox ID="Txt_Numero_Inventario" runat="server" Enabled="True" Width="98%"
                                        MaxLength="30"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario" runat="server" Enabled="True"
                                        FilterType="Numbers" InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Numero_Inventario">
                                    </cc1:FilteredTextBoxExtender>
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Numero_Inventario" runat="server" Enabled="True"
                                        TargetControlID="Txt_Numero_Inventario" WatermarkCssClass="watermarked" WatermarkText="<-- Automatico -->">
                                    </cc1:TextBoxWatermarkExtender>
                                </td>
                                <td width="18%">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Numero_Inventario_Anterior" runat="server" Text="* No. Inv. [Anterior]"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:TextBox ID="Txt_Numero_Inventario_Anterior" runat="server" Enabled="true" Width="98%"
                                        MaxLength="49"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario_Anterior" runat="server"
                                        Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        InvalidChars="'" TargetControlID="Txt_Numero_Inventario_Anterior" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="text-align: left;" width="18%">
                                    <asp:Label ID="Lbl_Procedencia_Bien" runat="server" Text="* Procedencia"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:DropDownList ID="Cmb_Procedencia" runat="server" Width="99%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td width="18%">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Costo" runat="server" Text="* Costo [$]"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:TextBox ID="Txt_Costo" runat="server" Enabled="true" Width="50%"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Costo" runat="server" Enabled="True" FilterType="Numbers, Custom"
                                        ValidChars="." TargetControlID="Txt_Costo">
                                    </cc1:FilteredTextBoxExtender>
                                    <asp:RegularExpressionValidator ID="REV_Txt_Costo" runat="server" ErrorMessage="[Verificar]"
                                        ControlToValidate="Txt_Costo" ValidationExpression="^\d+(\.\d\d)?$" Font-Size="X-Small"></asp:RegularExpressionValidator>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 18%;">
                                    <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="93%" Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Lanzar_Mpe_Proveedores" runat="server" OnClick="Btn_Lanzar_Mpe_Proveedores_Click"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Busqueda y Selección de Proveedor"
                                        AlternateText="Buscar" />
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Factura" runat="server" Text="* No. de Factura"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:TextBox ID="Txt_Factura" runat="server" Enabled="true" Width="98%" MaxLength="49"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Factura" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        InvalidChars="'" TargetControlID="Txt_Factura" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/_+-*@# ">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td width="18%">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Fecha_Adquisicion" runat="server" Text="* Fecha Facturación"></asp:Label>
                                </td>
                                <td style="width: 32%">
                                    <asp:TextBox ID="Txt_Fecha_Adquisicion" runat="server" Width="85%" MaxLength="20"
                                        Enabled="false"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Fecha_Adquisicion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Adquisicion" runat="server" TargetControlID="Txt_Fecha_Adquisicion"
                                        PopupButtonID="Btn_Fecha_Adquisicion" Format="dd/MMM/yyyy">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 13%; text-align: left;">
                                    <asp:Label ID="Lbl_No_Poliza" runat="server" Text="No. Póliza"></asp:Label>
                                </td>
                                <td style="width: 37%">
                                    <asp:TextBox ID="Txt_No_Poliza" runat="server" Width="99%" MaxLength="249"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TextBoxWatermarkExtender1" runat="server" Enabled="True"
                                        TargetControlID="Txt_No_Poliza" WatermarkCssClass="watermarked" WatermarkText="Límite de Caracteres 250">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FilteredTextBoxExtender1" runat="server" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        TargetControlID="Txt_No_Poliza" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ " Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td style="text-align: left; vertical-align: top; width: 18%">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Fecha_Inventario" runat="server" Text="* Fecha Resguardo"></asp:Label>
                                </td>
                                <td style="width: 32%;">
                                    <asp:TextBox ID="Txt_Fecha_Inventario" runat="server" Width="85%" MaxLength="20"
                                        Enabled="False"></asp:TextBox>
                                    <asp:ImageButton ID="Btn_Fecha_Inventario" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Inventario" runat="server" TargetControlID="Txt_Fecha_Inventario"
                                        PopupButtonID="Btn_Fecha_Inventario" Format="dd/MMM/yyyy" Enabled="True">
                                    </cc1:CalendarExtender>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Garantia" runat="server" Text="Garantía"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Garantia" runat="server" Width="99%" MaxLength="249"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Garantia" runat="server" Enabled="True"
                                        TargetControlID="Txt_Garantia" WatermarkCssClass="watermarked" WatermarkText="Límite de Caracteres 250">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Garantia" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                        TargetControlID="Txt_Garantia" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/ ">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                            <tr>
                                <td width="18%">
                                    <asp:Label ID="Lbl_Estatus" runat="server" Text="* Estatus"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="98%" Enabled="false">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                        <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                        <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td width="18%">
                                    &nbsp;&nbsp;
                                    <asp:Label ID="Lbl_Estado" runat="server" Text="* Estado"></asp:Label>
                                </td>
                                <td width="32%">
                                    <asp:DropDownList ID="Cmb_Estado" runat="server" Width="100%">
                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        <asp:ListItem>BUENO</asp:ListItem>
                                        <asp:ListItem>REGULAR</asp:ListItem>
                                        <asp:ListItem>MALO</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; width: 18%">
                                    <asp:Label ID="Lbl_Observaciones" runat="server" Text="Observaciones"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Observaciones" runat="server" Enabled="True" Rows="2" TextMode="MultiLine"
                                        Width="99%" MaxLength="100"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TBW_Txt_Observaciones" runat="server" Enabled="True"
                                        TargetControlID="Txt_Observaciones" WatermarkCssClass="watermarked" WatermarkText="Límite de Caracteres 255">
                                    </cc1:TextBoxWatermarkExtender>
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-_+$%&!#$%&¡?¿[]{}" Enabled="True">
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                            </tr>
                        </table>
                    </ContentTemplate>
                    <Triggers>
                        <asp:PostBackTrigger ControlID="Btn_Agregar_Archivo" />
                    </Triggers>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="Upd_Archivo" runat="server">
                    <ContentTemplate>
                        <table width="100%" class="estilo_fuente">
                            <tr>
                                <td style="width: 18%;">
                                    <asp:Label ID="Lbl_Seleccionar_Archivo" runat="server" Text="Archivo"></asp:Label>
                                </td>
                                <td style="width: 82%;">
                                    <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" Width="650px" CompleteBackColor="LightBlue"
                                        ErrorBackColor="Red" ThrobberID="Throbber" UploadingBackColor="LightGray" Enabled="true"
                                        OnClientUploadComplete="Carga_Completa" OnUploadedComplete="Asy_Cargar_Archivo_Complete" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="Throbber" runat="server" Text="">
                        <div id="Div1" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div2">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                        </asp:Label>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <asp:UpdatePanel ID="Upd_Mostrar_Archivos" runat="server" UpdateMode="Always">
                    <ContentTemplate>
                        <table width="100%" class="estilo_fuente">
                            <tr>
                                <td align="right">
                                    <asp:ImageButton ID="Btn_Agregar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                        Width="18px" Height="18px" CssClass="Img_Button" AlternateText="Agregar" ToolTip="Agregar Archivo"
                                        OnClick="Btn_Agregar_Archivo_Click" />
                                    <asp:ImageButton ID="Btn_Limpiar_FileUpload" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                        Height="16px" Width="16px" CssClass="Img_Button" AlternateText="Limpiar Archivo"
                                        OnClick="Btn_Limpiar_FileUpload_Click" ToolTip="Liberar Archivo" />
                                </td>
                            </tr>
                        </table>
                        <asp:UpdateProgress ID="Upd_Grid_Archivos" runat="server" AssociatedUpdatePanelID="Upd_Archivos"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                </div>
                                <div class="processMessage" id="div_progress">
                                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                </div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <asp:GridView ID="Grid_Archivos" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            DataKeyNames="ARCHIVO_BIEN_ID" ForeColor="#333333" GridLines="None" AllowPaging="True"
                            Width="99%" OnPageIndexChanging="Grid_Archivos_PageIndexChanging">
                            <RowStyle CssClass="GridItem" />
                            <Columns>
                                <asp:BoundField DataField="ARCHIVO_BIEN_ID" HeaderText="ARCHIVO_BIEN_ID" SortExpression="ARCHIVO_BIEN_ID" />
                                <asp:BoundField DataField="BIEN_ID" HeaderText="BIEN_ID" SortExpression="BIEN_ID" />
                                <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%" />
                                </asp:BoundField>
                                <asp:BoundField DataField="ARCHIVO" HeaderText="Nombre" SortExpression="ARCHIVO"
                                    NullDisplayText="-">
                                    <HeaderStyle HorizontalAlign="Left" />
                                    <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="90%" />
                                </asp:BoundField>
                            </Columns>
                            <PagerStyle CssClass="GridHeader" />
                            <SelectedRowStyle CssClass="GridSelected" />
                            <HeaderStyle CssClass="GridHeader" />
                            <AlternatingRowStyle CssClass="GridAltItem" />
                        </asp:GridView>
                    </ContentTemplate>
                </asp:UpdatePanel>
            </div>
            <br />
            <div id="Div3" runat="server" style="width: 97%;">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td>
                            <div runat="server" id="Div4">
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td class="label_titulo" colspan="4">
                                            Firmas
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Autorizo" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Autorizo" runat="server" Text="Autorizó"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Autorizo" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Autorizo" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Autorizo_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Operador" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Operador" runat="server" Text="Entregó"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Operador" runat="server" Width="95%" Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Operador" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Operador_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Funcionario_Recibe" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Funcionario_Recibe" runat="server" Text="Revisó"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Funcionario_Recibe" runat="server" Width="95%"
                                                Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Funcionario_Recibe" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Funcionario_Recibe_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="Div_Region_Operacion" runat="server" style="width: 97%;">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_Dependencia_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2" width="50%">
                            <asp:Label ID="Lbl_Asignacion_Secundaria" runat="server" Text="* Asignar a Bien Mueble, Vehículo o Empleado"></asp:Label>
                        </td>
                        <td colspan="2" width="50%">
                            <asp:DropDownList ID="Cmb_Asignacion_Secundaria" runat="server" Width="100%" AutoPostBack="true"
                                OnSelectedIndexChanged="Cmb_Asignacion_Secundaria_SelectedIndexChanged">
                                <asp:ListItem Value="SELECCIONE">&lt; - SELECCIONE - &gt;</asp:ListItem>
                                <asp:ListItem Value="NINGUNA">EMPLEADO</asp:ListItem>
                                <asp:ListItem Value="BIEN_MUEBLE">BIEN MUEBLE</asp:ListItem>
                                <asp:ListItem Value="VEHICULO">VEHÍCULO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div runat="server" id="Div_Producto_Bien_Mueble_Padre" width="97%">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo" colspan="4">
                            Bien Mueble [Bien Principal]
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_Bien_Padre_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Nombre_Parent" runat="server" Text="Producto"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Nombre_Parent" runat="server" Width="90%" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Lanzar_Buscar_Bien" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                OnClick="Btn_Lanzar_Buscar_Bien_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Numero_Inventario_Parent" runat="server" Text="No. Inventario"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Numero_Inventario_Parent" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Inventario_SIAS" runat="server" Text="No. Inventario [SIAS]"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Inventario_SIAS" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Marca_Padre" runat="server" Text="Marca"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Marca_Parent" runat="server" Width="100%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Modelo_Parent" runat="server" Text="Modelo"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Modelo_Parent" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Txt_Material_Parent" runat="server" Text="Material"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList runat="server" ID="Cmb_Material_Parent" Width="100%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Color_Parent" runat="server" Text="Color"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList runat="server" ID="Cmb_Color_Parent" Width="100%" Enabled="false">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Costo_Parent" runat="server" Text="Costo" CssClass="estilo_fuente"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Costo_Parent" runat="server" Width="97%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Fecha_Adquisicion_Parent" runat="server" Text="F. Adquisición"
                                CssClass="estilo_fuente"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:TextBox ID="Txt_Fecha_Adquisicion_Parent" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; width: 18%;">
                            <asp:Label ID="Lbl_Estado_Parent" runat="server" Text="Estado"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Estado_Parent" runat="server" Width="100%" Enabled="false">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                <asp:ListItem Value="BUENO">BUENO</asp:ListItem>
                                <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>
                                <asp:ListItem Value="MALO">MALO</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="text-align: left; width: 18%;">
                            <asp:Label ID="Lbl_Estatus_Parent" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td style="width: 32%">
                            <asp:DropDownList ID="Cmb_Estatus_Parent" runat="server" Width="100%" Enabled="False">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                <asp:ListItem Value="VIGENTE" Selected="True">VIGENTE</asp:ListItem>
                                <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="text-align: left; vertical-align: top; width: 18%;">
                            <asp:Label ID="Lbl_Observaciones_Parent" runat="server" Text="Observaciones"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Observaciones_Parent" runat="server" Width="99%" Rows="2" TextMode="MultiLine"
                                Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Vehiculo_Parent" runat="server" style="width: 98%;">
                <table width="98%" class="estilo_fuente">
                    <caption class="label_titulo">
                        Vehículo [Bien Principal]</caption>
                    <tr align="right" class="barra_delgada">
                        <td align="center" colspan="4">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Vehiculo_Nombre" runat="server" Text="Nombre"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Vehiculo_Nombre" runat="server" Width="92%" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar_Vehiculo" runat="server" AlternateText="Buscar Vehículo"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Vehiculo_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Vehiculo_No_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_No_Inventario" runat="server" Width="99%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Vehiculo_Numero_Serie" runat="server" Text="No. Serie"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Numero_Serie" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Vehiculo_Marca" runat="server" Text="Marca"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Marca" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Vehiculo_Modelo" runat="server" Text="Modelo"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Modelo" runat="server" Width="100%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Vehiculo_Tipo" runat="server" Text="Tipo"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Tipo" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Vehiculo_Color" runat="server" Text="Color"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Color" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Vehiculo_Numero_Economico" runat="server" Text="No. Economico"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Numero_Economico" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;&nbsp;
                            <asp:Label ID="Lbl_Vehiculo_Placas" runat="server" Text="Placas"></asp:Label>
                        </td>
                        <td style="width: 35%">
                            <asp:TextBox ID="Txt_Vehiculo_Placas" runat="server" Width="99.5%" Enabled="false"></asp:TextBox>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Resguardos" runat="server" style="width: 97%;">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo" colspan="4">
                            Resguardatarios
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr align="left">
                        <td style="width: 18%; text-align: left;">
                            <asp:Label ID="Lbl_Resguardantes" runat="server" Text="Empleados"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align: left;">
                            <asp:DropDownList ID="Cmb_Empleados" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left; vertical-align: top;">
                            <asp:Label ID="Lbl_Comentarios" runat="server" Text="Comentarios"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align: right;">
                            <asp:TextBox ID="Txt_Cometarios" runat="server" Rows="3" TextMode="MultiLine" Width="99%"
                                MaxLength="100"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cometarios" runat="server" Enabled="True"
                                TargetControlID="Txt_Cometarios" WatermarkCssClass="watermarked" WatermarkText="Límite de Caracteres 255">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%; text-align: left; vertical-align: top;">
                            <asp:ImageButton ID="Btn_Busqueda_Avanzada_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                Width="24px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Busqueda_Avanzada_Resguardante_Click" />
                        </td>
                        <td colspan="3" style="text-align: right;">
                            <asp:ImageButton ID="Btn_Agregar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                ToolTip="Agregar" AlternateText="Agregar" OnClick="Btn_Agregar_Resguardante_Click" />
                            <asp:ImageButton ID="Btn_Quitar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/quitar.png"
                                ToolTip="Quitar" AlternateText="Quitar" OnClick="Btn_Quitar_Resguardante_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <hr />
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <div id="Div_Listado_Resguardantes" runat="server" style="width: 97%;">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td align="center" colspan="4">
                            <br />
                            <asp:GridView ID="Grid_Resguardantes" runat="server" AllowPaging="True" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Resguardantes_PageIndexChanging"
                                PageSize="5" Width="97.5%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="EMPLEADO_ALMACEN_ID" HeaderText="EMPLEADO ALMACEN" SortExpression="EMPLEADO_RESGUARDO_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle Width="100px" HorizontalAlign="Left" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre del Empleado" SortExpression="NOMBRE_EMPLEADO">
                                        <HeaderStyle HorizontalAlign="Center" />
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                            <br />
                        </td>
                    </tr>
                </table>
            </div>

            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                        Auto_Completado_Cuentas_Gasto();
                        Auto_Completado_Productos();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Format_Producto(item) {
                        return item.clave_nombre;
                    }
                    function Auto_Completado_Productos() {
                        $("[id$='Txt_Nombre_Producto_Donado']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                        extraParams: { accion: 'autocompletado_busqueda_producto_bien_mueble' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.producto_id,
                                        result: row.nombre

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Producto_ID']").val("");
                                return Format_Producto(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Producto_ID']").val(item.producto_id);
                        });
                        $("[id$='Txt_Nombre_Producto_Donado']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Producto_ID']").val().length == 0)
                                $("[id$='Txt_Nombre_Producto_Donado']").val("");
                        }
                        });
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Contable']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Contable']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Contable_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Contable']").val("");
                        }
                        });
                    }
                    function Auto_Completado_Cuentas_Gasto() {
                        $("[id$='Txt_Cuenta_Gasto']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Gasto']").live({ 'blur': function() {
                            if ($("[id$='Hdf_Cuenta_Gasto_ID']").val().length == 0)
                                $("[id$='Txt_Cuenta_Gasto']").val("");
                        }
                        });
                    }
                } 
            </script>

        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Mpe_Proveedores" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Proveedores_Cabecera" runat="server" TargetControlID="Btn_Comodin_Mpe_Proveedores"
                PopupControlID="Pnl_Mpe_Proveedores" CancelControlID="Btn_Cerrar_Mpe_Proveedores"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Bienes" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Buscar_Bien" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Bien_Mueble" runat="server" TargetControlID="Btn_Comodin_Buscar_Bien"
                PopupControlID="Pnl_Busqueda_Bien_Mueble" CancelControlID="Btn_Cerrar" DropShadow="True"
                BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Aux_Donadores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Donadores" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Donadores" runat="server" TargetControlID="Btn_Comodin_Donadores"
                PopupControlID="Pnl_Donador" CancelControlID="Btn_Cerrar_Ventana_Cancelacion"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Proveedores" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Proveedores_Interno" runat="server" CssClass="estilo_fuente"
            Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
            border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Img_Mpe_Proveedores" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda y Selección de Proveedores
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Proveedores" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Proveedores" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Proveedores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <asp:Panel ID="Pnl_Busqueda_Proveedoress" runat="server" Width="98%" DefaultButton="Btn_Ejecutar_Busqueda_Proveedores">
                            <table width="100%">
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Proveedores_Buscar" runat="server" CssClass="estilo_fuente"
                                            Text="Nombre" Style="font-weight: bolder;"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Buscar" runat="server" Width="92%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            TargetControlID="Txt_Nombre_Proveedor_Buscar" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            Enabled="True" TargetControlID="Txt_Nombre_Proveedor_Buscar" WatermarkCssClass="watermarked"
                                            WatermarkText="<-- Razón Social ó Nombre Comercial -->">
                                        </cc1:TextBoxWatermarkExtender>
                                        <asp:ImageButton ID="Btn_Ejecutar_Busqueda_Proveedores" runat="server" OnClick="Btn_Ejecutar_Busqueda_Proveedores_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <asp:Panel ID="Pnl_Listado_Proveedores" runat="server" ScrollBars="Vertical" Style="white-space: normal;"
                            Width="100%" BorderColor="#3366FF" Height="330px">
                            <asp:GridView ID="Grid_Listado_Proveedores" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Listado_Proveedores_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Proveedores_SelectedIndexChanged" PageSize="150"
                                Width="100%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PROVEEDOR_ID" HeaderText="Proveedor ID" SortExpression="PROVEEDOR_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Razón Social" SortExpression="NOMBRE">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMPANIA" HeaderText="Nombre Comercial" SortExpression="COMPANIA">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
    <asp:Panel ID="Pnl_Donador" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Cabecera" runat="server" CssClass="estilo_fuente" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Introducir los datos del Donador
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana_Cancelacion" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="Upl_Pnl_Donadores" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrgr_Donadores" runat="server" AssociatedUpdatePanelID="Upl_Pnl_Donadores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <table width="100%">
                            <tr>
                                <td colspan="2">
                                    <table width="99%">
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Nombre_Donador_MPE" runat="server" Text="* Nombre" CssClass="estilo_fuente"
                                                    Style="font-weight: bolder;"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 82%; text-align: left;">
                                                <asp:TextBox ID="Txt_Nombre_Donador_MPE" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Donador_MPE" runat="server" TargetControlID="Txt_Nombre_Donador_MPE"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Apellido_Paterno_Donador" runat="server" Text="A. Paterno" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Apellido_Paterno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Paterno_Donador" runat="server"
                                                    TargetControlID="Txt_Apellido_Paterno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Apellido_Materno_Donador" runat="server" Text="A. Materno" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Apellido_Materno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Materno_Donador" runat="server"
                                                    TargetControlID="Txt_Apellido_Materno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Direccion_Donador" runat="server" Text="Dirección" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 82%; text-align: left;">
                                                <asp:TextBox ID="Txt_Direccion_Donador" runat="server" Width="99%" MaxLength="150"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Direccion_Donador" runat="server" TargetControlID="Txt_Direccion_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Ciudad_Donador" runat="server" Text="Ciudad" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Ciudad_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ciudad_Donador" runat="server" TargetControlID="Txt_Ciudad_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Estado_Donador" runat="server" Text="Estado" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Estado_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Estado_Donador" runat="server" TargetControlID="Txt_Estado_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Telefono_Donador" runat="server" Text="Telefono" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Telefono_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Telefono_Donador" runat="server" TargetControlID="Txt_Telefono_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_Celular_Donador" runat="server" Text="Celular" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_Celular_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Celular_Donador" runat="server" TargetControlID="Txt_Celular_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_CURP_Donador" runat="server" Text="CURP" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_CURP_Donador" runat="server" Width="98%" MaxLength="18"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_CURP_Donador" runat="server" TargetControlID="Txt_CURP_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="Ññ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 18%; text-align: left;">
                                                <asp:Label ID="Lbl_RFC_Donador_MPE" runat="server" Text="* RFC" CssClass="estilo_fuente"
                                                    Style="font-weight: bolder;"></asp:Label>
                                            </td>
                                            <td style="width: 32%; text-align: left;">
                                                <asp:TextBox ID="Txt_RFC_Donador_MPE" runat="server" Width="98%" MaxLength="15"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_RFC_Donador_MPE" runat="server" TargetControlID="Txt_RFC_Donador_MPE"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="Ññ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: left;">
                                    &nbsp;
                                </td>
                                <td style="width: 80%; text-align: right;">
                                    <asp:ImageButton ID="Btn_MPE_Donador_Aceptar" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                        CausesValidation="False" ToolTip="Aceptar" OnClick="Btn_MPE_Donador_Aceptar_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="Div_MPE_Donadores_Mensaje_Error" style="width: 98%;" runat="server" visible="false">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <asp:ImageButton ID="Btn_MPE_Donadores_Mensaje_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                        Enabled="false" Width="24px" Height="24px" />
                                                    <asp:Label ID="Lbl_MPE_Donadores_Cabecera_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 10%;">
                                                </td>
                                                <td style="width: 90%; text-align: left;" valign="top">
                                                    <asp:Label ID="Lbl_MPE_Donadores_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Btn_Agregar_Donador" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
    <asp:Panel ID="Pnl_Busqueda_Bien_Mueble" runat="server" HorizontalAlign="Center"
        Width="800px" Style="display: none; border-style: outset; border-color: Silver;
        background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Interno" runat="server" Style="background-color: Silver; color: Black;
            font-size: 12; font-weight: bold; border-style: outset;">
            <center>
                <asp:UpdatePanel ID="UpPnl_Busqueda" runat="server" UpdateMode="Conditional">
                    <ContentTemplate>
                        <asp:UpdateProgress ID="UpdPgr_Busqueda" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda"
                            DisplayAfter="0">
                            <ProgressTemplate>
                                <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                </div>
                                <div class="processMessage" id="div_progress">
                                    <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                            </ProgressTemplate>
                        </asp:UpdateProgress>
                        <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda" runat="server" Width="98%"
                            ActiveTabIndex="0">
                            <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Datos_Generales" ID="Tab_Panel_Datos_Generales_Busqueda"
                                Width="100%" Height="400px">
                                <HeaderTemplate>
                                    Datos Generales</HeaderTemplate>
                                <ContentTemplate>
                                    <div style="border-style: outset; width: 98%; height: 200px;">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">
                                                    <asp:Label ID="Lbl_Titulo_Busqueda" runat="server" Text="Búsqueda"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Inventario_Anterior" runat="server" CssClass="estilo_fuente"
                                                        Text="No. Inventario"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Inventario_Anterior" runat="server" Width="98%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Inventario_Anterior" runat="server"
                                                        Enabled="True" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                        InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Inventario_Anterior"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Inventario_SIAS" runat="server" CssClass="estilo_fuente"
                                                        Text="No. Inventario SIAS"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Inventario_SIAS" runat="server" Width="98%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Inventario_SIAS" runat="server"
                                                        Enabled="True" FilterType="Numbers" InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_Inventario_SIAS">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Producto" runat="server" CssClass="estilo_fuente" Text="Nombre Producto"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Producto" runat="server" Width="98%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Producto" runat="server" Enabled="True"
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                                        TargetControlID="Txt_Busqueda_Producto" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Dependencias" runat="server" CssClass="estilo_fuente"
                                                        Text="U. Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Dependencias" runat="server" Width="99%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Modelo" runat="server" CssClass="estilo_fuente" Text="Modelo"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Modelo" runat="server" Width="98%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Modelo" runat="server" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                        TargetControlID="Txt_Busqueda_Modelo" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-_%/$# ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Marca" runat="server" CssClass="estilo_fuente" Text="Marca"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Marca" runat="server" Width="98%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Factura" runat="server" CssClass="estilo_fuente" Text="No. Factura"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Factura" runat="server" Width="95%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Factura" runat="server" Enabled="True"
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                                        TargetControlID="Txt_Busqueda_Factura" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Estatus" runat="server" CssClass="estilo_fuente" Text="Estatus"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Estatus" runat="server" Width="98%">
                                                        <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                        <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                                        <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                                        <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Numero_Serie" runat="server" CssClass="estilo_fuente"
                                                        Text="No. Serie"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_Numero_Serie" runat="server" Width="95%"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Numero_Serie" runat="server" Enabled="True"
                                                        FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" InvalidChars="&lt;,&gt;,&amp;,',!,"
                                                        TargetControlID="Txt_Busqueda_Numero_Serie" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ /*-+$%&amp;">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: right;">
                                                    <asp:ImageButton ID="Btn_Buscar_Datos" runat="server" CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                        OnClick="Btn_Buscar_Datos_Click" ToolTip="Buscar" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Click"
                                                        ToolTip="Limpiar Filtros" Width="20px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                            <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Reguardantes" ID="Tab_Panel_Resguardantes_Busqueda"
                                Width="100%">
                                <HeaderTemplate>
                                    Reguardatarios</HeaderTemplate>
                                <ContentTemplate>
                                    <div style="border-style: outset; width: 98%; height: 200px;">
                                        <table width="100%">
                                            <tr>
                                                <td colspan="2" style="text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Listado" runat="server" Text="Búsqueda"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_RFC_Resguardante" runat="server" CssClass="estilo_fuente"
                                                        Text="RFC Reguardatario"></asp:Label>
                                                </td>
                                                <td style="width: 30%; text-align: left;">
                                                    <asp:TextBox ID="Txt_Busqueda_RFC_Resguardante" runat="server" Width="300px"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_RFC_Resguardante" runat="server"
                                                        Enabled="True" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                        InvalidChars="&lt;,&gt;,&amp;,',!," TargetControlID="Txt_Busqueda_RFC_Resguardante"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Resguardantes_Dependencias" runat="server" CssClass="estilo_fuente"
                                                        Text="U. Responsable"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Resguardantes_Dependencias" runat="server" AutoPostBack="true"
                                                        OnSelectedIndexChanged="Cmb_Busqueda_Resguardantes_Dependencias_SelectedIndexChanged"
                                                        Width="98%">
                                                        <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 20%; text-align: left;">
                                                    <asp:Label ID="Lbl_Busqueda_Nombre_Resguardante" runat="server" CssClass="estilo_fuente"
                                                        Text="Reguardatario"></asp:Label>
                                                </td>
                                                <td colspan="3" style="text-align: left;">
                                                    <asp:DropDownList ID="Cmb_Busqueda_Nombre_Resguardante" runat="server" Width="98%">
                                                        <asp:ListItem Text="&lt; SELECCIONE &gt;" Value="SELECCIONE"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: right;">
                                                    <asp:ImageButton ID="Btn_Buscar_Resguardante" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Resguardante_Click"
                                                        ToolTip="Buscar Listados" />
                                                    <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante" runat="server" CausesValidation="False"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Click"
                                                        ToolTip="Limpiar Filtros" Width="20px" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </ContentTemplate>
                            </cc1:TabPanel>
                        </cc1:TabContainer>
                        <div style="width: 97%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                            <center>
                                <caption>
                                    <asp:GridView ID="Grid_Listado_Bienes" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                        CssClass="GridView_1" GridLines="None" OnSelectedIndexChanged="Grid_Listado_Bienes_SelectedIndexChanged"
                                        PageSize="100" Width="98%" OnPageIndexChanging="Grid_Listado_Bienes_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="BIEN_MUEBLE_ID" HeaderText="BIEN_MUEBLE_ID" SortExpression="BIEN_MUEBLE_ID" />
                                            <asp:BoundField DataField="NO_INVENTARIO_ANTERIOR" HeaderText="Inv. Anterior" SortExpression="NO_INVENTARIO_ANTERIOR">
                                                <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_INVENTARIO" HeaderText="Inv. SIAS" SortExpression="NO_INVENTARIO">
                                                <ItemStyle Width="50px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE_PRODUCTO" HeaderText="Nombre" SortExpression="NOMBRE_PRODUCTO">
                                                <ItemStyle Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                                <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="COLOR" HeaderText="Color" SortExpression="COLOR">
                                                <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="ESTADO" HeaderText="Estado" SortExpression="ESTADO">
                                                <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <HeaderStyle CssClass="GridHeader" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </caption>
                            </center>
                        </div>
                    </ContentTemplate>
                </asp:UpdatePanel>
                <table width="95%">
                    <tr>
                        <td style="width: 100%">
                            <center>
                                <asp:Button ID="Btn_Cerrar" runat="server" TabIndex="202" Text="Cerrar" Width="80px"
                                    Height="26px" />
                            </center>
                        </td>
                    </tr>
                </table>
            </center>
        </asp:Panel>
    </asp:Panel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Panel_Filtro_Busqueda_Empleados" runat="server" Width="100%" DefaultButton="Btn_Busqueda_Empleados">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Empleado
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Empleado" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                    WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                RFC
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                    TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                    WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Nombre
                                            </td>
                                            <td style="width: 30%; text-align: left;" colspan="3">
                                                <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                    FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    ValidChars="áéíóúÁÉÍÓÚ .-" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrarón resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_MPE_Busqueda_Vehiculo" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Busqueda_Vehiculo" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Busqueda_Vehiculo" runat="server" TargetControlID="Btn_Comodin_Busqueda_Vehiculo"
                PopupControlID="Pnl_Busqueda_Vehiculo" CancelControlID="Btn_Cerrar_Busqueda_Vehiculo"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Vehiculo" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <center>
            <asp:Panel ID="Pnl_Busqueda_Vehiculo_Interno" runat="server" CssClass="estilo_fuente"
                Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
                border-style: outset;">
                <table class="estilo_fuente" width="100%">
                    <tr>
                        <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                            <asp:Image ID="Img_Encabezado_Busqueda_Vehiculos" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                            Busqueda de Vehículos
                        </td>
                        <td align="right">
                            <asp:ImageButton ID="Btn_Cerrar_Busqueda_Vehiculo" CausesValidation="false" runat="server"
                                Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                        </td>
                    </tr>
                </table>
            </asp:Panel>
            <asp:UpdatePanel ID="UpPnl_Busqueda_Vehiculo" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrg_Busqueda_Vehiculo" runat="server" AssociatedUpdatePanelID="UpPnl_Busqueda_Vehiculo"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <cc1:TabContainer ID="Tab_Contenedor_Pestagnas_Busqueda_Vehiculo" runat="server"
                        Width="100%" ActiveTabIndex="0" CssClass="Tab">
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Datos_Generales_Busqueda_Vehiculo"
                            ID="Tab_Panel_Datos_Generales_Busqueda_Vehiculo" Width="100%" Height="400px"
                            BackColor="White">
                            <HeaderTemplate>
                                Generales</HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left;" colspan="4">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Inventario" runat="server" Text="Número Inventario"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Inventario" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Numero_Inventario" FilterType="Numbers"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Numero_Economico" runat="server" Text="Número Económico"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Numero_Economico" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Numero_Economico" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Numero_Economico" InvalidChars="<,>,&,',!,"
                                                    FilterType="Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Modelo" runat="server" Text="Modelo" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Modelo" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Modelo" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Modelo" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ -_" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Marca" runat="server" Text="Marca" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Marca" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Tipo_Vehiculo" runat="server" Text="Tipo Vehículo"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Tipo_Vehiculo" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="SELECCIONE"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Tipo_Combustible" runat="server" Text="Combustible"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Tipo_Combustible" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Text="Año Fabricación"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server" Width="97%"
                                                    MaxLength="4"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_Anio_Fabricacion" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_Anio_Fabricacion" FilterType="Numbers"
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Color" runat="server" Text="Color" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Color" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Zonas" runat="server" Text="Zonas" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Zonas" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Estatus" runat="server" Text="Estatus" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Estatus" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                                    <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                                    <asp:ListItem Value="TEMPORAL">BAJA (TEMPORAL)</asp:ListItem>
                                                    <asp:ListItem Value="DEFINITIVA">BAJA (DEFINITIVA)</asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Dependencias" runat="server" Text="U. Responsable"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 80%; text-align: left;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Dependencias" runat="server" Width="85%">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:ImageButton ID="Btn_Buscar_Datos_Vehiculo" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                    CausesValidation="False" OnClick="Btn_Buscar_Datos_Vehiculo_Click" ToolTip="Buscar Contrarecibos" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo" runat="server" OnClick="Btn_Limpiar_Filtros_Buscar_Datos_Vehiculo_Click"
                                                    CausesValidation="False" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    ToolTip="Limpiar Filtros" Width="20px" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                        <cc1:TabPanel runat="server" HeaderText="Tab_Panel_Reguardantes_Busqueda_Vehiculo"
                            ID="Tab_Panel_Resguardantes_Busqueda_Vehiculo" Width="100%" BackColor="White">
                            <HeaderTemplate>
                                Resguardatarios</HeaderTemplate>
                            <ContentTemplate>
                                <div style="border-style: outset; width: 99.5%; height: 200px; background-color: White;">
                                    <table width="100%">
                                        <tr>
                                            <td style="text-align: left;" colspan="2">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Text="RFC Resguardatario"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server" Width="200px"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_RFC_Resguardante" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_RFC_Resguardante" InvalidChars="<,>,&,',!,"
                                                    FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ "
                                                    Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_No_Empleado" runat="server" Text="No. Empleado"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Busqueda_Vehiculo_No_Empleado" runat="server" Width="97%"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Vehiculo_No_Empleado" runat="server"
                                                    TargetControlID="Txt_Busqueda_Vehiculo_No_Empleado" InvalidChars="<,>,&,',!,"
                                                    FilterType="Numbers" Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server" Text="U. Responsable"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias" runat="server"
                                                    Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Busqueda_Vehiculo_Resguardantes_Dependencias_SelectedIndexChanged">
                                                    <asp:ListItem Text="&lt; TODAS &gt;" Value="TODAS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Text="Resguardatario"
                                                    CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="text-align: left;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Vehiculo_Nombre_Resguardante" runat="server" Width="100%">
                                                    <asp:ListItem Text="&lt;TODOS&gt;" Value="TODOS"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="4" style="text-align: right">
                                                <asp:ImageButton ID="Btn_Buscar_Resguardante_Vehiculo" runat="server" CausesValidation="False"
                                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Resguardante_Vehiculo_Click"
                                                    ToolTip="Buscar Listados" />
                                                <asp:ImageButton ID="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo" runat="server"
                                                    CausesValidation="False" OnClick="Btn_Limpiar_Filtros_Buscar_Resguardante_Vehiculo_Click"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="20px" ToolTip="Limpiar Filtros" />
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </ContentTemplate>
                        </cc1:TabPanel>
                    </cc1:TabContainer>
                    <div style="width: 99%; height: 150px; overflow: auto; border-style: outset; background-color: White;">
                        <center>
                            <caption>
                                <asp:GridView ID="Grid_Listado_Busqueda_Vehiculo" runat="server" AllowPaging="true"
                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" OnPageIndexChanging="Grid_Listado_Busqueda_Vehiculo_PageIndexChanging"
                                    OnSelectedIndexChanged="Grid_Listado_Busqueda_Vehiculo_SelectedIndexChanged"
                                    Width="98%" PageSize="100">
                                    <RowStyle CssClass="GridItem" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="VEHICULO_ID" HeaderText="VEHICULO_ID" SortExpression="VEHICULO_ID" />
                                        <asp:BoundField DataField="NUMERO_INVENTARIO" HeaderText="No. Inven." SortExpression="NUMERO_INVENTARIO">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="NUMERO_ECONOMICO" HeaderText="No. Eco." SortExpression="NUMERO_ECONOMICO">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="VEHICULO" HeaderText="Vehículo" SortExpression="VEHICULO">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MARCA" HeaderText="Marca" SortExpression="MARCA">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="MODELO" HeaderText="Modelo" SortExpression="MODELO">
                                            <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ANIO" HeaderText="Año" SortExpression="ANIO">
                                            <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle Width="90px" Font-Size="X-Small" HorizontalAlign="Center" />
                                        </asp:BoundField>
                                    </Columns>
                                    <HeaderStyle CssClass="GridHeader" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                </asp:GridView>
                            </caption>
                        </center>
                    </div>
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
