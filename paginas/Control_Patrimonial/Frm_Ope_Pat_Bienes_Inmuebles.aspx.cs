﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Catalogo_Usos_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Destinos_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Inmuebles.Negocio;
using JAPAMI.Catalogo_Tipos_Predio.Negocio;
using JAPAMI.Catalogo_Calles.Negocio;
using JAPAMI.Catalogo_Cuentas_Predial.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones_Zonas_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Origenes_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Orientaciones_Inmuebles.Negocio;
using JAPAMI.Catalogo_Notarios.Negocio;
using JAPAMI.Colonias.Negocios;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Control_Patrimonial_Reporte_Listado_Bienes.Negocio;
using System.Drawing;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using AjaxControlToolkit;
using JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Datos;
using System.Text;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;

public partial class paginas_Control_Patrimonial_Frm_Ope_Pat_Bienes_Inmuebles : System.Web.UI.Page
{

    #region "Page Load"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Evento que se ejecuta cuando se carga inicialmente la pagina
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Trim().Length == 0)
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        }
        if (!IsPostBack)
        {
            Llenar_Combo_Uso();
            Llenar_Combo_Destino();
            Llenar_Combo_Distritos();
            Llenar_Combo_Sectores();
            Llenar_Combo_Clasificaciones_Zonas();
            Llenar_Combo_Origenes();
            Llenar_Combo_Orientaciones();
            Llenar_Combo_Clase_Activo();
            Habiliar_Generales_Formulario(false);
            Limpiar_Generales_Formulario();
            if (Session["Operacion_Inicial"] != null)
            {
                String Operacion_Inicial = Session["Operacion_Inicial"].ToString();
                Session.Remove("Operacion_Inicial");
                if (Operacion_Inicial.Trim().Equals("NUEVO"))
                {
                    Btn_Nuevo_Click(Btn_Nuevo, null);
                }
                else if (Operacion_Inicial.Trim().Equals("VER_BIEN_INMUEBLE"))
                {
                    if (Session["Bien_Inmueble_ID"] != null)
                    {
                        String Bien_Inmueble_ID = Session["Bien_Inmueble_ID"].ToString();
                        Session.Remove("Bien_Mueble_ID");
                        Hdf_Bien_Inmueble_ID.Value = Bien_Inmueble_ID;
                        Mostrar_Detalles_Bien_Inmueble();
                    }
                    else
                    {
                        Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
                    }
                }
                else
                {
                    Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
                }
            }
            else
            {
                Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
            }
        }
    }

    #endregion

    #region "Metodos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Caracteristicas
    ///DESCRIPCIÓN: Mostrar_Caracteristicas
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Maryo/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Mostrar_Caracteristicas(DataTable Dt_Caracteristicas)
    {
        if (Dt_Caracteristicas != null)
        {
            if (Dt_Caracteristicas.Rows.Count > 0)
            {
                //Datos Generales
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString())) { Txt_Estado_Actual.Text = Dt_Caracteristicas.Rows[0]["Estado_Actual"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString())) { Txt_Uso_Inicial.Text = Dt_Caracteristicas.Rows[0]["Uso_Inicial"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString())) { Txt_Usuarios_Beneficiados.Text = Dt_Caracteristicas.Rows[0]["Usuarios_Beneficiados"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString())) { Txt_Anios_Sin_Operar.Text = Dt_Caracteristicas.Rows[0]["Anios_Sin_Operar"].ToString().Trim(); }
                //Ubicación
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Region"].ToString())) { Txt_Region.Text = Dt_Caracteristicas.Rows[0]["Region"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Acuifero"].ToString())) { Txt_Acuifero.Text = Dt_Caracteristicas.Rows[0]["Acuifero"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Cuenca"].ToString())) { Txt_Cuenca.Text = Dt_Caracteristicas.Rows[0]["Cuenca"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Latitud"].ToString())) { Txt_Latitud.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Latitud"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Longitud"].ToString())) { Txt_Longitud.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Longitud"]).ToString().Trim(); }
                //Volumenes Anuales
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"].ToString())) { Txt_Volumen_Consumo.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Vol_Anual_Consumo"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"].ToString())) { Txt_Volumen_Extraccion.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Vol_Anual_Extraccion"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"].ToString())) { Txt_Volumen_Descarga.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Vol_Anual_Descarga"]).ToString().Trim(); }
                //Equipo
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"].ToString())) { Txt_Diametro_Columna_Succionadora.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Diametro_Columna_Succionadora"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Diametro_Descarga"].ToString())) { Txt_Diametro_Descarga.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Diametro_Descarga"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString())) { Txt_Tipo_Bomba.Text = Dt_Caracteristicas.Rows[0]["Tipo_Bomba"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString())) { Txt_Accionada_Por_Motor.Text = Dt_Caracteristicas.Rows[0]["Accionada_Por_Motor"].ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Medidor"].ToString())) { Txt_Medidor.Text = Dt_Caracteristicas.Rows[0]["Medidor"].ToString().Trim(); }
                //Caracteristicas del Aprovechamiento
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Profundidad"].ToString())) { Txt_Profundidad.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Profundidad"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"].ToString())) { Txt_Diametro_Perforacion.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Diametro_Perforacion"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Diametro_Ademe"].ToString())) { Txt_Diametro_Ademe.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Diametro_Ademe"]).ToString().Trim(); }
                //Gasto (1/Seg)
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Requerido"].ToString())) { Txt_Requerido.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Requerido"]).ToString().Trim(); }
                if (!String.IsNullOrEmpty(Dt_Caracteristicas.Rows[0]["Maximo"].ToString())) { Txt_Maximo.Text = Convert.ToDouble(Dt_Caracteristicas.Rows[0]["Maximo"]).ToString().Trim(); }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Caracteristicas
    ///DESCRIPCIÓN: Obtener_Caracteristicas
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Maryo/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private DataTable Obtener_Caracteristicas()
    {
        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
        DataTable Dt_Caracteristicas = BI_Negocio.Obtener_Dt_Caracteristicas();
        Boolean Se_Creo_Fila = false;
        //Datos Generales
        if (Txt_Estado_Actual.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Estado_Actual", Txt_Estado_Actual.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Uso_Inicial.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Uso_Inicial", Txt_Uso_Inicial.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Usuarios_Beneficiados.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Usuarios_Beneficiados", Txt_Usuarios_Beneficiados.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Anios_Sin_Operar.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Anios_Sin_Operar", Txt_Anios_Sin_Operar.Text.Trim(), ref Se_Creo_Fila);
        //Ubicación
        if (Txt_Region.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Region", Txt_Region.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Acuifero.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Acuifero", Txt_Acuifero.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Cuenca.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Cuenca", Txt_Cuenca.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Latitud.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Latitud", Txt_Latitud.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Longitud.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Longitud", Txt_Longitud.Text.Trim(), ref Se_Creo_Fila);
        //Volumenes Anuales
        if (Txt_Volumen_Consumo.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Vol_Anual_Consumo", Txt_Volumen_Consumo.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Volumen_Extraccion.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Vol_Anual_Extraccion", Txt_Volumen_Extraccion.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Volumen_Descarga.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Vol_Anual_Descarga", Txt_Volumen_Descarga.Text.Trim(), ref Se_Creo_Fila);
        //Equipo
        if (Txt_Diametro_Columna_Succionadora.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Diametro_Columna_Succionadora", Txt_Diametro_Columna_Succionadora.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Diametro_Descarga.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Diametro_Descarga", Txt_Diametro_Descarga.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Tipo_Bomba.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Tipo_Bomba", Txt_Tipo_Bomba.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Accionada_Por_Motor.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Accionada_Por_Motor", Txt_Accionada_Por_Motor.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Medidor.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Medidor", Txt_Medidor.Text.Trim(), ref Se_Creo_Fila);
        //Caracteristicas del Aprovechamiento
        if (Txt_Profundidad.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Profundidad", Txt_Profundidad.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Diametro_Perforacion.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Diametro_Perforacion", Txt_Diametro_Perforacion.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Diametro_Ademe.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Diametro_Ademe", Txt_Diametro_Ademe.Text.Trim(), ref Se_Creo_Fila);
        //Gasto (1/Seg)
        if (Txt_Requerido.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Requerido", Txt_Requerido.Text.Trim(), ref Se_Creo_Fila);
        if (Txt_Maximo.Text.Trim().Length > 0) Agregar_Fila_Caracteristicas(ref Dt_Caracteristicas, "Maximo", Txt_Maximo.Text.Trim(), ref Se_Creo_Fila);
        return Dt_Caracteristicas;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Fila_Caracteristicas
    ///DESCRIPCIÓN: Agregar_Fila_Caracteristicas
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Maryo/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Agregar_Fila_Caracteristicas(ref DataTable Dt_Caracteristicas, String Columna, String Valor, ref Boolean Columna_Creada)
    {
        if (!Columna_Creada)
        {
            DataRow Dr_Fila = Dt_Caracteristicas.NewRow();
            Dt_Caracteristicas.Rows.Add(Dr_Fila);
            Columna_Creada = true;
        }
        if (Dt_Caracteristicas.Columns[Columna].DataType.FullName.ToString().Trim().Contains("Decimal")) Dt_Caracteristicas.Rows[0].SetField(Columna, Convert.ToDecimal(Valor));
        else if (Dt_Caracteristicas.Columns[Columna].DataType.FullName.ToString().Trim().Contains("Int64")) Dt_Caracteristicas.Rows[0].SetField(Columna, Convert.ToInt64(Valor));
        else if (Dt_Caracteristicas.Columns[Columna].DataType.FullName.ToString().Trim().Contains("Int32")) Dt_Caracteristicas.Rows[0].SetField(Columna, Convert.ToInt32(Valor));
        else if (Dt_Caracteristicas.Columns[Columna].DataType.FullName.ToString().Trim().Contains("String")) Dt_Caracteristicas.Rows[0].SetField(Columna, Convert.ToString(Valor));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Afectaciones
    ///DESCRIPCIÓN: Limpia los campos de la Seccion de Afectaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Contabilidad()
    {
        Cmb_Clase_Activo.SelectedIndex = 0;
        Hdf_Cuenta_Contable_ID.Value = "";
        Txt_Cuenta_Contable.Text = "";
        Txt_Nombre_Perito.Text = "";
        Txt_Fecha_Avaluo.Text = "";
        Txt_Valor_Avaluo.Text = "";
        Cmb_Clase_Activo_Terreno.SelectedIndex = 0;
        Hdf_Cuenta_Contable_Terreno_ID.Value = "";
        Txt_Cuenta_Contable_Terreno.Text = "";
        Txt_Nombre_Perito_Terreno.Text = "";
        Txt_Fecha_Avaluo_Terreno.Text = "";
        Txt_Valor_Avaluo_Terreno.Text = "";
        Hdf_Cuenta_Gasto_ID.Value = "";
        Txt_Cuenta_Gasto_ID.Text = "";
        Txt_Valor_Actual.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Contabilidad
    ///DESCRIPCIÓN: Habilita los campos de la Seccion de Contabilidad
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habilitar_Contabilidad(Boolean Habilitar)
    {
        Cmb_Clase_Activo.Enabled = Habilitar;
        Txt_Cuenta_Contable.Enabled = Habilitar;
        Txt_Nombre_Perito.Enabled = Habilitar;
        Btn_Fecha_Avaluo.Enabled = Habilitar;
        Txt_Valor_Avaluo.Enabled = Habilitar;
        Cmb_Clase_Activo_Terreno.Enabled = Habilitar;
        Txt_Cuenta_Contable_Terreno.Enabled = Habilitar;
        Txt_Nombre_Perito_Terreno.Enabled = Habilitar;
        Btn_Fecha_Avaluo_Terreno.Enabled = Habilitar;
        Txt_Valor_Avaluo_Terreno.Enabled = Habilitar;
        Txt_Cuenta_Gasto_ID.Enabled = Habilitar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Afectaciones
    ///DESCRIPCIÓN: Limpia los campos de la Seccion de Afectaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Caracteristicas()
    {
        //Datos Generales
        Txt_Estado_Actual.Text = "";
        Txt_Uso_Inicial.Text = "";
        Txt_Usuarios_Beneficiados.Text = "";
        Txt_Anios_Sin_Operar.Text = "";
        //Ubicación
        Txt_Region.Text = "";
        Txt_Acuifero.Text = "";
        Txt_Cuenca.Text = "";
        Txt_Latitud.Text = "";
        Txt_Longitud.Text = "";
        //Volumenes Anuales
        Txt_Volumen_Consumo.Text = "";
        Txt_Volumen_Extraccion.Text = "";
        Txt_Volumen_Descarga.Text = "";
        //Equipo
        Txt_Diametro_Columna_Succionadora.Text = "";
        Txt_Diametro_Descarga.Text = "";
        Txt_Tipo_Bomba.Text = "";
        Txt_Accionada_Por_Motor.Text = "";
        Txt_Medidor.Text = "";
        //Caracteristicas del Aprovechamiento
        Txt_Profundidad.Text = "";
        Txt_Diametro_Perforacion.Text = "";
        Txt_Diametro_Ademe.Text = "";
        //Gasto (1/Seg)
        Txt_Requerido.Text = "";
        Txt_Maximo.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Afectaciones
    ///DESCRIPCIÓN: Habilita los campos de la Seccion de Afectaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habilitar_Caracteristicas(Boolean Habilitar)
    {
        //Datos Generales
        Txt_Estado_Actual.Enabled = Habilitar;
        Txt_Uso_Inicial.Enabled = Habilitar;
        Txt_Usuarios_Beneficiados.Enabled = Habilitar;
        Txt_Anios_Sin_Operar.Enabled = Habilitar;
        //Ubicación
        Txt_Region.Enabled = Habilitar;
        Txt_Acuifero.Enabled = Habilitar;
        Txt_Cuenca.Enabled = Habilitar;
        Txt_Latitud.Enabled = Habilitar;
        Txt_Longitud.Enabled = Habilitar;
        //Volumenes Anuales
        Txt_Volumen_Consumo.Enabled = Habilitar;
        Txt_Volumen_Extraccion.Enabled = Habilitar;
        Txt_Volumen_Descarga.Enabled = Habilitar;
        //Equipo
        Txt_Diametro_Columna_Succionadora.Enabled = Habilitar;
        Txt_Diametro_Descarga.Enabled = Habilitar;
        Txt_Tipo_Bomba.Enabled = Habilitar;
        Txt_Accionada_Por_Motor.Enabled = Habilitar;
        Txt_Medidor.Enabled = Habilitar;
        //Caracteristicas del Aprovechamiento
        Txt_Profundidad.Enabled = Habilitar;
        Txt_Diametro_Perforacion.Enabled = Habilitar;
        Txt_Diametro_Ademe.Enabled = Habilitar;
        //Gasto (1/Seg)
        Txt_Requerido.Enabled = Habilitar;
        Txt_Maximo.Enabled = Habilitar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Anexos
    ///DESCRIPCIÓN: Limpia los campos de la Seccion de Anexos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Anexos()
    {
        Cmb_Tipo_Archivo.SelectedIndex = 0;
        Txt_Descripcion_Archivo.Text = "";
        if (AFU_Ruta_Archivo != null)
            Remover_Sesiones_Control_AsyncFileUpload(AFU_Ruta_Archivo.ClientID);
        Session.Remove("Anexo_Cargado");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Anexos
    ///DESCRIPCIÓN: Habilita los campos de la Seccion de Anexos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habilitar_Anexos(Boolean Habilitar)
    {
        Cmb_Tipo_Archivo.Enabled = Habilitar;
        Txt_Descripcion_Archivo.Enabled = Habilitar;
        Grid_Listado_Anexos.Columns[7].Visible = Habilitar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Medidas_Colindancias
    ///DESCRIPCIÓN: Limpia los campos de la Seccion de Medidas y Colindancias
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Medidas_Colindancias()
    {
        Cmb_Orientacion.SelectedIndex = 0;
        Txt_Medida.Text = "";
        Txt_Colindancia.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Medidas_Colindancias
    ///DESCRIPCIÓN: Habilita los campos de la Seccion de Medidas y Colindancias
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habilitar_Medidas_Colindancias(Boolean Habilitar)
    {
        Cmb_Orientacion.Enabled = Habilitar;
        Txt_Medida.Enabled = Habilitar;
        Txt_Colindancia.Enabled = Habilitar;
        Btn_Agregar_Medida_Colindancia.Visible = Habilitar;
        Grid_Listado_Medidas_Colindancias.Columns[3].Visible = Habilitar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Sustento_Juridico
    ///DESCRIPCIÓN: Limpia los campos de la Seccion de Juridico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Sustento_Juridico()
    {
        //Alta
        Hdf_No_Registro_Juridico_Alta.Value = "";
        Txt_Escritura.Text = "";
        Txt_Fecha_Escritura.Text = "";
        Txt_No_Notario.Text = "";
        Txt_Constacia_Registral.Text = "";
        Txt_Nombre_Notario.Text = "";
        Txt_Folio_Real.Text = "";
        Cmb_Libertad_Gravament.SelectedIndex = 0;
        Txt_Antecedente_Registral.Text = "";
        Txt_No_Consecion.Text = "";

        //Baja
        Txt_Fecha_Baja.Text = "";
        Hdf_No_Registro_Juridico_Baja.Value = "";
        Txt_Baja_No_Escritura.Text = "";
        Txt_Baja_Fecha_Escritura.Text = "";
        Txt_Baja_No_Notario.Text = "";
        Txt_Baja_Constancia_Registral.Text = "";
        Txt_Baja_Nombre_Notario.Text = "";
        Txt_Baja_Folio_Real.Text = "";
        Txt_Baja_Nuevo_Propietario.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habilitar_Sustento_Juridico
    ///DESCRIPCIÓN: Habilita los campos de la Seccion de Juridico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habilitar_Sustento_Juridico(Boolean Habilitar)
    {
        //Alta
        Txt_Escritura.Enabled = Habilitar;
        Txt_Fecha_Escritura.Enabled = false;
        Txt_No_Notario.Enabled = Habilitar;
        Txt_Constacia_Registral.Enabled = Habilitar;
        Txt_Nombre_Notario.Enabled = Habilitar;
        Txt_Folio_Real.Enabled = Habilitar;
        Cmb_Libertad_Gravament.Enabled = Habilitar;
        Txt_Antecedente_Registral.Enabled = Habilitar;
        Btn_Fecha_Escritura.Enabled = Habilitar;
        Txt_No_Consecion.Enabled = Habilitar;

        //Baja
        Txt_Fecha_Baja.Enabled = false;
        Btn_Fecha_Baja.Enabled = Habilitar;
        Txt_Baja_No_Escritura.Enabled = Habilitar;
        Txt_Baja_Fecha_Escritura.Enabled = false;
        Btn_Baja_Fecha_Escritura.Enabled = Habilitar;
        Txt_Baja_No_Notario.Enabled = Habilitar;
        Txt_Baja_Constancia_Registral.Enabled = Habilitar;
        Txt_Baja_Nombre_Notario.Enabled = Habilitar;
        Txt_Baja_Folio_Real.Enabled = Habilitar;
        Txt_Baja_Nuevo_Propietario.Enabled = Habilitar;
        Txt_Baja_No_Contrato.Enabled = Habilitar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Generales_Formulario
    ///DESCRIPCIÓN: Limpia los campos de todo el formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Generales_Formulario()
    {
        Hdf_Bien_Inmueble_ID.Value = "";
        Txt_Bien_Inmueble_ID.Text = "";
        Txt_Clave.Text = "";
        Txt_Nombre_Comun.Text = "";
        Txt_Fecha_Alta_Cta_Pub.Text = "";
        Txt_Calle.Text = "";
        Txt_Numero_Exterior.Text = "";
        Txt_Numero_Interior.Text = "";
        Txt_Colonia.Text = "";
        Cmb_Uso.SelectedIndex = 0;
        Cmb_Destino.SelectedIndex = 0;
        Cmb_Origen.SelectedIndex = 0;
        Cmb_Estatus.SelectedIndex = 0;
        Txt_Superficie.Text = "";
        Txt_Construccion_Resgistrada.Text = "";
        Txt_Fecha_Registro.Text = "";
        Txt_Manzana.Text = "";
        Txt_Lote.Text = "";
        Txt_Porcentaje_Ocupacion.Text = "";
        Txt_Numero_Cuenta_Predial.Text = "";
        Txt_Valor_Comercial.Text = "";
        Cmb_Sector.SelectedIndex = 0;
        Cmb_Sector.SelectedIndex = 0;
        Cmb_Clasificacion_Zona.SelectedIndex = 0;
        Txt_Vias_Aceso.Text = "";
        Cmb_Estado.SelectedIndex = 0;
        Txt_Observaciones.Text = "";
        Txt_Expropiacion.Text = "";
        Limpiar_Medidas_Colindancias();
        Limpiar_Sustento_Juridico();
        Limpiar_Anexos();
        Grid_Observaciones.DataSource = new DataTable();
        Grid_Observaciones.DataBind();
        Llenar_Listado_Medidas_Colindancias(new DataTable());
        Llenar_Listado_Anexos(new DataTable());
        Limpiar_Caracteristicas();
        Limpiar_Contabilidad();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Habiliar_Generales_Formulario
    ///DESCRIPCIÓN: Habilita los campos de todo el formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Habiliar_Generales_Formulario(Boolean Habilitar)
    {
        if (!Habilitar)
        {
            Btn_Nuevo.Visible = false;
            Btn_Nuevo.ToolTip = "Nuevo";
            Btn_Nuevo.AlternateText = "Nuevo";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
            Btn_Modificar.Visible = true;
            Btn_Modificar.ToolTip = "Modificar";
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Salir.ToolTip = "Salir";
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            Btn_Ver_Ficha_Tecnica_PDF.Visible = true;
            Btn_Ver_Ficha_Tecnica_Excel.Visible = true;
            Btn_Exportar_Caractetisticas.Visible = true;
        }
        else
        {
            Btn_Nuevo.ToolTip = "Dar de Alta";
            Btn_Nuevo.AlternateText = "Dar de Alta";
            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
            Btn_Modificar.ToolTip = "Actualizar Cambios";
            Btn_Modificar.AlternateText = "Actualizar Cambios";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
            Btn_Salir.ToolTip = "Cancelar";
            Btn_Salir.AlternateText = "Cancelar";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
            Btn_Ver_Ficha_Tecnica_PDF.Visible = false;
            Btn_Ver_Ficha_Tecnica_Excel.Visible = false;
            Btn_Exportar_Caractetisticas.Visible = false;
        }
        Btn_Fecha_Alta_Cta_Pub.Enabled = Habilitar;
        Txt_Calle.Enabled = Habilitar;
        Txt_Clave.Enabled = Habilitar;
        Txt_Nombre_Comun.Enabled = Habilitar;
        Txt_Numero_Exterior.Enabled = Habilitar;
        Txt_Numero_Interior.Enabled = Habilitar;
        Txt_Colonia.Enabled = Habilitar;
        Cmb_Uso.Enabled = Habilitar;
        Cmb_Destino.Enabled = Habilitar;
        Cmb_Origen.Enabled = Habilitar;
        Cmb_Origen_SelectedIndexChanged(Cmb_Origen, null);
        Cmb_Estatus.Enabled = Habilitar;
        Txt_Superficie.Enabled = Habilitar;
        Txt_Construccion_Resgistrada.Enabled = Habilitar;
        Btn_Fecha_Registro.Enabled = Habilitar;
        Txt_Manzana.Enabled = Habilitar;
        Txt_Lote.Enabled = Habilitar;
        Txt_Porcentaje_Ocupacion.Enabled = Habilitar;
        Txt_Numero_Cuenta_Predial.Enabled = Habilitar;
        Cmb_Distrito.Enabled = Habilitar;
        Cmb_Sector.Enabled = Habilitar;
        Cmb_Clasificacion_Zona.Enabled = Habilitar;
        Txt_Valor_Comercial.Enabled = Habilitar;
        Txt_Vias_Aceso.Enabled = Habilitar;
        if (Cmb_Estado.SelectedItem.Value == "BAJA") { Cmb_Estado.Enabled = false; } else { Cmb_Estado.Enabled = Habilitar; }
        Txt_Observaciones.Enabled = Habilitar;
        Txt_Expropiacion.Enabled = Habilitar;
        Habilitar_Medidas_Colindancias(Habilitar);
        Habilitar_Sustento_Juridico(Habilitar);
        Habilitar_Anexos(Habilitar);
        Habilitar_Caracteristicas(Habilitar);
        Habilitar_Contabilidad(Habilitar);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Uso
    ///DESCRIPCIÓN: Llena el Combo de Usos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Uso()
    {
        Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio Uso_Suelo = new Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio();
        Uso_Suelo.P_Estatus = "VIGENTE";
        Cmb_Uso.DataSource = Uso_Suelo.Consultar_Usos();
        Cmb_Uso.DataTextField = "DESCRIPCION";
        Cmb_Uso.DataValueField = "USO_ID";
        Cmb_Uso.DataBind();
        Cmb_Uso.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Destino
    ///DESCRIPCIÓN: Llena el Combo de Destinos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Destino()
    {
        Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio Destino_Suelo = new Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio();
        Destino_Suelo.P_Estatus = "VIGENTE";
        Cmb_Destino.DataSource = Destino_Suelo.Consultar_Destinos();
        Cmb_Destino.DataTextField = "DESCRIPCION";
        Cmb_Destino.DataValueField = "DESTINO_ID";
        Cmb_Destino.DataBind();
        Cmb_Destino.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Clase_Activo
    ///DESCRIPCIÓN: Llena el Combo de Clases de Activos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Clase_Activo()
    {
        Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
        CA_Negocio.P_Estatus = "VIGENTE";
        CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
        Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
        Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
        Cmb_Clase_Activo.DataTextField = "CLAVE_DESCRIPCION";
        Cmb_Clase_Activo.DataBind();
        Cmb_Clase_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
        Cmb_Clase_Activo_Terreno.DataSource = CA_Negocio.Consultar_DataTable();
        Cmb_Clase_Activo_Terreno.DataValueField = "CLASE_ACTIVO_ID";
        Cmb_Clase_Activo_Terreno.DataTextField = "CLAVE_DESCRIPCION";
        Cmb_Clase_Activo_Terreno.DataBind();
        Cmb_Clase_Activo_Terreno.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Origenes
    ///DESCRIPCIÓN: Llena el Combo de Origenes
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************   
    private void Llenar_Combo_Origenes()
    {
        Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio Destino_Suelo = new Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio();
        Destino_Suelo.P_Estatus = "VIGENTE";
        Cmb_Origen.DataSource = Destino_Suelo.Consultar_Origenes();
        Cmb_Origen.DataTextField = "NOMBRE";
        Cmb_Origen.DataValueField = "ORIGEN_ID";
        Cmb_Origen.DataBind();
        Cmb_Origen.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Sectores
    ///DESCRIPCIÓN: Llena el Combo de Sectores
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Sectores()
    {
        Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio Sectores = new Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio();
        Sectores.P_Estatus = "VIGENTE";
        Cmb_Sector.DataSource = Sectores.Consultar_Orientaciones();
        Cmb_Sector.DataTextField = "DESCRIPCION";
        Cmb_Sector.DataValueField = "ORIENTACION_ID";
        Cmb_Sector.DataBind();
        Cmb_Sector.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Clasificaciones_Zonas
    ///DESCRIPCIÓN: Llena el Combo de Clasificaciones de Zonas
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Clasificaciones_Zonas()
    {
        Cls_Cat_Pat_Com_Clasificaciones_Zonas_Inmuebles_Negocio Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Zonas_Inmuebles_Negocio();
        Negocio.P_Estatus = "VIGENTE";
        Cmb_Clasificacion_Zona.DataSource = Negocio.Consultar_Clasificaciones();
        Cmb_Clasificacion_Zona.DataTextField = "DESCRIPCION";
        Cmb_Clasificacion_Zona.DataValueField = "CLASIFICACION_ID";
        Cmb_Clasificacion_Zona.DataBind();
        Cmb_Clasificacion_Zona.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Orientaciones
    ///DESCRIPCIÓN: Llena el Combo de Orientaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Orientaciones()
    {
        Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio Orientaciones = new Cls_Cat_Pat_Com_Orientaciones_Inmuebles_Negocio();
        Orientaciones.P_Estatus = "VIGENTE";
        Cmb_Orientacion.DataSource = Orientaciones.Consultar_Orientaciones();
        Cmb_Orientacion.DataTextField = "DESCRIPCION";
        Cmb_Orientacion.DataValueField = "ORIENTACION_ID";
        Cmb_Orientacion.DataBind();
        Cmb_Orientacion.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Alta
    ///DESCRIPCIÓN: Registra en la Base de Datos el Bien Inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private Cls_Ope_Pat_Bienes_Inmuebles_Negocio Alta()
    {
        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
        if (Txt_Fecha_Alta_Cta_Pub.Text.Trim().Length > 0) { BI_Negocio.P_Fecha_Alta_Cuenta_Publica = Convert.ToDateTime(Txt_Fecha_Alta_Cta_Pub.Text); }
        BI_Negocio.P_Clave = Txt_Clave.Text.Trim();
        BI_Negocio.P_Nombre_Comun = Txt_Nombre_Comun.Text.Trim();
        BI_Negocio.P_Calle = Txt_Calle.Text.Trim();
        BI_Negocio.P_Colonia = Txt_Colonia.Text.Trim();
        if (Cmb_Uso.SelectedIndex > 0) { BI_Negocio.P_Uso_ID = Cmb_Uso.SelectedItem.Value; }
        if (Cmb_Origen.SelectedIndex > 0) { BI_Negocio.P_Origen_ID = Cmb_Origen.SelectedItem.Value; }
        if (Cmb_Estatus.SelectedIndex > 0) { BI_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value; }
        if (Txt_Superficie.Text.Trim().Length > 0) { BI_Negocio.P_Superficie = Convert.ToDouble(Txt_Superficie.Text); }
        if (Txt_Construccion_Resgistrada.Text.Trim().Length > 0) { BI_Negocio.P_Construccion_Construida = Convert.ToDouble(Txt_Construccion_Resgistrada.Text); }
        BI_Negocio.P_Manzana = Txt_Manzana.Text.Trim();
        BI_Negocio.P_Cuenta_Predial_ID = Txt_Numero_Cuenta_Predial.Text.Trim();
        BI_Negocio.P_Lote = Txt_Lote.Text.Trim();
        if (Txt_Porcentaje_Ocupacion.Text.Trim().Length > 0) { BI_Negocio.P_Ocupacion = Convert.ToDouble(Txt_Porcentaje_Ocupacion.Text); }
        if (Cmb_Sector.SelectedIndex > 0) { BI_Negocio.P_Sector_ID = Cmb_Sector.SelectedItem.Value; }
        if (Cmb_Clasificacion_Zona.SelectedIndex > 0) { BI_Negocio.P_Clasificacion_Zona_ID = Cmb_Clasificacion_Zona.SelectedItem.Value; }
        if (Txt_Valor_Comercial.Text.Trim().Length > 0) { BI_Negocio.P_Valor_Comercial = Convert.ToDouble(Txt_Valor_Comercial.Text); }
        BI_Negocio.P_Vias_Acceso = Txt_Vias_Aceso.Text.Trim();
        BI_Negocio.P_Estado = Cmb_Estado.SelectedItem.Value;
        BI_Negocio.P_No_Exterior = Txt_Numero_Exterior.Text.Trim();
        BI_Negocio.P_No_Interior = Txt_Numero_Interior.Text.Trim();
        if (Cmb_Destino.SelectedIndex > 0) { BI_Negocio.P_Destino_ID = Cmb_Destino.SelectedItem.Value; }
        BI_Negocio.P_Observaciones = Txt_Observaciones.Text.Trim();
        BI_Negocio.P_Fecha_Registro = Convert.ToDateTime(Txt_Fecha_Registro.Text);
        BI_Negocio.P_Dt_Medidas_Colindancias = (Session["Dt_Medidas_Colindancias"] != null) ? ((DataTable)Session["Dt_Medidas_Colindancias"]) : new DataTable();
        BI_Negocio.P_Distrito_ID = Cmb_Distrito.SelectedItem.Value;

        //Contabilidad
        BI_Negocio.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value;
        if (Txt_Cuenta_Contable.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Contable_ID = Hdf_Cuenta_Contable_ID.Value;
        BI_Negocio.P_Nombre_Perito = Txt_Nombre_Perito.Text.Trim();
        if (Txt_Fecha_Avaluo.Text.Trim().Length > 0) BI_Negocio.P_Fecha_Avaluo = Convert.ToDateTime(Txt_Fecha_Avaluo.Text);
        if (Txt_Valor_Avaluo.Text.Trim().Length > 0) { BI_Negocio.P_Valor_Fiscal = Convert.ToDouble(Txt_Valor_Avaluo.Text); BI_Negocio.P_Valor_Actual = Convert.ToDouble(Txt_Valor_Avaluo.Text); }
        BI_Negocio.P_Clase_Activo_Terreno_ID = Cmb_Clase_Activo_Terreno.SelectedItem.Value;
        if (Txt_Cuenta_Contable_Terreno.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_Terreno_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Contable_Terreno_ID = Hdf_Cuenta_Contable_Terreno_ID.Value;
        BI_Negocio.P_Nombre_Perito_Terreno = Txt_Nombre_Perito_Terreno.Text.Trim();
        if (Txt_Fecha_Avaluo_Terreno.Text.Trim().Length > 0) BI_Negocio.P_Fecha_Avaluo_Terreno = Convert.ToDateTime(Txt_Fecha_Avaluo_Terreno.Text);
        if (Txt_Valor_Avaluo_Terreno.Text.Trim().Length > 0) BI_Negocio.P_Valor_Fiscal_Terreno = Convert.ToDouble(Txt_Valor_Avaluo_Terreno.Text);
        if (Txt_Cuenta_Gasto_ID.Text.Trim().Length > 0 && Hdf_Cuenta_Gasto_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Gasto_ID = Hdf_Cuenta_Gasto_ID.Value;
        //Juridico
        if (Txt_Escritura.Text.Trim().Length > 0)
        {
            BI_Negocio.P_No_Escritura = Txt_Escritura.Text;
            BI_Negocio.P_Fecha_Escritura = Convert.ToDateTime(Txt_Fecha_Escritura.Text);
            BI_Negocio.P_No_Notario = Txt_No_Notario.Text.Trim();
            BI_Negocio.P_Notario_Nombre = Txt_Nombre_Notario.Text.Trim();
            BI_Negocio.P_Constancia_Registral = Txt_Constacia_Registral.Text;
            BI_Negocio.P_Folio_Real = Txt_Folio_Real.Text;
            BI_Negocio.P_Libre_Gravament = Cmb_Libertad_Gravament.SelectedItem.Value;
            BI_Negocio.P_Antecedente = Txt_Antecedente_Registral.Text;
            BI_Negocio.P_No_Consecion = Txt_No_Consecion.Text.Trim();
        }

        String Nombre_Archivo = (DateTime.Now).ToString().Replace(".", "").Replace(" ", "").Replace("/", "").Replace("-", "").Replace(":", "");
        if (Session["Anexo_Cargado"] != null)
        {
            AsyncFileUpload AFU = (AsyncFileUpload)Session["Anexo_Cargado"];
            if (AFU.HasFile)
            {
                BI_Negocio.P_Tipo_Anexo = Cmb_Tipo_Archivo.SelectedItem.Value.Trim();
                BI_Negocio.P_Archivo = Nombre_Archivo + "_" + AFU.FileName;
                BI_Negocio.P_Descripcion_Anexo = Txt_Descripcion_Archivo.Text.Trim();
            }
        }

        BI_Negocio.P_Dt_Caracteristicas = Obtener_Caracteristicas();

        //Expropiaciones
        BI_Negocio.P_Expropiacion = Txt_Expropiacion.Text.Trim();

        BI_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
        BI_Negocio.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
        BI_Negocio = BI_Negocio.Alta_Bien_Inmueble();
        return BI_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar
    ///DESCRIPCIÓN: Actualiza en la Base de Datos el Bien Inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private Cls_Ope_Pat_Bienes_Inmuebles_Negocio Modificar()
    {
        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
        if (Txt_Fecha_Alta_Cta_Pub.Text.Trim().Length > 0) { BI_Negocio.P_Fecha_Alta_Cuenta_Publica = Convert.ToDateTime(Txt_Fecha_Alta_Cta_Pub.Text); }
        BI_Negocio.P_Bien_Inmueble_ID = Hdf_Bien_Inmueble_ID.Value;
        BI_Negocio.P_Calle = Txt_Calle.Text.Trim();
        BI_Negocio.P_Colonia = Txt_Colonia.Text.Trim();
        BI_Negocio.P_Clave = Txt_Clave.Text.Trim();
        BI_Negocio.P_Nombre_Comun = Txt_Nombre_Comun.Text.Trim();
        BI_Negocio.P_Uso_ID = Cmb_Uso.SelectedItem.Value;
        BI_Negocio.P_Origen_ID = Cmb_Origen.SelectedItem.Value;
        BI_Negocio.P_Estatus = Cmb_Estatus.SelectedItem.Value;
        if (Txt_Superficie.Text.Trim().Length > 0) { BI_Negocio.P_Superficie = Convert.ToDouble(Txt_Superficie.Text); }
        if (Txt_Construccion_Resgistrada.Text.Trim().Length > 0) { BI_Negocio.P_Construccion_Construida = Convert.ToDouble(Txt_Construccion_Resgistrada.Text); }
        BI_Negocio.P_Manzana = Txt_Manzana.Text.Trim();
        BI_Negocio.P_Cuenta_Predial_ID = Txt_Numero_Cuenta_Predial.Text.Trim();
        BI_Negocio.P_Lote = Txt_Lote.Text.Trim();
        if (Txt_Porcentaje_Ocupacion.Text.Trim().Length > 0) { BI_Negocio.P_Ocupacion = Convert.ToDouble(Txt_Porcentaje_Ocupacion.Text); }
        BI_Negocio.P_Sector_ID = Cmb_Sector.SelectedItem.Value;
        BI_Negocio.P_Clasificacion_Zona_ID = Cmb_Clasificacion_Zona.SelectedItem.Value;
        if (Txt_Valor_Comercial.Text.Trim().Length > 0) { BI_Negocio.P_Valor_Comercial = Convert.ToDouble(Txt_Valor_Comercial.Text); }
        BI_Negocio.P_Vias_Acceso = Txt_Vias_Aceso.Text.Trim();
        BI_Negocio.P_Estado = Cmb_Estado.SelectedItem.Value;
        BI_Negocio.P_No_Exterior = Txt_Numero_Exterior.Text.Trim();
        BI_Negocio.P_No_Interior = Txt_Numero_Interior.Text.Trim();
        BI_Negocio.P_Destino_ID = Cmb_Destino.SelectedItem.Value;
        BI_Negocio.P_Distrito_ID = Cmb_Distrito.SelectedItem.Value;
        BI_Negocio.P_Observaciones = Txt_Observaciones.Text.Trim();
        BI_Negocio.P_Fecha_Registro = Convert.ToDateTime(Txt_Fecha_Registro.Text);
        BI_Negocio.P_Dt_Medidas_Colindancias = (Session["Dt_Medidas_Colindancias"] != null) ? ((DataTable)Session["Dt_Medidas_Colindancias"]) : new DataTable();
        BI_Negocio.P_Dt_Anexos_Bajas = (Session["Dt_Anexos_Baja"] != null) ? ((DataTable)Session["Dt_Anexos_Baja"]) : new DataTable();


        //Contabilidad
        BI_Negocio.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value;
        if (Txt_Cuenta_Contable.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Contable_ID = Hdf_Cuenta_Contable_ID.Value;
        BI_Negocio.P_Nombre_Perito = Txt_Nombre_Perito.Text.Trim();
        if (Txt_Fecha_Avaluo.Text.Trim().Length > 0) BI_Negocio.P_Fecha_Avaluo = Convert.ToDateTime(Txt_Fecha_Avaluo.Text);
        if (Txt_Valor_Avaluo.Text.Trim().Length > 0) BI_Negocio.P_Valor_Fiscal = Convert.ToDouble(Txt_Valor_Avaluo.Text);
        BI_Negocio.P_Clase_Activo_Terreno_ID = Cmb_Clase_Activo_Terreno.SelectedItem.Value;
        if (Txt_Cuenta_Contable_Terreno.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_Terreno_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Contable_Terreno_ID = Hdf_Cuenta_Contable_Terreno_ID.Value;
        BI_Negocio.P_Nombre_Perito_Terreno = Txt_Nombre_Perito_Terreno.Text.Trim();
        if (Txt_Fecha_Avaluo_Terreno.Text.Trim().Length > 0) BI_Negocio.P_Fecha_Avaluo_Terreno = Convert.ToDateTime(Txt_Fecha_Avaluo_Terreno.Text);
        if (Txt_Valor_Avaluo_Terreno.Text.Trim().Length > 0) BI_Negocio.P_Valor_Fiscal_Terreno = Convert.ToDouble(Txt_Valor_Avaluo_Terreno.Text);
        if (Txt_Cuenta_Gasto_ID.Text.Trim().Length > 0 && Hdf_Cuenta_Gasto_ID.Value.Trim().Length > 0) BI_Negocio.P_Cuenta_Gasto_ID = Hdf_Cuenta_Gasto_ID.Value;
        if (Txt_Valor_Actual.Text.Trim().Length > 0) BI_Negocio.P_Valor_Actual = Convert.ToDouble(Txt_Valor_Actual.Text.Replace("$", "").Replace(",", "").Trim());

        //Juridico
        if (Txt_Escritura.Text.Trim().Length > 0)
        {
            BI_Negocio.P_No_Escritura = Txt_Escritura.Text;
            BI_Negocio.P_Fecha_Escritura = Convert.ToDateTime(Txt_Fecha_Escritura.Text);
            BI_Negocio.P_No_Notario = Txt_No_Notario.Text.Trim();
            BI_Negocio.P_Notario_Nombre = Txt_Nombre_Notario.Text.Trim();
            BI_Negocio.P_Constancia_Registral = Txt_Constacia_Registral.Text;
            BI_Negocio.P_Folio_Real = Txt_Folio_Real.Text;
            BI_Negocio.P_Libre_Gravament = Cmb_Libertad_Gravament.SelectedItem.Value;
            BI_Negocio.P_Antecedente = Txt_Antecedente_Registral.Text;
            BI_Negocio.P_No_Registro_Alta_Juridico = Hdf_No_Registro_Juridico_Alta.Value.Trim();
            BI_Negocio.P_No_Consecion = Txt_No_Consecion.Text.Trim();
        }
        if (Cmb_Estado.SelectedItem.Value == "BAJA")
        {
            BI_Negocio.P_Fecha_Baja = Convert.ToDateTime(Txt_Fecha_Baja.Text);
            BI_Negocio.P_No_Escritura_Baja = Txt_Baja_No_Escritura.Text;
            BI_Negocio.P_Fecha_Escritura_Baja = Convert.ToDateTime(Txt_Baja_Fecha_Escritura.Text);
            BI_Negocio.P_No_Notario_Baja = Txt_Baja_No_Notario.Text.Trim();
            BI_Negocio.P_Notario_Nombre_Baja = Txt_Baja_Nombre_Notario.Text.Trim();
            BI_Negocio.P_Constancia_Registral_Baja = Txt_Baja_Constancia_Registral.Text;
            BI_Negocio.P_Folio_Real_Baja = Txt_Baja_Folio_Real.Text;
            BI_Negocio.P_Nuevo_Propietario_Juridico = Txt_Baja_Nuevo_Propietario.Text;
            BI_Negocio.P_No_Contrato_Baja = Txt_Baja_No_Contrato.Text;
            BI_Negocio.P_No_Registro_Baja_Juridico = Hdf_No_Registro_Juridico_Baja.Value.Trim();
        }

        String Nombre_Archivo = (DateTime.Now).ToString().Replace(".", "").Replace(" ", "").Replace("/", "").Replace("-", "").Replace(":", "");
        if (Session["Anexo_Cargado"] != null)
        {
            AsyncFileUpload AFU = (AsyncFileUpload)Session["Anexo_Cargado"];
            if (AFU.HasFile)
            {
                BI_Negocio.P_Tipo_Anexo = Cmb_Tipo_Archivo.SelectedItem.Value.Trim();
                BI_Negocio.P_Archivo = Nombre_Archivo + "_" + AFU.FileName;
                BI_Negocio.P_Descripcion_Anexo = Txt_Descripcion_Archivo.Text.Trim();
            }
        }

        BI_Negocio.P_Dt_Caracteristicas = Obtener_Caracteristicas();

        //Expropiaciones
        BI_Negocio.P_Expropiacion = Txt_Expropiacion.Text.Trim();

        BI_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
        BI_Negocio.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
        BI_Negocio.Modifica_Bien_Inmueble();
        return BI_Negocio;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Bien_Inmueble
    ///DESCRIPCIÓN: Muestra detalles del Bien inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Mostrar_Detalles_Bien_Inmueble()
    {
        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
        BI_Negocio.P_Bien_Inmueble_ID = Hdf_Bien_Inmueble_ID.Value;
        BI_Negocio = BI_Negocio.Consultar_Detalles_Bien_Inmueble();
        Txt_Bien_Inmueble_ID.Text = BI_Negocio.P_Bien_Inmueble_ID;
        Txt_Clave.Text = BI_Negocio.P_Clave;
        Txt_Nombre_Comun.Text = BI_Negocio.P_Nombre_Comun;
        Txt_Calle.Text = BI_Negocio.P_Calle;
        Txt_Colonia.Text = BI_Negocio.P_Colonia;
        if (BI_Negocio.P_Uso_ID != null && BI_Negocio.P_Uso_ID.Trim().Length > 0) { Cmb_Uso.SelectedIndex = Cmb_Uso.Items.IndexOf(Cmb_Uso.Items.FindByValue(BI_Negocio.P_Uso_ID)); }
        if (BI_Negocio.P_Origen_ID != null && BI_Negocio.P_Origen_ID.Trim().Length > 0) { Cmb_Origen.SelectedIndex = Cmb_Origen.Items.IndexOf(Cmb_Origen.Items.FindByValue(BI_Negocio.P_Origen_ID)); }
        if (BI_Negocio.P_Estatus != null && BI_Negocio.P_Estatus.Trim().Length > 0) { Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(BI_Negocio.P_Estatus)); }
        if (BI_Negocio.P_Superficie > (-1)) { Txt_Superficie.Text = BI_Negocio.P_Superficie.ToString(); }
        if (BI_Negocio.P_Construccion_Construida > (-1)) { Txt_Construccion_Resgistrada.Text = BI_Negocio.P_Construccion_Construida.ToString(); }
        if (BI_Negocio.P_Manzana != null && BI_Negocio.P_Manzana.Trim().Length > 0) { Txt_Manzana.Text = BI_Negocio.P_Manzana.Trim(); }
        Txt_Numero_Cuenta_Predial.Text = BI_Negocio.P_Cuenta_Predial_ID;
        Cmb_Distrito.SelectedIndex = Cmb_Distrito.Items.IndexOf(Cmb_Distrito.Items.FindByValue(BI_Negocio.P_Distrito_ID));
        if (BI_Negocio.P_Lote != null && BI_Negocio.P_Lote.Trim().Length > 0) { Txt_Lote.Text = BI_Negocio.P_Lote.Trim(); }
        if (BI_Negocio.P_Ocupacion > (-1)) { Txt_Porcentaje_Ocupacion.Text = BI_Negocio.P_Ocupacion.ToString(); }
        if (BI_Negocio.P_Sector_ID != null && BI_Negocio.P_Sector_ID.Trim().Length > 0) { Cmb_Sector.SelectedIndex = Cmb_Sector.Items.IndexOf(Cmb_Sector.Items.FindByValue(BI_Negocio.P_Sector_ID)); }
        if (BI_Negocio.P_Clasificacion_Zona_ID != null && BI_Negocio.P_Clasificacion_Zona_ID.Trim().Length > 0) { Cmb_Clasificacion_Zona.SelectedIndex = Cmb_Clasificacion_Zona.Items.IndexOf(Cmb_Clasificacion_Zona.Items.FindByValue(BI_Negocio.P_Clasificacion_Zona_ID)); }
        if (BI_Negocio.P_Valor_Comercial > (-1)) { Txt_Valor_Comercial.Text = BI_Negocio.P_Valor_Comercial.ToString(); }
        if (BI_Negocio.P_Vias_Acceso != null && BI_Negocio.P_Vias_Acceso.Trim().Length > 0) { Txt_Vias_Aceso.Text = BI_Negocio.P_Vias_Acceso.Trim(); }
        Cmb_Estado.SelectedIndex = Cmb_Estado.Items.IndexOf(Cmb_Estado.Items.FindByValue(BI_Negocio.P_Estado));
        Cmb_Estado_SelectedIndexChanged(Cmb_Estado, null);
        if (BI_Negocio.P_No_Exterior != null && BI_Negocio.P_No_Exterior.Trim().Length > 0) { Txt_Numero_Exterior.Text = BI_Negocio.P_No_Exterior.Trim(); }
        if (BI_Negocio.P_No_Interior != null && BI_Negocio.P_No_Interior.Trim().Length > 0) { Txt_Numero_Interior.Text = BI_Negocio.P_No_Interior.Trim(); }
        if (BI_Negocio.P_Destino_ID != null && BI_Negocio.P_Destino_ID.Trim().Length > 0) { Cmb_Destino.SelectedIndex = Cmb_Destino.Items.IndexOf(Cmb_Destino.Items.FindByValue(BI_Negocio.P_Destino_ID)); }
        if (!String.Format("{0:ddMMyyyy}", BI_Negocio.P_Fecha_Registro).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) { Txt_Fecha_Registro.Text = String.Format("{0:dd/MMM/yyyy}", BI_Negocio.P_Fecha_Registro); } else { Txt_Fecha_Registro.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today); }
        if (!String.Format("{0:ddMMyyyy}", BI_Negocio.P_Fecha_Baja).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) { Txt_Fecha_Baja.Text = String.Format("{0:dd/MMM/yyyy}", BI_Negocio.P_Fecha_Baja); } else { Txt_Fecha_Baja.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today); }
        if (!String.Format("{0:ddMMyyyy}", BI_Negocio.P_Fecha_Alta_Cuenta_Publica).Trim().Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) { Txt_Fecha_Alta_Cta_Pub.Text = String.Format("{0:dd/MMM/yyyy}", BI_Negocio.P_Fecha_Alta_Cuenta_Publica); }

        //Contabilidad
        Cmb_Clase_Activo.SelectedIndex = Cmb_Clase_Activo.Items.IndexOf(Cmb_Clase_Activo.Items.FindByValue(BI_Negocio.P_Clase_Activo_ID));
        if (!String.IsNullOrEmpty(BI_Negocio.P_Cuenta_Contable_ID))
        {
            Hdf_Cuenta_Contable_ID.Value = BI_Negocio.P_Cuenta_Contable_ID;
            DataTable Dt_Datos = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", BI_Negocio.P_Cuenta_Contable_ID);
            if (Dt_Datos != null) if (Dt_Datos.Rows.Count > 0) Txt_Cuenta_Contable.Text = Dt_Datos.Rows[0]["cuenta_descripcion"].ToString().Trim();
        }
        Txt_Nombre_Perito.Text = BI_Negocio.P_Nombre_Perito;
        if (!String.Format("{0:ddMMyyyy}", BI_Negocio.P_Fecha_Avaluo).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) Txt_Fecha_Avaluo.Text = String.Format("{0:dd/MMM/yyyy}", BI_Negocio.P_Fecha_Avaluo);
        if (BI_Negocio.P_Valor_Fiscal > 0) Txt_Valor_Avaluo.Text = String.Format("{0:c}", BI_Negocio.P_Valor_Fiscal);
        if (BI_Negocio.P_Valor_Actual > 0) Txt_Valor_Actual.Text = String.Format("{0:c}", BI_Negocio.P_Valor_Actual);
        Cmb_Clase_Activo_Terreno.SelectedIndex = Cmb_Clase_Activo_Terreno.Items.IndexOf(Cmb_Clase_Activo_Terreno.Items.FindByValue(BI_Negocio.P_Clase_Activo_Terreno_ID));
        if (!String.IsNullOrEmpty(BI_Negocio.P_Cuenta_Contable_Terreno_ID))
        {
            Hdf_Cuenta_Contable_Terreno_ID.Value = BI_Negocio.P_Cuenta_Contable_Terreno_ID;
            DataTable Dt_Datos = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", BI_Negocio.P_Cuenta_Contable_Terreno_ID);
            if (Dt_Datos != null) if (Dt_Datos.Rows.Count > 0) Txt_Cuenta_Contable_Terreno.Text = Dt_Datos.Rows[0]["cuenta_descripcion"].ToString().Trim();
        }
        Txt_Nombre_Perito_Terreno.Text = BI_Negocio.P_Nombre_Perito_Terreno;
        if (!String.Format("{0:ddMMyyyy}", BI_Negocio.P_Fecha_Avaluo_Terreno).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) Txt_Fecha_Avaluo_Terreno.Text = String.Format("{0:dd/MMM/yyyy}", BI_Negocio.P_Fecha_Avaluo_Terreno);
        if (BI_Negocio.P_Valor_Fiscal_Terreno > 0) Txt_Valor_Avaluo_Terreno.Text = String.Format("{0:c}", BI_Negocio.P_Valor_Fiscal_Terreno);
        if (!String.IsNullOrEmpty(BI_Negocio.P_Cuenta_Gasto_ID))
        {
            Hdf_Cuenta_Gasto_ID.Value = BI_Negocio.P_Cuenta_Gasto_ID;
            DataTable Dt_Datos = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", BI_Negocio.P_Cuenta_Gasto_ID);
            if (Dt_Datos != null) if (Dt_Datos.Rows.Count > 0) Txt_Cuenta_Gasto_ID.Text = Dt_Datos.Rows[0]["cuenta_descripcion"].ToString().Trim();
        }

        Grid_Observaciones.PageIndex = 0;
        Llenar_Listado_Observaciones(BI_Negocio.P_Dt_Observaciones);
        Grid_Listado_Medidas_Colindancias.PageIndex = 0;
        Llenar_Listado_Medidas_Colindancias(BI_Negocio.P_Dt_Medidas_Colindancias);
        Mostrar_Detalles_Juridico(BI_Negocio.P_Dt_Historico_Juridico);
        Mostrar_Caracteristicas(BI_Negocio.Obtener_Dt_Caracteristicas());
        Grid_Listado_Anexos.PageIndex = 0;
        Llenar_Listado_Anexos(BI_Negocio.P_Dt_Anexos);
        Grid_Expropiaciones.PageIndex = 0;
        Llenar_Listado_Expropiaciones(BI_Negocio.P_Dt_Expropiaciones);
        Caracteres_Permitidos(ref FTE_Txt_Superficie, "AGREGAR", ",");
        Caracteres_Permitidos(ref FTE_Txt_Valor_Comercial, "AGREGAR", ",");
        Caracteres_Permitidos(ref FTE_Txt_Construccion_Resgistrada, "AGREGAR", ",");
        Formato_Numerico(ref Txt_Superficie, "DISTANCIA");
        Formato_Numerico(ref Txt_Valor_Comercial, "DISTANCIA");
        Formato_Numerico(ref Txt_Construccion_Resgistrada, "DISTANCIA");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Juridico
    ///DESCRIPCIÓN: Muestra detalles del Bien inmueble Parte de Juridico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Mostrar_Detalles_Juridico(DataTable Dt_Juridico)
    {
        if (Dt_Juridico != null && Dt_Juridico.Rows.Count > 0)
        {
            DataRow[] Filas_Alta = Dt_Juridico.Select("MOVIMIENTO = 'ALTA'");
            foreach (DataRow Fila_Actual in Filas_Alta)
            {
                Hdf_No_Registro_Juridico_Alta.Value = (Fila_Actual["NO_REGISTRO"] != null) ? Fila_Actual["NO_REGISTRO"].ToString() : "";
                Txt_Escritura.Text = (Fila_Actual["ESCRITURA"] != null) ? Fila_Actual["ESCRITURA"].ToString() : "";
                Txt_Fecha_Escritura.Text = (Fila_Actual["FECHA_ESCRITURA"] != null) ? String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fila_Actual["FECHA_ESCRITURA"])) : "";
                Txt_No_Notario.Text = (Fila_Actual["NO_NOTARIO"] != null) ? Fila_Actual["NO_NOTARIO"].ToString() : "";
                Txt_Constacia_Registral.Text = (Fila_Actual["CONSTANCIA_REGISTRAL"] != null) ? Fila_Actual["CONSTANCIA_REGISTRAL"].ToString() : "";
                Txt_Nombre_Notario.Text = (Fila_Actual["NOMBRE_COMPLETO_NOTARIO"] != null) ? Fila_Actual["NOMBRE_COMPLETO_NOTARIO"].ToString() : "";
                Txt_Folio_Real.Text = (Fila_Actual["FOLIO_REAL"] != null) ? Fila_Actual["FOLIO_REAL"].ToString() : "";
                if (Fila_Actual["LIBERTAD_GRAVAMEN"] != null)
                {
                    Cmb_Libertad_Gravament.SelectedIndex = Cmb_Libertad_Gravament.Items.IndexOf(Cmb_Libertad_Gravament.Items.FindByValue(Fila_Actual["LIBERTAD_GRAVAMEN"].ToString()));
                }
                Txt_Antecedente_Registral.Text = (Fila_Actual["ANTECEDENTE_REGISTRAL"] != null) ? Fila_Actual["ANTECEDENTE_REGISTRAL"].ToString() : "";
                Txt_No_Consecion.Text = (Fila_Actual["NO_CONSECION"] != null) ? Fila_Actual["NO_CONSECION"].ToString() : "";
            }
            DataRow[] Filas_Baja = Dt_Juridico.Select("MOVIMIENTO = 'BAJA'");
            foreach (DataRow Fila_Actual in Filas_Baja)
            {
                Hdf_No_Registro_Juridico_Baja.Value = (Fila_Actual["NO_REGISTRO"] != null) ? Fila_Actual["NO_REGISTRO"].ToString() : "";
                Txt_Baja_No_Escritura.Text = (Fila_Actual["ESCRITURA"] != null) ? Fila_Actual["ESCRITURA"].ToString() : "";
                Txt_Baja_Fecha_Escritura.Text = (Fila_Actual["FECHA_ESCRITURA"] != null) ? String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Fila_Actual["FECHA_ESCRITURA"])) : "";
                Txt_Baja_No_Notario.Text = (Fila_Actual["NO_NOTARIO"] != null) ? Fila_Actual["NO_NOTARIO"].ToString() : "";
                Txt_Baja_Constancia_Registral.Text = (Fila_Actual["CONSTANCIA_REGISTRAL"] != null) ? Fila_Actual["CONSTANCIA_REGISTRAL"].ToString() : "";
                Txt_Baja_Nombre_Notario.Text = (Fila_Actual["NOMBRE_COMPLETO_NOTARIO"] != null) ? Fila_Actual["NOMBRE_COMPLETO_NOTARIO"].ToString() : "";
                Txt_Baja_Folio_Real.Text = (Fila_Actual["FOLIO_REAL"] != null) ? Fila_Actual["FOLIO_REAL"].ToString() : "";
                Txt_Baja_No_Contrato.Text = (Fila_Actual["NO_CONTRATO"] != null) ? Fila_Actual["NO_CONTRATO"].ToString() : "";
                Txt_Baja_Nuevo_Propietario.Text = (Fila_Actual["NUEVO_PROPIETARIO"] != null) ? Fila_Actual["NUEVO_PROPIETARIO"].ToString() : "";
                Cmb_Estado.Enabled = false;
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Distritos
    ///DESCRIPCIÓN: Llena el Combo de los Distritos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Combo_Distritos()
    {
        Cls_Rpt_Pat_Listado_Bienes_Negocio Rpt_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
        Rpt_Negocio.P_Estatus = "ACTIVO', 'ACTIVA";
        DataTable Dt_Distritos = Rpt_Negocio.Consultar_Distritos();
        Cmb_Distrito.DataSource = Dt_Distritos;
        Cmb_Distrito.DataTextField = "NOMBRE";
        Cmb_Distrito.DataValueField = "DISTRITO_ID";
        Cmb_Distrito.DataBind();
        Cmb_Distrito.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Observaciones
    ///DESCRIPCIÓN: Llena el Grid de Historial de Observaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Listado_Observaciones(DataTable Dt_Observaciones)
    {
        Grid_Observaciones.Columns[0].Visible = true;
        Dt_Observaciones.DefaultView.Sort = "NO_OBSERVACION DESC";
        Grid_Observaciones.DataSource = Dt_Observaciones;
        Grid_Observaciones.DataBind();
        Grid_Observaciones.Columns[0].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Medidas_Colindancias
    ///DESCRIPCIÓN: Llena el Grid de Medidas y Colindancias
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Listado_Medidas_Colindancias(DataTable Dt_Medidas_Colindancias)
    {
        if (Dt_Medidas_Colindancias != null && Dt_Medidas_Colindancias.Rows.Count > 0)
        {
            Session["Dt_Medidas_Colindancias"] = Dt_Medidas_Colindancias;
        }
        else
        {
            Session.Remove("Dt_Medidas_Colindancias");
        }
        Grid_Listado_Medidas_Colindancias.DataSource = Dt_Medidas_Colindancias;
        Grid_Listado_Medidas_Colindancias.DataBind();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Expropiaciones
    ///DESCRIPCIÓN: Llena el Grid de Historial de Observaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Listado_Expropiaciones(DataTable Dt_Expropiaciones)
    {
        Grid_Expropiaciones.Columns[0].Visible = true;
        Dt_Expropiaciones.DefaultView.Sort = "NO_EXPROPIACION DESC";
        Grid_Expropiaciones.DataSource = Dt_Expropiaciones;
        Grid_Expropiaciones.DataBind();
        Grid_Expropiaciones.Columns[0].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Anexos
    ///DESCRIPCIÓN: Llena el Grid de Anexos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    private void Llenar_Listado_Anexos(DataTable Dt_Anexos)
    {
        Session.Remove("Dt_Anexos_Baja");
        Grid_Listado_Anexos.Columns[0].Visible = true;
        Grid_Listado_Anexos.Columns[1].Visible = true;
        if (Dt_Anexos.Rows.Count > 0) { Dt_Anexos.DefaultView.Sort = "TIPO_ARCHIVO ASC"; }
        Grid_Listado_Anexos.DataSource = Dt_Anexos;
        Grid_Listado_Anexos.DataBind();
        Grid_Listado_Anexos.Columns[0].Visible = false;
        Grid_Listado_Anexos.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Remover_Sesiones_Control_AsyncFileUpload
    ///DESCRIPCIÓN: Limpia un control de AsyncFileUpload
    ///PROPIEDADES:     
    ///CREO: Juan Alberto Hernandez Negrete
    ///FECHA_CREO: 16/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Remover_Sesiones_Control_AsyncFileUpload(String Cliente_ID)
    {
        HttpContext Contexto;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            Contexto = HttpContext.Current;
        }
        else
        {
            Contexto = null;
        }
        if (Contexto != null)
        {
            foreach (String key in Contexto.Session.Keys)
            {
                if (key.Contains(Cliente_ID))
                {
                    Contexto.Session.Remove(key);
                    break;
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Medidas_Colindancias
    ///DESCRIPCIÓN: Validar los datos de Medidas y Colindancias
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Medidas_Colindancias()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Orientacion.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Seleccionar la Orientación.";
            Validacion = false;
        }
        if (Txt_Medida.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Medida.";
            Validacion = false;
        }
        else
        {
            if (!Validar_Valores_Decimales(Txt_Medida.Text.Trim()))
            {
                if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
                Mensaje_Error = Mensaje_Error + "+ La Medida tiene un Formato Incorrecto.";
                Validacion = false;
            }
        }
        if (Txt_Colindancia.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Colindancia.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Juridico
    ///DESCRIPCIÓN: Valida los datos para datos Juridicos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Juridico()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Txt_Escritura.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el No. de Escritura.";
            Validacion = false;
        }
        if (Txt_Fecha_Escritura.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Fecha de Escritura.";
            Validacion = false;
        }
        if (Txt_No_Notario.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el No. Notario.";
            Validacion = false;
        }
        if (Txt_Nombre_Notario.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre Notario.";
            Validacion = false;
        }
        if (Txt_Antecedente_Registral.Text.Length > 0)
        {
            if (Txt_Antecedente_Registral.Text.Trim().Length > 4000)
            {
                if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
                Int32 Sobrecarga = Txt_Antecedente_Registral.Text.Trim().Length - 4000;
                Mensaje_Error = Mensaje_Error + "+ El Antecedente Registral tiene como longitud Max. 4000 Caracteres [Sobrecarga de " + Sobrecarga;
                if (Sobrecarga > 1) { Mensaje_Error += " Carácteres]."; } else { Mensaje_Error += " Carácter]."; }
                Validacion = false;
            }
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Juridico_Baja
    ///DESCRIPCIÓN: Valida los datos para datos Juridicos cuando hay baja
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Juridico_Baja()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario completar los campos de la Sección de Baja de Registros.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Txt_Baja_No_Escritura.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el No. de Escritura.";
            Validacion = false;
        }
        if (Txt_Baja_Fecha_Escritura.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Fecha de Escritura.";
            Validacion = false;
        }
        if (Txt_Baja_No_Notario.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el No. Notario.";
            Validacion = false;
        }
        if (Txt_Baja_Nombre_Notario.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre Notario.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Anexo
    ///DESCRIPCIÓN: Valida los datos para cargar un anexo.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Anexo()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Tipo_Archivo.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Seleccionar el Tipo de Archivo.";
            Validacion = false;
        }
        if (Session["Anexo_Cargado"] == null)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Seleccionar el Archivo.";
            Validacion = false;
        }
        if (Txt_Descripcion_Archivo.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir la Descripción del Archivo.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Afectaciones
    ///DESCRIPCIÓN: Valida los datos para datos Afectaciones
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Caracteristicas()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Agregar_Dt_Anexos_Baja
    ///DESCRIPCIÓN: Lista los Anexos que se daran de Baja
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Agregar_Dt_Anexos_Baja(Int32 No_Registro)
    {
        DataTable Dt_Anexos_Baja = new DataTable();
        if (Session["Dt_Anexos_Baja"] == null)
        {
            Dt_Anexos_Baja.Columns.Add("No_Registro", Type.GetType("System.Int32"));
        }
        else
        {
            Dt_Anexos_Baja = (DataTable)Session["Dt_Anexos_Baja"];
        }
        DataRow Fila = Dt_Anexos_Baja.NewRow();
        Fila["No_Registro"] = No_Registro;
        Dt_Anexos_Baja.Rows.Add(Fila);
        Session["Dt_Anexos_Baja"] = Dt_Anexos_Baja;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Valores_Decimales
    ///DESCRIPCIÓN: Valida los valores decimales para un Anexo.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Validar_Valores_Decimales(String Valor)
    {
        Boolean Validacion = true;
        Regex Expresion_Regular = new Regex(@"^[0-9]{1,50}(\.[0-9]{0,4})?$");
        Validacion = Expresion_Regular.IsMatch(Valor);
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Numerico
    ///DESCRIPCIÓN: Pasa un valor decimal a un formato.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Abril/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Formato_Numerico(ref TextBox Txt_Campo, String Tipo)
    {
        if (Txt_Campo.Text.Trim().Length > 0)
        {
            Double Valor = Convert.ToDouble(Txt_Campo.Text.Trim().Replace("$", "").Replace("%", "").Replace(",", ""));
            if (Tipo.Equals("DISTANCIA"))
            {
                Txt_Campo.Text = String.Format("{0:#,###,##0.####}", Valor);
            }
            else if (Tipo.Equals("PORCENTUAL"))
            {
                Txt_Campo.Text = String.Format("{0:##0.####}", Valor);
            }
            else if (Tipo.Equals("NUMERICO"))
            {
                Txt_Campo.Text = Valor.ToString();
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Formato_Numerico
    ///DESCRIPCIÓN: Pasa un valor decimal a un formato.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Abril/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Caracteres_Permitidos(ref AjaxControlToolkit.FilteredTextBoxExtender FTE_Txt_Campo, String Operacion, String Caracter)
    {
        if (Operacion.Equals("AGREGAR"))
        {
            FTE_Txt_Campo.ValidChars = FTE_Txt_Campo.ValidChars + Caracter;
        }
        else if (Operacion.Equals("QUITAR"))
        {
            FTE_Txt_Campo.ValidChars = FTE_Txt_Campo.ValidChars.Replace(Caracter, "");
        }
    }

    /// *************************************************************************************************************************
    /// Nombre: Pasar_DataTable_A_Excel
    /// 
    /// Descripción: Pasa DataTable a Excel.
    /// 
    /// Parámetros: Dt_Reporte.- DataTable que se pasara a excel.
    /// 
    /// Usuario Creo: Juan Alberto Hernández Negrete.
    /// Fecha Creó: 18/Octubre/2011.
    /// Usuario Modifico:
    /// Fecha Modifico:
    /// Causa Modificación:
    /// *************************************************************************************************************************
    public void Pasar_DataTable_A_Excel(System.Data.DataTable Dt_Reporte)
    {
        String Ruta = "Caracteristicas de Pozos [" + (String.Format("{0:dd_MMM_yyyy_hh_mm_ss}", DateTime.Now)) + "].xls";//Variable que almacenara el nombre del archivo. 

        try
        {
            //Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

            Libro.Properties.Title = "Reporte de Cuenta Publica de Patrimonio";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "Patrimonio";

            //Creamos una hoja que tendrá el libro.
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
            //Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
            //Creamos el estilo contenido para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

            Estilo_Cabecera.Font.FontName = "Tahoma";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Cabecera.Font.Color = "#FFFFFF";
            Estilo_Cabecera.Interior.Color = "#193d61";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Cabecera.Alignment.WrapText = true;

            Estilo_Contenido.Font.FontName = "Tahoma";
            Estilo_Contenido.Font.Size = 8;
            Estilo_Contenido.Font.Bold = true;
            Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido.Font.Color = "#000000";
            Estilo_Contenido.Interior.Color = "White";
            Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
            Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
            Estilo_Contenido.Alignment.WrapText = true;

            //Adecuamos las Columnas del Reporte
            Dt_Reporte.Columns.RemoveAt(0);
            for (Int32 Cont = 0; Cont < Dt_Reporte.Columns.Count; Cont++)
                Dt_Reporte.Columns[Cont].ColumnName = Dt_Reporte.Columns[Cont].ColumnName.Replace("_", " ");

            //Agregamos las columnas que tendrá la hoja de excel.
            for (Int32 Cont = 0; Cont < Dt_Reporte.Columns.Count; Cont++)
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));

            if (Dt_Reporte is System.Data.DataTable)
            {
                if (Dt_Reporte.Rows.Count > 0)
                {
                    foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                    {
                        if (COLUMNA is System.Data.DataColumn)
                        {
                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName, "HeaderStyle"));
                        }
                        Renglon.Height = 25;
                    }

                    foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                    {
                        if (FILA is System.Data.DataRow)
                        {
                            Renglon = Hoja.Table.Rows.Add();

                            foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                            {
                                if (COLUMNA is System.Data.DataColumn)
                                {
                                    if (COLUMNA.ColumnName.Trim().Equals("IMPORTE"))
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:#,###,##0.00}", Convert.ToDouble(FILA[COLUMNA.ColumnName])), DataType.String, "BodyStyle"));
                                    }
                                    else
                                    {
                                        Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString(), DataType.String, "BodyStyle"));
                                    }
                                }
                            }
                            Renglon.Height = 15;
                            Renglon.AutoFitHeight = true;
                        }
                    }
                }
            }

            //Abre el archivo de excel
            Response.Clear();
            Response.Buffer = true;
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
            Response.Charset = "UTF-8";
            Response.ContentEncoding = Encoding.Default;
            Libro.Save(Response.OutputStream);
            Response.End();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion

    #region "Grids"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Medidas_Colindancias_RowDataBound
    ///DESCRIPCIÓN: Evento de llenado de datos en el Grid
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Medidas_Colindancias_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Int32 No_Fila = e.Row.RowIndex;
            if (e.Row.FindControl("Btn_Quitar_Medida_Colindancia") != null)
            {
                ImageButton Btn_Quitar_Medida_Colindancia = (ImageButton)e.Row.FindControl("Btn_Quitar_Medida_Colindancia");
                Btn_Quitar_Medida_Colindancia.CommandArgument = No_Fila.ToString();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Anexos_RowDataBound
    ///DESCRIPCIÓN: Evento de llenado de datos en el Grid
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Anexos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.FindControl("Btn_Ver_Anexo") != null)
            {
                ImageButton Btn_Ver_Anexo = (ImageButton)e.Row.FindControl("Btn_Ver_Anexo");
                Btn_Ver_Anexo.CommandArgument = HttpUtility.HtmlDecode(Hdf_Bien_Inmueble_ID.Value + "/" + e.Row.Cells[3].Text.Trim() + "/" + e.Row.Cells[1].Text.Trim());
            }
            if (e.Row.FindControl("Btn_Eliminar_Anexo") != null)
            {
                ImageButton Btn_Eliminar_Anexo = (ImageButton)e.Row.FindControl("Btn_Eliminar_Anexo");
                Btn_Eliminar_Anexo.CommandArgument = HttpUtility.HtmlDecode(e.Row.Cells[0].Text.Trim());
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Afectaciones_RowDataBound
    ///DESCRIPCIÓN: Evento de llenado de datos en el Grid
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Afectaciones_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            Int32 No_Fila = e.Row.RowIndex;
            if (e.Row.FindControl("Btn_Ver_Detalle_Afectaciones") != null)
            {
                ImageButton Btn_Ver_Detalle_Afectaciones = (ImageButton)e.Row.FindControl("Btn_Ver_Detalle_Afectaciones");
                Btn_Ver_Detalle_Afectaciones.CommandArgument = No_Fila.ToString();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    #region "Eventos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Origen_SelectedIndexChanged
    ///DESCRIPCIÓN: ejecuta el evento de cambio de seleccion
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Origen_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cmb_Destino.SelectedIndex = 0;
        Cmb_Destino.Enabled = false;
        if (Cmb_Origen.Enabled)
        {
            if (Cmb_Origen.SelectedIndex > 0)
            {
                String Origen = Cmb_Origen.SelectedItem.Value;
                Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio Origen_Negocio = new Cls_Cat_Pat_Com_Origenes_Inmuebles_Negocio();
                Origen_Negocio.P_Origen_ID = Origen.Trim();
                Origen_Negocio = Origen_Negocio.Consultar_Detalles_Origen();
                if (!String.IsNullOrEmpty(Origen_Negocio.P_Incluir_Destino))
                {
                    if (Origen_Negocio.P_Incluir_Destino.Trim().Equals("SI"))
                    {
                        Cmb_Destino.SelectedIndex = 0;
                        Cmb_Destino.Enabled = true;
                    }
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Ejecuta el evento para generar un nuevo registro de Bienees Inmuebles
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Nuevo.AlternateText.Trim().Equals("Nuevo"))
        {
            Limpiar_Generales_Formulario();
            Habiliar_Generales_Formulario(true);
            Btn_Modificar.Visible = false;
            Btn_Nuevo.Visible = true;
            Txt_Fecha_Registro.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
            Cmb_Estado.SelectedIndex = Cmb_Estado.Items.IndexOf(Cmb_Estado.Items.FindByValue("ALTA"));
            Cmb_Estado.Enabled = false;
            Cmb_Estado_SelectedIndexChanged(Cmb_Estado, null);
            Txt_Bien_Inmueble_ID.Text = "<< AUTOMATICO >>";
        }
        else
        {
            Boolean Validacion = true;
            if (Txt_Escritura.Text.Trim().Length > 0) { Validacion = Validar_Juridico(); }
            if (Validacion)
            {
                if (Session["Anexo_Cargado"] != null) { Validacion = Validar_Anexo(); }
                if (Validacion)
                {
                    if (Validacion)
                    {
                        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = Alta();
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('ALTA EXITOSA!!!');", true);
                        Habiliar_Generales_Formulario(false);
                        if (Session["Anexo_Cargado"] != null)
                        {
                            AsyncFileUpload AFU = (AsyncFileUpload)Session["Anexo_Cargado"];
                            if (AFU.HasFile)
                            {
                                String Ruta = Server.MapPath(Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + BI_Negocio.P_Bien_Inmueble_ID + "/" + Cmb_Tipo_Archivo.SelectedItem.Value);
                                if (!Directory.Exists(Ruta))
                                {
                                    Directory.CreateDirectory(Ruta);
                                }
                                Ruta = Ruta + "/" + BI_Negocio.P_Archivo;
                                AFU.SaveAs(Ruta);
                            }
                        }
                        Hdf_Bien_Inmueble_ID.Value = BI_Negocio.P_Bien_Inmueble_ID;
                        String BI_ID = null;
                        BI_ID = Hdf_Bien_Inmueble_ID.Value;
                        Limpiar_Generales_Formulario();
                        if (BI_Negocio.P_Bien_Inmueble_ID != null || BI_Negocio.P_Bien_Inmueble_ID.Trim().Length > 0)
                        {
                            Hdf_Bien_Inmueble_ID.Value = BI_Negocio.P_Bien_Inmueble_ID;
                            Mostrar_Detalles_Bien_Inmueble();
                        }
                    }
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Ejecuta el evento para actualizar un registro de Bienees Inmuebles
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Modificar.AlternateText.Trim().Equals("Modificar"))
        {
            if (Hdf_Bien_Inmueble_ID.Value.Trim().Length > 0)
            {
                Habiliar_Generales_Formulario(true);
                Btn_Nuevo.Visible = false;
                Caracteres_Permitidos(ref FTE_Txt_Superficie, "QUITAR", ",");
                Caracteres_Permitidos(ref FTE_Txt_Valor_Comercial, "QUITAR", ",");
                Caracteres_Permitidos(ref FTE_Txt_Construccion_Resgistrada, "QUITAR", ",");
                Formato_Numerico(ref Txt_Superficie, "NUMERICO");
                Formato_Numerico(ref Txt_Valor_Comercial, "NUMERICO");
                Formato_Numerico(ref Txt_Construccion_Resgistrada, "NUMERICO");
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Verificar.";
                Lbl_Mensaje_Error.Text = "Se debe seleccionar el Bien Inmueble a Actualizar su información.";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        else
        {
            Boolean Validacion = true;
            if (Txt_Escritura.Text.Trim().Length > 0) { Validacion = Validar_Juridico(); }
            if (Validacion)
            {
                if (Session["Anexo_Cargado"] != null) { Validacion = Validar_Anexo(); }
                if (Validacion)
                {
                    if (Validacion)
                    {
                        if (Cmb_Estado.SelectedItem.Value == "BAJA") { Validacion = Validar_Juridico_Baja(); }
                        if (Validacion)
                        {
                            Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = Modificar();
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('ACTUALIZACION EXITOSA!!!');", true);
                            Habiliar_Generales_Formulario(false);
                            if (Session["Anexo_Cargado"] != null)
                            {
                                AsyncFileUpload AFU = (AsyncFileUpload)Session["Anexo_Cargado"];
                                if (AFU.HasFile)
                                {
                                    String Ruta = Server.MapPath(Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + BI_Negocio.P_Bien_Inmueble_ID + "/" + Cmb_Tipo_Archivo.SelectedItem.Value);
                                    if (!Directory.Exists(Ruta))
                                    {
                                        Directory.CreateDirectory(Ruta);
                                    }
                                    Ruta = Ruta + "/" + BI_Negocio.P_Archivo;
                                    AFU.SaveAs(Ruta);
                                }
                            }
                            Hdf_Bien_Inmueble_ID.Value = BI_Negocio.P_Bien_Inmueble_ID;
                            String BI_ID = null;
                            BI_ID = Hdf_Bien_Inmueble_ID.Value;
                            Limpiar_Generales_Formulario();
                            if (BI_Negocio.P_Bien_Inmueble_ID != null || BI_Negocio.P_Bien_Inmueble_ID.Trim().Length > 0)
                            {
                                Hdf_Bien_Inmueble_ID.Value = BI_Negocio.P_Bien_Inmueble_ID;
                                Mostrar_Detalles_Bien_Inmueble();
                            }
                        }
                    }
                }
                else
                {
                    //rchivo = null;
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Ejecuta el evento para salir del formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.AlternateText.Trim().Equals("Salir"))
        {
            Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
        }
        else
        {
            if (Btn_Nuevo.Visible)
            {
                Limpiar_Generales_Formulario();
                Habiliar_Generales_Formulario(false);
                Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
            }
            else
            {
                String BI_ID = null;
                BI_ID = Hdf_Bien_Inmueble_ID.Value;
                Limpiar_Generales_Formulario();
                Habiliar_Generales_Formulario(false);
                if (BI_ID != null)
                {
                    Hdf_Bien_Inmueble_ID.Value = BI_ID;
                    Mostrar_Detalles_Bien_Inmueble();
                }
                else
                {
                    Response.Redirect("Frm_Ope_Pat_Entrada_Bienes_Inmuebles.aspx");
                }
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Medida_Colindancia_Click
    ///DESCRIPCIÓN: Agrada la medida como detalle del Bien Inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Medida_Colindancia_Click(object sender, EventArgs e)
    {
        if (Validar_Medidas_Colindancias())
        {
            DataTable Tabla = (DataTable)Grid_Listado_Medidas_Colindancias.DataSource;
            if (Tabla == null)
            {
                if (Session["Dt_Medidas_Colindancias"] == null)
                {
                    Tabla = new DataTable("Dt_Medidas_Colindancias");
                    Tabla.Columns.Add("ORIENTACION_ID", Type.GetType("System.String"));
                    Tabla.Columns.Add("ORIENTACION", Type.GetType("System.String"));
                    Tabla.Columns.Add("MEDIDA", Type.GetType("System.Double"));
                    Tabla.Columns.Add("COLINDANCIA", Type.GetType("System.String"));
                }
                else
                {
                    Tabla = (DataTable)Session["Dt_Medidas_Colindancias"];
                }
            }
            DataRow Fila = Tabla.NewRow();
            Fila["ORIENTACION_ID"] = Cmb_Orientacion.SelectedItem.Value;
            Fila["ORIENTACION"] = Cmb_Orientacion.SelectedItem.Text;
            Fila["MEDIDA"] = Convert.ToDouble(Txt_Medida.Text);
            Fila["COLINDANCIA"] = Txt_Colindancia.Text.Trim();
            Tabla.Rows.Add(Fila);
            Llenar_Listado_Medidas_Colindancias(Tabla);
            Limpiar_Medidas_Colindancias();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Medida_Colindancia_Click
    ///DESCRIPCIÓN: Quita la medida como detalle del Bien Inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Quitar_Medida_Colindancia_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton Btn_Quitar_Medida_Colindancia = (ImageButton)sender;
            if (!String.IsNullOrEmpty(Btn_Quitar_Medida_Colindancia.CommandArgument))
            {
                Int32 No_Fila = Convert.ToInt32(Btn_Quitar_Medida_Colindancia.CommandArgument);
                DataTable Dt_Medidas_Colindancias = (Session["Dt_Medidas_Colindancias"] != null) ? ((DataTable)Session["Dt_Medidas_Colindancias"]) : new DataTable();
                if (Dt_Medidas_Colindancias.Rows.Count > 0)
                {
                    Dt_Medidas_Colindancias.Rows.RemoveAt(No_Fila);
                    Llenar_Listado_Medidas_Colindancias(Dt_Medidas_Colindancias);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Anexo_Click
    ///DESCRIPCIÓN: Muestra el Anexo del Bien Inmueble
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ver_Anexo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton Btn_Ver_Anexo = (ImageButton)sender;
            if (!String.IsNullOrEmpty(Btn_Ver_Anexo.CommandArgument))
            {
                String Nombre_Archivo = Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + Btn_Ver_Anexo.CommandArgument;
                if (File.Exists(Server.MapPath(Nombre_Archivo)))
                {
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Archivo_Archivos", "window.open('" + Nombre_Archivo + "','Window_Archivo','left=0,top=0')", true);
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "El Archivo no esta disponible o fue eliminado";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Anexo_Click
    ///DESCRIPCIÓN: Hace una eliminacion Logica del Anexo
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Eliminar_Anexo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton Btn_Eliminar_Anexo = (ImageButton)sender;
            if (!String.IsNullOrEmpty(Btn_Eliminar_Anexo.CommandArgument))
            {
                Int32 No_Registro = Convert.ToInt32(Btn_Eliminar_Anexo.CommandArgument);
                Agregar_Dt_Anexos_Baja(No_Registro);
                Btn_Eliminar_Anexo.Visible = false;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Estado_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el Evento de Cambio de Estado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Estado_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (Cmb_Estado.SelectedItem.Value == "ALTA")
        {
            Div_Campos_Juridico_Baja.Visible = false;
        }
        else
        {
            Txt_Fecha_Baja.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Today);
            Div_Campos_Juridico_Baja.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Ficha_Tecnica_PDF_Click
    ///DESCRIPCIÓN: Lanza el Reporte de Ficha Tecnica del Bien Actual
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ver_Ficha_Tecnica_PDF_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Cargar_Tablas_Reporte("PDF");
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Ficha_Tecnica_Excel_Click
    ///DESCRIPCIÓN: Lanza el Reporte de Ficha Tecnica del Bien Actual
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ver_Ficha_Tecnica_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Cargar_Tablas_Reporte("EXCEL");
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Tablas_Reporte
    ///DESCRIPCIÓN: Maneja las tablas del Reporte de Ficha Tecnica
    ///PROPIEDADES:   1.  P_Imagen.  Imagen a Convertir.    
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Cargar_Tablas_Reporte(String Tipo)
    {
        Cls_Rpt_Pat_Listado_Bienes_Negocio Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
        Reporte_Negocio.P_Bien_ID = Hdf_Bien_Inmueble_ID.Value;
        DataTable Dt_Datos_Generales_Reporte = Reporte_Negocio.Consultar_Datos_Generales_BI_Ficha_Tecnica();
        DataTable Dt_Datos_Medidas_Colindancias_Reporte = Reporte_Negocio.Consultar_Datos_Medidas_Colindancias_BI_Ficha_Tecnica();
        Dt_Datos_Generales_Reporte.Columns.Add("FOTO", Type.GetType("System.Byte[]"));
        Dt_Datos_Generales_Reporte.Columns.Add("MAPA", Type.GetType("System.Byte[]"));
        Dt_Datos_Generales_Reporte.Columns.Add("COMPLEMENTOS", Type.GetType("System.String"));
        Dt_Datos_Generales_Reporte.Columns.Add("LEVANTAMIENTO_TOPOGRAFICO", Type.GetType("System.Byte[]"));
        StringBuilder Bienes_Inmuebles_ID = new StringBuilder();
        for (Int32 Contador = 0; Contador < Dt_Datos_Generales_Reporte.Rows.Count; Contador++)
        {
            if (Contador > 0)
            {
                Bienes_Inmuebles_ID.Append(",'");
            }
            Bienes_Inmuebles_ID.Append(Dt_Datos_Generales_Reporte.Rows[Contador]["BIEN_INMUEBLE_ID"].ToString().Trim() + "'");
            //Se carga la foto
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = true;
            Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            Reporte_Negocio.P_Bien_ID = Dt_Datos_Generales_Reporte.Rows[Contador]["BIEN_INMUEBLE_ID"].ToString().Trim();
            Reporte_Negocio.P_Tipo = "FOTOGRAFIA";
            DataTable Dt_Tmp = null;
            Dt_Tmp = Reporte_Negocio.Consultar_Datos_Archivos_BI_Ficha_Tecnica();
            if (Dt_Tmp != null && Dt_Tmp.Rows.Count > 0)
            {
                Dt_Datos_Generales_Reporte.Rows[Contador].BeginEdit();
                String Nombre_Archivo = Dt_Tmp.Rows[0]["RUTA_ARCHIVO"].ToString().Trim();
                String Directorio = Server.MapPath(Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + Dt_Tmp.Rows[0]["BIEN_INMUEBLE_ID"].ToString().Trim() + "/FOTOGRAFIA");
                String Nombre_Completo_Archivo = Directorio + "/" + Nombre_Archivo;
                if (File.Exists(Nombre_Completo_Archivo))
                {
                    Dt_Datos_Generales_Reporte.Rows[Contador]["FOTO"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Nombre_Completo_Archivo));
                }
                Dt_Datos_Generales_Reporte.Rows[Contador].EndEdit();
            }
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = false;

            //Se carga el mapa
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = true;
            Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            Reporte_Negocio.P_Bien_ID = Dt_Datos_Generales_Reporte.Rows[Contador]["BIEN_INMUEBLE_ID"].ToString().Trim();
            Reporte_Negocio.P_Tipo = "MAPA";
            Dt_Tmp = null;
            Dt_Tmp = Reporte_Negocio.Consultar_Datos_Archivos_BI_Ficha_Tecnica();
            if (Dt_Tmp != null && Dt_Tmp.Rows.Count > 0)
            {
                Dt_Datos_Generales_Reporte.Rows[Contador].BeginEdit();
                String Nombre_Archivo = Dt_Tmp.Rows[0]["RUTA_ARCHIVO"].ToString().Trim();
                String Directorio = Server.MapPath(Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + Dt_Tmp.Rows[0]["BIEN_INMUEBLE_ID"].ToString().Trim() + "/MAPA");
                String Nombre_Completo_Archivo = Directorio + "/" + Nombre_Archivo;
                if (File.Exists(Nombre_Completo_Archivo))
                {
                    Dt_Datos_Generales_Reporte.Rows[Contador]["MAPA"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Nombre_Completo_Archivo));
                }
                Dt_Datos_Generales_Reporte.Rows[Contador].EndEdit();
            }
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = false;

            //Se carga el Levantamiento Topografico
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = true;
            Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            Reporte_Negocio.P_Bien_ID = Dt_Datos_Generales_Reporte.Rows[Contador]["BIEN_INMUEBLE_ID"].ToString().Trim();
            Reporte_Negocio.P_Tipo = "LEVANTAMIENTO_TOPOGRAFICO";
            Dt_Tmp = null;
            Dt_Tmp = Reporte_Negocio.Consultar_Datos_Archivos_BI_Ficha_Tecnica();
            if (Dt_Tmp != null && Dt_Tmp.Rows.Count > 0)
            {
                Dt_Datos_Generales_Reporte.Rows[Contador].BeginEdit();
                String Nombre_Archivo = Dt_Tmp.Rows[0]["RUTA_ARCHIVO"].ToString().Trim();
                String Directorio = Server.MapPath(Ope_Pat_B_Inm_Archivos.Ruta_Archivos_Inmuebles + Dt_Tmp.Rows[0]["BIEN_INMUEBLE_ID"].ToString().Trim() + "/LEVANTAMIENTO_TOPOGRAFICO");
                String Nombre_Completo_Archivo = Directorio + "/" + Nombre_Archivo;
                if (File.Exists(Nombre_Completo_Archivo))
                {
                    Dt_Datos_Generales_Reporte.Rows[Contador]["LEVANTAMIENTO_TOPOGRAFICO"] = Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image.FromFile(Nombre_Completo_Archivo));
                }
                Dt_Datos_Generales_Reporte.Rows[Contador].EndEdit();
            }
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = false;

            //Se carga el mapa
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = true;
            Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            Reporte_Negocio.P_Bien_ID = Dt_Datos_Generales_Reporte.Rows[Contador]["BIEN_INMUEBLE_ID"].ToString().Trim();
            Dt_Tmp = null;
            Dt_Tmp = Reporte_Negocio.Consultar_Datos_Observaciones_BI_Ficha_Tecnica();
            if (Dt_Tmp != null && Dt_Tmp.Rows.Count > 0)
            {
                Dt_Datos_Generales_Reporte.Rows[Contador].BeginEdit();
                Dt_Datos_Generales_Reporte.Rows[Contador]["COMPLEMENTOS"] = Dt_Tmp.Rows[0]["OBSERVACION"].ToString();
                Dt_Datos_Generales_Reporte.Rows[Contador].EndEdit();
            }
            Dt_Datos_Generales_Reporte.DefaultView.AllowEdit = false;
        }

        //Se Consultan las Medidas y Colindancias
        Reporte_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
        Reporte_Negocio.P_Bien_ID = Bienes_Inmuebles_ID.ToString().Trim('\'');
        DataTable Dt_Tmp_ = null;
        Dt_Tmp_ = Reporte_Negocio.Consultar_Datos_Medidas_Colindancias_BI_Ficha_Tecnica();
        Dt_Datos_Generales_Reporte.TableName = "DT_GENERALES";
        Dt_Tmp_.TableName = "DT_MEDIAS_COLINDANCIAS";
        DataSet Ds_Consulta = new DataSet();
        Ds_Consulta.Tables.Add(Dt_Datos_Generales_Reporte.Copy());
        Ds_Consulta.Tables.Add(Dt_Tmp_.Copy());
        Ds_Rpt_Pat_Ficha_Tecnica_Bienes_Inmuebles Ds_Reporte = new Ds_Rpt_Pat_Ficha_Tecnica_Bienes_Inmuebles();
        if (Tipo.Equals("PDF")) { Generar_Reporte(Ds_Consulta, Ds_Reporte, "Rpt_Pat_Ficha_Tecnica_Bienes_Inmuebles.rpt"); }
        else if (Tipo.Equals("EXCEL")) { Generar_Reporte_Excel(Ds_Consulta, Ds_Reporte, "Rpt_Pat_Ficha_Tecnica_Bienes_Inmuebles.rpt"); }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Ruta = "../../Reporte/Ficha_Tecnica_" + Hdf_Bien_Inmueble_ID.Value + "_" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Excel
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Excel(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Ruta = "../../Reporte/Ficha_Tecnica_" + Hdf_Bien_Inmueble_ID.Value + "_" + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.Excel;
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_Imagen_A_Cadena_Bytes
    ///DESCRIPCIÓN: Convierte la Imagen a una Cadena de Bytes.
    ///PROPIEDADES:   1.  P_Imagen.  Imagen a Convertir.    
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: Marzo/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Byte[] Convertir_Imagen_A_Cadena_Bytes(System.Drawing.Image P_Imagen)
    {
        Byte[] Img_Bytes = null;
        try
        {
            if (P_Imagen != null)
            {
                MemoryStream MS_Tmp = new MemoryStream();
                P_Imagen.Save(MS_Tmp, P_Imagen.RawFormat);
                Img_Bytes = MS_Tmp.GetBuffer();
                MS_Tmp.Close();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "Verificar.";
            Div_Contenedor_Msj_Error.Visible = false;
        }
        return Img_Bytes;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Asy_Cargar_Archivo_Complete
    ///DESCRIPCIÓN: Maneja el evento de cuando se cargo completamente el archivo
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 01/Noviembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void AFU_Ruta_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            AsyncFileUpload AFU = (AsyncFileUpload) sender;
            if (AFU != null)
            {
                Session["Anexo_Cargado"] = AFU;

            }
            
        }
        catch (Exception ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al cargar la imagen " + ex.Message;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Caractetisticas_Click
    ///DESCRIPCIÓN: Exporta las Caracteristicas a un Archivo de Excel
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 04/Julio/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Exportar_Caractetisticas_Click(object sender, ImageClickEventArgs e)
    {
        try {
            Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
            BI_Negocio.P_Bien_Inmueble_ID = Hdf_Bien_Inmueble_ID.Value;
            BI_Negocio = BI_Negocio.Consultar_Detalles_Bien_Inmueble();
            DataTable Dt_Datos = BI_Negocio.Obtener_Dt_Caracteristicas().Copy();
            if (Dt_Datos.Rows.Count > 0)
                Pasar_DataTable_A_Excel(Dt_Datos);
            else
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No hay Caracteristicas capturadas que Exportar');", true);
        } catch (Exception Ex) {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

}