﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Pat_Com_Donadores.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Pat_Com_Donadores" Title="Donadores" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

   <!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->  
   <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
    
            //Funcion que ejecuta el evento del boton de buscar mediante la tecla enter en la caja de texto que implementa la funcion
        function get_KeyPress(textbox, evento) {
            //debugger;
            var keyCode;
            if (evento.which || evento.charCode) {
                keyCode = evento.which ? evento.which : evento.charCode;
                //return (keyCode != 13);
            }
            else if (window.event) {
                keyCode = event.keyCode;
                if (keyCode == 13) {
                    if (event.keyCode)
                        event.keyCode = 9;
                }
            }

            if (keyCode == 13) {

                (document.getElementById('<%=Btn_Buscar.ClientID %>')).click();
                //                alert("se presiono Enter");
                window.focus();
                return false;
            }
            return true;
        }
    
   </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="36000" EnableScriptLocalization="true" EnableScriptGlobalization="true"/> 
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_General" style="background-color:#ffffff; width:98%; height:100%;"> <%--Div General--%>
                <table  border="0" cellspacing="0" class="estilo_fuente" width="98%"> <%-- Tabla General--%>
                    <tr align="center">
                        <td class="label_titulo" colspan="2">Catálogo de Donadores</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td align="left" colspan="4">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;" >              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top" >
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                             </table>                   
                          </div>                          
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="2">&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width:50%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo"
                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Nuevo" OnClick="Btn_Nuevo_Click"/>
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar"
                                ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Modificar" onclick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar"
                                ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Eliminar" OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro de la Base de Datos?');"
                                onclick="Btn_Eliminar_Click"/>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir"
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Salir" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%;">Busqueda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180px" onkeypress="return get_KeyPress(this,event);" ></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false"
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar" 
                                onclick="Btn_Buscar_Click" AlternateText="Consultar"/>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" 
                                WatermarkText="<- Nombre ->" TargetControlID="Txt_Busqueda" 
                                WatermarkCssClass="watermarked"></cc1:TextBoxWatermarkExtender>    
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" 
                                TargetControlID="Txt_Busqueda" InvalidChars="<,>,&,',!," 
                                FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                            </cc1:FilteredTextBoxExtender>                                  
                        </td>                        
                    </tr>
                </table>  
                <br />
                <center>
                    <table width="99%" class="estilo_fuente">
                        <tr>
                            <td colspan="4">
                                <asp:HiddenField ID="Hdf_Donador_ID" runat="server" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Nombre_Donador_MPE" runat="server" Text="* Nombre" CssClass="estilo_fuente"
                                    Style="font-weight: bolder;"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 82%; text-align: left;">
                                <asp:TextBox ID="Txt_Nombre_Donador_MPE" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Donador_MPE" runat="server" TargetControlID="Txt_Nombre_Donador_MPE"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Apellido_Paterno_Donador" runat="server" Text="A. Paterno" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Apellido_Paterno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Paterno_Donador" runat="server"
                                    TargetControlID="Txt_Apellido_Paterno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Apellido_Materno_Donador" runat="server" Text="A. Materno" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Apellido_Materno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Materno_Donador" runat="server"
                                    TargetControlID="Txt_Apellido_Materno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Direccion_Donador" runat="server" Text="Dirección" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td colspan="3" style="width: 82%; text-align: left;">
                                <asp:TextBox ID="Txt_Direccion_Donador" runat="server" Width="99%" MaxLength="150"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Direccion_Donador" runat="server" TargetControlID="Txt_Direccion_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Ciudad_Donador" runat="server" Text="Ciudad" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Ciudad_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ciudad_Donador" runat="server" TargetControlID="Txt_Ciudad_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Estado_Donador" runat="server" Text="Estado" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Estado_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Estado_Donador" runat="server" TargetControlID="Txt_Estado_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Telefono_Donador" runat="server" Text="Telefono" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Telefono_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Telefono_Donador" runat="server" TargetControlID="Txt_Telefono_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Celular_Donador" runat="server" Text="Celular" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_Celular_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Celular_Donador" runat="server" TargetControlID="Txt_Celular_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_CURP_Donador" runat="server" Text="CURP" CssClass="estilo_fuente"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_CURP_Donador" runat="server" Width="98%" MaxLength="18"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_CURP_Donador" runat="server" TargetControlID="Txt_CURP_Donador"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="Ññ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_RFC_Donador_MPE" runat="server" Text="* RFC" CssClass="estilo_fuente"
                                    Style="font-weight: bolder;"></asp:Label>
                            </td>
                            <td style="width: 32%; text-align: left;">
                                <asp:TextBox ID="Txt_RFC_Donador_MPE" runat="server" Width="98%" MaxLength="15"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_RFC_Donador_MPE" runat="server" TargetControlID="Txt_RFC_Donador_MPE"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="Ññ">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                    </table>
                    <br />                                
                    <asp:GridView ID="Grid_Donadores" runat="server" CssClass="GridView_1"
                        AutoGenerateColumns="False" AllowPaging="True" PageSize="10" Width="99%"
                        GridLines= "None"
                        onselectedindexchanged="Grid_Donadores_SelectedIndexChanged" 
                        onpageindexchanging="Grid_Donadores_PageIndexChanging">
                        <RowStyle CssClass="GridItem" />
                        <Columns>
                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                <ItemStyle Width="30px"/>
                            </asp:ButtonField>
                            <asp:BoundField DataField="DONADOR_ID" HeaderText="DONADOR_ID" SortExpression="DONADOR_ID" >
                                <ItemStyle Width="90px" HorizontalAlign="Center" Font-Size="X-Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                                <HeaderStyle Width="110px" HorizontalAlign="Center"/>
                                <ItemStyle Width="110px" HorizontalAlign="Center" Font-Size="X-Small" />
                            </asp:BoundField>
                            <asp:BoundField DataField="NOMBRE_COMPLETO" HeaderText="Nombre" SortExpression="NOMBRE_COMPLETO">
                                <ItemStyle Font-Size="X-Small" />
                            </asp:BoundField>
                        </Columns>
                        <PagerStyle CssClass="GridHeader" />
                        <SelectedRowStyle CssClass="GridSelected" />
                        <HeaderStyle CssClass="GridHeader" />                                
                        <AlternatingRowStyle CssClass="GridAltItem" />       
                    </asp:GridView>
                </center>  
            </div>
      </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

