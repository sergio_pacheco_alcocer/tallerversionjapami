﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Common;
using System.Windows.Forms;
using System.Data.OleDb;
using System.Data;
using JAPAMI.Control_Patrimonial_Catalogo_Zonas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Combustible.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Vehiculo.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Constantes;
using SharpContent.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Data.OleDb;
using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Data.Common;
using System.Windows.Forms;
using System.Data.OleDb;
using SharpContent.ApplicationBlocks.Data;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using System.Data.SqlClient;
using JAPAMI.Control_Patrimonial_Catalogo_Zonas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Vehiculo.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Siniestros.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Cemovientes.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Bajas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Alimentacion.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Materiales.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Aseguradoras.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Ascendencia.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Razas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Vacunas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Veterinarios.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Funciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Adiestramiento.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Combustible.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Catalogo_Compras_Marcas.Negocio;
using JAPAMI.Catalogo_Compras_Modelos.Negocio;
using JAPAMI.Catalogo_Compras_Productos.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Cemovientes.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Detalles_Vehiculos.Negocio;


public partial class paginas_Control_Patrimonial_Frm_Pat_Migrar_Informacion : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Leer_Excel
    ///DESCRIPCIÓN:          Metodo que consulta un archivo EXCEL
    ///PARAMETROS:           String sqlExcel.- string que contiene el select
    ///CREO:                 Susana Trigueros Armenta
    ///FECHA_CREO:           23/Mayo/2011 
    ///MODIFICO:             Salvador Hernández Ramírez
    ///FECHA_MODIFICO:       26/Mayo/2011 
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public DataSet Leer_Excel(String sqlExcel) {
        //Para empezar definimos la conexión OleDb a nuestro fichero Excel.
        String Rta = @MapPath("../../Archivos/DATOS PARA LA MIGRACION DE CATALOGOS DE CONTROL PATRIMONIAL_2.xlsx");
        string sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                           "Data Source=" + Rta + ";" +
                           "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";

        //Definimos el DataSet donde insertaremos los datos que leemos del excel
        DataSet DS = new DataSet();

        //Definimos la conexión OleDb al fichero Excel y la abrimos
        OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
        oledbConn.Open();

        //Creamos un comand para ejecutar la sentencia SELECT.
        OleDbCommand oledbCmd = new OleDbCommand(sqlExcel, oledbConn);

        //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
        OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
        da.Fill(DS);
        return DS;
    }

    protected void Btn_Migrar_Zonas_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [Zonas$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Zonas_Negocio Zona = new Cls_Cat_Pat_Com_Zonas_Negocio();
            Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
            Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
            Zona.P_Usuario = "CONTEL";
            Zona.Alta_Zona();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }

    protected void Btn_Migrar_Colores_Click(object sender, EventArgs e)
    {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [COLORES$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++)
        {
            Cls_Cat_Pat_Com_Colores_Negocio Zona = new Cls_Cat_Pat_Com_Colores_Negocio();
            Zona.P_Descripcion = Zonas.Rows[cnt]["NOMBRE"].ToString();
            Zona.P_Estatus = "VIGENTE";
            Zona.P_Usuario = "CONTEL";
            Zona.Alta_Color();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }

    //protected void Btn_Migrar_Colores_Click(object sender, EventArgs e) {
    //    DataSet Ds_Zonas = new DataSet();
    //    String SqlExcel = "Select * From [Colores$]";
    //    Ds_Zonas = Leer_Excel(SqlExcel);
    //    DataTable Zonas = Ds_Zonas.Tables[0];
    //    for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
    //        Cls_Cat_Pat_Com_Colores_Negocio Zona = new Cls_Cat_Pat_Com_Colores_Negocio();
    //        Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
    //        Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
    //        Zona.P_Usuario = "CONTEL";
    //        Zona.Alta_Color();
    //    }
    //    ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    //}
    protected void Btn_Migrar_Procedencias_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [Procedencias$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Procedencias_Negocio Zona = new Cls_Cat_Pat_Com_Procedencias_Negocio();
            if (!String.IsNullOrEmpty(Zonas.Rows[cnt]["NOMBRE"].ToString())) {
                Zona.P_Nombre = Zonas.Rows[cnt]["NOMBRE"].ToString();
                Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
                Zona.P_Usuario = "CONTEL";
                Zona.Alta_Procedencia();
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }
    protected void Btn_Migrar_Combustibles_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [TiposCombustibles$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Tipos_Combustible_Negocio Zona = new Cls_Cat_Pat_Com_Tipos_Combustible_Negocio();
            Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
            Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
            Zona.P_Usuario = "CONTEL";
            Zona.Alta_Tipo_Combustible();
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }
    protected void Btn_Migrar_Tipos_Vehiculos_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [TipoVehiculo$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio Zona = new Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio();
            if (!String.IsNullOrEmpty(Zonas.Rows[cnt]["DESCRIPCION"].ToString())) {
                Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
                Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
                Zona.P_Usuario = "CONTEL";
                Zona.Alta_Tipo_Vehiculo();
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }
    protected void Btn_Migrar_Tipos_Activos_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [TipoActivo$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Clasificaciones_Negocio Zona = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            if (!String.IsNullOrEmpty(Zonas.Rows[cnt]["DESCRIPCION"].ToString())) {
                Zona.P_Clave = Zonas.Rows[cnt]["CLAVE"].ToString();
                Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
                Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
                Zona.P_Usuario = "CONTEL";
                Zona.Alta_Clasificacion();
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }
    protected void Btn_Migrar_Clases_Activo_Click(object sender, EventArgs e) {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [ClaseActivo$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++) {
            Cls_Cat_Pat_Com_Clases_Activo_Negocio Zona = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            if (!String.IsNullOrEmpty(Zonas.Rows[cnt]["DESCRIPCION"].ToString())) {
                Zona.P_Clave = Zonas.Rows[cnt]["CLAVE"].ToString();
                Zona.P_Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
                Zona.P_Estatus = Zonas.Rows[cnt]["ESTATUS"].ToString();
                Zona.P_Usuario = "CONTEL";
                Zona.Alta_Clase_Activo();
            }
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }
    protected void Btn_Actualizar_Clases_Activo_Click(object sender, EventArgs e)
    {
        DataSet Ds_Zonas = new DataSet();
        String SqlExcel = "Select * From [ClaseActivo$]";
        Ds_Zonas = Leer_Excel(SqlExcel);
        DataTable Zonas = Ds_Zonas.Tables[0];
        for (int cnt = 0; cnt < Zonas.Rows.Count; cnt++)
        {
            String Clave = Zonas.Rows[cnt]["CLAVE"].ToString();
            String Descripcion = Zonas.Rows[cnt]["DESCRIPCION"].ToString();
            String CA_ID = Zonas.Rows[cnt]["CLASE_ACTIVO_ID"].ToString();
            String Mi_SQl = "UPDATE " + Cat_Pat_Clases_Activo.Tabla_Cat_Pat_Clases_Activo
                            + " SET " + Cat_Pat_Clases_Activo.Campo_Clave + " = '" + Clave + "'"
                            + " , " + Cat_Pat_Clases_Activo.Campo_Descripcion + " = '" + Descripcion + "'"
                            + " WHERE " + Cat_Pat_Clases_Activo.Campo_Clase_Activo_ID + " = '" + CA_ID + "'";
            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQl);
            
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }



    ///02 MAYO 2013
    protected void Btn_Subir_Informacion_Vehiculos(object sender, EventArgs e) {
        DataSet Ds_Vehiculos = new DataSet();
        String SqlExcel = "Select * From [LISTO_VEH$]";
        Ds_Vehiculos = Leer_Excel(SqlExcel);
        DataTable Vehiculos = Ds_Vehiculos.Tables[0];
        for (int cnt = 0; cnt < Vehiculos.Rows.Count; cnt++)
        {
            StringBuilder Mi_Sql = new StringBuilder();
            String Vehiculo_ID = ((!String.IsNullOrEmpty(Vehiculos.Rows[cnt]["VEHICULO_ID"].ToString())) ? String.Format("{0:0000000000", Convert.ToInt64(Vehiculos.Rows[cnt]["VEHICULO_ID"].ToString())) : Obtener_ID_Consecutivo(Ope_Pat_Vehiculos.Tabla_Ope_Pat_Vehiculos, Ope_Pat_Vehiculos.Campo_Vehiculo_ID, 10));
            String Operacion_Migrar = ((!String.IsNullOrEmpty(Vehiculos.Rows[cnt]["VEHICULO_ID"].ToString())) ? "ACTUALIZAR" : "NUEVO");
            String Producto_ID = Obtener_ID_Producto(Vehiculos.Rows[cnt]["PRODUCTO"].ToString().Trim());
            String Nombre = Vehiculos.Rows[cnt]["PRODUCTO"].ToString().Trim();
            String Tipo_Vehiculo = Obtener_ID_Tipo_Vehiculo(Vehiculos.Rows[cnt]["TIPO_VEHICULO"].ToString().Trim());
        }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('Exito!');", true);
    }




    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_ID_Consecutivo
    ///DESCRIPCIÓN: Obtiene el ID Cosnecutivo disponible para dar de alta un Registro en la Tabla
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Marzo/2010 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    public static String Obtener_ID_Consecutivo(String Tabla, String Campo, Int32 Longitud_ID)
    {
        String Id = Convertir_A_Formato_ID(1, Longitud_ID); ;
        try
        {
            String Mi_SQL = "SELECT MAX(" + Campo + ") FROM " + Tabla;
            Object Obj_Temp = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            if (!(Obj_Temp is Nullable) && !Obj_Temp.ToString().Equals(""))
            {
                Id = Convertir_A_Formato_ID((Convert.ToInt32(Obj_Temp) + 1), Longitud_ID);
            }
        }
        catch (SqlException Ex)
        {
            new Exception(Ex.Message);
        }
        return Id;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
    ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
    ///PARAMETROS:     
    ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
    ///             2. Longitud_ID. Longitud que tendra el ID. 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Marzo/2010 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
    {
        String Retornar = "";
        String Dato = "" + Dato_ID;
        for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
        {
            Retornar = Retornar + "0";
        }
        Retornar = Retornar + Dato;
        return Retornar;
    }

    private String Obtener_ID_Tipo_Vehiculo(String Tipo_Vehiculo)
    {
        String Tipo_Vehiculo_ID = null;
        String Mi_SQL = null;
        Mi_SQL = "SELECT * FROM " + Cat_Pat_Tipos_Vehiculo.Tabla_Cat_Pat_Tipos_Vehiculo + " WHERE " + Cat_Pat_Tipos_Vehiculo.Campo_Descripcion + "='" + Tipo_Vehiculo.Trim() + "'";
        DataSet Ds_Aux_Tipo_Vehiculo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        if (Ds_Aux_Tipo_Vehiculo != null && Ds_Aux_Tipo_Vehiculo.Tables.Count > 0)
        {
            DataTable Dt_Aux_Tipo_Vehiculo = Ds_Aux_Tipo_Vehiculo.Tables[0];
            if (Dt_Aux_Tipo_Vehiculo != null && Dt_Aux_Tipo_Vehiculo.Rows.Count > 0)
            {
                Tipo_Vehiculo_ID = Dt_Aux_Tipo_Vehiculo.Rows[0][Cat_Pat_Tipos_Vehiculo.Campo_Tipo_Vehiculo_ID].ToString().Trim();
            }
        }
        if (Tipo_Vehiculo_ID == null)
        {
            Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio Tipo_Negocio = new Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio();
            Tipo_Negocio.P_Descripcion = Tipo_Vehiculo.Trim();
            Tipo_Negocio.P_Estatus = "VIGENTE";
            Tipo_Negocio.P_Usuario = "CARGA INICIAL";
            Tipo_Negocio.P_Dt_Archivos = new DataTable();
            Tipo_Negocio.P_Dt_Detalles = new DataTable();
            Tipo_Negocio.Alta_Tipo_Vehiculo();
            Tipo_Vehiculo_ID = Obtener_ID_Tipo_Vehiculo(Tipo_Vehiculo);
        }
        return Tipo_Vehiculo_ID;
    }

    private String Obtener_ID_Producto(String Producto)
    {
        String Producto_ID = null;
        String Mi_SQL = null;
        Mi_SQL = "SELECT * FROM " + Cat_Com_Productos.Tabla_Cat_Com_Productos + " WHERE " + Cat_Com_Productos.Campo_Nombre + " = '" + Producto.Trim() + "'";
        DataSet Ds_Aux_Producto = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
        if (Ds_Aux_Producto != null && Ds_Aux_Producto.Tables.Count > 0)
        {
            DataTable Dt_Aux_Producto = Ds_Aux_Producto.Tables[0];
            if (Dt_Aux_Producto != null && Dt_Aux_Producto.Rows.Count > 0)
            {
                Producto_ID = Dt_Aux_Producto.Rows[0][Cat_Com_Productos.Campo_Producto_ID].ToString();
            }
        }
        if (Producto_ID == null)
        {
            Cls_Cat_Com_Productos_Negocio Producto_Negocio = new Cls_Cat_Com_Productos_Negocio();
            Producto_Negocio.P_Nombre = Producto.Trim();
            Producto_Negocio.P_Estatus = "INICIAL";
            Producto_Negocio.P_Usuario_Creo = "CARGA INICIAL";
            Producto_Negocio.P_Tipo = "VEHICULO";
            Producto_Negocio.P_Unidad_ID = "00001";
            Producto_Negocio.P_Partida_Especifica_ID = "0000000001";
            Producto_Negocio.Alta_Producto();
            Producto_ID = Obtener_ID_Producto(Producto);
        }
        return Producto_ID;
    }
}
