﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Control_Patrimonial_Operacion_Depreciacion_Bienes.Negocio;
using System.Data;
using System.Text;
using System.Reflection;
using CarlosAg.ExcelXmlWriter;

public partial class paginas_Compras_Frm_Ope_Pat_Depreciacion_Bienes : System.Web.UI.Page {

    #region "Page Load"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Evento que se carga cuando la Pagina de Inicia.
        ///PARAMETROS:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e) {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            Lbl_Ecabezado_Mensaje.Text = "";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
            if (!IsPostBack) {
                Btn_Exportar_Bienes_No_Migrados.Visible = false;
                Llenar_Combos_Formulario();
            }
        }

    #endregion

    #region "Metodos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combos_Formulario
        ///DESCRIPCIÓN: Llena los Combos del Formulario
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private void Llenar_Combos_Formulario() {
            Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            CA_Negocio.P_Estatus = "VIGENTE";
            CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
            Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
            Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
            Cmb_Clase_Activo.DataTextField = "CLAVE_DESCRIPCION";
            Cmb_Clase_Activo.DataBind();
            Cmb_Clase_Activo.Items.Insert(0, new ListItem("< TODAS >", ""));

            Cls_Cat_Pat_Com_Clasificaciones_Negocio Clasificaciones_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            Clasificaciones_Negocio.P_Estatus = "VIGENTE";
            Clasificaciones_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
            Cmb_Tipo_Activo.DataSource = Clasificaciones_Negocio.Consultar_DataTable();
            Cmb_Tipo_Activo.DataValueField = "CLASIFICACION_ID";
            Cmb_Tipo_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Tipo_Activo.DataBind();
            Cmb_Tipo_Activo.Items.Insert(0, new ListItem("< TODOS >", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Realizar_Deprecicacion_Bienes
        ///DESCRIPCIÓN: Realiza la Depreciacion de los Bienes.
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private DataTable Realizar_Deprecicacion_Bienes(ref DataTable Dt_Bienes, ref DataTable Dt_No_Depreciados) {
            DataTable Depreciados = Dt_Bienes.Clone();  
            foreach (DataRow Fila in Dt_Bienes.Rows) {
                if (Validar_Ejecucion_Depreciacion(Fila)) {
                    Double Anios_Vida_Util = Convert.ToDouble(Fila["ANIOS_VIDA_UTIL"]);
                    Double Anios_Uso = Obtener_Diferencia_Fechas_Decimal(Convert.ToDateTime(Fila["FECHA_ADQUISICION"]), DateTime.Now);
                    Double Valor_Inicial = Convert.ToDouble(Fila["VALOR_INCIAL"]);
                    Double Valor_Anterior = Convert.ToDouble(Fila["VALOR_ANTERIOR"]);
                    Double Porcentaje_Depreciacion = Convert.ToDouble(Fila["PORCENTAJE_DEPRECIACION"]);
                    Double Valor_Depreciado = 1;
                    if (Anios_Uso < Anios_Vida_Util) {
                        Valor_Depreciado = Valor_Inicial - (Valor_Inicial * ((Porcentaje_Depreciacion / 100) * Anios_Uso));
                    }
                    Fila.SetField("VALOR_ACTUAL", Valor_Depreciado);
                    Fila.SetField("VALOR_DEPRECIADO", (Valor_Anterior - Valor_Depreciado));
                    Depreciados.ImportRow(Fila);
                } else {
                    DataRow Fila_No_Depreciada = Dt_No_Depreciados.NewRow();
                    Fila_No_Depreciada["Tipo"] = Fila["TIPO_BIEN"].ToString().Trim();
                    Fila_No_Depreciada["Inventario"] = Fila["INVENTARIO_SIAS"].ToString().Trim();
                    Fila_No_Depreciada["Importe"] = ((!String.IsNullOrEmpty(Fila["VALOR_INCIAL"].ToString())) ? "SI" : "NO");
                    Fila_No_Depreciada["Adquisicion"] = ((!String.IsNullOrEmpty(Fila["FECHA_ADQUISICION"].ToString())) ? "SI" : "NO");
                    Fila_No_Depreciada["Vida_Util"] = ((!String.IsNullOrEmpty(Fila["ANIOS_VIDA_UTIL"].ToString())) ? "SI" : "NO");
                    Fila_No_Depreciada["Depreciacion"] = ((!String.IsNullOrEmpty(Fila["PORCENTAJE_DEPRECIACION"].ToString())) ? "SI" : "NO");
                    Fila_No_Depreciada["Cuenta_Contable_Gasto"] = ((!String.IsNullOrEmpty(Fila["CUENTA_BIEN_ID"].ToString())) ? "SI" : "NO");
                    Fila_No_Depreciada["Cuenta_Contable_Depreciacion"] = ((!String.IsNullOrEmpty(Fila["CUENTA_CLASE_ID"].ToString())) ? "SI" : "NO");
                    Dt_No_Depreciados.Rows.Add(Fila_No_Depreciada);
                }
            }
            return Depreciados;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Realizar_Deprecicacion_Bienes
        ///DESCRIPCIÓN: Realiza la Depreciacion de los Bienes.
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        private DataTable Crear_DataTable_No_Depreciados() {
            DataTable Dt_Cabecera = new DataTable();
            Dt_Cabecera.Columns.Add("Tipo", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Inventario", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Importe", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Adquisicion", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Vida_Util", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Depreciacion", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Cuenta_Contable_Gasto", Type.GetType("System.String"));
            Dt_Cabecera.Columns.Add("Cuenta_Contable_Depreciacion", Type.GetType("System.String"));
            return Dt_Cabecera;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Diferencia_Fechas_Texto
        ///DESCRIPCIÓN: Obtiene en Texto la Diferencia de Fechas
        ///PARAMETROS:
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Abril/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private String Obtener_Diferencia_Fechas_Texto(DateTime Fecha_Inicial, DateTime Fecha_Final, Boolean Completo) {
            String Resultado = "NO HAY RETRASO";
            TimeSpan Diferencia = Fecha_Final.Subtract(Fecha_Inicial);
            if (Diferencia.TotalMilliseconds > 0) {
                DateTime Temporal = new DateTime().Add(TimeSpan.FromMilliseconds(Diferencia.TotalMilliseconds));
                Int32 Anio = Temporal.Year - 1;
                Int32 Mes = Temporal.Month - 1;
                if (Anio > 0) { Resultado = Anio + ((Anio > 1) ? " AÑOS " : " AÑO"); }
                if (Completo) {
                    if (!Resultado.Trim().Equals("NO HAY RETRASO")) Resultado = Resultado + " "; else { Resultado = ""; }
                    if (Mes > 0) { Resultado = Resultado + Mes + ((Mes > 1) ? " MESES " : " MES"); }
                }
            }
            return Resultado.Trim();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Obtener_Diferencia_Fechas_Decimal
        ///DESCRIPCIÓN: Obtiene en Decimal la Diferencia de Fechas
        ///PARAMETROS:
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private Double Obtener_Diferencia_Fechas_Decimal(DateTime Fecha_Inicial, DateTime Fecha_Final) {
            Double Resultado = 0.0;
            TimeSpan Diferencia = Fecha_Final.Subtract(Fecha_Inicial);
            if (Diferencia.TotalMilliseconds > 0) {
                DateTime Temporal = new DateTime().Add(TimeSpan.FromMilliseconds(Diferencia.TotalMilliseconds));
                Double Anio = Temporal.Year - 1;
                Double Mes = Temporal.Month - 1;
                Double Mes_tmp = 0.0;
                if (Mes>0) {
                    Mes_tmp = ((Mes * 100) / 12) / 100;
                }
                if (Mes_tmp > 0) Resultado = Anio + Mes_tmp;
                else Resultado = Convert.ToDouble(Anio);
            }
            return Resultado;
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Validar_Ejecucion_Depreciacion
        ///DESCRIPCIÓN: Validar_Ejecucion_Depreciacion
        ///PARAMETROS:
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 04/Nov/2011
        ///MODIFICO:
        ///FECHA_MODIFICO:
        ///CAUSA_MODIFICACIÓN:
        ///*******************************************************************************
        private Boolean Validar_Ejecucion_Depreciacion(DataRow Fila) {
            Boolean Validacion_Aprobada = true;
            if (String.IsNullOrEmpty(Fila["VALOR_INCIAL"].ToString())) Validacion_Aprobada = false;
            if (String.IsNullOrEmpty(Fila["FECHA_ADQUISICION"].ToString())) Validacion_Aprobada = false;
            if (String.IsNullOrEmpty(Fila["ANIOS_VIDA_UTIL"].ToString())) Validacion_Aprobada = false;
            if (String.IsNullOrEmpty(Fila["PORCENTAJE_DEPRECIACION"].ToString())) Validacion_Aprobada = false;
            if (String.IsNullOrEmpty(Fila["CUENTA_BIEN_ID"].ToString())) Validacion_Aprobada = false;
            if (String.IsNullOrEmpty(Fila["CUENTA_CLASE_ID"].ToString())) Validacion_Aprobada = false;
            return Validacion_Aprobada;
        }

    #endregion

    #region "Eventos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Depreciacion_Click
        ///DESCRIPCIÓN: Ejecuta la Depreciación en los bienes.
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Noviembre/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Ejecutar_Depreciacion_Click(Object sender, EventArgs e) {
            try {
                Session.Remove("DT_BIENES_NO_MIGRADOS");
                Btn_Exportar_Bienes_No_Migrados.Visible = false;
                DataTable Dt_Datos = new DataTable();
                DataTable Dt_No_Depreciados = Crear_DataTable_No_Depreciados();
                Cls_Ope_Pat_Depreciacion_Bienes_Negocio Cls_Negocio = new Cls_Ope_Pat_Depreciacion_Bienes_Negocio();
                if (Cmb_Clase_Activo.SelectedIndex > 0) Cls_Negocio.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
                if (Cmb_Tipo_Activo.SelectedIndex > 0) Cls_Negocio.P_Tipo_Activo_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
                Cls_Negocio.P_Estatus = "VIGENTE";
                Cls_Negocio.P_Fecha_Limite = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1);
                Cls_Negocio.P_Valor_Minimo = 1;
                if (Cmb_Tipo_Bien.SelectedItem.Value.Equals("BIEN_MUEBLE") || Cmb_Tipo_Bien.SelectedItem.Value.Equals("TODOS")) {
                    Dt_Datos.Merge(Cls_Negocio.Consultar_Bienes_Muebles());
                }
                if (Cmb_Tipo_Bien.SelectedItem.Value.Equals("VEHICULO") || Cmb_Tipo_Bien.SelectedItem.Value.Equals("TODOS")) {
                    Dt_Datos.Merge(Cls_Negocio.Consultar_Vehiculos());
                }
                if (Cmb_Tipo_Bien.SelectedItem.Value.Equals("BIEN_INMUEBLE") || Cmb_Tipo_Bien.SelectedItem.Value.Equals("TODOS")) {
                    Cls_Negocio.P_Estatus = "ALTA";
                    Dt_Datos.Merge(Cls_Negocio.Consultar_Bienes_Inmuebles());
                }
                DataTable Dt_Depreciados = Realizar_Deprecicacion_Bienes(ref Dt_Datos, ref Dt_No_Depreciados);
                Cls_Negocio = new Cls_Ope_Pat_Depreciacion_Bienes_Negocio();
                if (Cmb_Clase_Activo.SelectedIndex > 0) Cls_Negocio.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
                if (Cmb_Tipo_Activo.SelectedIndex > 0) Cls_Negocio.P_Tipo_Activo_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
                Cls_Negocio.P_Tipo_Bien = ((Cmb_Tipo_Bien.SelectedIndex > 0) ? Cmb_Tipo_Bien.SelectedItem.Text.Trim() : "TODOS");
                Cls_Negocio.P_Dt_Bienes_Depreciar = Dt_Depreciados;
                Cls_Negocio.P_Usuario_ID = Cls_Sessiones.Empleado_ID;
                Cls_Negocio.P_Nombre_Usuario = Cls_Negocio.P_Nombre_Usuario;
                Cls_Negocio.Actualizar_Bienes_Depreciados();
                ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No. Total de Bienes a Depreciar: " + Dt_Datos.Rows.Count + ".\\nNo. Total de Bienes Depreciados: " + Dt_Depreciados.Rows.Count + ".\\n\\nNOTA: LOS BIENES QUE NO SE DEPRECIARÓN ES POR FALTA DE ALGUN DATO PARA EL CALCULO.');", true);
                
                if (Dt_No_Depreciados.Rows.Count > 0)
                {
                    Session["DT_BIENES_NO_MIGRADOS"] = Dt_No_Depreciados;
                    Btn_Exportar_Bienes_No_Migrados.Visible = true;
                }
            } catch (Exception Ex) {
                Lbl_Ecabezado_Mensaje.Text = "Verificar";
                Lbl_Mensaje_Error.Text = "[" + Ex.Message + "]";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }

        /// *************************************************************************************************************************
        /// Nombre: Pasar_DataTable_A_Excel
        /// 
        /// Descripción: Pasa DataTable a Excel.
        /// 
        /// Parámetros: Dt_Reporte.- DataTable que se pasara a excel.
        /// 
        /// Usuario Creo: Juan Alberto Hernández Negrete.
        /// Fecha Creó: 18/Octubre/2011.
        /// Usuario Modifico:
        /// Fecha Modifico:
        /// Causa Modificación:
        /// *************************************************************************************************************************
        public void Pasar_DataTable_A_Excel(System.Data.DataTable Dt_Reporte)
        {
            String Ruta = "Bienes que no se depreciaron.xls";//Variable que almacenara el nombre del archivo. 

            try
            {
                //Creamos el libro de Excel.
                CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();

                Libro.Properties.Title = "Reporte de Cuenta Publica de Patrimonio";
                Libro.Properties.Created = DateTime.Now;
                Libro.Properties.Author = "Patrimonio";

                //Creamos una hoja que tendrá el libro.
                CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("Registros");
                //Agregamos un renglón a la hoja de excel.
                CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
                //Creamos el estilo cabecera para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("HeaderStyle");
                //Creamos el estilo contenido para la hoja de excel. 
                CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido = Libro.Styles.Add("BodyStyle");

                Estilo_Cabecera.Font.FontName = "Tahoma";
                Estilo_Cabecera.Font.Size = 10;
                Estilo_Cabecera.Font.Bold = true;
                Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.Center;
                Estilo_Cabecera.Font.Color = "#FFFFFF";
                Estilo_Cabecera.Interior.Color = "#193d61";
                Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Cabecera.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Cabecera.Alignment.WrapText = true;

                Estilo_Contenido.Font.FontName = "Tahoma";
                Estilo_Contenido.Font.Size = 8;
                Estilo_Contenido.Font.Bold = true;
                Estilo_Contenido.Alignment.Horizontal = StyleHorizontalAlignment.Center;
                Estilo_Contenido.Font.Color = "#000000";
                Estilo_Contenido.Interior.Color = "White";
                Estilo_Contenido.Interior.Pattern = StyleInteriorPattern.Solid;
                Estilo_Contenido.Borders.Add(StylePosition.Top, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Bottom, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Left, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Borders.Add(StylePosition.Right, LineStyleOption.Continuous, 1, "Black");
                Estilo_Contenido.Alignment.WrapText = true;

                //Agregamos las columnas que tendrá la hoja de excel.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//Tipo
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(85));//Inventario
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(70));//Importe
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//Fecha de Adquisición
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Vida Util.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//Depreciación.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));//Cuenta Contable Gasto.
                Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(180));//Cuenta Contable Depreciación.

                if (Dt_Reporte is System.Data.DataTable)
                {
                    if (Dt_Reporte.Rows.Count > 0)
                    {
                        foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                        {
                            if (COLUMNA is System.Data.DataColumn)
                            {
                                Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(COLUMNA.ColumnName.Replace("_", " "), "HeaderStyle"));
                            }
                            Renglon.Height = 20;
                        }

                        foreach (System.Data.DataRow FILA in Dt_Reporte.Rows)
                        {
                            if (FILA is System.Data.DataRow)
                            {
                                Renglon = Hoja.Table.Rows.Add();

                                foreach (System.Data.DataColumn COLUMNA in Dt_Reporte.Columns)
                                {
                                    if (COLUMNA is System.Data.DataColumn)
                                    {
                                        if (COLUMNA.ColumnName.Trim().Equals("IMPORTE"))
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(String.Format("{0:#,###,##0.00}", Convert.ToDouble(FILA[COLUMNA.ColumnName])), DataType.String, "BodyStyle"));
                                        }
                                        else
                                        {
                                            Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(FILA[COLUMNA.ColumnName].ToString().Replace("_", " "), DataType.String, "BodyStyle"));
                                        }
                                    }
                                }
                                Renglon.Height = 20;
                                Renglon.AutoFitHeight = true;
                            }
                        }
                    }
                }

                //Abre el archivo de excel
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Ruta);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                Response.End();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Bienes_No_Migrados_Click
        ///DESCRIPCIÓN: Btn_Exportar_Bienes_No_Migrados_Click
        ///PROPIEDADES:     
        ///CREO:                 
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: Julio/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Btn_Exportar_Bienes_No_Migrados_Click(object sender, ImageClickEventArgs e)
        {
            if (Session["DT_BIENES_NO_MIGRADOS"] != null)
            {
                DataTable Dt_Datos = (DataTable)Session["DT_BIENES_NO_MIGRADOS"];
                Pasar_DataTable_A_Excel(Dt_Datos);
            }
        }

    #endregion

}