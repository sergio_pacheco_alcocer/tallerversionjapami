﻿<%@ Page Title="Reporte de Listado de Catálogo" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Rpt_Pat_Listado_Catalogos.aspx.cs" Inherits="paginas_Control_Patrimonial_Frm_Rpt_Pat_Listado_Catalogos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
    <!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->

    <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="36000"
        EnableScriptLocalization="true" EnableScriptGlobalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="4">
                            Reporte de Listado de Catálogos
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="4" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td colspan="3">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="left">
                        <td style="width: 50%; text-align: left;">
                            &nbsp;
                            <asp:ImageButton ID="Btn_Exportar_Excel" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" OnClick="Btn_Exportar_Excel_Click"
                                Width="24px" ToolTip="Exportar a Listado a Excel" AlternateText="Exportar a Listado a Excel" />
                            &nbsp;
                        </td>
                        <td style="width: 50%; text-align: right;">
                            &nbsp;
                            <asp:ImageButton ID="Btn_Limpiar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" OnClick="Btn_Limpiar_Click"
                                Width="24px" ToolTip="Limpiar Filtros" AlternateText="Limpiar Filtros" />
                            &nbsp;
                        </td>
                    </tr>
                </table>
                <br />
                <table width="99%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td style="width: 18%;">
                            <asp:Label ID="Lbl_Catalogo" runat="server" Text="Catálogo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Catalogo" runat="server" Width="100%" >
                                <asp:ListItem Value="ASEGURADORAS">ASEGURADORAS</asp:ListItem>
                                <asp:ListItem Value="CLASES_ACTIVO">CLASE ACTIVOS</asp:ListItem>
                                <asp:ListItem Value="CLASIFICACION_ZONA">CLASIFICACIÓN ZONA</asp:ListItem>
                                <asp:ListItem Value="COLORES">COLORES</asp:ListItem>
                                <asp:ListItem Value="DESTINOS_INMUEBLES">DESTINOS INMUEBLES</asp:ListItem>
                                <asp:ListItem Value="DETALLES_VEHICULOS">DETALLES VEHICULOS</asp:ListItem>
                                <asp:ListItem Value="MATERIALES">MATERIALES</asp:ListItem>
                                <asp:ListItem Value="ORIENTACIONES">ORIENTACIONES</asp:ListItem>
                                <asp:ListItem Value="ORIGENES_INMUEBLES">ORIGENES INMUEBLES</asp:ListItem>
                                <asp:ListItem Value="PROCEDENCIAS">PROCEDENCIAS</asp:ListItem>
                                <asp:ListItem Value="TIPOS_ACTIVOS">TIPOS DE ACTIVO</asp:ListItem>
                                <asp:ListItem Value="TIPOS_BAJAS">TIPOS DE BAJA</asp:ListItem>
                                <asp:ListItem Value="TIPOS_COMBUSTIBLES">TIPOS DE COMBUSTIBLE</asp:ListItem>
                                <asp:ListItem Value="TIPOS_VEHICULOS">TIPOS DE VEHICULO</asp:ListItem>
                                <asp:ListItem Value="TIPOS_SINIESTROS">TIPOS SINIESTROS</asp:ListItem>
                                <asp:ListItem Value="UBICACIONES_FISICAS">UBICACIONES FISICAS</asp:ListItem>
                                <asp:ListItem Value="USOS_INMUEBLES">USOS INMUEBLES</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 18%;">
                            <asp:Label ID="Lbl_Nombre" runat="server" Text="Nombre / Descripción"></asp:Label>
                        </td>
                        <td style="width: 32%;">
                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="99%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre" runat="server" TargetControlID="Txt_Nombre"
                                FilterType="UppercaseLetters, Numbers, LowercaseLetters, Custom" ValidChars="_.:.;!$&/()=?¡[]¨*^`~ ñÑ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 18%;">
                            &nbsp;<asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td style="width: 32%;">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%">
                                <asp:ListItem Value="">&lt;-- TODOS --&gt;</asp:ListItem>
                                <asp:ListItem Value="VIGENTE">VIGENTE</asp:ListItem>
                                <asp:ListItem Value="BAJA">BAJA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Exportar_Excel" />
        </Triggers>
    </asp:UpdatePanel>
</asp:Content>
