﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Text;

public partial class paginas_Comercializacion_Sistema_Generales_Frm_Ope_Pat_Mostrar_Codigo_Barras : System.Web.UI.Page
{

    #region "PAGE LOAD"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: No tiene codigo
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Mayo/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e) {

        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Init
        ///DESCRIPCIÓN: Inicialización de la Página
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Mayo/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Page_Init(object sender, EventArgs e) {
            if (!IsPostBack)
            {
                if (Session["Numero_Codigo_Barras"] != null && Session["Imagen_Codigo_Barras"] != null)
                {
                    Div_Tamano_Normal.InnerHtml = Crear_Html_Tamano_Normal(Session["Numero_Codigo_Barras"].ToString().Trim(), Session["Imagen_Codigo_Barras"].ToString().Trim());
                    Div_Tamano_Pequeno.InnerHtml = Crear_Html_Tamano_Pequeno(Session["Numero_Codigo_Barras"].ToString().Trim(), Session["Imagen_Codigo_Barras"].ToString().Trim());
                    Session.Remove("Numero_Codigo_Barras");
                    Session.Remove("Imagen_Codigo_Barras");
                }
                if (Session["Tamano_Codigo_Barras"] != null)
                {
                    if (Session["Tamano_Codigo_Barras"].ToString().Trim() == "NORMAL") { Div_Tamano_Normal.Visible = true; Div_Tamano_Pequeno.Visible = false; }
                    if (Session["Tamano_Codigo_Barras"].ToString().Trim() == "PEQUENO") { Div_Tamano_Normal.Visible = false; Div_Tamano_Pequeno.Visible = true; }
                    Session.Remove("Tamano_Codigo_Barras");
                }
            }
        }

    #endregion "PAGE LOAD"

    #region "METODOS"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Crear_Html_Tamano_Pequeno
        ///DESCRIPCIÓN: Crea la Página Html en Pequeño
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Mayo/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public String Crear_Html_Tamano_Pequeno(String Codigo_Barras_Numero, String Codigo_Barras_Img) {
            StringBuilder Respuesta = new StringBuilder();
            Respuesta.Append("<table width=\"100%\">");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%; font-weight: bolder; font-size:6px;\">");
            Respuesta.Append("<span id=\"Lbl_Japami\">JAPAMI</span>");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%;\">");
            Respuesta.Append("<img src='" + Codigo_Barras_Img.Trim() + "'style='width:2cm;height:0.5cm' alt='sin codigo' />");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%; font-size:6px;\">");
            Respuesta.Append("<span id=\"Lbl_Numero_Inventario\">" + Codigo_Barras_Numero + "</span>");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("</table>");
            return Respuesta.ToString().Trim();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Crear_Html_Tamano_Normal
        ///DESCRIPCIÓN: Crea la Página Html en Normal
        ///PROPIEDADES:    
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Mayo/2013 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        public String Crear_Html_Tamano_Normal(String Codigo_Barras_Numero, String Codigo_Barras_Img) {
            StringBuilder Respuesta = new StringBuilder();
            Respuesta.Append("<table width=\"100%\">");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%; font-weight: bolder; font-size:small;\">");
            Respuesta.Append("<span id=\"Lbl_Japami\">JAPAMI</span>");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%;\">");
            Respuesta.Append("<img src='" + Codigo_Barras_Img.Trim() + "'style='width:4.9cm;height:1.48cm' alt='sin codigo' />");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("<tr>");
            Respuesta.Append("<td style=\"text-align:center; width:100%; font-size:small;\">");
            Respuesta.Append("<span id=\"Lbl_Numero_Inventario\">" + Codigo_Barras_Numero + "</span>");
            Respuesta.Append("</td>");
            Respuesta.Append("</tr>");

            Respuesta.Append("</table>");
            return Respuesta.ToString().Trim();
        }

    #endregion "METODOS"

}