﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Reportes;
using JAPAMI.Control_Patrimonial_Catalogo_Donadores.Negocio;
using System.Collections.Generic;

public partial class paginas_Compras_Frm_Cat_Pat_Com_Donadores : System.Web.UI.Page
{

    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        if (!IsPostBack)
        {
            Configuracion_Formulario(true);
            Llenar_Grid_Donadores(0);
        }
        Div_Contenedor_Msj_Error.Visible = false;
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
    ///PROPIEDADES:     
    ///             1. Estatus.    Estatus en el que se cargara la configuración de los
    ///                             controles.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Configuracion_Formulario(Boolean Estatus)
    {
        Btn_Nuevo.Visible = true;
        Btn_Nuevo.AlternateText = "Nuevo";
        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
        Btn_Modificar.Visible = true;
        Btn_Modificar.AlternateText = "Modificar";
        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
        Btn_Eliminar.Visible = Estatus;
        Txt_Nombre_Donador_MPE.Enabled = !Estatus;
        Txt_Apellido_Paterno_Donador.Enabled = !Estatus;
        Txt_Apellido_Materno_Donador.Enabled = !Estatus;
        Txt_Direccion_Donador.Enabled = !Estatus;
        Txt_Ciudad_Donador.Enabled = !Estatus;
        Txt_Estado_Donador.Enabled = !Estatus;
        Txt_Telefono_Donador.Enabled = !Estatus;
        Txt_Celular_Donador.Enabled = !Estatus;
        Txt_CURP_Donador.Enabled = !Estatus;
        Txt_RFC_Donador_MPE.Enabled = !Estatus;
        Grid_Donadores.Enabled = Estatus;
        Grid_Donadores.SelectedIndex = (-1);
        Btn_Buscar.Enabled = Estatus;
        Txt_Busqueda.Enabled = Estatus;

        Configuracion_Acceso("Frm_Cat_Pat_Com_Donadores.aspx");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Catalogo
    ///DESCRIPCIÓN: Limpia los controles del Formulario
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Catalogo()
    {
        Hdf_Donador_ID.Value = "";
        Txt_Nombre_Donador_MPE.Text = "";
        Txt_Apellido_Paterno_Donador.Text = "";
        Txt_Apellido_Materno_Donador.Text = "";
        Txt_Direccion_Donador.Text = "";
        Txt_Ciudad_Donador.Text = "";
        Txt_Estado_Donador.Text = "";
        Txt_Telefono_Donador.Text = "";
        Txt_Celular_Donador.Text = "";
        Txt_CURP_Donador.Text = "";
        Txt_RFC_Donador_MPE.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Donadores
    ///DESCRIPCIÓN: Llena la tabla de Donadores con una consulta que puede o no
    ///             tener Filtros.
    ///PROPIEDADES:     
    ///             1. Pagina.  Pagina en la cual se mostrará el Grid_VIew
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Donadores(int Pagina)
    {
        try
        {
            Cls_Cat_Pat_Com_Donadores_Negocio Donadores = new Cls_Cat_Pat_Com_Donadores_Negocio();
            Donadores.P_Nombre = Txt_Busqueda.Text.Trim();
            Grid_Donadores.Columns[1].Visible = true;
            Grid_Donadores.DataSource = Donadores.Consultar_Donadores();
            Grid_Donadores.PageIndex = Pagina;
            Grid_Donadores.DataBind();
            Grid_Donadores.Columns[1].Visible = false;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private bool Validar_Componentes()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Txt_Nombre_Donador_MPE.Text.Trim().Length == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Introducir el Nombre del Donador.";
            Validacion = false;
        }
        if (Txt_RFC_Donador_MPE.Text.Trim().Length == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Introducir el RFC.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    #endregion

    #endregion

    #region Grids

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Donadores_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de los Donadores
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Donadores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Donadores.SelectedIndex = (-1);
            Llenar_Grid_Donadores(e.NewPageIndex);
            Limpiar_Catalogo();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Donadores_SelectedIndexChanged
    ///DESCRIPCIÓN: Obtiene los datos de un Donador Seleccionado para mostrarlos a detalle
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Donadores_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Donadores.SelectedIndex > (-1))
            {
                Limpiar_Catalogo();
                Hdf_Donador_ID.Value = HttpUtility.HtmlDecode(Grid_Donadores.SelectedRow.Cells[1].Text.Trim());
                Cls_Cat_Pat_Com_Donadores_Negocio Donadores_Negocio = new Cls_Cat_Pat_Com_Donadores_Negocio();
                Donadores_Negocio.P_Donador_ID = Hdf_Donador_ID.Value.Trim();
                Donadores_Negocio = Donadores_Negocio.Consultar_Datos_Donador();
                Txt_Nombre_Donador_MPE.Text = Donadores_Negocio.P_Nombre.Trim();
                Txt_Apellido_Paterno_Donador.Text = Donadores_Negocio.P_Apellido_Paterno.Trim();
                Txt_Apellido_Materno_Donador.Text = Donadores_Negocio.P_Apellido_Materno.Trim();
                Txt_Direccion_Donador.Text = Donadores_Negocio.P_Direccion.Trim();
                Txt_Ciudad_Donador.Text = Donadores_Negocio.P_Cuidad.Trim();
                Txt_Estado_Donador.Text = Donadores_Negocio.P_Estado.Trim();
                Txt_Telefono_Donador.Text = Donadores_Negocio.P_Telefono.Trim();
                Txt_Celular_Donador.Text = Donadores_Negocio.P_Celular.Trim();
                Txt_CURP_Donador.Text = Donadores_Negocio.P_CURP.Trim();
                Txt_RFC_Donador_MPE.Text = Donadores_Negocio.P_RFC.Trim();
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Deja los componentes listos para dar de Alta una nuevo Donador.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Nuevo.AlternateText.Equals("Nuevo"))
            {
                Configuracion_Formulario(false);
                Limpiar_Catalogo();
                Btn_Nuevo.AlternateText = "Dar de Alta";
                Btn_Nuevo.ToolTip = "Dar de Alta";
                Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                Btn_Salir.AlternateText = "Cancelar";
                Btn_Salir.ToolTip = "Cancelar";
                Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                Btn_Modificar.Visible = false;
                Btn_Eliminar.Visible = false;
            }
            else
            {
                if (Validar_Componentes())
                {
                    Cls_Cat_Pat_Com_Donadores_Negocio Donadores_Negocio = new Cls_Cat_Pat_Com_Donadores_Negocio();
                    Donadores_Negocio.P_Nombre = Txt_Nombre_Donador_MPE.Text.Trim();
                    Donadores_Negocio.P_Apellido_Paterno = Txt_Apellido_Paterno_Donador.Text.Trim();
                    Donadores_Negocio.P_Apellido_Materno = Txt_Apellido_Materno_Donador.Text.Trim();
                    Donadores_Negocio.P_Direccion = Txt_Direccion_Donador.Text.Trim();
                    Donadores_Negocio.P_Cuidad = Txt_Ciudad_Donador.Text.Trim();
                    Donadores_Negocio.P_Estado = Txt_Estado_Donador.Text.Trim();
                    Donadores_Negocio.P_Telefono = Txt_Telefono_Donador.Text.Trim();
                    Donadores_Negocio.P_Celular = Txt_Celular_Donador.Text.Trim();
                    Donadores_Negocio.P_CURP = Txt_CURP_Donador.Text.Trim();
                    Donadores_Negocio.P_RFC = Txt_RFC_Donador_MPE.Text.Trim();
                    Donadores_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Donadores_Negocio.Alta_Donador();
                    Configuracion_Formulario(true);
                    Limpiar_Catalogo();
                    Llenar_Grid_Donadores(Grid_Donadores.PageIndex);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Donador", "alert('Alta de Donador Exitosa');", true);
                    Btn_Nuevo.AlternateText = "Nuevo";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Deja los componentes listos para hacer la modificacion de un Donador
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, EventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText.Equals("Modificar"))
            {
                if (Grid_Donadores.Rows.Count > 0 && Grid_Donadores.SelectedIndex > (-1))
                {
                    Configuracion_Formulario(false);
                    Btn_Modificar.AlternateText = "Actualizar";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Btn_Salir.AlternateText = "Cancelar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.Visible = false;
                    Btn_Eliminar.Visible = false;
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Modificar.";
                    Lbl_Mensaje_Error.Text = "";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            {
                if (Validar_Componentes())
                {
                    Cls_Cat_Pat_Com_Donadores_Negocio Donadores_Negocio = new Cls_Cat_Pat_Com_Donadores_Negocio();
                    Donadores_Negocio.P_Donador_ID = Hdf_Donador_ID.Value;
                    Donadores_Negocio.P_Nombre = Txt_Nombre_Donador_MPE.Text.Trim();
                    Donadores_Negocio.P_Apellido_Paterno = Txt_Apellido_Paterno_Donador.Text.Trim();
                    Donadores_Negocio.P_Apellido_Materno = Txt_Apellido_Materno_Donador.Text.Trim();
                    Donadores_Negocio.P_Direccion = Txt_Direccion_Donador.Text.Trim();
                    Donadores_Negocio.P_Cuidad = Txt_Ciudad_Donador.Text.Trim();
                    Donadores_Negocio.P_Estado = Txt_Estado_Donador.Text.Trim();
                    Donadores_Negocio.P_Telefono = Txt_Telefono_Donador.Text.Trim();
                    Donadores_Negocio.P_Celular = Txt_Celular_Donador.Text.Trim();
                    Donadores_Negocio.P_CURP = Txt_CURP_Donador.Text.Trim();
                    Donadores_Negocio.P_RFC = Txt_RFC_Donador_MPE.Text.Trim();
                    Donadores_Negocio.P_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Donadores_Negocio.Modificar_Donador();
                    Configuracion_Formulario(true);
                    Limpiar_Catalogo();
                    Llenar_Grid_Donadores(Grid_Donadores.PageIndex);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Donadores", "alert('Actualización de Donador Exitosa');", true);
                    Btn_Modificar.AlternateText = "Modificar";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Btn_Salir.AlternateText = "Salir";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Llena la Tabla con la opcion buscada
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Limpiar_Catalogo();
            Grid_Donadores.SelectedIndex = (-1);
            Grid_Donadores.SelectedIndex = (-1);
            Llenar_Grid_Donadores(0);
            if (Grid_Donadores.Rows.Count == 0 && Txt_Busqueda.Text.Trim().Length > 0)
            {
                Lbl_Ecabezado_Mensaje.Text = "Para la Busqueda con el nombre \"" + Txt_Busqueda.Text + "\" no se encontrarón coincidencias";
                Lbl_Mensaje_Error.Text = "(Se cargarón todos los Donadores almacenados)";
                Div_Contenedor_Msj_Error.Visible = true;
                Txt_Busqueda.Text = "";
                Llenar_Grid_Donadores(0);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Eliminar_Click
    ///DESCRIPCIÓN: Elimina un Donador de la Base de Datos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Eliminar_Click(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Donadores.Rows.Count > 0 && Grid_Donadores.SelectedIndex > (-1))
            {
                Cls_Cat_Pat_Com_Donadores_Negocio Donador = new Cls_Cat_Pat_Com_Donadores_Negocio();
                Donador.P_Donador_ID = Hdf_Donador_ID.Value;
                Donador.Eliminar_Donador();
                Grid_Donadores.SelectedIndex = (-1);
                Llenar_Grid_Donadores(Grid_Donadores.PageIndex);
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Donador", "alert('La Donador fue eliminada exitosamente');", true);
                Limpiar_Catalogo();
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Eliminar.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, EventArgs e)
    {
        if (Btn_Salir.AlternateText.Equals("Salir"))
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Configuracion_Formulario(true);
            Limpiar_Catalogo();
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ToolTip = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
        }
    }

    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// 
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// 
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS_AlternateText(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

}
