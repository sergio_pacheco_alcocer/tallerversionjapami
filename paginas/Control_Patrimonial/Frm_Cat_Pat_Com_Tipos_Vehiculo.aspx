<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Cat_Pat_Com_Tipos_Vehiculo.aspx.cs" Inherits="paginas_Compras_Frm_Cat_Pat_Com_Tipos_Vehiculo"
    Title="Catalogo de Tipos de Vehiculo" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <script type="text/javascript" language="javascript">
        //Funcion que ejecuta el evento del boton de buscar mediante la tecla enter en la caja de texto que implementa la funcion
        function get_KeyPress(textbox, evento) {
            //debugger;
            var keyCode;
            if (evento.which || evento.charCode) {
                keyCode = evento.which ? evento.which : evento.charCode;
                //return (keyCode != 13);
            }
            else if (window.event) {
                keyCode = event.keyCode;
                if (keyCode == 13) {
                    if (event.keyCode)
                        event.keyCode = 9;
                }
            }

            if (keyCode == 13) {

                (document.getElementById('<%=Btn_Buscar.ClientID %>')).click();
                //                alert("se presiono Enter");
                window.focus();
                return false;
            }
            return true;
        }
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Tipos_Vehiculo" runat="server" EnableScriptLocalization="true"
        EnableScriptGlobalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">
                            Cat�logo de Tipos de Vehiculo
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" style="width: 50;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Nuevo" OnClick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Modificar" OnClick="Btn_Modificar_Click" />
                            <asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Eliminar" OnClientClick="return confirm('�Esta seguro que desea eliminar el registro de la Base de Datos?');"
                                OnClick="Btn_Eliminar_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td style="width: 50;">
                            Busqueda
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="180px" onkeypress="return get_KeyPress(this,event);"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Buscar" runat="server" CausesValidation="false" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                ToolTip="Buscar" OnClick="Btn_Buscar_Click" AlternateText="Consultar" />
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkText="<- Descripci�n ->"
                                TargetControlID="Txt_Busqueda" WatermarkCssClass="watermarked">
                            </cc1:TextBoxWatermarkExtender>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" runat="server" TargetControlID="Txt_Busqueda"
                                InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                ValidChars="��.,:;()���������� ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                </table>
                <br />
                <cc1:TabContainer ID="Tab_Contenedor_Pestagnas" runat="server" Width="98%" ActiveTabIndex="0"
                    CssClass="Tab" Style="visibility: visible;">
                    <cc1:TabPanel runat="server" HeaderText="Datos Generales" ID="Tab_Datos_Generales"
                        Width="100%">
                        <HeaderTemplate>
                            Datos Generales</HeaderTemplate>
                        <ContentTemplate>
                            <center>
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td colspan="4">
                                            <asp:HiddenField ID="Hdf_Tipo_Vehiculo_ID" runat="server" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 13%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Tipo_Vehiculo_ID" runat="server" Text="Tipo ID" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width: 37%; text-align: left;">
                                            <asp:TextBox ID="Txt_Tipo_Vehiculo_ID" runat="server" Width="50%" MaxLength="10"
                                                Enabled="False"></asp:TextBox>
                                        </td>
                                        <td colspan="2">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 13%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Descripcion" runat="server" Text="* Descripci�n"></asp:Label>
                                        </td>
                                        <td style="width: 37%; text-align: left;">
                                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                                        </td>
                                        <td style="width: 13%; text-align: left; vertical-align: top;">
                                            <asp:Label ID="Lbl_Estatus" runat="server" Text="* Estatus"></asp:Label>
                                        </td>
                                        <td style="width: 37%; text-align: left;">
                                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%">
                                                <asp:ListItem Text="&lt;SELECCIONE&gt;" Value="SELECCIONE"></asp:ListItem>
                                                <asp:ListItem Text="VIGENTE" Value="VIGENTE"></asp:ListItem>
                                                <asp:ListItem Text ="BAJA" Value="BAJA"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                                <br />
                                <asp:GridView ID="Grid_Tipos_Vehiculo" runat="server" CssClass="GridView_1" AutoGenerateColumns="False"
                                    AllowPaging="True" Width="100%" GridLines="None" OnSelectedIndexChanged="Grid_Tipos_Vehiculo_SelectedIndexChanged"
                                    OnPageIndexChanging="Grid_Tipos_Vehiculo_PageIndexChanging">
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                            <ItemStyle Width="30px" />
                                        </asp:ButtonField>
                                        <asp:BoundField DataField="TIPO_VEHICULO_ID" HeaderText="Tipo ID" SortExpression="TIPO_VEHICULO_ID">
                                            <ItemStyle Width="90px" HorizontalAlign="Center" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripcion" SortExpression="DESCRIPCION">
                                            <ItemStyle Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ESTATUS" HeaderText="Estatus" SortExpression="ESTATUS">
                                            <ItemStyle Width="70px" HorizontalAlign="Center" Font-Size="X-Small" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="ASEGURADORA_ID" HeaderText="ASEGURADORA_ID" SortExpression="ASEGURADORA_ID" />
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </center>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Seguro" ID="Tab_Seguro" Width="100%">
                        <HeaderTemplate>
                            Seguro</HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td colspan="4">
                                        <asp:HiddenField ID="Hdf_Vehiculo_Aseduradora_ID" runat="server" />
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 13%; text-align: left;">
                                        <asp:Label ID="Lbl_Aseguradoras" runat="server" Text="Aseguradora"></asp:Label>
                                    </td>
                                    <td style="width: 87%; text-align: left;">
                                        <asp:DropDownList ID="Cmb_Aseguradoras" runat="server" Width="98%">
                                            <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 13%; text-align: left;">
                                        <asp:Label ID="Lbl_Numero_Poliza_Seguro" runat="server" Text="N�mero Poliza"></asp:Label>
                                    </td>
                                    <td style="width: 87%; text-align: left;">
                                        <asp:TextBox ID="Txt_Numero_Poliza_Seguro" runat="server" Width="98%" MaxLength="30"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Poliza_Seguro" runat="server" TargetControlID="Txt_Numero_Poliza_Seguro"
                                            InvalidChars="<,>,&,',!," FilterType="UppercaseLetters, LowercaseLetters, Numbers, Custom" ValidChars="��.,:;-/()���������� "
                                            Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width: 13%; text-align: left;">
                                        <asp:Label ID="Lbl_Cobertura_Seguro" runat="server" Text="Cobertura"></asp:Label>
                                    </td>
                                    <td style="width: 87%; text-align: left;">
                                        <asp:TextBox ID="Txt_Cobertura_Seguro" runat="server" Width="98%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cobertura_Seguro" runat="server" TargetControlID="Txt_Cobertura_Seguro"
                                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="��.,:;()���������� /*-_+$%&!#$%&�?�[]{}" Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cobertura_Seguro" runat="server" WatermarkText="Longitud Maxima de 8000 Caracteres"
                                            TargetControlID="Txt_Cobertura_Seguro" WatermarkCssClass="watermarked">
                                        </cc1:TextBoxWatermarkExtender>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Archivos" ID="Tab_Archivos" Width="100%">
                        <HeaderTemplate>
                            Archivos</HeaderTemplate>
                        <ContentTemplate>
                            <asp:UpdatePanel ID="Upd_Archivos" runat="server" UpdateMode="Conditional" >
                                <ContentTemplate>
                                    <table width="100%" class="estilo_fuente">
                                        <tr>
                                            <td style="width: 13%; text-align: left;">
                                                <asp:Label ID="Lbl_Nombre_Archivo" runat="server" Text="Nombre"></asp:Label>
                                            </td>
                                            <td style="width: 87%; text-align: left;">
                                                <asp:TextBox ID="Txt_Nombre_Archivo" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Archivo" runat="server" TargetControlID="Txt_Nombre_Archivo"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="��.,:;()���������� " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 13%; text-align: left;">
                                                <asp:Label ID="Lbl_Comentarios_Archivo" runat="server" Text="Comentarios"></asp:Label>
                                            </td>
                                            <td style="width: 87%; text-align: left;">
                                                <asp:TextBox ID="Txt_Comentarios_Archivo" runat="server" Width="98%" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Archivo" runat="server" TargetControlID="Txt_Comentarios_Archivo"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="��.,:;()���������� " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Comentarios_Archivo" runat="server" WatermarkText="Longitud Maxima de 200 Caracteres"
                                                    TargetControlID="Txt_Comentarios_Archivo" WatermarkCssClass="watermarked" Enabled="True">
                                                </cc1:TextBoxWatermarkExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </ContentTemplate>
                                <Triggers>
                                    <asp:PostBackTrigger ControlID="Btn_Agregar_Archivo" />
                                </Triggers>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel runat="server">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td style="width: 13%;">
                                                <asp:Label ID="Lbl_Archivo" runat="server" Text="Archivo" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 87%;">
                                                <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" Width="690px" CompleteBackColor="LightBlue"
                                                    ThrobberID="Lbl_Progress_File" UploadingBackColor="LightGray" FailedValidation="False" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:Label ID="Lbl_Progress_File" runat="server" Text="">
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div class="processMessage" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                        </div>
                                    </asp:Label>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                            <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always">
                                <ContentTemplate>
                                    <table>
                                        <tr>
                                            <td colspan="2" align="right">
                                                <asp:ImageButton ID="Btn_Agregar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                                    Width="18px" Height="18px" CssClass="Img_Button" AlternateText="Agregar" ToolTip="Agregar Archivo"
                                                    OnClick="Btn_Agregar_Archivo_Click" />
                                                <asp:ImageButton ID="Btn_Limpiar_FileUpload" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                    Width="16px" Height="16px" CssClass="Img_Button" AlternateText="Limpiar Archivo"
                                                    ToolTip="Limpiar Archivo" />
                                            </td>
                                        </tr>
                                    </table>
                                    <asp:UpdateProgress ID="Upd_Grid_Archivos" runat="server" AssociatedUpdatePanelID="Upd_Archivos"
                                        DisplayAfter="0">
                                        <ProgressTemplate>
                                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                            </div>
                                            <div class="processMessage" id="div_progress">
                                                <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                            </div>
                                        </ProgressTemplate>
                                    </asp:UpdateProgress>
                                    <asp:GridView ID="Grid_Archivos" runat="server" AutoGenerateColumns="False" CellPadding="4" DataKeyNames="ARCHIVO_ID"
                                        ForeColor="#333333" GridLines="None" AllowPaging="True" Width="99%" OnPageIndexChanging="Grid_Archivos_PageIndexChanging"
                                        OnRowDataBound="Grid_Archivos_RowDataBound" OnRowDeleting="Grid_Archivos_RowDeleting" >
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:BoundField DataField="ARCHIVO_ID" HeaderText="ARCHIVO_ID" SortExpression="ARCHIVO_ID" />
                                            <asp:BoundField DataField="NOMBRE_ARCHIVO" HeaderText="NOMBRE_ARCHIVO" SortExpression="NOMBRE_ARCHIVO" />
                                            <asp:BoundField DataField="COMENTARIOS" HeaderText="COMENTARIOS" SortExpression="COMENTARIOS" />
                                            <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}" >
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-" >
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="90%" />
                                            </asp:BoundField>
                                            <asp:TemplateField HeaderText="Ver" >
                                                <ItemTemplate>
                                                    <asp:ImageButton ID="Btn_Ver_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png"
                                                        Width="24px" CssClass="Img_Button" AlternateText="Ver Archivo" OnClick="Btn_Ver_Archivo_Click" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:ButtonField ButtonType="Image" Visible="false" CommandName="Delete" ImageUrl="~/paginas/imagenes/paginas/Eliminar_Incidencia.png" >
                                            </asp:ButtonField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </ContentTemplate>
                            </asp:UpdatePanel>
                        </ContentTemplate>
                    </cc1:TabPanel>
                    <cc1:TabPanel runat="server" HeaderText="Detalles de Veh�culo" ID="Tab_Detalles_Vehiculo"
                        Width="100%">
                        <HeaderTemplate>
                            Detalles de Veh�culo</HeaderTemplate>
                        <ContentTemplate>
                            <table width="100%">
                                <tr>
                                    <td colspan="4">
                                        <div id="Div_Detalles_Vehiculos" runat="server" style="width: 98%; height: 500px;
                                            overflow: auto;">
                                            <asp:GridView ID="Grid_Detalles_Vehiculo" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="#333333" GridLines="None" Width="98%" PageSize="5">
                                                <RowStyle CssClass="GridItem" />
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:CheckBox ID="Chk_Seleccion_Detalle" runat="server" />
                                                        </ItemTemplate>
                                                        <ItemStyle Width="30px" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="DETALLE_ID" HeaderText="DETALLE_ID" SortExpression="DETALLE_ID" />
                                                    <asp:BoundField DataField="NOMBRE" HeaderText="Listado de Partes que contendr� el Tipo Veh�culo"
                                                        SortExpression="NOMBRE">
                                                        <ItemStyle Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <PagerStyle CssClass="GridHeader" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <HeaderStyle CssClass="GridHeader" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </ContentTemplate>
                    </cc1:TabPanel>
                </cc1:TabContainer>
            </div>
            <br />
            <br />
            <br />
            <br />
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
