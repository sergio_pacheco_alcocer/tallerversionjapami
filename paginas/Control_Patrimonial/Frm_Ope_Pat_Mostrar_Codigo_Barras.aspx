<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Mostrar_Codigo_Barras.aspx.cs" Inherits="paginas_Comercializacion_Sistema_Generales_Frm_Ope_Pat_Mostrar_Codigo_Barras" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
   <meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
    <title>Impresi&oacute;n C�digos de Barras</title>
     <script type='text/javascript' language='javascript'>
        function printpage() {
            print();
            ventana = window.parent.self;
            ventana.opener = window.parent.self;
            ventana.close();
        }
    </script>
</head>
<body onload="printpage();"> 
    <form id="form1" runat="server">
        <div ID="Div_Tamano_Normal" runat="server" style="Width:5.08cm; text-align:center; height:2.54cm; border-color: Black; border-style:none; border-width:thin; margin-left:0px;">
           
        </div>
        <div ID="Div_Tamano_Pequeno" runat="server" style="Width:2.1cm; text-align:center; height:1.27cm; border-color: Black; border-style:dotted; border-width:thin; margin-left:0px;">
            
        </div>
    </form>   
</body>
</html>
