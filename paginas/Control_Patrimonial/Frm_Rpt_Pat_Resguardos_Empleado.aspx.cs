﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Control_Patrimonial_Reporte_Listado_Bienes.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Text;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;

public partial class paginas_Control_Patrimonial_Frm_Rpt_Pat_Resguardos_Empleado : System.Web.UI.Page
{
    #region "Page Load"
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Evento que se carga cuando la Pagina de Inicia.
    ///PARAMETROS:     
    ///CREO: Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 28/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty))
            Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;

        if (!IsPostBack)
        {
            Llenar_Combos();
        }
    }
    #endregion


    #region "Metodos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Evento que se carga cuando la Pagina de Inicia.
    ///PARAMETROS:     
    ///CREO: Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 28/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Llenar_Combos()
    {
        try
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio BM_Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            DataTable Dt_Temporal = null;

            //Se llena combo de Dependencias
            BM_Negocio.P_Tipo_DataTable = "DEPENDENCIAS";
            Dt_Temporal = BM_Negocio.Consultar_DataTable();
            Cmb_Busqueda_Dependencia.DataSource = Dt_Temporal;
            Cmb_Busqueda_Dependencia.DataTextField = "NOMBRE";
            Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencia.DataBind();
            Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("< TODAS >", ""));

            //SE LLENA EL COMBO DE GERENCIAS
            BM_Negocio.P_Tipo_DataTable = "GERENCIAS";
            DataTable Gerencias = BM_Negocio.Consultar_DataTable();
            DataRow Fila_Gerencia = Gerencias.NewRow();
            Fila_Gerencia["GERENCIA_ID"] = "";
            Fila_Gerencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt; TODAS &gt;");
            Gerencias.Rows.InsertAt(Fila_Gerencia, 0);
            Cmb_Gerencias.DataSource = Gerencias;
            Cmb_Gerencias.DataValueField = "GERENCIA_ID";
            Cmb_Gerencias.DataTextField = "NOMBRE";
            Cmb_Gerencias.DataBind();
            Gerencias.Rows.RemoveAt(0);

            //Se llena combo de Zonas
            BM_Negocio.P_Tipo_DataTable = "ZONAS";
            Dt_Temporal = BM_Negocio.Consultar_DataTable();
            Cmb_Zona.DataSource = Dt_Temporal;
            Cmb_Zona.DataTextField = "DESCRIPCION";
            Cmb_Zona.DataValueField = "ZONA_ID";
            Cmb_Zona.DataBind();
            Cmb_Zona.Items.Insert(0, new ListItem("< TODAS >", ""));

            Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            CA_Negocio.P_Estatus = "VIGENTE";
            CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
            CA_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
            Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
            Cmb_Clase_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Clase_Activo.DataBind();
            Cmb_Clase_Activo.Items.Insert(0, new ListItem("< TODAS >", ""));

            Cls_Cat_Pat_Com_Clasificaciones_Negocio Clasificaciones_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            Clasificaciones_Negocio.P_Estatus = "VIGENTE";
            Clasificaciones_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
            Clasificaciones_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Tipo_Activo.DataSource = Clasificaciones_Negocio.Consultar_DataTable();
            Cmb_Tipo_Activo.DataValueField = "CLASIFICACION_ID";
            Cmb_Tipo_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Tipo_Activo.DataBind();
            Cmb_Tipo_Activo.Items.Insert(0, new ListItem("< TODOS >", ""));

            Cls_Cat_Pat_Com_Procedencias_Negocio Procedencia_Negocio = new Cls_Cat_Pat_Com_Procedencias_Negocio();
            Procedencia_Negocio.P_Estatus = "VIGENTE";
            Procedencia_Negocio.P_Tipo_DataTable = "PROCEDENCIAS";
            Cmb_Procedencia.DataSource = Procedencia_Negocio.Consultar_DataTable();
            Cmb_Procedencia.DataTextField = "NOMBRE";
            Cmb_Procedencia.DataValueField = "PROCEDENCIA_ID";
            Cmb_Procedencia.DataBind();
            Cmb_Procedencia.Items.Insert(0, new ListItem("< TODAS >", ""));

            //Se llena combo de Colores
            BM_Negocio.P_Tipo_DataTable = "COLORES";
            Dt_Temporal = BM_Negocio.Consultar_DataTable();
            Cmb_Color.DataSource = Dt_Temporal;
            Cmb_Color.DataTextField = "DESCRIPCION";
            Cmb_Color.DataValueField = "COLOR_ID";
            Cmb_Color.DataBind();
            Cmb_Color.Items.Insert(0, new ListItem("< TODOS >", ""));

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar al Llenar los Combos.";
            Lbl_Mensaje_Error.Text = "'" + Ex.Message + "'";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Evento que se carga cuando la Pagina de Inicia.
    ///PARAMETROS:     
    ///CREO: Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 28/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Busqueda_Empleados_Resguardo()
    {
        Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        Negocio.P_Estatus_Empleado = "ACTIVO','INACTIVO";
        if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado_Resguardante = Convertir_A_Formato_ID(Convert.ToInt32(Txt_Busqueda_No_Empleado.Text.Trim()), 6); }
        if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Resguardante = Txt_Busqueda_RFC.Text.Trim(); }
        if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Resguardante = Txt_Busqueda_Nombre_Empleado.Text.Trim().ToUpper(); }
        if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
        Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados_Resguardos();
        Grid_Busqueda_Empleados_Resguardo.DataBind();
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: Tipo: El formato del reporte
    ///PARAMETROS:     
    ///CREO: Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO: 28/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Generar_Reporte(String Tipo)
    {
        Ds_Rpt_Pat_Resguardos_Empleado Ds_Resguardos = new Ds_Rpt_Pat_Resguardos_Empleado();
        Cls_Rpt_Pat_Listado_Bienes_Negocio Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
        DataTable Dt_Consulta = new DataTable();
        DataSet Ds_Reporte = new DataSet();
        try
        {
            Mostrar_Mensaje_Error(false, "");

            if (Cmb_Tipo_Activo.SelectedIndex > 0)
            {
                Negocio.P_Clasificacion_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
            }
            if (Cmb_Clase_Activo.SelectedIndex > 0)
            {
                Negocio.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
            }
            if (Cmb_Gerencias.SelectedIndex > 0)
            {
                Negocio.P_Gerencia_ID = Cmb_Gerencias.SelectedItem.Value.Trim();
            }
            if (Cmb_Dependencia.SelectedIndex > 0)
            {
                Negocio.P_Dependencia_ID = Cmb_Dependencia.SelectedItem.Value.Trim();
            }
            if (Cmb_Zona.SelectedIndex > 0)
            {
                Negocio.P_Zona_ID = Cmb_Zona.SelectedItem.Value.Trim();
            }
            if (Hdf_Resguardante_ID.Value.Trim().Length > 0)
            {
                Negocio.P_Resguardante_ID = Hdf_Resguardante_ID.Value.Trim();
            }


            if (Txt_Nombre_Producto.Text.Trim().Length > 0)
            {
                Negocio.P_Nombre_Producto = Txt_Nombre_Producto.Text.Trim();
            }
            if (Txt_Modelo.Text.Trim().Length > 0)
            {
                Negocio.P_Modelo = Txt_Modelo.Text.Trim();
            }
            if (Cmb_Color.SelectedIndex > 0)
            {
                Negocio.P_Color_ID = Cmb_Color.SelectedItem.Value.Trim();
            }
            if (Cmb_Procedencia.SelectedIndex > 0)
            {
                Negocio.P_Procedencia = Cmb_Procedencia.SelectedItem.Value.Trim();
            }
            if (Txt_Factura.Text.Trim().Length > 0)
            {
                Negocio.P_Factura = Txt_Factura.Text.Trim();
            }
            if (Txt_Numero_Serie.Text.Trim().Length > 0)
            {
                Negocio.P_Serie = Txt_Numero_Serie.Text.Trim();
            }

            Dt_Consulta = Negocio.Consultar_Resguardos_Empleado();
            Dt_Consulta.TableName = "Dt_Resguardos_Empleado";

            Ds_Reporte.Tables.Add(Dt_Consulta.Copy());

            Generar_Reporte_Completo(Ds_Reporte, Ds_Resguardos, "Rpt_Pat_Resguardos_Empleado.rpt", Tipo);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "'" + Ex.Message + "'";
            Div_Contenedor_Msj_Error.Visible = true;
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Mensaje_Error
    ///DESCRIPCIÓN: Metodo que llena el grid view con el metodo de Consulta_tramites
    ///PARAMETROS:   
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Octubre/2012
    ///MODIFICO:  
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Mostrar_Mensaje_Error(Boolean Estatus, String Mensaje)
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = Estatus;
            Lbl_Ecabezado_Mensaje.Text = Mensaje;
            Lbl_Mensaje_Error.Text = "";
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Mensaje_Error.Text = "'" + Ex.Message + "'";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN  :   caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS   :   1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                 2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                 3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO         :   Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO   :   28/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Completo(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, string Nombre_Reporte, string Tipo)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);

        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);

        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        String Ruta = "../../Reporte/Rpt_Pat_Resguardo_Empleado" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
        String Empleado_Firma = "";
        String Jefe_Firma = "";
        if (Tipo.Equals("Excel"))
        {
            Ruta = "../../Reporte/Rpt_Pat_Resguardo_Empleado" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";
        }
        if (Hdf_Resguardante_ID.Value.Length > 0) Empleado_Firma = Txt_Resguardante.Text.Trim().Split('-')[1].Trim();
        if (Hdf_Jefe_ID.Value.Length > 0) Jefe_Firma = Txt_Jefe.Text.Trim().Split('-')[1].Trim();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        if (Tipo.Equals("Excel"))
        {
            Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        }
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        if (Tipo.Equals("Excel"))
        {
            Export_Options.ExportFormatType = ExportFormatType.Excel;
        }
        Reporte.SetParameterValue("EMPLEADO_FIRMA", Empleado_Firma);
        Reporte.SetParameterValue("JEFE_FIRMA", Jefe_Firma);
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Convertir_A_Formato_ID
    ///DESCRIPCIÓN: Pasa un numero entero a Formato de ID.
    ///PARAMETROS:     
    ///             1. Dato_ID. Dato que se desea pasar al Formato de ID.
    ///             2. Longitud_ID. Longitud que tendra el ID. 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Marzo/2011 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    private static String Convertir_A_Formato_ID(Int32 Dato_ID, Int32 Longitud_ID)
    {
        String Retornar = "";
        String Dato = "" + Dato_ID;
        for (int Cont_Temp = Dato.Length; Cont_Temp < Longitud_ID; Cont_Temp++)
        {
            Retornar = Retornar + "0";
        }
        Retornar = Retornar + Dato;
        return Retornar;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Rellenar_Tabla
    ///DESCRIPCIÓN  : Rellenara la tabla con El nombre de "Producto sin clasificacion"
    ///PARAMETROS   :     
    ///                 1. Dt_Consulta. tabla con la consulta
    ///CREO         : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO   : 28/Diciembre/2011 
    ///MODIFICO             : 
    ///FECHA_MODIFICO       : 
    ///CAUSA_MODIFICACIÓN   : 
    ///*******************************************************************************
    private DataTable Rellenar_Tabla(DataTable Dt_Consulta)
    {
        try
        {
            // se llenan los activos que no pertenecen a ninguna clasificacion
            if (Dt_Consulta is DataTable)
            {
                if (Dt_Consulta != null)
                {
                    if (Dt_Consulta.Rows.Count > 0)
                    {

                        DataRow[] Filas_Sin_Clasificacion = Dt_Consulta.Select("CLASE_ACTIVO IS NULL");
                        foreach (DataRow Fila_Actual in Filas_Sin_Clasificacion) {
                            Fila_Actual.SetField("CLASE_ACTIVO", "SIN CLASIFICACIÓN");
                        }
                    }
                }
            }


            //  se orden los campos
            DataView Dv_Ordenar = new DataView(Dt_Consulta);
            Dv_Ordenar.Sort = "NOMBRE_EMPLEADO, CLASE_ACTIVO, NUMERO_INVENTARIO ";
            Dt_Consulta = Dv_Ordenar.ToTable();

            Dt_Consulta.AcceptChanges();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Dt_Consulta;

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
    ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias(String Gerencia_ID)
    {
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Gerencia_ID = Gerencia_ID;
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";
        DataTable Dependencias = Combos.Consultar_DataTable();
        Cmb_Dependencia.DataSource = Dependencias;//Se especifica la fuente de información que llenará el combo
        Cmb_Dependencia.DataValueField = "DEPENDENCIA_ID";//Se especifica el campo de la tabla dependencias que contendrá el valor de los renglones del combo
        Cmb_Dependencia.DataTextField = "NOMBRE";//Se especifica el campo de la tabla dependencias que contendrá el texto que mostrará el combo
        Cmb_Dependencia.DataBind();//Se unen los datos del combo
        Cmb_Dependencia.Items.Insert(0, new ListItem("< TODAS >", ""));//Se inserta el renglón de <-SELECCIONE-> en la posición 0

    }

    #endregion


    #region "Grids"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
            {
                if (Hdf_Tipo_Busqueda.Value.Trim().Equals("RESGUARDANTE"))
                {
                    Hdf_Resguardante_ID.Value = "";
                    Txt_Resguardante.Text = "";
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                    Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                    DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                    Hdf_Resguardante_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                    Txt_Resguardante.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                    Txt_Resguardante.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                    Txt_Resguardante.Text = Txt_Resguardante.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                    Txt_Resguardante.Text = Txt_Resguardante.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("JEFE"))
                {
                    Hdf_Jefe_ID.Value = "";
                    Txt_Jefe.Text = "";
                    String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                    Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                    Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                    DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                    Hdf_Jefe_ID.Value = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim() : null);
                    Txt_Jefe.Text += ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim() : null);
                    Txt_Jefe.Text += " - " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString().Trim() : null);
                    Txt_Jefe.Text = Txt_Jefe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString().Trim() : null);
                    Txt_Jefe.Text = Txt_Jefe.Text.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString().Trim() : null);
                }
                Hdf_Tipo_Busqueda.Value = "";
                MPE_Resguardante.Hide();
                Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion
    #region "Eventos"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Resguardante_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Avanzada_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "RESGUARDANTE";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Jefe_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Jefe.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Btn_Busqueda_Avanzada_Jefe_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "JEFE";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_PDF_Click
    ///DESCRIPCIÓN: Lanza el Reporte en PDF para Imprimir.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Generar_Reporte_PDF_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
                Generar_Reporte("Pdf");
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Excel_Click
    ///DESCRIPCIÓN: Lanza el Reporte en Excel.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 05/Diciembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Generar_Reporte_Excel_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Generar_Reporte("Excel");
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Grencias_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Gerencias
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue.ToString());
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    #endregion

}
