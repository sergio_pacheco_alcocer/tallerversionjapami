<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Com_Alta_Vehiculos.aspx.cs" Inherits="paginas_predial_Frm_Ope_Pat_Com_Alta_Vehiculos"
    Title="Operaci�n - Asignaci�n de Vehiculos" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <%--liberias pera autocompletar--%>
    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript" language="javascript">
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr() {
            document.getElementById("<%=Txt_Busqueda_No_Empleado.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_RFC.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Busqueda_Dependencia.ClientID%>").value = "";
            document.getElementById("<%=Txt_Busqueda_Nombre_Empleado.ClientID%>").value = "";
            return false;
        }
        
        //Metodos para limpiar los controles de la busqueda.
        function Limpiar_Ctlr_Donador() {
            document.getElementById("<%=Hdf_Donador_ID.ClientID%>").value = "";
            document.getElementById("<%=Txt_Nombre_Donador.ClientID%>").value = "";
            return false;
        }

        function Carga_Completa(sender, args) {
            var archivo = args.get_fileName(); //Se asigna el archivo a la variable archivo
            if (archivo != "") {
                // se obtiene la extension del archivo
                var arreglo = new Array;
                arreglo = archivo.split("\\");
                var tamano = arreglo.length;
                var imagen = arreglo[tamano - 1];
                var extension = imagen.substring(imagen.lastIndexOf(".") + 1);
                if (extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG") {
                    if (args.get_length() > 2621440) {
                        var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                        alert("El Archivo " + archivo + ". Excedio el Tama�o Permitido:\n\nTama�o del Archivo: [" + args.get_length() + " Bytes]\nTama�o Permitido: [2621440 Bytes o 2.5 Mb]" + mensaje);
                        Limpiar_Contenido();
                        return false;
                    }
                    return true;
                } else {
                    var mensaje = "\n\nTabla de Documentos Anexos se limpiara completamente. Favor de volver a seleccionar los\narchivos a subir.";
                    alert("Tipo de archivo inv�lido " + archivo + "\n\nFormatos Validos [.jpg, .jpeg]" + mensaje);
                    Limpiar_Contenido();
                    return false;
                }
            } else {
                Limpiar_Contenido();
                return false;
            }
            return true;
        }

        function Limpiar_Contenido() {
            var AsyncFileUpload = $get("<%=AFU_Archivo.ClientID%>"); //Se obtiene el componente para cargar el archivo
            var texto = AsyncFileUpload.getElementsByTagName("input"); //Se almacena el texto del componente para cargar el archivo
            for (var i = 0; i < texto.length; i++) {
                if (texto[i].type == "file") {
                    texto[i].style.backgroundColor = "red";
                }
            }
        }

        $(function() {
            $("[id$='Txt_Numero_Economico']").live({ 'keyup': function() {
                var Str_Economico = $("[id$='Txt_Numero_Economico']").val();
                var Longitud_Palabra = Str_Economico.length;
                if (!isNaN(Str_Economico[0]))
                    Str_Economico = 'U' + Str_Economico;
                else
                    Str_Economico = Str_Economico.toUpperCase();
                $("[id$='Txt_Numero_Economico']").val(Str_Economico);
            }, 'blur': function() {
                if ($("[id$='Txt_Numero_Economico']").val().length > 0) {
                    var Str_Economico = $("[id$='Txt_Numero_Economico']").val();
                    var Str_Economico_Copia = new Array(6);
                    Str_Economico_Copia[0] = Str_Economico[0];
                    Str_Economico_Copia[1] = '0';
                    Str_Economico_Copia[2] = '0';
                    Str_Economico_Copia[3] = '0';
                    Str_Economico_Copia[4] = '0';
                    Str_Economico_Copia[5] = '0';
                    Str_Economico_Copia[6] = '0';
                    Longitud_Palabra = Str_Economico.length;
                    if (Longitud_Palabra < 6) {
                        for (var i = 1; i < Longitud_Palabra; i++) {
                            Str_Economico_Copia[6 - (Longitud_Palabra - i)] = Str_Economico[i];
                        }
                        $("[id$='Txt_Numero_Economico']").val(Str_Economico_Copia[0] + Str_Economico_Copia[1] + Str_Economico_Copia[2] + Str_Economico_Copia[3] + Str_Economico_Copia[4] + Str_Economico_Copia[5]);
                    }
                }
            }
            });
        });
        
    </script>

</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ScptM_Vehiculos" runat="server" EnableScriptGlobalization="true"
        EnableScriptLocalization="True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color: #ffffff; width: 100%; height: 100%;">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">
                            Asignaci�n de Veh�culos
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Nuevo" OnClick="Btn_Nuevo_Click"
                                ToolTip="Nuevo" />
                            <asp:ImageButton ID="Btn_Generar_Reporte" runat="server" ToolTip="Ver Formato de Resguardo"
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Reporte" OnClick="Btn_Generar_Reporte_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                CausesValidation="false" Width="24px" CssClass="Img_Button" AlternateText="Salir"
                                OnClick="Btn_Salir_Click" ToolTip="Salir" />
                        </td>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right" runat="server" id="TR_Codigo_Barras">
                        <td align="left" colspan="2">
                            <asp:Label ID="Lbl_Tamano_Etiqueta" runat="server" Text="Tama�o de la Etiqueta"></asp:Label>&nbsp;&nbsp;
                            <asp:DropDownList ID="Cmb_Tamano_Etiqueta" runat="server" Width="150px">
                                <asp:ListItem Value="NORMAL">NORMAL</asp:ListItem>
                                <asp:ListItem Value="PEQUENO">PEQUE�A</asp:ListItem>
                            </asp:DropDownList>&nbsp;
                            <asp:ImageButton ID="Btn_Imprimir_Codigo_Barras" runat="server" ToolTip="Imprimir Codigo de Barras"
                                ImageUrl="~/paginas/imagenes/paginas/barcode.png" Width="24px" CssClass="Img_Button"
                                AlternateText="Codigo_Barras" OnClick="Btn_Imprimir_Codigo_Barras_Click" />
                        </td>
                    </tr>
                </table>
            </div>
            <%--Termina Div Requisitos--%>
            <br />
            <div id="Div_Datos_Generales" runat="server" style="width: 98%">
                <%--div Datos Generales--%>
                <table width="100%">
                    <tr>
                        <td class="label_titulo" colspan="4">
                            Datos Generales<br />
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <asp:HiddenField ID="Hdf_Vehiculo_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Producto_ID" runat="server" />
                            <asp:HiddenField ID="Hdf_Tipo_Busqueda" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <div runat="server" style="width: 100%;" id="Div_Generales_Otra_Procedencia">
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td style="width: 18%">
                                            <asp:Label ID="Lbl_Producto" runat="server" Text="* Producto"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Nombre_Producto_Donado" runat="server" Width="99.5%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="18%">
                                            <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase Activo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="18%">
                                            <asp:Label ID="Lbl_Tipo_Activo" runat="server" Text="Tipo Activo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:DropDownList ID="Cmb_Tipo_Activo" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="18%">
                                            <asp:HiddenField ID="Hdf_Cuenta_Contable_ID" runat="server" />
                                            <asp:Label ID="Lbl_Cuenta_Contable" runat="server" Text="Cuenta Activo"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" Width="99.5%">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="18%">
                                            <asp:HiddenField ID="Hdf_Cuenta_Gasto_ID" runat="server" />
                                            <asp:Label ID="Lbl_Cuenta_Gasto" runat="server" Text="Cuenta Gasto"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:TextBox ID="Txt_Cuenta_Gasto" runat="server" Width="99.5%">
                                            </asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="18%">
                                            <asp:HiddenField ID="Hdf_Partida_Reparacion_ID" runat="server" />
                                            <asp:Label ID="Lbl_Partida_Reparacion" runat="server" Text="Partida Reparaci�n"></asp:Label>
                                        </td>
                                        <td colspan="3">
                                            <asp:Panel runat="server" ID="Pnl_Partida_Reparacion" DefaultButton="Btn_Busqueda_Partida_Reparacion">
                                                <asp:TextBox ID="Txt_Clave_Partida_Reparacion" runat="server" Width="10%" MaxLength="4">
                                                </asp:TextBox>&nbsp;
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave_Partida_Reparacion" runat="server" FilterType="Numbers" TargetControlID="Txt_Clave_Partida_Reparacion"></cc1:FilteredTextBoxExtender>
                                                 <asp:ImageButton ID="Btn_Busqueda_Partida_Reparacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Busqueda_Partida_Reparacion_Click" 
                                                    AlternateText="Buscar Partida Reparaci�n" ToolTip="Buscar Partida Reparaci�n" Width="20px" />
                                                <asp:TextBox ID="Txt_Nombre_Partida_Reparacion" runat="server" Width="83.5%" Enabled="false">
                                                </asp:TextBox>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Gerencias" runat="server" Text="* Gerencia "></asp:Label>
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:DropDownList ID="Cmb_Gerencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Gerencias_SelectedIndexChanged"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Dependencia" runat="server" Text="* Unidad Responsable"></asp:Label>
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:DropDownList ID="Cmb_Dependencias" runat="server" Width="100%" OnSelectedIndexChanged="Cmb_Dependencias_SelectedIndexChanged"
                                                AutoPostBack="True">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Zonas" runat="server" Text="* Ubicaci�n Fisica"></asp:Label>
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:DropDownList ID="Cmb_Zonas" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Serie_Carroceria" runat="server" Text="* Serie"></asp:Label>
                                        </td>
                                        <td style="width: 32%" align="left">
                                            <asp:TextBox ID="Txt_Serie_Carroceria" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Serie" runat="server" TargetControlID="Txt_Serie_Carroceria"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="��.,:;()���������� -_/" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Marca" runat="server" Text="* Marca"></asp:Label>
                                        </td>
                                        <td style="width: 32%">
                                            <asp:DropDownList ID="Cmb_Marca" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Tipos_Vehiculos" runat="server" Text="* Tipo"></asp:Label>
                                        </td>
                                        <td style="width: 32%" align="left">
                                            <asp:DropDownList ID="Cmb_Tipos_Vehiculos" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Color" runat="server" Text="* Color"></asp:Label>
                                        </td>
                                        <td style="width: 32%" align="left">
                                            <asp:DropDownList ID="Cmb_Colores" runat="server" Width="100%">
                                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%; text-align: left;">
                                            <asp:Label ID="Lbl_Modelo" runat="server" Text="* Modelo"></asp:Label>
                                        </td>
                                        <td colspan="3" align="left">
                                            <asp:TextBox ID="Txt_Modelo" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Modelo" runat="server" TargetControlID="Txt_Modelo"
                                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                ValidChars="��.,:;()���������� -_" Enabled="True">
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align: left; width: 18%;">
                                            <asp:Label ID="Lbl_RFC_Donador" runat="server" Text="RFC Donador"></asp:Label>
                                            <asp:HiddenField runat="server" ID="Hdf_Donador_ID" />
                                        </td>
                                        <td style="text-align: left; width: 32%;">
                                            <asp:TextBox ID="Txt_RFC_Donador" runat="server" Width="60%"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Buscar_RFC_Donador" runat="server" Visible="false" ImageUrl="~/paginas/imagenes/paginas/busqueda.png"
                                                AlternateText="Buscar" OnClick="Btn_Buscar_RFC_Donador_Click" Width="16px" />
                                            <asp:ImageButton ID="Btn_Agregar_Donador" runat="server" Visible="false" AlternateText="Buscar"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_add.png" OnClick="Btn_Agregar_Donador_Click"
                                                Width="16px" />
                                            <asp:ImageButton ID="Btn_Limpiar_Donador" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                                Height="16px" Width="16px" CssClass="Img_Button" AlternateText="Limpiar Donador"
                                                OnClientClick="javascript:return Limpiar_Ctlr_Donador();" ToolTip="Limpiar Donador" />
                                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_RFC_Donador" runat="server" Enabled="True"
                                                TargetControlID="Txt_RFC_Donador" WatermarkCssClass="watermarked" WatermarkText="RFC de Donador">
                                            </cc1:TextBoxWatermarkExtender>
                                        </td>
                                        <td colspan="2">
                                            <asp:TextBox ID="Txt_Nombre_Donador" runat="server" Width="98%" Enabled="false"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <%-- Termina Div Datos Generales--%>
            <br />
            <div id="Div_Detalles" runat="server" style="width: 98%">
                <%--Div Detalles--%>
                <center>
                    <table width="100%" class="estilo_fuente">
                        <tr>
                            <td class="label_titulo" colspan="4">
                                Detalles del Veh�culo
                            </td>
                        </tr>
                        <tr align="right" class="barra_delgada">
                            <td colspan="4" align="center">
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Numero_Inventario" runat="server" Text="No. Inventario"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Numero_Inventario" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Inventario" runat="server" TargetControlID="Txt_Numero_Inventario"
                                    InvalidChars="<,>,&,',!," FilterType="Numbers" ValidChars="��.,:;()���������� "
                                    Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Numero_Inventario" runat="server" Enabled="True"
                                    TargetControlID="Txt_Numero_Inventario" WatermarkCssClass="watermarked" WatermarkText="<-- Automatico -->">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%; text-align: left;">
                                <asp:Label ID="Lbl_Procedencia_Bien" runat="server" Text="* Procedencia"></asp:Label>
                            </td>
                            <td colspan="3">
                                <asp:DropDownList ID="Cmb_Procedencia" runat="server" Width="100%">
                                    <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Costo" runat="server" Text="* Costo [$]"></asp:Label>
                                <asp:RegularExpressionValidator ID="REV_Txt_Costo" runat="server" ErrorMessage="[No Valido. Verificar]"
                                    ControlToValidate="Txt_Costo" ValidationExpression="^\d+(\.\d\d)?$" Font-Size="X-Small"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Costo" runat="server" Enabled="true" Width="97%" MaxLength="12"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Costo" runat="server" Enabled="True" FilterType="Numbers, Custom"
                                    ValidChars="." TargetControlID="Txt_Costo">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="text-align: left; vertical-align: top; width: 18%">
                                <asp:Label ID="Lbl_Fecha_Inventario" runat="server" Text="* Fecha Resguardo"></asp:Label>
                            </td>
                            <td style="width: 32%;">
                                <asp:TextBox ID="Txt_Fecha_Inventario" runat="server" Width="85%" MaxLength="20"
                                    Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Fecha_Inventario" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Inventario" runat="server" TargetControlID="Txt_Fecha_Inventario"
                                    PopupButtonID="Btn_Fecha_Inventario" Format="dd/MMM/yyyy" Enabled="True">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%;">
                                <asp:Label ID="Lbl_Proveedor" runat="server" Text="Proveedor"></asp:Label>
                                <asp:HiddenField ID="Hdf_Proveedor_ID" runat="server" />
                            </td>
                            <td colspan="3">
                                <asp:TextBox ID="Txt_Nombre_Proveedor" runat="server" Width="93%" Enabled="false"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Lanzar_Mpe_Proveedores" runat="server" OnClick="Btn_Lanzar_Mpe_Proveedores_Click"
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Busqueda y Selecci�n de Proveedor"
                                    AlternateText="Buscar" />
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_No_Factura" runat="server" Text="* No. Factura"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_No_Factura" runat="server" Width="97%" MaxLength="20"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Factura" runat="server" TargetControlID="Txt_No_Factura"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                    ValidChars="��.,:;()���������� -_/" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Fecha_Adquisicion" runat="server" Text="* Fecha Facturaci�n"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Fecha_Adquisicion" runat="server" Width="85%" MaxLength="20"
                                    Enabled="False"></asp:TextBox>
                                <asp:ImageButton ID="Btn_Fecha_Adquisicion" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Adquisicion" runat="server" TargetControlID="Txt_Fecha_Adquisicion"
                                    PopupButtonID="Btn_Fecha_Adquisicion" Format="dd/MMM/yyyy" Enabled="True">
                                </cc1:CalendarExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Placas" runat="server" Text="* No. Placas"></asp:Label>
                            </td>
                            <td style="width: 32%;" align="left">
                                <asp:TextBox ID="Txt_Placas" MaxLength="12" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Placas" runat="server" TargetControlID="Txt_Placas"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                    ValidChars="��.,:;()���������� -_/" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Numero_Economico" runat="server" Text="* No. Econ�mico"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Numero_Economico" runat="server" Width="97%" Enabled="false"
                                    MaxLength="25"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Economico" runat="server" TargetControlID="Txt_Numero_Economico"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                    ValidChars="��.,:;()���������� -_/" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Numero_Cilindros" runat="server" Text="* No. Cilindros"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Numero_Cilindros" MaxLength="2" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Cilindros" runat="server" TargetControlID="Txt_Numero_Cilindros"
                                    FilterType="Numbers" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Anio_Fabricacion" runat="server" Text="* A�o Fabricaci�n"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Anio_Fabricacion" MaxLength="4" runat="server" Width="97%" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anio_Fabricacion" runat="server" TargetControlID="Txt_Anio_Fabricacion"
                                    FilterType="Numbers" ValidChars="��.,:;()���������� " Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Capacidad_Carga" runat="server" Text="* Capacidad Carga"></asp:Label>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="Txt_Capacidad_Carga" runat="server" Width="99%" Enabled="False"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Capacidad_Carga" runat="server" TargetControlID="Txt_Capacidad_Carga"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                    ValidChars="��.,:;()���������� " Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Tipo_Combustible" runat="server" Text="* Combustible"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:DropDownList ID="Cmb_Tipo_Combustible" runat="server" Width="100%">
                                    <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Kilometraje" runat="server" Text="* Kilometraje"></asp:Label>
                                <asp:RegularExpressionValidator ID="REV_Txt_Kilometraje" runat="server" ErrorMessage="[No Valido. Verificar]"
                                    ControlToValidate="Txt_Kilometraje" ValidationExpression="^\d+(\.\d\d)?$" Font-Size="X-Small"></asp:RegularExpressionValidator>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:TextBox ID="Txt_Kilometraje" runat="server" Width="97%" Enabled="False" MaxLength="15"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Kilometraje" runat="server" Enabled="True"
                                    FilterType="Numbers" TargetControlID="Txt_Kilometraje">
                                </cc1:FilteredTextBoxExtender>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Estatus" runat="server" Text="* Estatus"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="100%" Enabled="False">
                                    <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    <asp:ListItem>VIGENTE</asp:ListItem>
                                    <asp:ListItem>BAJA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width: 18%; text-align: left;">
                                <asp:Label ID="Lbl_Odometro" runat="server" Text="* Odometro"></asp:Label>
                            </td>
                            <td style="width: 32%" align="left">
                                <asp:DropDownList ID="Cmb_Odometro" runat="server" Width="100%">
                                    <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                    <asp:ListItem>FUNCIONA</asp:ListItem>
                                    <asp:ListItem Value="NO_FUNCIONA">NO FUNCIONA</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td style="text-align: left; vertical-align: top; width: 18%">
                                <asp:Label ID="Lbl_Observaciones" runat="server" Text="Observaciones"></asp:Label>
                            </td>
                            <td colspan="3" align="left">
                                <asp:TextBox ID="Txt_Observaciones" runat="server" Width="98%" Enabled="False" Rows="3"
                                    TextMode="MultiLine"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                    ValidChars="��.,:;()���������� /*-_+$%&!#$%&�?�[]{}" Enabled="True">
                                </cc1:FilteredTextBoxExtender>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Observaciones" runat="server" Enabled="True"
                                    TargetControlID="Txt_Observaciones" WatermarkCssClass="watermarked" WatermarkText="L�mite de Caracteres 255">
                                </cc1:TextBoxWatermarkExtender>
                            </td>
                        </tr>
                    </table>
                    <asp:UpdatePanel ID="Upd_Archivo" runat="server" UpdateMode="Conditional">
                        <ContentTemplate>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width: 18%;">
                                        <asp:Label ID="Lbl_Seleccionar_Archivo" runat="server" Text="Archivo"></asp:Label>
                                    </td>
                                    <td style="width: 82%;">
                                        <cc1:AsyncFileUpload ID="AFU_Archivo" runat="server" Width="650px" CompleteBackColor="LightBlue"
                                            ErrorBackColor="Red" ThrobberID="Throbber" UploadingBackColor="LightGray" Enabled="true"
                                            OnClientUploadComplete="Carga_Completa" OnUploadedComplete="Asy_Cargar_Archivo_Complete" />
                                    </td>
                                </tr>
                            </table>
                            <asp:Label ID="Throbber" runat="server" Text="">
                                <div id="Div1" class="progressBackgroundFilter">
                                </div>
                                <div class="processMessage" id="div2">
                                    <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                </div>
                            </asp:Label>
                        </ContentTemplate>
                        <Triggers>
                            <asp:PostBackTrigger ControlID="Btn_Agregar_Archivo" />
                        </Triggers>
                    </asp:UpdatePanel>
                    <asp:UpdatePanel ID="Upd_Mostrar_Archivos" runat="server" UpdateMode="Always">
                        <ContentTemplate>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td align="right">
                                        <asp:ImageButton ID="Btn_Agregar_Archivo" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                            Width="18px" Height="18px" CssClass="Img_Button" AlternateText="Agregar" ToolTip="Agregar Archivo"
                                            OnClick="Btn_Agregar_Archivo_Click" />
                                        <asp:ImageButton ID="Btn_Limpiar_FileUpload" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png"
                                            Height="16px" Width="16px" CssClass="Img_Button" AlternateText="Limpiar Archivo"
                                            OnClick="Btn_Limpiar_FileUpload_Click" ToolTip="Liberar Archivo" />
                                    </td>
                                </tr>
                            </table>
                            <asp:UpdateProgress ID="Upd_Grid_Archivos" runat="server" AssociatedUpdatePanelID="Upd_Archivo"
                                DisplayAfter="0">
                                <ProgressTemplate>
                                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                    </div>
                                    <div class="processMessage" id="div_progress">
                                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                                    </div>
                                </ProgressTemplate>
                            </asp:UpdateProgress>
                            <asp:GridView ID="Grid_Archivos" runat="server" AutoGenerateColumns="False" CellPadding="4"
                                DataKeyNames="ARCHIVO_BIEN_ID" ForeColor="#333333" GridLines="None" AllowPaging="True"
                                Width="99%" OnPageIndexChanging="Grid_Archivos_PageIndexChanging">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="ARCHIVO_BIEN_ID" HeaderText="ARCHIVO_BIEN_ID" SortExpression="ARCHIVO_BIEN_ID" />
                                    <asp:BoundField DataField="BIEN_ID" HeaderText="BIEN_ID" SortExpression="BIEN_ID" />
                                    <asp:BoundField DataField="FECHA" HeaderText="Fecha" SortExpression="FECHA" DataFormatString="{0:dd/MMM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="ARCHIVO" HeaderText="Nombre" SortExpression="ARCHIVO"
                                        NullDisplayText="-">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" Width="90%" />
                                    </asp:BoundField>
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                            </asp:GridView>
                        </ContentTemplate>
                    </asp:UpdatePanel>
                </center>
            </div>
            <%-- Termina Div Detalles--%>
            <br />
            <div id="Div_Firmas" runat="server" style="width: 98%">
                <table width="100%" class="estilo_fuente">
                    <tr>
                        <td>
                            <div runat="server" id="Div3">
                                <table width="100%" class="estilo_fuente">
                                    <tr>
                                        <td class="label_titulo" colspan="4">
                                            Firmas
                                        </td>
                                    </tr>
                                    <tr align="right" class="barra_delgada">
                                        <td colspan="4" align="center">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Autorizo" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Autorizo" runat="server" Text="Autoriz�"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Autorizo" runat="server" Width="95%" 
                                                Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Autorizo" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Autorizo_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Operador" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Operador" runat="server" Text="Entreg�"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Operador" runat="server" Width="95%" 
                                                Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Operador" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Operador_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 18%;">
                                            <asp:HiddenField ID="Hdf_Resguardo_Completo_Funcionario_Recibe" runat="server" />
                                            <asp:Label ID="Lbl_Resguardo_Completo_Funcionario_Recibe" runat="server" Text="Revis�"></asp:Label>
                                        </td>
                                        <td style="width: 82%;">
                                            <asp:TextBox ID="Txt_Resguardo_Completo_Funcionario_Recibe" runat="server" Width="95%"
                                                Enabled="False"></asp:TextBox>
                                            <asp:ImageButton ID="Btn_Resguardo_Completo_Funcionario_Recibe" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                                Width="16px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Resguardo_Completo_Funcionario_Recibe_Click" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Resguardos" runat="server" style="width: 98%">
                <%-- Div Resguardos--%>
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td class="label_titulo" colspan="4">
                            Resguardatarios
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="4" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 13%; text-align: left;">
                            <asp:Label ID="Lbl_Resguardantes" runat="server" Text="Empleados"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align: left;">
                            <asp:DropDownList ID="Cmb_Empleados" runat="server" Width="100%">
                                <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 13%; text-align: left; vertical-align: top;">
                            <asp:Label ID="Lbl_Comentarios" runat="server" Text="Comentarios"></asp:Label>
                        </td>
                        <td colspan="3" style="text-align: left;">
                            <asp:TextBox ID="Txt_Cometarios" runat="server" TextMode="MultiLine" Rows="3" Width="99%"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Cometarios" runat="server" TargetControlID="Txt_Cometarios"
                                WatermarkText="L�mite de Caracteres 150" WatermarkCssClass="watermarked" Enabled="True" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left; vertical-align: top;">
                            &nbsp;&nbsp;&nbsp;
                            <asp:ImageButton ID="Btn_Busqueda_Avanzada_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png"
                                Width="24px" ToolTip="Buscar" AlternateText="Buscar" OnClick="Btn_Busqueda_Avanzada_Resguardante_Click" />
                        </td>
                        <td style="width: 85%; text-align: right;">
                            <asp:ImageButton ID="Btn_Agregar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_add.png"
                                ToolTip="Agregar" AlternateText="Agregar" OnClick="Btn_Agregar_Resguardante_Click" />
                            <asp:ImageButton ID="Btn_Quitar_Resguardante" runat="server" ImageUrl="~/paginas/imagenes/paginas/quitar.png"
                                ToolTip="Quitar" AlternateText="Quitar" OnClick="Btn_Quitar_Resguardante_Click" />
                            &nbsp;&nbsp;&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <hr />
                        </td>
                    </tr>
                </table>
                <br />
                <asp:GridView ID="Grid_Resguardantes" runat="server" AutoGenerateColumns="False"
                    CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="false" Width="98%"
                    OnRowDataBound="Grid_Resguardantes_RowDataBound">
                    <RowStyle CssClass="GridItem" />
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="30px" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="BIEN_RESGUARDO_ID" HeaderText="BIEN_RESGUARDO_ID" SortExpression="BIEN_RESGUARDO_ID">
                            <ItemStyle Width="110px" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Right" Width="40px" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle Width="100px" HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:BoundField DataField="NOMBRE_EMPLEADO" HeaderText="Nombre del Empleado" SortExpression="NOMBRE_EMPLEADO">
                            <HeaderStyle HorizontalAlign="Center" />
                            <ItemStyle HorizontalAlign="Left" Font-Size="X-Small" />
                        </asp:BoundField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:ImageButton ID="Btn_Ver_Informacion_Resguardo" runat="server" Width="16px" ImageUrl="~/paginas/imagenes/gridview/grid_info.png"
                                    ToolTip="Comentarios del Resguardo" OnClick="Btn_Ver_Informacion_Resguardo_Click" />
                            </ItemTemplate>
                            <ItemStyle Width="50px" />
                        </asp:TemplateField>
                    </Columns>
                    <PagerStyle CssClass="GridHeader" />
                    <SelectedRowStyle CssClass="GridSelected" />
                    <HeaderStyle CssClass="GridHeader" />
                    <AlternatingRowStyle CssClass="GridAltItem" />
                </asp:GridView>
            </div>
            <br />
            <br />
            <br />
            <script type="text/javascript" language="javascript">
                //registra los eventos para la p�gina
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                        Auto_Completado_Cuentas_Gasto();
                        Auto_Completado_Productos();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Format_Producto(item) {
                        return item.clave_nombre;
                    }
                    function Auto_Completado_Productos() {
                        $("[id$='Txt_Nombre_Producto_Donado']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_busqueda_producto_vehiculo' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.producto_id,
                                        result: row.nombre

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Producto_ID']").val("");
                                return Format_Producto(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Producto_ID']").val(item.producto_id);
                        });
                        $("[id$='Txt_Nombre_Producto_Donado']").live({ 'blur': function() {
                        if($("[id$='Hdf_Producto_ID']").val().length==0)
                            $("[id$='Txt_Nombre_Producto_Donado']").val("");
                        }
                        });
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Contable']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Contable']").live({ 'blur': function() {
                        if ($("[id$='Hdf_Cuenta_Contable_ID']").val().length == 0)
                            $("[id$='Txt_Cuenta_Contable']").val("");
                        }
                        });
                    }
                    function Auto_Completado_Cuentas_Gasto() {
                        $("[id$='Txt_Cuenta_Gasto']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Gasto']").live({ 'blur': function() {
                        if ($("[id$='Hdf_Cuenta_Gasto_ID']").val().length == 0)
                            $("[id$='Txt_Cuenta_Gasto']").val("");
                        }
                        });
                    }
                } 
            </script>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_aux_Busqueda_Resguardante" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_MPE_Resguardante" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Resguardante" runat="server" TargetControlID="Btn_Comodin_MPE_Resguardante"
                PopupControlID="Pnl_Busqueda_Contenedor" CancelControlID="Btn_Cerrar_Ventana"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:UpdatePanel runat="server" ID="UpPnl_Aux_MPE_Donadores" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin" runat="server" Text="Button" Style="display: none;" />
            <cc1:ModalPopupExtender ID="MPE_Donadores" runat="server" TargetControlID="Btn_Comodin"
                PopupControlID="Pnl_Donadores" CancelControlID="Btn_Cerrar_Ventana_Cancelacion"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Donadores" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Cabecera" runat="server" CssClass="estilo_fuente" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Image1" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Introducir los datos del Donador
                    </td>
                    <td align="right" colspan="3">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana_Cancelacion" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="Upl_Pnl_Donadores" runat="server">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPrgr_Donadores" runat="server" AssociatedUpdatePanelID="Upl_Pnl_Donadores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <table width="100%">
                            <tr>
                                <td colspan="2">
                                    <table width="99%">
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Nombre_Donador_MPE" runat="server" Text="* Nombre" CssClass="estilo_fuente"
                                                    Style="font-weight: bolder;"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 82%; text-align: left;">
                                                <asp:TextBox ID="Txt_Nombre_Donador_MPE" runat="server" Width="99%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Donador_MPE" runat="server" TargetControlID="Txt_Nombre_Donador_MPE"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Apellido_Paterno_Donador" runat="server" Text="A. Paterno" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Apellido_Paterno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Paterno_Donador" runat="server"
                                                    TargetControlID="Txt_Apellido_Paterno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Apellido_Materno_Donador" runat="server" Text="A. Materno" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Apellido_Materno_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Apellido_Materno_Donador" runat="server"
                                                    TargetControlID="Txt_Apellido_Materno_Donador" InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Direccion_Donador" runat="server" Text="Direcci�n" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td colspan="3" style="width: 82%; text-align: left;">
                                                <asp:TextBox ID="Txt_Direccion_Donador" runat="server" Width="99%" MaxLength="150"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Direccion_Donador" runat="server" TargetControlID="Txt_Direccion_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Ciudad_Donador" runat="server" Text="Ciudad" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Ciudad_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Ciudad_Donador" runat="server" TargetControlID="Txt_Ciudad_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Estado_Donador" runat="server" Text="Estado" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Estado_Donador" runat="server" Width="98%" MaxLength="50"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Estado_Donador" runat="server" TargetControlID="Txt_Estado_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"
                                                    ValidChars="��.,:;()���������� ">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Telefono_Donador" runat="server" Text="Telefono" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Telefono_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Telefono_Donador" runat="server" TargetControlID="Txt_Telefono_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_Celular_Donador" runat="server" Text="Celular" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_Celular_Donador" runat="server" Width="98%" MaxLength="20"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Celular_Donador" runat="server" TargetControlID="Txt_Celular_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers" ValidChars="()-">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_CURP_Donador" runat="server" Text="CURP" CssClass="estilo_fuente"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_CURP_Donador" runat="server" Width="98%" MaxLength="18"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_CURP_Donador" runat="server" TargetControlID="Txt_CURP_Donador"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="��">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                            <td style="width: 20%; text-align: left;">
                                                <asp:Label ID="Lbl_RFC_Donador_MPE" runat="server" Text="* RFC" CssClass="estilo_fuente"
                                                    Style="font-weight: bolder;"></asp:Label>
                                            </td>
                                            <td style="width: 30%; text-align: left;">
                                                <asp:TextBox ID="Txt_RFC_Donador_MPE" runat="server" Width="98%" MaxLength="15"></asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_RFC_Donador_MPE" runat="server" TargetControlID="Txt_RFC_Donador_MPE"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, UppercaseLetters, Numbers" ValidChars="��">
                                                </cc1:FilteredTextBoxExtender>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 20%; text-align: left;">
                                    &nbsp;
                                </td>
                                <td style="width: 80%; text-align: right;">
                                    <asp:ImageButton ID="Btn_MPE_Donador_Aceptar" runat="server" ImageUrl="~/paginas/imagenes/paginas/accept.png"
                                        CausesValidation="False" ToolTip="Aceptar" OnClick="Btn_MPE_Donador_Aceptar_Click" />
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div id="Div_MPE_Donadores_Mensaje_Error" style="width: 98%;" runat="server" visible="false">
                                        <table style="width: 100%;">
                                            <tr>
                                                <td colspan="2" align="left">
                                                    <asp:ImageButton ID="Btn_MPE_Donadores_Mensaje_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                        Enabled="false" Width="24px" Height="24px" />
                                                    <asp:Label ID="Lbl_MPE_Donadores_Cabecera_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 10%;">
                                                </td>
                                                <td style="width: 90%; text-align: left;" valign="top">
                                                    <asp:Label ID="Lbl_MPE_Donadores_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                                </td>
                                            </tr>
                                        </table>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="Btn_Agregar_Donador" EventName="Click" />
                </Triggers>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>    
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center"
        Width="850px" Style="display: none; border-style: outset; border-color: Silver;
        background-image: url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG); background-repeat: repeat-y;">
        <asp:Panel ID="Pnl_Busqueda_Resguardante_Cabecera" runat="server" Style="cursor: move;
            background-color: Silver; color: Black; font-size: 12; font-weight: bold; border-style: outset;">
            <table width="99%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold;">
                        <asp:Image ID="Img_Informatcion_Autorizacion" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        B&uacute;squeda: Empleados
                    </td>
                    <td align="right" style="width: 10%;">
                        <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;">
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Prestamos" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Prestamos" runat="server"
                                    AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Prestamos" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                                        </div>
                                        <div style="background-color: Transparent; position: fixed; top: 50%; left: 47%;
                                            padding: 10px; z-index: 1002;" id="div_progress">
                                            <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <asp:Panel ID="Pnl_Panel_Filtro_Busqueda_Empleados" runat="server" Width="100%" DefaultButton="Btn_Busqueda_Empleados">
                                    <table width="100%">
                                        <tr>
                                            <td style="width: 100%" colspan="4" align="right">
                                                <asp:ImageButton ID="Btn_Limpiar_Ctlr_Busqueda" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                    ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                No Empleado
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_No_Empleado" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_No_Empleado" runat="server" FilterType="Numbers"
                                                    TargetControlID="Txt_Busqueda_No_Empleado" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_No_Empleado" runat="server" TargetControlID="Txt_Busqueda_No_Empleado"
                                                    WatermarkText="Busqueda por No Empleado" WatermarkCssClass="watermarked" />
                                            </td>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                RFC
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;">
                                                <asp:TextBox ID="Txt_Busqueda_RFC" runat="server" Width="98%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_RFC" runat="server" FilterType="Numbers, UppercaseLetters"
                                                    TargetControlID="Txt_Busqueda_RFC" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Txt_Busqueda_RFC" runat="server" TargetControlID="Txt_Busqueda_RFC"
                                                    WatermarkText="Busqueda por RFC" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Nombre
                                            </td>
                                            <td style="width: 30%; text-align: left;" colspan="3">
                                                <asp:TextBox ID="Txt_Busqueda_Nombre_Empleado" runat="server" Width="99.5%" />
                                                <cc1:FilteredTextBoxExtender ID="Fte_Txt_Busqueda_Nombre_Empleado" runat="server"
                                                    FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    ValidChars="���������� .-" />
                                                <cc1:TextBoxWatermarkExtender ID="Twm_Nombre_Empleado" runat="server" TargetControlID="Txt_Busqueda_Nombre_Empleado"
                                                    WatermarkText="Busqueda por Nombre" WatermarkCssClass="watermarked" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 20%; text-align: left; font-size: 11px;">
                                                Unidad Responsable
                                            </td>
                                            <td style="width: 30%; text-align: left; font-size: 11px;" colspan="3">
                                                <asp:DropDownList ID="Cmb_Busqueda_Dependencia" runat="server" Width="100%" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%" colspan="4">
                                                <hr />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width: 100%; text-align: left;" colspan="4">
                                                <center>
                                                    <asp:Button ID="Btn_Busqueda_Empleados" runat="server" Text="Busqueda de Empleados"
                                                        CssClass="button" CausesValidation="false" Width="200px" OnClick="Btn_Busqueda_Empleados_Click" />
                                                </center>
                                            </td>
                                        </tr>
                                    </table>
                                </asp:Panel>
                                <br />
                                <div id="Div_Resultados_Busqueda_Resguardantes" runat="server" style="border-style: outset;
                                    width: 99%; height: 250px; overflow: auto;">
                                    <asp:GridView ID="Grid_Busqueda_Empleados_Resguardo" runat="server" AutoGenerateColumns="False"
                                        CellPadding="4" ForeColor="#333333" GridLines="None" AllowPaging="True" Width="100%"
                                        PageSize="100" EmptyDataText="No se encontrar�n resultados para los filtros de la busqueda"
                                        OnSelectedIndexChanged="Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged"
                                        OnPageIndexChanging="Grid_Busqueda_Empleados_Resguardo_PageIndexChanging">
                                        <RowStyle CssClass="GridItem" />
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                <ItemStyle Width="30px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:ButtonField>
                                            <asp:BoundField DataField="EMPLEADO_ID" HeaderText="EMPLEADO_ID" SortExpression="EMPLEADO_ID">
                                                <ItemStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="3px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NO_EMPLEADO" HeaderText="No. Empleado" SortExpression="NO_EMPLEADO">
                                                <ItemStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                <HeaderStyle Width="70px" Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE" NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DEPENDENCIA" HeaderText="Unidad Responsable" SortExpression="DEPENDENCIA"
                                                NullDisplayText="-">
                                                <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                <HeaderStyle Font-Size="X-Small" HorizontalAlign="Center" />
                                            </asp:BoundField>
                                        </Columns>
                                        <PagerStyle CssClass="GridHeader" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>
    <asp:UpdatePanel ID="UpPnl_Aux_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Mpe_Proveedores" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Proveedores_Cabecera" runat="server" TargetControlID="Btn_Comodin_Mpe_Proveedores"
                PopupControlID="Pnl_Mpe_Proveedores" CancelControlID="Btn_Cerrar_Mpe_Proveedores"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Proveedores" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Proveedores_Interno" runat="server" CssClass="estilo_fuente"
            Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
            border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Img_Mpe_Proveedores" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Busqueda y Selecci�n de Proveedores
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Proveedores" CausesValidation="false" runat="server"
                            Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Proveedores" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Proveedores" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Proveedores"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <br />
                    <div style="border-style: outset; width: 95%; height: 380px; background-color: White;">
                        <asp:Panel ID="Pnl_Buscar_Proveedor" runat="server" Width="98%" DefaultButton="Btn_Ejecutar_Busqueda_Proveedores">
                            <table width="100%">
                                <tr>
                                    <td style="width: 15%; text-align: left;">
                                        <asp:Label ID="Lbl_Nombre_Proveedores_Buscar" runat="server" CssClass="estilo_fuente"
                                            Text="Nombre" Style="font-weight: bolder;"></asp:Label>
                                    </td>
                                    <td style="width: 85%; text-align: left;">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Buscar" runat="server" Width="92%" ></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            TargetControlID="Txt_Nombre_Proveedor_Buscar" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                            ValidChars="��.,:;()���������� " Enabled="True">
                                        </cc1:FilteredTextBoxExtender>
                                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Nombre_Proveedor_Buscar" runat="server"
                                            Enabled="True" TargetControlID="Txt_Nombre_Proveedor_Buscar" WatermarkCssClass="watermarked"
                                            WatermarkText="<-- Raz�n Social � Nombre Comercial -->">
                                        </cc1:TextBoxWatermarkExtender>
                                        <asp:ImageButton ID="Btn_Ejecutar_Busqueda_Proveedores" runat="server" OnClick="Btn_Ejecutar_Busqueda_Proveedores_Click"
                                            ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Buscar Productos"
                                            AlternateText="Buscar" />
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                            
                        <br />
                        <asp:Panel ID="Pnl_Listado_Proveedores" runat="server" ScrollBars="Vertical" Style="white-space: normal;"
                            Width="100%" BorderColor="#3366FF" Height="330px">
                            <asp:GridView ID="Grid_Listado_Proveedores" runat="server" AllowPaging="true" AutoGenerateColumns="False"
                                CellPadding="4" ForeColor="#333333" GridLines="None" OnPageIndexChanging="Grid_Listado_Proveedores_PageIndexChanging"
                                OnSelectedIndexChanged="Grid_Listado_Proveedores_SelectedIndexChanged" PageSize="150"
                                Width="100%" CssClass="GridView_1">
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <Columns>
                                    <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                        <ItemStyle Width="30px" />
                                    </asp:ButtonField>
                                    <asp:BoundField DataField="PROVEEDOR_ID" HeaderText="Proveedor ID" SortExpression="PROVEEDOR_ID">
                                        <ItemStyle Width="30px" Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="RFC" HeaderText="RFC" SortExpression="RFC">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="NOMBRE" HeaderText="Raz�n Social" SortExpression="NOMBRE">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="COMPANIA" HeaderText="Nombre Comercial" SortExpression="COMPANIA">
                                        <ItemStyle Font-Size="X-Small" />
                                    </asp:BoundField>
                                </Columns>
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridHeader" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                        </asp:Panel>
                        <br />
                    </div>
                    <br />
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
