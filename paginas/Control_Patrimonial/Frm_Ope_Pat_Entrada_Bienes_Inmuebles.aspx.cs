﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using JAPAMI.Control_Patrimonial_Catalogo_Usos_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Destinos_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Inmuebles.Negocio;
using JAPAMI.Catalogo_Tipos_Predio.Negocio;
using JAPAMI.Catalogo_Calles.Negocio;
using JAPAMI.Catalogo_Cuentas_Predial.Negocio;
using JAPAMI.Colonias.Negocios;
using JAPAMI.Control_Patrimonial_Reporte_Listado_Bienes.Negocio;

public partial class paginas_Control_Patrimonial_Frm_Ope_Pat_Entrada_Bienes_Inmuebles : System.Web.UI.Page {

    #region "Page Load"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Page_Load
        ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Page_Load(object sender, EventArgs e) {
            Lbl_Ecabezado_Mensaje.Text = "";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Trim().Length == 0) {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            }
            if (!IsPostBack) {
                Session.Remove("FILTRO_BUSQUEDA_INMUEBLES");
                Llenar_Combo_Destino();
                Llenar_Combo_Uso();
                Llenar_Combo_Distritos();
                Grid_Listado_Busqueda_Bienes_Inmuebles.PageIndex = 0;
                Llenar_Listado_Busqueda_Bienes_Inmuebles();
            }
        }

    #endregion

    #region "Metodos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Uso
        ///DESCRIPCIÓN: Llenar_Combo_Uso
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Combo_Uso() {
            Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio Uso_Suelo = new Cls_Cat_Pat_Com_Usos_Inmuebles_Negocio();
            Uso_Suelo.P_Estatus = "VIGENTE";
            Cmb_Uso.DataSource = Uso_Suelo.Consultar_Usos();
            Cmb_Uso.DataTextField = "DESCRIPCION";
            Cmb_Uso.DataValueField = "USO_ID";
            Cmb_Uso.DataBind();
            Cmb_Uso.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Distritos
        ///DESCRIPCIÓN: Llena el Combo de los Distritos
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda
        ///FECHA_CREO: Marzo/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************  
        private void Llenar_Combo_Distritos()
        {
            Cls_Rpt_Pat_Listado_Bienes_Negocio Rpt_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
            Rpt_Negocio.P_Estatus = "ACTIVO', 'ACTIVA";
            DataTable Dt_Distritos = Rpt_Negocio.Consultar_Distritos();
            Cmb_Distrito.DataSource = Dt_Distritos;
            Cmb_Distrito.DataTextField = "NOMBRE";
            Cmb_Distrito.DataValueField = "DISTRITO_ID";
            Cmb_Distrito.DataBind();
            Cmb_Distrito.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Destino
        ///DESCRIPCIÓN:Llenar_Combo_Destino
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Combo_Destino() {
            Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio Destino_Suelo = new Cls_Cat_Pat_Com_Destinos_Inmuebles_Negocio();
            Destino_Suelo.P_Estatus = "VIGENTE";
            Cmb_Destino.DataSource = Destino_Suelo.Consultar_Destinos();
            Cmb_Destino.DataTextField = "DESCRIPCION";
            Cmb_Destino.DataValueField = "DESTINO_ID";
            Cmb_Destino.DataBind();
            Cmb_Destino.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Llenar_Listado_Busqueda_Bienes_Inmuebles
        ///DESCRIPCIÓN: Llenar_Listado_Busqueda_Bienes_Inmuebles
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        private void Llenar_Listado_Busqueda_Bienes_Inmuebles() {
            Grid_Listado_Busqueda_Bienes_Inmuebles.SelectedIndex = (-1);
            Cls_Ope_Pat_Bienes_Inmuebles_Negocio Tmp_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
            Tmp_Negocio.P_Calle = Txt_Calle.Text.Trim();
            Tmp_Negocio.P_Colonia = Txt_Colonia.Text.Trim();
            Tmp_Negocio.P_Uso_ID = Cmb_Uso.SelectedItem.Value; 
            Tmp_Negocio.P_Destino_ID = Cmb_Destino.SelectedItem.Value;
            Tmp_Negocio.P_Distrito_ID = Cmb_Distrito.SelectedItem.Value;
            Tmp_Negocio.P_Cuenta_Predial_ID = Txt_Numero_Cuenta_Predial.Text.Trim();
            Tmp_Negocio.P_No_Escritura = Txt_Escritura.Text.Trim();
            Tmp_Negocio.P_Bien_Inmueble_ID = (Txt_Bien_Mueble_ID.Text.Trim().Length > 0) ? String.Format("{0:0000000000}", Convert.ToInt32(Txt_Bien_Mueble_ID.Text.Trim())) : "";
            if (Txt_Superficie_Desde.Text.Trim().Length > 0) { Tmp_Negocio.P_Construccion_Desde = Convert.ToDouble(Txt_Superficie_Desde.Text); }
            if (Txt_Superficie_Hasta.Text.Trim().Length > 0) { Tmp_Negocio.P_Construccion_Hasta = Convert.ToDouble(Txt_Superficie_Hasta.Text); }
            DataTable Dt_Datos = Tmp_Negocio.Consultar_Bienes_Inmuebles();
            Grid_Listado_Busqueda_Bienes_Inmuebles.DataSource = Dt_Datos;
            Grid_Listado_Busqueda_Bienes_Inmuebles.DataBind();
        }

#endregion

    #region "Grids"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Bienes_Inmuebles_PageIndexChanging
        ///DESCRIPCIÓN: Grid_Listado_Busqueda_Bienes_Inmuebles_PageIndexChanging
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Grid_Listado_Busqueda_Bienes_Inmuebles_PageIndexChanging(object sender, GridViewPageEventArgs e) {
            Grid_Listado_Busqueda_Bienes_Inmuebles.PageIndex = e.NewPageIndex;
            Llenar_Listado_Busqueda_Bienes_Inmuebles();
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Busqueda_Bienes_Inmuebles_SelectedIndexChanged
        ///DESCRIPCIÓN: Grid_Listado_Busqueda_Bienes_Inmuebles_SelectedIndexChanged
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Grid_Listado_Busqueda_Bienes_Inmuebles_SelectedIndexChanged(object sender, EventArgs e) {
            if (Grid_Listado_Busqueda_Bienes_Inmuebles.SelectedIndex > (-1)) {
                String Bien_Inmueble_ID = HttpUtility.HtmlDecode(Grid_Listado_Busqueda_Bienes_Inmuebles.SelectedRow.Cells[1].Text.Trim());
                Session["Operacion_Inicial"] = "VER_BIEN_INMUEBLE";
                Session["Bien_Inmueble_ID"] = Bien_Inmueble_ID;
                Response.Redirect("Frm_Ope_Pat_Bienes_Inmuebles.aspx");
            }
        }

    #endregion

    #region "Eventos"

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
        ///DESCRIPCIÓN: Btn_Nuevo_Click
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e) {
            Session["Operacion_Inicial"] = "NUEVO";
            Response.Redirect("Frm_Ope_Pat_Bienes_Inmuebles.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
        ///DESCRIPCIÓN: Btn_Salir_Click
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e) {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }

        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_General_Click
        ///DESCRIPCIÓN: Btn_Ejecutar_Busqueda_General_Click
        ///PROPIEDADES:     
        ///CREO: Francisco Antonio Gallardo Castañeda.
        ///FECHA_CREO: 2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///******************************************************************************* 
        protected void Btn_Ejecutar_Busqueda_General_Click(object sender, ImageClickEventArgs e) {
            Grid_Listado_Busqueda_Bienes_Inmuebles.PageIndex = 0;
            Llenar_Listado_Busqueda_Bienes_Inmuebles();
        }


    #endregion

}
