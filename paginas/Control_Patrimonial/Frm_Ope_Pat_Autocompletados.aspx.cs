﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using JAPAMI.Ayudante_JQuery;
using JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Negocio;
using JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Datos;

public partial class paginas_Control_Patrimonial_Frm_Ope_Pat_Autocompletados : System.Web.UI.Page{

    protected void Page_Load(object sender, EventArgs e)
    {
        this.controlador(this.Response, this.Request);
    }

    public void controlador(HttpResponse response, HttpRequest request) {

        String accion = "";
        String strdatos = "";

        accion = Cls_Ope_Pat_Autocompletados_Negocio.accion;
        
        switch (accion)
        {
            case "autocompletado_cuenta_contable":
                strdatos = Obtener_Cuentas_Contables();
                break;
            case "autocompletado_busqueda_producto_bien_mueble":
                strdatos = Obtener_Productos("BIEN_MUEBLE','PRODUCTO");
                break;
            case "autocompletado_busqueda_producto_vehiculo":
                strdatos = Obtener_Productos("VEHICULO");
                break;          
        }

        response.Clear();
        response.ContentType = "application/json";
        response.Flush();
        response.Write(strdatos);
        response.End();

    }

    ///*******************************************************************************************************
    ///NOMBRE_FUNCIÓN: Obtene00000000r_Cuentas_Contables
    ///DESCRIPCIÓN: Obtener_Cuentas_Contables
    ///PARÁMETROS: 
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 17/06/2013
    ///MODIFICÓ:
    ///FECHA_MODIFICÓ:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************************************
    public string Obtener_Cuentas_Contables()
    {

        String Str_Datos = "{[]}";
        DataTable Dt_Consulta;
        String Letra;

        try
        {
            Letra = Cls_Ope_Pat_Autocompletados_Negocio.Obtener_Letra;
            Dt_Consulta = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables(Letra, "");
            Str_Datos = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Consulta);
        }
        catch (Exception ex)
        {
            Str_Datos = "{[]}";
        }
        return Str_Datos;
    }

    ///*******************************************************************************************************
    ///NOMBRE_FUNCIÓN: Obtener_Productos
    ///DESCRIPCIÓN: Obtener_Productos
    ///PARÁMETROS: 
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 17/06/2013
    ///MODIFICÓ:
    ///FECHA_MODIFICÓ:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************************************
    public string Obtener_Productos(String Tipo_Articulo)
    {

        String Str_Datos = "{[]}";
        DataTable Dt_Consulta;
        String Letra;

        try
        {
            Letra = Cls_Ope_Pat_Autocompletados_Negocio.Obtener_Letra;
            Dt_Consulta = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Productos(Letra, "", Tipo_Articulo);
            Str_Datos = Ayudante_JQuery.dataTableToJSONsintotalrenglones(Dt_Consulta);
        }
        catch (Exception ex)
        {
            Str_Datos = "{[]}";
        }
        return Str_Datos;
    }

}
