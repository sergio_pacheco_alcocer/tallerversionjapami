﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Pat_Migrar_Informacion.aspx.cs" Inherits="paginas_Control_Patrimonial_Frm_Pat_Migrar_Informacion" Title="Migrar" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
  <cc1:ToolkitScriptManager ID="ScptM_Bienes_Muebles" runat="server"  EnableScriptGlobalization ="true" EnableScriptLocalization = "True" />
 <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
        
        <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Zonas" runat="server" Text="Migra zonas"
                                     onclick="Btn_Migrar_Zonas_Click" />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Colores" runat="server" Text="Migra Colores" onclick="Btn_Migrar_Colores_Click"
                                      />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Procedencias" runat="server" Text="Migra Procedencias" onclick="Btn_Migrar_Procedencias_Click"
                                      />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Combustibles" runat="server" Text="Migra Combustibles" onclick="Btn_Migrar_Combustibles_Click"
                                      />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Tipos_Vehiculos" runat="server" Text="Migra Tipos Vehiculos" onclick="Btn_Migrar_Tipos_Vehiculos_Click"
                                      />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Tipos_Activos" runat="server" 
                            Text="Migra Tipos Activos" onclick="Btn_Migrar_Tipos_Activos_Click" 
                                      />
                        </td>
                    </tr>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Migrar_Clases_Activo" runat="server" 
                            Text="Migra Clases Activos" onclick="Btn_Migrar_Clases_Activo_Click" 
                                      />
                        </td>
                    </tr>
                    <hr/>
                    <tr align="center">
                        <td class="label_titulo">
                        <asp:Button ID="Btn_Actualizar_Clases_Activo" runat="server" Text="Actualizar Clases Activos" onclick="Btn_Actualizar_Clases_Activo_Click" />
                        </td>
                    </tr>
                    </table>
                    </div>
              
         </ContentTemplate>
         </asp:UpdatePanel>

</asp:Content>