﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Muebles.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Vehiculos.Negocio;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using System.IO;
using AjaxControlToolkit;
using JAPAMI.Almacen_Resguardos.Negocio;
using JAPAMI.Reportes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.ReportSource;
using System.Collections.Generic;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Procedencias.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Catalogo_Compras_Marcas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Vehiculo.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Colores.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Tipos_Bajas.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clasificaciones.Negocio;
using JAPAMI.Control_Patrimonial_Catalogo_Clases_Activos.Negocio;
using System.Security.Cryptography;
using JAPAMI.Control_Patrimonial.Manejo_Historial_Cambios.Negocio;
using JAPAMI.Control_Patrimonial_Operacion_Bienes_Inmuebles.Negocio;
using JAPAMI.Control_Patrimonial_Parametros.Autocompletados.Datos;
using JAPAMI.Control_Patrimonial_Reporte_Listado_Bienes.Negocio;
using System.Text;

public partial class paginas_Compras_Frm_Ope_Pat_Com_Actualizacion_Bienes_Muebles : System.Web.UI.Page
{

    #region Variables Internas

    Cls_Alm_Com_Resguardos_Negocio Consulta_Resguardos_Negocio = new Cls_Alm_Com_Resguardos_Negocio();
    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combo = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
    private static AsyncFileUpload Archivo;

    #endregion

    #region Page_Load

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Metodo que se carga cada que ocurre un PostBack de la Página
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        Lbl_Ecabezado_Mensaje.Text = "";
        Lbl_Mensaje_Error.Text = "";
        Div_Contenedor_Msj_Error.Visible = false;
        if (!IsPostBack)
        {
            Llenar_Grid_Proveedores(0);
            Llenar_Combos();
            Llenar_Combos_MPE_Busquedas();
            Llenar_Combo_Procedencias();
            Llenar_Combo_Tipos_Bajas();
            Grid_Listado_Bienes.Columns[1].Visible = false;
            Llenar_Combo_Bienes_Inmuebles();
            Configuracion_Formulario(false);
            Div_Producto_Bien_Mueble_Padre.Visible = false;
            Div_Vehiculo_Parent.Visible = false;
        }
    }

    #endregion

    #region Metodos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Dependencias
    ///DESCRIPCIÓN: Llena el combo dependencias de acuerdo a la gerencia que se le proporciona
    ///PROPIEDADES: el identificador de la gerencia para llenar el combo dependencias
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Dependencias(String Gerencia_ID)
    {
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combo.P_Gerencia_ID = Gerencia_ID;
        Combo.P_Tipo_DataTable = "DEPENDENCIAS";
        DataTable Dependencias = Combo.Consultar_DataTable();
        Cmb_Dependencias.DataSource = Dependencias.Copy();
        Cmb_Dependencias.DataValueField = "DEPENDENCIA_ID";
        Cmb_Dependencias.DataTextField = "NOMBRE";
        Cmb_Dependencias.DataBind();
        Cmb_Dependencias.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Tipos_Bajas
    ///DESCRIPCIÓN: Llenar_Combo_Tipos_Bajas
    ///PROPIEDADES: 
    ///CREO: 
    ///FECHA_CREO: 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Tipos_Bajas()
    {
        Cls_Cat_Pat_Com_Tipos_Bajas_Negocio Combos = new Cls_Cat_Pat_Com_Tipos_Bajas_Negocio();
        Combos.P_Tipo_DataTable = "TIPOS_BAJAS";
        Combos.P_Estatus = "VIGENTE";
        DataTable Bajas = Combos.Consultar_DataTable();
        Cmb_Tipo_Baja.DataSource = Bajas.Copy();
        Cmb_Tipo_Baja.DataValueField = "TIPO_BAJA_ID";
        Cmb_Tipo_Baja.DataTextField = "DESCRIPCION";
        Cmb_Tipo_Baja.DataBind();
        Cmb_Tipo_Baja.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combos
    ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combos()
    {
        try
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            //SE LLENA EL COMBO DE GERENCIAS
            Combos.P_Tipo_DataTable = "GERENCIAS";
            DataTable Gerencias = Combos.Consultar_DataTable();
            DataRow Fila_Gerencia = Gerencias.NewRow();
            Fila_Gerencia["GERENCIA_ID"] = "";
            Fila_Gerencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Gerencias.Rows.InsertAt(Fila_Gerencia, 0);
            Cmb_Gerencias.DataSource = Gerencias;
            Cmb_Gerencias.DataValueField = "GERENCIA_ID";
            Cmb_Gerencias.DataTextField = "NOMBRE";
            Cmb_Gerencias.DataBind();
            Gerencias.Rows.RemoveAt(0);

            //SE LLENA EL COMBO DE MATERIALES
            Combos.P_Tipo_DataTable = "MATERIALES";
            DataTable Materiales = Combos.Consultar_DataTable();
            Cmb_Materiales.DataSource = Materiales.Copy();
            Cmb_Materiales.DataValueField = "MATERIAL_ID";
            Cmb_Materiales.DataTextField = "DESCRIPCION";
            Cmb_Materiales.DataBind();
            Cmb_Materiales.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

            Cmb_Material_Parte.DataSource = Materiales.Copy();
            Cmb_Material_Parte.DataValueField = "MATERIAL_ID";
            Cmb_Material_Parte.DataTextField = "DESCRIPCION";
            Cmb_Material_Parte.DataBind();
            Cmb_Material_Parte.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

            Cmb_Material_Parent.DataSource = Materiales.Copy();
            Cmb_Material_Parent.DataValueField = "MATERIAL_ID";
            Cmb_Material_Parent.DataTextField = "DESCRIPCION";
            Cmb_Material_Parent.DataBind();
            Cmb_Material_Parent.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

            Combos.P_Tipo_DataTable = "COLORES";
            DataTable Colores = Combos.Consultar_DataTable();
            DataRow Fila_Color = Colores.NewRow();
            Fila_Color["COLOR_ID"] = "SELECCIONE";
            Fila_Color["DESCRIPCION"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Colores.Rows.InsertAt(Fila_Color, 0);
            Cmb_Colores.DataSource = Colores.Copy();
            Cmb_Colores.DataValueField = "COLOR_ID";
            Cmb_Colores.DataTextField = "DESCRIPCION";
            Cmb_Colores.DataBind();
            Cmb_Color_Parte.DataSource = Colores.Copy();
            Cmb_Color_Parte.DataValueField = "COLOR_ID";
            Cmb_Color_Parte.DataTextField = "DESCRIPCION";
            Cmb_Color_Parte.DataBind();
            Cmb_Color_Parent.DataSource = Colores.Copy();
            Cmb_Color_Parent.DataValueField = "COLOR_ID";
            Cmb_Color_Parent.DataTextField = "DESCRIPCION";
            Cmb_Color_Parent.DataBind();

            //SE LLENA EL COMBO DE MARCAS
            Combos.P_Tipo_DataTable = "MARCAS";
            DataTable Marcas = Combos.Consultar_DataTable();
            Cmb_Marca.DataSource = Marcas.Copy();
            Cmb_Marca.DataValueField = "MARCA_ID";
            Cmb_Marca.DataTextField = "NOMBRE";
            Cmb_Marca.DataBind();
            Cmb_Marca.Items.Insert(0, new ListItem("<SELECCIONE>", ""));
            Cmb_Marca_Parent.DataSource = Marcas.Copy();
            Cmb_Marca_Parent.DataValueField = "MARCA_ID";
            Cmb_Marca_Parent.DataTextField = "NOMBRE";
            Cmb_Marca_Parent.DataBind();
            Cmb_Marca_Parent.Items.Insert(0, new ListItem("<SELECCIONE>", ""));

            Combo.P_Tipo_DataTable = "ZONAS";
            DataTable Zonas = Combo.Consultar_DataTable();
            Cmb_Zonas.DataSource = Zonas;
            Cmb_Zonas.DataTextField = "DESCRIPCION";
            Cmb_Zonas.DataValueField = "ZONA_ID";
            Cmb_Zonas.DataBind();
            Cmb_Zonas.Items.Insert(0, new ListItem("<--SELECCIONE-->", ""));

            Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
            CA_Negocio.P_Estatus = "VIGENTE";
            CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
            CA_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Clase_Activo.DataSource = CA_Negocio.Consultar_DataTable();
            Cmb_Clase_Activo.DataValueField = "CLASE_ACTIVO_ID";
            Cmb_Clase_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Clase_Activo.DataBind();
            Cmb_Clase_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));

            Cls_Cat_Pat_Com_Clasificaciones_Negocio Clasificaciones_Negocio = new Cls_Cat_Pat_Com_Clasificaciones_Negocio();
            Clasificaciones_Negocio.P_Estatus = "VIGENTE";
            Clasificaciones_Negocio.P_Tipo_DataTable = "CLASIFICACIONES";
            Clasificaciones_Negocio.P_Orden = "DESCRIPCION";
            Cmb_Tipo_Activo.DataSource = Clasificaciones_Negocio.Consultar_DataTable();
            Cmb_Tipo_Activo.DataValueField = "CLASIFICACION_ID";
            Cmb_Tipo_Activo.DataTextField = "DESCRIPCION_CLAVE";
            Cmb_Tipo_Activo.DataBind();
            Cmb_Tipo_Activo.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
    ///DESCRIPCIÓN: Llena el combo de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados()
    {
        try
        {
            Cmb_Empleados.SelectedIndex = (-1);
            DataTable Tabla = new DataTable();
            if (Cmb_Dependencias.SelectedIndex > 0)
            {
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Empleados = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Empleados.P_Tipo_DataTable = "EMPLEADOS";
                Empleados.P_Dependencia_ID = Cmb_Dependencias.SelectedItem.Value;
                Tabla = Empleados.Consultar_DataTable();
            }
            Cmb_Empleados.DataSource = Tabla;
            Cmb_Empleados.DataValueField = "EMPLEADO_ID";
            Cmb_Empleados.DataTextField = "NOMBRE";
            Cmb_Empleados.DataBind();
            Cmb_Empleados.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados
    ///DESCRIPCIÓN: Llena el combo de Empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados(DataTable Tabla)
    {
        try
        {
            Cmb_Empleados.DataSource = Tabla;
            Cmb_Empleados.DataValueField = "EMPLEADO_ID";
            Cmb_Empleados.DataTextField = "NOMBRE";
            Cmb_Empleados.DataBind();
            Cmb_Empleados.Items.Insert(0, new ListItem("<- SELECCIONE ->", ""));
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Empleados_Busqueda
    ///DESCRIPCIÓN: Llena el combo de Empleados del Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Empleados_Busqueda(DataTable Tabla)
    {
        try
        {
            DataRow Fila_Empleado = Tabla.NewRow();
            Fila_Empleado["EMPLEADO_ID"] = "SELECCIONE";
            Fila_Empleado["NOMBRE"] = HttpUtility.HtmlDecode("&lt;SELECCIONE&gt;");
            Tabla.Rows.InsertAt(Fila_Empleado, 0);
            Cmb_Busqueda_Nombre_Resguardante.DataSource = Tabla;
            Cmb_Busqueda_Nombre_Resguardante.DataValueField = "EMPLEADO_ID";
            Cmb_Busqueda_Nombre_Resguardante.DataTextField = "NOMBRE";
            Cmb_Busqueda_Nombre_Resguardante.DataBind();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Procedencias
    ///DESCRIPCIÓN: Llena el combo de Procedencias.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 07/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Procedencias()
    {
        Cls_Cat_Pat_Com_Procedencias_Negocio Procedencia_Negocio = new Cls_Cat_Pat_Com_Procedencias_Negocio();
        Procedencia_Negocio.P_Estatus = "VIGENTE";
        Procedencia_Negocio.P_Tipo_DataTable = "PROCEDENCIAS";
        Cmb_Procedencia.DataSource = Procedencia_Negocio.Consultar_DataTable();
        Cmb_Procedencia.DataTextField = "NOMBRE";
        Cmb_Procedencia.DataValueField = "PROCEDENCIA_ID";
        Cmb_Procedencia.DataBind();
        Cmb_Procedencia.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combo_Bienes_Inmuebles
    ///DESCRIPCIÓN: Llena el combo de Bienes_Inmuebles.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 07/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combo_Bienes_Inmuebles()
    {
        Cls_Ope_Pat_Bienes_Inmuebles_Negocio BI_Negocio = new Cls_Ope_Pat_Bienes_Inmuebles_Negocio();
        BI_Negocio.P_Estatus = "ALTA";
        Cmb_Bien_Inmueble.DataSource = BI_Negocio.Consultar_Bienes_Inmuebles();
        Cmb_Bien_Inmueble.DataTextField = "NOMBRE_COMUN";
        Cmb_Bien_Inmueble.DataValueField = "BIEN_INMUEBLE_ID";
        Cmb_Bien_Inmueble.DataBind();
        Cmb_Bien_Inmueble.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Combos
    ///DESCRIPCIÓN: Se llenan los Combos Generales Independientes.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Combos_MPE_Busquedas()
    {
        try
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();

            //SE LLENA EL COMBO DE DEPENDENCIAS DE LAS BUSQUEDAS
            Combos.P_Tipo_DataTable = "DEPENDENCIAS";
            DataTable Dependencias = Combos.Consultar_DataTable();
            DataRow Fila_Dependencia = Dependencias.NewRow();
            Fila_Dependencia["DEPENDENCIA_ID"] = "TODAS";
            Fila_Dependencia["NOMBRE"] = HttpUtility.HtmlDecode("&lt;TODAS&gt;");
            Dependencias.Rows.InsertAt(Fila_Dependencia, 0);
            Cmb_Busqueda_Dependencias.DataSource = Dependencias;
            Cmb_Busqueda_Dependencias.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencias.DataTextField = "NOMBRE";
            Cmb_Busqueda_Dependencias.DataBind();
            Cmb_Busqueda_Resguardantes_Dependencias.DataSource = Dependencias;
            Cmb_Busqueda_Resguardantes_Dependencias.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Resguardantes_Dependencias.DataTextField = "NOMBRE";
            Cmb_Busqueda_Resguardantes_Dependencias.DataBind();
            Cmb_Busqueda_Dependencia.DataSource = Dependencias;
            Cmb_Busqueda_Dependencia.DataValueField = "DEPENDENCIA_ID";
            Cmb_Busqueda_Dependencia.DataTextField = "NOMBRE";
            Cmb_Busqueda_Dependencia.DataBind();

            //SE LLENA EL COMBO DE MARCAS DE LAS BUSQUEDAS
            Combos.P_Tipo_DataTable = "MARCAS";
            DataTable Marcas = Combos.Consultar_DataTable();
            DataRow Fila_Marca = Marcas.NewRow();
            Fila_Marca["MARCA_ID"] = "TODAS";
            Fila_Marca["NOMBRE"] = HttpUtility.HtmlDecode("&lt;TODAS&gt;");
            Marcas.Rows.InsertAt(Fila_Marca, 0);
            Cmb_Busqueda_Marca.DataSource = Marcas;
            Cmb_Busqueda_Marca.DataTextField = "NOMBRE";
            Cmb_Busqueda_Marca.DataValueField = "MARCA_ID";
            Cmb_Busqueda_Marca.DataBind();

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Listado_Bienes
    ///DESCRIPCIÓN: Se llenan el Grid de Bienes del Modal de Busqueda dependiendo de 
    ///             los filtros pasados.
    ///PROPIEDADES:     
    ///             1. Pagina.  Pagina en donde aparecerá el Grid.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Listado_Bienes(Int32 Pagina)
    {
        try
        {
            Grid_Listado_Bienes.Columns[1].Visible = true;
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bienes = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            Bienes.P_Tipo_DataTable = "BIENES";
            if (Session["FILTRO_BUSQUEDA"] != null)
            {
                Bienes.P_Tipo_Filtro_Busqueda = Session["FILTRO_BUSQUEDA"].ToString();
                if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("DATOS_GENERALES"))
                {
                    if (Txt_Busqueda_Producto.Text.Trim().Length > 0)
                    {
                        Bienes.P_Nombre_Producto = Txt_Busqueda_Producto.Text.Trim();
                    }
                    if (Cmb_Busqueda_Dependencias.SelectedIndex > 0)
                    {
                        Bienes.P_Dependencia_ID = Cmb_Busqueda_Dependencias.SelectedItem.Value;
                    }
                    if (Txt_Busqueda_Modelo.Text.Trim().Length > 0)
                    {
                        Bienes.P_Modelo = Txt_Busqueda_Modelo.Text.Trim();
                    }
                    if (Cmb_Busqueda_Marca.SelectedIndex > 0)
                    {
                        Bienes.P_Marca_ID = Cmb_Busqueda_Marca.SelectedItem.Value;
                    }
                    if (Cmb_Busqueda_Estatus.SelectedIndex > 0)
                    {
                        Bienes.P_Estatus = Cmb_Busqueda_Estatus.SelectedItem.Value;
                    }
                    if (Txt_Busqueda_Factura.Text.Trim().Length > 0)
                    {
                        Bienes.P_Factura = Txt_Busqueda_Factura.Text.Trim();
                    }
                    if (Txt_Busqueda_Numero_Serie.Text.Trim().Length > 0)
                    {
                        Bienes.P_Numero_Serie = Txt_Busqueda_Numero_Serie.Text.Trim();
                    }
                }
                else if (Session["FILTRO_BUSQUEDA"].ToString().Trim().Equals("RESGUARDANTES"))
                {
                    if (Txt_Busqueda_RFC_Resguardante.Text.Trim().Length > 0)
                    {
                        Bienes.P_RFC_Resguardante = Txt_Busqueda_RFC_Resguardante.Text.Trim();
                    }
                    if (Txt_Busqueda_No_Empleado_Resguardante.Text.Trim().Length > 0)
                    {
                        Bienes.P_No_Empleado_Resguardante = String.Format("{0:000000}", Convert.ToInt32(Txt_Busqueda_No_Empleado_Resguardante.Text.Trim()));
                    }
                    if (Cmb_Busqueda_Resguardantes_Dependencias.SelectedIndex > 0)
                    {
                        Bienes.P_Dependencia_ID = Cmb_Busqueda_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                    }
                    if (Cmb_Busqueda_Nombre_Resguardante.SelectedIndex > 0)
                    {
                        Bienes.P_Resguardante_ID = Cmb_Busqueda_Nombre_Resguardante.SelectedItem.Value.Trim();
                    }
                }
            }
            Grid_Listado_Bienes.DataSource = Bienes.Consultar_DataTable();
            Grid_Listado_Bienes.PageIndex = Pagina;
            Grid_Listado_Bienes.DataBind();
            Grid_Listado_Bienes.Columns[1].Visible = false;
            MPE_Busqueda_Bien_Mueble.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Resultados_Multiples
    ///DESCRIPCIÓN: Se llenan el Grid de Resultados Multiples del Modal de Busqueda 
    ///             dependiendo de los filtros pasados.
    ///PROPIEDADES:  Datos.  Fuente de donde se llenara el Grid.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Resultados_Multiples(DataTable Datos)
    {
        try
        {
            Grid_Resultados_Multiples.Columns[1].Visible = true;
            Grid_Resultados_Multiples.DataSource = Datos;
            Grid_Resultados_Multiples.DataBind();
            Grid_Resultados_Multiples.Columns[1].Visible = false;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Generales
    ///DESCRIPCIÓN: Se Limpian los campos Generales de los Bienes Muebles.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Generales()
    {
        try
        {
            Hdf_Cuenta_Gasto_ID.Value = "";
            Txt_Cuenta_Gasto.Text = "";
            Hdf_Partida_Reparacion_ID.Value = "";
            Txt_Clave_Partida_Reparacion.Text = "";
            Txt_Nombre_Partida_Reparacion.Text = "";
            Hdf_Bien_Mueble_ID.Value = "";
            Hdf_Tipo_Busqueda.Value = "";
            Hdf_Actualizar_Costo.Value = "";
            Hdf_Cuenta_Contable_ID.Value = "";
            Txt_Cuenta_Contable.Text = "";
            Cmb_Clase_Activo.SelectedIndex = 0;
            Cmb_Tipo_Activo.SelectedIndex = 0;
            Txt_Fecha_Adquisicion.Text = "";
            Txt_Fecha_Inventario.Text = "";
            Txt_Nombre_Producto.Text = "";
            Txt_Resguardo_Recibo.Text = "";
            Hdf_Proveedor_ID.Value = "";
            Txt_Nombre_Proveedor.Text = "";
            Cmb_Procedencia.SelectedIndex = 0;
            Cmb_Gerencias.SelectedIndex = 0;
            Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);
            Cmb_Dependencias.SelectedIndex = 0;
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Zonas.SelectedIndex = 0;
            Cmb_Bien_Inmueble.SelectedIndex = 0;
            Cmb_Marca.SelectedIndex = 0;
            Txt_Garantia.Text = "";
            Txt_Modelo.Text = "";
            Cmb_Materiales.SelectedIndex = 0;
            Cmb_Tipo_Baja.SelectedIndex = 0;
            Cmb_Colores.SelectedIndex = 0;
            Txt_Factura.Text = "";
            Txt_Numero_Serie.Text = "";
            Txt_Costo_Inicial.Text = "";
            Txt_Costo_Actual.Text = "";
            Cmb_Estatus.SelectedIndex = 0;
            Cmb_Estado.SelectedIndex = 0;
            Txt_Motivo_Baja.Text = "";
            Txt_Observaciones.Text = "";
            Txt_Numero_Inventario.Text = "";
            Txt_Invenario_Anterior.Text = "";
            Grid_Resguardantes.DataSource = new DataTable();
            Grid_Resguardantes.DataBind();
            Grid_Resguardantes.SelectedIndex = (-1);
            Grid_Historial_Resguardantes.DataSource = new DataTable();
            Grid_Historial_Resguardantes.DataBind();
            Grid_Historial_Resguardantes.SelectedIndex = (-1);
            Grid_Partes.DataSource = new DataTable();
            Grid_Partes.DataBind();
            Grid_Partes.SelectedIndex = (-1);
            Limpiar_Resguardantes();
            Limpiar_Historial_Resguardantes();
            Llenar_Combo_Empleados();
            Limpiar_SubBienes();
            //Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
            Limpiar_Parent();
            Div_Producto_Bien_Mueble_Padre.Visible = false;
            Div_Vehiculo_Parent.Visible = false;
            Txt_Busqueda_No_Empleado.Text = "";
            Txt_Busqueda_RFC.Text = "";
            Txt_Busqueda_Nombre_Empleado.Text = "";
            Cmb_Busqueda_Dependencia.SelectedIndex = 0;
            Grid_Busqueda_Empleados_Resguardo.DataSource = new DataTable();
            Grid_Busqueda_Empleados_Resguardo.DataBind();
            Session.Remove("Diccionario_Archivos");
            Session.Remove("Tabla_Archivos");
            Grid_Archivos.DataSource = new DataTable();
            Grid_Archivos.DataBind();
            Hdf_Proveniente.Value = "";
            Limpiar_Firmas();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Firmas
    ///DESCRIPCIÓN: Se Limpian los campos de la parte de Firmas.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 20/Octubre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Firmas()
    {
        Hdf_Resguardo_Completo_Operador.Value = "";
        Hdf_Resguardo_Completo_Funcionario_Recibe.Value = "";
        Hdf_Resguardo_Completo_Autorizo.Value = "";
        Txt_Resguardo_Completo_Operador.Text = "";
        Txt_Resguardo_Completo_Funcionario_Recibe.Text = "";
        Txt_Resguardo_Completo_Autorizo.Text = "";
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Resguardantes
    ///DESCRIPCIÓN: Se Limpian los campos de Resguardantes de los Bienes Muebles.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Resguardantes()
    {
        try
        {
            Cmb_Empleados.SelectedIndex = 0;
            Txt_Cometarios.Text = "";
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Historial_Resguardantes
    ///DESCRIPCIÓN: Se Limpian los campos de Historial de los Resguardantes de los 
    ///             Bienes Muebles.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Historial_Resguardantes()
    {
        try
        {
            Txt_Usuario_creo.Text = "";
            Txt_Usuario_Modifico.Text = "";
            Txt_Historial_Empleado_Resguardo.Text = "";
            Txt_Historial_Fecha_Inicial_Resguardo.Text = "";
            Txt_Historial_Fecha_Final_Resguardo.Text = "";
            Txt_Historial_Comentarios_Resguardo.Text = "";
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_SubBienes
    ///DESCRIPCIÓN: Limpia los campos donde se muestran los detalles de los subbienmes del bien.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Marzo/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_SubBienes()
    {
        try
        {
            Txt_Numero_Inventario_Parte.Text = "";
            Cmb_Material_Parte.SelectedIndex = 0;
            Cmb_Color_Parte.SelectedIndex = 0;
            Txt_Nombre_Parte.Text = "";
            Txt_Costo_Parte.Text = "";
            Txt_Fecha_Adquisicion_Parte.Text = "";
            Cmb_Estado_Parte.SelectedIndex = 0;
            Cmb_Estatus_Parte.SelectedIndex = 0;
            Txt_Comentarios_Parte.Text = "";
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Limpiar_Parent
    ///DESCRIPCIÓN: Limpia los campos donde se muestran los detalles del Parent.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Marzo/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Limpiar_Parent()
    {
        try
        {
            Hdf_Bien_Padre_ID.Value = "";
            Txt_Inventario_SIAS.Text = "";
            Txt_Numero_Inventario_Parent.Text = "";
            Cmb_Material_Parent.SelectedIndex = 0;
            Txt_Modelo_Parent.Text = "";
            Cmb_Marca_Parent.SelectedIndex = 0;
            Cmb_Color_Parent.SelectedIndex = 0;
            Txt_Nombre_Parent.Text = "";
            Txt_Costo_Parent.Text = "";
            Txt_Fecha_Adquisicion_Parent.Text = "";
            Cmb_Estado_Parent.SelectedIndex = 0;
            Cmb_Estatus_Parent.SelectedIndex = 0;
            Txt_Observaciones_Parent.Text = "";
            Txt_Vehiculo_Nombre.Text = "";
            Txt_Vehiculo_No_Inventario.Text = "";
            Txt_Vehiculo_Numero_Serie.Text = "";
            Txt_Vehiculo_Marca.Text = "";
            Txt_Vehiculo_Tipo.Text = "";
            Txt_Vehiculo_Color.Text = "";
            Txt_Vehiculo_Modelo.Text = "";
            Txt_Vehiculo_Numero_Economico.Text = "";
            Txt_Vehiculo_Placas.Text = "";
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Buscar_Clave_DataTable
    ///DESCRIPCIÓN: Busca una Clave en un DataTable, si la encuentra Retorna 'true'
    ///             en caso contrario 'false'.
    ///PROPIEDADES:  
    ///             1.  Clave.  Clave que se buscara en el DataTable
    ///             2.  Tabla.  Datatable donde se va a buscar la clave.
    ///             3.  Columna.Columna del DataTable donde se va a buscar la clave.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 29/Noviembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private Boolean Buscar_Clave_DataTable(String Clave, DataTable Tabla, Int32 Columna)
    {
        Boolean Resultado_Busqueda = false;
        if (Tabla != null && Tabla.Rows.Count > 0 && Tabla.Columns.Count > 0)
        {
            if (Tabla.Columns.Count > Columna)
            {
                for (Int32 Contador = 0; Contador < Tabla.Rows.Count; Contador++)
                {
                    if (Tabla.Rows[Contador][Columna].ToString().Trim().Equals(Clave.Trim()))
                    {
                        Resultado_Busqueda = true;
                        break;
                    }
                }
            }
        }
        return Resultado_Busqueda;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Resguardantes
    ///DESCRIPCIÓN: Llena la tabla de Resguardantes
    ///PROPIEDADES:     
    ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
    ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Resguardantes(Int32 Pagina, DataTable Tabla)
    {
        try
        {
            Session["Dt_Resguardantes"] = Tabla;
            Grid_Resguardantes.Columns[1].Visible = true;
            Grid_Resguardantes.Columns[2].Visible = true;
            Grid_Resguardantes.DataSource = Tabla;
            Grid_Resguardantes.PageIndex = Pagina;
            Grid_Resguardantes.DataBind();
            Grid_Resguardantes.Columns[1].Visible = false;
            Grid_Resguardantes.Columns[2].Visible = false;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Aqui";
            Lbl_Mensaje_Error.Text = Ex.Message;
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Historial_Resguardos
    ///DESCRIPCIÓN: Llena la tabla de Historial de Resguardantes
    ///PROPIEDADES:     
    ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
    ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Historial_Resguardos(Int32 Pagina, DataTable Tabla)
    {
        Grid_Historial_Resguardantes.Columns[1].Visible = true;
        Grid_Historial_Resguardantes.Columns[2].Visible = true;
        Grid_Historial_Resguardantes.DataSource = Tabla;
        Grid_Historial_Resguardantes.PageIndex = Pagina;
        Grid_Historial_Resguardantes.DataBind();
        Grid_Historial_Resguardantes.Columns[1].Visible = false;
        Grid_Historial_Resguardantes.Columns[2].Visible = false;
        Session["Dt_Historial_Resguardos"] = Tabla;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Partes_Bienes
    ///DESCRIPCIÓN: Llena la tabla de Partes de Bienes
    ///PROPIEDADES:     
    ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
    ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Marzo/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Partes_Bienes(Int32 Pagina, DataTable Tabla)
    {
        Grid_Partes.Columns[1].Visible = true;
        Grid_Partes.DataSource = Tabla;
        Grid_Partes.PageIndex = Pagina;
        Grid_Partes.DataBind();
        Grid_Partes.Columns[1].Visible = false;
        Session["Dt_Sub_Bienes"] = Tabla;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Proveedores
    ///DESCRIPCIÓN:      Llena el Grid de los Proveedores para que el usuario lo seleccione
    ///PARAMETROS:       Pagina. Pagina del Grid que se mostrará.     
    ///CREO:             Salvador Hernández Ramírez
    ///FECHA_CREO:       08/Agosto/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Proveedores(Int32 Pagina)
    {
        Grid_Listado_Proveedores.SelectedIndex = (-1);
        Grid_Listado_Proveedores.Columns[1].Visible = true;
        Cls_Cat_Com_Proveedores_Negocio Proveedores_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
        if (Txt_Nombre_Proveedor_Buscar.Text.Trim() != "")
        {
            Proveedores_Negocio.P_Busqueda = Txt_Nombre_Proveedor_Buscar.Text.Trim();
        }
        DataTable Dt_Proveedores = Proveedores_Negocio.Consulta_Datos_Proveedores();
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Proveedor_ID].ColumnName = "PROVEEDOR_ID";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Nombre].ColumnName = "NOMBRE";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_RFC].ColumnName = "RFC";
        Dt_Proveedores.Columns[Cat_Com_Proveedores.Campo_Compañia].ColumnName = "COMPANIA";
        Grid_Listado_Proveedores.DataSource = Dt_Proveedores;
        Grid_Listado_Proveedores.PageIndex = Pagina;
        Grid_Listado_Proveedores.DataBind();
        Grid_Listado_Proveedores.Columns[1].Visible = false;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Formulario
    ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
    ///PROPIEDADES:     
    ///             1. Estatus. Estatus en el que se cargara la configuración de los 
    ///                         controles.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Configuracion_Formulario(Boolean Estatus)
    {
        if (!Estatus)
        {
            Btn_Modificar.AlternateText = "Modificar";
            Btn_Modificar.ToolTip = "Modificar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
            Btn_Salir.AlternateText = "Salir";
            Btn_Salir.ToolTip = "Salir";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
            Btn_Generar_Reporte.Visible = true;
            Btn_Imprimir_Codigo_Barras.Visible = true;
            Lbl_Tamano_Etiqueta.Visible = true;
            Cmb_Tamano_Etiqueta.Visible = true;
            TR_Codigo_Barras.Visible = true;
            Btn_Exportar_Acta.Visible = true;
        }
        else
        {
            Btn_Modificar.AlternateText = "Actualizar";
            Btn_Modificar.ToolTip = "Actualizar";
            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
            Btn_Salir.AlternateText = "Cancelar";
            Btn_Salir.ToolTip = "Cancelar";
            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
            Btn_Generar_Reporte.Visible = false;
            Btn_Imprimir_Codigo_Barras.Visible = false;
            Lbl_Tamano_Etiqueta.Visible = false;
            Cmb_Tamano_Etiqueta.Visible = false;
            TR_Codigo_Barras.Visible = false;
            Btn_Exportar_Acta.Visible = false;
        }
        Btn_Limpiar_Campo_Proveedor.Visible = Estatus;
        Txt_Invenario_Anterior.Enabled = Estatus;
        Txt_Cuenta_Gasto.Enabled = Estatus;
        Btn_Busqueda_Partida_Reparacion.Enabled = Estatus;
        Txt_Clave_Partida_Reparacion.Enabled = Estatus;
        Txt_Cuenta_Contable.Enabled = Estatus;
        Btn_Agregar_Archivo.Visible = Estatus;
        Btn_Limpiar_FileUpload.Visible = Estatus;
        Cmb_Gerencias.Enabled = Estatus;
        Cmb_Tipo_Baja.Enabled = Estatus;
        Cmb_Dependencias.Enabled = Estatus;
        Cmb_Tipo_Activo.Enabled = Estatus;
        Cmb_Clase_Activo.Enabled = Estatus;
        Btn_Lanzar_Mpe_Proveedores.Visible = Estatus;
        Cmb_Marca.Enabled = Estatus;
        Btn_Fecha_Adquisicion.Enabled = Estatus;
        Btn_Fecha_Inventario.Enabled = Estatus;
        if (Estatus) { if (Hdf_Actualizar_Costo.Value == "SI") Txt_Costo_Inicial.Enabled = true; } else { Txt_Costo_Inicial.Enabled = false; }
        Txt_Modelo.Enabled = Estatus;
        Cmb_Procedencia.Enabled = Estatus;
        Txt_Garantia.Enabled = Estatus;
        Cmb_Materiales.Enabled = Estatus;
        Cmb_Zonas.Enabled = Estatus;
        Cmb_Bien_Inmueble.Enabled = Estatus;
        Cmb_Colores.Enabled = Estatus;
        Txt_Factura.Enabled = Estatus;
        Txt_Numero_Serie.Enabled = Estatus;
        Cmb_Estatus.Enabled = Estatus;
        Cmb_Estado.Enabled = Estatus;
        Txt_Motivo_Baja.Enabled = Estatus;
        Txt_No_Poliza.Enabled = Estatus;
        Txt_Observaciones.Enabled = Estatus;
        Btn_Resguardo_Completo_Operador.Visible = Estatus;
        Btn_Resguardo_Completo_Funcionario_Recibe.Visible = Estatus;
        Btn_Resguardo_Completo_Autorizo.Visible = Estatus;
        if (!Div_Producto_Bien_Mueble_Padre.Visible && !Div_Vehiculo_Parent.Visible)
        {
            Cmb_Empleados.Enabled = Estatus;
            Txt_Cometarios.Enabled = Estatus;
            Btn_Agregar_Resguardante.Visible = Estatus;
            Btn_Quitar_Resguardante.Visible = Estatus;
            Grid_Resguardantes.Columns[0].Visible = Estatus;
            Btn_Busqueda_Avanzada_Resguardante.Visible = Estatus;
        }
        else
        {
            Cmb_Empleados.Enabled = false;
            Txt_Cometarios.Enabled = false;
            Btn_Agregar_Resguardante.Visible = false;
            Btn_Quitar_Resguardante.Visible = false;
            Grid_Resguardantes.Columns[0].Visible = false;
            Btn_Busqueda_Avanzada_Resguardante.Visible = false;
        }
        Div_Busqueda.Visible = !Estatus;
        Div_Busqueda_No_Inventario_CONAC.Visible = !Estatus;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Bien_Mueble
    ///DESCRIPCIÓN: Carga una configuracion de los controles del Formulario
    ///PROPIEDADES:     
    ///             1. Bien_Mueble. Contiene los parametros que se desean mostrar.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Mostrar_Detalles_Bien_Mueble(Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble)
    {
        try
        {
            Hdf_Bien_Mueble_ID.Value = Bien_Mueble.P_Bien_Mueble_ID;
            Session["Producto_ID"] = Obtener_Producto_ID(Bien_Mueble.P_Bien_Mueble_ID);
            Cmb_Tipo_Activo.SelectedIndex = Cmb_Tipo_Activo.Items.IndexOf(Cmb_Tipo_Activo.Items.FindByValue(Bien_Mueble.P_Clasificacion_ID));
            Cmb_Clase_Activo.SelectedIndex = Cmb_Clase_Activo.Items.IndexOf(Cmb_Clase_Activo.Items.FindByValue(Bien_Mueble.P_Clase_Activo_ID));
            if (!String.Format("{0:dd/MMM/yyyy}", Bien_Mueble.P_Fecha_Adquisicion_).Equals(String.Format("{0:dd/MMM/yyyy}", new DateTime())))
                Txt_Fecha_Adquisicion.Text = String.Format("{0:dd/MMM/yyyy}", Bien_Mueble.P_Fecha_Adquisicion_);
            if (!String.Format("{0:dd/MMM/yyyy}", Bien_Mueble.P_Fecha_Inventario_).Equals(String.Format("{0:dd/MMM/yyyy}", new DateTime())))
                Txt_Fecha_Inventario.Text = String.Format("{0:dd/MMM/yyyy}", Bien_Mueble.P_Fecha_Inventario_);
            Txt_Nombre_Producto.Text = Bien_Mueble.P_Nombre_Producto;
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Bien_Mueble.P_Gerencia_ID));
            Cmb_Gerencias_SelectedIndexChanged(Cmb_Gerencias, null);
            Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Bien_Mueble.P_Dependencia_ID));
            Cmb_Dependencias_SelectedIndexChanged(Cmb_Dependencias, null);
            Cmb_Tipo_Baja.SelectedIndex = Cmb_Tipo_Baja.Items.IndexOf(Cmb_Tipo_Baja.Items.FindByValue(Bien_Mueble.P_Tipo_Baja_ID));
            Cmb_Marca.SelectedIndex = Cmb_Marca.Items.IndexOf(Cmb_Marca.Items.FindByValue(Bien_Mueble.P_Marca_ID));
            Txt_Garantia.Text = Bien_Mueble.P_Garantia;
            Txt_Modelo.Text = Bien_Mueble.P_Modelo;
            Cmb_Materiales.SelectedIndex = Cmb_Materiales.Items.IndexOf(Cmb_Materiales.Items.FindByValue(Bien_Mueble.P_Material_ID));
            Cmb_Colores.SelectedIndex = Cmb_Colores.Items.IndexOf(Cmb_Colores.Items.FindByValue(Bien_Mueble.P_Color_ID));
            Txt_Factura.Text = Bien_Mueble.P_Factura;
            Txt_Numero_Serie.Text = Bien_Mueble.P_Numero_Serie;
            Txt_Costo_Inicial.Text = Bien_Mueble.P_Costo_Inicial.ToString("#######0.00");
            Txt_Costo_Actual.Text = Bien_Mueble.P_Costo_Actual.ToString("#######0.00");
            Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue(Bien_Mueble.P_Estatus));
            Cmb_Procedencia.SelectedIndex = Cmb_Procedencia.Items.IndexOf(Cmb_Procedencia.Items.FindByValue(Bien_Mueble.P_Procedencia));
            Txt_Motivo_Baja.Text = Bien_Mueble.P_Motivo_Baja;
            Cmb_Estado.SelectedIndex = Cmb_Estado.Items.IndexOf(Cmb_Estado.Items.FindByValue(Bien_Mueble.P_Estado));
            Cmb_Zonas.SelectedIndex = Cmb_Zonas.Items.IndexOf(Cmb_Zonas.Items.FindByValue(Bien_Mueble.P_Zona));
            if (!String.IsNullOrEmpty(Bien_Mueble.P_Partida_ID)) Cargar_Nombre_Partida(Bien_Mueble.P_Partida_ID.Trim(), "IDENTIFICADOR");
            Cmb_Bien_Inmueble.SelectedIndex = Cmb_Bien_Inmueble.Items.IndexOf(Cmb_Bien_Inmueble.Items.FindByValue(Bien_Mueble.P_Bien_Inmueble_ID));
            Txt_Observaciones.Text = Bien_Mueble.P_Observaciones;
            Txt_Usuario_creo.Text = (Bien_Mueble.P_Dato_Creacion.Trim() != "[]") ? Bien_Mueble.P_Dato_Creacion : "";
            Txt_Usuario_Modifico.Text = (Bien_Mueble.P_Dato_Modificacion.Trim() != "[]") ? Bien_Mueble.P_Dato_Modificacion : "";
            Txt_Numero_Inventario.Text = Bien_Mueble.P_Numero_Inventario;
            Txt_Invenario_Anterior.Text = Bien_Mueble.P_Numero_Inventario_Anterior;
            Txt_Resguardo_Recibo.Text = Bien_Mueble.P_Operacion.Trim();
            Txt_Nombre_Proveedor.Text = Bien_Mueble.P_Razon_Social_Proveedor;
            Hdf_Proveedor_ID.Value = Bien_Mueble.P_Proveedor_ID;
            if (Bien_Mueble.P_Operacion.Trim() != null && Bien_Mueble.P_Operacion.Trim().Length > 0)
                Session["OPERACION_BM"] = Bien_Mueble.P_Operacion.Trim();
            else
                Session["OPERACION_BM"] = null;
            Llenar_Combo_Empleados();
            Llenar_Grid_Resguardantes(0, Bien_Mueble.P_Resguardantes);
            Llenar_Grid_Historial_Resguardos(0, Bien_Mueble.P_Historial_Resguardos);
            if (!String.IsNullOrEmpty(Bien_Mueble.P_Cuenta_Contable_ID))
            {
                Hdf_Cuenta_Contable_ID.Value = Bien_Mueble.P_Cuenta_Contable_ID;
                DataTable Dt_Datos = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", Bien_Mueble.P_Cuenta_Contable_ID);
                if (Dt_Datos != null) if (Dt_Datos.Rows.Count > 0) Txt_Cuenta_Contable.Text = Dt_Datos.Rows[0]["cuenta_descripcion"].ToString().Trim();
            }
            if (!String.IsNullOrEmpty(Bien_Mueble.P_Cuenta_Gasto_ID))
            {
                Hdf_Cuenta_Gasto_ID.Value = Bien_Mueble.P_Cuenta_Gasto_ID;
                DataTable Dt_Datos = Cls_Ope_Pat_Autocompletados_Datos.Consultar_Cuentas_Contables("", Bien_Mueble.P_Cuenta_Gasto_ID);
                if (Dt_Datos != null) if (Dt_Datos.Rows.Count > 0) Txt_Cuenta_Gasto.Text = Dt_Datos.Rows[0]["cuenta_descripcion"].ToString().Trim();
            }
            if (Bien_Mueble.P_Dt_Historial_Archivos != null && Bien_Mueble.P_Dt_Historial_Archivos.Rows.Count > 0)
            {
                DataColumn Dc_CHECKSUM = new DataColumn();
                Dc_CHECKSUM.ColumnName = "CHECKSUM";
                DataColumn Dc_RUTA_ARCHIVO = new DataColumn();
                Dc_RUTA_ARCHIVO.ColumnName = "RUTA_ARCHIVO";
                Bien_Mueble.P_Dt_Historial_Archivos.Columns.Add(Dc_CHECKSUM);
                Bien_Mueble.P_Dt_Historial_Archivos.Columns.Add(Dc_RUTA_ARCHIVO);
                Llenar_Grid_Historial_Archivos(0, Bien_Mueble.P_Dt_Historial_Archivos);
                Session["Tabla_Archivos"] = Bien_Mueble.P_Dt_Historial_Archivos;
            }
            else
            {
                Grid_Archivos.DataSource = new DataTable();
                Grid_Archivos.DataBind();
                Session.Remove("Tabla_Archivos");
            }

            Llenar_Grid_Partes_Bienes(0, Bien_Mueble.P_Dt_Bienes_Dependientes);
            Hdf_Proveniente.Value = Bien_Mueble.P_Proveniente;
            if (Bien_Mueble.P_Ascencendia != null && Bien_Mueble.P_Ascencendia.Trim().Length > 0)
            {
                if (Bien_Mueble.P_Proveniente != null && Bien_Mueble.P_Proveniente.Trim().Equals("BIEN_MUEBLE"))
                {
                    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Parent = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Bien_Parent.P_Bien_Mueble_ID = Bien_Mueble.P_Ascencendia;
                    Bien_Parent = Bien_Parent.Consultar_Detalles_Bien_Mueble();
                    Mostrar_Detalles_Bien_Parent(Bien_Parent);
                }
                else if (Bien_Mueble.P_Proveniente != null && Bien_Mueble.P_Proveniente.Trim().Equals("VEHICULO"))
                {
                    Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo = new Cls_Ope_Pat_Com_Vehiculos_Negocio();
                    Vehiculo.P_Vehiculo_ID = Bien_Mueble.P_Ascencendia;
                    Vehiculo = Vehiculo.Consultar_Detalles_Vehiculo();
                    Mostrar_Detalles_Vehiculo(Vehiculo);
                }
            }
            else
            {
                Limpiar_Parent();
                Div_Producto_Bien_Mueble_Padre.Visible = false;
                Div_Vehiculo_Parent.Visible = false;
            }


            if (Bien_Mueble.P_Empleado_Entrega != null && Bien_Mueble.P_Empleado_Entrega.Trim().Length > 0)
            {
                Hdf_Resguardo_Completo_Operador.Value = Bien_Mueble.P_Empleado_Entrega.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Hdf_Resguardo_Completo_Operador.Value.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Dt_Datos_Empleado.Rows.Count != 0)
                {
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Txt_Resguardo_Completo_Operador.Text = Texto.Trim();
                }
            }
            if (Bien_Mueble.P_Empleado_Revisa != null && Bien_Mueble.P_Empleado_Revisa.Trim().Length > 0)
            {
                Hdf_Resguardo_Completo_Funcionario_Recibe.Value = Bien_Mueble.P_Empleado_Revisa.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Hdf_Resguardo_Completo_Funcionario_Recibe.Value.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Dt_Datos_Empleado.Rows.Count != 0)
                {
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Txt_Resguardo_Completo_Funcionario_Recibe.Text = Texto.Trim();
                }
            }
            if (Bien_Mueble.P_Empleado_Autoriza != null && Bien_Mueble.P_Empleado_Autoriza.Trim().Length > 0)
            {
                Hdf_Resguardo_Completo_Autorizo.Value = Bien_Mueble.P_Empleado_Autoriza.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Hdf_Resguardo_Completo_Autorizo.Value.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Dt_Datos_Empleado.Rows.Count != 0)
                {
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Txt_Resguardo_Completo_Autorizo.Text = Texto.Trim();
                }
            }
            Hdf_Actualizar_Costo.Value = ((String.Format("{0:ddMMyyyy}", Bien_Mueble.P_Fecha_Ultima_Depreciacion).Equals(String.Format("{0:ddMMyyyy}", new DateTime()))) ? "SI" : "NO");
            Tab_Contenedor_Pestagnas.ActiveTabIndex = 0;

        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Mostrar_Detalles_Bien_Parent
    ///DESCRIPCIÓN: Muestra los detalles del Bien Mueble Parent
    ///PROPIEDADES:     
    ///             1. Bien_Mueble. Contiene los parametros que se desean mostrar.
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 25/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Mostrar_Detalles_Bien_Parent(Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble)
    {
        try
        {
            Limpiar_Parent();
            Hdf_Bien_Padre_ID.Value = Bien_Mueble.P_Bien_Mueble_ID;
            Txt_Numero_Inventario_Parent.Text = Bien_Mueble.P_Numero_Inventario_Anterior;
            Txt_Inventario_SIAS.Text = Bien_Mueble.P_Numero_Inventario;
            Txt_Nombre_Parent.Text = Bien_Mueble.P_Nombre_Producto.ToString();
            Txt_Modelo_Parent.Text = Bien_Mueble.P_Modelo;
            Cmb_Marca_Parent.SelectedIndex = Cmb_Marca_Parent.Items.IndexOf(Cmb_Marca_Parent.Items.FindByValue(Bien_Mueble.P_Marca_ID));
            Cmb_Material_Parent.SelectedIndex = Cmb_Material_Parent.Items.IndexOf(Cmb_Material_Parent.Items.FindByValue(Bien_Mueble.P_Material_ID));
            Cmb_Color_Parent.SelectedIndex = Cmb_Color_Parent.Items.IndexOf(Cmb_Color_Parent.Items.FindByValue(Bien_Mueble.P_Color_ID));
            Txt_Costo_Parent.Text = "$ " + Bien_Mueble.P_Costo_Actual.ToString();
            Txt_Fecha_Adquisicion_Parent.Text = (String.Format("{0:dd'/'MMMMMMMMMMMMMMM'/'yyyy}", Convert.ToDateTime(Bien_Mueble.P_Fecha_Adquisicion))).ToUpper();
            Cmb_Estado_Parent.SelectedIndex = Cmb_Estado_Parent.Items.IndexOf(Cmb_Estado_Parent.Items.FindByValue(Bien_Mueble.P_Estado));
            Cmb_Estatus_Parent.SelectedIndex = Cmb_Estatus_Parent.Items.IndexOf(Cmb_Estatus_Parent.Items.FindByValue(Bien_Mueble.P_Estatus));
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Bien_Mueble.P_Gerencia_ID));
            Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Bien_Mueble.P_Dependencia_ID));
            Txt_Observaciones_Parent.Text = Bien_Mueble.P_Observaciones;
            Div_Producto_Bien_Mueble_Padre.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Mostrar_Detalles_Vehiculo
    ///DESCRIPCIÓN          : Muestra la información de un Vehículo.
    ///PARAMETROS           : Vehiculo. Datos a Cargar.
    ///CREO                 : Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO           : 11/Noviembre/2011 
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    private void Mostrar_Detalles_Vehiculo(Cls_Ope_Pat_Com_Vehiculos_Negocio Vehiculo)
    {
        if (Vehiculo.P_Vehiculo_ID != null && Vehiculo.P_Vehiculo_ID.Trim().Length > 0)
        {
            Limpiar_Parent();
            Hdf_Bien_Padre_ID.Value = Vehiculo.P_Vehiculo_ID;
            Txt_Vehiculo_Nombre.Text = Vehiculo.P_Nombre_Producto;
            Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Vehiculo.P_Dependencia_ID));
            Txt_Vehiculo_No_Inventario.Text = Vehiculo.P_Numero_Inventario.ToString();
            Txt_Vehiculo_Numero_Serie.Text = Vehiculo.P_Serie_Carroceria.Trim();
            if (Vehiculo.P_Marca_ID != null && Vehiculo.P_Marca_ID.Trim().Length > 0)
            {
                Cls_Cat_Com_Marcas_Negocio Marca_Negocio = new Cls_Cat_Com_Marcas_Negocio();
                Marca_Negocio.P_Marca_ID = Vehiculo.P_Marca_ID;
                DataTable Dt_Marcas = Marca_Negocio.Consulta_Marcas();
                if (Dt_Marcas != null && Dt_Marcas.Rows.Count > 0)
                {
                    Txt_Vehiculo_Marca.Text = Dt_Marcas.Rows[0][Cat_Com_Marcas.Campo_Nombre].ToString();
                }
            }
            Txt_Vehiculo_Modelo.Text = Vehiculo.P_Modelo_ID;
            if (Vehiculo.P_Tipo_Vehiculo_ID != null && Vehiculo.P_Tipo_Vehiculo_ID.Trim().Length > 0)
            {
                Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio Tipo_Negocio = new Cls_Cat_Pat_Com_Tipos_Vehiculo_Negocio();
                Tipo_Negocio.P_Tipo_Vehiculo_ID = Vehiculo.P_Tipo_Vehiculo_ID;
                Tipo_Negocio = Tipo_Negocio.Consultar_Datos_Vehiculo();
                if (Tipo_Negocio.P_Tipo_Vehiculo_ID != null && Tipo_Negocio.P_Tipo_Vehiculo_ID.Trim().Length > 0)
                {
                    Txt_Vehiculo_Tipo.Text = Tipo_Negocio.P_Descripcion;
                }
            }
            if (Vehiculo.P_Color_ID != null && Vehiculo.P_Color_ID.Trim().Length > 0)
            {
                Cls_Cat_Pat_Com_Colores_Negocio Color_Negocio = new Cls_Cat_Pat_Com_Colores_Negocio();
                Color_Negocio.P_Color_ID = Vehiculo.P_Color_ID;
                Color_Negocio.P_Tipo_DataTable = "COLORES";
                DataTable Dt_Colores = Color_Negocio.Consultar_DataTable();
                if (Dt_Colores != null && Dt_Colores.Rows.Count > 0)
                {
                    Txt_Vehiculo_Color.Text = Dt_Colores.Rows[0][Cat_Pat_Colores.Campo_Descripcion].ToString();
                }
            }
            Txt_Vehiculo_Numero_Economico.Text = Vehiculo.P_Numero_Economico_;
            Txt_Vehiculo_Placas.Text = Vehiculo.P_Placas;
            Div_Vehiculo_Parent.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Remover_Sesiones_Control_AsyncFileUpload
    ///DESCRIPCIÓN: Limpia un control de AsyncFileUpload
    ///PROPIEDADES:     
    ///CREO: Juan Alberto Hernandez Negrete
    ///FECHA_CREO: 16/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Remover_Sesiones_Control_AsyncFileUpload(String Cliente_ID)
    {
        HttpContext Contexto;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            Contexto = HttpContext.Current;
        }
        else
        {
            Contexto = null;
        }
        if (Contexto != null)
        {
            foreach (String key in Contexto.Session.Keys)
            {
                if (key.Contains(Cliente_ID))
                {
                    Contexto.Session.Remove(key);
                    break;
                }
            }
        }
    }

    #region Validaciones

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes_Generales
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación de Actualizacion.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private bool Validar_Componentes_Generales()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Materiales.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Materiales.";
            Validacion = false;
        }
        if (Cmb_Procedencia.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Procedencias.";
            Validacion = false;
        }
        if (Cmb_Gerencias.SelectedIndex == 0 && Cmb_Estatus.SelectedItem.Value.Trim().Equals("VIGENTE"))
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo Gerencia.";
            Validacion = false;
        }
        if (Cmb_Dependencias.SelectedIndex == 0 && Cmb_Estatus.SelectedItem.Value.Trim().Equals("VIGENTE"))
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo Unidad Responsable.";
            Validacion = false;
        }
        if (Cmb_Colores.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Colores.";
            Validacion = false;
        }
        if (Txt_Numero_Serie.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Número de Serie del Bien.";
            Validacion = false;
        }
        if (Txt_Invenario_Anterior.Text.Trim().Length == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Introducir el Número de Inventario Anterior.";
            Validacion = false;
        }
        if (Cmb_Estatus.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Estatus.";
            Validacion = false;
        }
        else
        {
            if (!Cmb_Estatus.SelectedItem.Value.Equals("VIGENTE"))
            {
                if (Txt_Motivo_Baja.Text.Trim().Length == 0)
                {
                    if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
                    Mensaje_Error = Mensaje_Error + "+ Introducir la Motivo de Baja del Bien.";
                    Validacion = false;
                }
            }
        }
        if (Cmb_Estado.SelectedIndex == 0)
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe seleccionar una opción del Combo de Estado.";
            Validacion = false;
        }
        if ((Grid_Resguardantes.Rows.Count == 0 || Session["Dt_Resguardantes"] == null) && Cmb_Estatus.SelectedItem.Value.Trim().Equals("VIGENTE"))
        {
            if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
            Mensaje_Error = Mensaje_Error + "+ Debe haber como minimo un empleado para resguardo del Bien.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Componentes_Resguardos
    ///DESCRIPCIÓN: Hace una validacion de que haya datos en los componentes antes de hacer
    ///             una operación de la pestaña de Resguardos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private bool Validar_Componentes_Resguardos()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Empleados.SelectedIndex == 0)
        {
            Mensaje_Error = Mensaje_Error + "+ Seleccionar el Empleado para Resguardo.";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Baja_Bien
    ///DESCRIPCIÓN: Valida las Bajas de los bienes
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private bool Validar_Baja_Bien()
    {
        Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
        String Mensaje_Error = "";
        Boolean Validacion = true;
        if (Cmb_Estatus.SelectedItem.Value != "VIGENTE")
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            Negocio.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value;
            Negocio = Negocio.Consultar_Detalles_Bien_Mueble();
            if (Negocio.P_Resguardantes == null || Negocio.P_Resguardantes.Rows.Count == 0)
            {
                if (!Validacion) { Mensaje_Error = Mensaje_Error + "<br>"; }
                Mensaje_Error = Mensaje_Error + "+ Antes de dar una Baja es necesario Actualizarle un Resguardate al Bien Mueble.";
                Validacion = false;
            }
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Validar_Asignacion
    ///DESCRIPCIÓN: Valida la Solicitud de Servicio antes de ser Asignada a un mecanico
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 17/Mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///******************************************************************************* 
    private Boolean Validar_Archivo()
    {
        String Mensaje_Error = "";
        Boolean Validacion = true;
        String[] Extensiones_Permitidas = { ".jpg", ".jpeg", ".png", ".gif", ".doc", ".docx", ".ppt", ".pptx", ".pdf" };
        String Extension_Archivo = Path.GetExtension(AFU_Archivo.FileName).ToLower();

        if (AFU_Archivo.HasFile)
        {
            if (AFU_Archivo.FileBytes.Length > 2048000) // si la longitud del archivo recibido es mayor que 2MB, mostrar mensaje
            {
                Mensaje_Error += "+ El tamaño del archivo excede el limite permitido: " + AFU_Archivo.FileName;
                Mensaje_Error += " <br />";
                Validacion = false;
            }
            if (Array.IndexOf(Extensiones_Permitidas, Extension_Archivo) < 0)
            {
                Mensaje_Error += "+ No se permite subir archivos con extensión: " + Extension_Archivo;
                Mensaje_Error += " <br />";
                Validacion = false;
            }
        }
        else
        {
            Mensaje_Error += "+ Debe seleccionar un archivo para subir.";
            Mensaje_Error += " <br />";
            Validacion = false;
        }
        if (!Validacion)
        {
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Mensaje_Error);
            Div_Contenedor_Msj_Error.Visible = true;
        }
        return Validacion;
    }

    #endregion

    #region Reporte

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_DataSet_Resguardos_Bienes()
    ///DESCRIPCIÓN: Llena el dataSet "Data_Set_Resguardos_Bienes" con las personas a las que se les asigno el
    ///bien mueble y sus detalles, para que con estos datos se genere el reporte.
    ///PARAMETROS:  
    ///CREO: Salvador Hernández Ramírez
    ///FECHA_CREO: 17/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Llenar_DataSet_Resguardos_Bienes(Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Id)
    {
        String Formato = "PDF";
        Consulta_Resguardos_Negocio = new Cls_Alm_Com_Resguardos_Negocio();
        Bien_Id.P_Producto_Almacen = false;
        DataTable Data_Set_Resguardos_Bienes;
        Consulta_Resguardos_Negocio.P_Operacion = Bien_Id.P_Operacion.Trim();

        if (Session["OPERACION_BM"] != null)
            Consulta_Resguardos_Negocio.P_Operacion = Session["OPERACION_BM"].ToString().Trim();

        Data_Set_Resguardos_Bienes = Consulta_Resguardos_Negocio.Consulta_Resguardos_Bienes(Bien_Id);

        Ds_Alm_Com_Resguardos_Bienes Ds_Consulta_Resguardos_Bienes = new Ds_Alm_Com_Resguardos_Bienes();
        Generar_Reporte(Data_Set_Resguardos_Bienes, Ds_Consulta_Resguardos_Bienes, Formato);
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Jefe_Dependencia
    ///DESCRIPCIÓN: Regresa el nombre del empleado nombrado jefe de dependencia en la tabla organigrama
    ///PARAMETROS:  el identificador de la dependencia
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Jefe_Dependencia(String Dependencia_ID)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Jefe_Departamento(Dependencia_ID);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Jefe_Almacen
    ///DESCRIPCIÓN: Regresa el nombre del empleado nombrado jefe de almacen en la tabla organigrama
    ///PARAMETROS:  el tipo de puesto determinado en el campo tipo de la tabla organigrama
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Jefe_Almacen(String Almacen)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Jefe_Almacen(Almacen);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Almacen
    ///DESCRIPCIÓN: Regresa el valor del campo ALMACEN_GENERAL de la tabla productos para conocer de que almacen proviene el producto
    ///PARAMETROS:  el identificador del producto
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Almacen(String Bien_ID)
    {
        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Almacen(Bien_ID);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Obtener_Producto_ID
    ///DESCRIPCIÓN: Regresa el identificador del producto de acuerdo al bien mueble seleccionado
    ///PARAMETROS:  el identificador del bien mueble al que pertenece el resguardo
    ///CREO: Luis Daniel Guzmán Malagón
    ///FECHA_CREO: 29/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Obtener_Producto_ID(String Bien_ID)
    {

        return new Cls_Alm_Com_Resguardos_Negocio().Obtener_Producto_ID(Bien_ID);
        //return null;
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN:          Caraga el data set fisoco con el cual se genera el Reporte especificado
    ///PARAMETROS:           1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///                      2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///                      3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO:           15/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte(DataTable Data_Set_Consulta_DB, DataSet Ds_Reporte, String Formato)
    {

        String Ruta_Reporte_Crystal = "";
        String Nombre_Reporte_Generar = "";
        DataRow Renglon;

        try
        {
            Renglon = Data_Set_Consulta_DB.Rows[0];
            String Cantidad = Data_Set_Consulta_DB.Rows[0]["CANTIDAD"].ToString();
            String Costo = Data_Set_Consulta_DB.Rows[0]["COSTO_UNITARIO"].ToString();
            Double Resultado = (Convert.ToDouble(Cantidad)) * (Convert.ToDouble(Costo));
            Ds_Reporte.Tables[1].ImportRow(Renglon);
            Ds_Reporte.Tables[1].Rows[0].SetField("COSTO_TOTAL", ((!String.IsNullOrEmpty(Data_Set_Consulta_DB.Rows[0]["COSTO_TOTAL"].ToString()) ? Data_Set_Consulta_DB.Rows[0]["COSTO_TOTAL"].ToString() : "0")));

            if (Session["OPERACION_BM"] != null)
            {
                if (Session["OPERACION_BM"].ToString().Trim() == "RESGUARDO")
                    Ds_Reporte.Tables[1].Rows[0].SetField("OPERACION", "CONTROL DE RESGUARDOS DE BIENES MUEBLES");

                else if (Session["OPERACION_BM"].ToString().Trim() == "CUSTODIA")
                    Ds_Reporte.Tables[1].Rows[0].SetField("OPERACION", "CONTROL DE CUSTODIAS DE BIENES MUEBLES");
            }
            if ((string.IsNullOrEmpty(Ds_Reporte.Tables[1].Rows[0]["MARCA"].ToString())) | (Ds_Reporte.Tables[1].Rows[0]["MARCA"].ToString().Trim() == ""))
                Ds_Reporte.Tables[1].Rows[0].SetField("MARCA", "INDISTINTA");

            if (string.IsNullOrEmpty(Ds_Reporte.Tables[1].Rows[0]["MODELO"].ToString()))
                Ds_Reporte.Tables[1].Rows[0].SetField("MODELO", "INDISTINTO");

            for (int Cont_Elementos = 0; Cont_Elementos < Data_Set_Consulta_DB.Rows.Count; Cont_Elementos++)
            {
                Renglon = Data_Set_Consulta_DB.Rows[Cont_Elementos];
                String No_Empleado = Data_Set_Consulta_DB.Rows[Cont_Elementos]["NO_EMPLEADO"].ToString();
                String Nombre_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["NOMBRE_E"].ToString();
                String Apellido_Paterno_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["APELLIDO_PATERNO_E"].ToString();
                String Apellido_Materno_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["APELLIDO_MATERNO_E"].ToString();
                String RFC_E = Data_Set_Consulta_DB.Rows[Cont_Elementos]["RFC_E"].ToString();
                String Resguardante = Nombre_E + " " + Apellido_Paterno_E + " " + Apellido_Materno_E + " " + "(" + RFC_E + ")";
                if (!Resguardante.Trim().Equals("()"))
                {
                    Ds_Reporte.Tables[0].ImportRow(Renglon);
                    Ds_Reporte.Tables[0].Rows[Cont_Elementos].SetField("RESGUARDANTES", "[" + No_Empleado.Trim() + "] " + Resguardante);
                }
                else
                {
                    Ds_Reporte.Tables[0].ImportRow(Renglon);
                }
            }
            // Ruta donde se encuentra el reporte Crystal
            Ruta_Reporte_Crystal = "../Rpt/Almacen/Rpt_Alm_Com_Resguardos_Bienes_Almacen.rpt";

            // Se crea el nombre del reporte
            //String Nombre_Reporte;

            // Se da el nombre del reporte que se va generar
            if (Formato == "PDF")
                Nombre_Reporte_Generar = "Rpt_Alm_Com_Resguardos_Bienes_Almacen" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";  // Es el nombre del reporte PDF que se va a generar
            else if (Formato == "Excel")
                Nombre_Reporte_Generar = "Rpt_Alm_Com_Resguardos_Bienes_Almacen" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".xls";  // Es el nombre del repote en Excel que se va a generar

            Cls_Reportes Reportes = new Cls_Reportes();
            Reportes.Generar_Reporte(ref Ds_Reporte, Ruta_Reporte_Crystal, Nombre_Reporte_Generar, Formato);
            String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al llenar el DataSet. Error: [" + Ex.Message + "]");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Reporte_Codigo_Barras
    ///DESCRIPCIÓN: Genera el reporte de Codigo de Barras.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Crear_Reporte_Codigo_Barras(String No_Inventario)
    {
        DataTable Dt_Datos = new DataTable("DT_DATOS");
        Dt_Datos.Columns.Add("Numero_Inventario", Type.GetType("System.String"));
        Dt_Datos.Columns.Add("Numero_Inventario_Codigo", Type.GetType("System.String"));
        Dt_Datos.Columns.Add("Numero_Inventario_Imagen", Type.GetType("System.Byte[]"));
        DataRow Fila = Dt_Datos.NewRow();
        Fila["Numero_Inventario"] = No_Inventario.Trim();
        Fila["Numero_Inventario_Codigo"] = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.code128(No_Inventario.Trim());
        Fila["Numero_Inventario_Imagen"] = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.Imagen_Codigo_Barras(No_Inventario.Trim());
        Dt_Datos.Rows.Add(Fila);
        DataSet Ds_Consulta = new DataSet();
        Ds_Consulta.Tables.Add(Dt_Datos);
        Ds_Rpt_Pat_Codigo_Barras Ds_Reporte = new Ds_Rpt_Pat_Codigo_Barras();
        Generar_Reporte_Codigo_Barras(Ds_Consulta, Ds_Reporte, "Rpt_Pat_Codigo_Barras.rpt");
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Reporte_Codigo_Barras
    ///DESCRIPCIÓN: Genera el reporte de Codigo de Barras.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Generar_Reporte_Codigo_Barras(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte, String Nombre_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/" + Nombre_Reporte);
        Reporte.Load(File_Path);
        String Nombre_Reporte_Generar = "Rpt_Pat_Cod_Bar_" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
        String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            if (Formato == "PDF")
            {
                Pagina = Pagina + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else if (Formato == "Excel")
            {
                String Ruta = "../../Reporte/" + Nombre_Reporte_Generar;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,dire ctories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Consultar_Empleados
    ///DESCRIPCIÓN:          Metodo utilizado para consultar y llenar el combo con los empleados que pertenecen a la dependencia
    ///PROPIEDADES:     
    ///                      1.  Dependencia_ID. el identificador de la dependencia en base a la que se va hacer la consulta
    ///              
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO: 02/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    public void Consultar_Empleados(String Dependencia_ID)
    {
        try
        {
            Session.Remove("Dt_Resguardantes");
            Grid_Resguardantes.DataSource = new DataTable();
            Grid_Resguardantes.DataBind();

            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combo = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            Combo.P_Tipo_DataTable = "EMPLEADOS";
            Combo.P_Dependencia_ID = Dependencia_ID;
            DataTable Tabla = Combo.Consultar_DataTable();
            Llenar_Combo_Empleados(Tabla);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #region "Busqueda Resguardantes"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Busqueda_Empleados_Resguardo
    ///DESCRIPCIÓN: Llena el Grid con los empleados que cumplan el filtro
    ///PROPIEDADES:     
    ///CREO:                 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Busqueda_Empleados_Resguardo()
    {
        Grid_Busqueda_Empleados_Resguardo.SelectedIndex = (-1);
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = true;
        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        Negocio.P_Estatus_Empleado = "ACTIVO";
        if (Txt_Busqueda_No_Empleado.Text.Trim().Length > 0) { Negocio.P_No_Empleado_Resguardante = Txt_Busqueda_No_Empleado.Text.Trim(); }
        if (Txt_Busqueda_RFC.Text.Trim().Length > 0) { Negocio.P_RFC_Resguardante = Txt_Busqueda_RFC.Text.Trim(); }
        if (Txt_Busqueda_Nombre_Empleado.Text.Trim().Length > 0) { Negocio.P_Nombre_Resguardante = Txt_Busqueda_Nombre_Empleado.Text.Trim(); }
        if (Cmb_Busqueda_Dependencia.SelectedIndex > 0) { Negocio.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedItem.Value; }
        Grid_Busqueda_Empleados_Resguardo.DataSource = Negocio.Consultar_Empleados_Resguardos();
        Grid_Busqueda_Empleados_Resguardo.DataBind();
        Grid_Busqueda_Empleados_Resguardo.Columns[1].Visible = false;
    }

    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Llenar_Grid_Historial_Archivos
    ///DESCRIPCIÓN: Llena la tabla de Historial de Archivos
    ///PARAMETROS:     
    ///             1.  Pagina. Pagina en la cual se mostrará el Grid_VIew
    ///             2.  Tabla.  Tabla que se va a cargar en el Grid.    
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Llenar_Grid_Historial_Archivos(Int32 Pagina, DataTable Tabla)
    {
        Grid_Archivos.Columns[0].Visible = true;
        Grid_Archivos.Columns[1].Visible = true;
        Grid_Archivos.DataSource = Tabla;
        Grid_Archivos.PageIndex = Pagina;
        Grid_Archivos.DataBind();
        Grid_Archivos.Columns[0].Visible = false;
        Grid_Archivos.Columns[1].Visible = false;
        Session["Tabla_Archivos"] = Tabla;
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Generar_Tabla_Archivos
    /// DESCRIPCION: Genera la tabla de Documentos, el esquema para guardar los tipos de document a recibir
    /// PARAMETROS: 
    /// CREO: Jesus Toledo Rodriguez
    /// FECHA_CREO: 04-may-2012
    /// MODIFICO:
    /// FECHA_MODIFICO:
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private DataTable Generar_Tabla_Archivos()
    {
        DataTable Tabla_Nueva = new DataTable();
        DataColumn Columna1;
        DataColumn Columna2;
        DataColumn Columna3;
        DataColumn Columna4;
        DataColumn Columna5;
        DataColumn Columna6;
        DataColumn Columna7;
        DataColumn Columna8;
        DataColumn Columna9;

        // ---------- Inicializar columnas
        Columna1 = new DataColumn();
        Columna1.DataType = System.Type.GetType("System.String");
        Columna1.ColumnName = "ARCHIVO_BIEN_ID";
        Tabla_Nueva.Columns.Add(Columna1);
        Columna2 = new DataColumn();
        Columna2.DataType = System.Type.GetType("System.String");
        Columna2.ColumnName = "BIEN_ID";
        Tabla_Nueva.Columns.Add(Columna2);
        Columna3 = new DataColumn();
        Columna3.DataType = System.Type.GetType("System.String");
        Columna3.ColumnName = "TIPO";
        Tabla_Nueva.Columns.Add(Columna3);
        Columna4 = new DataColumn();
        Columna4.DataType = System.Type.GetType("System.DateTime");
        Columna4.ColumnName = "FECHA";
        Tabla_Nueva.Columns.Add(Columna4);
        Columna5 = new DataColumn();
        Columna5.DataType = System.Type.GetType("System.String");
        Columna5.ColumnName = "ARCHIVO";
        Tabla_Nueva.Columns.Add(Columna5);
        Columna6 = new DataColumn();
        Columna6.DataType = System.Type.GetType("System.String");
        Columna6.ColumnName = "TIPO_ARCHIVO";
        Tabla_Nueva.Columns.Add(Columna6);
        Columna7 = new DataColumn();
        Columna7.DataType = System.Type.GetType("System.String");
        Columna7.ColumnName = "DESCRIPCION_ARCHIVO";
        Tabla_Nueva.Columns.Add(Columna7);
        Columna8 = new DataColumn();
        Columna8.DataType = System.Type.GetType("System.String");
        Columna8.ColumnName = "CHECKSUM";
        Tabla_Nueva.Columns.Add(Columna8);
        Columna9 = new DataColumn();
        Columna9.DataType = System.Type.GetType("System.String");
        Columna9.ColumnName = "RUTA_ARCHIVO";
        Tabla_Nueva.Columns.Add(Columna9);

        return Tabla_Nueva;
    }

    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Obtener_Diccionario_Archivos
    /// 	DESCRIPCIÓN: Regresa el diccionario checksum-archivo si se encuentra en variable de sesion y si no,
    /// 	            regresa un diccionario vacio
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 04-may-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private Dictionary<String, Byte[]> Obtener_Diccionario_Archivos()
    {
        Dictionary<String, Byte[]> Diccionario_Archivos = new Dictionary<String, Byte[]>();

        // si existe el diccionario en variable de sesion
        if (Session["Diccionario_Archivos"] != null)
        {
            Diccionario_Archivos = (Dictionary<String, Byte[]>)Session["Diccionario_Archivos"];
        }

        return Diccionario_Archivos;
    }

    ///*******************************************************************************************************
    /// 	NOMBRE_FUNCIÓN: Guardar_Archivos
    /// 	DESCRIPCIÓN: Guardar en el servidor los archivos que se hayan recibido
    /// 	PARÁMETROS:
    /// 	CREO: Roberto González Oseguera
    /// 	FECHA_CREO: 10-may-2011
    /// 	MODIFICÓ: 
    /// 	FECHA_MODIFICÓ: 
    /// 	CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Guardar_Archivos()
    {
        DataTable Tabla_Tramites = (DataTable)Session["Tabla_Archivos"];
        Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();
        String Directorio = MapPath("../../" + Ope_Pat_Archivos_Bienes.Campo_Ruta_Fisica_Archivos + "/BIENES_MUEBLES/" + Hdf_Bien_Mueble_ID.Value.ToString().Trim());
        String Rura_Archivo = "";

        try
        {
            if (Tabla_Tramites != null)     //si la tabla tramites contiene datos
            {
                foreach (DataRow Fila_Tramite in Tabla_Tramites.Rows)   // recorrer la tabla
                {
                    if (!String.IsNullOrEmpty(Fila_Tramite["CHECKSUM"].ToString()))
                    {
                        if (!Directory.Exists(Directorio))//si el directorio no existe, crearlo
                            Directory.CreateDirectory(Directorio);

                        Rura_Archivo = Directorio + "/" + HttpUtility.HtmlDecode(Fila_Tramite["ARCHIVO"].ToString());
                        //crear filestream y binarywriter para guardar archivo
                        FileStream Escribir_Archivo = new FileStream(Rura_Archivo, FileMode.Create, FileAccess.Write);
                        BinaryWriter Datos_Archivo = new BinaryWriter(Escribir_Archivo);

                        // Guardar archivo (escribir datos en el filestream)                            
                        Datos_Archivo.Write(Diccionario_Archivos[Fila_Tramite["CHECKSUM"].ToString()]);
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Guardar_Archivos " + Ex.Message.ToString(), Ex);
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cargar_Nombre_Partida
    ///DESCRIPCIÓN: Cargar_Nombre_Partida
    ///PARAMETROS: 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Cargar_Nombre_Partida(String Filtro, String Tipo_Filtro)
    {
        Cls_Rpt_Pat_Listado_Bienes_Negocio Cls_Negocio = new Cls_Rpt_Pat_Listado_Bienes_Negocio();
        if (Tipo_Filtro.Equals("CLAVE")) Cls_Negocio.P_Clave_Partida = Filtro.Trim();
        if (Tipo_Filtro.Equals("IDENTIFICADOR")) Cls_Negocio.P_Partida_ID = Filtro.Trim();
        DataTable Dt_Partidas = Cls_Negocio.Consultar_Paridas_Especificas();
        if (Dt_Partidas != null)
        {
            if (Dt_Partidas.Rows.Count == 1)
            {
                Hdf_Partida_Reparacion_ID.Value = Dt_Partidas.Rows[0]["PARTIDA_ID"].ToString().Trim();
                Txt_Clave_Partida_Reparacion.Text = Dt_Partidas.Rows[0]["CLAVE"].ToString().Trim();
                Txt_Nombre_Partida_Reparacion.Text = Dt_Partidas.Rows[0]["NOMBRE"].ToString().Trim();
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "La Clave de la Partida para Reparación no es correcta.";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Crear_Xml_Acta_00001
    ///DESCRIPCIÓN: Crear_Xml_Acta_00001
    ///PARAMETROS: 
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 15/Julio/2013 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private String Crear_Xml_Acta_00001()
    {
        StringBuilder Xml_Acta_00001 = new StringBuilder();
        Xml_Acta_00001.Append("<root>");
        Xml_Acta_00001.Append("<Hora_Actual>");
        Xml_Acta_00001.Append(String.Format("{0:HH:mm}", DateTime.Now));
        Xml_Acta_00001.Append("</Hora_Actual>");
        Xml_Acta_00001.Append("<Dia_Actual>");
        Xml_Acta_00001.Append(String.Format("{0:dd}", DateTime.Now));
        Xml_Acta_00001.Append("</Dia_Actual>");
        Xml_Acta_00001.Append("<Mes_Actual>");
        Xml_Acta_00001.Append(String.Format("{0:MMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMMM}", DateTime.Now).ToUpper());
        Xml_Acta_00001.Append("</Mes_Actual>");
        Xml_Acta_00001.Append("<Anio_Actual>");
        Xml_Acta_00001.Append(String.Format("{0:yyyy}", DateTime.Now));
        Xml_Acta_00001.Append("</Anio_Actual>");
        Xml_Acta_00001.Append("<Descripcion_Bien>");
        Xml_Acta_00001.Append(Txt_Nombre_Producto.Text.ToUpper().Trim());
        Xml_Acta_00001.Append("</Descripcion_Bien>");
        Xml_Acta_00001.Append("<Marca_Bien>");
        Xml_Acta_00001.Append(Cmb_Marca.SelectedItem.Text.ToUpper().Trim());
        Xml_Acta_00001.Append("</Marca_Bien>");
        Xml_Acta_00001.Append("<Modelo_Bien>");
        Xml_Acta_00001.Append(Txt_Modelo.Text.ToUpper().Trim());
        Xml_Acta_00001.Append("</Modelo_Bien>");
        Xml_Acta_00001.Append("<Serie_Bien>");
        Xml_Acta_00001.Append(Txt_Numero_Serie.Text.ToUpper().Trim());
        Xml_Acta_00001.Append("</Serie_Bien>");
        Xml_Acta_00001.Append("<Responsable_Bien>");
        StringBuilder Responsables = new StringBuilder();
        if (Session["Dt_Resguardantes"] != null)
        {
            DataTable Dt_Datos = (DataTable)Session["Dt_Resguardantes"];
            foreach (DataRow Fila_Resposansable in Dt_Datos.Rows)
            {
                if (Responsables.ToString().Trim().Length > 0) Responsables.Append(", ");
                Responsables.Append(Fila_Resposansable["NOMBRE_EMPLEADO"].ToString().Trim());
            }
        }
        Xml_Acta_00001.Append(Responsables.ToString());
        Xml_Acta_00001.Append("</Responsable_Bien>");
        Xml_Acta_00001.Append("</root>");
        return Xml_Acta_00001.ToString();
    }

    #endregion

    #region Grid

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Bienes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Bienes del Modal de Busqueda
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Bienes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Bienes.SelectedIndex = (-1);
            Llenar_Grid_Listado_Bienes(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Bienes_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Bienes del
    ///             Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Bienes_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Bienes.SelectedIndex > (-1))
            {
                Limpiar_Generales();
                String Bien_Mueble_Seleccionado_ID = Grid_Listado_Bienes.SelectedRow.Cells[1].Text.Trim();
                Session["Producto_ID"] = Obtener_Producto_ID(Bien_Mueble_Seleccionado_ID);
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Bien_Mueble.P_Bien_Mueble_ID = Bien_Mueble_Seleccionado_ID;
                Bien_Mueble = Bien_Mueble.Consultar_Detalles_Bien_Mueble();
                Mostrar_Detalles_Bien_Mueble(Bien_Mueble);
                Grid_Listado_Bienes.SelectedIndex = -1;
                MPE_Busqueda_Bien_Mueble.Hide();
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Historial_Resguardantes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Historial de Resguardos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Historial_Resguardantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Historial_Resguardos"] != null)
            {
                Grid_Historial_Resguardantes.SelectedIndex = (-1);
                Llenar_Grid_Historial_Resguardos(e.NewPageIndex, (DataTable)Session["Dt_Historial_Resguardos"]);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Historial_Resguardantes_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Historial
    ///             de Resguardos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Historial_Resguardantes_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Historial_Resguardantes.SelectedIndex > (-1))
            {
                Limpiar_Historial_Resguardantes();
                if (Session["Dt_Historial_Resguardos"] != null)
                {
                    Int32 Registro = ((Grid_Historial_Resguardantes.PageIndex) * Grid_Historial_Resguardantes.PageSize) + (Grid_Historial_Resguardantes.SelectedIndex);
                    DataTable Tabla = (DataTable)Session["Dt_Historial_Resguardos"];
                    Txt_Historial_Empleado_Resguardo.Text = "[" + Tabla.Rows[Registro]["NO_EMPLEADO"].ToString().Trim() + "] " + Tabla.Rows[Registro]["NOMBRE_EMPLEADO"].ToString().Trim();
                    Txt_Historial_Comentarios_Resguardo.Text = Tabla.Rows[Registro]["COMENTARIOS"].ToString().Trim();
                    Txt_Historial_Fecha_Inicial_Resguardo.Text = String.Format("{0:dd/MMM/yyyy}", Tabla.Rows[Registro]["FECHA_INICIAL"]);
                    Txt_Historial_Fecha_Final_Resguardo.Text = String.Format("{0:dd/MMM/yyyy}", Tabla.Rows[Registro]["FECHA_FINAL"]);
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resguardantes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Resguardos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Resguardantes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Resguardantes"] != null)
            {
                Grid_Resguardantes.SelectedIndex = (-1);
                Llenar_Grid_Resguardantes(e.NewPageIndex, (DataTable)Session["Dt_Resguardantes"]);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resguardantes_RowDataBound
    ///DESCRIPCIÓN: Maneja el Evento RowDataBound del Grid de Resguardos.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Resguardantes_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                if (e.Row.FindControl("Btn_Ver_Informacion_Resguardo") != null)
                {
                    if (Session["Dt_Resguardantes"] != null)
                    {
                        ImageButton Btn_Informacion = (ImageButton)e.Row.FindControl("Btn_Ver_Informacion_Resguardo");
                        Btn_Informacion.CommandArgument = ((DataTable)Session["Dt_Resguardantes"]).Rows[e.Row.RowIndex]["COMENTARIOS"].ToString();
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = "Verificar.";
            Lbl_Ecabezado_Mensaje.Text = "[Excepción: '" + Ex.Message + "']";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Archivos_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Historial de Archivos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Archivos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            if (Session["Dt_Historial_Archivos"] != null)
            {
                Grid_Archivos.SelectedIndex = (-1);
                Llenar_Grid_Historial_Archivos(e.NewPageIndex, (DataTable)Session["Dt_Historial_Archivos"]);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Partes_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el Cambio de Pagina de Grid_Partes
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Marzo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Partes_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            DataTable Tabla = new DataTable();
            if (Session["Dt_Sub_Bienes"] != null)
            {
                Tabla = (DataTable)Session["Dt_Sub_Bienes"];
            }
            Llenar_Grid_Partes_Bienes(e.NewPageIndex, Tabla);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Partes_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el Cambio de Selección de Grid_Partes
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Marzo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Partes_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Limpiar_SubBienes();
            if (Grid_Partes.SelectedIndex > (-1))
            {
                String Bien_ID = Grid_Partes.SelectedRow.Cells[1].Text.Trim();
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Bien.P_Bien_Mueble_ID = Bien_ID;
                Bien = Bien.Consultar_Detalles_Bien_Mueble();
                Txt_Numero_Inventario_Parte.Text = Bien.P_Numero_Inventario;
                Txt_Nombre_Parte.Text = Bien.P_Nombre_Producto.ToString();
                Cmb_Material_Parte.SelectedIndex = Cmb_Material_Parte.Items.IndexOf(Cmb_Material_Parte.Items.FindByValue(Bien.P_Material_ID));
                Cmb_Color_Parte.SelectedIndex = Cmb_Color_Parte.Items.IndexOf(Cmb_Color_Parte.Items.FindByValue(Bien.P_Color_ID));
                Txt_Costo_Parte.Text = Bien.P_Costo_Actual.ToString();
                Txt_Fecha_Adquisicion_Parte.Text = String.Format("{0:dd 'de' MMMMMMMMMMMMMMM 'de' yyyy}", Convert.ToDateTime(Bien.P_Fecha_Adquisicion));
                Cmb_Estatus_Parte.SelectedIndex = Cmb_Estatus_Parte.Items.IndexOf(Cmb_Estatus_Parte.Items.FindByValue(Bien.P_Estatus));
                Cmb_Estado_Parte.SelectedIndex = Cmb_Estado_Parte.Items.IndexOf(Cmb_Estado_Parte.Items.FindByValue(Bien.P_Estado));
                Txt_Comentarios_Parte.Text = Bien.P_Observaciones;
                System.Threading.Thread.Sleep(1000);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Productos_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Productos del
    ///             Modal de Busqueda.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Julio/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Listado_Proveedores.SelectedIndex > (-1))
            {
                String Proveedor_ID = Grid_Listado_Proveedores.SelectedRow.Cells[1].Text.Trim();

                Cls_Cat_Com_Proveedores_Negocio Proveedor_Negocio = new Cls_Cat_Com_Proveedores_Negocio();
                Proveedor_Negocio.P_Proveedor_ID = Proveedor_ID;
                DataTable Dt_Proveedor_Seleccionado = Proveedor_Negocio.Consulta_Datos_Proveedores();
                if (Dt_Proveedor_Seleccionado != null && Dt_Proveedor_Seleccionado.Rows.Count > 0)
                {
                    //Hdf_Proveedor_ID.Value = Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Proveedor_ID].ToString().Trim();
                    Hdf_Proveedor_ID.Value = Proveedor_ID.Trim();

                    if (Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim() != "")
                        Txt_Nombre_Proveedor.Text = Dt_Proveedor_Seleccionado.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString().Trim();
                    else
                        Txt_Nombre_Proveedor.Text = "SIN NOMBRE";

                    //Upd_Panel.Update();
                    Mpe_Proveedores_Cabecera.Hide();
                }
                System.Threading.Thread.Sleep(500);
                //Grid_Listado_Productos.SelectedIndex = (-1);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Listado_Proveedores_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Proveedores del Modal de Busqueda
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 23/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Listado_Proveedores_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Listado_Proveedores.SelectedIndex = (-1);
            Llenar_Grid_Proveedores(e.NewPageIndex);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resultados_Multiples_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Seleccion del GridView de Bienes del
    ///             Modal de Resultados Multiples.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************
    protected void Grid_Resultados_Multiples_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Resultados_Multiples.SelectedIndex > (-1))
            {
                Limpiar_Generales();
                String Bien_Mueble_Seleccionado_ID = Grid_Resultados_Multiples.SelectedRow.Cells[1].Text.Trim();
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Bien_Mueble.P_Bien_Mueble_ID = Bien_Mueble_Seleccionado_ID;
                Bien_Mueble = Bien_Mueble.Consultar_Detalles_Bien_Mueble();
                Mostrar_Detalles_Bien_Mueble(Bien_Mueble);
                Grid_Listado_Bienes.SelectedIndex = -1;
                Mpe_Multiples_Resultados.Hide();
                System.Threading.Thread.Sleep(500);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Resultados_Multiples_PageIndexChanging
    ///DESCRIPCIÓN: Maneja la paginación del GridView de Resultados Multiples.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Resultados_Multiples_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Resultados_Multiples.PageIndex = e.NewPageIndex;
            Btn_Buscar_Click(Btn_Buscar, null);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_PageIndexChanging
    ///DESCRIPCIÓN: Maneja el evento de cambio de Página del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = e.NewPageIndex;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del GridView de Busqueda
    ///             de empleados.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Busqueda_Empleados_Resguardo_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Grid_Busqueda_Empleados_Resguardo.SelectedIndex > (-1))
            {
                String Empleado_Seleccionado_ID = Grid_Busqueda_Empleados_Resguardo.SelectedRow.Cells[1].Text.Trim();
                Cls_Cat_Empleados_Negocios Empleado_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleado_Negocio.P_Empleado_ID = Empleado_Seleccionado_ID.Trim();
                DataTable Dt_Datos_Empleado = Empleado_Negocio.Consulta_Empleados_General();
                if (Hdf_Tipo_Busqueda.Value == null || Hdf_Tipo_Busqueda.Value.Trim().Length == 0)
                {
                    String Dependencia_ID = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Dependencias.Campo_Dependencia_ID].ToString() : null;
                    Llenar_Combo_Dependencias("");//Llenar el combo dependencias con todas las dependencias registradas para buscar la del empleado seleccionado del grid
                    Int32 Index_Combo = (-1);
                    if (Dependencia_ID != null && Dependencia_ID.Trim().Length > 0)
                    {
                        Index_Combo = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Dependencia_ID));
                        if (Index_Combo > (-1))
                        {
                            if (Index_Combo == Cmb_Dependencias.SelectedIndex)
                            {
                                Cmb_Empleados.SelectedIndex = Cmb_Empleados.Items.IndexOf(Cmb_Empleados.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));
                            }
                            else
                            {
                                Ubicar_Gerencia(Dependencia_ID);
                                Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue);
                                Cmb_Dependencias.SelectedIndex = Cmb_Dependencias.Items.IndexOf(Cmb_Dependencias.Items.FindByValue(Dependencia_ID));
                                Consultar_Empleados(Dependencia_ID);
                                Cmb_Empleados.SelectedIndex = Cmb_Empleados.Items.IndexOf(Cmb_Empleados.Items.FindByValue(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString()));
                            }
                        }
                    }
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("OPERADOR"))
                {
                    Hdf_Resguardo_Completo_Operador.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : "";
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Operador.Text = Texto.Trim();
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("FUNCIONARIO_RECIBE"))
                {
                    Hdf_Resguardo_Completo_Funcionario_Recibe.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Funcionario_Recibe.Text = Texto.Trim();
                }
                else if (Hdf_Tipo_Busqueda.Value.Trim().Equals("AUTORIZO"))
                {
                    Hdf_Resguardo_Completo_Autorizo.Value = (!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString() : null;
                    String Texto = ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString() : "");
                    Texto = Texto.Trim() + " " + ((!String.IsNullOrEmpty(Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString())) ? Dt_Datos_Empleado.Rows[0][Cat_Empleados.Campo_Nombre].ToString() : "");
                    Txt_Resguardo_Completo_Autorizo.Text = Texto.Trim();
                }

                MPE_Resguardante.Hide();
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Ubicar_Gerencia
    ///DESCRIPCIÓN: Coloca el combo gerencia en la que le corresponde al empleado seleccionado del grid de búsqueda
    ///PROPIEDADES: el identificador de la dependencia para buscar la gerencia
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Ubicar_Gerencia(String Dependencia_ID)
    {

        Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combos = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
        //SE LLENA EL COMBO DE DEPENDENCIAS
        Combos.P_Tipo_DataTable = "DEPENDENCIAS";// Se llena el combo de dependencias para obtener la gerencia ID y asi colocar la gerencia a la que pertence el empleado seleccionado en la búsqueda
        DataTable Dependencias = Combos.Consultar_DataTable();
        DataRow DR_Fila;
        String Gerencia_ID = "";//variable para almacenar la gerencia_ID cuando se encuentre
        for (int i = 0; i < Dependencias.Rows.Count; i++)
        {//Ciclo para encontrar la dependencia_ID y obtener la gerencia_id cuando se encuentre
            DR_Fila = Dependencias.Rows[i];
            if (DR_Fila["DEPENDENCIA_ID"].ToString().Equals(Dependencia_ID))
            {
                Gerencia_ID = DR_Fila["GERENCIA_ID"].ToString();
            }
        }
        if (!String.IsNullOrEmpty(Gerencia_ID))//se valida que contenga valor la variable gerencia_id
            Cmb_Gerencias.SelectedIndex = Cmb_Gerencias.Items.IndexOf(Cmb_Gerencias.Items.FindByValue(Gerencia_ID));//Se coloca el combo gerencia en la posicion encontrada encontrada

    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Grid_Archivos_RowDataBound
    ///DESCRIPCIÓN: Maneja el evento de RowDataBound del Grid de Archivos
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Grid_Archivos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            ImageButton Boton = (ImageButton)e.Row.FindControl("Btn_Ver_Archivo");
            Boton.CommandArgument = e.Row.Cells[0].Text.Trim();
        }
    }

    #endregion

    #region Eventos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Partida_Reparacion_Click
    ///DESCRIPCIÓN: Btn_Busqueda_Partida_Reparacion_Click
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Busqueda_Partida_Reparacion_Click(object sender, ImageClickEventArgs e)
    {
        Hdf_Partida_Reparacion_ID.Value = "";
        Txt_Nombre_Partida_Reparacion.Text = "";
        if (Txt_Clave_Partida_Reparacion.Text.Trim().Length == 4)
        {
            Cargar_Nombre_Partida(Txt_Clave_Partida_Reparacion.Text.Trim(), "CLAVE");
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "La Clave de la Partida para Reparación no es correcta.";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }


    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ejecutar_Busqueda_Proveedores_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda de los proveedores.
    ///PARAMETROS:     
    ///CREO:        Salvador Hernández Ramírez
    ///FECHA_CREO:  22/Septiembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ejecutar_Busqueda_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Llenar_Grid_Proveedores(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Busqueda_Resguardantes_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Dependencias
    ///             del Modal de Busqueda (Parte de Resguardantes).
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Busqueda_Resguardantes_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Cmb_Busqueda_Resguardantes_Dependencias.SelectedIndex > 0)
            {
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Combo = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Combo.P_Tipo_DataTable = "EMPLEADOS";
                Combo.P_Dependencia_ID = Cmb_Busqueda_Resguardantes_Dependencias.SelectedItem.Value.Trim();
                DataTable Tabla = Combo.Consultar_DataTable();
                Llenar_Combo_Empleados_Busqueda(Tabla);
            }
            else
            {
                DataTable Tabla = new DataTable();
                Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                Tabla.Columns.Add("NOMBRE", Type.GetType("System.String"));
                Llenar_Combo_Empleados_Busqueda(Tabla);
            }
            MPE_Busqueda_Bien_Mueble.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Asy_Cargar_Archivo_Complete
    ///DESCRIPCIÓN: Maneja el evento de cuando se cargo completamente el archivo
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 01/Noviembre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        try
        {
            string savePath = MapPath("~/Uploads/" + Path.GetFileName(e.FileName));//Se obtiene la ruta del archivo cargado
            //Se valida la extension del archivo
            if (Path.GetExtension(e.FileName).Contains(".jpg") || Path.GetExtension(e.FileName).Contains(".JPG") || Path.GetExtension(e.FileName).Contains(".jpeg") || Path.GetExtension(e.FileName).Contains(".JPEG"))
                Archivo = (AsyncFileUpload)sender; //Se almacena el archivo en la variable global llamada Archivo 
        }
        catch (Exception ex)
        {
            Div_Contenedor_Msj_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Error al cargar la imagen " + ex.Message;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Prepara y Actualiza un Bien Mueble con uno o mas resguardantes.
    ///PROPIEDADES:     
    ///CREO:                Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO:         02/Diciembre/2010 
    ///MODIFICO:           Salvador Hernández Ramírez
    ///FECHA_MODIFICO      24-Agosto-11
    ///CAUSA_MODIFICACIÓN  Se agregó la condición para que se actualice el resguardo o recibo
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText.Equals("Modificar"))
            {
                if (Hdf_Bien_Mueble_ID.Value.Trim().Length > 0)
                {
                    if (!Cmb_Estatus.SelectedItem.Value.Equals("DEFINITIVA"))
                    {
                        Configuracion_Formulario(true);
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "";
                        Lbl_Mensaje_Error.Text = "El Estatus del Bien Mueble es \"BAJA DEFINITIVA\" y no puede ser actualizado el Bien";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                    Lbl_Mensaje_Error.Text = "Seleccionar el Bien a Modificar";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
            else
            { // Si se va a guardar
                if (Validar_Componentes_Generales())
                {
                    //if (Validar_Baja_Bien()) { 
                    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Bien_Mueble.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value.Trim();
                    Bien_Mueble.P_Gerencia_ID = Cmb_Gerencias.SelectedValue.Trim();
                    Bien_Mueble.P_Dependencia_ID = Cmb_Dependencias.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Nombre_Producto = Txt_Nombre_Producto.Text.Trim();
                    Bien_Mueble.P_Numero_Inventario = Txt_Numero_Inventario.Text.Trim();
                    Bien_Mueble.P_Numero_Inventario_Anterior = Txt_Invenario_Anterior.Text.Trim();

                    if (Cmb_Marca.SelectedIndex != 0)
                        Bien_Mueble.P_Marca_ID = Cmb_Marca.SelectedItem.Value.Trim();
                    else
                        Bien_Mueble.P_Marca_ID = "";

                    if (Cmb_Zonas.SelectedIndex != 0)
                        Bien_Mueble.P_Zona = Cmb_Zonas.SelectedItem.Value.Trim();
                    else
                        Bien_Mueble.P_Zona = "";

                    if (Txt_Cuenta_Contable.Text.Trim().Length > 0 && Hdf_Cuenta_Contable_ID.Value.Trim().Length > 0) Bien_Mueble.P_Cuenta_Contable_ID = Hdf_Cuenta_Contable_ID.Value;
                    if (Txt_Cuenta_Gasto.Text.Trim().Length > 0 && Hdf_Cuenta_Gasto_ID.Value.Trim().Length > 0) Bien_Mueble.P_Cuenta_Gasto_ID = Hdf_Cuenta_Gasto_ID.Value;
                    Bien_Mueble.P_Partida_ID = Hdf_Partida_Reparacion_ID.Value;
                    Bien_Mueble.P_Clase_Activo_ID = Cmb_Clase_Activo.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Clasificacion_ID = Cmb_Tipo_Activo.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Modelo = Txt_Modelo.Text.Trim();
                    Bien_Mueble.P_Garantia = Txt_Garantia.Text.Trim();
                    Bien_Mueble.P_Material_ID = Cmb_Materiales.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Tipo_Baja_ID = Cmb_Tipo_Baja.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Color_ID = Cmb_Colores.SelectedItem.Value.Trim();
                    if (!String.IsNullOrEmpty(Txt_Fecha_Adquisicion.Text))
                        Bien_Mueble.P_Fecha_Adquisicion_ = Convert.ToDateTime(Txt_Fecha_Adquisicion.Text.Trim());
                    if (!String.IsNullOrEmpty(Txt_Fecha_Inventario.Text))
                        Bien_Mueble.P_Fecha_Inventario_ = Convert.ToDateTime(Txt_Fecha_Inventario.Text.Trim());
                    if (Hdf_Actualizar_Costo.Value == "SI") Bien_Mueble.P_Actualizar_Costos = true;
                    Bien_Mueble.P_Costo_Actual = Convert.ToDouble(Txt_Costo_Actual.Text.Trim());
                    Bien_Mueble.P_Costo_Inicial = Convert.ToDouble(Txt_Costo_Inicial.Text.Trim());
                    Bien_Mueble.P_Factura = Txt_Factura.Text.Trim();
                    Bien_Mueble.P_Numero_Serie = Txt_Numero_Serie.Text.Trim();
                    Bien_Mueble.P_Estatus = Cmb_Estatus.SelectedItem.Value.Trim();
                    if (!Bien_Mueble.P_Estatus.Equals("VIGENTE"))
                    {
                        Bien_Mueble.P_Motivo_Baja = Txt_Motivo_Baja.Text;
                    }
                    else
                    {
                        Bien_Mueble.P_Motivo_Baja = "";
                    }
                    if (Cmb_Bien_Inmueble.SelectedIndex > 0) Bien_Mueble.P_Bien_Inmueble_ID = Cmb_Bien_Inmueble.SelectedItem.Value;
                    Bien_Mueble.P_Procedencia = Cmb_Procedencia.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Proveedor_ID = Hdf_Proveedor_ID.Value.Trim();
                    Bien_Mueble.P_Razon_Social_Proveedor = Txt_Nombre_Proveedor.Text.Trim();
                    Bien_Mueble.P_Estado = Cmb_Estado.SelectedItem.Value.Trim();
                    Bien_Mueble.P_Observaciones = Txt_Observaciones.Text.Trim();
                    Bien_Mueble.P_Proveniente = Hdf_Proveniente.Value;
                    Bien_Mueble.P_Empleado_Autoriza = Hdf_Resguardo_Completo_Autorizo.Value;
                    Bien_Mueble.P_Empleado_Entrega = Hdf_Resguardo_Completo_Operador.Value;
                    Bien_Mueble.P_Empleado_Revisa = Hdf_Resguardo_Completo_Funcionario_Recibe.Value;

                    if (Session["Tabla_Archivos"] != null)
                    {
                        Bien_Mueble.P_Dt_Historial_Archivos = (DataTable)Session["Tabla_Archivos"];
                    }
                    else
                    {
                        Bien_Mueble.P_Dt_Historial_Archivos = new DataTable();
                    }

                    Bien_Mueble.P_Resguardantes = (DataTable)Session["Dt_Resguardantes"];
                    Bien_Mueble.P_Usuario_Nombre = Cls_Sessiones.Nombre_Empleado;
                    Bien_Mueble.P_Usuario_ID = Cls_Sessiones.Empleado_ID;

                    if (Session["OPERACION_BM"] != null)
                        Bien_Mueble.P_Operacion = Session["OPERACION_BM"].ToString().Trim();

                    Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio Tmp_Negocio = new Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio();
                    Tmp_Negocio.P_Tipo_Bien = "BIEN_MUEBLE";
                    Tmp_Negocio.P_Bien_ID = Hdf_Bien_Mueble_ID.Value.Trim();
                    Tmp_Negocio.P_BM_Actualizado = Bien_Mueble;
                    Bien_Mueble.P_Dt_Cambios = Tmp_Negocio.Obtener_Tabla_Cambios();
                    Bien_Mueble.Modificar_Bien_Mueble();  // Se instancia el método que se utilizará para modificar el bien mueble

                    Bien_Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Bien_Mueble.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value.Trim();
                    Bien_Mueble = Bien_Mueble.Consultar_Detalles_Bien_Mueble();

                    Guardar_Archivos();
                    Limpiar_Generales();
                    Configuracion_Formulario(false);
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Actualización de Bienes Muebles", "alert('Actualización de Bien Mueble Exitosa');", true);
                    Mostrar_Detalles_Bien_Mueble(Bien_Mueble);
                    //}
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operación que esta en proceso (Alta o Actualizar) o Sale
    ///             del Formulario.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:    
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.AlternateText.Equals("Salir"))
        {
            Session["Dt_Resguardantes"] = null;
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
        else
        {
            Cls_Ope_Pat_Com_Bienes_Muebles_Negocio BI_Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
            BI_Negocio.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value;
            BI_Negocio = BI_Negocio.Consultar_Detalles_Bien_Mueble();
            Limpiar_Generales();
            Mostrar_Detalles_Bien_Mueble(BI_Negocio);
            Configuracion_Formulario(false);
            Tab_Contenedor_Pestagnas.TabIndex = 0;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Avanzada_Click
    ///DESCRIPCIÓN: Carga el Modal Popup de Busqueda Avanzada.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 11/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Avanzada_Click(object sender, EventArgs e)
    {
        Div_Contenedor_Msj_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Pnl_Busqueda_Bien_Mueble.Visible = true;
        MPE_Busqueda_Bien_Mueble.Show();
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Click
    ///DESCRIPCIÓN: Carga el Modal Popup de Busqueda Directa.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 13/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Txt_Busqueda.Text.Trim().Length > 0 || Txt_Busqueda_Anterior.Text.Trim().Length > 0 || Txt_Busqueda_No_Inventario_CONAC.Text.Trim().Length > 0)
            {
                Limpiar_Generales();
                Boolean Multiples_Resultados = false;
                String No_Inventario_SIAS = Txt_Busqueda.Text.Trim();
                String No_Inventario_Anterior = Txt_Busqueda_Anterior.Text.Trim();
                String Clave_Inventario_CONAC = Txt_Busqueda_No_Inventario_CONAC.Text.Trim();
                if (No_Inventario_Anterior.Trim().Length > 0 || No_Inventario_SIAS.Trim().Length > 0 || Clave_Inventario_CONAC.Trim().Length > 0)
                {
                    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble_Negocio = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Bien_Mueble_Negocio.P_Tipo_DataTable = "BIENES";
                    Bien_Mueble_Negocio.P_Numero_Inventario_Anterior = No_Inventario_Anterior.Trim();
                    Bien_Mueble_Negocio.P_Numero_Inventario = No_Inventario_SIAS.Trim();

                    if (!String.IsNullOrEmpty(Clave_Inventario_CONAC))
                        if (Clave_Inventario_CONAC.Trim().Length == 12)
                            Bien_Mueble_Negocio.P_Numero_Inventario = Convert.ToInt32(Clave_Inventario_CONAC.Substring(4)).ToString();

                    DataTable Dt_Temporal = Bien_Mueble_Negocio.Consultar_DataTable();
                    if (Dt_Temporal.Rows.Count > 1)
                    {
                        Multiples_Resultados = true;
                        Llenar_Grid_Resultados_Multiples(Dt_Temporal);
                        Mpe_Multiples_Resultados.Show();
                        return;
                    }
                    else if (Dt_Temporal.Rows.Count == 1)
                    {
                        No_Inventario_Anterior = Dt_Temporal.Rows[0]["NO_INVENTARIO_ANTERIOR"].ToString().Trim();
                    }
                }
                if (!Multiples_Resultados)
                {
                    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Bien_Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Bien_Mueble.P_Numero_Inventario = No_Inventario_SIAS;
                    Bien_Mueble.P_Numero_Inventario_Anterior = No_Inventario_Anterior;
                    Bien_Mueble.P_Buscar_Numero_Inventario = true;
                    Bien_Mueble = Bien_Mueble.Consultar_Detalles_Bien_Mueble();

                    String Operacion = Bien_Mueble.P_Operacion;
                    Txt_Resguardo_Recibo.Text = Operacion;

                    if (Operacion != null)
                        Session["OPERACION_BM"] = Operacion;
                    else
                        Session["OPERACION_BM"] = null;

                    if (Bien_Mueble.P_Bien_Mueble_ID != null && Bien_Mueble.P_Bien_Mueble_ID.Trim().Length > 0)
                    {
                        Mostrar_Detalles_Bien_Mueble(Bien_Mueble);
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = HttpUtility.HtmlDecode("No se encontro un Bien Mueble con el Número de Inventario.");
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                Lbl_Mensaje_Error.Text = "Introducir el Número de Inventario a Buscar";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Archivo_Click
    ///DESCRIPCIÓN: Limpia los componentes del MPE de Cancelación de Vacuna
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 16/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Ver_Archivo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            ImageButton Boton = (ImageButton)sender;
            String Archivo_Bien_ID = Boton.CommandArgument;
            for (Int32 Contador = 0; Contador < Grid_Archivos.Rows.Count; Contador++)
            {
                if (Grid_Archivos.Rows[Contador].Cells[0].Text.Trim().Equals(Archivo_Bien_ID))
                {
                    String Archivo = "../../" + Ope_Pat_Archivos_Bienes.Campo_Ruta_Fisica_Archivos + "/BIENES_MUEBLES/" + Hdf_Bien_Mueble_ID.Value + "/" + Grid_Archivos.Rows[Contador].Cells[3].Text.Trim();
                    if (File.Exists(Server.MapPath(Archivo)))
                    {
                        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Archivo_Archivos", "window.open('" + Archivo + "','Window_Archivo','left=0,top=0')", true);
                        break;
                    }
                    else
                    {
                        Lbl_Ecabezado_Mensaje.Text = "El Archivo no esta disponible o fue eliminado";
                        Lbl_Mensaje_Error.Text = "";
                        Div_Contenedor_Msj_Error.Visible = true;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Lanzar_Mpe_Proveedores_Click
    ///DESCRIPCIÓN: Lanza buscador de producto.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 18/Marzo/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Lanzar_Mpe_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Div_Contenedor_Msj_Error.Visible = false;
            Lbl_Mensaje_Error.Text = "";
            //Pnl_Busqueda.Visible = true;
            Mpe_Proveedores_Cabecera.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN: Genera el reporte detallado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Generar_Reporte_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.AlternateText.Equals("Modificar"))
            {
                if (Hdf_Bien_Mueble_ID.Value.Trim().Length > 0)
                {
                    Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                    Mueble.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value;

                    if (Session["OPERACION_BM"] != null)
                        Mueble.P_Operacion = Session["OPERACION_BM"].ToString().Trim();

                    Mueble = Mueble.Consultar_Detalles_Bien_Mueble();
                    Llenar_DataSet_Resguardos_Bienes(Mueble);
                }
                else
                {
                    Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                    Lbl_Mensaje_Error.Text = "Tener un Bien para Reimprimir el Resguardo";
                    Div_Contenedor_Msj_Error.Visible = true;
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Dependencias_SelectedIndexChanged
    ///DESCRIPCIÓN:          Evento utilizado para obtener el identificador de la dependencia que se selecciono y acceder al metodo para llenar el combo de los empleados
    ///PROPIEDADES:     
    ///                      1.  Dependencia_ID. el identificador de la dependencia en base a la que se va hacer la consulta
    ///              
    ///CREO:                 Salvador Hernández Ramírez
    ///FECHA_CREO: 02/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Dependencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        String Depencencia_Id = Cmb_Dependencias.SelectedItem.Value.Trim();
        Consultar_Empleados(Depencencia_Id);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Grencias_SelectedIndexChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Selección del Combo de Gerencias
    ///PROPIEDADES:     
    ///CREO: Luis Daniel Guzmán Malagón.
    ///FECHA_CREO: 30/Octubre/2012
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Cmb_Gerencias_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Combo_Dependencias(Cmb_Gerencias.SelectedValue.ToString());
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Generar_Reporte_Click
    ///DESCRIPCIÓN: Genera el reporte detallado.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 04/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Imprimir_Codigo_Barras_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Hdf_Bien_Mueble_ID.Value.Trim().Length > 0)
            {
                Cls_Ope_Pat_Com_Bienes_Muebles_Negocio Mueble = new Cls_Ope_Pat_Com_Bienes_Muebles_Negocio();
                Mueble.P_Bien_Mueble_ID = Hdf_Bien_Mueble_ID.Value;
                Mueble = Mueble.Consultar_Detalles_Bien_Mueble();
                String BarCode = "";
                if (!String.IsNullOrEmpty(Mueble.P_Clase_Activo_ID))
                {
                    Cls_Cat_Pat_Com_Clases_Activo_Negocio CA_Negocio = new Cls_Cat_Pat_Com_Clases_Activo_Negocio();
                    CA_Negocio.P_Clase_Activo_ID = Mueble.P_Clase_Activo_ID.Trim();
                    CA_Negocio.P_Tipo_DataTable = "CLASES_ACTIVOS";
                    DataTable Dt_Clase_Activo = CA_Negocio.Consultar_DataTable();
                    if (Dt_Clase_Activo != null)
                    {
                        if (Dt_Clase_Activo.Rows.Count > 0)
                        {
                            BarCode = Dt_Clase_Activo.Rows[0]["CLAVE"].ToString().Trim();
                        }
                    }
                }
                if (String.IsNullOrEmpty(BarCode)) BarCode = "0000";
                BarCode += String.Format("{0:00000000}", Convert.ToInt32(Mueble.P_Numero_Inventario));
                String Codigo_Barras_Embebido = JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.Obtener_Codigo_Barras_Embebido(JAPAMI.Control_Patrimonial_Codigo_Barras.Ayudante.Cls_Ope_Pat_Com_Codigo_Barras.CrearCodigo(BarCode.Trim()), System.Drawing.Imaging.ImageFormat.Jpeg);
                Session["Tamano_Codigo_Barras"] = Cmb_Tamano_Etiqueta.SelectedItem.Value.Trim();
                Session["Numero_Codigo_Barras"] = BarCode;
                Session["Imagen_Codigo_Barras"] = Codigo_Barras_Embebido;
                String Pagina = "../Control_Patrimonial/Frm_Ope_Pat_Mostrar_Codigo_Barras.aspx";
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt", "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "Es necesario.";
                Lbl_Mensaje_Error.Text = "Tener un Bien para Reimprimir el Codigo de Barras";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = false;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Nombre_Proveedor_Buscar_TextChanged
    ///DESCRIPCIÓN: Maneja el evento de cambio de Texto del Nombre del proveedor.
    ///PARAMETROS:     
    ///CREO:        Salvador Hernández Ramírez
    ///FECHA_CREO:  08/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Txt_Nombre_Proveedor_Buscar_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Llenar_Grid_Proveedores(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #region Resguardos

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Resguardante_Click
    ///DESCRIPCIÓN: Agrega una nuevo Empleado Resguardante para este Bien Mueble.
    ///             (No aun en la Base de Datos)
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        if (Validar_Componentes_Resguardos())
        {
            DataTable Tabla = (DataTable)Grid_Resguardantes.DataSource;
            if (Tabla == null)
            {
                if (Session["Dt_Resguardantes"] == null)
                {
                    Tabla = new DataTable("Resguardos");
                    Tabla.Columns.Add("BIEN_RESGUARDO_ID", Type.GetType("System.String"));
                    Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
                    Tabla.Columns.Add("NO_EMPLEADO", Type.GetType("System.String"));
                    Tabla.Columns.Add("NOMBRE_EMPLEADO", Type.GetType("System.String"));
                    Tabla.Columns.Add("COMENTARIOS", Type.GetType("System.String"));
                }
                else
                {
                    Tabla = (DataTable)Session["Dt_Resguardantes"];
                }
            }
            if (!Buscar_Clave_DataTable(Cmb_Empleados.SelectedItem.Value, Tabla, 1))
            {
                Cls_Cat_Empleados_Negocios Empleados_Negocio = new Cls_Cat_Empleados_Negocios();
                Empleados_Negocio.P_Empleado_ID = Cmb_Empleados.SelectedItem.Value;
                DataTable Dt_Empleado = Empleados_Negocio.Consulta_Datos_Empleado();
                if (Dt_Empleado != null && Dt_Empleado.Rows.Count > 0)
                {
                    DataRow Fila = Tabla.NewRow();
                    Fila["BIEN_RESGUARDO_ID"] = 0;
                    Fila["EMPLEADO_ID"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_Empleado_ID].ToString().Trim();
                    Fila["NO_EMPLEADO"] = Dt_Empleado.Rows[0][Cat_Empleados.Campo_No_Empleado].ToString().Trim();
                    Fila["NOMBRE_EMPLEADO"] = HttpUtility.HtmlDecode(Cmb_Empleados.SelectedItem.Text);
                    Fila["COMENTARIOS"] = HttpUtility.HtmlDecode(Txt_Cometarios.Text.Trim());
                    Tabla.Rows.Add(Fila);
                }
                Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
                Grid_Resguardantes.SelectedIndex = (-1);
                Cmb_Empleados.SelectedIndex = 0;
                Txt_Cometarios.Text = "";
            }
            else
            {
                Lbl_Ecabezado_Mensaje.Text = "El Empleado ya esta Agregado.";
                Lbl_Mensaje_Error.Text = "";
                Div_Contenedor_Msj_Error.Visible = true;
            }
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Quitar_Resguardante_Click
    ///DESCRIPCIÓN: Quita un Empleado resguardante para este bien (No en la Base de datos
    ///             aun).
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 02/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Quitar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        if (Grid_Resguardantes.Rows.Count > 0 && Grid_Resguardantes.SelectedIndex > (-1))
        {
            Int32 Registro = ((Grid_Resguardantes.PageIndex) * Grid_Resguardantes.PageSize) + (Grid_Resguardantes.SelectedIndex);
            if (Session["Dt_Resguardantes"] != null)
            {
                DataTable Tabla = (DataTable)Session["Dt_Resguardantes"];
                Tabla.Rows.RemoveAt(Registro);
                Session["Dt_Resguardantes"] = Tabla;
                Grid_Resguardantes.SelectedIndex = (-1);
                Llenar_Grid_Resguardantes(Grid_Resguardantes.PageIndex, Tabla);
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "Debe seleccionar el Registro que se desea Quitar.";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    #region Busqueda

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Filtros_Buscar_Datos_Click
    ///DESCRIPCIÓN: Maneja el Evento del Boton para realizar la Limpieza de los filtros
    ///             para la busqueda por parte de los Datos Generales.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Limpiar_Filtros_Buscar_Datos_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Txt_Busqueda_Producto.Text = "";
            Cmb_Busqueda_Dependencias.SelectedIndex = 0;
            Txt_Busqueda_Modelo.Text = "";
            Cmb_Busqueda_Marca.SelectedIndex = 0;
            Cmb_Busqueda_Estatus.SelectedIndex = 0;
            Txt_Busqueda_Numero_Serie.Text = "";
            Txt_Busqueda_Factura.Text = "";
            MPE_Busqueda_Bien_Mueble.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Datos_Click
    ///DESCRIPCIÓN: Maneja el Evento del Boton para realizar la Busqueda de los
    ///             Datos Generales.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Buscar_Datos_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["FILTRO_BUSQUEDA"] = "DATOS_GENERALES";
            Llenar_Grid_Listado_Bienes(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_Filtros_Buscar_Resguardante_Click
    ///DESCRIPCIÓN: Maneja el Evento del Boton para realizar la Limpieza de los filtros
    ///             para la busqueda por parte de los Listados.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************  
    protected void Btn_Limpiar_Filtros_Buscar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Txt_Busqueda_No_Empleado_Resguardante.Text = "";
            Txt_Busqueda_RFC_Resguardante.Text = "";
            Cmb_Busqueda_Nombre_Resguardante.SelectedIndex = 0;
            Cmb_Busqueda_Resguardantes_Dependencias.SelectedIndex = 0;
            DataTable Tabla = new DataTable();
            Tabla.Columns.Add("EMPLEADO_ID", Type.GetType("System.String"));
            Tabla.Columns.Add("NOMBRE", Type.GetType("System.String"));
            Llenar_Combo_Empleados_Busqueda(Tabla);
            MPE_Busqueda_Bien_Mueble.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Resguardante_Click
    ///DESCRIPCIÓN: Maneja el Evento del Boton para realizar la Busqueda de los
    ///             Reguardante
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Diciembre/2010 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Buscar_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Session["FILTRO_BUSQUEDA"] = "RESGUARDANTES";
            Llenar_Grid_Listado_Bienes(0);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    #region "Busqueda Resguardantes"

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Avanzada_Resguardante_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Avanzada_Resguardante_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Operador_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "OPERADOR";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Operador_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Funcionario_Recibe_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "FUNCIONARIO_RECIBE";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Resguardo_Completo_Autorizo_Click
    ///DESCRIPCIÓN: Lanza la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Resguardo_Completo_Autorizo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Hdf_Tipo_Busqueda.Value = "AUTORIZO";
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Busqueda_Empleados_Click
    ///DESCRIPCIÓN: Ejecuta la Busqueda Avanzada para el Resguardante.
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 24/Octubre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************    
    protected void Btn_Busqueda_Empleados_Click(object sender, EventArgs e)
    {
        try
        {
            Grid_Busqueda_Empleados_Resguardo.PageIndex = 0;
            Llenar_Grid_Busqueda_Empleados_Resguardo();
            MPE_Resguardante.Show();
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Ver_Informacion_Resguardo_Click
    ///DESCRIPCIÓN: Manda Visualizar los Comentarios del Resguardo.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 30/Septiembre/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************
    protected void Btn_Ver_Informacion_Resguardo_Click(object sender, ImageClickEventArgs e)
    {
        ImageButton Btn_Ver_Informacion_Resguardo = (ImageButton)sender;
        String Comentarios = "Sin Comentarios";
        if (Btn_Ver_Informacion_Resguardo.CommandArgument.Trim().Length > 0) { Comentarios = "Comentarios: " + Btn_Ver_Informacion_Resguardo.CommandArgument.Trim(); }
        ScriptManager.RegisterStartupScript(this, this.GetType(), "GACO", "alert('" + Comentarios + "');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Limpiar_FileUpload_Click
    ///DESCRIPCIÓN: Limpia el FileUpload que carga los archivos
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 10/Junio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Limpiar_FileUpload_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Agregar_Archivo_Click
    ///DESCRIPCIÓN: Se agrega un archivo a la sesion
    ///PARAMETROS:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 01/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Agregar_Archivo_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Tabla_Archivos;
        if (Session["Tabla_Archivos"] != null)
        {
            Tabla_Archivos = (DataTable)Session["Tabla_Archivos"];
        }
        else
        {
            Tabla_Archivos = Generar_Tabla_Archivos();
        }
        String Checksum_Archivo = null;

        try
        {
            if (Validar_Archivo())
            {
                HashAlgorithm sha = HashAlgorithm.Create("SHA1");//No se para que demonios hace esto
                Checksum_Archivo = BitConverter.ToString(sha.ComputeHash(AFU_Archivo.FileBytes));   //obtener checksum del archivo
                Dictionary<String, Byte[]> Diccionario_Archivos = Obtener_Diccionario_Archivos();   //obtener diccionario checksum-archivo

                String Directorio = "../../" + Ope_Pat_Archivos_Bienes.Campo_Ruta_Fisica_Archivos + "/BIENES_MUEBLES/" + Hdf_Bien_Mueble_ID.Value.ToString().Trim();
                String Ruta_archivo = Server.MapPath(Directorio);
                if (!Directory.Exists(Ruta_archivo))
                {
                    Directory.CreateDirectory(Ruta_archivo);
                }
                Ruta_archivo = @Directorio + AFU_Archivo.FileName;

                if (!Diccionario_Archivos.ContainsKey(Checksum_Archivo)) //si el checksum no esta en el diccionario, agregarlo y guardar en variable de sesion
                {
                    Diccionario_Archivos.Add(Checksum_Archivo, AFU_Archivo.FileBytes);
                    Session["Diccionario_Archivos"] = Diccionario_Archivos;

                    DataRow Nueva_Fila = Tabla_Archivos.NewRow();
                    Nueva_Fila["ARCHIVO_BIEN_ID"] = 0;
                    Nueva_Fila["BIEN_ID"] = Hdf_Bien_Mueble_ID.Value;
                    Nueva_Fila["TIPO"] = Cmb_Tipo_Activo.SelectedItem.ToString().Trim();
                    Nueva_Fila["FECHA"] = DateTime.Today;
                    Nueva_Fila["ARCHIVO"] = AFU_Archivo.FileName;
                    Nueva_Fila["TIPO_ARCHIVO"] = "NORMAL";
                    Nueva_Fila["DESCRIPCION_ARCHIVO"] = "";
                    Nueva_Fila["CHECKSUM"] = Checksum_Archivo;
                    Nueva_Fila["RUTA_ARCHIVO"] = @AFU_Archivo.PostedFile.FileName;
                    Tabla_Archivos.Rows.Add(Nueva_Fila);
                }
                Llenar_Grid_Historial_Archivos(0, Tabla_Archivos);
                Remover_Sesiones_Control_AsyncFileUpload(AFU_Archivo.ClientID);
            }
        }
        catch (Exception Ex)
        {
            Lbl_Ecabezado_Mensaje.Text = Ex.Message;
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }



    protected void Btn_Generar_Reporte_Cambios_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Hdf_Bien_Mueble_ID.Value))
        {
            Generar_Reporte_Cambios();
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "No hay Bien Seleccionado";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte_Cambios
    ///DESCRIPCIÓN: Generar el reporte de Cambios.
    ///PROPIEDADES:     
    ///CREO: Francisco Antonio Gallardo Castañeda.
    ///FECHA_CREO: 11/enero/2013
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************
    private void Generar_Reporte_Cambios()
    {
        Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio Negocio = new Cls_Ope_Pat_Manejo_Historial_Cambios_Negocio();
        Negocio.P_Tipo_Bien = "BIEN_MUEBLE";
        Negocio.P_Bien_ID = Hdf_Bien_Mueble_ID.Value.Trim();
        DataTable Dt_Registros = Negocio.Consultar_Cambios_Bien();
        if (Dt_Registros.Rows.Count > 0)
        {
            Dt_Registros.TableName = "DT_DATOS";
            DataSet Ds_Consulta = new DataSet();
            Ds_Consulta.Tables.Add(Dt_Registros.Copy());
            Ds_Pat_Historial_Cambios_Bienes Ds_Reporte = new Ds_Pat_Historial_Cambios_Bienes();
            Generar_Reporte_Cambios(Ds_Consulta, Ds_Reporte);
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "gaco", "alert('No se han registrado cambios para este Bien');", true);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Generar_Reporte
    ///DESCRIPCIÓN: caraga el data set fisico con el cual se genera el Reporte especificado
    ///PARAMETROS:  1.-Data_Set_Consulta_DB.- Contiene la informacion de la consulta a la base de datos
    ///             2.-Ds_Reporte, Objeto que contiene la instancia del Data set fisico del Reporte a generar
    ///             3.-Nombre_Reporte, contiene el nombre del Reporte a mostrar en pantalla
    ///CREO: Susana Trigueros Armenta.
    ///FECHA_CREO: 01/Mayo/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Generar_Reporte_Cambios(DataSet Data_Set_Consulta_DB, DataSet Ds_Reporte)
    {
        ReportDocument Reporte = new ReportDocument();
        String File_Path = Server.MapPath("../Rpt/Compras/Rpt_Pat_Historial_Cambios_Bienes.rpt");
        Reporte.Load(File_Path);
        Ds_Reporte = Data_Set_Consulta_DB;
        Reporte.SetDataSource(Ds_Reporte);
        ExportOptions Export_Options = new ExportOptions();
        DiskFileDestinationOptions Disk_File_Destination_Options = new DiskFileDestinationOptions();
        String Ruta = "../../Reporte/Rpt_Pat_Historial_Cambios_Bienes" + Session.SessionID + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".pdf";
        Disk_File_Destination_Options.DiskFileName = Server.MapPath(Ruta);
        Export_Options.ExportDestinationOptions = Disk_File_Destination_Options;
        Export_Options.ExportDestinationType = ExportDestinationType.DiskFile;
        Export_Options.ExportFormatType = ExportFormatType.PortableDocFormat;
        Reporte.SetParameterValue("USUARIO_GENERO", Cls_Sessiones.Nombre_Empleado);
        Reporte.Export(Export_Options);
        ScriptManager.RegisterStartupScript(this, this.GetType(), "open", "window.open('" + Ruta + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Exportar_Acta_Click
    ///DESCRIPCIÓN: Btn_Exportar_Acta_Click
    ///PARAMETROS: 
    ///CREO: Francisco Antonio Gallardo Castañeda
    ///FECHA_CREO: 15/Julio/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Exportar_Acta_Click(object sender, ImageClickEventArgs e)
    {
        if (!String.IsNullOrEmpty(Hdf_Bien_Mueble_ID.Value))
        {
            String Nombre_Plantilla = "ACTA_00001";
            String Xml_Armado_Acta = Crear_Xml_Acta_00001();
            String Ruta_Plantilla = String.Empty;
            String Documento_Salida = String.Empty;
            ReportDocument Reporte = new ReportDocument();
            DocumentFormat.OpenXml.Packaging.MainDocumentPart main;
            DocumentFormat.OpenXml.Packaging.CustomXmlPart CustomXml;
            String Nombre_Archivo = String.Empty;

            Ruta_Plantilla = Server.MapPath("Plantillas/" + Nombre_Plantilla + ".docx");
            Nombre_Archivo = Nombre_Plantilla + "_BIEN_MUEBLE_" + Hdf_Bien_Mueble_ID.Value.Trim() + String.Format("{0:ddMMyyyyhhmmss}", DateTime.Now) + ".doc";
            Documento_Salida = Server.MapPath("../../Reporte/" + Nombre_Archivo);

            File.Copy(Ruta_Plantilla, Documento_Salida);

            using (DocumentFormat.OpenXml.Packaging.WordprocessingDocument doc = DocumentFormat.OpenXml.Packaging.WordprocessingDocument.Open(Documento_Salida, true))
            {
                main = doc.MainDocumentPart;
                main.DeleteParts<DocumentFormat.OpenXml.Packaging.CustomXmlPart>(main.CustomXmlParts);
                CustomXml = main.AddCustomXmlPart(DocumentFormat.OpenXml.Packaging.CustomXmlPartType.CustomXml);

                using (StreamWriter ts = new StreamWriter(CustomXml.GetStream()))
                {
                    ts.Write(Xml_Armado_Acta);
                }
                // guardar los cambios en el documento
                main.Document.Save();
                doc.Close();
                Response.Clear();
                System.IO.FileInfo Info_Archivo = new FileInfo(Documento_Salida);
                Response.AddHeader("Content-Disposition", "attachment; filename=" + Info_Archivo.Name);
                Response.AddHeader("Content-Length", Info_Archivo.Length.ToString());
                Response.ContentType = "application/octet-stream";
                Response.WriteFile(Info_Archivo.FullName);
                Response.End();
            }
        }
        else
        {
            Lbl_Ecabezado_Mensaje.Text = "No hay Bien Seleccionado";
            Lbl_Mensaje_Error.Text = "";
            Div_Contenedor_Msj_Error.Visible = true;
        }
    }

    #endregion

}