﻿<%@ Page Title="Ejecutar Depreciación Bienes" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Depreciacion_Bienes.aspx.cs" Inherits="paginas_Compras_Frm_Ope_Pat_Depreciacion_Bienes" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Limpiar_Filtros() {
            document.getElementById("<%=Cmb_Clase_Activo.ClientID%>").value = "";
            document.getElementById("<%=Cmb_Tipo_Bien.ClientID%>").value = "TODOS";
            document.getElementById("<%=Cmb_Tipo_Activo.ClientID%>").value = "";
            return false;
        } 
    </script>

    <!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->  
    <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server"  AsyncPostBackTimeout="36000" EnableScriptLocalization="true" EnableScriptGlobalization="true"/> 
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"> <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>                     
            </asp:UpdateProgress>
            <div id="Div_Requisitos" style="background-color:#ffffff; width:100%; height:100%;">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="4">Proceso de Depreciación de Bienes</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                          <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>                          
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="4">&nbsp;</td>                        
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left" colspan="2" style="width:50%;">
                            <asp:ImageButton ID="Btn_Exportar_Bienes_No_Migrados" runat="server" 
                                ToolTip="Exportación de Bienes No Migrados" 
                                AlternateText="Exportación de Bienes No Migrados" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" 
                                onclick="Btn_Exportar_Bienes_No_Migrados_Click"/>
                        </td>
                        <td align="right" style="width:50%;" colspan="2">
                            <asp:ImageButton ID="Btn_Limpiar_Filtros" runat="server" ToolTip="Limpiar Filtros" AlternateText="Limpiar Filtros" ImageUrl="~/paginas/imagenes/paginas/icono_limpiar.png" Width="24px" OnClientClick="javascript:return Limpiar_Filtros();" />
                            &nbsp;
                        </td>                       
                    </tr>
                </table>   
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr><td colspan="4">&nbsp;</td></tr>
                    <tr>
                        <td style="width:18%; text-align:left; ">
                            &nbsp;
                            <asp:Label ID="Lbl_Tipo_Bienes" runat="server" Text="Tipo de Bien" ></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Tipo_Bien" runat="server" Width="100%">
                                <asp:ListItem Text="&lt; TODOS &gt;" Value="TODOS"></asp:ListItem>
                                <asp:ListItem Value="BIEN_INMUEBLE">BIENES INMUEBLES</asp:ListItem>
                                <asp:ListItem Value="BIEN_MUEBLE">BIENES MUEBLES</asp:ListItem>
                                <asp:ListItem Value="VEHICULO">VEHÍCULOS</asp:ListItem>
                            </asp:DropDownList>                                                 
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            &nbsp;
                            <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase Activo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                <asp:ListItem Value="">&lt;TODOS&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td width="18%">
                            &nbsp;
                            <asp:Label ID="Lbl_Tipo_Activo" runat="server" Text="Tipo Activo"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Tipo_Activo" runat="server" Width="100%">
                                <asp:ListItem Value="">&lt;TODOS&gt;</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
                <br />
                <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td style="text-align:right;">
                            <asp:Button ID="Btn_Ejecutar_Depreciacion" runat="server" 
                                Text="Ejecutar Depreciación de Bienes"
                                style="border-style:outset; background-color:White; width:98%; height:80px; font-weight:bolder; font-size:medium; cursor:hand;" 
                                OnClick="Btn_Ejecutar_Depreciacion_Click"/>   
                            &nbsp;&nbsp;&nbsp;  
                        </td>
                    </tr>
                </table>
            </div>         
            <br />                                            
            <br />
            <br />
            <br />
   
        </ContentTemplate>      
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Exportar_Bienes_No_Migrados" />
        </Triggers>     
    </asp:UpdatePanel> 
</asp:Content>

