﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master"
    AutoEventWireup="true" CodeFile="Frm_Ope_Pat_Bienes_Inmuebles.aspx.cs" Inherits="paginas_Control_Patrimonial_Frm_Ope_Pat_Bienes_Inmuebles"
    Title="Control de Bienes Inmuebles" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="Server">

    <script type="text/javascript" language="javascript">
        //Valida que los campos tengan el formato decimal correcto
        function Validar_Valores_Decimales() {
            var regEx = /^[0-9]{1,50}(\.[0-9]{0,4})?$/;
            var Superficie = replaceAll(document.getElementById("<%=Txt_Superficie.ClientID%>").value, ",", "");
            var Valor_Comercial = replaceAll(document.getElementById("<%=Txt_Valor_Comercial.ClientID%>").value, ",", "");
            var Contruccion_Registrada = replaceAll(document.getElementById("<%=Txt_Construccion_Resgistrada.ClientID%>").value, ",", "");
            var Resultado = true;
            if (Contruccion_Registrada.length > 0 && Resultado) {
                Valido = Contruccion_Registrada.match(regEx);
                if (!Valido) {
                    alert('Formato Incorrecto para el Campo \"Contrucción Registrada\".');
                    Resultado = false;
                }
            }
            if (Superficie.length > 0 && Resultado) {
                Valido = Superficie.match(regEx);
                if (!Valido) {
                    alert('Formato Incorrecto para el Campo \"Superficie\".');
                    Resultado = false;
                }
            }
            if (Valor_Comercial.length > 0 && Resultado) {
                Valido = Valor_Comercial.match(regEx);
                if (!Valido) { 
                    alert('Formato Incorrecto para el Campo \"Valor Comercial\".');
                    Resultado = false;
                }
            }
            if (Resultado) {
                Resultado = Validar_Valores_Porcentuales();
            }
            return Resultado;
        }

        //Valida que los campos tengan el formato porcentual correcto
        function Validar_Valores_Porcentuales() {
            var regEx = /^([1]{1})?[0-9]{1,2}(\.[0-9]{1,2})?$/;
            var Porcentaje_Ocupacion = document.getElementById("<%=Txt_Porcentaje_Ocupacion.ClientID%>").value;
            var Resultado = true;
            if (Porcentaje_Ocupacion.length > 0 && Resultado) {
                Valido = Porcentaje_Ocupacion.match(regEx);
                if (!Valido) {
                    alert('Formato Incorrecto para el Campo \"Porcentaje de Ocupación\".');
                    Resultado = false;
                } else {
                    Valor = parseFloat(Porcentaje_Ocupacion);
                    if (Valor > 100) {
                        alert('El valor del Campo \"Porcentaje de Ocupación\" no puede exceder el 100.');
                        Resultado = false;
                    }
                }
            }
            return Resultado;
        }

        function replaceAll(text, busca, reemplaza) {
            while (text.toString().indexOf(busca) != -1)
                text = text.toString().replace(busca, reemplaza);
            return text;
        }
    </script>

    <!--SCRIPT PARA LA VALIDACION QUE NO EXPERE LA SESSION-->

    <script language="javascript" type="text/javascript">
    <!--
        //El nombre del controlador que mantiene la sesión
        var CONTROLADOR = "../../Mantenedor_Session.ashx";

        //Ejecuta el script en segundo plano evitando así que caduque la sesión de esta página
        function MantenSesion() {
            var head = document.getElementsByTagName('head').item(0);
            script = document.createElement('script');
            script.src = CONTROLADOR;
            script.setAttribute('type', 'text/javascript');
            script.defer = true;
            head.appendChild(script);
        }

        //Temporizador para matener la sesión activa
        setInterval("MantenSesion()", <%=(int)(0.9*(Session.Timeout * 60000))%>);
        
    //-->
    </script>



</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="Server">

    <%--liberias pera autocompletar--%>
    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <%--cc pera autocompletar--%>
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />
    
    <script type="text/javascript">
        $(function() {
            Auto_Completado_Cuentas_Contables();
        });
        function Format_Cuenta(item) {
            return item.cuenta_descripcion;
        }
        function Auto_Completado_Cuentas_Contables() {
            $("[id$='Txt_Cuenta_Contable']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                extraParams: { accion: 'autocompletado_cuenta_contable' },
                dataType: "json",
                parse: function(data) {
                    return $.map(data, function(row) {
                        return {
                            data: row,
                            value: row.cuenta_contable_id,
                            result: row.cuenta_descripcion

                        }
                    });
                },
                formatItem: function(item) {
                    $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                    return Format_Cuenta(item);
                }
            }).result(function(e, item) {
                $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
            });
            $("[id$='Txt_Cuenta_Contable_Terreno']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                extraParams: { accion: 'autocompletado_cuenta_contable' },
                dataType: "json",
                parse: function(data) {
                    return $.map(data, function(row) {
                        return {
                            data: row,
                            value: row.cuenta_contable_id,
                            result: row.cuenta_descripcion

                        }
                    });
                },
                formatItem: function(item) {
                    $("[id$='Hdf_Cuenta_Contable_Terreno_ID']").val("");
                    return Format_Cuenta(item);
                }
            }).result(function(e, item) {
                $("[id$='Hdf_Cuenta_Contable_Terreno_ID']").val(item.cuenta_contable_id);
            });
            $("[id$='Txt_Cuenta_Gasto_ID']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                extraParams: { accion: 'autocompletado_cuenta_contable' },
                dataType: "json",
                parse: function(data) {
                    return $.map(data, function(row) {
                        return {
                            data: row,
                            value: row.cuenta_contable_id,
                            result: row.cuenta_descripcion

                        }
                    });
                },
                formatItem: function(item) {
                    $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                    return Format_Cuenta(item);
                }
            }).result(function(e, item) {
                $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
            });
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="Server">
    <cc1:ToolkitScriptManager ID="ToolkitScriptManager1" runat="server" AsyncPostBackTimeout="36000"
        EnableScriptLocalization="true" EnableScriptGlobalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_General" style="background-color: #ffffff; width: 100%; height: 100%;"
                runat="server">
                <table width="98%" border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo" colspan="2">
                            Control de Bienes Inmuebles
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="Div_Contenedor_Msj_Error" style="width: 98%;" runat="server" visible="false">
                                <table style="width: 100%;">
                                    <tr>
                                        <td colspan="2" align="left">
                                            <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png"
                                                Width="24px" Height="24px" />
                                            <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width: 10%;">
                                        </td>
                                        <td style="width: 90%; text-align: left;" valign="top">
                                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            &nbsp;
                        </td>
                    </tr>
                    <tr class="barra_busqueda" >
                        <td align="left" style="width: 50%;">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Nuevo" ToolTip="Nuevo" OnClick="Btn_Nuevo_Click"
                                OnClientClick="javascript:return Validar_Valores_Decimales();" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar"
                                OnClick="Btn_Modificar_Click" OnClientClick="javascript:return Validar_Valores_Decimales();" />
                            <asp:ImageButton ID="Btn_Ver_Ficha_Tecnica_PDF" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Ver Ficha Tenica de Bien Inmueble [PDF]"
                                ToolTip="Ver Ficha Tenica de Bien Inmueble [PDF]" OnClick="Btn_Ver_Ficha_Tecnica_PDF_Click" />
                            <asp:ImageButton ID="Btn_Ver_Ficha_Tecnica_Excel" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Ver Ficha Tenica de Bien Inmueble [EXCEL]"
                                ToolTip="Ver Ficha Tenica de Bien Inmueble [EXCEL]" OnClick="Btn_Ver_Ficha_Tecnica_Excel_Click" />
                            <asp:ImageButton ID="Btn_Salir" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                Width="24px" CssClass="Img_Button" AlternateText="Salir" ToolTip="Salir" OnClick="Btn_Salir_Click" />
                        </td>
                        <td align="right" style="width: 50%;">
                            <asp:ImageButton ID="Btn_Exportar_Caractetisticas" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png"
                                                        Width="24px" CssClass="Img_Button" AlternateText="Exportar Caracteristicas a Excel"
                                                        ToolTip="Exportar Caracteristicas a Excel" OnClick="Btn_Exportar_Caractetisticas_Click" />
                        </td>
                    </tr>
                </table>
                <br />
                <table border="0" width="98%" class="estilo_fuente">
                    <tr>
                        <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"
                            colspan="4">
                            DATOS GENERALES DEL BIEN INMUEBLE
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Bien_Inmueble_ID" runat="server" Text="No. Inventario" Style="font-weight: bolder;"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:HiddenField ID="Hdf_Bien_Inmueble_ID" runat="server" />
                            <asp:TextBox ID="Txt_Bien_Inmueble_ID" runat="server" Style="width: 98%; font-weight: bolder;"
                                Enabled="false"></asp:TextBox>
                        </td>
                        <td style="width: 15%;">
                            &nbsp;<asp:Label ID="Lbl_Clave" runat="server" Text="Clave"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Clave" runat="server" Style="width: 98%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Clave" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                InvalidChars="'" TargetControlID="Txt_Clave" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Nombre_Comun" runat="server" Text="Nombre Común"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Nombre_Comun" runat="server" Style="width: 99%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Comun" runat="server" Enabled="True"
                                FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                TargetControlID="Txt_Nombre_Comun" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Numero_Cuenta_Predial" runat="server" Text="Cuenta Predial"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Numero_Cuenta_Predial" runat="server" Style="width: 98%;" MaxLength="25"></asp:TextBox>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Valor_Comercial" runat="server" Text="Valor Comercial [$]"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Valor_Comercial" runat="server" Style="width: 98%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Valor_Comercial" runat="server" Enabled="True"
                                FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Valor_Comercial"
                                ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Observaciones" runat="server" Text="Observaciones"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Observaciones" runat="server" Style="width: 99%;" Rows="3" TextMode="MultiLine"></asp:TextBox>
                        </td>
                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                            InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                            ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                        </cc1:FilteredTextBoxExtender>
                        <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Observaciones" runat="server" TargetControlID="Txt_Observaciones"
                            WatermarkText="<< Para Agregar una Nueva Observación >>" WatermarkCssClass="watermarked"
                            Enabled="True" />
                    </tr>
                    <tr>
                        <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"
                            colspan="4">
                            DOMICILIO DEL BIEN INMUEBLE
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Calle" runat="server" Text="Calle"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Calle" runat="server" Style="width: 99.5%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Calle" runat="server" TargetControlID="Txt_Calle"
                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Colonia" runat="server" Text="Colonia"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Colonia" runat="server" Style="width: 99.5%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Colonia" runat="server" TargetControlID="Txt_Colonia"
                                InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Numero_Exterior" runat="server" Text="# Exterior"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Numero_Exterior" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Numero_Exterior" runat="server" Enabled="True"
                                FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                TargetControlID="Txt_Numero_Exterior" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Numero_Interior" runat="server" Text="# Interior"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Numero_Interior" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="fte_Txt_Numero_Interior" runat="server" Enabled="True"
                                FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                TargetControlID="Txt_Numero_Interior" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Manzana" runat="server" Text="Manzana"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Manzana" runat="server" Style="width: 98%;" MaxLength="4"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Manzana" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                InvalidChars="'" TargetControlID="Txt_Manzana" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Lote" runat="server" Text="Lote"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Lote" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Lote" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                InvalidChars="'" TargetControlID="Txt_Lote" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Distrito" runat="server" Text="Distrito"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:DropDownList ID="Cmb_Distrito" runat="server" Style="width: 100%;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Sector" runat="server" Text="Sector"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Sector" runat="server" Style="width: 100%;">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Clasificacion_Zona" runat="server" Text="Clasif. Zona"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Clasificacion_Zona" runat="server" Style="width: 100%;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Vias_Acceso" runat="server" Text="Vias de Acceso"></asp:Label>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="Txt_Vias_Aceso" runat="server" Style="width: 99%;" Rows="3" TextMode="MultiLine"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Vias_Aceso" runat="server" Enabled="True"
                                FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                TargetControlID="Txt_Vias_Aceso" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"
                            colspan="4">
                            DETALLES DEL BIEN INMUEBLE
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Fecha_Alta_Cta_Pub" runat="server" Text="Alta Cta. Pública"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Fecha_Alta_Cta_Pub" runat="server" Style="width: 80%;" Enabled="false"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Fecha_Alta_Cta_Pub" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Alta_Cta_Pub" runat="server" TargetControlID="Txt_Fecha_Alta_Cta_Pub"
                                PopupButtonID="Btn_Fecha_Alta_Cta_Pub" Format="dd/MMM/yyyy" Enabled="True">
                            </cc1:CalendarExtender>
                        </td>
                        <td style="width: 15%;"> &nbsp;</td>
                        <td style="width: 35%;"> &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Origen" runat="server" Text="Origen"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Origen" runat="server" Style="width: 100%;" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Origen_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Destino" runat="server" Text="Destino"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Destino" runat="server" Style="width: 100%;">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Uso" runat="server" Text="Uso"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Uso" runat="server" Style="width: 100%;">
                            </asp:DropDownList>
                        </td>
                        <td style="width: 15%;">&nbsp;
                            <asp:Label ID="Lbl_Superficie" runat="server" Text="Superficie"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Superficie" runat="server" Style="width: 80%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Superficie" runat="server" Enabled="True"
                                FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Superficie"
                                ValidChars=".,">
                            </cc1:FilteredTextBoxExtender>
                            <asp:Label ID="Lbl_Superficie_M2" runat="server" Text="[m2]"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%; text-align: left;">
                            <asp:Label ID="Lbl_Construccion_Registrada" runat="server" Text="Construcción Reg."></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Construccion_Resgistrada" runat="server" Style="width: 80%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Construccion_Resgistrada" runat="server"
                                Enabled="True" FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Construccion_Resgistrada"
                                ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                            <asp:Label ID="Lbl_Construccion_Registrada_M2" runat="server" Text="[m2]"></asp:Label>
                        </td>
                        <td style="width: 15%; text-align: left;">&nbsp;
                            <asp:Label ID="Lbl_Porcentaje_Ocupacion" runat="server" Text="Ocupación [%]"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:TextBox ID="Txt_Porcentaje_Ocupacion" runat="server" Style="width: 98%;"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Porcentaje_Ocupacion" runat="server" Enabled="True"
                                FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Porcentaje_Ocupacion"
                                ValidChars=".">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 15%;">
                            <asp:Label ID="Lbl_Estado" runat="server" Text="Estado"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Estado" runat="server" Style="width: 100%;" OnSelectedIndexChanged="Cmb_Estado_SelectedIndexChanged"
                                AutoPostBack="true">
                                <asp:ListItem Value="ALTA">ALTA</asp:ListItem>
                                <asp:ListItem Value="BAJA">BAJA</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td style="width: 15%; text-align: left;">
                            &nbsp;<asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label>
                        </td>
                        <td style="width: 35%;">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Style="width: 100%;">
                                <asp:ListItem Value="">&lt;SELECCIONE&gt;</asp:ListItem>
                                <asp:ListItem Value="IRRREGULAR">IRRREGULAR</asp:ListItem>
                                <asp:ListItem Value="PROCESO DE REGULARIZACION">PROCESO DE REGULARIZACIÓN</asp:ListItem>
                                <asp:ListItem Value="REGULAR">REGULAR</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <cc1:TabContainer ID="Tab_Contenedor_Pestagnas" runat="server" Width="100%" ActiveTabIndex="0"
                                CssClass="Tab">
                                <cc1:TabPanel runat="server" HeaderText="Tab_Medidas_Colindancias" ID="Tab_Medidas_Colindancias"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Medidas y Colindancias
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="98%" class="estilo_fuente" border="0">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Orientacion" runat="server" Text="Orientacion" Style="font-weight: bolder;"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Orientacion" runat="server" Style="width: 100%;">
                                                        <asp:ListItem>&lt;SELECCIONE&gt;</asp:ListItem>
                                                        <asp:ListItem Value="NORTE">NORTE</asp:ListItem>
                                                        <asp:ListItem Value="NOR_ORIENTE">NOR ORIENTE</asp:ListItem>
                                                        <asp:ListItem Value="NOR_PONIENTE">NOR PONIENTE</asp:ListItem>
                                                        <asp:ListItem Value="SUR">SUR</asp:ListItem>
                                                        <asp:ListItem Value="SUR_ORIENTE">SUR ORIENTE</asp:ListItem>
                                                        <asp:ListItem Value="SUR_PONIENTE">SUR PONIENTE</asp:ListItem>
                                                        <asp:ListItem Value="ESTE">ESTE</asp:ListItem>
                                                        <asp:ListItem Value="OESTE">OESTE</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Medida" runat="server" Text="Medida" Style="font-weight: bolder;"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Medida" runat="server" Style="width: 65%;"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Medida" runat="server" Enabled="True" FilterType="Custom, Numbers"
                                                        InvalidChars="'" TargetControlID="Txt_Medida" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                    <asp:Label ID="Lbl_Medida_M2" runat="server" Text="[metros]"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 13%;">
                                                    <asp:Label ID="Lbl_Colindancia" runat="server" Text="Colindancia"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Colindancia" runat="server" Style="width: 98%;" Rows="2" TextMode="MultiLine"></asp:TextBox>
                                                </td>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Colindancia" runat="server" TargetControlID="Txt_Colindancia"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: right;">
                                                    <asp:Button ID="Btn_Agregar_Medida_Colindancia" runat="server" Text="AGREGAR" Style="border-style: outset;
                                                        background-color: White; width: 150px; font-weight: bolder;" OnClick="Btn_Agregar_Medida_Colindancia_Click" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <div runat="server" id="Div_Listado_Medidas_Colindancias" style="width: 800px; overflow: auto;
                                                        height: 280px;">
                                                        <asp:GridView ID="Grid_Listado_Medidas_Colindancias" runat="server" AutoGenerateColumns="False"
                                                            CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Grid_Listado_Medidas_Colindancias_RowDataBound"
                                                            PageSize="100" Width="800px" CssClass="GridView_1">
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                            <Columns>
                                                                <asp:BoundField DataField="ORIENTACION" HeaderText="Orientación" SortExpression="ORIENTACION">
                                                                    <ItemStyle Width="60px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="MEDIDA" HeaderText="Medida [m]" SortExpression="MEDIDA">
                                                                    <ItemStyle Width="80px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="COLINDANCIA" HeaderText="Colindancia" SortExpression="COLINDANCIA">
                                                                    <HeaderStyle Width="620px" Wrap="True" />
                                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="620px" Wrap="True" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Quitar">
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="Btn_Quitar_Medida_Colindancia" runat="server" ToolTip="Quitar Medida y Colindancia"
                                                                            AlternateText="Quitar Medida y Colindancia" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                                            OnClick="Btn_Quitar_Medida_Colindancia_Click" />
                                                                    </ItemTemplate>
                                                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <RowStyle CssClass="GridItem" />
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Sustento_Juridico" ID="Tab_Sustento_Juridico"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Sustento Jurídico
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="98%" class="estilo_fuente" border="0">
                                            <tr>
                                                <td colspan="4">
                                                    <asp:HiddenField ID="Hdf_No_Registro_Juridico_Alta" runat="server" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Escritura" runat="server" Text="Escritura"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Escritura" runat="server" Style="width: 98%;" MaxLength="150"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Escritura" runat="server" TargetControlID="Txt_Escritura"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Fecha_Escritura" runat="server" Text="Fecha de Escritura"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Escritura" runat="server" Style="width: 90%;"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Escritura" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Escritura" runat="server" TargetControlID="Txt_Fecha_Escritura"
                                                        PopupButtonID="Btn_Fecha_Escritura" Format="dd/MMM/yyyy" Enabled="True">
                                                    </cc1:CalendarExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Fecha_Registro" runat="server" Text="Fecha de Registro"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Registro" runat="server" Style="width: 90%;" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Registro" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Registro" runat="server" TargetControlID="Txt_Fecha_Registro"
                                                        PopupButtonID="Btn_Fecha_Registro" Format="dd/MMM/yyyy" Enabled="True">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Constacia_Registral" runat="server" Text="Constancia Reg."></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Constacia_Registral" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Constacia_Registral" runat="server" TargetControlID="Txt_Constacia_Registral"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_No_Notario" runat="server" Text="No. Notario"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Notario" runat="server" Style="width: 98%;" MaxLength="20"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Notario" runat="server" TargetControlID="Txt_No_Notario"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_No_Consecion" runat="server" Text="No. Concesión"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_No_Consecion" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_No_Consecion" runat="server" TargetControlID="Txt_No_Consecion"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Nombre_Notario" runat="server" Text="Nombre Notario"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Nombre_Notario" runat="server" Style="width: 99%;" MaxLength="150"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Notario" runat="server" TargetControlID="Txt_Nombre_Notario"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Folio_Real" runat="server" Text="Folio Real"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Folio_Real" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Real" runat="server" TargetControlID="Txt_Folio_Real"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Libertad_Gravament" runat="server" Text="Libre Gravamen"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Libertad_Gravament" runat="server" Width="100%">
                                                        <asp:ListItem Value="NO">NO</asp:ListItem>
                                                        <asp:ListItem Value="SI">SI</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Antecedente_Registral" runat="server" Text="Origen Especifico"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Antecedente_Registral" runat="server" Style="width: 99%;" MaxLength="100"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Antecedente_Registral" runat="server" TargetControlID="Txt_Antecedente_Registral"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <div runat="server" id="Div_Campos_Juridico_Baja" style="width: 99%">
                                                        <table border="0" width="100%" class="estilo_fuente">
                                                            <tr>
                                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"
                                                                    colspan="4">
                                                                    CAMPOS NECESARIOS PARA LA BAJA DE UN BIEN INMUEBLE MUNICIPAL
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="4">
                                                                    <asp:HiddenField ID="Hdf_No_Registro_Juridico_Baja" runat="server" />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Fecha_Baja" runat="server" Text="Fecha de Baja"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Fecha_Baja" runat="server" Style="width: 80%;"></asp:TextBox>
                                                                    <asp:ImageButton ID="Btn_Fecha_Baja" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Baja" runat="server" TargetControlID="Txt_Fecha_Baja"
                                                                        PopupButtonID="Btn_Fecha_Baja" Format="dd/MMM/yyyy" Enabled="True">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_No_Escritura" runat="server" Text="Escritura"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_No_Escritura" runat="server" Style="width: 98%;" MaxLength="150"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_No_Escritura" runat="server" TargetControlID="Txt_Baja_No_Escritura"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td style="width: 15%; text-align: right;">
                                                                    <asp:Label ID="Lbl_Baja_Fecha_Escritura" runat="server" Text="Fecha de Escritura"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_Fecha_Escritura" runat="server" Style="width: 80%;"></asp:TextBox>
                                                                    <asp:ImageButton ID="Btn_Baja_Fecha_Escritura" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                                    <cc1:CalendarExtender ID="CE_Txt_Baja_Fecha_Escritura" runat="server" TargetControlID="Txt_Baja_Fecha_Escritura"
                                                                        PopupButtonID="Btn_Baja_Fecha_Escritura" Format="dd/MMM/yyyy" Enabled="True">
                                                                    </cc1:CalendarExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_No_Notario" runat="server" Text="No. Notario"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_No_Notario" runat="server" Style="width: 98%;" MaxLength="20"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_No_Notario" runat="server" TargetControlID="Txt_Baja_No_Notario"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td style="width: 15%; text-align: right;">
                                                                    <asp:Label ID="Lbl_Baja_Constancia_Registral" runat="server" Text="Constancia Reg."></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_Constancia_Registral" runat="server" Style="width: 98%;"
                                                                        MaxLength="100"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_Constancia_Registral" runat="server"
                                                                        TargetControlID="Txt_Baja_Constancia_Registral" InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_Nombre_Notario" runat="server" Text="Nombre Notario"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="Txt_Baja_Nombre_Notario" runat="server" Style="width: 99%;" MaxLength="150"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_Nombre_Notario" runat="server" TargetControlID="Txt_Baja_Nombre_Notario"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ  -_/" Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_Folio_Real" runat="server" Text="Folio Real"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_Folio_Real" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_Folio_Real" runat="server" TargetControlID="Txt_Baja_Folio_Real"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_No_Contrato" runat="server" Text="No. Contrato"></asp:Label>
                                                                </td>
                                                                <td style="width: 35%;">
                                                                    <asp:TextBox ID="Txt_Baja_No_Contrato" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_No_Contrato" runat="server" TargetControlID="Txt_Baja_No_Contrato"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td style="width: 15%;">
                                                                    <asp:Label ID="Lbl_Baja_Nuevo_Propietario" runat="server" Text="Nuevo Propietario"></asp:Label>
                                                                </td>
                                                                <td colspan="3">
                                                                    <asp:TextBox ID="Txt_Baja_Nuevo_Propietario" runat="server" Style="width: 99%;" MaxLength="150"></asp:TextBox>
                                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Baja_Nuevo_Propietario" runat="server" TargetControlID="Txt_Baja_Nuevo_Propietario"
                                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                                    </cc1:FilteredTextBoxExtender>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Caracteristicas" ID="Tab_Caracteristicas" Width="100%">
                                    <HeaderTemplate>
                                        Caracteristicas de Pozos
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="98%" class="estilo_fuente" border="0">
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    GENERALES
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Estado_Actual" runat="server" Text="Estado Actual"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Estado_Actual" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Estado_Actual" runat="server" TargetControlID="Txt_Estado_Actual"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Uso_Inicial" runat="server" Text="Uso Inicial"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Uso_Inicial" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Uso_Inicial" runat="server" TargetControlID="Txt_Uso_Inicial"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Usuarios_Beneficiados" runat="server" Text="Usuarios Beneficiados"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Usuarios_Beneficiados" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Usuarios_Beneficiados" runat="server" TargetControlID="Txt_Usuarios_Beneficiados"
                                                        FilterType="Numbers" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Anios_Sin_Operar" runat="server" Text="Años s/Operar"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Anios_Sin_Operar" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anios_Sin_Operar" runat="server" TargetControlID="Txt_Anios_Sin_Operar"
                                                        FilterType="Numbers" Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    UBICACIÓN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Region" runat="server" Text="Región"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Region" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Region" runat="server" TargetControlID="Txt_Region"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Acuifero" runat="server" Text="Acuífero"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Acuifero" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Acuifero" runat="server" TargetControlID="Txt_Acuifero"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Cuenca" runat="server" Text="Cuenca"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Cuenca" runat="server" Style="width: 99%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Cuenca" runat="server" TargetControlID="Txt_Cuenca"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Latitud" runat="server" Text="Latitud"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Latitud" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Latitud" runat="server" TargetControlID="Txt_Latitud"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Longitud" runat="server" Text="Longitud"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Longitud" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Longitud" runat="server" TargetControlID="Txt_Longitud"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".-">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    VOLÚMENES ANUALES
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Volumen_Consumo" runat="server" Text="De Consumo"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Volumen_Consumo" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Volumen_Consumo" runat="server" TargetControlID="Txt_Volumen_Consumo"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Volumen_Extraccion" runat="server" Text="De Extracción"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Volumen_Extraccion" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Volumen_Extraccion" runat="server" TargetControlID="Txt_Volumen_Extraccion"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Volumen_Descarga" runat="server" Text="De Descarga"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Volumen_Descarga" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Volumen_Descarga" runat="server" TargetControlID="Txt_Volumen_Descarga"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    EQUIPO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Diametro_Columna_Succionadora" runat="server" Text="Diámetro Columna Succionadora"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Diametro_Columna_Succionadora" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Diametro_Columna_Succionadora" runat="server" TargetControlID="Txt_Diametro_Columna_Succionadora"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Diametro_Descarga" runat="server" Text="Diámetro Descarga"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Diametro_Descarga" runat="server" Style="width: 98%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Diametro_Descarga" runat="server" TargetControlID="Txt_Diametro_Descarga"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Tipo_Bomba" runat="server" Text="Tipo bomba"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Tipo_Bomba" runat="server" Style="width: 99%;" MaxLength="500"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Tipo_Bomba" runat="server" TargetControlID="Txt_Tipo_Bomba"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Accionada_Por_Motor" runat="server" Text="Accionada p/Motor"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Accionada_Por_Motor" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Accionada_Por_Motor" runat="server" TargetControlID="Txt_Accionada_Por_Motor"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Medidor" runat="server" Text="Medidor"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Medidor" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Medidor" runat="server" TargetControlID="Txt_Medidor"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    CARACTERÍSTICAS DEL APROVECHAMIENTO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Profundidad" runat="server" Text="Profundidad"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Profundidad" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Profundidad" runat="server" TargetControlID="Txt_Profundidad"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Diametro_Perforacion" runat="server" Text="Diámetro Perforación"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Diametro_Perforacion" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Diametro_Perforacion" runat="server" TargetControlID="Txt_Diametro_Perforacion"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Diameto_Ademe" runat="server" Text="Diámetro Ademe"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Diametro_Ademe" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Diametro_Ademe" runat="server" TargetControlID="Txt_Diametro_Ademe"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td colspan="2">
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    GASTO (1/SEG)
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Requerido" runat="server" Text="Requerido"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Requerido" runat="server" Style="width: 98%;" MaxLength="10"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Requerido" runat="server" TargetControlID="Txt_Requerido"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Maximo" runat="server" Text="Máximo"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Maximo" runat="server" Style="width: 98%;" MaxLength="100"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Maximo" runat="server" TargetControlID="Txt_Maximo"
                                                        FilterType="Custom, Numbers" Enabled="True" ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Contabilidad" ID="Tab_Contabilidad"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Contabilidad
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="98%" class="estilo_fuente" border="0">
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    CONSTRUCCIÓN
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Clase_Activo" runat="server" Text="Clase Activo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="Cmb_Clase_Activo" runat="server" Width="100%">
                                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="15%">
                                                    <asp:HiddenField ID="Hdf_Cuenta_Contable_ID" runat="server" />
                                                    <asp:Label ID="Lbl_Cuenta_Contable" runat="server" Text="Cuenta Contable"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Cuenta_Contable" runat="server" Width="99.5%">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="15%">
                                                    <asp:Label ID="Lbl_Nombre_Perito" runat="server" Text="Nombre Perito"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Nombre_Perito" runat="server" Width="99.5%">
                                                    </asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Perito" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                                        TargetControlID="Txt_Nombre_Perito" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%; text-align: left;">
                                                    <asp:Label ID="Lbl_Fecha_Avaluo" runat="server" Text="Fecha Avaluo"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Avaluo" runat="server" Style="width: 80%;" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Avaluo" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Avaluo" runat="server" TargetControlID="Txt_Fecha_Avaluo"
                                                        PopupButtonID="Btn_Fecha_Avaluo" Format="dd/MMM/yyyy" Enabled="True">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td style="width: 15%; text-align: left;">
                                                    &nbsp;<asp:Label ID="Lbl_Valor_Avaluo" runat="server" Text="Valor Avaluo [$]"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Valor_Avaluo" runat="server" Style="width: 98%;"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Valor_Avaluo" runat="server" Enabled="True"
                                                        FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Valor_Avaluo"
                                                        ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            </tr>
                                                <td width="15%">
                                                    <asp:HiddenField ID="Hdf_Cuenta_Gasto_ID" runat="server" />
                                                    <asp:Label ID="Lbl_Cuenta_Gasto_ID" runat="server" Text="Cuenta Gasto"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Cuenta_Gasto_ID" runat="server" Width="99.5%">
                                                    </asp:TextBox>
                                                </td>
                                                <td style="width: 15%; text-align: left;">
                                                    &nbsp;<asp:Label ID="Lbl_Valor_Actual" runat="server" Text="Valor Actual [$]"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Valor_Actual" runat="server" Style="width: 98%;" Enabled="false"></asp:TextBox>
                                                </td>
                                            <tr>
                                            <tr>
                                                <td colspan="4">
                                                &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="text-align: left; font-weight:bolder;"colspan="4">
                                                    TERRENO
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="background-color: #4F81BD; color: White; font-weight: bolder; text-align: center;"colspan="4">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Clase_Activo_Terreno" runat="server" Text="Clase Activo"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:DropDownList ID="Cmb_Clase_Activo_Terreno" runat="server" Width="100%">
                                                        <asp:ListItem Value="SELECCIONE">&lt;SELECCIONE&gt;</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="15%">
                                                    <asp:HiddenField ID="Hdf_Cuenta_Contable_Terreno_ID" runat="server" />
                                                    <asp:Label ID="Lbl_Cuenta_Contable_Terreno" runat="server" Text="Cuenta Contable"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Cuenta_Contable_Terreno" runat="server" Width="99.5%">
                                                    </asp:TextBox>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td width="15%">
                                                    <asp:Label ID="Lbl_Nombre_Perito_Terreno" runat="server" Text="Nombre Perito"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Nombre_Perito_Terreno" runat="server" Width="99.5%">
                                                    </asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Perito_Terreno" runat="server" Enabled="True" FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters" InvalidChars="'"
                                                        TargetControlID="Txt_Nombre_Perito_Terreno" ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ ">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 15%; text-align: left;">
                                                    <asp:Label ID="Lbl_Fecha_Avaluo_Terreno" runat="server" Text="Fecha Avaluo"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Fecha_Avaluo_Terreno" runat="server" Style="width: 80%;" Enabled="false"></asp:TextBox>
                                                    <asp:ImageButton ID="Btn_Fecha_Avaluo_Terreno" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" />
                                                    <cc1:CalendarExtender ID="CE_Txt_Fecha_Avaluo_Terreno" runat="server" TargetControlID="Txt_Fecha_Avaluo_Terreno"
                                                        PopupButtonID="Btn_Fecha_Avaluo_Terreno" Format="dd/MMM/yyyy" Enabled="True">
                                                    </cc1:CalendarExtender>
                                                </td>
                                                <td style="width: 15%; text-align: left;">
                                                    &nbsp;<asp:Label ID="Lbl_Valor_Avaluo_Terreno" runat="server" Text="Valor Avaluo [$]"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:TextBox ID="Txt_Valor_Avaluo_Terreno" runat="server" Style="width: 98%;"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Valor_Avaluo_Terreno" runat="server" Enabled="True"
                                                        FilterType="Custom, Numbers" InvalidChars="'" TargetControlID="Txt_Valor_Avaluo_Terreno"
                                                        ValidChars=".">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Observaciones" ID="Tab_Observaciones"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Observaciones
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <div runat="server" id="Div_Grid_Observaciones" style="width: 98%; overflow: auto;
                                            height: 380px;">
                                            <asp:GridView ID="Grid_Observaciones" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="100" Width="100%"
                                                CssClass="GridView_1" AllowPaging="true">
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                <Columns>
                                                    <asp:BoundField DataField="NO_OBSERVACION" HeaderText="NO_OBSERVACION" SortExpression="NO_OBSERVACION">
                                                        <ItemStyle Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FECHA_OBSERVACION" HeaderText="Fecha / Hora" SortExpression="FECHA_OBSERVACION"
                                                        DataFormatString="{0:dd-MMM-yyyy, HH:mm:ss tt}">
                                                        <ItemStyle Width="170px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="USUARIO_CREO" HeaderText="Autor" SortExpression="USUARIO_CREO">
                                                        <ItemStyle Width="240px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="OBSERVACION" HeaderText="Observaciones" SortExpression="OBSERVACION">
                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="GridHeader" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <RowStyle CssClass="GridItem" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Expropiaciones" ID="Tab_Expropiaciones"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Expropiaciones
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table border="0" width="98%" class="estilo_fuente">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Expropiacion" runat="server" Text="Descripción"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Expropiacion" runat="server" Style="width: 99%;" Rows="3" TextMode="MultiLine" onkeyup="this.value = this.value.toUpperCase();"></asp:TextBox>
                                                </td>
                                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Expropiacion" runat="server" TargetControlID="Txt_Expropiacion"
                                                    InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                    ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                </cc1:FilteredTextBoxExtender>
                                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Expropiacion" runat="server" TargetControlID="Txt_Expropiacion"
                                                    WatermarkText="<< Para Agregar una Nueva Expropiación >>" WatermarkCssClass="watermarked"
                                                    Enabled="True" />
                                            </tr>
                                        </table>
                                        <div runat="server" id="Div_Expropiaciones" style="width: 98%; overflow: auto; height: 350px;">
                                            <asp:GridView ID="Grid_Expropiaciones" runat="server" AutoGenerateColumns="False"
                                                CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="100" Width="100%"
                                                CssClass="GridView_1" AllowPaging="true">
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                <Columns>
                                                    <asp:BoundField DataField="NO_EXPROPIACION" HeaderText="NO_EXPROPIACION" SortExpression="NO_EXPROPIACION">
                                                        <ItemStyle Font-Size="X-Small" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="FECHA_EXPROPIACION" HeaderText="Fecha / Hora" SortExpression="FECHA_EXPROPIACION"
                                                        DataFormatString="{0:dd-MMM-yyyy, HH:mm:ss tt}">
                                                        <ItemStyle Width="170px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="USUARIO_CREO" HeaderText="Autor" SortExpression="USUARIO_CREO">
                                                        <ItemStyle Width="240px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripción" SortExpression="DESCRIPCION">
                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                    </asp:BoundField>
                                                </Columns>
                                                <HeaderStyle CssClass="GridHeader" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <RowStyle CssClass="GridItem" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                            </asp:GridView>
                                        </div>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                                <cc1:TabPanel runat="server" HeaderText="Tab_Anexos_Archivos" ID="Tab_Anexos_Archivos"
                                    Width="100%">
                                    <HeaderTemplate>
                                        Anexos
                                    </HeaderTemplate>
                                    <ContentTemplate>
                                        <table width="98%" class="estilo_fuente" border="0">
                                            <tr>
                                                <td style="width: 15%;">
                                                    <asp:Label ID="Lbl_Tipo_Archivo" runat="server" Text="Tipo"></asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <asp:DropDownList ID="Cmb_Tipo_Archivo" runat="server" Style="width: 100%;">
                                                        <asp:ListItem Value="">&lt;SELECCIONE&gt;</asp:ListItem>
                                                        <asp:ListItem Value="AVALUO_COMERCIAL">AVALUO COMERCIAL</asp:ListItem>
                                                        <asp:ListItem Value="AVALUO_FISCAL">AVALUO FISCAL</asp:ListItem>
                                                        <asp:ListItem Value="ESCRITURA">ESCRITURA</asp:ListItem>
                                                        <asp:ListItem Value="EXPROPIACION">EXPROPIACIÓN</asp:ListItem>
                                                        <asp:ListItem Value="FOTOGRAFIA">FOTOGRAFÍA</asp:ListItem>
                                                        <asp:ListItem Value="LEVANTAMIENTO_TOPOGRAFICO">LEVANTAMIENTO TOPÓGRAFICO</asp:ListItem>
                                                        <asp:ListItem Value="MAPA">MAPA</asp:ListItem>
                                                        <asp:ListItem Value="OBRA_PUBLICA">OBRA PÚBLICA</asp:ListItem>
                                                        <asp:ListItem Value="OTRO">OTRO DOCUMENTO</asp:ListItem>
                                                    </asp:DropDownList>
                                                </td>
                                                <td style="width: 15%; text-align: right;">
                                                    <asp:Label ID="Lbl_Ruta_Archivo" runat="server" Text="Archivo"></asp:Label>
                                                    <asp:Label ID="Throbber" Text="wait" runat="server" Width="30px">                                                                     
                                                        <div id="Div1" class="progressBackgroundFilter"></div>
                                                        <div  class="processMessage" id="div2">
                                                            <img alt="" src="../imagenes/paginas/Updating.gif" />
                                                        </div>
                                                    </asp:Label>
                                                </td>
                                                <td style="width: 35%;">
                                                    <cc1:AsyncFileUpload ID="AFU_Ruta_Archivo" runat="server" Width="300px" ThrobberID="Throbber"
                                                        ForeColor="White" Font-Bold="True" CompleteBackColor="LightBlue" UploadingBackColor="LightGray"
                                                        FailedValidation="False" OnUploadedComplete="AFU_Ruta_Archivo_Complete" />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="width: 13%;">
                                                    <asp:Label ID="Lbl_Descripcion_Archivo" runat="server" Text="Descripción"></asp:Label>
                                                </td>
                                                <td colspan="3">
                                                    <asp:TextBox ID="Txt_Descripcion_Archivo" runat="server" Style="width: 100%;" Rows="2"
                                                        TextMode="MultiLine"></asp:TextBox>
                                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Descripcion_Archivo" runat="server" TargetControlID="Txt_Descripcion_Archivo"
                                                        InvalidChars="<,>,&,',!," FilterType="Custom, Numbers, UppercaseLetters, LowercaseLetters"
                                                        ValidChars="Ññ.,:;()áéíóúÁÉÍÓÚ-%/#@*+_ " Enabled="True">
                                                    </cc1:FilteredTextBoxExtender>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4" style="text-align: center;">
                                                    <asp:Label ID="Lbl_Nota_Archivos" runat="server" Text="*Nota: El Archivo se Agregará en cuanto se Guarde la Actualización del Bien Mueble."
                                                        Font-Bold="true"></asp:Label>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <hr />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="4">
                                                    <div runat="server" id="Div_Listado_Anexos" style="width: 98%; overflow: auto; height: 280px;">
                                                        <asp:GridView ID="Grid_Listado_Anexos" runat="server" AutoGenerateColumns="False"
                                                            CellPadding="4" ForeColor="#333333" GridLines="None" OnRowDataBound="Grid_Listado_Anexos_RowDataBound"
                                                            PageSize="100" Width="100%" CssClass="GridView_1" AllowPaging="true">
                                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                                            <Columns>
                                                                <asp:BoundField DataField="NO_REGISTRO" SortExpression="SEGUN" HeaderText="Según">
                                                                    <ItemStyle Width="170px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="RUTA_ARCHIVO" SortExpression="Ruta" HeaderText="RUTA_ARCHIVO">
                                                                    <ItemStyle Width="170px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Ver">
                                                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="Btn_Ver_Anexo" runat="server" ToolTip="Ver Anexo" AlternateText="Ver Anexo"
                                                                            ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="16px" OnClick="Btn_Ver_Anexo_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                                <asp:BoundField DataField="TIPO_ARCHIVO" HeaderText="Tipo" SortExpression="TIPO_ARCHIVO">
                                                                    <ItemStyle Width="60px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="FECHA_CARGO" HeaderText="Fecha / Hora" SortExpression="FECHA_CARGO"
                                                                    DataFormatString="{0:dd-MMM-yyyy, HH:mm:ss tt}">
                                                                    <ItemStyle Width="150px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="USUARIO_CREO" HeaderText="Autor" SortExpression="USUARIO_CREO">
                                                                    <ItemStyle Width="220px" Font-Size="X-Small" HorizontalAlign="Center" />
                                                                </asp:BoundField>
                                                                <asp:BoundField DataField="DESCRIPCION_ARCHIVO" HeaderText="Descripción" SortExpression="DESCRIPCION_ARCHIVO">
                                                                    <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" />
                                                                </asp:BoundField>
                                                                <asp:TemplateField HeaderText="Eliminar">
                                                                    <ItemStyle Width="40px" HorizontalAlign="Center" />
                                                                    <ItemTemplate>
                                                                        <asp:ImageButton ID="Btn_Eliminar_Anexo" runat="server" ToolTip="Eliminar Anexo"
                                                                            AlternateText="Eliminar Anexo" ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png"
                                                                            OnClientClick="return confirm('¿Esta seguro que desea eliminar el registro de la Base de Datos?');"
                                                                            OnClick="Btn_Eliminar_Anexo_Click" />
                                                                    </ItemTemplate>
                                                                </asp:TemplateField>
                                                            </Columns>
                                                            <HeaderStyle CssClass="GridHeader" />
                                                            <PagerStyle CssClass="GridHeader" />
                                                            <RowStyle CssClass="GridItem" />
                                                            <SelectedRowStyle CssClass="GridSelected" />
                                                        </asp:GridView>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                    </ContentTemplate>
                                </cc1:TabPanel>
                            </cc1:TabContainer>
                        </td>
                    </tr>
                </table>
            </div>
            <br />
            <br />
            <br />
            <br />

            <script type="text/javascript" language="javascript">
                //registra los eventos para la página
                Sys.WebForms.PageRequestManager.getInstance().add_pageLoaded(PageLoaded);
                Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(beginRequest);
                Sys.WebForms.PageRequestManager.getInstance().add_endRequest(endRequestHandler);

                //procedimientos de evento
                function beginRequest(sender, args) { }
                function PageLoaded(sender, args) { }
                function endRequestHandler(sender, args) {
                    $(function() {
                        Auto_Completado_Cuentas_Contables();
                    });
                    function Format_Cuenta(item) {
                        return item.cuenta_descripcion;
                    }
                    function Auto_Completado_Cuentas_Contables() {
                        $("[id$='Txt_Cuenta_Contable']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Contable_Terreno']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Contable_Terreno_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Contable_Terreno_ID']").val(item.cuenta_contable_id);
                        });
                        $("[id$='Txt_Cuenta_Gasto_ID']").autocomplete("Frm_Ope_Pat_Autocompletados.aspx", {
                            extraParams: { accion: 'autocompletado_cuenta_contable' },
                            dataType: "json",
                            parse: function(data) {
                                return $.map(data, function(row) {
                                    return {
                                        data: row,
                                        value: row.cuenta_contable_id,
                                        result: row.cuenta_descripcion

                                    }
                                });
                            },
                            formatItem: function(item) {
                                $("[id$='Hdf_Cuenta_Gasto_ID']").val("");
                                return Format_Cuenta(item);
                            }
                        }).result(function(e, item) {
                            $("[id$='Hdf_Cuenta_Gasto_ID']").val(item.cuenta_contable_id);
                        });
                    }
                } 
            </script>
        </ContentTemplate>
        <Triggers>
            <asp:PostBackTrigger ControlID="Btn_Exportar_Caractetisticas" />
        </Triggers>
    </asp:UpdatePanel>
    <asp:UpdatePanel ID="UpPnl_Detalles_Afectaciones" runat="server" UpdateMode="Conditional">
        <ContentTemplate>
            <asp:Button ID="Btn_Comodin_Afectaciones" runat="server" Text="" Style="display: none;" />
            <cc1:ModalPopupExtender ID="Mpe_Detalles_Afectaciones" runat="server" TargetControlID="Btn_Comodin_Afectaciones"
                PopupControlID="Pnl_Mpe_Detalles_Afectaciones" CancelControlID="Btn_Cerrar_Mpe_Detalles_Afectaciones"
                DropShadow="True" BackgroundCssClass="progressBackgroundFilter" />
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Mpe_Detalles_Afectaciones" runat="server" CssClass="drag" HorizontalAlign="Center"
        Style="display: none; border-style: outset; border-color: Silver; width: 760px;">
        <asp:Panel ID="Pnl_Mpe_Detalles_Afectaciones_Interno" runat="server" CssClass="estilo_fuente"
            Style="cursor: move; background-color: Silver; color: Black; font-size: 12; font-weight: bold;
            border-style: outset;">
            <table class="estilo_fuente" width="100%">
                <tr>
                    <td style="color: Black; font-size: 12; font-weight: bold; width: 90%">
                        <asp:Image ID="Image2" runat="server" ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                        Información de la Afectación
                    </td>
                    <td align="right">
                        <asp:ImageButton ID="Btn_Cerrar_Mpe_Detalles_Afectaciones" CausesValidation="false"
                            runat="server" Style="cursor: pointer;" ToolTip="Cerrar Ventana" ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <center>
            <asp:UpdatePanel ID="UpPnl_Mpe_Detalles_Afectaciones" runat="server" UpdateMode="Conditional">
                <ContentTemplate>
                    <asp:UpdateProgress ID="UpPgr_Mpe_Detalles_Afectaciones" runat="server" AssociatedUpdatePanelID="UpPnl_Mpe_Detalles_Afectaciones"
                        DisplayAfter="0">
                        <ProgressTemplate>
                            <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                            </div>
                            <div class="processMessage" id="div_progress">
                                <img alt="" src="../imagenes/paginas/Updating.gif" /></div>
                        </ProgressTemplate>
                    </asp:UpdateProgress>
                    <br />
                    <div style="border-style: outset; width: 95%; height: 150px; background-color: White;">
                        <table width="100%" class="estilo_fuente">
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_No_Contrato" runat="server" Text="No. Contrato"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Mpe_No_Contrato" runat="server" Style="width: 98%;" Enabled="false"></asp:TextBox>
                                </td>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_Fecha_Afectacion" runat="server" Text="Fecha Afectación"></asp:Label>
                                </td>
                                <td style="width: 35%;">
                                    <asp:TextBox ID="Txt_Mpe_Fecha_Afectacion" runat="server" Style="width: 98%;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_Nuevo_Propietario" runat="server" Text="Nuevo Propietario"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Mpe_Nuevo_Propietario" runat="server" Style="width: 99%;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_Session_Ayuntamiento" runat="server" Text="Sessión Ayunt."></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Mpe_Session_Ayuntamiento" runat="server" Style="width: 99%;"
                                        Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_Tramo" runat="server" Text="Tramo"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Mpe_Tramo" runat="server" Style="width: 99%;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="4">
                                    <hr />
                                </td>
                            </tr>
                            <tr>
                                <td style="width: 15%;">
                                    <asp:Label ID="Lbl_Mpe_Datos_Auditoria" runat="server" Text="Creación"></asp:Label>
                                </td>
                                <td colspan="3">
                                    <asp:TextBox ID="Txt_Mpe_Datos_Auditoria" runat="server" Style="width: 99%;" Enabled="false"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <br />
                </ContentTemplate>
            </asp:UpdatePanel>
        </center>
    </asp:Panel>
</asp:Content>
