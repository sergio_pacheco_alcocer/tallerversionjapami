﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Con_Parametros_Cuentas_Orden_Egresoso.aspx.cs" Inherits="paginas_Contabilidad_Frm_Cat_Con_Parametros_Cuentas_Orden_Egresoso" %>

<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">

    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                    </div>
                    <div class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <table width="100%" border="0" cellspacing="0" class="estilo_fuente">
                <tr align="center">
                    <td class="label_titulo">
                        Catalogo Parámetros Cuentas Orden Egresos
                    </td>
                </tr>
                <tr align="left">
                    <td>
                        <div id="Div_Contenedor_Msj_Error" runat="server">
                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" />
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr class="barra_busqueda" align="right">
                    <td align="left" valign="middle">
                        <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button"
                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" OnClick="Btn_Nuevo_Click" AlternateText="Nuevo" />
                        <asp:ImageButton ID="Btn_Modificar" runat="server" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Modificar" ToolTip="Modificar"
                            OnClick="Btn_Modificar_Click" />
                        <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Salir" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                            Width="24px" CssClass="Img_Button" AlternateText="Salir" OnClick="Btn_Salir_Click" />
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
            </table>
            <div id="Div_Datos_Generales" runat="server" style="width: 100%;">
                <table border="0" cellspacing="0" class="estilo_fuente" width="100%">
                    <tr>
                        <td colspan="24">
                            <asp:HiddenField ID="Hdf_Cuenta_Orden_ID" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="center">
                            Datos Generales
                        </td>
                    </tr>
                    <tr align="right" class="barra_delgada">
                        <td colspan="2" align="center">
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Momento Presupuestal
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_Momento_Presupuestal" runat="server" Width="95%"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Cuenta Contable
                        </td>
                        <td style="width: 80%;">
                            <asp:DropDownList ID="Cmb_Cuenta_Contable" runat="server" Width="95%"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 20%;">
                            Descripción
                        </td>
                        <td style="width: 80%;">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="94%" TextMode="MultiLine"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Descripcion" runat="server" TargetControlID="Txt_Descripcion"
                                WatermarkText="&lt; Limite de 400 Caracteres &gt;" WatermarkCssClass="watermarked">
                            </cc1:TextBoxWatermarkExtender>
                        </td>
                    </tr>
                </table>
            </div>
            <div id="Div_Campos" runat="server" style="width: 100%;">
                <asp:GridView ID="Grid_Listado" runat="server" AutoGenerateColumns="False" CssClass="GridView_1"
                    DataKeyNames="ID" EmptyDataText="No se encontraron registros"
                    GridLines="None" Width="100%" AllowPaging="true" PageSize="20" OnPageIndexChanging="Grid_Listado_PageIndexChanging"
                    OnSelectedIndexChanged="Grid_Listado_SelectedIndexChanged">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Select" ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                            <ItemStyle Width="2%" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="ID" HeaderText="ID" SortExpression="ID">
                        </asp:BoundField>
                        <asp:BoundField DataField="MOMENTO_PRESUPUESTAL" HeaderText="Momento Presupuestal" SortExpression="MOMENTO_PRESUPUESTAL">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Cuenta_Descripcion" HeaderText="Cuenta" SortExpression="Cuenta_Descripcion" Visible="True">
                            <HeaderStyle HorizontalAlign="Left" />
                            <ItemStyle HorizontalAlign="Left" Width="78%" />
                        </asp:BoundField>
                    </Columns>
                    <AlternatingRowStyle CssClass="GridAltItem" />
                    <HeaderStyle BackColor="#2F4E7D" ForeColor="White" />
                    <PagerStyle CssClass="GridHeader" />
                    <RowStyle CssClass="GridItem" />
                    <SelectedRowStyle CssClass="GridSelected" />
                </asp:GridView>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>

</asp:Content>

