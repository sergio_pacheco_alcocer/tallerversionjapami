﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using System.Data;
using SharpContent.ApplicationBlocks.Data;

public partial class paginas_Contabilidad_Frm_Ope_Con_Actualizar_Saldos : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void Btn_saldos_Click(object sender, EventArgs e)
    {
        String Mi_SQL = "";
        DataSet Ds_Cuentas = new DataSet();
        String Fecha;
        String Fecha_Saldo;
        DataSet _DataSet_Existe= new DataSet();
        String Cuenta_Contable_ID;
        DataTable Saldo;
        Double Saldo2;
        int contador;
        int Bandera;
        try
        {
            Mi_SQL = "SELECT * FROM CAT_CON_CUENTAS_CONTABLES WHERE  afectable='SI'";
            Ds_Cuentas = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
            for (int i = 0; i < Ds_Cuentas.Tables[0].Rows.Count; i++)
            {
                Mi_SQL = "SELECT * FROM OPE_CON_POLIZAS_DETALLES WHERE CUENTA_Contable_id = '" + Ds_Cuentas.Tables[0].Rows[i]["cuenta_contable_id"].ToString().Trim().ToUpper() + "' order by fecha,consecutivo,partida";
                 _DataSet_Existe = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                if (_DataSet_Existe.Tables[0].Rows.Count == 0)
                {
                    _DataSet_Existe = null;
                    Mi_SQL = "SELECT * FROM OPE_CON_POLIZAS_DETALLES WHERE CUENTA_Contable_id = '" + Ds_Cuentas.Tables[0].Rows[i]["cuenta_contable_id"].ToString().Trim().ToUpper() + "' and FECHA<='11/11/2013' order by fecha,consecutivo,partida";
                    _DataSet_Existe = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                }
                //Fecha = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime("10/12/2000"));
                //for (int j = 0; j < _DataSet_Existe.Tables[0].Rows.Count; j++)
                if (_DataSet_Existe.Tables[0].Rows.Count > 0)
                {
                    int j = 0;
                    Cuenta_Contable_ID = _DataSet_Existe.Tables[0].Rows[j]["cuenta_contable_id"].ToString().Trim().ToUpper();
                    Bandera = 0;
                    Saldo2 = 0;
                    contador = 0;
                    //se le resta a la fecha un dia 
                    Fecha = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(_DataSet_Existe.Tables[0].Rows[j]["fecha"].ToString().Trim().ToUpper()).AddDays(-1));
                    //Obtiene el ultimo movimiento de acuerdo a la fecha
                    Mi_SQL = "SELECT MAX(" + Ope_Con_Polizas_Detalles.Campo_Fecha + ")";
                    Mi_SQL += "FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + "<=CONVERT(DATETIME," + "'" + Fecha + "')";
                    Fecha_Saldo = SqlHelper.ExecuteScalar(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).ToString();
                    if (Fecha_Saldo != "" && Fecha_Saldo != null)
                    {
                        Fecha_Saldo = string.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Fecha_Saldo));
                        Mi_SQL = "SELECT " + Ope_Con_Polizas_Detalles.Campo_Saldo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                        Mi_SQL += " FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles + " WHERE " + Ope_Con_Polizas_Detalles.Campo_Fecha + "=CONVERT(DATETIME,'" + Fecha_Saldo + "')";
                        Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", ";
                        Mi_SQL += Ope_Con_Polizas_Detalles.Campo_Partida;
                        Saldo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                        if (Saldo.Rows.Count > 0)
                        {
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Saldo].ToString());
                        }
                    }
                    else
                    {
                        Saldo2 = 0;
                        Bandera = 1;
                        Fecha_Saldo = Fecha;
                    }
                    //Actualiza saldos
                    Mi_SQL = "SELECT * FROM " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                    Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Cuenta_Contable_ID + "' ";
                    Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Fecha + ">= CONVERT(DATETIME,'" + Fecha_Saldo + "')";
                    Mi_SQL += " ORDER BY " + Ope_Con_Polizas_Detalles.Campo_Fecha + ", " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + ", " + Ope_Con_Polizas_Detalles.Campo_Partida;
                    Saldo = SqlHelper.ExecuteDataset(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL).Tables[0];
                    if (Saldo2 == 0 && Bandera == 1 && Saldo.Rows.Count > 0)
                    {
                        if (Ds_Cuentas.Tables[0].Rows[i]["TIPO_CUENTA"].ToString().Trim().ToUpper() == "DEUDOR")
                        {
                            //Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Debe) - Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Haber)));
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                        }
                        else
                        {
                            //Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Haber) - Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Debe)));
                            Saldo2 = Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                        }

                    }
                    if (Saldo.Rows.Count > 0)
                    {
                        while (contador < Saldo.Rows.Count)
                        {
                            if (contador > 0)
                            {
                                if (Ds_Cuentas.Tables[0].Rows[i]["TIPO_CUENTA"].ToString().Trim().ToUpper() == "DEUDOR")
                                {
                                    Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                                }
                                else
                                {
                                    Saldo2 = Saldo2 + Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) - Convert.ToDouble(Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                                }
                            }
                            Mi_SQL = "UPDATE " + Ope_Con_Polizas_Detalles.Tabla_Ope_Con_Polizas_Detalles;
                            Mi_SQL += " SET " + Ope_Con_Polizas_Detalles.Campo_Saldo + " = " + Saldo2;
                            Mi_SQL += " WHERE " + Ope_Con_Polizas_Detalles.Campo_Consecutivo + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Consecutivo].ToString();
                            Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Partida + " = " + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Partida].ToString();
                            Mi_SQL += " AND " + Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID + "='" + Saldo.Rows[contador][Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() + "'";
                            //Manda Mi_SQL para ser procesada por ORACLE.
                            SqlHelper.ExecuteNonQuery(Cls_Constantes.Str_Conexion, CommandType.Text, Mi_SQL);
                            contador = contador + 1;
                        }
                    }
                }
            }
            ScriptManager.RegisterStartupScript(this, this.GetType(), "CUENTAS", "alert('ACTUALIZADAS ');", true);
        }//FIN DEL TRY
        catch (Exception ex)
        {
            Response.Write("<h3>Error: " + ex.Message + "</h3>");
        }
    }
}