<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Cat_Con_Matriz.aspx.cs" Inherits="paginas_Contabilidad_Frm_Cat_Con_Matriz" Title="Cat�logo de Matriz" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
  <asp:UpdateProgress ID="Uprg_Reporte" runat="server" 
        AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
        <ProgressTemplate>
            <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
            <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
        </ProgressTemplate>
    </asp:UpdateProgress>
    <cc1:ToolkitScriptManager ID="ScriptManager_Matriz" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
          <center>
            <div id="Div_Matriz" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Cat&aacute;logo de Matriz</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <div id="Div_Contenedor_Msj_Error" style="width:98%;" runat="server" visible="false">
                            <table style="width:100%;">
                              <tr>
                                <td colspan="2" align="left">
                                  <asp:ImageButton ID="IBtn_Imagen_Error" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                    Width="24px" Height="24px" Enabled="false"/>
                                    <asp:Label ID="Lbl_Ecabezado_Mensaje" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>            
                              </tr>
                              <tr>
                                <td style="width:10%;">              
                                </td>          
                                <td style="width:90%;text-align:left;" valign="top">
                                  <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" />
                                </td>
                              </tr>          
                            </table>                   
                          </div>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" CssClass="Img_Button" 
                                TabIndex="1" ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                onclick="Btn_Nuevo_Click" />
                            <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                onclick="Btn_Modificar_Click" />
                            <%--<asp:ImageButton ID="Btn_Eliminar" runat="server" ToolTip="Eliminar" CssClass="Img_Button" 
                                TabIndex="2" ImageUrl="~/paginas/imagenes/paginas/icono_eliminar.png" 
                                onclick="Btn_Eliminar_Click" />--%>
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="4" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png"
                                onclick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%">B&uacute;squeda
                            <asp:TextBox ID="Txt_Busqueda_Matriz" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar por Cuenta"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_Matriz" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Busqueda>" TargetControlID="Txt_Busqueda_Matriz" />
                            <asp:ImageButton ID="Btn_Buscar_Cuenta" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                OnClick="Btn_Consultar_Click"/>
                        </td> 
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td>
                            <div id="Div_Matriz_Detalle" runat="server" >
                                <table width="100%">
                                    <tr>
                                        <td colspan="4">&nbsp;
                                            <asp:HiddenField id="Hf_Matriz_Id" runat="server"/>
                                            <asp:HiddenField id="Hf_No_Matriz" runat="server"/>
                                            <asp:HiddenField id="Hf_Tipo_Matriz" runat="server"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:18%; text-align:left;">
                                            <asp:Label ID="Lbl_Matriz_ID" runat="server" Text="* No Matriz(COG-CRI)" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:17%; text-align:left;">
                                            <asp:TextBox ID="Txt_No_Matriz" runat="server" Width="99%" MaxLength="5"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender  ID="FTE_Txt_No_Matriz" runat="server" FilterType="Numbers"  TargetControlID="Txt_No_Matriz" ></cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style="width:10%; text-align:left;">
                                            <asp:Label ID="Lbl_Tipo" runat="server" Text="&nbsp; * Tipo" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="width:52%; text-align:left;">
                                            <asp:DropDownList ID="Cmb_Tipo" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <asp:Label ID="Lbl_Nombre" runat="server" Text=" * Nombre" CssClass="estilo_fuente" ></asp:Label>
                                        </td>
                                        <td style="text-align:left;" colspan="3">
                                            <asp:TextBox ID="Txt_Nombre" runat="server" Width="98.5%" MaxLength="250" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <asp:Label ID="Lbl_Tipo_Gasto" runat="server" Text="* Tipo Gasto" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList ID="Cmb_Tipo_Gasto" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                        <td style="text-align:left;">
                                            <asp:Label ID="Lbl_Nivel" runat="server" Text="&nbsp; * Nivel" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="text-align:left;">
                                            <asp:DropDownList ID="Cmb_Nivel" runat="server" Width="40%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Nivel_SelectedIndexChanged"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <asp:Label ID="Lbl_Cuenta_Cargo" runat="server" Text="* Cuenta Cargo" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Cuenta_Cargo" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="text-align:left;">
                                            <asp:Label ID="Lbl_Cuenta_Abono" runat="server" Text="* Cuenta Abono" CssClass="estilo_fuente"></asp:Label>
                                        </td>
                                        <td style="text-align:left;" colspan="3">
                                            <asp:DropDownList ID="Cmb_Cuenta_Abono" runat="server" Width="99%"></asp:DropDownList>
                                        </td>
                                    </tr> 
                                    <tr><td colspan="4">&nbsp;</td></tr>
                                </table>
                                <asp:Panel ID="Pnl_Matriz" runat="server"  GroupingText="Matriz de Conversi&oacute;n" Width="99%" >
                                  <div style="height:auto; max-height:300px; overflow:auto; width:100%; vertical-align:top;">
                                    <table style="width:100%;">
                                        <tr>
                                            <td style="width:100%;">
                                                <asp:GridView ID="Grid_Matriz" runat="server" style="white-space:normal"
                                                    CssClass="GridView_1" 
                                                    AutoGenerateColumns="false" GridLines="None" Width="100%"
                                                    EmptyDataText="No se encontraron registros"
                                                    DataKeyNames="Matriz_ID"
                                                    OnSorting = "Grid_Matriz_Sorting" AllowSorting ="true" 
                                                    OnSelectedIndexChanged="Grid_Matriz_SelectedIndexChanged">
                                                    <Columns>
                                                        <asp:ButtonField ButtonType="Image" CommandName="Select"
                                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                                            <ItemStyle Width="2%" />
                                                        </asp:ButtonField>
                                                        <asp:BoundField DataField="NO_MATRIZ" HeaderText="COG-CRI" SortExpression="NO_MATRIZ">
                                                            <HeaderStyle HorizontalAlign="Left" Width="8%"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="11px"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TIPO" HeaderText="Tipo" SortExpression="TIPO">
                                                            <HeaderStyle HorizontalAlign="Left" Width="4%"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="4%" Font-Size="11px"/>
                                                        </asp:BoundField>                                                                          
                                                        <asp:BoundField DataField="NOMBRE" HeaderText="Nombre" SortExpression="NOMBRE">
                                                            <HeaderStyle HorizontalAlign="Left" Width="22%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="22%" Font-Size="11px"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="TIPO_GASTO" HeaderText="Tipo Gasto" SortExpression="TIPO_GASTO">
                                                            <HeaderStyle HorizontalAlign="Center" Width="10%"/>
                                                            <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="11px"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA_CARGO" HeaderText="Cuenta Cargo" SortExpression="CUENTA_CARGO">
                                                            <HeaderStyle HorizontalAlign="Left" Width="26%"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="26%" Font-Size="11px"/>
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA_ABONO" HeaderText="Cuenta Abono" SortExpression="CUENTA_ABONO">
                                                            <HeaderStyle HorizontalAlign="Left" Width="25%"/>
                                                            <ItemStyle HorizontalAlign="Left" Width="25%" Font-Size="11px"/>
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                    onclick="Btn_Eliminar_Click" CommandArgument='<%#Eval("MATRIZ_ID")%>'
                                                                    OnClientClick="return confirm('�Esta seguro que desea elimina el registro?');"
                                                                    ToolTip="Eliminar"/>
                                                            </ItemTemplate>
                                                            <ItemStyle HorizontalAlign ="Center" Font-Size="X-Small" Width="2%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="CUENTA_ID_ABONO" />
                                                        <asp:BoundField DataField="CUENTA_ID_CARGO" />
                                                        <asp:BoundField DataField="MATRIZ_ID" />
                                                    </Columns>
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                </asp:GridView>                                
                                            </td>
                                        </tr>
                                    </table>
                                   </div>
                                </asp:Panel>
                            </div> 
                                    <%--<tr>
                                        <td colspan="4">
                                            <asp:Panel runat="server" >
                                            
                                            </asp:Panel>
                                            <div style="overflow:auto;height:250px;width:98%;vertical-align:top;">
                                                <asp:GridView ID="Grid_Matriz" runat="server" AllowPaging="True" Width="99%"
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                     OnRowDataBound="Grid_Cuentas_RowDataBound">
                                                    <Columns>                                               
                                                           <asp:BoundField DataField="Cuenta_ID_debe" HeaderText="ID">
                                                             <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Cuenta_Debe" HeaderText="Numero Cuenta">
                                                             <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                             <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                           </asp:BoundField>
                                                           <asp:BoundField DataField="Descripcion_Debe" HeaderText="Cuenta">
                                                           <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                           <ItemStyle HorizontalAlign="Left" Width="40%" />
                                                           </asp:BoundField>
                                                           <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <center>
                                                                    <asp:ImageButton ID="Btn_Eliminar" runat="server" 
                                                                        CausesValidation="false" 
                                                                        ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                        OnClick="Btn_Eliminar_Partida" 
                                                                        OnClientClick="return confirm('�Est� seguro de eliminar de la tabla la cuneta seleccionada?');" />
                                                                </center>
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                        </asp:TemplateField>
                                                    </Columns>                                                    
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView> 
                                           </div>                            
                                        </td>
                                    </tr> --%>
                                
                            <%--<div id="Div_Grid_Matriz"  style ="overflow:auto;height:600px;left:20px;">
                                <asp:GridView ID="Grid_Cat_Matriz_Cuentas"  runat="server"
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    DataKeyNames="Cuenta_ID_Haber" GridLines="None" 
                                    OnRowDataBound="Grid_Matriz_Cuentas_Debe_RowDataBound" Width="95%">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                    ImageUrl="~/paginas/imagenes/paginas/plus.gif" />
                                            </ItemTemplate>
                                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                            <ItemStyle HorizontalAlign="Left" Width="2%" />
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Cuenta_ID_Haber" HeaderText="Cuenta_ID_Haber">
                                            <HeaderStyle HorizontalAlign="Left" Width="5%" />
                                            <ItemStyle HorizontalAlign="Left" Width="5%" />
                                        </asp:BoundField>
                                         <asp:BoundField DataField="Cuenta" HeaderText="CUENTA(Haber)">
                                            <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                            <ItemStyle HorizontalAlign="Left" Width="30%" />
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Descripcion" HeaderText="NOMBRE">
                                            <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                            <ItemStyle HorizontalAlign="Left" Width="60%" />
                                        </asp:BoundField>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Label ID="Lbl_cuneta" runat="server"  
                                                    Text='<%# Bind("Cuenta_ID_Haber") %>' Visible="false"></asp:Label>
                                                <asp:Literal ID="Ltr_Inicio" runat="server"       
                                                   Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:none;position:relative;overflow:auto;'&gt;&lt;td colspan='4';left-padding:30px;&gt;" />
                                                <asp:GridView ID="Grid_matriz_Detalles" runat="server" AllowPaging="False"  
                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="80%">
                                                    <Columns>
                                                        <asp:BoundField DataField="Cuenta_ID_Debe" HeaderText="Cuenta_ID_Debe">
                                                            <HeaderStyle HorizontalAlign="Center" Width="0%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="0%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="CUENTA_Debe" HeaderText="CUENTA(Debe)">
                                                            <HeaderStyle HorizontalAlign="Left" Width="30%" />
                                                            <ItemStyle HorizontalAlign="left" Width="30%" />
                                                        </asp:BoundField>
                                                        <asp:BoundField DataField="Descripcion_Debe" HeaderText="NOMBRE">
                                                            <HeaderStyle HorizontalAlign="Left" Width="50%" />
                                                            <ItemStyle HorizontalAlign="left" Width="50%" />
                                                        </asp:BoundField>
                                                    </Columns>
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                    <PagerStyle CssClass="GridHeader" />
                                                    <HeaderStyle CssClass="tblHead" />
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                </asp:GridView>
                                                </div>
                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <PagerStyle CssClass="GridHeader" />
                                    <HeaderStyle CssClass="tblHead" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>--%>
                        </td>
                    </tr>
                </table>
            </div>
          </center>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

