﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Cierre_Anual.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Cierre_Anual" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script language="javascript" type="text/javascript">
    function Abrir_Modal_Popup() {
        $find('Busqueda_Cuentas_Contables').show();
        return false;
    }
    function Limpiar_Ctlr() {
        document.getElementById("<%=Txt_Busqueda_Cuenta_Contable.ClientID%>").value = "";
        return false;
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
<cc1:ToolkitScriptManager ID="ScriptManager_Polizas" runat="server" EnableScriptGlobalization="true" AsyncPostBackTimeout="9999999"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server"  AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
        </asp:UpdateProgress>
            <div id="Div_Cierre_Anual">
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Cierre Anual</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>               
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">
                        </td> 
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td width="15%"></td>
                        <td width="20%"></td>
                        <td width="10%"></td>
                        <td width="10%"></td>
                        <td width="45%"></td>
                    </tr>
                    <tr>
                        <td>*Cuenta Inicial</td>
                        <td colspan="4">
                            <asp:DropDownList ID="Cmb_Cuenta_Inicial" runat="server" Width="99%" 
                                AutoPostBack="True" onselectedindexchanged="Cmb_Cuenta_Inicial_SelectedIndexChanged"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>*Cuenta Final</td>
                        <td colspan="4">
                            <asp:DropDownList ID="Cmb_Cuenta_Final" runat="server" Width="99%"></asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td>*Cuenta Cierre</td>
                        <td style=" width:30%">
                            <asp:TextBox ID="Txt_Nueva_Cuenta" runat="server" Width="75%" 
                                AutoPostBack="True" ontextchanged="Txt_Nueva_Cuenta_TextChanged"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nueva_Cuenta" runat="server" TargetControlID="Txt_Nueva_Cuenta"
                                FilterType="Custom" ValidChars="1234567890">
                            </cc1:FilteredTextBoxExtender>
                            <asp:UpdatePanel ID="Udp_Busqueda_Cuenta_Modal" runat="server" UpdateMode="Conditional" RenderMode="Inline">
                                <ContentTemplate> 
                                        <asp:ImageButton ID="Btn_Mostrar_Busqueda_Cuentas" runat="server"
                                        ToolTip="Busqueda d Cuenta Contable" TabIndex="1"
                                        ImageUrl="~/paginas/imagenes/paginas/Busqueda_00001.png" Height="15px" Width="15px"
                                        OnClientClick="javascript:return Abrir_Modal_Popup();" CausesValidation="false" />
                                        <cc1:ModalPopupExtender  ID="Mpe_Busqueda_Cuenta_Contable" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Busqueda_Cuentas_Contables"
                                         PopupControlID="Pnl_Busqueda_Cuentas_Contables" TargetControlID="Btn_Abrir"  
                                        CancelControlID="Btn_Cerrar" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                        <asp:Button Style="background-color: transparent; border-style:none; width:.2px;" 
                                        ID="Btn_Abrir" runat="server" Text="" />
                                        <asp:Button  Style="background-color: transparent; border-style:none; width:.2px;" 
                                        ID="Btn_Cerrar" runat="server" Text="" />
                                 </ContentTemplate>
                             </asp:UpdatePanel>
                        </td>
                        <td style="text-align: center">*A&ntilde;o</td>
                        <td style=" width:30%" >
                            <asp:TextBox ID="Txt_Anio" runat="server" Width="75%"></asp:TextBox>
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Anio" runat="server" TargetControlID="Txt_Anio"
                                FilterType="Custom" ValidChars="1234567890">
                            </cc1:FilteredTextBoxExtender>
                        </td>
                    <tr>
                        <td></td>
                        <td colspan="3">
                            <asp:Label ID="Lbl_Recordatorio" runat="server" Text="**RECUERDA QUE ANTES DEBES CREAR LA CUENTA DE CIERRE"></asp:Label>
                        </td>
                    </tr>
                    </tr>
                    <tr>
                        <td>Descripci&oacute;n</td>
                        <td colspan="4">
                            <asp:TextBox ID="Txt_Descripcion" runat="server" Width="99%" 
                                 MaxLength="400" TextMode="MultiLine"></asp:TextBox>
                        </td>
                    </tr>
                    <tr><td></td></tr>
                    <tr>
                        <td colspan="4" style="text-align: center">
                            <asp:Button ID="Btn_Cierre_Anual" runat="server" Text="Generar Cierre" 
                                onclick="Btn_Cierre_Anual_Click" OnClientClick="return confirm('¿Está seguro de realizar el Cierre Anual?');"/>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <%--//BUSCAR CUENTA CONTABLE--%>
    <asp:Panel ID="Pnl_Busqueda_Cuentas_Contables" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
        style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
        <asp:Panel ID="Pnl_Cabecera_2" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                       <asp:Image ID="Image2" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                         B&uacute;squeda: Cuentas Contables
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="Btn_Cerrar_Modal" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClick="Btn_Cerrar_Modal_Click"/>  
                    </td>
                </tr>
            </table>
        </asp:Panel>
        <div style="color: #5D7B9D">
            <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Filtros_Busqueda_Cuentas_Contables" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Progress_Upnl_Filtros_Busqueda_Cuentas" runat="server" AssociatedUpdatePanelID="Upnl_Filtros_Busqueda_Cuentas_Contables" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                <table width="100%">
                                 <tr>
                                        <td colspan="4">
                                            <table style="width:80%;">
                                              <tr>
                                                <td align="left">
                                                  <asp:ImageButton ID="Img_Error_Busqueda_Cienta_Contable" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                    Width="24px" Height="24px" Visible="false" />
                                                    <asp:Label ID="Lbl_Error_Busqueda_Cuenta_Contable" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" Visible=false />
                                                </td>
                                              </tr>
                                            </table>
                                        </td>
                                   </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4" align="right">
                                            <asp:ImageButton ID="Btn_Limpiar_Ctlr_2" runat="server" OnClientClick="javascript:return Limpiar_Ctlr();"
                                                ImageUrl="~/paginas/imagenes/paginas/sias_clear.png" ToolTip="Limpiar Controles de Busqueda"/>                         
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td style="width:20%;text-align:left;font-size:11px;">Descripci&oacute;n</td><td colspan="3" style="text-align:left;font-size:11px;">
                                            <asp:TextBox ID="Txt_Busqueda_Cuenta_Contable" runat="server" Width="98%" TabIndex="11" MaxLength="100"/> 
                                             <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_Cuenta" runat="server" TargetControlID="Txt_Busqueda_Cuenta_Contable"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers"  InvalidChars="&lt;,&gt;,&amp;,',!," ValidChars="ÑñáéíóúÁÉÍÓÚ.">
                                        </cc1:FilteredTextBoxExtender>
                                        </td> 
                                        <td colspan="2" style="width:50%;text-align:left;font-size:11px;"></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan="4"><hr /></td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%;text-align:left;" colspan="4">
                                            <center>
                                                <asp:Button ID="Btn_Busqueda_Cuenta_Contable" runat="server"  Text="Buscar Cuenta Contable" CssClass="button"  
                                                    CausesValidation="false" Width="300px" TabIndex="15" OnClick="Btn_Busqueda_Cuentas_Popup_Click" />
                                            </center>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style="width:100%" colspan ="4">
                                            <div id="Div_Busqueda_Cuentas" runat="server" style="overflow:auto; max-height:200px; width:99%">
                                            <table style="width:100%" >
                                                <tr> 
                                                    <td>
                                                        <asp:GridView ID="Grid_Cuentas_Contables" runat="server" CssClass="GridView_1"  AutoGenerateColumns="False" GridLines="None" Width="99.9%"
                                                                AllowSorting="True" HeaderStyle-CssClass="tblHead">
                                                                <Columns>
                                                                   <asp:TemplateField HeaderText="">
                                                                        <ItemTemplate>
                                                                            <asp:ImageButton ID="Btn_Seleccionar_Fuente" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                 CommandArgument='<%# Eval("Cuenta") %>' OnClick="Btn_Seleccionar_Cuenta_Click" />
                                                                        </ItemTemplate>
                                                                        <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                        <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                    </asp:TemplateField>
                                                                    <asp:BoundField DataField="Cuenta_Contable_ID" HeaderText="Cuenta_Contable_ID">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="CUENTA" HeaderText="Cuenta" SortExpression="Cuenta">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="DESCRIPCION" HeaderText="Descripci&oacute;n" SortExpression="Descripcion">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="60%" />
                                                                        <ItemStyle HorizontalAlign="Left" Width="60%" />
                                                                    </asp:BoundField>
                                                                </Columns>
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                    </td>
                                                 </tr>
                                            </table>
                                            </div>
                                            <hr />
                                        </td>
                                    </tr>                                                       
                                </table>                                                                                                                                                              
                            </ContentTemplate>                                                                   
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr>
                    <td></td>
                </tr>
            </table>                                                   
        </div>
    </asp:Panel>
</asp:Content>