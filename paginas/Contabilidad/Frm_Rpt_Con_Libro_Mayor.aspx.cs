﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
public partial class paginas_Contabilidad_Frm_Rpt_Con_Libro_Mayor : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    DateTime _DateTime = DateTime.Now;
                    int dias = _DateTime.Day;
                    dias = dias * -1;
                    dias++;
                    _DateTime = _DateTime.AddDays(dias);
                    Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Cargar_Cuentas_Contables_Combos(); //Carga los cmbos con la descripci{on de las cuentas contables que se tienen dadas de alta0
                    Limpia_Controles(); //limpia los campos de la forma
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Cmb_Cuenta_Inicial.SelectedIndex = -1;
                    Txt_Cuenta.Text = "";
                    Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
                    Txt_Cuenta_Final.Text = "";
                    Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex = -1;
                    Txt_Cuenta_Inicial.Text = "";
                    Txt_Fecha_Final.Enabled = false;
                    Txt_Fecha_Inicio.Enabled = false;
                    Session["Dt_Datos_Detalles"] = null;
                    Session["Contador"] = null;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
            ///               generación del reporte
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 08-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Reporte()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (String.IsNullOrEmpty(Txt_Cuenta.Text) && Cmb_Cuenta_Inicial.SelectedIndex <= 0)
                {
                    if (String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text) && Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La cuenta a consultar o el rango de cuentas completo <br>";
                        Datos_Validos = false;
                    }
                    if (String.IsNullOrEmpty(Txt_Cuenta_Final.Text) && Cmb_Cuenta_Contable_Final.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La cuenta a consultar o el rango de cuentas completo <br>";
                        Datos_Validos = false;
                    }
                }
                if (!String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text) && Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Cuenta_Final.Text) && Cmb_Cuenta_Contable_Final.SelectedIndex > 0)
                {
                    if (Convert.ToDouble (Txt_Cuenta_Inicial.Text.Trim())>Convert.ToDouble(Txt_Cuenta_Final.Text.Trim()))
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La Cuenta inicial no puede ser mayor a la Cuenta final <br>";
                        Datos_Validos = false;
                    }
                }
                if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La fecha inicial no puede ser mayor a la fecha final <br>";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
        #endregion
        #region (Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cargar_Cuentas_Contables_Combos
            /// DESCRIPCION : Consulta las cuentas contables estan dadas de alta
            /// PARAMETROS  :                           
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Cargar_Cuentas_Contables_Combos()
            {
                DataTable Dt_Cuenta_Contable = new DataTable(); //Variable a contener los valores de la consulta
                DataTable Dt_Cuentas = new DataTable();
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de negocios
                try
                {
                    Rs_Consulta_Cat_Con_Cuentas_Contables.P_Afectable = "SI";
                    Dt_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta las cuentas contables que se tienen dadas de alta para cargar el combo
                    if (Dt_Cuenta_Contable.Rows.Count > 0)
                    {
                        if (Dt_Cuentas.Rows.Count <= 0)
                        {
                            Dt_Cuentas.Columns.Add("DESCRIPCION", typeof(System.String));
                            Dt_Cuentas.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                        }
                        foreach (DataRow Fila in Dt_Cuenta_Contable.Rows)
                        {
                            DataRow row = Dt_Cuentas.NewRow();
                            row["DESCRIPCION"] = Fila["CUENTA"].ToString() + "-" + Fila["DESCRIPCION"].ToString();
                            row["CUENTA_CONTABLE_ID"] = Fila["CUENTA_CONTABLE_ID"].ToString();
                            Dt_Cuentas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Cuentas.AcceptChanges();
                        }
                    }
                    Cmb_Cuenta_Inicial.DataSource = Dt_Cuentas;
                    Cmb_Cuenta_Inicial.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Cmb_Cuenta_Inicial.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Cmb_Cuenta_Inicial.DataBind();
                    Cmb_Cuenta_Inicial.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Cuenta_Inicial.SelectedIndex = 0;
                    // combo cuenta inicial
                    Cmb_Cuenta_Contable_Inicial_Rango.DataSource = Dt_Cuentas;
                    Cmb_Cuenta_Contable_Inicial_Rango.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Cmb_Cuenta_Contable_Inicial_Rango.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Cmb_Cuenta_Contable_Inicial_Rango.DataBind();
                    Cmb_Cuenta_Contable_Inicial_Rango.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex = 0;
                    // Combo cuenta final
                    Cmb_Cuenta_Contable_Final.DataSource = Dt_Cuentas;
                    Cmb_Cuenta_Contable_Final.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                    Cmb_Cuenta_Contable_Final.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;
                    Cmb_Cuenta_Contable_Final.DataBind();
                    Cmb_Cuenta_Contable_Final.Items.Insert(0, new ListItem("<-- Seleccione -->", ""));
                    Cmb_Cuenta_Contable_Final.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception("Cargar_Cuentas_Contables_Combos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ven_Click
            /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Cerrar_Ven_Click(object sender, ImageClickEventArgs e)
            {
                Grid_Cuentas.DataSource = null;   // Se iguala el DataTable con el Grid
                Grid_Cuentas.DataBind();    // Se ligan los datos.
                Txt_Busqueda_Cuenta.Text = "";
                Mpe_Busqueda_Cuenta.Hide();    //Oculta el ModalPopUp
            }
                ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ven_Inicial_Click
            /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Cerrar_Ven_Inicial_Click(object sender, ImageClickEventArgs e)
            {
                Grid_Cuentas_Inicial.DataSource = null;   // Se iguala el DataTable con el Grid
                Grid_Cuentas_Inicial.DataBind();    // Se ligan los datos.
                Txt_Busqueda_Cuenta_Inicial.Text = "";
                Mpe_Busqueda_Cuenta_Inicial.Hide();    //Oculta el ModalPopUp
            }
                ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ven_Final_Click
            /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Cerrar_Ven_Final_Click(object sender, ImageClickEventArgs e)
            {
                Grid_Cuentas_Final.DataSource = null;   // Se iguala el DataTable con el Grid
                Grid_Cuentas_Final.DataBind();    // Se ligan los datos.
                Txt_Busqueda_Cuenta_Final.Text = "";
                Mpe_Busqueda_Cuenta_Final.Hide();    //Oculta el ModalPopUp
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuenta_Inicial_Popup_Click
            /// DESCRIPCION : Ejecuta la busqueda de Polizas
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02/Mayo/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Busqueda_Cuenta_Inicial_Popup_Click(object sender, EventArgs e)
            {
                try
                {
                    String Cuenta = ((Button)sender).CommandArgument;
                    Consulta_Cuenta_Avanzada(Cuenta); //Consulta las polizas de acuerdo a la informacion proporcionada.
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Requisicion_Click
            ///DESCRIPCIÓN: Metodo para consultar la reserva
            ///CREO: Sergio Manuel Gallardo Andrade
            ///FECHA_CREO: 17/noviembre/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Btn_Seleccionar_Solicitud_Click(object sender, ImageClickEventArgs e)
            {
                String Cuenta = ((ImageButton)sender).CommandArgument;
                try
                {
                    Cmb_Cuenta_Inicial.SelectedValue = Cuenta;
                    Consulta_Cuenta_Contable("COMBO");
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Cuenta_Inicial_Click
            ///DESCRIPCIÓN: Metodo para consultar la reserva
            ///CREO: Sergio Manuel Gallardo Andrade
            ///FECHA_CREO: 17/noviembre/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Btn_Seleccionar_Cuenta_Inicial_Click(object sender, ImageClickEventArgs e)
            {
                String Cuenta = ((ImageButton)sender).CommandArgument;
                try
                {
                    Cmb_Cuenta_Contable_Inicial_Rango.SelectedValue = Cuenta;
                    Consulta_Cuenta_Contable_Por_Rango("COMBO","SI");
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Cuenta_Final_Click
            ///DESCRIPCIÓN: Metodo para consultar la reserva
            ///CREO: Sergio Manuel Gallardo Andrade
            ///FECHA_CREO: 17/noviembre/2011
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Btn_Seleccionar_Cuenta_Final_Click(object sender, ImageClickEventArgs e)
            {
                String Cuenta = ((ImageButton)sender).CommandArgument;
                try
                {
                    Cmb_Cuenta_Contable_Final.SelectedValue = Cuenta;
                    Consulta_Cuenta_Contable_Por_Rango("COMBO", "NO");
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Avanzada
            /// DESCRIPCION : Ejecuta la busqueda de Cuenta
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 22/Septiembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cuenta_Avanzada(String tipo)
            {
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cuenta = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Cuentas; //Variable que obtendra los datos de la consulta 
                Double bandera = 0;
                Lbl_Error_Busqueda_Cuenta.Visible = false;
                Img_Error_Busqueda_Cuenta.Visible = false;
                try
                {
                    if (tipo.Equals("1"))
                    {
                        if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta.Text))
                        {
                            Rs_Consulta_Cuenta.P_Descripcion = Txt_Busqueda_Cuenta.Text.ToString().ToUpper();
                            bandera = 1;
                        }
                        if (Txt_Busqueda_Cuenta.Text == "")
                        {
                            bandera = 2;
                            Lbl_Error_Busqueda_Cuenta.Text = "<br> Debes Ingresar una Descripcion  <br>";
                        }
                    }
                    if (tipo.Equals("2"))
                    {
                        if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta_Inicial.Text))
                        {
                            Rs_Consulta_Cuenta.P_Descripcion = Txt_Busqueda_Cuenta_Inicial.Text.ToString().ToUpper();
                            bandera = 1;
                        }
                        if (Txt_Busqueda_Cuenta_Inicial.Text == "")
                        {
                            bandera = 2;
                            Lbl_Error_Busqueda_Cuenta_Inicial.Text = "<br> Debes Ingresar una Descripcion  <br>";
                        }
                    }
                    if (tipo.Equals("3"))
                    {
                        if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta_Final.Text))
                        {
                            Rs_Consulta_Cuenta.P_Descripcion = Txt_Busqueda_Cuenta_Final.Text.ToString().ToUpper();
                            bandera = 1;
                        }
                        if (Txt_Busqueda_Cuenta_Final.Text == "")
                        {
                            bandera = 2;
                            Lbl_Error_Busqueda_Cuenta_Final.Text = "<br> Debes Ingresar una Descripcion  <br>";
                        }
                    }
                    if (bandera == 1)
                    {
                        Rs_Consulta_Cuenta.P_Afectable = "SI";
                        Dt_Cuentas = Rs_Consulta_Cuenta.Consulta_Cuentas_Contables();
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            if (tipo.Equals("1"))
                            {
                                // se llena el grid de cuentas encontradas
                                Grid_Cuentas.Columns[1].Visible = true;
                                Grid_Cuentas.DataSource = Dt_Cuentas;   // Se iguala el DataTable con el Grid
                                Grid_Cuentas.DataBind();    // Se ligan los datos.
                                Grid_Cuentas.Visible = true;
                                Grid_Cuentas.Columns[1].Visible = false;
                            }
                            if (tipo.Equals("2"))
                            {
                                // se llena el grid de cuentas encontradas
                                Grid_Cuentas_Inicial.Columns[1].Visible = true;
                                Grid_Cuentas_Inicial.DataSource = Dt_Cuentas;   // Se iguala el DataTable con el Grid
                                Grid_Cuentas_Inicial.DataBind();    // Se ligan los datos.
                                Grid_Cuentas_Inicial.Visible = true;
                                Grid_Cuentas_Inicial.Columns[1].Visible = false;
                            }
                            if (tipo.Equals("3"))
                            {
                                // se llena el grid de cuentas encontradas
                                Grid_Cuentas_Final.Columns[1].Visible = true;
                                Grid_Cuentas_Final.DataSource = Dt_Cuentas;   // Se iguala el DataTable con el Grid
                                Grid_Cuentas_Final.DataBind();    // Se ligan los datos.
                                Grid_Cuentas_Final.Visible = true;
                                Grid_Cuentas_Final.Columns[1].Visible = false;
                            }
                        }
                        else
                        {
                            if (tipo.Equals("1"))
                            {

                                Lbl_Error_Busqueda_Cuenta.Text = "<br> No se encontraron Coincidencias con la busqueda  <br>";
                                Lbl_Error_Busqueda_Cuenta.Visible = true;
                                Img_Error_Busqueda_Cuenta.Visible = true;
                            }
                            if (tipo.Equals("2"))
                            {

                                Lbl_Error_Busqueda_Cuenta_Inicial.Text = "<br> No se encontraron Coincidencias con la busqueda  <br>";
                                Lbl_Error_Busqueda_Cuenta_Inicial.Visible = true;
                                Lbl_Error_Busqueda_Cuenta_Inicial.Visible = true;
                            }
                            if (tipo.Equals("3"))
                            {

                                Lbl_Error_Busqueda_Cuenta_Final.Text = "<br> No se encontraron Coincidencias con la busqueda  <br>";
                                Lbl_Error_Busqueda_Cuenta_Final.Visible = true;
                                Lbl_Error_Busqueda_Cuenta_Final.Visible = true;
                            }
                        }
                        if (tipo.Equals("1"))
                        {
                            Grid_Cuentas.SelectedIndex = -1;
                            Mpe_Busqueda_Cuenta.Show();
                        }
                        else
                        {
                            if (tipo.Equals("2"))
                            {
                                Grid_Cuentas_Inicial.SelectedIndex = -1;
                                Mpe_Busqueda_Cuenta_Inicial.Show();
                            }else{
                                Grid_Cuentas_Final.SelectedIndex = -1;
                                Mpe_Busqueda_Cuenta_Final.Show();
                            }
                        }
                    }
                    else
                    {
                        if (tipo.Equals("1"))
                        {
                            if (bandera == 0) Lbl_Error_Busqueda_Cuenta.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                            Lbl_Error_Busqueda_Cuenta.Visible = true;
                            Img_Error_Busqueda_Cuenta.Visible = true;
                            Mpe_Busqueda_Cuenta.Show();
                        }
                        else
                        {
                            if (tipo.Equals("2"))
                            {
                                if (bandera == 0) Lbl_Error_Busqueda_Cuenta_Inicial.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                                Lbl_Error_Busqueda_Cuenta_Inicial.Visible = true;
                                Img_Error_Busqueda_Cuenta_inicial.Visible = true;
                                Mpe_Busqueda_Cuenta_Inicial.Show();
                            }
                            else
                            {
                                if (bandera == 0) Lbl_Error_Busqueda_Cuenta_Final.Text = "<br> Debes ingresar un filtro para poder realizar la busqueda  <br>";
                                Lbl_Error_Busqueda_Cuenta_Final.Visible = true;
                                Img_Error_Busqueda_Cuenta_Final.Visible = true;
                                Mpe_Busqueda_Cuenta_Final.Show();
                            }
                        }
                       
                    }

                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Cuenta_Avanzada " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable
            /// DESCRIPCION : Consulta que la cuenta contable que proporciono el usuario este
            ///               dada de alta si es así consulta el ID o Cuenta a la cual pertenece
            /// PARAMETROS  : Tipo_Busqueda: Indica si la consulta se realiza desde una caja
            ///                              de Texto o Combo y de acuerdo a esto realiza el
            ///                              filtro que debera llevar
            ///               Cuenta_Incia : Indica si se esta consultando la cuenta desde donde
            ///                              se iniciara la consulta de la cuentas contables                              
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cuenta_Contable(String Tipo_Busqueda)
            {
                DataTable Dt_Cuenta_Contable = new DataTable(); //Variable a contener los valores de la consulta
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Temporal = new DataTable();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                try
                {
                    //Si se pretende consultar la cuenta contable del combo de la cuenta contable
                    if (Tipo_Busqueda == "COMBO")
                    {
                        Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Inicial.SelectedValue;
                        Txt_Cuenta.Text = "";
                    }
                    //Si se pretende consultar el ID de la cuenta contable proporcionada por el usuario
                    else
                    {
                        Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta = Txt_Cuenta.Text.ToString();
                        Cmb_Cuenta_Inicial.SelectedIndex = -1;
                    }
                    Dt_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta el ID y número de cuenta de la cuenta que se pretende buscar
                    //Agrega los datos de la consulta en los controles correspondientes
                    if (Dt_Cuenta_Contable.Rows.Count > 0)
                    {
                        if (Tipo_Busqueda == "COMBO")
                        {
                            Txt_Cuenta.Text = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                        }
                        //Selecciona el ID de la cuenta contable que fue proporcionada por el usuario
                        else
                        {
                            Dt_Cuenta_Contable.DefaultView.RowFilter = "CUENTA=" + Txt_Cuenta.Text.ToString();
                            if (Dt_Cuenta_Contable.DefaultView.Count > 0)
                            {

                                Cmb_Cuenta_Inicial.SelectedValue = Dt_Cuenta_Contable.DefaultView.ToTable().Rows[0]["CUENTA_CONTABLE_ID"].ToString();
                            }
                            else
                            {
                                Lbl_Mensaje_Error.Text = "Favor de verificar la cuenta que introdujo ya que no se encuentran registros  o no es afectable la cuenta<br>";
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                            }
                        }
                       
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cmb_Cuenta_Inicial_SelectedIndexChanged
            /// DESCRIPCION : consulta la cuenta contable
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 08-MAyo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Cmb_Cuenta_Inicial_Rango_SelectedIndexChanged(object sender, EventArgs e)
            {
                Txt_Cuenta_Inicial.Text = "";
                if (Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex > 0) Consulta_Cuenta_Contable_Por_Rango("COMBO", "SI"); //Consulta el No Cuenta Contable que tiene seleccionado la descripcion de la cuenta proporcionada
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cmb_Cuenta_Contable_Final_SelectedIndexChanged
            /// DESCRIPCION : consulta la cuenta contable
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 08-MAyo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Cmb_Cuenta_Contable_Final_SelectedIndexChanged(object sender, EventArgs e)
            {
                Txt_Cuenta_Final.Text = "";
                if (Cmb_Cuenta_Contable_Final.SelectedIndex > 0) Consulta_Cuenta_Contable_Por_Rango("COMBO", "NO"); //Consulta el No Cuenta Contable que tiene seleccionado la descripcion de la cuenta proporcionada
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable
            /// DESCRIPCION : Consulta que la cuenta contable que proporciono el usuario este
            ///               dada de alta si es así consulta el ID o Cuenta a la cual pertenece
            /// PARAMETROS  : Tipo_Busqueda: Indica si la consulta se realiza desde una caja
            ///                              de Texto o Combo y de acuerdo a esto realiza el
            ///                              filtro que debera llevar
            ///               Cuenta_Incia : Indica si se esta consultando la cuenta desde donde
            ///                              se iniciara la consulta de la cuentas contables                              
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cuenta_Contable_Por_Rango(String Tipo_Busqueda, String Cuenta_Inicial)
            {
                DataTable Dt_Cuenta_Contable = new DataTable(); //Variable a contener los valores de la consulta
                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de negocios

                try
                {
                    //Si se pretende consultar la cuenta contable del combo de la cuenta contable
                    if (Tipo_Busqueda == "COMBO")
                    {
                        if (Cuenta_Inicial == "SI")
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable_Inicial_Rango.SelectedValue;
                            Txt_Cuenta_Inicial.Text = "";
                        }
                        else
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Contable_Final.SelectedValue;
                            Txt_Cuenta_Final.Text = "";
                        }
                    }
                    //Si se pretende consultar el ID de la cuenta contable proporcionada por el usuario
                    else
                    {
                        if (Cuenta_Inicial == "SI")
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Afectable = "SI";
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta = Txt_Cuenta_Inicial.Text.ToString();
                            Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex = -1;
                        }
                        else
                        {
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Afectable = "SI";
                            Rs_Consulta_Cat_Con_Cuentas_Contables.P_Cuenta = Txt_Cuenta_Final.Text.ToString();
                            Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
                        }
                    }
                    Dt_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta el ID y número de cuenta de la cuenta que se pretende buscar
                    //Agrega los datos de la consulta en los controles correspondientes
                    if (Dt_Cuenta_Contable.Rows.Count > 0)
                    {
                        if (Tipo_Busqueda == "COMBO")
                        {
                            if (Cuenta_Inicial == "SI")
                            {
                                Txt_Cuenta_Inicial.Text = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            }
                            else
                            {
                                Txt_Cuenta_Final.Text = Dt_Cuenta_Contable.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                            }
                        }
                        else
                        {
                                if (Cuenta_Inicial == "SI")
                                {
                                    Dt_Cuenta_Contable.DefaultView.RowFilter = "CUENTA=" + Txt_Cuenta_Inicial.Text.ToString();
                                    if (Dt_Cuenta_Contable.DefaultView.Count > 0)
                                    {
                                    Cmb_Cuenta_Contable_Inicial_Rango.SelectedValue = Dt_Cuenta_Contable.DefaultView.ToTable().Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                                    }
                                    else
                                    {
                                        Lbl_Mensaje_Error.Text = "Favor de verificar la cuenta que introdujo ya que no se encuentran registros o no es afectable la cuenta<br>";
                                        Lbl_Mensaje_Error.Visible = true;
                                        Img_Error.Visible = true;
                                    }
                                }
                                else
                                {
                                    Dt_Cuenta_Contable.DefaultView.RowFilter = "CUENTA=" + Txt_Cuenta_Final.Text.ToString();
                                    if (Dt_Cuenta_Contable.DefaultView.Count > 0)
                                    {
                                        Cmb_Cuenta_Contable_Final.SelectedValue = Dt_Cuenta_Contable.DefaultView.ToTable().Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                                    }
                                    else
                                    {
                                        Lbl_Mensaje_Error.Text = "Favor de verificar la cuenta que introdujo ya que no se encuentran registros o no es afectable la cuenta<br>";
                                        Lbl_Mensaje_Error.Visible = true;
                                        Img_Error.Visible = true;
                                    }
                                }
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Txt_Cuenta_Inicial_TextChanged
            /// DESCRIPCION : consulta la cuenta contable
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 08-MAyo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Txt_Cuenta_Inicial_TextChanged(object sender, EventArgs e)
            {
                Cmb_Cuenta_Inicial.SelectedIndex = -1;
                if (!String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text)) Consulta_Cuenta_Contable_Por_Rango("TEXTO", "SI"); //Consulta el ID de la cuenta contable que tiene asignado el n{umero de cuenta que fue proporcionada por el usuario
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Txt_Cuenta_Final_TextChanged
            /// DESCRIPCION : consulta la cuenta contable
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andradde
            /// FECHA_CREO  : 08-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Txt_Cuenta_Final_TextChanged(object sender, EventArgs e)
            {
                Cmb_Cuenta_Contable_Final.SelectedIndex = -1;
                if (!String.IsNullOrEmpty(Txt_Cuenta_Final.Text)) Consulta_Cuenta_Contable_Por_Rango("TEXTO", "NO"); //Consulta el ID de la cuenta contable que tiene asignado el n{umero de cuenta que fue proporcionada por el usuario
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Libro_Mayor
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Libro_Mayor()
            {
                //Double Saldo = 0; //Obtiene el saldo que va teniendo la cuenta de acuerdos a sus diferentes movimientos
                //String Mes; //Obtiene el mes para la genración del reporte
                //String Anio; //Obtiene el año para la genración del reporte
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Libro_Mayor" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Libro = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Libro_Mayor = new DataTable(); //Obtiene los valores a pasar al reporte
                Ds_Rpt_Con_Libro_Mayor Ds_Libro_Mayor = new Ds_Rpt_Con_Libro_Mayor();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Poliza_Detalles = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios

                try
                {
                    if (!String.IsNullOrEmpty(Txt_Cuenta.Text) && Cmb_Cuenta_Inicial.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Inicial = Txt_Cuenta.Text.ToString();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text) && Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Cuenta_Final.Text) && Cmb_Cuenta_Contable_Final.SelectedIndex > 0)
                        {
                            Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.ToString();
                            Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Final = Txt_Cuenta_Final.Text.ToString();
                        }
                    }
                    Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                    Dt_Libro = Rs_Consulta_Ope_Con_Poliza_Detalles.Consulta_Libro_Mayor(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario

                    if (Dt_Libro.Rows.Count > 0)
                    {
                        Dt_Libro.TableName = "Ds_Libro_Mayor";
                        Ds_Libro_Mayor.Clear();
                        Ds_Libro_Mayor.Tables.Clear();
                        Ds_Libro_Mayor.Tables.Add(Dt_Libro.Copy());
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Libro_Mayor.rpt");
                    Reporte.SetDataSource(Ds_Libro_Mayor);

                    ParameterFieldDefinitions Cr_Parametros;
                    ParameterFieldDefinition Cr_Parametro;
                    ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    Cr_Parametro = Cr_Parametros["Cuenta_Contable"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value =Txt_Fecha_Inicio.Text.ToString();
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);
                    
                    Cr_Parametro = Cr_Parametros["Anio"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Txt_Fecha_Final.Text.ToString();
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Mostrar_Reporte
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Mostrar_Reporte()
            {
                DataTable Dt_Libro = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Libro_Mayor = new DataTable(); //Obtiene los valores a pasar al reporte
                Ds_Rpt_Con_Libro_Mayor Ds_Libro_Mayor = new Ds_Rpt_Con_Libro_Mayor();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Poliza_Detalles = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                Boolean Insertar = true;
                try
                {
                    if (!String.IsNullOrEmpty(Txt_Cuenta.Text) && Cmb_Cuenta_Inicial.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Inicial = Txt_Cuenta.Text.ToString();
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Txt_Cuenta_Inicial.Text) && Cmb_Cuenta_Contable_Inicial_Rango.SelectedIndex > 0 && !String.IsNullOrEmpty(Txt_Cuenta_Final.Text) && Cmb_Cuenta_Contable_Final.SelectedIndex > 0)
                        {
                            Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Inicial = Txt_Cuenta_Inicial.Text.ToString();
                            Rs_Consulta_Ope_Con_Poliza_Detalles.P_Cuenta_Final = Txt_Cuenta_Final.Text.ToString();
                        }
                    }
                    Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Rs_Consulta_Ope_Con_Poliza_Detalles.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                    Dt_Libro = Rs_Consulta_Ope_Con_Poliza_Detalles.Consulta_Cuentas_Libro_Mayor(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Libro.Rows.Count > 0)
                    {
                        Session["Dt_Datos_Detalles"] = Dt_Libro;
                        Grid_Cuentas_Movimientos.DataSource = Dt_Libro;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Libro_Mayor.Visible = true;
                        Tr_Grid_Libro.Style.Add("display","block");
                    }
                    else
                    {
                        Grid_Cuentas_Movimientos.DataSource = null;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Libro_Mayor.Visible = false;
                        Tr_Grid_Libro.Style.Add("display","none");
                        Lbl_Mensaje_Error.Text="No tiene Movimientos esta cuenta en el rango de fechas que coloco <br>";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2011 10:01 am
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cuentas_Movimientos = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
                String Cuenta = "";
                int Contador;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        Cuenta = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["CUENTA"].ToString());
                        Rs_Cuentas_Movimientos.P_Cuenta_Inicial = Cuenta;
                        Rs_Cuentas_Movimientos.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                        Rs_Cuentas_Movimientos.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                        Ds_Consulta = Rs_Cuentas_Movimientos.Consulta_Libro_Mayor();
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Movimientos");
                        Gv_Detalles.DataSource = Ds_Consulta;
                        Gv_Detalles.DataBind();
                        Session["Contador"] = Contador + 1;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../../Reporte/";//"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
    #region Eventos
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Cuenta_TextChanged
    ///DESCRIPCIÓN: llena el combo con la cuenta que se agrego
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Txt_Cuenta_TextChanged(object sender, EventArgs e)
    {
        Cmb_Cuenta_Inicial.SelectedIndex = -1;
        if (!String.IsNullOrEmpty(Txt_Cuenta.Text)) Consulta_Cuenta_Contable("TEXTO"); //Consulta el ID de la cuenta contable que tiene asignado el núumero de cuenta que fue proporcionada por el usuario
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Cuenta_Inicial_SelectedIndexChanged
    ///DESCRIPCIÓN: llena el txt con la cuenta que se agrego
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Cmb_Cuenta_Inicial_SelectedIndexChanged(object sender, EventArgs e)
    {
        Txt_Cuenta.Text = "";
        if (Cmb_Cuenta_Inicial.SelectedIndex > 0) Consulta_Cuenta_Contable("COMBO"); //Consulta el No Cuenta Contable que tiene seleccionado la descripcion de la cuenta proporcionada
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Libro_Mayor_Click
    ///DESCRIPCIÓN: hace la llamada del metodo para realizar el reporte en pdf
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Reporte_Libro_Mayor_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Consulta_Libro_Mayor(); //Consulta el libro de mayor de la cuenta seleccionada
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Realiza la Consulta de las cuentas
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Mostrar_Reporte(); //Consulta el libro de mayor de la cuenta seleccionada
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: nos redirecciona a la pagina inicial
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Poliza_Click
    ///DESCRIPCIÓN: se generara la poliza para que el usuario la pueda visualizar
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo andrade
    ///FECHA_CREO : 24-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String fecha = ((LinkButton)sender).CommandArgument;
        String Tipo_poliza = ((LinkButton)sender).CssClass;
        String No_poliza = ((LinkButton)sender).Text;
        Imprimir(No_poliza, Tipo_poliza, fecha);
    }
    #endregion
    #region Poliza
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        String Mes = "";
        String Ano = "";
        try
        {
            Mes = FECHA.Substring(0, 2);
            Ano = FECHA.Substring(2, 2);
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Poliza.P_Mes_Ano = Mes + Ano;
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Poliza(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Poliza(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
#endregion

}
