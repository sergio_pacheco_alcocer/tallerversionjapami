﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
//using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using AjaxControlToolkit;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Gastos_Comprobar.Negocio;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Solicitud_Pagos.Datos;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Deudores.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cuentas_Gastos.Negocio;
using System.Data.SqlClient;

public partial class paginas_Contabilidad_Frm_Ope_Con_Gastos_Comprobar : System.Web.UI.Page
{
    #region (Page Load)
    private static String P_Dt_Solicitud = "P_Dt_Solicitud";
    protected void Page_Load(object sender, EventArgs e)
    {
        Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones   //Limpia los controles del forma
                ViewState["SortDirection"] = "ASC";
                Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                Llenar_Grid_Solicitudes_Pendientes_De_Comprobar();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:         Configuracion_Acceso
    /// DESCRIPCIÓN:    Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:     No Áplica.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   20/Enero/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Salir);
            Botones.Add(Btn_Modificar);
            //Botones.Add(Btn_Mostrar_Popup_Busqueda);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION :   Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS:     Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region Metodos
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        DataTable Dt_Parametros = new DataTable();
        try
        {
            Cls_Cat_Con_Parametros_Negocio consulta_parametros = new Cls_Cat_Con_Parametros_Negocio();
            Dt_Parametros = consulta_parametros.Consulta_Datos_Parametros();
            if (Dt_Parametros.Rows.Count > 0)
            {
                Txt_iva_parametro.Value = Dt_Parametros.Rows[0]["IVA"].ToString().Trim();
                Txt_ISR_parametro.Value = Dt_Parametros.Rows[0]["ISR"].ToString().Trim();
                Txt_Reten_Cedular_parametro.Value = Dt_Parametros.Rows[0]["CEDULAR"].ToString().Trim();
                Txt_reten_iva_parametro.Value = Dt_Parametros.Rows[0]["RETENCION_IVA"].ToString().Trim();
            }
            else
            {
                Txt_iva_parametro.Value = "0";
                Txt_ISR_parametro.Value = "0";
                Txt_Reten_Cedular_parametro.Value = "0";
                Txt_reten_iva_parametro.Value = "0";
            }
            Txt_IEPS.Text = "0";
            Txt_ISH.Text = "0";
            Txt_Total_Comprobacion.Text = "0";
            //Limpiar_Controles();
            HttpContext.Current.Session.Remove("Dt_Documentos");
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Consultar_Datos_Grid();
            //Llenar_Combos_Generales();
            Btn_Modificar.Visible = false;//    ocullta el boton de modificar 
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Total_Comprobar.Text = "";
            Txt_Total.Text = "";
            Txt_No_Solicitud.Text = "";
            Txt_No_Reserva.Text = "";
            Txt_Monto_Solicitud_Pago.Text = "";
            Txt_Fecha_Solicitud.Text = "";
            Txt_Beneficiario.Text = "";
            Cmb_Partida.SelectedIndex = 0;
            Txt_Subtotal.Text = "";
            Txt_Ruta.Text = "";
            Txt_Nombre_Archivo.Text = "";
            Txt_IVA.Text = "0";
            Txt_ISR.Text = "0";
            Txt_CURP.Text = "";
            Txt_Nom_Proveedor.Text = "";
            Txt_Reten_IVA.Text = "0";
            Txt_Reten_Cedular.Text = "";
            Txt_ISH.Text = "0";
            Txt_IEPS.Text = "0";
            Txt_RFC.Text = "";
            Txt_Subtotal_factura.Text = "";
            //  para cheque
            Cmb_Banco.SelectedIndex = 0;
            //Cmb_Cuenta_Banco.SelectedIndex = 0;
            Txt_No_Cheque.Text = "";
            Txt_Fecha_No_Pago.Text = "";
            Txt_Comentario_Pago.Text = "";

            Grid_Documentos.DataSource = new DataTable();
            Grid_Documentos.DataBind();
            if (Session["Consulta_Gastos_Comprobar"] != null)
            {
                Session.Remove("Consulta_Gastos_Comprobar");
            }
            if (Session["Grid_Detalle"] != null)
            {
                Session.Remove("Grid_Detalle");
            }
            Grid_Documentos.DataSource = new DataTable();
            Grid_Documentos.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles_Partida
    /// DESCRIPCION : Limpia los controles que se encuentran en la partida
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 21/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles_Partida()
    {
        try
        {
            //Cmb_Partida.SelectedIndex = 0;
            //Txt_No_Documento.Text = "";
            //Txt_Fecha_Inicial.Text = "";
            //Txt_Monto.Text = "";

        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION :   Habilita y Deshabilita los controles de la forma para prepara la página
    ///                 para a siguiente operación
    /// PARAMETROS:     1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                 si es una alta, modificacion
     /// CREO:          Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:     20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    //Grid_Gastos_Comprobar.SelectedIndex = -1;
                    Div_Presentacion.Style.Value = "overflow:auto;height:600px;width:99%;vertical-align:top;border-style:solid;border-color:Silver;display:block";
                    Div_Detalles.Style.Value = "display:none";
                    Div_Cheque.Style.Value = "display:none";
                    //Configuracion_Acceso("Frm_Ope_Con_Gastos_Comprobar.aspx");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            //  mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Txt_No_Factura_Solicitud_Pago.Enabled = Habilitado;
            Txt_Fecha_Factura_Solicitud_Pago.Enabled = false;
            Txt_Monto_Solicitud_Pago.Enabled = Habilitado;
            Btn_Fecha_Factura_Solicitud_Pago.Enabled = Habilitado;
            Cmb_Partida.Enabled = Habilitado;
            Btn_Agregar_Documento.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Btn_Agregar_Documento.Enabled = Habilitado;
            Txt_IVA.Enabled = Habilitado;
            Txt_ISR.Enabled = Habilitado;
            Txt_CURP.Enabled = Habilitado;
            Txt_Nom_Proveedor.Enabled = Habilitado;
            Txt_Reten_IVA.Enabled = Habilitado;
            Txt_RFC.Enabled = Habilitado;
            Cmb_Operacion.Enabled = Habilitado;
            Asy_Cargar_Archivo.Enabled = Habilitado;
            Txt_Total_Comprobar.Enabled = Habilitado;
            Txt_ISH.Enabled = Habilitado;
            Txt_IEPS.Enabled= Habilitado;
            Txt_Subtotal_factura.Enabled = Habilitado;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    #endregion

    #region (Metodos Consulta)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Datos_Grid
    /// DESCRIPCION : Carga la informacion de la consulta en el grid
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Datos_Grid()
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Consulta = new DataTable(); //Variable que obtendra los datos de la consulta 
        DataTable Dt_Detalle = new DataTable();
        DataRow Dr_Registro;
        String Estatus = "";
        try
        {
            Estatus = "PORCOMPROBAR";
            Consulta.P_Estatus = Estatus;
            Dt_Consulta = Consulta.Consultar_Estatus();

            Dt_Detalle.Columns.Add("NO_SOLICITUD_PAGO");
            Dt_Detalle.Columns.Add("NO_RESERVA");
            Dt_Detalle.Columns.Add("NOMBRE");
            Dt_Detalle.Columns.Add("FECHA_SOLICITUD");
            Dt_Detalle.Columns.Add("MONTO");
            
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Dr_Registro = Dt_Detalle.NewRow();
                Dr_Registro["NO_SOLICITUD_PAGO"] = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString();
                Dr_Registro["NO_RESERVA"] = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString();
                
                if (!String.IsNullOrEmpty(Registro["EMPLEADO_ID"].ToString()))
                {
                    Dr_Registro["Nombre"] = Registro["NOMBRE"].ToString();
                }
                else
                {
                    Dr_Registro["Nombre"] = Registro["PROVEEDOR"].ToString();
                }
                Dr_Registro["FECHA_SOLICITUD"] = Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud].ToString();
                Dr_Registro["MONTO"] = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                Dt_Detalle.Rows.Add(Dr_Registro);
            }

            //Grid_Gastos_Comprobar.Columns[5].Visible = true;
            Session["Consulta_Gastos_Comprobar"] = Dt_Detalle;
            //Grid_Gastos_Comprobar.DataSource = (DataTable)Session["Consulta_Gastos_Comprobar"];
            //Grid_Gastos_Comprobar.DataBind();
            //Grid_Gastos_Comprobar.Columns[5].Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Combos_Generales
    /// DESCRIPCION : Carga la informacion de los combos
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 23/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Combos_Generales()
    {
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
        DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");

        //Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        //DataTable Dt_Bancos = Bancos_Negocio.Consulta_Bancos();
        //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Bancos, "NOMBRE", "BANCO_ID");
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Chk_Autorizado_OnCheckedChanged
    // DESCRIPCIÓN: manipulacion de los checks
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 24/Junio/2013 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    protected void Chk_Autorizado_OnCheckedChanged(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0;
        Boolean Autorizado = false;
        String Proveedor_ID = ((CheckBox)sender).CssClass;
        Txt_Total_Comprobacion.Text = "0";
        foreach (GridViewRow Renglon_Grid in Grid_Gastos_Comprobacion.Rows)
        {
            GridView Grid_Deudores = (GridView)Renglon_Grid.FindControl("Grid_Deudores");
            foreach (GridViewRow Renglon in Grid_Deudores.Rows)
            {
                Indice++;
                Grid_Deudores.SelectedIndex = Indice;
                Autorizado = ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked;
                if (Autorizado)
                {
                    GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                    foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                    {
                        Indice_Solicitudes++;
                        Grid_Solicitudes_Pago.SelectedIndex = Indice;
                        if (((CheckBox)Fila.FindControl("Chk_Cheque")).ToolTip == Proveedor_ID)
                        {
                            ((CheckBox)Fila.FindControl("Chk_Cheque")).Checked = true;
                             Txt_Total_Comprobacion.Text = (Convert.ToDecimal(Txt_Total_Comprobacion.Text) + Convert.ToDecimal(Fila.Cells[4].Text.Replace(",", "").Replace("$", ""))).ToString();
                        }
                    }
                }
                else
                {
                    GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                    foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                    {
                        Indice_Solicitudes++;
                        Grid_Solicitudes_Pago.SelectedIndex = Indice;
                        if (((CheckBox)Fila.FindControl("Chk_Cheque")).ToolTip == Proveedor_ID)
                        {
                            ((CheckBox)Fila.FindControl("Chk_Cheque")).Checked = false;
                        }
                    }
                }
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Datos_Grid_Detalle
    /// DESCRIPCION : Carga la informacion de la consulta en el grid de detalle
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Datos_Grid_Detalle(DataTable Dt_Consulta)
    {
        try
        {
            Session["Grid_Detalle"] = Dt_Consulta;
            //Grid_Gastos_Comprobar.DataSource = (DataTable)Session["Grid_Detalle"];
            //Grid_Gastos_Comprobar.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Combo_Partida
    /// DESCRIPCION : Carga la informacion de la consulta en el grid
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Combo_Partida()
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Consulta = new DataTable(); //Variable que obtendra los datos de la consulta 
        String No_Reserva = "";
        try
        {
            No_Reserva = Txt_No_Reserva.Text;
            Consulta.P_No_Reserva = No_Reserva;
            //  realizar consulta
            Dt_Consulta = Consulta.Consultar_Reserva_Patida();
            Cmb_Partida.Items.Clear();
            Cmb_Partida.DataSource = Dt_Consulta;
            Cmb_Partida.DataValueField = "Codigo";
            Cmb_Partida.DataTextField = "Nombre_Partida";
            Cmb_Partida.DataBind();
            Cmb_Partida.Items.Insert(0, "< SELECCIONE PARTIDA >");
            Cmb_Partida.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Comprobar_Click
    /// DESCRIPCION : Da de Alta el gasto por comprobar
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Comprobar_Click(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0;
        Boolean Autorizado;
        Boolean Solicitud_por_Pagar;
        DataTable Dt_Solicitudes_Datos = new DataTable();
        try
        {
            foreach (GridViewRow Renglon_Grid in Grid_Gastos_Comprobacion.Rows)
            {
                GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Deudores");
                foreach (GridViewRow Renglon_Grid_Proveedores in Grid_Proveedores.Rows)
                {
                    Indice++;
                    Grid_Proveedores.SelectedIndex = Indice;
                    Autorizado = ((CheckBox)Renglon_Grid_Proveedores.FindControl("Chk_Autorizado_Pago")).Checked;
                    if (Autorizado)
                    {
                        GridView Grid_Datos_Solicitud = (GridView)Renglon_Grid_Proveedores.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow Fila_Solicitudes in Grid_Datos_Solicitud.Rows)
                        {
                            Indice_Solicitudes++;
                            Grid_Datos_Solicitud.SelectedIndex = Indice_Solicitudes;
                            Solicitud_por_Pagar = ((CheckBox)Fila_Solicitudes.FindControl("Chk_Cheque")).Checked;
                            if (Solicitud_por_Pagar)
                            {
                                if (Dt_Solicitudes_Datos.Rows.Count <= 0 && Dt_Solicitudes_Datos.Columns.Count <= 0)
                                {
                                    Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                                    Dt_Solicitudes_Datos.Columns.Add("Proveedor", typeof(System.String));
                                    Dt_Solicitudes_Datos.Columns.Add("No_Deuda", typeof(System.String));
                                    Dt_Solicitudes_Datos.Columns.Add("Monto", typeof(System.Decimal));
                                }
                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["No_Solicitud"] = ((CheckBox)Fila_Solicitudes.FindControl("Chk_Cheque")).CssClass;
                                row["Proveedor"] = Renglon_Grid_Proveedores.Cells[1].Text;
                                row["No_Deuda"] = Obtener_No_Deuda(((CheckBox)Fila_Solicitudes.FindControl("Chk_Cheque")).CssClass);
                                row["Monto"] = Convert.ToDecimal(Fila_Solicitudes.Cells[4].Text.Replace(",", "").Replace("$", ""));
                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Solicitudes_Datos.AcceptChanges();
                            }
                        }
                       Realizar_Comprobacion_Solicitudes(Dt_Solicitudes_Datos);
                       // break;
                    }
                }
            }
            Inicializa_Controles();
            Llenar_Grid_Solicitudes_Pendientes_De_Comprobar();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Realizar_Comprobacion_Solicitudes
    /// DESCRIPCION : Da de Alta la comprobacion de las solicitudes
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Realizar_Comprobacion_Solicitudes(DataTable Dt_Solicitudes_Datos)
    {
        try
        {
            String Resultado = "";
            String Cuentas_Por_Pagar = "";
            String Tipo_Caja_Chica = "";
            String Bancos_Otros = "";
            String Deudor_Caja_Chica = "";
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Consulta = new DataTable();
            DataTable Dt_Reserva = new DataTable();
            String Cheque = "";
            String Fondo_Revolvente = "";
            DataTable Dt_Banco_Cuenta_Contable = new DataTable();
            DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
            DataTable Dt_Parametros = new DataTable();
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
            Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
            Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            String Tipo_Solicitud_Pago = "";
            DataTable Dt_Solicitud_Datos = new DataTable();
            //consultar el tipo de solicitud de pago
            Dt_Parametros = Rs_Parametros.Consulta_Datos_Parametros_2();
            if (Dt_Parametros.Rows.Count > 0)
            {
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString()))
                {
                    Fondo_Revolvente = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Solicitud_Fondo_Revolvente].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Caja_Chica].ToString()))
                {
                    Tipo_Caja_Chica = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Tipo_Caja_Chica].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString()))
                {
                    Cuentas_Por_Pagar = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Por_Pagar_Deudor].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString()))
                {
                    Bancos_Otros = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Cta_Banco_Otros_Deudor].ToString();
                }
                if (!String.IsNullOrEmpty(Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Deudor_Caja_Chica].ToString()))
                {
                    Deudor_Caja_Chica = Dt_Parametros.Rows[0][Cat_Con_Parametros.Campo_Deudor_Caja_Chica].ToString();
                }
            }
            Rs_Solicitud_Pago.P_No_Solicitud_Pago = Dt_Solicitudes_Datos.Rows[0]["No_Solicitud"].ToString();
            Dt_Solicitud_Datos=Rs_Solicitud_Pago.Consulta_Datos_Solicitud_Pago();
            if (Dt_Solicitud_Datos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString() == Fondo_Revolvente)
            {
                if (!String.IsNullOrEmpty(Bancos_Otros) && !String.IsNullOrEmpty(Cuentas_Por_Pagar))
                {
                    Alta_Comprobacion_Fondo_Revolvente(Bancos_Otros, Cuentas_Por_Pagar,Dt_Solicitudes_Datos);
                }
                else
                {
                    if (String.IsNullOrEmpty(Bancos_Otros) && String.IsNullOrEmpty(Cuentas_Por_Pagar))
                    {
                        Lbl_Mensaje_Error.Text = "Error:";
                        Lbl_Mensaje_Error.Text = "No se han cargado las cuentas Deudor de los parametros, Favor de validarlo con Contabilidad";
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Visible = true;
                    }
                    else
                    {
                        if (String.IsNullOrEmpty(Cuentas_Por_Pagar))
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No se han cargado las cuentas por pagar del deudor, Favor de validarlo con Contabilidad";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Text = "Error:";
                            Lbl_Mensaje_Error.Text = "No se han cargado las cuentas otros pagos, Favor de validarlo con Contabilidad";
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Visible = true;
                        }
                    }
                }
            }
            else
            {
                if (Dt_Solicitud_Datos.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString() == Tipo_Caja_Chica)
                {
                    if (!String.IsNullOrEmpty(Deudor_Caja_Chica))
                    {
                        Alta_Comprobacion_Caja_Chica(Dt_Solicitudes_Datos, Deudor_Caja_Chica);
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "Error:";
                        Lbl_Mensaje_Error.Text = "No se han cargado la cuenta deudor de caja chica, Favor de validarlo con Contabilidad";
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Visible = true;
                    }
                }
                else
                {
                    Alta_Comprobacion_Gastos(Dt_Solicitudes_Datos);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Comprobacion_Fondo_Revolvente
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Comprobacion_Fondo_Revolvente(String Banco_Otros, String Cuentas_Por_Pagar, DataTable Dt_Solicitudes)
    {

        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Gastos_Por_Comprobar = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Cls_Cat_Empleados_Negocios Rs_Cuentas_Deudor = new Cls_Cat_Empleados_Negocios();        
        //String Cheque = "";
        String Resultado = "";
        DataRow row;
        Decimal Importe_Total = 0;
        String Tipo_Deudor = "";
        String Deudor_ID = "";
        int Partida = 0;
        String No_Deudor = "";
        String Cuenta_Contable_Banco="";
        String Importe_Por_Banco = "";
        String[] Banco_ID = new String[Dt_Solicitudes.Rows.Count];
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
        Cls_Ope_Con_Deudores_Negocio Rs_Deudores = new Cls_Ope_Con_Deudores_Negocio();
        Cls_Cat_Com_Proveedores_Negocio Rs_Proveedor_Deudor = new Cls_Cat_Com_Proveedores_Negocio();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        DataTable Dt_Datos_Deudor = new DataTable();
        DataTable Dt_Deudor_Banco = new DataTable();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        try
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));

            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                No_Deudor= No_Deudor+"'"+Fila[OPE_CON_DEUDORES.Campo_No_Deuda].ToString()+"',";
            }
            ////Consultar Bancos 
            Rs_Deudores.P_No_Deuda = "(" + No_Deudor.Substring(0, No_Deudor.Length - 1) + ")";
            Rs_Deudores.P_Cmmd = Cmmd;
            Dt_Deudor_Banco = Rs_Deudores.Consulta_Banco_Monto_Deudas();
            foreach (DataRow Renglon in Dt_Deudor_Banco.Rows)
            {
                Tipo_Deudor = Renglon[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString();
                Deudor_ID = Renglon[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["cuenta_banco_pago"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                foreach (DataRow Renglon_Definido in Dt_Banco_Cuenta_Contable.Rows)
                {
                    Cuenta_Contable_Banco= Renglon_Definido[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                    Importe_Por_Banco = Renglon["monto"].ToString();
                }
                Partida = Partida + 1;
                //Agrega el abono del registro de la póliza
                row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Importe_Por_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "FONDO REVOLVENTE"; 
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                row["BENEFICIARIO_ID"] = "";
                row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                Importe_Total = Importe_Total + Convert.ToDecimal(Importe_Por_Banco);
            }
            if (Tipo_Deudor == "EMPLEADO")
            {
                Rs_Cuentas_Deudor.P_Empleado_ID = Deudor_ID;
                Dt_Datos_Deudor=Rs_Cuentas_Deudor.Consulta_Datos_Empleado();
                if (Dt_Datos_Deudor.Rows.Count > 0)
                {
                    Partida = Partida + 1;
                    //Agrega el cargo del registro de la póliza
                    row = Dt_Partidas_Polizas.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Datos_Deudor.Rows[0][Cat_Empleados.Campo_Cuenta_Contable_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Total;
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "FONDO REVOLVENTE"; 
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                    row["BENEFICIARIO_ID"] = "";
                    row["TIPO_BENEFICIARIO"] = "";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                }
            }
            else
            {
                Rs_Proveedor_Deudor.P_Proveedor_ID = Deudor_ID;
                Dt_Datos_Deudor = Rs_Proveedor_Deudor.Consulta_Datos_Proveedores(); 
                if (Dt_Datos_Deudor.Rows.Count > 0)
                {
                    Partida = Partida + 1;
                    //Agrega el cargo del registro de la póliza
                    row = Dt_Partidas_Polizas.NewRow();
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Datos_Deudor.Rows[0][Cat_Com_Proveedores.Campo_Cuenta_Deudor_ID].ToString();
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Total;
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "FONDO REVOLVENTE"; 
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                    row["BENEFICIARIO_ID"] = "";
                    row["TIPO_BENEFICIARIO"] = "";
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                }
            }
            Partida = Partida + 1;
            //Agrega el abono del registro de la póliza
            row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Banco_Otros;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "FONDO REVOLVENTE"; 
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Total;
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            Partida = Partida + 1;
            //Agrega el cargo del registro de la póliza
            row = Dt_Partidas_Polizas.NewRow();
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuentas_Por_Pagar;
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "FONDO REVOLVENTE"; 
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Importe_Total;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
            row["BENEFICIARIO_ID"] = "";
            row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();


            Rs_Gastos_Por_Comprobar.P_Dt_Partidas_Poliza = Dt_Partidas_Polizas;
            Rs_Gastos_Por_Comprobar.P_Dt_Deudas = Dt_Solicitudes;
            Rs_Gastos_Por_Comprobar.P_Cmmd = Cmmd;
            Rs_Gastos_Por_Comprobar.P_Estatus = "COMPROBADO";
            Rs_Gastos_Por_Comprobar.P_Monto_Aprobado = Convert.ToString(Importe_Total*2);
            Rs_Gastos_Por_Comprobar.P_Partidas = Partida.ToString();
            Resultado=Rs_Gastos_Por_Comprobar.Alta_Comprobacion_Gastos();
            #region "CODIGO COMENTADO"
            //Rs_Solicitudes.P_Importe = Convert.ToString(Convert.ToDecimal(Txt_Monto.Text.Trim().Replace("$", "").Replace(",", "")) * 2);
            //Rs_Solicitudes.P_No_Partidas = "4";
            //Rs_Solicitudes.P_Estatus = Cmb_Estatus.SelectedItem.ToString();
            //Rs_Solicitudes.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Txt_Fecha_No_Pago.Text)).ToString();
            //Rs_Solicitudes.P_Forma_Pago = Cmb_Tipo_Pago.SelectedItem.ToString();
            //Rs_Solicitudes.P_Cuenta_Banco_Pago = Cmb_Cuenta_Banco.SelectedValue;
            //Rs_Solicitudes.P_Referencia = Txt_Referencia_Pago.Text.ToString();
            //Rs_Solicitudes.P_Cmmd = Cmmd;
            //if (Txt_Beneficiario_Pago.Text.Trim().Substring(1, 1) == "-")
            //{
            //    Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim().Substring(2);
            //}
            //else
            //{
            //    Rs_Solicitudes.P_Beneficiario_Pago = Txt_Beneficiario_Pago.Text.Trim();
            //}
            //////Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            //////Rs_Consultar_Folio.P_Cmmd = Cmmd;
            //////Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();
            ////////  obtendra el numero de folio de los cheques
            //////if (Dt_Consulta.Rows.Count > 0)
            //////{
            //////    foreach (DataRow Registro in Dt_Consulta.Rows)
            //////    {
            //////        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
            //////        {
            //////            Txt_No_Cheque.Text = Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString();
            //////        }
            //////    }
            //////}
            //////Rs_Solicitudes.P_No_Cheque = Txt_No_Cheque.Text.Trim();
            //////Rs_Solicitudes.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
            //////Cheque = Rs_Solicitudes.Alta_Cheque();
            //////Resultado = Cheque.Substring(6, 2);
            #endregion
            
            if (Resultado == "SI")
            {
                //Cheque = Cheque.Substring(0, 5);
                //Imprimir(Cheque, Convert.ToDecimal(Txt_Monto.Text.Trim().Replace("$", "").Replace(",", "")), Cmmd);
                //// cambiamos el numero de folio de los cheques
                //Modificar_Numero_Folio(Cmmd, null);
                Trans.Commit();
                //Limpia_Controles();
               // Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
            else
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        catch (SqlException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            if (Ex.Number == 547)
                throw new Exception("*Error: " + Ex.Message);
            else
                throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            Cn.Close();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Comprobacion_Caja_Chica
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Comprobacion_Caja_Chica(DataTable Dt_Solicitudes, String Deudor_Caja_Chica)
    {
        Cls_Ope_Con_Deudores_Negocio Rs_Deudores = new Cls_Ope_Con_Deudores_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Gastos_Por_Comprobar = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Datos_Polizas = new DataTable();
        String No_Deudor = "";
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Deudor_Banco = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
        int Partida = 0;
        String Resultado = "";
        Decimal Importe_Total = 0;
        DataRow Row;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        try
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                No_Deudor = No_Deudor+"'" + Fila["No_Solicitud"].ToString() + "',";
            }
            ////Consultar Bancos 
            Rs_Deudores.P_No_Deuda = "(" + No_Deudor.Substring(0, No_Deudor.Length - 1) + ")";
            Rs_Deudores.P_Cmmd = Cmmd;
            Dt_Deudor_Banco = Rs_Deudores.Consulta_Cuentas_Contables_De_Reserva();
            foreach (DataRow Renglon in Dt_Deudor_Banco.Rows)
            {
                Partida = Partida + 1;
                //Agrega el abono del registro de la póliza
                Row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                Row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Renglon["CUENTA_CONTABLE_RESERVA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Debe] = Renglon["MONTO_PARTIDA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                Row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Caja Chica Reserva No."+Renglon["NO_RESERVA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                Row["BENEFICIARIO_ID"] = "";
                Row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(Row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                Importe_Total = Importe_Total + Convert.ToDecimal(Renglon["MONTO_PARTIDA"].ToString());
            }
            Partida = Partida + 1;
            //Agrega el abono del registro de la póliza
            Row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            Row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
            Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Deudor_Caja_Chica;
            Row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Caja Chica Reserva";
            Row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            Row[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Total;
            Row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
            Row["BENEFICIARIO_ID"] = "";
            Row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(Row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            Rs_Gastos_Por_Comprobar.P_Dt_Partidas_Poliza = Dt_Partidas_Polizas;
            Rs_Gastos_Por_Comprobar.P_Dt_Deudas = Dt_Solicitudes;
            Rs_Gastos_Por_Comprobar.P_Cmmd = Cmmd;
            Rs_Gastos_Por_Comprobar.P_Estatus = "COMPROBADO";
            Rs_Gastos_Por_Comprobar.P_Monto_Aprobado = Convert.ToString(Importe_Total);
            Rs_Gastos_Por_Comprobar.P_Partidas = Partida.ToString();
            Resultado = Rs_Gastos_Por_Comprobar.Alta_Comprobacion_Gastos();
            if (Resultado == "SI")
            {
                Rs_Solicitudes.P_Dt_Datos_Agrupados = Dt_Solicitudes;
                Rs_Solicitudes.P_Estatus = "COMPROBADO";
                Rs_Solicitudes.P_Cmmd = Cmmd;
                Rs_Solicitudes.Comprobar_Vale();
                Trans.Commit();
            }
            else
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Comprobacion_Gastos
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Comprobacion_Gastos(DataTable Dt_Solicitudes)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Ope_Con_Deudores_Negocio Rs_Deudores = new Cls_Ope_Con_Deudores_Negocio();
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Gastos_Por_Comprobar = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Deudor_Banco = new DataTable();
        String No_Deudor = "";
        String Cuenta_Contable_Deudor="";
        String Resultado = "";
        DataRow Row;
        Decimal Importe_Total=0;
        int Partida = 0;
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos            
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Trans.Connection;
        Cmmd.Transaction = Trans;
        try
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("BENEFICIARIO_ID", typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                No_Deudor = No_Deudor + "'" + Fila["No_Solicitud"].ToString() + "',";
            }
            ////Consultar Bancos 
            Rs_Deudores.P_No_Deuda = "(" + No_Deudor.Substring(0, No_Deudor.Length - 1) + ")";
            Rs_Deudores.P_Cmmd = Cmmd;
            Dt_Deudor_Banco = Rs_Deudores.Consulta_Cuentas_Contables_De_Reserva();
            foreach (DataRow Renglon in Dt_Deudor_Banco.Rows)
            {
                Partida = Partida + 1;
                //Agrega el abono del registro de la póliza
                Row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                Row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Renglon["CUENTA_CONTABLE_RESERVA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Debe] = Renglon["MONTO_PARTIDA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                Row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Gasto por Comprobar Reserva No." + Renglon["NO_RESERVA"].ToString();
                Row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
                Row["BENEFICIARIO_ID"] = "";
                Row["TIPO_BENEFICIARIO"] = "";
                Dt_Partidas_Polizas.Rows.Add(Row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                Importe_Total = Importe_Total + Convert.ToDecimal(Renglon["MONTO_PARTIDA"].ToString());
                Cuenta_Contable_Deudor=Renglon["Cuenta_Deudor"].ToString();
            }
            Partida = Partida + 1;
            //Agrega el abono del registro de la póliza
            Row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
            Row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
            Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Deudor;
            Row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "Gasto por comprobar";
            Row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            Row[Ope_Con_Polizas_Detalles.Campo_Haber] = Importe_Total;
            Row[Ope_Con_Polizas_Detalles.Campo_Referencia] = "";
            Row["BENEFICIARIO_ID"] = "";
            Row["TIPO_BENEFICIARIO"] = "";
            Dt_Partidas_Polizas.Rows.Add(Row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();

            Rs_Gastos_Por_Comprobar.P_Dt_Partidas_Poliza = Dt_Partidas_Polizas;
            Rs_Gastos_Por_Comprobar.P_Dt_Deudas = Dt_Solicitudes;
            Rs_Gastos_Por_Comprobar.P_Cmmd = Cmmd;
            Rs_Gastos_Por_Comprobar.P_Estatus = "COMPROBADO";
            Rs_Gastos_Por_Comprobar.P_Monto_Aprobado = Convert.ToString(Importe_Total);
            Rs_Gastos_Por_Comprobar.P_Partidas = Partida.ToString();
            Resultado = Rs_Gastos_Por_Comprobar.Alta_Comprobacion_Gastos();
            if (Resultado == "SI")
            {
                Trans.Commit();
            }
            else
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para realizar el pago ";
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_CHEQUE " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_No_Deuda
    /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
    ///               proporcionados por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public String Obtener_No_Deuda(String No_Solicitud)
    {
        String No_Deuda = "";
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Gastos = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        try
        {
            Rs_Gastos.P_No_Solicitud = No_Solicitud;
            No_Deuda=Rs_Gastos.Consultar_No_Deuda();
           
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message;
            throw new Exception(Ex.Message, Ex);
        }
        return No_Deuda;
    }
    #endregion
    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Double Total = 0.0;
        Double Sub_Total = 0.0;
        String Espacios_Blanco = "";
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        try
        {
            Lbl_Mensaje_Error.Text ="";
            Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario Introducir: <br>";
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;

            if (Grid_Documentos.Rows.Count == 0)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Realice algun movimiento.<br>";
                Datos_Validos = false;
            }
            else
            {
                Total = Convert.ToDouble(Txt_Total.Text.Replace("$", "").Replace(",", ""));
                Sub_Total = Convert.ToDouble(Txt_Total_Comprobar.Text.Replace("$","").Replace(",",""));

                if (Sub_Total != Total)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*El total debe ser igual al soliciado .<br>";
                        Datos_Validos = false;
                    //if (Cmb_Banco.SelectedIndex == 0)
                    //{
                    //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione algun banco.<br>";
                    //    Datos_Validos = false;
                    //}
                    //if (Cmb_Cuenta_Banco.SelectedIndex == 0)
                    //{
                    //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Seleccione la cuenta del banco.<br>";
                    //    Datos_Validos = false;
                    //}
                    //if (Txt_No_Cheque.Text == "")
                    //{
                    //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el número del cheque.<br>";
                    //    Datos_Validos = false;
                    //}
                    //if (Txt_Comentario_Pago.Text == "")
                    //{
                    //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el comentario de la forma de pago.<br>";
                    //    Datos_Validos = false;
                    //}
                }
            }
        }
        catch
        {
        }
        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Grid
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Grid()
    {
        String Espacios_Blanco;
     
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text = "";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "Es necesario Introducir: <br>";
        //para partida
        if (Cmb_Partida.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese la partida de la reserva.<br>";
            Datos_Validos = false;
        }
        if (Txt_No_Factura_Solicitud_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese el número del documento.<br>";
            Datos_Validos = false;
        }
        if (Txt_Fecha_Factura_Solicitud_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Ingrese la fecha de la factura.<br>";
            Datos_Validos = false;
        }
        if (Txt_Monto_Solicitud_Pago.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "*Selecione el monto.<br>";
            Datos_Validos = false;
        }
         return Datos_Validos;
    }
     ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Comparar_Monto
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 23/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Comparar_Monto()
    {
        Boolean Comparacion = false;
        Double Total = 0.0;
        Double Sub_Total = 0.0;
        try
        {
            Total = Convert.ToDouble(Txt_Total.Text);
            Sub_Total = Convert.ToDouble(Txt_Total_Comprobar.Text.Replace("$","").Replace(",",""));

            if (Sub_Total > Total)
                Comparacion = true;


            return Comparacion;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #region (Operaciones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Construir_Tabla
    ///DESCRIPCIÓN: Realizara la tabla que se agregara al grid de detalle
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  21/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    public void Construir_Tabla()
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Detalle = new DataTable();
        DataTable Dt_Consulta= new DataTable();
        DataRow Dr_Registro;
        String Formato_Importe = "";
        Double Importe = 0.0;
        try
        {
            if (Session["Grid_Detalle"] != null)
            {
                Dt_Detalle = (DataTable)Session["Grid_Detalle"];
            }

            if (Dt_Detalle.Rows.Count == 0)
            {
                Dt_Detalle.Columns.Add("PARTIDA_ID");
                Dt_Detalle.Columns.Add("PARTIDA");
                Dt_Detalle.Columns.Add("DOCUMENTO");
                Dt_Detalle.Columns.Add("FECHA_SOLICITUD");
                Dt_Detalle.Columns.Add("IMPORTE");

                Dr_Registro = Dt_Detalle.NewRow();

                Dr_Registro["PARTIDA_ID"] = Cmb_Partida.SelectedValue;
                
                Consulta.P_Partida_ID = Cmb_Partida.SelectedValue;
                Dt_Consulta = Consulta.Consultar_Nombre_Patida();
                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Dr_Registro["PARTIDA"] = (Registro[Cat_Sap_Partidas_Especificas.Campo_Descripcion].ToString());
                }
                
                Dr_Registro["DOCUMENTO"] = Txt_Monto_Solicitud_Pago.Text;
                Dr_Registro["FECHA_SOLICITUD"] = String.Format("{0: MM/dd/yyyy}",
                                                Convert.ToDateTime(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()));
                Importe = Convert.ToDouble(Txt_Monto_Solicitud_Pago.Text);
                Formato_Importe = String.Format("{0:n}", Importe);
                Dr_Registro["IMPORTE"] = Formato_Importe;
                Dt_Detalle.Rows.Add(Dr_Registro);
            }
            else
            {
                
                Dr_Registro = Dt_Detalle.NewRow();
                Dr_Registro["PARTIDA_ID"] = Cmb_Partida.SelectedValue;
                
                Consulta.P_Partida_ID = Cmb_Partida.SelectedValue;
                Dt_Consulta = Consulta.Consultar_Nombre_Patida();
                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    Dr_Registro["PARTIDA"] = (Registro[Cat_Sap_Partidas_Especificas.Campo_Descripcion].ToString());
                }
                Dr_Registro["DOCUMENTO"] = Txt_Monto_Solicitud_Pago.Text;
                Dr_Registro["FECHA_SOLICITUD"] = String.Format("{0: MM/dd/yyyy}",
                                                Convert.ToDateTime(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()));
                Importe = Convert.ToDouble(Txt_Monto_Solicitud_Pago.Text);
                Formato_Importe = String.Format("{0:n}", Importe);
                Dr_Registro["IMPORTE"] = Formato_Importe;
                Dt_Detalle.Rows.Add(Dr_Registro);
            }
            Grid_Documentos.Columns[0].Visible = true;
            Grid_Documentos.DataSource = Dt_Detalle;
            Session["Grid_Detalle"] = Dt_Detalle;
            Grid_Documentos.DataBind();
            Grid_Documentos.SelectedIndex = -1;
            Grid_Documentos.Columns[0].Visible = false;

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Alta_Cheque
    ///DESCRIPCIÓN: Realizara la tabla que se agregara al grid de detalle
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Alta_Cheque(SqlCommand P_Cmmd)
    {
        Double Total = 0.0;
        Double Sub_Total = 0.0;
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        String Cuenta_Contable_ID_Banco = "";
        String SubTotal = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;

        try
        {
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            Total = Convert.ToDouble(Txt_Total.Text);
            Sub_Total = Convert.ToDouble(Txt_Subtotal.Text.Replace("$",""));

            Sub_Total = Sub_Total - Total;//    saco la diferendia del sub total con el total
            Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
            foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
            {
                Cuenta_Contable_ID_Banco = Renglon[Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
            }
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
            Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));

            DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

            //Agrega el abono del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 1;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_ID_Banco;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Sub_Total;

            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();
            row = Dt_Partidas_Polizas.NewRow();

            //Agrega el cargo del registro de la póliza
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = 2;
            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_ID_Banco;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Sub_Total;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;

            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();


            Rs_Ope_Con_Cheques.P_No_Solicitud_Pago = Txt_No_Solicitud.Text;
            Rs_Ope_Con_Cheques.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Ope_Con_Cheques.P_Fecha_Pago = Txt_Fecha_No_Pago.Text;
            Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Txt_Beneficiario.Text;
            Rs_Ope_Con_Cheques.P_Tipo_Pago = "CHEQUE";
            Rs_Ope_Con_Cheques.P_No_Cheque = Txt_No_Cheque.Text;
            Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
            Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PORCOMPROBAR";
            Rs_Ope_Con_Cheques.P_Comentario = Txt_Comentario_Pago.Text;
            Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            Rs_Ope_Con_Cheques.P_Monto = "" + Sub_Total;
            Rs_Ope_Con_Cheques.P_Mes_Ano = String.Format("{0:MMyy}", DateTime.Now).ToString();
            Rs_Ope_Con_Cheques.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva.Text.Trim());
            Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
            Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
            Rs_Ope_Con_Cheques.Alta_Cheque();
        }
        catch (SqlException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Numero_Folio
    ///DESCRIPCIÓN: Resibe la informacion para poder modificar el folio Actual
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Numero_Folio()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Modificar_Folio_Actual = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Int32 Folio = 0;
        try
        {
            Folio = Convert.ToInt32(Txt_No_Cheque.Text);
            Folio++;
            Rs_Modificar_Folio_Actual.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
            Rs_Modificar_Folio_Actual.P_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modificar_Folio_Actual.P_Folio_Actual = "" + Folio;
            Rs_Modificar_Folio_Actual.Modificar_Folio_Actual();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Alta
    ///DESCRIPCIÓN: Resibe la informacion para poder dar de alta un registro
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private String Alta(SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Solcitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio();    //Objeto de acceso a los metodos.
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Modificar_Comprobacion= new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        int Actualiza_Presupuesto = 0;
        int Registra_Movimiento;
        Double Monto_Aprobado = 0;
        String Tipo;
        String Monto;
        String Reserva;
        String Respuesta = "SI";
        String Accion = String.Empty;
        String No_Solicitud = String.Empty;
        String Comentario = String.Empty;
        DataTable Dt_solicitud;
        DataTable Dt_Partidas = new DataTable(); 
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        try
        {
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            No_Solicitud = Txt_No_Solicitud.Text;
            Rs_Solcitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
            Rs_Solcitud_Pago.P_Cmmd = Cmmd;
            Dt_solicitud = Rs_Solcitud_Pago.Consulta_Datos_Solicitud_Pago();
            if (Dt_solicitud.Rows.Count > 0)
            {
                Tipo = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                Poliza_Devengado(No_Solicitud, Cmmd);// se genera la poliza con estatus devengado
                Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                if (Actualiza_Presupuesto > 0)
                {
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Reserva), "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("DEVENGADO", "COMPROMETIDO", Dt_Partidas, Cmmd);
                    Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Reserva), "DEVENGADO", "COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("EJERCIDO", "DEVENGADO", Dt_Partidas, Cmmd);
                    Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "EJERCIDO", "DEVENGADO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                    Autoriza_Contabilidad_Solicitud_pago(No_Solicitud, Cmmd);// se genera la alta de la solicitud de pago
                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                    Actualiza_Presupuesto = 0;
                    Actualiza_Presupuesto=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PAGADO", "EJERCIDO", Dt_Partidas, Cmmd); //Actualiza el impote de la partida presupuestal
                    if (Actualiza_Presupuesto > 0)
                    {
                        Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal((Reserva), "PAGADO", "EJERCIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    }
                    else
                    {
                        Respuesta = "NO";
                    }
                }
                else
                {
                    Respuesta = "NO";
                }
            }
            if (Respuesta == "SI")
            {
                //  para el estatus
                Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                Rs_Solicitud_Pago.P_Estatus = "PAGADO";
                Rs_Solicitud_Pago.P_Comentario = Comentario;
                Rs_Solicitud_Pago.P_Empleado_ID_Contabilidad = Cls_Sessiones.Empleado_ID.ToString();
                Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();

                //  para los datos de comprobacion de documentos
                Rs_Modificar_Comprobacion.P_No_Solicitud = No_Solicitud;
                Monto_Aprobado = Convert.ToDouble(Txt_Total_Comprobar.Text.Replace("$", "").Replace(",", ""));
                Rs_Modificar_Comprobacion.P_Monto_Aprobado = "" + Monto_Aprobado;
                Rs_Modificar_Comprobacion.P_Usuario_Aprobo = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Comprobacion.P_Cmmd = Cmmd;
                Rs_Modificar_Comprobacion.Cambiar_Datos_Comprobacion_Solicitud_Pago();
            }

            return Respuesta;
        }
        catch (SqlException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : sergio manuel gallardo
    /// FECHA_CREO  : 01- marzo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        try
        {
            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
            Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            if (Txt_Nom_Proveedor.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El nombre del Proveedor. <br>";
                Datos_Validos = false;
            }
            if (Txt_No_Factura_Solicitud_Pago.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El No. del documento. <br>";
                Datos_Validos = false;
            }
            if (Txt_Fecha_Factura_Solicitud_Pago.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Fecha del Documento. <br>";
                Datos_Validos = false;
            }
            if (Txt_Subtotal_factura.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El monot del Documento. <br>";
                Datos_Validos = false;
            }
            if (Cmb_Partida.SelectedIndex <= 0)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Falta seleccionar la partida <br>";
                Datos_Validos = false;
            }
            if (Txt_IVA.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El porcentaje del iva. <br>";
                Datos_Validos = false;
            }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
        }
    }
    protected void Btn_Agregar_Documento_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
        Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
        DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documento = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
        //string Codigo_Programatico = "";
        String Espacios = "";
        String extension;
        int error = 0;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        //Valida que todos los datos requeridos los haya proporcionado el usuario
        if (Validar_Importe_Partidas())
        {
            Txt_Ruta.Text = "";
            Txt_Nombre_Archivo.Text = "";
            extension = Guardar_Documentos_Anexos();
            extension = extension.Substring(extension.LastIndexOf(".") + 1);
            if (Validar())
            {
                if (Session["Dt_Documentos"] == null)
                {
                    //Agrega los campos que va a contener el DataTable
                    Dt_Documento.Columns.Add("No_Documento", typeof(System.String));
                    Dt_Documento.Columns.Add("Fecha_Documento", typeof(System.String));
                    Dt_Documento.Columns.Add("Monto", typeof(System.String));
                    Dt_Documento.Columns.Add("Partida", typeof(System.String));
                    Dt_Documento.Columns.Add("Partida_Id", typeof(System.String));
                    Dt_Documento.Columns.Add("Fte_Financiamiento_Id", typeof(System.String));
                    Dt_Documento.Columns.Add("Proyecto_Programa_Id", typeof(System.String));
                    Dt_Documento.Columns.Add("Iva", typeof(System.String));
                    Dt_Documento.Columns.Add("IEPS", typeof(System.Double));
                    Dt_Documento.Columns.Add("Archivo", typeof(System.String));
                    Dt_Documento.Columns.Add("Ruta", typeof(System.String));
                    Dt_Documento.Columns.Add("Nombre_Proveedor_Fact", typeof(System.String));
                    Dt_Documento.Columns.Add("RFC", typeof(System.String));
                    Dt_Documento.Columns.Add("CURP", typeof(System.String));
                    Dt_Documento.Columns.Add("Operacion", typeof(System.String));
                    Dt_Documento.Columns.Add("Retencion_ISR", typeof(System.Double));
                    Dt_Documento.Columns.Add("Retencion_IVA", typeof(System.Double));
                    Dt_Documento.Columns.Add("Retencion_Celula", typeof(System.Double));
                    Dt_Documento.Columns.Add("ISH", typeof(System.Double));
                }
                else
                {
                    Dt_Documento = (DataTable)Session["Dt_Documentos"];
                    Session.Remove("Dt_Documentos");
                }
                DataRow row = Dt_Documento.NewRow(); //Crea un nuevo registro a la tabla
                //Asigna los valores al nuevo registro creado a la tabla
                row["No_Documento"] = Txt_No_Factura_Solicitud_Pago.Text.Trim();
                row["Fecha_Documento"] = String.Format("{0: MM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()));
                row["Monto"] = Txt_Monto_partida.Value;
                row["Partida_Id"] = Cmb_Partida.SelectedValue.Substring(0, 10);
                row["Fte_Financiamiento_Id"] = Cmb_Partida.SelectedValue.Substring(10, 5);
                row["Proyecto_Programa_Id"] = Cmb_Partida.SelectedValue.Substring(15, 10);
                row["iva"] = Txt_IVA.Text.Trim();
                row["Nombre_Proveedor_Fact"] = Txt_Nom_Proveedor.Text.Trim();
                row["RFC"] = Txt_RFC.Text.Trim();
                row["CURP"] = Txt_CURP.Text.Trim();
                row["Operacion"] = Cmb_Operacion.SelectedItem.Text.Trim();
                if (Cmb_Operacion.SelectedItem.Text.Trim() == "OTROS")
                {
                    row["Retencion_ISR"] = 0;
                    row["Retencion_IVA"] = 0;
                    row["Retencion_Celula"] = 0;
                    row["IEPS"] = Txt_IEPS.Text.Trim();
                    row["ISH"] = Txt_ISH.Text.Trim();
                }
                else
                {
                    if (Cmb_Operacion.SelectedItem.Text.Trim() == "HON. ASIMILABLE")
                    {
                        row["Retencion_ISR"] = Txt_ISR.Text.Trim();
                        row["Retencion_IVA"] = 0;
                        row["Retencion_Celula"] = 0;
                        row["IEPS"] = 0;
                        row["ISH"] = 0;
                    }
                    else
                    {
                        row["Retencion_ISR"] = Txt_ISR.Text.Trim();
                        row["Retencion_IVA"] = Txt_Reten_IVA.Text.Trim();
                        row["Retencion_Celula"] = Txt_Reten_Cedular.Text.Trim();
                        row["IEPS"] = 0;
                        row["ISH"] = 0;
                    }
                }
                Consulta.P_Partida_ID = Cmb_Partida.SelectedValue.Substring(0, 10);
                Dt_Consulta = Consulta.Consultar_Nombre_Patida();

                foreach (DataRow Registro in Dt_Consulta.Rows)
                {
                    row["Partida"] = (Registro[Cat_Sap_Partidas_Especificas.Campo_Descripcion].ToString());
                }
                if (!String.IsNullOrEmpty(Txt_Nombre_Archivo.Text.Trim()) && !String.IsNullOrEmpty(Txt_Ruta.Text.Trim()))
                {
                    if (extension == "txt" || extension == "doc" || extension == "pdf" || extension == "docx" || extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG" || extension == "png" || extension == "PNG" || extension == "gif" || extension == "GIF")
                    {
                        row["Archivo"] = Txt_Nombre_Archivo.Text;
                        row["Ruta"] = Txt_Ruta.Text;
                        ClearSession_AsyncFileUpload(Asy_Cargar_Archivo.ClientID);
                        Txt_Nombre_Archivo.Text = "";
                        Txt_Ruta.Text = "";
                    }
                    else
                    {
                        System.IO.File.Delete(Txt_Ruta.Text);
                        ClearSession_AsyncFileUpload(Asy_Cargar_Archivo.ClientID);
                        Txt_Ruta.Text = "";
                        Txt_Nombre_Archivo.Text = "";
                    }
                }
                Dt_Documento.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Documento.AcceptChanges();
                Txt_Total_Comprobar.Text = "0";
                Txt_Subtotal.Text = "0";
                Session["Dt_Documentos"] = Dt_Documento;//Agrega los valores del registro a la sesión
                Grid_Documentos.Columns[2].Visible = true;
                Grid_Documentos.Columns[6].Visible = true;
                Grid_Documentos.Columns[7].Visible = true;
                Grid_Documentos.Columns[8].Visible = true;
                Grid_Documentos.Columns[9].Visible = true;
                Grid_Documentos.Columns[10].Visible = true;
                Grid_Documentos.Columns[11].Visible = true;
                Grid_Documentos.Columns[12].Visible = true;
                Grid_Documentos.Columns[13].Visible = true;
                Grid_Documentos.Columns[14].Visible = true;
                Grid_Documentos.Columns[19].Visible = true;
                Grid_Documentos.Columns[20].Visible = true;
                Grid_Documentos.DataSource = Dt_Documento; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Documentos.DataBind();
                Grid_Documentos.Columns[2].Visible = false;
                Grid_Documentos.Columns[6].Visible = false;
                Grid_Documentos.Columns[7].Visible = false;
                Grid_Documentos.Columns[8].Visible = false;
                Grid_Documentos.Columns[9].Visible = false;
                Grid_Documentos.Columns[10].Visible = false;
                Grid_Documentos.Columns[11].Visible = false;
                Grid_Documentos.Columns[12].Visible = false;
                Grid_Documentos.Columns[13].Visible = false;
                Grid_Documentos.Columns[14].Visible = false;
                Grid_Documentos.Columns[19].Visible = false;
                Grid_Documentos.Columns[20].Visible = false;
                Txt_No_Factura_Solicitud_Pago.Text = "";
                Txt_Fecha_Factura_Solicitud_Pago.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
                Txt_Monto_Solicitud_Pago.Text = "0";
                Txt_Monto_partida.Value = "0";
                Cmb_Partida.SelectedIndex = 0;
                Txt_IVA.Text = "0";
                Txt_ISR.Text = "0";
                Txt_Reten_IVA.Text = "0";
                Txt_ISH.Text = "0";
                Txt_IEPS.Text = "0";
                Txt_Subtotal_factura.Text = "0";
                Txt_Reten_Cedular.Text = "0";
                Txt_Reten_IVA.Text = "0";
                Txt_IEPS.Text = "0";
                Txt_ISH.Text = "0";
                Cmb_Operacion.SelectedIndex = 0;
                Txt_Total_Comprobar.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Total_Comprobar.Text));
                Txt_Subtotal.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Subtotal.Text));
            }
        //Indica al usuario que datos son los que falta por proporcionar para poder agregar la partida a la poliza
            else
            {
                //error = 1;
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (String.IsNullOrEmpty(Txt_No_Factura_Solicitud_Pago.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + El Número del Documento <br>";
                }
                if (String.IsNullOrEmpty(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + La Fecha del Documento <br>";
                }
                if (String.IsNullOrEmpty(Txt_Monto_Solicitud_Pago.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios + " + El Monto del Documento <br>";
                }
            }
            if (error == 1)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        else
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
        if (!String.IsNullOrEmpty(Txt_ID_Proveedor.Value))
        {
            Consulta_PRO.P_Proveedor_ID = Txt_ID_Proveedor.Value;
            Dt_Consulta_PRO = Consulta_PRO.Consulta_Datos_Proveedores();
            if (Dt_Consulta_PRO.Rows.Count > 0)
            {
                Txt_Nom_Proveedor.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString();
                Txt_RFC.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString();
            }
        }
        if (!String.IsNullOrEmpty(Txt_ID_Empleado.Value))
        {
            Consulta_EMP.P_Empleado_ID = Txt_ID_Empleado.Value;
            Dt_Consulta_EMP = Consulta_EMP.Consulta_Datos_Empleado();
            if (Dt_Consulta_EMP.Rows.Count > 0)
            {
                Txt_Nom_Proveedor.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Nombre].ToString();
                Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString();
                Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString();
                Txt_RFC.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_RFC].ToString();
                Txt_CURP.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_CURP].ToString();
            }
        }
    }
    private void Poliza_Devengado(String No_Solicitud_Pago, SqlCommand P_Cmmd)//, String Comentario, String Empleado_ID, String Nombre_Empleado
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Banco = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Polizas_Negocio Rs_Polizas = new Cls_Ope_Con_Polizas_Negocio();
        DataTable Dt_reservas = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Banco = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento7
        DataTable Dt_Solicitud_Pagos = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        Double Total = 0.0;
        Double Sub_Total = 0.0;
        int partida = 0;
        DataRow row;

        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        try
        {
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            Rs_Banco.P_No_Solicitud = No_Solicitud_Pago;
            Rs_Banco.P_Cmmd = Cmmd;
            Dt_Datos_Banco = Rs_Banco.Consultar_Cuenta_Banco();
            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
            {
                partida = partida + 1;
                Txt_Monto_Solicitud_Anterior.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                if (!String.IsNullOrEmpty(Registro["proveedor_id"].ToString()))
                {
                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                    {
                        Txt_Cuenta_Contable_ID_Proveedor.Value = Registro["Pro_Cuenta_Deudor"].ToString();
                    }
                }
                else
                {
                    Rs_Reserva.P_No_Reserva = Registro["NO_RESERVA"].ToString();
                    Rs_Reserva.P_Cmmd = Cmmd;
                    Dt_reservas = Rs_Reserva.Consultar_Reservas();
                    if (Dt_reservas.Rows.Count > 0)
                    {
                        Rs_Solicitud.P_No_Deuda = Dt_reservas.Rows[0]["NO_DEUDA"].ToString();
                        Rs_Solicitud.P_Cmmd = Cmmd;
                        Dt_Solicitud_Pagos = Rs_Solicitud.Consulta_Datos_Solicitud_Pago();
                        foreach (DataRow Fila in Dt_Solicitud_Pagos.Rows)
                        {
                            if (!String.IsNullOrEmpty(Fila["CUENTA_DEUDOR"].ToString()))
                            {
                                Rs_Cuentas_Proveedor.P_Cuenta_Contable_ID = Fila["CUENTA_DEUDOR"].ToString();
                                Rs_Cuentas_Proveedor.P_Cmmd = Cmmd;
                                Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                                if (Dt_C_Proveedor.Rows.Count > 0)
                                {
                                    Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                                }
                            }
                        }
                    }
                }
                Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                Txt_No_Reservah.Value = Registro["NO_RESERVA"].ToString();

                Total = Convert.ToDouble(Txt_Total_Comprobar.Text.Replace("$", "").Replace(",", ""));
                Sub_Total = Convert.ToDouble(Txt_Subtotal.Text.Replace("$", "").Replace(",", ""));
                //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                if (Dt_Partidas_Polizas.Rows.Count == 0)
                {
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                }
                row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Registro["Monto_Partida"].ToString());
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
            }
            row = Dt_Partidas_Polizas.NewRow();
            Rs_Cuentas_Proveedor.P_Cuenta_Contable_ID = "";
            Rs_Cuentas_Proveedor.P_Cuenta = "211200001";
            Rs_Cuentas_Proveedor.P_Cmmd = Cmmd;
            Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
            if (Dt_C_Proveedor.Rows.Count > 0)
            {
                Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
            }

            //Agrega el abono del registro de la póliza
            partida = partida + 1;
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
            if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
            {
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
            }
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Sub_Total;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();
            
            
            //Alta poliza Devengado
            Rs_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
            Rs_Polizas.P_Tipo_Poliza_ID = "00002";
            Rs_Polizas.P_Mes_Ano = String.Format("{0:MMyy}", Convert.ToDateTime(DateTime.Now.ToString()));
            Rs_Polizas.P_Fecha_Poliza = Convert.ToDateTime(DateTime.Now.ToString());
            Rs_Polizas.P_Concepto = "Asignacion de Deuda al Acreedor";
            Rs_Polizas.P_Total_Debe = Convert.ToDouble(Convert.ToString(Sub_Total));
            Rs_Polizas.P_Total_Haber = Convert.ToDouble(Convert.ToString(Sub_Total));
            Rs_Polizas.P_No_Partida = Convert.ToInt32(partida);
            Rs_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
            Rs_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
            Rs_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
            Rs_Polizas.P_Cmmd = Cmmd;
            string[] Datos_Poliza = Rs_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    private void Autoriza_Contabilidad_Solicitud_pago(String No_Solicitud_Pago, SqlCommand P_Cmmd)//, String Comentario, String Empleado_ID, String Nombre_Empleado
    {
        String Espacios_Blanco;
        Cls_Cat_Con_Cuentas_Gastos_Negocio Rs_Cuentas_Gastos = new Cls_Cat_Con_Cuentas_Gastos_Negocio();
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Banco = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_reservas = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Datos_Banco = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento7
        DataTable Dt_Solicitud_Pagos = new DataTable();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Banco = new DataTable();
        DataTable Dt_Deudor = new DataTable();
        Double Total = 0.0;
        Double Sub_Total = 0.0;
        String Cuenta_Contable_Banco = "";
        int partida = 0;
        DataRow row;
        String Beneficiario_ID = "";
        String Tipo_Beneficiario = "";
        int error = 0;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;

        try
        {
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
            Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
            foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Monto_Partida"].ToString()))
                {
                    if (!String.IsNullOrEmpty(Registro["empleado_id"].ToString()))
                    {
                        Rs_Cuentas_Gastos.P_Gasto_Id = "00003";
                        Dt_Deudor = Rs_Cuentas_Gastos.Consultar_Gastos();
                        if (Dt_Deudor.Rows.Count > 0)
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_Deudor.Rows[0]["Cuenta_Contable_ID"].ToString();
                            Beneficiario_ID = Registro["empleado_id"].ToString();
                            Tipo_Beneficiario = "EMPLEADO";
                        }
                    }
                    else
                    {
                        Txt_Monto_Solicitud_Anterior.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                        Rs_Cuentas_Proveedor.P_Cuenta_Contable_ID = "";
                        Rs_Cuentas_Proveedor.P_Cuenta = "211200001";
                        Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                        if (Dt_C_Proveedor.Rows.Count > 0)
                        {
                            Txt_Cuenta_Contable_ID_Proveedor.Value = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                            Beneficiario_ID = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                            Tipo_Beneficiario = "PROVEEDOR";
                        }
                    }
                    partida = partida + 1;
                    Txt_Monto_Solicitud.Value = Registro["Monto"].ToString();
                    Txt_Concepto_Solicitud.Value = Registro["Concepto"].ToString();
                    Txt_Cuenta_Contable_reserva.Value = Registro["Cuenta_Contable_Reserva"].ToString();
                    Txt_No_Reservah.Value = Registro["NO_RESERVA"].ToString();
                    Rs_Banco.P_No_Solicitud = No_Solicitud_Pago;
                    Dt_Datos_Banco = Rs_Banco.Consultar_Cuenta_Banco();
                    Rs_Reserva.P_No_Reserva = Registro["NO_RESERVA"].ToString();
                    Dt_reservas = Rs_Reserva.Consultar_Reservas();
                    Rs_Solicitud.P_No_Deuda = Dt_reservas.Rows[0]["NO_DEUDA"].ToString();
                    Dt_Solicitud_Pagos = Rs_Solicitud.Consulta_Solicitud_Pago();
                    if (Dt_Solicitud_Pagos.Rows.Count > 0)
                    {
                        Rs_Bancos_Negocio.P_Banco_ID = Dt_Solicitud_Pagos.Rows[0]["CUENTA_BANCO_PAGO"].ToString();
                        Dt_Banco = Rs_Bancos_Negocio.Consulta_Bancos();
                        if (Dt_Banco.Rows.Count > 0)
                        {
                            Cuenta_Contable_Banco = Dt_Banco.Rows[0]["CUENTA_CONTABLE_ID"].ToString().Trim();
                        }
                    }
                    Total = Convert.ToDouble(Txt_Total_Comprobar.Text.Replace("$", "").Replace(",", ""));
                    Sub_Total = Convert.ToDouble(Txt_Subtotal.Text.Replace("$", "").Replace(",", ""));
                    //if (Sub_Total > Total)
                    //{
                    //    Resta = Sub_Total - Total;
                    //    Checar_Monto = true;
                    //    Debe = Resta;
                    //    Concepto = "Exedio el monto aprobado";
                    //}
                    //else if (Sub_Total == Total)
                    //{
                    //    Checar_Monto = false;
                    //}
                    //else
                    //{
                    //    Resta = Total - Sub_Total;
                    //    Checar_Monto = true;
                    //    Haber = Resta;
                    //    Concepto = "No se gasto el monto aprobado";
                    //}
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    if (Dt_Partidas_Polizas.Rows.Count == 0)
                    {
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID, typeof(System.String));
                        Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario, typeof(System.String));
                    }
                    row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_reserva.Value;
                    row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Registro["Monto_Partida"].ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Beneficiario_ID;
                    row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Tipo_Beneficiario;
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                }

            }

            row = Dt_Partidas_Polizas.NewRow();
            //Agrega el abono del registro de la póliza
            partida = partida + 1;
            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
            if (!String.IsNullOrEmpty(Txt_Cuenta_Contable_ID_Proveedor.Value.ToString()))
            {
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Txt_Cuenta_Contable_ID_Proveedor.Value;
            }
            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Txt_Concepto_Solicitud.Value;
            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total;
            row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Beneficiario_ID;
            row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Tipo_Beneficiario;
            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Partidas_Polizas.AcceptChanges();


            //if (Checar_Monto == true)
            //{
            //    //  para el banco con la diferencia
            //    //row = Dt_Partidas_Polizas.NewRow();
            //    //partida = partida + 1;
            //    //row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
            //    //row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Banco;
            //    //row[Ope_Con_Polizas_Detalles.Campo_Concepto] = Concepto;
            //    //row[Ope_Con_Polizas_Detalles.Campo_Debe] = Debe;
            //    //row[Ope_Con_Polizas_Detalles.Campo_Haber] = Haber;
            //    //Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            //    //Dt_Partidas_Polizas.AcceptChanges();
            //}

            //Agrega los valores a pasar a la capa de negocios para ser dados de alta
            if (Txt_Cuenta_Contable_ID_Proveedor.Value != "")
            {
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Partida = Convert.ToString(partida);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto = Txt_Concepto_Solicitud.Value;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto = Convert.ToDecimal(Txt_Monto_Solicitud.Value);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reservah.Value.ToString());
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Alta_Solicitud_Pago(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                Lbl_Mensaje_Error.Text = "La Cuenta del empleado o proveedor para la deuda";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }

        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }

        catch (Exception ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Alta_Documentos
    ///DESCRIPCIÓN: Resibe la informacion para poder dar de alta un registro
    ///PARAMETROS:  
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  23/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Alta_Documentos(SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Rs_Alta_documentos = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        DataTable Dt_Documentos = new DataTable();
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;

        try
        {
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }

            Rs_Alta_documentos.P_No_Solicitud = Txt_No_Solicitud.Text;
            Dt_Documentos = (DataTable)Session["Dt_Documentos"];
            Rs_Alta_documentos.P_Dt_Documentos_Comprobados = Dt_Documentos;
            Rs_Alta_documentos.P_Tipo_Documento = "COMPROBADO";
            Rs_Alta_documentos.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            Rs_Alta_documentos.P_Cmmd = Cmmd;
            Rs_Alta_documentos.Alta_Documentos_Comprobados();
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }

        catch (Exception ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }
        }
    }
    #endregion
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Importe_Partidas
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 18-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Importe_Partidas()
    {
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
        String Espacios_Blanco;
        DataTable Dt_Partidas;
        DataTable Dt_Consulta;
        Double No_Reserva = 0.0;
        Double Importe = 0.0;
        Double Importe_Tabla = 0.0;
        String Partida_Nombre = "";
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        try
        {
            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
            Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

            No_Reserva = Convert.ToDouble(Txt_No_Reserva.Text);
            Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
            Dt_Partidas = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva();

            Consulta.P_Partida_ID = Cmb_Partida.SelectedValue.Substring(0, 10);
            Dt_Consulta = Consulta.Consultar_Nombre_Patida();

            if (Txt_Monto_partida.Value == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto no se ha ingresado <br>";
                Datos_Validos = false;
            }
            foreach (DataRow Registro in Dt_Consulta.Rows)
            {
                Partida_Nombre = (Registro[Cat_Sap_Partidas_Especificas.Campo_Descripcion].ToString());
            }
            foreach (DataRow Registro in Dt_Partidas.Rows)
            {
                if (Partida_Nombre == Registro["PARTIDA"].ToString())
                {
                    Importe = Convert.ToDouble(Registro["IMPORTE_INICIAL"].ToString());
                    break;
                }
            }
            Dt_Consulta = null;
            if (Session["Dt_Documentos"] != null)
            {
                Dt_Consulta = (DataTable)Session["Dt_Documentos"];
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (Partida_Nombre == Registro["PARTIDA"].ToString())
                        {
                            Importe_Tabla += Convert.ToDouble(Registro["Monto"].ToString());
                        }
                    }
                    //if (Importe_Tabla > Importe)
                    //{
                    //    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Solicitado excede al saldo aprobado. <br>";
                    //    Datos_Validos = false;
                    //}
                }
            }
            else
            {
                if (Txt_Monto_partida.Value == "")
                {
                    Importe_Tabla = 0;

                }
                else
                {
                    Importe_Tabla = Convert.ToDouble(Txt_Monto_partida.Value);
                }
                //if (Importe_Tabla > Importe)
                //{
                //    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Solicitado excede al saldo aprobado. <br>";
                //    Datos_Validos = false;
                //}
            }
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Solicitudes_Pendientes_De_Comprobar
    // DESCRIPCIÓN: Llena el grid principal de las solicitudes de pago que estan autorizadas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 27/Agosto/2012
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Grid_Solicitudes_Pendientes_De_Comprobar()
    {
        DataTable Temporal = null;
        String Tipo;
        String Tipo_Deudor = "";
        //String Forma_Pago;
        DataTable Dt_Modificada = new DataTable();
        Boolean Insertar;
        Double Monto;
        String NO_RESERVA = "";
        DataTable Dt_Datos_Detalles = new DataTable();
        Session["Dt_Datos_Detalles"] = Dt_Datos_Detalles;
        DataTable Dt_Datos = new DataTable();
        Session["Dt_Datos"] = Dt_Datos;
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitudes = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        //Cls_Ope_Con_Cheques_Negocio Solicitudes = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
        Consulta.P_Estatus = "PORCOMPROBAR";
        Session[P_Dt_Solicitud] = Consulta.Consultar_Estatus();
        if (Session[P_Dt_Solicitud] != null && ((DataTable)Session[P_Dt_Solicitud]).Rows.Count > 0)
        {
            Temporal = Session[P_Dt_Solicitud] as DataTable;
            foreach (DataRow Fila in Temporal.Rows)
            {
                if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                {
                    Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("MONTO_TIPO", typeof(System.Double));
                    Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                    Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                }
                DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                Tipo = Fila["TIPO"].ToString().ToUpper();
                NO_RESERVA = Fila["NO_RESERVA"].ToString();
                if (!String.IsNullOrEmpty(Fila["EMPLEADO_ID"].ToString()))
                {
                    Tipo_Deudor = "EMPLEADO";
                }
                else
                {
                    Tipo_Deudor = "PROVEEDOR";
                }
               // Forma_Pago = Cmb_Forma_Pago.SelectedItem.Text.ToString();
                Insertar = true;
                Monto = 0;
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Modificada.Rows)
                {
                    //se verifica si selecciono un tipo de pago para solo mostrar las solicitudes de ese pago
                    if (Tipo.Equals(Fila2["TIPO"].ToString().ToUpper()))
                    {
                        Insertar = false;
                    }
                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Temporal.Rows)
                {
                    if (Tipo.Equals(Renglon["TIPO"].ToString().ToUpper()))
                    {
                        Monto = Monto + Convert.ToDouble(Renglon["MONTO"].ToString());
                    }
                }
                if (Insertar && Monto > 0)
                {
                    row["TIPO"] = Tipo.ToUpper();
                    row["MONTO_TIPO"] = Monto;
                   // row["FORMA_PAGO"] = Forma_Pago;
                    row["IDENTIFICADOR_TIPO"] = NO_RESERVA + Tipo + Monto;
                    row["TIPO_DEUDOR"] = Tipo_Deudor;
                    Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Modificada.AcceptChanges();
                }
            }
            Session["Dt_Datos_Detalles"] = Temporal;
            Session["Dt_Datos"] = Dt_Modificada;
            Grid_Gastos_Comprobacion.DataSource = Dt_Modificada;
            Grid_Gastos_Comprobacion.DataBind();
        }
        else
        {
            Session[P_Dt_Solicitud] = null;
            Grid_Gastos_Comprobacion.DataSource = null;
            Grid_Gastos_Comprobacion.DataBind();
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Pagos_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        DataTable Dt_Datos_Detalles_Proveedor = new DataTable();
        Dt_Datos_Detalles_Proveedor = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        Int32 Contador_Proveedor = 0;
        Session["Contador_Proveedor"] = Contador_Proveedor;
        String DEUDOR_ID = "";
        String TIPO_DEUDOR = "";
        String Tipo = "";
        String Forma_Pago = "";
        Boolean Agregar;
        Double Total;
        Int32 Contador;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (Int32)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    DEUDOR_ID = "";
                    TIPO_DEUDOR = "";
                    if (!String.IsNullOrEmpty(Filas["EMPLEADO_ID"].ToString()))
                    {
                        DEUDOR_ID = "E-" + Filas["EMPLEADO_ID"].ToString();
                        TIPO_DEUDOR = "EMPLEADO";
                    }
                    else
                    {
                        DEUDOR_ID = Filas["PROVEEDOR_ID"].ToString();
                        TIPO_DEUDOR = "PROVEEDOR";
                    }
                    Tipo = Convert.ToString(Dt_Datos.Rows[Contador]["Tipo"].ToString());
                    Forma_Pago = Dt_Datos.Rows[Contador]["IDENTIFICADOR_TIPO"].ToString().Substring(0, 4);
                    //Forma_Pago = Convert.ToString(Dt_Datos.Rows[Contador]["Forma_Pago"].ToString());
                    if (!String.IsNullOrEmpty(DEUDOR_ID))
                    {
                        if (TIPO_DEUDOR == "EMPLEADO")
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "TIPO='" + Tipo + "' and EMPLEADO_ID='" + DEUDOR_ID.Substring(DEUDOR_ID.IndexOf('-')+1) + "'";
                        }
                        else
                        {
                            Dt_Datos_Detalles.DefaultView.RowFilter = "TIPO='" + Tipo + "' and PROVEEDOR_ID='" + DEUDOR_ID + "'";
                        }
                       
                    }
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Deudores");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                                Dt_Modificada.Columns.Add("NOMBRE", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IMPORTE", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            Agregar = true;
                            Total = 0;
                            //insertar los que no se repiten
                            foreach (DataRow Fila2 in Dt_Modificada.Rows)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                            }
                            //se calcula el monto por tipo
                            foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    if (TIPO_DEUDOR == "EMPLEADO")
                                    {
                                        if (DEUDOR_ID.Equals("E-"+Renglon["EMPLEADO_ID"].ToString()))
                                        {
                                            Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                        }
                                    }
                                    else
                                    {
                                        if (DEUDOR_ID.Equals(Renglon["PROVEEDOR_ID"].ToString()))
                                        {
                                            Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                        }
                                    }
                                }
                            }
                            if (Agregar && Total > 0)
                            {
                                if (!String.IsNullOrEmpty(DEUDOR_ID))
                                {
                                    row["DEUDOR_ID"] = DEUDOR_ID;
                                    row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                    row["TIPO_MOVIMIENTO"] = Fila["TIPO"].ToString();
                                    row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                                }
                                row["IMPORTE"] = Total;
                                row["TIPO"] = Tipo;
                                row["ESTATUS"] = Fila["Estatus"].ToString();
                                if (TIPO_DEUDOR == "PROVEEDOR")
                                {
                                    row["IDENTIFICADOR"] = DEUDOR_ID + "P" + Tipo + Forma_Pago;
                                }
                                else
                                {
                                    row["IDENTIFICADOR"] = DEUDOR_ID + "E" + Tipo + Forma_Pago;
                                }
                                Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Modificada.AcceptChanges();
                            }
                        }
                    }
                }
                Session["Dt_Datos_Detalles_Proveedor"] = Dt_Datos_Detalles;
                Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                Gv_Detalles.Columns[7].Visible = true;
                Gv_Detalles.DataSource = Dt_Modificada;
                Gv_Detalles.DataBind();
                Gv_Detalles.Columns[7].Visible = false;
                Session["Contador"] = Contador + 1;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Chk_Cheque_Solicitud_OnCheckedChanged
    // DESCRIPCIÓN: manipulacion de los checks
    // operacion solicitada si las validaciones son positivas
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 24/Junio/2013 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    protected void Chk_Cheque_Solicitud_OnCheckedChanged(object sender, EventArgs e)
    {
        Int32 Indice = 0;
        Int32 Indice_Solicitudes = 0;
        Boolean Autorizado = false;
        Boolean Otro_Seleccionado = false;
        String Proveedor_ID = ((CheckBox)sender).ToolTip;
        Txt_Total_Comprobacion.Text = "0";
        foreach (GridViewRow Renglon_Grid in Grid_Gastos_Comprobacion.Rows)
        {
            GridView Grid_Proveedores = (GridView)Renglon_Grid.FindControl("Grid_Deudores");
            foreach (GridViewRow Renglon in Grid_Proveedores.Rows)
            {
                Indice++;
                Grid_Proveedores.SelectedIndex = Indice;
                Autorizado = ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked;
                if (Autorizado)
                {
                    GridView Grid_Solicitudes_Pago = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                    foreach (GridViewRow Fila in Grid_Solicitudes_Pago.Rows)
                    {
                        Indice_Solicitudes++;
                        Grid_Solicitudes_Pago.SelectedIndex = Indice;
                        if (((CheckBox)Fila.FindControl("Chk_Cheque")).Checked == true)
                        {
                            Otro_Seleccionado = true;
                            Txt_Total_Comprobacion.Text = (Convert.ToDecimal(Txt_Total_Comprobacion.Text) + Convert.ToDecimal(Fila.Cells[4].Text.Replace(",", "").Replace("$", ""))).ToString();
                        }
                    }
                    if (!Otro_Seleccionado)
                    {
                        if (((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).CssClass == Proveedor_ID)
                        {
                            ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked = false;
                            Txt_Total_Comprobacion.Text = "0";
                        }
                    }
                }
                else
                {
                    if (((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).CssClass == Proveedor_ID)
                    {
                        ((CheckBox)Renglon.FindControl("Chk_Autorizado_Pago")).Checked = true;
                    }
                }
            }
        }
    }
    
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        String Deudor_ID = "";
        String Tipo_Beneficiario = "";
        String Tipo = "";
        String NO_DEUDA = "";
        String Deudor="";
        Int32 Contador_Proveedor;
        String Forma_Pago = "";
        DataTable Dt_Forma_Pago = new DataTable();
        Dt_Forma_Pago = ((DataTable)(Session["Dt_Forma_Pago"]));
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Deudor");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio2");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Solicitud");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador_Proveedor = (Int32)(Session["Contador_Proveedor"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Proveedor"]));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles_Proveedor"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    Deudor_ID = "";
                    NO_DEUDA = Filas["NO_SOLICITUD_PAGO"].ToString();
                    Deudor_ID = Dt_Datos.Rows[Contador_Proveedor]["DEUDOR_ID"].ToString();
                    Tipo = Convert.ToString(Dt_Datos.Rows[Contador_Proveedor]["Tipo"].ToString());
                    Tipo_Beneficiario = Dt_Datos.Rows[Contador_Proveedor]["TIPO_DEUDOR"].ToString();
                    // Forma_Pago = Convert.ToStriDt_Datos.Rows[Contador_Proveedor]["TIPO_DEUDOR"].ToString();ng(Dt_Datos.Rows[Contador_Proveedor]["Forma_Pago"].ToString());
                    Deudor = Dt_Datos.Rows[Contador_Proveedor]["DEUDOR_ID"].ToString();
                    if (Tipo_Beneficiario == "EMPLEADO")
                    {
                       Dt_Datos_Detalles.DefaultView.RowFilter = "TIPO='" + Tipo + "' and EMPLEADO_ID='" + Deudor_ID.Substring(Deudor_ID.IndexOf('-')+1) + "' and NO_SOLICITUD_PAGO='" + NO_DEUDA + "'";
                    }
                    else
                    {
                        Dt_Datos_Detalles.DefaultView.RowFilter = "TIPO='" + Tipo + "' and PROVEEDOR_ID='" + Deudor_ID + "' and NO_SOLICITUD_PAGO='" + NO_DEUDA + "'";
                    }
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Datos_Solicitud");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("NO_DEUDA", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_MOVIMIENTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("IMPORTE", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                               // Dt_Modificada.Columns.Add("FORMA_PAGO", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            row["NO_DEUDA"] = Fila["NO_SOLICITUD_PAGO"].ToString();
                            row["TIPO_MOVIMIENTO"] = Fila["TIPO"].ToString();
                            row["IMPORTE"] = Fila["MONTO"].ToString();
                            row["DEUDOR_ID"] = Deudor;
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString();
                           // row["FORMA_PAGO"] = Forma_Pago;
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                        Session["Dt_Forma_Pago"] = Dt_Modificada;
                        Gv_Detalles.DataSource = Dt_Modificada;
                        Gv_Detalles.DataBind();
                    }
                }
                Session["Contador_Proveedor"] = Contador_Proveedor + 1;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
#endregion

    #region Eventos
    #region (Botones)
    protected void Btn_Eliminar_Partida(object sender, EventArgs e)
    {
        ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
        DataTable Dt_Partidas = (DataTable)Session["Dt_Documentos"];
        DataRow[] Filas = Dt_Partidas.Select("No_Documento" +
                "='" + Btn_Eliminar_Partida.CommandArgument + "'");

        if (!(Filas == null))
        {
            if (Filas.Length >= 0)
            {
                Dt_Partidas.Rows.Remove(Filas[0]);
                Session["Dt_Documentos"] = Dt_Partidas;
                Txt_Total_Comprobar.Text = "0";
                Txt_Subtotal.Text = "0";
                Grid_Documentos.Columns[2].Visible = true;
                Grid_Documentos.Columns[6].Visible = true;
                Grid_Documentos.Columns[7].Visible = true;
                Grid_Documentos.Columns[8].Visible = true;
                Grid_Documentos.Columns[9].Visible = true;
                Grid_Documentos.Columns[10].Visible = true;
                Grid_Documentos.Columns[11].Visible = true;
                Grid_Documentos.Columns[12].Visible = true;
                Grid_Documentos.Columns[13].Visible = true;
                Grid_Documentos.Columns[14].Visible = true;
                Grid_Documentos.Columns[20].Visible = true;
                Grid_Documentos.Columns[19].Visible = true;
                Grid_Documentos.DataSource = Session["Dt_Documentos"]; ; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Documentos.DataBind();
                Grid_Documentos.Columns[2].Visible = false;
                Grid_Documentos.Columns[6].Visible = false;
                Grid_Documentos.Columns[7].Visible = false;
                Grid_Documentos.Columns[8].Visible = false;
                Grid_Documentos.Columns[9].Visible = false;
                Grid_Documentos.Columns[10].Visible = false;
                Grid_Documentos.Columns[11].Visible = false;
                Grid_Documentos.Columns[12].Visible = false;
                Grid_Documentos.Columns[13].Visible = false;
                Grid_Documentos.Columns[14].Visible = false;
                Grid_Documentos.Columns[19].Visible = false;
                Grid_Documentos.Columns[20].Visible = false;
                Txt_Total_Comprobar.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Total_Comprobar.Text));
                Txt_Subtotal.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Subtotal.Text));
            }
            if (Convert.ToDouble(Txt_Total_Comprobar.Text.Trim().Replace("$", "").Replace(",", "")) < Convert.ToDouble(Txt_Total.Text.Trim().Replace("$", "").Replace(",", "")))
            {
                Div_Cheque.Style.Value = "display:none";
                Txt_Fecha_No_Pago.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            }
        }

    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: realizara una modificaion
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  20/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        String Respuesta = "SI";
        try
        {
            // crear transaccion para crear el convenio 
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Cn;
            Cmmd.Transaction = Trans;

            if (Validar_Datos())
            {
                Respuesta=Alta(Cmmd);
                if (Respuesta == "SI")
                {
                    Alta_Documentos(Cmmd);
                    Trans.Commit();
                    Inicializa_Controles();
                }
                else
                {
                    Trans.Rollback();
                    Lbl_Mensaje_Error.Text = "VALIDACION:/n";
                    Lbl_Mensaje_Error.Text = "NO HAY SUFICIENTE PRESUPUESTO PARA REALIZAR EL ALTA DEL GASTO POR COMPROBAR";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (SqlException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error:[" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            Cn.Close();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  20/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        if (Btn_Salir.ToolTip == "Cancelar")
        {
            Div_Presentacion.Style.Value = "overflow:auto;height:200px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
            Div_Detalles.Style.Value = "display:none";
            Div_Cheque.Style.Value = "display:none";
            Habilitar_Controles("Inicial");
            Btn_Modificar.Visible = false;
            Limpiar_Controles();
        }
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }
    #endregion

    #region(ComboBOx)
    
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
            Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta_Banco, Dt_Consulta, "NO_CUENTA", "BANCO_ID");
            Txt_No_Cheque.Text = "";
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Cuenta_Banco_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  10/Marzo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Cuenta_Banco_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Int32 Folio_Actual = 0;
        Int32 Folio_Final = 0;
        Boolean Estado_Folio = false;
        try
        {
            if (Cmb_Banco.SelectedIndex > 0)
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;

                Txt_No_Cheque.Text = "";
                Rs_Consultar_Folio.P_Banco_ID = Cmb_Cuenta_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Folio.Consultar_Folio_Actual();

                //  obtendra el numero de folio de los cheques
                if (Dt_Consulta.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Consulta.Rows)
                    {
                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()))
                            Folio_Actual = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Actual].ToString()));

                        else
                            Estado_Folio = true;

                        if (!String.IsNullOrEmpty(Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()))
                            Folio_Final = Convert.ToInt32((Registro[Cat_Nom_Bancos.Campo_Folio_Final].ToString()));

                        else
                            Estado_Folio = true;
                    }
                }
                //  si contiene informacion
                if (Estado_Folio == false)
                {
                    if (Folio_Actual <= Folio_Final)
                        Txt_No_Cheque.Text = "" + Folio_Actual;

                    //  si el folio se pasa del final manda un mensaje
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Se terminaron los folios del Talonario de cheques actual por favor ingrese nuevos folios";
                    }
                }
                //  no tiene asignado los numeros de folio de los chuques
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Por favor defina el rango de los folio del Talonario de cheques para poder utilizarlos en esta ventana";
                }
            }
            else
            {
                Txt_No_Cheque.Text = "";
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
#endregion

    #region Grid
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCION: Grid_Gastos_Comprobar_SelectedIndexChanged
    ///DESCRIPCION:   Consulta los datos del registro seleccionado
    ///CREO:          Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:    20/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Gastos_Comprobar_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Datos_Reserva; //Variable a obtener los datos de la consulta
        Double No_Reserva = 0.0;
        Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
        Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
        DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
        try
        {
            Grid_Documentos.DataSource = new DataTable();
            Grid_Documentos.DataBind();
            if (Session["Consulta_Gastos_Comprobar"] != null)
            {
                Session.Remove("Consulta_Gastos_Comprobar");
            }
            if (Session["Grid_Detalle"] != null)
            {
                Session.Remove("Grid_Detalle");
            }
            Grid_Documentos.DataSource = new DataTable();
            Grid_Documentos.DataBind();
            Btn_Modificar.Visible = true; // se muestrael boton de modificar
            Habilitar_Controles("Modificar");
            //Grid_Gastos_Comprobar.Columns[5].Visible = true;
            //GridViewRow selectedRow = Grid_Gastos_Comprobar.Rows[Grid_Gastos_Comprobar.SelectedIndex];
            //Txt_No_Solicitud.Text = HttpUtility.HtmlDecode(selectedRow.Cells[1].Text).ToString();
            //Txt_No_Reserva.Text = HttpUtility.HtmlDecode(selectedRow.Cells[2].Text).ToString();
            //Txt_Beneficiario.Text = HttpUtility.HtmlDecode(selectedRow.Cells[3].Text).ToString();
            //Txt_Fecha_Solicitud.Text = HttpUtility.HtmlDecode(selectedRow.Cells[4].Text).ToString();
            //Txt_Total.Text = HttpUtility.HtmlDecode(selectedRow.Cells[5].Text).ToString();
            //Grid_Gastos_Comprobar.Columns[5].Visible = false;
            Div_Detalles.Style.Value="display:block";
            Div_Presentacion.Style.Value = "display:none";
            Cargar_Combo_Partida();

            // para cargar el gird de partidas con su importe correspondiente
            No_Reserva = Convert.ToDouble(Txt_No_Reserva.Text);
            Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
            Dt_Datos_Reserva = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva(); //Consulta todos los datos correspondientes a la reserva que el usuario selecciono
            if (Dt_Datos_Reserva.Rows.Count > 0)
            {
                Grid_Partidas.DataSource = Dt_Datos_Reserva;
                Grid_Partidas.DataBind();
                if (!String.IsNullOrEmpty(Dt_Datos_Reserva.Rows[0]["Proveedor_ID"].ToString()))
                    {
                        Consulta_PRO.P_Proveedor_ID = Dt_Datos_Reserva.Rows[0]["Proveedor_ID"].ToString();
                        Dt_Consulta_PRO = Consulta_PRO.Consulta_Datos_Proveedores();
                        if (Dt_Consulta_PRO.Rows.Count > 0)
                        {
                            Txt_Nom_Proveedor.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString();
                            Txt_RFC.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString();
                        }
                    }
                if (!String.IsNullOrEmpty(Dt_Datos_Reserva.Rows[0]["Empleado_ID"].ToString()))
                    {
                        Consulta_EMP.P_Empleado_ID = Dt_Datos_Reserva.Rows[0]["Empleado_ID"].ToString();
                        Dt_Consulta_EMP = Consulta_EMP.Consulta_Datos_Empleado();
                        if (Dt_Consulta_EMP.Rows.Count > 0)
                        {
                            Txt_Nom_Proveedor.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Nombre].ToString();
                            Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + "  " + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString();
                            Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + "  " + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString();
                            Txt_RFC.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_RFC].ToString();
                            Txt_CURP.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_CURP].ToString();
                        }
                    }
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
    ///               para identicar la fila seleccionada de tabla.
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            HyperLink Hyp_Lnk_Ruta;
            if (Txt_Total_Comprobar.Text == "")
            {
                Txt_Total_Comprobar.Text = "0";
            }
            if (Txt_Subtotal.Text == "")
            {
                Txt_Subtotal.Text = "0";
            }
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[6].Text.Trim();
                ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).ToolTip = "Quitar el Documento " + e.Row.Cells[6].Text;
                Txt_Total_Comprobar.Text = Convert.ToString(Convert.ToDouble(Txt_Total_Comprobar.Text) + Convert.ToDouble(e.Row.Cells[5].Text));
                if (e.Row.Cells[4].Text == "OTROS")
                {
                    Txt_Subtotal.Text = Convert.ToString(Convert.ToDouble(Txt_Subtotal.Text) + Convert.ToDouble(e.Row.Cells[5].Text) - Convert.ToDouble(e.Row.Cells[3].Text));
                }
                else
                {
                    Txt_Subtotal.Text = Convert.ToString(Convert.ToDouble(Txt_Subtotal.Text) + Convert.ToDouble(e.Row.Cells[5].Text) + Convert.ToDouble(e.Row.Cells[15].Text) + Convert.ToDouble(e.Row.Cells[16].Text) + Convert.ToDouble(e.Row.Cells[17].Text));
                }
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[8].Text.Trim()) && e.Row.Cells[8].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[8].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion

    #region "Subir Archivos"
    /// *************************************************************************************
    /// NOMBRE:              Asy_Cargar_Archivo_Complete
    /// DESCRIPCIÓN:         Carga el archivo.
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs e)
    {
        System.Threading.Thread.Sleep(1000);
        try
        {
            AsyncFileUpload archivo;
            archivo = (AsyncFileUpload)sender;
            String Filename;
            Filename = archivo.FileName;
            if (!String.IsNullOrEmpty(Filename))
            {
                String[] arr1;
                arr1 = Filename.Split('\\');
                int len = arr1.Length;
                String img1 = arr1[len - 1];
                String filext = img1.Substring(img1.LastIndexOf(".") + 1);
                if (filext == "txt" || filext == "doc" || filext == "zip" || filext == "pdf" || filext == "rar" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx")
                {
                    if (archivo.FileContent.Length > 2621440) { return; }
                }
                else
                {
                    //Return
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              ClearSession_AsyncFileUpload
    /// DESCRIPCIÓN:         limpia el control de asuncfileupload.
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected void ClearSession_AsyncFileUpload(String ClientID)
    {
        HttpContext currentContext;
        if (HttpContext.Current != null && HttpContext.Current.Session != null)
        {
            currentContext = HttpContext.Current;
        }
        else
        {
            currentContext = null;
        }
        if (currentContext != null)
        {
            foreach (String Key in currentContext.Session.Keys)
            {
                if (Key.Contains(ClientID))
                {
                    currentContext.Session.Remove(Key);
                    break;
                }
            }
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Guardar_Documentos_Anexos
    /// DESCRIPCIÓN:         Guarda el archivo en una carpeta temporal y devuelve la extension
    /// PARÁMETROS:
    /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
    /// FECHA CREO:          28-febrero-2012
    /// USUARIO MODIFICO:    
    /// FECHA MODIFICO:      
    /// CAUSA MODIFICACIÓN:  
    /// *************************************************************************************
    protected String Guardar_Documentos_Anexos()
    {
        DataTable Dt_Documentos_Anexos = new DataTable();
        AsyncFileUpload Asy_FileUpload;
        String appPath = "";
        String Nombre_Directorio = "";
        try
        {
            //Se obtiene la direccion en donde se va a guardar el archivo
            appPath = Server.MapPath("~");
            //Crear el Directorio de los archivos
            if (!Directory.Exists(appPath))
            {
                System.IO.Directory.CreateDirectory(appPath);
            }
            //Se establece el nombre del directorio
            Nombre_Directorio = "Archivo_Solicitud_Pagos\\Temporal_Comprobacion";
            if (Directory.Exists(appPath))
            {
                //Se asigna el control AsyncFileUpload1 a la variable Async_FileUpload
                Asy_FileUpload = Asy_Cargar_Archivo;
                if (!String.IsNullOrEmpty(Asy_FileUpload.FileName))
                {
                    //Valida que no exista el directorio, si no existe lo crea
                    DirectoryInfo Directorio;
                    if (Directory.Exists(appPath + "\\" + Nombre_Directorio) == false)
                    {
                        Directorio = Directory.CreateDirectory(appPath + "\\" + Nombre_Directorio);
                    }
                    //Se asigna el directorio en donde se va a guardar los documentos
                    String saveDir = Nombre_Directorio + "\\";
                    String savePath = appPath + "\\" + saveDir + "" + Asy_FileUpload.FileName;
                    Server.HtmlEncode(Asy_FileUpload.FileName);
                    if (Asy_FileUpload.HasFile)
                    {
                        //Se guarda el archivo
                        Asy_FileUpload.SaveAs(HttpUtility.HtmlEncode(savePath));
                        Txt_Ruta.Text = HttpUtility.HtmlEncode(savePath);
                        Txt_Nombre_Archivo.Text = HttpUtility.HtmlEncode(Asy_FileUpload.FileName);
                    }
                }
            }
            return (appPath + "\\" + Nombre_Directorio + "\\" + Txt_Nombre_Archivo.Text);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }

    }
    #endregion
}
