﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos.Datos;
public partial class paginas_Contabilidad_Frm_Ope_Con_Recepcion_Documentos : System.Web.UI.Page
{
    #region PAGE LOAD
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    Recepcion_Documentos_Inicio();
                    Acciones();
                    Txt_Lector_Codigo_Barras.Focus();
                }
            }
            catch(Exception Ex) 
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error["+Ex.Message+"]");
            }
        }
    #endregion

    #region METODOS
     
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Recepcion_Documentos_Inicio
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Recepcion_Documentos_Inicio() 
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Limpiar_Controles();
                Llenar_Grid_Solicitudes_Pago();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }
        
        ///*********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
        ///DESCRIPCIÓN          : Metodo para limpiar los controles de la página
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Diciembre/2011 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Limpiar_Controles()
        {
            try
            {
                Hf_No_Solicitud_Autorizar.Value = "";
                Hf_Rechazo.Value = "";
                Txt_Comentario.Text = "";
                Txt_Documentos.Text = "";
                Txt_Comentario.Text = "";
                Grid_Solicitud_Pagos.DataSource = null;
                Grid_Solicitud_Pagos.DataBind();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al limpiar los controles de la página Error[" + Ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
        /// DESCRIPCION : Llena el grid Solicitudes de pago
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes_Pago()
        {
            String No_Autoriza_Ramo33="";
            String No_Autoriza_Ramo33_2="";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Resultado = new DataTable();
                Rs_Autoriza_Solicitud.P_Estatus = "PREAUTORIZADO";
                if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                {
                    Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RAMO 33";
                }
                else
                {
                    Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                }
                Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes();
                
                Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;

                if (Dt_Resultado.Rows.Count > 0)
                {
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[4].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[4].Visible = false;
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }

        // ****************************************************************************************
        //'NOMBRE DE LA FUNCION:Accion
        //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
        //'PARAMETROS  : 
        //'CREO        : Sergio Manuel Gallardo
        //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
        //'MODIFICO          :
        //'FECHA_MODIFICO    :
        //'CAUSA_MODIFICACION:
        //'****************************************************************************************
        protected void Acciones()
        {
            String Accion = String.Empty;
            String No_Solicitud = String.Empty;
            String Comentario = String.Empty;
            String Documento = String.Empty;
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            DataTable Dt_Consulta_Estatus = new DataTable();
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
            if (Request.QueryString["Accion"] != null)
            {
                Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
                if (Request.QueryString["id"] != null)
                {
                    No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                }
                if (Request.QueryString["x"] != null)
                {
                    Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
                }
                //Response.Clear()
                switch (Accion)
                {
                    case "Autorizar_Solicitud":
                        Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                        DataTable Dt_Usuario = new DataTable();
                        Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                        if (Dt_Usuario.Rows.Count > 0)
                        {
                            No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                            No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                        }
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                            if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                            {
                                Rs_Solicitud_Pago.P_Tipo_Recurso = "RAMO 33";
                            }
                            else
                            {
                                Rs_Solicitud_Pago.P_Tipo_Recurso = "RECURSO ASIGNADO";
                            }
                            Dt_Consulta_Estatus= Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                            if (Dt_Consulta_Estatus.Rows.Count > 0)
                            {
                                foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                                {
                                    if (Registro["Estatus"].ToString() == "PREAUTORIZADO")
                                    {
                                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                                        Rs_Solicitud_Pago.P_Estatus = "PRE-DOCUMENTADO";
                                        Rs_Solicitud_Pago.P_Comentario = Comentario;
                                        Rs_Solicitud_Pago.P_Usuario_Autorizo_Documentos = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.Modificar_Estatus_Recepcion_Pago();
                                    }
                                }
                            }
                        break;
                    case "Codigo_Barras":
                        if (No_Solicitud.Length < 10)
                        {
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        }
                        else
                        {
                            Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        }
                        Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario_2 = new Cls_Cat_Con_Parametros_Negocio();
                        DataTable Dt_Usuario_2 = new DataTable();
                        Dt_Usuario_2 = Rs_Validar_Usuario_2.Consulta_Datos_Parametros();
                        if (Dt_Usuario_2.Rows.Count > 0)
                        {
                            No_Autoriza_Ramo33 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                            No_Autoriza_Ramo33_2 = Dt_Usuario_2.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                        }
                        if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RAMO 33";
                        }
                        else
                        {
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RECURSO ASIGNADO";
                        }
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                            {
                                if (Registro["Estatus"].ToString() == "PREAUTORIZADO")
                                {
                                    Rs_Solicitud_Pago.P_Estatus = "PRE-DOCUMENTADO";
                                    Rs_Solicitud_Pago.P_Comentario = Comentario;
                                    Rs_Solicitud_Pago.P_Usuario_Autorizo_Documentos = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.Modificar_Estatus_Recepcion_Pago();
                                }
                            }
                        }
                        break;
                }
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private String Cancela_Solicitud_Pago(String No_Solicitud_Pago, String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
        {

            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Partidas = new DataTable();
            String Monto;
            int Actualiza_Presupuesto=0;
            int Registra_Movimiento=0;
            String Reserva;
            Monto = "0";
            Reserva = "0";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Solicitud_Negocio.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Solicitud_Negocio.P_Cmmd = Cmmd;
                Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)
                    {
                        Monto =Dt_Detalles.Rows[0]["MONTO"].ToString();
                        Reserva = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString();
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                //Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Monto);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                //Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto ="CANCELACION-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario_Recepcion = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Recibio = Cls_Sessiones.Nombre_Empleado;
                //Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Reserva);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_recepcion_Documentos = String.Format("{0:dd/MM/yyyy}",DateTime.Now);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Rechaza_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
                Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud_Pago, Cmmd);
                Actualiza_Presupuesto=Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("PRE_COMPROMETIDO", "COMPROMETIDO", Dt_Partidas,Cmmd ); //Actualiza el impote de la partida presupuestal
                if (Actualiza_Presupuesto > 0)
                {
                    Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Convert.ToString(Reserva), "PRE_COMPROMETIDO", "COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "",Cmmd); //Agrega el historial del movimiento de la partida presupuestal
                    return "SI";
                }
                else
                {
                    return "NO";
                }
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }
            }
        }
    #endregion

    #region EVENTOS
        protected void Btn_Cancelar_Click(object sender, EventArgs e)
        {
            try
            {
                Recepcion_Documentos_Inicio();
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al inicio de la página de recepción de documentos Error[" + Ex.Message + "]");
            }
        }

        protected void Btn_Comentar_Click(object sender, EventArgs e)
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
            DataTable Dt_Datos_Polizas = new DataTable();
            DataTable Dt_Tipo_Solicitud = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            ReportDocument Reporte = new ReportDocument();
            String Tipo_Solicitud = "";
            String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
            String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
            String Usuario = "";
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            String Resultado = "";
            // crear transaccion para crear el convenio 
            Cn.ConnectionString = Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Cn;
            Cmmd.Transaction = Trans;
            try
            {
                DataRow Row;
                DataTable Dt_Reporte = new DataTable();
                if (Txt_Comentario.Text != "")
                {
                    Resultado=Cancela_Solicitud_Pago(Hf_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(), Cmmd);
                    if (Resultado == "SI")
                    {

                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Hf_No_Solicitud_Autorizar.Value;
                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                        Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                        Trans.Commit();
                        //  se crea la tabla para el reporte de la cancelacion
                        Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                        Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                        Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                        Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                        Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                        Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                        Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                        Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                        Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                        Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                        Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                        Dt_Reporte.TableName = "Dt_Cancelacion";


                        foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                        {
                            Row = Dt_Reporte.NewRow();

                            Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                            Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                            Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                            Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                            Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                            Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                            //  para el tipo de solicitud
                            Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                            Rs_Tipo_Solicitud.P_Cmmd = Cmmd;
                            Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();
                            //Trans.Commit();
                            foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                            {
                                Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                            }
                            Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                            Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                            Row["COMENTARIO"] = Txt_Comentario.Text;
                            Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                            Usuario = Usuario.Replace(".", "");
                            Row["USUARIO_CREO"] = Usuario;
                            Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                            Usuario = Usuario.Replace(".", "");
                            Row["USUARIO_RECHAZO"] = Usuario;
                            Dt_Reporte.Rows.Add(Row);
                            Dt_Reporte.AcceptChanges();
                        }
                        Ds_Reporte.Clear();
                        Ds_Reporte.Tables.Clear();
                        Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                        Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                        Reporte.SetDataSource(Ds_Reporte);
                        DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                        Nombre_Archivo += ".pdf";
                        Ruta_Archivo = @Server.MapPath("../../Reporte/");
                        m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                        ExportOptions Opciones_Exportacion = new ExportOptions();
                        Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);

                        Abrir_Ventana(Nombre_Archivo);
                        Recepcion_Documentos_Inicio();
                    }
                    else
                    {
                        Trans.Rollback();
                        Lbl_Mensaje_Error.Text = "Error:";
                        Lbl_Mensaje_Error.Text = "No tienes suficiencia presupuestal para realizar la cancelacion";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                    //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);   
                }
                else
                {
                    Recepcion_Documentos_Inicio();
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

                }
            }
            catch (SqlException Ex)
            {
                Trans.Rollback();
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = "Error:";
                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                Img_Error.Visible = true;
            }
            finally
            {
                Cn.Close();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }

        protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else { 
                    Rs_Consultar_Solicitud_Pagos.P_Estatus="PREAUTORIZADO";
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }
                
                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[4].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[4].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Txt_Buscar_No_Solicitud_TextChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
            DataTable Dt_Resultado = new DataTable();
            String No_Autoriza_Ramo33 = "";
            String No_Autoriza_Ramo33_2 = "";
            try
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Validar_Usuario = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Usuario = new DataTable();
                Dt_Usuario = Rs_Validar_Usuario.Consulta_Datos_Parametros();
                if (Dt_Usuario.Rows.Count > 0)
                {
                    No_Autoriza_Ramo33 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC"].ToString();
                    No_Autoriza_Ramo33_2 = Dt_Usuario.Rows[0]["USUARIO_AUTORIZA_INV_PUBLIC2"].ToString();
                }
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
                {
                    Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else
                {
                    Rs_Consultar_Solicitud_Pagos.P_Estatus = "PREAUTORIZADO";
                    if (Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33 || Cls_Sessiones.Empleado_ID == No_Autoriza_Ramo33_2)
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RAMO 33";
                    }
                    else
                    {
                        Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    }
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes();
                }

                if (Dt_Resultado.Rows.Count <= 0)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                    Txt_Busqueda_No_Solicitud.Focus();
                }
                else
                {
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[4].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                    Grid_Solicitud_Pagos.DataBind();
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[4].Visible = false;
                    Txt_Busqueda_No_Solicitud.Text = "";
                }

            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        #region generar caratula
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Imprimir
        ///DESCRIPCIÓN: Imprime la solicitud
        ///PROPIEDADES:     
        ///CREO: Sergio Manuel Gallardo
        ///FECHA_CREO: 06/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Imprimir(String Numero_Solicitud)
        {
            DataSet Ds_Reporte = null;
            DataTable Dt_Pagos = null;
            try
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                Ds_Reporte = new DataSet();
                Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                if (Dt_Pagos.Rows.Count > 0)
                {
                    Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                    Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                }
            }
            //}
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                Lbl_Mensaje_Error.Visible = true;
            }

        }
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
        {
            ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
            String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
            try
            {
                Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                        Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        #endregion
        #region eventos
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
        ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
        ///PARÁMETROS :
        ///CREO       : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO  : 21-mayo-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        protected void Btn_Solicitud_Click(object sender, EventArgs e)
        {
            String No_Solicitud = ((LinkButton)sender).Text;
            Imprimir(No_Solicitud);
        }
            ///*********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 21/Diciembre/2011 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
                protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Inicio")
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Recepcion_Documentos_Inicio();
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
        #endregion
        #region"Grid"

                protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
                {
                    HyperLink Hyp_Lnk_Ruta;
                    try
                    {
                        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                        {
                            Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                            if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                                Hyp_Lnk_Ruta.Enabled = true;
                            }
                            else
                            {
                                Hyp_Lnk_Ruta.NavigateUrl = "";
                                Hyp_Lnk_Ruta.Enabled = false;
                            }
                        }
                    }
                    catch (Exception Ex)
                    {
                        throw new Exception(Ex.Message);
                    }
                }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
            ///PARAMETROS           :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 22/Diciembre/2011
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
            {
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Detalles = new DataTable();
                DataTable Dt_Documentos = new DataTable();

                try
                {
                    if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
                    {
                        Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[2].Text.Trim();
                        Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                        Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

                        if (Dt_Detalles != null)
                        {
                            if (Dt_Detalles.Rows.Count > 0)
                            {
                                Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                                Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                                Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                                Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                                Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                                Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                                Txt_Fecha_Autoriza_Director_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_AUTORIZO_RECHAZO_JEFE"]);
                                Txt_Fecha_Recibio_Documentacion_Fisica.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_RECIBIO_DOC_FISICA"]);
                                Txt_Recibio_Documentacion_Fisica.Text = Dt_Detalles.Rows[0]["USUARIO_RECIBIO_DOC_FISICA"].ToString().Trim();
                                Txt_No_poliza_Det.Text = Dt_Detalles.Rows[0]["NO_POLIZA"].ToString().Trim();
                                Grid_Solicitud_Pagos.SelectedIndex = -1;
                                if (String.IsNullOrEmpty(Txt_No_poliza_Det.Text))
                                {
                                    Tr_Poliza.Style.Add("Display", "none");
                                }
                                else
                                {
                                    Tr_Poliza.Style.Add("Display", "block");
                                }

                                Grid_Documentos.Columns[3].Visible = true;
                                Grid_Documentos.Columns[4].Visible = true;
                                Grid_Documentos.DataSource = Dt_Documentos;
                                Grid_Documentos.DataBind();
                                Grid_Documentos.Columns[3].Visible = false;
                                Grid_Documentos.Columns[4].Visible = false;
                                Mpe_Detalles.Show();
                            }
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
            /// DESCRIPCION : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 09/enero/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                try
                {
                    CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
                    CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazado");
                    if (e.Row.RowType == DataControlRowType.DataRow)
                    {
                        if (e.Row.Cells[8].Text != "PREAUTORIZADO")
                        {
                            chek.Enabled = false;
                            chek2.Enabled = false;
                        }
                        else
                        {
                            chek.Enabled = true;
                            chek2.Enabled = true;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
        #endregion

    #endregion


    
}
