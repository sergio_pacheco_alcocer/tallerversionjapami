﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Contabilidad_Transferencia.Negocio;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Parametros_Almacen_Cuentas.Negocio;
public partial class paginas_Contabilidad_Frm_Ope_Con_Transferencias : System.Web.UI.Page
{
    #region "Page_Load"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Refresca la session del usuario lagueado al sistema.
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Llenar_Combo_Banco();
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                //Cmb_Anio_Contable.SelectedValue = nuevo;
                //Llenar_Grid_Cierres_generales(nuevo);
                Acciones();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    
}
    #endregion

    #region "Metodos"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Limpia_Controles();             //Limpia los controles del forma
            Llenar_Grid_Solicitudes_Pago();
            Txt_Comision_Banco.Enabled = false;
            Txt_Iva_Comision.Enabled = false;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
        //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combo_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = true;
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Configuracion_Acceso("Frm_Ope_Con_Transferencias.aspx");
                    break;
            }
            //Cmb_Anio_Contable.Enabled = Habilitado;
            //Cmb_Mes_Contable.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_Transferencia_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Banco_Transferencia_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Modificada = new DataTable();
        Boolean Agregar;
        Double Total;
        String Proveedor_ID;
        String Cuenta;
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        String Empleado_ID = "";
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        DataTable Dt_Resultado = new DataTable();
        Lbl_Mensaje_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Img_Error.Visible = false;
        try
        {
            if (Cmb_Banco.SelectedValue != "0")
            {
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                if (Dt_Consulta.Rows.Count > 0)
                {

                    if (Chk_Spei.Checked == false)
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Txt_Comision_Banco.Text = "0";
                            Txt_Iva_Comision.Text = "0";
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Spay].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Spay].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Spay].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Spay].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Txt_Comision_Banco.Text = "0";
                            Txt_Iva_Comision.Text = "0";
                        }
                    }
                }
                else
                {
                    Txt_Comision_Banco.Text = "";
                    Txt_Iva_Comision.Text = "";
                    Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Txt_Comision_Banco.Text = "0";
                    Txt_Iva_Comision.Text = "0";
                }
                Rs_Consulta_Ejercidos.P_Banco = Cmb_Banco.SelectedValue;
                Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila["EMPLEADO_ID"].ToString()))
                        {
                            Empleado_ID = Fila["EMPLEADO_ID"].ToString();
                            Proveedor_ID = "";
                        }
                        else
                        {
                            Proveedor_ID = Fila["Proveedor_id"].ToString();
                            Empleado_ID = "";
                        } 
                        Cuenta = Fila["CUENTA_BANCO_PAGO_ID"].ToString();
                        if (Dt_Modificada.Rows.Count <= 0)
                        {
                            Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                            Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                            Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                            Dt_Modificada.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                        }
                        DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        Agregar = true;
                        Total = 0;
                        //insertar los que no se repiten
                        foreach (DataRow Fila2 in Dt_Modificada.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                        }
                        //se calcula el monto por tipo
                        foreach (DataRow Renglon in Dt_Resultado.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Renglon["Empleado_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Renglon["Proveedor_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                        }
                        if (Agregar && Total > 0)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["PROVEEDOR_ID"] = Empleado_ID;
                                row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                            }
                            else
                            {
                                row["PROVEEDOR_ID"] = Proveedor_ID;
                                row["TIPO_BENEFICIARIO"] = "PROVEEDOR";
                            }
                            row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                            row["MONTO"] = Total;
                            row["CUENTA"] = Fila["CUENTA_BANCO_PAGO_ID"].ToString().Trim();
                            row["ESTATUS"] = Fila["Estatus"].ToString();
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["IDENTIFICADOR_TIPO"] = "E-" + Empleado_ID + Total;
                            }
                            else
                            {
                                row["IDENTIFICADOR_TIPO"] = "P-" + Proveedor_ID + Total;
                            } 
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                    }
                    Session["Dt_Datos"] = Dt_Resultado;
                    Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                    Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                else
                {
                    Grid_Pagos.DataSource = null;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                }

            }
            else
            {
                Txt_Comision_Banco.Text = "";
                Txt_Iva_Comision.Text = "";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Chk_Spei_OnCheckedChanged
    ///DESCRIPCIÓN: Buscar la comision y el iva con spei
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo
    ///FECHA_CREO:  28/mayo/2013
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Chk_Spei_OnCheckedChanged(object sender, EventArgs e)
    {
        DataTable Dt_Modificada = new DataTable();
        Boolean Agregar;
        Double Total;
        String Proveedor_ID;
        String Cuenta;
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        String Empleado_ID = "";
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        DataTable Dt_Resultado = new DataTable();
        Lbl_Mensaje_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Img_Error.Visible = false;
        try
        {
            //Verificar si esta checado
            if (Chk_Spei.Checked == true)
            {
                Lbl_Comision.Text = "COMISION S.P.E.I.";
            }
            else
            {
                Lbl_Comision.Text = "COMISION T.E.F.";
            }
            if (Cmb_Banco.SelectedValue != "0")
            {
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                if (Dt_Consulta.Rows.Count > 0)
                {
                    if (Chk_Spei.Checked == false)
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Txt_Comision_Banco.Text = "0";
                            Txt_Iva_Comision.Text = "0";
                        }
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Spay].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Spay].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Spay].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Spay].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Txt_Comision_Banco.Text = "0";
                            Txt_Iva_Comision.Text = "0";
                        }
                    }
                }
                else
                {
                    Txt_Comision_Banco.Text = "";
                    Txt_Iva_Comision.Text = "";
                    Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Txt_Comision_Banco.Text = "0";
                    Txt_Iva_Comision.Text = "0";
                }
                Rs_Consulta_Ejercidos.P_Banco = Cmb_Banco.SelectedValue;
                Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila["EMPLEADO_ID"].ToString()))
                        {
                            Empleado_ID = Fila["EMPLEADO_ID"].ToString();
                            Proveedor_ID = "";
                        }
                        else
                        {
                            Proveedor_ID = Fila["Proveedor_id"].ToString();
                            Empleado_ID = "";
                        } 
                        Cuenta = Fila["CUENTA_BANCO_PAGO_ID"].ToString();
                        if (Dt_Modificada.Rows.Count <= 0)
                        {
                            Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                            Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                            Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                            Dt_Modificada.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                        }
                        DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        Agregar = true;
                        Total = 0;
                        //insertar los que no se repiten
                        foreach (DataRow Fila2 in Dt_Modificada.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                        }
                        //se calcula el monto por tipo
                        foreach (DataRow Renglon in Dt_Resultado.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Renglon["Empleado_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Renglon["Proveedor_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                        }
                        if (Agregar && Total > 0)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["PROVEEDOR_ID"] = Empleado_ID;
                                row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                            }
                            else
                            {
                                row["PROVEEDOR_ID"] = Proveedor_ID;
                                row["TIPO_BENEFICIARIO"] = "PROVEEDOR";
                            }
                            row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                            row["MONTO"] = Total;
                            row["CUENTA"] = Fila["CUENTA_BANCO_PAGO_ID"].ToString().Trim();
                            row["ESTATUS"] = Fila["Estatus"].ToString();
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["IDENTIFICADOR_TIPO"] = "E-" + Empleado_ID + Total;
                            }
                            else
                            {
                                row["IDENTIFICADOR_TIPO"] = "P-" + Proveedor_ID + Total;
                            } 
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                    }
                    Session["Dt_Datos"] = Dt_Resultado;
                    Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                    Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                else
                {
                    Grid_Pagos.DataSource = null;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                }

            }
            else
            {
                Txt_Comision_Banco.Text = "";
                Txt_Iva_Comision.Text = "";
            }
            //activar los check
            //Chk_IVA_Todos_CheckedChanged(this, new EventArgs());
            //Chk_Ref_Comisionar_Todos_CheckedChanged(this, new EventArgs());
            //Chk_Referenciar_Todos_CheckedChanged(this, new EventArgs());
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_Busqueda_No_Solicitud.Text = "";
            Txt_Monto_Solicitud.Value = "";
            Txt_Cuenta_Contable_ID_Proveedor.Value = "";
            Txt_Cuenta_Contable_reserva.Value = "";
            Txt_No_Reserva.Value = "";
            Grid_Pagos.DataSource = null;
            Grid_Pagos.DataBind();
            Session["Dt_Datos_Proveedor"] = null;
            Session["Dt_Datos"] = null;
            Session["Contador"] = null; 
            Session["Dt_Datos_Solicitud"] = null;
            Session["Dt_Datos_Det"] = null;
            Session["Contador_Solicitud"] = null;
            //Txt_Comentario.Text = "";
            //Txt_No_Solicitud_Autorizar.Value = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
    /// DESCRIPCION : Llena el grid Solicitudes de pago
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Grid_Solicitudes_Pago()
    {
        try
        {
            Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos= new Cls_Ope_Con_Transferencia_Negocio();
            DataTable Dt_Resultado = new DataTable();
            DataTable Dt_Modificada = new DataTable();
            Boolean Agregar;
            Double Total;
            String Proveedor_ID;
            String Empleado_ID = "";
            String Cuenta;
            DataTable Dt_Datos_Proveedor = new DataTable();
            Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
            DataTable Dt_Datos = new DataTable();
            Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
            Int32 Contador = 0;
            Session["Contador"] = Contador;
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Consulta = new DataTable();
            //Rs_Autoriza_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();

            Grid_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
            Grid_Pagos.DataBind();    // Se ligan los datos.;

            if (Dt_Resultado.Rows.Count > 0)
            {
                //Consultamos el banco con la primera traferencia
                Cmb_Banco.SelectedValue = Dt_Resultado.Rows[0]["Nombre_Banco"].ToString();
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                if (Dt_Consulta.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                    {
                        Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                        Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                    }
                    else
                    {
                        Txt_Comision_Banco.Text = "";
                        Txt_Iva_Comision.Text = "";
                        Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }

                Rs_Consulta_Ejercidos.P_Banco = Cmb_Banco.SelectedValue;
                Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();
                foreach (DataRow Fila in Dt_Resultado.Rows)
                {
                    if (!String.IsNullOrEmpty(Fila["EMPLEADO_ID"].ToString()))
                    {
                        Empleado_ID = Fila["EMPLEADO_ID"].ToString();
                        Proveedor_ID = "";
                    }else{
                        Proveedor_ID = Fila["Proveedor_id"].ToString();
                        Empleado_ID = "";
                    }
                    Cuenta = Fila["CUENTA_BANCO_PAGO_ID"].ToString();
                    if (Dt_Modificada.Rows.Count <= 0)
                    {
                        Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                        Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                        Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                        Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                        Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                        Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                    }
                    DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                    Agregar = true;
                    Total = 0;
                    //insertar los que no se repiten
                    foreach (DataRow Fila2 in Dt_Modificada.Rows)
                    {
                        if (!String.IsNullOrEmpty(Empleado_ID))
                        {
                            if (Empleado_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                            {
                                Agregar = false;
                            }
                        }
                        else
                        {
                            if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()))
                            {
                                Agregar = false;
                            }
                        }
                       
                    }
                    //se calcula el monto por tipo
                    foreach (DataRow Renglon in Dt_Resultado.Rows)
                    {
                        if (!String.IsNullOrEmpty(Empleado_ID))
                        {
                            if (Empleado_ID.Equals(Renglon["Empleado_ID"].ToString()))
                            {
                                Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                            }
                        }
                        else
                        {
                            if (Proveedor_ID.Equals(Renglon["Proveedor_ID"].ToString()))
                            {
                                Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                            }
                        }
                    }
                    if (Agregar && Total > 0)
                    {
                        if (!String.IsNullOrEmpty(Empleado_ID))
                        {
                            row["PROVEEDOR_ID"] = Empleado_ID;
                            row["TIPO_BENEFICIARIO"]="EMPLEADO";
                        }
                        else
                        {
                            row["PROVEEDOR_ID"] = Proveedor_ID;
                            row["TIPO_BENEFICIARIO"] = "PROVEEDOR";
                        }
                        row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                        row["MONTO"] = Total;
                        row["CUENTA"] = Fila["CUENTA_BANCO_PAGO_ID"].ToString().Trim();
                        row["ESTATUS"] = Fila["Estatus"].ToString();
                        if (!String.IsNullOrEmpty(Empleado_ID))
                        {
                            row["IDENTIFICADOR_TIPO"] ="E-" +Empleado_ID  + Total;
                        }
                        else
                        {
                            row["IDENTIFICADOR_TIPO"] = "P-"+Proveedor_ID + Total;
                        }
                        
                        Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificada.AcceptChanges();
                    }
                }
                Session["Dt_Datos"] = Dt_Resultado;
                Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                Grid_Pagos.Columns[6].Visible = true;
                Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                Grid_Pagos.DataBind();    // Se ligan los datos.;
                Grid_Pagos.Columns[6].Visible = false;
                //Grid_Solicitud_Pagos.Columns[3].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Pagos_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TextBox Txt_Fecha;
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable();
        Dt_Datos_Solicitud = ((DataTable)(Session["Dt_Datos_Solicitud"]));
        DataTable Dt_Datos_Det = new DataTable();
        Dt_Datos_Det = ((DataTable)(Session["Dt_Datos_Det"]));
        Int32 Contador_Solicitud = 0;
        Session["Contador_Solicitud"] = Contador_Solicitud;
        String Proveedor_ID = "";
        String Cuenta = "";
        Int32 Contador;
        String TIPO_BENEFICIARIO = "";
        Boolean Agregar;
        Double Total;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (Int32)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos"]));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Proveedor"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    Cuenta = Convert.ToString(Filas["CUENTA_BANCO_PAGO_ID"].ToString()).Trim();
                    if (Dt_Datos.Rows[Contador]["TIPO_BENEFICIARIO"].ToString()=="PROVEEDOR")
                    {
                        Proveedor_ID = Dt_Datos.Rows[Contador]["PROVEEDOR_ID"].ToString();
                        Dt_Datos_Detalles.DefaultView.RowFilter = "PROVEEDOR_ID='" + Proveedor_ID + "'";
                        TIPO_BENEFICIARIO = "PROVEEDOR";
                    }
                    else
                    {
                        Proveedor_ID = Dt_Datos.Rows[Contador]["PROVEEDOR_ID"].ToString();
                        Dt_Datos_Detalles.DefaultView.RowFilter = "EMPLEADO_ID='" + Proveedor_ID + "'";
                        TIPO_BENEFICIARIO = "EMPLEADO";
                    }
                    
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[5].FindControl("Grid_Cuentas");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            //Cuenta = Convert.ToString(Fila["CUENTA_BANCO_PAGO_ID"].ToString()).Trim();
                            if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("CUENTA_BANCO_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("NO_CUENTA", typeof(System.String));
                                Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                Dt_Modificada.Columns.Add("BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("ORDEN", typeof(System.String));
                                Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            Agregar = true;
                            Total = 0;
                            //insertar los que no se repiten
                            foreach (DataRow Fila2 in Dt_Modificada.Rows)
                            {
                                if (TIPO_BENEFICIARIO == "PROVEEDOR")
                                {
                                    if (Proveedor_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()) && Cuenta.Equals(Fila2["CUENTA_BANCO_ID"].ToString()) && TIPO_BENEFICIARIO.Equals(Fila2["TIPO_BENEFICIARIO"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                                else
                                {
                                    if (Proveedor_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()) && Cuenta.Equals(Fila2["CUENTA_BANCO_ID"].ToString()) && TIPO_BENEFICIARIO.Equals(Fila2["TIPO_BENEFICIARIO"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                                }
                               
                            }
                            //se calcula el monto por tipo
                            foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (TIPO_BENEFICIARIO == "PROVEEDOR")
                                {
                                    if (Proveedor_ID.Equals(Renglon["PROVEEDOR_ID"].ToString()) && Cuenta.Equals(Renglon["CUENTA_BANCO_PAGO_ID"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                    }
                                }
                                else
                                {
                                    if (Proveedor_ID.Equals(Renglon["EMPLEADO_ID"].ToString()) && Cuenta.Equals(Renglon["CUENTA_BANCO_PAGO_ID"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                    }
                                }
                            }
                            if (Agregar && Total > 0)
                            {
                                row["PROVEEDOR_ID"] = Proveedor_ID;
                                if(TIPO_BENEFICIARIO =="PROVEEDOR"){
                                    row["TIPO_BENEFICIARIO"] = "PROVEEDOR";
                                }else{
                                    row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                                }
                                row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                                row["MONTO"] = Total;
                                row["ESTATUS"] = Fila["Estatus"].ToString();
                                row["BANCO"] = Fila["NOMBRE_BANCO"].ToString();
                                row["CUENTA_BANCO_ID"] = Cuenta;
                                row["NO_CUENTA"] = Fila["NO_CUENTA"].ToString().Trim();
                                row["ORDEN"] = Fila["TRANSFERENCIA"].ToString();
                                row["IDENTIFICADOR"] = Proveedor_ID + Cuenta;
                                Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Modificada.AcceptChanges();
                            }
                        }
                    }
                }
                Session["Dt_Datos_Solicitud"] = Dt_Modificada;
                Session["Dt_Datos_Det"] = Dt_Datos_Detalles;
                Gv_Detalles.Columns[9].Visible = true;
                Gv_Detalles.Columns[6].Visible = true;
                Gv_Detalles.Columns[7].Visible = true;
                Gv_Detalles.DataSource = Dt_Modificada;
                Gv_Detalles.DataBind();
                Gv_Detalles.Columns[9].Visible = false;
                Gv_Detalles.Columns[6].Visible = false;
                Gv_Detalles.Columns[7].Visible = false;
                Session["Contador"] = Contador + 1;
                Txt_Fecha = (TextBox)e.Row.Cells[3].FindControl("Txt_Fecha_Poliza");
                Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();

            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        String Proveedor_ID = "";
        String Cuenta = "";
        String No_Solicitud;
        Int32 Contador_Solicitud;
        String Tipo_Beneficiario = "";
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Cuenta");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio2");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Solicitud");
        Cls_Cat_Com_Proveedores_Negocio Proveedor = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Banco_Proveedor = new DataTable();
        TextBox Txt_Ref_Comision;
        TextBox Txt_Ref_IVA;
        TextBox Txt_Ref;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador_Solicitud = (Int32)(Session["Contador_Solicitud"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Det"]));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Solicitud"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    No_Solicitud = Filas["NO_SOLICITUD_PAGO"].ToString();
                    Proveedor_ID = Dt_Datos.Rows[Contador_Solicitud]["PROVEEDOR_ID"].ToString();
                    Cuenta = Convert.ToString(Dt_Datos.Rows[Contador_Solicitud]["CUENTA_BANCO_ID"].ToString()).Trim();
                    Tipo_Beneficiario = Dt_Datos.Rows[Contador_Solicitud]["TIPO_BENEFICIARIO"].ToString();
                    if (Tipo_Beneficiario == "PROVEEDOR")
                    {
                        Dt_Datos_Detalles.DefaultView.RowFilter = "PROVEEDOR_ID='" + Proveedor_ID + "' AND CUENTA_BANCO_PAGO_ID='" + Cuenta + "' AND NO_SOLICITUD_PAGO='" + No_Solicitud + "'";
                    }
                    else
                    {
                        Dt_Datos_Detalles.DefaultView.RowFilter = "EMPLEADO_ID='" + Proveedor_ID + "' AND CUENTA_BANCO_PAGO_ID='" + Cuenta + "' AND NO_SOLICITUD_PAGO='" + No_Solicitud + "'";
                    }
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[4].FindControl("Grid_Datos_Solicitud");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("NO_SOLICITUD", typeof(System.String));
                                Dt_Modificada.Columns.Add("NO_RESERVA", typeof(System.String));
                                Dt_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("TRANSFERENCIA", typeof(System.String));
                                Dt_Modificada.Columns.Add("NOMBRE_BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            row["NO_SOLICITUD"] = Fila["NO_SOLICITUD_PAGO"].ToString();
                            row["NO_RESERVA"] = Fila["NO_RESERVA"].ToString();
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString();
                            row["TRANSFERENCIA"] = Fila["TRANSFERENCIA"].ToString();
                            row["NOMBRE_BANCO"] = Fila["NOMBRE_BANCO"].ToString();
                            row["MONTO"] = Fila["MONTO"].ToString();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString();
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                        Gv_Detalles.Columns[1].Visible = true;
                        Gv_Detalles.Columns[4].Visible = true;
                        Gv_Detalles.Columns[5].Visible = true;
                        Gv_Detalles.Columns[7].Visible = true;
                        Gv_Detalles.DataSource = Dt_Modificada;
                        Gv_Detalles.DataBind();
                        Gv_Detalles.Columns[1].Visible = false;
                        Gv_Detalles.Columns[4].Visible = false;
                        Gv_Detalles.Columns[5].Visible = false;
                        Gv_Detalles.Columns[7].Visible = false;
                    }
                }
                Session["Contador_Solicitud"] = Contador_Solicitud + 1;
                Proveedor.P_Proveedor_ID = e.Row.Cells[9].Text.Trim();
                Dt_Banco_Proveedor = Proveedor.Consulta_Datos_Proveedores();
                if (Dt_Banco_Proveedor.Rows.Count > 0)
                {
                    Txt_Ref_Comision = (TextBox)e.Row.Cells[5].FindControl("Txt_Referencia_Comision");
                    Txt_Ref_IVA = (TextBox)e.Row.Cells[6].FindControl("Txt_Referencia_IVA_Comision");
                    Txt_Ref = (TextBox)e.Row.Cells[4].FindControl("Txt_Beneficiario_Grid");
                    if (Dt_Banco_Proveedor.Rows[0]["BANCO_PROVEEDOR"].ToString().Trim().ToUpper() == e.Row.Cells[3].Text.Trim().Trim().ToUpper())
                    {

                        Txt_Ref.Style.Add("background-color", "#E6E6E6");
                        Txt_Ref.Enabled = false;
                    }
                    else
                    {
                        Txt_Ref.Style.Add("background-color", "#A9F5A9");
                        Txt_Ref.Enabled = true;
                    }
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    
    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:Accion
    //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Acciones()
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        String Accion = String.Empty;
        String No_Solicitud = String.Empty;
        String Comentario = String.Empty;
        DataTable Dt_Consulta_Estatus = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        ReportDocument Reporte = new ReportDocument();
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento

        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
        if (Request.QueryString["Accion"] != null)
        {
            Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
            if (Request.QueryString["id"] != null)
            {
                No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                
            }
            switch (Accion)
            {
                case "Rechazar_Transferencia":
                    Rs_Consulta_Ejercidos.P_No_solicitud_Pago = No_Solicitud;
                    Rs_Consulta_Ejercidos.Modificar_Transferencia();
                    //Inicializa_Controles();
                    break;
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Transferencia
    /// DESCRIPCION : Da de Alta el Cheque con los datos proporcionados
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private String Alta_Transferencia(DataTable Dt_Solicitudes, SqlCommand P_Cmmd)
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Folio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Reserva = new DataTable();
        DataTable Dt_Solicitudes_Modificada = new DataTable();
        DataTable Dt_Solicitudes_Completa = new DataTable();
        DataTable Dt_Solicitudes_Partidas = new DataTable();
        DataTable Dt_Datos_Proveedor = new DataTable();
        DataTable Dt_Tipo_Operacion = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Solicitud_Pagos = new DataTable(); //Variable a contener los datos de la consulta de la solicitud de pago
        Cls_Ope_Con_Reservas_Negocio Rs_Reserva = new Cls_Ope_Con_Reservas_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio Rs_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de coneción hacia la capa de datos
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consultar_Solicitud_Anticipo = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Proveedor = new Cls_Cat_Com_Proveedores_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_constulta_Empleado = new Cls_Ope_Con_Cheques_Negocio();
        Cls_Cat_Con_Parametros_Negocio Rs_Parametros =new Cls_Cat_Con_Parametros_Negocio();
        DataTable Dt_Consulta_Solicitud_Pago = new DataTable();
        //String Estatus_Reserva = "";
        Cls_Cat_Alm_Parametros_Cuentas_Negocio Rs_Parametros_Iva = new Cls_Cat_Alm_Parametros_Cuentas_Negocio();
        Decimal Total_Iva_por_Acreditar = 0;
        String Pago = "";
        String Cuenta_Contable_Proveedor = "";
        String Cuenta = "";
        String Proveedor = "";
        String CTA_IVA_Acreditable = "";
        String CTA_IVA_Pendiente_Acreditar = "";
        String Banco = "";
        String Cuenta_Contable_ID_Banco = "";
        String Cuenta_Contable_Proveedor_Banco_ID = "";
        String Cuenta_Servicios_financieros = "";
        String Cuenta_Contable_Serv_Financieros = "";
        String Tipo_Beneficiario;
        Int32 partida = 0;
        Boolean Agregar;
        Decimal Total;
        String Nombre_Proveedor = "";
        Decimal Total_Cedular;
        Decimal Total_ISR;
        String Resultados = "";
        Decimal Anticipo = 0;
        Decimal Calculo_Anticipo = 0;
        String Fecha_Pago = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        String Bandera = "1"; 
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_detalles = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas_Iva = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        String Beneficiario_ID = "";
        String Tipo_Beneficiario_Poliza = "";
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;
        if (P_Cmmd != null)
        {
            Cmmd = P_Cmmd;
        }
        else
        {
            Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
            Cn.Open();
            Trans = Cn.BeginTransaction();
            Cmmd.Connection = Trans.Connection;
            Cmmd.Transaction = Trans;
        }
        try
        {
            if (Dt_Solicitudes_Modificada.Rows.Count <= 0)
            {
                Dt_Solicitudes_Modificada.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Proveedor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Monto", typeof(System.Decimal));
                Dt_Solicitudes_Modificada.Columns.Add("Reserva", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Banco_Proveedor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Tipo_Beneficiario", typeof(System.String));
            }
            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                if (Fila["Tipo_Beneficiario"].ToString() == "EMPLEADO")
                {
                    Rs_constulta_Empleado.P_Empleado_ID_Jefe = Fila["PROVEEDOR"].ToString();
                    Rs_constulta_Empleado.P_Cmmd = Cmmd;
                    Dt_Datos_Proveedor = Rs_constulta_Empleado.Consultar_Datos_Empleado();
                    if (Dt_Datos_Proveedor.Rows.Count > 0)
                    {
                        Nombre_Proveedor = Dt_Datos_Proveedor.Rows[0]["NOMBRE"].ToString() + " " + Dt_Datos_Proveedor.Rows[0]["APELLIDO_PATERNO"].ToString() + " " + Dt_Datos_Proveedor.Rows[0]["APELLIDO_MATERNO"].ToString();
                        DataRow Rows = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        Rows["No_Solicitud"] = Fila["No_Solicitud"].ToString();
                        Rows["Referencia"] = Fila["Referencia"].ToString();
                        Rows["Banco"] = Fila["Banco"].ToString();
                        Rows["cuenta"] = Fila["cuenta"].ToString();
                        Rows["Proveedor"] = Fila["Proveedor"].ToString();
                        Rows["Monto"] = Fila["Monto"].ToString();
                        Rows["Reserva"] = Fila["Reserva"].ToString();
                        Rows["Tipo_Solicitud_Pago"] = Fila["Tipo_Solicitud_Pago"].ToString();
                        Rows["Banco_Proveedor"] = Dt_Datos_Proveedor.Rows[0]["BANCO"].ToString();
                        Rows["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString();
                        Rows["Ref_Comision"] = Fila["Ref_Comision"].ToString();
                        Rows["Ref_IVA"] = Fila["Ref_IVA"].ToString();
                        Rows["Tipo_Beneficiario"] = Fila["Tipo_Beneficiario"].ToString();
                        Dt_Solicitudes_Modificada.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes_Modificada.AcceptChanges();
                    }
                }
                else
                {
                    Rs_Consulta_Proveedor.P_Proveedor_ID = Fila["PROVEEDOR"].ToString();
                    Rs_Consulta_Proveedor.P_Cmmd = Cmmd;
                    Dt_Datos_Proveedor = Rs_Consulta_Proveedor.Consulta_Datos_Proveedores();
                    if (Dt_Datos_Proveedor.Rows.Count > 0)
                    {
                        Nombre_Proveedor = Dt_Datos_Proveedor.Rows[0]["COMPANIA"].ToString();
                        DataRow Rows = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        Rows["No_Solicitud"] = Fila["No_Solicitud"].ToString();
                        Rows["Referencia"] = Fila["Referencia"].ToString();
                        Rows["Banco"] = Fila["Banco"].ToString();
                        Rows["cuenta"] = Fila["cuenta"].ToString();
                        Rows["Proveedor"] = Fila["Proveedor"].ToString();
                        Rows["Monto"] = Fila["Monto"].ToString();
                        Rows["Reserva"] = Fila["Reserva"].ToString();
                        Rows["Tipo_Solicitud_Pago"] = Fila["Tipo_Solicitud_Pago"].ToString();
                        Rows["Banco_Proveedor"] = Dt_Datos_Proveedor.Rows[0]["BANCO_PROVEEDOR"].ToString();
                        Rows["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString();
                        Rows["Ref_Comision"] = Fila["Ref_Comision"].ToString();
                        Rows["Ref_IVA"] = Fila["Ref_IVA"].ToString();
                        Rows["Tipo_Beneficiario"] = Fila["Tipo_Beneficiario"].ToString();
                        Dt_Solicitudes_Modificada.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes_Modificada.AcceptChanges();
                    }
                }
               
            }
            if (Dt_Solicitudes_Completa.Rows.Count <= 0)
            {
                Dt_Solicitudes_Completa.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Monto", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Reserva", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Banco_Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_Proveedor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Monto_Cedular", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Monto_ISR", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Monto_Transferencia", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Anticipo", typeof(System.Decimal));
                Dt_Solicitudes_Completa.Columns.Add("Beneficiario_ID", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Proveedor_Bancario_ID", typeof(System.String));
            }
            foreach (DataRow Renglon in Dt_Solicitudes_Modificada.Rows)
            {
                DataRow ROW = Dt_Solicitudes_Completa.NewRow(); //Crea un nuevo registro a la tabla
                ROW["No_Solicitud"] = Renglon["No_Solicitud"].ToString();
                ROW["Referencia"] = Renglon["Referencia"].ToString();
                ROW["Banco"] = Renglon["Banco"].ToString();
                ROW["cuenta"] = Renglon["cuenta"].ToString();
                ROW["Proveedor"] = Renglon["Proveedor"].ToString();
                ROW["Monto"] = Renglon["Monto"].ToString();
                ROW["Reserva"] = Renglon["Reserva"].ToString();
                ROW["Tipo_Solicitud_Pago"] = Renglon["Tipo_Solicitud_Pago"].ToString();
                ROW["Banco_Proveedor"] = Renglon["BANCO_PROVEEDOR"].ToString();
                ROW["Fecha_Poliza"] = Renglon["Fecha_Poliza"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["Cuenta"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    ROW["Cuenta_Contable_ID_Banco"] = Cuenta_Contable_ID_Banco = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                Dt_Banco_Cuenta_Contable = new DataTable();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["Cuenta"].ToString();
                Rs_Ope_Con_Cheques.P_Cmmd = Cmmd;
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Proveedor_Bancario();
                if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    ROW["Cuenta_Proveedor_Bancario_ID"] = Cuenta_Contable_Proveedor_Banco_ID = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_Proveedor].ToString();
                }
                 Total_Cedular=0;
                 Total_ISR = 0;
                //  para Obtener la cuenta Contable del proveedor la cuenta contable
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                {
                    if (!String.IsNullOrEmpty(Registro["EMPLEADO_CUENTA"].ToString()))
                    {
                        ROW["Cuenta_Contable_Proveedor"] = Registro["EMPLEADO_CUENTA"].ToString();
                        ROW["Beneficiario_ID"] = Registro[Cat_Empleados.Campo_Empleado_ID].ToString();
                        ROW["Tipo_Beneficiario_Poliza"] = "EMPLEADO";
                    }
                    else
                    {
                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Acreedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "A")
                        {
                            ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Acreedor"].ToString();
                            ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                            ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                        }
                        else
                        {
                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Contratista"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "C")
                            {
                                ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Contratista"].ToString();
                                ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                            }
                            else
                            {
                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Deudor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "D")
                                {
                                    ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Deudor"].ToString();
                                    ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                    ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Judicial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "J")
                                    {
                                        ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Judicial"].ToString();
                                        ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                        ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                                    }
                                    else
                                    {
                                        if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Nomina"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "N")
                                        {
                                            ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Nomina"].ToString();
                                            ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                            ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Predial"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "Z")
                                            {
                                                ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Predial"].ToString();
                                                ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                                            }
                                            else
                                            {
                                                if (!String.IsNullOrEmpty(Registro["Pro_Cuenta_Proveedor"].ToString()) && Registro["BENEFICIARIO"].ToString().Substring(0, 1) == "P")
                                                {
                                                    ROW["Cuenta_Contable_Proveedor"] = Registro["Pro_Cuenta_Proveedor"].ToString();
                                                    ROW["Beneficiario_ID"] = Registro[Cat_Com_Proveedores.Campo_Proveedor_ID].ToString();
                                                    ROW["Tipo_Beneficiario_Poliza"] = "PROVEEDOR";
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (!String.IsNullOrEmpty(Registro["MONTO_CEDULAR"].ToString()))
                    {
                        Total_Cedular = Total_Cedular + Convert.ToDecimal(Registro["MONTO_CEDULAR"].ToString()); ;
                        Total_ISR = Total_Cedular + Convert.ToDecimal(Registro["MONTO_ISR"].ToString());
                    }
                    else
                    {
                        Total_Cedular = Total_Cedular;
                        Total_ISR = Total_ISR;
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_No_Solicitud_Pago = Renglon["No_Solicitud"].ToString();
                Rs_Modificar_Ope_Con_Solicitud_detalles.P_Cmmd = Cmmd;
                Dt_Datos_Solicitud = Rs_Modificar_Ope_Con_Solicitud_detalles.Consulta_Detalles_Solicitud();
                if (Dt_Datos_Solicitud.Rows.Count > 0)
                {
                    foreach (DataRow Fila_Iva in Dt_Datos_Solicitud.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString()))
                        {
                            Total_Iva_por_Acreditar = Total_Iva_por_Acreditar + Convert.ToDecimal(Fila_Iva[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString());
                        }
                    }
                }
                ROW["Monto_Cedular"] = Total_Cedular;
                ROW["Monto_ISR"] = Total_ISR;
                if (!String.IsNullOrEmpty(Renglon["Ref_Comision"].ToString()) && !String.IsNullOrEmpty(Renglon["Ref_IVA"].ToString()))
                {
                    ROW["Monto_Transferencia"] = Convert.ToString(Convert.ToDecimal(ROW["Monto"]) + Convert.ToDecimal(Txt_Comision_Banco.Text.ToString()) + Convert.ToDecimal(Txt_Iva_Comision.Text.ToString()) - (Convert.ToDecimal(ROW["Monto_ISR"].ToString()) + Convert.ToDecimal(ROW["Monto_Cedular"].ToString())));
                }
                else
                {
                    ROW["Monto_Transferencia"] = Convert.ToString(Convert.ToDecimal(ROW["Monto"]) - ((Convert.ToDecimal(ROW["Monto_ISR"].ToString())) + Convert.ToDecimal(ROW["Monto_Cedular"].ToString())));
                }
                ROW["Ref_Comision"] = Renglon["Ref_Comision"].ToString();
                ROW["Ref_IVA"] = Renglon["Ref_IVA"].ToString();
                ROW["Tipo_Beneficiario"] = Renglon["Tipo_Beneficiario"].ToString();
                Dt_Solicitudes_Completa.Rows.Add(ROW); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Solicitudes_Completa.AcceptChanges();
            }
            Proveedor = "";
            Banco ="";
            Cuenta = "";
            Cuenta_Contable_ID_Banco = "";
            Cuenta_Contable_Proveedor = "";
            Cuenta_Contable_Proveedor_Banco_ID = "";
            Tipo_Beneficiario = "";
            // Se agrupan los pagos por proveedor y cuenta 
            foreach (DataRow Fila in Dt_Solicitudes_Completa.Rows)
            {
                if (Dt_Solicitudes_Partidas.Rows.Count <= 0 && Dt_Solicitudes_Partidas.Columns.Count <= 0)
                {
                    Dt_Solicitudes_Partidas.Columns.Add("Referencia", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco_Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_Proveedor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Proveedor_Bancario_ID", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto_Cedular", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Monto_ISR", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Fecha_Poliza", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_Comision", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_IVA", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Anticipo", typeof(System.Decimal));
                    Dt_Solicitudes_Partidas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
                }
                DataRow Filas = Dt_Solicitudes_Partidas.NewRow(); //Crea un nuevo registro a la tabla
                Agregar = true;
                Total = 0;
                Total_Cedular = 0;
                Total_ISR = 0;
                Proveedor = Fila["Proveedor"].ToString();
                Banco = Fila["Banco"].ToString();
                Cuenta = Fila["Cuenta"].ToString();
                Cuenta_Contable_ID_Banco = Fila["Cuenta_Contable_ID_Banco"].ToString();
                Cuenta_Contable_Proveedor_Banco_ID = Fila["Cuenta_Proveedor_Bancario_ID"].ToString();
                Cuenta_Contable_Proveedor = Fila["Cuenta_Contable_Proveedor"].ToString();
                Tipo_Beneficiario = Fila["Tipo_Beneficiario"].ToString();
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Solicitudes_Partidas.Rows)
                {
                    if (Tipo_Beneficiario=="PROVEEDOR")
                    {
                        if (Proveedor.Equals(Fila2["PROVEEDOR"].ToString()) && Cuenta.Equals(Fila2["CUENTA"].ToString()) && Banco.Equals(Fila2["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Fila2["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Fila2["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Fila2["Tipo_Beneficiario"].ToString()))
                        {
                            Agregar = false;
                        }
                    }else{
                        if (Proveedor.Equals(Fila2["PROVEEDOR"].ToString()) && Cuenta.Equals(Fila2["CUENTA"].ToString()) && Banco.Equals(Fila2["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Fila2["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Fila2["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Fila2["Tipo_Beneficiario"].ToString()))
                        {
                            Agregar = false;
                        }
                    }
                    
                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Dt_Solicitudes_Completa.DefaultView.ToTable().Rows)
                {
                    if (Tipo_Beneficiario == "PROVEEDOR")
                    {
                        if (Proveedor.Equals(Renglon["PROVEEDOR"].ToString()) && Cuenta.Equals(Renglon["CUENTA"].ToString()) && Banco.Equals(Renglon["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Renglon["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Renglon["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Renglon["Tipo_Beneficiario"].ToString()))
                        {
                            Total = Total + Convert.ToDecimal(Renglon["Monto"].ToString());
                            if(!String.IsNullOrEmpty(Renglon["Monto_Cedular"].ToString())){
                                Total_Cedular = Total_Cedular + Convert.ToDecimal(Renglon["Monto_Cedular"].ToString());
                                Total_ISR = Total_ISR + Convert.ToDecimal(Renglon["Monto_ISR"].ToString());
                            }
                        }
                    }
                    else
                    {
                        if (Proveedor.Equals(Renglon["PROVEEDOR"].ToString()) && Cuenta.Equals(Renglon["CUENTA"].ToString()) && Banco.Equals(Renglon["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Renglon["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Proveedor.Equals(Renglon["Cuenta_Contable_Proveedor"].ToString()) && Tipo_Beneficiario.Equals(Renglon["Tipo_Beneficiario"].ToString()))
                        {
                            Total = Total + Convert.ToDecimal(Renglon["Monto"].ToString());
                            if (!String.IsNullOrEmpty(Renglon["Monto_Cedular"].ToString()))
                            {
                                Total_Cedular = Total_Cedular + Convert.ToDecimal(Renglon["Monto_Cedular"].ToString());
                                Total_ISR = Total_ISR + Convert.ToDecimal(Renglon["Monto_ISR"].ToString());
                            }
                        }
                    }
                    
                }
                if (Agregar && Total > 0)
                {
                    Filas["Banco"] = Fila["Banco"].ToString();
                    Filas["cuenta"] = Fila["cuenta"].ToString();
                    Filas["Proveedor"] = Fila["Proveedor"].ToString();
                    Filas["Monto"] = Total;
                    Filas["Banco_Proveedor"] = Fila["Banco_Proveedor"].ToString();
                    Filas["Referencia"] = Fila["Referencia"].ToString();
                    Filas["Cuenta_Contable_ID_Banco"] = Fila["Cuenta_Contable_ID_Banco"].ToString();
                    Filas["Cuenta_Proveedor_Bancario_ID"] = Fila["Cuenta_Proveedor_Bancario_ID"].ToString();
                    Filas["Cuenta_Contable_Proveedor"] = Fila["Cuenta_Contable_Proveedor"].ToString().Trim();
                    Filas["Monto_Cedular"] =Total_Cedular;
                    Filas["Monto_ISR"] = Total_ISR;
                    Filas["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString().Trim();
                    Filas["Ref_Comision"] = Fila["Ref_Comision"].ToString().Trim();
                    Filas["Ref_IVA"] = Fila["Ref_IVA"].ToString().Trim();
                    Filas["Tipo_Beneficiario"] = Fila["Tipo_Beneficiario"].ToString().Trim();
                    Filas["Beneficiario_ID"] = Fila["Beneficiario_ID"].ToString().Trim();
                    Filas["Tipo_Beneficiario_Poliza"] = Fila["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Solicitudes_Partidas.Rows.Add(Filas); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Solicitudes_Partidas.AcceptChanges();
                    Total_Cedular = 0;
                    Total_ISR = 0;
                    Anticipo =0;
                }
            }
            // Se realiza el registro del pago y de la poliza contable
            foreach (DataRow Registro in Dt_Solicitudes_Partidas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Ref_IVA"].ToString()) && !String.IsNullOrEmpty(Registro["Ref_Comision"].ToString()))
                {
                     Bandera="1";
                }
                else{
                     Bandera="2";
                }
                partida = 0;
                Dt_Partidas_Polizas = null;
                Dt_Partidas_Polizas = new DataTable();
                if (Dt_Partidas_Polizas.Rows.Count <= 0 && Dt_Partidas_Polizas.Columns.Count <= 0)
                {
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                }
                partida = partida + 1;
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                partida = partida + 1;
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_Proveedor"].ToString().Trim();//Cuenta_Contable_Proveedor;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                if (Cmb_Banco.SelectedItem.Text.ToString() != Registro["Banco_Proveedor"].ToString().Trim())
                {
                    // se agregan las partidas de la comision y el iva por comisiones
                    Rs_Parametros.P_Cmmd = Cmmd;
                    Dt_C_Proveedor = Rs_Parametros.Consulta_Datos_Parametros();
                    if (Dt_C_Proveedor.Rows.Count > 0)
                    {
                        Cuenta_Contable_Serv_Financieros = Dt_C_Proveedor.Rows[0][Cat_Con_Parametros.Campo_CTA_Comision_Bancaria].ToString();
                        //Cuenta_Servicios_financieros = Dt_C_Proveedor.Rows[0]["Campo_CTA_Comision_Bancaria"].ToString();
                    }
                    Rs_Parametros_Iva.P_Cmmd = Cmmd;
                    Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                    if (Dt_Cuentas_Iva.Rows.Count > 0)
                    {
                        CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                    }
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();
                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_IVA"].ToString().Trim();
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Serv_Financieros;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Comision_Banco.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_Comision"].ToString().Trim();
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_IVA"].ToString().Trim();
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Comision_Banco.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_Comision"].ToString().Trim();
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    partida = partida + 1;
                    Dt_Partidas_Polizas.AcceptChanges();
                }
                //Se agrega la partida de iva pendiente de acreditar
                Dt_Cuentas_Iva = new DataTable();
                Rs_Parametros_Iva.P_Cmmd = Cmmd;
                Dt_Cuentas_Iva = Rs_Parametros_Iva.Consultar_Cuentas();
                if (Dt_Cuentas_Iva.Rows.Count > 0)
                {
                    if (Total_Iva_por_Acreditar > 0)
                    {
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Acreditable = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Acreditable].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Acreditable))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = Total_Iva_por_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0; 
                            row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                            row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            partida = partida + 1;
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                        // CUENTA RETENCIONES IMPUESTO ISR 10% HONORARIOS consultamos el id que tiene la cuenta en el sistema
                        if (Dt_Cuentas_Iva.Rows.Count > 0)
                        {
                            CTA_IVA_Pendiente_Acreditar = Dt_Cuentas_Iva.Rows[0][Cat_Alm_Parametros_Cuentas.Campo_Cta_IVA_Pendiente].ToString().Trim();
                        }
                        if (!String.IsNullOrEmpty(CTA_IVA_Pendiente_Acreditar))
                        {
                            row = Dt_Partidas_Polizas.NewRow();
                            //Agrega el cargo del registro de la póliza
                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                            row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                            row[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Iva_por_Acreditar;
                            row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                            row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    
                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            partida = partida + 1; 
                            Dt_Partidas_Polizas.AcceptChanges();
                        }
                    }
                }
                //Se agrega la partida del proveedor bancario
                row = Dt_Partidas_Polizas.NewRow();
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Proveedor_Bancario_ID"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Txt_Comision_Banco.Text.ToString()) + Convert.ToDecimal(Txt_Iva_Comision.Text.ToString());//Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                partida = partida + 1;

                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Proveedor_Bancario_ID"].ToString().Trim();//Cuenta_Contable_Proveedor;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Txt_Comision_Banco.Text.ToString()) + Convert.ToDecimal(Txt_Iva_Comision.Text.ToString());//Convert.ToDecimal(Registro["Monto"].ToString().Trim()) - (Convert.ToDecimal(Registro["Monto_Cedular"].ToString().Trim()) + Convert.ToDecimal(Registro["Monto_ISR"].ToString().Trim()));//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                partida = partida + 1;

                //Se agrega la partida de iva pendiente de acreditar del iva de comision
                if (Convert.ToDouble(Txt_Iva_Comision.Text.ToString()) > 0)
                {
                    row = Dt_Partidas_Polizas.NewRow();
                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Acreditable;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();

                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    partida = partida + 1;
                    Dt_Partidas_Polizas.AcceptChanges();
                    row = Dt_Partidas_Polizas.NewRow();
                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = CTA_IVA_Pendiente_Acreditar;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row["Beneficiario_ID"] = Registro["Beneficiario_ID"].ToString().Trim();
                    row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    partida = partida + 1;
                    Dt_Partidas_Polizas.AcceptChanges();
                }
                Dt_Solicitudes_Partidas.DefaultView.RowFilter = "Proveedor='" + Registro["Proveedor"].ToString().Trim() + "' AND Cuenta='" + Registro["Cuenta"].ToString().Trim() + "' AND Tipo_Beneficiario='" + Registro["Tipo_Beneficiario"].ToString().Trim() + "'";
                Dt_Partidas_Polizas=Comprimir_Poliza(Dt_Partidas_Polizas);
                Rs_Ope_Con_Cheques.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Ope_Con_Cheques.P_Dt_Datos_Completos = Dt_Solicitudes_Completa;
                Rs_Ope_Con_Cheques.P_Dt_Datos_Agrupados = Dt_Solicitudes_Partidas.DefaultView.ToTable();
                Rs_Ope_Con_Cheques.P_No_Partidas = Dt_Partidas_Polizas.Rows.Count.ToString();
                Rs_Ope_Con_Cheques.P_Comentario = "";
                if (Bandera.Equals("1"))
                {
                    Rs_Ope_Con_Cheques.P_Monto_Comision = Txt_Comision_Banco.Text.ToString();
                    Rs_Ope_Con_Cheques.P_Monto_Iva = Txt_Iva_Comision.Text.ToString();
                }
                else
                {
                    Rs_Ope_Con_Cheques.P_Monto_Comision = "0";
                    Rs_Ope_Con_Cheques.P_Monto_Iva = "0";
                }
                Rs_Ope_Con_Cheques.P_Estatus = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Fecha_Pago)).ToString();
                Rs_Ope_Con_Cheques.P_Tipo_Pago = "TRANSFERENCIA";
                Rs_Ope_Con_Cheques.P_Beneficiario_Pago = Nombre_Proveedor;//Dt_Solicitudes_Partidas.Rows[0]["PROVEEDOR"].ToString().Trim();
                Rs_Ope_Con_Cheques.P_Estatus_Comparacion = "PAGADO";
                Rs_Ope_Con_Cheques.P_Fecha_Creo = String.Format("{0:dd/MM/yy}", DateTime.Now).ToString();
                Rs_Ope_Con_Cheques.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Ope_Con_Cheques.P_IVA = Total_Iva_por_Acreditar;
                Rs_Ope_Con_Cheques.P_Cmmd =Cmmd;
                Pago = Rs_Ope_Con_Cheques.Alta_Pago();
                Resultados = Pago.Substring(6, 2);
                Pago = Pago.Substring(0, 5);
                if (Resultados == "SI")
                {
                    //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
                else
                {
                    break;
                }
                
            }
            if (P_Cmmd == null)
            {
                Trans.Commit();
            }
            return Resultados;
        }
        catch (SqlException Ex)
        {
            if (P_Cmmd == null)
            {
                Trans.Rollback();
            }
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        catch (DBConcurrencyException Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
        }
        catch (Exception Ex)
        {
            if (Trans != null)
            {
                Trans.Rollback();
            }
            throw new Exception("Error: " + Ex.Message);
        }
        finally
        {
            if (P_Cmmd == null)
            {
                Cn.Close();
            }

        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private DataTable Comprimir_Poliza(DataTable Dt_partidas)
    {
        DataTable Dt_Temporal = new DataTable();
        Int16 Cont_Partidas = 0;
        DataRow Fila;
        Int16 Partida = 1;
        Boolean Insertar_Registro = true;
        if (Dt_Temporal.Rows.Count <= 0 && Dt_Temporal.Columns.Count <= 0)
        {
            //Agrega los campos que va a contener el DataTable de los detalles de la póliza
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Decimal));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Decimal));
            Dt_Temporal.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
            Dt_Temporal.Columns.Add("Beneficiario_ID", typeof(System.String));
            Dt_Temporal.Columns.Add("Tipo_Beneficiario", typeof(System.String));
        }
        foreach (DataRow Row in Dt_partidas.Rows)
        {
            if (Cont_Partidas == 0)
            {
                Fila = Dt_Temporal.NewRow();
                //Agrega el cargo del registro de la póliza
                Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString();
                Fila[Ope_Con_Polizas_Detalles.Campo_Referencia] = Row[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString();
                Fila["Beneficiario_ID"] = Row["Beneficiario_ID"].ToString();
                Fila["Tipo_Beneficiario"] = Row["Tipo_Beneficiario"].ToString();
                Dt_Temporal.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Temporal.AcceptChanges();
                Partida++;
                Cont_Partidas++;
            }
            else
            {
                Insertar_Registro = true;
                foreach (DataRow Fila_Temporal in Dt_Temporal.Rows)
                {
                    if (Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString() == Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString())
                    {
                        if (Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0 && Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) > 0)
                        {
                            Fila_Temporal.BeginEdit();
                            Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) + Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
                            Fila_Temporal.EndEdit();
                            Fila_Temporal.AcceptChanges();
                            Insertar_Registro = false;
                        }
                        else
                        {
                            if (Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0 && Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) > 0)
                            {
                                Fila_Temporal.BeginEdit();
                                Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDecimal(Fila_Temporal[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()) + Convert.ToDecimal(Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
                                Fila_Temporal.EndEdit();
                                Fila_Temporal.AcceptChanges();
                                Insertar_Registro = false;
                            }
                        }
                    }
                }
                if (Insertar_Registro)
                {
                    Fila = Dt_Temporal.NewRow();
                    //Agrega el cargo del registro de la póliza
                    Fila[Ope_Con_Polizas_Detalles.Campo_Partida] = Partida;
                    Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Debe] = Row[Ope_Con_Polizas_Detalles.Campo_Debe].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Haber] = Row[Ope_Con_Polizas_Detalles.Campo_Haber].ToString();
                    Fila[Ope_Con_Polizas_Detalles.Campo_Referencia] = Row[Ope_Con_Polizas_Detalles.Campo_Referencia].ToString();
                    Fila["Beneficiario_ID"] = Row["Beneficiario_ID"].ToString();
                    Fila["Tipo_Beneficiario"] = Row["Tipo_Beneficiario"].ToString();
                    Dt_Temporal.Rows.Add(Fila); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Temporal.AcceptChanges();
                    Partida++;
                }
            }
        }
        return Dt_Temporal;
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    #endregion
    #region "Eventos"
    /////*******************************************************************************
    /////NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    /////DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    /////PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    /////                             para mostrar los datos al usuario
    /////CREO       : Hugo Enrique Ramírez Aguilera
    /////FECHA_CREO  : 21-Febrero-2012
    /////MODIFICO          :
    /////FECHA_MODIFICO    :
    /////CAUSA_MODIFICACIÓN:
    /////******************************************************************************
    //private void Abrir_Ventana(String Nombre_Archivo)
    //{
    //    String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
    //    try
    //    {
    //        Pagina = Pagina + Nombre_Archivo;
    //        ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
    //        "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
    //    }
    //    catch (Exception ex)
    //    {
    //        throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
    //    }
    //}
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        Inicializa_Controles();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Autozizar_onclick
    ///DESCRIPCIÓN: Autoriza las transferencias de las solicitudes
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Sergio Manuel Gallardo
    ///FECHA_CREO  : 21-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Autozizar_onclick(object sender, EventArgs e)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
        Cls_Cat_Nom_Bancos_Negocio Rs_Ope_Con_Banco = new Cls_Cat_Nom_Bancos_Negocio(); //Variable de conexion con la capa de datos
        DataTable Dt_Consulta_Estatus = new DataTable();
        DataTable Dt_Solicitudes_Datos = new DataTable();
        DataTable Dt_Solicitudes_Modificada = new DataTable();
        Boolean Autorizado;
        DataTable Dt_Banco_Cuenta_Contable=new DataTable();
        String Cuenta_Proveedor_Bancario="";
        String Resultado = "";
        Int32 Indice = 0;
        //String Ref_Comision = "";
        //String Ref_Iva_Comision = "";
        try
        {
            DataTable Dt_Grid = (DataTable)(Grid_Pagos.DataSource);
            if (Cmb_Banco.SelectedValue != "0" && Txt_Comision_Banco.Text != "" && Txt_Iva_Comision.Text != "")
            {
                if (Convert.ToDecimal(Txt_Comision_Banco.Text) > 0 && Convert.ToDecimal(Txt_Iva_Comision.Text) > 0)
                {
                    Dt_Banco_Cuenta_Contable = new DataTable();
                    Rs_Ope_Con_Banco.P_Nombre = Cmb_Banco.SelectedValue.ToString();
                    Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Banco.Consulta_Bancos();
                    if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                    {
                        Cuenta_Proveedor_Bancario = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_Proveedor].ToString();
                    }
                    if (!String.IsNullOrEmpty(Cuenta_Proveedor_Bancario))
                    {
                        if (Dt_Solicitudes_Datos.Rows.Count <= 0)
                        {
                            Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Referencia", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Banco", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Proveedor", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Cuenta", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Fecha_Poliza", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Ref_Comision", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Ref_IVA", typeof(System.String));
                            Dt_Solicitudes_Datos.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                        }
                        foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
                        {
                            GridView Grid_Cuentas = (GridView)Renglon_Grid.FindControl("Grid_Cuentas");

                            foreach (GridViewRow Renglon in Grid_Cuentas.Rows)
                            {
                                GridView Grid_Datos_Solicitud = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                                foreach (GridViewRow fila in Grid_Datos_Solicitud.Rows)
                                {
                                    Indice++;
                                    Grid_Datos_Solicitud.SelectedIndex = Indice;
                                    Autorizado = ((CheckBox)fila.FindControl("Chk_Autorizado")).Checked;
                                    if (Autorizado && !String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                                    {
                                        if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                                        {
                                            if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                                            {
                                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                                //Asigna los valores al nuevo registro creado a la tabla
                                                row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Autorizado")).CssClass;
                                                row["Referencia"] = ((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString();
                                                row["Banco"] = fila.Cells[5].Text;
                                                row["cuenta"] = Renglon.Cells[1].Text;
                                                row["Proveedor"] = Renglon_Grid.Cells[1].Text;
                                                row["Fecha_Poliza"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)Renglon_Grid.FindControl("Txt_Fecha_Poliza")).Text));
                                                row["Ref_Comision"] = ((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_Comision")).Text.ToString();
                                                row["Ref_IVA"] = ((TextBox)Renglon.Cells[7].FindControl("Txt_Referencia_IVA_Comision")).Text.ToString();
                                                row["Tipo_Beneficiario"] = Renglon_Grid.Cells[6].Text;
                                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                                Dt_Solicitudes_Datos.AcceptChanges();
                                            }
                                        }
                                        else
                                        {
                                            if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                                            {
                                                DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                                //Asigna los valores al nuevo registro creado a la tabla
                                                row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Autorizado")).CssClass;
                                                row["Referencia"] = ((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString();
                                                row["Banco"] = fila.Cells[5].Text;
                                                row["cuenta"] = Renglon.Cells[1].Text;
                                                row["Proveedor"] = Renglon_Grid.Cells[1].Text;
                                                row["Fecha_Poliza"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)Renglon_Grid.FindControl("Txt_Fecha_Poliza")).Text));
                                                row["Ref_Comision"] = ((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_Comision")).Text.ToString();
                                                row["Ref_IVA"] = ((TextBox)Renglon.Cells[7].FindControl("Txt_Referencia_IVA_Comision")).Text.ToString();
                                                row["Tipo_Beneficiario"] = Renglon_Grid.Cells[6].Text;
                                                Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                                Dt_Solicitudes_Datos.AcceptChanges();
                                            }
                                        }
                                    }
                                }
                            }
                        }

                        if (Dt_Solicitudes_Modificada.Rows.Count <= 0)
                        {
                            Dt_Solicitudes_Modificada.Columns.Add("No_Solicitud", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Referencia", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Banco", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Proveedor", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Cuenta", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Monto", typeof(System.Double));
                            Dt_Solicitudes_Modificada.Columns.Add("Reserva", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Tipo_Solicitud_Pago", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Fecha_Poliza", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Ref_Comision", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Ref_IVA", typeof(System.String));
                            Dt_Solicitudes_Modificada.Columns.Add("Tipo_Beneficiario", typeof(System.String));
                        }
                        if (Dt_Solicitudes_Datos.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Solicitudes_Datos.Rows)
                            {
                                Rs_Solicitud_Pago.P_No_Solicitud_Pago = Registro["No_Solicitud"].ToString();
                                Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                                if (Dt_Consulta_Estatus.Rows.Count > 0)
                                {
                                    foreach (DataRow fila in Dt_Consulta_Estatus.Rows)
                                    {
                                        if (fila["Estatus"].ToString() == "EJERCIDO")
                                        {
                                            DataRow row = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                                            //Asigna los valores al nuevo registro creado a la tabla
                                            row["No_Solicitud"] = Registro["No_Solicitud"].ToString();
                                            row["Referencia"] = Registro["Referencia"].ToString();
                                            row["Banco"] = Registro["Banco"].ToString();
                                            row["cuenta"] = Registro["cuenta"].ToString();
                                            row["Proveedor"] = Registro["Proveedor"].ToString();
                                            row["Monto"] = fila["Monto"].ToString();
                                            row["Reserva"] = fila["No_Reserva"].ToString();
                                            row["Tipo_Solicitud_Pago"] = fila["Tipo_Solicitud_Pago_ID"].ToString();
                                            row["Fecha_Poliza"] = Registro["Fecha_Poliza"].ToString();
                                            row["Ref_Comision"] = Registro["Ref_Comision"].ToString();
                                            row["Ref_IVA"] = Registro["Ref_IVA"].ToString();
                                            row["Tipo_Beneficiario"] = Registro["Tipo_Beneficiario"].ToString();
                                            Dt_Solicitudes_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Solicitudes_Modificada.AcceptChanges();
                                        }
                                    }
                                }
                            }
                            if (Dt_Solicitudes_Modificada.Rows.Count > 0)
                            {
                                SqlConnection Cn = new SqlConnection();
                                SqlCommand Cmmd = new SqlCommand();
                                SqlTransaction Trans = null;

                                // crear transaccion para crear el convenio 
                                Cn.ConnectionString = JAPAMI.Constantes.Cls_Constantes.Str_Conexion;
                                Cn.Open();
                                Trans = Cn.BeginTransaction();
                                Cmmd.Connection = Cn;
                                Cmmd.Transaction = Trans;
                                try
                                {
                                    Resultado = Alta_Transferencia(Dt_Solicitudes_Modificada, Cmmd);
                                    if (Resultado == "SI")
                                    {
                                        //Trans.Rollback();
                                         Trans.Commit();
                                        ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                                    }
                                    else
                                    {
                                        Trans.Rollback();
                                        Lbl_Mensaje_Error.Text = "Error:";
                                        Lbl_Mensaje_Error.Text = "No se tiene suficiencia presupuestal para realizar el pago de la solicitud";
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                    }
                                }
                                catch (SqlException Ex)
                                {
                                    Trans.Rollback();
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                }
                                catch (Exception Ex)
                                {
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                }
                                finally
                                {
                                    Cn.Close();
                                }
                            }
                        }
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "El Banco no tiene cuenta contable como proveedor favor de revisarlo con contabilidad";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Text = "La comision y/o el Iva de la comision no pueden ser cero , Favor de verificarlo";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Debes tener ingresado la comision del banco y el iva de la comision para poder continuar";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }


    protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
    {

        DataTable Dt_Modificada = new DataTable();
        Boolean Agregar;
        Double Total;
        String Proveedor_ID;
        String Empleado_ID = "";
        String Cuenta;
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        DataTable Dt_Resultado = new DataTable();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consulta_Ejercidos.P_No_solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));

                Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    //Consultamos el banco con la primera traferencia
                    Cmb_Banco.SelectedValue = Dt_Resultado.Rows[0]["Nombre_Banco"].ToString();
                    Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                    Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                        }
                    }
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        if (!String.IsNullOrEmpty(Fila["EMPLEADO_ID"].ToString()))
                        {
                            Empleado_ID = Fila["EMPLEADO_ID"].ToString();
                            Proveedor_ID = "";
                        }
                        else
                        {
                            Proveedor_ID = Fila["Proveedor_id"].ToString();
                            Empleado_ID = "";
                        }
                        Cuenta = Fila["CUENTA_BANCO_PAGO_ID"].ToString();
                        if (Dt_Modificada.Rows.Count <= 0)
                        {
                            Dt_Modificada.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                            Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                            Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                            Dt_Modificada.Columns.Add("TIPO_BENEFICIARIO", typeof(System.String));
                        }
                        DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        Agregar = true;
                        Total = 0;
                        //insertar los que no se repiten
                        foreach (DataRow Fila2 in Dt_Modificada.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()))
                                {
                                    Agregar = false;
                                }
                            }
                            if (Proveedor_ID.Equals(Fila2["Proveedor_ID"].ToString()))
                            {
                                Agregar = false;
                            }
                        }
                        //se calcula el monto por tipo
                        foreach (DataRow Renglon in Dt_Resultado.Rows)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                if (Empleado_ID.Equals(Renglon["Empleado_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                            else
                            {
                                if (Proveedor_ID.Equals(Renglon["Proveedor_ID"].ToString()))
                                {
                                    Total = Total + Convert.ToDouble(Renglon["MONTO"].ToString());
                                }
                            }
                        }
                        if (Agregar && Total > 0)
                        {
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["PROVEEDOR_ID"] = Empleado_ID;
                                row["TIPO_BENEFICIARIO"] = "EMPLEADO";
                            }
                            else
                            {
                                row["PROVEEDOR_ID"] = Proveedor_ID;
                                row["TIPO_BENEFICIARIO"] = "PROVEEDOR";
                            }
                            row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                            row["MONTO"] = Total;
                            row["CUENTA"] = Fila["CUENTA_BANCO_PAGO_ID"].ToString().Trim();
                            row["ESTATUS"] = Fila["Estatus"].ToString();
                            if (!String.IsNullOrEmpty(Empleado_ID))
                            {
                                row["IDENTIFICADOR_TIPO"] = "E-" + Empleado_ID + Total;
                            }
                            else
                            {
                                row["IDENTIFICADOR_TIPO"] = "P-" + Proveedor_ID + Total;
                            } 
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                    }
                    Session["Dt_Datos"] = Dt_Resultado;
                    Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                    Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                else
                {
                    Grid_Pagos.DataSource = new DataTable();
                    Grid_Pagos.DataBind();
                }
            }
            else
            {
                Llenar_Grid_Solicitudes_Pago();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Chk_Referenciar_Todos_CheckedChanged(object sender, EventArgs e)
    {
        //Declaracion de variables
        int Cont_Elementos = 0; //variable para el contador
        TextBox Txt_Beneficiario_Grid_src; //Texbox referenciado del grid
        int Cont_Referencias = 0; //variable para el contador de las referencias
        GridView Grid_Cuentas_src; //Grid que contiene los elementos a referenciar

        try
        {
            //Ciclo para el barrido de los elementos del grid principal
            for (Cont_Elementos = 0; Cont_Elementos < Grid_Pagos.Rows.Count; Cont_Elementos++)
            {
                //Instanciar el grid de las cuentas
                Grid_Cuentas_src = ((GridView)Grid_Pagos.Rows[Cont_Elementos].Cells[5].FindControl("Grid_Cuentas"));

                //Ciclo para el grid de las cuentas
                for (Cont_Referencias = 0; Cont_Referencias < Grid_Cuentas_src.Rows.Count; Cont_Referencias++)
                {
                    //Instanciar la caja de texto de la referencia
                    Txt_Beneficiario_Grid_src = ((TextBox)Grid_Cuentas_src.Rows[Cont_Referencias].FindControl("Txt_Beneficiario_Grid"));

                    //verificar si el check esta seleccionado
                    if (Chk_Referenciar_Todos.Checked == true)
                    {
                        Txt_Beneficiario_Grid_src.Text = Txt_Referenciar_Todos.Text.Trim();
                    }
                    else
                    {
                        Txt_Beneficiario_Grid_src.Text = "";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
        }
    }
    protected void Chk_Ref_Comisionar_Todos_CheckedChanged(object sender, EventArgs e)
    {
        //Declaracion de variables
        int Cont_Elementos = 0; //variable para el contador
        TextBox Txt_Referencia_Comision_src; //Texbox referenciado del grid
        int Cont_Referencias = 0; //variable para el contador de las referencias
        GridView Grid_Cuentas_src; //Grid que contiene los elementos a referenciar

        try
        {
            //Ciclo para el barrido de los elementos del grid principal
            for (Cont_Elementos = 0; Cont_Elementos < Grid_Pagos.Rows.Count; Cont_Elementos++)
            {
                //Instanciar el grid de las cuentas
                Grid_Cuentas_src = ((GridView)Grid_Pagos.Rows[Cont_Elementos].Cells[5].FindControl("Grid_Cuentas"));

                //Ciclo para el grid de las cuentas
                for (Cont_Referencias = 0; Cont_Referencias < Grid_Cuentas_src.Rows.Count; Cont_Referencias++)
                {
                    //Instanciar la caja de texto de la referencia
                    Txt_Referencia_Comision_src = ((TextBox)Grid_Cuentas_src.Rows[Cont_Referencias].FindControl("Txt_Referencia_Comision"));

                    //verificar si el check esta seleccionado
                    //if (Chk_Ref_Comisionar_Todos.Checked == true)
                    //{
                    //    Txt_Referencia_Comision_src.Text = Txt_Ref_Comisionar_Todos.Text.Trim();
                    //}
                    //else
                    //{
                        Txt_Referencia_Comision_src.Text = "";
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
        }
    }
    protected void Chk_IVA_Todos_CheckedChanged(object sender, EventArgs e)
    {
        //Declaracion de variables
        int Cont_Elementos = 0; //variable para el contador
        TextBox Txt_Referencia_IVA_Comision_src; //Texbox referenciado del grid
        int Cont_Referencias = 0; //variable para el contador de las referencias
        GridView Grid_Cuentas_src; //Grid que contiene los elementos a referenciar

        try
        {
            //Ciclo para el barrido de los elementos del grid principal
            for (Cont_Elementos = 0; Cont_Elementos < Grid_Pagos.Rows.Count; Cont_Elementos++)
            {
                //Instanciar el grid de las cuentas
                Grid_Cuentas_src = ((GridView)Grid_Pagos.Rows[Cont_Elementos].Cells[5].FindControl("Grid_Cuentas"));

                //Ciclo para el grid de las cuentas
                for (Cont_Referencias = 0; Cont_Referencias < Grid_Cuentas_src.Rows.Count; Cont_Referencias++)
                {
                    //Instanciar la caja de texto de la referencia
                    Txt_Referencia_IVA_Comision_src = ((TextBox)Grid_Cuentas_src.Rows[Cont_Referencias].FindControl("Txt_Referencia_IVA_Comision"));

                    //verificar si el check esta seleccionado
                    //if (Chk_IVA_Todos.Checked == true)
                    //{
                    //    Txt_Referencia_IVA_Comision_src.Text = Txt_IVA_Todos.Text.Trim();
                    //}
                    //else
                    //{
                        Txt_Referencia_IVA_Comision_src.Text = "";
                    //}
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            //Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Accion"]))
                {
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

    #endregion

    #region"Grid"
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        HyperLink Hyp_Lnk_Ruta;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
    ///PARAMETROS           :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 22/Diciembre/2011
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN   :
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
        //DataTable Dt_Detalles = new DataTable();
        //DataTable Dt_Documentos = new DataTable();

        //try
        //{
        //    if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
        //    {
        //        Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[1].Text.Trim();
        //        Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
        //        Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

        //        if (Dt_Detalles != null)
        //        {
        //            if (Dt_Detalles.Rows.Count > 0)
        //            {
        //                Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
        //                Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
        //                Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
        //                Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
        //                Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
        //                Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
        //                Grid_Solicitud_Pagos.SelectedIndex = -1;

        //                Grid_Documentos.Columns[3].Visible = true;
        //                Grid_Documentos.Columns[4].Visible = true;
        //                Grid_Documentos.DataSource = Dt_Documentos;
        //                Grid_Documentos.DataBind();
        //                Grid_Documentos.Columns[3].Visible = false;
        //                Grid_Documentos.Columns[4].Visible = false;

        //                Mpe_Detalles.Show();
        //            }
        //        }
        //    }
        //}
        //catch (Exception Ex)
        //{
        //    throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
        //}
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
    /// DESCRIPCION : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 09/enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((System.Web.UI.WebControls.TextBox)e.Row.Cells[7].FindControl("Txt_Beneficiario_Grid")).ToolTip =  e.Row.Cells[0].Text;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
    ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO  : 21-mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Solicitud_Click(object sender, EventArgs e)
    {
        String No_Solicitud = ((LinkButton)sender).Text;
        Imprimir(No_Solicitud);
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Imprimir
    ///DESCRIPCIÓN: Imprime la solicitud
    ///PROPIEDADES:     
    ///CREO: Sergio Manuel Gallardo
    ///FECHA_CREO: 06/Enero/2012 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Imprimir(String Numero_Solicitud)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        try
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
            Ds_Reporte = new DataSet();
            Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
            Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            Lbl_Mensaje_Error.Visible = true;
        }
    }
    #endregion
    #region Metodos Reportes
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }


    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }

    #endregion
}
