﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Polizas_JQuery.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Polizas_JQuery" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <!--Referencias JQuery-EasyUI-->
    <link href="../../javascript/jquery-easyiu-1.3.1/themes/default/easyui.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/jquery-easyiu-1.3.1/themes/icon.css" rel="stylesheet" type="text/css" />
    <script src="../../javascript/jquery-easyiu-1.3.1/jquery.easyui.min.js" type="text/javascript"></script>

    <!--Referencias del autocompletado-->
    <link href="../../javascript/autocompletar/jquery.autocomplete.css" rel="stylesheet" type="text/css" />
    <link href="../../javascript/autocompletar/lib/thickbox.css" rel="stylesheet" type="text/css" />
    <script src="../../javascript/autocompletar/lib/jquery.bgiframe.min.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/jquery.ajaxQueue.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/lib/thickbox-compressed.js" type="text/javascript"></script>
    <script src="../../javascript/autocompletar/jquery.autocomplete.js" type="text/javascript"></script>

    <!--Referencia Parser de JSON-->
    <script src="../../javascript/validacion/json_parse.js" type="text/javascript"></script>
    <script src="../../javascript/validacion/json.js" type="text/javascript"></script>

    <!--auxiliar-->
    <script src="../../javascript/js_Funciones_Generales.js" type="text/javascript"></script>

    <!--Metodos y eventos del cliente-->
    <script src="../../javascript/Contabilidad/Js_Ope_Con_Polizas.js" type="text/javascript"></script>

    <div id="Div_Polizas" style="background-color:#ffffff; width:98%; height:100%;">
        <table width="100%" class="estilo_fuente">
            <tr align="center">
                <td class="label_titulo">P&oacute;lizas</td>
            </tr>
            <tr>
                <td>
                    <div id="Div_Error">
                        <img src="../imagenes/paginas/sias_warning.png" alt="Mensaje" id="Img_Error" />&nbsp;
                        <span class="estilo_fuente_mensaje_error" id="Lbl_Error"></span>
                    </div>
                </td>
            </tr>
            <tr align="center">
                <td>
                    <div align="left" class="barra_busqueda" id="Div_Controles">
                        <a id="Img_Btn_Guardar" href="#" class="easyui-linkbutton" iconCls="icon-save" title="Guardar" plain="true"></a>
                        <a id="Img_Btn_Salir" href="#" class="easyui-linkbutton" iconCls="icon-back" title="Salir" plain="true"></a>
                    </div>
                </td>
            </tr>
        </table>
        <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de la P&oacute;liza" Width="98%" BackColor="white">
            <table width="100%" class="estilo_fuente">
                <tr>
                    <td style="width:70px;">*Tipo P&oacute;liza</td>
                    <td style="width:200px;"><select id="Cmb_Tipo_Poliza" class="easyui-combobox" style="width:200px;"></select></td>
                    <td style="width:70px;">No P&oacute;liza</td>
                    <td style="width:100px;"><input type="text" id="Txt_No_Poliza" disabled="disabled" style="width:100px;" /></td>
                </tr>
                <tr>
                    <td style="width:70px;">Prefijo</td>
                    <td style="width:100px;"><input type="text" id="Txt_Prefijo" disabled="disabled" style="width:100px;" /></td>
                    <td style="width:70px;">*Fecha</td>
                    <td style="width:200px;"><input type="text" id="Dtp_Fecha_Poliza" style="width:100px;" /></td>
                </tr>
                <tr>
                    <td style="width:70px;">*Concepto</td>
                    <td colspan="3"><textarea id="Txt_Concepto_Poliza" cols="100" rows="3"></textarea></td>
                </tr>
                <tr>
                    <td style="width:70px;">*F. Financiamiento</td>
                    <td colspan="3"><select id="Cmb_Fuentes_Financiamiento" class="easyui-combobox" style="width:370px;"></select></td>
                </tr>
            </table>
        </asp:Panel>
        <asp:Panel ID="Pnl_Partidas_Polizas" runat="server" GroupingText="Partidas Contables" Width="98%" BackColor="White">
            <div id="Div_Momento" runat="server">
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td style="width:15%;">Momento Inicial</td>
                        <td style="width:35%;"><select id="Cmb_Momento_Inicial" class="easyui-combobox"></select></td>
                        <td style="width:15%;">Momento Final</td>
                        <td style="width:35%;"><select id="Cmb_Momento_Final" class="easyui-combobox"></select></td>
                    </tr>
                </table>
            </div>
            <table width="100%" class="estilo_fuente">
                <tr>
                    <td width="43%">Descripci&oacute;n</td>
                    <td width="14%">Cuenta</td>
                    <td width="21%">Concepto</td>
                    <td width="9%">Debe</td>
                    <td width="9%">Haber</td>
                    <td width="4%"></td>
                </tr>
                <tr>
                    <td><input type="text" id="Txt_Caja_Busqueda" class="validacion" tipo="caracteres" /></td>
                    <td><input type="text" id="Txt_Cuenta_Contable" disabled="disabled" /></td>
                    <td><input type="text" id="Txt_Concepto_Partida" /></td>
                    <td><input type="text" id="Txt_Debe_Partida" class="caja_texto_monto" tipo="debe" /></td>
                    <td><input type="text" id="Txt_Haber_Partida" class="caja_texto_monto" tipo="haber" /></td>
                    <td><a href="#" id="Img_Btn_Agregar_Partida" class="easyui-linkbutton" iconCls="icon-add" title="Agregar" plain="true"></a></td>
                </tr>
                <tr>
                    <td align="left" colspan="6">
                        <div id="Div_Partidas">
                            <table id="Grid_Partidas"></table>                        
                        </div>
                    </td>
                </tr>
                <tr>
                    <td align="left">No. Partidas</td>
                    <td align="left"><input type="text" id="Txt_No_Partidas" disabled="disabled" /></td>
                    <td align="left">Acumulado</td>
                    <td align="left"><input type="text" id="Txt_Total_Debe" disabled="disabled" class="caja_texto_monto_final"/></td>
                    <td align="left"><input type="text" id="Txt_Total_Haber" disabled="disabled" class="caja_texto_monto_final"/></td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td align="left" colspan="2">
                        <asp:Panel ID="Pnl_Empleado_Elabora" runat="server" GroupingText="Empleado Elabora" Width="98%" BackColor="White">
                            <input type="text" id="Txt_Empleado_Creo" disabled="disabled" />
                        </asp:Panel>
                    </td>
                    <td align="left" colspan="4">
                        <asp:Panel ID="Pnl_Empleado_Autoriza" runat="server" GroupingText="Empleado Autoriza" Width="100%" BackColor="White">
                            <input type="text" id="Txt_Empleado_Autorizo" style="width:100px;" />
                            <select id="Cmb_Nombre_Empleado" class="easyui-combobox"></select>
                        </asp:Panel>
                    </td>
                </tr>
                <tr>
                    <td colspan="6">
                        <input type="hidden" id="Txt_Partidas" />
                        <input type="hidden" id="Txt_Cuenta_Contable_ID" />
                    </td>
                </tr>
            </table>
        </asp:Panel>
    </div>
</asp:Content>

