﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Contabilidad_Transferencia.Negocio;
public partial class paginas_Contabilidad_Frm_Rpt_Con_Transferencias_Fecha : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
               // Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    DateTime _DateTime = DateTime.Now;
                    Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                Cls_Ope_Con_Transferencia_Negocio Rs_Orden = new Cls_Ope_Con_Transferencia_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    Limpia_Controles(); //limpia los campos de la forma
                    Rdb_Tipo.SelectedIndex = 0;
                    Rs_Orden.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    Dt_Existencia = Rs_Orden.Consultar_orden_Transferencia();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Orden, Dt_Existencia, "TRANSFERENCIA", "TRANSFERENCIA");
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Cmb_Orden.SelectedIndex=-1;
                    Txt_Fecha_Inicio.Enabled = false;
                    Session["Dt_Datos_Detalles"] = null;
                    Session["Contador"] = null;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
            ///               generación del reporte
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 08-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Reporte()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (Cmb_Orden.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+Debes Seleccionar el úmero de orden para poder realizar el reporte<br>";
                        Datos_Validos = false;
                    }
                return Datos_Validos;
            }
        #endregion
        #region (Consultas)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Transferencias_Realizadas
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15-mayo-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Transferencias_Realizadas()
            {
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Transferencias_Realizadas" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Transferencias = new DataTable();       //Va a conter los valores de la consulta realizada
                Ds_Rpt_Con_Transferencias Ds_Transferencias_realizadas = new Ds_Rpt_Con_Transferencias();
                Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Transferencias = new Cls_Ope_Con_Transferencia_Negocio(); //Conexion hacia la capa de negocios
                Double Tipo_02 = 0;
                Double Tipo_04 = 0;
                Double Monto_02 = 0;
                Double Monto_04 = 0;
                try
                {
                    if (Cmb_Orden.SelectedIndex > 0)
                    {
                        Rs_Consulta_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Dt_Transferencias = Rs_Consulta_Transferencias.Consultar_Transferencias_Detalles(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    foreach (DataRow Fila in Dt_Transferencias.Rows)
                    {
                        if (Fila["BANCO"].ToString().Equals(Fila["BANCO_TRANSFERENCIA"].ToString()))
                        {
                            Tipo_02 = Tipo_02 + 1;
                            Monto_02 = Monto_02 + Convert.ToDouble(Fila["IMPORTE"].ToString());
                        }else{
                            Tipo_04 = Tipo_04 + 1;
                            Monto_04 = Monto_04 + Convert.ToDouble(Fila["IMPORTE"].ToString());
                        }
                    }
                    if (Dt_Transferencias.Rows.Count > 0)
                    {
                        Dt_Transferencias.TableName = "DT_Transferencias";
                        Ds_Transferencias_realizadas.Clear();
                        Ds_Transferencias_realizadas.Tables.Clear();
                        Ds_Transferencias_realizadas.Tables.Add(Dt_Transferencias.Copy());
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Transferencias.rpt");
                    Reporte.SetDataSource(Ds_Transferencias_realizadas);

                    ParameterFieldDefinitions Cr_Parametros;
                    ParameterFieldDefinition Cr_Tipo_02;
                    ParameterFieldDefinition Cr_Tipo_04;
                    ParameterFieldDefinition Cr_Monto_02;
                    ParameterFieldDefinition Cr_Monto_04;
                    ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    Cr_Tipo_02 = Cr_Parametros["CONTADOR_TIPO_02"];
                    Cr_Valor_Parametro = Cr_Tipo_02.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Tipo_02;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Tipo_02.ApplyCurrentValues(Cr_Valor_Parametro);

                    Cr_Tipo_04 = Cr_Parametros["CONTADOR_TIPO_04"];
                    Cr_Valor_Parametro = Cr_Tipo_04.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Tipo_04;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Tipo_04.ApplyCurrentValues(Cr_Valor_Parametro);

                    Cr_Monto_02 = Cr_Parametros["MONTO_TIPO02"];
                    Cr_Valor_Parametro = Cr_Monto_02.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Monto_02;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Monto_02.ApplyCurrentValues(Cr_Valor_Parametro);

                    Cr_Monto_04 = Cr_Parametros["MONTO_TIPO04"];
                    Cr_Valor_Parametro = Cr_Monto_04.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = Monto_04;
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Monto_04.ApplyCurrentValues(Cr_Valor_Parametro);

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Transferencias_Realizadas_por_Tipo
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 20-junio-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Transferencias_Realizadas_por_Tipo()
            {
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Transferencias" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Transferencias = new DataTable();       //Va a conter los valores de la consulta realizada
                Ds_Rpt_Con_Transferencias Ds_Transferencias_realizadas = new Ds_Rpt_Con_Transferencias();
                Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Transferencias = new Cls_Ope_Con_Transferencia_Negocio(); //Conexion hacia la capa de negocios
                
                try
                {
                    if (Cmb_Orden.SelectedIndex > 0)
                    {
                        Rs_Consulta_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Transferencias.P_Tipo_Filtro = "2";
                    Rs_Consulta_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Dt_Transferencias = Rs_Consulta_Transferencias.Consultar_Transferencias_Detalles(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Transferencias.Rows.Count > 0)
                    {
                        Dt_Transferencias.TableName = "Dt_Transferencia_Tipo";
                        Ds_Transferencias_realizadas.Clear();
                        Ds_Transferencias_realizadas.Tables.Clear();
                        Ds_Transferencias_realizadas.Tables.Add(Dt_Transferencias.Copy());
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Transfeencias_Tipo.rpt");
                    Reporte.SetDataSource(Ds_Transferencias_realizadas);

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Reporte_Transferencia " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Mostrar_Reporte
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 24-Mayo-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Mostrar_Reporte()
            {
                DataTable Dt_Transferencias= new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Cheques_Diarios = new DataTable(); //Obtiene los valores a pasar al reporte
                Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Transferencias = new Cls_Ope_Con_Transferencia_Negocio(); //Conexion hacia la capa de negocios
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                try
                {
                    if (Cmb_Orden.SelectedIndex > 0)
                    {
                        Rs_Consulta_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Transferencias.P_Tipo_Filtro = "1";
                    Rs_Consulta_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Dt_Transferencias = Rs_Consulta_Transferencias.Consultar_Transferencias_Por_Orden(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Transferencias.Rows.Count > 0)
                    {
                        Session["Dt_Datos_Detalles"] = Dt_Transferencias;
                        Grid_Cuentas_Movimientos.DataSource = Dt_Transferencias;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Cheques.Visible = true;
                        Tr_Grid_Bancos.Style.Add("display","block");
                        DIV_BANCO.Style.Add("display", "block");
                        Div_Encabezado_Banco.Style.Add("display", "block");
                        Div_Encabezado_Tipo_Pago.Style.Add("display", "none");
                        DIV_TIPO_PAGO.Style.Add("display", "none");
                    }
                    else
                    {
                        Grid_Cuentas_Movimientos.DataSource = null;
                        Grid_Cuentas_Movimientos.DataBind();
                        Btn_Reporte_Cheques.Visible = false;
                        Tr_Grid_Bancos.Style.Add("display", "none");
                        Lbl_Mensaje_Error.Text="No tiene Movimientos en el rango de fechas que coloco <br>";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Div_Encabezado_Tipo_Pago.Style.Add("display", "none");
                        DIV_TIPO_PAGO.Style.Add("display", "none");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Mostrar_Reporte_Por_Tipo
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 20-Junio-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Mostrar_Reporte_Por_Tipo()
            {
                DataTable Dt_Transferencias = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Cheques_Diarios = new DataTable(); //Obtiene los valores a pasar al reporte
                Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Transferencias = new Cls_Ope_Con_Transferencia_Negocio(); //Conexion hacia la capa de negocios
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                DataTable Dt_Datos_Detalles = new DataTable();
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                int Contador = 0;
                Session["Contador"] = Contador;
                try
                {
                    if (Cmb_Orden.SelectedIndex > 0)
                    {
                        Rs_Consulta_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Transferencias.P_Tipo_Filtro = "2";
                    Rs_Consulta_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Dt_Transferencias = Rs_Consulta_Transferencias.Consultar_Transferencias_Por_Orden(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Transferencias.Rows.Count > 0)
                    {
                        Session["Dt_Datos_Detalles"] = Dt_Transferencias;
                        Grid_Tipos_Pago.DataSource = Dt_Transferencias;
                        Grid_Tipos_Pago.DataBind();
                        Btn_Reporte_Cheques.Visible = true;
                        Tr_Grid_Bancos.Style.Add("display", "block");
                        DIV_BANCO.Style.Add("display", "none");
                        Div_Encabezado_Banco.Style.Add("display", "none");
                        Div_Encabezado_Tipo_Pago.Style.Add("display", "block");
                        DIV_TIPO_PAGO.Style.Add("display", "block");
                    }
                    else
                    {
                        Grid_Tipos_Pago.DataSource = null;
                        Grid_Tipos_Pago.DataBind();
                        Btn_Reporte_Cheques.Visible = false;
                        Tr_Grid_Bancos.Style.Add("display", "none");
                        Lbl_Mensaje_Error.Text = "No tiene Movimientos en el rango de fechas que coloco <br>";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        DIV_BANCO.Style.Add("display", "none");
                        Div_Encabezado_Banco.Style.Add("display", "none");
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 16/julio/2012 
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Ope_Con_Transferencia_Negocio Rs_Transferencias= new Cls_Ope_Con_Transferencia_Negocio();
                String Banco = "";
                int Contador;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        Banco = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["BANCO"].ToString());
                        Rs_Transferencias.P_Banco = Banco;
                        if (Cmb_Orden.SelectedIndex > 0)
                        {
                            Rs_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                        }
                        Rs_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                        Ds_Consulta = Rs_Transferencias.Consultar_Transferencias_Detalles();
                        if (Ds_Consulta.Rows.Count > 0)
                        {
                            Gv_Detalles = (GridView)e.Row.Cells[2].FindControl("Grid_Movimientos");
                            Gv_Detalles.DataSource = Ds_Consulta;
                            Gv_Detalles.DataBind();
                            Session["Contador"] = Contador + 1;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            //'****************************************************************************************
            //'NOMBRE DE LA FUNCION: Grid_Tipos_Pago_RowDataBound
            //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            //'PARAMETROS  : 
            //'CREO        : Sergio Manuel Gallardo
            //'FECHA_CREO  : 20/julio/2012
            //'MODIFICO          :
            //'FECHA_MODIFICO    :
            //'CAUSA_MODIFICACION:
            //'****************************************************************************************
            protected void Grid_Tipos_Pago_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                GridView Gv_Detalles = new GridView();
                DataTable Dt_Datos_Detalles = new DataTable();
                DataTable Ds_Consulta = new DataTable();
                DataTable Ds_Consulta_Modificada = new DataTable();
                Cls_Ope_Con_Transferencia_Negocio Rs_Transferencias = new Cls_Ope_Con_Transferencia_Negocio();
                String Tipo_Pago = "";
                int Contador;
                Image Img = new Image();
                Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Tipos");
                Literal Lit = new Literal();
                Lit = (Literal)e.Row.FindControl("Ltr_Inicio_Tipos");
                Label Lbl_facturas = new Label();
                Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos_Tipos");
                try
                {
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {
                        Contador = (int)(Session["Contador"]);
                        Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                        Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                        + (Lbl_facturas.Text + ("\',\'"
                                        + (Img.ClientID + "\')")))));
                        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                        Tipo_Pago = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["Descripcion"].ToString());
                        if (Cmb_Orden.SelectedIndex > 0)
                        {
                            Rs_Transferencias.P_Orden = Cmb_Orden.SelectedItem.Text.ToString();
                        }
                        Rs_Transferencias.P_Tipo_Pago = Tipo_Pago;
                        Rs_Transferencias.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                        Ds_Consulta = Rs_Transferencias.Consultar_Transferencias_Detalles();
                        if (Ds_Consulta.Rows.Count > 0)
                        {
                            Gv_Detalles = (GridView)e.Row.Cells[2].FindControl("Grid_Por_Tipo_Pago");
                            Gv_Detalles.DataSource = Ds_Consulta;
                            Gv_Detalles.DataBind();
                            Session["Contador"] = Contador + 1;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Consultar_Clic
            /// DESCRIPCION : consulta la cuenta contable
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andradde
            /// FECHA_CREO  : 08-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Consultar_Clic(object sender, EventArgs e)
            {
                Cls_Ope_Con_Transferencia_Negocio Rs_Orden = new Cls_Ope_Con_Transferencia_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    //Cmb_Orden.DataSource = null;
                    Cmb_Orden.Items.Clear();
                    Rs_Orden.P_Fecha_Transferencia = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    Dt_Existencia = Rs_Orden.Consultar_orden_Transferencia();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Orden, Dt_Existencia, "TRANSFERENCIA", "TRANSFERENCIA");
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }

    protected void Btn_Reporte_Cheques_Emitidos_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                if (Rdb_Tipo.SelectedIndex == 0)
                {
                    Consulta_Transferencias_Realizadas(); //Consulta los cheques emitidos por fecha
                }
                else
                {
                    Consulta_Transferencias_Realizadas_por_Tipo(); //Consulta los cheques emitidos por fecha
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Btn_Nuevo_Click
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    /// PARÁMETROS:
    /// USUARIO CREO:       Sergio Manuel Gallardo Andrade
    /// FECHA CREO:         24/Mayo/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                if (Rdb_Tipo.SelectedIndex == 0)
                {
                    Mostrar_Reporte(); //Consulta el libro de mayor de la cuenta seleccionada
                }
                else
                {
                    Mostrar_Reporte_Por_Tipo();
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
}
