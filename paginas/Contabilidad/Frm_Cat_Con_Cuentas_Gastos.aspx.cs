﻿using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Cuentas_Gastos.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;

public partial class paginas_Contabilidad_Frm_Cat_Con_Cuentas_Gastos : System.Web.UI.Page
{
    #region(Init/Load)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Page_Load
    ///DESCRIPCIÓN: Page_Load
    ///CREO:  
    ///FECHA_CREO: 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {

        Lbl_Mensaje_Error.Text = ""; Lbl_Mensaje_Error.Visible = false;
        if (String.IsNullOrEmpty(Cls_Sessiones.Nombre_Empleado)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
        try
        {
            if (!IsPostBack)
            {
                Configuracion_Inicial();
            }
            Lbl_Mensaje_Error.Text = "";
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = Ex.Message;
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
    }
    #endregion

    #region(Métodos)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Configuracion_Inicial
    ///DESCRIPCIÓN: Configuracion Inicial del Catalogo de Bancos
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 17/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    private void Configuracion_Inicial()
    {
        Habilitar_Controles("Inicial");
        Limpiar_Controles();
        Consulta_Gastos();
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : sergio manuel gallardo
    /// FECHA_CREO  : 26- agosto-2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        try
        {
            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
            Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            if (Hdf_Partida_ID.Value == "")
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Debes seleccionar una partida. <br />";
                Datos_Validos = false;
            }
            else {
                if (Session["Dt_Documentos"] != null)
                {
                    DataTable Dt_Documentos = (DataTable)Session["Dt_Documentos"];
                    DataRow[] Filas = Dt_Documentos.Select("Partida_ID ='" + Hdf_Partida_ID.Value + "'");
                    if (Filas.Length > 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Partida ya esta agregada. <br />";
                        Datos_Validos = false;
                    }
                }
            }            
            return Datos_Validos;
        }
        catch (Exception ex)
        {
            throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
        }
    }
        
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Ctlr
    /// DESCRIPCION : Limpia los Controles de la pagina.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 17/Febrero/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        Txt_Concepto.Text = "";
        Txt_Descripcion.Text = "";
        Txt_Concepto.Text = "";
        Cmb_Estatus.SelectedIndex = 0;
        Hdf_Partida_ID.Value = "";
        Txt_Clave_Partida.Text = "";
        Txt_Nombre_Partida.Text = "";
        Grid_Partidas.DataSource = null;
        Grid_Partidas.DataBind();
        if (Session["Dt_Documentos"] != null)
        {
            Session.Remove("Dt_Documentos");
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 17/Febrero/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado;

        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Salir.ToolTip = "Inicio";
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    //Campo de Validacion
                    Lbl_Mensaje_Error.Text = "";
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Pnl_Partidas_Agregadas.Visible = false;
                    Grid_Partidas.Columns[4].Visible = false;
                    break;
                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Grid_Partidas.Columns[4].Visible = true;
                    break;
                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    Grid_Partidas.Columns[4].Visible = true;
                    break;
            }
            Txt_Descripcion.Enabled = Habilitado;
            Txt_Concepto.Enabled = Habilitado;
            Cmb_Estatus.Enabled = Habilitado;
            Txt_Clave_Partida.Enabled = Habilitado;
            Btn_Buscar_Partida.Enabled = Habilitado;
            Btn_Agregar_Documento.Enabled = Habilitado;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 17/Febrero/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        if (string.IsNullOrEmpty(Txt_Descripcion.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + La descripcion es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if (string.IsNullOrEmpty(Txt_Concepto.Text))
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + El Concepto es un dato requerido por el sistema. <br>";
            Datos_Validos = false;
        }

        if (Cmb_Estatus.SelectedIndex < 1)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Seleccione el estatus. <br>";
            Datos_Validos = false;
        }
        if (Grid_Partidas.Rows.Count <= 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Debes agregar por lo menos una cuenta. <br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Gasto
    /// DESCRIPCION : Ejecuta el alta de un Gasto.
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 10/Agosto/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Gasto()
    {
        Cls_Cat_Con_Cuentas_Gastos_Negocio Alta_Gasto = new Cls_Cat_Con_Cuentas_Gastos_Negocio();//Variable de conexion con la capa de negocios.
        DataTable Dt_Partidas = new DataTable();
        try
        {
            //Alta_Gasto.P_Partida_ID = Hdf_Partida_ID.Value;
            Dt_Partidas = (DataTable)Session["Dt_Documentos"];
            Alta_Gasto.P_Dt_Detalles = Dt_Partidas;
            Alta_Gasto.P_Descripcion = Txt_Descripcion.Text.ToUpper();
            Alta_Gasto.P_Estatus = Cmb_Estatus.SelectedValue;
            Alta_Gasto.P_Concepto = Txt_Concepto.Text.ToUpper();
            Alta_Gasto.P_Usuario_Creo = HttpUtility.HtmlDecode((String)Cls_Sessiones.Nombre_Empleado);

            if (Alta_Gasto.Alta_Gasto())
            {
                Configuracion_Inicial();
                Limpiar_Controles();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Operación Exitosa');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Verificar. [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Gasto
    /// DESCRIPCION : Ejecuta la modificar.
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 10/Agosto/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Gasto()
    {
        Cls_Cat_Con_Cuentas_Gastos_Negocio Modifica_Gasto = new Cls_Cat_Con_Cuentas_Gastos_Negocio();//Variable de conexion con la capa de negocios.
        DataTable Dt_Partidas = new DataTable();
        try
        {
           // Modifica_Gasto.P_Partida_ID = Hdf_Partida_ID.Value;
            Dt_Partidas = (DataTable)Session["Dt_Documentos"];
            Modifica_Gasto.P_Dt_Detalles = Dt_Partidas;
            Modifica_Gasto.P_Descripcion = Txt_Descripcion.Text.ToUpper();
            Modifica_Gasto.P_Estatus = Cmb_Estatus.SelectedValue;
            Modifica_Gasto.P_Concepto = Txt_Concepto.Text.ToUpper();
            Modifica_Gasto.P_Gasto_Id = Grid_Gastos.DataKeys[Grid_Gastos.SelectedRow.RowIndex].Value.ToString();


            if (Modifica_Gasto.Actualiza_Gasto())
            {
                Configuracion_Inicial();
                Limpiar_Controles();
                ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "", "alert('Operación Exitosa');", true);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Verificar. [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Gastos
    /// DESCRIPCION : Ejecuta el alta de un Banco.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 17/Febrero/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Gastos()
    {
        Cls_Cat_Con_Cuentas_Gastos_Negocio Gasto = new Cls_Cat_Con_Cuentas_Gastos_Negocio();//Variable de conexion con la capa de negocios.
        DataTable Dt_Gastos = new DataTable();
        try
        {
            Grid_Gastos.SelectedIndex = (-1);
            Dt_Gastos = Gasto.Consultar_Gastos();
            Grid_Gastos.Columns[1].Visible = true;
            Grid_Gastos.Columns[2].Visible = true;
            Grid_Gastos.DataSource = Dt_Gastos;
            Grid_Gastos.DataBind();
            Grid_Gastos.Columns[1].Visible = false;
            Grid_Gastos.Columns[2].Visible = false;
        }
        catch (Exception Ex)
        {
            throw new Exception("Verificar. [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Partida
    /// DESCRIPCION : Cargar_Partida
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 17/Febrero/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Partida(String Clave, String Tipo)
    {
        Cls_Cat_SAP_Partidas_Especificas_Negocio Partida_Negocio = new Cls_Cat_SAP_Partidas_Especificas_Negocio();
        if (Tipo.Equals("IDENTIFICADOR")) Partida_Negocio.P_Partida_ID = Clave;
        if (Tipo.Equals("CLAVE")) Partida_Negocio.P_Clave = Clave;
        DataTable Dt_Partida = Partida_Negocio.Consulta_Partida_Especifica();
        if (Dt_Partida != null)
        {
            if (Dt_Partida.Rows.Count > 0)
            {
                Hdf_Partida_ID.Value = Dt_Partida.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Partida_ID].ToString().Trim();
                Txt_Clave_Partida.Text = Dt_Partida.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Clave].ToString().Trim();
                Txt_Nombre_Partida.Text = Dt_Partida.Rows[0][Cat_Sap_Partidas_Especificas.Campo_Nombre].ToString().Trim();
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Verificar: La partida no existe.";
                Lbl_Mensaje_Error.Visible = true;
            }
        }
        else
        {
            Lbl_Mensaje_Error.Text = "Verificar: La partida no existe.";
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    #endregion

    #region (Eventos)
    ///*********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Agregar_Documento_Click
    ///DESCRIPCIÓN          : Agregar Facturas a la reserva
    ///PROPIEDADES          :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 26/Agosto/2013
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Agregar_Documento_Click(object sender, ImageClickEventArgs e)
    {
        //Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
        //Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
        DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documento = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
        //string Codigo_Programatico = "";
        String Espacios = "";
        String extension = String.Empty;
        int error = 0;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        String aux = String.Empty; //variable auxiliar
        if (Validar())
        {
            Pnl_Partidas_Agregadas.Visible = true;
            if (Session["Dt_Documentos"] == null)
            {
                //Agrega los campos que va a contener el DataTable
                Dt_Documento.Columns.Add("Partida_ID", typeof(System.String));
                Dt_Documento.Columns.Add("Descripcion", typeof(System.String));
                Dt_Documento.Columns.Add("Partida", typeof(System.String));
                Dt_Documento.Columns.Add("Clave", typeof(System.String));
            }
            else
            {
                Dt_Documento = (DataTable)Session["Dt_Documentos"];
                Session.Remove("Dt_Documentos");
            }
            DataRow row = Dt_Documento.NewRow(); //Crea un nuevo registro a la tabla
            //Asigna los valores al nuevo registro creado a la tabla
            row["Partida_ID"] = Hdf_Partida_ID.Value;
            row["Descripcion"] = Txt_Nombre_Partida.Text;
            row["Clave"] = Txt_Clave_Partida.Text;
            row["Partida"] = Convert.ToString(Convert.ToDecimal(Dt_Documento.Rows.Count) + 1) + Hdf_Partida_ID.Value;
            Dt_Documento.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Documento.AcceptChanges();
            Session["Dt_Documentos"] = Dt_Documento;//Agrega los valores del registro a la sesión
            Grid_Partidas.Columns[0].Visible = true;
            Grid_Partidas.Columns[1].Visible = true;
            Grid_Partidas.DataSource = Dt_Documento; //Agrega los valores de todas las partidas que se tienen al grid
            Grid_Partidas.DataBind();
            Grid_Partidas.Columns[0].Visible = false;
            Grid_Partidas.Columns[1].Visible = false;
            Txt_Nombre_Partida.Text = "";
            Txt_Clave_Partida.Text = "";
            Hdf_Partida_ID.Value = "";
        }
        //Indica al usuario que datos son los que falta por proporcionar para poder agregar la partida a la poliza
        else
        {
            //error = 1;
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Verificar: <br />";
            if (String.IsNullOrEmpty(Hdf_Partida_ID.Value))
            {
                Lbl_Mensaje_Error.Text += Espacios + " + Falta seleccionar la partida <br />";
            }
            else
            {
                if (Session["Dt_Documentos"] != null)
                {
                    DataTable Dt_Documentos = (DataTable)Session["Dt_Documentos"];
                    DataRow[] Filas = Dt_Documentos.Select("Partida_ID ='" + Hdf_Partida_ID.Value + "'");
                    if (Filas.Length > 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios + " + La Partida ya esta agregada. <br />";
                    }
                }
            }      
        }
        if (error == 1)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Eliminar_Partida
    /// DESCRIPCION : Eliminar la partida de un grid 
    /// CREO        : sergio manuel gallardo
    /// FECHA_CREO  : 26- agosto-2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Eliminar_Partida(object sender, EventArgs e)
    {
        ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
        DataTable Dt_Partidas = (DataTable)Session["Dt_Documentos"];
        DataRow[] Filas = Dt_Partidas.Select("Partida_ID" +
                "='" + Btn_Eliminar_Partida.CommandArgument + "'");
        if (!(Filas == null))
        {
            if (Filas.Length > 0)
            {
                Dt_Partidas.Rows.Remove(Filas[0]);
                Session["Dt_Documentos"] = Dt_Partidas;
                Grid_Partidas.Columns[0].Visible = true;
                Grid_Partidas.Columns[1].Visible = true;
                Grid_Partidas.DataSource = Session["Dt_Documentos"]; ; //Agrega los valores de todas las partidas que se tienen al grid
                Grid_Partidas.DataBind();
                Grid_Partidas.Columns[0].Visible = false;
                Grid_Partidas.Columns[1].Visible = false;

            }
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Nuevo_Click
    ///DESCRIPCIÓN: Alta Banco
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 17/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip.Equals("Nuevo"))
            {
                Habilitar_Controles("Nuevo");
                Limpiar_Controles();
                Cmb_Estatus.SelectedIndex = Cmb_Estatus.Items.IndexOf(Cmb_Estatus.Items.FindByValue("ACTIVA"));
                Grid_Gastos.SelectedIndex = (-1);
            }
            else
            {
                if (Validar_Datos())
                {
                    Alta_Gasto();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
            //ScriptManager.RegisterStartupScript(UPnl_Bancos, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Bancos();", true);
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir de la Operacion Actual
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 17/Febrero/2011 
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Inicio")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                Session.Remove("Dt_Gastos");
            }
            else
            {
                Configuracion_Inicial();//Habilita los controles para la siguiente operación del usuario en el catálogo
                Session.Remove("Dt_Gastos");
            }
            //ScriptManager.RegisterStartupScript(UPnl_Bancos, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Bancos();", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
 
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: Modificar Banco
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 17/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            if (Btn_Modificar.ToolTip.Equals("Modificar"))
            {
                if (Grid_Gastos.SelectedIndex != -1)
                {
                    Habilitar_Controles("Modificar");
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione el registro que desea modificar sus datos <br>";
                }
            }
            else
            {
                if (Validar_Datos())
                {
                    Modificar_Gasto();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_Partida_Click
    ///DESCRIPCIÓN: Modificar Banco
    ///CREO: Juan alberto Hernández Negrete
    ///FECHA_CREO: 17/Febrero/2011
    ///MODIFICO:
    ///FECHA_MODIFICO
    ///CAUSA_MODIFICACIÓN
    ///*******************************************************************************
    protected void Btn_Buscar_Partida_Click(object sender, ImageClickEventArgs e)
    {
        if (Txt_Clave_Partida.Text.Trim().Length == 4) {
            Cargar_Partida(Txt_Clave_Partida.Text.Trim(), "CLAVE");
        } else {
            Lbl_Mensaje_Error.Text = "Verificar. La partida debe contar con 4 números en la Clave.";
            Lbl_Mensaje_Error.Visible = true;
        }
    }

    #endregion

    #region Grid
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
    ///               para identicar la fila seleccionada de tabla.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 26/Agosto/2013
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Partidas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((ImageButton)e.Row.FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[0].Text.Trim();
                ((ImageButton)e.Row.FindControl("Btn_Eliminar")).ToolTip = "Quitar la partida " + e.Row.Cells[2].Text;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Grid_Gastos_SelectedIndexChanged
    ///DESCRIPCIÓN          : Evento del grid que pone visible el boton de modificar
    ///PARAMETROS: 
    ///CREO                 : Armando Zavala Moreno
    ///FECHA_CREO           : 10/08/2012 10:34:00 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Gastos_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Btn_Modificar.Visible = true;
            DataTable Dt_Detalles_Gasto = new DataTable();
            DataTable Dt_Documento = new DataTable();
            String Gasto_Seleccionado = HttpUtility.HtmlDecode(Grid_Gastos.SelectedRow.Cells[1].Text.Trim()).Trim();
            Cls_Cat_Con_Cuentas_Gastos_Negocio CG_Negocio = new Cls_Cat_Con_Cuentas_Gastos_Negocio();
            CG_Negocio.P_Gasto_Id = Gasto_Seleccionado;
            DataTable Dt_Gastos = CG_Negocio.Consultar_Gastos();
            if (Dt_Gastos != null) {
                if (Dt_Gastos.Rows.Count > 0) {
                    Session["Dt_Documentos"] = null;
                    Txt_Descripcion.Text = Dt_Gastos.Rows[0]["DESCRIPCION"].ToString().Trim();
                    Cmb_Estatus.SelectedValue = Dt_Gastos.Rows[0]["ESTATUS"].ToString().Trim();
                    Txt_Concepto.Text = Dt_Gastos.Rows[0]["CONCEPTO"].ToString().Trim();
                    //Cargar_Partida(Dt_Gastos.Rows[0]["PARTIDA_ID"].ToString().Trim(), "IDENTIFICADOR");
                    CG_Negocio.P_Gasto_Id = Gasto_Seleccionado;
                    Dt_Detalles_Gasto=CG_Negocio.Consultar_Gastos_Detalle();
                    //se llenan los detalles 
                    foreach (DataRow Fila in Dt_Detalles_Gasto.Rows)
                    {
                        Pnl_Partidas_Agregadas.Visible = true;
                        if (Session["Dt_Documentos"] == null)
                        {
                            //Agrega los campos que va a contener el DataTable
                            Dt_Documento.Columns.Add("Partida_ID", typeof(System.String));
                            Dt_Documento.Columns.Add("Descripcion", typeof(System.String));
                            Dt_Documento.Columns.Add("Partida", typeof(System.String));
                            Dt_Documento.Columns.Add("Clave", typeof(System.String));
                        }
                        else
                        {
                            Dt_Documento = (DataTable)Session["Dt_Documentos"];
                            Session.Remove("Dt_Documentos");
                        }
                        DataRow row = Dt_Documento.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["Partida_ID"] = Fila[Cat_Con_Cuentas_Gastos_Detalles.Campo_Partida_ID].ToString();
                        row["Descripcion"] = Fila["PARTIDA"].ToString();
                        row["Partida"] = Convert.ToString(Convert.ToDecimal(Dt_Documento.Rows.Count) + 1) + Fila[Cat_Con_Cuentas_Gastos_Detalles.Campo_Partida_ID].ToString();
                        row["Clave"] = Fila["CLAVE"].ToString();
                        Dt_Documento.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Documento.AcceptChanges();
                        Session["Dt_Documentos"] = Dt_Documento;//Agrega los valores del registro a la sesión
                        Grid_Partidas.Columns[0].Visible = true;
                        Grid_Partidas.Columns[1].Visible = true;
                        Grid_Partidas.DataSource = Dt_Documento; //Agrega los valores de todas las partidas que se tienen al grid
                        Grid_Partidas.DataBind();
                        Grid_Partidas.Columns[0].Visible = false;
                        Grid_Partidas.Columns[1].Visible = false;
                    }
                }
            }            
        }
        catch (Exception Ex)
        {
            throw new Exception("Error producido al dar de Alta Antiguedad Sindicato. Error: [" + Ex.Message + "]");
        }
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Grid_Gastos_PageIndexChanging
    ///DESCRIPCIÓN          : Maneja la paginación del GridView de las Cuentas Omitidas
    ///PARAMETROS: 
    ///CREO                 : Armando Zavala Moreno
    ///FECHA_CREO           : 10/08/2012 10:34:00 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Grid_Gastos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Grid_Gastos.PageIndex = e.NewPageIndex;
            Consulta_Gastos();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al cambiar de pagina. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}
