﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Reporte_Proveedores_Pagos.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Proveedores_Pagos : System.Web.UI.Page
{
    #region (Page load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                //Cmb_Proveedor.SelectedIndex=0;//Limpia los controles de la forma
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha_Inicial.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    #endregion
    #region (Metodos)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mensaje_Error
    /// DESCRIPCION : Funcion para lanzar un mensaje de error
    /// PARAMETROS  : Mensaje: Cadena de caracteres que se va a desplegar como mensaje de error
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 18-Marzo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje)
    {
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text = Mensaje;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mensaje_Error
    /// DESCRIPCION : Funcion limpiar el mensaje de error
    /// PARAMETROS  : 
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 18-Marzo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mensaje_Error()
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Valida las fechas
    /// PARAMETROS  : 
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 19-Abril-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    { 
        Boolean Datos_Correctos=true;
        DateTime Fecha_Ini;
        DateTime Fecha_Final;
        Fecha_Ini = Convert.ToDateTime(Txt_Fecha_Inicial.Text);
        Fecha_Final = Convert.ToDateTime(Txt_Fecha_Final.Text);
        if (Fecha_Ini > Fecha_Final)
        {
            Datos_Correctos = false;
        }        
        return Datos_Correctos;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Crear_Tabla_Proveedores
    ///DESCRIPCIÓN          : Devuelve un DataTable con la estructura para las cuentas del proveedor
    ///PARAMETROS: 
    ///CREO                 : Armando Zavala Moreno
    ///FECHA_CREO           : 03/02/2012 05:10:00 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Crear_Tabla_Proveedores()
    {
        DataTable Dt_Generadas = new DataTable();
        Dt_Generadas.Columns.Add(new DataColumn("FECHA_DE_PAGO", typeof(DateTime)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_MAYOR", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("NO_FACTURA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("FECHA_AUTORIZA_RECHAZA_EJD", typeof(DateTime)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("NO_SOLICITUD_PAGO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CONCEPTO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("USUARIO_CREO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("BENEFICIARIO", typeof(String)));
        return Dt_Generadas;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Crear_Tabla_Proveedores_Modificado
    ///DESCRIPCIÓN          : Devuelve un DataTable con la estructura para las cuentas del proveedor
    ///PARAMETROS: 
    ///CREO                 : Armando Zavala Moreno
    ///FECHA_CREO           : 03/02/2012 05:10:00 p.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected DataTable Crear_Tabla_Proveedores_Modificado()
    {
        DataTable Dt_Generadas = new DataTable();
        Dt_Generadas.Columns.Add(new DataColumn("FECHA_DE_PAGO", typeof(DateTime)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_ACREEDOR", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_DEUDOR", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_PROVEEDOR", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_NOMINA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_PREDIAL", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_CONTRATISTA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA_JUDICIAL", typeof(String)));        
        Dt_Generadas.Columns.Add(new DataColumn("NO_FACTURA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("FECHA_AUTORIZA_RECHAZA_EJD", typeof(DateTime)));
        Dt_Generadas.Columns.Add(new DataColumn("CUENTA", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("NO_SOLICITUD_PAGO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("CONCEPTO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("USUARIO_CREO", typeof(String)));
        Dt_Generadas.Columns.Add(new DataColumn("BENEFICIARIO", typeof(String)));
        return Dt_Generadas;
    }
    ///******************************************************************************* 
    ///NOMBRE DE LA FUNCIÓN : Llenar_DataRow_Determinadas
    ///DESCRIPCIÓN          : Agrega una nueva fila a las cuentas omitidas y Calcula el Perido, Rezago, etc
    ///PARAMETROS: 
    ///CREO                 : Armando Zavala Moreno
    ///FECHA_CREO           : 24/02/2012 11:49:00 a.m.
    ///MODIFICO: 
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Llenar_DataRow_Proveedores(DataTable Dt_Proveedores_Generados, DataTable Dt_Final,int Contador,String Cuenta_Mayor)
    {

        DataRow Dr_Generadas;
        Dr_Generadas = Dt_Final.NewRow();
        Dr_Generadas["FECHA_DE_PAGO"] = Dt_Proveedores_Generados.Rows[Contador]["FECHA_DE_PAGO"].ToString();
        Dr_Generadas["CUENTA_MAYOR"] = Cuenta_Mayor;
        Dr_Generadas["NO_FACTURA"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Con_Solicitud_Pagos.Campo_No_Factura].ToString();
        Dr_Generadas["FECHA_AUTORIZA_RECHAZA_EJD"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido].ToString();
        Dr_Generadas["CUENTA"] = Dt_Proveedores_Generados.Rows[Contador][Cat_Com_Proveedores.Campo_Cuenta].ToString();
        Dr_Generadas["NO_SOLICITUD_PAGO"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Con_Pagos.Campo_No_Solicitud_Pago].ToString();
        Dr_Generadas["CONCEPTO"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString();
        Dr_Generadas["USUARIO_CREO"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Con_Pagos.Campo_Usuario_Creo].ToString();
        Dr_Generadas["BENEFICIARIO"] = Dt_Proveedores_Generados.Rows[Contador][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
        Dt_Final.Rows.Add(Dr_Generadas);//Se asigna la nueva fila a la tabla        
    }

    /// ***********************************************************************************************
    /// NOMBRE: Consulta_Proveedores
    /// DESCRIPCIÓN: 
    /// PARÁMETROS: No Aplica.
    /// USUARIO CREÓ:Armando Zavala Moreno.
    /// FECHA CREÓ: 19/Abril/2012 10:30 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// ***********************************************************************************************
    protected System.Data.DataTable Consulta_Proveedores()
    {
        System.Data.DataTable Dt_Proveedores_Generados = null;
        DataTable Dt_Final = Crear_Tabla_Proveedores();
        DataTable Dt_Modificada = Crear_Tabla_Proveedores_Modificado();
        DataTable Dt_Auxiliar;
        DataRow Fila2;
        Cls_Rpt_Con_Proveedores_Pagos_Negocio obj_Proveedores = new Cls_Rpt_Con_Proveedores_Pagos_Negocio();
        String Tipo_Letra;
        String Cuenta_Mayor;
        String No_Docuento;
        String No_Solicitud_Pago;
        String Auxiliar;
        String No_Factura;
        try
        {
            if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex > 0) obj_Proveedores.P_Proveedor_ID = Cmb_Proveedor_Solicitud_Pago.SelectedValue.Trim();
            if (Txt_Fecha_Inicial.Text.Length > 0) obj_Proveedores.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
            if (Txt_Fecha_Final.Text.Length > 0) obj_Proveedores.P_Fecha_Final = Txt_Fecha_Final.Text;

            Dt_Proveedores_Generados = obj_Proveedores.Consulta_Proveedores_Pago();
            Dt_Auxiliar = Dt_Proveedores_Generados;
            if (Dt_Proveedores_Generados.Rows.Count > 0)
            {
                for (int Cont_tabla = 0; Cont_tabla < Dt_Proveedores_Generados.Rows.Count; Cont_tabla++)
                {
                    No_Solicitud_Pago = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Pagos.Campo_No_Solicitud_Pago].ToString();
                    No_Factura = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura].ToString();
                    for (int Cont_Interno = Cont_tabla + 1; Cont_Interno < Dt_Auxiliar.Rows.Count; Cont_Interno++)
                    {
                        Auxiliar = Dt_Auxiliar.Rows[Cont_Interno][Ope_Con_Pagos.Campo_No_Solicitud_Pago].ToString();
                        if (No_Solicitud_Pago == Auxiliar)
                        {
                            No_Factura += "-" + Dt_Auxiliar.Rows[Cont_Interno][Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura].ToString();
                            Cont_tabla++;
                        }
                    }
                    DataRow Dr_Generadas;
                    Dr_Generadas = Dt_Modificada.NewRow();
                    Dr_Generadas["FECHA_DE_PAGO"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["FECHA_DE_PAGO"].ToString();
                    Dr_Generadas["CUENTA_ACREEDOR"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_ACREEDOR"].ToString();
                    Dr_Generadas["CUENTA_DEUDOR"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_DEUDOR"].ToString();
                    Dr_Generadas["CUENTA_PROVEEDOR"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_PROVEEDOR"].ToString();
                    Dr_Generadas["CUENTA_NOMINA"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_NOMINA"].ToString();
                    Dr_Generadas["CUENTA_PREDIAL"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_PREDIAL"].ToString();
                    Dr_Generadas["CUENTA_CONTRATISTA"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_CONTRATISTA"].ToString();
                    Dr_Generadas["CUENTA_JUDICIAL"] = Dt_Proveedores_Generados.Rows[Cont_tabla]["CUENTA_JUDICIAL"].ToString();
                    Dr_Generadas["NO_FACTURA"] = No_Factura;
                    Dr_Generadas["FECHA_AUTORIZA_RECHAZA_EJD"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Solicitud_Pagos.Campo_Fecha_Autoriza_Rechaza_Ejercido].ToString();
                    Dr_Generadas["CUENTA"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Cat_Com_Proveedores.Campo_Cuenta].ToString();
                    Dr_Generadas["NO_SOLICITUD_PAGO"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Pagos.Campo_No_Solicitud_Pago].ToString();
                    Dr_Generadas["CONCEPTO"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString();
                    Dr_Generadas["USUARIO_CREO"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Con_Pagos.Campo_Usuario_Creo].ToString();
                    Dr_Generadas["BENEFICIARIO"] = Dt_Proveedores_Generados.Rows[Cont_tabla][Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                    Dt_Modificada.Rows.Add(Dr_Generadas);//Se asigna la nueva fila a la tabla                         
                }

                for (int Cont_tabla = 0; Cont_tabla < Dt_Modificada.Rows.Count; Cont_tabla++)
                {
                    Tipo_Letra = Dt_Modificada.Rows[Cont_tabla][Ope_Psp_Reservas.Campo_Beneficiario].ToString().Substring(0, 2);
                    switch (Tipo_Letra)
                    {
                        case "C-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_CONTRATISTA"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "D-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_DEUDOR"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "P-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_PROVEEDOR"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "N-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_NOMINA"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "Z-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_PREDIAL"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "A-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_ACREEDOR"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        case "J-":
                            Cuenta_Mayor = Dt_Modificada.Rows[Cont_tabla]["CUENTA_JUDICIAL"].ToString();
                            Llenar_DataRow_Proveedores(Dt_Modificada, Dt_Final, Cont_tabla, Cuenta_Mayor);
                            break;
                        default:
                            break;
                    }
                }
            }
            else
            {
                //MENSAJE DE PROVEEDOR NO TIENE PAGOS
                Mensaje_Error("El proveedor no tiene ningun pago");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al consultar los Proveedores. Error: [" + Ex.Message + "]");
        }
        return Dt_Final;
    }
    #region (Reportes)
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Exportar_Reporte
    /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
    /// PARÁMETROS:
    /// 		1. Ds_Reporte: Dataset con datos a imprimir
    /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
    /// 		3. Nombre_Archivo: Nombre del archivo a generar
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 04-sep-2011
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo, String Extension_Archivo, ExportFormatType Formato)
    {
        ReportDocument Reporte = new ReportDocument();
        String Ruta = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);

        try
        {
            Reporte.Load(Ruta);
            Reporte.SetDataSource(Ds_Reporte);
        }
        catch
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
        }

        String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
        try
        {
            ExportOptions Export_Options_Calculo = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
            Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
            Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
            Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options_Calculo.ExportFormatType = Formato;
            Reporte.Export(Export_Options_Calculo);

            if (Formato == ExportFormatType.Excel)
            {
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-excel");
            }
            else if (Formato == ExportFormatType.WordForWindows)
            {
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-word");
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Excel
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en excel.
    ///              
    /// PARÁMETROS: No Aplica
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 10/Diciembre/2011.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
    {
        try
        {
            System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
            if (ArchivoExcel.Exists)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = Contenido;
                Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.WriteFile(ArchivoExcel.FullName);
                Response.End();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en pantalla.
    ///              
    /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Empleados",
                "window.open('" + Pagina + "', 'Reporte_Proveedores_Pagos','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #endregion

    #region (Eventos)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Buscar_Proveedor_Solicitud_Pagos_Click
    /// DESCRIPCION : Consulta a todos los proveedores que coincidan con el nombre, rfc
    ///               o compañia de acuerdo a lo proporcionado por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Buscar_Proveedor_Solicitud_Pagos_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Proveedores;
        DataTable Dt_Proveedores_Tipo = new DataTable();
        DataRow Fila;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Nombre_Proveedor_Solicitud_Pago.Text))
            {
                Rs_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
                Rs_Consulta_Cat_Com_Proveedores.P_tipo = "CONTABILIDAD";
                Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                Dt_Proveedores_Tipo.Columns.Add("COMPANIA", typeof(System.String));
                Dt_Proveedores_Tipo.Columns.Add("PROVEEDOR_ID", typeof(System.String));
               
                Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores;
                Cmb_Proveedor_Solicitud_Pago.DataTextField = Cat_Com_Proveedores.Campo_Compañia;
                Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
            }
            else
            {
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    #endregion
    protected void Btn_Reporte_Analitico_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable Dt_Puestos = null;
        System.Data.DataSet Ds_Puestos = null;
        Mensaje_Error();

        try
        {
            if (Validar_Datos())
            {
                Ds_Puestos = new System.Data.DataSet();
                Dt_Puestos = Consulta_Proveedores();
                if (Dt_Puestos.Rows.Count > 0)
                {
                    Dt_Puestos.TableName = "Proveedores";
                    Ds_Puestos.Tables.Add(Dt_Puestos.Copy());

                    Generar_Reporte(ref Ds_Puestos, "Rpt_Con_Proveedor_Pagos.rpt", "Reporte_Proveedores" + Session.SessionID + ".pdf");
                }
                else
                {
                    Mensaje_Error("El proveedor no tiene datos para mostrar");
                }
            }
            else
            {
                Mensaje_Error("La Fecha Inicial no debe ser Mayor que la Fecha Final");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.ToString());
        }
    }
    protected void Btn_Excel_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable Dt_Puestos = null;
        System.Data.DataSet Ds_Puestos = null;
        Mensaje_Error();
        try
        {
            if (Validar_Datos())
            {
                Ds_Puestos = new System.Data.DataSet();
                Dt_Puestos = Consulta_Proveedores();
                if (Dt_Puestos.Rows.Count > 0)
                {
                    Dt_Puestos.TableName = "Proveedores";
                    Ds_Puestos.Tables.Add(Dt_Puestos.Copy());

                    Exportar_Reporte(Ds_Puestos, "Rpt_Con_Proveedor_Pagos.rpt", "Reporte_Proveedores" + Session.SessionID, "xls", ExportFormatType.Excel);
                }
                else
                {
                    Mensaje_Error("El proveedor no tiene datos para mostrar");
                }
            }
            else
            {
                Mensaje_Error("La Fecha Inicial no debe ser Mayor que la Fecha Final");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.ToString());
        }
    }
}
