﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Matriz.Negocio;

public partial class paginas_Contabilidad_Frm_Cat_Con_Matriz : System.Web.UI.Page
{
    #region PAGE LOAD
        ///*****************************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Page_Load
        ///DESCRIPCIÓN          : Inicio de la pagina
        ///PARAMETROS           : 
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 12/Enero/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*****************************************************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                if (!IsPostBack)
                {
                    ViewState["SortDirection"] = "ASC";
                    Matriz_Conversion_Inicio();
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error en el inicio del catalago de matriz. Error [" + Ex.Message + "]");
            }
        }
    #endregion

    #region METODOS
        #region METODOS GENERALES
            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Matriz_Conversion_Inicio
            ///DESCRIPCIÓN          : Metodo de inicio de la página
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            private void Matriz_Conversion_Inicio()
            {
                try
                {
                    Div_Contenedor_Msj_Error.Visible = false;
                    Limpiar_Controles();
                    Llenar_Combo_Tipos();
                    Habilitar_Forma(false);
                    Estado_Botones("inicial");
                    Llenar_Grid_Matriz();
                    Llenar_Combo_Tipo_Gasto();
                    Llenar_Combo_Nivel();
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error en el inicio de la matriz de conversión. Error [" + Ex.Message + "]");
                }
            }

            ///*****************************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Limpiar_Controles
            ///DESCRIPCIÓN          : Metodo para limpiar los controles del formulario
            ///PARAMETROS           : 
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*****************************************************************************************************************
            private void Limpiar_Controles()
            {
                try
                {
                    Div_Contenedor_Msj_Error.Visible = false;
                    Txt_No_Matriz.Text = "";
                    Txt_Nombre.Text = "";
                    Cmb_Tipo_Gasto.SelectedIndex = -1;
                    Cmb_Cuenta_Abono.SelectedIndex = -1;
                    Cmb_Cuenta_Cargo.SelectedIndex = -1; Cmb_Tipo.SelectedIndex = -1;
                    Lbl_Ecabezado_Mensaje.Text = "";
                    Lbl_Mensaje_Error.Text = "";
                    Hf_Matriz_Id.Value = "";
                    Hf_No_Matriz.Value = "";
                    Hf_Tipo_Matriz.Value = "";
                    Cmb_Nivel.SelectedIndex = -1;
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al limpiar los controles Error [" + Ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
            ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
            ///PROPIEDADES          1 Estatus true o false para habilitar los controles
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Habilitar_Forma(Boolean Estatus)
            {
                try
                {
                    Cmb_Tipo_Gasto.Enabled = Estatus;
                    Txt_Nombre.Enabled = Estatus;
                    Txt_No_Matriz.Enabled = Estatus;
                    Txt_Busqueda_Matriz.Enabled = !Estatus;
                    Cmb_Tipo.Enabled = Estatus;
                    Cmb_Cuenta_Abono.Enabled = Estatus;
                    Cmb_Cuenta_Cargo.Enabled = Estatus;
                    Grid_Matriz.Enabled = !Estatus;
                    Btn_Buscar_Cuenta.Enabled = !Estatus;
                    Cmb_Nivel.Enabled = Estatus;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Habilitar_Forma. Error[" + ex.Message + "]");
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Estado_Botones
            ///DESCRIPCIÓN          : metodo que muestra los botones de acuerdo al estado en el que se encuentre
            ///PARAMETROS:          1.- String Estado: El estado de los botones solo puede tomar 
            ///                        + inicial
            ///                        + nuevo
            ///                        + modificar
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN   :
            ///*******************************************************************************
            public void Estado_Botones(String Estado)
            {
                switch (Estado)
                {
                    case "inicial":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        //Boton Salir
                        Btn_Salir.ToolTip = "Inicio";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";

                        //Configuracion_Acceso("Frm_Cat_SAP_Area_Funcional.aspx");
                        break;
                    case "nuevo":
                        //Boton Nuevo
                        Btn_Nuevo.ToolTip = "Guardar";
                        Btn_Nuevo.Enabled = true;
                        Btn_Nuevo.Visible = true;
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        //Boton Modificar
                        Btn_Modificar.Visible = false;
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                    case "modificar":
                        //Boton Nuevo
                        Btn_Nuevo.Visible = false;
                        //Boton Modificar
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Modificar.Enabled = true;
                        Btn_Modificar.Visible = true;
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        //Boton Salir
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Salir.Enabled = true;
                        Btn_Salir.Visible = true;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        break;
                }//fin del switch
            }

            //********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Validar_Datos
            ///DESCRIPCIÓN          : Metodo para validar los datos necesarios para guardar los datos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private Boolean Validar_Datos()
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                Boolean Datos_Validos = true;
                Lbl_Ecabezado_Mensaje.Text = "Favor de:";
                Lbl_Mensaje_Error.Text = String.Empty;
                DataTable Dt_Matriz = new DataTable();

                try
                {
                    if (String.IsNullOrEmpty(Txt_No_Matriz.Text.Trim()))
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir el No. Matriz. <br />";
                        Datos_Validos = false;
                    }
                    else 
                    {
                        if (Btn_Modificar.Visible)
                        {
                            if (!Hf_No_Matriz.Value.Equals(Txt_No_Matriz.Text.Trim()))
                            {
                                if (Cmb_Tipo.SelectedIndex > 0)
                                {
                                    Matriz_Negocio.P_No_Matriz = Txt_No_Matriz.Text.Trim();
                                    Matriz_Negocio.P_Tipo_Matriz = Cmb_Tipo.SelectedItem.Value.Trim();
                                    Dt_Matriz = Matriz_Negocio.Consulta_Matriz();
                                    if (Dt_Matriz != null && Dt_Matriz.Rows.Count > 0)
                                    {
                                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir una No Matriz diferente. <br />";
                                        Datos_Validos = false;
                                    }
                                }
                            }
                        }
                        else
                        {
                            if (Cmb_Tipo.SelectedIndex > 0)
                            {
                                Matriz_Negocio.P_No_Matriz = Txt_No_Matriz.Text.Trim();
                                Matriz_Negocio.P_Tipo_Matriz = Cmb_Tipo.SelectedItem.Value.Trim();
                                Dt_Matriz = Matriz_Negocio.Consulta_Matriz();
                                if (Dt_Matriz != null && Dt_Matriz.Rows.Count > 0)
                                {
                                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir una Matriz ID diferente. <br />";
                                    Datos_Validos = false;
                                }
                            }
                        }
                    }
                    if (Cmb_Tipo.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un Tipo. <br />";
                        Datos_Validos = false;
                    }
                    else 
                    {
                        if (Btn_Modificar.Visible)
                        {
                            if (!Cmb_Tipo.SelectedItem.Value.Trim().Equals(Hf_Tipo_Matriz.Value.Trim()))
                            {
                                if (!String.IsNullOrEmpty(Txt_No_Matriz.Text.Trim()))
                                {
                                    Matriz_Negocio.P_No_Matriz = Txt_No_Matriz.Text.Trim();
                                    Matriz_Negocio.P_Tipo_Matriz = Cmb_Tipo.SelectedItem.Value.Trim();
                                    Dt_Matriz = Matriz_Negocio.Consulta_Matriz();
                                    if (Dt_Matriz != null && Dt_Matriz.Rows.Count > 0)
                                    {
                                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un Tipo diferente. <br />";
                                        Datos_Validos = false;
                                    }
                                }
                            }
                        }

                        if (Cmb_Tipo.SelectedItem.Value.Trim().Equals("MD") || Cmb_Tipo.SelectedItem.Value.Trim().Equals("MPG"))
                        {
                            if (Cmb_Tipo_Gasto.SelectedIndex <= 0)
                            {
                                Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar el Tipo de Gasto. <br />";
                                Datos_Validos = false;
                            }
                        }
                    }
                    if (String.IsNullOrEmpty(Txt_Nombre.Text.Trim()))
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Introducir un Nombre. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Cuenta_Cargo.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una Cuenta Cargo. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Nivel.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar un nivel. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Cuenta_Abono.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; - Seleccionar una Cuenta Abono. <br />";
                        Datos_Validos = false;
                    }
                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de validar los datos Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Grid_Matriz
            ///DESCRIPCIÓN          : Metodo para llenar el grid de la matriz
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Grid_Matriz()
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                DataTable Dt_Matriz = new DataTable();
                
                try
                {
                    Dt_Matriz = Matriz_Negocio.Consulta_Matriz();
                    if (Dt_Matriz != null)
                    {
                        if (Dt_Matriz.Rows.Count > 0)
                        {
                            Grid_Matriz.Columns[8].Visible = true;
                            Grid_Matriz.Columns[9].Visible = true;
                            Grid_Matriz.Columns[10].Visible = true;
                            Grid_Matriz.DataSource = Dt_Matriz;
                            Grid_Matriz.DataBind();
                            Grid_Matriz.Columns[8].Visible = false;
                            Grid_Matriz.Columns[9].Visible = false;
                            Grid_Matriz.Columns[10].Visible = false;
                        }
                        else
                        {
                            Grid_Matriz.DataSource = Dt_Matriz;
                            Grid_Matriz.DataBind();
                        }
                    }
                    else
                    {
                        Grid_Matriz.DataSource = Dt_Matriz;
                        Grid_Matriz.DataBind();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de llenar la tabla de las partidas asignadas Error[" + ex.Message + "]");
                }
            }
        #endregion

        #region METODOS COMBOS
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipos
            ///DESCRIPCIÓN          : Metodo para llenar el combo de tipos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Tipos()
            {
                try
                {
                    Cmb_Tipo.Items.Clear();
                    Cmb_Tipo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Tipo.Items.Insert(1, new ListItem("MD - Matriz Devengado de Gasto", "MD"));
                    Cmb_Tipo.Items.Insert(2, new ListItem("MPG - Matriz Pagado de Gasto", "MPG"));
                    Cmb_Tipo.Items.Insert(3, new ListItem("MID - Matriz Ingresos Devengados", "MID"));
                    Cmb_Tipo.Items.Insert(4, new ListItem("MIR - Matriz Ingresos Recaudado", "MIR"));
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Partidas ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Tipo_Gasto
            ///DESCRIPCIÓN          : Metodo para llenar el combo de tipos de gastos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Tipo_Gasto()
            {
                try
                {
                    Cmb_Tipo_Gasto.Items.Clear();
                    Cmb_Tipo_Gasto.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Tipo_Gasto.Items.Insert(1, new ListItem("1", "1"));
                    Cmb_Tipo_Gasto.Items.Insert(2, new ListItem("2", "2"));
                    Cmb_Tipo_Gasto.Items.Insert(3, new ListItem("1-2", "1-2"));
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Tipo_Gasto ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combo_Nivel
            ///DESCRIPCIÓN          : Metodo para llenar el combo del nivel al que se filtraran la cuenta cargo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combo_Nivel()
            {
                try
                {
                    Cmb_Nivel.Items.Clear();
                    Cmb_Nivel.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                    Cmb_Nivel.Items.Insert(1, new ListItem("CUENTA", "CUENTA"));
                    Cmb_Nivel.Items.Insert(2, new ListItem("SUBCUENTA", "SUBCUENTA"));

                    Cmb_Nivel.SelectedIndex = 1;
                    Llenar_Combos_Cuentas("CUENTA");
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combo_Nivel ERROR[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Llenar_Combos_Cuentas
            ///DESCRIPCIÓN          : Metodo para llenar el combo de cuentas abono y cuentas cargo 
            ///PROPIEDADES          1 Nivel: tipo de nivel en q se consultaran las cuentas si a nivel de cuenta o subcuenta
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            private void Llenar_Combos_Cuentas(String Nivel)
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                DataTable Dt_Cuentas = new DataTable();

                try
                {
                    Cmb_Cuenta_Cargo.Items.Clear();
                    Cmb_Cuenta_Abono.Items.Clear();
                    Matriz_Negocio.P_Nivel = Nivel.Trim();
                    Dt_Cuentas = Matriz_Negocio.Consulta_Cuenta();
                    if (Dt_Cuentas != null)
                    {
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            Cmb_Cuenta_Cargo.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Cuenta_Cargo.DataTextField = "CUENTA";
                            Cmb_Cuenta_Cargo.DataSource = Dt_Cuentas;
                            Cmb_Cuenta_Cargo.DataBind();
                            Cmb_Cuenta_Cargo.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        }
                    }

                    Dt_Cuentas = new DataTable();
                    Matriz_Negocio.P_Nivel = "CUENTA";
                    Dt_Cuentas = Matriz_Negocio.Consulta_Cuenta();
                    if (Dt_Cuentas != null)
                    {
                        if (Dt_Cuentas.Rows.Count > 0)
                        {
                            Cmb_Cuenta_Abono.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;
                            Cmb_Cuenta_Abono.DataTextField = "CUENTA";
                            Cmb_Cuenta_Abono.DataSource = Dt_Cuentas;
                            Cmb_Cuenta_Abono.DataBind();
                            Cmb_Cuenta_Abono.Items.Insert(0, new ListItem("<-- SELECCIONE -->", ""));
                        }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al Llenar_Combos_Cuentas ERROR[" + ex.Message + "]");
                }
            }

        #endregion
    #endregion

    #region EVENTOS

        #region EVENTOS_GENERALES
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Salir_Click
            ///DESCRIPCIÓN          : Evento del boton de salir
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Salir_Click(object sender, EventArgs e)
            {
                switch (Btn_Salir.ToolTip)
                {
                    case "Cancelar":
                        Matriz_Conversion_Inicio();
                        Grid_Matriz.SelectedIndex = -1;
                        break;

                    case "Inicio":
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        break;
                }//fin del switch
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Nuevo_Click
            ///DESCRIPCIÓN          : Evento del boton nuevo
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Nuevo_Click(object sender, EventArgs e)
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                try
                {
                    switch (Btn_Nuevo.ToolTip)
                    {
                        case "Nuevo":
                            Estado_Botones("nuevo");
                            Limpiar_Controles();
                            Habilitar_Forma(true);
                            Grid_Matriz.SelectedIndex = -1;
                            Cmb_Nivel.SelectedIndex = 1;
                            break;
                        case "Guardar":
                            if (Validar_Datos())
                            {
                                Matriz_Negocio.P_No_Matriz = Txt_No_Matriz.Text.Trim();
                                Matriz_Negocio.P_Tipo_Matriz = Cmb_Tipo.SelectedItem.Value;
                                Matriz_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                                Matriz_Negocio.P_Tipo_Gasto = Cmb_Tipo_Gasto.SelectedItem.Value.Trim();
                                Matriz_Negocio.P_Cuenta_Abono = Cmb_Cuenta_Abono.SelectedItem.Value.Trim();
                                Matriz_Negocio.P_Cuenta_Cargo = Cmb_Cuenta_Cargo.SelectedItem.Value.Trim();
                                Matriz_Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;

                                Matriz_Negocio.Alta_Cuenta_Matriz(); 

                                Matriz_Conversion_Inicio();
                                
                                ScriptManager.RegisterStartupScript(this, this.GetType(), "Alta_Matriz", "alert('El registro se guardo satisfactoriamente');", true);
                            }
                            else 
                            {
                                Div_Contenedor_Msj_Error.Visible = true;
                            }
                            break;
                    }//fin del swirch
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al tratar de dar de alta los datos. Error[" + ex.Message + "]");
                }
            }//fin del boton Nuevo

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Modificar_Click
            ///DESCRIPCIÓN          : Evento del boton de modificar
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Modificar_Click(object sender, EventArgs e)
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();

                switch (Btn_Modificar.ToolTip)
                {
                    case "Modificar":
                        if (Grid_Matriz.SelectedIndex > -1)
                        {
                            Estado_Botones("modificar");
                            Habilitar_Forma(true);
                        }
                        else 
                        {
                            Div_Contenedor_Msj_Error.Visible = true;
                            Lbl_Ecabezado_Mensaje.Text = "Favor de seleccionar un registro de la tabla. ";
                        }
                        break;

                    case "Actualizar":
                        if (Validar_Datos())
                        {
                            Matriz_Negocio.P_Matriz_ID = Hf_Matriz_Id.Value.Trim();
                            Matriz_Negocio.P_No_Matriz = Txt_No_Matriz.Text.Trim();
                            Matriz_Negocio.P_Tipo_Matriz = Cmb_Tipo.SelectedItem.Value;
                            Matriz_Negocio.P_Nombre = Txt_Nombre.Text.Trim();
                            Matriz_Negocio.P_Tipo_Gasto = Cmb_Tipo_Gasto.SelectedItem.Value;
                            Matriz_Negocio.P_Cuenta_Abono = Cmb_Cuenta_Abono.SelectedItem.Value;
                            Matriz_Negocio.P_Cuenta_Cargo = Cmb_Cuenta_Cargo.SelectedItem.Value;
                            Matriz_Negocio.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado;

                            Matriz_Negocio.Actualizar_Cuenta_Matriz();

                            Matriz_Conversion_Inicio();
                            Grid_Matriz.SelectedIndex = -1;
                            ScriptManager.RegisterStartupScript(this, this.GetType(), "Actualizacion_Matriz", "alert('El registro fue actualizado');", true);
                        }
                        else
                        {
                            Div_Contenedor_Msj_Error.Visible = true;
                        }
                        break;
                }//fin del switch
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Eliminar_Click
            ///DESCRIPCIÓN          : Evento del boton de eliminar un registro 
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 12/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Eliminar_Click(object sender, EventArgs e)
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                ImageButton Btn_Eliminar = (ImageButton)sender;
                try
                {
                    Matriz_Negocio.P_Matriz_ID = Btn_Eliminar.CommandArgument.ToString().Trim();
                    Matriz_Negocio.Eliminar_Cuenta_Matriz();
                    Matriz_Conversion_Inicio();
                    Grid_Matriz.SelectedIndex = -1;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Eliminar", "alert('El registro fue eliminado');", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al eliminar el registro el grid. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Btn_Consultar_Click
            ///DESCRIPCIÓN          : Evento del boton de Consultar un registro del grid
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Btn_Consultar_Click(object sender, EventArgs e)
            {
                Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
                DataTable Dt_Matriz = new DataTable();
                DataView Dv_Matriz = new DataView();
                String Expresion_Busqueda = String.Empty;

                try
                {
                    Dt_Matriz = Matriz_Negocio.Consulta_Matriz();
                    Dv_Matriz = new DataView(Dt_Matriz);

                    Expresion_Busqueda = String.Format("{0} '%{1}%'", Grid_Matriz.SortExpression, Txt_Busqueda_Matriz.Text.Trim());

                    Dv_Matriz.RowFilter = Cat_Con_Matriz_De_Cuentas.Campo_No_Matriz + " like " + Expresion_Busqueda;
                    Dv_Matriz.RowFilter += " or " + Cat_Con_Matriz_De_Cuentas.Campo_Nombre+ " like " + Expresion_Busqueda;
                    Dv_Matriz.RowFilter += " or " + Cat_Con_Matriz_De_Cuentas.Campo_Tipo_Gasto + " like " + Expresion_Busqueda;
                    Dv_Matriz.RowFilter += " or TIPO like " + Expresion_Busqueda;
                    Dv_Matriz.RowFilter += " or CUENTA_CARGO like " + Expresion_Busqueda;
                    Dv_Matriz.RowFilter += " or CUENTA_ABONO like " + Expresion_Busqueda;

                    Grid_Matriz.Columns[8].Visible = true;
                    Grid_Matriz.Columns[9].Visible = true;
                    Grid_Matriz.Columns[10].Visible = true;
                    Grid_Matriz.DataSource = Dv_Matriz;
                    Grid_Matriz.DataBind();
                    Grid_Matriz.Columns[8].Visible = false;
                    Grid_Matriz.Columns[9].Visible = false;
                    Grid_Matriz.Columns[10].Visible = false;
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al consultar un registro de la tabla Error[" + ex.Message + "]");
                }
            }

        #endregion

        #region EVENTOS_GRID
            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Matriz_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del grid del registro que seleccionaremos
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Matriz_SelectedIndexChanged(object sender, EventArgs e)
            {
                try
                {
                    Limpiar_Controles();
                    if(Grid_Matriz.SelectedIndex > -1)
                    {
                        Txt_No_Matriz.Text = HttpUtility.HtmlDecode(Grid_Matriz.SelectedRow.Cells[1].Text.Trim());
                        Hf_Matriz_Id.Value = HttpUtility.HtmlDecode(Grid_Matriz.SelectedRow.Cells[10].Text.Trim());
                        Hf_No_Matriz.Value = HttpUtility.HtmlDecode(Grid_Matriz.SelectedRow.Cells[1].Text.Trim());
                        Txt_Nombre.Text = HttpUtility.HtmlDecode(Grid_Matriz.SelectedRow.Cells[3].Text.Trim());
                        Cmb_Tipo_Gasto.SelectedIndex = Cmb_Tipo_Gasto.Items.IndexOf(Cmb_Tipo_Gasto.Items.FindByValue(Grid_Matriz.SelectedRow.Cells[4].Text.Trim()));
                        Cmb_Tipo.SelectedIndex = Cmb_Tipo.Items.IndexOf(Cmb_Tipo.Items.FindByValue(Grid_Matriz.SelectedRow.Cells[2].Text.Trim()));
                        Hf_Tipo_Matriz.Value = HttpUtility.HtmlDecode(Grid_Matriz.SelectedRow.Cells[2].Text.Trim());
                        Cmb_Cuenta_Abono.SelectedIndex = Cmb_Cuenta_Abono.Items.IndexOf(Cmb_Cuenta_Abono.Items.FindByValue(Grid_Matriz.SelectedRow.Cells[8].Text.Trim()));
                        Cmb_Cuenta_Cargo.SelectedIndex = Cmb_Cuenta_Cargo.Items.IndexOf(Cmb_Cuenta_Cargo.Items.FindByValue(Grid_Matriz.SelectedRow.Cells[9].Text.Trim()));
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al seleccionar el registro el grid. Error[" + ex.Message + "]");
                }
            }

            ///********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Grid_Matriz_Sorting
            ///DESCRIPCIÓN          : Evento del grid para ordenar ascendentemente o descendentemente las columnas
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 13/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Grid_Matriz_Sorting(object sender, GridViewSortEventArgs e)
            {
                Llenar_Grid_Matriz();
                DataTable Dt_Matriz = (Grid_Matriz.DataSource as DataTable);

                if (Dt_Matriz != null)
                {
                    DataView Dv_Matriz = new DataView(Dt_Matriz);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC"))
                    {
                        Dv_Matriz.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    }
                    else
                    {
                        Dv_Matriz.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }

                    Grid_Matriz.Columns[8].Visible = true;
                    Grid_Matriz.Columns[9].Visible = true;
                    Grid_Matriz.Columns[10].Visible = true;
                    Grid_Matriz.DataSource = Dv_Matriz;
                    Grid_Matriz.DataBind();
                    Grid_Matriz.Columns[8].Visible = false;
                    Grid_Matriz.Columns[9].Visible = false;
                    Grid_Matriz.Columns[10].Visible = false;
                }
            }
        #endregion

        #region EVENTOS_COMBOS
            ///*********************************************************************************************************
            ///NOMBRE DE LA FUNCIÓN : Cmb_Nivel_SelectedIndexChanged
            ///DESCRIPCIÓN          : Evento del combo de tipo nivel
            ///PROPIEDADES          :
            ///CREO                 : Leslie González Vázquez
            ///FECHA_CREO           : 23/Enero/2012 
            ///MODIFICO             :
            ///FECHA_MODIFICO       :
            ///CAUSA_MODIFICACIÓN...:
            ///*********************************************************************************************************
            protected void Cmb_Nivel_SelectedIndexChanged(object sender, EventArgs e)
            {

                try
                {
                    if (Cmb_Nivel.SelectedIndex > 0)
                    {
                        Llenar_Combos_Cuentas(Cmb_Nivel.SelectedItem.Value.Trim());
                    }
                    else 
                    {
                        Cmb_Cuenta_Abono.Items.Clear();
                        Cmb_Cuenta_Cargo.Items.Clear();
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Error al generar el evento del combo nivel Error[" + ex.Message + "]");
                }
            }
        #endregion
    #endregion
}   


 /***** Codigo MANE
  #region (Métodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 27-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();             //Limpia los controles del forma
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Grid_Matriz();
        }
        catch (Exception ex)
        {
            throw new Exception("Inicializa_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 27-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            //Txt_Matriz_ID.Text = "";
            Txt_No_Relaciones.Text = "";
            Txt_Cuenta_Debe.Text = "";
            Txt_Busqueda_Matriz.Text = "";
            Txt_Cuenta_Haber.Text = "";
            Cmb_Cuenta_Haber.Items.Clear();
            Cmb_Cuenta_Debe.Items.Clear();
            Grid_Matriz.DataSource = null;
            Grid_Matriz.DataBind();
            if (Session["Dt_Cuentas"] != null)
            {
                Session.Remove("Dt_Cuentas");
            }
            if (Session["Consulta_Cuenta"] != null)
            {
                Session.Remove("Consulta_Cuenta");
            }
            if (Session["P_Dt_Matriz"] != null)
            {
                Session.Remove("P_Dt_Matriz");
            }
            if (Session["P_Dt_Matriz_Detalles"] != null)
            {
                Session.Remove("P_Dt_Matriz_Detalles");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 27-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Nuevo.Visible = true;
                    Btn_Nuevo.CausesValidation = false;
                    Btn_Modificar.CausesValidation = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Configuracion_Acceso("Frm_Cat_Con_Matriz.aspx");
                    Div_Matriz_Detalle.Style.Add(HtmlTextWriterStyle.Display,"none");//.Add("display","none");
                    Grid_Cat_Matriz_Cuentas.Style.Add(HtmlTextWriterStyle.Display, "block"); ;//.Add("display", "block");
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    break;

                case "Nuevo":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Dar de Alta";
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = true;
                    Btn_Modificar.Visible = false;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                    Div_Matriz_Detalle.Style.Add(HtmlTextWriterStyle.Display, "block");//.Add("display", "block");
                    Grid_Cat_Matriz_Cuentas.Style.Add(HtmlTextWriterStyle.Display, "none");//.Add("display", "none");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Nuevo.ToolTip = "Nuevo";
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Nuevo.Visible = false;
                    Btn_Modificar.Visible = true;
                    Btn_Eliminar.Visible = false;
                    Btn_Nuevo.CausesValidation = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            Cmb_Cuenta_Debe.Enabled = Habilitado;
            Cmb_Cuenta_Haber.Enabled = Habilitado;
            //Txt_Matriz_ID.Enabled = Habilitado;
            Txt_No_Relaciones.Enabled = Habilitado;
            Btn_Buscar_Cuenta.Enabled = !Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consultar_Cuentas_Contables
    /// DESCRIPCION : Carga los combos con las cuentas contables que existen
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 27-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consultar_Cuentas_Contables()
    {
        DataTable Dt_Cuentas_Contables = null;  //Almacenara los datos de las cuentas contables.
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexion con la capa de Datos
        try
        {
            Rs_Consulta_Con_Cuentas_Contables.P_Afectable = "SI";
            Dt_Cuentas_Contables = Rs_Consulta_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta las cuentas contables
            Cmb_Cuenta_Debe.DataSource = Dt_Cuentas_Contables; //Liga los datos con el combo
            Cmb_Cuenta_Debe.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;    //Asigna el campo de la tabla que se visualizara
            Cmb_Cuenta_Debe.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;    //Asigna el campo de la tabla que sera usado como valor
            Cmb_Cuenta_Debe.DataBind();
            Cmb_Cuenta_Debe.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Cuenta_Debe.SelectedIndex = -1;

            //Rs_Consulta_Con_Cuentas_Contables.P_Afectable = "SI";
            //Dt_Cuentas_Contables = Rs_Consulta_Con_Cuentas_Contables.Consulta_Cuentas_Contables(); //Consulta las cuentas contables
            Cmb_Cuenta_Haber.DataSource = Dt_Cuentas_Contables; //Liga los datos con el combo
            Cmb_Cuenta_Haber.DataTextField = Cat_Con_Cuentas_Contables.Campo_Descripcion;    //Asigna el campo de la tabla que se visualizara
            Cmb_Cuenta_Haber.DataValueField = Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID;    //Asigna el campo de la tabla que sera usado como valor
            Cmb_Cuenta_Haber.DataBind();
            Cmb_Cuenta_Haber.Items.Insert(0, new ListItem("< Seleccione >", ""));
            Cmb_Cuenta_Haber.SelectedIndex = -1;
        }
        catch (Exception Ex)
        {
            throw new Exception("Error generado al consultar las Percepciones Deducciones. Error: [" + Ex.Message + "]");
        }
    }
    //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Grid_Matriz
    // DESCRIPCIÓN: Llena el grid principal de Matriz
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 27/Diciembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
  /**  public void Llenar_Grid_Matriz()
    {
        Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();      
        //Requisicion_Negocio.P_Fecha_Inicial = Txt_Fecha_Inicial.Text;
        DataTable Dt_Datos_Matriz = new DataTable();
        Dt_Datos_Matriz = (DataTable)Session["P_Dt_Matriz"];
        Dt_Datos_Matriz = Matriz_Negocio.Consulta_Matriz();
        Session["P_Dt_Matriz"] = Dt_Datos_Matriz;
        Dt_Datos_Matriz = Matriz_Negocio.Consulta_Matriz_detalles();
        Session["P_Dt_Matriz_Detalles"] = Dt_Datos_Matriz;
        if (Session["P_Dt_Matriz"] != null && ((DataTable)Session["P_Dt_Matriz"]).Rows.Count > 0)
        {
            Grid_Cat_Matriz_Cuentas.Columns[1].Visible = true;
            Grid_Cat_Matriz_Cuentas.DataSource = Session["P_Dt_Matriz"] as DataTable;
            Grid_Cat_Matriz_Cuentas.DataBind();
            Grid_Cat_Matriz_Cuentas.Columns[1].Visible = false;
        }
        else
        {
            Session["P_Dt_Matriz"] = null;
            Grid_Cat_Matriz_Cuentas.DataSource = null;
            Grid_Cat_Matriz_Cuentas.DataBind();
        }
    }
  #endregion

    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ  : 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Nuevo);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Eliminar);
            Botones.Add(Btn_Buscar_Cuenta);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Cuenta_Debe_SelectedIndexChanged
    /// DESCRIPCION : Consulta la cuenta contable de la descripción de la cuentan
    ///               contable que fue seleccionada por el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Cuenta_Debe_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Debe.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Debe.Text = Aplicar_Mascara_Cuenta_Contable(Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString()).ToString();
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cmb_Cuenta_Haber_SelectedIndexChanged
    /// DESCRIPCION : Consulta la cuenta contable de la descripción de la cuentan
    ///               contable que fue seleccionada por el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Cmb_Cuenta_Haber_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Debe.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta los datos de la cuenta contable seleccionada
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Haber.Text = Aplicar_Mascara_Cuenta_Contable(Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString()).ToString();
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Aplicar_Mascara_Cuenta_Contable
    /// DESCRIPCION : Aplica la Mascara a la Cuenta Contable
    /// PARAMETROS  : Cuenta_Contable: Recibe el numero de cuenta contable3613  1
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 20/Septiembre/2011
    /// MODIFICO          : sergio manuel gallardo andrade
    /// FECHA_MODIFICO    :3-noviembre-2011
    /// CAUSA_MODIFICACION:no funciona correctamente al aplicarle la mascara 
    ///*******************************************************************************
    private string Aplicar_Mascara_Cuenta_Contable(string Cuenta_Contable)
    {
        try
        {
            string Mascara_Cuenta_Contable = Consulta_Parametros(); //Consulta y almacena la mascara contable actual.
            string Cuenta_Contable_Con_Formato = "";    //Almacenara la cuenta contable ya estandarizada de acuerdo al formato.
            Boolean Primer_Numero = true;   //Detecta si el primer caracter es un numero.
            int Caracteres_Extraidos_Cuenta_Contable = 0;  //Variable que almacena la cantidad de caracteres extraidos de la cuenta.
            int contador_nuevo = 0;
            int Inicio_Extraccion = 0; //Variable que almacena el inicio de la cadena a extraer.
            int Fin_Extraccion = 0;    //Variable que almacena el fin de la cadena a extraer.
            for (int Cont_Desplazamiento = 0; Cont_Desplazamiento < Mascara_Cuenta_Contable.Length; Cont_Desplazamiento++)  //Ciclo de desplazamiento
            {
                if (Primer_Numero == true && Mascara_Cuenta_Contable.Substring(Cont_Desplazamiento, 1) == "#")  //Detecta el primer numero dentro de la mascara contable
                {
                    if (Cont_Desplazamiento == 0)
                        Inicio_Extraccion = Cont_Desplazamiento;
                    else
                        Inicio_Extraccion = contador_nuevo;
                    Primer_Numero = false;
                }
                if (Mascara_Cuenta_Contable.Substring(Cont_Desplazamiento, 1) != "#") //Detecta si el caracter es diferente de un numero en la mascara contable.
                {
                    Fin_Extraccion = Cont_Desplazamiento;
                    if (Inicio_Extraccion == 0)
                    {
                        Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Inicio_Extraccion, Fin_Extraccion - Inicio_Extraccion);
                        Caracteres_Extraidos_Cuenta_Contable = Fin_Extraccion - Inicio_Extraccion;
                    }
                    else
                    {
                        Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Inicio_Extraccion, Fin_Extraccion - Inicio_Extraccion - contador_nuevo);
                        Caracteres_Extraidos_Cuenta_Contable += Fin_Extraccion - Inicio_Extraccion - contador_nuevo;
                    }
                    if (contador_nuevo < Cuenta_Contable.Length)
                    {
                        contador_nuevo = contador_nuevo + 1;
                    }
                    Primer_Numero = true;
                    Cuenta_Contable_Con_Formato += "-";

                }
            }
            if (Caracteres_Extraidos_Cuenta_Contable != Cuenta_Contable.Length) //Concatena los caracteres sobrantes en la cuenta contable.
            {
                Cuenta_Contable_Con_Formato += Cuenta_Contable.Substring(Caracteres_Extraidos_Cuenta_Contable, Cuenta_Contable.Length - Caracteres_Extraidos_Cuenta_Contable);
            }
            return Cuenta_Contable_Con_Formato;
        }
        catch (Exception ex)
        {
            throw new Exception("Aplicar_Mascara_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Parametros
    /// DESCRIPCION : Consulta la mascara para la cuenta contable actual.
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 19/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private string Consulta_Parametros()
    {
        Cls_Cat_Con_Parametros_Negocio Rs_Consulta_Cat_Con_Parametros_Negocio = new Cls_Cat_Con_Parametros_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Parametros; //Variable que obtendra los datos de la consulta 
        string Mascara_Cuenta_Contable; //Recibe la mascara contable actual.
        try
        {
            Session.Remove("Consulta_Parametros");
            Dt_Parametros = Rs_Consulta_Cat_Con_Parametros_Negocio.Consulta_Parametros();//Consulta los datos generales de las Cuentas Contables dados de alta en la BD
            Session["Consulta_Parametros"] = Dt_Parametros;
            Mascara_Cuenta_Contable = Dt_Parametros.Rows[0][0].ToString();
            return Mascara_Cuenta_Contable;
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Parametros" + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Cuenta_Contable_TextChanged
    /// DESCRIPCION : Consulta la Descripción más cercana de la cuenta que esta 
    ///               proporcionando el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Cuenta_Haber_TextChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio();  //Variable de conexion con la capa de negocio
        DataTable Dt_Descripcion_Cuenta_Contable;   //Almacena la descripcion de la cuenta contable proporcionada.
        try
        {
            if (Validar_Mascara_Cuenta_Contable(Txt_Cuenta_Haber.Text))
            {
                Rs_Consulta_Cat_Con_Cuentas_Contables.P_Descripcion = Txt_Cuenta_Haber.Text;
                Dt_Descripcion_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta las cuentas contables
                foreach (DataRow Registro in Dt_Descripcion_Cuenta_Contable.Rows)
                {
                    Cmb_Cuenta_Haber.SelectedValue = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                    Consulta_Cuenta_Contable_Haber();
                    Txt_Cuenta_Haber.Text = Aplicar_Mascara_Cuenta_Contable(Txt_Cuenta_Haber.Text);
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        // upd_panel
    }   
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Txt_Cuenta_Debe_TextChanged
    /// DESCRIPCION : Consulta la Descripción más cercana de la cuenta que esta 
    ///               proporcionando el usuario
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Txt_Cuenta_Debe_TextChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cat_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio();  //Variable de conexion con la capa de negocio
        DataTable Dt_Descripcion_Cuenta_Contable;   //Almacena la descripcion de la cuenta contable proporcionada.
        try
        {
            if (Validar_Mascara_Cuenta_Contable(Txt_Cuenta_Debe.Text))
            {
                Rs_Consulta_Cat_Con_Cuentas_Contables.P_Descripcion = Txt_Cuenta_Debe.Text;
                Dt_Descripcion_Cuenta_Contable = Rs_Consulta_Cat_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();  //Consulta las cuentas contables
                foreach (DataRow Registro in Dt_Descripcion_Cuenta_Contable.Rows)
                {
                    Cmb_Cuenta_Debe.SelectedValue = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
                 Consulta_Cuenta_Contable_Debe();
                 Txt_Cuenta_Debe.Text = Aplicar_Mascara_Cuenta_Contable(Txt_Cuenta_Debe.Text);
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
        // upd_panel
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable_Haber
    /// DESCRIPCION : Consulta las cuentas contables
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 10/OCtubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Consulta_Cuenta_Contable_Haber()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Haber.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Haber.Text = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuenta_Contable_Debe
    /// DESCRIPCION : Consulta las cuentas contables
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 10/OCtubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Consulta_Cuenta_Contable_Debe()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Con_Cuentas_Contables = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión a la capa de negocios
        DataTable Dt_Cuenta_Contable = null; //Obtiene la cuenta contable de la descripción que fue seleccionada por el usuario

        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Rs_Consulta_Con_Cuentas_Contables.P_Cuenta_Contable_ID = Cmb_Cuenta_Debe.SelectedValue;
            Dt_Cuenta_Contable = Rs_Consulta_Con_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();
            if (Dt_Cuenta_Contable.Rows.Count > 0)
            {
                //Agrega la cuenta contable a la caja de texto correspondiente
                foreach (DataRow Registro in Dt_Cuenta_Contable.Rows)
                {
                    Txt_Cuenta_Debe.Text = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Mascara_Cuenta_Contable
    /// DESCRIPCION : Validar que el formato de la mascara sea el correcto.
    /// PARAMETROS  : TEXTO: Recibe el valor contenido en la propiedad Text de la Txt_Cuenta_Contable
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 19/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Mascara_Cuenta_Contable(string Texto)
    {
        try
        {
            string Mascara_Cuenta_Contable = Consulta_Parametros(); //Almacena el formato actual que debe tener la cuenta contable.
            int Cont_Guiones = 0;   //Almacenara la cantidad de guiones presentes en la mascara.
            for (int Cont_Caracter = 0; Cont_Caracter < Mascara_Cuenta_Contable.Length; Cont_Caracter++)    //Ciclo para contar los guiones
            {
                if (Mascara_Cuenta_Contable.Substring(Cont_Caracter, 1) == "-")
                    Cont_Guiones++;
            }
            if (Texto.Length == Mascara_Cuenta_Contable.Length) //Valida que Texto y la Mascara sean del mismo tamaño
            {
                for (int Cont_Caracteres = 0; Cont_Caracteres < Mascara_Cuenta_Contable.Length; Cont_Caracteres++)  //Recorre Texto para compararlo con cada caracter de la mascara contable
                {
                    if (Texto.Substring(Cont_Caracteres, 1) == "-")
                    {
                        if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != Texto.Substring(Cont_Caracteres, 1))
                            throw new Exception("El formato de entrada de la cuenta contable no coincide con lo establecido en la mascara.");
                    }
                    else
                    {
                        if (Mascara_Cuenta_Contable.Substring(Cont_Caracteres, 1) != "#")
                            throw new Exception("El formato de entrada de la cuenta contable no coincide con lo establecido en la mascara.");
                    }
                }
                return (Boolean)true;
            }
            else if (Texto.Length == (Mascara_Cuenta_Contable.Length - Cont_Guiones))
            {
                return (Boolean)true;
            }
            else
            {
                throw new Exception("La Cuenta Contable no cumple con lo requerido.");
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Validar_Mascara_Cuenta_Contable " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuentas
    /// DESCRIPCION : Consulta las Cuentas de haber que estan dadas de alta en la BD
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29/diciembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Cuentas()
    {
        Cls_Cat_Con_Matriz_Negocio Rs_Consulta_Cat_Con_Cuentas = new Cls_Cat_Con_Matriz_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas; //Variable que obtendra los datos de la consulta 

        try
        {
            if (!string.IsNullOrEmpty(Txt_Busqueda_Matriz.Text.Trim()))
            {
                Rs_Consulta_Cat_Con_Cuentas.P_Cuenta = Txt_Busqueda_Matriz.Text;
            }
            Dt_Cuentas = Rs_Consulta_Cat_Con_Cuentas.Consulta_Cuenta(); //Consulta los datos generales de los Niveles de Poliza dados de alta en la BD
            if (Dt_Cuentas == null)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                Habilitar_Controles("Inicial");
            }
            else {
                Session["Consulta_Cuenta"] = Dt_Cuentas;
                Evento_Datos_Cuenta();
            }

            //Llena_Grid_Niveles(); //Agrega los Niveles de Poliza obtenidas de la consulta anterior
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuenta_haber " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Evento_Datos_Cuenta
    ///DESCRIPCIÓN:
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/Noviembre/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Evento_Datos_Cuenta()
    {
        Consultar_Cuentas_Contables();
        DataTable Dt_Cuenta = new DataTable(); //Variable a contener los datos de la consulta
        DataTable Dt_Tempora = new DataTable();
        Dt_Cuenta = (DataTable)Session["Consulta_Cuenta"];
        Div_Matriz_Detalle.Style.Add(HtmlTextWriterStyle.Display, "block");
        Grid_Cat_Matriz_Cuentas.Style.Add(HtmlTextWriterStyle.Display, "none");
        Btn_Agregar_Cuenta.Enabled = false;
        Grid_Matriz.Enabled = false;
        if(Dt_Cuenta != null && Dt_Cuenta.Rows.Count>0)
        {
            Dt_Tempora.Columns.Add("CUENTA_ID_DEBE", typeof(System.String));
            Dt_Tempora.Columns.Add("CUENTA_DEBE", typeof(System.String));
            Dt_Tempora.Columns.Add("DESCRIPCION_DEBE", typeof(System.String));
            DataRow fila;
            foreach (DataRow renglon in Dt_Cuenta.Rows)
            {
                fila = Dt_Tempora.NewRow();
                fila["CUENTA_ID_DEBE"] = renglon["CUENTA_ID_DEBE"].ToString();
                fila["CUENTA_DEBE"] = renglon["CUENTA_DEBE"].ToString();
                fila["DESCRIPCION_DEBE"] = renglon["DESCRIPCION_DEBE"].ToString();
                Dt_Tempora.Rows.Add(fila);
            }
            Session["Dt_Cuentas"] = Dt_Tempora;
            Txt_Cuenta_Haber.Text = Dt_Cuenta.Rows[0]["CUENTA_HABER"].ToString();
            Cmb_Cuenta_Haber.SelectedValue = Dt_Cuenta.Rows[0]["CUENTA_ID_HABER"].ToString();
            Txt_No_Relaciones.Text = Dt_Cuenta.Rows[0]["NUMERO_RELACIONES"].ToString();
            Grid_Matriz.DataSource=null;
            Grid_Matriz.DataBind();
            Grid_Matriz.DataSource = Dt_Cuenta;
            Grid_Matriz.DataBind();
        }

    }
    #endregion
    
   

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Nivel
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 10-Junio-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    {
        String Espacios_Blanco;
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

        if (string.IsNullOrEmpty(Txt_No_Relaciones.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Debes ingresar la cantidad de relaciones que puede tener esta cuenta . <br>";
            Datos_Validos = false;
        }
        if (Grid_Matriz.Rows.Count==0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Para poder generar el alta debe existir por lo menos una relacion con la cuenta haber. <br>";
            Datos_Validos = false;
        }
        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Cuenta_en_Matriz
    /// DESCRIPCION : Da de Alta del Nivel de Poliza con los datos proporcionados por 
    ///               el usuario
    /// PARAMETROS  : 
    /// CREO        :Sergio Manuel Gallardo
    /// FECHA_CREO  : 28-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Cuenta_en_Matriz()
    {
        Cls_Cat_Con_Matriz_Negocio Rs_Alta_Cat_Con_Matriz = new Cls_Cat_Con_Matriz_Negocio();  //Variable de conexión hacia la capa de Negocios
        try
        {
            Rs_Alta_Cat_Con_Matriz.P_Cuenta_ID_Haber = Cmb_Cuenta_Haber.SelectedValue;
            Rs_Alta_Cat_Con_Matriz.P_Numero_Relaciones = Txt_No_Relaciones.Text;
            Rs_Alta_Cat_Con_Matriz.P_Datos_Matriz_Cuenta = (DataTable)Session["Dt_Cuentas"];
            Rs_Alta_Cat_Con_Matriz.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Alta_Cat_Con_Matriz.Alta_Cuenta_Matriz(); //Da de alto los datos del Nivel de Poliza en la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Habilitar_Controles("Inicial");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de matriz", "alert('El Alta de la Cuenta en la Matriz fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_Nivel_Poliza " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Modificar_Cuenta
    /// DESCRIPCION : Modifica los datos de la cuenta por los datos proporcionados
    ///               por el usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade    
    /// FECHA_CREO  : 29-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Modificar_Cuenta()
    {
        Cls_Cat_Con_Matriz_Negocio Rs_Modifica_Cat_Con_Matriz = new Cls_Cat_Con_Matriz_Negocio();
        try
        {
            Rs_Modifica_Cat_Con_Matriz.P_Cuenta_ID_Haber = Cmb_Cuenta_Haber.SelectedValue;
            Rs_Modifica_Cat_Con_Matriz.P_Numero_Relaciones = Txt_No_Relaciones.Text;
            Rs_Modifica_Cat_Con_Matriz.P_Datos_Matriz_Cuenta = (DataTable)Session["Dt_Cuentas"];
            Rs_Modifica_Cat_Con_Matriz.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
            Rs_Modifica_Cat_Con_Matriz.Modifica_Cuenta_Matriz(); //Da de alto los datos del Nivel de Poliza en la BD
            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            Habilitar_Controles("Inicial");
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Matriz", "alert('La Modificación de la cuenta fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Modificar_Nivel_Poliza " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Eliminar_Cuenta
    /// DESCRIPCION : Elimina los datos de la cuenta que fue seleccionada por el Usuario
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 29-Diciembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Eliminar_Cuenta()
    {
        Cls_Cat_Con_Matriz_Negocio Rs_Eliminar_Cat_Con_Matriz = new Cls_Cat_Con_Matriz_Negocio();

        try
        {
            Rs_Eliminar_Cat_Con_Matriz.P_Cuenta_ID_Haber  = Cmb_Cuenta_Haber.SelectedValue;
            Rs_Eliminar_Cat_Con_Matriz.Eliminar_Cuenta();//Elimina el Nivel de Poliza seleccionada por el usuario de la BD

            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Matriz", "alert('La Eliminación de la cuenta fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Eliminar_Nivel_Poliza" + ex.Message.ToString(), ex);
        }
    }
    //    #endregion
    //#endregion

   #region (Eventos)
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Nuevo.ToolTip == "Nuevo")
            {
                Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                Consultar_Cuentas_Contables();
                Btn_Agregar_Cuenta.Enabled = true;
            }
            else
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                if (Validar_Datos())
                {
                    Alta_Cuenta_en_Matriz(); //Da de alta el Nivel de Poliza con los datos que proporciono el usuario
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                
                if (!string.IsNullOrEmpty(Txt_Cuenta_Haber.Text.Trim()))
                {
                    Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                    Btn_Agregar_Cuenta.Enabled = true;
                    Txt_Cuenta_Haber.Enabled = false;
                    Cmb_Cuenta_Haber.Enabled = false;
                    Grid_Matriz.Enabled = true;

                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = "Seleccione La cuenta que desea modificar sus datos <br>";
                }
            }
            else
            {
               
                if (Validar_Datos())
                {
                    Modificar_Cuenta(); //Modifica los datos del Nivel de Poliza con los datos proporcionados por el usuario
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Eliminar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            //Si el usuario selecciono un Nivel de Poliza entonces la elimina de la base de datos
            if (!string.IsNullOrEmpty(Txt_Cuenta_Haber.Text.Trim()))
            {
                Eliminar_Cuenta(); //Elimina el Nivel de Poliza que fue seleccionada por el usuario
            }
            //Si el usuario no selecciono algún Nivel de Poliza manda un mensaje indicando que es necesario que 
            //seleccione alguna para poder eliminar
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Seleccione la cuenta que desea eliminar <br>";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session.Remove("Consulta_Cuenta");
                Session.Remove("Dt_Cuentas");
                Session.Remove("P_Dt_Matriz");
                Session.Remove("P_Dt_Matriz_Detalles");
                if ( Txt_Cuenta_Haber.Text != "")
                {
                    Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                    Limpia_Controles();//Limpia los controles de la forma
                }else{
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
                
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Buscar_Cuenta_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Consulta_Cuentas(); //Consulta los Niveles de Poliza que coincidan con el nombre porporcionado por el usuario
            //Si no se encontraron Niveles de Poliza con una descripción similar al proporcionado por el usuario entonces manda un mensaje al usuario
            if (Grid_Matriz.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro la cuenta proporcionada favor de verificarla <br>";
            }
            Btn_Modificar.Visible = true;
            Btn_Eliminar.Visible = true;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Agregar_Cuenta_Click(object sender, ImageClickEventArgs e)
    {
        DataTable Dt_Cuentas = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
        //string Codigo_Programatico = "";
        String Espacios = "";
        int error = 0;
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        //Valida que todos los datos requeridos los haya proporcionado el usuario
        if (Cmb_Cuenta_Debe.SelectedIndex > 0 && Cmb_Cuenta_Haber.SelectedIndex >0 && Txt_Cuenta_Haber.Text!= "" && Txt_Cuenta_Debe.Text !="")
        {
           if (Session["Dt_Cuentas"] == null)
            {
                //Agrega los campos que va a contener el DataTable
                Dt_Cuentas.Columns.Add("CUENTA_ID_DEBE", typeof(System.String));
                Dt_Cuentas.Columns.Add("CUENTA_DEBE", typeof(System.String));
                Dt_Cuentas.Columns.Add("DESCRIPCION_DEBE", typeof(System.String));
            }
            else
            {
                Dt_Cuentas = (DataTable)Session["Dt_Cuentas"];
                Session.Remove("Dt_Cuentas");
            }
            DataRow row = Dt_Cuentas.NewRow(); //Crea un nuevo registro a la tabla
            //Asigna los valores al nuevo registro creado a la tabla
            row["CUENTA_ID_DEBE"] = Cmb_Cuenta_Debe.SelectedValue;
            row["CUENTA_DEBE"] = Txt_Cuenta_Debe.Text;
            row["DESCRIPCION_DEBE"] = Cmb_Cuenta_Debe.SelectedItem.Text;
            Dt_Cuentas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            Dt_Cuentas.AcceptChanges();
            Session["Dt_Cuentas"] = Dt_Cuentas;//Agrega los valores del registro a la sesión

            Grid_Matriz.Columns[0].Visible = true;
            Grid_Matriz.DataSource = Dt_Cuentas; //Agrega los valores de todas las partidas que se tienen al grid
            Grid_Matriz.DataBind();
            Grid_Matriz.Columns[0].Visible = false;
            Txt_Cuenta_Debe.Text = "";
            Cmb_Cuenta_Debe.SelectedIndex = 0;
            Cmb_Cuenta_Haber.Enabled = false;
            Txt_Cuenta_Haber.Enabled = false;
        }
        //Indica al usuario que datos son los que falta por proporcionar para poder agregar la partida a la poliza
        else
        {
           error = 1;
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
            if (Cmb_Cuenta_Debe.SelectedIndex <=  0 || Txt_Cuenta_Debe.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios+ " + Numero de Cuenta(debe)<br>";
            }
            if (Cmb_Cuenta_Haber.SelectedIndex <= 0 || Txt_Cuenta_Haber.Text == "")
            {
                Lbl_Mensaje_Error.Text += Espacios+ " + Numero de Cuenta(haber)<br>";
            }
        }
        if (error == 1)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
        }
    }
    protected void Btn_Eliminar_Partida(object sender, EventArgs e)
    {
        
        ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
        DataTable Dt_Partidas = (DataTable)Session["Dt_Cuentas"];
        DataRow[] Filas = Dt_Partidas.Select("CUENTA_ID" +
                "='" + Btn_Eliminar_Partida.CommandArgument + "'");

        if (!(Filas == null))
        {
            if (Filas.Length > 0)
            {
                Dt_Partidas.Rows.Remove(Filas[0]);
                Session["Dt_Cuentas"] = Dt_Partidas;
                Grid_Matriz.Columns[0].Visible = true;
                Grid_Matriz.DataSource = (DataTable)Session["Dt_Cuentas"];
                Grid_Matriz.DataBind();
                Grid_Matriz.Columns[0].Visible = false;               
            }
            if (Dt_Partidas.Rows.Count == 0)
            {
                Txt_Cuenta_Haber.Enabled = true;
                Cmb_Cuenta_Haber.Enabled = true;
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
    ///               para identicar la fila seleccionada de tabla.
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[0].Text.Trim();
                ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).ToolTip = "Quitar la Cuenta " + e.Row.Cells[1].Text;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //****************************************************************************************
    //NOMBRE DE LA FUNCION: Grid_Grupos_Documentos
    //DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //PARAMETROS  : 
    //CREO        : Sergio Manuel Gallardo
    //FECHA_CREO  : 16/julio/2011 10:01 am
    //MODIFICO          :
    //FECHA_MODIFICO    :
    //CAUSA_MODIFICACION:
    //****************************************************************************************
    protected void Grid_Matriz_Cuentas_Debe_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView GV_Cuentas = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataSet Ds_Consulta = new DataSet();
        Cls_Cat_Con_Matriz_Negocio Matriz_Negocio = new Cls_Cat_Con_Matriz_Negocio();
        Image imagen = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal literal = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_cuneta = (Label)e.Row.FindControl("Lbl_cuneta");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                literal.Text =literal.Text.Replace("Renglon_Grid", "Renglon_Grid" + Lbl_cuneta.Text);
                imagen.Attributes.Add("OnClick", "Mostrar_Tabla('Renglon_Grid" + Lbl_cuneta.Text + "','" + imagen.ClientID + "')");
                Dt_Datos_Detalles = (DataTable)Session["P_Dt_Matriz_Detalles"];
                Dt_Datos_Detalles.DefaultView.RowFilter = "Cuenta_ID_Haber='" + e.Row.Cells[1].Text + "'";
                GV_Cuentas = (GridView)e.Row.Cells[3].FindControl("Grid_matriz_Detalles");
                GV_Cuentas.Columns[0].Visible = true;
                GV_Cuentas.DataSource = Dt_Datos_Detalles.DefaultView;
                GV_Cuentas.DataBind();
                GV_Cuentas.Columns[0].Visible = false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion

    //#region (Grid)
    //    protected void Grid_Nivel_Sorting(object sender, GridViewSortEventArgs e)
    //    {
    //        //Se consultan los Niveles de Poliza que actualmente se encuentran registradas en el sistema.
    //        Consulta_Niveles_Polizas();

    //        DataTable Dt_Nivel_Poliza = (Grid_Nivel.DataSource as DataTable);

    //        if (Dt_Nivel_Poliza != null)
    //        {
    //            DataView Dv_Nivel_Poliza = new DataView(Dt_Nivel_Poliza);
    //            String Orden = ViewState["SortDirection"].ToString();

    //            if (Orden.Equals("ASC"))
    //            {
    //                Dv_Nivel_Poliza.Sort = e.SortExpression + " " + "DESC";
    //                ViewState["SortDirection"] = "DESC";
    //            }
    //            else
    //            {
    //                Dv_Nivel_Poliza.Sort = e.SortExpression + " " + "ASC";
    //                ViewState["SortDirection"] = "ASC";
    //            }

    //            Grid_Nivel.DataSource = Dv_Nivel_Poliza;
    //            Grid_Nivel.DataBind();
    //        }
    //    }
    //    ///*******************************************************************************
    //    /// NOMBRE DE LA FUNCION: Grid_Nivel_SelectedIndexChanged
    //    /// DESCRIPCION : Consulta los datos del Nivel de Poliza seleccionado por el usuario
    //    /// CREO        : Yazmin Abigail Delgado Gómez
    //    /// FECHA_CREO  : 10-Junio-2011
    //    /// MODIFICO          :
    //    /// FECHA_MODIFICO    :
    //    /// CAUSA_MODIFICACION:
    //    ///*******************************************************************************
    //    protected void Grid_Nivel_SelectedIndexChanged(object sender, EventArgs e)
    //    {
    //        Cls_Cat_Con_Niveles_Negocio Rs_Consulta_Con_Niveles = new Cls_Cat_Con_Niveles_Negocio(); //Variable de conexión hacia la capa de Negocios
    //        DataTable Dt_Niveles; //Variable que obtendra los datos de la consulta 

    //        try
    //        {
    //            Lbl_Mensaje_Error.Visible = false;
    //            Img_Error.Visible = false;
    //            Limpia_Controles(); //Limpia los controles del la forma para poder agregar los valores del registro seleccionado

    //            Rs_Consulta_Con_Niveles.P_Nivel_ID = Grid_Nivel.SelectedRow.Cells[1].Text;
    //            Dt_Niveles = Rs_Consulta_Con_Niveles.Consulta_Datos_Nivel(); //Consulta todos los datos del Nivel de Polizas que fue seleccionada por el usuario
    //            if (Dt_Niveles.Rows.Count > 0)
    //            {
    //                //Asigna los valores de los campos obtenidos de la consulta anterior a los controles de la forma
    //                foreach (DataRow Registro in Dt_Niveles.Rows)
    //                {
    //                    if (!String.IsNullOrEmpty(Registro[Cat_Con_Niveles.Campo_Nivel_ID].ToString()))
    //                        Txt_Nivel_ID.Text = Registro[Cat_Con_Niveles.Campo_Nivel_ID].ToString();

    //                    if (!String.IsNullOrEmpty(Registro[Cat_Con_Niveles.Campo_Descripcion].ToString()))
    //                        Txt_Descripcion_Nivel.Text = Registro[Cat_Con_Niveles.Campo_Descripcion].ToString();

    //                    if (!String.IsNullOrEmpty(Registro[Cat_Con_Niveles.Campo_Inicio_Nivel].ToString()))
    //                        Txt_Inicio_Nivel.Text = Registro[Cat_Con_Niveles.Campo_Inicio_Nivel].ToString();

    //                    if (!String.IsNullOrEmpty(Registro[Cat_Con_Niveles.Campo_Final_Nivel].ToString()))
    //                        Txt_Final_Nivel.Text = Registro[Cat_Con_Niveles.Campo_Final_Nivel].ToString();

    //                    if (!String.IsNullOrEmpty(Registro[Cat_Con_Niveles.Campo_Comentarios].ToString()))
    //                        Txt_Comentarios_Nivel.Text = Registro[Cat_Con_Niveles.Campo_Comentarios].ToString();
    //                }
    //            }
    //        }
    //        catch (Exception ex)
    //        {
    //            Lbl_Mensaje_Error.Visible = true;
    //            Img_Error.Visible = true;
    //            Lbl_Mensaje_Error.Text = ex.Message.ToString();
    //        }
    //    }
    //    protected void Grid_Nivel_PageIndexChanging(object sender, GridViewPageEventArgs e)
    //    {
    //        try
    //        {
    //            Limpia_Controles();                    //Limpia los controles de la forma
    //            Grid_Nivel.PageIndex = e.NewPageIndex; //Asigna la nueva página que selecciono el usuario
    //            Llena_Grid_Niveles();                  //Muestra los Niveles de Polizas que estan asignadas en la página seleccionada por el usuario
    //            Grid_Nivel.SelectedIndex = -1;
    //        }
    //        catch (Exception ex)
    //        {
    //            Lbl_Mensaje_Error.Visible = true;
    //            Img_Error.Visible = true;
    //            Lbl_Mensaje_Error.Text = ex.Message.ToString();
    //        }
    //    }
    //#endregion
}


    ***/