﻿<%@ Page Language="C#"  MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Proveedores.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Proveedores" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" runat="server">
    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" runat="server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel_proveedores" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Proveedores" runat="server" AssociatedUpdatePanelID="Upd_Panel_proveedores" DisplayAfter="0">
                 <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Proveedores" style="width: 97%; height: 700px;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte Proveedores</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td>                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Reporte_Proveedores" runat="server" ToolTip="Reporte" 
                                                CssClass="Img_Button" TabIndex="1" Visible="false"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" onclick="Btn_Reporte_Proveedores_Click" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click" />
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td colspan="3">
                            <table  width="100%" class="estilo_fuente">
                                <tr>
                                   <td style="width:20%">
                                        <asp:Label ID="Lbl_Proveedor" runat="server" Text="*Proveedor"></asp:Label>
                                    </td>
                                    <td style="width:30%" colspan="2">
                                        <asp:TextBox ID="Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" MaxLength="100" TabIndex="11" Width="80%"></asp:TextBox>
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Nombre_Proveedor_Solicitud_Pago" runat="server" TargetControlID="Txt_Nombre_Proveedor_Solicitud_Pago"
                                            FilterType="Custom, UppercaseLetters, LowercaseLetters" ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                        </cc1:FilteredTextBoxExtender>
                                        <asp:ImageButton ID="Btn_Buscar_Proveedor_Solicitud_Pagos" 
                                            runat="server" ToolTip="Consultar"
                                            TabIndex="12" ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                            onclick="Btn_Buscar_Proveedor_Solicitud_Pagos_Click"/>
                                    </td>
                                    <td  style="width:50%"colspan="4">
                                        <asp:DropDownList ID="Cmb_Proveedor_Solicitud_Pago" runat="server" TabIndex="13" Width="97%" />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                        <tr>
                            <td style="width:20%">
                                <asp:Label ID="Lbl_Estatus" runat="server" Text="*Estatus"></asp:Label>
                            </td>
                            <td style="width:30%">
                                <asp:DropDownList ID="Cmb_Estatus" runat="server" TabIndex="13" Width="83%">
                                    <asp:ListItem Value="1"><-SELECCIONE-></asp:ListItem>
                                    <asp:ListItem Value="2">PENDIENTES</asp:ListItem>
                                    <asp:ListItem Value="3">PAGADOS</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td style="width:50%">
                            </td>
                        </tr>
                    <tr>
                        <td colspan="3">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%">
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                        <td style="width:20%">
                                            <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width:30%">
                                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                         <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Final" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="MaskedEditValidator1" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Final"
                                            ControlExtender="Mee_Txt_Fecha_Final" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Final Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td  runat="server" id="Tr_Grid_Proveedor" colspan="4" style="display:none;">
                               <div>
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr>
                                            <td Font-Size="XX-Small"  style=" width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">No. Padron</td>
                                            <td Font-Size="XX-Small" style="width:90%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">PROVEEDOR</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                    <asp:GridView ID="Grid_Proveedores" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" OnRowDataBound="Grid_Proveedores_RowDataBound"
                                        DataKeyNames="Proveedor_ID" GridLines="None" Width="99%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                        ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                <ItemStyle HorizontalAlign="Left" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CUENTA" HeaderText="Padron">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="NOMBRE" HeaderText="">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%"  />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Facturas" runat="server" 
                                                        Text='<%# Bind("Proveedor_ID") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';width='99%' align='right';&gt;" />
                                                    <asp:GridView ID="Grid_Facturas" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="95%">
                                                        <Columns>
                                                            <asp:BoundField DataField="No_Factura" HeaderText="Factura">
                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FECHA_FACTURA" HeaderText="Fecha Factura" DataFormatString="{0:dd/MMM/yyyy}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="12%" Font-Size="X-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="MONTO_FACTURA" HeaderText="Monto"  DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="12%" Font-Size="X-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="NO_SOLICITUD_PAGO" HeaderText="Solicitud Pago">
                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="10%" Font-Size="X-Small"/>
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="FECHA_CREO" HeaderText="Fecha Solicitud"  DataFormatString="{0:dd/MMM/yyyy}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="12%" Font-Size="X-Small"/>
                                                            </asp:BoundField>                                                            
                                                            <asp:TemplateField HeaderText="Póliza Dev" Visible="True">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("POLIZA_DEVENGADO") %>' OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANIO_DEVENGADO") %>' CssClass='<%# Eval("TIPO_POLIZA_DEVENGADO") %>' CommandName='<%# Eval("NO_SOLICITUD_PAGO") %>' ForeColor="Blue"  Font-Size="XX-Small" />
                                                                </ItemTemplate >
                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                            </asp:TemplateField>
                                                            <asp:BoundField DataField="FECHA_PAGO" HeaderText="Fecha Pago"  DataFormatString="{0:dd/MMM/yyyy}">
                                                                <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="12%" Font-Size="X-Small"/>
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="FORMA_PAGO" HeaderText=" Forma Pago">
                                                                <HeaderStyle HorizontalAlign="Left" Width="12%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="left" Width="12%" Font-Size="X-Small"/>
                                                            </asp:BoundField>
                                                            <asp:TemplateField HeaderText="Póliza Pago" Visible="True">
                                                                <ItemTemplate>
                                                                    <asp:LinkButton ID="Btn_Seleccionar_Poliza_2" runat="server" Text= '<%# Eval("POLIZA_PAGADO") %>' OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANO_PAGADO") %>' CssClass='<%# Eval("TIPO_POLIZA_PAGADO") %>' CommandName='<%# Eval("NO_SOLICITUD_PAGO") %>' ForeColor="Blue"  Font-Size="XX-Small"/>
                                                                </ItemTemplate >
                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                                <ItemStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
