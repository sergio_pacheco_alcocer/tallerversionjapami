﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Autoriza_Ejercido.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Solicitud_Pagos.Datos;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
public partial class paginas_Contabilidad_Frm_Ope_Con_Autoriza_Solicitud_Pago : System.Web.UI.Page
{
    #region "Page_Load"
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Page_Load
        /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                
                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                    Acciones();
                    Txt_Lector_Codigo_Barras.Focus();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region "Metodos"
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Inicializa_Controles
        /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
        ///               diferentes operaciones
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade 
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
                Limpia_Controles();             //Limpia los controles del forma
                Llenar_Grid_Solicitudes_Pago();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Habilitar_Controles
        /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
        ///                para a siguiente operación
        /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
        ///                           si es una alta, modificacion
        ///                           
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
            try
            {
                Habilitado = false;
                switch (Operacion)
                {
                    case "Inicial":
                        Habilitado = true;
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Configuracion_Acceso("Frm_Ope_Con_Autoriza_Solicitud_Pago.aspx");
                        break;
                }
                //Cmb_Anio_Contable.Enabled = Habilitado;
                //Cmb_Mes_Contable.Enabled = Habilitado;
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Limpiar_Controles
        /// DESCRIPCION : Limpia los controles que se encuentran en la forma
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade 
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Limpia_Controles()
        {
            try
            {
                Txt_Busqueda_No_Solicitud.Text = "";
                Txt_Monto_Solicitud.Value = "";
                Txt_Cuenta_Contable_ID_Proveedor.Value = ""; 
                Txt_Cuenta_Contable_reserva.Value = ""; 
                Txt_No_Reserva.Value = "";
                Txt_Comentario.Text = "";
                Txt_No_Solicitud_Autorizar.Value = "";
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
        /// DESCRIPCION : Llena el grid Solicitudes de pago
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes_Pago()
        {
            try
            {
                String Rol_Grupo="";
                Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
                DataTable Dt_Resultado = new DataTable();
                Rs_Autoriza_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                 Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol.Rows.Count > 0)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            Rs_Autoriza_Solicitud.P_Dependencia_ID = "";
                            Rs_Autoriza_Solicitud.P_Grupo_Dependencia = Rol_Grupo;
                            Rs_Autoriza_Solicitud.P_Tipo_Recurso = "RECURSO ASIGNADO";
                            Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_SinAutorizar();
                        }
                        else {
                            Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_SinAutorizar();
                        }
                    }
                    else
                    {
                        Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_SinAutorizar();
                    }
                Grid_Solicitud_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                
                if (Dt_Resultado.Rows.Count > 0)
                {
                    Grid_Solicitud_Pagos.Columns[2].Visible = true;
                    Grid_Solicitud_Pagos.Columns[4].Visible = true;
                    Grid_Solicitud_Pagos.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Pagos.DataBind();    // Se ligan los datos.;
                    Grid_Solicitud_Pagos.Columns[2].Visible = false;
                    Grid_Solicitud_Pagos.Columns[4].Visible = false;
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }
        // ****************************************************************************************
        //'NOMBRE DE LA FUNCION:Accion
        //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
        //'PARAMETROS  : 
        //'CREO        : Sergio Manuel Gallardo
        //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
        //'MODIFICO          :
        //'FECHA_MODIFICO    :
        //'CAUSA_MODIFICACION:
        //'****************************************************************************************
        protected void Acciones()
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago_2 = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
            String Accion = String.Empty;
            String No_Solicitud = String.Empty;
            String Comentario = String.Empty;
            String Reserva;
            String Monto;
            String Tipo;
            int Actualiza_Presupuesto=0;
            int Registra_Movimiento;
            String Estatus;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            ReportDocument Reporte = new ReportDocument();
            String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
            String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
            DataTable Dt_solicitud;
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
            if (Request.QueryString["Accion"] != null)
            {
                Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
                if (Request.QueryString["id"] != null)
                {
                    No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                }
                 if (Request.QueryString["x"] != null)
                {
                    Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
                }
                //Response.Clear()
                switch (Accion)
                {
                    case "Rechazar_Solicitud":
                        //OracleConnection Cn_Cancelacion = new OracleConnection();
                        //OracleCommand Cmmd_Cancelacion = new OracleCommand();
                        //OracleTransaction Trans_Cancelacion = null;

                        //    // crear transaccion para crear el convenio 
                        //Cn_Cancelacion.ConnectionString = Presidencia.Constantes.Cls_Constantes.Str_Conexion;
                        //Cn_Cancelacion.Open();
                        //Trans_Cancelacion = Cn_Cancelacion.BeginTransaction();
                        //Cmmd_Cancelacion.Connection = Cn_Cancelacion;
                        //Cmmd_Cancelacion.Transaction = Trans_Cancelacion;
                        //    try
                        //    {
                        //        Cancela_Solicitud_Pago(No_Solicitud, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(),Cmmd_Cancelacion);
                        //        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud;
                        //        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd_Cancelacion;
                        //        Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                        //        Trans_Cancelacion.Commit();
                        //        //Inicializa_Controles();
                        //    }
                        //    catch (OracleException Ex)
                        //    {
                        //        Trans_Cancelacion.Rollback();
                        //        Lbl_Mensaje_Error.Text = "Error:";
                        //        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                        //        Img_Error.Visible = true;
                        //    }
                        //    catch (Exception Ex)
                        //    {
                        //        Lbl_Mensaje_Error.Text = "Error:";
                        //        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                        //        Img_Error.Visible = true;
                        //    }
                        //    finally
                        //    {
                        //        Cn_Cancelacion.Close();
                        //    }
                        
                        break;
                    //case "Codigo_Barras":
                        //OracleConnection Cn2 = new OracleConnection();
                        //OracleCommand Cmmd2 = new OracleCommand();
                        //OracleTransaction Trans2 = null;

                        //// crear transaccion para crear el convenio 
                        //Cn2.ConnectionString = Presidencia.Constantes.Cls_Constantes.Str_Conexion;
                        //Cn2.Open();
                        //Trans2 = Cn2.BeginTransaction();
                        //Cmmd2.Connection = Cn2;
                        //Cmmd2.Transaction = Trans2;
                        //try
                        //{

                        //    if (No_Solicitud.Length < 10)
                        //    {
                        //        Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        //    }
                        //    else
                        //    {
                        //        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        //    }
                        //    Rs_Solicitud_Pago.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        //    Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                        //    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        //    Dependencia_Negocio.P_Cmmd = Cmmd2;
                        //    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                        //    String Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                        //    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                        //    if (Dt_Grupo_Rol.Rows.Count > 0)
                        //    {
                        //        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        //        if (Grupo_Rol == "00003")
                        //        {
                        //            Rs_Solicitud_Pago.P_Dependencia_ID = "";
                        //            Rs_Solicitud_Pago.P_Grupo_Dependencia = Rol_Grupo;
                        //            Rs_Solicitud_Pago.P_Tipo_Recurso = "MUNICIPAL";
                        //            Rs_Solicitud_Pago.P_Cmmd = Cmmd2;
                        //            Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        //        }
                        //        else
                        //        {
                        //            Rs_Solicitud_Pago.P_Cmmd = Cmmd2;
                        //            Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        //        }
                        //    }
                        //    else
                        //    {
                        //        Rs_Solicitud_Pago.P_Cmmd = Cmmd2;
                        //        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        //    }
                        //    if (Dt_Consulta_Estatus.Rows.Count > 0)
                        //    {
                        //        foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                        //        {
                        //            if (Registro["Estatus"].ToString() == "PENDIENTE")
                        //            {
                        //                Rs_Solicitud_Pago.P_Estatus = "PRE-PREAUTORIZADO";
                        //                Rs_Solicitud_Pago.P_Comentario = Comentario;
                        //                Rs_Solicitud_Pago.P_Empleado_ID_Jefe = Cls_Sessiones.Empleado_ID.ToString();
                        //                Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                        //                Rs_Solicitud_Pago.P_Cmmd = Cmmd2;
                        //                Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                        //                Rs_Solicitud_Pago_2.P_Cmmd = Cmmd2;
                        //                Dt_solicitud = Rs_Solicitud_Pago_2.Consultar_Solicitud_Pago();
                        //                if (Dt_solicitud.Rows.Count > 0)
                        //                {
                        //                    Tipo = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                        //                    Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                        //                    if (Tipo != "00001" && Tipo != "00003")
                        //                    {
                        //                        if (Estatus == "PRE-PREAUTORIZADO")
                        //                        {
                        //                            Actualiza_Presupuesto = 0;
                        //                            Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                        //                            Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                        //                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud,Cmmd2);
                        //                            Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual( "COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd2);
                        //                            if (Actualiza_Presupuesto > 0)
                        //                            {
                        //                                Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd2);
                        //                                Trans2.Commit();
                        //                            }
                        //                            else
                        //                            {
                        //                                Trans2.Rollback();
                        //                                Lbl_Mensaje_Error.Text = "Error:";
                        //                                Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode("");
                        //                                Img_Error.Visible = true;
                        //                            }
                        //                            //Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "",Cmmd2);
                        //                        }
                        //                    }
                        //                    else
                        //                    {
                        //                        Trans2.Commit();
                        //                    }
                        //                }
                        //                else
                        //                {
                        //                    Trans2.Commit();
                        //                }
                        //            }
                        //        }
                        //    }
                        //}
                        //catch (OracleException Ex)
                        //{
                        //    Trans2.Rollback();
                        //    Lbl_Mensaje_Error.Text = "Error:";
                        //    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                        //    Img_Error.Visible = true;
                        //}
                        //catch (Exception Ex)
                        //{
                        //    Lbl_Mensaje_Error.Text = "Error:";
                        //    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                        //    Img_Error.Visible = true;
                        //}
                        //finally
                        //{
                        //    Cn2.Close();
                        //}
                        //break;
                }
            }
        }
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Txt_Lector_Codigo_Barras_TextChanged
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 02/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Txt_Lector_Codigo_Barras_TextChanged(object sender, EventArgs e)
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago_2 = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
            String No_Solicitud = String.Empty;
            String Reserva;
            String Monto;
            String Tipo;
            int Actualiza_Presupuesto = 0;
            int Registra_Movimiento;
            String Estatus;
            String Servicios_Generales = "";
            Boolean Afectado = true;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            ReportDocument Reporte = new ReportDocument();
            String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
            String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
            DataTable Dt_solicitud;
            String Rol_Grupo = ""; 
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
            DataTable Dt_Resultado = new DataTable();
            try
            {
                if (!String.IsNullOrEmpty(Txt_Lector_Codigo_Barras.Text))
                {
                    No_Solicitud = Txt_Lector_Codigo_Barras.Text;
                    if (No_Solicitud.Length < 10)
                    {
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        Rs_Solicitud_Pago_2.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud));
                        No_Solicitud = String.Format("{0:0000000000}", Convert.ToInt32(No_Solicitud)); 
                    }
                    else
                    {
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        Rs_Solicitud_Pago_2.P_No_Solicitud_Pago = No_Solicitud;
                    }
                    Rs_Solicitud_Pago.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol.Rows.Count > 0)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            Rs_Solicitud_Pago.P_Dependencia_ID = "";
                            Rs_Solicitud_Pago.P_Grupo_Dependencia = Rol_Grupo;
                            Rs_Solicitud_Pago.P_Tipo_Recurso = "RECURSO ASIGNADO";
                            Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        }
                        else
                        {
                            Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        }
                    }
                    else
                    {
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                    }
                    if (Dt_Consulta_Estatus.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                        {
                            if (Registro["Estatus"].ToString() == "PENDIENTE")
                            {

                                SqlConnection Cn = new SqlConnection();
                                SqlCommand Cmmd = new SqlCommand();
                                SqlTransaction Trans = null;

                                // crear transaccion para crear el convenio 
                                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                                Cn.Open();
                                Trans = Cn.BeginTransaction();
                                Cmmd.Connection = Cn;
                                Cmmd.Transaction = Trans;
                                try
                                {

                                    Rs_Solicitud_Pago.P_Estatus = "PRE-PREAUTORIZADO";
                                    Rs_Solicitud_Pago.P_Comentario = "";
                                    Rs_Solicitud_Pago.P_Empleado_ID_Jefe = Cls_Sessiones.Empleado_ID.ToString();
                                    Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                    Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                    Rs_Solicitud_Pago_2.P_Cmmd = Cmmd;
                                    Dt_solicitud = Rs_Solicitud_Pago_2.Consultar_Solicitud_Pago();
                                    if (Dt_solicitud.Rows.Count > 0)
                                    {
                                        Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                        Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                                        Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                        if (Tipo !="SI")
                                        {
                                            //revisar si la solicitud es de servicios masivos generales 
                                            if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                                            {
                                                Servicios_Generales = "SI";
                                            }
                                            else
                                            {
                                                Servicios_Generales = "NO";
                                            }
                                            if (Estatus == "PRE-PREAUTORIZADO")
                                            {
                                                DataTable Dt_Tipo = new DataTable();
                                                Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                                Rs_Consulta.P_Cmmd = Cmmd;
                                                Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                                                if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                                                {
                                                    Actualiza_Presupuesto = 0;
                                                    Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                    Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                    if (Servicios_Generales == "SI")
                                                    {
                                                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(No_Solicitud, Cmmd);
                                                    }
                                                    else
                                                    {
                                                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                                                    }
                                                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                                                    if (Actualiza_Presupuesto > 0)
                                                    {
                                                        Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                        Trans.Commit();
                                                    }
                                                    else
                                                    {
                                                        Trans.Rollback();
                                                        Lbl_Mensaje_Error.Text = "Error:";
                                                        Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para autorizar la solicitud No." + No_Solicitud;
                                                        Img_Error.Visible = true;
                                                        Lbl_Mensaje_Error.Visible = true;
                                                        Afectado = false;
                                                    }
                                                }
                                                else
                                                {
                                                    Actualiza_Presupuesto = 0;
                                                    Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                    Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                    Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(No_Solicitud, Cmmd);
                                                    Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                                                   if (Actualiza_Presupuesto > 0)
                                                    {
                                                        Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                        Trans.Commit();
                                                    }
                                                    else
                                                    {
                                                        Trans.Rollback();
                                                        Lbl_Mensaje_Error.Text = "Error:";
                                                        Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para autorizar la solicitud No." + No_Solicitud;
                                                        Img_Error.Visible = true;
                                                        Lbl_Mensaje_Error.Visible = true;
                                                        Afectado = false;
                                                    }
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Trans.Commit();
                                        }
                                    }
                                    else
                                    {
                                        Trans.Commit();
                                    }
                                }
                                catch (SqlException Ex)
                                {
                                    Trans.Rollback();
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                    Afectado = false;
                                }
                                catch (Exception Ex)
                                {
                                    Trans.Rollback();
                                    Lbl_Mensaje_Error.Text = "Error:";
                                    Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                    Img_Error.Visible = true;
                                    Lbl_Mensaje_Error.Visible = true;
                                    Afectado = false;
                                }
                                finally
                                {
                                    Cn.Close();
                                }
                            }
                        }
                    }
                    if (Afectado != false)
                    {
                        ScriptManager.RegisterStartupScript(this, GetType(), "refresh", "window.setTimeout('window.location.reload(true);',1);", true);
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }
    
        ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Autorizar_Solicitud
        ///DESCRIPCIÓN          : Metodo para actualizar los datos de autorizado 
        ///PROPIEDADES          :
        ///CREO                 : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO           : 02/Junio/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        protected void Autorizar_Solicitud(object sender, EventArgs e)
        {
            Int32 Indice = 0;
            String Estatus;
            String Autorizados = "Ninguno";
            String No_Solicitud;
            Boolean Autorizado;
            DataTable Dt_solicitud = new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            String Reserva;
            String Monto;
            String Servicios_Generales = "";
            String Tipo;
            Boolean Afectado = true;
            int Actualiza_Presupuesto = 0;
            int Registra_Movimiento;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Consulta = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();    //Objeto de acceso a los metodos.
            Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Solicitud_Pago_2 = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();    //Objeto de acceso a los metodos.
            try
            {
                foreach (GridViewRow Renglon_Grid in Grid_Solicitud_Pagos.Rows)
                {
                    Indice++;
                    Grid_Solicitud_Pagos.SelectedIndex = Indice;
                    Autorizado = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).Checked;
                    if (Autorizado)
                    {
                        Autorizados = "SI";
                        No_Solicitud = ((System.Web.UI.WebControls.CheckBox)Renglon_Grid.FindControl("Chk_Autorizado")).CssClass;
                        Rs_Solicitud_Pago_2.P_No_Solicitud_Pago = No_Solicitud;
                        Rs_Solicitud_Pago.P_No_Solicitud_Pago = No_Solicitud;
                        Rs_Solicitud_Pago.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                            {
                                if (Registro["Estatus"].ToString() == "PENDIENTE")
                                {

                                    SqlConnection Cn = new SqlConnection();
                                    SqlCommand Cmmd = new SqlCommand();
                                    SqlTransaction Trans = null;

                                    // crear transaccion para crear el convenio 
                                    Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                                    Cn.Open();
                                    Trans = Cn.BeginTransaction();
                                    Cmmd.Connection = Cn;
                                    Cmmd.Transaction = Trans;
                                    try
                                    {
                                        //Consultar la solicitud de pago
                                        Dt_solicitud = Rs_Solicitud_Pago_2.Consultar_Solicitud_Pago();

                                        //verificar si tiene registros
                                        if (Dt_solicitud.Rows.Count > 0)
                                        {
                                            //revisar si la solicitud es de servicios masivos generales 
                                            if (!String.IsNullOrEmpty(Dt_solicitud.Rows[0][Ope_Con_Solicitud_Pagos.Campo_Servicios_Generales].ToString().Trim()))
                                            {
                                                Servicios_Generales = "SI";
                                            }
                                            else
                                            {
                                                Servicios_Generales = "NO";
                                            }
                                            //Consultar el tipo de la solicitud
                                            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                            Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();

                                            //Verificar el tipo
                                            if (Tipo == "SI")
                                            {
                                                Rs_Solicitud_Pago.P_Estatus = "PORCOMPROBAR";
                                            }
                                            else
                                            {
                                                Rs_Solicitud_Pago.P_Estatus = "PRE-PREAUTORIZADO";
                                            }
                                        }
                                        
                                        Rs_Solicitud_Pago.P_Comentario = "";
                                        Rs_Solicitud_Pago.P_Empleado_ID_Jefe = Cls_Sessiones.Empleado_ID.ToString();
                                        Rs_Solicitud_Pago.P_Usuario_Modifico = Cls_Sessiones.Nombre_Empleado.ToString();
                                        Rs_Solicitud_Pago.P_Cmmd = Cmmd;
                                        Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud_Pago();
                                        Rs_Solicitud_Pago_2.P_Cmmd = Cmmd;

                                        //Limpiar la tabla
                                        Dt_solicitud = new DataTable();
                                        Dt_solicitud = Rs_Solicitud_Pago_2.Consultar_Solicitud_Pago();
                                        if (Dt_solicitud.Rows.Count > 0)
                                        {
                                            Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                            Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos_Comprobacion();
                                            //Tipo = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                            Estatus = Dt_solicitud.Rows[0]["ESTATUS"].ToString().Trim();
                                            if (Tipo =="NO")
                                            {
                                                if (Estatus == "PRE-PREAUTORIZADO")
                                                {
                                                    DataTable Dt_Tipo = new DataTable();
                                                    Rs_Consulta.P_Tipo_Solicitud_Pago_ID = Dt_solicitud.Rows[0]["Tipo_Solicitud_Pago_ID"].ToString().Trim();
                                                    Rs_Consulta.P_Cmmd = Cmmd;
                                                    Dt_Tipo = Rs_Consulta.Consulta_Tipo_Solicitud_Pagos();
                                                    if (Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().ToUpper() != "FINIQUITO" || Dt_Tipo.Rows[0]["DESCRIPCION"].ToString().Substring(0, 4).ToUpper() != "FINI")
                                                    {
                                                        Actualiza_Presupuesto = 0;
                                                        Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                        Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                        if (Servicios_Generales == "SI")
                                                        {
                                                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Servicios_Generales(No_Solicitud, Cmmd);
                                                        }
                                                        else
                                                        {
                                                            Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud(No_Solicitud, Cmmd);
                                                        }
                                                       Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                                                        if (Actualiza_Presupuesto > 0)
                                                        {
                                                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                            Trans.Commit();
                                                        }
                                                        else
                                                        {
                                                            Trans.Rollback();
                                                            Lbl_Mensaje_Error.Text = "Error:";
                                                            Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para autorizar la solicitud No." + No_Solicitud;
                                                            Img_Error.Visible = true;
                                                            Lbl_Mensaje_Error.Visible = true;
                                                            Afectado = false;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        Actualiza_Presupuesto = 0;
                                                        Reserva = Dt_solicitud.Rows[0]["NO_RESERVA"].ToString().Trim();
                                                        Monto = Dt_solicitud.Rows[0]["MONTO"].ToString().Trim();
                                                        Dt_Partidas = Cls_Ope_Con_Solicitud_Pagos_Datos.Consultar_detalles_partidas_de_solicitud_Finiquito(No_Solicitud, Cmmd);
                                                        Actualiza_Presupuesto = Cls_Ope_Psp_Manejo_Presupuesto.Actualizar_Momentos_Presupuestales_Mensual("COMPROMETIDO", "PRE_COMPROMETIDO", Dt_Partidas, Cmmd);
                                                        if (Actualiza_Presupuesto > 0)
                                                        {
                                                            Registra_Movimiento = Cls_Ope_Psp_Manejo_Presupuesto.Registro_Movimiento_Presupuestal(Reserva, "COMPROMETIDO", "PRE_COMPROMETIDO", Convert.ToDouble(Monto), "", "", "", "", Cmmd);
                                                            Trans.Commit();
                                                        }
                                                        else
                                                        {
                                                            Trans.Rollback();
                                                            Lbl_Mensaje_Error.Text = "Error:";
                                                            Lbl_Mensaje_Error.Text = "No hay suficiencia presupuestal para autorizar la solicitud No." + No_Solicitud;
                                                            Img_Error.Visible = true;
                                                            Lbl_Mensaje_Error.Visible = true;
                                                            Afectado = false;
                                                        }
                                                    }
                                                }
                                            }
                                            else
                                            {
                                                Trans.Commit();
                                            }
                                        }
                                        else
                                        {
                                            Trans.Commit();
                                        }
                                    }
                                    catch (SqlException Ex)
                                    {
                                        Trans.Rollback();
                                        Lbl_Mensaje_Error.Text = "Error:";
                                        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                    }
                                    catch (Exception Ex)
                                    {
                                        Lbl_Mensaje_Error.Text = "Error:";
                                        Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
                                        Img_Error.Visible = true;
                                        Lbl_Mensaje_Error.Visible = true;
                                    }
                                    finally
                                    {
                                        Cn.Close();
                                    }
                                }
                                if (Afectado == false)
                                {
                                    break;
                                }
                            }
                        }
                    }
                    if (Afectado == false)
                    {
                        break;
                    }
                }
                if (Autorizados != "SI")
                {
                    Lbl_Mensaje_Error.Text = "Error:";
                    Lbl_Mensaje_Error.Text = "No Selecciono ninguna Solicitud para ser Autorizado";
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Visible = true;
                }
                else
                {
                    if (Afectado != false)
                    {
                        Inicializa_Controles();
                    }
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Error al actualizar los datos. Error[" + ex.Message + "]");
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Cancela_Solicitud_Pago(String No_Solicitud_Pago,String Comentario, String Empleado_ID, String Nombre_Empleado, SqlCommand P_Cmmd)
        {
            Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            String Monto;
            String Reserva;
            Monto = "0";
            Reserva = "0";
            SqlDataAdapter Da_SQL = new SqlDataAdapter();
            DataSet Ds_SQL = new DataSet();
            SqlConnection Cn = new SqlConnection();
            SqlCommand Cmmd = new SqlCommand();
            SqlTransaction Trans = null;
            if (P_Cmmd != null)
            {
                Cmmd = P_Cmmd;
            }
            else
            {
                Cn.ConnectionString = Cls_Constantes.Str_Conexion;
                Cn.Open();
                Trans = Cn.BeginTransaction();
                Cmmd.Connection = Trans.Connection;
                Cmmd.Transaction = Trans;
            }
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Solicitud_Negocio.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Solicitud_Negocio.P_Cmmd = Cmmd;
                Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();

                if (Dt_Detalles != null)
                {
                    if (Dt_Detalles.Rows.Count > 0)

                    {
                        Monto =Dt_Detalles.Rows[0]["MONTO"].ToString();
                        Reserva = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString();
                    }
                }
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Monto);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "CANCELADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto ="CANCELACION-" + No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autorizo_Rechazo_Jefe = String.Format("{0:dd/MM/yyyy}",DateTime.Today);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Empleado_ID_Jefe_Area = Empleado_ID;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentarios_Jefe_Area = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Reserva);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados              
                if (P_Cmmd == null)
                {
                    Trans.Commit();
                }
            }
            catch (SqlException Ex)
            {
                if (P_Cmmd == null)
                {
                    Trans.Rollback();
                }
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            catch (DBConcurrencyException Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Lo siento, los datos fueron actualizados por otro Usuario. Error: [" + Ex.Message + "]");
            }
            catch (Exception Ex)
            {
                if (Trans != null)
                {
                    Trans.Rollback();
                }
                throw new Exception("Error: " + Ex.Message);
            }
            finally
            {
                if (P_Cmmd == null)
                {
                    Cn.Close();
                }

            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }

        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
#region generar caratula
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Imprimir
        ///DESCRIPCIÓN: Imprime la solicitud
        ///PROPIEDADES:     
        ///CREO: Sergio Manuel Gallardo
        ///FECHA_CREO: 06/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Imprimir(String Numero_Solicitud)
        {
            DataSet Ds_Reporte = null;
            DataTable Dt_Pagos = null;
            try
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                Ds_Reporte = new DataSet();
                Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                if (Dt_Pagos.Rows.Count > 0)
                {
                    Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                    Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                }
            }
            //}
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                Lbl_Mensaje_Error.Visible = true;
            }

        }
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
        {
            ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
            String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
            try
            {
                Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                        Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }
#endregion
    #endregion

    #region "Eventos"
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
        ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
        ///PARÁMETROS :
        ///CREO       : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO  : 21-mayo-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        protected void Btn_Solicitud_Click(object sender, EventArgs e)
        {
            String No_Solicitud = ((LinkButton)sender).Text;
            Imprimir(No_Solicitud);
        }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        Inicializa_Controles();
    }
    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Tipo_Solicitud = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
        DataTable Dt_Datos_Polizas = new DataTable();
        DataTable Dt_Tipo_Solicitud = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        ReportDocument Reporte = new ReportDocument();
        String Tipo_Solicitud = "";
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "Rechazado";// +Convert.ToString(String.Format("{0:ddMMMyyy}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        String Usuario = "";    
        SqlConnection Cn = new SqlConnection();
        SqlCommand Cmmd = new SqlCommand();
        SqlTransaction Trans = null;

        // crear transaccion para crear el convenio 
        Cn.ConnectionString = Cls_Constantes.Str_Conexion;
        Cn.Open();
        Trans = Cn.BeginTransaction();
        Cmmd.Connection = Cn;
        Cmmd.Transaction = Trans;
        try
        {
            DataRow Row;
            DataTable Dt_Reporte = new DataTable();

            if (Txt_Comentario.Text != "")
            {
                Cancela_Solicitud_Pago(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString(),Cmmd );
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Autorizar.Value;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Cmmd = Cmmd;
                Dt_Datos_Polizas = Rs_Modificar_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                Trans.Commit();
                //  se crea la tabla para el reporte de la cancelacion
                Dt_Reporte.Columns.Add("NO_SOLICITUD", typeof(System.String));
                Dt_Reporte.Columns.Add("NO_RESERVA", typeof(System.String));
                Dt_Reporte.Columns.Add("PROVEEDOR", typeof(System.String));
                Dt_Reporte.Columns.Add("MONTO", typeof(System.Double));
                Dt_Reporte.Columns.Add("FECHA_CREO", typeof(System.DateTime));
                Dt_Reporte.Columns.Add("FECHA_RECHAZO", typeof(System.DateTime));
                Dt_Reporte.Columns.Add("TIPO_SOLICITUD_PAGO_ID", typeof(System.String));
                Dt_Reporte.Columns.Add("CONCEPTO_SOLICITUD", typeof(System.String));
                Dt_Reporte.Columns.Add("COMENTARIO", typeof(System.String));
                Dt_Reporte.Columns.Add("USUARIO_CREO", typeof(System.String));
                Dt_Reporte.Columns.Add("USUARIO_RECHAZO", typeof(System.String));
                Dt_Reporte.TableName = "Dt_Cancelacion";


                foreach (DataRow Registro in Dt_Datos_Polizas.Rows)
                {
                    Row = Dt_Reporte.NewRow();

                    Row["NO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString());
                    Row["NO_RESERVA"] = (Registro["Reserva"].ToString());
                    Row["PROVEEDOR"] = (Registro["Proveedor"].ToString());
                    Row["MONTO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString());
                    Row["FECHA_CREO"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Creo].ToString());
                    Row["FECHA_RECHAZO"] = "" + DateTime.Now;

                    //  para el tipo de solicitud
                    Rs_Tipo_Solicitud.P_Tipo_Solicitud = (Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString());
                    Dt_Tipo_Solicitud = Rs_Tipo_Solicitud.Consulta_Tipo_Solicitud();

                    foreach (DataRow Tipo in Dt_Tipo_Solicitud.Rows)
                    {
                        Tipo_Solicitud = (Tipo[Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion].ToString());
                    }
                    Row["TIPO_SOLICITUD_PAGO_ID"] = Tipo_Solicitud;
                    Row["CONCEPTO_SOLICITUD"] = (Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString());
                    Row["COMENTARIO"] = Txt_Comentario.Text;
                    Usuario = (Registro[Ope_Con_Solicitud_Pagos.Campo_Usuario_Creo].ToString());
                    Usuario = Usuario.Replace(".", "");
                    Row["USUARIO_CREO"] = Usuario;
                    Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Usuario = Usuario.Replace(".", "");
                    Row["USUARIO_RECHAZO"] = Usuario;
                    Dt_Reporte.Rows.Add(Row);
                    Dt_Reporte.AcceptChanges();
                }
                Ds_Reporte.Clear();
                Ds_Reporte.Tables.Clear();
                Ds_Reporte.Tables.Add(Dt_Reporte.Copy());
                Reporte.Load(Ruta_Archivo + "Rpt_Con_Cancelacion_Recepcion.rpt");
                Reporte.SetDataSource(Ds_Reporte);
                DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                Nombre_Archivo += ".pdf";
                Ruta_Archivo = @Server.MapPath("../../Reporte/");
                m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                ExportOptions Opciones_Exportacion = new ExportOptions();
                Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);

                //Abrir_Ventana(Nombre_Archivo);
                //Mostrar_Reporte(Ruta_Archivo + Nombre_Archivo);
                Mostrar_Reporte(Nombre_Archivo);
                Inicializa_Controles();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";
            }
        }
        catch (SqlException Ex)
        {
            Trans.Rollback();
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Text = "Error:";
            Lbl_Mensaje_Error.Text = HttpUtility.HtmlDecode(Ex.Message);
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Visible = true;
        }
        finally
        {
            Cn.Close();
        }
    }
    protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
         DataTable Dt_Resultado = new DataTable();
         String Rol_Grupo = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
            }
            Rs_Consultar_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
            Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
            Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
            DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
            if (Dt_Grupo_Rol.Rows.Count > 0)
            {
                String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                if (Grupo_Rol == "00003")
                {
                    Rs_Consultar_Solicitud_Pagos.P_Dependencia_ID = "";
                    Rs_Consultar_Solicitud_Pagos.P_Grupo_Dependencia = Rol_Grupo;
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else
                {
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
            }
            else
            {
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
            }
            if (Dt_Resultado.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                Txt_Busqueda_No_Solicitud.Focus();
            }
            else {
                Grid_Solicitud_Pagos.Columns[2].Visible = true;
                Grid_Solicitud_Pagos.Columns[4].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                Grid_Solicitud_Pagos.DataBind();
                Grid_Solicitud_Pagos.Columns[2].Visible = false;
                Grid_Solicitud_Pagos.Columns[4].Visible = false;
                Txt_Busqueda_No_Solicitud.Text = "";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE: Txt_Buscar_No_Solicitud_TextChanged
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Sergio Manuel Gallardo Andrade
    /// FECHA CREÓ  : 30/Septiembre/2013
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Txt_Buscar_No_Solicitud_TextChanged(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Rs_Consultar_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Resultado = new DataTable();
        String Rol_Grupo = "";
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consultar_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
            }
            Rs_Consultar_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
            Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
            Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
            DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
            if (Dt_Grupo_Rol.Rows.Count > 0)
            {
                String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                if (Grupo_Rol == "00003")
                {
                    Rs_Consultar_Solicitud_Pagos.P_Dependencia_ID = "";
                    Rs_Consultar_Solicitud_Pagos.P_Grupo_Dependencia = Rol_Grupo;
                    Rs_Consultar_Solicitud_Pagos.P_Tipo_Recurso = "RECURSO ASIGNADO";
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
                else
                {
                    Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
                }
            }
            else
            {
                Dt_Resultado = Rs_Consultar_Solicitud_Pagos.Consulta_Solicitudes_SinAutorizar();
            }
            if (Dt_Resultado.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br />";
                Txt_Busqueda_No_Solicitud.Focus();
            }
            else
            {
                Grid_Solicitud_Pagos.Columns[2].Visible = true;
                Grid_Solicitud_Pagos.Columns[4].Visible = true;
                Grid_Solicitud_Pagos.DataSource = Dt_Resultado;
                Grid_Solicitud_Pagos.DataBind();
                Grid_Solicitud_Pagos.Columns[2].Visible = false;
                Grid_Solicitud_Pagos.Columns[4].Visible = false;
                Txt_Busqueda_No_Solicitud.Text = "";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            //Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Accion"]))
                {
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }

    #endregion

    #region"Grid"
    protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        HyperLink Hyp_Lnk_Ruta;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                if (!String.IsNullOrEmpty(e.Row.Cells[3].Text.Trim()) && e.Row.Cells[3].Text.Trim() != "&nbsp;")
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[3].Text.Trim();
                    Hyp_Lnk_Ruta.Enabled = true;
                }
                else
                {
                    Hyp_Lnk_Ruta.NavigateUrl = "";
                    Hyp_Lnk_Ruta.Enabled = false;
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Grid_Solicitud_Pagos_SelectedIndexChanged
        ///DESCRIPCIÓN          : Evento de seleccion de un registro del grid
        ///PARAMETROS           :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 22/Diciembre/2011
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN   :
        ///*******************************************************************************
        protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e) 
        {
            Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Documentos = new DataTable();

            try 
            {
                if (Grid_Solicitud_Pagos.SelectedIndex > (-1))
                {
                    Solicitud_Negocio.P_No_Solicitud_Pago = Grid_Solicitud_Pagos.SelectedRow.Cells[2].Text.Trim();
                    Dt_Detalles = Solicitud_Negocio.Consultar_Detalles();
                    Dt_Documentos = Solicitud_Negocio.Consulta_Documentos();

                    if(Dt_Detalles != null)
                    {
                        if(Dt_Detalles.Rows.Count > 0)
                        {
                            Txt_No_Pago_Det.Text = Dt_Detalles.Rows[0]["NO_SOLICITUD_PAGO"].ToString().Trim();
                            Txt_No_Reserva_Det.Text = Dt_Detalles.Rows[0]["NO_RESERVA"].ToString().Trim();
                            Txt_Concepto_Reserva_Det.Text = Dt_Detalles.Rows[0]["CONCEPTO_RESERVA"].ToString().Trim();
                            Txt_Beneficiario_Det.Text = Dt_Detalles.Rows[0]["BENEFICIARIO"].ToString().Trim();
                            Txt_Fecha_Solicitud_Det.Text = String.Format("{0:dd/MMM/yyyy HH:mm:ss}", Dt_Detalles.Rows[0]["FECHA_SOLICITUD"]);
                            Txt_Monto_Solicitud_Det.Text = String.Format("{0:c}", Dt_Detalles.Rows[0]["MONTO"]);
                            Grid_Solicitud_Pagos.SelectedIndex = -1;

                            Grid_Documentos.Columns[3].Visible = true;
                            Grid_Documentos.Columns[4].Visible = true;
                            Grid_Documentos.DataSource = Dt_Documentos;
                            Grid_Documentos.DataBind();
                            Grid_Documentos.Columns[3].Visible = false;
                            Grid_Documentos.Columns[4].Visible = false;
                            
                            Mpe_Detalles.Show();
                        }
                    }
                }
            }
            catch(Exception Ex)
            {
                throw new Exception("Error al tratar de seleccionar un registro de la tabla Error[" + Ex.Message + "]");
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Grid_Solicitud_Pagos_RowDataBound
        /// DESCRIPCION : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 09/enero/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Grid_Solicitud_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
        {
            try
            {
                CheckBox chek = (CheckBox)e.Row.FindControl("Chk_Autorizado");
                CheckBox chek2 = (CheckBox)e.Row.FindControl("Chk_Rechazado");
                if (e.Row.RowType == DataControlRowType.DataRow)
                {
                    if (e.Row.Cells[8].Text != "PENDIENTE")
                    {
                        chek.Enabled = false;
                        chek2.Enabled = false;
                    }
                    else
                    {
                        chek.Enabled = true;
                        chek2.Enabled = true;
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception(Ex.Message);
            }
        }
    #endregion

       
}
