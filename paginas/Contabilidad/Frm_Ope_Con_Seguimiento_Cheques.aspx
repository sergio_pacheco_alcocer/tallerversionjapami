﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Seguimiento_Cheques.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Seguimiento_Cheques" Title="Seguimiento de Cheques" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager1" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true">
    </cc1:ToolkitScriptManager>
    <div id="Div_General" style="width: 98%;" visible="true" runat="server">
         <asp:UpdatePanel ID="Upl_Contenedor" runat="server">
            <ContentTemplate>
                <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upl_Contenedor"
                    DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter">
                        </div>
                        <div class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>                    
                </asp:UpdateProgress>
                 <div id="Div_Encabezado" runat="server">
                    <table style="width: 100%;" border="0" cellspacing="0">
                        <tr align="center">
                            <td colspan="2" class="label_titulo">
                                Seguimiento de Cheques
                            </td>
                        </tr>
                        <tr align="left">
                            <td colspan="2">
                                <asp:Image ID="Img_Warning" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" ForeColor="#990000"></asp:Label>
                            </td>
                        </tr>
                        <tr class="barra_busqueda" align="right">
                            <td align="left" valign="middle">
                                <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Inicio" OnClick="Btn_Salir_Click" />
                            </td>
                            <td>
                            </td>
                        </tr>
                    </table>
                 </div>
                 <div id="Div_Listado_Cheques" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width:15%">
                                No. Cheque
                            </td>
                            <td style="width:30%">
                                <asp:TextBox ID="Txt_No_Cheque" runat="server" Width="95%"></asp:TextBox>
                            </td>
                            <td></td>
                        </tr>
                        <tr>
                            <td style="width:15%">
                                Fecha Edición
                            </td>
                            <td colspan="2">
                                 <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                    ValidChars="/_" />
                                <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                &nbsp;&nbsp;&nbsp;&nbsp;al &nbsp;&nbsp;&nbsp;&nbsp;
                                <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="85px" Enabled="false"></asp:TextBox>
                                <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                            </td>
                        </tr>
                        <tr>
                            <td  style="width:15%">
                                Etapa
                            </td>
                            <td style="width:30%">
                                <asp:DropDownList ID="Cmb_Etapa" runat="server" Width="95%">
                                </asp:DropDownList>
                            </td>
                            <td style="width:15%">
                                Tipo Beneficiario
                            </td>
                            <td colspan="2">
                                <asp:DropDownList ID="Cmb_Tipo_Beneficiario" runat="server" Width="89%">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                           <td style="width:15%">
                                Beneficiario
                           </td>
                           <td colspan="3">
                               <asp:TextBox ID="Txt_Beneficiario" runat="server" Width="45%" 
                                   ontextchanged="Txt_Beneficiario_TextChanged" AutoPostBack="true"></asp:TextBox>
                               <asp:DropDownList ID="Cmb_Beneficiario" Width="50%" runat="server" 
                                   onselectedindexchanged="Cmb_Beneficiario_SelectedIndexChanged" AutoPostBack="true" > 
                               </asp:DropDownList>
                           </td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right"> <asp:ImageButton ID="Btn_Buscar" runat="server" 
                                    ImageUrl="~/paginas/imagenes/paginas/busqueda.png" OnClick="Btn_Buscar_Click" 
                                    ToolTip="Consultar" /> 
                            </td>
                        </tr>
                        <tr>
                           <td align="center" colspan="4" style="width: 99%">
                               <div style="overflow:auto;height:320px;width:99%;vertical-align:top;
                                   border-style:outset;border-color: Silver;">
                                   <asp:GridView ID="Grid_Cheques" runat="server" AllowSorting="true" 
                                            AutoGenerateColumns="False" CssClass="GridView_1" DataKeyNames="No_Cheque" 
                                            EmptyDataText="No se encontraron cheques" GridLines="None" 
                                            HeaderStyle-CssClass="tblHead" Width="100%">
                                            <RowStyle CssClass="GridItem" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="">
                                                    <ItemTemplate>
                                                        <asp:ImageButton ID="Btn_Seleccionar_Cheque" runat="server" 
                                                            CommandArgument='<%# Eval("No_Cheque") %>' 
                                                            ImageUrl="~/paginas/imagenes/gridview/blue_button.png" 
                                                            OnClick="Btn_Seleccionar_Cheque_Click" />
                                                    </ItemTemplate>
                                                    <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="5%" />
                                                </asp:TemplateField>
                                                 <asp:BoundField DataField="No_Cheque" HeaderText="No. Cheque" 
                                                     Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Folio" HeaderText="Folio" 
                                                     Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="12%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Beneficiario"
                                                    HeaderText="Beneficiario" Visible="True">
                                                    <HeaderStyle HorizontalAlign="Left" Wrap="true" />
                                                    <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Concepto" HeaderText="Concepto" 
                                                     Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Importe" HeaderText="Importe" 
                                                     Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                                     Visible="True">
                                                    <FooterStyle HorizontalAlign="Left" />
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="No_Cheque" HeaderText="ID" Visible="false">
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <ItemStyle HorizontalAlign="Left" />
                                                </asp:BoundField>
                                            </Columns>
                                            <PagerStyle CssClass="GridHeader" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                            <HeaderStyle CssClass="GridHeader" />
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                        </asp:GridView>
                               </div>
                           </td>
                        </tr>
                    </table>
                 </div>
                 <div id="Div_Contenido" runat="server">
                    <table style="width: 100%;">
                        <tr>
                            <td style="width: 15%;" align="left">
                                No. Cheque 
                            </td>
                            <td style="width:25%;"><asp:TextBox ID="Txt_No_Cheque_Contenido" runat="server" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Estatus Actual
                            </td>
                            <td style="width:25%;"><asp:TextBox ID="Txt_Estatus_Contenido" runat="server" Width="90%"></asp:TextBox>
                            </td>
                            <td style="width: 15%;" align="left">
                                Importe
                            </td>
                            <td style="width:25%;"><asp:TextBox ID="Txt_Importe_Contenido" runat="server" Width="90%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td style="width: 15%;" align="left">
                                Beneficiario 
                            </td>
                            <td colspan="3"><asp:TextBox ID="Txt_Beneficiario_Contenido" runat="server" Width="96.8%"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="4">
                                <asp:GridView ID="Grid_Detalle_Cheques" runat="server" 
                                    AutoGenerateColumns="False" CssClass="GridView_1"  
                                    GridLines="None" Width="100%"                                     
                                    style="white-space:normal" >
                                    <RowStyle CssClass="GridItem" />
                                    <Columns>
                                        <asp:BoundField DataField="Estatus_Entrada" HeaderText="Estatus_Entrada" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Fecha_Entrada" HeaderText="Fecha Entrada" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Estatus_salida" HeaderText="Estatus Salida" Visible="True">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="15%" />
                                        </asp:BoundField>                                 
                                        <asp:BoundField DataField="Fecha_salida" HeaderText="Fecha Salida" Visible="True" DataFormatString="{0:dd/MMM/yyyy}">
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="11%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Empleado" HeaderText="Empleado" Visible="True" >
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left" Width="22%"/>
                                        </asp:BoundField>
                                        <asp:BoundField DataField="Comentarios" HeaderText="Comentarios" Visible="True" >
                                            <FooterStyle HorizontalAlign="Left" />
                                            <HeaderStyle HorizontalAlign="Left" />
                                            <ItemStyle HorizontalAlign="Left"/>
                                        </asp:BoundField>
                                    </Columns>
                                    <PagerStyle CssClass="GridHeader" />
                                    <SelectedRowStyle CssClass="GridSelected" />
                                    <HeaderStyle CssClass="GridHeader" />
                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </td>
                        </tr>
                     </table>
                   </div>
            </ContentTemplate>
         </asp:UpdatePanel> 
    </div>
</asp:Content>
