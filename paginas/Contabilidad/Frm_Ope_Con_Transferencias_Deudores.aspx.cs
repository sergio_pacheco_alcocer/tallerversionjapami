﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Solicitud_Pago.Negocio;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Contabilidad_Reporte_Situacion_Financiera.Negocio;
using JAPAMI.Contabilidad_Transferencia.Negocio;
using JAPAMI.Cheque.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Autoriza_Solicitud_Pago_Contabilidad.Negocio;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.SAP_Partidas_Especificas.Negocio;
using JAPAMI.Catalogo_SAP_Fuente_Financiamiento.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Area_Funcional.Negocio;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Compromisos_Contabilidad.Negocios;
using JAPAMI.Generar_Reservas.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;
public partial class paginas_Contabilidad_Frm_Ope_Con_Transferencias_Deudores : System.Web.UI.Page
{
    #region "Page_Load"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Page_Load
    /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Refresca la session del usuario lagueado al sistema.
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            //Valida que exista algun usuario logueado al sistema.
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                Llenar_Combo_Banco();
                Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
                //Cmb_Anio_Contable.SelectedValue = nuevo;
                //Llenar_Grid_Cierres_generales(nuevo);
                Acciones();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region "Metodos"
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Limpia_Controles();             //Limpia los controles del forma
            Llenar_Grid_Solicitudes();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
        //*******************************************************************************
    // NOMBRE DE LA FUNCIÓN: Llenar_Combos_Generales()
    // DESCRIPCIÓN: Llena los combos principales de la interfaz de usuario
    // RETORNA: 
    // CREO: Sergio Manuel Gallardo Andrade
    // FECHA_CREO: 17/Noviembre/2011 
    // MODIFICO:
    // FECHA_MODIFICO:
    // CAUSA_MODIFICACIÓN:
    //********************************************************************************/
    public void Llenar_Combo_Banco()
    {
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
        Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///                para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                           si es una alta, modificacion
    ///                           
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = true;
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Configuracion_Acceso("Frm_Ope_Con_Transferencias_Deudores.aspx");
                    break;
            }
            //Cmb_Anio_Contable.Enabled = Habilitado;
            //Cmb_Mes_Contable.Enabled = Habilitado;
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Cmb_Banco_Transferencia_OnSelectedIndexChanged
    ///DESCRIPCIÓN: habilita el siguiente combo y pasa la informacion de la clave
    ///PARAMETROS: 
    ///CREO:        Sergio Manuel Gallardo
    ///FECHA_CREO:  31/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Cmb_Banco_Transferencia_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        DataTable Dt_Modificada = new DataTable();
        Boolean Agregar;
        Double Total;
        String DEUDOR_ID = "";
        String TIPO_DEUDOR = "";
        String Cuenta;
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consulta_Porpagar = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_Resultado = new DataTable();
        Lbl_Mensaje_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
        Img_Error.Visible = false;
        try
        {
            if (Cmb_Banco.SelectedValue != "0")
            {
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                if (Dt_Consulta.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                    {
                        Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                        Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                    }
                    else
                    {
                        Txt_Comision_Banco.Text = "";
                        Txt_Iva_Comision.Text = "";
                        Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Txt_Comision_Banco.Text = "0";
                        Txt_Iva_Comision.Text = "0";
                    }
                }
                else
                {
                    Txt_Comision_Banco.Text = "";
                    Txt_Iva_Comision.Text = "";
                    Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Txt_Comision_Banco.Text = "0";
                    Txt_Iva_Comision.Text = "0";
                }
                Rs_Consulta_Porpagar.P_Banco = Cmb_Banco.SelectedValue;
                Dt_Resultado = Rs_Consulta_Porpagar.Consultar_Solicitud_Transferencia();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        DEUDOR_ID = Fila["DEUDOR_ID"].ToString();
                        TIPO_DEUDOR = Fila["TIPO_DEUDOR"].ToString();
                        Cuenta = Fila["CUENTA_BANCO_PAGO"].ToString();
                        if (Dt_Modificada.Rows.Count <= 0)
                        {
                            Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                            Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                            Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                            Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                        }
                        DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        Agregar = true;
                        Total = 0;
                        //insertar los que no se repiten
                        foreach (DataRow Fila2 in Dt_Modificada.Rows)
                        {
                            if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                            {
                                Agregar = false;
                            }
                        }
                        //se calcula el monto por tipo
                        foreach (DataRow Renglon in Dt_Resultado.Rows)
                        {
                            if (DEUDOR_ID.Equals(Renglon["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Renglon["TIPO_DEUDOR"].ToString()))
                            {
                                Total = Total + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                            }
                        }
                        if (Agregar && Total > 0)
                        {
                            row["DEUDOR_ID"] = DEUDOR_ID;
                            row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                            row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                            row["MONTO"] = Total;
                            row["CUENTA"] = Fila["CUENTA_BANCO_PAGO"].ToString().Trim();
                            row["ESTATUS"] = Fila["Estatus"].ToString();
                            if (TIPO_DEUDOR == "EMPLEADO")
                            {
                                row["IDENTIFICADOR_TIPO"] = "E-" + DEUDOR_ID + Total;
                            }
                            else
                            {
                                row["IDENTIFICADOR_TIPO"] = "P-" + DEUDOR_ID + Total;
                            }

                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                    }
                    Session["Dt_Datos"] = Dt_Resultado;
                    Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                    Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                else
                {
                    Grid_Pagos.DataSource = null;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                }

            }
            else
            {
                Txt_Comision_Banco.Text = "";
                Txt_Iva_Comision.Text = "";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade 
    /// FECHA_CREO  : 15/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Txt_Busqueda_No_Solicitud.Text = "";
            Txt_Monto_Solicitud.Value = "";
            Txt_Cuenta_Contable_ID_Proveedor.Value = "";
            Txt_Cuenta_Contable_reserva.Value = "";
            Txt_No_Reserva.Value = "";
            Grid_Pagos.DataSource = null;
            Grid_Pagos.DataBind();
            Session["Dt_Datos_Proveedor"] = null;
            Session["Dt_Datos"] = null;
            Session["Contador"] = null; 
            Session["Dt_Datos_Solicitud"] = null;
            Session["Dt_Datos_Det"] = null;
            Session["Contador_Solicitud"] = null;
            //Txt_Comentario.Text = "";
            //Txt_No_Solicitud_Autorizar.Value = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes
    /// DESCRIPCION : Llena el grid Solicitudes de pago
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 15/noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Grid_Solicitudes()
    {
        try
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consulta_Porpagar = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
            Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos= new Cls_Ope_Con_Transferencia_Negocio();
            DataTable Dt_Resultado = new DataTable();
            DataTable Dt_Modificada = new DataTable();
            Boolean Agregar;
            Double Total;
            String TIPO_DEUDOR="";
            String DEUDOR_ID = "";
            String Cuenta;
            DataTable Dt_Datos_Proveedor = new DataTable();
            Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
            DataTable Dt_Datos = new DataTable();
            Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
            Int32 Contador = 0;
            Session["Contador"] = Contador;
            Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
            DataTable Dt_Consulta = new DataTable();
            //Rs_Autoriza_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            //Dt_Resultado = Rs_Consulta_Ejercidos.Consultar_Solicitud_Transferencia();
            Dt_Resultado = Rs_Consulta_Porpagar.Consultar_Solicitud_Transferencia();
            Grid_Pagos.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
            Grid_Pagos.DataBind();    // Se ligan los datos.;

            if (Dt_Resultado.Rows.Count > 0)
            {
                //Consultamos el banco con la primera traferencia
                Cmb_Banco.SelectedValue = Dt_Resultado.Rows[0]["Nombre_Banco"].ToString();
                Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                if (Dt_Consulta.Rows.Count > 0)
                {
                    if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                    {
                        Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                        Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                    }
                    else
                    {
                        Txt_Comision_Banco.Text = "";
                        Txt_Iva_Comision.Text = "";
                        Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }

                Rs_Consulta_Ejercidos.P_Banco = Cmb_Banco.SelectedValue;
                Dt_Resultado = Rs_Consulta_Porpagar.Consultar_Solicitud_Transferencia();
                foreach (DataRow Fila in Dt_Resultado.Rows)
                {
                        DEUDOR_ID= Fila["DEUDOR_ID"].ToString();
                        TIPO_DEUDOR = Fila["TIPO_DEUDOR"].ToString();
                    Cuenta = Fila["CUENTA_BANCO_PAGO"].ToString();
                    if (Dt_Modificada.Rows.Count <= 0)
                    {
                        Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                        Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                        Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                        Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                        Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                        Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                        Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                    }
                    DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                    Agregar = true;
                    Total = 0;
                    //insertar los que no se repiten
                    foreach (DataRow Fila2 in Dt_Modificada.Rows)
                    {
                      if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                       {
                         Agregar = false;
                       }
                    }
                    //se calcula el monto por tipo
                    foreach (DataRow Renglon in Dt_Resultado.Rows)
                    {
                        if (DEUDOR_ID.Equals(Renglon["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Renglon["TIPO_DEUDOR"].ToString()))
                        {
                            Total = Total + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                        }
                    }
                    if (Agregar && Total > 0)
                    {
                        row["DEUDOR_ID"] = DEUDOR_ID;
                        row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                        row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                        row["MONTO"] = Total;
                        row["CUENTA"] = Fila["CUENTA_BANCO_PAGO"].ToString().Trim();
                        row["ESTATUS"] = Fila["Estatus"].ToString();
                        if (TIPO_DEUDOR=="EMPLEADO")
                        {
                            row["IDENTIFICADOR_TIPO"] ="E-" +DEUDOR_ID  + Total;
                        }
                        else
                        {
                            row["IDENTIFICADOR_TIPO"] = "P-"+DEUDOR_ID + Total;
                        }
                        
                        Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Modificada.AcceptChanges();
                    }
                }
                Session["Dt_Datos"] = Dt_Resultado;
                Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                Grid_Pagos.Columns[6].Visible = true;
                Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                Grid_Pagos.DataBind();    // Se ligan los datos.;
                Grid_Pagos.Columns[6].Visible = false;
                //Grid_Solicitud_Pagos.Columns[3].Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Pagos_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Pagos_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        TextBox Txt_Fecha;
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        DataTable Dt_Datos_Solicitud = new DataTable();
        Dt_Datos_Solicitud = ((DataTable)(Session["Dt_Datos_Solicitud"]));
        DataTable Dt_Datos_Det = new DataTable();
        Dt_Datos_Det = ((DataTable)(Session["Dt_Datos_Det"]));
        Int32 Contador_Solicitud = 0;
        Session["Contador_Solicitud"] = Contador_Solicitud;
        String TIPO_DEUDOR = "";
        String Cuenta = "";
        Int32 Contador;
        String DEUDOR_ID = "";
        Boolean Agregar;
        Double Total;
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (Int32)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos"]));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Proveedor"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    Cuenta = Convert.ToString(Filas["CUENTA_BANCO_PAGO"].ToString()).Trim();
                    DEUDOR_ID = Dt_Datos.Rows[Contador]["DEUDOR_ID"].ToString();
                    TIPO_DEUDOR = Dt_Datos.Rows[Contador]["TIPO_DEUDOR"].ToString();
                    Dt_Datos_Detalles.DefaultView.RowFilter = "DEUDOR_ID='" + DEUDOR_ID + "' and TIPO_DEUDOR='" + TIPO_DEUDOR + "'";
                    
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[5].FindControl("Grid_Cuentas");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            //Cuenta = Convert.ToString(Fila["CUENTA_BANCO_PAGO_ID"].ToString()).Trim();
                            if (Dt_Modificada.Rows.Count <= 0 && Dt_Modificada.Columns.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("CUENTA_BANCO_ID", typeof(System.String));
                                Dt_Modificada.Columns.Add("NO_CUENTA", typeof(System.String));
                                Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                                Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                Dt_Modificada.Columns.Add("BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("ORDEN", typeof(System.String));
                                Dt_Modificada.Columns.Add("IDENTIFICADOR", typeof(System.String));
                                Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            Agregar = true;
                            Total = 0;
                            //insertar los que no se repiten
                            foreach (DataRow Fila2 in Dt_Modificada.Rows)
                            {
                                if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && Cuenta.Equals(Fila2["CUENTA_BANCO_ID"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                                    {
                                        Agregar = false;
                                    }
                               
                            }
                            //se calcula el monto por tipo
                            foreach (DataRow Renglon in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                            {
                                if (DEUDOR_ID.Equals(Renglon["DEUDOR_ID"].ToString()) && Cuenta.Equals(Renglon["CUENTA_BANCO_PAGO"].ToString()) && TIPO_DEUDOR.Equals(Renglon["TIPO_DEUDOR"].ToString()))
                                    {
                                        Total = Total + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                                    }
                            }
                            if (Agregar && Total > 0)
                            {
                                row["DEUDOR_ID"] = DEUDOR_ID;
                                row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                                row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                                row["MONTO"] = Total;
                                row["ESTATUS"] = Fila["Estatus"].ToString();
                                row["BANCO"] = Fila["NOMBRE_BANCO"].ToString();
                                row["CUENTA_BANCO_ID"] = Cuenta;
                                row["NO_CUENTA"] = Fila["NO_CUENTA"].ToString().Trim();
                                row["ORDEN"] = Fila["TRANSFERENCIA"].ToString();
                                row["IDENTIFICADOR"] = DEUDOR_ID + Cuenta;
                                Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Modificada.AcceptChanges();
                            }
                        }
                    }
                }
                Session["Dt_Datos_Solicitud"] = Dt_Modificada;
                Session["Dt_Datos_Det"] = Dt_Datos_Detalles;
                Gv_Detalles.Columns[9].Visible=true;
                Gv_Detalles.DataSource = Dt_Modificada;
                Gv_Detalles.DataBind();
                 Gv_Detalles.Columns[9].Visible=false;
                Session["Contador"] = Contador + 1;
                Txt_Fecha = (TextBox)e.Row.Cells[3].FindControl("Txt_Fecha_Poliza");
                Txt_Fecha.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();

            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Dt_Datos = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        DataTable Dt_Temporal = new DataTable();
        DataTable Dt_Modificada = new DataTable();
        String DEUDOR_ID = "";
        String Cuenta = "";
        String No_Solicitud;
        Int32 Contador_Solicitud;
        String TIPO_DEUDOR = "";
        Cls_Ope_Con_Cheques_Negocio consulta_Negocio = new Cls_Ope_Con_Cheques_Negocio();
        Image Img = new Image();
        Img = (Image)e.Row.FindControl("Img_Btn_Expandir_Cuenta");
        Literal Lit = new Literal();
        Lit = (Literal)e.Row.FindControl("Ltr_Inicio2");
        Label Lbl_facturas = new Label();
        Lbl_facturas = (Label)e.Row.FindControl("Lbl_Solicitud");
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Datos_Deudor = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Cat_Com_Proveedores_Negocio Proveedor = new Cls_Cat_Com_Proveedores_Negocio();
        DataTable Dt_Banco_Proveedor = new DataTable();
        TextBox Txt_Ref_Comision;
        TextBox Txt_Ref_IVA;
        TextBox Txt_Ref;
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador_Solicitud = (Int32)(Session["Contador_Solicitud"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Det"]));
                Dt_Datos = ((DataTable)(Session["Dt_Datos_Solicitud"]));
                foreach (DataRow Filas in Dt_Datos_Detalles.Rows)
                {
                    No_Solicitud = Filas["NO_DEUDA"].ToString();
                    DEUDOR_ID = Dt_Datos.Rows[Contador_Solicitud]["DEUDOR_ID"].ToString();
                    //Proveedor_ID = Dt_Datos.Rows[Contador_Solicitud]["PROVEEDOR_ID"].ToString();
                    Cuenta = Convert.ToString(Dt_Datos.Rows[Contador_Solicitud]["CUENTA_BANCO_ID"].ToString()).Trim();
                    TIPO_DEUDOR = Dt_Datos.Rows[Contador_Solicitud]["TIPO_DEUDOR"].ToString();
                    Dt_Datos_Detalles.DefaultView.RowFilter = "DEUDOR_ID='" + DEUDOR_ID + "' AND CUENTA_BANCO_PAGO='" + Cuenta + "' AND NO_DEUDA='" + No_Solicitud + "' AND TIPO_DEUDOR='" + TIPO_DEUDOR + "'";
                    if (Dt_Datos_Detalles.DefaultView.ToTable().Rows.Count > 0)
                    {
                        Gv_Detalles = (GridView)e.Row.Cells[4].FindControl("Grid_Datos_Solicitud");
                        foreach (DataRow Fila in Dt_Datos_Detalles.DefaultView.ToTable().Rows)
                        {
                            if (Dt_Modificada.Rows.Count <= 0)
                            {
                                Dt_Modificada.Columns.Add("NO_DEUDA", typeof(System.String));
                                Dt_Modificada.Columns.Add("CONCEPTO", typeof(System.String));
                                Dt_Modificada.Columns.Add("TRANSFERENCIA", typeof(System.String));
                                Dt_Modificada.Columns.Add("NOMBRE_BANCO", typeof(System.String));
                                Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                                Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            }
                            DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                            row["NO_DEUDA"] = Fila["NO_DEUDA"].ToString();
                            row["CONCEPTO"] = Fila["CONCEPTO"].ToString();
                            row["TRANSFERENCIA"] = Fila["TRANSFERENCIA"].ToString();
                            row["NOMBRE_BANCO"] = Fila["NOMBRE_BANCO"].ToString();
                            row["MONTO"] = Fila["IMPORTE"].ToString();
                            row["ESTATUS"] = Fila["ESTATUS"].ToString();
                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                        Gv_Detalles.Columns[2].Visible = true;
                        Gv_Detalles.Columns[3].Visible = true;
                        Gv_Detalles.DataSource = Dt_Modificada;
                        Gv_Detalles.DataBind();
                        Gv_Detalles.Columns[2].Visible = false;
                        Gv_Detalles.Columns[3].Visible = false;
                    }
                }
                Session["Contador_Solicitud"] = Contador_Solicitud + 1;
                Datos_Deudor.P_Deudor_ID = e.Row.Cells[9].Text.Trim();
                Datos_Deudor.P_Tipo_Deudor = TIPO_DEUDOR;
                Dt_Banco_Proveedor = Datos_Deudor.Consulta_Datos_Deudor();
                if (Dt_Banco_Proveedor.Rows.Count > 0)
                {
                    Txt_Ref_Comision = (TextBox)e.Row.Cells[5].FindControl("Txt_Referencia_Comision");
                    Txt_Ref_IVA = (TextBox)e.Row.Cells[6].FindControl("Txt_Referencia_IVA_Comision");
                    Txt_Ref = (TextBox)e.Row.Cells[4].FindControl("Txt_Beneficiario_Grid");
                        if (Dt_Banco_Proveedor.Rows[0]["BANCO_DEUDOR"].ToString() == e.Row.Cells[3].Text.Trim())
                        {
                            Txt_Ref_Comision.Style.Add("background-color", "#E6E6E6");
                            Txt_Ref_IVA.Style.Add("background-color", "#E6E6E6");
                            Txt_Ref_Comision.Enabled = false;
                            Txt_Ref_IVA.Enabled = false;

                        }
                        else
                        {
                            Txt_Ref_Comision.Style.Add("background-color", "#A9F5A9");
                            Txt_Ref_IVA.Style.Add("background-color", "#A9F5A9");
                            Txt_Ref_Comision.Enabled = true;
                            Txt_Ref_IVA.Enabled = true;
                        }
                    Txt_Ref.Style.Add("background-color", "#A9F5A9");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:Accion
    //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Acciones()
    {

      //  Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
        String Accion = String.Empty;
        String No_Solicitud = String.Empty;
        String Comentario = String.Empty;
        DataTable Dt_Consulta_Estatus = new DataTable();
        DataTable Dt_Datos_Polizas = new DataTable();
        Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud= new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        if (Request.QueryString["Accion"] != null)
        {
            Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
            if (Request.QueryString["id"] != null)
            {
                No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                
            }
            switch (Accion)
            {
                case "Rechazar_Transferencia":
                    Rs_Solicitud.P_No_Deuda = No_Solicitud;
                    Rs_Solicitud.Modificar_Transferencia();
                    //Inicializa_Controles();
                    break;
            }
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Alta_Transferencia
    /// DESCRIPCION : Da de Alta el Cheque con los datos proporcionados
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 21/Noviembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Alta_Transferencia(DataTable Dt_Solicitudes)
    {
        DataTable Dt_C_Proveedor = new DataTable();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Reserva = new DataTable();
        DataTable Dt_Solicitudes_Modificada = new DataTable();
        DataTable Dt_Solicitudes_Completa = new DataTable();
        DataTable Dt_Solicitudes_Partidas = new DataTable();
        DataTable Dt_Datos_Deudor = new DataTable();
        DataTable Dt_Banco_Cuenta_Contable = new DataTable();
        DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
        DataTable Dt_Solicitud_Pagos = new DataTable(); //Variable a contener los datos de la consulta de la solicitud de pago
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_Proveedor = new Cls_Cat_Con_Cuentas_Contables_Negocio();
        Cls_Ope_Con_Cheques_Negocio Rs_Ope_Con_Cheques = new Cls_Ope_Con_Cheques_Negocio(); //Variable de conexion con la capa de datos
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Alta_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consulta = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_Consulta_Solicitud_Pago = new DataTable();
        //String Estatus_Reserva = "";
        String Pago = "";
        String Cuenta_Contable_Deudor = "";
        String Cuenta = "";
        String Deudor = "";
        String Banco = "";
        String Cuenta_Contable_ID_Banco = "";
        String Cuenta_Servicios_financieros = "";
        String Cuenta_Contable_Serv_Financieros = "";
        String Tipo_Deudor;
        Int32 partida = 0;
        Boolean Agregar;
        Double Total;
        String Nombre_Proveedor="";
        String Fecha_Pago = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
        String Bandera="1";
        try
        {
            if (Dt_Solicitudes_Modificada.Rows.Count <= 0)
            {
                Dt_Solicitudes_Modificada.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Deudor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Importe", typeof(System.Double));
                Dt_Solicitudes_Modificada.Columns.Add("Banco_Proveedor", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Modificada.Columns.Add("Tipo_Deudor", typeof(System.String));
            }
            foreach (DataRow Fila in Dt_Solicitudes.Rows)
            {
                Rs_Consulta.P_Deudor_ID = Fila["Deudor"].ToString();
                Rs_Consulta.P_Tipo_Deudor = Fila["Tipo_Deudor"].ToString();
                Dt_Datos_Deudor = Rs_Consulta.Consulta_Datos_Solicitud_Pago();
                if (Dt_Datos_Deudor.Rows.Count > 0)
                    {
                        Nombre_Proveedor = Dt_Datos_Deudor.Rows[0]["NOMBRE"].ToString();
                        DataRow Rows = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        Rows["No_Solicitud"] = Fila["No_Solicitud"].ToString();
                        Rows["Referencia"] = Fila["Referencia"].ToString();
                        Rows["Banco"] = Fila["Banco"].ToString();
                        Rows["cuenta"] = Fila["cuenta"].ToString();
                        Rows["Deudor"] = Fila["Deudor"].ToString();
                        Rows["Importe"] = Fila["Importe"].ToString();
                        Rows["Banco_Proveedor"] = Dt_Datos_Deudor.Rows[0]["BANCO_DEUDOR"].ToString();
                        Rows["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString();
                        Rows["Ref_Comision"] = Fila["Ref_Comision"].ToString();
                        Rows["Ref_IVA"] = Fila["Ref_IVA"].ToString();
                        Rows["Tipo_Deudor"] = Fila["Tipo_Deudor"].ToString();
                        Dt_Solicitudes_Modificada.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Solicitudes_Modificada.AcceptChanges();
                    }               
            }
            if (Dt_Solicitudes_Completa.Rows.Count <= 0)
            {
                Dt_Solicitudes_Completa.Columns.Add("No_Solicitud", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Referencia", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Deudor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Importe", typeof(System.Double));
                Dt_Solicitudes_Completa.Columns.Add("Banco_Deudor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Cuenta_Contable_Deudor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Monto_Transferencia", typeof(System.Double));
                Dt_Solicitudes_Completa.Columns.Add("Fecha_Poliza", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_Comision", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Ref_IVA", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Deudor", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Beneficiario_ID", typeof(System.String));
                Dt_Solicitudes_Completa.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
            }
            foreach (DataRow Renglon in Dt_Solicitudes_Modificada.Rows)
            {
                DataRow ROW = Dt_Solicitudes_Completa.NewRow(); //Crea un nuevo registro a la tabla
                ROW["No_Solicitud"] = Renglon["No_Solicitud"].ToString();
                ROW["Referencia"] = Renglon["Referencia"].ToString();
                ROW["Banco"] = Renglon["Banco"].ToString();
                ROW["cuenta"] = Renglon["cuenta"].ToString();
                ROW["Deudor"] = Renglon["Deudor"].ToString();
                ROW["Importe"] = Renglon["Importe"].ToString();
                ROW["Banco_Deudor"] = Renglon["Banco_Proveedor"].ToString();
                ROW["Fecha_Poliza"] = Renglon["Fecha_Poliza"].ToString();
                Rs_Ope_Con_Cheques.P_Banco_ID = Renglon["Cuenta"].ToString();
                Dt_Banco_Cuenta_Contable = Rs_Ope_Con_Cheques.Consulta_Cuenta_Contable_Banco();
                if (Dt_Banco_Cuenta_Contable.Rows.Count > 0)// foreach (DataRow Renglon in Dt_Banco_Cuenta_Contable.Rows)
                {
                    ROW["Cuenta_Contable_ID_Banco"] = Cuenta_Contable_ID_Banco = Dt_Banco_Cuenta_Contable.Rows[0][Cat_Nom_Bancos.Campo_Cuenta_Contable_ID].ToString();
                }
                //  para Obtener la cuenta Contable del proveedor la cuenta contable
                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Deuda = Renglon["No_Solicitud"].ToString(); ;
                Dt_Solicitud_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago();
                foreach (DataRow Registro in Dt_Solicitud_Pagos.Rows)
                {
                    if(!String.IsNullOrEmpty(Registro["CUENTA_DEUDOR"].ToString())){
                        ROW["Cuenta_Contable_Deudor"] = Registro["CUENTA_DEUDOR"].ToString();
                        ROW["Beneficiario_ID"] = Registro[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                        ROW["Tipo_Beneficiario_Poliza"] = Registro[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString();
                    }else{
                        Rs_Cuentas_Proveedor.P_Cuenta = "112300001";
                         Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                         if (Dt_C_Proveedor.Rows.Count > 0)
                         {
                             ROW["Cuenta_Contable_Deudor"] = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                             ROW["Beneficiario_ID"] = Registro[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                             ROW["Tipo_Beneficiario_Poliza"] = Registro[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString();
                         }
                    }
                }
                if (!String.IsNullOrEmpty(Renglon["Ref_Comision"].ToString()) && !String.IsNullOrEmpty(Renglon["Ref_IVA"].ToString()))
                {
                    ROW["Monto_Transferencia"] = Convert.ToString(Convert.ToDouble(ROW["IMPORTE"]) + Convert.ToDouble(Txt_Comision_Banco.Text.ToString()) + Convert.ToDouble(Txt_Iva_Comision.Text.ToString()));
                }
                else
                {
                    ROW["Monto_Transferencia"] = Convert.ToString(Convert.ToDouble(ROW["IMPORTE"]));
                }
                ROW["Ref_Comision"] = Renglon["Ref_Comision"].ToString();
                ROW["Ref_IVA"] = Renglon["Ref_IVA"].ToString();
                ROW["Tipo_Deudor"] = Renglon["Tipo_Deudor"].ToString();
                Dt_Solicitudes_Completa.Rows.Add(ROW); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Solicitudes_Completa.AcceptChanges();
            }
            Deudor = "";
            Banco ="";
            Cuenta = "";
            Cuenta_Contable_ID_Banco = "";
            Cuenta_Contable_Deudor = "";
            Tipo_Deudor = "";
            // Se agrupan los pagos por proveedor y cuenta 
            foreach (DataRow Fila in Dt_Solicitudes_Completa.Rows)
            {
                if (Dt_Solicitudes_Partidas.Rows.Count <= 0 && Dt_Solicitudes_Partidas.Columns.Count <= 0)
                {
                    Dt_Solicitudes_Partidas.Columns.Add("Referencia", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Deudor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Importe", typeof(System.Double));
                    Dt_Solicitudes_Partidas.Columns.Add("Banco_Deudor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_ID_Banco", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Cuenta_Contable_Deudor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Fecha_Poliza", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_Comision", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Ref_IVA", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Deudor", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Beneficiario_ID", typeof(System.String));
                    Dt_Solicitudes_Partidas.Columns.Add("Tipo_Beneficiario_Poliza", typeof(System.String));
                }
                DataRow Filas = Dt_Solicitudes_Partidas.NewRow(); //Crea un nuevo registro a la tabla
                Agregar = true;
                Total = 0;
                Deudor = Fila["Deudor"].ToString();
                Banco = Fila["Banco"].ToString();
                Cuenta = Fila["Cuenta"].ToString();
                Cuenta_Contable_ID_Banco = Fila["Cuenta_Contable_ID_Banco"].ToString();
                Cuenta_Contable_Deudor = Fila["Cuenta_Contable_Deudor"].ToString();
                Tipo_Deudor = Fila["Tipo_Deudor"].ToString();
                //insertar los que no se repiten
                foreach (DataRow Fila2 in Dt_Solicitudes_Partidas.Rows)
                {
                        if (Deudor.Equals(Fila2["Deudor"].ToString()) && Cuenta.Equals(Fila2["CUENTA"].ToString()) && Banco.Equals(Fila2["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Fila2["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Deudor.Equals(Fila2["Cuenta_Contable_Deudor"].ToString()) && Tipo_Deudor.Equals(Fila2["Tipo_Deudor"].ToString()))
                        {
                            Agregar = false;
                        }
                }
                //se calcula el monto por tipo
                foreach (DataRow Renglon in Dt_Solicitudes_Completa.DefaultView.ToTable().Rows)
                {
                    if (Deudor.Equals(Renglon["Deudor"].ToString()) && Cuenta.Equals(Renglon["CUENTA"].ToString()) && Banco.Equals(Renglon["Banco"].ToString()) && Cuenta_Contable_ID_Banco.Equals(Renglon["Cuenta_Contable_ID_Banco"].ToString()) && Cuenta_Contable_Deudor.Equals(Renglon["Cuenta_Contable_Deudor"].ToString()) && Tipo_Deudor.Equals(Renglon["Tipo_Deudor"].ToString()))
                    {
                        Total = Total + Convert.ToDouble(Renglon["Importe"].ToString());
                    }
                }
                if (Agregar && Total > 0)
                {
                    Filas["Banco"] = Fila["Banco"].ToString();
                    Filas["cuenta"] = Fila["cuenta"].ToString();
                    Filas["Deudor"] = Fila["Deudor"].ToString();
                    Filas["Importe"] = Total;
                    Filas["Banco_Deudor"] = Fila["Banco_Deudor"].ToString();
                    Filas["Referencia"] = Fila["Referencia"].ToString();
                    Filas["Cuenta_Contable_ID_Banco"] = Fila["Cuenta_Contable_ID_Banco"].ToString();
                    Filas["Cuenta_Contable_Deudor"] = Fila["Cuenta_Contable_Deudor"].ToString().Trim();
                    Filas["Fecha_Poliza"] = Fila["Fecha_Poliza"].ToString().Trim();
                    Filas["Ref_Comision"] = Fila["Ref_Comision"].ToString().Trim();
                    Filas["Ref_IVA"] = Fila["Ref_IVA"].ToString().Trim();
                    Filas["Tipo_Deudor"] = Fila["Tipo_Deudor"].ToString().Trim();
                    Filas["Beneficiario_ID"] = Fila["Beneficiario_ID"].ToString().Trim();
                    Filas["Tipo_Beneficiario_Poliza"] = Fila["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Solicitudes_Partidas.Rows.Add(Filas); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Solicitudes_Partidas.AcceptChanges();
                }
            }
            // Se realiza el registro del pago y de la poliza contable
            foreach (DataRow Registro in Dt_Solicitudes_Partidas.Rows)
            {
                if (!String.IsNullOrEmpty(Registro["Ref_IVA"].ToString()) && !String.IsNullOrEmpty(Registro["Ref_Comision"].ToString()))
                {
                     Bandera="1";
                }
                else{
                     Bandera="2";
                }
                partida = 0;
                Dt_Partidas_Polizas = null;
                Dt_Partidas_Polizas = new DataTable();
                if (Dt_Partidas_Polizas.Rows.Count <= 0 && Dt_Partidas_Polizas.Columns.Count <= 0)
                {
                    //Agrega los campos que va a contener el DataTable de los detalles de la póliza
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Referencia, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID, typeof(System.String));
                    Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario, typeof(System.String));
                }
                partida = partida + 1;
                DataRow row = Dt_Partidas_Polizas.NewRow(); //Crea un nuevo registro a la tabla
                //Agrega el abono del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Registro["Importe"].ToString().Trim());//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                partida = partida + 1;
                row = Dt_Partidas_Polizas.NewRow();
                //Agrega el cargo del registro de la póliza
                row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_Deudor"].ToString().Trim();//Cuenta_Contable_Proveedor;
                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Registro["Importe"].ToString().Trim());//Convert.ToDouble(Monto) - (Monto_Cedular + Monto_ISR);
                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Referencia"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                Dt_Partidas_Polizas.AcceptChanges();
                if (Cmb_Banco.SelectedItem.Text.ToString() != Registro["Banco_Deudor"].ToString().Trim())
                {
                    // se agregan las partidas de la comision y el iva por comisiones
                    Rs_Cuentas_Proveedor.P_Cuenta = "513403411";
                    Dt_C_Proveedor = Rs_Cuentas_Proveedor.Consulta_Existencia_Cuenta_Contable();
                    if (Dt_C_Proveedor.Rows.Count > 0)
                    {
                        Cuenta_Contable_Serv_Financieros = Dt_C_Proveedor.Rows[0]["Cuenta_Contable_ID"].ToString();
                        Cuenta_Servicios_financieros = Dt_C_Proveedor.Rows[0]["Cuenta"].ToString();
                    }
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();
                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Serv_Financieros;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_IVA"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Cuenta_Contable_Serv_Financieros;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = Convert.ToDouble(Txt_Comision_Banco.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_Comision"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Iva_Comision.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_IVA"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                    partida = partida + 1;
                    row = Dt_Partidas_Polizas.NewRow();

                    //Agrega el cargo del registro de la póliza
                    row[Ope_Con_Polizas_Detalles.Campo_Partida] = partida;
                    row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro["Cuenta_Contable_ID_Banco"].ToString().Trim(); //Cuenta_Contable_ID_Banco;
                    row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
                    row[Ope_Con_Polizas_Detalles.Campo_Haber] = Convert.ToDouble(Txt_Comision_Banco.Text.ToString());
                    row[Ope_Con_Polizas_Detalles.Campo_Referencia] = Registro["Ref_Comision"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Beneficiario_ID] = Registro["Beneficiario_ID"].ToString().Trim();
                    row[Ope_Con_Polizas_Detalles.Campo_Tipo_Beneficiario] = Registro["Tipo_Beneficiario_Poliza"].ToString().Trim();
                    Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    Dt_Partidas_Polizas.AcceptChanges();
                }
                Dt_Solicitudes_Partidas.DefaultView.RowFilter = "Deudor='" + Registro["Deudor"].ToString().Trim() + "' AND Cuenta='" + Registro["Cuenta"].ToString().Trim() + "' AND Tipo_Deudor='" + Registro["Tipo_Deudor"].ToString().Trim() + "'";
                Rs_Alta_Pago.P_Dt_Detalles_Poliza = Dt_Partidas_Polizas;
                Rs_Alta_Pago.P_Dt_Datos_Completos = Dt_Solicitudes_Completa;
                Rs_Alta_Pago.P_Dt_Datos_Agrupados = Dt_Solicitudes_Partidas.DefaultView.ToTable();
                Rs_Alta_Pago.P_No_Partidas = Convert.ToString(partida);
                Rs_Alta_Pago.P_Comentario = "";
                if (Bandera.Equals("1"))
                {
                    Rs_Alta_Pago.P_Monto_Comision = Txt_Comision_Banco.Text.ToString();
                    Rs_Alta_Pago.P_Monto_Iva = Txt_Iva_Comision.Text.ToString();
                }
                else
                {
                    Rs_Alta_Pago.P_Monto_Comision = "0";
                    Rs_Alta_Pago.P_Monto_Iva = "0";
                }
                Rs_Alta_Pago.P_Estatus = "PAGADO";
                Rs_Alta_Pago.P_Fecha_Pago = String.Format("{0:dd/MM/yy}", Convert.ToDateTime(Fecha_Pago)).ToString();
                Rs_Alta_Pago.P_Forma_Pago = "TRANSFERENCIA";
                Rs_Alta_Pago.P_Beneficiario_Pago = Nombre_Proveedor;//Dt_Solicitudes_Partidas.Rows[0]["PROVEEDOR"].ToString().Trim();
                Rs_Alta_Pago.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
                Pago = Rs_Alta_Pago.Alta_Pago();
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Alta_pago " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
    ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO  : 21-Febrero-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    private void Abrir_Ventana(String Nombre_Archivo)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
        try
        {
            Pagina = Pagina + Nombre_Archivo;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
            "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception ex)
        {
            throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
        }
    }
    #endregion
    #region "Eventos"
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Salir de la forma
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo
    ///FECHA_CREO  : 21-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Cancelar_Click
    ///DESCRIPCIÓN: Cancela la accion
    ///PARÁMETROS :
    ///CREO       : Sergio Manuel Gallardo
    ///FECHA_CREO  : 21-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        Inicializa_Controles();
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Autozizar_onclick
    ///DESCRIPCIÓN: Autoriza las transferencias de las solicitudes
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Sergio Manuel Gallardo
    ///FECHA_CREO  : 21-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Autozizar_onclick(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_Consulta_Estatus = new DataTable();
        DataTable Dt_Solicitudes_Datos = new DataTable();
        DataTable Dt_Solicitudes_Modificada = new DataTable();
        Boolean Autorizado;
        Int32 Indice = 0;
        try
        {
            DataTable Dt_Grid = (DataTable)(Grid_Pagos.DataSource);
            if (Cmb_Banco.SelectedValue != "0" && Txt_Comision_Banco.Text != "" && Txt_Iva_Comision.Text != "")
            {
                if (Dt_Solicitudes_Datos.Rows.Count <= 0)
                {
                    Dt_Solicitudes_Datos.Columns.Add("No_Solicitud", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Referencia", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Deudor", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Cuenta", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Fecha_Poliza", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Ref_Comision", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Ref_IVA", typeof(System.String));
                    Dt_Solicitudes_Datos.Columns.Add("Tipo_Deudor", typeof(System.String));
                }
                foreach (GridViewRow Renglon_Grid in Grid_Pagos.Rows)
                {
                    GridView Grid_Cuentas = (GridView)Renglon_Grid.FindControl("Grid_Cuentas");

                    foreach (GridViewRow Renglon in Grid_Cuentas.Rows)
                    {
                        GridView Grid_Datos_Solicitud = (GridView)Renglon.FindControl("Grid_Datos_Solicitud");
                        foreach (GridViewRow fila in Grid_Datos_Solicitud.Rows)
                        {
                            Indice++;
                            Grid_Datos_Solicitud.SelectedIndex = Indice;
                            Autorizado = ((CheckBox)fila.FindControl("Chk_Autorizado")).Checked;
                            if (Autorizado && !String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                            {
                                if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString())&&((TextBox)Renglon.Cells[5].FindControl("Txt_Referencia_Comision")).Enabled == true && ((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_IVA_Comision")).Enabled == true)
                                {
                                    if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString())&&!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Referencia_Comision")).Text.ToString()) && !String.IsNullOrEmpty(((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_IVA_Comision")).Text.ToString()))
                                    {
                                        DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                        //Asigna los valores al nuevo registro creado a la tabla
                                        row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Autorizado")).CssClass;
                                        row["Referencia"] = ((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString();
                                        row["Banco"] = fila.Cells[3].Text;
                                        row["cuenta"] = Renglon.Cells[1].Text;
                                        row["Deudor"] = Renglon_Grid.Cells[1].Text;
                                        row["Fecha_Poliza"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)Renglon_Grid.FindControl("Txt_Fecha_Poliza")).Text));
                                        row["Ref_Comision"] = ((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_Comision")).Text.ToString();
                                        row["Ref_IVA"] = ((TextBox)Renglon.Cells[7].FindControl("Txt_Referencia_IVA_Comision")).Text.ToString();
                                        row["Tipo_Deudor"] = Renglon_Grid.Cells[6].Text;
                                        Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Solicitudes_Datos.AcceptChanges();
                                    }
                                }
                                else
                                {
                                    if (!String.IsNullOrEmpty(((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString()))
                                    {
                                        DataRow row = Dt_Solicitudes_Datos.NewRow(); //Crea un nuevo registro a la tabla
                                        //Asigna los valores al nuevo registro creado a la tabla
                                        row["No_Solicitud"] = ((CheckBox)fila.FindControl("Chk_Autorizado")).CssClass;
                                        row["Referencia"] = ((TextBox)Renglon.Cells[5].FindControl("Txt_Beneficiario_Grid")).Text.ToString();
                                        row["Banco"] = fila.Cells[3].Text;
                                        row["cuenta"] = Renglon.Cells[1].Text;
                                        row["Deudor"] = Renglon_Grid.Cells[1].Text;
                                        row["Fecha_Poliza"] = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(((TextBox)Renglon_Grid.FindControl("Txt_Fecha_Poliza")).Text));
                                        row["Ref_Comision"] = ((TextBox)Renglon.Cells[6].FindControl("Txt_Referencia_Comision")).Text.ToString();
                                        row["Ref_IVA"] = ((TextBox)Renglon.Cells[7].FindControl("Txt_Referencia_IVA_Comision")).Text.ToString();
                                        row["Tipo_Deudor"] = Renglon_Grid.Cells[6].Text;
                                        Dt_Solicitudes_Datos.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                        Dt_Solicitudes_Datos.AcceptChanges();
                                    }
                                }
                            }
                        }
                    }
                }

                if (Dt_Solicitudes_Modificada.Rows.Count <= 0)
                {
                    Dt_Solicitudes_Modificada.Columns.Add("No_Solicitud", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Referencia", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Banco", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Deudor", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Cuenta", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Importe", typeof(System.Double));
                    Dt_Solicitudes_Modificada.Columns.Add("Fecha_Poliza", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Ref_Comision", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Ref_IVA", typeof(System.String));
                    Dt_Solicitudes_Modificada.Columns.Add("Tipo_Deudor", typeof(System.String));
                }
                if (Dt_Solicitudes_Datos.Rows.Count > 0)
                {
                    foreach (DataRow Registro in Dt_Solicitudes_Datos.Rows)
                    {
                        Rs_Solicitud_Pago.P_No_Deuda = Registro["No_Solicitud"].ToString();
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Datos_Solicitud_Pago();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            foreach (DataRow fila in Dt_Consulta_Estatus.Rows)
                            {
                                if (fila["Estatus"].ToString() == "PORPAGAR")
                                {
                                    DataRow row = Dt_Solicitudes_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                                    //Asigna los valores al nuevo registro creado a la tabla
                                    row["No_Solicitud"] = Registro["No_Solicitud"].ToString();
                                    row["Referencia"] = Registro["Referencia"].ToString();
                                    row["Banco"] = Registro["Banco"].ToString();
                                    row["cuenta"] = Registro["cuenta"].ToString();
                                    row["Deudor"] = Registro["Deudor"].ToString();
                                    row["Importe"] = fila["Importe"].ToString();
                                    row["Fecha_Poliza"] = Registro["Fecha_Poliza"].ToString();
                                    row["Ref_Comision"] = Registro["Ref_Comision"].ToString();
                                    row["Ref_IVA"] = Registro["Ref_IVA"].ToString();
                                    row["Tipo_Deudor"] = Registro["Tipo_Deudor"].ToString();
                                    Dt_Solicitudes_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Solicitudes_Modificada.AcceptChanges();
                                }
                            }
                        }
                    }
                    if (Dt_Solicitudes_Modificada.Rows.Count > 0)
                    {
                        Alta_Transferencia(Dt_Solicitudes_Modificada);
                    }
                }
            }
            else
            {
                Lbl_Mensaje_Error.Text = "Debes tener ingresado la comision del banco y el iva de la comision para poder continuar";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar_No_Solicitud_Click
    ///DESCRIPCIÓN: Busca las solicitudes
    ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
    ///                             para mostrar los datos al usuario
    ///CREO       : Sergio Manuel Gallardo
    ///FECHA_CREO  : 21-Mayo-2012
    ///MODIFICO          :
    ///FECHA_MODIFICO    :
    ///CAUSA_MODIFICACIÓN:
    ///******************************************************************************
    protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
    {

        DataTable Dt_Modificada = new DataTable();
        Boolean Agregar;
        Double Total;
        String DEUDOR_ID;
        String TIPO_DEUDOR;
        String Cuenta;
        DataTable Dt_Datos_Proveedor = new DataTable();
        Dt_Datos_Proveedor = ((DataTable)(Session["Dt_Datos_Proveedor"]));
        DataTable Dt_Datos = new DataTable();
        Dt_Datos = ((DataTable)(Session["Dt_Datos"]));
        Int32 Contador = 0;
        Session["Contador"] = Contador;
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consulta_PorPagar = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        //Cls_Ope_Con_Transferencia_Negocio Rs_Consulta_Ejercidos = new Cls_Ope_Con_Transferencia_Negocio();
        DataTable Dt_Resultado = new DataTable();
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consulta_PorPagar.P_No_Deuda = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));

                Dt_Resultado = Rs_Consulta_PorPagar.Consultar_Solicitud_Transferencia();
                if (Dt_Resultado.Rows.Count > 0)
                {
                    //Consultamos el banco con la primera traferencia
                    Cmb_Banco.SelectedValue = Dt_Resultado.Rows[0]["Nombre_Banco"].ToString();
                    Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedValue;
                    Dt_Consulta = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
                    if (Dt_Consulta.Rows.Count > 0)
                    {
                        if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString()) && !String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString()))
                        {
                            Txt_Comision_Banco.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Comision_Transferencia].ToString();
                            Txt_Iva_Comision.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_IVA_Comision].ToString();
                        }
                        else
                        {
                            Txt_Comision_Banco.Text = "";
                            Txt_Iva_Comision.Text = "";
                            Lbl_Mensaje_Error.Text = "La comision y/o el iva de la comision no estan dadas de alta porfavor ingresalas en el catalogo de comisiones bancarias";
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                        }
                    }
                    foreach (DataRow Fila in Dt_Resultado.Rows)
                    {
                        DEUDOR_ID = Fila["DEUDOR_ID"].ToString();
                        TIPO_DEUDOR = Fila["TIPO_DEUDOR"].ToString();
                        Cuenta = Fila["CUENTA_BANCO_PAGO"].ToString();
                        if (Dt_Modificada.Rows.Count <= 0)
                        {
                            Dt_Modificada.Columns.Add("DEUDOR_ID", typeof(System.String));
                            Dt_Modificada.Columns.Add("BENEFICIARIO", typeof(System.String));
                            Dt_Modificada.Columns.Add("MONTO", typeof(System.Double));
                            Dt_Modificada.Columns.Add("ESTATUS", typeof(System.String));
                            Dt_Modificada.Columns.Add("CUENTA", typeof(System.String));
                            Dt_Modificada.Columns.Add("IDENTIFICADOR_TIPO", typeof(System.String));
                            Dt_Modificada.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                        }
                        DataRow row = Dt_Modificada.NewRow(); //Crea un nuevo registro a la tabla
                        Agregar = true;
                        Total = 0;
                        //insertar los que no se repiten
                        foreach (DataRow Fila2 in Dt_Modificada.Rows)
                        {
                            if (DEUDOR_ID.Equals(Fila2["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Fila2["TIPO_DEUDOR"].ToString()))
                            {
                                Agregar = false;
                            }
                        }
                        //se calcula el monto por tipo
                        foreach (DataRow Renglon in Dt_Resultado.Rows)
                        {
                            if (DEUDOR_ID.Equals(Renglon["DEUDOR_ID"].ToString()) && TIPO_DEUDOR.Equals(Renglon["TIPO_DEUDOR"].ToString()))
                            {
                                Total = Total + Convert.ToDouble(Renglon["IMPORTE"].ToString());
                            }
                        }
                        if (Agregar && Total > 0)
                        {
                            row["DEUDOR_ID"] = DEUDOR_ID;
                            row["TIPO_DEUDOR"] = TIPO_DEUDOR;
                            row["BENEFICIARIO"] = Fila["BENEFICIARIO"].ToString();
                            row["MONTO"] = Total;
                            row["CUENTA"] = Fila["CUENTA_BANCO_PAGO"].ToString().Trim();
                            row["ESTATUS"] = Fila["Estatus"].ToString();
                            if (TIPO_DEUDOR == "EMPLEADO")
                            {
                                row["IDENTIFICADOR_TIPO"] = "E-" + DEUDOR_ID + Total;
                            }
                            else
                            {
                                row["IDENTIFICADOR_TIPO"] = "P-" + DEUDOR_ID + Total;
                            }

                            Dt_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Modificada.AcceptChanges();
                        }
                    }
                    Session["Dt_Datos"] = Dt_Resultado;
                    Session["Dt_Datos_Proveedor"] = Dt_Modificada;
                    Grid_Pagos.DataSource = Dt_Modificada;   // Se iguala el DataTable con el Grid
                    Grid_Pagos.DataBind();    // Se ligan los datos.;
                    //Grid_Solicitud_Pagos.Columns[3].Visible = false;
                }
                else
                {
                    Grid_Pagos.DataSource = new DataTable();
                    Grid_Pagos.DataBind();
                }
            }
            else
            {
                Llenar_Grid_Solicitudes();
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            //Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Accion"]))
                {
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
}
