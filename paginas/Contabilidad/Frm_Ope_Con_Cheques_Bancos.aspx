﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Cheques_Bancos.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Cheques_Bancos" Title="Cheques Bancos"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
   <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
       <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
             <ContentTemplate>
               <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                    <ProgressTemplate>
                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                        <div  class="processMessage" id="div_progress">
                            <img alt="" src="../Imagenes/paginas/Updating.gif" />
                        </div>
                    </ProgressTemplate>
                </asp:UpdateProgress>
                
            <div id="Div_General" runat="server"  style="background-color:#ffffff; width:100%; height:100%;"> <%--Fin del div General--%>
                    <table  width="98%" border="0" cellspacing="0" class="estilo_fuente" frame="border" >
                        <tr align="center">
                            <td class="label_titulo" colspan="2">Cheques Bancos
                            </td>
                       </tr>
                        <tr> <!--Bloque del mensaje de error-->
                            <td colspan="2">
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>      
                        </tr>
                        
                        <tr class="barra_busqueda" align="right">
                            <td align="left">
                               <asp:ImageButton ID="Btn_Modificar" runat="server"  ToolTip="Modificar" CssClass="Img_Button" TabIndex="2"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" onclick="Btn_Modificar_Click" />     
                                <asp:ImageButton ID="Btn_Salir" runat="server" 
                                    CssClass="Img_Button" 
                                    ToolTip="Salir"
                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                    onclick="Btn_Salir_Click"/>
                             </td>
                             <td style="width:50%">Busqueda   
                                <asp:TextBox ID="Txt_Busqueda" runat="server" MaxLength="100" TabIndex="5"  ToolTip = "Buscar por Nombre" Width="180px"/>
                                <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Ingrese nombre del banco>" TargetControlID="Txt_Busqueda" />
                                <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda" 
                                        runat="server" TargetControlID="Txt_Busqueda" FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" ValidChars="ÑñáéíóúÁÉÍÓÚ. "/>
                                 <asp:ImageButton ID="Btn_Buscar" runat="server" TabIndex="6"
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" ToolTip="Consultar"
                                        onclick="Btn_Buscar_Click" />
                            </td>                         
                             
                        </tr>
                    </table>
                
                    <div id="Div_Detalles" runat="server" style="overflow:auto;height:400px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:none">
                        <asp:UpdatePanel ID="Upnl_Detalle" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>             
                                <asp:Panel ID="Pnl_Detalle" runat="server" GroupingText="Datos Generales" Width="98%" BackColor="white">
                                    <table class="estilo_fuente" width="98%">
                                         <tr >
                                            <td style="width:15%"></td>
                                            <td style="width:25%"></td>
                                            <td style="width:15%" ></td>
                                            <td style="width:25%" ></td>
                                            <td style="width:20%" ></td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" >
                                                <asp:Label ID="Lbl_Cmb_Bancos" runat="server" Text="Bancos ID"></asp:Label>
                                            </td>
                                            <td style="width:25%" >
                                               <%-- <asp:DropDownList ID="Cmb_Banco" runat="server" Width="98%">
                                                    </asp:DropDownList>--%>
                                                    <asp:TextBox ID="Txt_Banco_ID" runat="server" Width="75%" MaxLength="20"
                                                        ReadOnly="true" ></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:Label ID="Lbl_Numero_Cuenta" runat="server" Text="Número cuenta"></asp:Label>
                                            </td>
                                            <td style="width:25%" >                                   
                                                <asp:TextBox ID="Txt_No_Cuenta" runat="server" Width="75%" MaxLength="20"
                                                    ReadOnly="true" ></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="width:15%" >
                                                <asp:Label ID="Lbl_Nombre_Banco" runat="server" Text="Nombre banco"></asp:Label>
                                            </td>
                                           <td colspan="2">
                                                    <asp:TextBox ID="Txt_Nombre_Banco" runat="server" Width="98%" ReadOnly="true" ></asp:TextBox>
                                           </td>
                                        </tr>
                                        
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>        
                        </asp:UpdatePanel>
                            
                        <asp:UpdatePanel ID="Upnl_Generales_Poliza" runat="server" UpdateMode="Conditional">
                            <ContentTemplate>             
                                <asp:Panel ID="Pnl_Datos_Folio" runat="server" GroupingText="Folios del talonario de cheques" Width="98%" BackColor="white">
                                    <table width="100%" class="estilo_fuente">
                                        <tr>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Folio_Inicial" runat="server" Text="Folio Inicial"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                <asp:TextBox ID="Txt_Folio_Inicial" runat="server" Width="75%" MaxLength="20">
                                                </asp:TextBox>
                                                <cc1:FilteredTextBoxExtender ID="Txt_Folio_Inicial_FilteredTextBoxExtender1" 
                                                    runat="server" TargetControlID="Txt_Folio_Inicial" FilterType="Numbers" 
                                                    ValidChars="0123456789"/>
                                            </td>
                                            <td style="width:15%">
                                                <asp:Label ID="Lbl_Folio_Final" runat="server" Text="Folio Final"></asp:Label>
                                            </td>
                                            <td style="width:25%">
                                                 <asp:TextBox ID="Txt_Folio_Final" runat="server" Width="75%" MaxLength="20"
                                                     Enabled="false" ></asp:TextBox>
                                                  <cc1:FilteredTextBoxExtender ID="Txt_Folio_Final_FilteredTextBoxExtender1" 
                                                    runat="server" TargetControlID="Txt_Folio_Final" FilterType="Numbers" 
                                                    ValidChars="0123456789"/>  
                                            </td>
                                            <td style="width:20%" ></td>
                                        </tr>
                                        
                                        <tr>
                                            <td>
                                                <asp:Label ID="Lbl_Folio_Actual" runat="server" Text="Folio Actual"></asp:Label>
                                            </td>
                                            <td>
                                                 <asp:TextBox ID="Txt_Folio_Actual" runat="server" Width="75%" MaxLength="20"
                                                     Enabled="false" ></asp:TextBox>
                                                 <cc1:FilteredTextBoxExtender ID="Txt_Folio_Actual_FilteredTextBoxExtender1" 
                                                    runat="server" TargetControlID="Txt_Folio_Actual" FilterType="Numbers" 
                                                    ValidChars="0123456789"/>  
                                            </td>
                                        </tr>
                                   
                                    </table>
                                </asp:Panel>
                            </ContentTemplate>        
                        </asp:UpdatePanel>
                    </div>
                    
                    
                    <table class="estilo_fuente" width="98%" >
                    
                         <%--<tr>
                            <td colspan="3" >
                                <table width="99%"  border="0" cellspacing="0" class="estilo_fuente">--%>
                                <tr >  
                                    <td>
                                        <div id="Div_Grid" runat="server" style="overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block">
                        
                                            <asp:GridView ID="Grid_Bancos" runat="server" Width="100%"
                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                 OnSelectedIndexChanged="Grid_Bancos_SelectedIndexChanged" >
                                                <Columns>   
                                                     <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                        ImageUrl="~/paginas/imagenes/gridview/blue_button.png">
                                                        <ItemStyle Width="7%" />
                                                    </asp:ButtonField>  
                                                      <asp:BoundField DataField="Banco_ID" HeaderText="BANCO ID" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                                         <ItemStyle HorizontalAlign="Left" Width="8%" />
                                                       </asp:BoundField>                                          
                                                       <asp:BoundField DataField="NOMBRE" HeaderText="NOMBRE" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Left" Width="15%"   Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Left" Width="15%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="NO_CUENTA" HeaderText="NO CUENTA" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Center" Width="15%"   Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="Folio_Inicial" HeaderText="INICIAL" ItemStyle-Font-Size="Small">
                                                         <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                         <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="Folio_Final" HeaderText="FINAL" ItemStyle-Font-Size="Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                       </asp:BoundField>
                                                       <asp:BoundField DataField="Folio_Actual" HeaderText="ACTUAL" ItemStyle-Font-Size="Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                           <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                       </asp:BoundField>
                                                        <asp:BoundField DataField="USUARIO_MODIFICO" HeaderText="USUARIO MODIFICO" ItemStyle-Font-Size="Small">
                                                           <HeaderStyle HorizontalAlign="Center" Width="20%" Font-Size="X-Small"/>
                                                           <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                       </asp:BoundField>
                                                       <%--<asp:TemplateField HeaderText="Eliminar">
                                                            <ItemTemplate>
                                                                <asp:ImageButton ID="Btn_Quitar" runat="server" CommandName="Quitar_Registro" 
                                                                    ImageUrl="~/paginas/imagenes/gridview/grid_garbage.png" 
                                                                    CommandArgument='<%# Eval("Banco_ID") %>' 
                                                                    OnClick="Btn_Quitar_Click"/>
                                                                </ItemTemplate>
                                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                                <ItemStyle HorizontalAlign="Center" Width="15%" />                                                        
                                                       </asp:TemplateField>     --%>
                                                </Columns>                                                    
                                                <SelectedRowStyle CssClass="GridSelected" />
                                                <PagerStyle CssClass="GridHeader" />
                                                <HeaderStyle CssClass="tblHead" />
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                            </asp:GridView> 
                                        </div>                         
                                    </td>  
                                </tr>                                
                        </table>
                      
                        
                </div>
            </ContentTemplate>
      </asp:UpdatePanel>
</asp:Content>