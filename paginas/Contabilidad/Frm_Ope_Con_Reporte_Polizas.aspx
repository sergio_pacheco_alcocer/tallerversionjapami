﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Reporte_Polizas.aspx.cs" Inherits="paginas_contabilidad_Frm_Ope_Con_Reporte_Polizas" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">

    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Rpt_Tipos_Polizas" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
       <ContentTemplate>
        <asp:UpdateProgress ID="Uprg_Reporte" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
            <ProgressTemplate>
                <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
            </ProgressTemplate>
        </asp:UpdateProgress>        
        <div style="width:98%; background-color:White;">
            <table width="100%" title="Control_Errores"> 
                <tr align="center">
                    <td class="label_titulo">Reporte de Tipos de P&oacute;lizas</td>               
                </tr>            
                <tr>
                    <td style="width:100%; text-align:left; cursor:default;">
                        <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />
                        <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>        
                    </td>               
                </tr>
            </table>
            <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Consultar Polizas" 
                                                        CssClass="Img_Button" TabIndex="1"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" 
                                                        onclick="Btn_Nuevo_Click" />
                                             <asp:ImageButton ID="Btn_Reporte_Pdf" runat="server" ToolTip="Generar Reporte"  Visible="false"
                                                    CssClass="Img_Button" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                    onclick="Btn_Reporte_PDF_Click"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click" />
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
            <asp:Panel ID="Pnl_Tipos_Polizas" runat="server" Width="98%" GroupingText="Filtrar por Tipos de Póliza" BackColor="White">
                <table width="100%">
                    <tr>
                        <td style="width:20%;text-align:left;">
                            <asp:CheckBoxList ID="Chk_Tipos_Poliza" runat="server" RepeatColumns="8" 
                                RepeatDirection="Horizontal">
                            </asp:CheckBoxList>
                        </td>
                    </tr>                               
                </table>
            </asp:Panel>
            <table width="98%">
                
                <tr>
                    <td style="width:20%;text-align:left; cursor:default;">Fecha Inicio</td>
                    <td style="width:30%;text-align:left; cursor:default;">
                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="180px" TabIndex="14" Enabled="false"/>
                        <cc1:CalendarExtender ID="Cal_Txt_Fecha_Inicio" runat="server" 
                            TargetControlID="Txt_Fecha_Inicio" PopupButtonID="Btn_Fecha_Inicio" Format="dd/MMM/yyyy"/>
                        <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                            ToolTip="Seleccione la Fecha"/>                                                                              
                    </td>
                    <td style="width:20%;text-align:left; cursor:default;">&nbsp;&nbsp;Fecha Final</td>
                    <td style="width:30%;text-align:left; cursor:default;">
                        <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="180px" TabIndex="15" Enabled="false"/>                                                 
                        <cc1:CalendarExtender ID="Cal_Txt_Fecha_Final" runat="server" 
                            TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                        <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                            ToolTip="Seleccione la Fecha"/>                       
                    </td>                                                            
                </tr>
                <tr>
                    <td>Concepto Poliza</td>
                    <td colspan="3">
                        <asp:TextBox ID="Txt_Concepto" runat="server" Width="85%"></asp:TextBox> </td>
                </tr>
                
                <tr >
                    <td runat="server" id="Tr_Grid_Polizas" colspan="4">
                           <div  width="100%">
                                <table width="100%"  border="0" cellspacing="0">
                                    <tr >
                                        <td Font-Size="XX-Small"  style=" width:8%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Fecha</td>
                                        <td Font-Size="XX-Small" style="width:8%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Tipo </td>
                                        <td Font-Size="XX-Small" style="width:39%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;">Concepto</td>
                                        <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Debe</td>
                                        <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center">Haber</td>
                                        <td Font-Size="XX-Small" style="width:10%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center"> Póliza</td>
                                        <td Font-Size="XX-Small" style="width:15%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="center"> Usuario Creo</td>
                                    </tr>
                                </table>
                            </div>
                            <div style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; " >
                                    <asp:GridView ID="Grid_Polizas" runat="server"  ShowHeader="false" OnRowDataBound="Grid_Polizas_RowDataBound"
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" DataKeyNames="P_No_Poliza" 
                                        EmptyDataText="No se encontro ningun registro con los filtros seleccionados" Width="100%">
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                ImageUrl="~/paginas/imagenes/paginas/add_up.png" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="2%" Font-Size="XX-Small" />
                                        <ItemStyle HorizontalAlign="Left" Width="2%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="FECHA_POLIZA"  DataFormatString="{0:dd/MMM/yyyy}">
                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="XX-Small" />
                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="8%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="TIPO_POLIZA" >
                                        <HeaderStyle HorizontalAlign="center" Width="8%" Font-Size="XX-Small" />
                                        <ItemStyle Font-Size="X-Small"  HorizontalAlign="center" Width="8%" />
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="CONCEPTO_POLIZA" >
                                        <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="XX-Small" />
                                        <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="38%" />
                                    </asp:BoundField> 
                                    <asp:BoundField DataField="DEBE"  DataFormatString="{0:C}" >
                                        <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="XX-Small" />
                                        <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right" Width="10%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="HABER"  DataFormatString="{0:C}">
                                        <HeaderStyle HorizontalAlign="Right" Width="10%" Font-Size="XX-Small" />
                                        <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right"  Width="10%" />
                                    </asp:BoundField>
                                    <asp:TemplateField  Visible="True">
                                            <ItemTemplate>
                                                <asp:LinkButton Font-Size="X-Small"  ID="Btn_Seleccionar_Poliza" runat="server" Text= '<%# Eval("P_No_Poliza") %>'                                             
                                                OnClick="Btn_Poliza_Click" CommandArgument='<%# Eval("MES_ANO") %>' CssClass='<%# Eval("Tipo_Poliza_ID") %>' ForeColor="Blue"  />
                                            </ItemTemplate >
                                            <HeaderStyle HorizontalAlign="Center"  Font-Size="XX-Small"/>
                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Center"  Width="10%"/>
                                        </asp:TemplateField> 
                                     <asp:BoundField DataField="USUARIO" >
                                        <HeaderStyle HorizontalAlign="Right"  Font-Size="XX-Small" />
                                        <ItemStyle Font-Size="XX-Small"  HorizontalAlign="Right"  Width="22%" />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Poliza" runat="server" 
                                                        Text='<%# Bind("P_No_Poliza") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:none;position:static'&gt;&lt;td colspan='8';left-padding:10px;&gt;" />
                                                    <asp:GridView ID="Grid_Movimientos" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"  Width="100%">
                                                        <Columns>
                                                            <asp:BoundField HeaderText="Partida" DataField="Partida">
                                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                                <ItemStyle  Font-Size="X-Small" HorizontalAlign="Center" Width="15%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Cuenta_Contable" HeaderText="Cuenta" >
                                                                <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="15%" />
                                                            </asp:BoundField>
                                                             <asp:BoundField DataField="Cuenta" HeaderText="Descripcion" >
                                                                <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="40%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="DEBE"  HeaderText="Debe" DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="Right" Width="15%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right"  Width="15%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="HABER"  HeaderText="Haber" DataFormatString="{0:C}" >
                                                                <HeaderStyle HorizontalAlign="Right" Width="15%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Right" Width="15%" />
                                                            </asp:BoundField>
                                                            
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                                <ItemStyle HorizontalAlign="Left" Width="100%" />
                                </asp:TemplateField>
                                </Columns>
                                <SelectedRowStyle CssClass="GridSelected" />
                                <PagerStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                </asp:GridView>
                            </div>
                    </td>
                </tr>   
            </table>
        </div>
    </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

