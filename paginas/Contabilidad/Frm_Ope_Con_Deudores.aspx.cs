﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using JAPAMI.Sessiones;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Deudores.Negocios;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using JAPAMI.Parametros_Contabilidad.Negocio;
public partial class paginas_Contabilidad_Frm_Ope_Con_Deudores : System.Web.UI.Page
{
    #region (Page Load)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Page_Load
        /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
        /// CREO        : Yazmin Abigail Delgado Gómez
        /// FECHA_CREO  : 09-Junio-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                    Consultar_Tipos_Solicitud();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
            ///               diferentes operaciones
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
                    Limpia_Controles();             //Limpia los controles del forma
                    Consulta_Deudores();        //Consulta todas los Tipos de Polizas que fueron dadas de alta en la BD
                    Llenar_Grid_Deudores();
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message.ToString());
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Txt_Concepto.Text = "";
                    Cmb_Estatus.SelectedIndex = -1;
                    Txt_Fecha_Inicio.Text = "";
                    Txt_Importe.Text = "";
                    Txt_Busqueda_Deudores.Text = "";
                    Cmb_Deudores.SelectedIndex = -1;
                    Cmb_Tipo.SelectedIndex = -1;
                    Txt_No_Movimiento.Text = "";
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Habilitar_Controles
            /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
            ///                para a siguiente operación
            /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
            ///                           si es una alta, modificacion
            ///                           
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 09-Junio-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Habilitar_Controles(String Operacion)
            {
            Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
            try
            {
                Habilitado = false;
                switch (Operacion)
                {
                    case "Inicial":
                        Habilitado = false;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = true;
                        //Btn_Eliminar.Visible = true;
                        Btn_Nuevo.CausesValidation = false;
                        Btn_Modificar.CausesValidation = false;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                        Cmb_Estatus.Enabled = Habilitado;
                        Configuracion_Acceso("Frm_Ope_Con_Deudores.aspx");
                        Div_Datos_Deudores.Style.Add("Display","none");
                        Div_Presentacion_Deudores.Style.Add("Display", "block");
                        Btn_Imprimir.Visible = false;
                        break;

                    case "Nuevo":
                        Habilitado = true;
                        Btn_Nuevo.ToolTip = "Dar de Alta";
                        Btn_Modificar.ToolTip = "Modificar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = true;
                        Btn_Modificar.Visible = false;
                        //Btn_Eliminar.Visible = false;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Modificar.CausesValidation = true;
                        Cmb_Estatus.Enabled = false;
                        Cmb_Estatus.SelectedIndex =1;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                        Div_Datos_Deudores.Style.Add("Display", "block");
                        Div_Presentacion_Deudores.Style.Add("Display", "none");
                        Btn_Imprimir.Visible = false;
                        break;

                    case "Modificar":
                        Habilitado = true;
                        Btn_Nuevo.ToolTip = "Nuevo";
                        Btn_Modificar.ToolTip = "Actualizar";
                        Btn_Salir.ToolTip = "Cancelar";
                        Btn_Nuevo.Visible = false;
                        Btn_Modificar.Visible = true;
                        Btn_Nuevo.CausesValidation = true;
                        Btn_Modificar.CausesValidation = true;
                        Cmb_Estatus.Enabled = Habilitado;
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                        Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                        Btn_Imprimir.Visible = false;
                        break;
                }
                Txt_Concepto.Enabled = Habilitado;
                Txt_Fecha_Inicio.Enabled = false;
                Btn_Fecha_Inicio.Enabled = Habilitado;
                Txt_Importe.Enabled = Habilitado;
                Txt_Busqueda_Deudores.Enabled = !Habilitado;
                Btn_Buscar_Descripcion_Deudores.Enabled = !Habilitado;
                Cmb_Deudores.Enabled = Habilitado;
                Btn_Mostrar_Busqueda.Enabled = Habilitado;
                Txt_No_Movimiento.Enabled = Habilitado;
                Cmb_Tipo.Enabled = Habilitado;
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
            }
        }
        #endregion

        #region (Control Acceso Pagina)
            ///*******************************************************************************
            /// NOMBRE: Configuracion_Acceso
            /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
            /// PARÁMETROS  :
            /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
            /// FECHA CREÓ  : 23/Mayo/2011 10:43 a.m.
            /// USUARIO MODIFICO  :
            /// FECHA MODIFICO    :
            /// CAUSA MODIFICACIÓN:
            ///*******************************************************************************
            protected void Configuracion_Acceso(String URL_Pagina)
            {
                List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
                DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

                try
                {
                    //Agregamos los botones a la lista de botones de la página.
                    Botones.Add(Btn_Nuevo);
                    Botones.Add(Btn_Modificar);
                    //Botones.Add(Btn_Eliminar);
                    Botones.Add(Btn_Buscar_Descripcion_Deudores);

                    if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
                    {
                        if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                        {
                            //Consultamos el menu de la página.
                            Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                            if (Dr_Menus.Length > 0)
                            {
                                //Validamos que el menu consultado corresponda a la página a validar.
                                if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                                {
                                    Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                                }
                                else
                                {
                                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                                }
                            }
                            else
                            {
                                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: IsNumeric
            /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
            /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
            /// CREO        : Juan Alberto Hernandez Negrete
            /// FECHA_CREO  : 29/Noviembre/2010
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Es_Numero(String Cadena)
        {
            Boolean Resultado = true;
            Char[] Array = Cadena.ToCharArray();
            try
            {
                for (int index = 0; index < Array.Length; index++)
                {
                    if (!Char.IsDigit(Array[index])) return false;
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
            }
            return Resultado;
        }
        #endregion

        //#region (Método Consulta)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Deudores
            /// DESCRIPCION : Consulta los Proveedores que tienen cuenta de deudor y los empleados
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02-agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Deudores()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Consulta_deudores = new Cls_Ope_Con_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Deudor_Proveedor= new DataTable(); //Variable que obtendra los datos de la consulta 
                DataTable Dt_Deudor_Empleado = new DataTable(); //Variable que obtendra los datos de la consulta 
                DataTable Dt_Deudores = new DataTable(); //Variable que obtendra los datos de la consulta 

                try
                {
                    if (Dt_Deudores.Rows.Count <= 0 && Dt_Deudores.Columns.Count<=0)
                    {
                        Dt_Deudores.Columns.Add("DEUDOR_ID", typeof(System.String));
                        Dt_Deudores.Columns.Add("NOMBRE", typeof(System.String));
                        Dt_Deudores.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                    }
                    //Dt_Deudor_Proveedor = Rs_Consulta_deudores.Consulta_Proveedores_Deudores(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                    Dt_Deudor_Empleado = Rs_Consulta_deudores.Consulta_Empleados_Deudores(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                    //if (Dt_Deudor_Proveedor.Rows.Count >0)
                    //{
                    //    foreach (DataRow Fila in Dt_Deudor_Proveedor.Rows)
                    //    {
                    //        DataRow row = Dt_Deudores.NewRow(); //Crea un nuevo registro a la tabla
                    //        //Asigna los valores al nuevo registro creado a la tabla
                    //        row["DEUDOR_ID"] = "P"+Fila["PROVEEDOR_ID"].ToString();
                    //        row["NOMBRE"] = Fila["NOMBRE"].ToString();
                    //        row["TIPO_DEUDOR"] = "PROVEEDOR";
                    //        Dt_Deudores.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                    //        Dt_Deudores.AcceptChanges();
                    //    }
                    //}
                   if (Dt_Deudor_Empleado.Rows.Count > 0)
                    {
                        foreach (DataRow Fila in Dt_Deudor_Empleado.Rows)
                        {
                            DataRow row = Dt_Deudores.NewRow(); //Crea un nuevo registro a la tabla
                            //Asigna los valores al nuevo registro creado a la tabla
                            row["DEUDOR_ID"] = "E" + Fila["EMPLEADO_ID"].ToString();
                            row["NOMBRE"] = Fila["NOMBRE"].ToString();
                            row["TIPO_DEUDOR"] = "EMPLEADO";
                            Dt_Deudores.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Deudores.AcceptChanges();
                        }
                    }
                    Dt_Deudores.DefaultView.Sort = "NOMBRE";
                    LLenar_Combo_Deudores(Dt_Deudores.DefaultView.ToTable());
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Tipo_Poliza " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: LLenar_Combo_Deudores
            /// DESCRIPCION : Llena el Cmb_Deudores.
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02-Agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void LLenar_Combo_Deudores(DataTable Dt_Resultado)
            {
               try
                {
                    Cmb_Deudores.Items.Clear();
                    Cmb_Deudores.DataSource = Dt_Resultado;
                    Cmb_Deudores.DataTextField = Cat_Com_Proveedores.Campo_Nombre;
                    Cmb_Deudores.DataValueField = "DEUDOR_ID";
                    Cmb_Deudores.DataBind();
                    Cmb_Deudores.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    Cmb_Deudores.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Imprimir
            ///DESCRIPCIÓN: Imprime la solicitud
            ///PROPIEDADES:     
            ///CREO: Sergio Manuel Gallardo
            ///FECHA_CREO: 06/Enero/2012 
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Imprimir(String Numero_Solicitud)
            {
                DataSet Ds_Reporte = null;
                DataTable Dt_Pagos = null;
                try
                {
                    Cls_Ope_Con_Deudores_Negocio Solicitud_Deudor = new Cls_Ope_Con_Deudores_Negocio();
                    Ds_Reporte = new DataSet();
                    Solicitud_Deudor.P_No_Deuda = Numero_Solicitud;
                    Dt_Pagos = Solicitud_Deudor.Consulta_Datos_impresion();
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        Dt_Pagos.TableName = "Dt_Datos_Deudor";
                        Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        Generar_Reporte(ref Ds_Reporte, "Rpt_Solicitud_Deudor.rpt", "Reporte_Solicitud_Gasto" + Numero_Solicitud, ".pdf");
                    }
                }
                //}
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                    Lbl_Mensaje_Error.Visible = true;
                }

            }

            #region Metodos Reportes
            /// *************************************************************************************
            /// NOMBRE:             Generar_Reporte
            /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
            ///              
            /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
            ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
            ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
            ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
            /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
            /// FECHA MODIFICO:     16/Mayo/2011
            /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
            ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
            /// *************************************************************************************
            public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
            {
                ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
                String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
                try
                {
                    Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                    Reporte.Load(Ruta);

                    if (Ds_Reporte_Crystal is DataSet)
                    {
                        if (Ds_Reporte_Crystal.Tables.Count > 0)
                        {
                            Reporte.SetDataSource(Ds_Reporte_Crystal);
                            Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                            Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            /// *************************************************************************************
            /// NOMBRE:             Exportar_Reporte_PDF
            /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
            ///                     especificada.
            /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
            ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
            /// USUARIO CREO:       Juan Alberto Hernández Negrete.
            /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
            /// USUARIO MODIFICO:
            /// FECHA MODIFICO:
            /// CAUSA MODIFICACIÓN:
            /// *************************************************************************************
            public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
            {
                ExportOptions Opciones_Exportacion = new ExportOptions();
                DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

                try
                {
                    if (Reporte is ReportDocument)
                    {
                        Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                        Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                        Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                        Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Reporte.Export(Opciones_Exportacion);
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            protected void Mostrar_Reporte(String Nombre_Reporte)
            {
                String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

                try
                {
                    Pagina = Pagina + Nombre_Reporte;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                        "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
                }
            }
            #endregion
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Llenar_Grid_Deudores
            /// DESCRIPCION : Llena el grid Solicitudes de pago
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15/noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Llenar_Grid_Deudores()
            {
                try
                {
                    Cls_Ope_Con_Deudores_Negocio Rs_Deudores = new Cls_Ope_Con_Deudores_Negocio();
                    DataTable Dt_Resultado = new DataTable();
                    //Rs_Autoriza_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    Dt_Resultado = Rs_Deudores.Consulta_Deudores();
                    Grid_Deudores_Diversos.DataSource = null;   // Se iguala el DataTable con el Grid
                    Grid_Deudores_Diversos.DataBind();    // Se ligan los datos.;
                    if (Dt_Resultado.Rows.Count>0)
                    {
                        Grid_Deudores_Diversos.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                        Grid_Deudores_Diversos.DataBind();    // Se ligan los datos.;
                    }else{
                        Grid_Deudores_Diversos.DataSource = null;   // Se iguala el DataTable con el Grid
                        Grid_Deudores_Diversos.DataBind();    // Se ligan los datos.;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Busqueda_Deudor_Popup_Click
            /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 02/Agosto/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Busqueda_Deudor_Popup_Click(object sender, EventArgs e)
            {
                try
                {
                    Consulta_Deudores_Busqueda();
                }
                catch (Exception Ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Deudores
            /// DESCRIPCION : Ejecuta la busqueda de Cuenta
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  :02/Agosto/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Deudores_Busqueda()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Consulta_Deudores = new Cls_Ope_Con_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Deudores =new DataTable(); //Variable que obtendra los datos de la consulta 
                DataTable Dt_Deudor_Proveedor= new DataTable();
                DataTable Dt_Deudor_Empleado = new DataTable();
                DataTable Dt_Temporal = new DataTable();//contendra las fuentes que esten activas aun
                Lbl_Error_Busqueda_Cuenta.Visible = false;
                Img_Error_Busqueda_Cuenta.Visible = false;
                try
                {
                    if (Dt_Deudores.Rows.Count <= 0 && Dt_Deudores.Columns.Count <= 0)
                    {
                        Dt_Deudores.Columns.Add("DEUDOR_ID", typeof(System.String));
                        Dt_Deudores.Columns.Add("NOMBRE", typeof(System.String));
                        Dt_Deudores.Columns.Add("TIPO_DEUDOR", typeof(System.String));
                    }
                    if (!string.IsNullOrEmpty(Txt_Busqueda_Deudor.Text))
                    {
                        Rs_Consulta_Deudores.P_Deudor = Txt_Busqueda_Deudor.Text.ToString().ToUpper();
                    }
                    Dt_Deudor_Proveedor = Rs_Consulta_Deudores.Consulta_Proveedores_Deudores(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                        if (Dt_Deudor_Proveedor.Rows.Count > 0)
                        {
                            foreach (DataRow Fila in Dt_Deudor_Proveedor.Rows)
                            {
                                DataRow row = Dt_Deudores.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["DEUDOR_ID"] = "P" + Fila["PROVEEDOR_ID"].ToString();
                                row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                row["TIPO_DEUDOR"] = "PROVEEDOR";
                                Dt_Deudores.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Deudores.AcceptChanges();
                            }
                        }
                        Dt_Deudor_Empleado = Rs_Consulta_Deudores.Consulta_Empleados_Deudores(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                        if (Dt_Deudor_Empleado.Rows.Count > 0)
                        {
                            if (!string.IsNullOrEmpty(Txt_Busqueda_Deudor.Text))
                            {
                                Dt_Deudor_Empleado.DefaultView.RowFilter = "NOMBRE like'%" + Txt_Busqueda_Deudor.Text.ToString().ToUpper() + "%'";
                            }
                            else
                            {
                                Dt_Deudor_Empleado.DefaultView.Sort = "NOMBRE";
                            }
                            foreach (DataRow Fila in Dt_Deudor_Empleado.DefaultView.ToTable().Rows)
                            {
                                DataRow row = Dt_Deudores.NewRow(); //Crea un nuevo registro a la tabla
                                //Asigna los valores al nuevo registro creado a la tabla
                                row["DEUDOR_ID"] = "E" + Fila["EMPLEADO_ID"].ToString();
                                row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                row["TIPO_DEUDOR"] = "EMPLEADO";
                                Dt_Deudores.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                Dt_Deudores.AcceptChanges();
                            }
                        }
                        if (Dt_Deudores.Rows.Count > 0)
                        {
                            Dt_Deudores.DefaultView.Sort = "NOMBRE";
                            Grid_Deudores.DataSource = null;
                            Grid_Deudores.DataBind();
                            Grid_Deudores.DataSource = Dt_Deudores.DefaultView.ToTable();
                            Grid_Deudores.DataBind();
                        }
                        else
                        {
                            Grid_Deudores.DataSource = null;
                            Grid_Deudores.DataBind();
                        }
                        
                        Grid_Deudores.SelectedIndex = -1;
                        Mpe_Busqueda_Deudor.Show();
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Solicitud_Deuda " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Deudores_Busqueda_2
            /// DESCRIPCION : Ejecuta la busqueda de Cuenta
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  :02/Agosto/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Deudores_Busqueda_2()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Consulta_Deudores = new Cls_Ope_Con_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Deudor = new DataTable();
                DataTable Dt_Temporal = new DataTable();//contendra las fuentes que esten activas aun
                Lbl_Error_Busqueda_Cuenta.Visible = false;
                Img_Error_Busqueda_Cuenta.Visible = false;
                try
                {
                    if (!string.IsNullOrEmpty(Txt_Busqueda_Deudores.Text))
                    {
                        if (Txt_Busqueda_Deudores.Text.Length != 10)
                        {
                            Rs_Consulta_Deudores.P_No_Deuda = String.Format("{0:0000000000}",Convert.ToInt32(Txt_Busqueda_Deudores.Text.ToString().ToUpper()));
                        }
                        else
                        {
                            Rs_Consulta_Deudores.P_No_Deuda = Txt_Busqueda_Deudores.Text.ToString().ToUpper();
                        }
                    }
                    Dt_Deudor= Rs_Consulta_Deudores.Consulta_Deudores(); //Consulta los datos generales de los Tipos de Poliza dados de alta en la BD
                    Grid_Deudores_Diversos.DataSource = null;
                    Grid_Deudores_Diversos.DataBind();
                    if (Dt_Deudor.Rows.Count > 0)
                    {
                        Grid_Deudores_Diversos.DataSource = Dt_Deudor;
                        Grid_Deudores_Diversos.DataBind();
                    }
                    else
                    {
                        Grid_Deudores_Diversos.DataSource=null;
                        Grid_Deudores_Diversos.DataBind();
                    }
                    Txt_Busqueda_Deudores.Text = "";
                    Grid_Deudores_Diversos.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Solicitud_Deuda " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Deudor_Click
            ///DESCRIPCIÓN: Metodo para consultar la reserva
            ///CREO: Sergio Manuel Gallardo Andrade
            ///FECHA_CREO: 02-Agosto-2012
            ///MODIFICO:
            ///FECHA_MODIFICO:
            ///CAUSA_MODIFICACIÓN:
            ///*******************************************************************************
            protected void Btn_Seleccionar_Deudor_Click(object sender, ImageClickEventArgs e)
            {
                String Deudor = ((ImageButton)sender).CommandArgument;
                try
                {
                    Cmb_Deudores.SelectedValue = Deudor;
                    Txt_Busqueda_Deudor.Text = "";
                    Grid_Deudores.DataSource = null;
                    Grid_Deudores.DataBind();
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Click
            ///DESCRIPCIÓN: Realiza la seleccion de un elemento de la tabla
            ///CREO: Sergio Manuel Gallardo Andrade
            ///FECHA_CREO: 04-Agosto-2012
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACIÓN
            ///*******************************************************************************
            protected void Btn_Seleccionar_Click(object sender, EventArgs e)
            {
                Cls_Ope_Con_Deudores_Negocio Consulta_Deudor = new Cls_Ope_Con_Deudores_Negocio();//Variable de conexion con l capa de negocios.
                DataTable Dt_Deudor = null;//Variable que almacenara una lista de los bancos registrados en el sistema.
                String Deudor_ID = "";//Variable que almacena el identificador único del banco.

                try
                {
                        Deudor_ID = ((ImageButton)sender).CommandArgument;

                        Consulta_Deudor.P_No_Deuda = Deudor_ID;
                        Dt_Deudor = Consulta_Deudor.Consulta_Deudores();

                        if (Dt_Deudor is DataTable)
                        {
                            Div_Datos_Deudores.Style.Add("display", "block");
                            Div_Presentacion_Deudores.Style.Add("display", "none");
                            Txt_No_Movimiento.Text = "";
                            Txt_Importe.Text = "";
                            Txt_Fecha_Inicio.Text = "";
                            Txt_Concepto.Text = "";
                            Cmb_Deudores.SelectedIndex = -1;
                            Cmb_Estatus.SelectedIndex = -1;
                            Cmb_Tipo.SelectedIndex = -1;
                            if (Dt_Deudor.Rows.Count > 0)
                            {
                                foreach (DataRow Renglon in Dt_Deudor.Rows)
                                {
                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_No_Deuda].ToString()))
                                        Txt_No_Movimiento.Text = Renglon[OPE_CON_DEUDORES.Campo_No_Deuda].ToString();

                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Importe].ToString()))
                                        Txt_Importe.Text = Renglon[OPE_CON_DEUDORES.Campo_Importe].ToString();

                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Fecha_Limite].ToString()))
                                        Txt_Fecha_Inicio.Text = String.Format("{0:dd/MMM/yyyy}",Convert.ToDateTime(Renglon[OPE_CON_DEUDORES.Campo_Fecha_Limite].ToString()));

                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Estatus].ToString())){
                                        if(Renglon[OPE_CON_DEUDORES.Campo_Estatus].ToString()=="PENDIENTE"){
                                            Cmb_Estatus.SelectedValue  = "1";
                                            }else{
                                                if (Renglon[OPE_CON_DEUDORES.Campo_Estatus].ToString() == "AUTORIZADO")
                                                {
                                                 Cmb_Estatus.SelectedValue  = "2";
                                                }else{
                                                        if(Renglon[OPE_CON_DEUDORES.Campo_Estatus].ToString()=="CANCELADO"){
                                                            Cmb_Estatus.SelectedValue  = "3";
                                                        }else{
                                                            if (Renglon[OPE_CON_DEUDORES.Campo_Estatus].ToString() == "PORPAGAR")
                                                            {
                                                                Cmb_Estatus.SelectedValue = "5";
                                                            }
                                                            else{
                                                                    Cmb_Estatus.SelectedValue = "6";
                                                            }
                                                          }
                                                      }
                                                }
                                    }
                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Concepto].ToString()))
                                        Txt_Concepto.Text = Renglon[OPE_CON_DEUDORES.Campo_Concepto].ToString();

                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Tipo_Movimiento].ToString()))
                                    {
                                        Cmb_Tipo.SelectedValue = Renglon[OPE_CON_DEUDORES.Campo_Tipo_Solicitud_ID].ToString();
                                        //if (Renglon[OPE_CON_DEUDORES.Campo_Tipo_Movimiento].ToString() == "ANTICIPO")
                                        //{
                                        //    Cmb_Tipo.SelectedValue = "1";
                                        //}
                                        //else
                                        //{
                                        //    if (Renglon[OPE_CON_DEUDORES.Campo_Tipo_Movimiento].ToString() == "FONDO FIJO")
                                        //    {
                                        //        Cmb_Tipo.SelectedValue = "2";
                                        //    }
                                        //    else
                                        //    {
                                        //            Cmb_Tipo.SelectedValue = "3";
                                        //    }
                                        //}
                                    }

                                    if (!string.IsNullOrEmpty(Renglon[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString()))
                                    {
                                        if (Renglon[OPE_CON_DEUDORES.Campo_Tipo_Deudor].ToString()=="PROVEEDOR") {
                                            Cmb_Deudores.SelectedValue = "P" + Renglon[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                                        }
                                        else{
                                            Cmb_Deudores.SelectedValue = "E" + Renglon[OPE_CON_DEUDORES.Campo_Deudor_ID].ToString();
                                        }
                                    }
                                }
                            }
                            Btn_Imprimir.Visible = true;
                        }
                    //}
                    //ScriptManager.RegisterStartupScript(UPnl_Bancos, typeof(string), "Imagen", "javascript:Inicializar_Eventos_Bancos();", true);
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error seleccionar un elemento de la tabla. Error: [" + Ex.Message + "]");
                }
            }
        //    ///*******************************************************************************
        //    /// NOMBRE DE LA FUNCION: Llena_Grid_Tipo_Poliza
        //    /// DESCRIPCION : Llena el grid con los Tipos de Poliza que se encuentran en la 
        //    ///               base de datos
        //    /// PARAMETROS  : 
        //    /// CREO        : Yazmin A Delgado Gómez
        //    /// FECHA_CREO  : 09-Junio-2011
        //    /// MODIFICO          :
        //    /// FECHA_MODIFICO    :
        //    /// CAUSA_MODIFICACION:
        //    ///*******************************************************************************
        //    private void Llena_Grid_Tipo_Poliza()
        //    {
        //        DataTable Dt_Tipo_Poliza; //Variable que obtendra los datos de la consulta 
        //        try
        //        {
        //            Grid_Tipo_Poliza.DataBind();
        //            Dt_Tipo_Poliza = (DataTable)Session["Consulta_Tipo_Poliza"];
        //            Grid_Tipo_Poliza.DataSource = Dt_Tipo_Poliza;
        //            Grid_Tipo_Poliza.DataBind();
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception("Llena_Grid_Tipo_Poliza " + ex.Message.ToString(), ex);
        //        }
        //    }
        //#endregion

        //#region (Metodos Validacion)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Deudor
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 09/Junio/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Deudor()
            {
                String Espacios_Blanco;
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                if (String.IsNullOrEmpty(Txt_Importe.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El importe del Deudor. <br />";
                    Datos_Validos = false;
                }
                else
                {
                    if (Convert.ToDouble(Txt_Importe.Text.Trim()) <= 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El importe del Deudor. <br />";
                        Datos_Validos = false;
                    }
                }
                if (String.IsNullOrEmpty(Txt_Concepto.Text.Trim()))
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Concepto del adeudo. <br />";
                    Datos_Validos = false;
                }
                if (Txt_Fecha_Inicio.Text.Trim() == "__/___/____")
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Fecha Limite del Deudor. <br />";
                    Datos_Validos = false;
                }
                if (Cmb_Tipo.SelectedIndex<=0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Tipo de Adeudo que se realizara. <br />";
                    Datos_Validos = false;
                }
                if (Cmb_Deudores.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Nombre del Deudor. <br />";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
        //#endregion

        //#region (Métodos Operación)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Deudor
            /// DESCRIPCION : Da de Alta del Tipo de Poliza con los datos proporcionados por 
            ///               el usuario
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 03-Agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Alta_Deudor()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Alta_Deudor = new Cls_Ope_Con_Deudores_Negocio();  //Variable de conexión hacia la capa de Negocios
                String Deuda = "";
                try
                {
                    Rs_Alta_Deudor.P_Deudor_ID = Cmb_Deudores.SelectedValue.Substring(1);
                    if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "P")
                    {
                        Rs_Alta_Deudor.P_Tipo_Deudor = "PROVEEDOR";
                    }
                    else
                    {
                        Rs_Alta_Deudor.P_Tipo_Deudor = "EMPLEADO";
                    }
                    Rs_Alta_Deudor.P_Tipo_Movimiento = Cmb_Tipo.SelectedItem.Text.ToString();
                    Rs_Alta_Deudor.P_Tipo_Solicitud_ID = Cmb_Tipo.SelectedValue;
                    Rs_Alta_Deudor.P_Fecha_Limite = String.Format("{0:dd/MM/yyyy}",Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    Rs_Alta_Deudor.P_Estatus = Cmb_Estatus.SelectedItem.Text.ToString();
                    if (Txt_Concepto.Text.ToString().Length >240)
                    {
                        Rs_Alta_Deudor.P_Concepto = Txt_Concepto.Text.ToString().Substring(0, 240);
                    }else{
                        Rs_Alta_Deudor.P_Concepto = Txt_Concepto.Text.ToString();
                    }
                    Rs_Alta_Deudor.P_Importe =Txt_Importe.Text.ToString();
                    Rs_Alta_Deudor.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado.ToString();
                    Rs_Alta_Deudor.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado.ToString();
                    Deuda=Rs_Alta_Deudor.Alta_Deudor(); //Da de alto los datos del Tipo de Poliza en la BD
                    Limpia_Controles();             //Limpia los controles del forma
                    Llenar_Grid_Deudores();
                    Habilitar_Controles("Inicial");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Deudor", "alert('La Solicitud No.-"+Deuda+"  fue Exitosa');", true);
                    Imprimir(Deuda);
                }
                catch (Exception ex)
                {
                    throw new Exception("Alta_Tipo_Poliza " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Deudor
            /// DESCRIPCION : Modifica los datos del Deudor por los datos proporcionados
            ///               por el usuario
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo 
            /// FECHA_CREO  : 03-agosto-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************0
            private void Modificar_Deudor()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Modifica_Deudor = new Cls_Ope_Con_Deudores_Negocio();
                try
                {
                    Rs_Modifica_Deudor.P_No_Deuda = Txt_No_Movimiento.Text.Trim();
                    Rs_Modifica_Deudor.P_Importe = Txt_Importe.Text.Trim();
                    if (Txt_Concepto.Text.ToString().Length > 240)
                    {
                        Rs_Modifica_Deudor.P_Concepto = Txt_Concepto.Text.ToString().Substring(0, 240);
                    }
                    else
                    {
                        Rs_Modifica_Deudor.P_Concepto = Txt_Concepto.Text.ToString();
                    }
                    Rs_Modifica_Deudor.P_Deudor_ID = Cmb_Deudores.SelectedValue.Substring(1);
                    if (Cmb_Deudores.SelectedValue.Substring(0, 1) == "P")
                    {
                        Rs_Modifica_Deudor.P_Tipo_Deudor = "PROVEEDOR";
                    }
                    else
                    {
                        Rs_Modifica_Deudor.P_Tipo_Deudor = "EMPLEADO";
                    }
                    Rs_Modifica_Deudor.P_Tipo_Movimiento = Cmb_Tipo.SelectedItem.Text.ToString();
                    Rs_Modifica_Deudor.P_Estatus = Cmb_Estatus.SelectedItem.Text.ToString();
                    Rs_Modifica_Deudor.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Rs_Modifica_Deudor.Modificar_Deudor();
                    Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    Habilitar_Controles("Inicial");
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Deudores", "alert('La Modificación de la Solicitud fue Exitosa');", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Modificar_Solicitud " + ex.Message.ToString(), ex);
                }
            }
        //    ///*******************************************************************************
        //    /// NOMBRE DE LA FUNCION: Eliminar_Tipo_Poliza
        //    /// DESCRIPCION : Elimina los datos del Tipo de Poliza que fue seleccionada por el Usuario
        //    /// PARAMETROS  : 
        //    /// CREO        : Yazmin A Delgado Gómez
        //    /// FECHA_CREO  : 09-Junio-2011
        //    /// MODIFICO          :
        //    /// FECHA_MODIFICO    :
        //    /// CAUSA_MODIFICACION:
        //    ///*******************************************************************************
        //    private void Eliminar_Tipo_Poliza()
        //    {
        //        Cls_Cat_Con_Tipo_Polizas_Negocio Rs_Eliminar_Cat_Con_Tipo_Polizas = new Cls_Cat_Con_Tipo_Polizas_Negocio();

        //        try
        //        {
        //            Rs_Eliminar_Cat_Con_Tipo_Polizas.P_Tipo_Poliza_ID = Txt_Tipo_Poliza_ID.Text.Trim();
        //            Rs_Eliminar_Cat_Con_Tipo_Polizas.Eliminar_Tipo_Poliza();//Elimina el Tipo de Poliza seleccionada por el usuario de la BD

        //            Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
        //            ScriptManager.RegisterStartupScript(this, this.GetType(), "Catalogo de Tipo de Poliza", "alert('La Eliminación del Tipo de Poliza fue Exitosa');", true);
        //        }
        //        catch (Exception ex)
        //        {
        //            throw new Exception("Eliminar_Tipo_Poliza" + ex.Message.ToString(), ex);
        //        }
        //    }
        //#endregion
    #endregion

    //#region (Eventos)
            protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Cat_Con_Parametros_Negocio Rs_Parametros = new Cls_Cat_Con_Parametros_Negocio();
                DataTable Dt_Resultado= new DataTable();
                try
                {
                    if (Btn_Nuevo.ToolTip == "Nuevo")
                    {
                        Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                        Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                        DateTime _DateTime = DateTime.Now;
                        Dt_Resultado=Rs_Parametros.Consulta_Datos_Parametros();
                        if(Dt_Resultado.Rows.Count>0)
                        {
                            int dias = Convert.ToInt16(Dt_Resultado.Rows[0][Cat_Con_Parametros.Campo_Dias_Comprobacion].ToString());
                            _DateTime = _DateTime.AddDays(dias);
                            Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy");
                        }
                
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = false;
                        Img_Error.Visible = false;
                        //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                        if (Validar_Datos_Deudor())
                        {
                            Alta_Deudor(); //Da de alta al deudor con los datos que proporciono el usuario
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    if (Btn_Modificar.ToolTip == "Modificar")
                    {
                        //Si el usuario selecciono un Tipo de Poliza entonces habilita los controles para que pueda modificar la información
                        if (!string.IsNullOrEmpty(Txt_No_Movimiento.Text.Trim()))
                        {
                            if (Cmb_Estatus.SelectedValue == "5" || Cmb_Estatus.SelectedValue == "2" || Cmb_Estatus.SelectedValue == "6")
                            {
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text = "No se puede modificar en el estatus que se encuentra<br />";
                            }
                            else
                            {
                                Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                            }
                        }
                        //Si el usuario no selecciono un Tipo de Poliza le indica al usuario que la seleccione para poder modificar
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Seleccione el Deudor que desea modificar sus datos <br />";
                        }
                    }
                    else
                    {
                       // Si el usuario proporciono todos los datos requeridos entonces modificar los datos del Tipo de Poliza en la BD
                        if (Validar_Datos_Deudor())
                        {
                            if (Cmb_Estatus.SelectedItem.Text.ToString() != "AUTORIZADO" && Cmb_Estatus.SelectedItem.Text.ToString() != "TEMINADO" && Cmb_Estatus.SelectedItem.Text.ToString() != "PREAUTORIZADO" && Cmb_Estatus.SelectedItem.Text.ToString() != "PAGADO" && Cmb_Estatus.SelectedItem.Text.ToString() != "PORPAGAR")
                            {
                                Modificar_Deudor(); //Modifica los datos del Tipo de Poliza con los datos proporcionados por el usuario
                            }
                            else
                            {
                                Lbl_Mensaje_Error.Visible = true;
                                Img_Error.Visible = true;
                                Lbl_Mensaje_Error.Text ="En este proceso no puedes poner el estatus de AUTORIZADO, TERMINADO ó PREAUTORIZADO";
                            }
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Btn_Imprimir_Click
            /// DESCRIPCION : REALIZA LA IMPRESION DE LA SOLICITUD
            /// PARAMETROS  : 
            /// CREO        :SERGIO MANUEL GALLARDO ANDRADE
            /// FECHA_CREO  : 22-AGOSTO-2013
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    if (!String.IsNullOrEmpty(Txt_No_Movimiento.Text))
                    {
                        Imprimir(Txt_No_Movimiento.Text);
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Text = "Es necesario seleccionar una solicitud: <br />";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Tipos_Solicitud
            /// DESCRIPCION : Llena el Cmb_Tipo_Solicitud_Pago con los tipos de solicitud de pago.
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consultar_Tipos_Solicitud()
            {
                Cls_Ope_Con_Deudores_Negocio Rs_Cat_Con_Tipo_Solicitud_Pagos = new Cls_Ope_Con_Deudores_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Tipo_Solicitud; //Variable a obtener los datos de la consulta
                try
                {
                    Rs_Cat_Con_Tipo_Solicitud_Pagos.P_Tipo_Comprobacion = "SI";
                    Dt_Tipo_Solicitud = Rs_Cat_Con_Tipo_Solicitud_Pagos.Consulta_Tipo_Solicitud_Pagos_Combo(); //Consulta los tipos de solicitud que fueron dados de alta en la base de datos
                    Cmb_Tipo.Items.Clear();
                    Cmb_Tipo.DataSource = Dt_Tipo_Solicitud;
                    Cmb_Tipo.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
                    Cmb_Tipo.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                    Cmb_Tipo.DataBind();
                    Cmb_Tipo.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("&larr;Seleccione&rarr;"), ""));
                    Cmb_Tipo.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
                }
            }
            protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    if (Btn_Salir.ToolTip == "Salir")
                    {
                        //Session.Remove("Consulta_Tipo_Polizas");
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                    else
                    {
                        Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                    }
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
            protected void Btn_Buscar_Descripcion_Deudores_Click(object sender, ImageClickEventArgs e)
            {
                try
                {
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Consulta_Deudores_Busqueda_2();
                }
                catch (Exception ex)
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = ex.Message.ToString();
                }
            }
}
