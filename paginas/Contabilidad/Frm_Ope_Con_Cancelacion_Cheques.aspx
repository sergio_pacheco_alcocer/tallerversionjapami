﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Cancelacion_Cheques.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Cancelacion_Cheques" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script language="javascript" type="text/javascript">
        function selec_todo2() {
            var y;
            y = $('#chkAll').is(':checked');
            var $chkBox = $("input:checkbox[id$=Chk_Rechazado]");
            if (y == true) {
                $chkBox.attr("checked", true);
            } else {
                $chkBox.attr("checked", false);
            }
        } 
        function Cancelar_Seleccionados() {
            //limpiar controles
            var a = $("input:checkbox[id$=Chk_Rechazado]")
            var cont = 0;
            var comentario;
            var x1="";
            var y = $('#chkAll').is(':checked');
            if (y == true) {               
                if (document.getElementById("<%=Txt_Comentarios_Todos.ClientID%>").value==''){
                    alert("Faltan los comentarios.");
                    return;
                }
            }
            x1 = confirm('&iquest;Esta seguro que deseas Cancelar los cheques ?');
            if (x1 == true) {
                for (var i = 0; i < a.length; i++) {
                    MostrarProgress();
                    if ($(a[i]).is(':checked') == true) {
                        cont++;
                        var No_Cheque = $(a[i]).parent().attr('class');                    
                        comentario = document.getElementById("<%=Txt_Comentarios_Todos.ClientID%>").value;              
                        var cadena = "Accion=Rechazar_Solicitud&id=" + No_Cheque + "&x=" + comentario + "&";
                        $.ajax({
                            url: "Frm_Ope_Con_Cancelacion_Cheques.aspx?" + cadena,
                            type: 'POST',
                            async: false,
                            cache: false,
                            success: function(data) {
                                //                        Limpiar_Ctlr();
                            }
                        });
                    }
                }
                if (cont == 0) {
                    alert("No tienes ninga Solicitud de Pago Seleccionada");
                    OcultarProgress();
                } else {
                    location.reload();
                }
            }            
        } 
        function Abrir_Popup(Control) {
             $find('Contenedor').show();
             document.getElementById("<%=Hf_No_Cheque_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
             document.getElementById("<%=Hf_Rechazo.ClientID%>").value = 0;
             $("#Tr_Rechazar").hide();
         }
         // El popup para el rechazo de la solicitud
         function Abrir_Popup2(Control) {
             $find('Contenedor').show();
             document.getElementById("<%=Hf_No_Cheque_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
             document.getElementById("<%=Hf_Rechazo.ClientID%>").value = 1;
             $("#Tr_Rechazar").show();
         }
         
         function Cerrar_Modal_Popup() {
            $find('Contenedor').hide();
            Limpiar_Ctlr();
            return false;
        } 
        
        function Limpiar_Ctlr()
        {
            var $chkBox = $("input:checkbox[id$=Chk_Rechazado]");
            document.getElementById("<%=Hf_No_Cheque_Autorizar.ClientID%>").value = "";
            document.getElementById("<%=Hf_Rechazo.ClientID%>").value = "";
            document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
            $chkBox.attr('checked', false);
        }
        
        function Guardar_Comentario() {
            if (document.getElementById("<%=Hf_Rechazo.ClientID%>").value == 0) {
                Autorizar_Solicitud();
            }
            if (document.getElementById("<%=Hf_Rechazo.ClientID%>").value == 1) {
                Rechazar_Cheque();
            }
        }
        function MostrarProgress() {
            $('[id$=Up_Autorizar_Cheques]').show();
        }
        function OcultarProgress() {
            $('[id$=Up_Autorizar_Cheques]').delay(10000).hide();
        }
        function Rechazar_Cheque() {
            var No_Cheque = document.getElementById("<%=Hf_No_Cheque_Autorizar.ClientID%>").value;
            var comentario;
            var $chkBox = $("input:checkbox[id$=Chk_Rechazado]");
            var x1 = "";
            var comentario = "";
                x1 = confirm('&iquest;Esta seguro que deseas Cancelar el cheque ?');
                if (x1 == true) {
                    comentario = document.getElementById("<%=Txt_Comentario.ClientID%>").value;
                    var cadena = "Accion=Rechazar_Cheque&id=" + No_Cheque + "&x=" + comentario + "&";
                    $.ajax({
                    url: "Frm_Ope_Con_Cancelacion_Cheques.aspx?" + cadena,
                        type: 'POST',
                        async: false,
                        cache: false,
                        success: function(data) {
                            Limpiar_Ctlr();
                            location.reload();
                        }
                    });
                } else {
                    Limpiar_Ctlr();
                }
        }
        
         function Cerrar_Modal_Popup_Detalles() {
            $find('Mp_Detalles').hide();
            $("input[id$=Txt_No_Pago_Det]").val('');
            $("input[id$=Txt_No_Reserva_Det]").val('');
            $("input[id$=Txt_Concepto_Reserva_Det]").val('');
            $("input[id$=Txt_Beneficiario_Det]").val('');
            $("input[id$=Txt_Fecha_Solicitud_Det]").val('');
            $("input[id$=Txt_Monto_Solicitud_Det]").val('');
            $("input[id$=Txt_Fecha_Autoriza_Director_Det]").val('');
            $("input[id$=Txt_Comentario_Dir_Det]").val('');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Recepcion_Documentos" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Up_Autorizar_Cheques" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Recepcion_Documentos" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Cancelaci&oacute;n de Cheques</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                        </td>
                    </tr>          
                </table>
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td >
                            <table width="100%" class="estilo_fuente">
                            <tr>
                                <td style ="width:20%;">  Folio Cheque</td>
                                <td style ="width:30%;">
                                 <asp:TextBox ID="Txt_Folio_Busqueda" runat="server" MaxLength="100" TabIndex="10" ToolTip="Buscar No. Solicitud" Width="80%"></asp:TextBox>
                                    <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Folio_Busqueda" runat="server" WatermarkCssClass="watermarked"
                                        WatermarkText="<Ingrese el Folio >" TargetControlID="Txt_Folio_Busqueda" />
                                    <cc1:FilteredTextBoxExtender ID="FTE_Txt_Folio_Busqueda" runat="server" 
                                        TargetControlID="Txt_Folio_Busqueda" FilterType="Numbers" >
                                    </cc1:FilteredTextBoxExtender>
                                </td>
                                <td colspan="2" style ="width:50%;"></td>
                            </tr>
                            <tr> 
                                <td> 
                                    <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="Fecha Inicio"></asp:Label>
                                </td>
                                <td>
                                     <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                    <cc1:FilteredTextBoxExtender ID="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" FilterType="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                        ValidChars="/_" />
                                    <cc1:CalendarExtender ID="Txt_Fecha_Inicial_CalendarExtender" runat="server" TargetControlID="Txt_Fecha_Inicial" PopupButtonID="Btn_Fecha_Inicial" Format="dd/MMM/yyyy" />
                                    <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Inicial" />
                                </td>
                                <td>
                                    <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="80%" Enabled="false"></asp:TextBox>
                                    <cc1:CalendarExtender ID="CalendarExtender3" runat="server" TargetControlID="Txt_Fecha_Final" PopupButtonID="Btn_Fecha_Final" Format="dd/MMM/yyyy" />
                                    <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif" ToolTip="Seleccione la Fecha Final" />
                                </td>
                                <td style ="width:20%;">
                                </td>
                            </tr>
                            <%--<tr>
                                 <td >
                                Tipo Beneficiario
                                </td>
                                <td>
                                    <asp:DropDownList ID="Cmb_Tipo_Beneficiario" runat="server" Width="45%">
                                    </asp:DropDownList>
                                </td>
                            </tr>--%>
                                <tr>
                                   <td >
                                        Beneficiario
                                   </td>
                                   <td colspan="3">
                                       <asp:TextBox ID="Txt_Beneficiario" runat="server" Width="28%" 
                                           ontextchanged="Txt_Beneficiario_TextChanged" AutoPostBack="true"></asp:TextBox>
                                       <asp:DropDownList ID="Cmb_Beneficiario" Width="65%" runat="server" 
                                           onselectedindexchanged="Cmb_Beneficiario_SelectedIndexChanged" AutoPostBack="true"> 
                                       </asp:DropDownList>
                                        <asp:ImageButton ID="Btn_Buscar_No_Cheque" runat="server"  Height="19px" 
                                        ToolTip="Consultar" TabIndex="6" 
                                        ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                        onclick="Btn_Buscar_No_Cheque_Click" />
                                   </td>
                                </tr>
                                <tr>
                                    <td >
                                        <asp:Label ID="Lbl_Comentarios_Todos" runat="server" Text="Comentario Cancelaci&oacute;n"></asp:Label>
                                    </td>
                                    <td colspan="2">
                                        <asp:TextBox ID="Txt_Comentarios_Todos" runat="server" MaxLength="250" Width="100%" />
                                        <cc1:FilteredTextBoxExtender ID="FTE_Txt_Comentarios_Todos" runat="server" 
                                            FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters" 
                                            TargetControlID="Txt_Comentarios_Todos" ValidChars="áéíóúÁÉÍÓÚ " />
                                    </td>
                                    <td align="right">
                                        <asp:Label ID="Lbl_Seleccion_Masiva" runat="server" Text="TODOS" Font-Size="X-Small"></asp:Label>
                                        <input type="checkbox"  id="chkAll"  onclick="selec_todo2();"/>
                                         &nbsp;
                                         <asp:Button ID="Btn_Cancelar" runat="server" Text="Cancelar" CssClass="button" Font-Size="X-Small" 
                                          CausesValidation="false"  Width="75px" OnClick="Btn_Cheque_Click" />
                                         &nbsp;
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;text-align:center;vertical-align:top;"> 
                            <center>
                                <div style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                    <asp:GridView ID="Grid_Solicitud_Pagos" runat="server" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                        EmptyDataText="En este momento no se tienen cheques pendientes por autorizar"
                                        Width="100%"
                                        OnRowDataBound="Grid_Solicitud_Pagos_RowDataBound">
                                        <Columns> 
                                            <asp:BoundField DataField="No_Cheque" HeaderText="No. Cheque">
                                                <HeaderStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small" />
                                                <ItemStyle HorizontalAlign="Left" Width="5%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Folio" HeaderText="FOLIO">
                                                <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                                <ItemStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Beneficiario" HeaderText="BENEFICIARIO">
                                                <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small" />
                                                <ItemStyle HorizontalAlign="Left" Width="20%"  Font-Size="X-Small" />
                                            </asp:BoundField>
                                             <asp:BoundField DataField="Concepto" HeaderText="CONCEPTO">
                                                <HeaderStyle HorizontalAlign="Left" Width="28%" Font-Size="X-Small"  />
                                                <ItemStyle HorizontalAlign="Left" Width="28%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Importe" HeaderText="IMPORTE" DataFormatString="{0:c}">
                                                <HeaderStyle HorizontalAlign="Right" Width="9%" Font-Size="X-Small"  />
                                                <ItemStyle HorizontalAlign="Right" Width="9%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Estatus" HeaderText="ESTATUS">
                                                <HeaderStyle HorizontalAlign="Right" Width="13%" Font-Size="X-Small"  />
                                                <ItemStyle HorizontalAlign="Right" Width="13%" Font-Size="X-Small"  />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="FECHA_EMISION" HeaderText="FECHA" DataFormatString="{0:dd/MMM/yyyy}"  >
                                                <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small" />
                                                <ItemStyle HorizontalAlign="Right" Width="10%" Font-Size="X-Small" />
                                            </asp:BoundField>
                                            <asp:TemplateField  HeaderText= "CANCELAR">
                                                <HeaderStyle HorizontalAlign="center" Width="7%" Font-Size="X-Small" />                                                
                                                <ItemStyle HorizontalAlign="center" Width="7%" Font-Size="X-Small" />
                                                <ItemTemplate >
                                                 <asp:CheckBox id="Chk_Rechazado"  runat="server" CssClass='<%# Eval("No_Cheque") %>'  />
                                                </ItemTemplate>
                                            </asp:TemplateField>                                          
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </center>                                       
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                            <asp:HiddenField ID="Txt_No_Reserva" runat="server" />
                            <asp:HiddenField ID="Hf_Rechazo" runat="server" />
                            <cc1:ModalPopupExtender ID="Mpe_Busqueda" runat="server" BackgroundCssClass="progressBackgroundFilter"  BehaviorID="Contenedor"
                                PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open"  
                                CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"  />  
                                <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" OnClientClick="javascript:return false;" />
                            <asp:HiddenField ID="Hf_No_Cheque_Autorizar" runat="server" />
                        </td>
                    </tr>
                </table>                       
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
        <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag" HorizontalAlign="Center" Width="650px" 
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;" >                         
                    <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" CssClass="estilo_fuente" 
                        style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                        <table width="99%">
                            <tr style="display:none;" id="Tr_Rechazar">
                                <td style="color:Black;font-size:12;font-weight:bold;">
                                   <asp:Image ID="Img_Informatcion_Rechazo" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                                     RECHAZO DE SOLICITUD 
                                </td>
                                <td align="right" style="width:10%;">
                                   <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup();"/>  
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>                                                                          
                           <div style="color: #5D7B9D">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Comentario" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Progress_Upnl_Comentario" runat="server" AssociatedUpdatePanelID="Upnl_Comentario" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress> 
                                                                             
                                                  <table width="100%">
                                                   <tr>
                                                        <td colspan="2">
                                                            <table style="width:80%;">
                                                              <tr>
                                                                <td align="left" >
                                                                  <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                                    Width="24px" Height="24px" style="display:none" />
                                                                    <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                                </td>            
                                                              </tr>         
                                                            </table>  
                                                        </td>
                                                    </tr>     
                                                    <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:15%">
                                                            *Comentarios
                                                        </td>
                                                        <td >
                                                            <asp:TextBox ID="Txt_Comentario" runat="server" Width="99.5%" MaxLength="250" />
                                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Comentario" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                                TargetControlID="Txt_Comentario" ValidChars="áéíóúÁÉÍÓÚ "/>                                                                                            
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>                                    
                                                    <tr id="Tr_Aceptar" style="display:block;"  >
                                                        <td style="width:100%;text-align:left;" colspan="2">
                                                            <center>
                                                               <asp:Button ID="Btn_Comentar" runat="server" Text="Aceptar" CssClass="button"  
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Comentar_Click"/> 
                                                                <asp:Button ID="Bnt_Cancelar" runat="server" Text="Cancelar" CssClass="button"                                                                   
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Cancelar_Click"/> 
                                                            </center>
                                                        </td>                                                 
                                                    </tr>                                                                        
                                                  </table>                                                                                                                                                              
                                            </ContentTemplate>                                                          
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>                                                      
                                    </td>
                                </tr>
                             </table>                                                   
                           </div>                 
                    </asp:Panel>
</asp:Content>

