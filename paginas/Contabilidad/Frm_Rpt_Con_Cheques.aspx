﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Cheques.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Cheques" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                  <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
        
            <div id="Div_Contenido" style="width: 97%; height: 700px;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="4" class="label_titulo">Reporte de Cheques</td>
                    </tr>
                    <tr>
                        <td colspan="4">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="4" style="width:20%;">
                            <!--Bloque de la busqueda-->
                            <asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" 
                                onclick="Btn_Consultar_Click" />
                            <asp:ImageButton ID="Btn_Imprimir" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="NUEVO" ToolTip="Exportar PDF" 
                                onclick="Btn_Imprimir_Click" />  
                            <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Imprimir Excel" ToolTip="Exportar Excel" 
                                onclick="Btn_Imprimir_Excel_Click" />  
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click" />
                        </td>                                 
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Banco</td>
                        <td align="left" colspan="3"><asp:DropDownList ID="Cmb_Bancos" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Tipo</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Tipos" runat="server" Width="150px" AutoPostBack="true" 
                                onselectedindexchanged="Cmb_Tipos_SelectedIndexChanged">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Empleado" Value="Empleado"></asp:ListItem>
                                <asp:ListItem Text="Proveedor" Value="Proveedor"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left">Estatus</td>
                        <td align="left">
                            <asp:DropDownList ID="Cmb_Estatus" runat="server" Width="150px">
                                <asp:ListItem Text="Seleccione" Value=""></asp:ListItem>
                                <asp:ListItem Text="Autorizado" Value="AUTORIZADO"></asp:ListItem>
                                <asp:ListItem Text="Cancelado" Value="CANCELADO"></asp:ListItem>
                                <asp:ListItem Text="Ejercido" Value="EJERCIDO"></asp:ListItem>
                                <asp:ListItem Text="Pagado" Value="PAGADO"></asp:ListItem>
                                <asp:ListItem Text="Pendiente" Value="PENDIENTE"></asp:ListItem>
                                <asp:ListItem Text="Por Comprobar" Value="POR_COMPROBAR"></asp:ListItem>
                                <asp:ListItem Text="Por Pagar" Value="POR_PAGAR"></asp:ListItem>
                                <asp:ListItem Text="Preautorizado" Value="PREAUTORIZADO"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Fecha Inicial</td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Fecha_Inicial" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                            <cc1:filteredtextboxextender id="Txt_Fecha_Inicial_FilteredTextBoxExtender" runat="server"
                                targetcontrolid="Txt_Fecha_Inicial" filtertype="Custom, Numbers, LowercaseLetters, UppercaseLetters"
                                validchars="/_" />
                            <cc1:calendarextender id="Txt_Fecha_Inicial_CalendarExtender" runat="server" targetcontrolid="Txt_Fecha_Inicial"
                                popupbuttonid="Btn_Fecha_Inicial" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Inicial" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                    ToolTip="Seleccione la Fecha Inicial" />
                        </td>
                        <td align="left">Fecha Final</td>
                        <td align="left">                                
                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="150px" Enabled="false"></asp:TextBox>
                            <cc1:calendarextender id="CalendarExtender3" runat="server" targetcontrolid="Txt_Fecha_Final"
                                popupbuttonid="Btn_Fecha_Final" format="dd/MMM/yyyy" />
                            <asp:ImageButton ID="Btn_Fecha_Final" runat="server" ImageUrl="~/paginas/imagenes/paginas/SmallCalendar.gif"
                                ToolTip="Seleccione la Fecha Final" />
                        </td>                    
                    </tr>
                    <tr>
                        <td align="left">
                            <asp:Label ID="Lbl_Empleado_Proveedor" runat="server"></asp:Label>
                        </td>
                        <td align="left">
                            <asp:TextBox ID="Txt_Busqueda" runat="server" Width="150px"></asp:TextBox>
                            <asp:ImageButton ID="Btn_Busqueda_Empleado_Proveedor" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="BUSCAR" 
                                onclick="Btn_Busqueda_Empleado_Proveedor_Click" />
                        </td>
                        <td align="left" colspan="2"><asp:DropDownList ID="Cmb_Empleado_Proveedor" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="4">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left" colspan="4">
                            <asp:GridView ID="Grid_Cheques" runat="server" style="white-space:normal;" 
                                AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" 
                                GridLines="None" PageSize="5" Width="100%" AllowPaging="true" 
                                onpageindexchanging="Grid_Cheques_PageIndexChanging">
                                <RowStyle CssClass="GridItem" />
                                <Columns>
                                    <asp:BoundField DataField="No_Pago" HeaderText="No Pago" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Fecha_Pago" HeaderText="Fecha Pago" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Concepto" HeaderText="Concepto Pago" HeaderStyle-HorizontalAlign="Justify" ItemStyle-HorizontalAlign="Justify" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="No_Solicitud_Pago" HeaderText="No Solicitud" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                    <asp:BoundField DataField="Fecha_Solicitud" HeaderText="Fecha Solicitud" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Prefijo" HeaderText="Prefijo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" />
                                    <asp:BoundField DataField="Mes_Ano" HeaderText="Mes/A&ntilde;o" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Fecha_Poliza" HeaderText="Fecha P&oacute;liza" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" DataFormatString="{0:dd/MMM/yyyy}"  ItemStyle-Font-Size="X-Small"/>
                                    <asp:BoundField DataField="Tipo_Poliza" HeaderText="Tipo P&oacute;liza" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Banco" HeaderText="Banco" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="No_Cheque" HeaderText="Cheque" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Monto" HeaderText="Monto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Proveedor" HeaderText="Proveedor" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small" />
                                    <asp:BoundField DataField="Empleado" HeaderText="Empleado" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" ItemStyle-Font-Size="X-Small" />
                                </Columns>
                                <PagerStyle CssClass="GridHeader" />
                                <SelectedRowStyle CssClass="GridSelected" />
                                <HeaderStyle CssClass="GridHeader" />
                                <AlternatingRowStyle CssClass="GridAltItem" />                            
                            </asp:GridView>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

