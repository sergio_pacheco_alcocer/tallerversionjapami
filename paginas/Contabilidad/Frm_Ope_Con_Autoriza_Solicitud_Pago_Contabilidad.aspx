﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad" Title="Untitled Page" %>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="../../jquery/jquery-1.5.js" type="text/javascript"></script>
 <script type="text/javascript" language="javascript">
        function selec_todo2() {
            var y;
            y = $('#chkAll').is(':checked');
            var $chkBox = $("input:checkbox[id$=Chk_Autorizado]");
            if (y == true) {
                $chkBox.attr("checked", true);
            } else {
                $chkBox.attr("checked", false);
            }
        }
        function Rechazar_Solicitud() {
            //limpiar controles
            OcultarProgress2();
            var No_Solicitud = document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value;
            var comentario;
            var $chkBox = $("input:checkbox[id$=Chk_Rechazado]");
            var x1 = "";
            var comentario = "";
            x1 = confirm('&iquest;Est&aacute; seguro que desea Rechazar la Solicitud de Pago?');
            if (x1 == true) {
                comentario = document.getElementById("<%=Txt_Comentario.ClientID%>").value;
                MostrarProgress();
                var cadena = "Accion=Rechazar_Solicitud&id=" + No_Solicitud + "&x=" + comentario + "&";
                $.ajax({
                    url: "Frm_Ope_Con_Autoriza_Solicitud_Pago_Contabilidad.aspx?" + cadena,
                    type: 'POST',
                    async: false,
                    cache: false,
                    success: function(data) {
                        location.reload();
                    }
                });
            } else {
                $chkBox.attr('checked', false);
                document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
                document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = "";
                document.getElementById("<%=Txt_Rechazo.ClientID%>").value = "";
                document.getElementById("<%=Txt_Cuenta_Contable_ID_Proveedor.ClientID%>").value = "";
                document.getElementById("<%=Txt_Concepto_Solicitud.ClientID%>").value = "";
                document.getElementById("<%=Txt_Cuenta_Contable_ID_Empleado.ClientID%>").value = "";
                document.getElementById("<%=Txt_Monto_Solicitud.ClientID%>").value = "";
            }
        }
        //Modal progress, uso de animación de preogresso, indicador de actividad -------------------------------------------------
        function Validar_Cantidad_Caracteres(control) {
            var limit = 250;
            var len = $(control).val().length; //cuenta los caracteres conforme se van introduciendo
            if (len > limit) {
                control.value = control.value.substring(250, limit); //elimina los caracteres de mas
            }
            if (len > 0) {
                $("#Tr_Aceptar").show();
            } else { $("#Tr_Aceptar").hide(); }
        }
        // Se direcciona hacia el metodo de rechazo o cierre dependiendo de la variable
        function Guardar_Comentario() {
            if (document.getElementById("<%=Txt_Rechazo.ClientID%>").value == 0) {
                Autorizar_Solicitud();
            }
            if (document.getElementById("<%=Txt_Rechazo.ClientID%>").value == 1) {
                Rechazar_Solicitud();
            }
        }
        function MostrarProgress() {
            $('[id$=Up_Autorizacion]').show();
        }
        function OcultarProgress() {
            $('[id$=Up_Autorizacion]').delay(10000).hide();
        }
        // El popup para la autorizacion de la solicitud
        function Abrir_Popup(Control) {
            $find('Contenedor').show();
            document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
            document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 0;
            $("#Tr_Autorizar").show();
            $("#Tr_Rechazar").hide();
        }
        // El popup para el rechazo de la solicitud
        function Abrir_Popup2(Control) {
            $find('Contenedor').show();
            document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
            document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 1;
            $("#Tr_Autorizar").hide();
            $("#Tr_Rechazar").show();
        }
        function OcultarProgress2() {
            $find('Contenedor').hide();
            return false;
        }
        
        function Cerrar_Modal_Popup_Detalles() {
            $find('Mp_Detalles').hide();
            $("input[id$=Txt_No_Pago_Det]").val('');
            $("input[id$=Txt_No_Reserva_Det]").val('');
            $("input[id$=Txt_Concepto_Reserva_Det]").val('');
            $("input[id$=Txt_Beneficiario_Det]").val('');
            $("input[id$=Txt_Fecha_Solicitud_Det]").val('');
            $("input[id$=Txt_Monto_Solicitud_Det]").val('');
            $("input[id$=Txt_Fecha_Autoriza_Director_Det]").val('');
            $("input[id$=Txt_Comentario_Dir_Det]").val('');
            $("input[id$=Txt_Comentario_Recepcion_Doc_Det]").val('');
            $("input[id$=Txt_Fecha_Recepcion_Doc_Det]").val('');
            return false;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManager_Parametros_Contabilidad" runat="server"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Up_Autorizacion" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Parametros_Contabilidad" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Autorizaci&oacute;n de Devengado</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click" />
                        </td>
                        <td style="width:50%">B&uacute;squeda
                            <asp:TextBox ID="Txt_Busqueda_No_Solicitud" runat="server" MaxLength="100" TabIndex="5" ToolTip="Buscar No. Solicitud" AutoPostBack="true" OnTextChanged="Txt_Buscar_No_Solicitud_TextChanged" ></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_No_Solicitud" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese No. Solicitud>" TargetControlID="Txt_Busqueda_No_Solicitud" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Solicitud" runat="server" 
                                TargetControlID="Txt_Busqueda_No_Solicitud" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_No_Solicitud" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Buscar_No_Solicitud_Click" />
                        </td> 
                    </tr>          
                </table>
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td>
                            <table width="99.9%" class="estilo_fuente">
                                    <tr>
                                        <td style="width:30%">
                                        <asp:Label ID="Lbl_Codigo_Barras" runat="server" Text="Codigo Barras"></asp:Label>
                                            <asp:TextBox ID="Txt_Lector_Codigo_Barras" runat="server"  Width="100px" MaxLength="10"  AutoPostBack="true" ontextchanged="Txt_Lector_Codigo_Barras_TextChanged" TabIndex="1"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Lector_Codigo_Barras" runat="server" 
                                                TargetControlID="Txt_Lector_Codigo_Barras" FilterType="Numbers" >
                                            </cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="Lbl_Seleccion_Masiva" runat="server" Text="AUTORIZAR TODOS"></asp:Label>
                                            <input type="checkbox"  id="chkAll"  onclick="selec_todo2();"/>
                                             &nbsp;
                                             <asp:Button ID="Btn_Autozizar" runat="server" ToolTip="Autorizar" Text='Autorizar' CssClass="Img_Button" TabIndex="3"  onclick="Autorizar_Solicitud" Width="15%" />
                                             <%--<input id="Btn_Autozizar" type="button"  class="boton" onclick="Autorizar_Seleccionados();" value="Autorizar" />--%>
                                             &nbsp;
                                        </td>
                                    </tr>
                            </table>
<%--                            <asp:Label ID="Lbl_Seleccion_Masiva" runat="server" Text="AUTORIZAR TODOS"></asp:Label>
                            <input type="checkbox"  id="chkAll"  onclick="selec_todo2();"/>
                             &nbsp;
                             <input id="Btn_Autozizar" type="button"  class="boton" onclick="Autorizar_Seleccionados();" value="Autorizar" />
                             &nbsp;--%>
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;text-align:center;vertical-align:top;"> 
                            <center>
                                <div style="overflow:auto;height:300px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                    <asp:GridView ID="Grid_Solicitud_Pagos" runat="server" 
                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                        EmptyDataText="En este momento no se tienen pagos pendientes por autorizar"
                                        Width="100%" OnSelectedIndexChanged="Grid_Solicitud_Pagos_SelectedIndexChanged"
                                       OnRowDataBound="Grid_Solicitud_Pagos_RowDataBound">
                                        <Columns>
                                            <asp:ButtonField ButtonType="Image" CommandName="Select" 
                                                ImageUrl="~/paginas/imagenes/gridview/blue_button.png"  >
                                                <ItemStyle Width="3%" />
                                            </asp:ButtonField>
                                            <asp:TemplateField HeaderText="Solicitud">
                                                <ItemTemplate>
                                                    <asp:LinkButton ID="Btn__Solicitud" runat="server" Text= '<%# Eval("No_Solicitud_Pago") %>'                                             
                                                    OnClick="Btn_Solicitud_Click" ForeColor="Blue"  />
                                                </ItemTemplate >
                                                <HeaderStyle HorizontalAlign="Center" Width="12%"/>
                                                <ItemStyle HorizontalAlign="Center" Width="12%"/>
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="No_Solicitud_Pago" HeaderText="Solicitud">
                                                <HeaderStyle HorizontalAlign="Left" Width="12%" />
                                                <ItemStyle HorizontalAlign="Left" Width="12%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="No_Reserva" HeaderText="Reserva">
                                                <HeaderStyle HorizontalAlign="center" Width="11%" />
                                                <ItemStyle HorizontalAlign="center" Width="11%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Tipo_Solicitud_Pago_ID"></asp:BoundField>
                                             <asp:BoundField DataField="Tipo_Pago" HeaderText="T.Pago">
                                                <HeaderStyle HorizontalAlign="Left" Width="17%" />
                                                <ItemStyle HorizontalAlign="Left" Width="17%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Concepto" HeaderText="Concepto">
                                                <HeaderStyle HorizontalAlign="Left" Width="29%" />
                                                <ItemStyle HorizontalAlign="Left" Width="29%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString ="{0:c}">
                                                <HeaderStyle HorizontalAlign="Left" Width="12%" />
                                                <ItemStyle HorizontalAlign="Left" Width="12%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                                <HeaderStyle HorizontalAlign="Left" Width="12%" />
                                                <ItemStyle HorizontalAlign="Left" Width="12%" />
                                            </asp:BoundField>
                                            <asp:TemplateField  HeaderText= "Autorizar">
                                                <HeaderStyle HorizontalAlign="center" Width="8%" />
                                                <ItemStyle HorizontalAlign="center" Width="8%" />
                                                <ItemTemplate >
                                                    <asp:CheckBox ID="Chk_Autorizado" runat="server"  CssClass='<%# Eval("No_Solicitud_Pago") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField  HeaderText= "Rechazar">
                                                <HeaderStyle HorizontalAlign="center" Width="8%" />                                                
                                                <ItemStyle HorizontalAlign="center" Width="8%" />
                                                <ItemTemplate >
                                                 <asp:CheckBox ID="Chk_Rechazado"  runat="server" onclick="Abrir_Popup2(this);" CssClass='<%# Eval("No_Solicitud_Pago") %>' />
                                                </ItemTemplate>
                                            </asp:TemplateField>                                          
                                        </Columns>
                                        <SelectedRowStyle CssClass="GridSelected" />
                                        <PagerStyle CssClass="GridHeader" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                    </asp:GridView>
                                </div>
                            </center>
                        </td>
                    </tr>
                    <tr>
                        <td>
                        &nbsp;
                            <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                            <asp:HiddenField ID="Txt_No_Reserva" runat="server" />
                            <asp:HiddenField ID="Txt_Rechazo" runat="server" />
                            <asp:HiddenField ID="Txt_Concepto_Solicitud" runat="server" />
                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Empleado" runat="server" />
                        <cc1:ModalPopupExtender ID="Mpe_Busqueda" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Contenedor"
                            PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open" 
                            CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                            <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                            <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" OnClientClick="javascript:return false;" />
                            <asp:HiddenField ID="Txt_No_Solicitud_Autorizar" runat="server" />
                            
                            <cc1:ModalPopupExtender ID="Mpe_Detalles" runat="server" BackgroundCssClass="popUpStyle"  
                                BehaviorID="Mp_Detalles" TargetControlID="Btn_Comodin_Open_Detalles" 
                                PopupControlID="Pnl_Detalles_Contenedor" CancelControlID="Btn_Comodin_Close_Detalles"
                                 DropShadow="True" DynamicServicePath="" Enabled="True"/>
                            <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close_Detalles" runat="server" Text="" />
                            <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open_Detalles" runat="server" Text="" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
    <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
                    <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
                        style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                        <table width="99%">
                            <tr style="display:none;" id="Tr_Autorizar">
                                <td style="color:Black;font-size:12;font-weight:bold;">
                                   <asp:Image ID="Img_Informatcion_Autorizacion" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                                     AUTORIZACI&Oacute;N DE SOLICITUD 
                                </td>
                            </tr>
                            <tr style="display:none;" id="Tr_Rechazar">
                                <td style="color:Black;font-size:12;font-weight:bold;">
                                   <asp:Image ID="Img_Informatcion_Rechazo" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                                     RECHAZO DE SOLICITUD 
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>                                                                          
                           <div style="color: #5D7B9D">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Comentario" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Progress_Upnl_Comentario" runat="server" AssociatedUpdatePanelID="Upnl_Comentario" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress> 
                                                                             
                                                  <table width="100%">
                                                   <tr>
                                                        <td colspan="2">
                                                            <table style="width:80%;">
                                                              <tr>
                                                                <td align="left" >
                                                                  <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                                    Width="24px" Height="24px" style="display:none" />
                                                                    <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                                </td>            
                                                              </tr>         
                                                            </table>  
                                                        </td>
                                                    </tr>     
                                                   <tr>
                                                        <td style="width:100%" colspan="4">
                                                            <hr />
                                                        </td>
                                                    </tr>                                                                                                  
                                                    <tr>
                                                        <td>
                                                            Ingresa Alg&uacute;n Comentario Sobre la Solicitud de Pago
                                                        </td>                                                       
                                                    </tr>
                                                    <tr>
                                                    <td >
                                                            <asp:TextBox ID="Txt_Comentario" runat="server" Width="99.5%" MaxLength="250" />
                                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Comentario" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                                TargetControlID="Txt_Comentario" ValidChars="áéíóúÁÉÍÓÚ "/>                                                                                            
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td style="width:100%" colspan="4">
                                                            <hr />
                                                        </td>
                                                    </tr>                                    
                                                    <tr id="Tr_Aceptar" style="display:block;"  >
                                                        <td style="width:100%;text-align:left;" colspan="4">
                                                           <center>
                                                                <asp:Button ID="Btn_Comentar" runat="server" Text="Aceptar" CssClass="button"  
                                                                 CausesValidation="false"  Width="200px" OnClick="Btn_Comentar_Click"/> 
                                                                                                                            
                                                                 <asp:Button ID="Bnt_Cancelar" runat="server" Text="Cancelar" CssClass="button"                                                                   
                                                                   CausesValidation="false"  Width="200px" OnClick="Btn_Cancelar_Click"/> 
                                                            </center>
                                                        </td>                                                 
                                                    </tr>                                                                        
                                                  </table>                                                                                                                                                              
                                            </ContentTemplate>                                                          
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>                                                      
                                    </td>
                                </tr>
                             </table>                                                   
                           </div>                 
                    </asp:Panel>
                    
                    <asp:Panel ID="Pnl_Detalles_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px"
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
                        <asp:Panel ID="Pnl_Detalles" runat="server" 
                            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                            <table width="99%">
                                <tr>
                                    <td style="color:Black;font-size:12;font-weight:bold;">
                                         DETALLES DE LA SOLICITUD 
                                    </td>
                                    <td align="right" style="width:10%;">
                                       <asp:ImageButton ID="ImageButton2" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup_Detalles();"/>  
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>                                                                          
                        <center>
                            <div style="color: #5D7B9D; width:100%">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Detalles" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Prg_Detalles" runat="server" AssociatedUpdatePanelID="Upnl_Detalles" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress> 

                                                  <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                                    <tr style="border:outset 1px Silver;">
                                                           <td style="width:25%; font-size:x-small;">No. Pago</td>
                                                            <td style="width:25%;"> 
                                                                <asp:TextBox ID="Txt_No_Pago_Det" runat="server" Width="80%" ReadOnly="true"  font-size="x-small"/>
                                                            </td>
                                                            <td style="width:20%; font-size:x-small;">&nbsp;No. Reserva</td>
                                                            <td style="width:30%;"> 
                                                                <asp:TextBox ID="Txt_No_Reserva_Det" runat="server" Width="98%" ReadOnly="true" style="text-align:right; font-size:x-small;"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Concepto Reserva</td>
                                                            <td colspan="3"> 
                                                                <asp:TextBox ID="Txt_Concepto_Reserva_Det" runat="server" Width="99%" ReadOnly="true" TextMode="MultiLine" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Beneficiario</td>
                                                            <td colspan="3"> 
                                                                <asp:TextBox ID="Txt_Beneficiario_Det" runat="server" Width="99%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Fecha Solicitud Pago</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Fecha_Solicitud_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td style="font-size:x-small;"> &nbsp;Monto</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Monto_Solicitud_Det" runat="server" Width="98%" style="text-align:right;" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Fecha Autoriz&oacute; Director</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Fecha_Autoriza_Director_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td colspan="2"> &nbsp;</td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Fecha Recibio Doc.</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Fecha_Recibio_Documentacion_Fisica" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td style="font-size:x-small;">Recibio Doc.</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Recibio_Documentacion_Fisica" runat="server" Width="99%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Fecha Autoriz&oacute; Doc.</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Doc_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td style="font-size:x-small;">Autoriz&oacute; Doc.</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Aut_Documentacion_Fisica" runat="server" Width="99%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style="font-size:x-small;">Fecha Recibi&oacute; Contabilidad</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Fecha_Recepcion_Doc_Contabilidad" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td style="font-size:x-small;">Recibi&oacute; Contabilidad</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_Recibio_Documentacion_Contabilidad" runat="server" Width="99%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                        </tr>
                                                        <tr id="Tr_Poliza" runat="server">
                                                            <td style="font-size:x-small;">No P&oacute;liza</td>
                                                            <td> 
                                                                <asp:TextBox ID="Txt_No_poliza_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                                            </td>
                                                            <td colspan="2">
                                                            </td>
                                                        </tr>
                                                  </table>
                                                   <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                                    <tr>
                                                        <td style="width:100%">
                                                            <div style=" max-height:150px; overflow:auto; width:100%; vertical-align:top;">
                                                                <table style="width:97%;">
                                                                    <tr>
                                                                        <td style="width:100%;">
                                                                             <asp:GridView ID="Grid_Documentos" runat="server" Width="100%"
                                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                                    OnRowDataBound="Grid_Documentos_RowDataBound"
                                                                                    EmptyDataText="No se encuentra ningun documento">
                                                                                    <Columns>
                                                                                            <asp:TemplateField HeaderText="Link">
                                                                                                <ItemTemplate>
                                                                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink>
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle HorizontalAlign ="Left" width ="7%" />
                                                                                                <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                                            </asp:TemplateField> 
                                                                                            <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                                               <HeaderStyle HorizontalAlign="Center" Width="63%" />
                                                                                               <ItemStyle HorizontalAlign="Center" Width="63%" />
                                                                                           </asp:BoundField>   
                                                                                           <asp:BoundField DataField="MONTO_FACTURA" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                                               <HeaderStyle HorizontalAlign="Right" Width="30%" />
                                                                                               <ItemStyle HorizontalAlign="Right" Width="30%" />
                                                                                           </asp:BoundField>
                                                                                            <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                                                               <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                                               <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                                           </asp:BoundField>
                                                                                           <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                                                               <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                                               <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                                           </asp:BoundField>
                                                                                    </Columns>                                                    
                                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                                    <PagerStyle CssClass="GridHeader" />
                                                                                    <HeaderStyle CssClass="tblHead" />
                                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                </asp:GridView>
                                                                             </td>
                                                                          </tr>
                                                                     </table>
                                                                 </div>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                             </table>
                           </div>
                        </center>
                    </asp:Panel>
</asp:Content>

