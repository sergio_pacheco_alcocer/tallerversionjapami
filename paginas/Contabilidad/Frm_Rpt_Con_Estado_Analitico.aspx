﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Estado_Analitico.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Estado_Analitico" Title="Reporte estado analitico del activo"%>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>


<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <asp:ScriptManager ID="ScriptManager_Polizas" runat="server" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
          <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
                
                <div id="Div_Reporte_Balance_Mensual" style="background-color:#ffffff; width:100%; height:100%;">    
                    <table width="100%" class="estilo_fuente">
                        <tr align="center">
                            <td class="label_titulo">Reporte estado analitico del activo</td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                                <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                            </td>
                        </tr>
                    </table>
                    <table width="98%"  border="0" cellspacing="0">
                        <tr align="center">
                            <td>                
                                <div align="right" class="barra_busqueda">                        
                                    <table style="width:100%;height:28px;">
                                        <tr>
                                            <td align="left" style="width:59%;">
                                                <asp:ImageButton ID="Btn_Reporte_Analitico" runat="server" ToolTip="Generar Reporte" 
                                                    CssClass="Img_Button" TabIndex="1"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                    onclick="Btn_Reporte_Analitico_Click"/>
                                                       
                                                 <asp:ImageButton ID="Btn_Generar_Reporte_Excel" runat="server" CssClass="Img_Button"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" 
                                                    OnClick="Btn_Generar_Reporte_Excel_Click"  />
                                                    
                                                <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                    CssClass="Img_Button" TabIndex="2"
                                                    ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                    onclick="Btn_Salir_Click"/>
                                            </td>
                                            <td align="right" style="width:41%;">&nbsp;</td>       
                                        </tr>         
                                    </table>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    <table width="98%" class="estilo_fuente"> 
                    <tr>
                            <td style="width:20%;text-align:left;" class="button_agregar">*REPORTE:</td>
                            <td  colspan="3" style="width:80%;text-align:left;">
                                <asp:DropDownList id="Cmb_Tipo_Reporte" runat="server" Width="98%">
                                    <asp:ListItem>--- SELECCIONE ---</asp:ListItem>
                                    <asp:ListItem>ESTADO ANALÍTICO DEL ACTIVO</asp:ListItem>
                                    <asp:ListItem>ESTADO DE FLUJO DE EFECTIVO</asp:ListItem>
                                    <asp:ListItem>ESTADO ANALÍTICO DE LA DEUDA Y OTROS PASIVOS</asp:ListItem>
                                    <%--<asp:ListItem>BASE DE DATOS CON MOVIMIENTOS CONTABLES</asp:ListItem>--%>
                                </asp:DropDownList>
                            </td>
                     </tr>
                      <tr >
                        <td style="width:20%;text-align:left;">*Año</td>
                        <td style="width:30%;text-align:left;">
                            <asp:DropDownList id="Cmb_Anio" runat="server" Width="98%" AutoPostBack="true" 
                                OnSelectedIndexChanged="Cmb_Anio_OnSelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td style="width:20%;text-align:left;">*Mes</td>
                        <td style="width:30%;text-align:left;">
                            <asp:DropDownList id="Cmb_Mes" runat="server" Width="95%">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Campo Oculto</td>
                        <td align="left"><asp:TextBox ID="Txt_Campo_Oculto" runat="server" ReadOnly="true" Width="150px"></asp:TextBox></td>
                        <td colspan="2" style="width:100%;text-align:center;">
                            <asp:CheckBox ID="Chk_Movimientos_Saldo_No_Cero" Text="Movimientos con Saldo no Ceros" runat="server" TabIndex="12"/>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="left">
                            <b>NOTA:&nbsp;</b>El campo oculto es un campo que tienen los reportes de CONAC generalmente al lado del encabezado y del mismo color, favor de verificar 
                            que este valor sea el mismo de los reportes descargados de la pagina de CONAC, de lo contrario, hay que modificar este valor en el cat&aacute;logo 
                            de par&aacute;metros.                        
                        </td>
                    </tr>
                    </table>
               </div>
        </ContentTemplate>
         
         <Triggers>
            <%--<asp:AsyncPostBackTrigger ControlID="Btn_Reporte_Balance" EventName="Click" />--%>
            <asp:PostBackTrigger ControlID="Btn_Generar_Reporte_Excel" />            
            <%--<asp:PostBackTrigger ControlID="Btn_Generar_Reporte_Word" />--%>
        </Triggers>
        
    </asp:UpdatePanel>
</asp:Content>
