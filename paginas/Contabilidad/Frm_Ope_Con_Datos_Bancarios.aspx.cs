﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sindicatos.Negocios;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using AjaxControlToolkit;
using System.IO;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using JAPAMI.Catalogo_Con_Proveedores.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Datos_Bancarios : System.Web.UI.Page
{
    #region Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones   //Limpia los controles del forma
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE:         Configuracion_Acceso
    /// DESCRIPCIÓN:    Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS:     No Áplica.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            Botones.Add(Btn_Salir);
            Botones.Add(Btn_Modificar);
            Botones.Add(Btn_Buscar);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION :   Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS:     Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREO:   Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  :   30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region Metodos
    #region(Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
    ///               realizar diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpiar_Controles();
            //Habilitar_Controles("Nuevo");
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Cargar_Grid(-1);
            Btn_Modificar.Visible = false;

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpiar_Controles()
    {
        try
        {
            Txt_Busqueda.Text = "";
            Txt_Banco_Proveedor.Text = "";
            //Txt_Bancos_ID.Text = "";
            Txt_Clabe.Text = "";
            Txt_Cuenta_Banco.Text = "";
            Txt_Sucursal.Text = "";
            Txt_Correo_Tranferencia.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION :   Habilita y Deshabilita los controles de la forma para prepara la página
    ///                 para a siguiente operación
    /// PARAMETROS:     1.- Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                 si es una alta, modificacion
    /// CREO:          Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO:     20/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = false;
                    Btn_Modificar.ToolTip = "Modificar";
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Modificar.CausesValidation = false;
                    Btn_Modificar.Visible = false;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                    Configuracion_Acceso("Frm_Ope_Con_Datos_Bancarios.aspx");
                    break;

                case "Modificar":
                    Habilitado = true;
                    Btn_Modificar.ToolTip = "Actualizar";
                    Btn_Salir.ToolTip = "Cancelar";
                    Btn_Modificar.Visible = true;
                    Btn_Modificar.CausesValidation = true;
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                    Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                    break;
            }
            //  mensajes de error
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Txt_Busqueda.Enabled = !Habilitado;
        }

        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString());
        }
    }
    #endregion

    #region(Validacion)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Validar que se se encuentre todos los datos para continuar con el proceso
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 17/Agosoto/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    public Boolean Validar_Datos()
    {
        String Clabe_Banco;
        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Cls_Ope_Con_Cheques_Bancos_Negocio = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        Cls_Cat_Nom_Bancos_Negocio Rs_Bancos = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();
        String Espacios_Blanco = "";
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "";
        Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        Lbl_Mensaje_Error.Text += Espacios_Blanco + "<br>";
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;

        //  para el banco
        if (Txt_Banco_Proveedor.Text == "")
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario Ingresar el banco del Proveedor<br/>";
            Datos_Validos = false;
        }
        //if (Txt_Bancos_ID.Text.Trim() == String.Empty)
        //{
        //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario indicar el id del banco del proveedor<br/>";
        //    Datos_Validos = false;
        //}
        //if (String.IsNullOrEmpty(Txt_Cuenta_Banco.Text.Trim()))
        //{
        //    //Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario indicar la cuenta del banco del proveedor<br/>";
        //    //Datos_Validos = false;
        //}
        //else
        //{
        //    if (Txt_Cuenta_Banco.Text.Length < 11)
        //    {
        //        Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario introducir los 11 caracteres del Número Cuenta <br/>";
        //        Datos_Validos = false;
        //    }
        //}
        if (String.IsNullOrEmpty(Txt_Clabe.Text.Trim()))
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario indicar la clabe del banco del proveedor<br/>";
            Datos_Validos = false;
        }
        else
        {
            if (Txt_Clabe.Text.Length < 3)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario introducir los 3 caracteres de la clabe<br/>";
                Datos_Validos = false;
            }
        }
        //if (!String.IsNullOrEmpty(Txt_Banco_Proveedor.Text.ToString()) )
        //{
        //    Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Nombre_Banco = Txt_Banco_Proveedor.Text.ToString();
        //    Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.P_Comisiones = "true";
        //    Dt_Consulta = Rs_Cls_Ope_Con_Cheques_Bancos_Negocio.Consultar_Bancos_Existentes();

        //    Clabe_Banco = Dt_Consulta.Rows.Count > 0 ? Dt_Consulta.Rows[0]["BANCO_CLABE"].ToString().Trim() : "";

        //    if (!Txt_Clabe.Text.StartsWith(Clabe_Banco))
        //    {
        //        Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario que la clabe del proveedor contenga la clabe del banco anteriormente agregado<br/>";
        //        Datos_Validos = false;
        //    }
        //    //else
        //    //{
        //    //    Lbl_Mensaje_Error.Text = Lbl_Mensaje_Error.Text + "+ Este banco no tiene asignado la clabe favor de ingresarla en el catalogo de parametros bancarios<br/>";
        //    //    Datos_Validos = false;
        //    //}
        //}
        //if (!Txt_Clabe.Text.EndsWith(Txt_Cuenta_Banco.Text.Trim()))
        //{
        //    Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario que el numero de cuenta este en el número de clabe<br/>";
        //    Datos_Validos = false;
        //}
        if (!String.IsNullOrEmpty(Txt_Correo_Tranferencia.Text))
        {
            Regex Exp_Regular = new Regex("^\\w+([\\.-]?\\w+)*@\\w+([\\.-]?\\w+)*(\\.\\w{2,3})+$");
            Match Comparar = Exp_Regular.Match(Txt_Correo_Tranferencia.Text);

            if (!Comparar.Success)
            {
                Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ El contenido del Correo Electronico es incorrecto<br/>";
                Datos_Validos = false;
            }
        }

        //para el tipo de la cuenta
        if (Cmb_Tipo_Cuenta_Proveedor.SelectedIndex <= 0)
        {
            Lbl_Mensaje_Error.Text += Espacios_Blanco + "+ Es necesario proporcionar el tipo de cuenta del Proveedor<br />";
            Datos_Validos = false;
        }

        Lbl_Mensaje_Error.Visible = true;
        return Datos_Validos;

    }
    #endregion

    #region (Consultas)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid(int Pagina)
    {
        Cls_Cat_Con_Proveedores_Negocio Rs_Bancos_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Dt_Consulta = Rs_Bancos_Negocio.Consultar_Datos_Bancos();

            DataView Dv_Ordenar = new DataView(Dt_Consulta);
            Dv_Ordenar.Sort = "PROVEEDOR_ID";
            Dt_Consulta = Dv_Ordenar.ToTable();


            Session["Grid_Bancos"] = Dt_Consulta;
            Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
            
            //Verificar si hay paginacion
            if (Pagina > -1)
            {
                Grid_Bancos.PageIndex = Pagina;
            }
            Grid_Bancos.DataBind();
        }
        catch (Exception ex)
        {
            throw new Exception("Datos Bancos" + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Grid_Banco
    /// DESCRIPCION : Carga la informacion en el combo banco
    /// PARAMETROS  : 
    /// CREO        : Hugo Enrique Ramírez Aguilera
    /// FECHA_CREO  : 30/Enero/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cargar_Grid_Banco()
    {
        Cls_Cat_Con_Proveedores_Negocio Rs_Bancos_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            if (Txt_Busqueda.Text.Length > 0)
            {
                Rs_Bancos_Negocio.P_Filtro_Dinamico = Txt_Busqueda.Text.Trim();
                Dt_Consulta = Rs_Bancos_Negocio.Consultar_Datos_Bancos();

                DataView Dv_Ordenar = new DataView(Dt_Consulta);
                Dv_Ordenar.Sort = "PROVEEDOR_ID";
                Dt_Consulta = Dv_Ordenar.ToTable();


                Session["Grid_Bancos"] = Dt_Consulta;
                Grid_Bancos.DataSource = (DataTable)Session["Grid_Bancos"];
                Grid_Bancos.PageIndex = 0;
                Grid_Bancos.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Datos Bancos" + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Operaciones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Modificar_Banco
    ///DESCRIPCIÓN: Pasa los elementos a la capa de negocios
    ///PARAMETROS:  
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  30/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    private void Modificar_Banco()
    {
        Cls_Cat_Con_Proveedores_Negocio Rs_Bancos_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
        try
        {
            Rs_Bancos_Negocio.P_Banco_Proveedor = Txt_Banco_Proveedor.Text.ToUpper();
            //Rs_Bancos_Negocio.P_Clabe = Txt_Clabe.Text;
            Rs_Bancos_Negocio.P_Banco_Proveedor_ID = Txt_Clabe.Text;
            Rs_Bancos_Negocio.P_Cuenta = Txt_Cuenta_Banco.Text;
            Rs_Bancos_Negocio.P_Proveedor_ID = Hdf_Proveedor_Id.Value;
            Rs_Bancos_Negocio.P_Sucursal_Banco_Proveedor = Txt_Sucursal.Text;
            Rs_Bancos_Negocio.P_E_Mail_Transferencia = Txt_Correo_Tranferencia.Text;
            Rs_Bancos_Negocio.P_Tipo_Cuenta = Cmb_Tipo_Cuenta_Proveedor.SelectedItem.Value;
            Rs_Bancos_Negocio.Actualiza_Bacos_Datos();
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Datos Bancarios", "alert('La modificacion de los datos bancarios fue Exitosa');", true);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion

    #endregion

    #region Eventos

    #region (Botones)
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Modificar_Click
    ///DESCRIPCIÓN: realizara una modificaion
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  20/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Modificar.ToolTip == "Modificar")
            {
                //if (Txt_Bancos_ID.Text != "")
                    Habilitar_Controles("Modificar");

                //else
                //{
                //    Lbl_Mensaje_Error.Visible = true;
                //    Img_Error.Visible = true;
                //    Lbl_Mensaje_Error.Text = "*Seleccione algun registro de la tabla.<br>";
                //}
            }
            else
            {
                if (Validar_Datos())
                {
                    Modificar_Banco();
                    Div_Grid.Style.Value = "overflow:auto;height:auto;width:100%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
                    Div_Detalles.Style.Value = "display:none";
                    Inicializa_Controles();
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Buscar
    ///DESCRIPCIÓN: Busca los bancos
    ///PARAMETROS: 
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  17/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Buscar_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Cargar_Grid_Banco();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Salir_Click
    ///DESCRIPCIÓN: Cancela la operacion actual que se este realizando
    ///PARAMETROS: 
    ///CREO:        Hugo Enrique Ramírez Aguilera
    ///FECHA_CREO:  30/Enero/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        //Div_Grid.Style.Value = "overflow:auto;height:200px;width:98%;vertical-align:top;border-style:outset;border-color:Silver;display:block"";
        //            Div_Detalles.Style.Value = "display:none";
        if ((Btn_Salir.ToolTip == "Cancelar") || (Div_Grid.Style.Value == "display:none"))
        {
            Div_Grid.Style.Value = "overflow:auto;height:auto;width:100%;vertical-align:top;border-style:outset;border-color:Silver;display:block";
            Div_Detalles.Style.Value = "display:none";
            Inicializa_Controles();
            Grid_Bancos.SelectedIndex = -1;
        }
        else
        {
            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Banco_Proveedor_TextChanged
    ///DESCRIPCIÓN: Busca los bancos
    ///PARAMETROS: 
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  17/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Txt_Banco_Proveedor_TextChanged(object sender, EventArgs e)
    {
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();

        Bancos_Negocio.P_Nombre = Txt_Banco_Proveedor.Text.Trim().ToUpper();
        Dt_Consulta = Bancos_Negocio.Consulta_Bancos();

        if (Dt_Consulta.Rows.Count > 0)
        {
            //Txt_Bancos_ID.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Banco_Clabe].ToString().Trim();
            Txt_Clabe.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Banco_Clabe].ToString().Trim();
            if (!String.IsNullOrEmpty(Txt_Cuenta_Banco.Text))
                Txt_Clabe.Text += Txt_Cuenta_Banco.Text;
        }
        else
        {
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Datos Bancarios", "alert('No se encontro el banco');", true);
            Txt_Clabe.Text = Txt_Cuenta_Banco.Text;
        }
    }

    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Txt_Cuenta_Banco_TextChanged
    ///DESCRIPCIÓN: Busca los bancos
    ///PARAMETROS: 
    ///CREO:        Armando Zavala Moreno
    ///FECHA_CREO:  17/Agosto/2011
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Txt_Cuenta_Banco_TextChanged(object sender, EventArgs e)
    {
        Cls_Cat_Nom_Bancos_Negocio Bancos_Negocio = new Cls_Cat_Nom_Bancos_Negocio();
        DataTable Dt_Consulta = new DataTable();

        Bancos_Negocio.P_Nombre = Txt_Banco_Proveedor.Text.Trim().ToUpper();
        Dt_Consulta = Bancos_Negocio.Consulta_Bancos();

        if (Dt_Consulta.Rows.Count > 0)
        {
            Txt_Clabe.Text = Dt_Consulta.Rows[0][Cat_Nom_Bancos.Campo_Banco_Clabe].ToString().Trim() + Txt_Cuenta_Banco.Text.Trim();
        }
        else
        {
            Txt_Clabe.Text = Txt_Cuenta_Banco.Text.Trim();
        }
    }

    #endregion

    #endregion

    #region Grids
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Grid_Bancos_SelectedIndexChanged
    /// DESCRIPCION : Consulta los datos de los bancos seleccionada por el usuario
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 17/Agosto/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Grid_Bancos_SelectedIndexChanged(object sender, EventArgs e)
    {
        Cls_Cat_Con_Proveedores_Negocio Rs_Bancos_Negocio = new Cls_Cat_Con_Proveedores_Negocio();
        DataTable Dt_Consulta = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Hdf_Proveedor_Id.Value = Grid_Bancos.DataKeys[Grid_Bancos.SelectedRow.RowIndex].Value.ToString();
            //Txt_Proveedor.Text = HttpUtility.HtmlDecode(Grid_Bancos.Rows[Grid_Bancos.SelectedRow.RowIndex].Cells[2].Text.ToString().Trim());
            Rs_Bancos_Negocio.P_Proveedor_ID = Hdf_Proveedor_Id.Value;
            Dt_Consulta = Rs_Bancos_Negocio.Consulta_Datos_Proveedores();
            if (Dt_Consulta != null && Dt_Consulta.Rows.Count > 0)
            {
                Txt_Proveedor.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Nombre].ToString();
                Txt_Banco_Proveedor.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor].ToString();
                //Txt_Bancos_ID.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString();
                Txt_Clabe.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Banco_Proveedor_ID].ToString();
                Txt_Cuenta_Banco.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Cuenta].ToString();
                Txt_Sucursal.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Sucursal_Banco_Proveedor].ToString();

                if (String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_E_Mail_Transferencia].ToString().Trim()))
                {
                    if (!String.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Trim()))
                    {
                        if (Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Contains(";"))
                            Txt_Correo_Tranferencia.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().Substring(0, Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString().IndexOf(';'));
                        else
                            Txt_Correo_Tranferencia.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Correo_Electronico].ToString();
                    }
                }
                else
                {
                    Txt_Correo_Tranferencia.Text = Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_E_Mail_Transferencia].ToString();
                }

                //verificar si esta el tipo de cuenta
                if (string.IsNullOrEmpty(Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Cuenta].ToString()) == false)
                {
                    Cmb_Tipo_Cuenta_Proveedor.SelectedIndex = Cls_Util.Entrega_Indice_Combo(Cmb_Tipo_Cuenta_Proveedor, Dt_Consulta.Rows[0][Cat_Com_Proveedores.Campo_Tipo_Cuenta].ToString().Trim());
                }

                Div_Grid.Style.Value = "display:none";
                Div_Detalles.Style.Value = "display:block";

                Txt_Busqueda.Enabled = false;

                Habilitar_Controles("Modificar");
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }

    protected void Grid_Bancos_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            Cargar_Grid(e.NewPageIndex);
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message;
        }
    }
    #endregion
}
