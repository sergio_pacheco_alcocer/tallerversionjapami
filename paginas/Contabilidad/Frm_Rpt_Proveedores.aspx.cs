﻿using System;
using System.Text;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.Security;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using JAPAMI.Reporte_Proveedores.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Autoriza_Ejercido.Negocio;

public partial class paginas_Contabilidad_Frm_Rpt_Proveedores : System.Web.UI.Page
{
    #region (Page load)
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
            if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

            if (!IsPostBack)
            {
                //Cmb_Proveedor.SelectedIndex=0;//Limpia los controles de la forma
                Inicializa_Controles();
                DateTime _DateTime = DateTime.Now;
                int dias = _DateTime.Day;
                dias = dias * -1;
                dias++;
                _DateTime = _DateTime.AddDays(dias);
                Txt_Fecha_Inicio.Text = _DateTime.ToString("dd/MMM/yyyy").ToUpper();
                Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
            }
        }
        catch (Exception ex)
        {
            Mensaje_Error(ex.Message.ToString());
        }
    }
    #endregion
    #region (Metodos)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
    ///               reporte
    /// PARAMETROS  : 
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 08-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles(); //limpia los campos de la forma
            Llenar_Combo_Proveedores();
        }
        catch (Exception ex)
        {
            throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cargar_Cuentas_Contables_Combos
    /// DESCRIPCION : Consulta las cuentas contables estan dadas de alta
    /// PARAMETROS  :                           
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 08-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Llenar_Combo_Proveedores()
    {
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Proveedores;
        DataTable Dt_Proveedores_Tipo = new DataTable();
        try
        {
            Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
            Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor_Con_Cuenta(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
            Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
            Cmb_Proveedor_Solicitud_Pago.DataBind();
            Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores;
            Cmb_Proveedor_Solicitud_Pago.DataTextField = "descripcion_cuenta";
            Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
            Cmb_Proveedor_Solicitud_Pago.DataBind();
            Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
            Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Yazmin Abigail Delgado Gómez
    /// FECHA_CREO  : 08-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
            Cmb_Estatus.SelectedIndex = -1;
            Txt_Fecha_Final.Enabled = false;
            Txt_Fecha_Inicio.Enabled = false;
            Session["Dt_Datos_Detalles"] = null;
            Session["Contador"] = null;

        }
        catch (Exception ex)
        {
            throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Buscar_Proveedor_Solicitud_Pagos_Click
    /// DESCRIPCION : Consulta a todos los proveedores que coincidan con el nombre, rfc
    ///               o compañia de acuerdo a lo proporcionado por el usuario
    /// PARAMETROS  : 
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 23-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Buscar_Proveedor_Solicitud_Pagos_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Cat_Com_Proveedores_Negocio Rs_Consulta_Cat_Com_Proveedores = new Cls_Cat_Com_Proveedores_Negocio(); //Variable de conexión hacia la capa de negocios
        DataTable Dt_Proveedores;
        DataTable Dt_Proveedores_Tipo = new DataTable();
        DataRow Fila;
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Nombre_Proveedor_Solicitud_Pago.Text))
            {
                Rs_Consulta_Cat_Com_Proveedores.P_Nombre_Comercial = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
            }
                Rs_Consulta_Cat_Com_Proveedores.P_Estatus = "ACTIVO";
                Dt_Proveedores = Rs_Consulta_Cat_Com_Proveedores.Consulta_Avanzada_Proveedor_Con_Cuenta(); //Consulta los proveedores que coincidan con el nombre, compañia, rfc
                Cmb_Proveedor_Solicitud_Pago.DataSource = new DataTable();
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.DataSource = Dt_Proveedores;
                Cmb_Proveedor_Solicitud_Pago.DataTextField = "descripcion_cuenta";
                Cmb_Proveedor_Solicitud_Pago.DataValueField = Cat_Com_Proveedores.Campo_Proveedor_ID;
                Cmb_Proveedor_Solicitud_Pago.DataBind();
                Cmb_Proveedor_Solicitud_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                if (Cmb_Proveedor_Solicitud_Pago.Items.Count == 2)
                {
                    Cmb_Proveedor_Solicitud_Pago.SelectedIndex = 1;
                }
                else
                {
                    Cmb_Proveedor_Solicitud_Pago.SelectedIndex = -1;
                }


        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mensaje_Error
    /// DESCRIPCION : Funcion para lanzar un mensaje de error
    /// PARAMETROS  : Mensaje: Cadena de caracteres que se va a desplegar como mensaje de error
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 18-Marzo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mensaje_Error(String Mensaje)
    {
        Lbl_Mensaje_Error.Visible = true;
        Img_Error.Visible = true;
        Lbl_Mensaje_Error.Text = Mensaje;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mensaje_Error
    /// DESCRIPCION : Funcion limpiar el mensaje de error
    /// PARAMETROS  : 
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 18-Marzo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mensaje_Error()
    {
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Lbl_Mensaje_Error.Text = "";
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos
    /// DESCRIPCION : Valida las fechas
    /// PARAMETROS  : 
    /// CREO        : Armando Zavala Moreno
    /// FECHA_CREO  : 19-Abril-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos()
    { 
        Boolean Datos_Correctos=true;
        DateTime Fecha_Ini;
        DateTime Fecha_Final;
        Fecha_Ini = Convert.ToDateTime(Txt_Fecha_Inicio.Text);
        Fecha_Final = Convert.ToDateTime(Txt_Fecha_Final.Text);
        if (Fecha_Ini > Fecha_Final)
        {
            Datos_Correctos = false;
        }        
        return Datos_Correctos;
    }

    #region (Reportes)
    ///*******************************************************************************************************
    /// NOMBRE_FUNCIÓN: Exportar_Reporte
    /// DESCRIPCIÓN: Genera el reporte de Crystal con los datos proporcionados en el DataTable 
    /// PARÁMETROS:
    /// 		1. Ds_Reporte: Dataset con datos a imprimir
    /// 		2. Nombre_Reporte: Nombre del archivo de reporte .rpt
    /// 		3. Nombre_Archivo: Nombre del archivo a generar
    /// CREO: Roberto González Oseguera
    /// FECHA_CREO: 04-sep-2011
    /// MODIFICÓ: 
    /// FECHA_MODIFICÓ: 
    /// CAUSA_MODIFICACIÓN: 
    ///*******************************************************************************************************
    private void Exportar_Reporte(DataSet Ds_Reporte, String Nombre_Reporte, String Nombre_Archivo, String Extension_Archivo, ExportFormatType Formato)
    {
        ReportDocument Reporte = new ReportDocument();
        String Ruta = Server.MapPath("../Rpt/Contabilidad/" + Nombre_Reporte);

        try
        {
            Reporte.Load(Ruta);
            Reporte.SetDataSource(Ds_Reporte);
        }
        catch
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "No se pudo cargar el reporte";
        }

        String Archivo_Reporte = Nombre_Archivo + "." + Extension_Archivo;  // formar el nombre del archivo a generar 
        try
        {
            ExportOptions Export_Options_Calculo = new ExportOptions();
            DiskFileDestinationOptions Disk_File_Destination_Options_Calculo = new DiskFileDestinationOptions();
            Disk_File_Destination_Options_Calculo.DiskFileName = Server.MapPath("../../Reporte/" + Archivo_Reporte);
            Export_Options_Calculo.ExportDestinationOptions = Disk_File_Destination_Options_Calculo;
            Export_Options_Calculo.ExportDestinationType = ExportDestinationType.DiskFile;
            Export_Options_Calculo.ExportFormatType = Formato;
            Reporte.Export(Export_Options_Calculo);

            if (Formato == ExportFormatType.Excel)
            {
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-excel");
            }
            else if (Formato == ExportFormatType.WordForWindows)
            {
                Mostrar_Excel(Server.MapPath("../../Reporte/" + Archivo_Reporte), "application/vnd.ms-word");
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Excel
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en excel.
    ///              
    /// PARÁMETROS: No Aplica
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 10/Diciembre/2011.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    private void Mostrar_Excel(string Ruta_Archivo, string Contenido)
    {
        try
        {
            System.IO.FileInfo ArchivoExcel = new System.IO.FileInfo(Ruta_Archivo);
            if (ArchivoExcel.Exists)
            {
                Response.Clear();
                Response.Buffer = true;
                Response.ContentType = Contenido;
                Response.AddHeader("Content-Disposition", "attachment;filename=" + ArchivoExcel.Name);
                Response.Charset = "UTF-8";
                Response.ContentEncoding = Encoding.Default;
                Response.WriteFile(ArchivoExcel.FullName);
                Response.End();
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte en excel. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Generar_Reporte
    /// 
    /// DESCRIPCIÓN: Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS: Nombre_Plantilla_Reporte.- Nombre del archivo del Crystal Report.
    ///             Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Generar_Reporte(ref DataSet Ds_Datos, String Nombre_Plantilla_Reporte, String Nombre_Reporte_Generar)
    {
        ReportDocument Reporte = new ReportDocument();//Variable de tipo reporte.
        String Ruta = String.Empty;//Variable que almacenara la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Nombre_Plantilla_Reporte);
            Reporte.Load(Ruta);

            if (Ds_Datos is DataSet)
            {
                if (Ds_Datos.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Datos);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar);
                    Mostrar_Reporte(Nombre_Reporte_Generar);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Exportar_Reporte_PDF
    /// 
    /// DESCRIPCIÓN: Método que guarda el reporte generado en formato PDF en la ruta
    ///              especificada.
    ///              
    /// PARÁMETROS: Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///             Nombre_Reporte.- Nombre que se le dará al reporte.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE: Mostrar_Reporte
    /// 
    /// DESCRIPCIÓN: Muestra el reporte en pantalla.
    ///              
    /// PARÁMETROS: Nombre_Reporte.- Nombre que tiene el reporte que se mostrara en pantalla.
    /// 
    /// USUARIO CREO: Juan Alberto Hernández Negrete.
    /// FECHA CREO: 3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Mostrar_Reporte(String Nombre_Reporte)
    {
        String Pagina = "../../Reporte/"; //"../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt_Empleados",
                "window.open('" + Pagina + "', 'Reporte_Proveedores_Pagos','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600');", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
    #endregion

    #region (Eventos)
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Mostrar_Reporte_Proveedores(); //Consulta el libro de mayor de la cuenta seleccionada
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
    /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
    ///               generación del reporte
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 08-Octubre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Validar_Datos_Reporte()
    {
        Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
        Lbl_Mensaje_Error.Text = "Se requiere que: <br>";
        if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La fecha inicial no puede ser mayor a la fecha final <br>";
        }

        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Mostrar_Reporte_Proveedores
    /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
    ///               por el usuario desde el primer mes hasta el mes seleccionado
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 08-Noviembre-2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Mostrar_Reporte_Proveedores()
    {
        DataTable Dt_Proveedor = new DataTable();       //Va a conter los valores de la consulta realizada
        DataTable Dt_Datos_Detalles = new DataTable();       //Va a conter los valores de la consulta realizada
        DataTable Dt_Proveedores_Depurados = new DataTable();       //Va a conter los valores de la consulta realizada
        Cls_Rpt_Con_Proveedores_Negocio Rs_Proveedores = new Cls_Rpt_Con_Proveedores_Negocio();
        Lbl_Mensaje_Error.Visible = false;
        Img_Error.Visible = false;
        Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
        int Contador = 0;
        Session["Contador"] = Contador;
        Boolean Insertar = true;
        try
        {
            if (Cmb_Proveedor_Solicitud_Pago.SelectedIndex > 0)
            {
                Rs_Proveedores.P_Proveedor_ID = Cmb_Proveedor_Solicitud_Pago.SelectedValue;
            }
            if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
            {
                Rs_Proveedores.P_Estatus = "PENDIENTES";
            }
            Rs_Proveedores.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
            Rs_Proveedores.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
            Dt_Proveedor = Rs_Proveedores.Consulta_Proveedores(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
            if (Dt_Proveedores_Depurados.Rows.Count <= 0)
            {
                Dt_Proveedores_Depurados.Columns.Add("PROVEEDOR_ID", typeof(System.String));
                Dt_Proveedores_Depurados.Columns.Add("COMPANIA", typeof(System.String));
                Dt_Proveedores_Depurados.Columns.Add("CUENTA", typeof(System.String));
                Dt_Proveedores_Depurados.Columns.Add("NOMBRE", typeof(System.String));
            }
            if (Dt_Proveedor.Rows.Count > 0)
            {
                if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
                {
                    Dt_Proveedor.DefaultView.RowFilter = "FECHA_PAGO IS NULL";
                    if (Dt_Proveedor.DefaultView.ToTable().Rows.Count > 0)
                    {
                       foreach(DataRow Fila in Dt_Proveedor.DefaultView.ToTable().Rows ){
                           DataRow row = Dt_Proveedores_Depurados.NewRow(); //Crea un nuevo registro a la tabla
                           String Proveedor = Fila["PROVEEDOR_ID"].ToString();
                           Insertar = true;
                           foreach(DataRow Fila2 in Dt_Proveedores_Depurados.Rows){
                               if (Proveedor.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                               {
                                   Insertar = false;
                               }
                           }
                           if (Insertar)
                           {
                               row["PROVEEDOR_ID"] = Fila["PROVEEDOR_ID"].ToString();
                               row["COMPANIA"] = Fila["COMPANIA"].ToString();
                               row["CUENTA"] = Fila["CUENTA"].ToString();
                               row["NOMBRE"] = Fila["NOMBRE"].ToString();
                               Dt_Proveedores_Depurados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                               Dt_Proveedores_Depurados.AcceptChanges();
                           }
                       }
                       Session["Dt_Datos_Detalles"] = Dt_Proveedores_Depurados;
                       Grid_Proveedores.DataSource = Dt_Proveedores_Depurados;
                    }
                    else
                    {
                        Grid_Proveedores.DataSource = null;
                        Grid_Proveedores.DataBind();
                        Btn_Reporte_Proveedores.Visible = false;
                        Tr_Grid_Proveedor.Style.Add("display", "none");
                        Lbl_Mensaje_Error.Text = "No tiene Movimientos esta cuenta en el rango de fechas que coloco <br>";
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }else{
                        if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PAGADOS"))
                        {
                            Dt_Proveedor.DefaultView.RowFilter = "FECHA_PAGO IS NOT NULL";
                            if (Dt_Proveedor.DefaultView.ToTable().Rows.Count > 0)
                                {
                                    foreach (DataRow Fila in Dt_Proveedor.DefaultView.ToTable().Rows)
                                    {
                                        DataRow row = Dt_Proveedores_Depurados.NewRow(); //Crea un nuevo registro a la tabla
                                        String Proveedor = Fila["PROVEEDOR_ID"].ToString();
                                        Insertar = true;
                                        foreach (DataRow Fila2 in Dt_Proveedores_Depurados.Rows)
                                        {
                                            if (Proveedor.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                                            {
                                                Insertar = false;
                                            }
                                        }
                                        if (Insertar)
                                        {
                                            row["PROVEEDOR_ID"] = Fila["PROVEEDOR_ID"].ToString();
                                            row["COMPANIA"] = Fila["COMPANIA"].ToString();
                                            row["CUENTA"] = Fila["CUENTA"].ToString();
                                            row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                            Dt_Proveedores_Depurados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                            Dt_Proveedores_Depurados.AcceptChanges();
                                        }
                                    }
                                    Session["Dt_Datos_Detalles"] = Dt_Proveedores_Depurados;
                                    Grid_Proveedores.DataSource = Dt_Proveedores_Depurados;
                                 }
                                else
                                {
                                    Grid_Proveedores.DataSource = null;
                                    Grid_Proveedores.DataBind();
                                    Btn_Reporte_Proveedores.Visible = false;
                                    Tr_Grid_Proveedor.Style.Add("display", "none");
                                    Lbl_Mensaje_Error.Text = "No tiene Movimientos esta cuenta en el rango de fechas que coloco <br>";
                                    Lbl_Mensaje_Error.Visible = true;
                                    Img_Error.Visible = true;
                                }
                        }
                        else
                        {
                            foreach (DataRow Fila in Dt_Proveedor.Rows)
                            {
                                DataRow row = Dt_Proveedores_Depurados.NewRow(); //Crea un nuevo registro a la tabla
                                String Proveedor = Fila["PROVEEDOR_ID"].ToString();
                                Insertar = true;
                                foreach (DataRow Fila2 in Dt_Proveedores_Depurados.Rows)
                                {
                                    if (Proveedor.Equals(Fila2["PROVEEDOR_ID"].ToString()))
                                    {
                                        Insertar = false;
                                    }
                                }
                                if (Insertar)
                                {
                                    row["PROVEEDOR_ID"] = Fila["PROVEEDOR_ID"].ToString();
                                    row["COMPANIA"] = Fila["COMPANIA"].ToString();
                                    row["CUENTA"] = Fila["CUENTA"].ToString();
                                    row["NOMBRE"] = Fila["NOMBRE"].ToString();
                                    Dt_Proveedores_Depurados.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Proveedores_Depurados.AcceptChanges();
                                }
                            }
                            Session["Dt_Datos_Detalles"] = Dt_Proveedores_Depurados;
                            Grid_Proveedores.DataSource = Dt_Proveedores_Depurados;
                        }
                    }
                Grid_Proveedores.DataBind();
                Btn_Reporte_Proveedores.Visible = true;
                Tr_Grid_Proveedor.Style.Add("display", "");
            }
            else
            {
                Grid_Proveedores.DataSource = null;
                Grid_Proveedores.DataBind();
                Btn_Reporte_Proveedores.Visible = false;
                Tr_Grid_Proveedor.Style.Add("display", "none");
                Lbl_Mensaje_Error.Text = "No tiene Movimientos esta cuenta en el rango de fechas que coloco <br>";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
        }
    }
    #region "POLIZA"
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Btn_Poliza_Click
    //'DESCRIPCION : genera la consulta para que muestre la poliza
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String Mes_Anio =((LinkButton)sender).CommandArgument;
        String No_poliza = ((LinkButton)sender).Text;
        String Tipo_Poliza = ((LinkButton)sender).CssClass;
        String No_Solicitud = ((LinkButton)sender).CommandName;
        Imprimir(No_poliza, Tipo_Poliza, Mes_Anio, No_Solicitud);
    }
    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Imprimir
    //'DESCRIPCION : genera la consulta para que muestre la poliza
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA, String Solicitud)
    {
        DataTable Dt_Pagos = null;
        Cls_Ope_Con_Autoriza_Ejercido_Negocio Rs_Ejercido = new Cls_Ope_Con_Autoriza_Ejercido_Negocio();
        DataTable Dt_Beneficiario = new DataTable();
        DataSet Ds_Reporte = null;
        try
        {
            Rs_Ejercido.P_No_Solicitud_Pago = Solicitud;
            Dt_Beneficiario = Rs_Ejercido.Consulta_Solicitud_Pago_Completa();
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Poliza.P_Mes_Ano = FECHA;
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            if (Dt_Pagos.Rows.Count > 0)
            {
                foreach (DataRow registro in Dt_Pagos.Rows)
                {
                    registro.BeginEdit();
                    registro["BENEFICIARIO"] = Dt_Beneficiario.Rows[0]["COMPANIA"].ToString();
                    registro.EndEdit();
                    registro.AcceptChanges();
                    Dt_Pagos.AcceptChanges();
                }
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte_Poliza(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte_Poliza(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion

    //'****************************************************************************************
    //'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
    //'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
    //'PARAMETROS  : 
    //'CREO        : Sergio Manuel Gallardo
    //'FECHA_CREO  : 16/julio/2011 10:01 am
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    protected void Grid_Proveedores_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        GridView Gv_Detalles = new GridView();
        DataTable Dt_Datos_Detalles = new DataTable();
        DataTable Ds_Consulta = new DataTable();
        Cls_Rpt_Con_Proveedores_Negocio Proveedores_Negocios = new Cls_Rpt_Con_Proveedores_Negocio();
        String Proveedor_ID="";
        int Contador;
        Image Img = new Image();
        Img=(Image)e.Row.FindControl("Img_Btn_Expandir");
        Literal Lit= new Literal();
        Lit=(Literal)e.Row.FindControl("Ltr_Inicio");
        Label Lbl_facturas= new Label();
        Lbl_facturas=(Label)e.Row.FindControl("Lbl_Facturas");
        try
        {
            if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            {
                Contador = (int)(Session["Contador"]);
                Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
                Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
                                + (Lbl_facturas.Text + ("\',\'"
                                + (Img.ClientID + "\')")))));
                Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
                Proveedor_ID = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["PROVEEDOR_ID"].ToString());
                Proveedores_Negocios.P_Proveedor_ID = Proveedor_ID;
                if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
                {
                    Proveedores_Negocios.P_Estatus = "PENDIENTES";
                }
                Proveedores_Negocios.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                Proveedores_Negocios.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                Ds_Consulta =Proveedores_Negocios.Consulta_Proveedores_Datos();
                Gv_Detalles = (GridView)e.Row.Cells[3].FindControl("Grid_Facturas");
                if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
                {
                    Ds_Consulta.DefaultView.RowFilter = " FECHA_PAGO IS NULL";
                    Gv_Detalles.DataSource = Ds_Consulta.DefaultView.ToTable();
                }
                else
                {
                    if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PAGADOS"))
                    {
                        Ds_Consulta.DefaultView.RowFilter = " FECHA_PAGO IS NOT NULL";
                        Gv_Detalles.DataSource = Ds_Consulta.DefaultView.ToTable();
                    }
                    else
                    {
                        Gv_Detalles.DataSource = Ds_Consulta;
                    }
                }
                Gv_Detalles.DataBind();
                Session["Contador"] = Contador + 1;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #endregion
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Reporte_Proveedores_Click
    ///DESCRIPCIÓN: Metodo para consultar el proveedor
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 17/mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Reporte_Proveedores_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Reporte())
            {
                Consulta_Proveedores(); //Consulta el libro de mayor de la cuenta seleccionada
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Proveedores
    /// DESCRIPCION : Consulta las cuentas por pagar de los proveedores
    ///               por el usuario desde el primer mes hasta el mes seleccionado
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 08-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Proveedores()
    {
        String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
        String Nombre_Archivo = "RPT_PROVEEDORES" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
        DataTable Dt_Proveedor = new DataTable();       //Va a conter los valores de la consulta realizada
        Ds_Rpt_Con_Proveedores_Cuentas_Por_Pagar Ds_Proveedor_Facturas = new Ds_Rpt_Con_Proveedores_Cuentas_Por_Pagar();
        Cls_Rpt_Con_Proveedores_Negocio Rs_Consulta = new Cls_Rpt_Con_Proveedores_Negocio(); //Conexion hacia la capa de negocios

        try
        {
            Rs_Consulta.P_Proveedor_ID = Cmb_Proveedor_Solicitud_Pago.SelectedValue;
            if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
            {
                Rs_Consulta.P_Estatus = "PENDIENTES";
            }
            Rs_Consulta.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
            Rs_Consulta.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
            Dt_Proveedor = Rs_Consulta.Consulta_Proveedores_Datos(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario

            if (Dt_Proveedor.Rows.Count > 0)
            {
                Dt_Proveedor.TableName = "Dt_Proveedores";
                Ds_Proveedor_Facturas.Clear();
                Ds_Proveedor_Facturas.Tables.Clear();
                if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PENDIENTES"))
                {
                    Dt_Proveedor.DefaultView.RowFilter = " FECHA_PAGO IS NULL";
                    Ds_Proveedor_Facturas.Tables.Add(Dt_Proveedor.DefaultView.ToTable().Copy());
                }
                else
                {
                    if (Cmb_Estatus.SelectedItem.Text.ToString().Equals("PAGADOS"))
                    {
                        Dt_Proveedor.DefaultView.RowFilter = " FECHA_PAGO IS NOT NULL";
                        Ds_Proveedor_Facturas.Tables.Add(Dt_Proveedor.DefaultView.ToTable().Copy());
                    }
                    else
                    {
                        Ds_Proveedor_Facturas.Tables.Add(Dt_Proveedor.DefaultView.ToTable().Copy());
                    }
                }
            }
            ReportDocument Reporte = new ReportDocument();
            Reporte.Load(Ruta_Archivo + "Rpt_Con_Proveedores_Cuentas_Por_Pagar.rpt");
            Reporte.SetDataSource(Ds_Proveedor_Facturas);
            DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();
            Nombre_Archivo += ".pdf";
            Ruta_Archivo = @Server.MapPath("../../Reporte/");
            m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

            ExportOptions Opciones_Exportacion = new ExportOptions();
            Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
            Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
            Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
            Reporte.Export(Opciones_Exportacion);

            Mostrar_Reporte(Nombre_Archivo);
        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Proveedor " + ex.Message.ToString(), ex);
        }
    }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Excel_Click
    /// DESCRIPCION : Consulta las cuentas por pagar de los proveedores
    ///               por el usuario desde el primer mes hasta el mes seleccionado
    /// PARAMETROS  : 
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 08-Mayo-2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Excel_Click(object sender, ImageClickEventArgs e)
    {
        System.Data.DataTable Dt_Puestos = null;
        System.Data.DataSet Ds_Puestos = null;
        Mensaje_Error();
        try
        {
            if (Validar_Datos())
            {
                Ds_Puestos = new System.Data.DataSet();
                //Dt_Puestos = Consulta_Proveedores();
                if (Dt_Puestos.Rows.Count > 0)
                {
                    Dt_Puestos.TableName = "Proveedores";
                    Ds_Puestos.Tables.Add(Dt_Puestos.Copy());

                    Exportar_Reporte(Ds_Puestos, "Rpt_Con_Proveedor_Pagos.rpt", "Reporte_Proveedores" + Session.SessionID, "xls", ExportFormatType.Excel);
                }
                else
                {
                    Mensaje_Error("El proveedor no tiene datos para mostrar");
                }
            }
            else
            {
                Mensaje_Error("La Fecha Inicial no debe ser Mayor que la Fecha Final");
            }
        }
        catch (Exception Ex)
        {
            Mensaje_Error(Ex.ToString());
        }
    }
}
