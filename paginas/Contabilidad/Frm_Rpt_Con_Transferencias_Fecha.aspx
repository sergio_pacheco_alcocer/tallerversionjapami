<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Transferencias_Fecha.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Transferencias_Fecha" Title="Reporte Transferencias" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server" >
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Cheques_Realizados" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Cuentas_Afectables"  style="width: 97%; height: 700px;">    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte transferencias</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Reporte_Cheques" runat="server" ToolTip="Reporte" 
                                                CssClass="Img_Button" TabIndex="1" Visible="false"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                onclick="Btn_Reporte_Cheques_Emitidos_Click"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%;text-align:left;">Mostrar por:</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:RadioButtonList ID="Rdb_Tipo" runat="server" RepeatDirection="Horizontal" >
                                        <asp:ListItem Value="Bancos">Bancos</asp:ListItem>
                                        <asp:ListItem Value="Tipo_Pago">Tipo Pago</asp:ListItem>
                                        </asp:RadioButtonList>
                                    </td>
                                    <td style="width:20%;text-align:left;"></td>
                                    <td style="width:30%;text-align:left;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Orden</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Orden" runat="server" Width="90%" TabIndex="2"/>
                                    </td>
                                    <td style="width:20%;text-align:left;"></td>
                                    <td style="width:30%;text-align:left;">
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="75%" TabIndex="6" MaxLength="11" Height="18px"/>
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/A�o" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de P�liza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                        <td align="left">
                                            <asp:Button ID="Btn_Consultar" runat="server" Text="Consultar"  OnClick="Btn_Consultar_Clic"/>
                                        </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td  runat="server" id="Tr_Grid_Bancos" colspan="4" style="display:none;">
                               <div id="Div_Encabezado_Banco" style="display:none;" runat="server">
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr >
                                            <td Font-Size="XX-Small" style="width:85%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Banco</td>
                                        </tr>
                                    </table>
                                </div>
                                <div id="Div_Encabezado_Tipo_Pago" style="display:none;" runat="server">
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr >
                                            <td Font-Size="XX-Small" style="width:85%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Tipo Pago</td>
                                        </tr>
                                    </table>
                                </div>
                               <div id="DIV_BANCO" runat="server" style="display:none;overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Cuentas_Movimientos" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" OnRowDataBound="Grid_Cuentas_RowDataBound"
                                        DataKeyNames="BANCO" GridLines="None" Width="99%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                        ImageUrl="~/paginas/imagenes/paginas/add_up.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                <ItemStyle HorizontalAlign="Left" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="BANCO" HeaderText="BANCO">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Movimientos" runat="server" 
                                                        Text='<%# Bind("BANCO") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='4'; align='left';&gt;" />
                                                    <asp:GridView ID="Grid_Movimientos" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99.9%">
                                                        <Columns>
                                                            <asp:BoundField DataField="ID_BANCO" HeaderText="ID Banco" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="Left" Width="8%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="8%" />
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="BENEFICIARIO"  HeaderStyle-Font-Size="X-Small" HeaderText="Beneficiario">
                                                                <HeaderStyle HorizontalAlign="left" Width="30%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="30%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="CUENTA"  HeaderText="Cuenta" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="left" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="10%" />
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="CLABE"  HeaderText="Clabe" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="center" Width="15%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="right"  Width="15%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IMPORTE"  HeaderStyle-Font-Size="X-Small" HeaderText="Importe"  DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="center" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="right" Width="10%" />
                                                            </asp:BoundField>
                                                             <asp:BoundField DataField="ORDEN" HeaderStyle-Font-Size="X-Small" HeaderText="Orden" >
                                                                <HeaderStyle HorizontalAlign="center" Width="10%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="center"  Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="CONCEPTO" HeaderStyle-Font-Size="X-Small" HeaderText="CONCEPTO">
                                                                <HeaderStyle HorizontalAlign="left" Width="17%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="left"  Width="17%" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                               <div  id="DIV_TIPO_PAGO" runat="server" style="display:none;overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Tipos_Pago" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" OnRowDataBound="Grid_Tipos_Pago_RowDataBound"
                                        DataKeyNames="DESCRIPCION" GridLines="None" Width="99%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="Img_Btn_Expandir_Tipos" runat="server"
                                                        ImageUrl="~/paginas/imagenes/paginas/add_up.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                <ItemStyle HorizontalAlign="Left" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="Tipo Pago">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Label ID="Lbl_Movimientos_Tipos" runat="server" 
                                                        Text='<%# Bind("DESCRIPCION") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio_Tipos" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:none;position:static'&gt;&lt;td colspan='4';left-padding:10px;&gt;" />
                                                    <asp:GridView ID="Grid_Por_Tipo_Pago" runat="server" AllowPaging="False" 
                                                        AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99.9%">
                                                        <Columns>
                                                            <asp:BoundField DataField="BENEFICIARIO"  HeaderStyle-Font-Size="X-Small" HeaderText="Beneficiario">
                                                                <HeaderStyle HorizontalAlign="left" Width="30%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="30%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="No_solicitud_pago"  HeaderText="Folio" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="left" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="IMPORTE"  HeaderStyle-Font-Size="X-Small" HeaderText="Importe"  DataFormatString="{0:C}">
                                                                <HeaderStyle HorizontalAlign="center" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="right" Width="10%" />
                                                            </asp:BoundField>
                                                             <asp:BoundField DataField="Cuenta" HeaderStyle-Font-Size="X-Small" HeaderText="Cuenta" >
                                                                <HeaderStyle HorizontalAlign="center" Width="10%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="center"  Width="10%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="ORDEN" HeaderStyle-Font-Size="X-Small" HeaderText="Orden">
                                                                <HeaderStyle HorizontalAlign="left" Width="17%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="left"  Width="17%" />
                                                            </asp:BoundField>
                                                            <asp:BoundField DataField="Concepto" HeaderStyle-Font-Size="X-Small" HeaderText="Concepto">
                                                                <HeaderStyle HorizontalAlign="left" Width="17%" />
                                                                <ItemStyle Font-Size="XX-Small"  HorizontalAlign="left"  Width="17%" />
                                                            </asp:BoundField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

