﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Historial_Reservas.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Historial_Reservas" Title=" Historial Reservas" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "block";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server" >
    <cc1:ToolkitScriptManager ID="ScriptManager_Areas" runat="server" EnableScriptLocalization="true" EnableScriptGlobalization="true"></cc1:ToolkitScriptManager>
    <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Cheques_Realizados" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                    <%--<div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>--%>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Cuentas_Afectables" style="background-color:#ffffff; width:100%; height:100%;">    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Historial Reservas</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td>                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" />
                                            <asp:ImageButton ID="Btn_Reporte_Cheques" runat="server" ToolTip="Reporte" 
                                                CssClass="Img_Button" TabIndex="1" Visible="false"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" />
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" />
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td>
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%;text-align:left;">Dependencia</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Dependencia_ID" runat="server" Width="90%" TabIndex="1" AutoPostBack="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Financiamiento</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Financiamiento_Des" runat="server" Width="90%" TabIndex="1" AutoPostBack="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Proyectos Programas</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Proyectos_Programas_Des" runat="server" Width="90%" TabIndex="1" AutoPostBack="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Partida Generica</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Partida_Generica_Des" runat="server" Width="90%" 
                                            TabIndex="1" 
                                            onselectedindexchanged="Cmb_Partida_Generica_Des_SelectedIndexChanged" AutoPostBack="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Partidas Especificas</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Partidas_Especificas_Des" runat="server" Width="90%" TabIndex="1"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%;text-align:left;">Capitulo</td>
                                    <td style="width:30%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Capitulo_Des" runat="server" Width="90%" TabIndex="1" AutoPostBack="true"/>
                                    </td>
                                </tr>
                                <tr>
                                    <td style="width:20%">
                                        <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="*Fecha Inicio"></asp:Label>
                                    </td>
                                    <td style="width:30%">
                                        <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Inicio" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese Fecha"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        
                                        </td>
                                        <td style="width:20%">
                                            <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="*Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width:30%">
                                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="18px" />
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                         <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Final" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Final" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Final"
                                            ControlExtender="Mee_Txt_Fecha_Final" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Final Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese Fecha"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                    </td>
                                </tr>
                            </table>
                            
                            <table width="100%" class="estilo_fuente">
                            
                            <asp:GridView ID="Grid_Historial_Reservas" runat="server" AllowSorting="true" 
                                            AutoGenerateColumns="false" CssClass="GridView_1" 
                                            EmptyDataText="No se encuentro ningun registro" GridLines="None" 
                                            
                                            OnSorting="Grid_Historial_Reservas_Sorting" style="white-space:normal" 
                                            Width="99%">
                                            <Columns>
                                                <asp:BoundField DataField="No_Reserva" HeaderText="No_Reserva" 
                                                    SortExpression="No_Reserva">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Estatus" HeaderText="Estatus" 
                                                    SortExpression="Estatus">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Concepto" HeaderText="Concepto" 
                                                    SortExpression="Concepto">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Fecha" HeaderText="Fecha" SortExpression="Fecha" DataFormatString="{0:dd/MMM/yyyy}">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Dependencia" 
                                                    SortExpression="Nombre">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Nombre" HeaderText="Proyecto_Programa" 
                                                    SortExpression="Proyecto_Programa_Id">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Partida_Id" HeaderText="Partida" 
                                                    SortExpression="Partida_Id">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Importe_Inicial" HeaderText="Importe_Inicial" 
                                                    SortExpression="Importe_Inicial" DataFormatString="{0:c}">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                                <asp:BoundField DataField="Saldo" HeaderText="Saldo" SortExpression="Saldo" DataFormatString="{0:c}">
                                                    <HeaderStyle HorizontalAlign="Center" Width="11%" />
                                                    <ItemStyle HorizontalAlign="Center" Width="11%" />
                                                </asp:BoundField>
                                            </Columns>
                                            <AlternatingRowStyle CssClass="GridAltItem" />
                                            <FooterStyle CssClass="GridHeader" />
                                            <HeaderStyle CssClass="GridHeader_Nested" />
                                            <PagerStyle CssClass="GridHeader" />
                                            <RowStyle CssClass="GridItem" />
                                            <SelectedRowStyle CssClass="GridSelected" />
                                        </asp:GridView>
                            </table>
                        </td>
                    </tr>
                    
               </table>
               <table width="97%">
               <tr>
                    <td align = "center" style="width:100%;">
                    <div style="overflow:auto;height:320px;width:99%;vertical-align:top; width:100%;" >
                    
                   </div>
                   </td>
                   </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>