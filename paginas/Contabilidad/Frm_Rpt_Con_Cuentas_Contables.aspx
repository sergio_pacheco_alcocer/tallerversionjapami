﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Cuentas_Contables.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Cuentas_Contables" Title="Reporte Cuentas Contables" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
    <script type="text/javascript" language="javascript">
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server" >
    <cc1:ToolkitScriptManager ID="ScriptManager_Parametros_Contabilidad" runat="server"></cc1:ToolkitScriptManager>
   <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>        
            <asp:UpdateProgress ID="Uprg_Cheques_Realizados" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
               <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Reporte_Cuentas_Afectables" style="background-color:#ffffff; width:100%; height:100%;">    
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Reporte Cuentas Contables</td>
                    </tr>
                    <tr>
                        <td>&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                </table>
                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td colspan="2">                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;">
                                            <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                CssClass="Img_Button" TabIndex="1"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                onclick="Btn_Nuevo_Click" />
                                            <asp:ImageButton ID="Btn_Reporte_Cheques" runat="server" ToolTip="Reporte" 
                                                CssClass="Img_Button" TabIndex="1" Visible="false"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_pdf.png" 
                                                onclick="Btn_Reporte_Cuentas_Contables_Click"/>
                                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                CssClass="Img_Button" TabIndex="2"
                                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                onclick="Btn_Salir_Click"/>
                                        </td>
                                      <td align="right" style="width:41%;">&nbsp;</td>       
                                    </tr>         
                                </table>                      
                            </div>
                        </td>
                    </tr>
                </table>   
                <table width="99%" class="estilo_fuente">
                    <tr>
                        <td colspan="4">
                            <table width="100%" class="estilo_fuente">
                                <tr>
                                    <td style="width:20%;text-align:left;">Genero</td>
                                    <td style="width:50%;text-align:left;">
                                        <asp:DropDownList ID="Cmb_Generos" runat="server" Width="90%" TabIndex="2"/>
                                    </td>
                                    <td style="width:10%;text-align:left;"></td>
                                    <td style="width:20%;text-align:left;">
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                     <tr>
                        <td  runat="server" id="Tr_Grid_Cuentas_Contables" colspan="4" style="display:none;">
                               <div>
                                    <table width="100%"  border="0" cellspacing="0">
                                        <tr >
                                            <td Font-Size="XX-Small" style="width:85%;background-image: url(../imagenes/gridview/HeaderChrome.jpg);background-position:center;background-repeat:repeat-x;background-color:#1d1d1d;border-bottom: #1d1d1d 1px solid;color:White;" align="left">Cuenta</td>
                                        </tr>
                                    </table>
                                </div>
                                <div style="overflow:auto;height:600px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                    <asp:GridView ID="Grid_Cuentas_Contables" runat="server" AllowPaging="False"  ShowHeader="false"
                                        AutoGenerateColumns="False" CssClass="GridView_1" OnRowDataBound="Grid_Cuentas_RowDataBound"
                                        DataKeyNames="CUENTA" GridLines="None" Width="99%">
                                        <Columns>
                                            <asp:TemplateField>
                                                <ItemTemplate>
                                                    <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                        ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                </ItemTemplate>
                                                <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                <ItemStyle HorizontalAlign="Left" Width="2%" />
                                            </asp:TemplateField>
                                            <asp:BoundField DataField="CUENTA" HeaderText="CUENTA">
                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                <ItemStyle HorizontalAlign="Left" Width="10%" />
                                            </asp:BoundField>
                                            <asp:BoundField DataField="DESCRIPCION" HeaderText="DESCRIPCION">
                                                <HeaderStyle HorizontalAlign="Left" Width="80%" />
                                                <ItemStyle HorizontalAlign="Left" Width="80%" />
                                            </asp:BoundField>
                                            <asp:TemplateField>
                                                <ItemTemplate> 
                                                    <asp:Label ID="Lbl_Movimientos" runat="server" 
                                                        Text='<%# Bind("CUENTA") %>' Visible="false"></asp:Label>
                                                    <asp:Literal ID="Ltr_Inicio" runat="server" 
                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';left-padding:10px;&gt;" />
                                                    <asp:GridView ID="Grid_Cuentas_Nivel_2" runat="server" AllowPaging="False"  DataKeyNames="CUENTA" OnRowDataBound="Grid_Cuentas_2_RowDataBound"
                                                        AutoGenerateColumns="False" CssClass="GridView_1" ShowHeader="false" GridLines="None" Width="99.9%">
                                                        <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Image ID="Img_Btn_Expandir_2" runat="server"
                                                                    ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="center" Width="5%" />
                                                            <ItemStyle HorizontalAlign="center" Width="5%" />
                                                        </asp:TemplateField>
                                                            <asp:BoundField DataField="CUENTA" HeaderText="CUENTA" HeaderStyle-Font-Size="X-Small">
                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="20%" />
                                                            </asp:BoundField>                                                            
                                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small" HeaderText="DESCRIPCION">
                                                                <HeaderStyle HorizontalAlign="left" Width="80%" />
                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="80%" />
                                                            </asp:BoundField>
                                                            <asp:TemplateField>
                                                                <ItemTemplate>
                                                                    <asp:Label ID="Lbl_Cuentas" runat="server" 
                                                                        Text='<%# Bind("CUENTA") %>' Visible="false"></asp:Label>
                                                                    <asp:Literal ID="Ltr_Inicio_2" runat="server" 
                                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';left-padding:10px;&gt;" />
                                                                    <asp:GridView ID="Grid_Cuentas_Nivel_3" runat="server" AllowPaging="False" DataKeyNames="CUENTA" OnRowDataBound="Grid_Cuentas_3_RowDataBound"
                                                                        AutoGenerateColumns="False" CssClass="GridView_1"  ShowHeader="false" GridLines="None" Width="99.9%">
                                                                        <Columns>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Image ID="Img_Btn_Expandir_3" runat="server"
                                                                                        ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign="Center" Width="10%" />
                                                                                <ItemStyle HorizontalAlign="Center" Width="10%" />
                                                                            </asp:TemplateField>
                                                                            <asp:BoundField DataField="CUENTA" HeaderText="CUENTA" HeaderStyle-Font-Size="X-Small">
                                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="10%" />
                                                                            </asp:BoundField>                                                            
                                                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small" HeaderText="DESCRIPCION">
                                                                                <HeaderStyle HorizontalAlign="left" Width="70%" />
                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="70%" />
                                                                            </asp:BoundField>
                                                                            <asp:TemplateField>
                                                                                <ItemTemplate>
                                                                                    <asp:Label ID="Lbl_Cuentas_2" runat="server" 
                                                                                        Text='<%# Bind("CUENTA") %>' Visible="false"></asp:Label>
                                                                                    <asp:Literal ID="Ltr_Inicio_3" runat="server" 
                                                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';left-padding:20px;&gt;" />
                                                                                    <asp:GridView ID="Grid_Cuentas_Nivel_4" runat="server" AllowPaging="False" DataKeyNames="CUENTA" OnRowDataBound="Grid_Cuentas_4_RowDataBound"
                                                                                        AutoGenerateColumns="False" CssClass="GridView_1"  ShowHeader="false" GridLines="None" Width="99.9%">
                                                                                        <Columns>
                                                                                             <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Image ID="Img_Btn_Expandir_4" runat="server"
                                                                                                        ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                                                                </ItemTemplate>
                                                                                                <HeaderStyle HorizontalAlign="Center" Width="15%" />
                                                                                                <ItemStyle HorizontalAlign="Center" Width="15%" />
                                                                                            </asp:TemplateField>
                                                                                            <asp:BoundField DataField="CUENTA" HeaderText="CUENTA" HeaderStyle-Font-Size="X-Small">
                                                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="10%" />
                                                                                            </asp:BoundField>                                                            
                                                                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small" HeaderText="DESCRIPCION">
                                                                                                <HeaderStyle HorizontalAlign="left" Width="70%" />
                                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="70%" />
                                                                                            </asp:BoundField>
                                                                                            <asp:TemplateField>
                                                                                                <ItemTemplate>
                                                                                                    <asp:Label ID="Lbl_Cuentas_3" runat="server" 
                                                                                                        Text='<%# Bind("CUENTA") %>' Visible="false"></asp:Label>
                                                                                                    <asp:Literal ID="Ltr_Inicio_4" runat="server" 
                                                                                                        Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';left-padding:30px;&gt;" />
                                                                                                    <asp:GridView ID="Grid_Cuentas_Nivel_5" runat="server" AllowPaging="False" DataKeyNames="CUENTA" OnRowDataBound="Grid_Cuentas_5_RowDataBound"
                                                                                                        AutoGenerateColumns="False" CssClass="GridView_1" ShowHeader="false" GridLines="None" Width="99.9%">
                                                                                                        <Columns>
                                                                                                            <asp:TemplateField>
                                                                                                                <ItemTemplate>
                                                                                                                    <asp:Image ID="Img_Btn_Expandir_5" runat="server"
                                                                                                                        ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                                                                                </ItemTemplate>
                                                                                                                <HeaderStyle HorizontalAlign="Center" Width="20%" />
                                                                                                                <ItemStyle HorizontalAlign="Center" Width="20%" />
                                                                                                            </asp:TemplateField>
                                                                                                            <asp:BoundField DataField="CUENTA" HeaderText="CUENTA" HeaderStyle-Font-Size="X-Small">
                                                                                                                <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="10%" />
                                                                                                            </asp:BoundField>                                                            
                                                                                                            <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small" HeaderText="DESCRIPCION">
                                                                                                                <HeaderStyle HorizontalAlign="left" Width="65%" />
                                                                                                                <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="65%" />
                                                                                                            </asp:BoundField>
                                                                                                            <asp:TemplateField>
                                                                                                            <ItemTemplate>
                                                                                                                <asp:Label ID="Lbl_Cuentas_4" runat="server" 
                                                                                                                    Text='<%# Bind("CUENTA") %>' Visible="false"></asp:Label>
                                                                                                                <asp:Literal ID="Ltr_Inicio_5" runat="server" 
                                                                                                                    Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' &gt;&lt;td colspan='4';left-padding:30px;&gt;" />
                                                                                                                <asp:GridView ID="Grid_Cuentas_Nivel_6" runat="server" ShowHeader="false" AllowPaging="False" 
                                                                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="99.9%">
                                                                                                                    <Columns>
                                                                                                                        <asp:TemplateField>
                                                                                                                        <ItemTemplate>
                                                                                                                        </ItemTemplate>
                                                                                                                            <HeaderStyle HorizontalAlign="Center" Width="25%" />
                                                                                                                            <ItemStyle HorizontalAlign="Center" Width="25%" />
                                                                                                                        </asp:TemplateField>
                                                                                                                        <asp:BoundField DataField="CUENTA" HeaderText="CUENTA" HeaderStyle-Font-Size="X-Small">
                                                                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" />
                                                                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="Left" Width="10%" />
                                                                                                                        </asp:BoundField>                                                            
                                                                                                                        <asp:BoundField DataField="DESCRIPCION"  HeaderStyle-Font-Size="X-Small" HeaderText="DESCRIPCION">
                                                                                                                            <HeaderStyle HorizontalAlign="left" Width="60%" />
                                                                                                                            <ItemStyle Font-Size="X-Small"  HorizontalAlign="left"  Width="60%" />
                                                                                                                        </asp:BoundField>
                                                                                                                    </Columns>
                                                                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                                                    <FooterStyle CssClass="GridPager" />
                                                                                                                    <HeaderStyle CssClass="GridHeader_Nested" />
                                                                                                                    <PagerStyle CssClass="GridPager" />
                                                                                                                    <RowStyle CssClass="GridItem" />
                                                                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                                                                </asp:GridView>
                                                                                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                                                            </ItemTemplate>
                                                                                                        </asp:TemplateField>
                                                                                                        </Columns>
                                                                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                                        <FooterStyle CssClass="GridPager" />
                                                                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                                                                        <PagerStyle CssClass="GridPager" />
                                                                                                        <RowStyle CssClass="GridItem" />
                                                                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                                                                    </asp:GridView>
                                                                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                                                </ItemTemplate>
                                                                                            </asp:TemplateField>
                                                                                        </Columns>
                                                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                        <FooterStyle CssClass="GridPager" />
                                                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                                                        <PagerStyle CssClass="GridPager" />
                                                                                        <RowStyle CssClass="GridItem" />
                                                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                                                    </asp:GridView>
                                                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                                </ItemTemplate>
                                                                            </asp:TemplateField>
                                                                        </Columns>
                                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                                        <FooterStyle CssClass="GridPager" />
                                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                                        <PagerStyle CssClass="GridPager" />
                                                                        <RowStyle CssClass="GridItem" />
                                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                                    </asp:GridView>
                                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                </ItemTemplate>
                                                            </asp:TemplateField>
                                                        </Columns>
                                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                                        <FooterStyle CssClass="GridPager" />
                                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                                        <PagerStyle CssClass="GridPager" />
                                                        <RowStyle CssClass="GridItem" />
                                                        <SelectedRowStyle CssClass="GridSelected" />
                                                    </asp:GridView>
                                                    <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                </ItemTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <AlternatingRowStyle CssClass="GridAltItem" />
                                        <FooterStyle CssClass="GridPager" />
                                        <HeaderStyle CssClass="GridHeader_Nested" />
                                        <PagerStyle CssClass="GridPager" />
                                        <RowStyle CssClass="GridItem" />
                                        <SelectedRowStyle CssClass="GridSelected" />
                                    </asp:GridView>
                                </div>
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

