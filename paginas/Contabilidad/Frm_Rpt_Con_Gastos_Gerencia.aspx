﻿<%@ Page Title="" Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Rpt_Con_Gastos_Gerencia.aspx.cs" Inherits="paginas_Contabilidad_Frm_Rpt_Con_Gastos_Gerencia" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Reportes" runat="server" EnableScriptGlobalization = "true" EnableScriptLocalization = "True" AsyncPostBackTimeout="720000"  />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" UpdateMode="Always">
        <ContentTemplate>
           <asp:UpdateProgress ID="Upgrade" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                  <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress">
                        <img alt="" src="../Imagenes/paginas/Updating.gif" />
                    </div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            
            <div id="Div_Contenido" style="width: 97%; height: 700px;">
                <table width="97%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr>
                        <td colspan="6" class="label_titulo">Reporte Gastos por Gerencia</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <!--Bloque del mensaje de error-->
                            <div id="Div_Contenedor_Msj_Error" style="width:95%;font-size:9px;" runat="server" visible="false">
                                <table style="width:100%;">
                                    <tr>
                                        <td align="left" style="font-size:12px;color:Red;font-family:Tahoma;text-align:left;">
                                            <asp:Image ID="Img_Warning" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                            Width="24px" Height="24px" />
                                        </td>            
                                        <td style="font-size:9px;width:90%;text-align:left;" valign="top">
                                            <asp:Label ID="Lbl_Informacion" runat="server" ForeColor="Red" />
                                        </td>
                                    </tr> 
                                </table>                   
                            </div>
                        </td>
                    </tr>
                    <tr class="barra_busqueda">
                        <td colspan="6" style="width:20%;">
                            <!--Bloque de la busqueda-->
                            <%--<asp:ImageButton ID="Btn_Consultar" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_consultar.png" Width="24px" 
                                CssClass="Img_Button" AlternateText="CONSULTAR" ToolTip="Consultar" />--%>
                            <asp:ImageButton ID="Btn_Imprimir_Excel" runat="server" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_rep_excel.png" Width="24px" CssClass="Img_Button" 
                                AlternateText="Imprimir Excel" ToolTip="Exportar Excel" 
                                onclick="Btn_Imprimir_Excel_Click" />  
                            <asp:ImageButton ID="Btn_Salir" runat="server" CssClass="Img_Button" 
                                ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" ToolTip="Salir" 
                                AlternateText="Salir" onclick="Btn_Salir_Click" />
                        </td>                                 
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td align="left">Grupo Gerencia</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Grupo_Dependencias" runat="server" Width="100%" AutoPostBack="true"  OnSelectedIndexChanged="Cmb_Grupo_Dependencia_OnseledIndexChanged"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Gerencia</td>
                        <td align="left" colspan="5"><asp:DropDownList ID="Cmb_Dependencias" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">A&ntilde;o</td>
                        <td align="left" style=" width:20%;"><asp:DropDownList ID="Cmb_Anios" runat="server" Width="100%" AutoPostBack="true" OnSelectedIndexChanged="Cmb_Anio_OnSelectedIndexChanged"></asp:DropDownList></td>
                        <td align="left">Mes Inicial</td>
                        <td align="left"style=" width:20%;"><asp:DropDownList ID="Cmb_Mes_Inicial" runat="server" Width="100%"></asp:DropDownList></td>
                        <td align="left">Mes Final</td>
                        <td align="left"style=" width:20%;"><asp:DropDownList ID="Cmb_Mes_Final" runat="server" Width="100%"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td colspan="6">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="6">
                            <div id="Div_Gastos_Gerencia" runat="server">
                                <asp:GridView ID="Grid_Gastos_Gerencia" runat="server" style="white-space:normal;" 
                                    AutoGenerateColumns="False" CellPadding="1" CssClass="GridView_1" GridLines="None" PageSize="5" Width="100%">
                                    <Columns>
                                        <asp:BoundField DataField="CUENTA" HeaderText="Cuenta" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center" />
                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left" />
                                        <asp:BoundField DataField="ENERO" HeaderText="Enero" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="FEBRERO" HeaderText="Febrero" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="MARZO" HeaderText="Marzo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="ABRIL" HeaderText="Abril" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="MAYO" HeaderText="Mayo" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="JUNIO" HeaderText="Junio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="JULIO" HeaderText="Julio" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="AGOSTO" HeaderText="Agosto" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="SEPTIEMBRE" HeaderText="Septiembre" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="OCTUBRE" HeaderText="Octubre" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="NOVIEMBRE" HeaderText="Noviembre" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                        <asp:BoundField DataField="DICIEMBRE" HeaderText="Diciembre" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Right" DataFormatString="{0:n}" />
                                    </Columns>
                                </asp:GridView>
                            </div>
                        </td>
                    </tr>
                </table>
            </div>        
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>

