﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Data.Odbc;
using System.Data.OleDb;
using System.Linq;
using System.Windows.Forms;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using AjaxControlToolkit;
using System.IO;
using System.Data.SqlClient;
using System.Globalization;
using System.Text.RegularExpressions;
using JAPAMI.Cierre_Anual.Negocio;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cierre_Mensual.Negocio;
using System.Text;
using SharpContent.ApplicationBlocks.Data;

public partial class paginas_Contabilidad_Frm_Ope_Con_Cierre_Anual : System.Web.UI.Page
{
    #region (Page Load)
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

        try
        {
            if (!IsPostBack)
            {
                Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                ViewState["SortDirection"] = "ASC";
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    /// *****************************************************************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS: No Áplica.
    /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
    /// FECHA CREÓ: 23/Mayo/2011 10:43 a.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *****************************************************************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// CREO        : Juan Alberto Hernandez Negrete
    /// FECHA_CREO  : 29/Noviembre/2010
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion

    #region (Metodos Generales)
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Inicializa_Controles
    /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
    ///               diferentes operaciones
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 24/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Inicializa_Controles()
    {
        try
        {
            Limpia_Controles();             //Limpia los controles de la forma
            Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
            Llenar_Cmb_Cuenta_Inicial();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message.ToString());
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Limpiar_Controles
    /// DESCRIPCION : Limpia los controles que se encuentran en la forma
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 24/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private void Limpia_Controles()
    {
        try
        {
            Cmb_Cuenta_Final.Items.Clear();
            Cmb_Cuenta_Inicial.Items.Clear();
            Txt_Anio.Text = "";
            Txt_Descripcion.Text = "";
            Txt_Nueva_Cuenta.Text = "";
        }
        catch (Exception ex)
        {
            throw new Exception("Limpiar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Habilitar_Controles
    /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
    ///               para a siguiente operación
    /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
    ///                          si es una alta, modificacion
    /// CREO        : Yazmin A Delgado Gómez
    /// FECHA_CREO  : 11/Julio/2011
    /// MODIFICO          : Salvador L. Rea Ayala
    /// FECHA_MODIFICO    : 10/Octubre/2011
    /// CAUSA_MODIFICACION: Se agregaron los nuevos controles.
    ///*******************************************************************************
    private void Habilitar_Controles(String Operacion)
    {
        Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
        try
        {
            Habilitado = false;
            switch (Operacion)
            {
                case "Inicial":
                    Habilitado = true;
                    Btn_Salir.ToolTip = "Salir";
                    Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                    Configuracion_Acceso("Frm_Ope_Con_Cierre_Anual.aspx");
                    break;
            }

            Cmb_Cuenta_Inicial.Enabled = Habilitado;
            Cmb_Cuenta_Final.Enabled = Habilitado;
            Txt_Nueva_Cuenta.Enabled = Habilitado;
            Txt_Anio.Text = "";
            Txt_Descripcion.Text = "";

            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
        }
        catch (Exception ex)
        {
            throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Validar_Datos_Cierre_Anual
    /// DESCRIPCION : Valida que los datos necesarios se encuentren presentes
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 25/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private Boolean Validar_Datos_Cierre_Anual()
    {
        Boolean Datos_Validos = true;
        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";

        if (Cmb_Cuenta_Inicial.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Cuenta Inicial. <br>";
            Datos_Validos = false;
        }
        if (Cmb_Cuenta_Final.SelectedIndex == 0)
        {
            Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; + Cuenta Final. <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Nueva_Cuenta.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Cuenta Cierre. <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Anio.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Año. <br>";
            Datos_Validos = false;
        }
        if (string.IsNullOrEmpty(Txt_Descripcion.Text))
        {
            Lbl_Mensaje_Error.Text += "+ Descripcion. <br>";
            Datos_Validos = false;
        }

        return Datos_Validos;
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Obtener_Debe_Haber_Cuentas
    /// DESCRIPCION : Obtiene el total del debe y el haber del rango de cuentas seleccionado
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 26/Octubre/2011
    /// MODIFICO          : 
    /// FECHA_MODIFICO    : 
    /// CAUSA_MODIFICACION: 
    ///*******************************************************************************
    private Decimal Obtener_Debe_Haber_Cuentas(DataRow Registro)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;

            Decimal Saldo = 0;   //Almacena el total del debe y el haber de las cuentas seleccionadas.
            Cls_Ope_Con_Polizas_Negocio Rs_Polizas = new Cls_Ope_Con_Polizas_Negocio(); //Variable de conexion con la capa de negocios.
            DataTable Dt_Saldo = null;    //Almacena los datos de la consulta.
            String Fecha = "";
            Rs_Polizas.P_Cuenta_Contable_ID = Registro["CUENTA_CONTABLE_ID"].ToString();
            Rs_Polizas.P_Fecha_Inicial = "31/12/" + (Convert.ToDouble(Txt_Anio.Text)).ToString();
            Fecha = Rs_Polizas.Consulta_Fecha_Poliza_Anual();
            if (Fecha != "")
            {
                Rs_Polizas.P_Fecha_Inicial = Fecha;
                Dt_Saldo = Rs_Polizas.Consulta_Saldo_Consecutivo_Anual();
                Saldo = Convert.ToDecimal(Dt_Saldo.Rows[0][Ope_Con_Polizas_Detalles.Campo_Saldo].ToString());
            }
            else
            {
                Saldo = 0;
            }
            return Saldo;
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
            return 0;
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Consulta_Cuentas_Contables_Avanzada
    /// DESCRIPCION : Ejecuta la busqueda de Cuenta
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 22/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Consulta_Cuentas_Contables_Avanzada()
    {
        Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Consulta_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio(); //Variable de conexión hacia la capa de Negocios
        DataTable Dt_Cuentas; //Variable que obtendra los datos de la consulta 
        Double bandera = 0;
        DataRow Renglon;//Es el renglon que se agregara al datatable temporal
        DataTable Dt_Temporal = new DataTable();//contendra las fuentes que esten activas aun
        Lbl_Error_Busqueda_Cuenta_Contable.Visible = false;
        Img_Error_Busqueda_Cienta_Contable.Visible = false;
        try
        {
            if (!string.IsNullOrEmpty(Txt_Busqueda_Cuenta_Contable.Text))
            {
                Rs_Consulta_Cuentas.P_Descripcion = Txt_Busqueda_Cuenta_Contable.Text.ToString().ToUpper();
                bandera = 1;
            }
            Rs_Consulta_Cuentas.P_Afectable = "SI";
            if (Txt_Busqueda_Cuenta_Contable.Text == "")
            {
                bandera = 0;
                Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br />Debes Ingresar una Descripcion<br />";
            }
            if (bandera == 1)
            {
                Dt_Cuentas = Rs_Consulta_Cuentas.Consulta_Cuentas_Contables();
                if (Dt_Cuentas.Rows.Count > 0)
                {
                    if (Dt_Cuentas.Rows.Count > 0)
                    {
                        foreach (DataRow Fila in Dt_Cuentas.Rows)
                        {
                            if (Dt_Temporal.Rows.Count <= 0)
                            {
                                //Agrega los campos que va a contener el DataTable
                                Dt_Temporal.Columns.Add("CUENTA_CONTABLE_ID", typeof(System.String));
                                Dt_Temporal.Columns.Add("CUENTA", typeof(System.String));
                                Dt_Temporal.Columns.Add("DESCRIPCION", typeof(System.String));
                            }
                            Renglon = Dt_Temporal.NewRow();
                            Renglon["CUENTA_CONTABLE_ID"] = Fila["CUENTA_CONTABLE_ID"].ToString();
                            Renglon["CUENTA"] = Fila["CUENTA"].ToString();
                            Renglon["DESCRIPCION"] = Fila["DESCRIPCION"].ToString();
                            Dt_Temporal.Rows.Add(Renglon); //Agrega el registro creado con todos sus valores a la tabla
                            Dt_Temporal.AcceptChanges();
                        }
                        // se llena el grid de cuentas encontradas
                        Grid_Cuentas_Contables.Columns[1].Visible = true;
                        Grid_Cuentas_Contables.DataSource = Dt_Temporal;   // Se iguala el DataTable con el Grid
                        Grid_Cuentas_Contables.DataBind();    // Se ligan los datos.
                        Grid_Cuentas_Contables.Visible = true;
                        Grid_Cuentas_Contables.Columns[1].Visible = false;
                    }
                }
                else
                {
                    Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br />No se encontraron Coincidencias con la busqueda<br />";
                    Lbl_Error_Busqueda_Cuenta_Contable.Visible = true;
                    Img_Error_Busqueda_Cienta_Contable.Visible = true;
                }

                Grid_Cuentas_Contables.SelectedIndex = -1;
                Mpe_Busqueda_Cuenta_Contable.Show();
            }
            else
            {
                if (bandera == 0) Lbl_Error_Busqueda_Cuenta_Contable.Text = "<br />Debes ingresar un filtro para poder realizar la busqueda<br />";
                Lbl_Error_Busqueda_Cuenta_Contable.Visible = true;
                Img_Error_Busqueda_Cienta_Contable.Visible = true;
                Mpe_Busqueda_Cuenta_Contable.Show();
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Consulta_Cuentas_Contables_Avanzada " + ex.Message.ToString(), ex);
        }
    }
    #endregion

    #region (Metodos)
    private void Llenar_Cmb_Cuenta_Inicial()
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            DataTable Dt_Tabla_Temporal = new DataTable();
            Cls_Ope_Con_Cierre_Anual_Negocio Rs_Cuentas_Contables = new Cls_Ope_Con_Cierre_Anual_Negocio();   //Variable de conexion con la capa de negocios.
            DataTable Dt_Cuentas_Contables = null;
            Rs_Cuentas_Contables.P_Nivel_ID="00001";
            Dt_Cuentas_Contables = Rs_Cuentas_Contables.Consulta_Datos_Cuentas_Contables_Cierre();
            if (Dt_Cuentas_Contables.Rows.Count > 0)
            {
                if (Dt_Tabla_Temporal.Rows.Count <= 0 && Dt_Tabla_Temporal.Columns.Count <= 0)
                {
                    Dt_Tabla_Temporal.Columns.Add("CUENTA", typeof(System.String));//typeof(System.String)
                    Dt_Tabla_Temporal.Columns.Add("DESCRIPCION", typeof(System.String));
                    foreach(DataRow Fila in Dt_Cuentas_Contables.Rows){
                        if (Fila["Cuenta"].ToString().Substring(0, 1) == "4" || Fila["Cuenta"].ToString().Substring(0, 1) == "5" || Fila["Cuenta"].ToString().Substring(0, 1) == "6")
                        {
                            DataRow Dr_Temp = Dt_Tabla_Temporal.NewRow(); 
                            Dr_Temp["Cuenta"] = Fila["Cuenta"].ToString();
                            Dr_Temp["Descripcion"] = Fila["Descripcion"].ToString();
                            Dt_Tabla_Temporal.Rows.Add(Dr_Temp);
                            Dt_Tabla_Temporal.AcceptChanges();
                        }
                    }
                }
                Cmb_Cuenta_Inicial.Items.Clear();
                Cmb_Cuenta_Inicial.DataSource = Dt_Tabla_Temporal;
                Cmb_Cuenta_Inicial.DataValueField = "CUENTA";
                Cmb_Cuenta_Inicial.DataTextField = "DESCRIPCION";
                Cmb_Cuenta_Inicial.DataBind();
                Cmb_Cuenta_Inicial.Items.Insert(0, "< SELECCIONE >");
                Cmb_Cuenta_Inicial.SelectedIndex = 0;
            }
            
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Llenar_Cmb_Cuenta_Inicial: " + ex.Message.ToString();
        }
    }

    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Cierre_Mensual
    /// DESCRIPCION : Comienza la operacion del Cierre Mensual
    /// PARAMETROS  : 
    /// CREO        : Salvador L. Rea Ayala
    /// FECHA_CREO  : 15/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private void Cierre_Mensual(int Anio)
    {
        Cls_Ope_Con_Cierre_Mensual_Negocio Rs_Cierre_Mensual = new Cls_Ope_Con_Cierre_Mensual_Negocio();    //Objeto de acceso a los metodos.
        DataTable Dt_Cierre_Mensual = null;     //Almacenara los resultados a calcular.
        DataTable Dt_Cuentas = null;            //Almacenara las cuentas que tuvieron movimientos en el mes.
        DataTable Dt_Cuentas_Cierre_Mensual = null; //Almacenara las cuentas existentes de un determinado cierre mensual.
        DataTable Dt_Movimientos = new DataTable();     //Almacenara los resultados de los movimientos de cada una de las cuentas.
        DataTable Dt_Saldo_Inicial = null;      //Almacenara el saldo final del cierre mensual anterior.                
        Decimal Debe = 0;
        Decimal Haber = 0;

        try
        {
            Dt_Movimientos.Columns.Add("Cuenta_Contable_ID", typeof(string));
            Dt_Movimientos.Columns.Add("Debe", typeof(Decimal));
            Dt_Movimientos.Columns.Add("Haber", typeof(Decimal));
            Dt_Movimientos.Columns.Add("Naturaleza", typeof(String));
            Rs_Cierre_Mensual.P_Mes_Anio = "13" + Anio.ToString().Trim().Substring(2, 2);
            Dt_Cuentas = Rs_Cierre_Mensual.Cuentas_Contables_Afectables();
            Dt_Cierre_Mensual = Rs_Cierre_Mensual.Cierre_Mensual();

            //llENA EL DATA TABLE CON EL TOTAL HABER, TOTAL DEBE DE LAS CUENTAS QUE SUFRIERON MOVIMIENTOS EN EL MES
            for (int Cont_Cuentas = 0; Cont_Cuentas < Dt_Cuentas.Rows.Count; Cont_Cuentas++)
            {
                for (int Cont_Cierre_Mensual = 0; Cont_Cierre_Mensual < Dt_Cierre_Mensual.Rows.Count; Cont_Cierre_Mensual++)
                {
                    if (Dt_Cuentas.Rows[Cont_Cuentas][0].ToString() == Dt_Cierre_Mensual.Rows[Cont_Cierre_Mensual][0].ToString())
                    {
                        Debe += Convert.ToDecimal(Dt_Cierre_Mensual.Rows[Cont_Cierre_Mensual][1].ToString());
                        Haber += Convert.ToDecimal(Dt_Cierre_Mensual.Rows[Cont_Cierre_Mensual][2].ToString());

                    }
                }
                Dt_Movimientos.Rows.Add(Dt_Cuentas.Rows[Cont_Cuentas]["Cuenta_Contable_ID"].ToString(), Debe, Haber, Dt_Cuentas.Rows[Cont_Cuentas]["Naturaleza"].ToString());
                Debe = Haber = 0;
            }
            Rs_Cierre_Mensual.P_Mes_Anio = "13" + Anio.ToString().Trim().Substring(2, 2);
            Dt_Cierre_Mensual = new DataTable();
            Dt_Cuentas_Cierre_Mensual = Rs_Cierre_Mensual.Consulta_Cierre_Mensual();
            if (Dt_Cuentas_Cierre_Mensual.Rows.Count > 0)
            {
                Rs_Cierre_Mensual.Limpiar_Cierre_Mensual();
            }
            for (int Cont_Cuentas = 0; Cont_Cuentas < Dt_Cuentas.Rows.Count; Cont_Cuentas++)
            {
                Rs_Cierre_Mensual.P_Cuenta_Contable_ID = Dt_Movimientos.Rows[Cont_Cuentas][0].ToString();
                Rs_Cierre_Mensual.P_Fecha_Inicio = String.Format("{0:dd/MM/yy}", new DateTime(Anio,12,31));
                Rs_Cierre_Mensual.P_Fecha_Final = String.Format("{0:dd/MM/yy}", DateTime.Now);
                Dt_Saldo_Inicial = Rs_Cierre_Mensual.Saldo_Inicial_Cierre_Mensual();
                if (Dt_Saldo_Inicial.Rows.Count == 0)
                    Rs_Cierre_Mensual.P_Saldo_Inicial = "0";
                else
                    Rs_Cierre_Mensual.P_Saldo_Inicial = Dt_Saldo_Inicial.Rows[0][0].ToString();

                Rs_Cierre_Mensual.P_Total_Debe = Dt_Movimientos.Rows[Cont_Cuentas][1].ToString();
                Rs_Cierre_Mensual.P_Total_Haber = Dt_Movimientos.Rows[Cont_Cuentas][2].ToString();
                if (Dt_Movimientos.Rows[Cont_Cuentas][3].ToString().ToUpper() == "DEUDOR")
                {
                    Rs_Cierre_Mensual.P_Saldo_Final = Convert.ToString((Convert.ToDecimal(Rs_Cierre_Mensual.P_Saldo_Inicial) + Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Debe) - Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Haber)));
                }
                else
                {
                    Rs_Cierre_Mensual.P_Saldo_Final = Convert.ToString((Convert.ToDecimal(Rs_Cierre_Mensual.P_Saldo_Inicial) + Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Haber) - Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Debe)));
                }
                Rs_Cierre_Mensual.P_Diferencia = (Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Debe) - Convert.ToDecimal(Rs_Cierre_Mensual.P_Total_Haber)).ToString();
                Rs_Cierre_Mensual.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
                Rs_Cierre_Mensual.Cierre_Mensual_Alta();
            }

        }
        catch (Exception ex)
        {
            throw new Exception("Cierre_Mensual" + ex.Message.ToString(), ex);
        }
    }

    // ****************************************************************************************
    //'NOMBRE DE LA FUNCION:    Valida_Cierre_Mensual
    //'DESCRIPCION :            Validar si el mes esta equilibrado
    //'PARAMETROS  :            Cmb_Mes_Contable: Cadena de texto con el mes contable
    //'CREO        :            Noe Mosqueda Valadez
    //'FECHA_CREO  :            30/Julio/2012 19:00
    //'MODIFICO          :
    //'FECHA_MODIFICO    :
    //'CAUSA_MODIFICACION:
    //'****************************************************************************************
    private String Valida_Cierre_Mensual(int Anio)
    {
        //Declaracion de variables
        String Resultado = String.Empty; //variable para el resultado
        DataTable Dt_Aux = new DataTable(); //tabla auxiliar para las consultas
        Cls_Ope_Con_Cierre_Mensual_Negocio Cierre_Mensual_Negocio = new Cls_Ope_Con_Cierre_Mensual_Negocio(); //Variable para la capa de negocios
        int Cont_Elementos = 0; //variable para el contador
        Double Debe = 0; //variable para el debe
        Double Haber = 0; //variable para el haber
        Double Diferencia = 0; //variable para la diferencia
        DateTime mes;

        try
        {
            //Consulta general para verificar si cuadra
            Cierre_Mensual_Negocio.P_Mes_Anio = "13" + Anio.ToString().Substring(2, 2);
            Dt_Aux = Cierre_Mensual_Negocio.Consulta_Total_Cierre_Mensual();

            //Verificar si la consulta arrojo resultados
            if (Dt_Aux.Rows.Count > 0)
            {
                //Hacer consulta a detalle para las polizas
                Dt_Aux = new DataTable();
                Dt_Aux = Cierre_Mensual_Negocio.Consulta_Totales_Poliza_Cierre_Mensual();

                //Ciclo para el barrido de la tabla
                for (Cont_Elementos = 0; Cont_Elementos < Dt_Aux.Rows.Count; Cont_Elementos++)
                {
                    //limpiar variables
                    Debe = 0;
                    Haber = 0;
                    Diferencia = 0;

                    //Asignar valores del debe y el haber
                    if (Dt_Aux.Rows[Cont_Elementos]["Total_Debe"] != DBNull.Value)
                    {
                        Debe = Convert.ToDouble(Dt_Aux.Rows[Cont_Elementos]["Total_Debe"]);
                    }

                    if (Dt_Aux.Rows[Cont_Elementos]["Total_Haber"] != DBNull.Value)
                    {
                        Haber = Convert.ToDouble(Dt_Aux.Rows[Cont_Elementos]["Total_Haber"]);
                    }

                    Diferencia = Math.Abs(Debe - Haber);

                    //Verificar si la diferencia es cero
                    if (Diferencia > 0)
                    {
                        Resultado = "La Poliza " + Dt_Aux.Rows[Cont_Elementos]["No_Poliza"].ToString().Trim();

                        //Verificar si tiene prefijo
                        if (Dt_Aux.Rows[Cont_Elementos]["Prefijo"] != DBNull.Value)
                        {
                            Resultado += " con el prefijo " + Dt_Aux.Rows[Cont_Elementos]["Prefijo"].ToString().Trim();
                        }

                        Resultado += " no esta cuadrada, favor de verificarla.";

                        //Salir del ciclo
                        break;
                    }
                }
            }

            //Entregar resultado
            return Resultado;
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }

    // ****************************************************************************************
    //NOMBRE DE LA FUNCION:    Cierre_Anual_Completo
    //DESCRIPCION :            Realizar el cierre anual completo en una transaccion
    //PARAMETROS  :            
    //CREO        :            Noe Mosqueda Valadez
    //FECHA_CREO  :            03/Mayo/2013 16:00
    //MODIFICO          :
    //FECHA_MODIFICO    :
    //CAUSA_MODIFICACION:
    //****************************************************************************************
    private void Cierre_Anual_Completo()
    {
        //Declaracion de variables
        Cls_Ope_Con_Cierre_Anual_Negocio Cierre_Anual_Negocio = new Cls_Ope_Con_Cierre_Anual_Negocio(); //variable para la capa de negocios

        try
        {
            //Asignar propiedades
            Cierre_Anual_Negocio.P_Cuenta_Inicial = Cmb_Cuenta_Inicial.SelectedItem.Value;
            Cierre_Anual_Negocio.P_Cuenta_Final = Cmb_Cuenta_Final.SelectedItem.Value;
            Cierre_Anual_Negocio.P_Cuenta_Contable_Rango = true;
            Cierre_Anual_Negocio.P_Afectable = "SI";
            Cierre_Anual_Negocio.P_Anio = Txt_Anio.Text.Trim();
            Cierre_Anual_Negocio.P_Cuenta_Nueva = Txt_Nueva_Cuenta.Text.Trim();
            Cierre_Anual_Negocio.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
            Cierre_Anual_Negocio.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
            Cierre_Anual_Negocio.P_Descripcion = Txt_Descripcion.Text;
            Cierre_Anual_Negocio.Cierre_Anual();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message, ex);
        }
    }
    #endregion
    
    #region (Eventos)
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Salir.ToolTip == "Salir")
            {
                Session.Remove("Dt_Partidas_Poliza");
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                Limpia_Controles();//Limpia los controles de la forma
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Cmb_Cuenta_Inicial_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Ope_Con_Cierre_Anual_Negocio Rs_Cuentas_Contables = new Cls_Ope_Con_Cierre_Anual_Negocio(); //Variable de conexcion con la capa de negocios.
            DataTable Dt_Cuentas = null;    //Almacena los datos de la consulta.
            DataTable Dt_Tabla_Temporal = new DataTable();
            Cmb_Cuenta_Final.Enabled = true;
            Cmb_Cuenta_Final.Items.Clear();
            Rs_Cuentas_Contables.P_Cuenta_Inicial = Cmb_Cuenta_Inicial.SelectedValue.ToString();
            Rs_Cuentas_Contables.P_Cuenta_Contable_Rango = true;
            Rs_Cuentas_Contables.P_Nivel_ID = "00001";
            Dt_Cuentas = Rs_Cuentas_Contables.Consulta_Datos_Cuentas_Contables_Cierre();
            if (Dt_Cuentas.Rows.Count > 0)
            {
                if (Dt_Tabla_Temporal.Rows.Count <= 0 && Dt_Tabla_Temporal.Columns.Count <= 0)
                {
                    Dt_Tabla_Temporal.Columns.Add("CUENTA", typeof(System.String));//typeof(System.String)
                    Dt_Tabla_Temporal.Columns.Add("DESCRIPCION", typeof(System.String));
                    foreach (DataRow Fila in Dt_Cuentas.Rows)
                    {
                        if (Fila["Cuenta"].ToString().Substring(0, 1) == "4" || Fila["Cuenta"].ToString().Substring(0, 1) == "5" || Fila["Cuenta"].ToString().Substring(0, 1) == "6")
                        {
                            DataRow Dr_Temp = Dt_Tabla_Temporal.NewRow();
                            Dr_Temp["Cuenta"] = Fila["Cuenta"].ToString();
                            Dr_Temp["Descripcion"] = Fila["Descripcion"].ToString();
                            Dt_Tabla_Temporal.Rows.Add(Dr_Temp);
                            Dt_Tabla_Temporal.AcceptChanges();
                        }
                    }
                }
                Cmb_Cuenta_Final.Items.Clear();
                Cmb_Cuenta_Final.DataSource = Dt_Tabla_Temporal;
                Cmb_Cuenta_Final.DataValueField = "CUENTA";
                Cmb_Cuenta_Final.DataTextField = "DESCRIPCION";
                Cmb_Cuenta_Final.DataBind();
                Cmb_Cuenta_Final.Items.Insert(0, "< SELECCIONE >");
                Cmb_Cuenta_Final.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Cmb_Cuenta_Inicial_SelectedIndexChanged: " + ex.Message.ToString();
        }
    }
    //////protected void Btn_Cierre_Anual_Click(object sender, EventArgs e)
    //////{
    //////    //Declaracion de variables
    //////    String Validacion = String.Empty; //variable para la validacion de diciembre
    //////    try
    //////    {
    //////        Lbl_Mensaje_Error.Visible = false;
    //////        Img_Error.Visible = false;
    //////        if (Validar_Datos_Cierre_Anual())
    //////        {
    //////            //validsr que el mes de diciembre este cuadrado
    //////            Validacion = Valida_Cierre_Mensual(Convert.ToInt32(Txt_Anio.Text.Trim()));
    //////            if (String.IsNullOrEmpty(Validacion) == true)
    //////            {
    //////                Cls_Ope_Con_Cierre_Anual_Negocio Rs_Alta_Cierre = new Cls_Ope_Con_Cierre_Anual_Negocio();   //Variable de conexion con la capa de negocios.
    //////                Decimal Totales = 0;   //Recibe el total del debe y el haber del rango de cuentas seleccionadas.
    //////                Decimal Total_Haber = 0;
    //////                Decimal TOTAL_GENERAL = 0;
    //////                Decimal Total_Debe = 0;
    //////                Decimal Total_Poliza = 0;
    //////                Decimal Total_Cuenta = 0;
    //////                Cls_Ope_Con_Cierre_Anual_Negocio Rs_Cuentas_Contables = new Cls_Ope_Con_Cierre_Anual_Negocio(); //Variable de conexion con la capa de negocios.
    //////                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas = new Cls_Cat_Con_Cuentas_Contables_Negocio();
    //////                DataTable Dt_Cuenta_ID = new DataTable();
    //////                DataTable Dt_Cuentas = new DataTable();    //Almacena los datos de la consulta;
    //////                int Partidas = 0;
    //////                DataTable Dt_Partidas_Polizas = new DataTable();
    //////                //columnas del datatable
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida, typeof(System.Int32));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Cat_Con_Cuentas_Contables.Campo_Cuenta, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Concepto, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Debe, typeof(System.Double));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Haber, typeof(System.Double));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Partida_ID, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Dependencia_ID, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add(Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID, typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add("MOMENTO_INICIAL", typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add("MOMENTO_FINAL", typeof(System.String));
    //////                Dt_Partidas_Polizas.Columns.Add("Nombre_Cuenta", typeof(System.String));
    //////                Rs_Cuentas_Contables.P_Cuenta_Inicial = Cmb_Cuenta_Inicial.SelectedValue.ToString();
    //////                Rs_Cuentas_Contables.P_Cuenta_Final = Cmb_Cuenta_Final.SelectedValue.ToString();
    //////                Rs_Cuentas_Contables.P_Cuenta_Contable_Rango = true;
    //////                Rs_Cuentas_Contables.P_Afectable = "SI";
    //////                Dt_Cuentas = Rs_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();

    //////                foreach (DataRow Registro in Dt_Cuentas.Rows)
    //////                {
    //////                    Totales = Obtener_Debe_Haber_Cuentas(Registro);
    //////                    if (Totales != 0)
    //////                    {
    //////                        Partidas = Partidas + 1;
    //////                        if (Totales > 0)
    //////                        {
    //////                            TOTAL_GENERAL = TOTAL_GENERAL + Totales;
    //////                            DataRow row = Dt_Partidas_Polizas.NewRow();
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
    //////                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Txt_Anio.Text;
    //////                            if (Registro[Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().ToUpper() == "ACREEDOR")
    //////                            {
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = Totales;
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
    //////                                Total_Debe = Total_Debe + Totales;
    //////                                Total_Poliza = Total_Poliza + Totales;
    //////                            }
    //////                            else
    //////                            {
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = Totales;
    //////                                Total_Haber = Total_Haber + Totales;
    //////                            }
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
    //////                            row["MOMENTO_INICIAL"] = "";
    //////                            row["MOMENTO_FINAL"] = "";
    //////                            row["Nombre_Cuenta"] = Registro[Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString();
    //////                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
    //////                            Dt_Partidas_Polizas.AcceptChanges();

    //////                        }
    //////                        else
    //////                        {
    //////                            DataRow row = Dt_Partidas_Polizas.NewRow();
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Registro[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
    //////                            row[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Registro[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Txt_Anio.Text;
    //////                            if (Registro[Cat_Con_Cuentas_Contables.Campo_Tipo_Cuenta].ToString().ToUpper() == "ACREEDOR")
    //////                            {
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = (Totales * -1);
    //////                                Total_Haber = Total_Haber + (Totales * -1);
    //////                            }
    //////                            else
    //////                            {
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Debe] = (Totales * -1);
    //////                                row[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
    //////                                Total_Debe = Total_Debe + Totales;
    //////                                Total_Poliza = Total_Poliza + (Totales * -1);
    //////                            }
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
    //////                            row[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
    //////                            row["MOMENTO_INICIAL"] = "";
    //////                            row["MOMENTO_FINAL"] = "";
    //////                            row["Nombre_Cuenta"] = Registro[Cat_Con_Cuentas_Contables.Campo_Descripcion].ToString();
    //////                            Dt_Partidas_Polizas.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
    //////                            Dt_Partidas_Polizas.AcceptChanges();
    //////                        }
    //////                    }
    //////                }
    //////                Rs_Cuentas.P_Cuenta = Txt_Nueva_Cuenta.Text;
    //////               Dt_Cuenta_ID = Rs_Cuentas.Consulta_Cuentas_Contables();
    //////                Total_Cuenta = Total_Debe - Total_Haber;
    //////                DataRow Rows = Dt_Partidas_Polizas.NewRow();
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Partida] = Partidas;
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID] = Dt_Cuenta_ID.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
    //////                Rows[Cat_Con_Cuentas_Contables.Campo_Cuenta] = Txt_Nueva_Cuenta.Text;
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Concepto] = "CIERRE EJERCICIO " + Txt_Anio.Text;
    //////                if (Total_Cuenta < 0)
    //////                {
    //////                    Rows[Ope_Con_Polizas_Detalles.Campo_Debe] = (Total_Cuenta * -1);
    //////                    Rows[Ope_Con_Polizas_Detalles.Campo_Haber] = 0;
    //////                }
    //////                else
    //////                {
    //////                    Rows[Ope_Con_Polizas_Detalles.Campo_Debe] = 0;
    //////                    Rows[Ope_Con_Polizas_Detalles.Campo_Haber] = Total_Cuenta;
    //////                }
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Fuente_Financiamiento_ID] = null;
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Partida_ID] = null;
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Dependencia_ID] = null;
    //////                Rows[Ope_Con_Polizas_Detalles.Campo_Proyecto_Programa_ID] = null;
    //////                Rows["MOMENTO_INICIAL"] = "";
    //////                Rows["MOMENTO_FINAL"] = "";
    //////                Rows["Nombre_Cuenta"] = "";
    //////                Dt_Partidas_Polizas.Rows.Add(Rows); //Agrega el registro creado con todos sus valores a la tabla
    //////                Dt_Partidas_Polizas.AcceptChanges();

    //////                Cls_Ope_Con_Polizas_Negocio Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
    //////                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID = Cls_Sessiones.Empleado_ID;
    //////                Rs_Alta_Ope_Con_Polizas = new Cls_Ope_Con_Polizas_Negocio();
    //////                Rs_Alta_Ope_Con_Polizas.P_Tipo_Poliza_ID = "00003";
    //////                //Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = "12"+Txt_Anio.Text.Substring(2,2);
    //////                Rs_Alta_Ope_Con_Polizas.P_Mes_Ano = "13" + Txt_Anio.Text.Substring(2, 2);
    //////                Rs_Alta_Ope_Con_Polizas.P_Fecha_Poliza = new DateTime(Convert.ToInt32(Txt_Anio.Text.Trim()), 12, 31);
    //////                Rs_Alta_Ope_Con_Polizas.P_Concepto = "CIERRE EJERCICIO " + Txt_Anio.Text;
    //////                Rs_Alta_Ope_Con_Polizas.P_Total_Debe = Convert.ToDouble(Total_Poliza);
    //////                Rs_Alta_Ope_Con_Polizas.P_Total_Haber = Convert.ToDouble(Total_Poliza);
    //////                Rs_Alta_Ope_Con_Polizas.P_No_Partida = Convert.ToInt32(Partidas);
    //////                Rs_Alta_Ope_Con_Polizas.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
    //////                Rs_Alta_Ope_Con_Polizas.P_Dt_Detalles_Polizas = Dt_Partidas_Polizas;
    //////                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Creo = Cls_Sessiones.Empleado_ID;
    //////                Rs_Alta_Ope_Con_Polizas.P_Empleado_ID_Autorizo = Cls_Sessiones.Empleado_ID;
    //////                Rs_Alta_Ope_Con_Polizas.P_Prefijo = "";
    //////                string[] Datos_Poliza = Rs_Alta_Ope_Con_Polizas.Alta_Poliza(); //Da de alta los datos de la Póliza proporcionados por el usuario en la BD
    //////                Cls_Cat_Con_Cuentas_Contables_Negocio Rs_Cuentas_2 = new Cls_Cat_Con_Cuentas_Contables_Negocio();
    //////                DataTable Dt_Cuentas_Resultado = new DataTable();
    //////                foreach (DataRow Fila in Dt_Partidas_Polizas.Rows)
    //////                {
    //////                    Rs_Alta_Cierre.P_Anio = Txt_Anio.Text;
    //////                    Dt_Cuentas_Resultado = new DataTable();
    //////                    Rs_Cuentas_2.P_Cuenta = Cmb_Cuenta_Final.SelectedValue;
    //////                    Dt_Cuentas_Resultado = Rs_Cuentas_2.Consulta_Cuentas_Contables();
    //////                    Rs_Alta_Cierre.P_Cuenta_Contable_ID_Fin = Dt_Cuentas_Resultado.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
    //////                    Rs_Cuentas_2.P_Cuenta = Fila[Cat_Con_Cuentas_Contables.Campo_Cuenta].ToString();
    //////                    Dt_Cuentas_Resultado = new DataTable();
    //////                    Rs_Cuentas_2.P_Cuenta = Cmb_Cuenta_Inicial.SelectedValue;
    //////                    Dt_Cuentas_Resultado = Rs_Cuentas_2.Consulta_Cuentas_Contables();
    //////                    Rs_Alta_Cierre.P_Cuenta_Contable_ID_Inicio = Dt_Cuentas_Resultado.Rows[0][Cat_Con_Cuentas_Contables.Campo_Cuenta_Contable_ID].ToString();
    //////                    Rs_Alta_Cierre.P_Cuenta_Contable_ID = Fila[Ope_Con_Polizas_Detalles.Campo_Cuenta_Contable_ID].ToString();
    //////                    Rs_Alta_Cierre.P_Descripcion = Txt_Descripcion.Text;
    //////                    Rs_Alta_Cierre.P_Diferencia = Math.Abs(Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Debe].ToString()) - Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Haber].ToString()));
    //////                    Rs_Alta_Cierre.P_Total_Debe = Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Debe].ToString());
    //////                    Rs_Alta_Cierre.P_Total_Haber = Convert.ToDouble(Fila[Ope_Con_Polizas_Detalles.Campo_Haber].ToString());
    //////                    Rs_Alta_Cierre.P_Usuario_Creo = Cls_Sessiones.Nombre_Empleado;
    //////                    Rs_Alta_Cierre.Alta_Cierre_Mensual();
    //////                }

    //////                //hacer el cierre mensual
    //////                Cierre_Mensual(Convert.ToInt32(Txt_Anio.Text.Trim()));
    //////                Inicializa_Controles(); //Inicializa los controles
    //////                ScriptManager.RegisterStartupScript(this, this.GetType(), "Cierre Anual", "alert('El Alta del Cierre Anual fue Exitosa');", true);
    //////            }
    //////            else
    //////            {
    //////                Lbl_Mensaje_Error.Visible = true;
    //////                Img_Error.Visible = true;
    //////                Lbl_Mensaje_Error.Text = Validacion;
    //////            }
    //////        }
    //////        else
    //////        {
    //////            Lbl_Mensaje_Error.Visible = true;
    //////            Img_Error.Visible = true;
    //////        }
    //////    }
    //////    catch (Exception ex)
    //////    {
    //////        Lbl_Mensaje_Error.Visible = true;
    //////        Img_Error.Visible = true;
    //////        Lbl_Mensaje_Error.Text = "Btn_Cierre_Anual_Click: " + ex.Message.ToString();
    //////    }
    //////}

    protected void Btn_Cierre_Anual_Click(object sender, EventArgs e)
    {
        //Declaracion de variables
        String Validacion = String.Empty; //variable para la validacion de diciembre
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Validar_Datos_Cierre_Anual())
            {
                //validsr que el mes de diciembre este cuadrado
                Validacion = Valida_Cierre_Mensual(Convert.ToInt32(Txt_Anio.Text.Trim()));
                if (String.IsNullOrEmpty(Validacion) == true)
                {
                    Cierre_Anual_Completo();
                    Inicializa_Controles(); //Inicializa los controles
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Cierre Anual", "alert('El Alta del Cierre Anual fue Exitosa');", true);
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                    Lbl_Mensaje_Error.Text = Validacion;
                }
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Btn_Cierre_Anual_Click: " + ex.Message.ToString();
        }
    }

    protected void Txt_Nueva_Cuenta_TextChanged(object sender, EventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            Cls_Ope_Con_Cierre_Anual_Negocio Rs_Cuentas_Contables = new Cls_Ope_Con_Cierre_Anual_Negocio(); //Variable de conexion con la capa de negocios.
            DataTable Dt_Cuentas = null;    //Almacena los datos de la consulta.

            Rs_Cuentas_Contables.P_Cuenta_Inicial = Txt_Nueva_Cuenta.Text;
            Dt_Cuentas = Rs_Cuentas_Contables.Consulta_Datos_Cuentas_Contables();

            if (Dt_Cuentas.Rows.Count == 0)
            {
                Txt_Nueva_Cuenta.Focus();
                Txt_Nueva_Cuenta.Text = "";
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "La cuenta contable no existe aun en el catálogo";
            }
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = "Txt_Nueva_Cuenta_TextChanged: " + Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Cerrar_Modal_Click
    /// DESCRIPCION : Cierra la ventana de busqueda cuentas.
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Cerrar_Modal_Click(object sender, ImageClickEventArgs e)
    {
        Grid_Cuentas_Contables.DataSource = null;   // Se iguala el DataTable con el Grid
        Grid_Cuentas_Contables.DataBind();    // Se ligan los datos.
        Txt_Busqueda_Cuenta_Contable.Text = "";
        Mpe_Busqueda_Cuenta_Contable.Hide();    //Oculta el ModalPopUp
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: Btn_Busqueda_Cuentas_Popup_Click
    /// DESCRIPCION : Busca las cuentas contables referentes a la descripcion
    /// CREO        : Sergio Manuel Gallardo Andrade
    /// FECHA_CREO  : 02/Mayo/2012
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    protected void Btn_Busqueda_Cuentas_Popup_Click(object sender, EventArgs e)
    {
        try
        {
            Consulta_Cuentas_Contables_Avanzada();
        }
        catch (Exception Ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = Ex.Message.ToString();
        }
    }
    ///*******************************************************************************
    ///NOMBRE DE LA FUNCIÓN: Btn_Seleccionar_Cuenta_Click
    ///DESCRIPCIÓN: Metodo para asignar al combo la cuenta contable
    ///CREO: Sergio Manuel Gallardo Andrade
    ///FECHA_CREO: 18/mayo/2012
    ///MODIFICO:
    ///FECHA_MODIFICO:
    ///CAUSA_MODIFICACIÓN:
    ///*******************************************************************************
    protected void Btn_Seleccionar_Cuenta_Click(object sender, ImageClickEventArgs e)
    {
        String Cuenta = ((ImageButton)sender).CommandArgument;
        try
        {
            Txt_Nueva_Cuenta.Text = Cuenta;
            Txt_Busqueda_Cuenta_Contable.Text = "";
            Grid_Cuentas_Contables.DataSource = null;
            Grid_Cuentas_Contables.DataBind();
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion
}
