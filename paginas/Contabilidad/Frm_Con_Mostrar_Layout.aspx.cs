﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.IO;
using CarlosAg.ExcelXmlWriter;
using System.Text;

public partial class paginas_Contabilidad_Frm_Con_Mostrar_Layout : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            Controlador();
        }
        catch (Exception Ex)
        {
            //Response.Redirect("Frm_Ope_Con_Cheques.aspx?PAGINA=596");
        }
    }
    //         '*******************************************************************************
    //'NOMBRE DE LA FUNCION:    Controlador
    //'DESCRIPCION:             Entregar los resultados de las consultas en JSON o los archivos de consultas (ASCII, Excel)
    //'PARAMETROS:              1. objResponse: Objeto tipo Response para los parametros de las funciones
    //'                         2. objRequest: Objeto tipo Request para los parametros de las funciones
    //'CREO:                    Noe Mosqueda Valadez
    //'FECHA_CREO:              17/Agosto/2011 13:30 
    //'MODIFICO:                
    //'FECHA_MODIFICO:          
    //'CAUSA_MODIFICACION:      
    //'*******************************************************************************
    protected void Controlador()
    {
        //'Declaracion de variables
        try{
        WorksheetCell Celda = new WorksheetCell();
        WorksheetCell Celda2 = new WorksheetCell();
        DataTable Dt_Consulta = new DataTable();
        DataTable Dt_Documentos = new DataTable();
        String Ruta_Archivo = "";
        String Nombre_Archivo = "";
        String Numero_Orden = Request.QueryString["Orden"];
        DataTable Dt_Reporte = (DataTable)Session["Dt_Reporte"];
            Nombre_Archivo = "Transferencias_banco.xls";
            //  Creamos el libro de Excel.
            CarlosAg.ExcelXmlWriter.Workbook Libro = new CarlosAg.ExcelXmlWriter.Workbook();
            //  propiedades del libro
            Libro.Properties.Title = "LATOUT TB-" + Numero_Orden + "-11";
            Libro.Properties.Created = DateTime.Now;
            Libro.Properties.Author = "JAPAMI_Irapuato";

            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera = Libro.Styles.Add("Encabezado");
            //  Creamos el estilo cabecera para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Cabecera_Izquierda = Libro.Styles.Add("Encabezado_Izquierda");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Indice = Libro.Styles.Add("Contenido_Indice");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre = Libro.Styles.Add("Contenido_Nombre");
            ////  Creamos el estilo cabecera 2 para la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Nombre_Negritas = Libro.Styles.Add("Contenido_Nombre_Negritas");
            //  Creamos el estilo cabecera 2 para la hoja de excel. 
            CarlosAg.ExcelXmlWriter.WorksheetStyle Estilo_Contenido_Firma = Libro.Styles.Add("Contenido_Firma");

            #region Estilos
            //***************************************inicio de los estilos***********************************************************
            //  estilo para la cabecera    Encabezado
            Estilo_Cabecera.Font.FontName = "Arial";
            Estilo_Cabecera.Font.Size = 10;
            Estilo_Cabecera.Font.Bold = true;
            Estilo_Cabecera.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera.Alignment.Rotate = 0;
            Estilo_Cabecera.Font.Color = "#000000";
            Estilo_Cabecera.Interior.Color = "white";
            Estilo_Cabecera.Interior.Pattern = StyleInteriorPattern.None;
            //  estilo para la cabecera    Encabezado_Izquierda
            Estilo_Cabecera_Izquierda.Font.FontName = "Arial";
            Estilo_Cabecera_Izquierda.Font.Size = 10;
            Estilo_Cabecera_Izquierda.Font.Bold = true;
            Estilo_Cabecera_Izquierda.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Cabecera_Izquierda.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Cabecera_Izquierda.Alignment.Rotate = 0;
            Estilo_Cabecera_Izquierda.Font.Color = "#000000";
            Estilo_Cabecera_Izquierda.Interior.Color = "white";
            Estilo_Cabecera_Izquierda.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el contenido   contenido_indice
            //estilo para el contenido   contenido_indice
            Estilo_Contenido_Indice.Font.FontName = "Arial";
            Estilo_Contenido_Indice.Font.Size = 10;
            Estilo_Contenido_Indice.Font.Bold = false;
            Estilo_Contenido_Indice.Alignment.Horizontal = StyleHorizontalAlignment.Left;
            Estilo_Contenido_Indice.Alignment.Vertical = StyleVerticalAlignment.Center;
            Estilo_Contenido_Indice.Alignment.Rotate = 0;
            Estilo_Contenido_Indice.Font.Color = "#000000";
            Estilo_Contenido_Indice.Interior.Color = "White";
            Estilo_Contenido_Indice.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Nombre
            Estilo_Contenido_Nombre.Font.FontName = "Arial";
            Estilo_Contenido_Nombre.Font.Size = 10;
            Estilo_Contenido_Nombre.Font.Bold = true;
            Estilo_Contenido_Nombre.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Nombre.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre.Font.Color = "#000000";
            Estilo_Contenido_Nombre.Interior.Color = "white";
            Estilo_Contenido_Nombre.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Firma
            Estilo_Contenido_Nombre_Negritas.Font.FontName = "Arial";
            Estilo_Contenido_Nombre_Negritas.Font.Size = 10;
            Estilo_Contenido_Nombre_Negritas.Font.Bold = true;
            Estilo_Contenido_Nombre_Negritas.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Nombre_Negritas.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Nombre_Negritas.Alignment.Rotate = 0;
            Estilo_Contenido_Nombre_Negritas.Font.Color = "#000000";
            Estilo_Contenido_Nombre_Negritas.Interior.Color = "white";
            Estilo_Contenido_Nombre_Negritas.Interior.Pattern = StyleInteriorPattern.None;
            //estilo para el    Contenido_Nombre Negritas
            Estilo_Contenido_Firma.Font.FontName = "Arial";
            Estilo_Contenido_Firma.Font.Size = 10;
            Estilo_Contenido_Firma.Font.Bold = true;
            Estilo_Contenido_Firma.Alignment.Horizontal = StyleHorizontalAlignment.Center;
            Estilo_Contenido_Firma.Alignment.Vertical = StyleVerticalAlignment.JustifyDistributed;
            Estilo_Contenido_Firma.Alignment.Rotate = 0;
            Estilo_Contenido_Firma.Font.Color = "#000000";
            Estilo_Contenido_Firma.Interior.Color = "white";
            Estilo_Contenido_Firma.Interior.Pattern = StyleInteriorPattern.None;
            //*************************************** fin de los estilos***********************************************************
            #endregion

            #region Hojas
            //  Creamos una hoja que tendrá la BB
            CarlosAg.ExcelXmlWriter.Worksheet Hoja = Libro.Worksheets.Add("BB");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon = Hoja.Table.Rows.Add();
            //  para la hoja de BP
            CarlosAg.ExcelXmlWriter.Worksheet Hoja_Ea = Libro.Worksheets.Add("BP");
            //  Agregamos un renglón a la hoja de excel.
            CarlosAg.ExcelXmlWriter.WorksheetRow Renglon_Ea = Hoja_Ea.Table.Rows.Add();
            #endregion
            #region
            //  Agregamos las columnas que tendrá la hoja de excel.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(20));//   1 tipo.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(80));//  2 id.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  3 cuenta origen.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  4 cuenta destino.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  5 importe.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(40));//  6 orden.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  7 concepto.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(20));//   8 tm.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(90));//  9 rfc municipio.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  10 ceros.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(250));//  11 email.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  12 fecha.
            Hoja.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  13 beneficiario.
            //  Agregamos las columnas que tendrá la hoja de excel.

            #region ENCABEZADOS
            Celda = Renglon.Cells.Add("TIPO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("ID");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CUENTA ORIGEN");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CUENTA DESTINO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("IMPORTE");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("ORDEN");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CONCEPTO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("TM");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("RFC MUNICIPIO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("CEROS");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("EMAIL");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("FECHA");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Celda = Renglon.Cells.Add("BENEFICIARIO");
            Celda.MergeDown = 1; // Merge two cells together
            Celda.StyleID = "Encabezado";

            Renglon = Hoja.Table.Rows.Add();

            #endregion

            Dt_Reporte.DefaultView.RowFilter = "PESTANIA='BB'";
            foreach (DataRow Dr in Dt_Reporte.DefaultView.ToTable().Rows )
            {
                Renglon = Hoja.Table.Rows.Add();

                foreach (DataColumn Columna in Dt_Reporte.DefaultView.ToTable().Columns)
                {
                        if (Columna is DataColumn)
                        {
                            if (Columna.ColumnName.ToString() != "PESTANIA")
                            {
                                    Renglon.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Contenido_Indice"));
                            }
                          }
                }
            }
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//   1 tipo.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(100));//  2 id.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  3 cuenta origen.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(200));//  4 cuenta destino.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  5 importe.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  6 orden.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//  7 concepto.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(50));//   8 tm.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  9 rfc municipio.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(150));//  10 ceros.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(300));//  11 email.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(120));//  12 fecha.
            Hoja_Ea.Table.Columns.Add(new CarlosAg.ExcelXmlWriter.WorksheetColumn(400));//  13 beneficiario.
            #region ENCABEZADOS Hoja_Ea
            Celda2 = Renglon_Ea.Cells.Add("TIPO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("ID");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CUENTA ORIGEN");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CUENTA DESTINO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("IMPORTE");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("ORDEN");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CONCEPTO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("TM");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("RFC MUNICIPIO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("CEROS");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("EMAIL");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("FECHA");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Celda2 = Renglon_Ea.Cells.Add("BENEFICIARIO");
            Celda2.MergeDown = 1; // Merge two cells together
            Celda2.StyleID = "Encabezado";

            Renglon_Ea = Hoja_Ea.Table.Rows.Add();

            #endregion

            Dt_Reporte.DefaultView.RowFilter = "PESTANIA='BP'";

            foreach (DataRow Dr in Dt_Reporte.DefaultView.ToTable().Rows)
            {
                Renglon_Ea = Hoja_Ea.Table.Rows.Add();

                foreach (DataColumn Columna in Dt_Reporte.DefaultView.ToTable().Columns)
                {
                    if (Columna is DataColumn)
                    {
                        if (Columna.ColumnName.ToString() != "PESTANIA")
                        {
                                Renglon_Ea.Cells.Add(new CarlosAg.ExcelXmlWriter.WorksheetCell(Dr[Columna.ColumnName].ToString(), DataType.String, "Contenido_Indice"));
                        }
                    }
                }
            }
            #endregion
            //Abre el archivo de excel
            Ruta_Archivo = @Server.MapPath("../../Reporte/" + Nombre_Archivo);
                //Response.Clear();
                //Response.Buffer = true;
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Disposition", "attachment;filename=" + Nombre_Archivo);
                //Response.WriteFile(Ruta_Archivo);
                //Response.Charset = "UTF-8";
                //Response.ContentEncoding = Encoding.Default;
                Libro.Save(Response.OutputStream);
                //Response.End();
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al abrir los archivos de dispersión a bancos. Error: [" + Ex.Message + "]");
        }
    }
}
