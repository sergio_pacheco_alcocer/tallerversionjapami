﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Pago_Solicitud_Gastos.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Pago_Solicitud_Gastos" Title="Untitled Page" %>
<%@ Register assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server"></asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
<script src="../../easyui/jquery-1.4.2.js" type="text/javascript"></script>
    <script src="../jquery/jquery-1.5.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $("select[id$=Cmb_Tipo_Pago]").change(function() {
            $(".Cmb_Tipo_Pago option:selected").each(function() {
                var Tipo = $(this).val();
                if (Tipo == "CHEQUE") {
                    $("#Tr_Cheques").show();
                    $("#Tr_Referencia").show();
                    $("#Td_Cuenta").hide();
                    $("#Td_Numero_Cuenta").hide();
                }
                if (Tipo == "TRANSFERENCIA") {
                    $("#Tr_Cheques").hide();
                    $("#Tr_Referencia").show();
                    $("#Td_Cuenta").show();
                    $("#Td_Numero_Cuenta").show();
                    document.getElementById("<%=Txt_No_Cheque.ClientID%>").value = ""
                }
            });
        }).trigger('change');
        $("select[id$=Cmb_Estatus]").change(function() {
            $(".Estatus option:selected").each(function() {
                var Tipo = $(this).val();
                if (Tipo == "PAGADO") {
                    $("#Tr_Estatus").hide();
                    document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                }
                if (Tipo == "CANCELADO") {
                    $("#Tr_Estatus").show();
                    document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                }
            });
        }).trigger('change');
    });
    function selec_todo2() {
        var y;
        y = $('#chkAll').is(':checked');
        var $chkBox = $("input:checkbox[id$=Chk_Transferencia]");
        if (y == true) {
            $chkBox.attr("checked", true);
        } else {
            $chkBox.attr("checked", false);
        }
        Agregar();
    }
    function Agregar() {
        document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = ""
        var a = $("input:checkbox[id$=Chk_Transferencia]")
        for (var j = 0; j < a.length; j++) {
            if ($(a[j]).is(':checked') == true) {
                document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value +$(a[j]).parent().attr('class') +"-";
            }
        }
    }
    function Refrescar(){
        location.reload();
    }
    function Cerrar_Modal_Popup_Detalles() {
        $find('Mp_Detalles').hide();
        $("input[id$=Txt_Fecha_Solicitud_Det]").val('');
        $("input[id$=Txt_Monto_Solicitud_Det]").val('');
        $("input[id$=Txt_No_Pago_Det]").val('');
        $("input[id$=Txt_No_Reserva_Det]").val('');
        return false;
    }
    function Cerrar_Modal_Popup_Proveedor() {
        $find('Mp_Proveedor').hide();
        $("input[id$=Txt_Banco_Proveedor]").val('');
        $("input[id$=Txt_Clabe]").val('');
        $("input[id$=Txt_Numero_Cuenta]").val('');
        $("input[id$=Txt_Padron_ID]").val('');
        return false;
    }
    function Mostrar_Tabla(Renglon, Imagen) {
        object = document.getElementById(Renglon);
        if (object.style.display == "none") {
            object.style.display = "";
            document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
        } else {
            object.style.display = "none";
            document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
        }
    }
    // El popup para el rechazo de la solicitud
    function Abrir_Popup2(Control) {
        $find('Contenedor').show();
        document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
        document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 1;
        $("#Tr_Autorizar").hide();
        $("#Tr_Rechazar").show();
    }
    function Cerrar_Modal_Popup() {
        $find('Contenedor').hide();
        Limpiar_Ctlr();
        return false;
    }
    function Limpiar_Ctlr() {
        var $chkBox = $("input:checkbox[id$=Chk_Rechazar]");
        document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = "";
        document.getElementById("<%=Txt_Rechazo.ClientID%>").value = "";
        document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
        $chkBox.attr('checked', false);
    }
</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
    <cc1:ToolkitScriptManager ID="ScriptManager_Recepcion_Documentos" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
    <asp:UpdatePanel ID="Upd_Panel" runat="server" >
        <ContentTemplate>
           <asp:UpdateProgress ID="Uprg_Polizas" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                   <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Compromisos" style="background-color:#ffffff; width:98%; height:100%;">
                <table width="100%" class="estilo_fuente">
                    <tr align="center">
                        <td class="label_titulo">Pagos de Solicitud de Gastos por Comprobar</td>
                    </tr>
                    <tr>
                        <td aling="left">&nbsp;
                           <asp:UpdatePanel ID="Upnl_Mensajes_Error" runat="server" UpdateMode="Always" RenderMode="Inline">
                                <ContentTemplate>                         
                                    <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                                    <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"/>
                                </ContentTemplate>                                
                            </asp:UpdatePanel>                                  
                        </td>
                    </tr> 
                </table>

                <table width="98%"  border="0" cellspacing="0">
                    <tr align="center">
                        <td>                
                            <div align="right" class="barra_busqueda">                        
                                <table style="width:100%;height:28px;">
                                    <tr>
                                        <td align="left" style="width:59%;"> 
                                                    <asp:ImageButton ID="Btn_Nuevo" runat="server" ToolTip="Nuevo" 
                                                        CssClass="Img_Button" TabIndex="1"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_nuevo.png" 
                                                        onclick="Btn_Nuevo_Click" />
                                                    <asp:ImageButton ID="Btn_Modificar" runat="server" ToolTip="Modificar" 
                                                        CssClass="Img_Button" TabIndex="2"  OnClientClick="return confirm('¿Esta Seguro de Cancelar el Pago?');" 
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_modificar.png" 
                                                        onclick="Btn_Modificar_Click" />
                                                    <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" 
                                                        CssClass="Img_Button" TabIndex="4"
                                                        ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" 
                                                        onclick="Btn_Salir_Click" />                                            
                                        </td>
                                        <td align="right" style="width:41%;">                                  
                                        </td>       
                                    </tr>         
                                    <tr>
                                        <td>
                                          <%--  <cc1:ModalPopupExtender ID="Mpe_Detalles" runat="server" BackgroundCssClass="popUpStyle"  
                                            BehaviorID="Mp_Detalles" TargetControlID="Btn_Comodin_Open_Detalles" PopupDragHandleControlID="Pnl_Detalles"
                                            PopupControlID="Pnl_Detalles_Contenedor" CancelControlID="Btn_Comodin_Close_Detalles"
                                             DropShadow="True" DynamicServicePath="" Enabled="True"/>
                                        <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close_Detalles" runat="server" Text="" />
                                        <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open_Detalles" runat="server" Text="" />--%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <cc1:ModalPopupExtender ID="Mpe_Proveedor" runat="server" BackgroundCssClass="popUpStyle"  
                                            BehaviorID="Mp_Proveedor" TargetControlID="Btn_Comodin_Open_Proveedor" PopupDragHandleControlID="Pnl_Proveedor"
                                            PopupControlID="Pnl_Proveedor_Contenedor" CancelControlID="Btn_Comodin_Close_Proveedor"
                                             DropShadow="True" DynamicServicePath="" Enabled="True"/>
                                        <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close_Proveedor" runat="server" Text="" />
                                        <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open_Proveedor" runat="server" Text="" />
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                </table>
                 <div id="Div_Solicitudes_Pendientes" runat="server" style="display:block" >
                        <asp:Panel ID="Pnl_Busqueda" runat="server" GroupingText="Datos para busqueda" Width="98%" BackColor="White">
                            <table width ="98%" class="estilo_fuente">
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Fecha_Inicio" runat="server" Text="Fecha Inicio"></asp:Label>
                                        </td>
                                        <td style=" width:15%">
                                            <asp:TextBox ID="Txt_Fecha_Inicio" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="15px"  Font-Size="Smaller"/>
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Inicio" runat="server" 
                                            TargetControlID="Txt_Fecha_Inicio" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_inicio"/>
                                         <asp:ImageButton ID="Btn_Fecha_Inicio" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Inicio" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Inicio" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="Mev_Txt_Fecha_Poliza" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Inicio"
                                            ControlExtender="Mee_Txt_Fecha_Inicio" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Inicio Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                        <td style="width:10%">
                                            <asp:Label ID="Lbl_Fecha_Final" runat="server" Text="Fecha Final"></asp:Label>
                                        </td>
                                        <td style="width:15%">
                                            <asp:TextBox ID="Txt_Fecha_Final" runat="server" Width="70%" TabIndex="6" MaxLength="11" Height="15px" Font-Size="Smaller"/>
                                        <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" WatermarkCssClass="watermarked" 
                                            WatermarkText="Dia/Mes/Año" Enabled="True" />
                                        <cc1:CalendarExtender ID="CE_Txt_Fecha_Final" runat="server" 
                                            TargetControlID="Txt_Fecha_Final" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Final"/>
                                         <asp:ImageButton ID="Btn_Fecha_Final" runat="server"
                                            ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                            Height="18px" CausesValidation="false"/>           
                                        <cc1:MaskedEditExtender 
                                            ID="Mee_Txt_Fecha_Final" 
                                            Mask="99/LLL/9999" 
                                            runat="server"
                                            MaskType="None" 
                                            UserDateFormat="DayMonthYear" 
                                            UserTimeFormat="None" Filtered="/"
                                            TargetControlID="Txt_Fecha_Final" 
                                            Enabled="True" 
                                            ClearMaskOnLostFocus="false"/>  
                                        <cc1:MaskedEditValidator 
                                            ID="MaskedEditValidator1" 
                                            runat="server" 
                                            ControlToValidate="Txt_Fecha_Final"
                                            ControlExtender="Mee_Txt_Fecha_Final" 
                                            EmptyValueMessage="Fecha Requerida"
                                            InvalidValueMessage="Fecha Final Invalida" 
                                            IsValidEmpty="false" 
                                            TooltipMessage="Ingrese o Seleccione la Fecha de Póliza"
                                            Enabled="true" style="font-size:10px;background-color:#F0F8FF;color:Black;font-weight:bold;"/>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style=" width:10%">
                                        &nbsp;&nbsp;
                                            <asp:Label ID="Lbl_Folio" runat="server" Text="Número Solicitud"></asp:Label>
                                        </td>
                                        <td style=" width:10%">                                        
                                            <asp:TextBox ID="Txt_No_Solicitud_Busqueda" runat="server" Width="70%"   MaxLength="10" Font-Size="Smaller"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender  ID="FilteredTextBoxExtender1" runat="server" FilterType ="Numbers" TargetControlID="Txt_No_Solicitud_Busqueda" ></cc1:FilteredTextBoxExtender>
                                        </td>
                                        <td style=" width:10%">
                                            <asp:Label ID="Lbl_Estatus_filtro" runat="server" Text="Estatus"></asp:Label>
                                        </td>
                                        <td style=" width:15%">
                                            <asp:DropDownList ID="Cmb_Estatus_filtro" runat="server"  Width="70%" Font-Size="Smaller">
                                            <asp:ListItem Value="0">&larr;SELECCIONE&rarr;</asp:ListItem>
                                            <asp:ListItem Value="PORPAGAR">PORPAGAR</asp:ListItem>
                                            <asp:ListItem Value="PAGADO">PAGADO</asp:ListItem>
                                            </asp:DropDownList> 
                                        </td>
                                        <td style="width:10%">
                                            <asp:ImageButton ID="Btn_Buscar_Solicitud" runat="server" 
                                                ToolTip="Consultar" TabIndex="6" 
                                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" onclick="Btn_Buscar_Solicitud_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style=" width:10%">
                                            <asp:Label ID="Lbl_Forma_Pago_Filtro" runat="server" Text="Forma Pago"></asp:Label> 
                                        </td>
                                        <td style=" width:15%">
                                            <asp:DropDownList ID="Cmb_Forma_Pago_Filtro" runat="server"  Width="70%" Font-Size="Smaller">
                                            <%--<asp:ListItem Value="Transferencia">Transferencia</asp:ListItem>--%>
                                            <asp:ListItem Value="Cheque">Cheque</asp:ListItem>
                                            </asp:DropDownList> 
                                        </td>
                                        <td style=" width:10%">
                                        <%--    <asp:Label ID="Lbl_Orden_Busqueda" runat="server" Text="Ordenar:"></asp:Label> --%>
                                        </td>
                                        <td style=" width:15%">
                                           <%-- <asp:RadioButtonList ID="RBL_Orden_Busqueda" runat="server" RepeatDirection="Horizontal">
                                            <asp:ListItem Value="BANCO">Banco</asp:ListItem>
                                            <asp:ListItem Value="TIPO_SOLICITUD">Tipo Pago</asp:ListItem>
                                            </asp:RadioButtonList>--%>
                                        </td>
                                    </tr>
                            </table>
                            </asp:Panel>
                            <table width ="98%" class="estilo_fuente">
                                    <tr>
                                        <td>
                                             <asp:Panel ID="Pnl_Datos_Transferencia" runat="server" GroupingText="Datos para Transferencia" Width="100%" BackColor="White">
                                                <table  width ="98%" class="estilo_fuente">
                                                        <tr>
                                                            <td style=" width:10%">
                                                                <asp:Label ID="Lbl_Forma_Pago_solicitud" runat="server" Text="Forma Pago"></asp:Label> 
                                                            </td>
                                                            <td style=" width:15%">
                                                                <asp:DropDownList ID="Cmb_Forma_Pago" runat="server"  Width="70%" AutoPostBack="true" OnSelectedIndexChanged="RBL_Orden_SelectedIndexChanged" Font-Size="Smaller">
                                                                <%--<asp:ListItem Value="Transferencia">Transferencia</asp:ListItem>--%>
                                                                <asp:ListItem Value="Cheque">Cheque</asp:ListItem>
                                                                </asp:DropDownList> 
                                                            </td>
                                                            <td style=" width:10%">
                                                            <%--<asp:Label ID="Lbl_Ordenar" runat="server" Text="Ordenar por:"></asp:Label> --%>
                                                            </td>
                                                            <td style=" width:15%">
                                                               <%-- <asp:RadioButtonList ID="RBL_Orden" runat="server" RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="RBL_Orden_SelectedIndexChanged">
                                                                <asp:ListItem Value="BANCO">Banco</asp:ListItem>
                                                                <asp:ListItem Value="TIPO_SOLICITUD">Tipo Pago</asp:ListItem>
                                                                </asp:RadioButtonList>--%>
                                                            </td>
                                                            <td style=" width:10%">
                                                            </td>
                                                       </tr>
                                                </table>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <div style="overflow:auto;height:500px;width:99%;vertical-align:top;border-style:outset;border-color:Silver; position:static" >
                                                <asp:GridView ID="Grid_Pagos" runat="server" AllowPaging="False"  ShowHeader="false"
                                                    AutoGenerateColumns="False" CssClass="GridView_1"  EmptyDataText="No se encontraron partidas con saldo" 
                                                    DataKeyNames="Tipo" GridLines="None" Width="99%" OnRowDataBound="Grid_Pagos_RowDataBound" >
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                                    ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                            </ItemTemplate>
                                                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                            <ItemStyle HorizontalAlign="Center" Width="2%" />
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="TIPO" HeaderText="Filtro">
                                                            <HeaderStyle HorizontalAlign="Left" Width="75%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="75%" />
                                                        </asp:BoundField>
					                                    <asp:BoundField DataField="Monto_Tipo" HeaderText="Monto" DataFormatString="{0:c}">
                                                            <HeaderStyle HorizontalAlign="right" Width="23%" />
                                                            <ItemStyle HorizontalAlign="right" Width="23%"  />
                                                        </asp:BoundField>
                                                        <asp:TemplateField>
                                                            <ItemTemplate>
                                                                <asp:Label ID="Lbl_Movimientos" runat="server" Text='<%# Bind("IDENTIFICADOR_TIPO") %>' Visible="false"></asp:Label>
                                                                <asp:Literal ID="Ltr_Inicio" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='3'; align='right'&gt;" />
                                                                <asp:GridView ID="Grid_Deudores" runat="server" AllowPaging="False" DataKeyNames="DEUDOR_ID" OnSelectedIndexChanged="Grid_Solicitud_Pagos_SelectedIndexChanged"
                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%" OnRowDataBound="Grid_Proveedores_RowDataBound">
                                                                    <Columns>
                                                                        <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Image ID="Img_Btn_Expandir_Deudor" runat="server"
                                                                                    ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="2%" />
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="DEUDOR_ID" HeaderText="Deudor">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                                        </asp:BoundField>
                                                                         <asp:ButtonField ButtonType="Link" CommandName="Select" DataTextField="NOMBRE" ControlStyle-Font-Size="X-Small">
                                                                             <HeaderStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="50%" />
                                                                            <ItemStyle Font-Size="XX-Small" Width="50%" ForeColor="Blue" />
                                                                        </asp:ButtonField>
                                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Monto" DataFormatString="{0:c}">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                        </asp:BoundField>
                                                                        <asp:BoundField DataField="Estatus" HeaderText="Estatus">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                        </asp:BoundField>
							                                            <asp:TemplateField>
                                                                            <ItemTemplate>
                                                                                <asp:Label ID="Lbl_Solicitud" runat="server" 
                                                                                    Text='<%# Bind("IDENTIFICADOR") %>' Visible="false"></asp:Label>
                                                                                    <asp:Literal ID="Ltr_Inicio2" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='8'; align='right';&gt;" />
                                                                                <asp:GridView ID="Grid_Datos_Solicitud" runat="server" AllowPaging="False"  OnSelectedIndexChanged="Grid_Solicitud_Detalles_SelectedIndexChanged"
                                                                                     OnRowDataBound="Grid_Solicitudes_RowDataBound" AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%">
                                                                                    <Columns>
                                                                                        <asp:TemplateField >
                                                                                            <ItemTemplate>
                                                                                                <asp:ImageButton ID="Btn_Seleccionar_Solicitudes" runat="server" ImageUrl="~/paginas/imagenes/gridview/blue_button.png"
                                                                                                     CommandArgument='<%# Eval("NO_DEUDA") %>' OnClick="Btn_Seleccionar_Solicitud_Click" />
                                                                                            </ItemTemplate>
                                                                                            <HeaderStyle HorizontalAlign="Center" Width="3%" />
                                                                                            <ItemStyle HorizontalAlign="Center" Width="3%" />
                                                                                        </asp:TemplateField>
                                                                                        <asp:BoundField DataField="NO_DEUDA" HeaderText="Solicitud">
                                                                                            <HeaderStyle HorizontalAlign="center" Width="10%" Font-Size="X-Small" />
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="10%" />
                                                                                        </asp:BoundField>
                                                                                         <asp:BoundField DataField="TIPO_MOVIMIENTO" HeaderText="Tipo Deuda">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="CONCEPTO" HeaderText="Concepto">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small"/>
                                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="30%" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="IMPORTE" HeaderText="Monto" DataFormatString="{0:c}">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                                            <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                                        </asp:BoundField>
                                                                                        <asp:BoundField DataField="Banco" HeaderText="Banco">
                                                                                            <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                                                        </asp:BoundField>
                                                                                        <asp:TemplateField  HeaderText= "Fecha">
                                                                                            <HeaderStyle  Font-Size="X-Small" HorizontalAlign="center" Width="15%" />
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="15%" />
                                                                                            <ItemTemplate >
                                                                                                <asp:TextBox ID="Txt_Fecha_Transferencia" runat="server" Width="60%" MaxLength="11"  Font-Size="X-Small"/>
                                                                                                <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Transferencia" runat="server" 
                                                                                                    TargetControlID="Txt_Fecha_Transferencia" WatermarkCssClass="watermarked" 
                                                                                                    WatermarkText="Dia/Mes/Año" Enabled="True" />
                                                                                                <cc1:CalendarExtender ID="CE_Txt_Fecha_Transferencia" runat="server" 
                                                                                                    TargetControlID="Txt_Fecha_Transferencia" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Transferencia"/>
                                                                                                 <asp:ImageButton ID="Btn_Fecha_Transferencia" runat="server"
                                                                                                    ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                                                                    Height="14px" CausesValidation="false"/>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                        <%--<asp:TemplateField  HeaderText= "Trans">
                                                                                            <HeaderStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                            <ItemTemplate >
                                                                                                <asp:CheckBox ID="Chk_Transferencia" runat="server" CssClass='<%# Eval("NO_DEUDA") %>' onclick="Agregar();"/>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>--%>
                                                                                        <asp:TemplateField  HeaderText= "Rechazar">
                                                                                            <HeaderStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="5%" />
                                                                                            <ItemTemplate >
                                                                                                <asp:CheckBox ID="Chk_Rechazar" runat="server" CssClass='<%# Eval("NO_DEUDA") %>' onclick="Abrir_Popup2(this);"/>
                                                                                            </ItemTemplate>
                                                                                        </asp:TemplateField>
                                                                                    </Columns>
                                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                                    <FooterStyle CssClass="GridPager" />
                                                                                    <HeaderStyle CssClass="GridHeader_Nested" />
                                                                                    <PagerStyle CssClass="GridPager" />
                                                                                    <RowStyle CssClass="GridItem" />
                                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                                </asp:GridView>
                                                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                                            </ItemTemplate>
                                                                        </asp:TemplateField>
                                                                        <asp:BoundField DataField="TIPO_DEUDOR" HeaderText="bene">
                                                                            <HeaderStyle HorizontalAlign="Left" Width="0.5%" Font-Size="X-Small"/>
                                                                            <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="0.5%" />
                                                                        </asp:BoundField>
                                                                    </Columns>
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                    <FooterStyle CssClass="GridPager" />
                                                                    <HeaderStyle CssClass="GridHeader_Nested" />
                                                                    <PagerStyle CssClass="GridPager" />
                                                                    <RowStyle CssClass="GridItem" />
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                </asp:GridView>
                                                                <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                    <FooterStyle CssClass="GridPager" />
                                                    <HeaderStyle CssClass="GridHeader" />
                                                    <PagerStyle CssClass="GridPager" />
                                                    <RowStyle CssClass="GridItem" />
                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                </asp:GridView>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Banco" runat="server" />
                                            <asp:HiddenField ID="Txt_Cuenta_Contable_Proveedor" runat="server" />
                                            <asp:HiddenField ID="Txt_Beneficiario_ID" runat="server" />
                                            <asp:HiddenField ID="Txt_TIpo_Benenficiario" runat="server" />
                                            <asp:HiddenField ID="Txt_Tipo_Solicitud" runat="server" />
                                            <asp:HiddenField ID="Txt_No_Solicitud_Autorizar" runat="server"  />
                                        </td>
                                    </tr>
                            </table>
                        </div>
                        <div id="Div_Datos_Solicitud" runat="server"  style=" display:block" >
                        <asp:Panel ID="Pnl_Datos_Generales" runat="server" GroupingText="Datos de la Solicitud"
                        Width="98%">
                            <table width="98%" class="estilo_fuente">
                                <tr>
                                    <td colspan="4">
                                          &nbsp;                                  
                                    </td>
                                </tr>
                                <tr>
                                    <td style=" width:25%">
                                    &nbsp;
                                    &nbsp;
                                        <asp:Label ID="Lbl_No_Solicitud" runat="server" Text="No. Solicitud"></asp:Label> 
                                    </td>
                                    <td style=" width:30%">
                                        <asp:TextBox ID="Txt_No_Solicitud" runat="server" ReadOnly ="true" Width="90%"></asp:TextBox>
                                    </td>
                                    <td style=" width:13%">
                                    &nbsp;
                                        <asp:Label ID="Lbl_Fecha" runat="server" Text="Fecha Solicitud"></asp:Label> 
                                    </td>
                                    <td style=" width:32%">
                                        <asp:TextBox ID="Txt_Fecha" runat="server" Enabled="False" Width="56%"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style=" width:25%">
                                    &nbsp;
                                    &nbsp;
                                        <asp:Label ID="Lbl_Tipo_Solicitud_pago" runat="server" Text="Tipo Solicitud"></asp:Label> 
                                    </td>
                                    <td style=" width:30%">
                                        <asp:TextBox ID="Txt_Tipo_Solicitud_Pago" runat="server" ReadOnly ="true" Width="90%"></asp:TextBox>
                                        <asp:HiddenField ID="Hd_TIpo_Solicitud_Pago" runat="server" />
                                    </td>
                                    <td style=" width:13%">
                                    &nbsp;
                                        <asp:Label ID="Lbl_Estatus" runat="server" Text="Estatus"></asp:Label> 
                                    </td>
                                    <td style=" width:32%">
                                        <asp:TextBox ID="Txt_Estatus_Solicitud" runat="server" Enabled="False" Width="56%"></asp:TextBox>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    &nbsp;
                                    &nbsp;
                                        <asp:Label ID="Lbl_Concepto" runat="server" Text="Concepto"></asp:Label></td>
                                    <td colspan="3" style=" width:60%">
                                            <asp:TextBox ID="Txt_Concepto" runat="server" MaxLength="200" Width="82%"></asp:TextBox>
                                    </td>
                                </tr>
                                 <tr>
                                    <td style=" width:25%">
                                    &nbsp;
                                    &nbsp;
                                        <asp:Label ID="Lbl_Monto" runat="server" Text="Monto"></asp:Label> 
                                    </td>
                                    <td style=" width:30%">
                                        <asp:TextBox ID="Txt_Monto" runat="server" ReadOnly ="true" Width="90%"></asp:TextBox>
                                    </td>
                                    <td style=" width:13%">
                                    &nbsp;
                                    </td>
                                    <td style=" width:32%">
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                            <asp:Panel ID="Pnl_Datos_Pago" runat="server" GroupingText="Datos de Pago" 
                                Width="98%">
                                <table class="estilo_fuente" width="98%">
                                    <tr>
                                        <td colspan="4">
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style=" width:25%">
                                            &nbsp; &nbsp;
                                            <asp:Label ID="Lbl_No_Pago" runat="server" Text="No. Pago"></asp:Label>
                                        </td>
                                        <td style=" width:30%">
                                            <asp:TextBox ID="Txt_No_Pago" runat="server" ReadOnly="true" Width="90%"></asp:TextBox>
                                        </td>
                                        <td style=" width:13%">
                                            &nbsp;
                                            <asp:Label ID="Lbl_Fecha_No_Pago" runat="server" Text="Fecha Pago" ></asp:Label>
                                        </td>
                                        <td style=" width:32%">
                                            <asp:TextBox ID="Txt_Fecha_No_Pago" runat="server" ReadOnly="true" Width="56%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td style=" width:25%">
                                            &nbsp; &nbsp;
                                            <asp:Label ID="Lbl_Tipo_Pago" runat="server" Text="Tipo_Pago"></asp:Label>
                                        </td>
                                        <td style=" width:30%">
                                            <asp:DropDownList ID="Cmb_Tipo_Pago" runat="server" CssClass="Cmb_Tipo_Pago" Width="92%">
                                                <asp:ListItem Value="CHEQUE">CHEQUE</asp:ListItem>
                                                <%--<asp:ListItem Value="TRANSFERENCIA">TRANSFERENCIA</asp:ListItem>--%>
                                            </asp:DropDownList>
                                        </td>
                                         <td style=" width:13%">
                                            &nbsp;
                                            <asp:Label ID="Lbl_Estatus_Pago" runat="server" Text="Estatus"></asp:Label>
                                        </td>
                                        <td style=" width:32%">
                                            <asp:DropDownList ID="Cmb_Estatus" runat="server" CssClass="Estatus" width="58%" >
                                            <asp:ListItem Value="PAGADO">PAGADO</asp:ListItem>
                                            <asp:ListItem Value="CANCELADO">CANCELADO</asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>   
                                    <tr style="display:none;" id="Tr_Estatus" >
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_Motivo_Cancelacion" runat="server" Text="*Motivo Cancelación" ></asp:Label>
                                        </td>
                                         <td colspan="3" style=" width:60%">
                                         <asp:TextBox ID="Txt_Motivo_Cancelacion" runat="server"  Width="82%" MaxLength ="250"  ></asp:TextBox>
                                            </td>
                                    </tr>
                                    
                                     <tr>
                                        <td style=" width:25%">
                                            &nbsp; &nbsp;
                                            <asp:Label ID="Lbl_banco" runat="server" Text="Banco"></asp:Label>
                                        </td>
                                        <td style=" width:30%">
                                            <asp:DropDownList ID="Cmb_Banco" runat="server" Width="92%" 
                                                AutoPostBack="true" OnSelectedIndexChanged="Cmb_Banco_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                        <td style=" width:10%;display:none" id="Td_Cuenta">Cuenta
                                        </td>
                                        <td style=" width:35%;display:none" id="Td_Numero_Cuenta">
                                            <asp:TextBox ID="Txt_Cuenta" runat="server" ReadOnly="true" Width="56%"></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp; &nbsp; 
                                            <asp:Label ID="Lbl_Cuenta_Banco" runat="server" Text="Cuenta"></asp:Label>
                                        </td>
                                         <td colspan="3" style="width:75%">
                                            <asp:DropDownList ID="Cmb_Cuenta_Banco" runat="server" CssClass="Estatus" width="82%" 
                                                AutoPostBack="true" OnSelectedIndexChanged="Cmb_Cuenta_Banco_OnSelectedIndexChanged">
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                    <tr style="display:block;" id="Tr_Cheques" >
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_No_Cheque" runat="server" Text="*No_Cheque" ></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="Txt_No_Cheque" runat="server"  Width="90%" MaxLength ="20"  ReadOnly="true"></asp:TextBox>
                                            <cc1:FilteredTextBoxExtender  ID="FTE_Txt_Cheque" runat="server" FilterType ="Numbers" TargetControlID="Txt_No_Cheque" ></cc1:FilteredTextBoxExtender>
                                        </td>
                                    </tr>
                                    <tr style="display:none;" id="Tr_Referencia" >
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_Referencia" runat="server" Text="Referencia" ></asp:Label>
                                        </td>
                                        <td colspan="3" style=" width:60%">
                                            <asp:TextBox ID="Txt_Referencia_Pago" runat="server"  Width="82%" MaxLength ="20"  ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_comentario" runat="server" Text="Comentario" ></asp:Label>
                                        </td>
                                        <td colspan="3" style=" width:60%">
                                            <asp:TextBox ID="Txt_Comentario_Pago" runat="server"  Width="82%" MaxLength ="250"  ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp; &nbsp;   
                                            <asp:Label ID="Lbl_Beneficiario" runat="server" Text="*Beneficiario_Pago" ></asp:Label>
                                        </td>
                                        <td colspan="3" style=" width:60%">
                                            <asp:TextBox ID="Txt_Beneficiario_Pago" runat="server"  Width="82%" MaxLength ="250"  ReadOnly ="true" ></asp:TextBox>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                        <cc1:ModalPopupExtender ID="Mpe_Busqueda" runat="server" BackgroundCssClass="popUpStyle"  BehaviorID="Contenedor"
                                            PopupControlID="Pnl_Busqueda_Contenedor" TargetControlID="Btn_Comodin_Open" PopupDragHandleControlID="Pnl_Busqueda_Cabecera" 
                                            CancelControlID="Btn_Comodin_Close" DropShadow="True" DynamicServicePath="" Enabled="True"/>  
                                            <asp:Button Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Close" runat="server" Text="" />
                                            <asp:Button  Style="background-color: transparent; border-style:none;" ID="Btn_Comodin_Open" runat="server" Text="" OnClientClick="javascript:return false;" />
                                           <asp:HiddenField ID="Txt_No_Reserva" runat="server" />
                                            <asp:HiddenField ID="Txt_Rechazo" runat="server" />
                                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                                            <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Empleado" runat="server" />
                                            <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                                             <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                                        </td>
                                    </tr>
                                </table>
                                  <script type="text/javascript" language="javascript">
                                      Sys.WebForms.PageRequestManager.getInstance().add_beginRequest(iniciar_peticion);
                                      Sys.WebForms.PageRequestManager.getInstance().add_endRequest(fin_peticion);

                                      function fin_peticion() {
                                          $(document).ready(function() {
                                              $("select[id$=Cmb_Tipo_Pago]").change(function() {
                                                  $(".Cmb_Tipo_Pago option:selected").each(function() {
                                                      var Tipo = $(this).val();
                                                      if (Tipo == "CHEQUE") {
                                                          $("#Tr_Cheques").show();
                                                          $("#Tr_Referencia").show();
                                                          $("#Td_Cuenta").hide();
                                                          $("#Td_Numero_Cuenta").hide();
                                                      }
                                                      if (Tipo == "TRANSFERENCIA") {
                                                          $("#Tr_Cheques").hide();
                                                          $("#Tr_Referencia").show();
                                                          $("#Td_Cuenta").show();
                                                          $("#Td_Numero_Cuenta").show();
                                                          document.getElementById("<%=Txt_No_Cheque.ClientID%>").value = ""
                                                      }
                                                  });
                                              }).trigger('change');
                                              $("select[id$=Cmb_Estatus]").change(function() {
                                                  $(".Estatus option:selected").each(function() {
                                                      var Tipo = $(this).val();
                                                      if (Tipo == "PAGADO") {
                                                          $("#Tr_Estatus").hide();
                                                          document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                                                      }
                                                      if (Tipo == "CANCELADO") {
                                                          $("#Tr_Estatus").show();
                                                          document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                                                      }
                                                  });
                                              }).trigger('change');
                                          });
                                          function selec_todo2() {
                                              var y;
                                              y = $('#chkAll').is(':checked');
                                              var $chkBox = $("input:checkbox[id$=Chk_Transferencia]");
                                              if (y == true) {
                                                  $chkBox.attr("checked", true);
                                              } else {
                                                  $chkBox.attr("checked", false);
                                              }
                                              Agregar();
                                          } 
                                          function Agregar() {
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = ""
                                              var a = $("input:checkbox[id$=Chk_Transferencia]")
                                              for (var j = 0; j < a.length; j++) {
                                                  if ($(a[j]).is(':checked') == true) {
                                                      document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value + $(a[j]).parent().attr('class') + "-";
                                                  }
                                              }
                                          }
                                          function Refrescar() {
                                              location.reload();
                                          }
                                          function Cerrar_Modal_Popup_Detalles() {
                                              $find('Mp_Detalles').hide();
                                              $("input[id$=Txt_Fecha_Solicitud_Det]").val('');
                                              $("input[id$=Txt_Monto_Solicitud_Det]").val('');
                                              $("input[id$=Txt_No_Pago_Det]").val('');
                                              $("input[id$=Txt_No_Reserva_Det]").val('');
                                              return false;
                                          }
                                          function Cerrar_Modal_Popup_Proveedor() {
                                              $find('Mp_Proveedor').hide();
                                              $("input[id$=Txt_Banco_Proveedor]").val('');
                                              $("input[id$=Txt_Clabe]").val('');
                                              $("input[id$=Txt_Numero_Cuenta]").val('');
                                              $("input[id$=Txt_Padron_ID]").val('');
                                              return false;
                                          }
                                          function Mostrar_Tabla(Renglon, Imagen) {
                                              object = document.getElementById(Renglon);
                                              if (object.style.display == "none") {
                                                  object.style.display = "block";
                                                  document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
                                              } else {
                                                  object.style.display = "none";
                                                  document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
                                              }
                                          }
                                          // El popup para el rechazo de la solicitud
                                          function Abrir_Popup2(Control) {
                                              $find('Contenedor').show();
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
                                              document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 1;
                                              $("#Tr_Autorizar").hide();
                                              $("#Tr_Rechazar").show();
                                          }
                                          function Cerrar_Modal_Popup() {
                                              $find('Contenedor').hide();
                                              Limpiar_Ctlr();
                                              return false;
                                          }
                                          function Limpiar_Ctlr() {
                                              var $chkBox = $("input:checkbox[id$=Chk_Rechazar]");
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = "";
                                              document.getElementById("<%=Txt_Rechazo.ClientID%>").value = "";
                                              document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
                                              $chkBox.attr('checked', false);
                                          }
                                      }
                                      function iniciar_peticion() {
                                          $(document).ready(function() {
                                              $("select[id$=Cmb_Tipo_Pago]").change(function() {
                                                  $(".Cmb_Tipo_Pago option:selected").each(function() {
                                                      var Tipo = $(this).val();
                                                      if (Tipo == "CHEQUE") {
                                                          $("#Tr_Cheques").show();
                                                          $("#Tr_Referencia").show();
                                                      }
                                                      if (Tipo == "TRANSFERENCIA") {
                                                          $("#Tr_Cheques").hide();
                                                          $("#Tr_Referencia").show();
                                                          document.getElementById("<%=Txt_No_Cheque.ClientID%>").value = ""
                                                      }
                                                  });
                                              }).trigger('change');
                                              $("select[id$=Cmb_Estatus]").change(function() {
                                                  $(".Estatus option:selected").each(function() {
                                                      var Tipo = $(this).val();
                                                      if (Tipo == "PAGADO") {
                                                          $("#Tr_Estatus").hide();
                                                          document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                                                      }
                                                      if (Tipo == "CANCELADO") {
                                                          $("#Tr_Estatus").show();
                                                          document.getElementById("<%=Txt_Motivo_Cancelacion.ClientID%>").value = ""
                                                      }
                                                  });
                                              }).trigger('change');
                                          });
                                          function selec_todo2() {
                                              var y;
                                              y = $('#chkAll').is(':checked');
                                              var $chkBox = $("input:checkbox[id$=Chk_Transferencia]");
                                              if (y == true) {
                                                  $chkBox.attr("checked", true);
                                              } else {
                                                  $chkBox.attr("checked", false);
                                              }
                                              Agregar();
                                          }
                                          function Agregar() {
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = ""
                                              var a = $("input:checkbox[id$=Chk_Transferencia]")
                                              for (var j = 0; j < a.length; j++) {
                                                  if ($(a[j]).is(':checked') == true) {
                                                      document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value + $(a[j]).parent().attr('class') + "-";
                                                  }
                                              }
                                          }
                                          function Refrescar() {
                                              location.reload();
                                          }
                                          function Cerrar_Modal_Popup_Detalles() {
                                              $find('Mp_Detalles').hide();
                                              $("input[id$=Txt_Fecha_Solicitud_Det]").val('');
                                              $("input[id$=Txt_Monto_Solicitud_Det]").val('');
                                              $("input[id$=Txt_No_Pago_Det]").val('');
                                              $("input[id$=Txt_No_Reserva_Det]").val('');
                                              return false;
                                          }
                                          function Cerrar_Modal_Popup_Proveedor() {
                                              $find('Mp_Proveedor').hide();
                                              $("input[id$=Txt_Banco_Proveedor]").val('');
                                              $("input[id$=Txt_Clabe]").val('');
                                              $("input[id$=Txt_Numero_Cuenta]").val('');
                                              $("input[id$=Txt_Padron_ID]").val('');
                                              return false;
                                          }
                                          function Mostrar_Tabla(Renglon, Imagen) {
                                              object = document.getElementById(Renglon);
                                              if (object.style.display == "none") {
                                                  object.style.display = "block";
                                                  document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
                                              } else {
                                                  object.style.display = "none";
                                                  document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
                                              }
                                          }
                                          // El popup para el rechazo de la solicitud
                                          function Abrir_Popup2(Control) {
                                              $find('Contenedor').show();
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = $(Control).parent().attr('class');
                                              document.getElementById("<%=Txt_Rechazo.ClientID%>").value = 1;
                                              $("#Tr_Autorizar").hide();
                                              $("#Tr_Rechazar").show();
                                          }
                                          function Cerrar_Modal_Popup() {
                                              $find('Contenedor').hide();
                                              Limpiar_Ctlr();
                                              return false;
                                          }
                                          function Limpiar_Ctlr() {
                                              var $chkBox = $("input:checkbox[id$=Chk_Rechazar]");
                                              document.getElementById("<%=Txt_No_Solicitud_Autorizar.ClientID%>").value = "";
                                              document.getElementById("<%=Txt_Rechazo.ClientID%>").value = "";
                                              document.getElementById("<%=Txt_Comentario.ClientID%>").value = "";
                                              $chkBox.attr('checked', false);
                                          }
                                      }
                                </script>
                            </asp:Panel>
		                </div>
              </div>
         </ContentTemplate>
         <Triggers>
          <%-- <asp:PostBackTrigger  ControlID="Btn_transferir"/>--%>
         </Triggers>
      </asp:UpdatePanel>
      <asp:Panel ID="Pnl_Busqueda_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">                         
                    <asp:Panel ID="Pnl_Busqueda_Cabecera" runat="server" 
                        style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                        <table width="99%">
                            <tr style="display:none;" id="Tr_Rechazar">
                                <td style="color:Black;font-size:12;font-weight:bold;">
                                   <asp:Image ID="Img_Informatcion_Rechazo" runat="server"  ImageUrl="~/paginas/imagenes/paginas/C_Tips_Md_N.png" />
                                     RECHAZO DE SOLICITUD 
                                </td>
                                <td align="right" style="width:10%;">
                                   <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                        ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup();"/>  
                                </td>
                            </tr>
                        </table>
                    </asp:Panel>                                                                          
                           <div style="color: #5D7B9D">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Comentario" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Progress_Upnl_Comentario" runat="server" AssociatedUpdatePanelID="Upnl_Comentario" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress"><img alt="" src="../Imagenes/paginas/Sias_Roler.gif" /></div></ProgressTemplate></asp:UpdateProgress><table width="100%">
                                                   <tr>
                                                        <td colspan="2">
                                                            <table style="width:80%;">
                                                              <tr>
                                                                <td align="left" >
                                                                  <asp:ImageButton ID="Img_Error_Busqueda" runat="server" ImageUrl="../imagenes/paginas/sias_warning.png" 
                                                                    Width="24px" Height="24px" style="display:none" />
                                                                    <asp:Label ID="Lbl_Error_Busqueda" runat="server" Text="" CssClass="estilo_fuente_mensaje_error" style="display:none"/>
                                                                </td>            
                                                              </tr>         
                                                            </table>  
                                                        </td>
                                                    </tr>     
                                                    <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td style="width:15%">
                                                            *Comentarios
                                                        </td>
                                                        <td >
                                                            <asp:TextBox ID="Txt_Comentario" runat="server" TextMode="MultiLine" Width="99.5%" MaxLength="500" Height="50px" />
                                                           <cc1:FilteredTextBoxExtender ID="Fte_Txt_Comentario" runat="server" FilterType="Custom, LowercaseLetters, Numbers, UppercaseLetters"
                                                                TargetControlID="Txt_Comentario" ValidChars="áéíóúÁÉÍÓÚ., " InvalidChars="&lt;,&gt;,&amp;,',!,"/>
                                                        </td>
                                                    </tr>
                                                   <tr>
                                                        <td colspan="2">
                                                            <hr />
                                                        </td>
                                                    </tr>                                    
                                                    <tr id="Tr_Aceptar" style="display:block;"  >
                                                        <td style="width:100%;text-align:left;" colspan="2">
                                                            <center>
                                                               <asp:Button ID="Btn_Comentar" runat="server" Text="Aceptar" CssClass="button"  
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Comentar_Click"/> 
                                                                <asp:Button ID="Bnt_Cancelar" runat="server" Text="Cancelar" CssClass="button"
                                                                CausesValidation="false"  Width="200px" OnClick="Btn_Cancelar_Click"/> 
                                                            </center>
                                                        </td>
                                                    </tr>
                                                  </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                    </td>
                                </tr>
                             </table>
                           </div>
                    </asp:Panel>
      <%--<asp:Panel ID="Pnl_Detalles_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px"
    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
        <asp:Panel ID="Pnl_Detalles" runat="server" 
            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
            <table width="99%">
                <tr>
                    <td style="color:Black;font-size:12;font-weight:bold;">
                         DETALLES DE LA SOLICITUD 
                    </td>
                    <td align="right" style="width:10%;">
                       <asp:ImageButton ID="ImageButton2" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup_Detalles();"/>  
                    </td>
                </tr>
            </table>
        </asp:Panel>                                                                          
        <center>
            <div style="color: #5D7B9D; width:100%">
             <table width="100%">
                <tr>
                    <td align="left" style="text-align: left;" >                                    
                        <asp:UpdatePanel ID="Upnl_Detalles" runat="server">
                            <ContentTemplate>
                                <asp:UpdateProgress ID="Prg_Detalles" runat="server" AssociatedUpdatePanelID="Upnl_Detalles" DisplayAfter="0">
                                    <ProgressTemplate>
                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                        </div>
                                    </ProgressTemplate>
                                </asp:UpdateProgress>
                                  <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                    <tr style="border:outset 1px Silver;">
                                        <td style="width:25%; font-size:x-small;">No. Pago</td>
                                            <td style="width:25%;"> 
                                                <asp:TextBox ID="Txt_No_Pago_Det" runat="server" Width="80%" ReadOnly="true"  font-size="x-small"/>
                                            </td>
                                            <td style="width:20%; font-size:x-small;">&nbsp;No. Reserva</td>
                                            <td style="width:30%;"> 
                                                <asp:TextBox ID="Txt_No_Reserva_Det" runat="server" Width="98%" ReadOnly="true" style="text-align:right; font-size:x-small;"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:x-small;">Concepto Reserva</td>
                                            <td colspan="3"> 
                                                <asp:TextBox ID="Txt_Concepto_Reserva_Det" runat="server" Width="99%" ReadOnly="true" TextMode="MultiLine" Font-Size="X-Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:x-small;">Beneficiario</td>
                                            <td colspan="3"> 
                                                <asp:TextBox ID="Txt_Beneficiario_Det" runat="server" Width="99%" ReadOnly="true" Font-Size="X-Small"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="font-size:x-small;">Fecha Solicitud Pago</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Fecha_Solicitud_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                            </td>
                                            <td style="font-size:x-small;"> &nbsp;Monto</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_Monto_Solicitud_Det" runat="server" Width="98%" style="text-align:right;" ReadOnly="true" Font-Size="X-Small"/>
                                            </td>
                                        </tr>
                                        <tr id="Tr_Poliza" runat="server">
                                            <td style="font-size:x-small;">No_Poliza</td>
                                            <td> 
                                                <asp:TextBox ID="Txt_No_poliza_Det" runat="server" Width="80%" ReadOnly="true" Font-Size="X-Small"/>
                                            </td>
                                            <td colspan="2">
                                            </td>
                                        </tr>
                                  </table>
                                   <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                    <tr>
                                        <td style="width:100%">
                                            <div style=" max-height:150px; overflow:auto; width:100%; vertical-align:top;">
                                                <table style="width:97%;">
                                                    <tr>
                                                        <td style="width:100%;">
                                                          <asp:GridView ID="Grid_Documentos" runat="server" Width="100%"
                                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                OnRowDataBound="Grid_Documentos_RowDataBound"
                                                                EmptyDataText="No se encuentra ningun documento">
                                                                <Columns>
                                                                        <asp:TemplateField HeaderText="Link">
                                                                            <ItemTemplate>
                                                                                <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink>
                                                                            </ItemTemplate>
                                                                            <HeaderStyle HorizontalAlign ="Left" width ="7%" />
                                                                            <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                        </asp:TemplateField> 
                                                                        <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Center" Width="63%" />
                                                                           <ItemStyle HorizontalAlign="Center" Width="63%" />
                                                                       </asp:BoundField>   
                                                                       <asp:BoundField DataField="MONTO_FACTURA" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Right" Width="30%" />
                                                                           <ItemStyle HorizontalAlign="Right" Width="30%" />
                                                                       </asp:BoundField>
                                                                        <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                       </asp:BoundField>
                                                                       <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                                           <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                           <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                       </asp:BoundField>
                                                                </Columns>                                                    
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                                <PagerStyle CssClass="GridHeader" />
                                                                <HeaderStyle CssClass="tblHead" />
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                            </asp:GridView>
                                                        </td>
                                                    </tr>
                                                </table>
                                             </div> 
                                        </td>
                                    </tr>
                                </table>
                            </ContentTemplate>
                        </asp:UpdatePanel>
                    </td>
                </tr>
                <tr><td>&nbsp;</td></tr>
             </table>
           </div>
        </center>
    </asp:Panel>--%>
      <asp:Panel ID="Pnl_Proveedor_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px"
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
                        <asp:Panel ID="Pnl_Proveedor" runat="server" 
                            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                            <table width="99%">
                                <tr>
                                    <td style="color:Black;font-size:12;font-weight:bold;">
                                         DATOS DEL DEUDOR
                                    </td>
                                    <td align="right" style="width:10%;">
                                       <asp:ImageButton ID="Img_Boton_Cerrar" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup_Proveedor();"/>  
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>                                                                          
                        <center>
                            <div style="color: #5D7B9D; width:100%">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Detalles_Proveedor" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Prg_proveedor" runat="server" AssociatedUpdatePanelID="Upnl_Detalles_Proveedor" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress>
                                                  <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                                        <tr>
                                                            <td colspan="4">
                                                                <asp:Panel ID="Pnl_Proveedor_Datos" runat="server" GroupingText="Datos Banco" Width="100%" >
                                                                    <table width="100%"  border="0" cellspacing="0" class="estilo_fuente">
                                                                        <tr>
                                                                            <td style="font-size:x-small; width:25%;">Banco Beneficiario</td>
                                                                            <td style=" width:25%;">
                                                                                <asp:TextBox ID="Txt_Banco_Proveedor" runat="server" Width="98%" style="text-align:left;" ReadOnly="true" Font-Size="X-Small"/>
                                                                            </td>
                                                                            <td style="font-size:x-small; width:25%;">ID</td>
                                                                            <td style="width:25%;">
                                                                                <asp:TextBox ID="Txt_Padron_ID" runat="server" Width="98%" style="text-align:left;" ReadOnly="true" Font-Size="X-Small"/>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td style="font-size:x-small; width:25%;">
                                                                                Clabe
                                                                            </td>
                                                                            <td style=" width:25%;">
                                                                                <asp:TextBox ID="Txt_Clabe" runat="server" Width="98%" style="text-align:left;" ReadOnly="true" Font-Size="X-Small"/>
                                                                            </td> 
                                                                            <td style="font-size:x-small; width:25%;">
                                                                                N&uacute;mero Cuenta
                                                                            </td>
                                                                            <td style=" width:25%;">
                                                                                <asp:TextBox ID="Txt_Numero_Cuenta" runat="server" Width="98%" style="text-align:left;" ReadOnly="true" Font-Size="X-Small"/>
                                                                            </td> 
                                                                       </tr>
                                                                    </table>
                                                                </asp:Panel>
                                                            </td>
                                                        </tr>
                                                  </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                             </table>
                           </div>
                        </center>
                    </asp:Panel>
</asp:Content>