﻿using System;
using System.IO;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Tipo_Solicitud_Pagos.Negocios;
using JAPAMI.Solicitud_Pagos.Negocio;
using JAPAMI.Dependencias.Negocios;
using JAPAMI.Gastos_Comprobar.Negocio;
using JAPAMI.Manejo_Presupuesto.Datos;
using JAPAMI.Empleados.Negocios;
using JAPAMI.Catalogo_Compras_Proveedores.Negocio;
using AjaxControlToolkit;
using JAPAMI.Parametros_Contabilidad.Negocio;
using JAPAMI.Generar_Reservas.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Solicitud_Pagos : System.Web.UI.Page
{
    #region (Page Load)
        protected void Page_Load(object sender, EventArgs e)
        {

            try
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Solicitudes_Pagos; //Variable que va a contener los datos de la consulta realizada
                String Rol_Grupo="";

                //Valida que existe un usuario logueado en el sistema
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    HttpContext.Current.Session.Remove("Archivo_Documento");
                    ViewState["SortDirection"] = "ASC";
                    DateTime _DateTime = DateTime.Now;
                    int dias = _DateTime.Day;
                    dias = dias * -1;
                    dias++;
                    _DateTime = _DateTime.AddDays(dias);
                     Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol.Rows.Count > 0)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Inicial = _DateTime.ToString("dd/MM/yyyy").ToUpper();
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Final = DateTime.Now.ToString("dd/MM/yyyy").ToUpper();
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_Grupo = Rol_Grupo;
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Recurso = "RECURSO ASIGNADO";
                            Dt_Solicitudes_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consultar_Solicitud_Pago(); //Consulta las solicitudes de pagos que coinciden con los parámetros seleccionados por el usuario
                            Session["Consulta_Solicitud_Pagos"] = Dt_Solicitudes_Pagos;
                            Llena_Grid_Solicitudes_Pagos(); //Agrega las Solicitudes de Pagos obtenidas de la consulta anterior
                        }
                        else
                        {
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Inicial = _DateTime.ToString("dd/MM/yyyy").ToUpper();
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Final = DateTime.Now.ToString("dd/MM/yyyy").ToUpper();
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                            Dt_Solicitudes_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consultar_Solicitud_Pago(); //Consulta las solicitudes de pagos que coinciden con los parámetros seleccionados por el usuario
                            Session["Consulta_Solicitud_Pagos"] = Dt_Solicitudes_Pagos;
                            Llena_Grid_Solicitudes_Pagos(); //Agrega las Solicitudes de Pagos obtenidas de la consulta anterior
                        }
                    }
                    else
                    {
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Inicial = _DateTime.ToString("dd/MM/yyyy").ToUpper();
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Final = DateTime.Now.ToString("dd/MM/yyyy").ToUpper();
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        Dt_Solicitudes_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consultar_Solicitud_Pago(); //Consulta las solicitudes de pagos que coinciden con los parámetros seleccionados por el usuario
                        Session["Consulta_Solicitud_Pagos"] = Dt_Solicitudes_Pagos;
                        Llena_Grid_Solicitudes_Pagos(); //Agrega las Solicitudes de Pagos obtenidas de la consulta anterior
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda
            ///               realizar diferentes operaciones
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                try
                {
                    Cls_Cat_Con_Parametros_Negocio  consulta_parametros= new Cls_Cat_Con_Parametros_Negocio();
                    DataTable Dt_Parametros= new DataTable();
                    Dt_Parametros = consulta_parametros.Consulta_Datos_Parametros();
                    if(Dt_Parametros.Rows.Count>0){
                        Txt_iva_parametro.Value =Dt_Parametros.Rows[0]["IVA"].ToString().Trim();
                        Txt_ISR_parametro.Value = Dt_Parametros.Rows[0]["ISR"].ToString().Trim();
                        Txt_Reten_Cedular_parametro.Value = Dt_Parametros.Rows[0]["CEDULAR"].ToString().Trim();
                        Txt_reten_iva_parametro.Value = Dt_Parametros.Rows[0]["RETENCION_IVA"].ToString().Trim();
                    }else{
                        Txt_iva_parametro.Value = "0";
                        Txt_ISR_parametro.Value = "0";
                        Txt_Reten_Cedular_parametro.Value = "0";
                        Txt_reten_iva_parametro.Value = "0";
                    }
                    Txt_IEPS.Text = "0";
                    Txt_ISH.Text = "0";
                    Consultar_Tipos_Solicitud();    //Consulta todos los tipos de solicitud que esten dados de alta y que se encuentren activos
                    Consulta_Reservas();            //Consulta las reservas
                    Consulta_Dependencia();         //Consulta las dependencias que fueron dadas de alta con anterioridad
                    Limpia_Controles();             //Limpia los controles del forma
                    Habilitar_Controles("Inicial"); //Inicializa todos los controles
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString());
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;
                    Cmb_Reserva_Pago.SelectedIndex = -1;
                    Cmb_Busqueda_Dependencia.SelectedIndex = -1;
                    Cmb_Busqueda_Estatus_Solicitud_Pago.SelectedIndex = -1;
                    Cmb_Busqueda_Tipo_Solicitud.SelectedIndex = -1;
                    Cmb_Operacion.SelectedIndex =0;
                    Txt_Busqueda_Fecha_Fin.Text = "";
                    Txt_Busqueda_Fecha_Inicio.Text = "";
                    Txt_Busqueda_No_Reserva.Text = "";
                    Txt_Busqueda_No_Solicitud_Pago.Text = "";
                    Txt_Concepto_Reserva.Text = "";
                    Txt_Concepto_Solicitud_Pago.Text = "";
                    Txt_Estatus_solicitud_Pago.Text = "";
                    Txt_Fecha_Factura_Solicitud_Pago.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
                    Txt_Fecha_Solicitud_Pago.Text = "";
                    Txt_Monto_Solicitud_Pago.Text = "";
                    Txt_No_Factura_Solicitud_Pago.Text = "";
                    Txt_No_Solicitud_Pago.Text = "";
                    Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
                    Txt_Saldo_Reserva.Text = "";
                    Txt_No_Reserva_Anterior.Value = "";
                    Txt_Total_anterior.Value="";
                    Txt_Monto_Solicitud_Anterior.Value = "";
                    Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
                    Txt_ID_Proveedor.Value = "";
                    Txt_ID_Empleado.Value = "";
                    Txt_Total.Text = "0";
                    Txt_IEPS.Text = "0";
                    Txt_ISH.Text = "0";
                    Txt_Subtotal.Text="0";
                    Txt_Ruta.Text = "";
                    Txt_Nombre_Archivo.Text = "";
                    Txt_IVA.Text = "0";
                    Txt_ISR.Text = "0";
                    Txt_CURP.Text = "0";
                    Txt_Nom_Proveedor.Text = "";
                    Txt_Reten_Cedular.Text = "0";
                    Txt_Reten_IVA.Text = "0";
                    Txt_RFC.Text = "";
                   if (Session["Dt_Documentos"] != null)
                    {
                        Session.Remove("Dt_Documentos");
                    }
                    Grid_Documentos.DataSource = null;
                    Grid_Documentos.DataBind();
                    Grid_Partidas.DataSource = null;
                    Grid_Partidas.DataBind();
                    Grid_Solicitud_Pagos.DataSource = null;
                    Grid_Solicitud_Pagos.DataBind();

                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString());
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpia_Datos_Reserva
            /// DESCRIPCION : Limpia los controles que pertenecen a la reserva
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Datos_Reserva()
            {
                Txt_Saldo_Reserva.Text = "";
                Txt_Concepto_Reserva.Text = "";
                Grid_Partidas.DataSource = null;
                Grid_Partidas.DataBind();
                Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;
                Txt_Nombre_Proveedor_Solicitud_Pago.Text = "";
                Txt_ID_Proveedor.Value = "";
                Txt_ID_Empleado.Value = "";
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Habilitar_Controles
            /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
            ///                para a siguiente operación
            /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
            ///                           si es una alta, modificacion
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Habilitar_Controles(String Operacion)
            {
                Boolean Habilitado; ///Indica si el control de la forma va hacer habilitado para utilización del usuario
                try
                {
                    Habilitado = false;
                    switch (Operacion)
                    {
                        case "Inicial":
                            Habilitado = false;
                            Btn_Nuevo.ToolTip = "Nuevo";
                            Btn_Modificar.ToolTip = "Modificar";
                            Btn_Salir.ToolTip = "Salir";
                            Btn_Nuevo.Visible = true;
                            Btn_Imprimir.Visible = false;
                            Btn_Modificar.Visible = true;
                            Txt_No_Reserva.Visible = false;
                            //Btn_Eliminar.Visible = true;
                            Btn_Nuevo.CausesValidation = false;
                            Btn_Modificar.CausesValidation = false;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_nuevo.png";
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_modificar.png";
                            Cmb_Reserva_Pago.Enabled = false;
                            Cmb_Reserva_Pago.Visible = true;
                            Pnl_Datos_Generales_Solicitud_Pagos.Visible = true;
                            //Configuracion_Acceso("Frm_Ope_Con_Solicitud_Pagos.aspx");
                            break;

                        case "Nuevo":
                            Habilitado = true;
                            Btn_Nuevo.ToolTip = "Dar de Alta";
                            Btn_Modificar.ToolTip = "Modificar";
                            Btn_Salir.ToolTip = "Cancelar";
                            Txt_No_Reserva.Visible = false;
                            Btn_Nuevo.Visible = true;
                            Btn_Modificar.Visible = false;
                            Btn_Imprimir.Visible = false;
                            //Btn_Eliminar.Visible = false;
                            Btn_Nuevo.CausesValidation = true;
                            Btn_Modificar.CausesValidation = true;
                            Cmb_Reserva_Pago.Enabled = true;
                            Cmb_Reserva_Pago.Visible = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            Btn_Nuevo.ImageUrl = "~/paginas/imagenes/paginas/icono_guardar.png";
                            Pnl_Datos_Generales_Solicitud_Pagos.Visible = false;
                            break;

                        case "Modificar":
                            Cmb_Reserva_Pago.Visible = false;
                            Cmb_Reserva_Pago.Enabled = false;
                            Habilitado = true;
                            Btn_Nuevo.ToolTip = "Nuevo";
                            Btn_Modificar.ToolTip = "Actualizar";
                            Btn_Salir.ToolTip = "Cancelar";
                            Btn_Nuevo.Visible = false;
                            Btn_Modificar.Visible = true;
                            Btn_Imprimir.Visible = false;
                            Txt_No_Reserva.Visible = true;
                            //Btn_Eliminar.Visible = false;
                            Btn_Nuevo.CausesValidation = true;
                            Btn_Modificar.CausesValidation = true;
                            Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_cancelar.png";
                            Btn_Modificar.ImageUrl = "~/paginas/imagenes/paginas/icono_actualizar.png";
                            Grid_Documentos.Columns[18].Visible = true;
                            Pnl_Datos_Generales_Solicitud_Pagos.Visible = true;
                            break;
                    }
                    Cmb_Tipo_Solicitud_Pago.Enabled = false;
                    //Cmb_Proveedor_Solicitud_Pago.Enabled = Habilitado;
                    Txt_No_Factura_Solicitud_Pago.Enabled = Habilitado;
                    Txt_Fecha_Factura_Solicitud_Pago.Enabled = false;
                    Txt_Monto_Solicitud_Pago.Enabled = Habilitado;
                    Txt_Nombre_Proveedor_Solicitud_Pago.Enabled = Habilitado;
                    Txt_Concepto_Solicitud_Pago.Enabled = Habilitado;
                    Btn_Fecha_Factura_Solicitud_Pago.Enabled = Habilitado;
                    //Btn_Buscar_Proveedor_Solicitud_Pagos.Enabled = Habilitado;
                    Cmb_Partida.Enabled = Habilitado;
                    Btn_Agregar_Documento.Enabled = Habilitado;
                    Btn_Mostrar_Popup_Busqueda.Enabled = !Habilitado;
                    Grid_Solicitud_Pagos.Enabled = !Habilitado;
                    Lbl_Mensaje_Error.Visible = false;
                    Img_Error.Visible = false;
                    Btn_Agregar_Documento.Enabled = Habilitado;
                    Txt_IVA.Enabled = Habilitado;
                    Txt_ISR.Enabled = Habilitado;
                    Txt_CURP.Enabled = Habilitado;
                    Txt_Nom_Proveedor.Enabled = Habilitado;
                    Txt_Reten_Cedular.Enabled = Habilitado;
                    Txt_ISH.Enabled = Habilitado;
                    Txt_IEPS.Enabled = Habilitado;
                    Txt_Reten_IVA.Enabled = Habilitado;
                    Txt_RFC.Enabled = Habilitado;
                    Cmb_Operacion.Enabled = Habilitado;
                    Asy_Cargar_Archivo.Enabled = Habilitado;
                    Txt_Subtotal_factura.Enabled = Habilitado;
                   
                }
                catch (Exception ex)
                {
                    throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
                }
            }            
        #endregion
        #region (Control Acceso Pagina)
            ///******************************************************************************
            /// NOMBRE: Configuracion_Acceso
            /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
            /// PARÁMETROS  :
            /// USUARIO CREÓ: Juan Alberto Hernández Negrete.
            /// FECHA CREÓ  : 23/Mayo/2011 10:43 a.m.
            /// USUARIO MODIFICO  :
            /// FECHA MODIFICO    :
            /// CAUSA MODIFICACIÓN:
            ///******************************************************************************
            protected void Configuracion_Acceso(String URL_Pagina)
            {
                List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
                DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

                try
                {
                    //Agregamos los botones a la lista de botones de la página.
                    Botones.Add(Btn_Nuevo);
                    Botones.Add(Btn_Modificar);
                    //Botones.Add(Btn_Eliminar);

                    if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
                    {
                        if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                        {
                            //Consultamos el menu de la página.
                            Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                            if (Dr_Menus.Length > 0)
                            {
                                //Validamos que el menu consultado corresponda a la página a validar.
                                if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                                {
                                    Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                                }
                                else
                                {
                                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                                }
                            }
                            else
                            {
                                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                            }
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: IsNumeric
            /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
            /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
            /// CREO        : Juan Alberto Hernandez Negrete
            /// FECHA_CREO  : 29/Noviembre/2010
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Es_Numero(String Cadena)
            {
                Boolean Resultado = true;
                Char[] Array = Cadena.ToCharArray();
                try
                {
                    for (int index = 0; index < Array.Length; index++)
                    {
                        if (!Char.IsDigit(Array[index])) return false;
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
                }
                return Resultado;
            }
        #endregion
        #region (Método Consulta)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consultar_Tipos_Solicitud
            /// DESCRIPCION : Llena el Cmb_Tipo_Solicitud_Pago con los tipos de solicitud de pago.
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consultar_Tipos_Solicitud()
            {
                Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio Rs_Cat_Con_Tipo_Solicitud_Pagos = new Cls_Cat_Con_Tipo_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Tipo_Solicitud; //Variable a obtener los datos de la consulta
                try
                {   

                    Dt_Tipo_Solicitud = Rs_Cat_Con_Tipo_Solicitud_Pagos.Consulta_Tipo_Solicitud_Pagos_Combo(); //Consulta los tipos de solicitud que fueron dados de alta en la base de datos

                    Cmb_Tipo_Solicitud_Pago.DataSource = Dt_Tipo_Solicitud;
                    Cmb_Tipo_Solicitud_Pago.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
                    Cmb_Tipo_Solicitud_Pago.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                    Cmb_Tipo_Solicitud_Pago.DataBind();
                    Cmb_Tipo_Solicitud_Pago.Items.Insert(0, new ListItem("<- SELECCIONAR ->", ""));
                    Cmb_Tipo_Solicitud_Pago.SelectedIndex = -1;

                    Cmb_Busqueda_Tipo_Solicitud.DataSource = Dt_Tipo_Solicitud;
                    Cmb_Busqueda_Tipo_Solicitud.DataTextField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Descripcion;
                    Cmb_Busqueda_Tipo_Solicitud.DataValueField = Cat_Con_Tipo_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID;
                    Cmb_Busqueda_Tipo_Solicitud.DataBind();
                    Cmb_Busqueda_Tipo_Solicitud.Items.Insert(0, new ListItem("<- SELECCIONAR ->", ""));
                    Cmb_Busqueda_Tipo_Solicitud.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    throw new Exception("Consultar_Tipos_Solicitud " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Dependencia
            /// DESCRIPCION : Consulta las dependecias que fueron dados de alta con anterioridad
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 19-Noviembre-2011s
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Dependencia()
            {
                Cls_Cat_Dependencias_Negocio Rs_Cat_Dependencias = new Cls_Cat_Dependencias_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Dependecias; //Variable a obtener los datos de la consulta
                try
                {
                    String Rol_Grupo = "";Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Busqueda_Dependencia, Dt_Dependencias, 1, 0);
                    Cmb_Busqueda_Dependencia.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                    //Verificar si su rol es jefe de dependencia, admin de modulo o admin de sistema
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol != null)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            DataTable Dt_URs = Cls_Util.Consultar_URS_Por_Grupo(Rol_Grupo);
                            if (Dt_URs.Rows.Count > 1)
                            {
                                Cmb_Busqueda_Dependencia.Items.Clear();
                                //Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Busqueda_Dependencia, Dt_URs, 1, 0);
                                Cmb_Busqueda_Dependencia.DataTextField = Dt_URs.Columns[1].ColumnName;
                                Cmb_Busqueda_Dependencia.DataValueField = Dt_URs.Columns[0].ColumnName;
                                Cmb_Busqueda_Dependencia.DataBind();
                                Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem(HttpUtility.HtmlDecode("<SELECCIONAR>"), "0"));
                                Cmb_Busqueda_Dependencia.SelectedValue = Cls_Sessiones.Dependencia_ID_Empleado;
                            }
                        }
                    }
                    //Dt_Dependecias = Rs_Cat_Dependencias.Consulta_Dependencias(); //Consulta las dependencias que fueron dadas de alta con anterioridad
                    //Cmb_Busqueda_Dependencia.DataSource = Dt_Dependecias;
                    //Cmb_Busqueda_Dependencia.DataTextField = Cat_Dependencias.Campo_Nombre;
                    //Cmb_Busqueda_Dependencia.DataValueField = Cat_Dependencias.Campo_Dependencia_ID;
                    //Cmb_Busqueda_Dependencia.DataBind();
                    //Cmb_Busqueda_Dependencia.Items.Insert(0, new ListItem("<- Seleccione ->",""));
                    //Cmb_Busqueda_Dependencia.SelectedIndex = -1;
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Dependencia " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Reservas
            /// DESCRIPCION : Llena el Cmb_Reserva_Pago con todas las reservas que tiene todavia
            ///               saldo.
            /// PARAMETROS  : 
            /// CREO        : Yazmisn Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Reservas()
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexi{on hacia la capa de negocios
                DataTable Dt_Reservas = new DataTable(); //Variable a obtener los datos de la consulta
                String Rol_Grupo = "";
                DataTable Dt_Aux = new DataTable(); //tabla auxiliar para la consulta
                int Cont_Elementos = 0; //variable para el contador
                DataRow Renglon; //renglon para el llenado de la tabla
                Boolean Incluir_Reserva = false; //Variable que indica si se tiene que incluir la reserva

                try
                {
                    Session.Remove("Consulta_Reservas");
                    Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                    Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                    DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                    Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                    DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                    if (Dt_Grupo_Rol.Rows.Count > 0)
                    {
                        String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                        if (Grupo_Rol == "00003")
                        {
                            Dt_Aux = Cls_Util.Consultar_Reservas_Por_Grupo(Rol_Grupo);
                        }
                        else
                        {
                            Rs_Consulta_Ope_PSP_Reservas.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                            Dt_Aux = Rs_Consulta_Ope_PSP_Reservas.Consulta_Reservas(); //Consulta todas las reservas que aun tenga saldo
                        }
                    }
                    else
                    {
                        Rs_Consulta_Ope_PSP_Reservas.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        Dt_Aux = Rs_Consulta_Ope_PSP_Reservas.Consulta_Reservas(); //Consulta todas las reservas que aun tenga saldo
                    }

                    //Clonar la tabla
                    Dt_Reservas = Dt_Aux.Clone();

                    //Ciclo para el barrido de la tabla
                    for (Cont_Elementos = 0; Cont_Elementos < Dt_Aux.Rows.Count; Cont_Elementos++)
                    {
                        //Instanciar renglon
                        Renglon = Dt_Aux.Rows[Cont_Elementos];

                        //Verificar si se tiene que incluir el renglon
                        Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = Convert.ToDouble(Renglon["NO_RESERVA"]);
                        Incluir_Reserva = Rs_Consulta_Ope_PSP_Reservas.Incluir_Reserva();
                        if (Incluir_Reserva == true)
                        {
                            Dt_Reservas.ImportRow(Renglon);
                        }
                    }

                    //Llenar el combo de las reservas
                    Cmb_Reserva_Pago.DataSource = new DataTable();
                    Cmb_Reserva_Pago.DataBind();
                    Cmb_Reserva_Pago.DataSource = Dt_Reservas;
                    Session["Consulta_Reservas"] = Dt_Reservas;
                    Cmb_Reserva_Pago.DataTextField = "Reserva";
                    Cmb_Reserva_Pago.DataValueField = Ope_Psp_Reservas.Campo_No_Reserva;
                    Cmb_Reserva_Pago.DataBind();
                    Cmb_Reserva_Pago.Items.Insert(0, new ListItem("<- Seleccione ->", ""));
                    Cmb_Reserva_Pago.SelectedIndex = -1;

                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Reservas " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Cargar_Combo_Partida
            /// DESCRIPCION : Carga la informacion de la consulta en el grid
            /// PARAMETROS  : 
            /// CREO        : Hugo Enrique Ramírez Aguilera
            /// FECHA_CREO  : 20/Enero/2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Cargar_Combo_Partida()
            {
                Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
                DataTable Dt_Consulta = new DataTable(); //Variable que obtendra los datos de la consulta 
                String No_Reserva = "";
                try
                {
                    No_Reserva = Cmb_Reserva_Pago.SelectedValue;
                    Consulta.P_No_Reserva = No_Reserva;
                    //  realizar consulta
                    Dt_Consulta = Consulta.Consultar_Reserva_Patida();
                    Cmb_Partida.Items.Clear();
                    Cmb_Partida.DataSource = Dt_Consulta;
                    Cmb_Partida.DataValueField = "Codigo";
                    Cmb_Partida.DataTextField = "Codigo"+"Nombre_Partida";
                    Cmb_Partida.DataBind();
                    Cmb_Partida.Items.Insert(0, "< SELECCIONE PARTIDA >");
                    Cmb_Partida.SelectedIndex = 0;
                }
                catch (Exception ex)
                {
                    throw new Exception("Gastos por Comprobar" + ex.Message.ToString(), ex);
                }
            } 
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Reserva
            /// DESCRIPCION : Consulta todos los datos que corresponden al número de reserva
            ///               que fue seleccionada por el usuario
            /// PARAMETROS  : No_Reserva : Indica el No de Reserva a obtener los datos de la
            ///                             base de datos
            /// CREO        : Yazmisn Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Datos_Reserva(Double No_Reserva)
            {
                Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
                Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
                Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
                DataTable Dt_Consulta = new DataTable(); //Variable que obtendra los datos de la consulta 
                DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
                DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Datos_Reserva; //Variable a obtener los datos de la consulta
                Txt_Monto_Solicitud_Anterior.Value = "0";
                try
                {
                    Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
                    Dt_Datos_Reserva = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva(); //Consulta todos los datos correspondientes a la reserva que el usuario selecciono
                    foreach (DataRow Registro in Dt_Datos_Reserva.Rows)
                    {
                        Txt_Monto_Solicitud_Anterior.Value = Convert.ToString(Convert.ToDouble(Txt_Monto_Solicitud_Anterior.Value) + Convert.ToDouble(Registro["saldo"].ToString()));
                        Txt_Concepto_Solicitud_Pago.Text= Registro["Reservado"].ToString();
                        Txt_Concepto_Reserva.Text = Registro["Reservado"].ToString();
                        Txt_Saldo_Reserva.Text = Registro["Saldo_Total"].ToString();
                        if (!String.IsNullOrEmpty(Registro["Proveedor_ID"].ToString())) {
                            Txt_ID_Proveedor.Value = Registro["Proveedor_ID"].ToString();
                            Consulta_PRO.P_Proveedor_ID = Registro["Proveedor_ID"].ToString();
                            Dt_Consulta_PRO = Consulta_PRO.Consulta_Datos_Proveedores();
                            if (Dt_Consulta_PRO.Rows.Count>0)
                            {
                                Txt_Nom_Proveedor.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString();
                                Txt_RFC.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString();
                                Txt_CURP.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_CURP].ToString();
                            }

                            //Verificar si la cadena empieza con P-
                            if (Registro[Ope_Psp_Reservas.Campo_Beneficiario].ToString().StartsWith("P-") == true)
                            {
                                Txt_Nombre_Proveedor_Solicitud_Pago.Text = Registro[Ope_Psp_Reservas.Campo_Beneficiario].ToString().Substring(2);
                            }
                            else
                            {
                                Txt_Nombre_Proveedor_Solicitud_Pago.Text = Registro[Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                            }

                        }
                        if (!String.IsNullOrEmpty(Registro["Empleado_ID"].ToString()))
                        {
                            Txt_ID_Empleado.Value = Registro["Empleado_ID"].ToString();
                            Consulta_EMP.P_Empleado_ID = Registro["Empleado_ID"].ToString();
                            Dt_Consulta_EMP = Consulta_EMP.Consulta_Datos_Empleado();
                            if (Dt_Consulta_EMP.Rows.Count > 0)
                            {
                                Txt_Nom_Proveedor.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Nombre].ToString();
                                Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text +"  "+ Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString();
                                Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text +"  "+ Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString();
                                Txt_RFC.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_RFC].ToString();
                                Txt_CURP.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_CURP].ToString();
                            }
                            Txt_Nombre_Proveedor_Solicitud_Pago.Text = Registro[Ope_Psp_Reservas.Campo_Beneficiario].ToString();
                        }
                        Cmb_Tipo_Solicitud_Pago.SelectedValue = Registro["Tipo_Solicitud_Pago_ID"].ToString();
                        //Txt_Cuenta_Contable_ID.Value = Registro[Cat_Sap_Partidas_Especificas.Campo_Cuenta_Contable_ID].ToString();
                    }
                    if (Dt_Datos_Reserva.Rows.Count > 0)
                    {
                        Grid_Partidas.DataSource = Dt_Datos_Reserva;
                        Grid_Partidas.DataBind();
                    }
                    Consulta.P_No_Reserva = Convert.ToString(No_Reserva);
                    //  realizar consulta
                    Dt_Consulta = Consulta.Consultar_Reserva_Patida();
                    Cmb_Partida.Items.Clear();
                    Cmb_Partida.DataSource = Dt_Consulta;
                    Cmb_Partida.DataValueField = "Codigo";
                    Cmb_Partida.DataTextField = "Nombre_Partida";
                    Cmb_Partida.DataBind();
                    Cmb_Partida.Items.Insert(0, "< SELECCIONE PARTIDA >");
                    Cmb_Partida.SelectedIndex = 0;


                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Datos_Reserva " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Solicitudes_Pagos
            /// DESCRIPCION : Consulta las Solicitudfes de pago que coincidan con los parametros
            ///               proporcionados por el empleado
            /// PARAMETROS  :
            /// CREO        : Yazmisn Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Solicitudes_Pagos()
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Solicitudes_Pagos; //Variable que va a contener los datos de la consulta realizada
                String Rol_Grupo = "";
                try
                {
                    Session.Remove("Consulta_Solicitud_Pagos");
                    if(!String.IsNullOrEmpty(Txt_Busqueda_No_Reserva.Text)) Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_Busqueda_No_Reserva.Text);
                    if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud_Pago.Text)) Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud_Pago.Text));
                    if(Cmb_Busqueda_Estatus_Solicitud_Pago.SelectedIndex > 0) Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Estatus = Cmb_Busqueda_Estatus_Solicitud_Pago.SelectedValue;
                    if(Cmb_Busqueda_Tipo_Solicitud.SelectedIndex > 0) Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Tipo_Solicitud_Pago_ID = Cmb_Busqueda_Tipo_Solicitud.SelectedValue;
                    if (Cmb_Busqueda_Dependencia.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cmb_Busqueda_Dependencia.SelectedValue;
                    }
                    else
                    {
                        Cls_Cat_Dependencias_Negocio Dependencia_Negocio = new Cls_Cat_Dependencias_Negocio();
                        Dependencia_Negocio.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        DataTable Dt_Dependencias = Dependencia_Negocio.Consulta_Dependencias();
                        Rol_Grupo = Dt_Dependencias.Rows[0]["GRUPO_DEPENDENCIA_ID"].ToString();
                        DataTable Dt_Grupo_Rol = Cls_Util.Consultar_Grupo_Rol_ID(Cls_Sessiones.Rol_ID.ToString());
                        if (Dt_Grupo_Rol.Rows.Count > 0)
                        {
                            String Grupo_Rol = Dt_Grupo_Rol.Rows[0][Apl_Cat_Roles.Campo_Grupo_Roles_ID].ToString();
                            if (Grupo_Rol == "00003")
                            {
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_Grupo = Rol_Grupo;
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Recurso = "RECURSO ASIGNADO";
                            }
                            else
                            {
                                Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                            }
                        }
                        else
                        {
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        }
                    }
                    if (!String.IsNullOrEmpty(Txt_Busqueda_Fecha_Inicio.Text) && !String.IsNullOrEmpty(Txt_Busqueda_Fecha_Fin.Text))
                    {
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}",Convert.ToDateTime(Txt_Busqueda_Fecha_Inicio.Text));
                        Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}",Convert.ToDateTime(Txt_Busqueda_Fecha_Fin.Text));
                    }
                    Dt_Solicitudes_Pagos = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consultar_Solicitud_Pago(); //Consulta las solicitudes de pagos que coinciden con los parámetros seleccionados por el usuario
                    Limpia_Controles();
                    Session["Consulta_Solicitud_Pagos"] = Dt_Solicitudes_Pagos;
                    Llena_Grid_Solicitudes_Pagos(); //Agrega las Solicitudes de Pagos obtenidas de la consulta anterior
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Solicitudes_Pagos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Llena_Grid_Solicitudes_Pagos
            /// DESCRIPCION : Llena el grid con las Solicitudes de pago que fueron consultadas
            /// PARAMETROS  : 
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 19-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Llena_Grid_Solicitudes_Pagos()
            {
                DataTable Dt_Solicitudes_Pagos; //Variable que obtendra los datos de la consulta 
                try
                {
                    Grid_Solicitud_Pagos.DataBind();
                    Dt_Solicitudes_Pagos = (DataTable)Session["Consulta_Solicitud_Pagos"];
                    Grid_Solicitud_Pagos.DataSource = Dt_Solicitudes_Pagos;
                    Grid_Solicitud_Pagos.DataBind();
                }
                catch (Exception ex)
                {
                    throw new Exception("Llena_Grid_Solicitudes_Pagos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Datos_Solicitud_Pago
            /// DESCRIPCION : Consulta los datos de la solicitud de pago que fue seleccionada
            ///               por el usuario
            /// PARAMETROS  : 
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 19-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Datos_Solicitud_Pago(String No_Solicitud_Pago)
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Solicitud_Pago; //Variable a contener los datos de la consulta
                DataTable Dt_Reservaciones = new DataTable(); //Obtiene las reservaciones que se tiene consultadas 
                DataTable Dt_Solicitud_Detalles;
                DataTable Dt_Doc = new DataTable();
                try
                {   
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = No_Solicitud_Pago;
                    Dt_Solicitud_Pago = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Datos_Solicitud_Pago(); //Consulta todos los datos de la solicitud que fue seleccionado por el usuario
                    Limpia_Controles();
                    if (Dt_Solicitud_Pago.Rows.Count > 0)
                    {
                        foreach (DataRow Registro in Dt_Solicitud_Pago.Rows)
                        {
                            //Datos generales de la solicitud
                            Txt_No_Solicitud_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Solicitud_Pago].ToString();
                            Txt_Estatus_solicitud_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_Estatus].ToString();
                            Txt_Fecha_Solicitud_Pago.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Solicitud].ToString()));
                            Cmb_Tipo_Solicitud_Pago.SelectedValue = Registro[Ope_Con_Solicitud_Pagos.Campo_Tipo_Solicitud_Pago_ID].ToString();
                            //Datos de la solicitud
                            //Txt_No_Factura_Solicitud_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Factura].ToString();
                            //Txt_Fecha_Factura_Solicitud_Pago.Text = String.Format("{0:dd/MMM/yyyy}", Convert.ToDateTime(Registro[Ope_Con_Solicitud_Pagos.Campo_Fecha_Factura].ToString()));
                            //Txt_Monto_Solicitud_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                            Txt_Total.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                            Txt_Total_anterior.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                            //Txt_Monto_Solicitud_Anterior.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_Monto].ToString();
                            Txt_Concepto_Solicitud_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString();
                            //Datos de la reservación
                            Txt_No_Reserva_Anterior.Value = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString();
                            Dt_Reservaciones = (DataTable)Session["Consulta_Reservas"];
                            Cmb_Reserva_Pago.SelectedIndex = -1;
                            Cmb_Reserva_Pago.Visible = false;
                            Txt_No_Reserva.Visible = true;
                            Txt_No_Reserva.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() + "-" + Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString();
                            if (Dt_Reservaciones.Rows.Count > 0)
                            {
                                foreach (DataRow Reserva in Dt_Reservaciones.Rows)
                                {
                                    if (Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() == Reserva[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString()) Cmb_Reserva_Pago.SelectedValue = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString();
                                }
                                if (Cmb_Reserva_Pago.SelectedIndex <= 0)
                                {

                                    Cmb_Reserva_Pago.Items.Insert(Dt_Reservaciones.Rows.Count + 1, new ListItem(Registro["Reserva"].ToString(), Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString()));
                                    Cmb_Reserva_Pago.SelectedIndex = Dt_Reservaciones.Rows.Count;
                                }
                            }
                            //Txt_Cuenta_Contable_ID_Proveedor.Value = Registro[Cat_Com_Proveedores.Campo_Cuenta_Contable_ID].ToString();
                            //Txt_Cuenta_Contable_ID_Proveedor_Anterior.Value = Registro[Cat_Com_Proveedores.Campo_Cuenta_Contable_ID].ToString();
                            Consulta_Datos_Reserva(Convert.ToDouble(Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString())); //Consulta los datos de la reserva que fue asignada a la solicitud;
                        //se llenan los detalles de la solicitud del pago 
                            //Cmb_Reserva_Pago.Text = Registro[Ope_Con_Solicitud_Pagos.Campo_No_Reserva].ToString() +"-" + Registro[Ope_Con_Solicitud_Pagos.Campo_Concepto].ToString();
                        }
                       
                            Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago= No_Solicitud_Pago;
                            Dt_Solicitud_Detalles = Rs_Consulta_Ope_Con_Solicitud_Pagos.Consulta_Detalles_Solicitud();
                            if (Dt_Solicitud_Detalles.Rows.Count > 0)
                            {
                                foreach (DataRow Fila in Dt_Solicitud_Detalles.Rows)
                                {
                                    if (Session["Dt_Documentos"] == null)
                                    {
                                        Dt_Doc.Columns.Add("No_Documento", typeof(System.String));
                                        Dt_Doc.Columns.Add("Fecha_Documento", typeof(System.DateTime));
                                        Dt_Doc.Columns.Add("Monto", typeof(System.String));
                                        Dt_Doc.Columns.Add("Partida", typeof(System.String));
                                        Dt_Doc.Columns.Add("Partida_Id", typeof(System.String));
                                        Dt_Doc.Columns.Add("Fte_Financiamiento_Id", typeof(System.String));
                                        Dt_Doc.Columns.Add("Proyecto_Programa_Id", typeof(System.String));
                                        Dt_Doc.Columns.Add("Iva", typeof(System.String));
                                        Dt_Doc.Columns.Add("Archivo", typeof(System.String));
                                        Dt_Doc.Columns.Add("Ruta", typeof(System.String));
                                        Dt_Doc.Columns.Add("Nombre_Proveedor_Fact", typeof(System.String));
                                        Dt_Doc.Columns.Add("RFC", typeof(System.String));
                                        Dt_Doc.Columns.Add("CURP", typeof(System.String));
                                        Dt_Doc.Columns.Add("Operacion", typeof(System.String));
                                        Dt_Doc.Columns.Add("Retencion_ISR", typeof(System.Double));
                                        Dt_Doc.Columns.Add("Retencion_IVA", typeof(System.Double));
                                        Dt_Doc.Columns.Add("Retencion_Celula", typeof(System.Double));
                                        Dt_Doc.Columns.Add("IEPS", typeof(System.Double));
                                        Dt_Doc.Columns.Add("ISH", typeof(System.Double));
                                        //Agrega los campos que va a contener el DataTable
                                    }
                                    else
                                    {
                                        Dt_Doc = (DataTable)Session["Dt_Documentos"];
                                        Session.Remove("Dt_Documentos");
                                    }
                                    DataRow row = Dt_Doc.NewRow(); //Crea un nuevo registro a la tabla
                                    //Asigna los valores al nuevo registro creado a la tabla
                                    row["No_Documento"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_No_Factura].ToString();
                                    row["Fecha_Documento"] = String.Format("{0: dd/MM/yyyy}", Convert.ToDateTime(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fecha_Factura].ToString()));
                                    row["Monto"] =Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Monto_Factura].ToString());
                                    row["Partida_ID"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Partida_ID].ToString();
                                    row["Partida"] = Fila["Descripcion"].ToString();
                                    row["Fte_Financiamiento_Id"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Fte_Financimiento_ID].ToString();
                                    row["Proyecto_Programa_Id"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Proyecto_Programa_ID].ToString();
                                    row["Iva"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Iva].ToString();
                                    row["Archivo"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Archivo].ToString();
                                    row["Ruta"] = HttpUtility.HtmlEncode(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Ruta].ToString());
                                    row["Nombre_Proveedor_Fact"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Nombre_Proveedor_Factura].ToString();
                                    row["RFC"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_RFC].ToString();
                                    row["CURP"] = Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_CURP].ToString();
                                    row["Operacion"] =Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Tipo_Operacion].ToString();
                                    row["Retencion_ISR"] = Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISR].ToString());
                                    row["Retencion_IVA"] = Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Retencion_IVA].ToString());
                                    row["Retencion_Celula"] = Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_Cedula].ToString());
                                    row["IEPS"] = Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_IEPS].ToString());
                                    row["ISH"] = Convert.ToDouble(Fila[Ope_Con_Solicitud_Pagos_Detalles.Campo_ISH].ToString());
                                    Dt_Doc.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                                    Dt_Doc.AcceptChanges();
                                    Session["Dt_Documentos"] = Dt_Doc;//Agrega los valores del registro a la sesión                                  
                                }


                                Txt_Total.Text = "0";
                                Txt_Subtotal.Text = "0";
                                Grid_Documentos.Columns[2].Visible = true;
                                Grid_Documentos.Columns[6].Visible = true;
                                Grid_Documentos.Columns[7].Visible = true;
                                Grid_Documentos.Columns[8].Visible = true;
                                Grid_Documentos.Columns[9].Visible = true;
                                Grid_Documentos.Columns[10].Visible = true;
                                Grid_Documentos.Columns[11].Visible = true;
                                Grid_Documentos.Columns[12].Visible = true;
                                Grid_Documentos.Columns[13].Visible = true;
                                Grid_Documentos.Columns[14].Visible = true;
                                Grid_Documentos.Columns[20].Visible = true;
                                Grid_Documentos.Columns[19].Visible = true;
                                Grid_Documentos.Columns[18].Visible = true;
                                Grid_Documentos.DataSource = Dt_Doc; //Agrega los valores de todas las partidas que se tienen al grid
                                Grid_Documentos.DataBind();
                                Grid_Documentos.Columns[2].Visible = false;
                                Grid_Documentos.Columns[6].Visible = false;
                                Grid_Documentos.Columns[7].Visible = false;
                                Grid_Documentos.Columns[8].Visible = false;
                                Grid_Documentos.Columns[9].Visible = false;
                                Grid_Documentos.Columns[10].Visible = false;
                                Grid_Documentos.Columns[11].Visible = false;
                                Grid_Documentos.Columns[12].Visible = false;
                                Grid_Documentos.Columns[13].Visible = false;
                                Grid_Documentos.Columns[14].Visible = false;
                                Grid_Documentos.Columns[19].Visible = false;
                                Grid_Documentos.Columns[20].Visible = false;
                                Grid_Documentos.Columns[18].Visible = false;
                            }
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Datos_Solicitud_Pago " + ex.Message.ToString(), ex);
                }
            }
        #endregion
        #region (Metodos Validacion)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Solicitud_Pagos
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Solicitud_Pagos()
            {
                String Espacios_Blanco;
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                DataTable Dt_Saldo = new DataTable();
                DataTable Dt_Documentos = new DataTable();
                DataTable Dt_Solicitudes = new DataTable();
                String Valido = "true";
                Boolean Documento_anexo = false;
                try
                {
                    Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                    Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";

                    if (Grid_Documentos.Rows.Count == 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Debes ingresar por lo menos un documento que indique el monto a solicitar. <br />";
                        Datos_Validos = false;
                    }
                    else
                    {
                        //Dt_Documentos =(DataTable)Session["Dt_Documentos"];
                        //foreach (DataRow Fila in Dt_Documentos.Rows)
                        //{
                        //    if (Fila["Ruta"].ToString() != "")
                        //    {
                                Documento_anexo = true;
                        //    }
                        //}
                        if(Cmb_Tipo_Solicitud_Pago.SelectedValue !="00001"){
                            if (Documento_anexo == false)
                            {
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Debes ingresar por lo menos un documento anexo a la solicitud de pago para poder continuar. <br />";
                                Datos_Validos = false;
                            }
                        }                       
                        if (Btn_Nuevo.ToolTip != "Nuevo")
                        {
                            //if (Convert.ToDouble(Txt_Saldo_Reserva.Text) < Convert.ToDouble(Txt_Monto_Solicitud_Pago.Text.Replace(",", "").Replace("$", "")))
                            if (Convert.ToDouble(Txt_Monto_Solicitud_Anterior.Value) < Convert.ToDouble(Txt_Total.Text.Replace(",", "").Replace("$", "")))
                            {
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Total a Solicitar no debe ser mayor al Saldo de la Reserva. <br />";
                                Datos_Validos = false;
                            }
                        }
                        else
                        {
                                Double Saldo = 0; //Variable a Contener el saldo final de la reserva
                                Dt_Saldo = Cls_Ope_Psp_Manejo_Presupuesto.Consultar_Reserva(Txt_No_Reserva_Anterior.Value);
                                // se verifica que no tenga solicitudes anteriores
                                Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva_Anterior.Value);
                                Dt_Solicitudes = Rs_Consulta_Ope_PSP_Reservas.Consultar_Solicitud_Pago();
                                if (Dt_Solicitudes.Rows.Count > 0)
                                {
                                    foreach (DataRow Registro in Dt_Solicitudes.Rows)
                                    {
                                        if (Registro["ESTATUS"].ToString() != "CANCELADO" && Registro["ESTATUS"].ToString() != "PENDIENTE")
                                        {
                                            Valido = "false";
                                        }
                                    }
                                }
                                if (Dt_Saldo.Rows.Count > 0 && Valido == "true")
                                {
                                    Saldo = Convert.ToDouble(Dt_Saldo.Rows[0]["IMPORTE_INICIAL"].ToString());
                                }
                                else
                                {
                                    Saldo = Convert.ToDouble(Txt_Saldo_Reserva.Text);
                                }
                                //if (Saldo < Convert.ToDouble(Txt_Monto_Solicitud_Pago.Text.Replace(",", "").Replace("$", "")))
                                if (Txt_Reten_Cedular.Text == "")
                                {
                                    Txt_Reten_Cedular.Text = "0";
                                } if (Txt_ISR.Text == "")
                                {
                                    Txt_ISR.Text = "0";
                                }
                                if (Saldo < (Convert.ToDouble(Txt_Total.Text.Replace(",", "").Replace("$", "")) + Convert.ToDouble(Txt_ISR.Text) + Convert.ToDouble(Txt_Reten_Cedular.Text)))
                                {
                                    Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto a Solicitar no debe ser mayor al Saldo de la Reserva. <br />";
                                    Datos_Validos = false;
                                }

                            //}
                        }

                        
                    }
                    if (String.IsNullOrEmpty(Txt_Concepto_Solicitud_Pago.Text.Trim()))
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Concepto de la Solicitud es un dato requerido por el sistema. <br />";
                        Datos_Validos = false;
                    }
                    if (Cmb_Tipo_Solicitud_Pago.SelectedIndex == 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Tipo de Solicitud es un dato requerido por el sistema. <br />";
                        Datos_Validos = false;
                    }
                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
                }
            }

            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Importe_Partidas
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Importe_Partidas()
            {
                Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_PSP_Reservas = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                String Espacios_Blanco;
                DataTable Dt_Partidas;
                DataTable Dt_Consulta;
                DataTable Dt_Solicitudes = new DataTable() ;
                Double No_Reserva = 0.0;
                Double Importe = 0.0;
                String Valido = "true";
                Double Importe_Tabla = 0.0;
                String Partida_Nombre = "";
                String[] Reserva =new String[3];
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                try
                {
                    Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                    Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if (Cmb_Partida.SelectedIndex <= 0)
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La partida a la cual se le realizara el cargo <br />";
                        Datos_Validos = false;
                    }
                    else
                    {
                        if (Txt_No_Reserva.Visible==true)
                        {
                            No_Reserva = Convert.ToDouble(Txt_No_Reserva.Text.Substring(0, Txt_No_Reserva.Text.IndexOf('-')));
                            Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
                            Dt_Partidas = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva();
                        }
                        else
                        {
                            No_Reserva = Convert.ToDouble(Cmb_Reserva_Pago.SelectedValue);
                            Rs_Consulta_Ope_PSP_Reservas.P_No_Reserva = No_Reserva;
                            Dt_Partidas = Rs_Consulta_Ope_PSP_Reservas.Consulta_Datos_Reserva();
                        }                       

                        Consulta.P_Partida_ID = Cmb_Partida.SelectedValue.Substring(0, 10);
                        Dt_Consulta = Consulta.Consultar_Nombre_Patida();
                        if (Txt_Monto_partida.Value == "" || Txt_Monto_partida.Value == "NaN")
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto no se ha ingresado <br />";
                            Datos_Validos = false;
                        }
                        // se verifica que no tenga solicitudes anteriores
                        Dt_Solicitudes=Rs_Consulta_Ope_PSP_Reservas.Consultar_Solicitud_Pago();
                        if (Dt_Solicitudes.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Solicitudes.Rows)
                            {
                                if (Registro["ESTATUS"].ToString() != "CANCELADO" && Registro["ESTATUS"].ToString() != "PENDIENTE")
                                {
                                    Valido = "false";
                                }
                            }
                        }
                        if (Btn_Modificar.ToolTip == "Actualizar" && Valido=="true")
                        {

                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                Partida_Nombre = (Registro["Nombre"].ToString());
                            }
                            foreach (DataRow Registro in Dt_Partidas.Rows)
                            {
                                if (Partida_Nombre == Registro["NOMBRE_PARTIDA"].ToString())
                                {
                                    Importe = Convert.ToDouble(Registro["IMPORTE_INICIAL"].ToString());
                                    break;
                                }
                            }
                        }
                        else {
                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                Partida_Nombre = (Registro["Nombre"].ToString());
                            }
                            foreach (DataRow Registro in Dt_Partidas.Rows)
                            {
                                if (Partida_Nombre == Registro["NOMBRE_PARTIDA"].ToString())
                                {
                                    Importe = Convert.ToDouble(Registro["SALDO"].ToString());
                                    break;
                                }
                            }
                        }
                    }

                    Dt_Consulta = null;
                    if (Session["Dt_Documentos"] != null)
                    {
                        Dt_Consulta = (DataTable)Session["Dt_Documentos"];
                        if (Dt_Consulta.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta.Rows)
                            {
                                if (Partida_Nombre == Registro["PARTIDA"].ToString())
                                {
                                    Importe_Tabla += (Convert.ToDouble(Registro["Monto"].ToString()) + Convert.ToDouble(Registro["Retencion_ISR"].ToString()) + Convert.ToDouble(Registro["Retencion_Celula"].ToString()));
                                }
                            }
                            Importe_Tabla = Importe - Importe_Tabla;
                            Importe_Tabla = Math.Round(Importe_Tabla, 3);

                            //Verificar que tenga valores
                            if (Txt_Reten_Cedular.Text == "")
                            {
                                Txt_Reten_Cedular.Text = "0";
                            } if (Txt_ISR.Text == "")
                            {
                                Txt_ISR.Text = "0";
                            }
                            if ((Convert.ToDouble(Txt_Monto_partida.Value)+Convert.ToDouble(Txt_Reten_Cedular.Text)+Convert.ToDouble(Txt_ISR.Text)) > Importe_Tabla)
                            {
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Solicitado excede al saldo aprobado. <br />";
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Recuerda que si es honorario el ISR y La Retencion Cedular tambien se toman del monto. <br />";
                                Datos_Validos = false;
                            }
                        }
                        else
                        {
                            if (Txt_Monto_partida.Value == "" || Txt_Monto_partida.Value == "NaN")
                            {
                                Importe_Tabla = 0;

                            }
                            else
                            {
                                if (Txt_Reten_Cedular.Text == "")
                                {
                                    Txt_Reten_Cedular.Text = "0";
                                } if (Txt_ISR.Text == "")
                                {
                                    Txt_ISR.Text = "0";
                                }
                                Importe_Tabla = (Convert.ToDouble(Txt_Monto_partida.Value) + Convert.ToDouble(Txt_Reten_Cedular.Text) + Convert.ToDouble(Txt_ISR.Text));
                            }
                            if (Importe_Tabla > Importe)
                            {
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Solicitado excede al saldo aprobado. <br />";
                                Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Recuerda que si es honorario el ISR y La Retencion Cedular tambien se toman del monto. <br />";
                                Datos_Validos = false;
                            }
                        }
                    }
                    else
                    {
                        if (Txt_Monto_partida.Value == "" || Txt_Monto_partida.Value == "NaN")
                        {
                            Importe_Tabla = 0;

                        }else{
                            if (Txt_Reten_Cedular.Text == "")
                            {
                                Txt_Reten_Cedular.Text = "0";
                            } if (Txt_ISR.Text=="")
                            {
                                Txt_ISR.Text = "0";
                            }
                            Importe_Tabla = (Convert.ToDouble(Txt_Monto_partida.Value) + Convert.ToDouble(Txt_Reten_Cedular.Text) + Convert.ToDouble(Txt_ISR.Text));
                        }
                        if (Importe_Tabla > Importe)
                        {
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El Monto Solicitado excede al saldo aprobado. <br />";
                            Lbl_Mensaje_Error.Text += Espacios_Blanco + " + Recuerda que si es honorario el ISR y La Retencion Cedular tambien se toman del monto. <br />";
                            Datos_Validos = false;
                        }
                    }
                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos.
            /// CREO        : sergio manuel gallardo
            /// FECHA_CREO  : 01- marzo-2012
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar()
            {
                String Espacios_Blanco;
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                try
                {
                    Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                    Espacios_Blanco = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
                    if (Txt_Nom_Proveedor.Text=="")
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El nombre del Proveedor. <br />";
                        Datos_Validos = false;
                    }
                    if (Txt_No_Factura_Solicitud_Pago.Text == "")
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El No. del documento. <br />";
                        Datos_Validos = false;
                    }
                    if (Txt_Fecha_Factura_Solicitud_Pago.Text == "")
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + La Fecha del Documento. <br />";
                        Datos_Validos = false;
                    }
                    if (Txt_Monto_partida.Value == "")
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El monot del Documento. <br />";
                        Datos_Validos = false;
                    }
                    if (Txt_IVA.Text == "")
                    {
                        Lbl_Mensaje_Error.Text += Espacios_Blanco + " + El porcentaje del iva. <br />";
                        Datos_Validos = false;
                    }
                    return Datos_Validos;
                }
                catch (Exception ex)
                {
                    throw new Exception("Validar_Datos_Solicitud_Pagos " + ex.Message.ToString(), ex);
                }
            }
            protected void Btn_Agregar_Documento_Click(object sender, ImageClickEventArgs e)
            {
                Cls_Cat_Empleados_Negocios Consulta_EMP = new Cls_Cat_Empleados_Negocios();
                Cls_Cat_Com_Proveedores_Negocio Consulta_PRO = new Cls_Cat_Com_Proveedores_Negocio();
                DataTable Dt_Consulta_EMP = new DataTable(); //Variable que obtendra los datos de la consulta
                DataTable Dt_Consulta_PRO = new DataTable(); //Variable que obtendra los datos de la consulta
                Cls_Ope_Con_Gastos_Comprobar_Negocio Consulta = new Cls_Ope_Con_Gastos_Comprobar_Negocio();
                DataTable Dt_Consulta = new DataTable();
                DataTable Dt_Documento = new DataTable(); //Obtiene los datos de la póliza que fueron proporcionados por el usuario
                //string Codigo_Programatico = "";
                String Espacios = "";
                String extension = String.Empty;
                int error = 0;
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                String aux = String.Empty; //variable auxiliar

                //Valida que todos los datos requeridos los haya proporcionado el usuario
                if (Validar_Importe_Partidas())
                {
                    Txt_Ruta.Text = "";
                    Txt_Nombre_Archivo.Text = "";
                    extension = "";
                    //extension = Guardar_Documentos_Anexos();
                    //verificar si se guardo un archivo
                    if (HttpContext.Current.Session["Archivo_Documento"] != null)
                    {
                        extension = HttpContext.Current.Session["Archivo_Documento"].ToString().Trim();
                        extension = extension.Substring(extension.LastIndexOf(".") + 1);
                        Txt_Ruta.Text = HttpContext.Current.Session["Archivo_Documento"].ToString().Trim();
                        aux = HttpContext.Current.Session["Archivo_Documento"].ToString().Trim();
                        Txt_Nombre_Archivo.Text = aux.Substring(aux.LastIndexOf("\\") + 1);
                    }
                    if (Validar())
                    {
                        if (Session["Dt_Documentos"] == null)
                        {
                            //Agrega los campos que va a contener el DataTable
                            Dt_Documento.Columns.Add("No_Documento", typeof(System.String));
                            Dt_Documento.Columns.Add("Fecha_Documento", typeof(System.DateTime));
                            Dt_Documento.Columns.Add("Monto", typeof(System.String));
                            Dt_Documento.Columns.Add("Partida", typeof(System.String));
                            Dt_Documento.Columns.Add("Partida_Id", typeof(System.String));
                            Dt_Documento.Columns.Add("Fte_Financiamiento_Id", typeof(System.String));
                            Dt_Documento.Columns.Add("Proyecto_Programa_Id", typeof(System.String));
                            Dt_Documento.Columns.Add("Iva", typeof(System.String));
                            Dt_Documento.Columns.Add("IEPS", typeof(System.Decimal));
                            Dt_Documento.Columns.Add("Archivo", typeof(System.String));
                            Dt_Documento.Columns.Add("Ruta", typeof(System.String));
                            Dt_Documento.Columns.Add("Nombre_Proveedor_Fact", typeof(System.String));
                            Dt_Documento.Columns.Add("RFC", typeof(System.String));
                            Dt_Documento.Columns.Add("CURP", typeof(System.String));
                            Dt_Documento.Columns.Add("Operacion", typeof(System.String));
                            Dt_Documento.Columns.Add("Retencion_ISR", typeof(System.Decimal));
                            Dt_Documento.Columns.Add("Retencion_IVA", typeof(System.Decimal));
                            Dt_Documento.Columns.Add("Retencion_Celula", typeof(System.Decimal));
                            Dt_Documento.Columns.Add("ISH", typeof(System.Decimal));
                        }
                        else
                        {
                            Dt_Documento = (DataTable)Session["Dt_Documentos"];
                            Session.Remove("Dt_Documentos");
                        }                      
                        DataRow row = Dt_Documento.NewRow(); //Crea un nuevo registro a la tabla
                        //Asigna los valores al nuevo registro creado a la tabla
                        row["No_Documento"] = Txt_No_Factura_Solicitud_Pago.Text.Trim();
                        //row["Fecha_Documento"] = String.Format("{0:MM/dd/yyyy}", Convert.ToDateTime(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()));
                        row["Fecha_Documento"] = Convierte_Fecha_DT(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim());
                        row["Monto"] = Txt_Monto_partida.Value;
                        row["Partida_Id"] = Cmb_Partida.SelectedValue.Substring(0,10);
                        row["Fte_Financiamiento_Id"] = Cmb_Partida.SelectedValue.Substring(10, 5);
                        row["Proyecto_Programa_Id"] = Cmb_Partida.SelectedValue.Substring(15,10);
                        row["iva"]= Txt_IVA.Text.Trim();                        
                        row["Nombre_Proveedor_Fact"] = Txt_Nom_Proveedor.Text.Trim();
                        row["RFC"] = Txt_RFC.Text.Trim();
                        row["CURP"] = Txt_CURP.Text.Trim();
                        row["Operacion"] = Cmb_Operacion.SelectedItem.Text.Trim();
                        if (Cmb_Operacion.SelectedItem.Text.Trim() =="OTROS")
                        {
                            row["Retencion_ISR"]  = 0;
                            row["Retencion_IVA"] = 0;
                            row["Retencion_Celula"] = 0;
                            row["IEPS"] = Txt_IEPS.Text.Trim();
                            row["ISH"] = Txt_ISH.Text.Trim();
                        }
                        else
                        {
                            if (Cmb_Operacion.SelectedItem.Text.Trim() == "HON. ASIMILABLE")
                            {
                                row["Retencion_ISR"] = Txt_ISR.Text.Trim();
                                row["Retencion_IVA"] = 0;
                                row["Retencion_Celula"] = 0;
                                row["IEPS"] = 0;
                                row["ISH"] = 0;
                            }
                            else
                            {
                                row["Retencion_ISR"] = Txt_ISR.Text.Trim();
                                row["Retencion_IVA"] = Txt_Reten_IVA.Text.Trim();
                                row["Retencion_Celula"] = Txt_Reten_Cedular.Text.Trim();
                                row["IEPS"] = 0;
                                row["ISH"] = 0;
                            }
                        }
                        Consulta.P_Partida_ID = Cmb_Partida.SelectedValue.Substring(0, 10);
                        Dt_Consulta = Consulta.Consultar_Nombre_Patida();

                        foreach (DataRow Registro in Dt_Consulta.Rows)
                        {
                            row["Partida"] = (Registro["Nombre"].ToString());
                        }
                        if (!String.IsNullOrEmpty(Txt_Nombre_Archivo.Text.Trim()) && !String.IsNullOrEmpty(Txt_Ruta.Text.Trim())){
                            if (extension == "txt" || extension == "doc" || extension == "pdf" || extension == "docx" || extension == "jpg" || extension == "JPG" || extension == "jpeg" || extension == "JPEG" || extension == "png" || extension == "PNG" || extension == "gif" || extension == "GIF")
                            {
                                row["Archivo"] = Txt_Nombre_Archivo.Text;
                                row["Ruta"]= Txt_Ruta.Text;
                                ClearSession_AsyncFileUpload(Asy_Cargar_Archivo.ClientID);
                                Txt_Nombre_Archivo.Text = "";
                                Txt_Ruta.Text = "";
                            }
                            else
                            {
                                System.IO.File.Delete(Txt_Ruta.Text);
                                ClearSession_AsyncFileUpload(Asy_Cargar_Archivo.ClientID);
                                Txt_Ruta.Text = "";
                                Txt_Nombre_Archivo.Text = "";
                            }   
                            }
                        Dt_Documento.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
                        Dt_Documento.AcceptChanges();
                        Txt_Total.Text = "0";
                        Txt_Subtotal.Text = "0";
                        Session["Dt_Documentos"] = Dt_Documento;//Agrega los valores del registro a la sesión
                        Grid_Documentos.Columns[2].Visible = true;
                        Grid_Documentos.Columns[6].Visible = true;
                        Grid_Documentos.Columns[7].Visible = true;
                        Grid_Documentos.Columns[8].Visible = true;
                        Grid_Documentos.Columns[9].Visible = true;
                        Grid_Documentos.Columns[10].Visible = true;
                        Grid_Documentos.Columns[11].Visible = true;
                        Grid_Documentos.Columns[12].Visible = true;
                        Grid_Documentos.Columns[13].Visible = true;
                        Grid_Documentos.Columns[14].Visible = true;
                        Grid_Documentos.Columns[19].Visible = true;
                        Grid_Documentos.Columns[20].Visible = true;
                        Grid_Documentos.DataSource = Dt_Documento; //Agrega los valores de todas las partidas que se tienen al grid
                        Grid_Documentos.DataBind();
                        Grid_Documentos.Columns[2].Visible = false;
                        Grid_Documentos.Columns[6].Visible = false;
                        Grid_Documentos.Columns[7].Visible = false;
                        Grid_Documentos.Columns[8].Visible = false;
                        Grid_Documentos.Columns[9].Visible = false;
                        Grid_Documentos.Columns[10].Visible = false;
                        Grid_Documentos.Columns[11].Visible = false;
                        Grid_Documentos.Columns[12].Visible = false;
                        Grid_Documentos.Columns[13].Visible = false;
                        Grid_Documentos.Columns[14].Visible = false;
                        Grid_Documentos.Columns[19].Visible = false;
                        Grid_Documentos.Columns[20].Visible = false;
                        Txt_No_Factura_Solicitud_Pago.Text = "";
                        Txt_Fecha_Factura_Solicitud_Pago.Text = String.Format("{0:dd/MMM/yyyy}", DateTime.Now);
                        Txt_Monto_Solicitud_Pago.Text = "0";
                        Txt_Monto_partida.Value = "0";
                        Cmb_Partida.SelectedIndex = 0;
                        Txt_IVA.Text = "0";
                        Txt_ISR.Text = "0";
                        Txt_Reten_IVA.Text = "0";
                        Txt_ISH.Text = "0";
                        Txt_IEPS.Text = "0";
                        //Txt_CURP.Text = "";
                        Txt_Subtotal_factura.Text = "0";
                        Txt_Nom_Proveedor.Text = "";
                        Txt_Reten_Cedular.Text = "0";
                        Txt_Reten_IVA.Text = "0";
                        Txt_RFC.Text = "";
                        Txt_IEPS.Text = "0";
                        Txt_ISH.Text = "0";
                        Cmb_Operacion.SelectedIndex = 0;
                        Txt_Total.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Total.Text));
                        Txt_Subtotal.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Subtotal.Text));
                    }

                //Indica al usuario que datos son los que falta por proporcionar para poder agregar la partida a la poliza
                    else
                    {
                        //error = 1;
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br />";
                        if (String.IsNullOrEmpty(Txt_No_Factura_Solicitud_Pago.Text.Trim()))
                        {
                            Lbl_Mensaje_Error.Text += Espacios + " + El Número del Documento <br />";
                        }
                        if (String.IsNullOrEmpty(Txt_Fecha_Factura_Solicitud_Pago.Text.Trim()))
                        {
                            Lbl_Mensaje_Error.Text += Espacios + " + La Fecha del Documento <br />";
                        }
                        if (String.IsNullOrEmpty(Txt_Monto_Solicitud_Pago.Text.Trim()))
                        {
                            Lbl_Mensaje_Error.Text += Espacios + " + El Monto del Documento <br />";
                        }
                    }
                    if (error == 1)
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
                else
                {
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }
                if (!String.IsNullOrEmpty(Txt_ID_Proveedor.Value))
                {
                    Consulta_PRO.P_Proveedor_ID = Txt_ID_Proveedor.Value;
                    Dt_Consulta_PRO = Consulta_PRO.Consulta_Datos_Proveedores();
                    if (Dt_Consulta_PRO.Rows.Count > 0)
                    {
                        Txt_Nom_Proveedor.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_Compañia].ToString();
                        Txt_RFC.Text = Dt_Consulta_PRO.Rows[0][Cat_Com_Proveedores.Campo_RFC].ToString();
                    }
                }
                if (!String.IsNullOrEmpty(Txt_ID_Empleado.Value))
                {
                    Consulta_EMP.P_Empleado_ID = Txt_ID_Empleado.Value;
                    Dt_Consulta_EMP = Consulta_EMP.Consulta_Datos_Empleado();
                    if (Dt_Consulta_EMP.Rows.Count > 0)
                    {
                        Txt_Nom_Proveedor.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Nombre].ToString();
                        Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Paterno].ToString();
                        Txt_Nom_Proveedor.Text = Txt_Nom_Proveedor.Text + Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_Apellido_Materno].ToString();
                        Txt_RFC.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_RFC].ToString();
                        Txt_CURP.Text = Dt_Consulta_EMP.Rows[0][Cat_Empleados.Campo_CURP].ToString();
                    }
                }
            }
            protected void Btn_Eliminar_Partida(object sender, EventArgs e)
            {

                ImageButton Btn_Eliminar_Partida = (ImageButton)sender;
                DataTable Dt_Partidas = (DataTable)Session["Dt_Documentos"];
                DataRow[] Filas = Dt_Partidas.Select("No_Documento" +
                        "='" + Btn_Eliminar_Partida.CommandArgument + "'");

                if (!(Filas == null))
                {
                    if (Filas.Length >= 0)
                    {
                        Dt_Partidas.Rows.Remove(Filas[0]);
                        Session["Dt_Documentos"] = Dt_Partidas;
                        Txt_Total.Text = "0";
                        Txt_Subtotal.Text = "0";
                        Grid_Documentos.Columns[2].Visible = true;
                        Grid_Documentos.Columns[6].Visible = true;
                        Grid_Documentos.Columns[7].Visible = true;
                        Grid_Documentos.Columns[8].Visible = true;
                        Grid_Documentos.Columns[9].Visible = true;
                        Grid_Documentos.Columns[10].Visible = true;
                        Grid_Documentos.Columns[11].Visible = true;
                        Grid_Documentos.Columns[12].Visible = true;
                        Grid_Documentos.Columns[13].Visible = true;
                        Grid_Documentos.Columns[14].Visible = true;
                        Grid_Documentos.Columns[20].Visible = true;
                        Grid_Documentos.Columns[19].Visible = true;
                        Grid_Documentos.DataSource = Session["Dt_Documentos"]; ; //Agrega los valores de todas las partidas que se tienen al grid
                        Grid_Documentos.DataBind();
                        Grid_Documentos.Columns[2].Visible = false;
                        Grid_Documentos.Columns[6].Visible = false;
                        Grid_Documentos.Columns[7].Visible = false;
                        Grid_Documentos.Columns[8].Visible = false;
                        Grid_Documentos.Columns[9].Visible = false;
                        Grid_Documentos.Columns[10].Visible = false;
                        Grid_Documentos.Columns[11].Visible = false;
                        Grid_Documentos.Columns[12].Visible = false;
                        Grid_Documentos.Columns[13].Visible = false;
                        Grid_Documentos.Columns[14].Visible = false;
                        Grid_Documentos.Columns[19].Visible = false;
                        Grid_Documentos.Columns[20].Visible = false;
                        Txt_Total.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Total.Text));
                        Txt_Subtotal.Text = String.Format("{0:c}", Convert.ToDouble(Txt_Subtotal.Text));
                    }
                }
            }

            ///*******************************************************************************
            ///NOMBRE DE LA FUNCION:    Convierte_Fecha_DT
            ///DESCRIPCION:             Convertir la fecha de una cadena de texto en DateTime
            ///PARAMETROS:              Busqueda: Cadena de texto con el texto a buscar de los proveedores
            ///CREO:                    Noe Mosqueda Valadez
            ///FECHA_CREO:              28/Febrero/2013 12:00
            ///MODIFICO:
            ///FECHA_MODIFICO
            ///CAUSA_MODIFICACION
            ///*******************************************************************************
            private DateTime Convierte_Fecha_DT(String Fecha)
            {
                //Declaracion de variables
                int Dia = 0; //variable para el dia
                int Mes = 0; //variable para el mes
                int Anio = 0; //Variable para el año
                DateTime Resultado = new DateTime(1900, 1, 1); //Variable para el resultado

                try
                {
                    //Obtener el dia
                    Dia = Convert.ToInt32(Fecha.Substring(0, 2));

                    //Obtener el año
                    Anio = Convert.ToInt32(Fecha.Substring(7, 4));

                    //Obtener el mes
                    switch (Fecha.Substring(3, 3).ToUpper())
                    {
                        case "ENE":
                        case "JAN":
                            Mes = 1;
                            break;

                        case "FEB":
                            Mes = 2;
                            break;

                        case "MAR":
                            Mes = 3;
                            break;

                        case "ABR":
                        case "APR":
                            Mes = 4;
                            break;

                        case "MAY":
                            Mes = 5;
                            break;

                        case "JUN":
                            Mes = 6;
                            break;

                        case "JUL":
                            Mes = 7;
                            break;

                        case "AGO":
                        case "AUG":
                            Mes = 8;
                            break;

                        case "SEP":
                            Mes = 9;
                            break;

                        case "OCT":
                            Mes = 10;
                            break;

                        case "NOV":
                            Mes = 11;
                            break;

                        case "DIC":
                        case "DEC":
                            Mes = 12;
                            break;

                        default:
                            Mes = 1;
                            break;
                    }

                    //Construir el resultado
                    Resultado = new DateTime(Anio, Mes, Dia);

                    //Entregar resultado
                    return Resultado;
                }
                catch (Exception ex)
                {
                    throw new Exception(ex.Message, ex);
                }
            }
        #endregion
        #region (Métodos Operación)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Alta_Solicitud_Pagos
            /// DESCRIPCION : Da de Alta la Solicitud del Pago con los datos proporcionados por 
            ///               el usuario
            /// PARAMETROS  : 
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 18-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************      
            private void Alta_Solicitud_Pagos()
            {
                String Numero_Solicitud;
                DataTable Dt_Documentos;
                DataSet Ds_Reporte = null;
                DataTable Dt_Pagos = null;
                ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
                String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Alta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocio
                String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                ExportOptions Opciones_Exportacion = new ExportOptions();
                DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
                PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();
                Boolean Habilitado=false;
                Decimal Impuestos = 0;
                Cls_Ope_Con_Reservas_Negocio Rs_Consulta_Reserva = new Cls_Ope_Con_Reservas_Negocio();
                DataTable Dt_Reserva = new DataTable();
                try
                {

                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Cmb_Reserva_Pago.SelectedValue);
                    Rs_Consulta_Reserva.P_No_Reserva = Cmb_Reserva_Pago.SelectedValue;
                    Dt_Reserva=Rs_Consulta_Reserva.Consultar_Reservas();
                    if (!String.IsNullOrEmpty(Dt_Reserva.Rows[0][Ope_Psp_Reservas.Campo_Empleado_ID].ToString()))
                    {
                        Rs_Alta_Ope_Con_Solicitud_Pagos.P_Empleado_ID = Txt_ID_Empleado.Value;
                    }
                    else
                    {
                        Rs_Alta_Ope_Con_Solicitud_Pagos.P_Proveedor_ID = Txt_ID_Proveedor.Value;
                    }
                    Dt_Documentos = (DataTable)Session["Dt_Documentos"];
                    if (Dt_Documentos.Rows.Count >0)
                    {
                        foreach (DataRow Fila in Dt_Documentos.Rows) {
                            Impuestos = Impuestos + Convert.ToDecimal(Fila["Retencion_ISR"].ToString()) + Convert.ToDecimal(Fila["Retencion_Celula"].ToString());
                        }
                    }
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Beneficiario = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Concepto = Txt_Concepto_Solicitud_Pago.Text;
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Monto = (Convert.ToDecimal(Txt_Total.Text.Replace(",", "").Replace("$", "")) + Impuestos);
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Tipo_Documento = "DOCUMENTO";
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Solicitud = Dt_Documentos;
                    Rs_Alta_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Numero_Solicitud = Rs_Alta_Ope_Con_Solicitud_Pagos.Alta_Solicitud_Pago_Sin_Poliza(); //Da de alto los datos de la Solicitud de Pago en la BD
                    Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                    Ds_Reporte = new DataSet();
                    Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                    Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                    //System.Threading.Thread.Sleep(10000);
                    if (Dt_Pagos.Rows.Count > 0)
                    {
                        Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                        Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                        //Se llama al método que ejecuta la operación de generar el reporte.
                        //Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                        //Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
                        Ruta = @Server.MapPath("../Rpt/Contabilidad/Rpt_Con_Solicitud_Pago.rpt");
                        Reporte.Load(Ruta);
                        if (Ds_Reporte is DataSet)
                        {
                            if (Ds_Reporte.Tables.Count > 0)
                            {
                                Reporte.SetDataSource(Ds_Reporte);
                               //Exportar_Reporte_PDF(Reporte, "Reporte_Solicitud_Pagos" + Numero_Solicitud + ".pdf");
                                if (Reporte is ReportDocument)
                                {
                                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + "Reporte_Solicitud_Pagos" + Numero_Solicitud + ".pdf");
                                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                                    Reporte.Export(Opciones_Exportacion);
                                }
                                //Mostrar_Reporte("Reporte_Solicitud_Pagos" + Numero_Solicitud, ".pdf");
                                //Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                                //Pagina = Pagina + "Reporte_Solicitud_Pagos" + Numero_Solicitud + ".pdf";
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                                //        "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                                //ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                                //    "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                            }
                        }

                        Imprimir(Numero_Solicitud);

                        ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Eliminar", "alert('No. Solicitud: " + Numero_Solicitud.Trim()  + "');", true);
                    }

                    Inicializa_Controles();
                    HttpContext.Current.Session.Remove("Archivo_Documento");
                }
                catch (Exception ex)
                {
                    throw new Exception("Alta_Solicitud_Pagos " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Modificar_Solicitud_Pago
            /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
            ///               proporcionados por el usuario
            /// PARAMETROS  : 
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 20-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Modificar_Solicitud_Pago()
            {
                String Numero_Solicitud;
                DataTable Dt_Partidas_Polizas = new DataTable(); //Obtiene los detalles de la póliza que se debera generar para el movimiento
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de Negocios
                DataTable Dt_Documentos;
                try
                {
                    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                    Dt_Documentos = (DataTable)Session["Dt_Documentos"];
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Pago.Text;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Tipo_Solicitud_Pago_ID = Cmb_Tipo_Solicitud_Pago.SelectedValue;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva = Convert.ToDouble(Txt_No_Reserva_Anterior.Value);
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Reserva_Anterior = Convert.ToDouble(Txt_No_Reserva_Anterior.Value);
                    if (Cmb_Tipo_Solicitud_Pago.SelectedValue != "00001")
                    {
                        Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Proveedor_ID = Txt_ID_Proveedor.Value;
                    }
                    else
                    {
                        if (Cmb_Tipo_Solicitud_Pago.SelectedValue == "00001")
                        {
                            Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Empleado_ID = Txt_ID_Empleado.Value;
                        }
                    }
                    //if (Cmb_Tipo_Solicitud_Pago.Text == "Pago a Proveedores")
                    //{
                    //    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Proveedor_ID = Txt_ID_Proveedor.Value;
                    //}
                    //if (Cmb_Tipo_Solicitud_Pago.Text == "Gastos por Pagar")
                    //{
                    //    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Empleado_ID = Txt_ID_Empleado.Value;
                    //}
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Beneficiario = Txt_Nombre_Proveedor_Solicitud_Pago.Text;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Concepto = Txt_Concepto_Solicitud_Pago.Text;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto = Convert.ToDecimal(Txt_Total.Text.Replace(",", "").Replace("$", ""));
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Monto_Anterior = Convert.ToDouble(Txt_Total_anterior.Value.Replace(",", "").Replace("$", ""));
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Modifica_Solicitud = "S";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Dt_Detalles_Solicitud = Dt_Documentos;
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "PENDIENTE";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Tipo_Documento = "DOCUMENTO";
                    Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Nombre_Usuario = Cls_Sessiones.Nombre_Empleado;
                    Numero_Solicitud=Rs_Modificar_Ope_Con_Solicitud_Pagos.Modificar_Solicitud_Pago_Sin_Poliza(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados
                    Imprimir(Numero_Solicitud);

                    ScriptManager.RegisterClientScriptBlock(this, this.GetType(), "Modificar Solicitud", "alert('La Solicitud No.: " + Txt_No_Solicitud_Pago.Text.Trim() + " ha sido modificada.');", true);

                    Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    HttpContext.Current.Session.Remove("Archivo_Documento");
                }
                catch (Exception ex)
                {
                    throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Eliminar_Solicitud_Pago
            /// DESCRIPCION : Elimina los datos de la Solicitud del Pago que fue 
            ///               seleccionada por el Usuario
            /// PARAMETROS  : 
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 20-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Eliminar_Solicitud_Pago()
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Rs_Consulta_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Solicitud_Pagos_Negocio(); //Variable de conexión hacia la capa de negocios
                try
                {
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Reserva =Convert.ToDouble(Cmb_Reserva_Pago.SelectedValue);
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_Monto = Convert.ToDecimal(Txt_Monto_Solicitud_Pago.Text.Replace(",", "").Replace("$", ""));
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.P_No_Solicitud_Pago = Txt_No_Solicitud_Pago.Text.Trim();
                    Rs_Consulta_Ope_Con_Solicitud_Pagos.Eliminar_Solicitud_Pago();//Elimina la solicitud seleccionada por el usuario de la BD

                    Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Eliminación de la Solicitud de Pago fue Exitosa');", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Eliminar_Solicitud_Pago" + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
    #region(Grid)
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Grid_Cuentas_RowDataBound
        /// DESCRIPCION : Agrega un identificador al boton de cancelar de la tabla
        ///               para identicar la fila seleccionada de tabla.
        /// CREO        : Yazmin A Delgado Gómez
        /// FECHA_CREO  : 11/Julio/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Grid_Documentos_RowDataBound(object sender, GridViewRowEventArgs e)
            {
                try
                {
                    HyperLink Hyp_Lnk_Ruta;
                    if (Txt_Total.Text == "")
                    {
                        Txt_Total.Text = "0";
                    }
                    if (Txt_Subtotal.Text == "")
                    {
                        Txt_Subtotal.Text = "0";
                    }
                    if (e.Row.RowType.Equals(DataControlRowType.DataRow))
                    {                        
                        ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).CommandArgument = e.Row.Cells[6].Text.Trim();
                        ((ImageButton)e.Row.Cells[3].FindControl("Btn_Eliminar")).ToolTip = "Quitar el Documento " + e.Row.Cells[6].Text;
                        Txt_Total.Text = Convert.ToString(Convert.ToDouble(Txt_Total.Text) + Convert.ToDouble(e.Row.Cells[5].Text));
                        if (e.Row.Cells[4].Text == "OTROS")
                        {
                            Txt_Subtotal.Text = Convert.ToString(Convert.ToDouble(Txt_Subtotal.Text)+ Convert.ToDouble(e.Row.Cells[5].Text)- Convert.ToDouble(e.Row.Cells[3].Text));
                        }
                        else
                        {
                            Txt_Subtotal.Text = Convert.ToString(Convert.ToDouble(Txt_Subtotal.Text) + Convert.ToDouble(e.Row.Cells[5].Text) + Convert.ToDouble(e.Row.Cells[15].Text) + Convert.ToDouble(e.Row.Cells[16].Text) + Convert.ToDouble(e.Row.Cells[17].Text));
                        }                       
                        Hyp_Lnk_Ruta = (HyperLink)e.Row.Cells[0].FindControl("Hyp_Lnk_Ruta");
                        if (!String.IsNullOrEmpty(e.Row.Cells[8].Text.Trim()) &&  e.Row.Cells[8].Text.Trim()!= "&nbsp;" )
                        {
                            Hyp_Lnk_Ruta.NavigateUrl = "Frm_Con_Mostrar_Archivos.aspx?Documento=" + e.Row.Cells[8].Text.Trim();
                            Hyp_Lnk_Ruta.Enabled = true;
                        }
                        else
                        {
                            Hyp_Lnk_Ruta.NavigateUrl = "";
                            Hyp_Lnk_Ruta.Enabled = false;
                        }
                    }
                }
                catch (Exception Ex)
                {
                    throw new Exception(Ex.Message);
                }
            }
        protected void Grid_Solicitud_Pagos_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;                
                Consulta_Datos_Solicitud_Pago(Grid_Solicitud_Pagos.SelectedRow.Cells[1].Text); //Consulta los datos de la solicitud que fue seleccionada por el usuario
                if(!String.IsNullOrEmpty(Txt_Estatus_solicitud_Pago.Text.Trim()))
                {
                    if (Txt_Estatus_solicitud_Pago.Text.Trim().Equals("CANCELADO") || Txt_Estatus_solicitud_Pago.Text.Trim().Equals("PAGADO"))
                    {
                         Btn_Imprimir.Visible = false;
                    }
                    else
                    {
                        Btn_Imprimir.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Grid_Solicitud_Pagos_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Limpia_Controles();                              //Limpia los controles de la forma
                Grid_Solicitud_Pagos.PageIndex = e.NewPageIndex; //Asigna la nueva página que selecciono el usuario
                Llena_Grid_Solicitudes_Pagos();                  //Muestra la solicitudes que estan asignadas en la página seleccionada por el usuario
                Grid_Solicitud_Pagos.SelectedIndex = -1;
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Grid_Solicitud_Pagos_Sorting(object sender, GridViewSortEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                //Se consultan los Tipos de solicitudes que actualmente se encuentran registradas en el sistema.
                Consulta_Solicitudes_Pagos();

                DataTable Dt_Solicitud = (Grid_Solicitud_Pagos.DataSource as DataTable);

                if (Dt_Solicitud != null)
                {
                    DataView Dv_Solicitud = new DataView(Dt_Solicitud);
                    String Orden = ViewState["SortDirection"].ToString();

                    if (Orden.Equals("ASC"))
                    {
                        Dv_Solicitud.Sort = e.SortExpression + " " + "DESC";
                        ViewState["SortDirection"] = "DESC";
                    }
                    else
                    {
                        Dv_Solicitud.Sort = e.SortExpression + " " + "ASC";
                        ViewState["SortDirection"] = "ASC";
                    }
                    Grid_Solicitud_Pagos.DataSource = Dv_Solicitud;
                    Grid_Solicitud_Pagos.DataBind();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Eventos)
        protected void Cmb_Reserva_Pago_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Lbl_Mensaje_Error.Text = "";
                Limpia_Datos_Reserva(); //Limpia los controles que pertenecen a la reserva
                if (Cmb_Reserva_Pago.SelectedIndex > 0) Consulta_Datos_Reserva(Convert.ToDouble(Cmb_Reserva_Pago.SelectedValue)); //Consulta los datos de la reserva que fue seleccionada por el usuario
                if (Session["Dt_Documentos"] != null)
                {
                    Session.Remove("Dt_Documentos");
                }
                Grid_Documentos.DataSource = null;
                Grid_Documentos.DataBind();
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }

        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Btn_Cerrar_Ventana_Click
        /// DESCRIPCION : Cierra la ventana de busqueda de empleados.
        /// CREO        : Yazmin Delgado Gómez
        /// FECHA_CREO  : 19-Noviembre-2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Btn_Cerrar_Ventana_Click(object sender, ImageClickEventArgs e)
        {
            Mpe_Busqueda_Solicitud_Pago.Hide();
        }
        protected void Btn_Busqueda_Solicitud_Pago_Click(object sender, EventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Consulta_Solicitudes_Pagos(); //Consulta a todos las solicitudes de pagos con los datos proporcionados por el usuario
                Mpe_Busqueda_Solicitud_Pago.Hide();
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;

                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }    
        protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Btn_Nuevo.ToolTip == "Nuevo")
                {
                    Consulta_Reservas();          //Consulta las reservas que tienen un saldo pendiente
                    Limpia_Controles();           //Limpia los controles de la forma para poder introducir nuevos datos
                    Habilitar_Controles("Nuevo"); //Habilita los controles para la introducción de datos por parte del usuario
                }
                else
                {
                    //Valida si todos los campos requeridos estan llenos si es así da de alta los datos en la base de datos
                    if (Validar_Datos_Solicitud_Pagos())
                    {   
                        Alta_Solicitud_Pagos(); //Da de alta la Solicitud del Pago con los datos que proporciono el usuario   
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Btn_Imprimir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (!String.IsNullOrEmpty(Txt_No_Solicitud_Pago.Text)){
                    Imprimir(Txt_No_Solicitud_Pago.Text);
                }else{
                    Lbl_Mensaje_Error.Text = "Es necesario Tener el número de solicitud: <br />";
                    Lbl_Mensaje_Error.Visible = true;
                    Img_Error.Visible = true;
                }               
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Imprimir
        ///DESCRIPCIÓN: Imprime la solicitud
        ///PROPIEDADES:     
        ///CREO: Sergio Manuel Gallardo
        ///FECHA_CREO: 06/Enero/2012 
        ///MODIFICO:
        ///FECHA_MODIFICO
        ///CAUSA_MODIFICACIÓN
        ///*******************************************************************************
        protected void Imprimir(String Numero_Solicitud)
        {
            DataSet Ds_Reporte = null;
            DataTable Dt_Pagos = null;
            try
            {
                Cls_Ope_Con_Solicitud_Pagos_Negocio Solicitud_Pago = new Cls_Ope_Con_Solicitud_Pagos_Negocio();
                Ds_Reporte = new DataSet();
                Solicitud_Pago.P_No_Solicitud_Pago = Numero_Solicitud;
                Dt_Pagos = Solicitud_Pago.Consulta_Solicitud_Pagos_con_Detalles();
                if (Dt_Pagos.Rows.Count > 0)
                {
                    Dt_Pagos.TableName = "Dt_Solicitud_Pago";
                    Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                    //Se llama al método que ejecuta la operación de generar el reporte.
                    Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Solicitud_Pago.rpt", "Reporte_Solicitud_Pagos" + Numero_Solicitud , ".pdf");
                }
            }
            //}
            catch (Exception Ex)
            {
                Lbl_Mensaje_Error.Text = Ex.Message.ToString();
                Lbl_Mensaje_Error.Visible = true;
            }

        }
        protected void Btn_Modificar_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Btn_Modificar.ToolTip == "Modificar")
                {
                    //Si el usuario selecciono un Tipo de Solicitud entonces habilita los controles para que pueda modificar la información
                    if (!string.IsNullOrEmpty(Txt_No_Solicitud_Pago.Text.Trim()))
                    {
                        if (Txt_Estatus_solicitud_Pago.Text == "PENDIENTE")
                        {
                            Habilitar_Controles("Modificar"); //Habilita los controles para la modificación de los datos
                        }
                        else
                        {
                            Lbl_Mensaje_Error.Visible = true;
                            Img_Error.Visible = true;
                            Lbl_Mensaje_Error.Text = "Solo se puede mdificar Solicitudes pendientes de autorización <br />";
                        }
                    }
                    //Si el usuario no selecciono una Solicitud le indica al usuario que la seleccione para poder modificar
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                        Lbl_Mensaje_Error.Text = "Seleccione la de Solicitud de Pago que desea modificar sus datos <br />";
                    }
                }
                else
                {
                    //Si el usuario proporciono todos los datos requeridos entonces modificar los datos de la Solicitud en la BD
                    if (Validar_Datos_Solicitud_Pagos())
                    {
                        Modificar_Solicitud_Pago(); //Modifica los datos de la Solicitud con los datos proporcionados por el usuario
                    }
                    else
                    {
                        Lbl_Mensaje_Error.Visible = true;
                        Img_Error.Visible = true;
                    }
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
        protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
        {
            try
            {
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                if (Btn_Salir.ToolTip == "Salir")
                {
                    Session.Remove("Consulta_Solicitud_Pagos");
                    Session.Remove("Consulta_Reservas");
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
                else
                {
                    Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }    
    #endregion
    #region Metodos Reportes
        /// *************************************************************************************
        /// NOMBRE:             Generar_Reporte
        /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
        ///              
        /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
        ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
        ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
        ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
        /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
        /// FECHA MODIFICO:     16/Mayo/2011
        /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
        ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
        /// *************************************************************************************
        public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
        {
            ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
            String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 
            try
            {
                Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
                Reporte.Load(Ruta);

                if (Ds_Reporte_Crystal is DataSet)
                {
                    if (Ds_Reporte_Crystal.Tables.Count > 0)
                    {
                        Reporte.SetDataSource(Ds_Reporte_Crystal);
                        Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                        Mostrar_Reporte(Nombre_Reporte_Generar + Formato);
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        /// *************************************************************************************
        /// NOMBRE:             Exportar_Reporte_PDF
        /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
        ///                     especificada.
        /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
        ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
        /// USUARIO CREO:       Juan Alberto Hernández Negrete.
        /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
        /// USUARIO MODIFICO:
        /// FECHA MODIFICO:
        /// CAUSA MODIFICACIÓN:
        /// *************************************************************************************
        public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
        {
            ExportOptions Opciones_Exportacion = new ExportOptions();
            DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
            PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

            try
            {
                if (Reporte is ReportDocument)
                {
                    Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                    Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
            }
        }
        protected void Mostrar_Reporte(String Nombre_Reporte)
        {
            String Pagina = "../../Reporte/";// "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

            try
            {
                Pagina = Pagina + Nombre_Reporte;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window",
                    "window.open('" + Pagina + "', 'Requisición','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
        }
    #endregion
    #region "Subir Archivos"
        /// *************************************************************************************
        /// NOMBRE:              Asy_Cargar_Archivo_Complete
        /// DESCRIPCIÓN:         Carga el archivo.
        /// PARÁMETROS:
        /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
        /// FECHA CREO:          28-febrero-2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:  
        /// *************************************************************************************
        protected void Asy_Cargar_Archivo_Complete(Object sender, AjaxControlToolkit.AsyncFileUploadEventArgs  e)
        {
            System.Threading.Thread.Sleep(1000);

            //Declaracion de variables
            String aux = String.Empty;

            try
            {
                AsyncFileUpload archivo;
                archivo = (AsyncFileUpload)sender;
                String Filename;
                Filename = archivo.FileName;
                if(!String.IsNullOrEmpty(Filename)) {
                    String[] arr1;
                    arr1= Filename.Split('\\');
                    int len = arr1.Length;
                    String img1 = arr1[len - 1];
                    String filext = img1.Substring(img1.LastIndexOf(".") + 1);
                    if( filext == "txt" || filext == "doc" || filext == "zip" || filext == "pdf" || filext == "rar" || filext == "docx" || filext == "jpg" || filext == "JPG" || filext == "jpeg" || filext == "JPEG" || filext == "png" || filext == "PNG" || filext == "gif" || filext == "GIF" || filext == "xlsx")
                    {
                        if (archivo.FileContent.Length > 2621440) { return; }

                        //Guardar dolcumento
                        aux = Guardar_Documentos_Anexos();

                        //Colocar el nombre del archivo en una variable de sesion
                        HttpContext.Current.Session["Archivo_Documento"] = aux;
                    }
                    else{
                    //Return
                    }
                }
            }
            catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }
         }
        /// *************************************************************************************
        /// NOMBRE:              ClearSession_AsyncFileUpload
        /// DESCRIPCIÓN:         limpia el control de asuncfileupload.
        /// PARÁMETROS:
        /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
        /// FECHA CREO:          28-febrero-2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:  
        /// *************************************************************************************
        protected void ClearSession_AsyncFileUpload(String ClientID){
            HttpContext currentContext;
            if (HttpContext.Current!= null && HttpContext.Current.Session!=null){
             currentContext = HttpContext.Current;
            }else{
             currentContext = null;
            }
            if(currentContext!=null){
                foreach(String Key in currentContext.Session.Keys){
                    if(Key.Contains(ClientID)){
                        currentContext.Session.Remove(Key);
                        break;
                    }
                }
            }
        }
        /// *************************************************************************************
        /// NOMBRE:              Guardar_Documentos_Anexos
        /// DESCRIPCIÓN:         Guarda el archivo en una carpeta temporal y devuelve la extension
        /// PARÁMETROS:
        /// USUARIO CREO:        Sergio Manuel Gallardo Andrade
        /// FECHA CREO:          28-febrero-2012
        /// USUARIO MODIFICO:    
        /// FECHA MODIFICO:      
        /// CAUSA MODIFICACIÓN:  
        /// *************************************************************************************
         protected String Guardar_Documentos_Anexos(){
             DataTable Dt_Documentos_Anexos=new DataTable();
             AsyncFileUpload  Asy_FileUpload;
             String appPath="";
             String Nombre_Directorio="";
             try{
             //Se obtiene la direccion en donde se va a guardar el archivo
                appPath = Server.MapPath("~");
                //Crear el Directorio de los archivos
                 if(!Directory.Exists(appPath)){
                     System.IO.Directory.CreateDirectory(appPath);
                 }
                 //Se establece el nombre del directorio
                  Nombre_Directorio = "Archivo_Solicitud_Pagos\\Temporal";
                     if (Directory.Exists(appPath)){
                         //Se asigna el control AsyncFileUpload1 a la variable Async_FileUpload
                        Asy_FileUpload = Asy_Cargar_Archivo;
                         if(!String.IsNullOrEmpty(Asy_FileUpload.FileName)){
                         //Valida que no exista el directorio, si no existe lo crea
                             DirectoryInfo Directorio;
                             if (Directory.Exists(appPath +"\\"+ Nombre_Directorio)==false){
                                 Directorio = Directory.CreateDirectory(appPath + "\\"+ Nombre_Directorio);
                            }
                             //Se asigna el directorio en donde se va a guardar los documentos
                             String saveDir= Nombre_Directorio + "\\" ;
                             String savePath = appPath + "\\" + saveDir + "" + Asy_FileUpload.FileName;
                             Server.HtmlEncode(Asy_FileUpload.FileName);
                             if (Asy_FileUpload.HasFile) {
                                    //Se guarda el archivo
                                    Asy_FileUpload.SaveAs(HttpUtility.HtmlEncode(savePath));
                                    Txt_Ruta.Text = HttpUtility.HtmlEncode(savePath);
                                    Txt_Nombre_Archivo.Text = HttpUtility.HtmlEncode(Asy_FileUpload.FileName);
                             }
                         }
                     }
                 return (appPath + "\\" + Nombre_Directorio + "\\" + Txt_Nombre_Archivo.Text);
             }
               catch (Exception Ex)
            {
                throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
            }    
        
    }
#endregion

}
