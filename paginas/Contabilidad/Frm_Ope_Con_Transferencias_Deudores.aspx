﻿<%@ Page Language="C#" MasterPageFile="~/paginas/Paginas_Generales/MasterPage.master" AutoEventWireup="true" CodeFile="Frm_Ope_Con_Transferencias_Deudores.aspx.cs" Inherits="paginas_Contabilidad_Frm_Ope_Con_Transferencias_Deudores" Title="Untitled Page"%>
<%@ Register Assembly="AjaxControlToolkit" namespace="AjaxControlToolkit" tagprefix="cc1"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
<script src="../jquery/jquery-1.5.js" type="text/javascript"></script>
    <script type="text/javascript" language="javascript">
        document.onkeydown = function(evt) {
            return (evt ? evt.which : event.keyCode) != 13;
        }
        function selec_todo2() {
            var y;
            y = $('#chkAll').is(':checked');
            var $chkBox = $("input:checkbox[id$=Chk_Autorizado]");
            if (y == true) {
                $chkBox.attr("checked", true);
            } else {
            $chkBox.attr("checked", false);
                //        x= $('.ser').attr('checked', false);
            }
        }
        function Rechazar_Solicitud() {
            //limpiar controles
            var a = $("input:checkbox[id$=Chk_Autorizado]")
            var cont = 0;
            MostrarProgress();
            for (var i = 0; i < a.length; i++) {
                if ($(a[i]).is(':checked') == true) {
                    cont++;
                    var No_Solicitud = $(a[i]).parent().attr('class');
                    //                        var comentario = "";
                    var cadena = "Accion=Rechazar_Transferencia&id=" + No_Solicitud + "&x";
                    $.ajax({
                        url: "Frm_Ope_Con_Transferencias_Deudores.aspx?" + cadena,
                        type: 'POST',
                        async: false,
                        cache: false,
                        success: function(data) {
                        }
                    });
                }
            }
            if (cont == 0) {
                alert("No tienes ninga Solicitud");
            } else {
                location.reload();
            }
        }
        function MostrarProgress() {
            $('[id$=Up_Autorizacion]').show();
        }
        function Mostrar_Tabla(Renglon, Imagen) {
            object = document.getElementById(Renglon);
            if (object.style.display == "none") {
                object.style.display = "";
                document.getElementById(Imagen).src = " ../../paginas/imagenes/paginas/stocks_indicator_down.png";
            } else {
                object.style.display = "none";
                document.getElementById(Imagen).src = "../../paginas/imagenes/paginas/add_up.png";
            }
        }
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="Cph_Area_Trabajo2" Runat="Server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="Cph_Area_Trabajo1" Runat="Server">
 <cc1:ToolkitScriptManager ID="ScriptManagerTransferencia" runat="server" EnableScriptGlobalization="true" EnableScriptLocalization="true" />
      <asp:UpdatePanel ID="Upd_Panel" runat="server">
        <ContentTemplate>
            <asp:UpdateProgress ID="Up_Autorizacion" runat="server" AssociatedUpdatePanelID="Upd_Panel" DisplayAfter="0">
                <ProgressTemplate>
                    <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                    <div  class="processMessage" id="div_progress"><img alt="" src="../Imagenes/paginas/Updating.gif" /></div>
                </ProgressTemplate>
            </asp:UpdateProgress>
            <div id="Div_Parametros_Contabilidad" >
                <table width="98%"  border="0" cellspacing="0" class="estilo_fuente">
                    <tr align="center">
                        <td colspan="2" class="label_titulo">Autorizacion de Transferencia de Deudores</td>
                    </tr>
                    <tr>
                        <td colspan="2">&nbsp;
                            <asp:Image ID="Img_Error" runat="server" ImageUrl="~/paginas/imagenes/paginas/sias_warning.png" Visible="false" />&nbsp;
                            <asp:Label ID="Lbl_Mensaje_Error" runat="server" Text="Mensaje" Visible="false" CssClass="estilo_fuente_mensaje_error"></asp:Label>
                        </td>
                    </tr>
                    <tr class="barra_busqueda" align="right">
                        <td align="left">
                            <asp:ImageButton ID="Btn_Salir" runat="server" ToolTip="Inicio" CssClass="Img_Button" 
                                TabIndex="3" ImageUrl="~/paginas/imagenes/paginas/icono_salir.png" onclick="Btn_Salir_Click"/>
                        </td>
                        <td style="width:50%">Busqueda
                            <asp:TextBox ID="Txt_Busqueda_No_Solicitud" runat="server" MaxLength="10" TabIndex="1" ToolTip="Buscar No. Solicitud" Width="120px"></asp:TextBox>
                            <cc1:TextBoxWatermarkExtender ID="TWE_Txt_Busqueda_No_Solicitud" runat="server" WatermarkCssClass="watermarked"
                                WatermarkText="<Ingrese No. Solicitud>" TargetControlID="Txt_Busqueda_No_Solicitud" />
                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Busqueda_No_Solicitud" runat="server" 
                                TargetControlID="Txt_Busqueda_No_Solicitud" FilterType="Numbers" >
                            </cc1:FilteredTextBoxExtender>
                            <asp:ImageButton ID="Btn_Buscar_No_Solicitud" runat="server" 
                                ToolTip="Consultar" TabIndex="6" 
                                ImageUrl="~/paginas/imagenes/paginas/busqueda.png" 
                                onclick="Btn_Buscar_No_Solicitud_Click" />
                        </td>
                    </tr>
                </table>
                <table width="98%" class="estilo_fuente">
                    <tr>
                        <td>
                            <table table width="99%" class="estilo_fuente">
                               <tr>
                                    <td style="width:22%;">
                                        <asp:Label ID="Lbl_Banco" runat="server" Text="BANCO TRANSFERENCIA"></asp:Label>
                                    </td>
                                    <td style="width:25%;">
                                        <asp:DropDownList ID="Cmb_Banco" runat="server" Width="90%"  TabIndex="2" OnSelectedIndexChanged="Cmb_Banco_Transferencia_OnSelectedIndexChanged" AutoPostBack="true">
                                        </asp:DropDownList>
                                    </td>
                                    <td style="width:15%;">
                                        <asp:Label ID="Lbl_Comision" runat="server" Text="COMISION BANCO"></asp:Label>
                                    </td>
                                    <td style="width:12%;"> 
                                        <asp:TextBox ID="Txt_Comision_Banco" runat="server" Width="90%" TabIndex="3" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';" MaxLength="5"></asp:TextBox>
                                    </td>
                                    <td style="width:12%;">
                                        <asp:Label ID="Lbl_Iva_Banco" runat="server" Text="IVA COMISION" ></asp:Label>
                                    </td>
                                    <td style="width:12%;">
                                        <asp:TextBox ID="Txt_Iva_Comision" runat="server" Width="90%" TabIndex="4" OnKeyPress="this.value=(this.value.match(/^[0-9]*(\.[0-9]{0,2})?$/))?this.value :'';" MaxLength="5"></asp:TextBox>
                                    </td>
                               </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Label ID="Lbl_Seleccion_Masiva" runat="server" Text="AUTORIZAR TODOS"></asp:Label>
                            <input type="checkbox"  id="chkAll" TabIndex="5"  onclick="selec_todo2();"/>
                             &nbsp;
                            <asp:Button ID="Btn_Ejecutar" runat="server" TabIndex="6" Text="Ejecutar" OnClick="Btn_Autozizar_onclick" />
                             &nbsp;                             
                             <input id="Btn_Cancelar" type="button" TabIndex="7"  class="boton" onclick="Rechazar_Solicitud();" value="Cancelar" />
                        </td>
                    </tr>
                    <tr>
                        <td style="width:100%;text-align:center;vertical-align:top;"> 
                            <center>
                                <div style="overflow:auto;height:600px;width:99%;vertical-align:top;border-style:outset;border-color:Silver;" >
                                <asp:GridView ID="Grid_Pagos" runat="server" AllowPaging="False"  ShowHeader="false"
                                AutoGenerateColumns="False" CssClass="GridView_1" 
                                DataKeyNames="DEUDOR_ID" GridLines="None" Width="99.5%" OnRowDataBound="Grid_Pagos_RowDataBound" >
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Image ID="Img_Btn_Expandir" runat="server"
                                                ImageUrl="~/paginas/imagenes/paginas/stocks_indicator_down.png" />
                                        </ItemTemplate>
                                        <HeaderStyle HorizontalAlign="Center" Width="2%" />
                                        <ItemStyle HorizontalAlign="Center" Width="2%" />
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="DEUDOR_ID" HeaderText="Padron">
                                        <HeaderStyle HorizontalAlign="Left" Width="15%" />
                                        <ItemStyle HorizontalAlign="Left" Width="15%" />
                                    </asp:BoundField>
                                    <asp:BoundField DataField="Beneficiario" HeaderText="Beneficiario">
                                        <HeaderStyle HorizontalAlign="Left" Width="40%" />
                                        <ItemStyle HorizontalAlign="Left" Width="40%"  Font-Size="XX-Small"/>
                                    </asp:BoundField>
                                    <%--<asp:ButtonField ButtonType="Link" CommandName="Select" DataTextField="Beneficiario" ControlStyle-Font-Size="X-Small">
                                         <HeaderStyle Font-Size="XX-Small" HorizontalAlign="Left" Width="40%" />
                                        <ItemStyle Font-Size="XX-Small" Width="40%" ForeColor="Blue" />
                                    </asp:ButtonField>--%>
                                    <asp:TemplateField  HeaderText= "Fecha">
                                        <HeaderStyle  Font-Size="X-Small" HorizontalAlign="center" Width="20%" />
                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="20%" />
                                        <ItemTemplate >
                                            <asp:TextBox ID="Txt_Fecha_Poliza" runat="server" Width="60%" MaxLength="11"  Font-Size="X-Small"/>
                                            <cc1:TextBoxWatermarkExtender ID="TBWE_Txt_Fecha_Poliza" runat="server" 
                                                TargetControlID="Txt_Fecha_Poliza" WatermarkCssClass="watermarked" 
                                                WatermarkText="Dia/Mes/Año" Enabled="True" />
                                            <cc1:CalendarExtender ID="CE_Txt_Fecha_Poliza" runat="server" 
                                                TargetControlID="Txt_Fecha_Poliza" Format="dd/MMM/yyyy" Enabled="True" PopupButtonID="Btn_Fecha_Poliza"/>
                                             <asp:ImageButton ID="Btn_Fecha_Poliza" runat="server"
                                                ImageUrl="../imagenes/paginas/SmallCalendar.gif" style="vertical-align:top;"
                                                Height="14px" CausesValidation="false"/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}">
                                        <HeaderStyle HorizontalAlign="right" Width="15%" />
                                        <ItemStyle HorizontalAlign="right" Width="15%"  />
                                    </asp:BoundField>
                                    <asp:TemplateField>
                                        <ItemTemplate>
                                            <asp:Label ID="Lbl_Movimientos" runat="server" Text='<%# Bind("IDENTIFICADOR_TIPO") %>' Visible="false"></asp:Label>
                                            <asp:Literal ID="Ltr_Inicio" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='position:static'&gt;&lt;td colspan='5'; align='right'&gt;" />
                                            <asp:GridView ID="Grid_Cuentas" runat="server" AllowPaging="False" DataKeyNames="DEUDOR_ID" OnRowDataBound="Grid_Cuentas_RowDataBound"
                                                AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%" > <%--OnSelectedIndexChanged="Grid_Solicitud_Pagos_SelectedIndexChanged" OnRowDataBound="Grid_Proveedores_RowDataBound"--%>
                                                <Columns>
                                                    <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Image ID="Img_Btn_Expandir_Cuenta" runat="server"
                                                                ImageUrl="~/paginas/imagenes/paginas/add_up.png" />
                                                        </ItemTemplate>
                                                        <HeaderStyle HorizontalAlign="Center" Width="5%" />
                                                        <ItemStyle HorizontalAlign="Left" Width="5%" />
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="CUENTA_BANCO_ID" HeaderText="Banco ID">
                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="No_Cuenta" HeaderText="Cuenta">
                                                        <HeaderStyle HorizontalAlign="Left" Width="15%" Font-Size="X-Small"/>
                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="15%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Banco" HeaderText="Banco">
                                                        <HeaderStyle HorizontalAlign="Left" Width="20%" Font-Size="X-Small"/>
                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="Left" Width="20%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="Orden" HeaderText="Orden">
                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                        <ItemStyle Font-Size="X-Small" HorizontalAlign="Left" Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:TemplateField HeaderText="Referencia">
                                                        <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="X-Small"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Beneficiario_Grid" runat="server" Font-Names="Courier New"
                                                                Font-Size="10px" MaxLength ="10"
                                                                CssClass='<%# Eval("No_Cuenta") %>'
                                                                 name="<%=Txt_Beneficiario_Grid.ClientID %>" 
                                                                Width="90%"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Beneficiario_Grid" runat="server" 
                                                                Enabled="True" TargetControlID="Txt_Beneficiario_Grid" 
                                                                 FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                                 ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ref. Comision">
                                                        <HeaderStyle HorizontalAlign="Center" Width="12%" Font-Size="X-Small"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="12%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Referencia_Comision" runat="server" Font-Names="Courier New"
                                                                Font-Size="10px" MaxLength ="10"
                                                                CssClass='<%# Eval("No_Cuenta") %>'
                                                                 name="<%=Txt_Referencia_Comision.ClientID %>" 
                                                                Width="90%"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Referencia_Comision" runat="server" 
                                                                Enabled="True" TargetControlID="Txt_Referencia_Comision" 
                                                                 FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                                 ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:TemplateField HeaderText="Ref.Iva">
                                                        <HeaderStyle HorizontalAlign="Center" Width="13%" Font-Size="X-Small"/>
                                                        <ItemStyle HorizontalAlign="Center" Width="13%" />
                                                        <ItemTemplate>
                                                            <asp:TextBox ID="Txt_Referencia_IVA_Comision" runat="server" Font-Names="Courier New"
                                                                Font-Size="10px" MaxLength ="10"
                                                                CssClass='<%# Eval("No_Cuenta") %>'
                                                                 name="<%=Txt_Referencia_IVA_Comision.ClientID %>" 
                                                                Width="90%"></asp:TextBox>
                                                            <cc1:FilteredTextBoxExtender ID="FTE_Txt_Referencia_IVA_Comision" runat="server" 
                                                                Enabled="True" TargetControlID="Txt_Referencia_IVA_Comision" 
                                                                 FilterType="Custom, UppercaseLetters, LowercaseLetters, Numbers" 
                                                                 ValidChars="ÑñáéíóúÁÉÍÓÚ. ">
                                                            </cc1:FilteredTextBoxExtender>
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                    <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}">
                                                        <HeaderStyle HorizontalAlign="Center" Width="10%" Font-Size="X-Small"/>
                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="right" Width="10%" />
                                                    </asp:BoundField>
                                                    <asp:BoundField DataField="DEUDOR_ID" HeaderText="Padron">
                                                        <HeaderStyle HorizontalAlign="Left" Width="20%" />
                                                            <ItemStyle HorizontalAlign="Left" Width="20%" />
                                                    </asp:BoundField>
		                                            <asp:TemplateField>
                                                        <ItemTemplate>
                                                            <asp:Label ID="Lbl_Solicitud" runat="server" 
                                                                Text='<%# Bind("IDENTIFICADOR") %>' Visible="false"></asp:Label>
                                                                <asp:Literal ID="Ltr_Inicio2" runat="server" Text="&lt;/td&gt;&lt;tr id='Renglon_Grid' style='display:none;position:static'&gt;&lt;td colspan='8'; align='right';&gt;" />
                                                            <asp:GridView ID="Grid_Datos_Solicitud" runat="server" AllowPaging="False"  
                                                                  AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None" Width="98%"> <%--OnSelectedIndexChanged="Grid_Solicitud_Detalles_SelectedIndexChanged" OnRowDataBound="Grid_Solicitudes_RowDataBound"--%>
                                                                <Columns>
                                                                    <asp:BoundField DataField="NO_DEUDA" HeaderText="solicitud">
                                                                        <HeaderStyle HorizontalAlign="center" Width="10%" Font-Size="X-Small" />
                                                                        <ItemStyle  Font-Size="X-Small" HorizontalAlign="center" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Concepto" HeaderText="Concepto"
                                                                        HeaderStyle-Font-Size="12px"  ItemStyle-Font-Size="10px">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="30%" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="30%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Transferencia" HeaderText="Orden"
                                                                        HeaderStyle-Font-Size="12px"  ItemStyle-Font-Size="10px">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="7%" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                    </asp:BoundField>
                                                                     <asp:BoundField DataField="Nombre_Banco" HeaderText="Banco"
                                                                        HeaderStyle-Font-Size="12px"  ItemStyle-Font-Size="10px">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Monto" HeaderText="Monto" DataFormatString="{0:c}"
                                                                        HeaderStyle-Font-Size="12px"  ItemStyle-Font-Size="10px">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="10%" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="10%" />
                                                                    </asp:BoundField>
                                                                    <asp:BoundField DataField="Estatus" HeaderText="Estatus"
                                                                        HeaderStyle-Font-Size="12px"  ItemStyle-Font-Size="10px">
                                                                        <HeaderStyle HorizontalAlign="Left" Width="8%" Font-Size="X-Small"/>
                                                                        <ItemStyle HorizontalAlign="Left" Width="8%" />
                                                                    </asp:BoundField>
                                                                    <asp:TemplateField  HeaderText= "Accion">
                                                                        <HeaderStyle HorizontalAlign="center" Width="5%" />
                                                                        <ItemStyle HorizontalAlign="center" Width="5%" />
                                                                        <ItemTemplate >
                                                                            <asp:CheckBox id="Chk_Autorizado" runat="server"  CssClass='<%# Eval("NO_DEUDA") %>'/>
                                                                        </ItemTemplate>
                                                                    </asp:TemplateField>
                                                                </Columns>
                                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                                <FooterStyle CssClass="GridPager" />
                                                                <HeaderStyle CssClass="GridHeader_Nested" />
                                                                <PagerStyle CssClass="GridPager" />
                                                                <RowStyle CssClass="GridItem" />
                                                                <SelectedRowStyle CssClass="GridSelected" />
                                                            </asp:GridView>
                                                            <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                                        </ItemTemplate>
                                                    </asp:TemplateField>
                                                </Columns>
                                                <AlternatingRowStyle CssClass="GridAltItem" />
                                                <FooterStyle CssClass="GridPager" />
                                                <HeaderStyle CssClass="GridHeader_Nested" />
                                                <PagerStyle CssClass="GridPager" />
                                                <RowStyle CssClass="GridItem" />
                                                <SelectedRowStyle CssClass="GridSelected" />
                                            </asp:GridView>
                                            <asp:Literal ID="Ltr_Fin" runat="server" Text="&lt;/td&gt;&lt;/tr&gt;" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:BoundField DataField="TIPO_DEUDOR" HeaderText="T.BENIOEFICIAR">
                                        <HeaderStyle HorizontalAlign="Left" Width="1%" />
                                        <ItemStyle HorizontalAlign="Left" Width="1%" />
                                    </asp:BoundField>
                                </Columns>
                                <AlternatingRowStyle CssClass="GridAltItem" />
                                <FooterStyle CssClass="GridPager" />
                                <HeaderStyle CssClass="GridHeader" />
                                <PagerStyle CssClass="GridPager" />
                                <RowStyle CssClass="GridItem" />
                                <SelectedRowStyle CssClass="GridSelected" />
                            </asp:GridView>
                                </div>
                            </center>                                       
                        </td>
                    </tr>
                    <tr>
                        <td>
                        &nbsp;
                        <asp:HiddenField ID="Txt_Monto_Solicitud" runat="server" />
                        <asp:HiddenField ID="Txt_Cuenta_Contable_ID_Proveedor" runat="server" />
                        <asp:HiddenField ID="Txt_Cuenta_Contable_reserva" runat="server" />
                        <asp:HiddenField ID="Txt_No_Reserva" runat="server" />
                            <asp:HiddenField ID="Txt_Rechazo" runat="server" />
                        </td>
                    </tr>
                </table>
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>                    
                    <%--<asp:Panel ID="Pnl_Detalles_Contenedor" runat="server" CssClass="drag"  HorizontalAlign="Center" Width="650px" 
                    style="display:none;border-style:outset;border-color:Silver;background-image:url(~/paginas/imagenes/paginas/Sias_Fondo_Azul.PNG);background-repeat:repeat-y;">
                        <asp:Panel ID="Pnl_Detalles" runat="server" 
                            style="cursor: move;background-color:Silver;color:Black;font-size:12;font-weight:bold;border-style:outset;">
                            <table width="99%">
                                <tr>
                                    <td style="color:Black;font-size:12;font-weight:bold;">
                                         DETALLES DE LA SOLICITUD 
                                    </td>
                                    <td align="right" style="width:10%;">
                                       <asp:ImageButton ID="Btn_Cerrar_Ventana" CausesValidation="false" runat="server" style="cursor:pointer;" ToolTip="Cerrar Ventana"
                                            ImageUrl="~/paginas/imagenes/paginas/Sias_Close.png" OnClientClick="javascript:return Cerrar_Modal_Popup_Detalles();"/>  
                                    </td>
                                </tr>
                            </table>
                        </asp:Panel>
                        <br />
                        <div style="color: #5D7B9D">
                             <table width="100%">
                                <tr>
                                    <td align="left" style="text-align: left;" >                                    
                                        <asp:UpdatePanel ID="Upnl_Detalles" runat="server">
                                            <ContentTemplate>
                                                <asp:UpdateProgress ID="Prg_Detalles" runat="server" AssociatedUpdatePanelID="Upnl_Detalles" DisplayAfter="0">
                                                    <ProgressTemplate>
                                                        <div id="progressBackgroundFilter" class="progressBackgroundFilter"></div>
                                                        <div  style="background-color:Transparent;position:fixed;top:50%;left:47%;padding:10px; z-index:1002;" id="div_progress">
                                                            <img alt="" src="../Imagenes/paginas/Sias_Roler.gif" />
                                                        </div>
                                                    </ProgressTemplate>
                                                </asp:UpdateProgress> 

                                                  <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                                    <tr>
                                                        <td style="width:25%;">No. Pago</td>
                                                        <td style="width:30%;"> 
                                                            <asp:TextBox ID="Txt_No_Pago_Det" runat="server" Width="99%" ReadOnly="true"/>
                                                        </td>
                                                        <td style="width:15%;">&nbsp;No. Reserva</td>
                                                        <td style="width:30%;"> 
                                                            <asp:TextBox ID="Txt_No_Reserva_Det" runat="server" Width="99%" ReadOnly="true" style="text-align:right"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Concepto Reserva</td>
                                                        <td colspan="3"> 
                                                            <asp:TextBox ID="Txt_Concepto_Reserva_Det" runat="server" Width="99%" ReadOnly="true" TextMode="MultiLine"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Beneficiario</td>
                                                        <td colspan="3"> 
                                                            <asp:TextBox ID="Txt_Beneficiario_Det" runat="server" Width="99%" ReadOnly="true"/>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>Fecha Solicitud Pago</td>
                                                        <td> 
                                                            <asp:TextBox ID="Txt_Fecha_Solicitud_Det" runat="server" Width="99%" ReadOnly="true"/>
                                                        </td>
                                                        <td> &nbsp;Monto</td>
                                                        <td> 
                                                            <asp:TextBox ID="Txt_Monto_Solicitud_Det" runat="server" Width="99%" style="text-align:right;" ReadOnly="true"/>
                                                        </td>
                                                    </tr>
                                                  </table>
                                                  
                                                  <table width="100%" style="border:outset 1px Silver;" class="button_autorizar">
                                                    <tr>
                                                        <td style="width:100%">
                                                             <asp:GridView ID="Grid_Documentos" runat="server" Width="100%"
                                                                    AutoGenerateColumns="False" CssClass="GridView_1" GridLines="None"
                                                                    OnRowDataBound="Grid_Documentos_RowDataBound"
                                                                    EmptyDataText="No se encuentra ningun documento">
                                                                    <Columns>
                                                                            <asp:TemplateField HeaderText="Link">
                                                                                <ItemTemplate>
                                                                                    <asp:HyperLink ID="Hyp_Lnk_Ruta" ForeColor="Blue" runat="server" >Archivo</asp:HyperLink>
                                                                                </ItemTemplate>
                                                                                <HeaderStyle HorizontalAlign ="Left" width ="7%" />
                                                                                <ItemStyle HorizontalAlign="Left" Width="7%" />
                                                                            </asp:TemplateField> 
                                                                            <asp:BoundField DataField="Partida" HeaderText="Partida" ItemStyle-Font-Size="X-Small">
                                                                               <HeaderStyle HorizontalAlign="Center" Width="63%" />
                                                                               <ItemStyle HorizontalAlign="Center" Width="63%" />
                                                                           </asp:BoundField>   
                                                                           <asp:BoundField DataField="MONTO_FACTURA" HeaderText="Monto" DataFormatString="{0:c}" ItemStyle-Font-Size="X-Small">
                                                                               <HeaderStyle HorizontalAlign="Right" Width="30%" />
                                                                               <ItemStyle HorizontalAlign="Right" Width="30%" />
                                                                           </asp:BoundField>
                                                                            <asp:BoundField DataField="Ruta" HeaderText="Ruta" ItemStyle-Font-Size="X-Small">
                                                                               <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                               <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                           <asp:BoundField DataField="Archivo" HeaderText="Archivo" ItemStyle-Font-Size="X-Small">
                                                                               <HeaderStyle HorizontalAlign="Left" Width="0%" />
                                                                               <ItemStyle HorizontalAlign="Left" Width="0%" />
                                                                           </asp:BoundField>
                                                                    </Columns>                                                    
                                                                    <SelectedRowStyle CssClass="GridSelected" />
                                                                    <PagerStyle CssClass="GridHeader" />
                                                                    <HeaderStyle CssClass="tblHead" />
                                                                    <AlternatingRowStyle CssClass="GridAltItem" />
                                                                </asp:GridView> 
                                                        </td>
                                                    </tr>
                                                </table>
                                            </ContentTemplate>
                                        </asp:UpdatePanel>
                                    </td>
                                </tr>
                                <tr><td>&nbsp;</td></tr>
                                
                                
                             </table>
                           </div>
                    </asp:Panel>--%>
</asp:Content>
