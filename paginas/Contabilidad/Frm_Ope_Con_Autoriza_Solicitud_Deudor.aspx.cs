﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using System.Collections.Generic;
using JAPAMI.Sessiones;
using JAPAMI.Constantes;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using CrystalDecisions.ReportSource;
using JAPAMI.Autoriza_Solicitud_Deudores.Negocio;

public partial class paginas_Contabilidad_Frm_Ope_Con_Autoriza_Solicitud_Deudor : System.Web.UI.Page
{
    #region "Page_Load"
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Page_Load
        /// DESCRIPCION : Carga la configuración inicial de los controles de la página.
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");
                
                if (!IsPostBack)
                {
                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    ViewState["SortDirection"] = "ASC";
                    Acciones();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion

    #region "Metodos"
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Inicializa_Controles
        /// DESCRIPCION : Prepara los controles en la forma para que el usuario pueda realizar
        ///               diferentes operaciones
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade 
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Inicializa_Controles()
        {
            try
            {
                Habilitar_Controles("Inicial"); //Habilita los controles de la forma para que el usuario pueda indica que operación desea realizar
                Limpia_Controles();             //Limpia los controles del forma
                Llenar_Grid_Solicitudes();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message.ToString());
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Habilitar_Controles
        /// DESCRIPCION : Habilita y Deshabilita los controles de la forma para prepara la página
        ///                para a siguiente operación
        /// PARAMETROS  : Operacion: Indica la operación que se desea realizar por parte del usuario
        ///                           si es una alta, modificacion
        ///                           
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Habilitar_Controles(String Operacion)
        {
            try
            {
                Boolean Habilitado;
                Habilitado = false;
                switch (Operacion)
                {
                    case "Inicial":
                        Habilitado = true;
                        Btn_Salir.ToolTip = "Salir";
                        Btn_Salir.ImageUrl = "~/paginas/imagenes/paginas/icono_salir.png";
                        Configuracion_Acceso("Frm_Ope_Con_Autoriza_Solicitud_Deudor.aspx");
                        break;
                }
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
            }
            catch (Exception ex)
            {
                throw new Exception("Habilitar_Controles " + ex.Message.ToString(), ex);
            }
        }

        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Limpiar_Controles
        /// DESCRIPCION : Limpia los controles que se encuentran en la forma
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade 
        /// FECHA_CREO  : 15/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Limpia_Controles()
        {
            try
            {
                Txt_Busqueda_No_Solicitud.Text = "";
                Txt_Monto_Solicitud.Value = "";
                Txt_No_Solicitud_Autorizar.Value = "";
            }
            catch (Exception ex)
            {
                throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes
        /// DESCRIPCION : Llena el grid Solicitudes de deudores
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 15/agosto/2012
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Llenar_Grid_Solicitudes()
        {
            try
            {
                Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Autoriza_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
                DataTable Dt_Resultado = new DataTable();
                //Rs_Autoriza_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                Dt_Resultado = Rs_Autoriza_Solicitud.Consulta_Solicitudes_SinAutorizar();
                
                Grid_Solicitud_Deudores.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                Grid_Solicitud_Deudores.DataBind();    // Se ligan los datos.;
                
                if (Dt_Resultado.Rows.Count > 0)
                {
                    Grid_Solicitud_Deudores.DataSource = Dt_Resultado;   // Se iguala el DataTable con el Grid
                    Grid_Solicitud_Deudores.DataBind();    // Se ligan los datos.;
                }
                //else
                //{
                //    ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitudes de Pago", "alert('En este momento no se tienen pagos pendientes por autorizar');", true);
                //}
            }
            catch (Exception ex)
            {
                throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
            }
        }
        // ****************************************************************************************
        //'NOMBRE DE LA FUNCION:Accion
        //'DESCRIPCION : realiza la modificacion la preautorización del pago o el rechazo de la solicitud de pago
        //'PARAMETROS  : 
        //'CREO        : Sergio Manuel Gallardo
        //'FECHA_CREO  : 07/Noviembre/2011 12:12 pm
        //'MODIFICO          :
        //'FECHA_MODIFICO    :
        //'CAUSA_MODIFICACION:
        //'****************************************************************************************
        protected void Acciones()
        {
            String Accion = String.Empty;
            String No_Solicitud = String.Empty;
            String Comentario = String.Empty;
            DataTable Dt_Partidas = new DataTable();
            DataTable Dt_Consulta_Estatus = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Solicitud_Pago = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();    //Objeto de acceso a los metodos.
            if (Request.QueryString["Accion"] != null)
            {
                Accion = HttpUtility.UrlDecode(Request.QueryString["Accion"].ToString());
                if (Request.QueryString["id"] != null)
                {
                    No_Solicitud = HttpUtility.UrlDecode(Request.QueryString["id"].ToString());
                }
                if (Request.QueryString["x"] != null)
                {
                    Comentario = HttpUtility.UrlDecode(Request.QueryString["x"].ToString());
                }
                //Response.Clear()
                switch (Accion)
                {
                    case "Autorizar_Solicitud":
                        Rs_Solicitud_Pago.P_No_Deuda = No_Solicitud;
                       // Rs_Solicitud_Pago.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
                        Dt_Consulta_Estatus = Rs_Solicitud_Pago.Consulta_Solicitudes_SinAutorizar();
                        if (Dt_Consulta_Estatus.Rows.Count > 0)
                        {
                            foreach (DataRow Registro in Dt_Consulta_Estatus.Rows)
                            {
                                if (Registro["Estatus"].ToString() == "PENDIENTE")
                                {
                                    Rs_Solicitud_Pago.P_Estatus = "AUTORIZADO";
                                    Rs_Solicitud_Pago.P_Fecha_Autoriza_Cancela = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                                    Rs_Solicitud_Pago.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado.ToString();
                                    Rs_Solicitud_Pago.Cambiar_Estatus_Solicitud();
                                }
                            }
                        }
                        break;
                    case "Rechazar_Solicitud":
                        Cancela_Solicitud_Pago(No_Solicitud, Comentario, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                        //Inicializa_Controles();
                        break;
                }
            }
        }
        ///*******************************************************************************
        /// NOMBRE DE LA FUNCION: Cancela_Solicitud_Pago
        /// DESCRIPCION : Modifica los datos de la Solicitud del Pago con los datos 
        ///               proporcionados por el usuario
        /// PARAMETROS  : 
        /// CREO        : Sergio Manuel Gallardo Andrade
        /// FECHA_CREO  : 24/Noviembre/2011
        /// MODIFICO          :
        /// FECHA_MODIFICO    :
        /// CAUSA_MODIFICACION:
        ///*******************************************************************************
        private void Cancela_Solicitud_Pago(String No_Solicitud_Pago,String Comentario, String Empleado_ID, String Nombre_Empleado)
        {
            Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio(); //Variable de conexión hacia la capa de Negocios
            //Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio Solicitud_Negocio = new Cls_Ope_Con_Autoriza_Solicitud_Pago_Negocio();
            DataTable Dt_Detalles = new DataTable();
            DataTable Dt_Datos_Polizas = new DataTable();
            Ds_Rpt_Con_Cancelacion Ds_Reporte = new Ds_Rpt_Con_Cancelacion();
            try
            {    //Agrega los valores a pasar a la capa de negocios para ser dados de alta
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_No_Deuda = No_Solicitud_Pago;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Estatus = "CANCELADO";
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Comentario = Comentario;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Fecha_Autoriza_Cancela  = String.Format("{0:dd/MM/yyyy}", DateTime.Today);
                Rs_Modificar_Ope_Con_Solicitud_Pagos.P_Usuario_Autorizo = Cls_Sessiones.Nombre_Empleado;
                Rs_Modificar_Ope_Con_Solicitud_Pagos.Cambiar_Estatus_Solicitud(); //Modifica el registro que fue seleccionado por el usuario con los nuevos datos proporcionados              
                //Inicializa_Controles(); //Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones



                //ScriptManager.RegisterStartupScript(this, this.GetType(), "Solicitud de Pagos", "alert('La Modificación de la Solicitud de Pago fue Exitosa');", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Modificar_Solicitud_Pago " + ex.Message.ToString(), ex);
            }
        }
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
        ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
        ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
        ///                             para mostrar los datos al usuario
        ///CREO       : Hugo Enrique Ramírez Aguilera
        ///FECHA_CREO  : 21-Febrero-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        private void Abrir_Ventana(String Nombre_Archivo)
        {
            String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
            try
            {
                Pagina = Pagina + Nombre_Archivo;
                ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
            }
            catch (Exception ex)
            {
                throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
            }
        }
    #endregion

        #region "Eventos"
        ///*******************************************************************************
        ///NOMBRE DE LA FUNCIÓN: Btn_Solicitud_Click
        ///DESCRIPCIÓN: manda llamar el metodo de la impresion de la caratula 
        ///PARÁMETROS :
        ///CREO       : Sergio Manuel Gallardo Andrade
        ///FECHA_CREO  : 21-mayo-2012
        ///MODIFICO          :
        ///FECHA_MODIFICO    :
        ///CAUSA_MODIFICACIÓN:
        ///******************************************************************************
        protected void Btn_Solicitud_Click(object sender, EventArgs e)
        {
            String No_Solicitud = ((LinkButton)sender).Text;
            //Imprimir(No_Solicitud);
        }

    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            if (Btn_Salir.ToolTip == "Salir")
            {
                Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
            }
            else
            {
                Inicializa_Controles();//Habilita los controles para la siguiente operación del usuario en el catálogo
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Cancelar_Click(object sender, EventArgs e)
    {
        Inicializa_Controles();
        Mpe_Busqueda.Hide();
    }
    protected void Btn_Comentar_Click(object sender, EventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Modificar_Ope_Con_Solicitud_Pagos= new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();

        try
        {
            DataTable Dt_Reporte = new DataTable();

            if (Txt_Comentario.Text != "")
            {
                Cancela_Solicitud_Pago(Txt_No_Solicitud_Autorizar.Value, Txt_Comentario.Text, Cls_Sessiones.Empleado_ID.ToString(), Cls_Sessiones.Nombre_Empleado.ToString());
                Inicializa_Controles();
            }
            else
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "Ingrese el comentario de la cancelación";

            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    protected void Btn_Buscar_No_Solicitud_Click(object sender, ImageClickEventArgs e)
    {
        Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio Rs_Consultar_Solicitud = new Cls_Ope_Con_Autoriza_Solicitud_Deudores_Negocio();
        DataTable Dt_Resultado = new DataTable();
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (!String.IsNullOrEmpty(Txt_Busqueda_No_Solicitud.Text))
            {
                Rs_Consultar_Solicitud.P_No_Deuda = String.Format("{0:0000000000}", Convert.ToDouble(Txt_Busqueda_No_Solicitud.Text));
            }
           // Rs_Consultar_Solicitud.P_Dependencia_ID = Cls_Sessiones.Dependencia_ID_Empleado;
            Dt_Resultado = Rs_Consultar_Solicitud.Consulta_Solicitudes_SinAutorizar();
            if (Dt_Resultado.Rows.Count <= 0)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = "No se encontro ninguna solicitud con la busqueda <br>";
                Txt_Busqueda_No_Solicitud.Focus();
                Grid_Solicitud_Deudores.DataSource = null;
                Grid_Solicitud_Deudores.DataBind();
            }
            else{
                Grid_Solicitud_Deudores.DataSource = null;
                Grid_Solicitud_Deudores.DataBind();
                Grid_Solicitud_Deudores.DataSource = Dt_Resultado;
                Grid_Solicitud_Deudores.DataBind();
                Txt_Busqueda_No_Solicitud.Text = "";
            }

        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    #endregion

    #region (Control Acceso Pagina)
    ///*******************************************************************************
    /// NOMBRE: Configuracion_Acceso
    /// DESCRIPCIÓN: Habilita las operaciones que podrá realizar el usuario en la página.
    /// PARÁMETROS  :
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// USUARIO MODIFICO  :
    /// FECHA MODIFICO    :
    /// CAUSA MODIFICACIÓN:
    ///*******************************************************************************
    protected void Configuracion_Acceso(String URL_Pagina)
    {
        List<ImageButton> Botones = new List<ImageButton>();//Variable que almacenara una lista de los botones de la página.
        DataRow[] Dr_Menus = null;//Variable que guardara los menus consultados.

        try
        {
            //Agregamos los botones a la lista de botones de la página.
            //Botones.Add(Btn_Nuevo);

            if (!String.IsNullOrEmpty(Request.QueryString["PAGINA"]))
            {
                if (Es_Numero(Request.QueryString["PAGINA"].Trim()))
                {
                    //Consultamos el menu de la página.
                    Dr_Menus = Cls_Sessiones.Menu_Control_Acceso.Select("MENU_ID=" + Request.QueryString["PAGINA"]);

                    if (Dr_Menus.Length > 0)
                    {
                        //Validamos que el menu consultado corresponda a la página a validar.
                        if (Dr_Menus[0][Apl_Cat_Menus.Campo_URL_Link].ToString().Contains(URL_Pagina))
                        {
                            Cls_Util.Configuracion_Acceso_Sistema_SIAS(Botones, Dr_Menus[0]);//Habilitamos la configuracón de los botones.
                        }
                        else
                        {
                            Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                        }
                    }
                    else
                    {
                        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                    }
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
            else
            {
                if (!String.IsNullOrEmpty(Request.QueryString["Accion"]))
                {
                }
                else
                {
                    Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al habilitar la configuración de accesos a la página. Error: [" + Ex.Message + "]");
        }
    }
    ///*******************************************************************************
    /// NOMBRE DE LA FUNCION: IsNumeric
    /// DESCRIPCION : Evalua que la cadena pasada como parametro sea un Numerica.
    /// PARÁMETROS: Cadena.- El dato a evaluar si es numerico.
    /// USUARIO CREÓ: Salvador L. Rea Ayala
    /// FECHA CREÓ  : 30/Septiembre/2011
    /// MODIFICO          :
    /// FECHA_MODIFICO    :
    /// CAUSA_MODIFICACION:
    ///*******************************************************************************
    private Boolean Es_Numero(String Cadena)
    {
        Boolean Resultado = true;
        Char[] Array = Cadena.ToCharArray();
        try
        {
            for (int index = 0; index < Array.Length; index++)
            {
                if (!Char.IsDigit(Array[index])) return false;
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al Validar si es un dato numerico. Error [" + Ex.Message + "]");
        }
        return Resultado;
    }
    #endregion
       
}
