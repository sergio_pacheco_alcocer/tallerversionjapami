﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using CrystalDecisions.Web;
using JAPAMI.Constantes;
using JAPAMI.Sessiones;
using JAPAMI.Cuentas_Contables.Negocio;
using JAPAMI.Polizas.Negocios;
using JAPAMI.Cheques_Bancos.Negocio;
using JAPAMI.Reporte_Basicos_Contabilidad.Negocio;
using JAPAMI.Bancos_Nomina.Negocio;
using System.Data.OleDb;
using AjaxControlToolkit;
using System.IO;
public partial class paginas_Contabilidad_Frm_Rpt_Con_Cheques_Por_Fecha : System.Web.UI.Page
{
    #region (Load/Init)
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                //Refresca la session del usuario lagueado al sistema.
                Response.AddHeader("Refresh", Convert.ToString((Session.Timeout * 60) + 5));
                //Valida que exista algun usuario logueado al sistema.
                if (Cls_Sessiones.Nombre_Empleado == null || Cls_Sessiones.Nombre_Empleado.Equals(String.Empty)) Response.Redirect("../Paginas_Generales/Frm_Apl_Login.aspx");

                if (!IsPostBack)
                {

                    Inicializa_Controles();//Inicializa los controles de la pantalla para que el usuario pueda realizar las siguientes operaciones
                    DateTime _DateTime = DateTime.Now;
                    Txt_Fecha_Inicio.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                    Txt_Fecha_Final.Text = DateTime.Now.ToString("dd/MMM/yyyy").ToUpper();
                }
            }
            catch (Exception ex)
            {
                Lbl_Mensaje_Error.Visible = true;
                Img_Error.Visible = true;
                Lbl_Mensaje_Error.Text = ex.Message.ToString();
            }
        }
    #endregion
    #region (Metodos)
        #region (Métodos Generales)
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Inicializa_Controles
            /// DESCRIPCION : Inicializa los controles de la forma para prepararla para el
            ///               reporte
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Inicializa_Controles()
            {
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Distintos_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                DataTable Dt_Existencia = new DataTable();
                try
                {
                    Habilitar_Forma(false);
                    Limpia_Controles(); //limpia los campos de la forma
                    Dt_Existencia = Rs_Distintos_Bancos.Consultar_Bancos_Existentes();
                    Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Banco, Dt_Existencia, "NOMBRE", "NOMBRE");
                }
                catch (Exception ex)
                {
                    throw new Exception("Inicializa_Controles " + ex.Message.ToString(), ex);
                }
            }
     ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Habilitar_Forma
        ///DESCRIPCIÓN          : Metodo para habilitar o deshabilitar los controles de la pagina
        ///PROPIEDADES          1 Estatus true o false para habilitar los controles
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 27/Marzo/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Habilitar_Forma(Boolean Estatus)
        {
            try
            {
                AFU_Archivo.Enabled=Estatus;
                Btn_Fecha_Inicio.Enabled=Estatus;
                Btn_Fecha_Final.Enabled=Estatus;
                Btn_Cargar_Archivo.Enabled=Estatus;
                Cmb_Banco.Enabled = Estatus;
                Cmb_Cuenta.Enabled = Estatus;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al Habilitar_Forma ERROR[" + ex.Message + "]");
            }
        }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Limpiar_Controles
            /// DESCRIPCION : Limpia los controles que se encuentran en la forma
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Limpia_Controles()
            {
                try
                {
                    Session["Archivo"] = null;
                    Cmb_Banco.SelectedIndex = -1;
                    Cmb_Cuenta.SelectedIndex = -1;
                    Txt_Fecha_Final.Enabled = false;
                    Txt_Fecha_Inicio.Enabled = false;
                }
                catch (Exception ex)
                {
                    throw new Exception("Limpia_Controles " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Validar_Datos_Reporte
            /// DESCRIPCION : Validar que se hallan proporcionado todos los datos para la
            ///               generación del reporte
            /// CREO        : Yazmin A Delgado Gómez
            /// FECHA_CREO  : 08-Octubre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private Boolean Validar_Datos_Reporte()
            {
                Boolean Datos_Validos = true;//Variable que almacena el valor de true si todos los datos fueron ingresados de forma correcta, o false en caso contrario.
                DataTable Dt_Temporal = new DataTable();
                Dt_Temporal = ((DataTable)(Session["Archivo"]));
                Lbl_Mensaje_Error.Text = "Es necesario Introducir: <br>";
                if (Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()).CompareTo(Convert.ToDateTime(Txt_Fecha_Final.Text.Trim())) > 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La fecha inicial no puede ser mayor a la fecha final <br>";
                    Datos_Validos = false;
                }
                if (Cmb_Cuenta.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+La Cuenta para realizar la conciliacion <br>";
                    Datos_Validos = false;
                }
                if (Cmb_Banco.SelectedIndex <= 0)
                {
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+El banco al que se le realizara la conciliacion <br>";
                    Datos_Validos = false;
                }
                if(Dt_Temporal.Rows.Count<=0){
                    Lbl_Mensaje_Error.Text += "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;+El archivo de conciliaciones<br>";
                    Datos_Validos = false;
                }
                return Datos_Validos;
            }
        #endregion
        #region (Consultas)
            protected void AFU_Archivo_Excel_UploadedComplete(object sender, AsyncFileUploadEventArgs e)
            {
                //****************************************
                //CUANDO LA CARGA DEL ARCHIVO TERMINA LO
                //GUARDA EN LA UBICACION PRESTABLECIDA
                //****************************************
                try
                {
                    if (AFU_Archivo.HasFile)
                    {
                        string currentDateTime = String.Format("{0:yyyy-MM-dd_HH.mm.sstt}", DateTime.Now);
                        string fileNameOnServer = System.IO.Path.GetFileName(AFU_Archivo.FileName).Replace(" ", "_");

                        bool xls = Path.GetExtension(AFU_Archivo.PostedFile.FileName).Contains(".xls");
                        bool xlsx = Path.GetExtension(AFU_Archivo.PostedFile.FileName).Contains(".xlsx");

                        if (xls || xlsx)
                            AFU_Archivo.SaveAs(@"C:/Polizas/" + fileNameOnServer);
                        else
                            return;
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Consulta_Cheques_Emitidos
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Yazmin Abigail Delgado Gómez
            /// FECHA_CREO  : 08-Noviembre-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consulta_Cheques_Emitidos()
            {
                String Ruta_Archivo = @Server.MapPath("../Rpt/Contabilidad/");//Obtiene la ruta en la cual será guardada el archivo
                String Nombre_Archivo = "Cheques_Emitidos" + Session.SessionID + Convert.ToString(String.Format("{0:ddMMMyyyHHmmss}", DateTime.Today)); //Obtiene el nombre del archivo que sera asignado al documento
                DataTable Dt_Cheques = new DataTable();       //Va a conter los valores de la consulta realizada
                Ds_Rpt_Con_Cheques_Emitidos Ds_Cheques_Emitidos = new Ds_Rpt_Con_Cheques_Emitidos();
                Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Consulta_Ope_Con_Cehques = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio(); //Conexion hacia la capa de negocios
                
                try
                {
                    if (Cmb_Banco.SelectedIndex > 0)
                    {
                        Rs_Consulta_Ope_Con_Cehques.P_Nombre_Banco = Cmb_Banco.SelectedItem.Text.ToString();
                    }
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
                    Rs_Consulta_Ope_Con_Cehques.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));
                    Dt_Cheques = Rs_Consulta_Ope_Con_Cehques.Consulta_Cheques_Banco_Fecha(); //Consulta los valores de la cuenta del mes y el año que selecciono el usuario
                    if (Dt_Cheques.Rows.Count > 0)
                    {
                        Dt_Cheques.TableName = "Dt_Cheques_Emitidos";
                        Ds_Cheques_Emitidos.Clear();
                        Ds_Cheques_Emitidos.Tables.Clear();
                        Ds_Cheques_Emitidos.Tables.Add(Dt_Cheques.Copy());
                    }
                    ReportDocument Reporte = new ReportDocument();
                    Reporte.Load(Ruta_Archivo + "Rpt_Con_Cheques_Emitidos.rpt");
                    Reporte.SetDataSource(Ds_Cheques_Emitidos);

                    ParameterFieldDefinitions Cr_Parametros;
                    ParameterFieldDefinition Cr_Parametro;
                    ParameterValues Cr_Valor_Parametro = new ParameterValues();
                    ParameterDiscreteValue Cr_Valor = new ParameterDiscreteValue();

                    Cr_Parametros = Reporte.DataDefinition.ParameterFields;

                    Cr_Parametro = Cr_Parametros["Fecha_Inicial"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = String.Format("{0:dd/mm/yyyy}",Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    Cr_Parametro = Cr_Parametros["Fecha_Final"];
                    Cr_Valor_Parametro = Cr_Parametro.CurrentValues;
                    Cr_Valor_Parametro.Clear();

                    Cr_Valor.Value = String.Format("{0:dd/mm/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.ToString()));
                    Cr_Valor_Parametro.Add(Cr_Valor);
                    Cr_Parametro.ApplyCurrentValues(Cr_Valor_Parametro);

                    DiskFileDestinationOptions m_crDiskFileDestinationOptions = new DiskFileDestinationOptions();

                    Nombre_Archivo += ".pdf";
                    Ruta_Archivo = @Server.MapPath("../../Reporte/");
                    m_crDiskFileDestinationOptions.DiskFileName = Ruta_Archivo + Nombre_Archivo;

                    ExportOptions Opciones_Exportacion = new ExportOptions();
                    Opciones_Exportacion.ExportDestinationOptions = m_crDiskFileDestinationOptions;
                    Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                    Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                    Reporte.Export(Opciones_Exportacion);

                    Abrir_Ventana(Nombre_Archivo);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Realizar_Conciliacion
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 24-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Realizar_Conciliacion()
            {
                DataTable Dt_Movimientos = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Conciliados = new DataTable(); //Obtiene los valores a pasar al reporte
                DataTable Dt_Cuentas = new DataTable();
                DataTable Dt_Cuentas_Contables = new DataTable();
                DataTable Dt_Cuenta_Con_Cuenta_contable = new DataTable();
                Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Ope_Con_Cehques = new Cls_Ope_Con_Polizas_Negocio(); //Conexion hacia la capa de negocios
                Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Bancos = new Cls_Ope_Con_Cheques_Bancos_Negocio();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Dt_Movimientos = ((DataTable)(Session["Archivo"]));
                Boolean Insertar = true;
                DataRow Fil;
                try
                {
                    if (Dt_Cuentas.Rows.Count <= 0 && Dt_Cuentas.Columns.Count <= 0)
                    {
                        Dt_Cuentas.Columns.Add("NUMERO_CUENTA", System.Type.GetType("System.String"));
                    }
                    foreach (DataRow registro in Dt_Movimientos.Rows)
                    {
                        if (Dt_Cuentas.Rows.Count == 0)
                        {
                            Fil = Dt_Cuentas.NewRow();
                            Fil["NUMERO_CUENTA"] = registro["NUMERO_CUENTA"].ToString();
                            Dt_Cuentas.Rows.Add(Fil);
                            Dt_Cuentas.AcceptChanges();
                        }
                        else
                        {
                            Insertar=true;
                            foreach (DataRow Filas in Dt_Cuentas.Rows)
                            {
                                if (registro["NUMERO_CUENTA"].ToString() == Filas["NUMERO_CUENTA"].ToString())
                                {
                                    Insertar = false;
                                }
                            }
                            if (Insertar)
                            {
                                Fil = Dt_Cuentas.NewRow();
                                Fil["NUMERO_CUENTA"] = registro["NUMERO_CUENTA"].ToString();
                                Dt_Cuentas.Rows.Add(Fil);
                                Dt_Cuentas.AcceptChanges();
                            }
                        }
                    }
                    foreach (DataRow Row in Dt_Cuentas.Rows)
                    {
                        Rs_Bancos.P_No_Cuenta_Banco = Row["NUMERO_CUENTA"].ToString();
                       Dt_Cuentas_Contables= Rs_Bancos.Consultar_Bancos();
                       if (Dt_Cuentas_Contables.Rows.Count > 0)
                       {
                           if (Dt_Cuenta_Con_Cuenta_contable.Rows.Count <= 0 && Dt_Cuenta_Con_Cuenta_contable.Columns.Count <= 0)
                                {
                                    Dt_Cuenta_Con_Cuenta_contable.Columns.Add("NUMERO_CUENTA", System.Type.GetType("System.String"));
                                    Dt_Cuenta_Con_Cuenta_contable.Columns.Add("CUENTA_CONTABLE_ID", System.Type.GetType("System.String"));
                                }
                           Fil = Dt_Cuenta_Con_Cuenta_contable.NewRow();
                           Fil["NUMERO_CUENTA"] = Row["NUMERO_CUENTA"].ToString();
                           Fil["CUENTA_CONTABLE_ID"] = Dt_Cuentas_Contables.Rows[0]["CUENTA_CONTABLE_ID"];
                           Dt_Cuenta_Con_Cuenta_contable.Rows.Add(Fil);
                           Dt_Cuenta_Con_Cuenta_contable.AcceptChanges();
                       }
                    }
                    Consultar_Polizas(Dt_Cuenta_Con_Cuenta_contable);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Realizar_Conciliacion
            /// DESCRIPCION : Consulta los movimientos que ha tenido la cuenta que fue seleccionada
            ///               por el usuario desde el primer mes hasta el mes seleccionado
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 24-Mayo-2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Consultar_Polizas(DataTable Dt_Cuentas_ID)
            {
                DataTable Dt_Registros_Encontrados = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Registros_NO_Encontrados = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Movimientos = new DataTable();       //Va a conter los valores de la consulta realizada
                DataTable Dt_Resultado = new DataTable();       //Va a conter los valores de la consulta realizada
                Cls_Ope_Con_Polizas_Negocio Rs_Consulta_Polizas = new Cls_Ope_Con_Polizas_Negocio();
                Lbl_Mensaje_Error.Visible = false;
                Img_Error.Visible = false;
                Dt_Movimientos = ((DataTable)(Session["Archivo"]));
                Boolean Insertar = true;
                DataRow Fil;
                DataRow Registro;
                try
                {
                    //tabla con  enconytrados
                    Dt_Registros_Encontrados.Columns.Add("NUMERO_CUENTA", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("FECHA_INGRESO", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("FECHA_VALOR", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("REFERENCIA_CLIENTE", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("REFERENCIA_BANCO", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("MONTO", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("NO_POLIZA", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("TIPO_POLIZA_ID", System.Type.GetType("System.String"));
                    Dt_Registros_Encontrados.Columns.Add("MES_ANO", System.Type.GetType("System.String"));
                    //tabla con  no enconytrados
                    Dt_Registros_NO_Encontrados.Columns.Add("NUMERO_CUENTA", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("FECHA_INGRESO", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("FECHA_VALOR", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("REFERENCIA_CLIENTE", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("REFERENCIA_BANCO", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
                    Dt_Registros_NO_Encontrados.Columns.Add("MONTO", System.Type.GetType("System.String"));

                    //foreach (DataRow Row in Dt_Cuentas_ID.Rows)
                    //{
                        //Dt_Movimientos.DefaultView.RowFilter="NUMERO_CUENTAlike'"+Row["NUMERO_CUENTA"].ToString()+"'";
                    if(Cmb_Cuenta.SelectedItem.Text.ToString().Substring(0,1)==" "){
                         Dt_Cuentas_ID.DefaultView.RowFilter = "NUMERO_CUENTA LIKE '%" + Cmb_Cuenta.SelectedItem.Text.ToString().Substring(1, Cmb_Cuenta.SelectedItem.Text.ToString().Substring(1).IndexOf(' ')) + "%'";
                         Dt_Movimientos.DefaultView.RowFilter = "NUMERO_CUENTA LIKE '%" + Cmb_Cuenta.SelectedItem.Text.ToString().Substring(1, Cmb_Cuenta.SelectedItem.Text.ToString().Substring(1).IndexOf(' ')) + "%'";
                    
                    }else{
                         Dt_Cuentas_ID.DefaultView.RowFilter = "NUMERO_CUENTA LIKE '%" + Cmb_Cuenta.SelectedItem.Text.ToString().Substring(0, Cmb_Cuenta.SelectedItem.Text.ToString().IndexOf(' ')) + "%'";
                         Dt_Movimientos.DefaultView.RowFilter = "NUMERO_CUENTA LIKE '%" + Cmb_Cuenta.SelectedItem.Text.ToString().Substring(0, Cmb_Cuenta.SelectedItem.Text.ToString().IndexOf(' ')) + "%'";
                    }
                       if (Dt_Movimientos.DefaultView.ToTable().Rows.Count > 0)
                        {
                            foreach (DataRow Renglon in Dt_Movimientos.DefaultView.ToTable().Rows)
                            {
                                Rs_Consulta_Polizas.P_Cuenta_Contable_ID = Dt_Cuentas_ID.DefaultView.ToTable().Rows[0]["CUENTA_CONTABLE_ID"].ToString();
                                Rs_Consulta_Polizas.P_Referencia = Renglon["REFERENCIA_CLIENTE"].ToString();
                                Rs_Consulta_Polizas.P_Fecha_Inicial= String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(Txt_Fecha_Inicio.Text.ToString()));
                                 Rs_Consulta_Polizas.P_Fecha_Final =String.Format("{0:MM/dd/yyyy}",Convert.ToDateTime(Txt_Fecha_Final.Text.ToString()));
                                 Dt_Resultado= Rs_Consulta_Polizas.Consulta_Detalles_Poliza_Por_Referencia();
                                 if (Dt_Resultado.Rows.Count > 0)
                                 {
                                     Fil = Dt_Registros_Encontrados.NewRow();
                                     Fil["NUMERO_CUENTA"] = Renglon["NUMERO_CUENTA"].ToString();
                                     Fil["FECHA_INGRESO"] = Renglon["FECHA_INGRESO"].ToString();
                                     Fil["FECHA_VALOR"] = Renglon["FECHA_VALOR"].ToString();
                                     Fil["REFERENCIA_CLIENTE"] = Renglon["REFERENCIA_CLIENTE"].ToString();
                                     Fil["REFERENCIA_BANCO"] = Renglon["REFERENCIA_BANCO"].ToString();
                                     Fil["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                                     Fil["MONTO"] = Renglon["MONTO"].ToString();
                                     Fil["NO_POLIZA"] = Dt_Resultado.Rows[0]["NO_POLIZA"].ToString();
                                     Fil["TIPO_POLIZA_ID"] = Dt_Resultado.Rows[0]["TIPO_POLIZA_ID"].ToString();
                                     Fil["MES_ANO"] = Dt_Resultado.Rows[0]["MES_ANO"].ToString();
                                     Dt_Registros_Encontrados.Rows.Add(Fil);
                                     Dt_Registros_Encontrados.AcceptChanges();
                                 }
                                 else
                                 {
                                     Registro = Dt_Registros_NO_Encontrados.NewRow();
                                     Registro["NUMERO_CUENTA"] = Renglon["NUMERO_CUENTA"].ToString();
                                     Registro["FECHA_INGRESO"] = Renglon["FECHA_INGRESO"].ToString();
                                     Registro["FECHA_VALOR"] = Renglon["FECHA_VALOR"].ToString();
                                     Registro["REFERENCIA_CLIENTE"] = Renglon["REFERENCIA_CLIENTE"].ToString();
                                     Registro["REFERENCIA_BANCO"] = Renglon["REFERENCIA_BANCO"].ToString();
                                     Registro["DESCRIPCION"] = Renglon["DESCRIPCION"].ToString();
                                     Registro["MONTO"] = Renglon["MONTO"].ToString();
                                     Dt_Registros_NO_Encontrados.Rows.Add(Registro);
                                     Dt_Registros_NO_Encontrados.AcceptChanges();
                                 }
                            }
                        }
                    //}
                       Llenar_Grids(Dt_Registros_NO_Encontrados, Dt_Registros_Encontrados);
                }
                catch (Exception ex)
                {
                    throw new Exception("Consulta_Libro_Mayor " + ex.Message.ToString(), ex);
                }
            }
            ///*******************************************************************************
            /// NOMBRE DE LA FUNCION: Llenar_Grid_Solicitudes_Pago
            /// DESCRIPCION : Llena el grid Solicitudes de pago
            /// PARAMETROS  : 
            /// CREO        : Sergio Manuel Gallardo Andrade
            /// FECHA_CREO  : 15/noviembre/2011
            /// MODIFICO          :
            /// FECHA_MODIFICO    :
            /// CAUSA_MODIFICACION:
            ///*******************************************************************************
            private void Llenar_Grids( DataTable Dt_No_Encontrados, DataTable Dt_Encontrados)
            {
                try
                {
                    Grid_Cuentas_Identificadas.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                    Grid_Cuentas_Identificadas.DataBind();    // Se ligan los datos.;
                    Grid_Cuentas_No_Identificadas.DataSource = new DataTable();   // Se iguala el DataTable con el Grid
                    Grid_Cuentas_No_Identificadas.DataBind();    // Se ligan los datos.;
                    if (Dt_Encontrados.Rows.Count>0)
                    {
                        Grid_Cuentas_Identificadas.DataSource = Dt_Encontrados;   // Se iguala el DataTable con el Grid
                        Grid_Cuentas_Identificadas.DataBind();    // Se ligan los datos.;
                    }
                    if (Dt_No_Encontrados.Rows.Count > 0)
                    {
                        Grid_Cuentas_No_Identificadas.DataSource = Dt_No_Encontrados;   // Se iguala el DataTable con el Grid
                        Grid_Cuentas_No_Identificadas.DataBind();    // Se ligan los datos.;
                    }
                }
                catch (Exception ex)
                {
                    throw new Exception("Llena_Grid_Meses estatus " + ex.Message.ToString(), ex);
                }
            }

            ////////'****************************************************************************************
            ////////'NOMBRE DE LA FUNCION: Grid_Proveedores_RowDataBound
            ////////'DESCRIPCION : llena los grid anidados que se encuentran en el grid de grupos documentos
            ////////'PARAMETROS  : 
            ////////'CREO        : Sergio Manuel Gallardo
            ////////'FECHA_CREO  : 16/julio/2011 10:01 am
            ////////'MODIFICO          :
            ////////'FECHA_MODIFICO    :
            ////////'CAUSA_MODIFICACION:
            ////////'****************************************************************************************
            //////protected void Grid_Cuentas_RowDataBound(object sender, GridViewRowEventArgs e)
            //////{
            //////    GridView Gv_Detalles = new GridView();
            //////    DataTable Dt_Datos_Detalles = new DataTable();
            //////    DataTable Ds_Consulta = new DataTable();
            //////    DataTable Ds_Consulta_Modificada = new DataTable();
            //////    Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio Rs_Cheques_Movimientos = new Cls_Rpt_Con_Reporte_Basicos_Contabilidad_Negocio();
            //////    String Banco = "";
            //////    int Contador;
            //////    Image Img = new Image();
            //////    Img = (Image)e.Row.FindControl("Img_Btn_Expandir");
            //////    Literal Lit = new Literal();
            //////    Lit = (Literal)e.Row.FindControl("Ltr_Inicio");
            //////    Label Lbl_facturas = new Label();
            //////    Lbl_facturas = (Label)e.Row.FindControl("Lbl_Movimientos");
            //////    try
            //////    {
            //////        if (e.Row.RowType.Equals(DataControlRowType.DataRow))
            //////        {
            //////            Contador = (int)(Session["Contador"]);
            //////            Lit.Text = Lit.Text.Trim().Replace("Renglon_Grid", ("Renglon_Grid" + Lbl_facturas.Text));
            //////            Img.Attributes.Add("OnClick", ("Mostrar_Tabla(\'Renglon_Grid"
            //////                            + (Lbl_facturas.Text + ("\',\'"
            //////                            + (Img.ClientID + "\')")))));
            //////            Dt_Datos_Detalles = ((DataTable)(Session["Dt_Datos_Detalles"]));
            //////            Banco = Convert.ToString(Dt_Datos_Detalles.Rows[Contador]["NOMBRE"].ToString());
            //////            Rs_Cheques_Movimientos.P_Nombre_Banco = Banco;
            //////            Rs_Cheques_Movimientos.P_Fecha_Inicial = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Inicio.Text.Trim()));
            //////            Rs_Cheques_Movimientos.P_Fecha_Final = String.Format("{0:dd/MM/yyyy}", Convert.ToDateTime(Txt_Fecha_Final.Text.Trim()));                    
            //////            Ds_Consulta = Rs_Cheques_Movimientos.Consulta_Cheques_Banco_Fecha();
            //////            if (Ds_Consulta.Rows.Count > 0)
            //////            {
            //////                if (Ds_Consulta_Modificada.Rows.Count <= 0)
            //////                {
            //////                    Ds_Consulta_Modificada.Columns.Add("NO_PAGO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("NO_POLIZA", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("TIPO_POLIZA_ID", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("MES_ANO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("FECHA_PAGO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("BENEFICIARIO_PAGO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("NO_CHEQUE", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("USUARIO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("NOMBRE", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("NO_SOLICITUD_PAGO", typeof(System.String));
            //////                    Ds_Consulta_Modificada.Columns.Add("MONTO", typeof(System.Double));
            //////                }
            //////                foreach (DataRow Fila in Ds_Consulta.Rows)
            //////                {
            //////                    DataRow row = Ds_Consulta_Modificada.NewRow(); //Crea un nuevo registro a la tabla
            //////                    //Asigna los valores al nuevo registro creado a la tabla
            //////                    row["NO_PAGO"] = Fila["NO_PAGO"].ToString();
            //////                    row["NO_POLIZA"] = Fila["NO_POLIZA"].ToString();
            //////                    row["TIPO_POLIZA_ID"] = Fila["TIPO_POLIZA_ID"].ToString();
            //////                    row["MES_ANO"] = Fila["MES_ANO"].ToString();
            //////                    row["FECHA_PAGO"] = String.Format("{0:dd/MMM/yyyy}",Convert.ToDateTime(Fila["FECHA_PAGO"].ToString()));
            //////                    if (Fila["BENEFICIARIO_PAGO"].ToString().Contains('-'))
            //////                    {
            //////                        row["BENEFICIARIO_PAGO"] = Fila["BENEFICIARIO_PAGO"].ToString().Substring(Fila["BENEFICIARIO_PAGO"].ToString().IndexOf('-')+1);
            //////                    }
            //////                    else
            //////                    {
            //////                        row["BENEFICIARIO_PAGO"] = Fila["BENEFICIARIO_PAGO"].ToString();
            //////                    }
            //////                    row["NO_CHEQUE"] = Fila["NO_CHEQUE"].ToString();
            //////                    row["USUARIO"] = Fila["USUARIO_CREO"].ToString();
            //////                    row["NOMBRE"] = Fila["NOMBRE"].ToString();
            //////                    row["MONTO"] = Fila["MONTO"].ToString();
            //////                    row["NO_SOLICITUD_PAGO"] = Fila["NO_SOLICITUD_PAGO"].ToString();
            //////                    Ds_Consulta_Modificada.Rows.Add(row); //Agrega el registro creado con todos sus valores a la tabla
            //////                    Ds_Consulta_Modificada.AcceptChanges();
            //////                }
            //////            }
            //////            Gv_Detalles = (GridView)e.Row.Cells[2].FindControl("Grid_Movimientos");
            //////            Gv_Detalles.DataSource = Ds_Consulta_Modificada;
            //////            Gv_Detalles.DataBind();
            //////            Session["Contador"] = Contador + 1;
            //////        }
            //////    }
            //////    catch (Exception Ex)
            //////    {
            //////        throw new Exception(Ex.Message);
            //////    }
            //////}
            ///*******************************************************************************
            ///NOMBRE DE LA FUNCIÓN: Abrir_Ventana
            ///DESCRIPCIÓN: Abre en otra ventana el archivo pdf
            ///PARÁMETROS : Nombre_Archivo: Guarda el nombre del archivo que se desea abrir
            ///                             para mostrar los datos al usuario
            ///CREO       : Yazmin A Delgado Gómez
            ///FECHA_CREO : 12-Octubre-2011
            ///MODIFICO          :
            ///FECHA_MODIFICO    :
            ///CAUSA_MODIFICACIÓN:
            ///******************************************************************************
            private void Abrir_Ventana(String Nombre_Archivo)
            {
                String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";
                try
                {
                    Pagina = Pagina + Nombre_Archivo;
                    ScriptManager.RegisterStartupScript(this, this.GetType(), "Window_Rpt",
                    "window.open('" + Pagina + "', 'Reporte','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
                }
                catch (Exception ex)
                {
                    throw new Exception("Abrir_Ventana " + ex.Message.ToString(), ex);
                }
            }
        #endregion
    #endregion
    /// *************************************************************************************
    /// NOMBRE:             Btn_Nuevo_Click
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    /// PARÁMETROS:
    /// USUARIO CREO:       Sergio Manuel Gallardo Andrade
    /// FECHA CREO:         24/Mayo/2012
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    protected void Btn_Nuevo_Click(object sender, ImageClickEventArgs e)
    {
        try
        {
            Lbl_Mensaje_Error.Visible = false;
            Img_Error.Visible = false;
            if (Btn_Nuevo.ToolTip=="Nuevo")
            {
                Limpia_Controles(); //Consulta el libro de mayor de la cuenta seleccionada
                Habilitar_Forma(true);
                Btn_Nuevo.ToolTip="Crear";
            }
            else
            {
                if (Validar_Datos_Reporte())
                {
                    Realizar_Conciliacion();
                }
            }
        }
        catch (Exception ex)
        {
            Lbl_Mensaje_Error.Visible = true;
            Img_Error.Visible = true;
            Lbl_Mensaje_Error.Text = ex.Message.ToString();
        }
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Btn_Cargar_Excel_Click
    ///DESCRIPCIÓN          : Evento del boton de cargar excel
    ///PROPIEDADES          :
    ///CREO                 : Leslie González Vázquez
    ///FECHA_CREO           : 21/Abril/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Btn_Cargar_Excel_Click(object sender, EventArgs e)
    {
        DataTable Dt_Registros = new DataTable();
        String Ruta_Destino = String.Empty;
        String Extension = String.Empty;
        String Archivo = String.Empty;
        DataTable Dt_Registos = new DataTable();

        try
        {
            //Ruta_Destino = Server.MapPath("~") + "\\Archivos";
            //Ruta_Destino = Server.MapPath("..\\..\\Archivos\\");
            if (!String.IsNullOrEmpty(AFU_Archivo.PostedFile.FileName))
            {
                int Caracter_Inicio = 0;    //Posicion del caracter de inicio del nombre del archivo.

                for (int Cont_Caracter = 0; Cont_Caracter < AFU_Archivo.PostedFile.FileName.Length; Cont_Caracter++)
                {
                    if (AFU_Archivo.PostedFile.FileName[Cont_Caracter] == Convert.ToChar(92))
                    {
                        Caracter_Inicio = Cont_Caracter + 1;
                    }
                }

                Ruta_Destino = "C:\\Polizas\\" + AFU_Archivo.PostedFile.FileName.Substring(Caracter_Inicio, AFU_Archivo.PostedFile.FileName.Length - Caracter_Inicio);

                Archivo = AFU_Archivo.PostedFile.FileName.Substring(Caracter_Inicio, AFU_Archivo.PostedFile.FileName.Length - Caracter_Inicio);
                Extension = Archivo.Substring(Archivo.IndexOf(".") + 1);

                if (Extension.Trim().Equals("xls") || Extension.Trim().Equals("xlsx"))
                {
                    if (System.IO.Directory.Exists(Ruta_Destino))
                    {
                        System.IO.Directory.CreateDirectory(Ruta_Destino);
                    }
                    //Archivo = AFU_Archivo.FileName;
                    Extension = Archivo.Substring(Archivo.IndexOf(".") + 1);
                    //AFU_Archivo.SaveAs(Ruta_Destino + Archivo);
                    Cargar_Excel(Ruta_Destino , Extension);
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception("Error al tratar de cargar los datos del pronostico ERROR[" + ex.Message + "]");
        }
    }//fin de Modificar
     ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Cargar_Excel
        ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private void Cargar_Excel(String Ruta, String Extension)
        {
            DataTable Dt_Registros = new DataTable();
            try
            {
                Session["Archivo"] = null;
                Dt_Registros = Leer_Excel(Ruta, Extension);
                Dt_Registros = Crear_Dt_Registros_Carga_Excel(Dt_Registros);
                Session["Archivo"] = Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
        }
     ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Leer_Excel
        ///DESCRIPCIÓN          : Metodo para cargar los datos del excel 
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Leer_Excel(String Ruta, String Extension)
        {
            DataTable Dt_Informacion = new DataTable();
            DataSet Ds_Datos = new DataSet();
            OleDbDataAdapter Adaptador = new OleDbDataAdapter();
            OleDbConnection Conexion = new OleDbConnection();
            OleDbCommand Comando = new OleDbCommand();
            String SqlExcel = "Select * From [Sheet2$]";
            try
            {
                string sConnectionString = "";// @"Provider=Microsoft.Jet.OLEDB.4.0;Data Source=" + Rta + ";Extended Properties=Excel 8.0;";
                if (Extension.Equals(".xlsx"))       // Formar la cadena de conexion si el archivo es Exceml xml
                {
                    sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                            "Data Source=" + Ruta + ";" +
                            "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                }
                else if (Extension.Equals("xls"))   // Formar la cadena de conexion si el archivo es Exceml binario
                {
                    sConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                            "Data Source=" + Ruta + ";" +
                            "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                    //sConnectionString = @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                    //        "Data Source=" + Rta + ";" +
                    //        "Extended Properties=Excel 8.0;";
                }

                //Definimos el DataSet donde insertaremos los datos que leemos del excel
                DataSet DS = new DataSet();

                //Definimos la conexión OleDb al fichero Excel y la abrimos
                OleDbConnection oledbConn = new OleDbConnection(sConnectionString);
                oledbConn.Open();

                //Creamos un comand para ejecutar la sentencia SELECT.
                OleDbCommand oledbCmd = new OleDbCommand(SqlExcel, oledbConn);

                //Creamos un dataAdapter para leer los datos y asocialor al DataSet.
                OleDbDataAdapter da = new OleDbDataAdapter(oledbCmd);
                da.Fill(DS);
                //////Archivo = File.Open(Ruta + "." + Extension, FileMode.Open, FileAccess.Read);
                ////if (Extension.Equals("xls"))
                ////{
                ////    //excelReader = ExcelReaderFactory.CreateBinaryReader(Archivo);
                ////    Conexion.ConnectionString= @"Provider=Microsoft.Jet.OLEDB.4.0;" +
                ////    "Data Source=" + Ruta + ";" +
                ////    "Extended Properties= Excel 8.0;";
                ////}
                ////else if (Extension.Equals("xlsx")) 
                ////{
                ////    //excelReader = ExcelReaderFactory.CreateOpenXmlReader(Archivo);
                ////    Conexion.ConnectionString = @"Provider=Microsoft.ACE.OLEDB.12.0;" +
                ////    "Data Source=" + Ruta + ";" +
                ////    "Extended Properties=\"Excel 12.0 Xml;HDR=YES\"";
                ////}
                ////if (!String.IsNullOrEmpty(Conexion.ConnectionString))//Conexion.ConnectionString))
                ////{
                ////    Conexion.Open();
                ////    Comando.CommandText = "SELECT * FROM [Sheet2$]";
                ////    Comando.Connection = Conexion;
                ////    Adaptador.SelectCommand = Comando;
                ////    Adaptador.Fill(Ds_Datos);
                ////    Conexion.Close();
                ////}
                Dt_Informacion = DS.Tables[0];
            }
            catch (Exception ex)
            {
                throw new Exception("Error al crear el datatable con consecutivo Erro[" + ex.Message + "]");
            }
            return Dt_Informacion;
        }
    ///********************************************************************************************************
        ///NOMBRE DE LA FUNCIÓN : Crear_Dt_Registros_Carga_Excel
        ///DESCRIPCIÓN          : Metodo para crear las columnas del datatable
        ///PROPIEDADES          :
        ///CREO                 : Leslie González Vázquez
        ///FECHA_CREO           : 21/Abril/2012 
        ///MODIFICO             :
        ///FECHA_MODIFICO       :
        ///CAUSA_MODIFICACIÓN...:
        ///*********************************************************************************************************
        private DataTable Crear_Dt_Registros_Carga_Excel(DataTable Dt_Datos)
        {
            //Cls_Ope_Psp_Pronostico_Ingresos_Negocio Ingresos_Negocio = new Cls_Ope_Psp_Pronostico_Ingresos_Negocio();
            //Cls_Cat_Psp_SubConceptos_Negocio Negocio_Ing = new Cls_Cat_Psp_SubConceptos_Negocio();
            DataTable Dt_Registros = new DataTable();
            DataTable Dt_Fte_Financiamiento = new DataTable();
            DataTable Dt_Conceptos = new DataTable();
            String Concepto = String.Empty;
            String FF = String.Empty;
            String Concepto_ID = String.Empty;
            String Rubro_ID = String.Empty;
            String SubConcepto_ID = String.Empty;
            String Clase_ID = String.Empty;
            String Tipo_ID = String.Empty;
            String FF_ID = String.Empty;
            DataRow Fila;
            Int32 Contador = -1;
            DataTable Dt_Pronostico_Ing = new DataTable();
            Boolean Repetido = false;
            String Cuenta = "";

            try
            {
                Dt_Registros.Columns.Add("NUMERO_CUENTA", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("FECHA_INGRESO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("FECHA_VALOR", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("REFERENCIA_CLIENTE", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("REFERENCIA_BANCO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("DESCRIPCION", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("MONTO", System.Type.GetType("System.String"));
                Dt_Registros.Columns.Add("NO_POLIZA", System.Type.GetType("System.String"));

                if (Dt_Datos != null)
                {
                    if (Dt_Datos.Rows.Count > 0)
                    {
                        String Nombre_columna= Dt_Datos.Columns[0].ColumnName.ToString();
                        for(int i=0;i<Dt_Datos.Rows.Count;i++)
                        {
                            if(Dt_Datos.Rows[i][0].ToString()=="Número y Nombre de la Cuenta"){
                                Cuenta =Dt_Datos.Rows[i][1].ToString();
                                while (Dt_Datos.Rows[i][0].ToString() != "Fecha de Ingreso")
                                {
                                    i = i + 1;
                                }
                                if(Dt_Datos.Rows[i][0].ToString()=="Fecha de Ingreso"){
                                    i=i+1;
                                    while(Dt_Datos.Rows[i][0].ToString()!="" && Dt_Datos.Rows[i][0].ToString()!="Nombre del Banco"&& Dt_Datos.Rows[i][0].ToString()!="Criterios de Selección"){
                                        Fila = Dt_Registros.NewRow();
                                        Fila["NUMERO_CUENTA"] = Cuenta;
                                        Fila["FECHA_INGRESO"]=Dt_Datos.Rows[i][0].ToString();
                                        Fila["FECHA_VALOR"]=Dt_Datos.Rows[i][1].ToString();
                                        Fila["REFERENCIA_CLIENTE"] = Dt_Datos.Rows[i][2].ToString();
                                        Fila["REFERENCIA_BANCO"] = Dt_Datos.Rows[i][3].ToString();
                                        Fila["DESCRIPCION"] = Dt_Datos.Rows[i][4].ToString();
                                        Fila["MONTO"] = Dt_Datos.Rows[i][6].ToString();
                                        Fila["NO_POLIZA"]="";
                                        Dt_Registros.Rows.Add(Fila);
                                        Dt_Registros.AcceptChanges();
                                        i = i + 1;
                                        }
                                }
                            }
                         }
                     }
                }
                return Dt_Registros;
            }
            catch (Exception ex)
            {
                throw new Exception("Error al tratar de crear las columnas de la tabla Error[" + ex.Message + "]");
            }
        }
    protected void Btn_Salir_Click(object sender, ImageClickEventArgs e)
    {
        Session["Archivo"] = null;
        Response.Redirect("../Paginas_Generales/Frm_Apl_Principal.aspx");
    }
    ///********************************************************************************************************
    ///NOMBRE DE LA FUNCIÓN : Cmb_Banco_Transferencia_OnSelectedIndexChanged
    ///DESCRIPCIÓN          : Metodo para llenar el combo de cuentas por registro dependiendo la fila que se selecciono
    ///PROPIEDADES          :
    ///CREO                 : Sergio Manuel Gallardo Andrade
    ///FECHA_CREO           : 12/Junio/2012
    ///MODIFICO             :
    ///FECHA_MODIFICO       :
    ///CAUSA_MODIFICACIÓN...:
    ///*********************************************************************************************************
    protected void Cmb_Banco_Transferencia_OnSelectedIndexChanged(object sender, EventArgs e)
    {

        Cls_Ope_Con_Cheques_Bancos_Negocio Rs_Consultar_Cuentas_Banco = new Cls_Ope_Con_Cheques_Bancos_Negocio();
        DataTable Dt_Existencia = new DataTable();
        try
        {
            Rs_Consultar_Cuentas_Banco.P_Nombre_Banco = Cmb_Banco.SelectedItem.Text.ToString();
            Dt_Existencia = Rs_Consultar_Cuentas_Banco.Consultar_Cuenta_Bancos_con_Descripcion();
            Cls_Util.Llenar_Combo_Con_DataTable_Generico(Cmb_Cuenta , Dt_Existencia, "CUENTA", "BANCO_ID");
        }
        catch (Exception Ex)
        {
            throw new Exception(Ex.Message);
        }
    }
    #region Mostrar_Poliza 
    protected void Btn_Poliza_Click(object sender, EventArgs e)
    {
        String fecha = ((LinkButton)sender).CommandArgument;
        String Tipo_poliza = ((LinkButton)sender).CssClass;
        String No_poliza = ((LinkButton)sender).Text;
        Imprimir(No_poliza, Tipo_poliza, fecha);
    }
    protected void Imprimir(String NO_POLIZA, String TIPO_POLIZA, String FECHA)
    {
        DataSet Ds_Reporte = null;
        DataTable Dt_Pagos = null;
        String Mes = "";
        String Ano = "";
        try
        {
            Mes = FECHA.Substring(0, 2);
            Ano = FECHA.Substring(2, 2);
            Cls_Ope_Con_Polizas_Negocio Poliza = new Cls_Ope_Con_Polizas_Negocio();
            Ds_Reporte = new DataSet();
            Poliza.P_No_Poliza = NO_POLIZA;
            Poliza.P_Tipo_Poliza_ID = TIPO_POLIZA;
            Poliza.P_Mes_Ano = Mes + Ano;
            Dt_Pagos = Poliza.Consulta_Detalle_Poliza();
            if (Dt_Pagos.Rows.Count > 0)
            {
                Dt_Pagos.TableName = "Dt_Datos_Poliza";
                Ds_Reporte.Tables.Add(Dt_Pagos.Copy());
                //Se llama al método que ejecuta la operación de generar el reporte.
                Generar_Reporte(ref Ds_Reporte, "Rpt_Con_Poliza.rpt", "Poliza" + NO_POLIZA, ".pdf");
            }
        }
        //}
        catch (Exception Ex)
        {
            //Lbl_Mensaje_Error.Text = Ex.Message.ToString();
            //Lbl_Mensaje_Error.Visible = true;
        }

    }
    /// *************************************************************************************
    /// NOMBRE:             Generar_Reporte
    /// DESCRIPCIÓN:        Método que invoca la generación del reporte.
    ///              
    /// PARÁMETROS:         Ds_Reporte_Crystal.- Es el DataSet con el que se muestra el reporte en cristal 
    ///                     Ruta_Reporte_Crystal.-  Ruta y Nombre del archivo del Crystal Report.
    ///                     Nombre_Reporte_Generar.- Nombre que tendrá el reporte generado.
    ///                     Formato.- Es el tipo de reporte "PDF", "Excel"
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:15 p.m.
    /// USUARIO MODIFICO:   Salvador Henrnandez Ramirez
    /// FECHA MODIFICO:     16/Mayo/2011
    /// CAUSA MODIFICACIÓN: Se cambio Nombre_Plantilla_Reporte por Ruta_Reporte_Crystal, ya que este contendrá tambien la ruta
    ///                     y se asigno la opción para que se tenga acceso al método que muestra el reporte en Excel.
    /// *************************************************************************************
    public void Generar_Reporte(ref DataSet Ds_Reporte_Crystal, String Ruta_Reporte_Crystal, String Nombre_Reporte_Generar, String Formato)
    {
        ReportDocument Reporte = new ReportDocument(); // Variable de tipo reporte.
        String Ruta = String.Empty;  // Variable que almacenará la ruta del archivo del crystal report. 

        try
        {
            Ruta = @Server.MapPath("../Rpt/Contabilidad/" + Ruta_Reporte_Crystal);
            Reporte.Load(Ruta);

            if (Ds_Reporte_Crystal is DataSet)
            {
                if (Ds_Reporte_Crystal.Tables.Count > 0)
                {
                    Reporte.SetDataSource(Ds_Reporte_Crystal);
                    Exportar_Reporte_PDF(Reporte, Nombre_Reporte_Generar + Formato);
                    Mostrar_Poliza(Nombre_Reporte_Generar, Formato);
                }
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al generar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:             Exportar_Reporte_PDF
    /// DESCRIPCIÓN:        Método que guarda el reporte generado en formato PDF en la ruta
    ///                     especificada.
    /// PARÁMETROS:         Reporte.- Objeto de tipo documento que contiene el reporte a guardar.
    ///                     Nombre_Reporte.- Nombre que se le dio al reporte.
    /// USUARIO CREO:       Juan Alberto Hernández Negrete.
    /// FECHA CREO:         3/Mayo/2011 18:19 p.m.
    /// USUARIO MODIFICO:
    /// FECHA MODIFICO:
    /// CAUSA MODIFICACIÓN:
    /// *************************************************************************************
    public void Exportar_Reporte_PDF(ReportDocument Reporte, String Nombre_Reporte_Generar)
    {
        ExportOptions Opciones_Exportacion = new ExportOptions();
        DiskFileDestinationOptions Direccion_Guardar_Disco = new DiskFileDestinationOptions();
        PdfRtfWordFormatOptions Opciones_Formato_PDF = new PdfRtfWordFormatOptions();

        try
        {
            if (Reporte is ReportDocument)
            {
                Direccion_Guardar_Disco.DiskFileName = @Server.MapPath("../../Reporte/" + Nombre_Reporte_Generar);
                Opciones_Exportacion.ExportDestinationOptions = Direccion_Guardar_Disco;
                Opciones_Exportacion.ExportDestinationType = ExportDestinationType.DiskFile;
                Opciones_Exportacion.ExportFormatType = ExportFormatType.PortableDocFormat;
                Reporte.Export(Opciones_Exportacion);
            }
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al exportar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    /// *************************************************************************************
    /// NOMBRE:              Mostrar_Reporte
    /// DESCRIPCIÓN:         Muestra el reporte en pantalla.
    /// PARÁMETROS:          Nombre_Reporte_Generar.- Nombre que tiene el reporte que se mostrará en pantalla.
    ///                      Formato.- Variable que contiene el formato en el que se va a generar el reporte "PDF" O "Excel"
    /// USUARIO CREO:        Juan Alberto Hernández Negrete.
    /// FECHA CREO:          3/Mayo/2011 18:20 p.m.
    /// USUARIO MODIFICO:    Salvador Hernández Ramírez
    /// FECHA MODIFICO:      16-Mayo-2011
    /// CAUSA MODIFICACIÓN:  Se asigno la opción para que en el mismo método se muestre el reporte en excel
    /// *************************************************************************************
    protected void Mostrar_Poliza(String Nombre_Reporte_Generar, String Formato)
    {
        String Pagina = "../Paginas_Generales/Frm_Apl_Mostrar_Reportes.aspx?Reporte=";

        try
        {
            Pagina = Pagina + Nombre_Reporte_Generar + Formato;
            ScriptManager.RegisterStartupScript(this, this.GetType(), "open",
                "window.open('" + Pagina + "', 'Reportes','toolbar=0,directories=0,menubar=0,status=0,scrollbars=0,resizable=1,width=1000,height=600')", true);
        }
        catch (Exception Ex)
        {
            throw new Exception("Error al mostrar el reporte. Error: [" + Ex.Message + "]");
        }
    }
    #endregion
}
